﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Payment.Api.Models;
using Znode.Engine.Payment.Api.PaymentProviders;

namespace Znode.Engine.Payment.Api.Controllers
{
    public class PaymentController : Controller
    {
        private IPaymentProviders _provider = null;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        public ActionResult Index(PaymentModel paymentModel = null)
        {
            if(paymentModel!=null & paymentModel.Products!=null )
                TempData["Products"] = paymentModel.Products;
           return View("Index", paymentModel);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        [System.Web.Mvc.HttpPost]
        public JsonResult SubmitPayment(PaymentModel paymentModel)
        {
            var token = string.Empty;
            var message = string.Empty;
            
            var response = this.ProcessCreditCard(paymentModel);
            if (response.IsSuccess)
            {
                token = response.TransactionId;
            }
            else
            {
                message = response.ResponseText.Replace("<br>", "");
            }
            return Json(new { Token = token, Message = HttpUtility.UrlEncode(message) });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        private GatewayResponseModel ProcessCreditCard(PaymentModel paymentModel)
        {
            GatewayType gatewayType;

            paymentModel = DecryptModel(paymentModel);
           
            //TODO: moke - 3-11-2015 - consider using StructureMap (we already use this DI container in some of our apps)
            // dependency injection framework to scan for all implementations 
            // of IPaymentProvider.  This makes this much more dynamic, 
            // and znode customers can add their own IPaymentProvider implementations
            Enum.TryParse(paymentModel.GatewayType, out gatewayType);
            switch (gatewayType)
            {
                case GatewayType.AuthorizeNet:
                    _provider = new AuthroizeNetProvider();
                    break;
                case GatewayType.CyberSource:
                    _provider = new CyberSourceProvider();
                    break;
                case GatewayType.Stripe:
                    _provider = new StripeProvider();
                    break;
            }

            var response = _provider.ValidateCreditcard(paymentModel);

            return response;
        }

        private PaymentModel DecryptModel(PaymentModel paymentModel)
        {
            paymentModel.BillingCity = Decrypt(HttpUtility.UrlDecode(paymentModel.BillingCity));
            paymentModel.BillingCountryCode = Decrypt(HttpUtility.UrlDecode(paymentModel.BillingCountryCode));
            paymentModel.BillingEmailId = Decrypt(HttpUtility.UrlDecode(paymentModel.BillingEmailId));
            paymentModel.BillingFirstName = Decrypt(HttpUtility.UrlDecode(paymentModel.BillingFirstName));
            paymentModel.BillingLastName = Decrypt(HttpUtility.UrlDecode(paymentModel.BillingLastName));
            paymentModel.BillingName = Decrypt(HttpUtility.UrlDecode(paymentModel.BillingName));
            paymentModel.BillingPhoneNumber = Decrypt(HttpUtility.UrlDecode(paymentModel.BillingPhoneNumber));
            paymentModel.BillingPostalCode = Decrypt(HttpUtility.UrlDecode(paymentModel.BillingPostalCode));
            paymentModel.BillingStateCode = Decrypt(HttpUtility.UrlDecode(paymentModel.BillingStateCode));
            paymentModel.BillingStreetAddress1 = Decrypt(HttpUtility.UrlDecode(paymentModel.BillingStreetAddress1));
            paymentModel.BillingStreetAddress2 = Decrypt(HttpUtility.UrlDecode(paymentModel.BillingStreetAddress2));

            paymentModel.ShippingCity = Decrypt(HttpUtility.UrlDecode(paymentModel.ShippingCity));
            paymentModel.ShippingCountryCode = Decrypt(HttpUtility.UrlDecode(paymentModel.ShippingCountryCode));
            paymentModel.ShippingEmailId = Decrypt(HttpUtility.UrlDecode(paymentModel.ShippingEmailId));
            paymentModel.ShippingFirstName = Decrypt(HttpUtility.UrlDecode(paymentModel.ShippingFirstName));
            paymentModel.ShippingLastName = Decrypt(HttpUtility.UrlDecode(paymentModel.ShippingLastName));
            paymentModel.ShippingName = Decrypt(HttpUtility.UrlDecode(paymentModel.ShippingName));
            paymentModel.ShippingPhoneNumber = Decrypt(HttpUtility.UrlDecode(paymentModel.ShippingPhoneNumber));
            paymentModel.ShippingPostalCode = Decrypt(HttpUtility.UrlDecode(paymentModel.ShippingPostalCode));
            paymentModel.ShippingStateCode = Decrypt(HttpUtility.UrlDecode(paymentModel.ShippingStateCode));
            paymentModel.ShippingStreetAddress1 = Decrypt(HttpUtility.UrlDecode(paymentModel.ShippingStreetAddress1));
            paymentModel.ShippingStreetAddress2 = Decrypt(HttpUtility.UrlDecode(paymentModel.ShippingStreetAddress2));


            paymentModel.Discount = Decrypt(HttpUtility.UrlDecode(paymentModel.Discount.ToString()));
            paymentModel.GatewayCurrencyCode = Decrypt(HttpUtility.UrlDecode(paymentModel.GatewayCurrencyCode));
            paymentModel.GatewayCustom1 = Decrypt(HttpUtility.UrlDecode(paymentModel.GatewayCustom1));
            paymentModel.GatewayLoginName = Decrypt(HttpUtility.UrlDecode(paymentModel.GatewayLoginName));
            paymentModel.GatewayLoginPassword = Decrypt(HttpUtility.UrlDecode(paymentModel.GatewayLoginPassword));
            paymentModel.GatewayTransactionKey = Decrypt(HttpUtility.UrlDecode(paymentModel.GatewayTransactionKey));
            paymentModel.GatewayType = Decrypt(HttpUtility.UrlDecode(paymentModel.GatewayType));

            paymentModel.SubTotal = Decrypt(HttpUtility.UrlDecode(paymentModel.SubTotal));
            paymentModel.Total = Decrypt(HttpUtility.UrlDecode(paymentModel.Total));
            paymentModel.TaxCost = Decrypt(HttpUtility.UrlDecode(paymentModel.TaxCost));
            paymentModel.OrderId = Decrypt(HttpUtility.UrlDecode(paymentModel.OrderId));
            paymentModel.ShippingCost = Decrypt(HttpUtility.UrlDecode(paymentModel.ShippingCost));

            if(TempData["Products"]!=null)
            paymentModel.Products = (List<ProductModel>) TempData["Products"];
            return paymentModel;
        }

        private string Decrypt(string cipherText)
        {
            if (string.IsNullOrEmpty(cipherText))
                return string.Empty;
            const string encryptionKey = "znode123";
            cipherText = cipherText.Replace(" ", "+");
            byte[] cipherBytes = Convert.FromBase64String(cipherText);
            using (Aes encryptor = Aes.Create())
            {
                var pdb = new Rfc2898DeriveBytes(encryptionKey,
                                                 new byte[]
                                                     {
                                                         0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64,
                                                         0x65, 0x76
                                                     });
                if (encryptor != null)
                {
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (var ms = new MemoryStream())
                    {
                        using (var cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            return cipherText;
        }


    }
}