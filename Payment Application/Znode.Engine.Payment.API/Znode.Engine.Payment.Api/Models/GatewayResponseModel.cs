﻿
namespace Znode.Engine.Payment.Api.Models
{
    public class GatewayResponseModel
    {
        public string CardAuthCode { get; set; }
        public bool IsSuccess { get; set; }
        public string GatewayResponseData{ get; set; }
        public string ReferenceNumber { get; set; }
        public string ResponseCode { get; set; }
        public string ResponseText { get; set; }
        public string Token { get; set; }
        public string TransactionId { get; set; }

        
    }
}