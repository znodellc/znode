﻿
using System.Collections.Generic;

namespace Znode.Engine.Payment.Api.Models
{
    public class ProductModel
    {
        public decimal Discount { get; set; }
        public string Description { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public string Sku { get; set; }
        public decimal UnitPrice { get; set; }
      
    }
}