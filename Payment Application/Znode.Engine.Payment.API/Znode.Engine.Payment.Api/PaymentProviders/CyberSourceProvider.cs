﻿using CyberSource.Clients;
using CyberSource.Clients.SoapWebReference;
using System;
using Znode.Engine.Payment.Api.Models;

namespace Znode.Engine.Payment.Api.PaymentProviders
{
    public class CyberSourceProvider : IPaymentProviders
    {
        public GatewayResponseModel ValidateCreditcard(PaymentModel paymentModel)
        {
            var PaymentGatewayResponse = new GatewayResponseModel();
            var request = new RequestMessage
                {
                    merchantReferenceCode = paymentModel.OrderId.ToString(),
                    clientLibrary = "NVP Client",
                    clientEnvironment =
                        Environment.OSVersion.Platform + Environment.OSVersion.Version.ToString() + "-CLR" +
                        Environment.Version.ToString()
                };

            // Replace the generic value with your reference number for the current transaction.

            // Information about application to trouble shoot.
            var billTo = new BillTo
                {
                    firstName = paymentModel.BillingFirstName,
                    lastName = paymentModel.BillingLastName,
                    street1 = paymentModel.BillingStreetAddress1,
                    city = paymentModel.BillingCity,
                    state = paymentModel.BillingStateCode,
                    postalCode = paymentModel.BillingPostalCode,
                    country = paymentModel.BillingCountryCode,
                    email = paymentModel.BillingEmailId,
                    ipAddress = paymentModel.CustomerIpAddress
                };
            request.billTo = billTo;

            var shipTo = new ShipTo
                {
                    firstName = paymentModel.ShippingFirstName,
                    lastName = paymentModel.ShippingLastName,
                    street1 = paymentModel.ShippingStreetAddress1,
                    city = paymentModel.ShippingCity,
                    state = paymentModel.ShippingStateCode,
                    postalCode = paymentModel.ShippingPostalCode,
                    country = paymentModel.ShippingCountryCode,
                    email = paymentModel.ShippingEmailId
                };
            request.shipTo = shipTo;

            request.decisionManager = new DecisionManager { enabled = "false" };

            request.ccAuthService = new CCAuthService { run = "true" };


            // Process using credit card
            // If Authorize only
            if (paymentModel.GatewayPreAuthorize)
            {
                request.ccAuthService = new CCAuthService { run = "true" };
            }
            else
            {
                // If Authorize and Capture
                // Authorize
                request.ccAuthService = new CCAuthService { run = "true" };

                // Authorize and Capture
                request.ccCaptureService = new CCCaptureService();
                request.ccCaptureService.run = "true";
            }

            // Card Details
            var card = new Card
                {
                    fullName = billTo.firstName + " " + billTo.lastName,
                    accountNumber = paymentModel.CardNumber
                };
            var month = paymentModel.CardExpirationMonth;
            var year = paymentModel.CardExpirationYear;
            card.expirationMonth = month;
            card.expirationYear = year;
            card.cvNumber = paymentModel.CardSecurityCode;
            request.card = card;


            // Getting Tax and shipping details
            var purchaseTotals = new PurchaseTotals
                {
                    currency = paymentModel.GatewayCurrencyCode,
                    taxAmount = paymentModel.TaxCost.ToString(),
                    freightAmount = paymentModel.ShippingCost.ToString(),
                    discountAmount = paymentModel.Discount.ToString(),
                    grandTotalAmount = paymentModel.Total.ToString()
                };
            request.purchaseTotals = purchaseTotals;


            try
            {
                ReplyMessage reply = SoapClient.RunTransaction(request);

                // To retrieve individual reply fields, follow these examples.
                PaymentGatewayResponse.GatewayResponseData = reply.ToString();
                PaymentGatewayResponse.ResponseCode = reply.reasonCode;
                PaymentGatewayResponse.ResponseText = this.GetReasoncodeDescription(reply.reasonCode);
                PaymentGatewayResponse.TransactionId = reply.requestID;
                PaymentGatewayResponse.CardAuthCode = reply.requestToken;
                if (reply.reasonCode == "100")
                    PaymentGatewayResponse.IsSuccess = true;

            }
            catch (Exception e)
            {
                PaymentGatewayResponse.ResponseText = this.GetReasoncodeDescription(string.Empty);
            }

            return PaymentGatewayResponse;
        }

        /// <summary>
        /// Get the reason code description
        /// </summary>
        /// <param name="reasonCode">Reason code</param>
        /// <returns>Returns the reason description</returns>
        private string GetReasoncodeDescription(string reasonCode)
        {
            switch (reasonCode)
            {
                case "100":
                    return "Successful transaction.";
                case "203":
                    return "General decline of the card.";
                case "204":
                    return "Insufficient funds in the account.";
                case "208":
                    return "Inactive card or card not authorized for card-not-present transactions.";
                case "210":
                    return "The card has reached the credit limit.";
                case "211":
                    return "Invalid card verification number.";
                case "232":
                    return "The card type is not accepted by the payment processor.";
                case "234":
                    return "There is a problem with your CyberSource merchant configuration.";
                case "235":
                    return "The requested amount exceeds the originally authorized amount. Occurs, for example, if you try to capture an amount larger than the original authorization amount.";
                case "237":
                    return "The authorization has already been reversed.";
                case "238":
                    return "The authorization has already been captured.";
                case "239":
                    return "The requested transaction amount must match the previous transaction amount.";
                case "240":
                    return "The card type sent is invalid or does not correlate with the credit card number.";
                case "241":
                    return "The request ID is invalid.";
                case "243":
                    return "The transaction has already been settled or reversed.";
                case "247":
                    return "You requested a credit for a capture that was previously voided.";
                default:
                    break;
            }

            return "Unable to process, please contact customer support.";
        }
    }
}