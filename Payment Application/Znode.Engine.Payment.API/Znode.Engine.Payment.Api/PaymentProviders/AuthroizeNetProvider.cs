﻿using Znode.Engine.Payment.Api.Models;
using System;
using System.Net;

namespace Znode.Engine.Payment.Api.PaymentProviders
{
    public class AuthroizeNetProvider : IPaymentProviders
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="paymentModel"></param>
        /// <returns></returns>
        public GatewayResponseModel ValidateCreditcard(PaymentModel paymentModel)
        {
            var paymentGatewayResponse = new GatewayResponseModel();
            try
            {
              string baseAddress = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["AuthorizeNetGatewayAddress"]); ;
            string authNetVersion = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["AuthorizeNetVersion"]);
         
           
           
            var objRequest = new WebClient();
            var objInf = new System.Collections.Specialized.NameValueCollection(30);
            var objRetInf = new System.Collections.Specialized.NameValueCollection(30);

            // Merchant account Information
            objInf.Add("x_login", paymentModel.GatewayLoginName);
            objInf.Add("x_tran_key", paymentModel.GatewayTransactionKey);
            objInf.Add("x_version", authNetVersion);

            // Gateway Response settings            
            objInf.Add("x_delim_data", "True");
            objInf.Add("x_relay_response", "False");
            objInf.Add("x_delim_char", ",");
            objInf.Add("x_encap_char", "|");
            objInf.Add("x_method", "CC"); // Payment method

            if (paymentModel.GatewayPreAuthorize)
            {
                objInf.Add("x_type", "AUTH_ONLY"); // authorize
            }
            else
            {
                objInf.Add("x_type", "AUTH_CAPTURE"); // authorize and capture
            }

            objInf.Add("x_currency_code", paymentModel.GatewayCurrencyCode);

            // Billing Address 
            objInf.Add("x_first_name", paymentModel.BillingFirstName);
            objInf.Add("x_last_name", paymentModel.BillingLastName);
            objInf.Add("x_address", paymentModel.BillingStreetAddress1 + " " + paymentModel.BillingStreetAddress2);
            objInf.Add("x_city", paymentModel.BillingCity);
            objInf.Add("x_state", paymentModel.BillingStateCode);
            objInf.Add("x_zip", paymentModel.BillingPostalCode);
            objInf.Add("x_country", paymentModel.BillingCountryCode);
            objInf.Add("x_phone", paymentModel.BillingPhoneNumber);
            objInf.Add("x_email", paymentModel.BillingEmailId);

            // Shipping Information - Optional fields
            if (paymentModel.IsMultipleShipAddress)
            {
                objInf.Add("x_ship_to_first_name", "You have selected multiple addresses");
            }
            else
            {
                objInf.Add("x_ship_to_first_name", paymentModel.ShippingFirstName);
                objInf.Add("x_ship_to_last_name", paymentModel.ShippingLastName);
                objInf.Add("x_ship_to_address", paymentModel.ShippingStreetAddress1 + " " + paymentModel.ShippingStreetAddress2);
                objInf.Add("x_ship_to_city", paymentModel.ShippingCity);
                objInf.Add("x_ship_to_state", paymentModel.ShippingStateCode);
                objInf.Add("x_ship_to_zip", paymentModel.ShippingPostalCode);
                objInf.Add("x_ship_to_country", paymentModel.ShippingCountryCode);
            }


            // Invoice Information
            objInf.Add("x_description", "Description of Order");
            objInf.Add("x_invoice_num", paymentModel.OrderId.ToString());


            objInf.Add("x_card_num", paymentModel.CardNumber);
            objInf.Add("x_exp_date", paymentModel.CardExpiration);
            objInf.Add("x_card_code", paymentModel.CardSecurityCode); // Authorisation code of the card (CCV)                      
            objInf.Add("x_amount", paymentModel.Total.ToString());

            // Additional Customer Information - Customer IP address, to avoid fraud transactions
            objInf.Add("x_customer_ip", paymentModel.CustomerIpAddress);

           
                // Check for Authorize.NET Test mode
                if (paymentModel.GatewayTestMode)
                {
                    objInf.Add("x_test_request", "False");
                }
                else
                {
                    objInf.Add("x_test_request", "False");
                }
                objRequest.BaseAddress = baseAddress;
                // Post the values into Authorize.net Server
                byte[] objRetBytes = objRequest.UploadValues(objRequest.BaseAddress, "POST", objInf);
                string[] objRetVals = System.Text.Encoding.ASCII.GetString(objRetBytes).Split(",".ToCharArray());

                // Transaction Approved if the Response code is 1
                if (objRetVals[0].Trim(char.Parse("|")) == "1")
                {
                    // the transaction was approved!
                    if (objRetVals[6].Trim(char.Parse("|")).Length > 0)
                    {
                        paymentGatewayResponse.TransactionId = objRetVals[6].Trim(char.Parse("|"));
                        paymentGatewayResponse.IsSuccess = true;
                    }
                }
                else
                {
                    paymentGatewayResponse.IsSuccess = false;
                }

                // Set Response code and Response Text
                paymentGatewayResponse.ResponseCode = "Response Code: " + objRetVals[0].Trim(char.Parse("|")) + "<br>";
                paymentGatewayResponse.ResponseText = "Response Reason Code :" + objRetVals[2].Trim(char.Parse("|")) + " <br> Description: " + objRetVals[3].Trim(char.Parse("|"));
                paymentGatewayResponse.ReferenceNumber = "MD5 Hash value :" + objRetVals[37].Trim(char.Parse("|"));
            }
            catch (Exception ex)
            {
                paymentGatewayResponse.IsSuccess = false;
                paymentGatewayResponse.ResponseText = ex.Message;
            }

            // Return Payment GatewayResponseModel
            return paymentGatewayResponse;
        }

        }
    }
