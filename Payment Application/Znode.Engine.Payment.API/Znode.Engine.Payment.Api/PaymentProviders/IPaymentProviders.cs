﻿using Znode.Engine.Payment.Api.Models;

namespace Znode.Engine.Payment.Api.PaymentProviders
{
    /// <summary>
    /// 
    /// </summary>
    public interface IPaymentProviders
    {
        GatewayResponseModel ValidateCreditcard(PaymentModel paymentModel);
    }

    /// <summary>
    /// 
    /// </summary>
    public enum GatewayType
    {
        AuthorizeNet = 1,
        TwoCheckout = 2,
        CyberSource = 3,
        Stripe = 4,
        BrainTree = 5
    }

   
}
