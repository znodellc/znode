﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace TestPayment.Controllers
{
    public class HomeController : Controller
    {
        [HttpPost]
        public ActionResult Index(string orderId)
        {
            //string Url = "http://localhost.paymentapi.com/payment";
            string Url = "http://localhost:56247/payment";
			string Method = "post";
            string FormName = "ccForm_Name";
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            System.Web.HttpContext.Current.Response.Clear();
          
            System.Web.HttpContext.Current.Response.Write("<html><head>");

            System.Web.HttpContext.Current.Response.Write(string.Format("</head><body onload=\"document.{0}.submit()\">", FormName));
            System.Web.HttpContext.Current.Response.Write(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", FormName, Method, Url));
            System.Web.HttpContext.Current.Response.Write("<p><h2 style=\"text-align:center;\">Please wait, your order is being processed and you will be redirected to the payment website.</h2></p>");

            //Order details
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "OrderId", HttpUtility.UrlEncode(Encrypt("1"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "Discount", HttpUtility.UrlEncode(Encrypt("1"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingCost", HttpUtility.UrlEncode(Encrypt("0.00"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "SubTotal", HttpUtility.UrlEncode(Encrypt("5"))));
            System.Web.HttpContext.Current.Response.Write(
               string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "TaxCost", HttpUtility.UrlEncode(Encrypt("1"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "Total", HttpUtility.UrlEncode(Encrypt("5"))));

            // Billing Address 
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingCity",  HttpUtility.UrlEncode(Encrypt("dublin"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingCountryCode",  HttpUtility.UrlEncode(Encrypt("US"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingFirstName",  HttpUtility.UrlEncode(Encrypt("my"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingLastName",  HttpUtility.UrlEncode(Encrypt("name"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingName",  HttpUtility.UrlEncode(Encrypt("name"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingPhoneNumber",  HttpUtility.UrlEncode(Encrypt("3534535345"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingPostalCode",  HttpUtility.UrlEncode(Encrypt("43017"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingStateCode",  HttpUtility.UrlEncode(Encrypt("OH"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingStreetAddress1",  HttpUtility.UrlEncode(Encrypt("test34343"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingStreetAddress2",  HttpUtility.UrlEncode(Encrypt("test"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingEmailId", HttpUtility.UrlEncode(Encrypt("menaga@znode.com"))));

            //Shipping Address
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingCity",  HttpUtility.UrlEncode(Encrypt("dublin"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingCountryCode",  HttpUtility.UrlEncode(Encrypt("US"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingFirstName",  HttpUtility.UrlEncode(Encrypt("dfgdfg"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingLastName",  HttpUtility.UrlEncode(Encrypt("dfgfd"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingName",  HttpUtility.UrlEncode(Encrypt("dfgdfg"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingPhoneNumber",  HttpUtility.UrlEncode(Encrypt("4564564561"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingPostalCode",  HttpUtility.UrlEncode(Encrypt("43017"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingStateCode",  HttpUtility.UrlEncode(Encrypt("OH"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingStreetAddress1",  HttpUtility.UrlEncode(Encrypt("dfgdfgdg dfgfg"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingStreetAddress2",  HttpUtility.UrlEncode(Encrypt("dfgdfg"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingEmailId",  HttpUtility.UrlEncode(Encrypt("menaga@znode.com"))));

            //Gateway Details 
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayLoginName", HttpUtility.UrlEncode(Encrypt("9HyAn87K")))); 
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayLoginPassword",  HttpUtility.UrlEncode(Encrypt("1"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayTestMode", "false"));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayCustom1",  HttpUtility.UrlEncode(Encrypt("1"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayPreAuthorize", "false"));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayTransactionKey",  HttpUtility.UrlEncode(Encrypt("76sErk5k7VR262MS"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayCurrencyCode",  HttpUtility.UrlEncode(Encrypt("USD")))); 
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayType", HttpUtility.UrlEncode(Encrypt("AuthorizeNet")))); 
            System.Web.HttpContext.Current.Response.Write(
               //string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ReturnUrl", "http://localhost.testpayment.com?token="));
               string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ReturnUrl", "http://localhost:62984?token="));
            System.Web.HttpContext.Current.Response.Write(
             //string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "CancelUrl", "http://localhost.testpayment.com?Message="));
              string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "CancelUrl", "http://localhost:62984?Message="));

      //      for (int i = 0; i <= 2; i++)
      //      {
      //          System.Web.HttpContext.Current.Response.Write(
      //     string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "Products[" + i + "].ProductName", "test product" + i));
      //          System.Web.HttpContext.Current.Response.Write(
      //     string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "Products[" + i + "].ProductCode", "test product number" + i));
      //          System.Web.HttpContext.Current.Response.Write(
      //     string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "Products[" + i + "].ProductPrice", 12 + i));
      //          System.Web.HttpContext.Current.Response.Write(
      //    string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "Products[" + i + "].SKU", "SKU" + i));

      //          System.Web.HttpContext.Current.Response.Write(
      //  string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "Products[" + i + "].Discount", 1 + i));
      //          System.Web.HttpContext.Current.Response.Write(
      // string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "Products[" + i + "].UnitPrice", 3 + i));
      //          System.Web.HttpContext.Current.Response.Write(
      //string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "Products[" + i + "].Quantity", 4));

      //    //      System.Web.HttpContext.Current.Response.Write(
      //    //string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "li_" + i + "_AddonCount", 1));
      //    //      System.Web.HttpContext.Current.Response.Write("<ul id=\"addonitems_" + i + "\">");
      //    //      for (int j = 1; j <= 1; j++)
      //    //      {
      //    //          System.Web.HttpContext.Current.Response.Write(
      //    //              string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "li_" + i + "_" + j + "_AddonName", "Addon" + j));

      //    //          System.Web.HttpContext.Current.Response.Write(
      //    //             string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "li_" + i + "_" + j + "_AddonValue", "gift" + j));

      //    //          System.Web.HttpContext.Current.Response.Write(
      //    //             string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "li_" + i + "_" + j + "_AddonPrice", 2 + j));

      //    //      }
      //      }
            System.Web.HttpContext.Current.Response.Write("</form>");
            System.Web.HttpContext.Current.Response.Write("</body></html>");

            System.Web.HttpContext.Current.Response.End();

            return View();
           
        }

        [HttpPost]
        public ActionResult Indexcyber()
        {
            //string Url = "http://localhost.paymentapi.com/payment";
            string Url = "http://localhost:56247/payment";
            string Method = "post";
            string FormName = "ccForm_Name";
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            System.Web.HttpContext.Current.Response.Clear();

            System.Web.HttpContext.Current.Response.Write("<html><head>");

            System.Web.HttpContext.Current.Response.Write(string.Format("</head><body onload=\"document.{0}.submit()\">", FormName));
            System.Web.HttpContext.Current.Response.Write(string.Format("<form name=\"{0}\" method=\"{1}\" action=\"{2}\" >", FormName, Method, Url));
            System.Web.HttpContext.Current.Response.Write("<p><h2 style=\"text-align:center;\">Please wait, your order is being processed and you will be redirected to the payment website.</h2></p>");

            //Order details
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "OrderId", HttpUtility.UrlEncode(Encrypt("1"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "Discount", HttpUtility.UrlEncode(Encrypt("1"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingCost", HttpUtility.UrlEncode(Encrypt("0.00"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "SubTotal", HttpUtility.UrlEncode(Encrypt("5"))));
            System.Web.HttpContext.Current.Response.Write(
               string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "TaxCost", HttpUtility.UrlEncode(Encrypt("1"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "Total", HttpUtility.UrlEncode(Encrypt("5"))));

            // Billing Address 
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingCity", HttpUtility.UrlEncode(Encrypt("dublin"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingCountryCode", HttpUtility.UrlEncode(Encrypt("US"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingFirstName", HttpUtility.UrlEncode(Encrypt("my"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingLastName", HttpUtility.UrlEncode(Encrypt("name"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingName", HttpUtility.UrlEncode(Encrypt("name"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingPhoneNumber", HttpUtility.UrlEncode(Encrypt("3534535345"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingPostalCode", HttpUtility.UrlEncode(Encrypt("43017"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingStateCode", HttpUtility.UrlEncode(Encrypt("OH"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingStreetAddress1", HttpUtility.UrlEncode(Encrypt("test34343"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingStreetAddress2", HttpUtility.UrlEncode(Encrypt("test"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "BillingEmailId", HttpUtility.UrlEncode(Encrypt("menaga@znode.com"))));

            //Shipping Address
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingCity", HttpUtility.UrlEncode(Encrypt("dublin"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingCountryCode", HttpUtility.UrlEncode(Encrypt("US"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingFirstName", HttpUtility.UrlEncode(Encrypt("dfgdfg"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingLastName", HttpUtility.UrlEncode(Encrypt("dfgfd"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingName", HttpUtility.UrlEncode(Encrypt("dfgdfg"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingPhoneNumber", HttpUtility.UrlEncode(Encrypt("4564564561"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingPostalCode", HttpUtility.UrlEncode(Encrypt("43017"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingStateCode", HttpUtility.UrlEncode(Encrypt("OH"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingStreetAddress1", HttpUtility.UrlEncode(Encrypt("dfgdfgdg dfgfg"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingStreetAddress2", HttpUtility.UrlEncode(Encrypt("dfgdfg"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ShippingEmailId", HttpUtility.UrlEncode(Encrypt("menaga@znode.com"))));

            //Gateway Details 
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayLoginName", HttpUtility.UrlEncode(Encrypt("9HyAn87K"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayLoginPassword", HttpUtility.UrlEncode(Encrypt("1"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayTestMode", "false"));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayCustom1", HttpUtility.UrlEncode(Encrypt("1"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayPreAuthorize", "false"));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayTransactionKey", HttpUtility.UrlEncode(Encrypt("76sErk5k7VR262MS"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayCurrencyCode", HttpUtility.UrlEncode(Encrypt("USD"))));
            System.Web.HttpContext.Current.Response.Write(
                string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "GatewayType", HttpUtility.UrlEncode(Encrypt("CyberSource"))));
            System.Web.HttpContext.Current.Response.Write(
               //string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ReturnUrl", "http://localhost.testpayment.com?token="));
               string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "ReturnUrl", "http://localhost:62984?token="));
            System.Web.HttpContext.Current.Response.Write(
             //string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "CancelUrl", "http://localhost.testpayment.com?Message="));
             string.Format("<input name=\"{0}\" type=\"hidden\" value=\"{1}\">", "CancelUrl", "http://localhost:62984?Message="));

            //      for (int i = 0; i <= 2; i++)
            //      {
            //          System.Web.HttpContext.Current.Response.Write(
            //     string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "Products[" + i + "].ProductName", "test product" + i));
            //          System.Web.HttpContext.Current.Response.Write(
            //     string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "Products[" + i + "].ProductCode", "test product number" + i));
            //          System.Web.HttpContext.Current.Response.Write(
            //     string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "Products[" + i + "].ProductPrice", 12 + i));
            //          System.Web.HttpContext.Current.Response.Write(
            //    string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "Products[" + i + "].SKU", "SKU" + i));

            //          System.Web.HttpContext.Current.Response.Write(
            //  string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "Products[" + i + "].Discount", 1 + i));
            //          System.Web.HttpContext.Current.Response.Write(
            // string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "Products[" + i + "].UnitPrice", 3 + i));
            //          System.Web.HttpContext.Current.Response.Write(
            //string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "Products[" + i + "].Quantity", 4));

            //    //      System.Web.HttpContext.Current.Response.Write(
            //    //string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "li_" + i + "_AddonCount", 1));
            //    //      System.Web.HttpContext.Current.Response.Write("<ul id=\"addonitems_" + i + "\">");
            //    //      for (int j = 1; j <= 1; j++)
            //    //      {
            //    //          System.Web.HttpContext.Current.Response.Write(
            //    //              string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "li_" + i + "_" + j + "_AddonName", "Addon" + j));

            //    //          System.Web.HttpContext.Current.Response.Write(
            //    //             string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "li_" + i + "_" + j + "_AddonValue", "gift" + j));

            //    //          System.Web.HttpContext.Current.Response.Write(
            //    //             string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"  />", "li_" + i + "_" + j + "_AddonPrice", 2 + j));

            //    //      }
            //      }
            System.Web.HttpContext.Current.Response.Write("</form>");
            System.Web.HttpContext.Current.Response.Write("</body></html>");

            System.Web.HttpContext.Current.Response.End();

            return View("Index");

        }
        private string Encrypt(string clearText)
        {
            string EncryptionKey = "znode123";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());
                }
            }
            return clearText;
        }
        public ActionResult Index()
        {
            if (!string.IsNullOrEmpty( Request.QueryString["token"]))
                ViewBag.Token = "Token Id : " + Request.QueryString["token"];

            if (!string.IsNullOrEmpty(Request.QueryString["Message"]))
                ViewBag.Message = "Error Message: " + Request.QueryString["Message"];
            return View();
        }
        [HttpGet]
        public ActionResult About()
        {
         
            return View();
        }

      

    }
}
