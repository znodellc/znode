﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace TestPayment.Models
{
    public class PaymentViewModel
    {

        public string BillingCity { get; set; }
        public string BillingCountryCode { get; set; }
        public string BillingFirstName { get; set; }
        public string BillingLastName { get; set; }
        public string BillingName { get; set; }
        public string BillingPhoneNumber { get; set; }
        public string BillingPostalCode { get; set; }
        public string BillingStateCode { get; set; }
        public string BillingStreetAddress1 { get; set; }
        public string BillingStreetAddress2 { get; set; }
        public string BillingEmailId { get; set; }


        [DataType(DataType.CreditCard)]
        [Required(ErrorMessage = "Card Number Required.")]
        [RegularExpression("(^[3|4|5|6]([0-9]{15}$|[0-9]{12}$|[0-9]{13}$|[0-9]{14}$))", ErrorMessage = "Enter Valid Credit Card Number")]
        public string CardNumber { get; set; }

        [Required(ErrorMessage = "Card holder name required.")]
        public string CardHolderName { get; set; }

        [Required(ErrorMessage = "Card Year Required.")]
        public string CardExpirationYear { get; set; }

        [Required(ErrorMessage = "Card Month Required.")]
        public string CardExpirationMonth { get; set; }

        [Required(ErrorMessage = "Card security required.")]
        [StringLength(3, MinimumLength = 3, ErrorMessage = "Card security number must be a 3 digit number.")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "Card security number must be a 3 digit number.")]
        public string CardSecurityCode { get; set; }

        public IEnumerable<SelectListItem> Month
        {
            get
            {
                for (int month = 1; month <= 12; month++)
                {
                    yield return new SelectListItem() { Text = month.ToString("00"), Value = month.ToString("00") };
                }
            }
        }

        public IEnumerable<SelectListItem> Year
        {
            get
            {
                for (int year = DateTime.Now.Year; year <= DateTime.Now.Year + 10; year++)
                {
                    yield return new SelectListItem() { Text = year.ToString("0000"), Value = year.ToString("0000") };
                }
            }
        }

        public string CardExpiration
        {
            get { return CardExpirationMonth + "/" + CardExpirationYear; }
        }


        public string CancelUrl { get; set; }
        
      
        public string CustomerIpAddress { get; set; }
        public decimal Discount { get; set; }

        public string GatewayType { get; set; }
        public string GatewayLoginName { get; set; }
        public string GatewayLoginPassword { get; set; }
        public bool GatewayTestMode { get; set; }
        public string GatewayCustom1 { get; set; }
        public bool GatewayPreAuthorize { get; set; }
        public string GatewayTransactionKey { get; set; }
        public string GatewayCurrencyCode { get; set; }

        public bool IsMultipleShipAddress { get; set; }
        public string OrderId { get; set; }
        public int PaymentSettingId { get; set; }


        public string ShippingCity { get; set; }
        public string ShippingCountryCode { get; set; }
        public string ShippingFirstName { get; set; }
        public string ShippingLastName { get; set; }
        public string ShippingName { get; set; }
        public string ShippingPhoneNumber { get; set; }
        public string ShippingPostalCode { get; set; }
        public string ShippingStateCode { get; set; }
        public string ShippingStreetAddress1 { get; set; }
        public string ShippingStreetAddress2 { get; set; }
        public string ShippingEmailId { get; set; }


        public decimal ShippingCost { get; set; }
        public decimal SubTotal { get; set; }
        public string ReturnUrl { get; set; }
        public decimal TaxCost { get; set; }
        public decimal Total { get; set; }
        public string Token { get; set; }
        public string TransactionId { get; set; }
     
       public PaymentViewModel()
       {
          
       }
        
    }

    public class ResponseModel
    {
        public PaymentViewModel PaymentViewModel { get; set; }
        public int? ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public bool HasError { get; set; }
    }
}