﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
namespace Znode.Libraries.Helpers.Extensions
{
    public static class DataTableExtensions
    {
        public static List<T> ToList<T>(this DataTable table) where T : new()
        {
            List<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            List<T> rowList = new List<T>();

            foreach (DataRow row in table.Rows)
            {
                var item = CreateItemFromRow<T>(row, properties);
                rowList.Add(item);
            }

            return rowList;
        }

        private static T CreateItemFromRow<T>(DataRow row, IList<PropertyInfo> properties) where T : new()
        {
            T item = new T();
            foreach (var property in properties)
            {
                if (row.Table.Columns.Contains(property.Name))
                {
                    object value = row[property.Name];
                    if (value == DBNull.Value)
                        value = null;
                    property.SetValue(item, value, null);
                }
            }
            return item;
        }

        public static T ToSingleRow<T>(this DataRow row) where T : new()
        {
            IList<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            T singleRow = new T();
            singleRow = CreateItemFromRow<T>(row, properties);
            return singleRow;
        }


        public static T ToSingleRow<T>(this DataRow[] rows) where T : new()
        {
            IList<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            T singleRow = new T();
            foreach (DataRow row in rows)
            {
                singleRow = CreateItemFromRow<T>(row, properties);
            }
            return singleRow;
        }

        public static List<T> ToRowList<T>(this  DataRow[] dataRowList) where T : new()
        {
            List<PropertyInfo> properties = typeof(T).GetProperties().ToList();
            List<T> rowList = new List<T>();

            foreach (DataRow row in dataRowList)
            {
                var item = CreateItemFromRow<T>(row, properties);
                rowList.Add(item);
            }

            return rowList;
        }


    }
}
