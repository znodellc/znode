﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Libraries.Helpers.Attributes
{
    /// <summary>
    /// This attribute can be used on properties like HttpPostedFileBase etc.
    /// </summary>
    public class AllowMultiple : Attribute
    {
    }
}
