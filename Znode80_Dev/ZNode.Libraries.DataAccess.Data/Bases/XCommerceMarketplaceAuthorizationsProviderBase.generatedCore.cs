﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="XCommerceMarketplaceAuthorizationsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class XCommerceMarketplaceAuthorizationsProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations, ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizationsKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizationsKey key)
		{
			return Delete(transactionManager, key.AccountID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_accountID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.String _accountID)
		{
			return Delete(null, _accountID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.String _accountID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_XCommerceMarketplaceAuthorizations_XCommerceMarketplaces key.
		///		FK_XCommerceMarketplaceAuthorizations_XCommerceMarketplaces Description: 
		/// </summary>
		/// <param name="_xCommerceMarketplaceID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations objects.</returns>
		public TList<XCommerceMarketplaceAuthorizations> GetByXCommerceMarketplaceID(System.String _xCommerceMarketplaceID)
		{
			int count = -1;
			return GetByXCommerceMarketplaceID(_xCommerceMarketplaceID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_XCommerceMarketplaceAuthorizations_XCommerceMarketplaces key.
		///		FK_XCommerceMarketplaceAuthorizations_XCommerceMarketplaces Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_xCommerceMarketplaceID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations objects.</returns>
		/// <remarks></remarks>
		public TList<XCommerceMarketplaceAuthorizations> GetByXCommerceMarketplaceID(TransactionManager transactionManager, System.String _xCommerceMarketplaceID)
		{
			int count = -1;
			return GetByXCommerceMarketplaceID(transactionManager, _xCommerceMarketplaceID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_XCommerceMarketplaceAuthorizations_XCommerceMarketplaces key.
		///		FK_XCommerceMarketplaceAuthorizations_XCommerceMarketplaces Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_xCommerceMarketplaceID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations objects.</returns>
		public TList<XCommerceMarketplaceAuthorizations> GetByXCommerceMarketplaceID(TransactionManager transactionManager, System.String _xCommerceMarketplaceID, int start, int pageLength)
		{
			int count = -1;
			return GetByXCommerceMarketplaceID(transactionManager, _xCommerceMarketplaceID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_XCommerceMarketplaceAuthorizations_XCommerceMarketplaces key.
		///		fKXCommerceMarketplaceAuthorizationsXCommerceMarketplaces Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_xCommerceMarketplaceID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations objects.</returns>
		public TList<XCommerceMarketplaceAuthorizations> GetByXCommerceMarketplaceID(System.String _xCommerceMarketplaceID, int start, int pageLength)
		{
			int count =  -1;
			return GetByXCommerceMarketplaceID(null, _xCommerceMarketplaceID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_XCommerceMarketplaceAuthorizations_XCommerceMarketplaces key.
		///		fKXCommerceMarketplaceAuthorizationsXCommerceMarketplaces Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_xCommerceMarketplaceID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations objects.</returns>
		public TList<XCommerceMarketplaceAuthorizations> GetByXCommerceMarketplaceID(System.String _xCommerceMarketplaceID, int start, int pageLength,out int count)
		{
			return GetByXCommerceMarketplaceID(null, _xCommerceMarketplaceID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_XCommerceMarketplaceAuthorizations_XCommerceMarketplaces key.
		///		FK_XCommerceMarketplaceAuthorizations_XCommerceMarketplaces Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_xCommerceMarketplaceID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations objects.</returns>
		public abstract TList<XCommerceMarketplaceAuthorizations> GetByXCommerceMarketplaceID(TransactionManager transactionManager, System.String _xCommerceMarketplaceID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizationsKey key, int start, int pageLength)
		{
			return GetByAccountID(transactionManager, key.AccountID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_XCommerceMarketplaceAuthorizations index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations GetByAccountID(System.String _accountID)
		{
			int count = -1;
			return GetByAccountID(null,_accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_XCommerceMarketplaceAuthorizations index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations GetByAccountID(System.String _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_XCommerceMarketplaceAuthorizations index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations GetByAccountID(TransactionManager transactionManager, System.String _accountID)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_XCommerceMarketplaceAuthorizations index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations GetByAccountID(TransactionManager transactionManager, System.String _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_XCommerceMarketplaceAuthorizations index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations GetByAccountID(System.String _accountID, int start, int pageLength, out int count)
		{
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_XCommerceMarketplaceAuthorizations index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations GetByAccountID(TransactionManager transactionManager, System.String _accountID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;XCommerceMarketplaceAuthorizations&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;XCommerceMarketplaceAuthorizations&gt;"/></returns>
		public static TList<XCommerceMarketplaceAuthorizations> Fill(IDataReader reader, TList<XCommerceMarketplaceAuthorizations> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("XCommerceMarketplaceAuthorizations")
					.Append("|").Append((System.String)reader[((int)XCommerceMarketplaceAuthorizationsColumn.AccountID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<XCommerceMarketplaceAuthorizations>(
					key.ToString(), // EntityTrackingKey
					"XCommerceMarketplaceAuthorizations",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AccountID = (System.String)reader[((int)XCommerceMarketplaceAuthorizationsColumn.AccountID - 1)];
					c.OriginalAccountID = c.AccountID;
					c.UserID = (System.String)reader[((int)XCommerceMarketplaceAuthorizationsColumn.UserID - 1)];
					c.Environment = (System.Int32)reader[((int)XCommerceMarketplaceAuthorizationsColumn.Environment - 1)];
					c.XCommerceMarketplaceID = (reader.IsDBNull(((int)XCommerceMarketplaceAuthorizationsColumn.XCommerceMarketplaceID - 1)))?null:(System.String)reader[((int)XCommerceMarketplaceAuthorizationsColumn.XCommerceMarketplaceID - 1)];
					c.Expiration = (reader.IsDBNull(((int)XCommerceMarketplaceAuthorizationsColumn.Expiration - 1)))?null:(System.DateTime?)reader[((int)XCommerceMarketplaceAuthorizationsColumn.Expiration - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations entity)
		{
			if (!reader.Read()) return;
			
			entity.AccountID = (System.String)reader[((int)XCommerceMarketplaceAuthorizationsColumn.AccountID - 1)];
			entity.OriginalAccountID = (System.String)reader["AccountID"];
			entity.UserID = (System.String)reader[((int)XCommerceMarketplaceAuthorizationsColumn.UserID - 1)];
			entity.Environment = (System.Int32)reader[((int)XCommerceMarketplaceAuthorizationsColumn.Environment - 1)];
			entity.XCommerceMarketplaceID = (reader.IsDBNull(((int)XCommerceMarketplaceAuthorizationsColumn.XCommerceMarketplaceID - 1)))?null:(System.String)reader[((int)XCommerceMarketplaceAuthorizationsColumn.XCommerceMarketplaceID - 1)];
			entity.Expiration = (reader.IsDBNull(((int)XCommerceMarketplaceAuthorizationsColumn.Expiration - 1)))?null:(System.DateTime?)reader[((int)XCommerceMarketplaceAuthorizationsColumn.Expiration - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AccountID = (System.String)dataRow["AccountID"];
			entity.OriginalAccountID = (System.String)dataRow["AccountID"];
			entity.UserID = (System.String)dataRow["UserID"];
			entity.Environment = (System.Int32)dataRow["Environment"];
			entity.XCommerceMarketplaceID = Convert.IsDBNull(dataRow["XCommerceMarketplaceID"]) ? null : (System.String)dataRow["XCommerceMarketplaceID"];
			entity.Expiration = Convert.IsDBNull(dataRow["Expiration"]) ? null : (System.DateTime?)dataRow["Expiration"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region XCommerceMarketplaceIDSource	
			if (CanDeepLoad(entity, "XCommerceMarketplaces|XCommerceMarketplaceIDSource", deepLoadType, innerList) 
				&& entity.XCommerceMarketplaceIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.XCommerceMarketplaceID ?? string.Empty);
				XCommerceMarketplaces tmpEntity = EntityManager.LocateEntity<XCommerceMarketplaces>(EntityLocator.ConstructKeyFromPkItems(typeof(XCommerceMarketplaces), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.XCommerceMarketplaceIDSource = tmpEntity;
				else
					entity.XCommerceMarketplaceIDSource = DataRepository.XCommerceMarketplacesProvider.GetByXCommerceMarketplaceID(transactionManager, (entity.XCommerceMarketplaceID ?? string.Empty));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'XCommerceMarketplaceIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.XCommerceMarketplaceIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.XCommerceMarketplacesProvider.DeepLoad(transactionManager, entity.XCommerceMarketplaceIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion XCommerceMarketplaceIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region XCommerceMarketplaceIDSource
			if (CanDeepSave(entity, "XCommerceMarketplaces|XCommerceMarketplaceIDSource", deepSaveType, innerList) 
				&& entity.XCommerceMarketplaceIDSource != null)
			{
				DataRepository.XCommerceMarketplacesProvider.Save(transactionManager, entity.XCommerceMarketplaceIDSource);
				entity.XCommerceMarketplaceID = entity.XCommerceMarketplaceIDSource.XCommerceMarketplaceID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region XCommerceMarketplaceAuthorizationsChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.XCommerceMarketplaceAuthorizations</c>
	///</summary>
	public enum XCommerceMarketplaceAuthorizationsChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>XCommerceMarketplaces</c> at XCommerceMarketplaceIDSource
		///</summary>
		[ChildEntityType(typeof(XCommerceMarketplaces))]
		XCommerceMarketplaces,
	}
	
	#endregion XCommerceMarketplaceAuthorizationsChildEntityTypes
	
	#region XCommerceMarketplaceAuthorizationsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;XCommerceMarketplaceAuthorizationsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="XCommerceMarketplaceAuthorizations"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class XCommerceMarketplaceAuthorizationsFilterBuilder : SqlFilterBuilder<XCommerceMarketplaceAuthorizationsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the XCommerceMarketplaceAuthorizationsFilterBuilder class.
		/// </summary>
		public XCommerceMarketplaceAuthorizationsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the XCommerceMarketplaceAuthorizationsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public XCommerceMarketplaceAuthorizationsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the XCommerceMarketplaceAuthorizationsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public XCommerceMarketplaceAuthorizationsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion XCommerceMarketplaceAuthorizationsFilterBuilder
	
	#region XCommerceMarketplaceAuthorizationsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;XCommerceMarketplaceAuthorizationsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="XCommerceMarketplaceAuthorizations"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class XCommerceMarketplaceAuthorizationsParameterBuilder : ParameterizedSqlFilterBuilder<XCommerceMarketplaceAuthorizationsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the XCommerceMarketplaceAuthorizationsParameterBuilder class.
		/// </summary>
		public XCommerceMarketplaceAuthorizationsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the XCommerceMarketplaceAuthorizationsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public XCommerceMarketplaceAuthorizationsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the XCommerceMarketplaceAuthorizationsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public XCommerceMarketplaceAuthorizationsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion XCommerceMarketplaceAuthorizationsParameterBuilder
	
	#region XCommerceMarketplaceAuthorizationsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;XCommerceMarketplaceAuthorizationsColumn&gt;"/> class
	/// that is used exclusively with a <see cref="XCommerceMarketplaceAuthorizations"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class XCommerceMarketplaceAuthorizationsSortBuilder : SqlSortBuilder<XCommerceMarketplaceAuthorizationsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the XCommerceMarketplaceAuthorizationsSqlSortBuilder class.
		/// </summary>
		public XCommerceMarketplaceAuthorizationsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion XCommerceMarketplaceAuthorizationsSortBuilder
	
} // end namespace
