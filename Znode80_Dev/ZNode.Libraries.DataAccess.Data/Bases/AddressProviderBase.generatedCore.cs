﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AddressProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AddressProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Address, ZNode.Libraries.DataAccess.Entities.AddressKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AddressKey key)
		{
			return Delete(transactionManager, key.AddressID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_addressID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _addressID)
		{
			return Delete(null, _addressID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addressID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _addressID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Address Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AddressKey key, int start, int pageLength)
		{
			return GetByAddressID(transactionManager, key.AddressID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAddress_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Address&gt;"/> class.</returns>
		public TList<Address> GetByAccountID(System.Int32 _accountID)
		{
			int count = -1;
			return GetByAccountID(null,_accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAddress_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Address&gt;"/> class.</returns>
		public TList<Address> GetByAccountID(System.Int32 _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAddress_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Address&gt;"/> class.</returns>
		public TList<Address> GetByAccountID(TransactionManager transactionManager, System.Int32 _accountID)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAddress_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Address&gt;"/> class.</returns>
		public TList<Address> GetByAccountID(TransactionManager transactionManager, System.Int32 _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAddress_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Address&gt;"/> class.</returns>
		public TList<Address> GetByAccountID(System.Int32 _accountID, int start, int pageLength, out int count)
		{
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAddress_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Address&gt;"/> class.</returns>
		public abstract TList<Address> GetByAccountID(TransactionManager transactionManager, System.Int32 _accountID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeAddress index.
		/// </summary>
		/// <param name="_addressID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Address"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Address GetByAddressID(System.Int32 _addressID)
		{
			int count = -1;
			return GetByAddressID(null,_addressID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeAddress index.
		/// </summary>
		/// <param name="_addressID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Address"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Address GetByAddressID(System.Int32 _addressID, int start, int pageLength)
		{
			int count = -1;
			return GetByAddressID(null, _addressID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeAddress index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addressID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Address"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Address GetByAddressID(TransactionManager transactionManager, System.Int32 _addressID)
		{
			int count = -1;
			return GetByAddressID(transactionManager, _addressID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeAddress index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addressID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Address"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Address GetByAddressID(TransactionManager transactionManager, System.Int32 _addressID, int start, int pageLength)
		{
			int count = -1;
			return GetByAddressID(transactionManager, _addressID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeAddress index.
		/// </summary>
		/// <param name="_addressID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Address"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Address GetByAddressID(System.Int32 _addressID, int start, int pageLength, out int count)
		{
			return GetByAddressID(null, _addressID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeAddress index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_addressID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Address"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Address GetByAddressID(TransactionManager transactionManager, System.Int32 _addressID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Address&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Address&gt;"/></returns>
		public static TList<Address> Fill(IDataReader reader, TList<Address> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Address c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Address")
					.Append("|").Append((System.Int32)reader[((int)AddressColumn.AddressID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Address>(
					key.ToString(), // EntityTrackingKey
					"Address",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Address();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AddressID = (System.Int32)reader[((int)AddressColumn.AddressID - 1)];
					c.FirstName = (reader.IsDBNull(((int)AddressColumn.FirstName - 1)))?null:(System.String)reader[((int)AddressColumn.FirstName - 1)];
					c.MiddleName = (reader.IsDBNull(((int)AddressColumn.MiddleName - 1)))?null:(System.String)reader[((int)AddressColumn.MiddleName - 1)];
					c.LastName = (reader.IsDBNull(((int)AddressColumn.LastName - 1)))?null:(System.String)reader[((int)AddressColumn.LastName - 1)];
					c.CompanyName = (reader.IsDBNull(((int)AddressColumn.CompanyName - 1)))?null:(System.String)reader[((int)AddressColumn.CompanyName - 1)];
					c.Street = (reader.IsDBNull(((int)AddressColumn.Street - 1)))?null:(System.String)reader[((int)AddressColumn.Street - 1)];
					c.Street1 = (reader.IsDBNull(((int)AddressColumn.Street1 - 1)))?null:(System.String)reader[((int)AddressColumn.Street1 - 1)];
					c.City = (reader.IsDBNull(((int)AddressColumn.City - 1)))?null:(System.String)reader[((int)AddressColumn.City - 1)];
					c.StateCode = (reader.IsDBNull(((int)AddressColumn.StateCode - 1)))?null:(System.String)reader[((int)AddressColumn.StateCode - 1)];
					c.PostalCode = (System.String)reader[((int)AddressColumn.PostalCode - 1)];
					c.CountryCode = (reader.IsDBNull(((int)AddressColumn.CountryCode - 1)))?null:(System.String)reader[((int)AddressColumn.CountryCode - 1)];
					c.PhoneNumber = (reader.IsDBNull(((int)AddressColumn.PhoneNumber - 1)))?null:(System.String)reader[((int)AddressColumn.PhoneNumber - 1)];
					c.IsDefaultBilling = (System.Boolean)reader[((int)AddressColumn.IsDefaultBilling - 1)];
					c.AccountID = (System.Int32)reader[((int)AddressColumn.AccountID - 1)];
					c.IsDefaultShipping = (System.Boolean)reader[((int)AddressColumn.IsDefaultShipping - 1)];
					c.Name = (System.String)reader[((int)AddressColumn.Name - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Address"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Address"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Address entity)
		{
			if (!reader.Read()) return;
			
			entity.AddressID = (System.Int32)reader[((int)AddressColumn.AddressID - 1)];
			entity.FirstName = (reader.IsDBNull(((int)AddressColumn.FirstName - 1)))?null:(System.String)reader[((int)AddressColumn.FirstName - 1)];
			entity.MiddleName = (reader.IsDBNull(((int)AddressColumn.MiddleName - 1)))?null:(System.String)reader[((int)AddressColumn.MiddleName - 1)];
			entity.LastName = (reader.IsDBNull(((int)AddressColumn.LastName - 1)))?null:(System.String)reader[((int)AddressColumn.LastName - 1)];
			entity.CompanyName = (reader.IsDBNull(((int)AddressColumn.CompanyName - 1)))?null:(System.String)reader[((int)AddressColumn.CompanyName - 1)];
			entity.Street = (reader.IsDBNull(((int)AddressColumn.Street - 1)))?null:(System.String)reader[((int)AddressColumn.Street - 1)];
			entity.Street1 = (reader.IsDBNull(((int)AddressColumn.Street1 - 1)))?null:(System.String)reader[((int)AddressColumn.Street1 - 1)];
			entity.City = (reader.IsDBNull(((int)AddressColumn.City - 1)))?null:(System.String)reader[((int)AddressColumn.City - 1)];
			entity.StateCode = (reader.IsDBNull(((int)AddressColumn.StateCode - 1)))?null:(System.String)reader[((int)AddressColumn.StateCode - 1)];
			entity.PostalCode = (System.String)reader[((int)AddressColumn.PostalCode - 1)];
			entity.CountryCode = (reader.IsDBNull(((int)AddressColumn.CountryCode - 1)))?null:(System.String)reader[((int)AddressColumn.CountryCode - 1)];
			entity.PhoneNumber = (reader.IsDBNull(((int)AddressColumn.PhoneNumber - 1)))?null:(System.String)reader[((int)AddressColumn.PhoneNumber - 1)];
			entity.IsDefaultBilling = (System.Boolean)reader[((int)AddressColumn.IsDefaultBilling - 1)];
			entity.AccountID = (System.Int32)reader[((int)AddressColumn.AccountID - 1)];
			entity.IsDefaultShipping = (System.Boolean)reader[((int)AddressColumn.IsDefaultShipping - 1)];
			entity.Name = (System.String)reader[((int)AddressColumn.Name - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Address"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Address"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Address entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AddressID = (System.Int32)dataRow["AddressID"];
			entity.FirstName = Convert.IsDBNull(dataRow["FirstName"]) ? null : (System.String)dataRow["FirstName"];
			entity.MiddleName = Convert.IsDBNull(dataRow["MiddleName"]) ? null : (System.String)dataRow["MiddleName"];
			entity.LastName = Convert.IsDBNull(dataRow["LastName"]) ? null : (System.String)dataRow["LastName"];
			entity.CompanyName = Convert.IsDBNull(dataRow["CompanyName"]) ? null : (System.String)dataRow["CompanyName"];
			entity.Street = Convert.IsDBNull(dataRow["Street"]) ? null : (System.String)dataRow["Street"];
			entity.Street1 = Convert.IsDBNull(dataRow["Street1"]) ? null : (System.String)dataRow["Street1"];
			entity.City = Convert.IsDBNull(dataRow["City"]) ? null : (System.String)dataRow["City"];
			entity.StateCode = Convert.IsDBNull(dataRow["StateCode"]) ? null : (System.String)dataRow["StateCode"];
			entity.PostalCode = (System.String)dataRow["PostalCode"];
			entity.CountryCode = Convert.IsDBNull(dataRow["CountryCode"]) ? null : (System.String)dataRow["CountryCode"];
			entity.PhoneNumber = Convert.IsDBNull(dataRow["PhoneNumber"]) ? null : (System.String)dataRow["PhoneNumber"];
			entity.IsDefaultBilling = (System.Boolean)dataRow["IsDefaultBilling"];
			entity.AccountID = (System.Int32)dataRow["AccountID"];
			entity.IsDefaultShipping = (System.Boolean)dataRow["IsDefaultShipping"];
			entity.Name = (System.String)dataRow["Name"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Address"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Address Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Address entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AccountIDSource	
			if (CanDeepLoad(entity, "Account|AccountIDSource", deepLoadType, innerList) 
				&& entity.AccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AccountID;
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AccountIDSource = tmpEntity;
				else
					entity.AccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, entity.AccountID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.AccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AccountIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Address object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Address instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Address Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Address entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AccountIDSource
			if (CanDeepSave(entity, "Account|AccountIDSource", deepSaveType, innerList) 
				&& entity.AccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.AccountIDSource);
				entity.AccountID = entity.AccountIDSource.AccountID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AddressChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Address</c>
	///</summary>
	public enum AddressChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Account</c> at AccountIDSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
	}
	
	#endregion AddressChildEntityTypes
	
	#region AddressFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AddressColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Address"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AddressFilterBuilder : SqlFilterBuilder<AddressColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AddressFilterBuilder class.
		/// </summary>
		public AddressFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AddressFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AddressFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AddressFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AddressFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AddressFilterBuilder
	
	#region AddressParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AddressColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Address"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AddressParameterBuilder : ParameterizedSqlFilterBuilder<AddressColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AddressParameterBuilder class.
		/// </summary>
		public AddressParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AddressParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AddressParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AddressParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AddressParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AddressParameterBuilder
	
	#region AddressSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AddressColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Address"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AddressSortBuilder : SqlSortBuilder<AddressColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AddressSqlSortBuilder class.
		/// </summary>
		public AddressSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AddressSortBuilder
	
} // end namespace
