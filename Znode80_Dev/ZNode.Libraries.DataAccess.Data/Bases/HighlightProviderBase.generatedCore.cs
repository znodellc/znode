﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="HighlightProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class HighlightProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Highlight, ZNode.Libraries.DataAccess.Entities.HighlightKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.HighlightKey key)
		{
			return Delete(transactionManager, key.HighlightID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_highlightID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _highlightID)
		{
			return Delete(null, _highlightID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_highlightID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _highlightID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Highlight Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.HighlightKey key, int start, int pageLength)
		{
			return GetByHighlightID(transactionManager, key.HighlightID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeHighlight_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByActiveInd(System.Boolean? _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByActiveInd(System.Boolean? _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByActiveInd(TransactionManager transactionManager, System.Boolean? _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByActiveInd(TransactionManager transactionManager, System.Boolean? _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByActiveInd(System.Boolean? _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public abstract TList<Highlight> GetByActiveInd(TransactionManager transactionManager, System.Boolean? _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeHighlight_HighlightTypeID index.
		/// </summary>
		/// <param name="_highlightTypeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByHighlightTypeID(System.Int32? _highlightTypeID)
		{
			int count = -1;
			return GetByHighlightTypeID(null,_highlightTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_HighlightTypeID index.
		/// </summary>
		/// <param name="_highlightTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByHighlightTypeID(System.Int32? _highlightTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByHighlightTypeID(null, _highlightTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_HighlightTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_highlightTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByHighlightTypeID(TransactionManager transactionManager, System.Int32? _highlightTypeID)
		{
			int count = -1;
			return GetByHighlightTypeID(transactionManager, _highlightTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_HighlightTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_highlightTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByHighlightTypeID(TransactionManager transactionManager, System.Int32? _highlightTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByHighlightTypeID(transactionManager, _highlightTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_HighlightTypeID index.
		/// </summary>
		/// <param name="_highlightTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByHighlightTypeID(System.Int32? _highlightTypeID, int start, int pageLength, out int count)
		{
			return GetByHighlightTypeID(null, _highlightTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_HighlightTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_highlightTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public abstract TList<Highlight> GetByHighlightTypeID(TransactionManager transactionManager, System.Int32? _highlightTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeHighlight_LocaleId index.
		/// </summary>
		/// <param name="_localeId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByLocaleId(System.Int32 _localeId)
		{
			int count = -1;
			return GetByLocaleId(null,_localeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_LocaleId index.
		/// </summary>
		/// <param name="_localeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByLocaleId(System.Int32 _localeId, int start, int pageLength)
		{
			int count = -1;
			return GetByLocaleId(null, _localeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_LocaleId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByLocaleId(TransactionManager transactionManager, System.Int32 _localeId)
		{
			int count = -1;
			return GetByLocaleId(transactionManager, _localeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_LocaleId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByLocaleId(TransactionManager transactionManager, System.Int32 _localeId, int start, int pageLength)
		{
			int count = -1;
			return GetByLocaleId(transactionManager, _localeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_LocaleId index.
		/// </summary>
		/// <param name="_localeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByLocaleId(System.Int32 _localeId, int start, int pageLength, out int count)
		{
			return GetByLocaleId(null, _localeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_LocaleId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_localeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public abstract TList<Highlight> GetByLocaleId(TransactionManager transactionManager, System.Int32 _localeId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeHighlight_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByPortalID(System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(null,_portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByPortalID(System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public TList<Highlight> GetByPortalID(System.Int32? _portalID, int start, int pageLength, out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeHighlight_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Highlight&gt;"/> class.</returns>
		public abstract TList<Highlight> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SC_Highlight index.
		/// </summary>
		/// <param name="_highlightID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Highlight"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Highlight GetByHighlightID(System.Int32 _highlightID)
		{
			int count = -1;
			return GetByHighlightID(null,_highlightID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Highlight index.
		/// </summary>
		/// <param name="_highlightID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Highlight"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Highlight GetByHighlightID(System.Int32 _highlightID, int start, int pageLength)
		{
			int count = -1;
			return GetByHighlightID(null, _highlightID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Highlight index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_highlightID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Highlight"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Highlight GetByHighlightID(TransactionManager transactionManager, System.Int32 _highlightID)
		{
			int count = -1;
			return GetByHighlightID(transactionManager, _highlightID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Highlight index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_highlightID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Highlight"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Highlight GetByHighlightID(TransactionManager transactionManager, System.Int32 _highlightID, int start, int pageLength)
		{
			int count = -1;
			return GetByHighlightID(transactionManager, _highlightID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Highlight index.
		/// </summary>
		/// <param name="_highlightID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Highlight"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Highlight GetByHighlightID(System.Int32 _highlightID, int start, int pageLength, out int count)
		{
			return GetByHighlightID(null, _highlightID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Highlight index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_highlightID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Highlight"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Highlight GetByHighlightID(TransactionManager transactionManager, System.Int32 _highlightID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Highlight&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Highlight&gt;"/></returns>
		public static TList<Highlight> Fill(IDataReader reader, TList<Highlight> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Highlight c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Highlight")
					.Append("|").Append((System.Int32)reader[((int)HighlightColumn.HighlightID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Highlight>(
					key.ToString(), // EntityTrackingKey
					"Highlight",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Highlight();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.HighlightID = (System.Int32)reader[((int)HighlightColumn.HighlightID - 1)];
					c.ImageFile = (reader.IsDBNull(((int)HighlightColumn.ImageFile - 1)))?null:(System.String)reader[((int)HighlightColumn.ImageFile - 1)];
					c.ImageAltTag = (reader.IsDBNull(((int)HighlightColumn.ImageAltTag - 1)))?null:(System.String)reader[((int)HighlightColumn.ImageAltTag - 1)];
					c.Name = (reader.IsDBNull(((int)HighlightColumn.Name - 1)))?null:(System.String)reader[((int)HighlightColumn.Name - 1)];
					c.Description = (reader.IsDBNull(((int)HighlightColumn.Description - 1)))?null:(System.String)reader[((int)HighlightColumn.Description - 1)];
					c.DisplayPopup = (System.Boolean)reader[((int)HighlightColumn.DisplayPopup - 1)];
					c.Hyperlink = (reader.IsDBNull(((int)HighlightColumn.Hyperlink - 1)))?null:(System.String)reader[((int)HighlightColumn.Hyperlink - 1)];
					c.HyperlinkNewWinInd = (reader.IsDBNull(((int)HighlightColumn.HyperlinkNewWinInd - 1)))?null:(System.Boolean?)reader[((int)HighlightColumn.HyperlinkNewWinInd - 1)];
					c.HighlightTypeID = (reader.IsDBNull(((int)HighlightColumn.HighlightTypeID - 1)))?null:(System.Int32?)reader[((int)HighlightColumn.HighlightTypeID - 1)];
					c.ActiveInd = (reader.IsDBNull(((int)HighlightColumn.ActiveInd - 1)))?null:(System.Boolean?)reader[((int)HighlightColumn.ActiveInd - 1)];
					c.DisplayOrder = (reader.IsDBNull(((int)HighlightColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)HighlightColumn.DisplayOrder - 1)];
					c.ShortDescription = (reader.IsDBNull(((int)HighlightColumn.ShortDescription - 1)))?null:(System.String)reader[((int)HighlightColumn.ShortDescription - 1)];
					c.LocaleId = (System.Int32)reader[((int)HighlightColumn.LocaleId - 1)];
					c.PortalID = (reader.IsDBNull(((int)HighlightColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)HighlightColumn.PortalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Highlight"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Highlight"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Highlight entity)
		{
			if (!reader.Read()) return;
			
			entity.HighlightID = (System.Int32)reader[((int)HighlightColumn.HighlightID - 1)];
			entity.ImageFile = (reader.IsDBNull(((int)HighlightColumn.ImageFile - 1)))?null:(System.String)reader[((int)HighlightColumn.ImageFile - 1)];
			entity.ImageAltTag = (reader.IsDBNull(((int)HighlightColumn.ImageAltTag - 1)))?null:(System.String)reader[((int)HighlightColumn.ImageAltTag - 1)];
			entity.Name = (reader.IsDBNull(((int)HighlightColumn.Name - 1)))?null:(System.String)reader[((int)HighlightColumn.Name - 1)];
			entity.Description = (reader.IsDBNull(((int)HighlightColumn.Description - 1)))?null:(System.String)reader[((int)HighlightColumn.Description - 1)];
			entity.DisplayPopup = (System.Boolean)reader[((int)HighlightColumn.DisplayPopup - 1)];
			entity.Hyperlink = (reader.IsDBNull(((int)HighlightColumn.Hyperlink - 1)))?null:(System.String)reader[((int)HighlightColumn.Hyperlink - 1)];
			entity.HyperlinkNewWinInd = (reader.IsDBNull(((int)HighlightColumn.HyperlinkNewWinInd - 1)))?null:(System.Boolean?)reader[((int)HighlightColumn.HyperlinkNewWinInd - 1)];
			entity.HighlightTypeID = (reader.IsDBNull(((int)HighlightColumn.HighlightTypeID - 1)))?null:(System.Int32?)reader[((int)HighlightColumn.HighlightTypeID - 1)];
			entity.ActiveInd = (reader.IsDBNull(((int)HighlightColumn.ActiveInd - 1)))?null:(System.Boolean?)reader[((int)HighlightColumn.ActiveInd - 1)];
			entity.DisplayOrder = (reader.IsDBNull(((int)HighlightColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)HighlightColumn.DisplayOrder - 1)];
			entity.ShortDescription = (reader.IsDBNull(((int)HighlightColumn.ShortDescription - 1)))?null:(System.String)reader[((int)HighlightColumn.ShortDescription - 1)];
			entity.LocaleId = (System.Int32)reader[((int)HighlightColumn.LocaleId - 1)];
			entity.PortalID = (reader.IsDBNull(((int)HighlightColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)HighlightColumn.PortalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Highlight"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Highlight"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Highlight entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.HighlightID = (System.Int32)dataRow["HighlightID"];
			entity.ImageFile = Convert.IsDBNull(dataRow["ImageFile"]) ? null : (System.String)dataRow["ImageFile"];
			entity.ImageAltTag = Convert.IsDBNull(dataRow["ImageAltTag"]) ? null : (System.String)dataRow["ImageAltTag"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.DisplayPopup = (System.Boolean)dataRow["DisplayPopup"];
			entity.Hyperlink = Convert.IsDBNull(dataRow["Hyperlink"]) ? null : (System.String)dataRow["Hyperlink"];
			entity.HyperlinkNewWinInd = Convert.IsDBNull(dataRow["HyperlinkNewWinInd"]) ? null : (System.Boolean?)dataRow["HyperlinkNewWinInd"];
			entity.HighlightTypeID = Convert.IsDBNull(dataRow["HighlightTypeID"]) ? null : (System.Int32?)dataRow["HighlightTypeID"];
			entity.ActiveInd = Convert.IsDBNull(dataRow["ActiveInd"]) ? null : (System.Boolean?)dataRow["ActiveInd"];
			entity.DisplayOrder = Convert.IsDBNull(dataRow["DisplayOrder"]) ? null : (System.Int32?)dataRow["DisplayOrder"];
			entity.ShortDescription = Convert.IsDBNull(dataRow["ShortDescription"]) ? null : (System.String)dataRow["ShortDescription"];
			entity.LocaleId = (System.Int32)dataRow["LocaleId"];
			entity.PortalID = Convert.IsDBNull(dataRow["PortalID"]) ? null : (System.Int32?)dataRow["PortalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Highlight"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Highlight Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Highlight entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region HighlightTypeIDSource	
			if (CanDeepLoad(entity, "HighlightType|HighlightTypeIDSource", deepLoadType, innerList) 
				&& entity.HighlightTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.HighlightTypeID ?? (int)0);
				HighlightType tmpEntity = EntityManager.LocateEntity<HighlightType>(EntityLocator.ConstructKeyFromPkItems(typeof(HighlightType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.HighlightTypeIDSource = tmpEntity;
				else
					entity.HighlightTypeIDSource = DataRepository.HighlightTypeProvider.GetByHighlightTypeID(transactionManager, (entity.HighlightTypeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'HighlightTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.HighlightTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.HighlightTypeProvider.DeepLoad(transactionManager, entity.HighlightTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion HighlightTypeIDSource

			#region LocaleIdSource	
			if (CanDeepLoad(entity, "Locale|LocaleIdSource", deepLoadType, innerList) 
				&& entity.LocaleIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.LocaleId;
				Locale tmpEntity = EntityManager.LocateEntity<Locale>(EntityLocator.ConstructKeyFromPkItems(typeof(Locale), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.LocaleIdSource = tmpEntity;
				else
					entity.LocaleIdSource = DataRepository.LocaleProvider.GetByLocaleID(transactionManager, entity.LocaleId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LocaleIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.LocaleIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.LocaleProvider.DeepLoad(transactionManager, entity.LocaleIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion LocaleIdSource

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PortalID ?? (int)0);
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, (entity.PortalID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByHighlightID methods when available
			
			#region ProductHighlightCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductHighlight>|ProductHighlightCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductHighlightCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductHighlightCollection = DataRepository.ProductHighlightProvider.GetByHighlightID(transactionManager, entity.HighlightID);

				if (deep && entity.ProductHighlightCollection.Count > 0)
				{
					deepHandles.Add("ProductHighlightCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductHighlight>) DataRepository.ProductHighlightProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductHighlightCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Highlight object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Highlight instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Highlight Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Highlight entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region HighlightTypeIDSource
			if (CanDeepSave(entity, "HighlightType|HighlightTypeIDSource", deepSaveType, innerList) 
				&& entity.HighlightTypeIDSource != null)
			{
				DataRepository.HighlightTypeProvider.Save(transactionManager, entity.HighlightTypeIDSource);
				entity.HighlightTypeID = entity.HighlightTypeIDSource.HighlightTypeID;
			}
			#endregion 
			
			#region LocaleIdSource
			if (CanDeepSave(entity, "Locale|LocaleIdSource", deepSaveType, innerList) 
				&& entity.LocaleIdSource != null)
			{
				DataRepository.LocaleProvider.Save(transactionManager, entity.LocaleIdSource);
				entity.LocaleId = entity.LocaleIdSource.LocaleID;
			}
			#endregion 
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<ProductHighlight>
				if (CanDeepSave(entity.ProductHighlightCollection, "List<ProductHighlight>|ProductHighlightCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductHighlight child in entity.ProductHighlightCollection)
					{
						if(child.HighlightIDSource != null)
						{
							child.HighlightID = child.HighlightIDSource.HighlightID;
						}
						else
						{
							child.HighlightID = entity.HighlightID;
						}

					}

					if (entity.ProductHighlightCollection.Count > 0 || entity.ProductHighlightCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductHighlightProvider.Save(transactionManager, entity.ProductHighlightCollection);
						
						deepHandles.Add("ProductHighlightCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductHighlight >) DataRepository.ProductHighlightProvider.DeepSave,
							new object[] { transactionManager, entity.ProductHighlightCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region HighlightChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Highlight</c>
	///</summary>
	public enum HighlightChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>HighlightType</c> at HighlightTypeIDSource
		///</summary>
		[ChildEntityType(typeof(HighlightType))]
		HighlightType,
		
		///<summary>
		/// Composite Property for <c>Locale</c> at LocaleIdSource
		///</summary>
		[ChildEntityType(typeof(Locale))]
		Locale,
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		///<summary>
		/// Collection of <c>Highlight</c> as OneToMany for ProductHighlightCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductHighlight>))]
		ProductHighlightCollection,
	}
	
	#endregion HighlightChildEntityTypes
	
	#region HighlightFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;HighlightColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Highlight"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HighlightFilterBuilder : SqlFilterBuilder<HighlightColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HighlightFilterBuilder class.
		/// </summary>
		public HighlightFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the HighlightFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HighlightFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HighlightFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HighlightFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HighlightFilterBuilder
	
	#region HighlightParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;HighlightColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Highlight"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class HighlightParameterBuilder : ParameterizedSqlFilterBuilder<HighlightColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HighlightParameterBuilder class.
		/// </summary>
		public HighlightParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the HighlightParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public HighlightParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the HighlightParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public HighlightParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion HighlightParameterBuilder
	
	#region HighlightSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;HighlightColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Highlight"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class HighlightSortBuilder : SqlSortBuilder<HighlightColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the HighlightSqlSortBuilder class.
		/// </summary>
		public HighlightSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion HighlightSortBuilder
	
} // end namespace
