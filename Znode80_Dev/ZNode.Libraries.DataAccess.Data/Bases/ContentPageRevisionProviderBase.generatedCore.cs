﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ContentPageRevisionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ContentPageRevisionProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ContentPageRevision, ZNode.Libraries.DataAccess.Entities.ContentPageRevisionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ContentPageRevisionKey key)
		{
			return Delete(transactionManager, key.RevisionID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_revisionID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _revisionID)
		{
			return Delete(null, _revisionID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_revisionID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _revisionID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePageRevision_ZNodePage key.
		///		FK_ZNodePageRevision_ZNodePage Description: 
		/// </summary>
		/// <param name="_contentPageID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPageRevision objects.</returns>
		public TList<ContentPageRevision> GetByContentPageID(System.Int32 _contentPageID)
		{
			int count = -1;
			return GetByContentPageID(_contentPageID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePageRevision_ZNodePage key.
		///		FK_ZNodePageRevision_ZNodePage Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contentPageID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPageRevision objects.</returns>
		/// <remarks></remarks>
		public TList<ContentPageRevision> GetByContentPageID(TransactionManager transactionManager, System.Int32 _contentPageID)
		{
			int count = -1;
			return GetByContentPageID(transactionManager, _contentPageID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePageRevision_ZNodePage key.
		///		FK_ZNodePageRevision_ZNodePage Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contentPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPageRevision objects.</returns>
		public TList<ContentPageRevision> GetByContentPageID(TransactionManager transactionManager, System.Int32 _contentPageID, int start, int pageLength)
		{
			int count = -1;
			return GetByContentPageID(transactionManager, _contentPageID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePageRevision_ZNodePage key.
		///		fKZNodePageRevisionZNodePage Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_contentPageID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPageRevision objects.</returns>
		public TList<ContentPageRevision> GetByContentPageID(System.Int32 _contentPageID, int start, int pageLength)
		{
			int count =  -1;
			return GetByContentPageID(null, _contentPageID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePageRevision_ZNodePage key.
		///		fKZNodePageRevisionZNodePage Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_contentPageID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPageRevision objects.</returns>
		public TList<ContentPageRevision> GetByContentPageID(System.Int32 _contentPageID, int start, int pageLength,out int count)
		{
			return GetByContentPageID(null, _contentPageID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePageRevision_ZNodePage key.
		///		FK_ZNodePageRevision_ZNodePage Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_contentPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ContentPageRevision objects.</returns>
		public abstract TList<ContentPageRevision> GetByContentPageID(TransactionManager transactionManager, System.Int32 _contentPageID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ContentPageRevision Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ContentPageRevisionKey key, int start, int pageLength)
		{
			return GetByRevisionID(transactionManager, key.RevisionID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodePageRevision index.
		/// </summary>
		/// <param name="_revisionID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPageRevision"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ContentPageRevision GetByRevisionID(System.Int32 _revisionID)
		{
			int count = -1;
			return GetByRevisionID(null,_revisionID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePageRevision index.
		/// </summary>
		/// <param name="_revisionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPageRevision"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ContentPageRevision GetByRevisionID(System.Int32 _revisionID, int start, int pageLength)
		{
			int count = -1;
			return GetByRevisionID(null, _revisionID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePageRevision index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_revisionID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPageRevision"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ContentPageRevision GetByRevisionID(TransactionManager transactionManager, System.Int32 _revisionID)
		{
			int count = -1;
			return GetByRevisionID(transactionManager, _revisionID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePageRevision index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_revisionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPageRevision"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ContentPageRevision GetByRevisionID(TransactionManager transactionManager, System.Int32 _revisionID, int start, int pageLength)
		{
			int count = -1;
			return GetByRevisionID(transactionManager, _revisionID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePageRevision index.
		/// </summary>
		/// <param name="_revisionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPageRevision"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ContentPageRevision GetByRevisionID(System.Int32 _revisionID, int start, int pageLength, out int count)
		{
			return GetByRevisionID(null, _revisionID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePageRevision index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_revisionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPageRevision"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ContentPageRevision GetByRevisionID(TransactionManager transactionManager, System.Int32 _revisionID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ContentPageRevision&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ContentPageRevision&gt;"/></returns>
		public static TList<ContentPageRevision> Fill(IDataReader reader, TList<ContentPageRevision> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ContentPageRevision c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ContentPageRevision")
					.Append("|").Append((System.Int32)reader[((int)ContentPageRevisionColumn.RevisionID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ContentPageRevision>(
					key.ToString(), // EntityTrackingKey
					"ContentPageRevision",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ContentPageRevision();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.RevisionID = (System.Int32)reader[((int)ContentPageRevisionColumn.RevisionID - 1)];
					c.ContentPageID = (System.Int32)reader[((int)ContentPageRevisionColumn.ContentPageID - 1)];
					c.UpdateDate = (System.DateTime)reader[((int)ContentPageRevisionColumn.UpdateDate - 1)];
					c.UpdateUser = (System.String)reader[((int)ContentPageRevisionColumn.UpdateUser - 1)];
					c.Description = (reader.IsDBNull(((int)ContentPageRevisionColumn.Description - 1)))?null:(System.String)reader[((int)ContentPageRevisionColumn.Description - 1)];
					c.HtmlText = (reader.IsDBNull(((int)ContentPageRevisionColumn.HtmlText - 1)))?null:(System.String)reader[((int)ContentPageRevisionColumn.HtmlText - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPageRevision"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ContentPageRevision"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ContentPageRevision entity)
		{
			if (!reader.Read()) return;
			
			entity.RevisionID = (System.Int32)reader[((int)ContentPageRevisionColumn.RevisionID - 1)];
			entity.ContentPageID = (System.Int32)reader[((int)ContentPageRevisionColumn.ContentPageID - 1)];
			entity.UpdateDate = (System.DateTime)reader[((int)ContentPageRevisionColumn.UpdateDate - 1)];
			entity.UpdateUser = (System.String)reader[((int)ContentPageRevisionColumn.UpdateUser - 1)];
			entity.Description = (reader.IsDBNull(((int)ContentPageRevisionColumn.Description - 1)))?null:(System.String)reader[((int)ContentPageRevisionColumn.Description - 1)];
			entity.HtmlText = (reader.IsDBNull(((int)ContentPageRevisionColumn.HtmlText - 1)))?null:(System.String)reader[((int)ContentPageRevisionColumn.HtmlText - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ContentPageRevision"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ContentPageRevision"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ContentPageRevision entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.RevisionID = (System.Int32)dataRow["RevisionID"];
			entity.ContentPageID = (System.Int32)dataRow["ContentPageID"];
			entity.UpdateDate = (System.DateTime)dataRow["UpdateDate"];
			entity.UpdateUser = (System.String)dataRow["UpdateUser"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.HtmlText = Convert.IsDBNull(dataRow["HtmlText"]) ? null : (System.String)dataRow["HtmlText"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ContentPageRevision"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ContentPageRevision Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ContentPageRevision entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ContentPageIDSource	
			if (CanDeepLoad(entity, "ContentPage|ContentPageIDSource", deepLoadType, innerList) 
				&& entity.ContentPageIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ContentPageID;
				ContentPage tmpEntity = EntityManager.LocateEntity<ContentPage>(EntityLocator.ConstructKeyFromPkItems(typeof(ContentPage), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ContentPageIDSource = tmpEntity;
				else
					entity.ContentPageIDSource = DataRepository.ContentPageProvider.GetByContentPageID(transactionManager, entity.ContentPageID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ContentPageIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ContentPageIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ContentPageProvider.DeepLoad(transactionManager, entity.ContentPageIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ContentPageIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ContentPageRevision object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ContentPageRevision instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ContentPageRevision Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ContentPageRevision entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ContentPageIDSource
			if (CanDeepSave(entity, "ContentPage|ContentPageIDSource", deepSaveType, innerList) 
				&& entity.ContentPageIDSource != null)
			{
				DataRepository.ContentPageProvider.Save(transactionManager, entity.ContentPageIDSource);
				entity.ContentPageID = entity.ContentPageIDSource.ContentPageID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ContentPageRevisionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ContentPageRevision</c>
	///</summary>
	public enum ContentPageRevisionChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>ContentPage</c> at ContentPageIDSource
		///</summary>
		[ChildEntityType(typeof(ContentPage))]
		ContentPage,
	}
	
	#endregion ContentPageRevisionChildEntityTypes
	
	#region ContentPageRevisionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ContentPageRevisionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContentPageRevision"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContentPageRevisionFilterBuilder : SqlFilterBuilder<ContentPageRevisionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContentPageRevisionFilterBuilder class.
		/// </summary>
		public ContentPageRevisionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ContentPageRevisionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ContentPageRevisionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ContentPageRevisionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ContentPageRevisionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ContentPageRevisionFilterBuilder
	
	#region ContentPageRevisionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ContentPageRevisionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContentPageRevision"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ContentPageRevisionParameterBuilder : ParameterizedSqlFilterBuilder<ContentPageRevisionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContentPageRevisionParameterBuilder class.
		/// </summary>
		public ContentPageRevisionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ContentPageRevisionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ContentPageRevisionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ContentPageRevisionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ContentPageRevisionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ContentPageRevisionParameterBuilder
	
	#region ContentPageRevisionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ContentPageRevisionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ContentPageRevision"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ContentPageRevisionSortBuilder : SqlSortBuilder<ContentPageRevisionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ContentPageRevisionSqlSortBuilder class.
		/// </summary>
		public ContentPageRevisionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ContentPageRevisionSortBuilder
	
} // end namespace
