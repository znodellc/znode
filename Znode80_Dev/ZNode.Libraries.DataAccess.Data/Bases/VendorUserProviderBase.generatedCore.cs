﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="VendorUserProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class VendorUserProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.VendorUser, ZNode.Libraries.DataAccess.Entities.VendorUserKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.VendorUserKey key)
		{
			return Delete(transactionManager, key.SupplierID, key.UserId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_supplierID">. Primary Key.</param>
		/// <param name="_userId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _supplierID, System.Guid _userId)
		{
			return Delete(null, _supplierID, _userId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID">. Primary Key.</param>
		/// <param name="_userId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _supplierID, System.Guid _userId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeVendorUser_ZNodeSupplier key.
		///		FK_ZNodeVendorUser_ZNodeSupplier Description: 
		/// </summary>
		/// <param name="_supplierID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.VendorUser objects.</returns>
		public TList<VendorUser> GetBySupplierID(System.Int32 _supplierID)
		{
			int count = -1;
			return GetBySupplierID(_supplierID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeVendorUser_ZNodeSupplier key.
		///		FK_ZNodeVendorUser_ZNodeSupplier Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.VendorUser objects.</returns>
		/// <remarks></remarks>
		public TList<VendorUser> GetBySupplierID(TransactionManager transactionManager, System.Int32 _supplierID)
		{
			int count = -1;
			return GetBySupplierID(transactionManager, _supplierID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeVendorUser_ZNodeSupplier key.
		///		FK_ZNodeVendorUser_ZNodeSupplier Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.VendorUser objects.</returns>
		public TList<VendorUser> GetBySupplierID(TransactionManager transactionManager, System.Int32 _supplierID, int start, int pageLength)
		{
			int count = -1;
			return GetBySupplierID(transactionManager, _supplierID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeVendorUser_ZNodeSupplier key.
		///		fKZNodeVendorUserZNodeSupplier Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_supplierID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.VendorUser objects.</returns>
		public TList<VendorUser> GetBySupplierID(System.Int32 _supplierID, int start, int pageLength)
		{
			int count =  -1;
			return GetBySupplierID(null, _supplierID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeVendorUser_ZNodeSupplier key.
		///		fKZNodeVendorUserZNodeSupplier Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_supplierID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.VendorUser objects.</returns>
		public TList<VendorUser> GetBySupplierID(System.Int32 _supplierID, int start, int pageLength,out int count)
		{
			return GetBySupplierID(null, _supplierID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeVendorUser_ZNodeSupplier key.
		///		FK_ZNodeVendorUser_ZNodeSupplier Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.VendorUser objects.</returns>
		public abstract TList<VendorUser> GetBySupplierID(TransactionManager transactionManager, System.Int32 _supplierID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.VendorUser Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.VendorUserKey key, int start, int pageLength)
		{
			return GetBySupplierIDUserId(transactionManager, key.SupplierID, key.UserId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeVendorUser index.
		/// </summary>
		/// <param name="_supplierID"></param>
		/// <param name="_userId"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.VendorUser"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.VendorUser GetBySupplierIDUserId(System.Int32 _supplierID, System.Guid _userId)
		{
			int count = -1;
			return GetBySupplierIDUserId(null,_supplierID, _userId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeVendorUser index.
		/// </summary>
		/// <param name="_supplierID"></param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.VendorUser"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.VendorUser GetBySupplierIDUserId(System.Int32 _supplierID, System.Guid _userId, int start, int pageLength)
		{
			int count = -1;
			return GetBySupplierIDUserId(null, _supplierID, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeVendorUser index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <param name="_userId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.VendorUser"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.VendorUser GetBySupplierIDUserId(TransactionManager transactionManager, System.Int32 _supplierID, System.Guid _userId)
		{
			int count = -1;
			return GetBySupplierIDUserId(transactionManager, _supplierID, _userId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeVendorUser index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.VendorUser"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.VendorUser GetBySupplierIDUserId(TransactionManager transactionManager, System.Int32 _supplierID, System.Guid _userId, int start, int pageLength)
		{
			int count = -1;
			return GetBySupplierIDUserId(transactionManager, _supplierID, _userId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeVendorUser index.
		/// </summary>
		/// <param name="_supplierID"></param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.VendorUser"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.VendorUser GetBySupplierIDUserId(System.Int32 _supplierID, System.Guid _userId, int start, int pageLength, out int count)
		{
			return GetBySupplierIDUserId(null, _supplierID, _userId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeVendorUser index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <param name="_userId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.VendorUser"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.VendorUser GetBySupplierIDUserId(TransactionManager transactionManager, System.Int32 _supplierID, System.Guid _userId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;VendorUser&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;VendorUser&gt;"/></returns>
		public static TList<VendorUser> Fill(IDataReader reader, TList<VendorUser> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.VendorUser c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("VendorUser")
					.Append("|").Append((System.Int32)reader[((int)VendorUserColumn.SupplierID - 1)])
					.Append("|").Append((System.Guid)reader[((int)VendorUserColumn.UserId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<VendorUser>(
					key.ToString(), // EntityTrackingKey
					"VendorUser",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.VendorUser();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.SupplierID = (System.Int32)reader[((int)VendorUserColumn.SupplierID - 1)];
					c.OriginalSupplierID = c.SupplierID;
					c.UserId = (System.Guid)reader[((int)VendorUserColumn.UserId - 1)];
					c.OriginalUserId = c.UserId;
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.VendorUser"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.VendorUser"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.VendorUser entity)
		{
			if (!reader.Read()) return;
			
			entity.SupplierID = (System.Int32)reader[((int)VendorUserColumn.SupplierID - 1)];
			entity.OriginalSupplierID = (System.Int32)reader["SupplierID"];
			entity.UserId = (System.Guid)reader[((int)VendorUserColumn.UserId - 1)];
			entity.OriginalUserId = (System.Guid)reader["UserId"];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.VendorUser"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.VendorUser"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.VendorUser entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SupplierID = (System.Int32)dataRow["SupplierID"];
			entity.OriginalSupplierID = (System.Int32)dataRow["SupplierID"];
			entity.UserId = (System.Guid)dataRow["UserId"];
			entity.OriginalUserId = (System.Guid)dataRow["UserId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.VendorUser"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.VendorUser Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.VendorUser entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region SupplierIDSource	
			if (CanDeepLoad(entity, "Supplier|SupplierIDSource", deepLoadType, innerList) 
				&& entity.SupplierIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SupplierID;
				Supplier tmpEntity = EntityManager.LocateEntity<Supplier>(EntityLocator.ConstructKeyFromPkItems(typeof(Supplier), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SupplierIDSource = tmpEntity;
				else
					entity.SupplierIDSource = DataRepository.SupplierProvider.GetBySupplierID(transactionManager, entity.SupplierID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SupplierIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SupplierIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SupplierProvider.DeepLoad(transactionManager, entity.SupplierIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SupplierIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.VendorUser object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.VendorUser instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.VendorUser Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.VendorUser entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region SupplierIDSource
			if (CanDeepSave(entity, "Supplier|SupplierIDSource", deepSaveType, innerList) 
				&& entity.SupplierIDSource != null)
			{
				DataRepository.SupplierProvider.Save(transactionManager, entity.SupplierIDSource);
				entity.SupplierID = entity.SupplierIDSource.SupplierID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region VendorUserChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.VendorUser</c>
	///</summary>
	public enum VendorUserChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Supplier</c> at SupplierIDSource
		///</summary>
		[ChildEntityType(typeof(Supplier))]
		Supplier,
		}
	
	#endregion VendorUserChildEntityTypes
	
	#region VendorUserFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;VendorUserColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VendorUser"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VendorUserFilterBuilder : SqlFilterBuilder<VendorUserColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VendorUserFilterBuilder class.
		/// </summary>
		public VendorUserFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VendorUserFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VendorUserFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VendorUserFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VendorUserFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VendorUserFilterBuilder
	
	#region VendorUserParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;VendorUserColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VendorUser"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VendorUserParameterBuilder : ParameterizedSqlFilterBuilder<VendorUserColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VendorUserParameterBuilder class.
		/// </summary>
		public VendorUserParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VendorUserParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VendorUserParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VendorUserParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VendorUserParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VendorUserParameterBuilder
	
	#region VendorUserSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;VendorUserColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VendorUser"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class VendorUserSortBuilder : SqlSortBuilder<VendorUserColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VendorUserSqlSortBuilder class.
		/// </summary>
		public VendorUserSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion VendorUserSortBuilder
	
} // end namespace
