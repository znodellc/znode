﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SKUProfileProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SKUProfileProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.SKUProfile, ZNode.Libraries.DataAccess.Entities.SKUProfileKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKUProfileKey key)
		{
			return Delete(transactionManager, key.SKUProfileID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_sKUProfileID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _sKUProfileID)
		{
			return Delete(null, _sKUProfileID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUProfileID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _sKUProfileID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfile_ZNodeProfile key.
		///		FK_ZNodeSKUProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="_profileID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfile objects.</returns>
		public TList<SKUProfile> GetByProfileID(System.Int32 _profileID)
		{
			int count = -1;
			return GetByProfileID(_profileID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfile_ZNodeProfile key.
		///		FK_ZNodeSKUProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfile objects.</returns>
		/// <remarks></remarks>
		public TList<SKUProfile> GetByProfileID(TransactionManager transactionManager, System.Int32 _profileID)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfile_ZNodeProfile key.
		///		FK_ZNodeSKUProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfile objects.</returns>
		public TList<SKUProfile> GetByProfileID(TransactionManager transactionManager, System.Int32 _profileID, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfile_ZNodeProfile key.
		///		fKZNodeSKUProfileZNodeProfile Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_profileID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfile objects.</returns>
		public TList<SKUProfile> GetByProfileID(System.Int32 _profileID, int start, int pageLength)
		{
			int count =  -1;
			return GetByProfileID(null, _profileID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfile_ZNodeProfile key.
		///		fKZNodeSKUProfileZNodeProfile Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_profileID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfile objects.</returns>
		public TList<SKUProfile> GetByProfileID(System.Int32 _profileID, int start, int pageLength,out int count)
		{
			return GetByProfileID(null, _profileID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfile_ZNodeProfile key.
		///		FK_ZNodeSKUProfile_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfile objects.</returns>
		public abstract TList<SKUProfile> GetByProfileID(TransactionManager transactionManager, System.Int32 _profileID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfile_ZNodeSKU key.
		///		FK_ZNodeSKUProfile_ZNodeSKU Description: 
		/// </summary>
		/// <param name="_sKUID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfile objects.</returns>
		public TList<SKUProfile> GetBySKUID(System.Int32 _sKUID)
		{
			int count = -1;
			return GetBySKUID(_sKUID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfile_ZNodeSKU key.
		///		FK_ZNodeSKUProfile_ZNodeSKU Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfile objects.</returns>
		/// <remarks></remarks>
		public TList<SKUProfile> GetBySKUID(TransactionManager transactionManager, System.Int32 _sKUID)
		{
			int count = -1;
			return GetBySKUID(transactionManager, _sKUID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfile_ZNodeSKU key.
		///		FK_ZNodeSKUProfile_ZNodeSKU Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfile objects.</returns>
		public TList<SKUProfile> GetBySKUID(TransactionManager transactionManager, System.Int32 _sKUID, int start, int pageLength)
		{
			int count = -1;
			return GetBySKUID(transactionManager, _sKUID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfile_ZNodeSKU key.
		///		fKZNodeSKUProfileZNodeSKU Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_sKUID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfile objects.</returns>
		public TList<SKUProfile> GetBySKUID(System.Int32 _sKUID, int start, int pageLength)
		{
			int count =  -1;
			return GetBySKUID(null, _sKUID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfile_ZNodeSKU key.
		///		fKZNodeSKUProfileZNodeSKU Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_sKUID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfile objects.</returns>
		public TList<SKUProfile> GetBySKUID(System.Int32 _sKUID, int start, int pageLength,out int count)
		{
			return GetBySKUID(null, _sKUID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKUProfile_ZNodeSKU key.
		///		FK_ZNodeSKUProfile_ZNodeSKU Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKUProfile objects.</returns>
		public abstract TList<SKUProfile> GetBySKUID(TransactionManager transactionManager, System.Int32 _sKUID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.SKUProfile Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKUProfileKey key, int start, int pageLength)
		{
			return GetBySKUProfileID(transactionManager, key.SKUProfileID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeSKUProfile index.
		/// </summary>
		/// <param name="_sKUProfileID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUProfile GetBySKUProfileID(System.Int32 _sKUProfileID)
		{
			int count = -1;
			return GetBySKUProfileID(null,_sKUProfileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSKUProfile index.
		/// </summary>
		/// <param name="_sKUProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUProfile GetBySKUProfileID(System.Int32 _sKUProfileID, int start, int pageLength)
		{
			int count = -1;
			return GetBySKUProfileID(null, _sKUProfileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSKUProfile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUProfileID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUProfile GetBySKUProfileID(TransactionManager transactionManager, System.Int32 _sKUProfileID)
		{
			int count = -1;
			return GetBySKUProfileID(transactionManager, _sKUProfileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSKUProfile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUProfile GetBySKUProfileID(TransactionManager transactionManager, System.Int32 _sKUProfileID, int start, int pageLength)
		{
			int count = -1;
			return GetBySKUProfileID(transactionManager, _sKUProfileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSKUProfile index.
		/// </summary>
		/// <param name="_sKUProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKUProfile GetBySKUProfileID(System.Int32 _sKUProfileID, int start, int pageLength, out int count)
		{
			return GetBySKUProfileID(null, _sKUProfileID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSKUProfile index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfile"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.SKUProfile GetBySKUProfileID(TransactionManager transactionManager, System.Int32 _sKUProfileID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SKUProfile&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SKUProfile&gt;"/></returns>
		public static TList<SKUProfile> Fill(IDataReader reader, TList<SKUProfile> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.SKUProfile c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SKUProfile")
					.Append("|").Append((System.Int32)reader[((int)SKUProfileColumn.SKUProfileID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SKUProfile>(
					key.ToString(), // EntityTrackingKey
					"SKUProfile",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.SKUProfile();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.SKUProfileID = (System.Int32)reader[((int)SKUProfileColumn.SKUProfileID - 1)];
					c.SKUID = (System.Int32)reader[((int)SKUProfileColumn.SKUID - 1)];
					c.ProfileID = (System.Int32)reader[((int)SKUProfileColumn.ProfileID - 1)];
					c.ProfileLimit = (System.Int32)reader[((int)SKUProfileColumn.ProfileLimit - 1)];
					c.ExpirationDate = (System.DateTime)reader[((int)SKUProfileColumn.ExpirationDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfile"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfile"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.SKUProfile entity)
		{
			if (!reader.Read()) return;
			
			entity.SKUProfileID = (System.Int32)reader[((int)SKUProfileColumn.SKUProfileID - 1)];
			entity.SKUID = (System.Int32)reader[((int)SKUProfileColumn.SKUID - 1)];
			entity.ProfileID = (System.Int32)reader[((int)SKUProfileColumn.ProfileID - 1)];
			entity.ProfileLimit = (System.Int32)reader[((int)SKUProfileColumn.ProfileLimit - 1)];
			entity.ExpirationDate = (System.DateTime)reader[((int)SKUProfileColumn.ExpirationDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfile"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfile"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.SKUProfile entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SKUProfileID = (System.Int32)dataRow["SKUProfileID"];
			entity.SKUID = (System.Int32)dataRow["SKUID"];
			entity.ProfileID = (System.Int32)dataRow["ProfileID"];
			entity.ProfileLimit = (System.Int32)dataRow["ProfileLimit"];
			entity.ExpirationDate = (System.DateTime)dataRow["ExpirationDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SKUProfile"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SKUProfile Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKUProfile entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ProfileIDSource	
			if (CanDeepLoad(entity, "Profile|ProfileIDSource", deepLoadType, innerList) 
				&& entity.ProfileIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProfileID;
				Profile tmpEntity = EntityManager.LocateEntity<Profile>(EntityLocator.ConstructKeyFromPkItems(typeof(Profile), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProfileIDSource = tmpEntity;
				else
					entity.ProfileIDSource = DataRepository.ProfileProvider.GetByProfileID(transactionManager, entity.ProfileID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProfileIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProfileIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProfileProvider.DeepLoad(transactionManager, entity.ProfileIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProfileIDSource

			#region SKUIDSource	
			if (CanDeepLoad(entity, "SKU|SKUIDSource", deepLoadType, innerList) 
				&& entity.SKUIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SKUID;
				SKU tmpEntity = EntityManager.LocateEntity<SKU>(EntityLocator.ConstructKeyFromPkItems(typeof(SKU), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SKUIDSource = tmpEntity;
				else
					entity.SKUIDSource = DataRepository.SKUProvider.GetBySKUID(transactionManager, entity.SKUID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SKUIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SKUIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SKUProvider.DeepLoad(transactionManager, entity.SKUIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SKUIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.SKUProfile object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.SKUProfile instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SKUProfile Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKUProfile entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ProfileIDSource
			if (CanDeepSave(entity, "Profile|ProfileIDSource", deepSaveType, innerList) 
				&& entity.ProfileIDSource != null)
			{
				DataRepository.ProfileProvider.Save(transactionManager, entity.ProfileIDSource);
				entity.ProfileID = entity.ProfileIDSource.ProfileID;
			}
			#endregion 
			
			#region SKUIDSource
			if (CanDeepSave(entity, "SKU|SKUIDSource", deepSaveType, innerList) 
				&& entity.SKUIDSource != null)
			{
				DataRepository.SKUProvider.Save(transactionManager, entity.SKUIDSource);
				entity.SKUID = entity.SKUIDSource.SKUID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SKUProfileChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.SKUProfile</c>
	///</summary>
	public enum SKUProfileChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Profile</c> at ProfileIDSource
		///</summary>
		[ChildEntityType(typeof(Profile))]
		Profile,
		
		///<summary>
		/// Composite Property for <c>SKU</c> at SKUIDSource
		///</summary>
		[ChildEntityType(typeof(SKU))]
		SKU,
	}
	
	#endregion SKUProfileChildEntityTypes
	
	#region SKUProfileFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SKUProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKUProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUProfileFilterBuilder : SqlFilterBuilder<SKUProfileColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUProfileFilterBuilder class.
		/// </summary>
		public SKUProfileFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUProfileFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUProfileFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUProfileFilterBuilder
	
	#region SKUProfileParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SKUProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKUProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUProfileParameterBuilder : ParameterizedSqlFilterBuilder<SKUProfileColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUProfileParameterBuilder class.
		/// </summary>
		public SKUProfileParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUProfileParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUProfileParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUProfileParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUProfileParameterBuilder
	
	#region SKUProfileSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SKUProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKUProfile"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SKUProfileSortBuilder : SqlSortBuilder<SKUProfileColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUProfileSqlSortBuilder class.
		/// </summary>
		public SKUProfileSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SKUProfileSortBuilder
	
} // end namespace
