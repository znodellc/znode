﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductExtensionOptionProductProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductExtensionOptionProductProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct, ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProductKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProductKey key)
		{
			return Delete(transactionManager, key.ProductExtensionOptionProductLinkID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_productExtensionOptionProductLinkID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _productExtensionOptionProductLinkID)
		{
			return Delete(null, _productExtensionOptionProductLinkID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionOptionProductLinkID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _productExtensionOptionProductLinkID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtensionOptionProduct_ZNodeProductExtensionOption key.
		///		FK_ZNodeProductExtensionOptionProduct_ZNodeProductExtensionOption Description: 
		/// </summary>
		/// <param name="_productExtensionOptionID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct objects.</returns>
		public TList<ProductExtensionOptionProduct> GetByProductExtensionOptionID(System.Int32? _productExtensionOptionID)
		{
			int count = -1;
			return GetByProductExtensionOptionID(_productExtensionOptionID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtensionOptionProduct_ZNodeProductExtensionOption key.
		///		FK_ZNodeProductExtensionOptionProduct_ZNodeProductExtensionOption Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionOptionID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct objects.</returns>
		/// <remarks></remarks>
		public TList<ProductExtensionOptionProduct> GetByProductExtensionOptionID(TransactionManager transactionManager, System.Int32? _productExtensionOptionID)
		{
			int count = -1;
			return GetByProductExtensionOptionID(transactionManager, _productExtensionOptionID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtensionOptionProduct_ZNodeProductExtensionOption key.
		///		FK_ZNodeProductExtensionOptionProduct_ZNodeProductExtensionOption Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionOptionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct objects.</returns>
		public TList<ProductExtensionOptionProduct> GetByProductExtensionOptionID(TransactionManager transactionManager, System.Int32? _productExtensionOptionID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductExtensionOptionID(transactionManager, _productExtensionOptionID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtensionOptionProduct_ZNodeProductExtensionOption key.
		///		fKZNodeProductExtensionOptionProductZNodeProductExtensionOption Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productExtensionOptionID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct objects.</returns>
		public TList<ProductExtensionOptionProduct> GetByProductExtensionOptionID(System.Int32? _productExtensionOptionID, int start, int pageLength)
		{
			int count =  -1;
			return GetByProductExtensionOptionID(null, _productExtensionOptionID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtensionOptionProduct_ZNodeProductExtensionOption key.
		///		fKZNodeProductExtensionOptionProductZNodeProductExtensionOption Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productExtensionOptionID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct objects.</returns>
		public TList<ProductExtensionOptionProduct> GetByProductExtensionOptionID(System.Int32? _productExtensionOptionID, int start, int pageLength,out int count)
		{
			return GetByProductExtensionOptionID(null, _productExtensionOptionID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtensionOptionProduct_ZNodeProductExtensionOption key.
		///		FK_ZNodeProductExtensionOptionProduct_ZNodeProductExtensionOption Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionOptionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct objects.</returns>
		public abstract TList<ProductExtensionOptionProduct> GetByProductExtensionOptionID(TransactionManager transactionManager, System.Int32? _productExtensionOptionID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProductKey key, int start, int pageLength)
		{
			return GetByProductExtensionOptionProductLinkID(transactionManager, key.ProductExtensionOptionProductLinkID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeProductExtensionOptionProduct index.
		/// </summary>
		/// <param name="_productExtensionOptionProductLinkID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct GetByProductExtensionOptionProductLinkID(System.Int32 _productExtensionOptionProductLinkID)
		{
			int count = -1;
			return GetByProductExtensionOptionProductLinkID(null,_productExtensionOptionProductLinkID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtensionOptionProduct index.
		/// </summary>
		/// <param name="_productExtensionOptionProductLinkID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct GetByProductExtensionOptionProductLinkID(System.Int32 _productExtensionOptionProductLinkID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductExtensionOptionProductLinkID(null, _productExtensionOptionProductLinkID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtensionOptionProduct index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionOptionProductLinkID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct GetByProductExtensionOptionProductLinkID(TransactionManager transactionManager, System.Int32 _productExtensionOptionProductLinkID)
		{
			int count = -1;
			return GetByProductExtensionOptionProductLinkID(transactionManager, _productExtensionOptionProductLinkID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtensionOptionProduct index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionOptionProductLinkID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct GetByProductExtensionOptionProductLinkID(TransactionManager transactionManager, System.Int32 _productExtensionOptionProductLinkID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductExtensionOptionProductLinkID(transactionManager, _productExtensionOptionProductLinkID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtensionOptionProduct index.
		/// </summary>
		/// <param name="_productExtensionOptionProductLinkID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct GetByProductExtensionOptionProductLinkID(System.Int32 _productExtensionOptionProductLinkID, int start, int pageLength, out int count)
		{
			return GetByProductExtensionOptionProductLinkID(null, _productExtensionOptionProductLinkID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtensionOptionProduct index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionOptionProductLinkID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct GetByProductExtensionOptionProductLinkID(TransactionManager transactionManager, System.Int32 _productExtensionOptionProductLinkID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ProductExtensionOptionProduct&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ProductExtensionOptionProduct&gt;"/></returns>
		public static TList<ProductExtensionOptionProduct> Fill(IDataReader reader, TList<ProductExtensionOptionProduct> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ProductExtensionOptionProduct")
					.Append("|").Append((System.Int32)reader[((int)ProductExtensionOptionProductColumn.ProductExtensionOptionProductLinkID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ProductExtensionOptionProduct>(
					key.ToString(), // EntityTrackingKey
					"ProductExtensionOptionProduct",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ProductExtensionOptionProductLinkID = (System.Int32)reader[((int)ProductExtensionOptionProductColumn.ProductExtensionOptionProductLinkID - 1)];
					c.ProductExtensionOptionID = (reader.IsDBNull(((int)ProductExtensionOptionProductColumn.ProductExtensionOptionID - 1)))?null:(System.Int32?)reader[((int)ProductExtensionOptionProductColumn.ProductExtensionOptionID - 1)];
					c.ProductID = (reader.IsDBNull(((int)ProductExtensionOptionProductColumn.ProductID - 1)))?null:(System.Int32?)reader[((int)ProductExtensionOptionProductColumn.ProductID - 1)];
					c.SKU = (reader.IsDBNull(((int)ProductExtensionOptionProductColumn.SKU - 1)))?null:(System.String)reader[((int)ProductExtensionOptionProductColumn.SKU - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct entity)
		{
			if (!reader.Read()) return;
			
			entity.ProductExtensionOptionProductLinkID = (System.Int32)reader[((int)ProductExtensionOptionProductColumn.ProductExtensionOptionProductLinkID - 1)];
			entity.ProductExtensionOptionID = (reader.IsDBNull(((int)ProductExtensionOptionProductColumn.ProductExtensionOptionID - 1)))?null:(System.Int32?)reader[((int)ProductExtensionOptionProductColumn.ProductExtensionOptionID - 1)];
			entity.ProductID = (reader.IsDBNull(((int)ProductExtensionOptionProductColumn.ProductID - 1)))?null:(System.Int32?)reader[((int)ProductExtensionOptionProductColumn.ProductID - 1)];
			entity.SKU = (reader.IsDBNull(((int)ProductExtensionOptionProductColumn.SKU - 1)))?null:(System.String)reader[((int)ProductExtensionOptionProductColumn.SKU - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProductExtensionOptionProductLinkID = (System.Int32)dataRow["ProductExtensionOptionProductLinkID"];
			entity.ProductExtensionOptionID = Convert.IsDBNull(dataRow["ProductExtensionOptionID"]) ? null : (System.Int32?)dataRow["ProductExtensionOptionID"];
			entity.ProductID = Convert.IsDBNull(dataRow["ProductID"]) ? null : (System.Int32?)dataRow["ProductID"];
			entity.SKU = Convert.IsDBNull(dataRow["SKU"]) ? null : (System.String)dataRow["SKU"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ProductExtensionOptionIDSource	
			if (CanDeepLoad(entity, "ProductExtensionOption|ProductExtensionOptionIDSource", deepLoadType, innerList) 
				&& entity.ProductExtensionOptionIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ProductExtensionOptionID ?? (int)0);
				ProductExtensionOption tmpEntity = EntityManager.LocateEntity<ProductExtensionOption>(EntityLocator.ConstructKeyFromPkItems(typeof(ProductExtensionOption), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductExtensionOptionIDSource = tmpEntity;
				else
					entity.ProductExtensionOptionIDSource = DataRepository.ProductExtensionOptionProvider.GetByProductExtensionOptionID(transactionManager, (entity.ProductExtensionOptionID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductExtensionOptionIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductExtensionOptionIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductExtensionOptionProvider.DeepLoad(transactionManager, entity.ProductExtensionOptionIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductExtensionOptionIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ProductExtensionOptionIDSource
			if (CanDeepSave(entity, "ProductExtensionOption|ProductExtensionOptionIDSource", deepSaveType, innerList) 
				&& entity.ProductExtensionOptionIDSource != null)
			{
				DataRepository.ProductExtensionOptionProvider.Save(transactionManager, entity.ProductExtensionOptionIDSource);
				entity.ProductExtensionOptionID = entity.ProductExtensionOptionIDSource.ProductExtensionOptionID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductExtensionOptionProductChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ProductExtensionOptionProduct</c>
	///</summary>
	public enum ProductExtensionOptionProductChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>ProductExtensionOption</c> at ProductExtensionOptionIDSource
		///</summary>
		[ChildEntityType(typeof(ProductExtensionOption))]
		ProductExtensionOption,
	}
	
	#endregion ProductExtensionOptionProductChildEntityTypes
	
	#region ProductExtensionOptionProductFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductExtensionOptionProductColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtensionOptionProduct"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionOptionProductFilterBuilder : SqlFilterBuilder<ProductExtensionOptionProductColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionProductFilterBuilder class.
		/// </summary>
		public ProductExtensionOptionProductFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionProductFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionOptionProductFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionProductFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionOptionProductFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionOptionProductFilterBuilder
	
	#region ProductExtensionOptionProductParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductExtensionOptionProductColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtensionOptionProduct"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionOptionProductParameterBuilder : ParameterizedSqlFilterBuilder<ProductExtensionOptionProductColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionProductParameterBuilder class.
		/// </summary>
		public ProductExtensionOptionProductParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionProductParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionOptionProductParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionProductParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionOptionProductParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionOptionProductParameterBuilder
	
	#region ProductExtensionOptionProductSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductExtensionOptionProductColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtensionOptionProduct"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductExtensionOptionProductSortBuilder : SqlSortBuilder<ProductExtensionOptionProductColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionOptionProductSqlSortBuilder class.
		/// </summary>
		public ProductExtensionOptionProductSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductExtensionOptionProductSortBuilder
	
} // end namespace
