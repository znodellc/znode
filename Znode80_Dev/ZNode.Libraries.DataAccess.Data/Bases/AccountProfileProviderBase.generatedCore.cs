﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AccountProfileProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AccountProfileProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.AccountProfile, ZNode.Libraries.DataAccess.Entities.AccountProfileKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AccountProfileKey key)
		{
			return Delete(transactionManager, key.AccountProfileID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_accountProfileID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _accountProfileID)
		{
			return Delete(null, _accountProfileID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountProfileID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _accountProfileID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalAccount_ZNodeAccount key.
		///		FK_ZNodePortalAccount_ZNodeAccount Description: 
		/// </summary>
		/// <param name="_accountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountProfile objects.</returns>
		public TList<AccountProfile> GetByAccountID(System.Int32 _accountID)
		{
			int count = -1;
			return GetByAccountID(_accountID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalAccount_ZNodeAccount key.
		///		FK_ZNodePortalAccount_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountProfile objects.</returns>
		/// <remarks></remarks>
		public TList<AccountProfile> GetByAccountID(TransactionManager transactionManager, System.Int32 _accountID)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalAccount_ZNodeAccount key.
		///		FK_ZNodePortalAccount_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountProfile objects.</returns>
		public TList<AccountProfile> GetByAccountID(TransactionManager transactionManager, System.Int32 _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalAccount_ZNodeAccount key.
		///		fKZNodePortalAccountZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_accountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountProfile objects.</returns>
		public TList<AccountProfile> GetByAccountID(System.Int32 _accountID, int start, int pageLength)
		{
			int count =  -1;
			return GetByAccountID(null, _accountID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalAccount_ZNodeAccount key.
		///		fKZNodePortalAccountZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_accountID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountProfile objects.</returns>
		public TList<AccountProfile> GetByAccountID(System.Int32 _accountID, int start, int pageLength,out int count)
		{
			return GetByAccountID(null, _accountID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalAccount_ZNodeAccount key.
		///		FK_ZNodePortalAccount_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountProfile objects.</returns>
		public abstract TList<AccountProfile> GetByAccountID(TransactionManager transactionManager, System.Int32 _accountID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalAccount_ZNodeProfile key.
		///		FK_ZNodePortalAccount_ZNodeProfile Description: 
		/// </summary>
		/// <param name="_profileID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountProfile objects.</returns>
		public TList<AccountProfile> GetByProfileID(System.Int32? _profileID)
		{
			int count = -1;
			return GetByProfileID(_profileID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalAccount_ZNodeProfile key.
		///		FK_ZNodePortalAccount_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountProfile objects.</returns>
		/// <remarks></remarks>
		public TList<AccountProfile> GetByProfileID(TransactionManager transactionManager, System.Int32? _profileID)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalAccount_ZNodeProfile key.
		///		FK_ZNodePortalAccount_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountProfile objects.</returns>
		public TList<AccountProfile> GetByProfileID(TransactionManager transactionManager, System.Int32? _profileID, int start, int pageLength)
		{
			int count = -1;
			return GetByProfileID(transactionManager, _profileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalAccount_ZNodeProfile key.
		///		fKZNodePortalAccountZNodeProfile Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_profileID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountProfile objects.</returns>
		public TList<AccountProfile> GetByProfileID(System.Int32? _profileID, int start, int pageLength)
		{
			int count =  -1;
			return GetByProfileID(null, _profileID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalAccount_ZNodeProfile key.
		///		fKZNodePortalAccountZNodeProfile Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_profileID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountProfile objects.</returns>
		public TList<AccountProfile> GetByProfileID(System.Int32? _profileID, int start, int pageLength,out int count)
		{
			return GetByProfileID(null, _profileID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePortalAccount_ZNodeProfile key.
		///		FK_ZNodePortalAccount_ZNodeProfile Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.AccountProfile objects.</returns>
		public abstract TList<AccountProfile> GetByProfileID(TransactionManager transactionManager, System.Int32? _profileID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.AccountProfile Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AccountProfileKey key, int start, int pageLength)
		{
			return GetByAccountProfileID(transactionManager, key.AccountProfileID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeAccountProfile_AccountIDProfileID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="_profileID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AccountProfile&gt;"/> class.</returns>
		public TList<AccountProfile> GetByAccountIDProfileID(System.Int32 _accountID, System.Int32? _profileID)
		{
			int count = -1;
			return GetByAccountIDProfileID(null,_accountID, _profileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccountProfile_AccountIDProfileID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AccountProfile&gt;"/> class.</returns>
		public TList<AccountProfile> GetByAccountIDProfileID(System.Int32 _accountID, System.Int32? _profileID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountIDProfileID(null, _accountID, _profileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccountProfile_AccountIDProfileID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="_profileID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AccountProfile&gt;"/> class.</returns>
		public TList<AccountProfile> GetByAccountIDProfileID(TransactionManager transactionManager, System.Int32 _accountID, System.Int32? _profileID)
		{
			int count = -1;
			return GetByAccountIDProfileID(transactionManager, _accountID, _profileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccountProfile_AccountIDProfileID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AccountProfile&gt;"/> class.</returns>
		public TList<AccountProfile> GetByAccountIDProfileID(TransactionManager transactionManager, System.Int32 _accountID, System.Int32? _profileID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountIDProfileID(transactionManager, _accountID, _profileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccountProfile_AccountIDProfileID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;AccountProfile&gt;"/> class.</returns>
		public TList<AccountProfile> GetByAccountIDProfileID(System.Int32 _accountID, System.Int32? _profileID, int start, int pageLength, out int count)
		{
			return GetByAccountIDProfileID(null, _accountID, _profileID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeAccountProfile_AccountIDProfileID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="_profileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;AccountProfile&gt;"/> class.</returns>
		public abstract TList<AccountProfile> GetByAccountIDProfileID(TransactionManager transactionManager, System.Int32 _accountID, System.Int32? _profileID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodePortalAccount index.
		/// </summary>
		/// <param name="_accountProfileID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AccountProfile GetByAccountProfileID(System.Int32 _accountProfileID)
		{
			int count = -1;
			return GetByAccountProfileID(null,_accountProfileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalAccount index.
		/// </summary>
		/// <param name="_accountProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AccountProfile GetByAccountProfileID(System.Int32 _accountProfileID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountProfileID(null, _accountProfileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalAccount index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountProfileID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AccountProfile GetByAccountProfileID(TransactionManager transactionManager, System.Int32 _accountProfileID)
		{
			int count = -1;
			return GetByAccountProfileID(transactionManager, _accountProfileID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalAccount index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AccountProfile GetByAccountProfileID(TransactionManager transactionManager, System.Int32 _accountProfileID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountProfileID(transactionManager, _accountProfileID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalAccount index.
		/// </summary>
		/// <param name="_accountProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountProfile"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AccountProfile GetByAccountProfileID(System.Int32 _accountProfileID, int start, int pageLength, out int count)
		{
			return GetByAccountProfileID(null, _accountProfileID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalAccount index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountProfileID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AccountProfile"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.AccountProfile GetByAccountProfileID(TransactionManager transactionManager, System.Int32 _accountProfileID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AccountProfile&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AccountProfile&gt;"/></returns>
		public static TList<AccountProfile> Fill(IDataReader reader, TList<AccountProfile> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.AccountProfile c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AccountProfile")
					.Append("|").Append((System.Int32)reader[((int)AccountProfileColumn.AccountProfileID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AccountProfile>(
					key.ToString(), // EntityTrackingKey
					"AccountProfile",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.AccountProfile();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.AccountProfileID = (System.Int32)reader[((int)AccountProfileColumn.AccountProfileID - 1)];
					c.ProfileID = (reader.IsDBNull(((int)AccountProfileColumn.ProfileID - 1)))?null:(System.Int32?)reader[((int)AccountProfileColumn.ProfileID - 1)];
					c.AccountID = (System.Int32)reader[((int)AccountProfileColumn.AccountID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.AccountProfile"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AccountProfile"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.AccountProfile entity)
		{
			if (!reader.Read()) return;
			
			entity.AccountProfileID = (System.Int32)reader[((int)AccountProfileColumn.AccountProfileID - 1)];
			entity.ProfileID = (reader.IsDBNull(((int)AccountProfileColumn.ProfileID - 1)))?null:(System.Int32?)reader[((int)AccountProfileColumn.ProfileID - 1)];
			entity.AccountID = (System.Int32)reader[((int)AccountProfileColumn.AccountID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.AccountProfile"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AccountProfile"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.AccountProfile entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.AccountProfileID = (System.Int32)dataRow["AccountProfileID"];
			entity.ProfileID = Convert.IsDBNull(dataRow["ProfileID"]) ? null : (System.Int32?)dataRow["ProfileID"];
			entity.AccountID = (System.Int32)dataRow["AccountID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AccountProfile"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.AccountProfile Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AccountProfile entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AccountIDSource	
			if (CanDeepLoad(entity, "Account|AccountIDSource", deepLoadType, innerList) 
				&& entity.AccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AccountID;
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AccountIDSource = tmpEntity;
				else
					entity.AccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, entity.AccountID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.AccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AccountIDSource

			#region ProfileIDSource	
			if (CanDeepLoad(entity, "Profile|ProfileIDSource", deepLoadType, innerList) 
				&& entity.ProfileIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ProfileID ?? (int)0);
				Profile tmpEntity = EntityManager.LocateEntity<Profile>(EntityLocator.ConstructKeyFromPkItems(typeof(Profile), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProfileIDSource = tmpEntity;
				else
					entity.ProfileIDSource = DataRepository.ProfileProvider.GetByProfileID(transactionManager, (entity.ProfileID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProfileIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProfileIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProfileProvider.DeepLoad(transactionManager, entity.ProfileIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProfileIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.AccountProfile object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.AccountProfile instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.AccountProfile Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AccountProfile entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AccountIDSource
			if (CanDeepSave(entity, "Account|AccountIDSource", deepSaveType, innerList) 
				&& entity.AccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.AccountIDSource);
				entity.AccountID = entity.AccountIDSource.AccountID;
			}
			#endregion 
			
			#region ProfileIDSource
			if (CanDeepSave(entity, "Profile|ProfileIDSource", deepSaveType, innerList) 
				&& entity.ProfileIDSource != null)
			{
				DataRepository.ProfileProvider.Save(transactionManager, entity.ProfileIDSource);
				entity.ProfileID = entity.ProfileIDSource.ProfileID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AccountProfileChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.AccountProfile</c>
	///</summary>
	public enum AccountProfileChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Account</c> at AccountIDSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
		
		///<summary>
		/// Composite Property for <c>Profile</c> at ProfileIDSource
		///</summary>
		[ChildEntityType(typeof(Profile))]
		Profile,
	}
	
	#endregion AccountProfileChildEntityTypes
	
	#region AccountProfileFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AccountProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AccountProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountProfileFilterBuilder : SqlFilterBuilder<AccountProfileColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountProfileFilterBuilder class.
		/// </summary>
		public AccountProfileFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountProfileFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountProfileFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountProfileFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountProfileFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountProfileFilterBuilder
	
	#region AccountProfileParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AccountProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AccountProfile"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AccountProfileParameterBuilder : ParameterizedSqlFilterBuilder<AccountProfileColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountProfileParameterBuilder class.
		/// </summary>
		public AccountProfileParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AccountProfileParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AccountProfileParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AccountProfileParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AccountProfileParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AccountProfileParameterBuilder
	
	#region AccountProfileSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AccountProfileColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AccountProfile"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AccountProfileSortBuilder : SqlSortBuilder<AccountProfileColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AccountProfileSqlSortBuilder class.
		/// </summary>
		public AccountProfileSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AccountProfileSortBuilder
	
} // end namespace
