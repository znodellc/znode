﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TaxClassProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TaxClassProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.TaxClass, ZNode.Libraries.DataAccess.Entities.TaxClassKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TaxClassKey key)
		{
			return Delete(transactionManager, key.TaxClassID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_taxClassID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _taxClassID)
		{
			return Delete(null, _taxClassID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _taxClassID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTaxClass_ZNodePortal key.
		///		FK_ZNodeTaxClass_ZNodePortal Description: 
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TaxClass objects.</returns>
		public TList<TaxClass> GetByPortalID(System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(_portalID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTaxClass_ZNodePortal key.
		///		FK_ZNodeTaxClass_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TaxClass objects.</returns>
		/// <remarks></remarks>
		public TList<TaxClass> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTaxClass_ZNodePortal key.
		///		FK_ZNodeTaxClass_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TaxClass objects.</returns>
		public TList<TaxClass> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTaxClass_ZNodePortal key.
		///		fKZNodeTaxClassZNodePortal Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TaxClass objects.</returns>
		public TList<TaxClass> GetByPortalID(System.Int32? _portalID, int start, int pageLength)
		{
			int count =  -1;
			return GetByPortalID(null, _portalID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTaxClass_ZNodePortal key.
		///		fKZNodeTaxClassZNodePortal Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_portalID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TaxClass objects.</returns>
		public TList<TaxClass> GetByPortalID(System.Int32? _portalID, int start, int pageLength,out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeTaxClass_ZNodePortal key.
		///		FK_ZNodeTaxClass_ZNodePortal Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.TaxClass objects.</returns>
		public abstract TList<TaxClass> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.TaxClass Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TaxClassKey key, int start, int pageLength)
		{
			return GetByTaxClassID(transactionManager, key.TaxClassID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeTaxClass_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxClass&gt;"/> class.</returns>
		public TList<TaxClass> GetByActiveInd(System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxClass_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxClass&gt;"/> class.</returns>
		public TList<TaxClass> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxClass_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxClass&gt;"/> class.</returns>
		public TList<TaxClass> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxClass_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxClass&gt;"/> class.</returns>
		public TList<TaxClass> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxClass_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxClass&gt;"/> class.</returns>
		public TList<TaxClass> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxClass_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxClass&gt;"/> class.</returns>
		public abstract TList<TaxClass> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeTaxClass_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxClass&gt;"/> class.</returns>
		public TList<TaxClass> GetByExternalID(System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(null,_externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxClass_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxClass&gt;"/> class.</returns>
		public TList<TaxClass> GetByExternalID(System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxClass_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxClass&gt;"/> class.</returns>
		public TList<TaxClass> GetByExternalID(TransactionManager transactionManager, System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxClass_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxClass&gt;"/> class.</returns>
		public TList<TaxClass> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxClass_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxClass&gt;"/> class.</returns>
		public TList<TaxClass> GetByExternalID(System.String _externalID, int start, int pageLength, out int count)
		{
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxClass_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxClass&gt;"/> class.</returns>
		public abstract TList<TaxClass> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeTaxClass_Name_PortalID index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="_portalID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxClass GetByNamePortalID(System.String _name, System.Int32? _portalID)
		{
			int count = -1;
			return GetByNamePortalID(null,_name, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxClass_Name_PortalID index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxClass GetByNamePortalID(System.String _name, System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByNamePortalID(null, _name, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxClass_Name_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxClass GetByNamePortalID(TransactionManager transactionManager, System.String _name, System.Int32? _portalID)
		{
			int count = -1;
			return GetByNamePortalID(transactionManager, _name, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxClass_Name_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxClass GetByNamePortalID(TransactionManager transactionManager, System.String _name, System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByNamePortalID(transactionManager, _name, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxClass_Name_PortalID index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxClass GetByNamePortalID(System.String _name, System.Int32? _portalID, int start, int pageLength, out int count)
		{
			return GetByNamePortalID(null, _name, _portalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxClass_Name_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.TaxClass GetByNamePortalID(TransactionManager transactionManager, System.String _name, System.Int32? _portalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeTaxClass index.
		/// </summary>
		/// <param name="_taxClassID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxClass GetByTaxClassID(System.Int32 _taxClassID)
		{
			int count = -1;
			return GetByTaxClassID(null,_taxClassID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTaxClass index.
		/// </summary>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxClass GetByTaxClassID(System.Int32 _taxClassID, int start, int pageLength)
		{
			int count = -1;
			return GetByTaxClassID(null, _taxClassID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTaxClass index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxClass GetByTaxClassID(TransactionManager transactionManager, System.Int32 _taxClassID)
		{
			int count = -1;
			return GetByTaxClassID(transactionManager, _taxClassID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTaxClass index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxClass GetByTaxClassID(TransactionManager transactionManager, System.Int32 _taxClassID, int start, int pageLength)
		{
			int count = -1;
			return GetByTaxClassID(transactionManager, _taxClassID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTaxClass index.
		/// </summary>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxClass GetByTaxClassID(System.Int32 _taxClassID, int start, int pageLength, out int count)
		{
			return GetByTaxClassID(null, _taxClassID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTaxClass index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.TaxClass GetByTaxClassID(TransactionManager transactionManager, System.Int32 _taxClassID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;TaxClass&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;TaxClass&gt;"/></returns>
		public static TList<TaxClass> Fill(IDataReader reader, TList<TaxClass> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.TaxClass c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("TaxClass")
					.Append("|").Append((System.Int32)reader[((int)TaxClassColumn.TaxClassID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<TaxClass>(
					key.ToString(), // EntityTrackingKey
					"TaxClass",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.TaxClass();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.TaxClassID = (System.Int32)reader[((int)TaxClassColumn.TaxClassID - 1)];
					c.Name = (System.String)reader[((int)TaxClassColumn.Name - 1)];
					c.DisplayOrder = (reader.IsDBNull(((int)TaxClassColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)TaxClassColumn.DisplayOrder - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)TaxClassColumn.ActiveInd - 1)];
					c.PortalID = (reader.IsDBNull(((int)TaxClassColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)TaxClassColumn.PortalID - 1)];
					c.ExternalID = (reader.IsDBNull(((int)TaxClassColumn.ExternalID - 1)))?null:(System.String)reader[((int)TaxClassColumn.ExternalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.TaxClass entity)
		{
			if (!reader.Read()) return;
			
			entity.TaxClassID = (System.Int32)reader[((int)TaxClassColumn.TaxClassID - 1)];
			entity.Name = (System.String)reader[((int)TaxClassColumn.Name - 1)];
			entity.DisplayOrder = (reader.IsDBNull(((int)TaxClassColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)TaxClassColumn.DisplayOrder - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)TaxClassColumn.ActiveInd - 1)];
			entity.PortalID = (reader.IsDBNull(((int)TaxClassColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)TaxClassColumn.PortalID - 1)];
			entity.ExternalID = (reader.IsDBNull(((int)TaxClassColumn.ExternalID - 1)))?null:(System.String)reader[((int)TaxClassColumn.ExternalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.TaxClass entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.TaxClassID = (System.Int32)dataRow["TaxClassID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.DisplayOrder = Convert.IsDBNull(dataRow["DisplayOrder"]) ? null : (System.Int32?)dataRow["DisplayOrder"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.PortalID = Convert.IsDBNull(dataRow["PortalID"]) ? null : (System.Int32?)dataRow["PortalID"];
			entity.ExternalID = Convert.IsDBNull(dataRow["ExternalID"]) ? null : (System.String)dataRow["ExternalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.TaxClass"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.TaxClass Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TaxClass entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PortalID ?? (int)0);
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, (entity.PortalID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByTaxClassID methods when available
			
			#region TaxRuleCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TaxRule>|TaxRuleCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TaxRuleCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TaxRuleCollection = DataRepository.TaxRuleProvider.GetByTaxClassID(transactionManager, entity.TaxClassID);

				if (deep && entity.TaxRuleCollection.Count > 0)
				{
					deepHandles.Add("TaxRuleCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TaxRule>) DataRepository.TaxRuleProvider.DeepLoad,
						new object[] { transactionManager, entity.TaxRuleCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AddOnValueCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AddOnValue>|AddOnValueCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AddOnValueCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AddOnValueCollection = DataRepository.AddOnValueProvider.GetByTaxClassID(transactionManager, entity.TaxClassID);

				if (deep && entity.AddOnValueCollection.Count > 0)
				{
					deepHandles.Add("AddOnValueCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AddOnValue>) DataRepository.AddOnValueProvider.DeepLoad,
						new object[] { transactionManager, entity.AddOnValueCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Product>|ProductCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductCollection = DataRepository.ProductProvider.GetByTaxClassID(transactionManager, entity.TaxClassID);

				if (deep && entity.ProductCollection.Count > 0)
				{
					deepHandles.Add("ProductCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Product>) DataRepository.ProductProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProfileCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Profile>|ProfileCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProfileCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProfileCollection = DataRepository.ProfileProvider.GetByTaxClassID(transactionManager, entity.TaxClassID);

				if (deep && entity.ProfileCollection.Count > 0)
				{
					deepHandles.Add("ProfileCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Profile>) DataRepository.ProfileProvider.DeepLoad,
						new object[] { transactionManager, entity.ProfileCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.TaxClass object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.TaxClass instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.TaxClass Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TaxClass entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<TaxRule>
				if (CanDeepSave(entity.TaxRuleCollection, "List<TaxRule>|TaxRuleCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TaxRule child in entity.TaxRuleCollection)
					{
						if(child.TaxClassIDSource != null)
						{
							child.TaxClassID = child.TaxClassIDSource.TaxClassID;
						}
						else
						{
							child.TaxClassID = entity.TaxClassID;
						}

					}

					if (entity.TaxRuleCollection.Count > 0 || entity.TaxRuleCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TaxRuleProvider.Save(transactionManager, entity.TaxRuleCollection);
						
						deepHandles.Add("TaxRuleCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TaxRule >) DataRepository.TaxRuleProvider.DeepSave,
							new object[] { transactionManager, entity.TaxRuleCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<AddOnValue>
				if (CanDeepSave(entity.AddOnValueCollection, "List<AddOnValue>|AddOnValueCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AddOnValue child in entity.AddOnValueCollection)
					{
						if(child.TaxClassIDSource != null)
						{
							child.TaxClassID = child.TaxClassIDSource.TaxClassID;
						}
						else
						{
							child.TaxClassID = entity.TaxClassID;
						}

					}

					if (entity.AddOnValueCollection.Count > 0 || entity.AddOnValueCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AddOnValueProvider.Save(transactionManager, entity.AddOnValueCollection);
						
						deepHandles.Add("AddOnValueCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AddOnValue >) DataRepository.AddOnValueProvider.DeepSave,
							new object[] { transactionManager, entity.AddOnValueCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Product>
				if (CanDeepSave(entity.ProductCollection, "List<Product>|ProductCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Product child in entity.ProductCollection)
					{
						if(child.TaxClassIDSource != null)
						{
							child.TaxClassID = child.TaxClassIDSource.TaxClassID;
						}
						else
						{
							child.TaxClassID = entity.TaxClassID;
						}

					}

					if (entity.ProductCollection.Count > 0 || entity.ProductCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductProvider.Save(transactionManager, entity.ProductCollection);
						
						deepHandles.Add("ProductCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Product >) DataRepository.ProductProvider.DeepSave,
							new object[] { transactionManager, entity.ProductCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Profile>
				if (CanDeepSave(entity.ProfileCollection, "List<Profile>|ProfileCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Profile child in entity.ProfileCollection)
					{
						if(child.TaxClassIDSource != null)
						{
							child.TaxClassID = child.TaxClassIDSource.TaxClassID;
						}
						else
						{
							child.TaxClassID = entity.TaxClassID;
						}

					}

					if (entity.ProfileCollection.Count > 0 || entity.ProfileCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProfileProvider.Save(transactionManager, entity.ProfileCollection);
						
						deepHandles.Add("ProfileCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Profile >) DataRepository.ProfileProvider.DeepSave,
							new object[] { transactionManager, entity.ProfileCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TaxClassChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.TaxClass</c>
	///</summary>
	public enum TaxClassChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		///<summary>
		/// Collection of <c>TaxClass</c> as OneToMany for TaxRuleCollection
		///</summary>
		[ChildEntityType(typeof(TList<TaxRule>))]
		TaxRuleCollection,
		///<summary>
		/// Collection of <c>TaxClass</c> as OneToMany for AddOnValueCollection
		///</summary>
		[ChildEntityType(typeof(TList<AddOnValue>))]
		AddOnValueCollection,
		///<summary>
		/// Collection of <c>TaxClass</c> as OneToMany for ProductCollection
		///</summary>
		[ChildEntityType(typeof(TList<Product>))]
		ProductCollection,
		///<summary>
		/// Collection of <c>TaxClass</c> as OneToMany for ProfileCollection
		///</summary>
		[ChildEntityType(typeof(TList<Profile>))]
		ProfileCollection,
	}
	
	#endregion TaxClassChildEntityTypes
	
	#region TaxClassFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TaxClassColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TaxClass"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TaxClassFilterBuilder : SqlFilterBuilder<TaxClassColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TaxClassFilterBuilder class.
		/// </summary>
		public TaxClassFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TaxClassFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TaxClassFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TaxClassFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TaxClassFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TaxClassFilterBuilder
	
	#region TaxClassParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TaxClassColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TaxClass"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TaxClassParameterBuilder : ParameterizedSqlFilterBuilder<TaxClassColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TaxClassParameterBuilder class.
		/// </summary>
		public TaxClassParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TaxClassParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TaxClassParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TaxClassParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TaxClassParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TaxClassParameterBuilder
	
	#region TaxClassSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TaxClassColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TaxClass"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TaxClassSortBuilder : SqlSortBuilder<TaxClassColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TaxClassSqlSortBuilder class.
		/// </summary>
		public TaxClassSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TaxClassSortBuilder
	
} // end namespace
