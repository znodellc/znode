﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="GiftCardProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class GiftCardProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.GiftCard, ZNode.Libraries.DataAccess.Entities.GiftCardKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.GiftCardKey key)
		{
			return Delete(transactionManager, key.GiftCardId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_giftCardId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _giftCardId)
		{
			return Delete(null, _giftCardId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_giftCardId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _giftCardId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.GiftCard Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.GiftCardKey key, int start, int pageLength)
		{
			return GetByGiftCardId(transactionManager, key.GiftCardId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeGiftCard_AccountId index.
		/// </summary>
		/// <param name="_accountId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByAccountId(System.Int32? _accountId)
		{
			int count = -1;
			return GetByAccountId(null,_accountId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_AccountId index.
		/// </summary>
		/// <param name="_accountId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByAccountId(System.Int32? _accountId, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountId(null, _accountId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_AccountId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByAccountId(TransactionManager transactionManager, System.Int32? _accountId)
		{
			int count = -1;
			return GetByAccountId(transactionManager, _accountId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_AccountId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByAccountId(TransactionManager transactionManager, System.Int32? _accountId, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountId(transactionManager, _accountId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_AccountId index.
		/// </summary>
		/// <param name="_accountId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByAccountId(System.Int32? _accountId, int start, int pageLength, out int count)
		{
			return GetByAccountId(null, _accountId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_AccountId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public abstract TList<GiftCard> GetByAccountId(TransactionManager transactionManager, System.Int32? _accountId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeGiftCard_Amount index.
		/// </summary>
		/// <param name="_amount"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByAmount(System.Decimal? _amount)
		{
			int count = -1;
			return GetByAmount(null,_amount, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_Amount index.
		/// </summary>
		/// <param name="_amount"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByAmount(System.Decimal? _amount, int start, int pageLength)
		{
			int count = -1;
			return GetByAmount(null, _amount, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_Amount index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_amount"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByAmount(TransactionManager transactionManager, System.Decimal? _amount)
		{
			int count = -1;
			return GetByAmount(transactionManager, _amount, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_Amount index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_amount"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByAmount(TransactionManager transactionManager, System.Decimal? _amount, int start, int pageLength)
		{
			int count = -1;
			return GetByAmount(transactionManager, _amount, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_Amount index.
		/// </summary>
		/// <param name="_amount"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByAmount(System.Decimal? _amount, int start, int pageLength, out int count)
		{
			return GetByAmount(null, _amount, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_Amount index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_amount"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public abstract TList<GiftCard> GetByAmount(TransactionManager transactionManager, System.Decimal? _amount, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeGiftCard_CardNumber index.
		/// </summary>
		/// <param name="_cardNumber"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.GiftCard GetByCardNumber(System.String _cardNumber)
		{
			int count = -1;
			return GetByCardNumber(null,_cardNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_CardNumber index.
		/// </summary>
		/// <param name="_cardNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.GiftCard GetByCardNumber(System.String _cardNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByCardNumber(null, _cardNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_CardNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cardNumber"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.GiftCard GetByCardNumber(TransactionManager transactionManager, System.String _cardNumber)
		{
			int count = -1;
			return GetByCardNumber(transactionManager, _cardNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_CardNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cardNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.GiftCard GetByCardNumber(TransactionManager transactionManager, System.String _cardNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByCardNumber(transactionManager, _cardNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_CardNumber index.
		/// </summary>
		/// <param name="_cardNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.GiftCard GetByCardNumber(System.String _cardNumber, int start, int pageLength, out int count)
		{
			return GetByCardNumber(null, _cardNumber, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_CardNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cardNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.GiftCard GetByCardNumber(TransactionManager transactionManager, System.String _cardNumber, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeGiftCard_CreateDate index.
		/// </summary>
		/// <param name="_createDate"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByCreateDate(System.DateTime _createDate)
		{
			int count = -1;
			return GetByCreateDate(null,_createDate, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_CreateDate index.
		/// </summary>
		/// <param name="_createDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByCreateDate(System.DateTime _createDate, int start, int pageLength)
		{
			int count = -1;
			return GetByCreateDate(null, _createDate, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_CreateDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createDate"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByCreateDate(TransactionManager transactionManager, System.DateTime _createDate)
		{
			int count = -1;
			return GetByCreateDate(transactionManager, _createDate, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_CreateDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByCreateDate(TransactionManager transactionManager, System.DateTime _createDate, int start, int pageLength)
		{
			int count = -1;
			return GetByCreateDate(transactionManager, _createDate, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_CreateDate index.
		/// </summary>
		/// <param name="_createDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByCreateDate(System.DateTime _createDate, int start, int pageLength, out int count)
		{
			return GetByCreateDate(null, _createDate, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_CreateDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public abstract TList<GiftCard> GetByCreateDate(TransactionManager transactionManager, System.DateTime _createDate, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeGiftCard_CreatedBy index.
		/// </summary>
		/// <param name="_createdBy"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByCreatedBy(System.Int32 _createdBy)
		{
			int count = -1;
			return GetByCreatedBy(null,_createdBy, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_CreatedBy index.
		/// </summary>
		/// <param name="_createdBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByCreatedBy(System.Int32 _createdBy, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedBy(null, _createdBy, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_CreatedBy index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdBy"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByCreatedBy(TransactionManager transactionManager, System.Int32 _createdBy)
		{
			int count = -1;
			return GetByCreatedBy(transactionManager, _createdBy, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_CreatedBy index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByCreatedBy(TransactionManager transactionManager, System.Int32 _createdBy, int start, int pageLength)
		{
			int count = -1;
			return GetByCreatedBy(transactionManager, _createdBy, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_CreatedBy index.
		/// </summary>
		/// <param name="_createdBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByCreatedBy(System.Int32 _createdBy, int start, int pageLength, out int count)
		{
			return GetByCreatedBy(null, _createdBy, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_CreatedBy index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_createdBy"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public abstract TList<GiftCard> GetByCreatedBy(TransactionManager transactionManager, System.Int32 _createdBy, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeGiftCard_ExpirationDate index.
		/// </summary>
		/// <param name="_expirationDate"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByExpirationDate(System.DateTime? _expirationDate)
		{
			int count = -1;
			return GetByExpirationDate(null,_expirationDate, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_ExpirationDate index.
		/// </summary>
		/// <param name="_expirationDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByExpirationDate(System.DateTime? _expirationDate, int start, int pageLength)
		{
			int count = -1;
			return GetByExpirationDate(null, _expirationDate, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_ExpirationDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_expirationDate"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByExpirationDate(TransactionManager transactionManager, System.DateTime? _expirationDate)
		{
			int count = -1;
			return GetByExpirationDate(transactionManager, _expirationDate, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_ExpirationDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_expirationDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByExpirationDate(TransactionManager transactionManager, System.DateTime? _expirationDate, int start, int pageLength)
		{
			int count = -1;
			return GetByExpirationDate(transactionManager, _expirationDate, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_ExpirationDate index.
		/// </summary>
		/// <param name="_expirationDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByExpirationDate(System.DateTime? _expirationDate, int start, int pageLength, out int count)
		{
			return GetByExpirationDate(null, _expirationDate, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_ExpirationDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_expirationDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public abstract TList<GiftCard> GetByExpirationDate(TransactionManager transactionManager, System.DateTime? _expirationDate, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeGiftCard_Name index.
		/// </summary>
		/// <param name="_name"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByName(System.String _name)
		{
			int count = -1;
			return GetByName(null,_name, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_Name index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByName(System.String _name, int start, int pageLength)
		{
			int count = -1;
			return GetByName(null, _name, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_Name index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByName(TransactionManager transactionManager, System.String _name)
		{
			int count = -1;
			return GetByName(transactionManager, _name, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_Name index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByName(TransactionManager transactionManager, System.String _name, int start, int pageLength)
		{
			int count = -1;
			return GetByName(transactionManager, _name, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_Name index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByName(System.String _name, int start, int pageLength, out int count)
		{
			return GetByName(null, _name, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_Name index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public abstract TList<GiftCard> GetByName(TransactionManager transactionManager, System.String _name, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeGiftCard_OrderItemId index.
		/// </summary>
		/// <param name="_orderItemId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByOrderItemId(System.Int32? _orderItemId)
		{
			int count = -1;
			return GetByOrderItemId(null,_orderItemId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_OrderItemId index.
		/// </summary>
		/// <param name="_orderItemId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByOrderItemId(System.Int32? _orderItemId, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderItemId(null, _orderItemId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_OrderItemId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderItemId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByOrderItemId(TransactionManager transactionManager, System.Int32? _orderItemId)
		{
			int count = -1;
			return GetByOrderItemId(transactionManager, _orderItemId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_OrderItemId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderItemId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByOrderItemId(TransactionManager transactionManager, System.Int32? _orderItemId, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderItemId(transactionManager, _orderItemId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_OrderItemId index.
		/// </summary>
		/// <param name="_orderItemId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByOrderItemId(System.Int32? _orderItemId, int start, int pageLength, out int count)
		{
			return GetByOrderItemId(null, _orderItemId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_OrderItemId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderItemId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public abstract TList<GiftCard> GetByOrderItemId(TransactionManager transactionManager, System.Int32? _orderItemId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeGiftCard_PortalId index.
		/// </summary>
		/// <param name="_portalId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByPortalId(System.Int32 _portalId)
		{
			int count = -1;
			return GetByPortalId(null,_portalId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_PortalId index.
		/// </summary>
		/// <param name="_portalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByPortalId(System.Int32 _portalId, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalId(null, _portalId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_PortalId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByPortalId(TransactionManager transactionManager, System.Int32 _portalId)
		{
			int count = -1;
			return GetByPortalId(transactionManager, _portalId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_PortalId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByPortalId(TransactionManager transactionManager, System.Int32 _portalId, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalId(transactionManager, _portalId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_PortalId index.
		/// </summary>
		/// <param name="_portalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public TList<GiftCard> GetByPortalId(System.Int32 _portalId, int start, int pageLength, out int count)
		{
			return GetByPortalId(null, _portalId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeGiftCard_PortalId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;GiftCard&gt;"/> class.</returns>
		public abstract TList<GiftCard> GetByPortalId(TransactionManager transactionManager, System.Int32 _portalId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeGiftCard index.
		/// </summary>
		/// <param name="_giftCardId"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.GiftCard GetByGiftCardId(System.Int32 _giftCardId)
		{
			int count = -1;
			return GetByGiftCardId(null,_giftCardId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeGiftCard index.
		/// </summary>
		/// <param name="_giftCardId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.GiftCard GetByGiftCardId(System.Int32 _giftCardId, int start, int pageLength)
		{
			int count = -1;
			return GetByGiftCardId(null, _giftCardId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeGiftCard index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_giftCardId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.GiftCard GetByGiftCardId(TransactionManager transactionManager, System.Int32 _giftCardId)
		{
			int count = -1;
			return GetByGiftCardId(transactionManager, _giftCardId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeGiftCard index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_giftCardId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.GiftCard GetByGiftCardId(TransactionManager transactionManager, System.Int32 _giftCardId, int start, int pageLength)
		{
			int count = -1;
			return GetByGiftCardId(transactionManager, _giftCardId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeGiftCard index.
		/// </summary>
		/// <param name="_giftCardId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.GiftCard GetByGiftCardId(System.Int32 _giftCardId, int start, int pageLength, out int count)
		{
			return GetByGiftCardId(null, _giftCardId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeGiftCard index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_giftCardId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.GiftCard GetByGiftCardId(TransactionManager transactionManager, System.Int32 _giftCardId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;GiftCard&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;GiftCard&gt;"/></returns>
		public static TList<GiftCard> Fill(IDataReader reader, TList<GiftCard> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.GiftCard c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("GiftCard")
					.Append("|").Append((System.Int32)reader[((int)GiftCardColumn.GiftCardId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<GiftCard>(
					key.ToString(), // EntityTrackingKey
					"GiftCard",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.GiftCard();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.GiftCardId = (System.Int32)reader[((int)GiftCardColumn.GiftCardId - 1)];
					c.PortalId = (System.Int32)reader[((int)GiftCardColumn.PortalId - 1)];
					c.Name = (reader.IsDBNull(((int)GiftCardColumn.Name - 1)))?null:(System.String)reader[((int)GiftCardColumn.Name - 1)];
					c.CardNumber = (System.String)reader[((int)GiftCardColumn.CardNumber - 1)];
					c.CreatedBy = (System.Int32)reader[((int)GiftCardColumn.CreatedBy - 1)];
					c.CreateDate = (System.DateTime)reader[((int)GiftCardColumn.CreateDate - 1)];
					c.OrderItemId = (reader.IsDBNull(((int)GiftCardColumn.OrderItemId - 1)))?null:(System.Int32?)reader[((int)GiftCardColumn.OrderItemId - 1)];
					c.AccountId = (reader.IsDBNull(((int)GiftCardColumn.AccountId - 1)))?null:(System.Int32?)reader[((int)GiftCardColumn.AccountId - 1)];
					c.ExpirationDate = (reader.IsDBNull(((int)GiftCardColumn.ExpirationDate - 1)))?null:(System.DateTime?)reader[((int)GiftCardColumn.ExpirationDate - 1)];
					c.Amount = (reader.IsDBNull(((int)GiftCardColumn.Amount - 1)))?null:(System.Decimal?)reader[((int)GiftCardColumn.Amount - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.GiftCard entity)
		{
			if (!reader.Read()) return;
			
			entity.GiftCardId = (System.Int32)reader[((int)GiftCardColumn.GiftCardId - 1)];
			entity.PortalId = (System.Int32)reader[((int)GiftCardColumn.PortalId - 1)];
			entity.Name = (reader.IsDBNull(((int)GiftCardColumn.Name - 1)))?null:(System.String)reader[((int)GiftCardColumn.Name - 1)];
			entity.CardNumber = (System.String)reader[((int)GiftCardColumn.CardNumber - 1)];
			entity.CreatedBy = (System.Int32)reader[((int)GiftCardColumn.CreatedBy - 1)];
			entity.CreateDate = (System.DateTime)reader[((int)GiftCardColumn.CreateDate - 1)];
			entity.OrderItemId = (reader.IsDBNull(((int)GiftCardColumn.OrderItemId - 1)))?null:(System.Int32?)reader[((int)GiftCardColumn.OrderItemId - 1)];
			entity.AccountId = (reader.IsDBNull(((int)GiftCardColumn.AccountId - 1)))?null:(System.Int32?)reader[((int)GiftCardColumn.AccountId - 1)];
			entity.ExpirationDate = (reader.IsDBNull(((int)GiftCardColumn.ExpirationDate - 1)))?null:(System.DateTime?)reader[((int)GiftCardColumn.ExpirationDate - 1)];
			entity.Amount = (reader.IsDBNull(((int)GiftCardColumn.Amount - 1)))?null:(System.Decimal?)reader[((int)GiftCardColumn.Amount - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.GiftCard entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.GiftCardId = (System.Int32)dataRow["GiftCardId"];
			entity.PortalId = (System.Int32)dataRow["PortalId"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.CardNumber = (System.String)dataRow["CardNumber"];
			entity.CreatedBy = (System.Int32)dataRow["CreatedBy"];
			entity.CreateDate = (System.DateTime)dataRow["CreateDate"];
			entity.OrderItemId = Convert.IsDBNull(dataRow["OrderItemId"]) ? null : (System.Int32?)dataRow["OrderItemId"];
			entity.AccountId = Convert.IsDBNull(dataRow["AccountId"]) ? null : (System.Int32?)dataRow["AccountId"];
			entity.ExpirationDate = Convert.IsDBNull(dataRow["ExpirationDate"]) ? null : (System.DateTime?)dataRow["ExpirationDate"];
			entity.Amount = Convert.IsDBNull(dataRow["Amount"]) ? null : (System.Decimal?)dataRow["Amount"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.GiftCard"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.GiftCard Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.GiftCard entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AccountIdSource	
			if (CanDeepLoad(entity, "Account|AccountIdSource", deepLoadType, innerList) 
				&& entity.AccountIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.AccountId ?? (int)0);
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AccountIdSource = tmpEntity;
				else
					entity.AccountIdSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, (entity.AccountId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AccountIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.AccountIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AccountIdSource

			#region OrderItemIdSource	
			if (CanDeepLoad(entity, "OrderLineItem|OrderItemIdSource", deepLoadType, innerList) 
				&& entity.OrderItemIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.OrderItemId ?? (int)0);
				OrderLineItem tmpEntity = EntityManager.LocateEntity<OrderLineItem>(EntityLocator.ConstructKeyFromPkItems(typeof(OrderLineItem), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.OrderItemIdSource = tmpEntity;
				else
					entity.OrderItemIdSource = DataRepository.OrderLineItemProvider.GetByOrderLineItemID(transactionManager, (entity.OrderItemId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderItemIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.OrderItemIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.OrderLineItemProvider.DeepLoad(transactionManager, entity.OrderItemIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion OrderItemIdSource

			#region PortalIdSource	
			if (CanDeepLoad(entity, "Portal|PortalIdSource", deepLoadType, innerList) 
				&& entity.PortalIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.PortalId;
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIdSource = tmpEntity;
				else
					entity.PortalIdSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, entity.PortalId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByGiftCardId methods when available
			
			#region GiftCardHistoryCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<GiftCardHistory>|GiftCardHistoryCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'GiftCardHistoryCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.GiftCardHistoryCollection = DataRepository.GiftCardHistoryProvider.GetByGiftCardID(transactionManager, entity.GiftCardId);

				if (deep && entity.GiftCardHistoryCollection.Count > 0)
				{
					deepHandles.Add("GiftCardHistoryCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<GiftCardHistory>) DataRepository.GiftCardHistoryProvider.DeepLoad,
						new object[] { transactionManager, entity.GiftCardHistoryCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.GiftCard object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.GiftCard instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.GiftCard Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.GiftCard entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AccountIdSource
			if (CanDeepSave(entity, "Account|AccountIdSource", deepSaveType, innerList) 
				&& entity.AccountIdSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.AccountIdSource);
				entity.AccountId = entity.AccountIdSource.AccountID;
			}
			#endregion 
			
			#region OrderItemIdSource
			if (CanDeepSave(entity, "OrderLineItem|OrderItemIdSource", deepSaveType, innerList) 
				&& entity.OrderItemIdSource != null)
			{
				DataRepository.OrderLineItemProvider.Save(transactionManager, entity.OrderItemIdSource);
				entity.OrderItemId = entity.OrderItemIdSource.OrderLineItemID;
			}
			#endregion 
			
			#region PortalIdSource
			if (CanDeepSave(entity, "Portal|PortalIdSource", deepSaveType, innerList) 
				&& entity.PortalIdSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIdSource);
				entity.PortalId = entity.PortalIdSource.PortalID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<GiftCardHistory>
				if (CanDeepSave(entity.GiftCardHistoryCollection, "List<GiftCardHistory>|GiftCardHistoryCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(GiftCardHistory child in entity.GiftCardHistoryCollection)
					{
						if(child.GiftCardIDSource != null)
						{
							child.GiftCardID = child.GiftCardIDSource.GiftCardId;
						}
						else
						{
							child.GiftCardID = entity.GiftCardId;
						}

					}

					if (entity.GiftCardHistoryCollection.Count > 0 || entity.GiftCardHistoryCollection.DeletedItems.Count > 0)
					{
						//DataRepository.GiftCardHistoryProvider.Save(transactionManager, entity.GiftCardHistoryCollection);
						
						deepHandles.Add("GiftCardHistoryCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< GiftCardHistory >) DataRepository.GiftCardHistoryProvider.DeepSave,
							new object[] { transactionManager, entity.GiftCardHistoryCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region GiftCardChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.GiftCard</c>
	///</summary>
	public enum GiftCardChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Account</c> at AccountIdSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
		
		///<summary>
		/// Composite Property for <c>OrderLineItem</c> at OrderItemIdSource
		///</summary>
		[ChildEntityType(typeof(OrderLineItem))]
		OrderLineItem,
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIdSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		///<summary>
		/// Collection of <c>GiftCard</c> as OneToMany for GiftCardHistoryCollection
		///</summary>
		[ChildEntityType(typeof(TList<GiftCardHistory>))]
		GiftCardHistoryCollection,
	}
	
	#endregion GiftCardChildEntityTypes
	
	#region GiftCardFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;GiftCardColumn&gt;"/> class
	/// that is used exclusively with a <see cref="GiftCard"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class GiftCardFilterBuilder : SqlFilterBuilder<GiftCardColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GiftCardFilterBuilder class.
		/// </summary>
		public GiftCardFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the GiftCardFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public GiftCardFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the GiftCardFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public GiftCardFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion GiftCardFilterBuilder
	
	#region GiftCardParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;GiftCardColumn&gt;"/> class
	/// that is used exclusively with a <see cref="GiftCard"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class GiftCardParameterBuilder : ParameterizedSqlFilterBuilder<GiftCardColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GiftCardParameterBuilder class.
		/// </summary>
		public GiftCardParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the GiftCardParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public GiftCardParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the GiftCardParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public GiftCardParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion GiftCardParameterBuilder
	
	#region GiftCardSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;GiftCardColumn&gt;"/> class
	/// that is used exclusively with a <see cref="GiftCard"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class GiftCardSortBuilder : SqlSortBuilder<GiftCardColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the GiftCardSqlSortBuilder class.
		/// </summary>
		public GiftCardSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion GiftCardSortBuilder
	
} // end namespace
