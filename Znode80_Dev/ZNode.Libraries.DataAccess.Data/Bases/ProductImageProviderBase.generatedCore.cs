﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductImageProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductImageProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ProductImage, ZNode.Libraries.DataAccess.Entities.ProductImageKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductImageKey key)
		{
			return Delete(transactionManager, key.ProductImageID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_productImageID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _productImageID)
		{
			return Delete(null, _productImageID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productImageID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _productImageID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductImage_ZNodeProductImageType key.
		///		FK_ZNodeProductImage_ZNodeProductImageType Description: 
		/// </summary>
		/// <param name="_productImageTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		public TList<ProductImage> GetByProductImageTypeID(System.Int32? _productImageTypeID)
		{
			int count = -1;
			return GetByProductImageTypeID(_productImageTypeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductImage_ZNodeProductImageType key.
		///		FK_ZNodeProductImage_ZNodeProductImageType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productImageTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		/// <remarks></remarks>
		public TList<ProductImage> GetByProductImageTypeID(TransactionManager transactionManager, System.Int32? _productImageTypeID)
		{
			int count = -1;
			return GetByProductImageTypeID(transactionManager, _productImageTypeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductImage_ZNodeProductImageType key.
		///		FK_ZNodeProductImage_ZNodeProductImageType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productImageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		public TList<ProductImage> GetByProductImageTypeID(TransactionManager transactionManager, System.Int32? _productImageTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductImageTypeID(transactionManager, _productImageTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductImage_ZNodeProductImageType key.
		///		fKZNodeProductImageZNodeProductImageType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productImageTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		public TList<ProductImage> GetByProductImageTypeID(System.Int32? _productImageTypeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByProductImageTypeID(null, _productImageTypeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductImage_ZNodeProductImageType key.
		///		fKZNodeProductImageZNodeProductImageType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productImageTypeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		public TList<ProductImage> GetByProductImageTypeID(System.Int32? _productImageTypeID, int start, int pageLength,out int count)
		{
			return GetByProductImageTypeID(null, _productImageTypeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductImage_ZNodeProductImageType key.
		///		FK_ZNodeProductImage_ZNodeProductImageType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productImageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		public abstract TList<ProductImage> GetByProductImageTypeID(TransactionManager transactionManager, System.Int32? _productImageTypeID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductImage_ZNodeProductReviewState key.
		///		FK_ZNodeProductImage_ZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="_reviewStateID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		public TList<ProductImage> GetByReviewStateID(System.Int32? _reviewStateID)
		{
			int count = -1;
			return GetByReviewStateID(_reviewStateID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductImage_ZNodeProductReviewState key.
		///		FK_ZNodeProductImage_ZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reviewStateID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		/// <remarks></remarks>
		public TList<ProductImage> GetByReviewStateID(TransactionManager transactionManager, System.Int32? _reviewStateID)
		{
			int count = -1;
			return GetByReviewStateID(transactionManager, _reviewStateID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductImage_ZNodeProductReviewState key.
		///		FK_ZNodeProductImage_ZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reviewStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		public TList<ProductImage> GetByReviewStateID(TransactionManager transactionManager, System.Int32? _reviewStateID, int start, int pageLength)
		{
			int count = -1;
			return GetByReviewStateID(transactionManager, _reviewStateID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductImage_ZNodeProductReviewState key.
		///		fKZNodeProductImageZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_reviewStateID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		public TList<ProductImage> GetByReviewStateID(System.Int32? _reviewStateID, int start, int pageLength)
		{
			int count =  -1;
			return GetByReviewStateID(null, _reviewStateID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductImage_ZNodeProductReviewState key.
		///		fKZNodeProductImageZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_reviewStateID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		public TList<ProductImage> GetByReviewStateID(System.Int32? _reviewStateID, int start, int pageLength,out int count)
		{
			return GetByReviewStateID(null, _reviewStateID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductImage_ZNodeProductReviewState key.
		///		FK_ZNodeProductImage_ZNodeProductReviewState Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reviewStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		public abstract TList<ProductImage> GetByReviewStateID(TransactionManager transactionManager, System.Int32? _reviewStateID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductView_ZNodeProduct key.
		///		FK_ZNodeProductView_ZNodeProduct Description: 
		/// </summary>
		/// <param name="_productID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		public TList<ProductImage> GetByProductID(System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(_productID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductView_ZNodeProduct key.
		///		FK_ZNodeProductView_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		/// <remarks></remarks>
		public TList<ProductImage> GetByProductID(TransactionManager transactionManager, System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductView_ZNodeProduct key.
		///		FK_ZNodeProductView_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		public TList<ProductImage> GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductView_ZNodeProduct key.
		///		fKZNodeProductViewZNodeProduct Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		public TList<ProductImage> GetByProductID(System.Int32 _productID, int start, int pageLength)
		{
			int count =  -1;
			return GetByProductID(null, _productID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductView_ZNodeProduct key.
		///		fKZNodeProductViewZNodeProduct Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		public TList<ProductImage> GetByProductID(System.Int32 _productID, int start, int pageLength,out int count)
		{
			return GetByProductID(null, _productID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductView_ZNodeProduct key.
		///		FK_ZNodeProductView_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductImage objects.</returns>
		public abstract TList<ProductImage> GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ProductImage Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductImageKey key, int start, int pageLength)
		{
			return GetByProductImageID(transactionManager, key.ProductImageID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_Search index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="_activeInd"></param>
		/// <param name="_productImageTypeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductImage&gt;"/> class.</returns>
		public TList<ProductImage> GetByProductIDActiveIndProductImageTypeID(System.Int32 _productID, System.Boolean _activeInd, System.Int32? _productImageTypeID)
		{
			int count = -1;
			return GetByProductIDActiveIndProductImageTypeID(null,_productID, _activeInd, _productImageTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Search index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="_activeInd"></param>
		/// <param name="_productImageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductImage&gt;"/> class.</returns>
		public TList<ProductImage> GetByProductIDActiveIndProductImageTypeID(System.Int32 _productID, System.Boolean _activeInd, System.Int32? _productImageTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductIDActiveIndProductImageTypeID(null, _productID, _activeInd, _productImageTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Search index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="_activeInd"></param>
		/// <param name="_productImageTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductImage&gt;"/> class.</returns>
		public TList<ProductImage> GetByProductIDActiveIndProductImageTypeID(TransactionManager transactionManager, System.Int32 _productID, System.Boolean _activeInd, System.Int32? _productImageTypeID)
		{
			int count = -1;
			return GetByProductIDActiveIndProductImageTypeID(transactionManager, _productID, _activeInd, _productImageTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Search index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="_activeInd"></param>
		/// <param name="_productImageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductImage&gt;"/> class.</returns>
		public TList<ProductImage> GetByProductIDActiveIndProductImageTypeID(TransactionManager transactionManager, System.Int32 _productID, System.Boolean _activeInd, System.Int32? _productImageTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductIDActiveIndProductImageTypeID(transactionManager, _productID, _activeInd, _productImageTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Search index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="_activeInd"></param>
		/// <param name="_productImageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductImage&gt;"/> class.</returns>
		public TList<ProductImage> GetByProductIDActiveIndProductImageTypeID(System.Int32 _productID, System.Boolean _activeInd, System.Int32? _productImageTypeID, int start, int pageLength, out int count)
		{
			return GetByProductIDActiveIndProductImageTypeID(null, _productID, _activeInd, _productImageTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_Search index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="_activeInd"></param>
		/// <param name="_productImageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductImage&gt;"/> class.</returns>
		public abstract TList<ProductImage> GetByProductIDActiveIndProductImageTypeID(TransactionManager transactionManager, System.Int32 _productID, System.Boolean _activeInd, System.Int32? _productImageTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeProductView index.
		/// </summary>
		/// <param name="_productImageID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductImage GetByProductImageID(System.Int32 _productImageID)
		{
			int count = -1;
			return GetByProductImageID(null,_productImageID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductView index.
		/// </summary>
		/// <param name="_productImageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductImage GetByProductImageID(System.Int32 _productImageID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductImageID(null, _productImageID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductView index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productImageID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductImage GetByProductImageID(TransactionManager transactionManager, System.Int32 _productImageID)
		{
			int count = -1;
			return GetByProductImageID(transactionManager, _productImageID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductView index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productImageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductImage GetByProductImageID(TransactionManager transactionManager, System.Int32 _productImageID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductImageID(transactionManager, _productImageID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductView index.
		/// </summary>
		/// <param name="_productImageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductImage GetByProductImageID(System.Int32 _productImageID, int start, int pageLength, out int count)
		{
			return GetByProductImageID(null, _productImageID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductView index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productImageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImage"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductImage GetByProductImageID(TransactionManager transactionManager, System.Int32 _productImageID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ProductImage&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ProductImage&gt;"/></returns>
		public static TList<ProductImage> Fill(IDataReader reader, TList<ProductImage> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ProductImage c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ProductImage")
					.Append("|").Append((System.Int32)reader[((int)ProductImageColumn.ProductImageID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ProductImage>(
					key.ToString(), // EntityTrackingKey
					"ProductImage",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ProductImage();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ProductImageID = (System.Int32)reader[((int)ProductImageColumn.ProductImageID - 1)];
					c.ProductID = (System.Int32)reader[((int)ProductImageColumn.ProductID - 1)];
					c.Name = (reader.IsDBNull(((int)ProductImageColumn.Name - 1)))?null:(System.String)reader[((int)ProductImageColumn.Name - 1)];
					c.ImageFile = (reader.IsDBNull(((int)ProductImageColumn.ImageFile - 1)))?null:(System.String)reader[((int)ProductImageColumn.ImageFile - 1)];
					c.ImageAltTag = (reader.IsDBNull(((int)ProductImageColumn.ImageAltTag - 1)))?null:(System.String)reader[((int)ProductImageColumn.ImageAltTag - 1)];
					c.AlternateThumbnailImageFile = (reader.IsDBNull(((int)ProductImageColumn.AlternateThumbnailImageFile - 1)))?null:(System.String)reader[((int)ProductImageColumn.AlternateThumbnailImageFile - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)ProductImageColumn.ActiveInd - 1)];
					c.ShowOnCategoryPage = (System.Boolean)reader[((int)ProductImageColumn.ShowOnCategoryPage - 1)];
					c.ProductImageTypeID = (reader.IsDBNull(((int)ProductImageColumn.ProductImageTypeID - 1)))?null:(System.Int32?)reader[((int)ProductImageColumn.ProductImageTypeID - 1)];
					c.DisplayOrder = (reader.IsDBNull(((int)ProductImageColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)ProductImageColumn.DisplayOrder - 1)];
					c.ReviewStateID = (reader.IsDBNull(((int)ProductImageColumn.ReviewStateID - 1)))?null:(System.Int32?)reader[((int)ProductImageColumn.ReviewStateID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImage"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductImage"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ProductImage entity)
		{
			if (!reader.Read()) return;
			
			entity.ProductImageID = (System.Int32)reader[((int)ProductImageColumn.ProductImageID - 1)];
			entity.ProductID = (System.Int32)reader[((int)ProductImageColumn.ProductID - 1)];
			entity.Name = (reader.IsDBNull(((int)ProductImageColumn.Name - 1)))?null:(System.String)reader[((int)ProductImageColumn.Name - 1)];
			entity.ImageFile = (reader.IsDBNull(((int)ProductImageColumn.ImageFile - 1)))?null:(System.String)reader[((int)ProductImageColumn.ImageFile - 1)];
			entity.ImageAltTag = (reader.IsDBNull(((int)ProductImageColumn.ImageAltTag - 1)))?null:(System.String)reader[((int)ProductImageColumn.ImageAltTag - 1)];
			entity.AlternateThumbnailImageFile = (reader.IsDBNull(((int)ProductImageColumn.AlternateThumbnailImageFile - 1)))?null:(System.String)reader[((int)ProductImageColumn.AlternateThumbnailImageFile - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)ProductImageColumn.ActiveInd - 1)];
			entity.ShowOnCategoryPage = (System.Boolean)reader[((int)ProductImageColumn.ShowOnCategoryPage - 1)];
			entity.ProductImageTypeID = (reader.IsDBNull(((int)ProductImageColumn.ProductImageTypeID - 1)))?null:(System.Int32?)reader[((int)ProductImageColumn.ProductImageTypeID - 1)];
			entity.DisplayOrder = (reader.IsDBNull(((int)ProductImageColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)ProductImageColumn.DisplayOrder - 1)];
			entity.ReviewStateID = (reader.IsDBNull(((int)ProductImageColumn.ReviewStateID - 1)))?null:(System.Int32?)reader[((int)ProductImageColumn.ReviewStateID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImage"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductImage"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ProductImage entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProductImageID = (System.Int32)dataRow["ProductImageID"];
			entity.ProductID = (System.Int32)dataRow["ProductID"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.ImageFile = Convert.IsDBNull(dataRow["ImageFile"]) ? null : (System.String)dataRow["ImageFile"];
			entity.ImageAltTag = Convert.IsDBNull(dataRow["ImageAltTag"]) ? null : (System.String)dataRow["ImageAltTag"];
			entity.AlternateThumbnailImageFile = Convert.IsDBNull(dataRow["AlternateThumbnailImageFile"]) ? null : (System.String)dataRow["AlternateThumbnailImageFile"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.ShowOnCategoryPage = (System.Boolean)dataRow["ShowOnCategoryPage"];
			entity.ProductImageTypeID = Convert.IsDBNull(dataRow["ProductImageTypeID"]) ? null : (System.Int32?)dataRow["ProductImageTypeID"];
			entity.DisplayOrder = Convert.IsDBNull(dataRow["DisplayOrder"]) ? null : (System.Int32?)dataRow["DisplayOrder"];
			entity.ReviewStateID = Convert.IsDBNull(dataRow["ReviewStateID"]) ? null : (System.Int32?)dataRow["ReviewStateID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductImage"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductImage Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductImage entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ProductImageTypeIDSource	
			if (CanDeepLoad(entity, "ProductImageType|ProductImageTypeIDSource", deepLoadType, innerList) 
				&& entity.ProductImageTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ProductImageTypeID ?? (int)0);
				ProductImageType tmpEntity = EntityManager.LocateEntity<ProductImageType>(EntityLocator.ConstructKeyFromPkItems(typeof(ProductImageType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductImageTypeIDSource = tmpEntity;
				else
					entity.ProductImageTypeIDSource = DataRepository.ProductImageTypeProvider.GetByProductImageTypeID(transactionManager, (entity.ProductImageTypeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductImageTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductImageTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductImageTypeProvider.DeepLoad(transactionManager, entity.ProductImageTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductImageTypeIDSource

			#region ReviewStateIDSource	
			if (CanDeepLoad(entity, "ProductReviewState|ReviewStateIDSource", deepLoadType, innerList) 
				&& entity.ReviewStateIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ReviewStateID ?? (int)0);
				ProductReviewState tmpEntity = EntityManager.LocateEntity<ProductReviewState>(EntityLocator.ConstructKeyFromPkItems(typeof(ProductReviewState), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ReviewStateIDSource = tmpEntity;
				else
					entity.ReviewStateIDSource = DataRepository.ProductReviewStateProvider.GetByReviewStateID(transactionManager, (entity.ReviewStateID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ReviewStateIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ReviewStateIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductReviewStateProvider.DeepLoad(transactionManager, entity.ReviewStateIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ReviewStateIDSource

			#region ProductIDSource	
			if (CanDeepLoad(entity, "Product|ProductIDSource", deepLoadType, innerList) 
				&& entity.ProductIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProductID;
				Product tmpEntity = EntityManager.LocateEntity<Product>(EntityLocator.ConstructKeyFromPkItems(typeof(Product), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductIDSource = tmpEntity;
				else
					entity.ProductIDSource = DataRepository.ProductProvider.GetByProductID(transactionManager, entity.ProductID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductProvider.DeepLoad(transactionManager, entity.ProductIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ProductImage object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ProductImage instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductImage Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductImage entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ProductImageTypeIDSource
			if (CanDeepSave(entity, "ProductImageType|ProductImageTypeIDSource", deepSaveType, innerList) 
				&& entity.ProductImageTypeIDSource != null)
			{
				DataRepository.ProductImageTypeProvider.Save(transactionManager, entity.ProductImageTypeIDSource);
				entity.ProductImageTypeID = entity.ProductImageTypeIDSource.ProductImageTypeID;
			}
			#endregion 
			
			#region ReviewStateIDSource
			if (CanDeepSave(entity, "ProductReviewState|ReviewStateIDSource", deepSaveType, innerList) 
				&& entity.ReviewStateIDSource != null)
			{
				DataRepository.ProductReviewStateProvider.Save(transactionManager, entity.ReviewStateIDSource);
				entity.ReviewStateID = entity.ReviewStateIDSource.ReviewStateID;
			}
			#endregion 
			
			#region ProductIDSource
			if (CanDeepSave(entity, "Product|ProductIDSource", deepSaveType, innerList) 
				&& entity.ProductIDSource != null)
			{
				DataRepository.ProductProvider.Save(transactionManager, entity.ProductIDSource);
				entity.ProductID = entity.ProductIDSource.ProductID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductImageChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ProductImage</c>
	///</summary>
	public enum ProductImageChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>ProductImageType</c> at ProductImageTypeIDSource
		///</summary>
		[ChildEntityType(typeof(ProductImageType))]
		ProductImageType,
		
		///<summary>
		/// Composite Property for <c>ProductReviewState</c> at ReviewStateIDSource
		///</summary>
		[ChildEntityType(typeof(ProductReviewState))]
		ProductReviewState,
		
		///<summary>
		/// Composite Property for <c>Product</c> at ProductIDSource
		///</summary>
		[ChildEntityType(typeof(Product))]
		Product,
	}
	
	#endregion ProductImageChildEntityTypes
	
	#region ProductImageFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductImageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductImage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductImageFilterBuilder : SqlFilterBuilder<ProductImageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductImageFilterBuilder class.
		/// </summary>
		public ProductImageFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductImageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductImageFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductImageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductImageFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductImageFilterBuilder
	
	#region ProductImageParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductImageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductImage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductImageParameterBuilder : ParameterizedSqlFilterBuilder<ProductImageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductImageParameterBuilder class.
		/// </summary>
		public ProductImageParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductImageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductImageParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductImageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductImageParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductImageParameterBuilder
	
	#region ProductImageSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductImageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductImage"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductImageSortBuilder : SqlSortBuilder<ProductImageColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductImageSqlSortBuilder class.
		/// </summary>
		public ProductImageSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductImageSortBuilder
	
} // end namespace
