﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PortalWorkflowProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PortalWorkflowProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.PortalWorkflow, ZNode.Libraries.DataAccess.Entities.PortalWorkflowKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalWorkflowKey key)
		{
			return Delete(transactionManager, key.PortalWorkflowID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_portalWorkflowID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _portalWorkflowID)
		{
			return Delete(null, _portalWorkflowID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalWorkflowID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _portalWorkflowID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.PortalWorkflow Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalWorkflowKey key, int start, int pageLength)
		{
			return GetByPortalWorkflowID(transactionManager, key.PortalWorkflowID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodePortalWorkflow index.
		/// </summary>
		/// <param name="_portalWorkflowID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalWorkflow"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalWorkflow GetByPortalWorkflowID(System.Int32 _portalWorkflowID)
		{
			int count = -1;
			return GetByPortalWorkflowID(null,_portalWorkflowID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalWorkflow index.
		/// </summary>
		/// <param name="_portalWorkflowID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalWorkflow"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalWorkflow GetByPortalWorkflowID(System.Int32 _portalWorkflowID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalWorkflowID(null, _portalWorkflowID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalWorkflow index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalWorkflowID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalWorkflow"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalWorkflow GetByPortalWorkflowID(TransactionManager transactionManager, System.Int32 _portalWorkflowID)
		{
			int count = -1;
			return GetByPortalWorkflowID(transactionManager, _portalWorkflowID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalWorkflow index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalWorkflowID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalWorkflow"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalWorkflow GetByPortalWorkflowID(TransactionManager transactionManager, System.Int32 _portalWorkflowID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalWorkflowID(transactionManager, _portalWorkflowID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalWorkflow index.
		/// </summary>
		/// <param name="_portalWorkflowID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalWorkflow"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PortalWorkflow GetByPortalWorkflowID(System.Int32 _portalWorkflowID, int start, int pageLength, out int count)
		{
			return GetByPortalWorkflowID(null, _portalWorkflowID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePortalWorkflow index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalWorkflowID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PortalWorkflow"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.PortalWorkflow GetByPortalWorkflowID(TransactionManager transactionManager, System.Int32 _portalWorkflowID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;PortalWorkflow&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;PortalWorkflow&gt;"/></returns>
		public static TList<PortalWorkflow> Fill(IDataReader reader, TList<PortalWorkflow> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.PortalWorkflow c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("PortalWorkflow")
					.Append("|").Append((System.Int32)reader[((int)PortalWorkflowColumn.PortalWorkflowID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<PortalWorkflow>(
					key.ToString(), // EntityTrackingKey
					"PortalWorkflow",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.PortalWorkflow();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.PortalWorkflowID = (System.Int32)reader[((int)PortalWorkflowColumn.PortalWorkflowID - 1)];
					c.WorkflowID = (System.Int32)reader[((int)PortalWorkflowColumn.WorkflowID - 1)];
					c.PortalID = (System.Int32)reader[((int)PortalWorkflowColumn.PortalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PortalWorkflow"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PortalWorkflow"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.PortalWorkflow entity)
		{
			if (!reader.Read()) return;
			
			entity.PortalWorkflowID = (System.Int32)reader[((int)PortalWorkflowColumn.PortalWorkflowID - 1)];
			entity.WorkflowID = (System.Int32)reader[((int)PortalWorkflowColumn.WorkflowID - 1)];
			entity.PortalID = (System.Int32)reader[((int)PortalWorkflowColumn.PortalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PortalWorkflow"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PortalWorkflow"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.PortalWorkflow entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PortalWorkflowID = (System.Int32)dataRow["PortalWorkflowID"];
			entity.WorkflowID = (System.Int32)dataRow["WorkflowID"];
			entity.PortalID = (System.Int32)dataRow["PortalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PortalWorkflow"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PortalWorkflow Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalWorkflow entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.PortalWorkflow object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.PortalWorkflow instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PortalWorkflow Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PortalWorkflow entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PortalWorkflowChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.PortalWorkflow</c>
	///</summary>
	public enum PortalWorkflowChildEntityTypes
	{
	}
	
	#endregion PortalWorkflowChildEntityTypes
	
	#region PortalWorkflowFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PortalWorkflowColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalWorkflow"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalWorkflowFilterBuilder : SqlFilterBuilder<PortalWorkflowColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalWorkflowFilterBuilder class.
		/// </summary>
		public PortalWorkflowFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalWorkflowFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalWorkflowFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalWorkflowFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalWorkflowFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalWorkflowFilterBuilder
	
	#region PortalWorkflowParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PortalWorkflowColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalWorkflow"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PortalWorkflowParameterBuilder : ParameterizedSqlFilterBuilder<PortalWorkflowColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalWorkflowParameterBuilder class.
		/// </summary>
		public PortalWorkflowParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PortalWorkflowParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PortalWorkflowParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PortalWorkflowParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PortalWorkflowParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PortalWorkflowParameterBuilder
	
	#region PortalWorkflowSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PortalWorkflowColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PortalWorkflow"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PortalWorkflowSortBuilder : SqlSortBuilder<PortalWorkflowColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PortalWorkflowSqlSortBuilder class.
		/// </summary>
		public PortalWorkflowSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PortalWorkflowSortBuilder
	
} // end namespace
