﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="MessageTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class MessageTypeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.MessageType, ZNode.Libraries.DataAccess.Entities.MessageTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.MessageTypeKey key)
		{
			return Delete(transactionManager, key.MessageTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_messageTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _messageTypeID)
		{
			return Delete(null, _messageTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_messageTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _messageTypeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.MessageType Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.MessageTypeKey key, int start, int pageLength)
		{
			return GetByMessageTypeID(transactionManager, key.MessageTypeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeMessageType index.
		/// </summary>
		/// <param name="_messageTypeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MessageType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.MessageType GetByMessageTypeID(System.Int32 _messageTypeID)
		{
			int count = -1;
			return GetByMessageTypeID(null,_messageTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeMessageType index.
		/// </summary>
		/// <param name="_messageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MessageType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.MessageType GetByMessageTypeID(System.Int32 _messageTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByMessageTypeID(null, _messageTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeMessageType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_messageTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MessageType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.MessageType GetByMessageTypeID(TransactionManager transactionManager, System.Int32 _messageTypeID)
		{
			int count = -1;
			return GetByMessageTypeID(transactionManager, _messageTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeMessageType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_messageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MessageType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.MessageType GetByMessageTypeID(TransactionManager transactionManager, System.Int32 _messageTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByMessageTypeID(transactionManager, _messageTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeMessageType index.
		/// </summary>
		/// <param name="_messageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MessageType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.MessageType GetByMessageTypeID(System.Int32 _messageTypeID, int start, int pageLength, out int count)
		{
			return GetByMessageTypeID(null, _messageTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeMessageType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_messageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MessageType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.MessageType GetByMessageTypeID(TransactionManager transactionManager, System.Int32 _messageTypeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;MessageType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;MessageType&gt;"/></returns>
		public static TList<MessageType> Fill(IDataReader reader, TList<MessageType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.MessageType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("MessageType")
					.Append("|").Append((System.Int32)reader[((int)MessageTypeColumn.MessageTypeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<MessageType>(
					key.ToString(), // EntityTrackingKey
					"MessageType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.MessageType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.MessageTypeID = (System.Int32)reader[((int)MessageTypeColumn.MessageTypeID - 1)];
					c.Name = (System.String)reader[((int)MessageTypeColumn.Name - 1)];
					c.DisplayOrder = (System.Int32)reader[((int)MessageTypeColumn.DisplayOrder - 1)];
					c.IsActive = (System.Boolean)reader[((int)MessageTypeColumn.IsActive - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.MessageType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.MessageType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.MessageType entity)
		{
			if (!reader.Read()) return;
			
			entity.MessageTypeID = (System.Int32)reader[((int)MessageTypeColumn.MessageTypeID - 1)];
			entity.Name = (System.String)reader[((int)MessageTypeColumn.Name - 1)];
			entity.DisplayOrder = (System.Int32)reader[((int)MessageTypeColumn.DisplayOrder - 1)];
			entity.IsActive = (System.Boolean)reader[((int)MessageTypeColumn.IsActive - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.MessageType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.MessageType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.MessageType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.MessageTypeID = (System.Int32)dataRow["MessageTypeID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.DisplayOrder = (System.Int32)dataRow["DisplayOrder"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.MessageType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.MessageType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.MessageType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByMessageTypeID methods when available
			
			#region MessageConfigCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<MessageConfig>|MessageConfigCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MessageConfigCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.MessageConfigCollection = DataRepository.MessageConfigProvider.GetByMessageTypeID(transactionManager, entity.MessageTypeID);

				if (deep && entity.MessageConfigCollection.Count > 0)
				{
					deepHandles.Add("MessageConfigCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<MessageConfig>) DataRepository.MessageConfigProvider.DeepLoad,
						new object[] { transactionManager, entity.MessageConfigCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.MessageType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.MessageType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.MessageType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.MessageType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<MessageConfig>
				if (CanDeepSave(entity.MessageConfigCollection, "List<MessageConfig>|MessageConfigCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(MessageConfig child in entity.MessageConfigCollection)
					{
						if(child.MessageTypeIDSource != null)
						{
							child.MessageTypeID = child.MessageTypeIDSource.MessageTypeID;
						}
						else
						{
							child.MessageTypeID = entity.MessageTypeID;
						}

					}

					if (entity.MessageConfigCollection.Count > 0 || entity.MessageConfigCollection.DeletedItems.Count > 0)
					{
						//DataRepository.MessageConfigProvider.Save(transactionManager, entity.MessageConfigCollection);
						
						deepHandles.Add("MessageConfigCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< MessageConfig >) DataRepository.MessageConfigProvider.DeepSave,
							new object[] { transactionManager, entity.MessageConfigCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region MessageTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.MessageType</c>
	///</summary>
	public enum MessageTypeChildEntityTypes
	{
		///<summary>
		/// Collection of <c>MessageType</c> as OneToMany for MessageConfigCollection
		///</summary>
		[ChildEntityType(typeof(TList<MessageConfig>))]
		MessageConfigCollection,
	}
	
	#endregion MessageTypeChildEntityTypes
	
	#region MessageTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;MessageTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MessageType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MessageTypeFilterBuilder : SqlFilterBuilder<MessageTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MessageTypeFilterBuilder class.
		/// </summary>
		public MessageTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MessageTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MessageTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MessageTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MessageTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MessageTypeFilterBuilder
	
	#region MessageTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;MessageTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MessageType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MessageTypeParameterBuilder : ParameterizedSqlFilterBuilder<MessageTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MessageTypeParameterBuilder class.
		/// </summary>
		public MessageTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MessageTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MessageTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MessageTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MessageTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MessageTypeParameterBuilder
	
	#region MessageTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;MessageTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MessageType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class MessageTypeSortBuilder : SqlSortBuilder<MessageTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MessageTypeSqlSortBuilder class.
		/// </summary>
		public MessageTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion MessageTypeSortBuilder
	
} // end namespace
