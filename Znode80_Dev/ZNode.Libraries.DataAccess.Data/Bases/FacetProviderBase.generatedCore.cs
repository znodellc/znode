﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="FacetProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class FacetProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Facet, ZNode.Libraries.DataAccess.Entities.FacetKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.FacetKey key)
		{
			return Delete(transactionManager, key.FacetID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_facetID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _facetID)
		{
			return Delete(null, _facetID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _facetID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Facet Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.FacetKey key, int start, int pageLength)
		{
			return GetByFacetID(transactionManager, key.FacetID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZnodeFacet_FacetGroupId index.
		/// </summary>
		/// <param name="_facetGroupID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Facet&gt;"/> class.</returns>
		public TList<Facet> GetByFacetGroupID(System.Int32? _facetGroupID)
		{
			int count = -1;
			return GetByFacetGroupID(null,_facetGroupID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeFacet_FacetGroupId index.
		/// </summary>
		/// <param name="_facetGroupID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Facet&gt;"/> class.</returns>
		public TList<Facet> GetByFacetGroupID(System.Int32? _facetGroupID, int start, int pageLength)
		{
			int count = -1;
			return GetByFacetGroupID(null, _facetGroupID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeFacet_FacetGroupId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Facet&gt;"/> class.</returns>
		public TList<Facet> GetByFacetGroupID(TransactionManager transactionManager, System.Int32? _facetGroupID)
		{
			int count = -1;
			return GetByFacetGroupID(transactionManager, _facetGroupID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeFacet_FacetGroupId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Facet&gt;"/> class.</returns>
		public TList<Facet> GetByFacetGroupID(TransactionManager transactionManager, System.Int32? _facetGroupID, int start, int pageLength)
		{
			int count = -1;
			return GetByFacetGroupID(transactionManager, _facetGroupID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeFacet_FacetGroupId index.
		/// </summary>
		/// <param name="_facetGroupID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Facet&gt;"/> class.</returns>
		public TList<Facet> GetByFacetGroupID(System.Int32? _facetGroupID, int start, int pageLength, out int count)
		{
			return GetByFacetGroupID(null, _facetGroupID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeFacet_FacetGroupId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetGroupID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Facet&gt;"/> class.</returns>
		public abstract TList<Facet> GetByFacetGroupID(TransactionManager transactionManager, System.Int32? _facetGroupID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK__ZNodeFacet__657CFA4C683A954D index.
		/// </summary>
		/// <param name="_facetID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Facet"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Facet GetByFacetID(System.Int32 _facetID)
		{
			int count = -1;
			return GetByFacetID(null,_facetID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__657CFA4C683A954D index.
		/// </summary>
		/// <param name="_facetID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Facet"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Facet GetByFacetID(System.Int32 _facetID, int start, int pageLength)
		{
			int count = -1;
			return GetByFacetID(null, _facetID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__657CFA4C683A954D index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Facet"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Facet GetByFacetID(TransactionManager transactionManager, System.Int32 _facetID)
		{
			int count = -1;
			return GetByFacetID(transactionManager, _facetID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__657CFA4C683A954D index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Facet"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Facet GetByFacetID(TransactionManager transactionManager, System.Int32 _facetID, int start, int pageLength)
		{
			int count = -1;
			return GetByFacetID(transactionManager, _facetID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__657CFA4C683A954D index.
		/// </summary>
		/// <param name="_facetID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Facet"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Facet GetByFacetID(System.Int32 _facetID, int start, int pageLength, out int count)
		{
			return GetByFacetID(null, _facetID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__ZNodeFacet__657CFA4C683A954D index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_facetID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Facet"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Facet GetByFacetID(TransactionManager transactionManager, System.Int32 _facetID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Facet&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Facet&gt;"/></returns>
		public static TList<Facet> Fill(IDataReader reader, TList<Facet> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Facet c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Facet")
					.Append("|").Append((System.Int32)reader[((int)FacetColumn.FacetID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Facet>(
					key.ToString(), // EntityTrackingKey
					"Facet",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Facet();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.FacetID = (System.Int32)reader[((int)FacetColumn.FacetID - 1)];
					c.FacetGroupID = (reader.IsDBNull(((int)FacetColumn.FacetGroupID - 1)))?null:(System.Int32?)reader[((int)FacetColumn.FacetGroupID - 1)];
					c.FacetName = (reader.IsDBNull(((int)FacetColumn.FacetName - 1)))?null:(System.String)reader[((int)FacetColumn.FacetName - 1)];
					c.FacetDisplayOrder = (reader.IsDBNull(((int)FacetColumn.FacetDisplayOrder - 1)))?null:(System.Int32?)reader[((int)FacetColumn.FacetDisplayOrder - 1)];
					c.IconPath = (reader.IsDBNull(((int)FacetColumn.IconPath - 1)))?null:(System.String)reader[((int)FacetColumn.IconPath - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Facet"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Facet"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Facet entity)
		{
			if (!reader.Read()) return;
			
			entity.FacetID = (System.Int32)reader[((int)FacetColumn.FacetID - 1)];
			entity.FacetGroupID = (reader.IsDBNull(((int)FacetColumn.FacetGroupID - 1)))?null:(System.Int32?)reader[((int)FacetColumn.FacetGroupID - 1)];
			entity.FacetName = (reader.IsDBNull(((int)FacetColumn.FacetName - 1)))?null:(System.String)reader[((int)FacetColumn.FacetName - 1)];
			entity.FacetDisplayOrder = (reader.IsDBNull(((int)FacetColumn.FacetDisplayOrder - 1)))?null:(System.Int32?)reader[((int)FacetColumn.FacetDisplayOrder - 1)];
			entity.IconPath = (reader.IsDBNull(((int)FacetColumn.IconPath - 1)))?null:(System.String)reader[((int)FacetColumn.IconPath - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Facet"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Facet"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Facet entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.FacetID = (System.Int32)dataRow["FacetID"];
			entity.FacetGroupID = Convert.IsDBNull(dataRow["FacetGroupID"]) ? null : (System.Int32?)dataRow["FacetGroupID"];
			entity.FacetName = Convert.IsDBNull(dataRow["FacetName"]) ? null : (System.String)dataRow["FacetName"];
			entity.FacetDisplayOrder = Convert.IsDBNull(dataRow["FacetDisplayOrder"]) ? null : (System.Int32?)dataRow["FacetDisplayOrder"];
			entity.IconPath = Convert.IsDBNull(dataRow["IconPath"]) ? null : (System.String)dataRow["IconPath"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Facet"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Facet Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Facet entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region FacetGroupIDSource	
			if (CanDeepLoad(entity, "FacetGroup|FacetGroupIDSource", deepLoadType, innerList) 
				&& entity.FacetGroupIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.FacetGroupID ?? (int)0);
				FacetGroup tmpEntity = EntityManager.LocateEntity<FacetGroup>(EntityLocator.ConstructKeyFromPkItems(typeof(FacetGroup), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.FacetGroupIDSource = tmpEntity;
				else
					entity.FacetGroupIDSource = DataRepository.FacetGroupProvider.GetByFacetGroupID(transactionManager, (entity.FacetGroupID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FacetGroupIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.FacetGroupIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.FacetGroupProvider.DeepLoad(transactionManager, entity.FacetGroupIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion FacetGroupIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByFacetID methods when available
			
			#region FacetProductSKUCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FacetProductSKU>|FacetProductSKUCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FacetProductSKUCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FacetProductSKUCollection = DataRepository.FacetProductSKUProvider.GetByFacetID(transactionManager, entity.FacetID);

				if (deep && entity.FacetProductSKUCollection.Count > 0)
				{
					deepHandles.Add("FacetProductSKUCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FacetProductSKU>) DataRepository.FacetProductSKUProvider.DeepLoad,
						new object[] { transactionManager, entity.FacetProductSKUCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Facet object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Facet instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Facet Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Facet entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region FacetGroupIDSource
			if (CanDeepSave(entity, "FacetGroup|FacetGroupIDSource", deepSaveType, innerList) 
				&& entity.FacetGroupIDSource != null)
			{
				DataRepository.FacetGroupProvider.Save(transactionManager, entity.FacetGroupIDSource);
				entity.FacetGroupID = entity.FacetGroupIDSource.FacetGroupID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<FacetProductSKU>
				if (CanDeepSave(entity.FacetProductSKUCollection, "List<FacetProductSKU>|FacetProductSKUCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FacetProductSKU child in entity.FacetProductSKUCollection)
					{
						if(child.FacetIDSource != null)
						{
							child.FacetID = child.FacetIDSource.FacetID;
						}
						else
						{
							child.FacetID = entity.FacetID;
						}

					}

					if (entity.FacetProductSKUCollection.Count > 0 || entity.FacetProductSKUCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FacetProductSKUProvider.Save(transactionManager, entity.FacetProductSKUCollection);
						
						deepHandles.Add("FacetProductSKUCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FacetProductSKU >) DataRepository.FacetProductSKUProvider.DeepSave,
							new object[] { transactionManager, entity.FacetProductSKUCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region FacetChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Facet</c>
	///</summary>
	public enum FacetChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>FacetGroup</c> at FacetGroupIDSource
		///</summary>
		[ChildEntityType(typeof(FacetGroup))]
		FacetGroup,
		///<summary>
		/// Collection of <c>Facet</c> as OneToMany for FacetProductSKUCollection
		///</summary>
		[ChildEntityType(typeof(TList<FacetProductSKU>))]
		FacetProductSKUCollection,
	}
	
	#endregion FacetChildEntityTypes
	
	#region FacetFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;FacetColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Facet"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetFilterBuilder : SqlFilterBuilder<FacetColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetFilterBuilder class.
		/// </summary>
		public FacetFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetFilterBuilder
	
	#region FacetParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;FacetColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Facet"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class FacetParameterBuilder : ParameterizedSqlFilterBuilder<FacetColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetParameterBuilder class.
		/// </summary>
		public FacetParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the FacetParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public FacetParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the FacetParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public FacetParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion FacetParameterBuilder
	
	#region FacetSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;FacetColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Facet"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class FacetSortBuilder : SqlSortBuilder<FacetColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the FacetSqlSortBuilder class.
		/// </summary>
		public FacetSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion FacetSortBuilder
	
} // end namespace
