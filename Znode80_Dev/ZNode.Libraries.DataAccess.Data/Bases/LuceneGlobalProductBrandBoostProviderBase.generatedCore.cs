﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="LuceneGlobalProductBrandBoostProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class LuceneGlobalProductBrandBoostProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoostKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoostKey key)
		{
			return Delete(transactionManager, key.LuceneGlobalProductBrandBoostID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_luceneGlobalProductBrandBoostID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _luceneGlobalProductBrandBoostID)
		{
			return Delete(null, _luceneGlobalProductBrandBoostID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneGlobalProductBrandBoostID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _luceneGlobalProductBrandBoostID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoostKey key, int start, int pageLength)
		{
			return GetByLuceneGlobalProductBrandBoostID(transactionManager, key.LuceneGlobalProductBrandBoostID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeLuceneGlobalProductBrandBoost index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost GetByProductID(System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(null,_productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeLuceneGlobalProductBrandBoost index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost GetByProductID(System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(null, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeLuceneGlobalProductBrandBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost GetByProductID(TransactionManager transactionManager, System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeLuceneGlobalProductBrandBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeLuceneGlobalProductBrandBoost index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost GetByProductID(System.Int32 _productID, int start, int pageLength, out int count)
		{
			return GetByProductID(null, _productID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeLuceneGlobalProductBrandBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeLuceneGlobalProductBrandBoost index.
		/// </summary>
		/// <param name="_luceneGlobalProductBrandBoostID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost GetByLuceneGlobalProductBrandBoostID(System.Int32 _luceneGlobalProductBrandBoostID)
		{
			int count = -1;
			return GetByLuceneGlobalProductBrandBoostID(null,_luceneGlobalProductBrandBoostID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneGlobalProductBrandBoost index.
		/// </summary>
		/// <param name="_luceneGlobalProductBrandBoostID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost GetByLuceneGlobalProductBrandBoostID(System.Int32 _luceneGlobalProductBrandBoostID, int start, int pageLength)
		{
			int count = -1;
			return GetByLuceneGlobalProductBrandBoostID(null, _luceneGlobalProductBrandBoostID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneGlobalProductBrandBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneGlobalProductBrandBoostID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost GetByLuceneGlobalProductBrandBoostID(TransactionManager transactionManager, System.Int32 _luceneGlobalProductBrandBoostID)
		{
			int count = -1;
			return GetByLuceneGlobalProductBrandBoostID(transactionManager, _luceneGlobalProductBrandBoostID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneGlobalProductBrandBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneGlobalProductBrandBoostID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost GetByLuceneGlobalProductBrandBoostID(TransactionManager transactionManager, System.Int32 _luceneGlobalProductBrandBoostID, int start, int pageLength)
		{
			int count = -1;
			return GetByLuceneGlobalProductBrandBoostID(transactionManager, _luceneGlobalProductBrandBoostID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneGlobalProductBrandBoost index.
		/// </summary>
		/// <param name="_luceneGlobalProductBrandBoostID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost GetByLuceneGlobalProductBrandBoostID(System.Int32 _luceneGlobalProductBrandBoostID, int start, int pageLength, out int count)
		{
			return GetByLuceneGlobalProductBrandBoostID(null, _luceneGlobalProductBrandBoostID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeLuceneGlobalProductBrandBoost index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_luceneGlobalProductBrandBoostID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost GetByLuceneGlobalProductBrandBoostID(TransactionManager transactionManager, System.Int32 _luceneGlobalProductBrandBoostID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;LuceneGlobalProductBrandBoost&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;LuceneGlobalProductBrandBoost&gt;"/></returns>
		public static TList<LuceneGlobalProductBrandBoost> Fill(IDataReader reader, TList<LuceneGlobalProductBrandBoost> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("LuceneGlobalProductBrandBoost")
					.Append("|").Append((System.Int32)reader[((int)LuceneGlobalProductBrandBoostColumn.LuceneGlobalProductBrandBoostID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<LuceneGlobalProductBrandBoost>(
					key.ToString(), // EntityTrackingKey
					"LuceneGlobalProductBrandBoost",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.LuceneGlobalProductBrandBoostID = (System.Int32)reader[((int)LuceneGlobalProductBrandBoostColumn.LuceneGlobalProductBrandBoostID - 1)];
					c.ProductID = (System.Int32)reader[((int)LuceneGlobalProductBrandBoostColumn.ProductID - 1)];
					c.Boost = (System.Double)reader[((int)LuceneGlobalProductBrandBoostColumn.Boost - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost entity)
		{
			if (!reader.Read()) return;
			
			entity.LuceneGlobalProductBrandBoostID = (System.Int32)reader[((int)LuceneGlobalProductBrandBoostColumn.LuceneGlobalProductBrandBoostID - 1)];
			entity.ProductID = (System.Int32)reader[((int)LuceneGlobalProductBrandBoostColumn.ProductID - 1)];
			entity.Boost = (System.Double)reader[((int)LuceneGlobalProductBrandBoostColumn.Boost - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.LuceneGlobalProductBrandBoostID = (System.Int32)dataRow["LuceneGlobalProductBrandBoostID"];
			entity.ProductID = (System.Int32)dataRow["ProductID"];
			entity.Boost = (System.Double)dataRow["Boost"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ProductIDSource	
			if (CanDeepLoad(entity, "Product|ProductIDSource", deepLoadType, innerList) 
				&& entity.ProductIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProductID;
				Product tmpEntity = EntityManager.LocateEntity<Product>(EntityLocator.ConstructKeyFromPkItems(typeof(Product), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductIDSource = tmpEntity;
				else
					entity.ProductIDSource = DataRepository.ProductProvider.GetByProductID(transactionManager, entity.ProductID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductProvider.DeepLoad(transactionManager, entity.ProductIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ProductIDSource
			if (CanDeepSave(entity, "Product|ProductIDSource", deepSaveType, innerList) 
				&& entity.ProductIDSource != null)
			{
				DataRepository.ProductProvider.Save(transactionManager, entity.ProductIDSource);
				entity.ProductID = entity.ProductIDSource.ProductID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region LuceneGlobalProductBrandBoostChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.LuceneGlobalProductBrandBoost</c>
	///</summary>
	public enum LuceneGlobalProductBrandBoostChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Product</c> at ProductIDSource
		///</summary>
		[ChildEntityType(typeof(Product))]
		Product,
	}
	
	#endregion LuceneGlobalProductBrandBoostChildEntityTypes
	
	#region LuceneGlobalProductBrandBoostFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;LuceneGlobalProductBrandBoostColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductBrandBoost"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneGlobalProductBrandBoostFilterBuilder : SqlFilterBuilder<LuceneGlobalProductBrandBoostColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBrandBoostFilterBuilder class.
		/// </summary>
		public LuceneGlobalProductBrandBoostFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBrandBoostFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneGlobalProductBrandBoostFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBrandBoostFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneGlobalProductBrandBoostFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneGlobalProductBrandBoostFilterBuilder
	
	#region LuceneGlobalProductBrandBoostParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;LuceneGlobalProductBrandBoostColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductBrandBoost"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class LuceneGlobalProductBrandBoostParameterBuilder : ParameterizedSqlFilterBuilder<LuceneGlobalProductBrandBoostColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBrandBoostParameterBuilder class.
		/// </summary>
		public LuceneGlobalProductBrandBoostParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBrandBoostParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public LuceneGlobalProductBrandBoostParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBrandBoostParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public LuceneGlobalProductBrandBoostParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion LuceneGlobalProductBrandBoostParameterBuilder
	
	#region LuceneGlobalProductBrandBoostSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;LuceneGlobalProductBrandBoostColumn&gt;"/> class
	/// that is used exclusively with a <see cref="LuceneGlobalProductBrandBoost"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class LuceneGlobalProductBrandBoostSortBuilder : SqlSortBuilder<LuceneGlobalProductBrandBoostColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the LuceneGlobalProductBrandBoostSqlSortBuilder class.
		/// </summary>
		public LuceneGlobalProductBrandBoostSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion LuceneGlobalProductBrandBoostSortBuilder
	
} // end namespace
