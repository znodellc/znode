﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="VwZnodeCategoriesCatalogsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class VwZnodeCategoriesCatalogsProviderBaseCore : EntityViewProviderBase<VwZnodeCategoriesCatalogs>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;VwZnodeCategoriesCatalogs&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;VwZnodeCategoriesCatalogs&gt;"/></returns>
		protected static VList&lt;VwZnodeCategoriesCatalogs&gt; Fill(DataSet dataSet, VList<VwZnodeCategoriesCatalogs> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<VwZnodeCategoriesCatalogs>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;VwZnodeCategoriesCatalogs&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<VwZnodeCategoriesCatalogs>"/></returns>
		protected static VList&lt;VwZnodeCategoriesCatalogs&gt; Fill(DataTable dataTable, VList<VwZnodeCategoriesCatalogs> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					VwZnodeCategoriesCatalogs c = new VwZnodeCategoriesCatalogs();
					c.CategoryID = (Convert.IsDBNull(row["CategoryID"]))?(int)0:(System.Int32)row["CategoryID"];
					c.CatalogID = (Convert.IsDBNull(row["CatalogID"]))?(int)0:(System.Int32?)row["CatalogID"];
					c.DisplayOrder = (Convert.IsDBNull(row["DisplayOrder"]))?(int)0:(System.Int32?)row["DisplayOrder"];
					c.ExternalID = (Convert.IsDBNull(row["ExternalID"]))?string.Empty:(System.String)row["ExternalID"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.PortalID = (Convert.IsDBNull(row["PortalID"]))?(int)0:(System.Int32?)row["PortalID"];
					c.Title = (Convert.IsDBNull(row["Title"]))?string.Empty:(System.String)row["Title"];
					c.VisibleInd = (Convert.IsDBNull(row["VisibleInd"]))?false:(System.Boolean)row["VisibleInd"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;VwZnodeCategoriesCatalogs&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;VwZnodeCategoriesCatalogs&gt;"/></returns>
		protected VList<VwZnodeCategoriesCatalogs> Fill(IDataReader reader, VList<VwZnodeCategoriesCatalogs> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					VwZnodeCategoriesCatalogs entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<VwZnodeCategoriesCatalogs>("VwZnodeCategoriesCatalogs",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new VwZnodeCategoriesCatalogs();
					}
					
					entity.SuppressEntityEvents = true;

					entity.CategoryID = (System.Int32)reader[((int)VwZnodeCategoriesCatalogsColumn.CategoryID)];
					//entity.CategoryID = (Convert.IsDBNull(reader["CategoryID"]))?(int)0:(System.Int32)reader["CategoryID"];
					entity.CatalogID = (reader.IsDBNull(((int)VwZnodeCategoriesCatalogsColumn.CatalogID)))?null:(System.Int32?)reader[((int)VwZnodeCategoriesCatalogsColumn.CatalogID)];
					//entity.CatalogID = (Convert.IsDBNull(reader["CatalogID"]))?(int)0:(System.Int32?)reader["CatalogID"];
					entity.DisplayOrder = (reader.IsDBNull(((int)VwZnodeCategoriesCatalogsColumn.DisplayOrder)))?null:(System.Int32?)reader[((int)VwZnodeCategoriesCatalogsColumn.DisplayOrder)];
					//entity.DisplayOrder = (Convert.IsDBNull(reader["DisplayOrder"]))?(int)0:(System.Int32?)reader["DisplayOrder"];
					entity.ExternalID = (reader.IsDBNull(((int)VwZnodeCategoriesCatalogsColumn.ExternalID)))?null:(System.String)reader[((int)VwZnodeCategoriesCatalogsColumn.ExternalID)];
					//entity.ExternalID = (Convert.IsDBNull(reader["ExternalID"]))?string.Empty:(System.String)reader["ExternalID"];
					entity.Name = (System.String)reader[((int)VwZnodeCategoriesCatalogsColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.PortalID = (reader.IsDBNull(((int)VwZnodeCategoriesCatalogsColumn.PortalID)))?null:(System.Int32?)reader[((int)VwZnodeCategoriesCatalogsColumn.PortalID)];
					//entity.PortalID = (Convert.IsDBNull(reader["PortalID"]))?(int)0:(System.Int32?)reader["PortalID"];
					entity.Title = (reader.IsDBNull(((int)VwZnodeCategoriesCatalogsColumn.Title)))?null:(System.String)reader[((int)VwZnodeCategoriesCatalogsColumn.Title)];
					//entity.Title = (Convert.IsDBNull(reader["Title"]))?string.Empty:(System.String)reader["Title"];
					entity.VisibleInd = (System.Boolean)reader[((int)VwZnodeCategoriesCatalogsColumn.VisibleInd)];
					//entity.VisibleInd = (Convert.IsDBNull(reader["VisibleInd"]))?false:(System.Boolean)reader["VisibleInd"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="VwZnodeCategoriesCatalogs"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="VwZnodeCategoriesCatalogs"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, VwZnodeCategoriesCatalogs entity)
		{
			reader.Read();
			entity.CategoryID = (System.Int32)reader[((int)VwZnodeCategoriesCatalogsColumn.CategoryID)];
			//entity.CategoryID = (Convert.IsDBNull(reader["CategoryID"]))?(int)0:(System.Int32)reader["CategoryID"];
			entity.CatalogID = (reader.IsDBNull(((int)VwZnodeCategoriesCatalogsColumn.CatalogID)))?null:(System.Int32?)reader[((int)VwZnodeCategoriesCatalogsColumn.CatalogID)];
			//entity.CatalogID = (Convert.IsDBNull(reader["CatalogID"]))?(int)0:(System.Int32?)reader["CatalogID"];
			entity.DisplayOrder = (reader.IsDBNull(((int)VwZnodeCategoriesCatalogsColumn.DisplayOrder)))?null:(System.Int32?)reader[((int)VwZnodeCategoriesCatalogsColumn.DisplayOrder)];
			//entity.DisplayOrder = (Convert.IsDBNull(reader["DisplayOrder"]))?(int)0:(System.Int32?)reader["DisplayOrder"];
			entity.ExternalID = (reader.IsDBNull(((int)VwZnodeCategoriesCatalogsColumn.ExternalID)))?null:(System.String)reader[((int)VwZnodeCategoriesCatalogsColumn.ExternalID)];
			//entity.ExternalID = (Convert.IsDBNull(reader["ExternalID"]))?string.Empty:(System.String)reader["ExternalID"];
			entity.Name = (System.String)reader[((int)VwZnodeCategoriesCatalogsColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.PortalID = (reader.IsDBNull(((int)VwZnodeCategoriesCatalogsColumn.PortalID)))?null:(System.Int32?)reader[((int)VwZnodeCategoriesCatalogsColumn.PortalID)];
			//entity.PortalID = (Convert.IsDBNull(reader["PortalID"]))?(int)0:(System.Int32?)reader["PortalID"];
			entity.Title = (reader.IsDBNull(((int)VwZnodeCategoriesCatalogsColumn.Title)))?null:(System.String)reader[((int)VwZnodeCategoriesCatalogsColumn.Title)];
			//entity.Title = (Convert.IsDBNull(reader["Title"]))?string.Empty:(System.String)reader["Title"];
			entity.VisibleInd = (System.Boolean)reader[((int)VwZnodeCategoriesCatalogsColumn.VisibleInd)];
			//entity.VisibleInd = (Convert.IsDBNull(reader["VisibleInd"]))?false:(System.Boolean)reader["VisibleInd"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="VwZnodeCategoriesCatalogs"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="VwZnodeCategoriesCatalogs"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, VwZnodeCategoriesCatalogs entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CategoryID = (Convert.IsDBNull(dataRow["CategoryID"]))?(int)0:(System.Int32)dataRow["CategoryID"];
			entity.CatalogID = (Convert.IsDBNull(dataRow["CatalogID"]))?(int)0:(System.Int32?)dataRow["CatalogID"];
			entity.DisplayOrder = (Convert.IsDBNull(dataRow["DisplayOrder"]))?(int)0:(System.Int32?)dataRow["DisplayOrder"];
			entity.ExternalID = (Convert.IsDBNull(dataRow["ExternalID"]))?string.Empty:(System.String)dataRow["ExternalID"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.PortalID = (Convert.IsDBNull(dataRow["PortalID"]))?(int)0:(System.Int32?)dataRow["PortalID"];
			entity.Title = (Convert.IsDBNull(dataRow["Title"]))?string.Empty:(System.String)dataRow["Title"];
			entity.VisibleInd = (Convert.IsDBNull(dataRow["VisibleInd"]))?false:(System.Boolean)dataRow["VisibleInd"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region VwZnodeCategoriesCatalogsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeCategoriesCatalogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeCategoriesCatalogsFilterBuilder : SqlFilterBuilder<VwZnodeCategoriesCatalogsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesCatalogsFilterBuilder class.
		/// </summary>
		public VwZnodeCategoriesCatalogsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesCatalogsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeCategoriesCatalogsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesCatalogsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeCategoriesCatalogsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeCategoriesCatalogsFilterBuilder

	#region VwZnodeCategoriesCatalogsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeCategoriesCatalogs"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeCategoriesCatalogsParameterBuilder : ParameterizedSqlFilterBuilder<VwZnodeCategoriesCatalogsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesCatalogsParameterBuilder class.
		/// </summary>
		public VwZnodeCategoriesCatalogsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesCatalogsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeCategoriesCatalogsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesCatalogsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeCategoriesCatalogsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeCategoriesCatalogsParameterBuilder
	
	#region VwZnodeCategoriesCatalogsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeCategoriesCatalogs"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class VwZnodeCategoriesCatalogsSortBuilder : SqlSortBuilder<VwZnodeCategoriesCatalogsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeCategoriesCatalogsSqlSortBuilder class.
		/// </summary>
		public VwZnodeCategoriesCatalogsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion VwZnodeCategoriesCatalogsSortBuilder

} // end namespace
