﻿#region Using directives

using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="VwZnodeProductsCategoriesPromotionsProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract class VwZnodeProductsCategoriesPromotionsProviderBaseCore : EntityViewProviderBase<VwZnodeProductsCategoriesPromotions>
	{
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions
		
		/*
		///<summary>
		/// Fill an VList&lt;VwZnodeProductsCategoriesPromotions&gt; From a DataSet
		///</summary>
		/// <param name="dataSet">the DataSet</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList&lt;VwZnodeProductsCategoriesPromotions&gt;"/></returns>
		protected static VList&lt;VwZnodeProductsCategoriesPromotions&gt; Fill(DataSet dataSet, VList<VwZnodeProductsCategoriesPromotions> rows, int start, int pagelen)
		{
			if (dataSet.Tables.Count == 1)
			{
				return Fill(dataSet.Tables[0], rows, start, pagelen);
			}
			else
			{
				return new VList<VwZnodeProductsCategoriesPromotions>();
			}	
		}
		
		
		///<summary>
		/// Fill an VList&lt;VwZnodeProductsCategoriesPromotions&gt; From a DataTable
		///</summary>
		/// <param name="dataTable">the DataTable that hold the data.</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pagelen">number of row.</param>
		///<returns><see chref="VList<VwZnodeProductsCategoriesPromotions>"/></returns>
		protected static VList&lt;VwZnodeProductsCategoriesPromotions&gt; Fill(DataTable dataTable, VList<VwZnodeProductsCategoriesPromotions> rows, int start, int pagelen)
		{
			int recordnum = 0;
			
			System.Collections.IEnumerator dataRows =  dataTable.Rows.GetEnumerator();
			
			while (dataRows.MoveNext() && (pagelen != 0))
			{
				if(recordnum >= start)
				{
					DataRow row = (DataRow)dataRows.Current;
				
					VwZnodeProductsCategoriesPromotions c = new VwZnodeProductsCategoriesPromotions();
					c.PromotionID = (Convert.IsDBNull(row["PromotionID"]))?(int)0:(System.Int32)row["PromotionID"];
					c.DiscountTypeID = (Convert.IsDBNull(row["DiscountTypeID"]))?(int)0:(System.Int32)row["DiscountTypeID"];
					c.CategoryID = (Convert.IsDBNull(row["CategoryID"]))?(int)0:(System.Int32)row["CategoryID"];
					c.ProductID = (Convert.IsDBNull(row["ProductID"]))?(int)0:(System.Int32)row["ProductID"];
					c.Name = (Convert.IsDBNull(row["Name"]))?string.Empty:(System.String)row["Name"];
					c.ShortDescription = (Convert.IsDBNull(row["ShortDescription"]))?string.Empty:(System.String)row["ShortDescription"];
					c.Description = (Convert.IsDBNull(row["Description"]))?string.Empty:(System.String)row["Description"];
					c.FeaturesDesc = (Convert.IsDBNull(row["FeaturesDesc"]))?string.Empty:(System.String)row["FeaturesDesc"];
					c.ProductNum = (Convert.IsDBNull(row["ProductNum"]))?string.Empty:(System.String)row["ProductNum"];
					c.ProductTypeID = (Convert.IsDBNull(row["ProductTypeID"]))?(int)0:(System.Int32)row["ProductTypeID"];
					c.RetailPrice = (Convert.IsDBNull(row["RetailPrice"]))?0:(System.Decimal?)row["RetailPrice"];
					c.SalePrice = (Convert.IsDBNull(row["SalePrice"]))?0:(System.Decimal?)row["SalePrice"];
					c.WholesalePrice = (Convert.IsDBNull(row["WholesalePrice"]))?0:(System.Decimal?)row["WholesalePrice"];
					c.ImageFile = (Convert.IsDBNull(row["ImageFile"]))?string.Empty:(System.String)row["ImageFile"];
					c.ImageAltTag = (Convert.IsDBNull(row["ImageAltTag"]))?string.Empty:(System.String)row["ImageAltTag"];
					c.Weight = (Convert.IsDBNull(row["Weight"]))?0.0m:(System.Decimal?)row["Weight"];
					c.Length = (Convert.IsDBNull(row["Length"]))?0.0m:(System.Decimal?)row["Length"];
					c.Width = (Convert.IsDBNull(row["Width"]))?0.0m:(System.Decimal?)row["Width"];
					c.Height = (Convert.IsDBNull(row["Height"]))?0.0m:(System.Decimal?)row["Height"];
					c.BeginActiveDate = (Convert.IsDBNull(row["BeginActiveDate"]))?DateTime.MinValue:(System.DateTime?)row["BeginActiveDate"];
					c.EndActiveDate = (Convert.IsDBNull(row["EndActiveDate"]))?DateTime.MinValue:(System.DateTime?)row["EndActiveDate"];
					c.DisplayOrder = (Convert.IsDBNull(row["DisplayOrder"]))?(int)0:(System.Int32?)row["DisplayOrder"];
					c.ActiveInd = (Convert.IsDBNull(row["ActiveInd"]))?false:(System.Boolean)row["ActiveInd"];
					c.CallForPricing = (Convert.IsDBNull(row["CallForPricing"]))?false:(System.Boolean)row["CallForPricing"];
					c.HomepageSpecial = (Convert.IsDBNull(row["HomepageSpecial"]))?false:(System.Boolean)row["HomepageSpecial"];
					c.CategorySpecial = (Convert.IsDBNull(row["CategorySpecial"]))?false:(System.Boolean)row["CategorySpecial"];
					c.InventoryDisplay = (Convert.IsDBNull(row["InventoryDisplay"]))?(byte)0:(System.Byte)row["InventoryDisplay"];
					c.Keywords = (Convert.IsDBNull(row["Keywords"]))?string.Empty:(System.String)row["Keywords"];
					c.ManufacturerID = (Convert.IsDBNull(row["ManufacturerID"]))?(int)0:(System.Int32?)row["ManufacturerID"];
					c.AdditionalInfoLink = (Convert.IsDBNull(row["AdditionalInfoLink"]))?string.Empty:(System.String)row["AdditionalInfoLink"];
					c.AdditionalInfoLinkLabel = (Convert.IsDBNull(row["AdditionalInfoLinkLabel"]))?string.Empty:(System.String)row["AdditionalInfoLinkLabel"];
					c.ShippingRuleTypeID = (Convert.IsDBNull(row["ShippingRuleTypeID"]))?(int)0:(System.Int32?)row["ShippingRuleTypeID"];
					c.ShippingRate = (Convert.IsDBNull(row["ShippingRate"]))?0:(System.Decimal?)row["ShippingRate"];
					c.SEOTitle = (Convert.IsDBNull(row["SEOTitle"]))?string.Empty:(System.String)row["SEOTitle"];
					c.SEOKeywords = (Convert.IsDBNull(row["SEOKeywords"]))?string.Empty:(System.String)row["SEOKeywords"];
					c.SEODescription = (Convert.IsDBNull(row["SEODescription"]))?string.Empty:(System.String)row["SEODescription"];
					c.Custom1 = (Convert.IsDBNull(row["Custom1"]))?string.Empty:(System.String)row["Custom1"];
					c.Custom2 = (Convert.IsDBNull(row["Custom2"]))?string.Empty:(System.String)row["Custom2"];
					c.Custom3 = (Convert.IsDBNull(row["Custom3"]))?string.Empty:(System.String)row["Custom3"];
					c.ShipEachItemSeparately = (Convert.IsDBNull(row["ShipEachItemSeparately"]))?false:(System.Boolean?)row["ShipEachItemSeparately"];
					c.AllowBackOrder = (Convert.IsDBNull(row["AllowBackOrder"]))?false:(System.Boolean?)row["AllowBackOrder"];
					c.BackOrderMsg = (Convert.IsDBNull(row["BackOrderMsg"]))?string.Empty:(System.String)row["BackOrderMsg"];
					c.DropShipInd = (Convert.IsDBNull(row["DropShipInd"]))?false:(System.Boolean?)row["DropShipInd"];
					c.DropShipEmailID = (Convert.IsDBNull(row["DropShipEmailID"]))?string.Empty:(System.String)row["DropShipEmailID"];
					c.Specifications = (Convert.IsDBNull(row["Specifications"]))?string.Empty:(System.String)row["Specifications"];
					c.AdditionalInformation = (Convert.IsDBNull(row["AdditionalInformation"]))?string.Empty:(System.String)row["AdditionalInformation"];
					c.InStockMsg = (Convert.IsDBNull(row["InStockMsg"]))?string.Empty:(System.String)row["InStockMsg"];
					c.OutOfStockMsg = (Convert.IsDBNull(row["OutOfStockMsg"]))?string.Empty:(System.String)row["OutOfStockMsg"];
					c.TrackInventoryInd = (Convert.IsDBNull(row["TrackInventoryInd"]))?false:(System.Boolean?)row["TrackInventoryInd"];
					c.DownloadLink = (Convert.IsDBNull(row["DownloadLink"]))?string.Empty:(System.String)row["DownloadLink"];
					c.FreeShippingInd = (Convert.IsDBNull(row["FreeShippingInd"]))?false:(System.Boolean?)row["FreeShippingInd"];
					c.NewProductInd = (Convert.IsDBNull(row["NewProductInd"]))?false:(System.Boolean?)row["NewProductInd"];
					c.SEOURL = (Convert.IsDBNull(row["SEOURL"]))?string.Empty:(System.String)row["SEOURL"];
					c.MaxQty = (Convert.IsDBNull(row["MaxQty"]))?(int)0:(System.Int32?)row["MaxQty"];
					c.ShipSeparately = (Convert.IsDBNull(row["ShipSeparately"]))?false:(System.Boolean)row["ShipSeparately"];
					c.FeaturedInd = (Convert.IsDBNull(row["FeaturedInd"]))?false:(System.Boolean)row["FeaturedInd"];
					c.WebServiceDownloadDte = (Convert.IsDBNull(row["WebServiceDownloadDte"]))?DateTime.MinValue:(System.DateTime?)row["WebServiceDownloadDte"];
					c.UpdateDte = (Convert.IsDBNull(row["UpdateDte"]))?DateTime.MinValue:(System.DateTime?)row["UpdateDte"];
					c.SupplierID = (Convert.IsDBNull(row["SupplierID"]))?(int)0:(System.Int32?)row["SupplierID"];
					c.RecurringBillingInd = (Convert.IsDBNull(row["RecurringBillingInd"]))?false:(System.Boolean)row["RecurringBillingInd"];
					c.RecurringBillingInstallmentInd = (Convert.IsDBNull(row["RecurringBillingInstallmentInd"]))?false:(System.Boolean)row["RecurringBillingInstallmentInd"];
					c.RecurringBillingPeriod = (Convert.IsDBNull(row["RecurringBillingPeriod"]))?string.Empty:(System.String)row["RecurringBillingPeriod"];
					c.RecurringBillingFrequency = (Convert.IsDBNull(row["RecurringBillingFrequency"]))?string.Empty:(System.String)row["RecurringBillingFrequency"];
					c.RecurringBillingTotalCycles = (Convert.IsDBNull(row["RecurringBillingTotalCycles"]))?(int)0:(System.Int32?)row["RecurringBillingTotalCycles"];
					c.RecurringBillingInitialAmount = (Convert.IsDBNull(row["RecurringBillingInitialAmount"]))?0:(System.Decimal?)row["RecurringBillingInitialAmount"];
					c.TaxClassID = (Convert.IsDBNull(row["TaxClassID"]))?(int)0:(System.Int32?)row["TaxClassID"];
					c.MinQty = (Convert.IsDBNull(row["MinQty"]))?(int)0:(System.Int32?)row["MinQty"];
					c.ReviewStateID = (Convert.IsDBNull(row["ReviewStateID"]))?(int)0:(System.Int32?)row["ReviewStateID"];
					c.AffiliateUrl = (Convert.IsDBNull(row["AffiliateUrl"]))?string.Empty:(System.String)row["AffiliateUrl"];
					c.IsShippable = (Convert.IsDBNull(row["IsShippable"]))?false:(System.Boolean?)row["IsShippable"];
					c.AccountID = (Convert.IsDBNull(row["AccountID"]))?(int)0:(System.Int32?)row["AccountID"];
					c.PortalID = (Convert.IsDBNull(row["PortalID"]))?(int)0:(System.Int32?)row["PortalID"];
					c.Franchisable = (Convert.IsDBNull(row["Franchisable"]))?false:(System.Boolean)row["Franchisable"];
					c.ExpirationPeriod = (Convert.IsDBNull(row["ExpirationPeriod"]))?(int)0:(System.Int32?)row["ExpirationPeriod"];
					c.ExpirationFrequency = (Convert.IsDBNull(row["ExpirationFrequency"]))?(int)0:(System.Int32?)row["ExpirationFrequency"];
					c.CreateDate = (Convert.IsDBNull(row["CreateDate"]))?DateTime.MinValue:(System.DateTime?)row["CreateDate"];
					c.ExternalID = (Convert.IsDBNull(row["ExternalID"]))?string.Empty:(System.String)row["ExternalID"];
					c.AcceptChanges();
					rows.Add(c);
					pagelen -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		*/	
						
		///<summary>
		/// Fill an <see cref="VList&lt;VwZnodeProductsCategoriesPromotions&gt;"/> From a DataReader.
		///</summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Start row</param>
		/// <param name="pageLength">number of row.</param>
		///<returns>a <see cref="VList&lt;VwZnodeProductsCategoriesPromotions&gt;"/></returns>
		protected VList<VwZnodeProductsCategoriesPromotions> Fill(IDataReader reader, VList<VwZnodeProductsCategoriesPromotions> rows, int start, int pageLength)
		{
			int recordnum = 0;
			while (reader.Read() && (pageLength != 0))
			{
				if(recordnum >= start)
				{
					VwZnodeProductsCategoriesPromotions entity = null;
					if (DataRepository.Provider.UseEntityFactory)
					{
						entity = EntityManager.CreateViewEntity<VwZnodeProductsCategoriesPromotions>("VwZnodeProductsCategoriesPromotions",  DataRepository.Provider.EntityCreationalFactoryType); 
					}
					else
					{
						entity = new VwZnodeProductsCategoriesPromotions();
					}
					
					entity.SuppressEntityEvents = true;

					entity.PromotionID = (System.Int32)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.PromotionID)];
					//entity.PromotionID = (Convert.IsDBNull(reader["PromotionID"]))?(int)0:(System.Int32)reader["PromotionID"];
					entity.DiscountTypeID = (System.Int32)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.DiscountTypeID)];
					//entity.DiscountTypeID = (Convert.IsDBNull(reader["DiscountTypeID"]))?(int)0:(System.Int32)reader["DiscountTypeID"];
					entity.CategoryID = (System.Int32)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.CategoryID)];
					//entity.CategoryID = (Convert.IsDBNull(reader["CategoryID"]))?(int)0:(System.Int32)reader["CategoryID"];
					entity.ProductID = (System.Int32)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ProductID)];
					//entity.ProductID = (Convert.IsDBNull(reader["ProductID"]))?(int)0:(System.Int32)reader["ProductID"];
					entity.Name = (System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Name)];
					//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
					entity.ShortDescription = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ShortDescription)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ShortDescription)];
					//entity.ShortDescription = (Convert.IsDBNull(reader["ShortDescription"]))?string.Empty:(System.String)reader["ShortDescription"];
					entity.Description = (System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Description)];
					//entity.Description = (Convert.IsDBNull(reader["Description"]))?string.Empty:(System.String)reader["Description"];
					entity.FeaturesDesc = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.FeaturesDesc)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.FeaturesDesc)];
					//entity.FeaturesDesc = (Convert.IsDBNull(reader["FeaturesDesc"]))?string.Empty:(System.String)reader["FeaturesDesc"];
					entity.ProductNum = (System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ProductNum)];
					//entity.ProductNum = (Convert.IsDBNull(reader["ProductNum"]))?string.Empty:(System.String)reader["ProductNum"];
					entity.ProductTypeID = (System.Int32)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ProductTypeID)];
					//entity.ProductTypeID = (Convert.IsDBNull(reader["ProductTypeID"]))?(int)0:(System.Int32)reader["ProductTypeID"];
					entity.RetailPrice = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.RetailPrice)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.RetailPrice)];
					//entity.RetailPrice = (Convert.IsDBNull(reader["RetailPrice"]))?0:(System.Decimal?)reader["RetailPrice"];
					entity.SalePrice = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.SalePrice)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.SalePrice)];
					//entity.SalePrice = (Convert.IsDBNull(reader["SalePrice"]))?0:(System.Decimal?)reader["SalePrice"];
					entity.WholesalePrice = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.WholesalePrice)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.WholesalePrice)];
					//entity.WholesalePrice = (Convert.IsDBNull(reader["WholesalePrice"]))?0:(System.Decimal?)reader["WholesalePrice"];
					entity.ImageFile = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ImageFile)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ImageFile)];
					//entity.ImageFile = (Convert.IsDBNull(reader["ImageFile"]))?string.Empty:(System.String)reader["ImageFile"];
					entity.ImageAltTag = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ImageAltTag)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ImageAltTag)];
					//entity.ImageAltTag = (Convert.IsDBNull(reader["ImageAltTag"]))?string.Empty:(System.String)reader["ImageAltTag"];
					entity.Weight = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Weight)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Weight)];
					//entity.Weight = (Convert.IsDBNull(reader["Weight"]))?0.0m:(System.Decimal?)reader["Weight"];
					entity.Length = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Length)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Length)];
					//entity.Length = (Convert.IsDBNull(reader["Length"]))?0.0m:(System.Decimal?)reader["Length"];
					entity.Width = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Width)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Width)];
					//entity.Width = (Convert.IsDBNull(reader["Width"]))?0.0m:(System.Decimal?)reader["Width"];
					entity.Height = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Height)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Height)];
					//entity.Height = (Convert.IsDBNull(reader["Height"]))?0.0m:(System.Decimal?)reader["Height"];
					entity.BeginActiveDate = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.BeginActiveDate)))?null:(System.DateTime?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.BeginActiveDate)];
					//entity.BeginActiveDate = (Convert.IsDBNull(reader["BeginActiveDate"]))?DateTime.MinValue:(System.DateTime?)reader["BeginActiveDate"];
					entity.EndActiveDate = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.EndActiveDate)))?null:(System.DateTime?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.EndActiveDate)];
					//entity.EndActiveDate = (Convert.IsDBNull(reader["EndActiveDate"]))?DateTime.MinValue:(System.DateTime?)reader["EndActiveDate"];
					entity.DisplayOrder = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.DisplayOrder)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.DisplayOrder)];
					//entity.DisplayOrder = (Convert.IsDBNull(reader["DisplayOrder"]))?(int)0:(System.Int32?)reader["DisplayOrder"];
					entity.ActiveInd = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ActiveInd)];
					//entity.ActiveInd = (Convert.IsDBNull(reader["ActiveInd"]))?false:(System.Boolean)reader["ActiveInd"];
					entity.CallForPricing = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.CallForPricing)];
					//entity.CallForPricing = (Convert.IsDBNull(reader["CallForPricing"]))?false:(System.Boolean)reader["CallForPricing"];
					entity.HomepageSpecial = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.HomepageSpecial)];
					//entity.HomepageSpecial = (Convert.IsDBNull(reader["HomepageSpecial"]))?false:(System.Boolean)reader["HomepageSpecial"];
					entity.CategorySpecial = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.CategorySpecial)];
					//entity.CategorySpecial = (Convert.IsDBNull(reader["CategorySpecial"]))?false:(System.Boolean)reader["CategorySpecial"];
					entity.InventoryDisplay = (System.Byte)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.InventoryDisplay)];
					//entity.InventoryDisplay = (Convert.IsDBNull(reader["InventoryDisplay"]))?(byte)0:(System.Byte)reader["InventoryDisplay"];
					entity.Keywords = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Keywords)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Keywords)];
					//entity.Keywords = (Convert.IsDBNull(reader["Keywords"]))?string.Empty:(System.String)reader["Keywords"];
					entity.ManufacturerID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ManufacturerID)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ManufacturerID)];
					//entity.ManufacturerID = (Convert.IsDBNull(reader["ManufacturerID"]))?(int)0:(System.Int32?)reader["ManufacturerID"];
					entity.AdditionalInfoLink = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.AdditionalInfoLink)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.AdditionalInfoLink)];
					//entity.AdditionalInfoLink = (Convert.IsDBNull(reader["AdditionalInfoLink"]))?string.Empty:(System.String)reader["AdditionalInfoLink"];
					entity.AdditionalInfoLinkLabel = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.AdditionalInfoLinkLabel)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.AdditionalInfoLinkLabel)];
					//entity.AdditionalInfoLinkLabel = (Convert.IsDBNull(reader["AdditionalInfoLinkLabel"]))?string.Empty:(System.String)reader["AdditionalInfoLinkLabel"];
					entity.ShippingRuleTypeID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ShippingRuleTypeID)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ShippingRuleTypeID)];
					//entity.ShippingRuleTypeID = (Convert.IsDBNull(reader["ShippingRuleTypeID"]))?(int)0:(System.Int32?)reader["ShippingRuleTypeID"];
					entity.ShippingRate = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ShippingRate)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ShippingRate)];
					//entity.ShippingRate = (Convert.IsDBNull(reader["ShippingRate"]))?0:(System.Decimal?)reader["ShippingRate"];
					entity.SEOTitle = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.SEOTitle)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.SEOTitle)];
					//entity.SEOTitle = (Convert.IsDBNull(reader["SEOTitle"]))?string.Empty:(System.String)reader["SEOTitle"];
					entity.SEOKeywords = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.SEOKeywords)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.SEOKeywords)];
					//entity.SEOKeywords = (Convert.IsDBNull(reader["SEOKeywords"]))?string.Empty:(System.String)reader["SEOKeywords"];
					entity.SEODescription = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.SEODescription)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.SEODescription)];
					//entity.SEODescription = (Convert.IsDBNull(reader["SEODescription"]))?string.Empty:(System.String)reader["SEODescription"];
					entity.Custom1 = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Custom1)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Custom1)];
					//entity.Custom1 = (Convert.IsDBNull(reader["Custom1"]))?string.Empty:(System.String)reader["Custom1"];
					entity.Custom2 = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Custom2)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Custom2)];
					//entity.Custom2 = (Convert.IsDBNull(reader["Custom2"]))?string.Empty:(System.String)reader["Custom2"];
					entity.Custom3 = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Custom3)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Custom3)];
					//entity.Custom3 = (Convert.IsDBNull(reader["Custom3"]))?string.Empty:(System.String)reader["Custom3"];
					entity.ShipEachItemSeparately = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ShipEachItemSeparately)))?null:(System.Boolean?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ShipEachItemSeparately)];
					//entity.ShipEachItemSeparately = (Convert.IsDBNull(reader["ShipEachItemSeparately"]))?false:(System.Boolean?)reader["ShipEachItemSeparately"];
					entity.AllowBackOrder = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.AllowBackOrder)))?null:(System.Boolean?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.AllowBackOrder)];
					//entity.AllowBackOrder = (Convert.IsDBNull(reader["AllowBackOrder"]))?false:(System.Boolean?)reader["AllowBackOrder"];
					entity.BackOrderMsg = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.BackOrderMsg)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.BackOrderMsg)];
					//entity.BackOrderMsg = (Convert.IsDBNull(reader["BackOrderMsg"]))?string.Empty:(System.String)reader["BackOrderMsg"];
					entity.DropShipInd = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.DropShipInd)))?null:(System.Boolean?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.DropShipInd)];
					//entity.DropShipInd = (Convert.IsDBNull(reader["DropShipInd"]))?false:(System.Boolean?)reader["DropShipInd"];
					entity.DropShipEmailID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.DropShipEmailID)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.DropShipEmailID)];
					//entity.DropShipEmailID = (Convert.IsDBNull(reader["DropShipEmailID"]))?string.Empty:(System.String)reader["DropShipEmailID"];
					entity.Specifications = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Specifications)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Specifications)];
					//entity.Specifications = (Convert.IsDBNull(reader["Specifications"]))?string.Empty:(System.String)reader["Specifications"];
					entity.AdditionalInformation = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.AdditionalInformation)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.AdditionalInformation)];
					//entity.AdditionalInformation = (Convert.IsDBNull(reader["AdditionalInformation"]))?string.Empty:(System.String)reader["AdditionalInformation"];
					entity.InStockMsg = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.InStockMsg)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.InStockMsg)];
					//entity.InStockMsg = (Convert.IsDBNull(reader["InStockMsg"]))?string.Empty:(System.String)reader["InStockMsg"];
					entity.OutOfStockMsg = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.OutOfStockMsg)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.OutOfStockMsg)];
					//entity.OutOfStockMsg = (Convert.IsDBNull(reader["OutOfStockMsg"]))?string.Empty:(System.String)reader["OutOfStockMsg"];
					entity.TrackInventoryInd = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.TrackInventoryInd)))?null:(System.Boolean?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.TrackInventoryInd)];
					//entity.TrackInventoryInd = (Convert.IsDBNull(reader["TrackInventoryInd"]))?false:(System.Boolean?)reader["TrackInventoryInd"];
					entity.DownloadLink = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.DownloadLink)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.DownloadLink)];
					//entity.DownloadLink = (Convert.IsDBNull(reader["DownloadLink"]))?string.Empty:(System.String)reader["DownloadLink"];
					entity.FreeShippingInd = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.FreeShippingInd)))?null:(System.Boolean?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.FreeShippingInd)];
					//entity.FreeShippingInd = (Convert.IsDBNull(reader["FreeShippingInd"]))?false:(System.Boolean?)reader["FreeShippingInd"];
					entity.NewProductInd = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.NewProductInd)))?null:(System.Boolean?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.NewProductInd)];
					//entity.NewProductInd = (Convert.IsDBNull(reader["NewProductInd"]))?false:(System.Boolean?)reader["NewProductInd"];
					entity.SEOURL = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.SEOURL)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.SEOURL)];
					//entity.SEOURL = (Convert.IsDBNull(reader["SEOURL"]))?string.Empty:(System.String)reader["SEOURL"];
					entity.MaxQty = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.MaxQty)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.MaxQty)];
					//entity.MaxQty = (Convert.IsDBNull(reader["MaxQty"]))?(int)0:(System.Int32?)reader["MaxQty"];
					entity.ShipSeparately = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ShipSeparately)];
					//entity.ShipSeparately = (Convert.IsDBNull(reader["ShipSeparately"]))?false:(System.Boolean)reader["ShipSeparately"];
					entity.FeaturedInd = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.FeaturedInd)];
					//entity.FeaturedInd = (Convert.IsDBNull(reader["FeaturedInd"]))?false:(System.Boolean)reader["FeaturedInd"];
					entity.WebServiceDownloadDte = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.WebServiceDownloadDte)))?null:(System.DateTime?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.WebServiceDownloadDte)];
					//entity.WebServiceDownloadDte = (Convert.IsDBNull(reader["WebServiceDownloadDte"]))?DateTime.MinValue:(System.DateTime?)reader["WebServiceDownloadDte"];
					entity.UpdateDte = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.UpdateDte)))?null:(System.DateTime?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.UpdateDte)];
					//entity.UpdateDte = (Convert.IsDBNull(reader["UpdateDte"]))?DateTime.MinValue:(System.DateTime?)reader["UpdateDte"];
					entity.SupplierID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.SupplierID)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.SupplierID)];
					//entity.SupplierID = (Convert.IsDBNull(reader["SupplierID"]))?(int)0:(System.Int32?)reader["SupplierID"];
					entity.RecurringBillingInd = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingInd)];
					//entity.RecurringBillingInd = (Convert.IsDBNull(reader["RecurringBillingInd"]))?false:(System.Boolean)reader["RecurringBillingInd"];
					entity.RecurringBillingInstallmentInd = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingInstallmentInd)];
					//entity.RecurringBillingInstallmentInd = (Convert.IsDBNull(reader["RecurringBillingInstallmentInd"]))?false:(System.Boolean)reader["RecurringBillingInstallmentInd"];
					entity.RecurringBillingPeriod = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingPeriod)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingPeriod)];
					//entity.RecurringBillingPeriod = (Convert.IsDBNull(reader["RecurringBillingPeriod"]))?string.Empty:(System.String)reader["RecurringBillingPeriod"];
					entity.RecurringBillingFrequency = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingFrequency)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingFrequency)];
					//entity.RecurringBillingFrequency = (Convert.IsDBNull(reader["RecurringBillingFrequency"]))?string.Empty:(System.String)reader["RecurringBillingFrequency"];
					entity.RecurringBillingTotalCycles = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingTotalCycles)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingTotalCycles)];
					//entity.RecurringBillingTotalCycles = (Convert.IsDBNull(reader["RecurringBillingTotalCycles"]))?(int)0:(System.Int32?)reader["RecurringBillingTotalCycles"];
					entity.RecurringBillingInitialAmount = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingInitialAmount)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingInitialAmount)];
					//entity.RecurringBillingInitialAmount = (Convert.IsDBNull(reader["RecurringBillingInitialAmount"]))?0:(System.Decimal?)reader["RecurringBillingInitialAmount"];
					entity.TaxClassID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.TaxClassID)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.TaxClassID)];
					//entity.TaxClassID = (Convert.IsDBNull(reader["TaxClassID"]))?(int)0:(System.Int32?)reader["TaxClassID"];
					entity.MinQty = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.MinQty)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.MinQty)];
					//entity.MinQty = (Convert.IsDBNull(reader["MinQty"]))?(int)0:(System.Int32?)reader["MinQty"];
					entity.ReviewStateID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ReviewStateID)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ReviewStateID)];
					//entity.ReviewStateID = (Convert.IsDBNull(reader["ReviewStateID"]))?(int)0:(System.Int32?)reader["ReviewStateID"];
					entity.AffiliateUrl = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.AffiliateUrl)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.AffiliateUrl)];
					//entity.AffiliateUrl = (Convert.IsDBNull(reader["AffiliateUrl"]))?string.Empty:(System.String)reader["AffiliateUrl"];
					entity.IsShippable = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.IsShippable)))?null:(System.Boolean?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.IsShippable)];
					//entity.IsShippable = (Convert.IsDBNull(reader["IsShippable"]))?false:(System.Boolean?)reader["IsShippable"];
					entity.AccountID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.AccountID)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.AccountID)];
					//entity.AccountID = (Convert.IsDBNull(reader["AccountID"]))?(int)0:(System.Int32?)reader["AccountID"];
					entity.PortalID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.PortalID)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.PortalID)];
					//entity.PortalID = (Convert.IsDBNull(reader["PortalID"]))?(int)0:(System.Int32?)reader["PortalID"];
					entity.Franchisable = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Franchisable)];
					//entity.Franchisable = (Convert.IsDBNull(reader["Franchisable"]))?false:(System.Boolean)reader["Franchisable"];
					entity.ExpirationPeriod = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ExpirationPeriod)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ExpirationPeriod)];
					//entity.ExpirationPeriod = (Convert.IsDBNull(reader["ExpirationPeriod"]))?(int)0:(System.Int32?)reader["ExpirationPeriod"];
					entity.ExpirationFrequency = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ExpirationFrequency)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ExpirationFrequency)];
					//entity.ExpirationFrequency = (Convert.IsDBNull(reader["ExpirationFrequency"]))?(int)0:(System.Int32?)reader["ExpirationFrequency"];
					entity.CreateDate = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.CreateDate)))?null:(System.DateTime?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.CreateDate)];
					//entity.CreateDate = (Convert.IsDBNull(reader["CreateDate"]))?DateTime.MinValue:(System.DateTime?)reader["CreateDate"];
					entity.ExternalID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ExternalID)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ExternalID)];
					//entity.ExternalID = (Convert.IsDBNull(reader["ExternalID"]))?string.Empty:(System.String)reader["ExternalID"];
					entity.AcceptChanges();
					entity.SuppressEntityEvents = false;
					
					rows.Add(entity);
					pageLength -= 1;
				}
				recordnum += 1;
			}
			return rows;
		}
		
		
		/// <summary>
		/// Refreshes the <see cref="VwZnodeProductsCategoriesPromotions"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="VwZnodeProductsCategoriesPromotions"/> object to refresh.</param>
		protected void RefreshEntity(IDataReader reader, VwZnodeProductsCategoriesPromotions entity)
		{
			reader.Read();
			entity.PromotionID = (System.Int32)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.PromotionID)];
			//entity.PromotionID = (Convert.IsDBNull(reader["PromotionID"]))?(int)0:(System.Int32)reader["PromotionID"];
			entity.DiscountTypeID = (System.Int32)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.DiscountTypeID)];
			//entity.DiscountTypeID = (Convert.IsDBNull(reader["DiscountTypeID"]))?(int)0:(System.Int32)reader["DiscountTypeID"];
			entity.CategoryID = (System.Int32)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.CategoryID)];
			//entity.CategoryID = (Convert.IsDBNull(reader["CategoryID"]))?(int)0:(System.Int32)reader["CategoryID"];
			entity.ProductID = (System.Int32)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ProductID)];
			//entity.ProductID = (Convert.IsDBNull(reader["ProductID"]))?(int)0:(System.Int32)reader["ProductID"];
			entity.Name = (System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Name)];
			//entity.Name = (Convert.IsDBNull(reader["Name"]))?string.Empty:(System.String)reader["Name"];
			entity.ShortDescription = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ShortDescription)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ShortDescription)];
			//entity.ShortDescription = (Convert.IsDBNull(reader["ShortDescription"]))?string.Empty:(System.String)reader["ShortDescription"];
			entity.Description = (System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Description)];
			//entity.Description = (Convert.IsDBNull(reader["Description"]))?string.Empty:(System.String)reader["Description"];
			entity.FeaturesDesc = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.FeaturesDesc)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.FeaturesDesc)];
			//entity.FeaturesDesc = (Convert.IsDBNull(reader["FeaturesDesc"]))?string.Empty:(System.String)reader["FeaturesDesc"];
			entity.ProductNum = (System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ProductNum)];
			//entity.ProductNum = (Convert.IsDBNull(reader["ProductNum"]))?string.Empty:(System.String)reader["ProductNum"];
			entity.ProductTypeID = (System.Int32)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ProductTypeID)];
			//entity.ProductTypeID = (Convert.IsDBNull(reader["ProductTypeID"]))?(int)0:(System.Int32)reader["ProductTypeID"];
			entity.RetailPrice = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.RetailPrice)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.RetailPrice)];
			//entity.RetailPrice = (Convert.IsDBNull(reader["RetailPrice"]))?0:(System.Decimal?)reader["RetailPrice"];
			entity.SalePrice = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.SalePrice)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.SalePrice)];
			//entity.SalePrice = (Convert.IsDBNull(reader["SalePrice"]))?0:(System.Decimal?)reader["SalePrice"];
			entity.WholesalePrice = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.WholesalePrice)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.WholesalePrice)];
			//entity.WholesalePrice = (Convert.IsDBNull(reader["WholesalePrice"]))?0:(System.Decimal?)reader["WholesalePrice"];
			entity.ImageFile = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ImageFile)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ImageFile)];
			//entity.ImageFile = (Convert.IsDBNull(reader["ImageFile"]))?string.Empty:(System.String)reader["ImageFile"];
			entity.ImageAltTag = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ImageAltTag)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ImageAltTag)];
			//entity.ImageAltTag = (Convert.IsDBNull(reader["ImageAltTag"]))?string.Empty:(System.String)reader["ImageAltTag"];
			entity.Weight = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Weight)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Weight)];
			//entity.Weight = (Convert.IsDBNull(reader["Weight"]))?0.0m:(System.Decimal?)reader["Weight"];
			entity.Length = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Length)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Length)];
			//entity.Length = (Convert.IsDBNull(reader["Length"]))?0.0m:(System.Decimal?)reader["Length"];
			entity.Width = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Width)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Width)];
			//entity.Width = (Convert.IsDBNull(reader["Width"]))?0.0m:(System.Decimal?)reader["Width"];
			entity.Height = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Height)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Height)];
			//entity.Height = (Convert.IsDBNull(reader["Height"]))?0.0m:(System.Decimal?)reader["Height"];
			entity.BeginActiveDate = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.BeginActiveDate)))?null:(System.DateTime?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.BeginActiveDate)];
			//entity.BeginActiveDate = (Convert.IsDBNull(reader["BeginActiveDate"]))?DateTime.MinValue:(System.DateTime?)reader["BeginActiveDate"];
			entity.EndActiveDate = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.EndActiveDate)))?null:(System.DateTime?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.EndActiveDate)];
			//entity.EndActiveDate = (Convert.IsDBNull(reader["EndActiveDate"]))?DateTime.MinValue:(System.DateTime?)reader["EndActiveDate"];
			entity.DisplayOrder = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.DisplayOrder)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.DisplayOrder)];
			//entity.DisplayOrder = (Convert.IsDBNull(reader["DisplayOrder"]))?(int)0:(System.Int32?)reader["DisplayOrder"];
			entity.ActiveInd = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ActiveInd)];
			//entity.ActiveInd = (Convert.IsDBNull(reader["ActiveInd"]))?false:(System.Boolean)reader["ActiveInd"];
			entity.CallForPricing = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.CallForPricing)];
			//entity.CallForPricing = (Convert.IsDBNull(reader["CallForPricing"]))?false:(System.Boolean)reader["CallForPricing"];
			entity.HomepageSpecial = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.HomepageSpecial)];
			//entity.HomepageSpecial = (Convert.IsDBNull(reader["HomepageSpecial"]))?false:(System.Boolean)reader["HomepageSpecial"];
			entity.CategorySpecial = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.CategorySpecial)];
			//entity.CategorySpecial = (Convert.IsDBNull(reader["CategorySpecial"]))?false:(System.Boolean)reader["CategorySpecial"];
			entity.InventoryDisplay = (System.Byte)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.InventoryDisplay)];
			//entity.InventoryDisplay = (Convert.IsDBNull(reader["InventoryDisplay"]))?(byte)0:(System.Byte)reader["InventoryDisplay"];
			entity.Keywords = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Keywords)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Keywords)];
			//entity.Keywords = (Convert.IsDBNull(reader["Keywords"]))?string.Empty:(System.String)reader["Keywords"];
			entity.ManufacturerID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ManufacturerID)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ManufacturerID)];
			//entity.ManufacturerID = (Convert.IsDBNull(reader["ManufacturerID"]))?(int)0:(System.Int32?)reader["ManufacturerID"];
			entity.AdditionalInfoLink = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.AdditionalInfoLink)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.AdditionalInfoLink)];
			//entity.AdditionalInfoLink = (Convert.IsDBNull(reader["AdditionalInfoLink"]))?string.Empty:(System.String)reader["AdditionalInfoLink"];
			entity.AdditionalInfoLinkLabel = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.AdditionalInfoLinkLabel)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.AdditionalInfoLinkLabel)];
			//entity.AdditionalInfoLinkLabel = (Convert.IsDBNull(reader["AdditionalInfoLinkLabel"]))?string.Empty:(System.String)reader["AdditionalInfoLinkLabel"];
			entity.ShippingRuleTypeID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ShippingRuleTypeID)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ShippingRuleTypeID)];
			//entity.ShippingRuleTypeID = (Convert.IsDBNull(reader["ShippingRuleTypeID"]))?(int)0:(System.Int32?)reader["ShippingRuleTypeID"];
			entity.ShippingRate = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ShippingRate)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ShippingRate)];
			//entity.ShippingRate = (Convert.IsDBNull(reader["ShippingRate"]))?0:(System.Decimal?)reader["ShippingRate"];
			entity.SEOTitle = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.SEOTitle)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.SEOTitle)];
			//entity.SEOTitle = (Convert.IsDBNull(reader["SEOTitle"]))?string.Empty:(System.String)reader["SEOTitle"];
			entity.SEOKeywords = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.SEOKeywords)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.SEOKeywords)];
			//entity.SEOKeywords = (Convert.IsDBNull(reader["SEOKeywords"]))?string.Empty:(System.String)reader["SEOKeywords"];
			entity.SEODescription = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.SEODescription)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.SEODescription)];
			//entity.SEODescription = (Convert.IsDBNull(reader["SEODescription"]))?string.Empty:(System.String)reader["SEODescription"];
			entity.Custom1 = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Custom1)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Custom1)];
			//entity.Custom1 = (Convert.IsDBNull(reader["Custom1"]))?string.Empty:(System.String)reader["Custom1"];
			entity.Custom2 = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Custom2)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Custom2)];
			//entity.Custom2 = (Convert.IsDBNull(reader["Custom2"]))?string.Empty:(System.String)reader["Custom2"];
			entity.Custom3 = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Custom3)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Custom3)];
			//entity.Custom3 = (Convert.IsDBNull(reader["Custom3"]))?string.Empty:(System.String)reader["Custom3"];
			entity.ShipEachItemSeparately = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ShipEachItemSeparately)))?null:(System.Boolean?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ShipEachItemSeparately)];
			//entity.ShipEachItemSeparately = (Convert.IsDBNull(reader["ShipEachItemSeparately"]))?false:(System.Boolean?)reader["ShipEachItemSeparately"];
			entity.AllowBackOrder = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.AllowBackOrder)))?null:(System.Boolean?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.AllowBackOrder)];
			//entity.AllowBackOrder = (Convert.IsDBNull(reader["AllowBackOrder"]))?false:(System.Boolean?)reader["AllowBackOrder"];
			entity.BackOrderMsg = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.BackOrderMsg)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.BackOrderMsg)];
			//entity.BackOrderMsg = (Convert.IsDBNull(reader["BackOrderMsg"]))?string.Empty:(System.String)reader["BackOrderMsg"];
			entity.DropShipInd = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.DropShipInd)))?null:(System.Boolean?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.DropShipInd)];
			//entity.DropShipInd = (Convert.IsDBNull(reader["DropShipInd"]))?false:(System.Boolean?)reader["DropShipInd"];
			entity.DropShipEmailID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.DropShipEmailID)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.DropShipEmailID)];
			//entity.DropShipEmailID = (Convert.IsDBNull(reader["DropShipEmailID"]))?string.Empty:(System.String)reader["DropShipEmailID"];
			entity.Specifications = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.Specifications)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Specifications)];
			//entity.Specifications = (Convert.IsDBNull(reader["Specifications"]))?string.Empty:(System.String)reader["Specifications"];
			entity.AdditionalInformation = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.AdditionalInformation)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.AdditionalInformation)];
			//entity.AdditionalInformation = (Convert.IsDBNull(reader["AdditionalInformation"]))?string.Empty:(System.String)reader["AdditionalInformation"];
			entity.InStockMsg = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.InStockMsg)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.InStockMsg)];
			//entity.InStockMsg = (Convert.IsDBNull(reader["InStockMsg"]))?string.Empty:(System.String)reader["InStockMsg"];
			entity.OutOfStockMsg = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.OutOfStockMsg)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.OutOfStockMsg)];
			//entity.OutOfStockMsg = (Convert.IsDBNull(reader["OutOfStockMsg"]))?string.Empty:(System.String)reader["OutOfStockMsg"];
			entity.TrackInventoryInd = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.TrackInventoryInd)))?null:(System.Boolean?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.TrackInventoryInd)];
			//entity.TrackInventoryInd = (Convert.IsDBNull(reader["TrackInventoryInd"]))?false:(System.Boolean?)reader["TrackInventoryInd"];
			entity.DownloadLink = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.DownloadLink)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.DownloadLink)];
			//entity.DownloadLink = (Convert.IsDBNull(reader["DownloadLink"]))?string.Empty:(System.String)reader["DownloadLink"];
			entity.FreeShippingInd = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.FreeShippingInd)))?null:(System.Boolean?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.FreeShippingInd)];
			//entity.FreeShippingInd = (Convert.IsDBNull(reader["FreeShippingInd"]))?false:(System.Boolean?)reader["FreeShippingInd"];
			entity.NewProductInd = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.NewProductInd)))?null:(System.Boolean?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.NewProductInd)];
			//entity.NewProductInd = (Convert.IsDBNull(reader["NewProductInd"]))?false:(System.Boolean?)reader["NewProductInd"];
			entity.SEOURL = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.SEOURL)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.SEOURL)];
			//entity.SEOURL = (Convert.IsDBNull(reader["SEOURL"]))?string.Empty:(System.String)reader["SEOURL"];
			entity.MaxQty = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.MaxQty)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.MaxQty)];
			//entity.MaxQty = (Convert.IsDBNull(reader["MaxQty"]))?(int)0:(System.Int32?)reader["MaxQty"];
			entity.ShipSeparately = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ShipSeparately)];
			//entity.ShipSeparately = (Convert.IsDBNull(reader["ShipSeparately"]))?false:(System.Boolean)reader["ShipSeparately"];
			entity.FeaturedInd = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.FeaturedInd)];
			//entity.FeaturedInd = (Convert.IsDBNull(reader["FeaturedInd"]))?false:(System.Boolean)reader["FeaturedInd"];
			entity.WebServiceDownloadDte = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.WebServiceDownloadDte)))?null:(System.DateTime?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.WebServiceDownloadDte)];
			//entity.WebServiceDownloadDte = (Convert.IsDBNull(reader["WebServiceDownloadDte"]))?DateTime.MinValue:(System.DateTime?)reader["WebServiceDownloadDte"];
			entity.UpdateDte = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.UpdateDte)))?null:(System.DateTime?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.UpdateDte)];
			//entity.UpdateDte = (Convert.IsDBNull(reader["UpdateDte"]))?DateTime.MinValue:(System.DateTime?)reader["UpdateDte"];
			entity.SupplierID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.SupplierID)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.SupplierID)];
			//entity.SupplierID = (Convert.IsDBNull(reader["SupplierID"]))?(int)0:(System.Int32?)reader["SupplierID"];
			entity.RecurringBillingInd = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingInd)];
			//entity.RecurringBillingInd = (Convert.IsDBNull(reader["RecurringBillingInd"]))?false:(System.Boolean)reader["RecurringBillingInd"];
			entity.RecurringBillingInstallmentInd = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingInstallmentInd)];
			//entity.RecurringBillingInstallmentInd = (Convert.IsDBNull(reader["RecurringBillingInstallmentInd"]))?false:(System.Boolean)reader["RecurringBillingInstallmentInd"];
			entity.RecurringBillingPeriod = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingPeriod)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingPeriod)];
			//entity.RecurringBillingPeriod = (Convert.IsDBNull(reader["RecurringBillingPeriod"]))?string.Empty:(System.String)reader["RecurringBillingPeriod"];
			entity.RecurringBillingFrequency = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingFrequency)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingFrequency)];
			//entity.RecurringBillingFrequency = (Convert.IsDBNull(reader["RecurringBillingFrequency"]))?string.Empty:(System.String)reader["RecurringBillingFrequency"];
			entity.RecurringBillingTotalCycles = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingTotalCycles)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingTotalCycles)];
			//entity.RecurringBillingTotalCycles = (Convert.IsDBNull(reader["RecurringBillingTotalCycles"]))?(int)0:(System.Int32?)reader["RecurringBillingTotalCycles"];
			entity.RecurringBillingInitialAmount = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingInitialAmount)))?null:(System.Decimal?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.RecurringBillingInitialAmount)];
			//entity.RecurringBillingInitialAmount = (Convert.IsDBNull(reader["RecurringBillingInitialAmount"]))?0:(System.Decimal?)reader["RecurringBillingInitialAmount"];
			entity.TaxClassID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.TaxClassID)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.TaxClassID)];
			//entity.TaxClassID = (Convert.IsDBNull(reader["TaxClassID"]))?(int)0:(System.Int32?)reader["TaxClassID"];
			entity.MinQty = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.MinQty)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.MinQty)];
			//entity.MinQty = (Convert.IsDBNull(reader["MinQty"]))?(int)0:(System.Int32?)reader["MinQty"];
			entity.ReviewStateID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ReviewStateID)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ReviewStateID)];
			//entity.ReviewStateID = (Convert.IsDBNull(reader["ReviewStateID"]))?(int)0:(System.Int32?)reader["ReviewStateID"];
			entity.AffiliateUrl = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.AffiliateUrl)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.AffiliateUrl)];
			//entity.AffiliateUrl = (Convert.IsDBNull(reader["AffiliateUrl"]))?string.Empty:(System.String)reader["AffiliateUrl"];
			entity.IsShippable = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.IsShippable)))?null:(System.Boolean?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.IsShippable)];
			//entity.IsShippable = (Convert.IsDBNull(reader["IsShippable"]))?false:(System.Boolean?)reader["IsShippable"];
			entity.AccountID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.AccountID)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.AccountID)];
			//entity.AccountID = (Convert.IsDBNull(reader["AccountID"]))?(int)0:(System.Int32?)reader["AccountID"];
			entity.PortalID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.PortalID)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.PortalID)];
			//entity.PortalID = (Convert.IsDBNull(reader["PortalID"]))?(int)0:(System.Int32?)reader["PortalID"];
			entity.Franchisable = (System.Boolean)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.Franchisable)];
			//entity.Franchisable = (Convert.IsDBNull(reader["Franchisable"]))?false:(System.Boolean)reader["Franchisable"];
			entity.ExpirationPeriod = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ExpirationPeriod)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ExpirationPeriod)];
			//entity.ExpirationPeriod = (Convert.IsDBNull(reader["ExpirationPeriod"]))?(int)0:(System.Int32?)reader["ExpirationPeriod"];
			entity.ExpirationFrequency = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ExpirationFrequency)))?null:(System.Int32?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ExpirationFrequency)];
			//entity.ExpirationFrequency = (Convert.IsDBNull(reader["ExpirationFrequency"]))?(int)0:(System.Int32?)reader["ExpirationFrequency"];
			entity.CreateDate = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.CreateDate)))?null:(System.DateTime?)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.CreateDate)];
			//entity.CreateDate = (Convert.IsDBNull(reader["CreateDate"]))?DateTime.MinValue:(System.DateTime?)reader["CreateDate"];
			entity.ExternalID = (reader.IsDBNull(((int)VwZnodeProductsCategoriesPromotionsColumn.ExternalID)))?null:(System.String)reader[((int)VwZnodeProductsCategoriesPromotionsColumn.ExternalID)];
			//entity.ExternalID = (Convert.IsDBNull(reader["ExternalID"]))?string.Empty:(System.String)reader["ExternalID"];
			reader.Close();
	
			entity.AcceptChanges();
		}
		
		/*
		/// <summary>
		/// Refreshes the <see cref="VwZnodeProductsCategoriesPromotions"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="VwZnodeProductsCategoriesPromotions"/> object.</param>
		protected static void RefreshEntity(DataSet dataSet, VwZnodeProductsCategoriesPromotions entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PromotionID = (Convert.IsDBNull(dataRow["PromotionID"]))?(int)0:(System.Int32)dataRow["PromotionID"];
			entity.DiscountTypeID = (Convert.IsDBNull(dataRow["DiscountTypeID"]))?(int)0:(System.Int32)dataRow["DiscountTypeID"];
			entity.CategoryID = (Convert.IsDBNull(dataRow["CategoryID"]))?(int)0:(System.Int32)dataRow["CategoryID"];
			entity.ProductID = (Convert.IsDBNull(dataRow["ProductID"]))?(int)0:(System.Int32)dataRow["ProductID"];
			entity.Name = (Convert.IsDBNull(dataRow["Name"]))?string.Empty:(System.String)dataRow["Name"];
			entity.ShortDescription = (Convert.IsDBNull(dataRow["ShortDescription"]))?string.Empty:(System.String)dataRow["ShortDescription"];
			entity.Description = (Convert.IsDBNull(dataRow["Description"]))?string.Empty:(System.String)dataRow["Description"];
			entity.FeaturesDesc = (Convert.IsDBNull(dataRow["FeaturesDesc"]))?string.Empty:(System.String)dataRow["FeaturesDesc"];
			entity.ProductNum = (Convert.IsDBNull(dataRow["ProductNum"]))?string.Empty:(System.String)dataRow["ProductNum"];
			entity.ProductTypeID = (Convert.IsDBNull(dataRow["ProductTypeID"]))?(int)0:(System.Int32)dataRow["ProductTypeID"];
			entity.RetailPrice = (Convert.IsDBNull(dataRow["RetailPrice"]))?0:(System.Decimal?)dataRow["RetailPrice"];
			entity.SalePrice = (Convert.IsDBNull(dataRow["SalePrice"]))?0:(System.Decimal?)dataRow["SalePrice"];
			entity.WholesalePrice = (Convert.IsDBNull(dataRow["WholesalePrice"]))?0:(System.Decimal?)dataRow["WholesalePrice"];
			entity.ImageFile = (Convert.IsDBNull(dataRow["ImageFile"]))?string.Empty:(System.String)dataRow["ImageFile"];
			entity.ImageAltTag = (Convert.IsDBNull(dataRow["ImageAltTag"]))?string.Empty:(System.String)dataRow["ImageAltTag"];
			entity.Weight = (Convert.IsDBNull(dataRow["Weight"]))?0.0m:(System.Decimal?)dataRow["Weight"];
			entity.Length = (Convert.IsDBNull(dataRow["Length"]))?0.0m:(System.Decimal?)dataRow["Length"];
			entity.Width = (Convert.IsDBNull(dataRow["Width"]))?0.0m:(System.Decimal?)dataRow["Width"];
			entity.Height = (Convert.IsDBNull(dataRow["Height"]))?0.0m:(System.Decimal?)dataRow["Height"];
			entity.BeginActiveDate = (Convert.IsDBNull(dataRow["BeginActiveDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["BeginActiveDate"];
			entity.EndActiveDate = (Convert.IsDBNull(dataRow["EndActiveDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["EndActiveDate"];
			entity.DisplayOrder = (Convert.IsDBNull(dataRow["DisplayOrder"]))?(int)0:(System.Int32?)dataRow["DisplayOrder"];
			entity.ActiveInd = (Convert.IsDBNull(dataRow["ActiveInd"]))?false:(System.Boolean)dataRow["ActiveInd"];
			entity.CallForPricing = (Convert.IsDBNull(dataRow["CallForPricing"]))?false:(System.Boolean)dataRow["CallForPricing"];
			entity.HomepageSpecial = (Convert.IsDBNull(dataRow["HomepageSpecial"]))?false:(System.Boolean)dataRow["HomepageSpecial"];
			entity.CategorySpecial = (Convert.IsDBNull(dataRow["CategorySpecial"]))?false:(System.Boolean)dataRow["CategorySpecial"];
			entity.InventoryDisplay = (Convert.IsDBNull(dataRow["InventoryDisplay"]))?(byte)0:(System.Byte)dataRow["InventoryDisplay"];
			entity.Keywords = (Convert.IsDBNull(dataRow["Keywords"]))?string.Empty:(System.String)dataRow["Keywords"];
			entity.ManufacturerID = (Convert.IsDBNull(dataRow["ManufacturerID"]))?(int)0:(System.Int32?)dataRow["ManufacturerID"];
			entity.AdditionalInfoLink = (Convert.IsDBNull(dataRow["AdditionalInfoLink"]))?string.Empty:(System.String)dataRow["AdditionalInfoLink"];
			entity.AdditionalInfoLinkLabel = (Convert.IsDBNull(dataRow["AdditionalInfoLinkLabel"]))?string.Empty:(System.String)dataRow["AdditionalInfoLinkLabel"];
			entity.ShippingRuleTypeID = (Convert.IsDBNull(dataRow["ShippingRuleTypeID"]))?(int)0:(System.Int32?)dataRow["ShippingRuleTypeID"];
			entity.ShippingRate = (Convert.IsDBNull(dataRow["ShippingRate"]))?0:(System.Decimal?)dataRow["ShippingRate"];
			entity.SEOTitle = (Convert.IsDBNull(dataRow["SEOTitle"]))?string.Empty:(System.String)dataRow["SEOTitle"];
			entity.SEOKeywords = (Convert.IsDBNull(dataRow["SEOKeywords"]))?string.Empty:(System.String)dataRow["SEOKeywords"];
			entity.SEODescription = (Convert.IsDBNull(dataRow["SEODescription"]))?string.Empty:(System.String)dataRow["SEODescription"];
			entity.Custom1 = (Convert.IsDBNull(dataRow["Custom1"]))?string.Empty:(System.String)dataRow["Custom1"];
			entity.Custom2 = (Convert.IsDBNull(dataRow["Custom2"]))?string.Empty:(System.String)dataRow["Custom2"];
			entity.Custom3 = (Convert.IsDBNull(dataRow["Custom3"]))?string.Empty:(System.String)dataRow["Custom3"];
			entity.ShipEachItemSeparately = (Convert.IsDBNull(dataRow["ShipEachItemSeparately"]))?false:(System.Boolean?)dataRow["ShipEachItemSeparately"];
			entity.AllowBackOrder = (Convert.IsDBNull(dataRow["AllowBackOrder"]))?false:(System.Boolean?)dataRow["AllowBackOrder"];
			entity.BackOrderMsg = (Convert.IsDBNull(dataRow["BackOrderMsg"]))?string.Empty:(System.String)dataRow["BackOrderMsg"];
			entity.DropShipInd = (Convert.IsDBNull(dataRow["DropShipInd"]))?false:(System.Boolean?)dataRow["DropShipInd"];
			entity.DropShipEmailID = (Convert.IsDBNull(dataRow["DropShipEmailID"]))?string.Empty:(System.String)dataRow["DropShipEmailID"];
			entity.Specifications = (Convert.IsDBNull(dataRow["Specifications"]))?string.Empty:(System.String)dataRow["Specifications"];
			entity.AdditionalInformation = (Convert.IsDBNull(dataRow["AdditionalInformation"]))?string.Empty:(System.String)dataRow["AdditionalInformation"];
			entity.InStockMsg = (Convert.IsDBNull(dataRow["InStockMsg"]))?string.Empty:(System.String)dataRow["InStockMsg"];
			entity.OutOfStockMsg = (Convert.IsDBNull(dataRow["OutOfStockMsg"]))?string.Empty:(System.String)dataRow["OutOfStockMsg"];
			entity.TrackInventoryInd = (Convert.IsDBNull(dataRow["TrackInventoryInd"]))?false:(System.Boolean?)dataRow["TrackInventoryInd"];
			entity.DownloadLink = (Convert.IsDBNull(dataRow["DownloadLink"]))?string.Empty:(System.String)dataRow["DownloadLink"];
			entity.FreeShippingInd = (Convert.IsDBNull(dataRow["FreeShippingInd"]))?false:(System.Boolean?)dataRow["FreeShippingInd"];
			entity.NewProductInd = (Convert.IsDBNull(dataRow["NewProductInd"]))?false:(System.Boolean?)dataRow["NewProductInd"];
			entity.SEOURL = (Convert.IsDBNull(dataRow["SEOURL"]))?string.Empty:(System.String)dataRow["SEOURL"];
			entity.MaxQty = (Convert.IsDBNull(dataRow["MaxQty"]))?(int)0:(System.Int32?)dataRow["MaxQty"];
			entity.ShipSeparately = (Convert.IsDBNull(dataRow["ShipSeparately"]))?false:(System.Boolean)dataRow["ShipSeparately"];
			entity.FeaturedInd = (Convert.IsDBNull(dataRow["FeaturedInd"]))?false:(System.Boolean)dataRow["FeaturedInd"];
			entity.WebServiceDownloadDte = (Convert.IsDBNull(dataRow["WebServiceDownloadDte"]))?DateTime.MinValue:(System.DateTime?)dataRow["WebServiceDownloadDte"];
			entity.UpdateDte = (Convert.IsDBNull(dataRow["UpdateDte"]))?DateTime.MinValue:(System.DateTime?)dataRow["UpdateDte"];
			entity.SupplierID = (Convert.IsDBNull(dataRow["SupplierID"]))?(int)0:(System.Int32?)dataRow["SupplierID"];
			entity.RecurringBillingInd = (Convert.IsDBNull(dataRow["RecurringBillingInd"]))?false:(System.Boolean)dataRow["RecurringBillingInd"];
			entity.RecurringBillingInstallmentInd = (Convert.IsDBNull(dataRow["RecurringBillingInstallmentInd"]))?false:(System.Boolean)dataRow["RecurringBillingInstallmentInd"];
			entity.RecurringBillingPeriod = (Convert.IsDBNull(dataRow["RecurringBillingPeriod"]))?string.Empty:(System.String)dataRow["RecurringBillingPeriod"];
			entity.RecurringBillingFrequency = (Convert.IsDBNull(dataRow["RecurringBillingFrequency"]))?string.Empty:(System.String)dataRow["RecurringBillingFrequency"];
			entity.RecurringBillingTotalCycles = (Convert.IsDBNull(dataRow["RecurringBillingTotalCycles"]))?(int)0:(System.Int32?)dataRow["RecurringBillingTotalCycles"];
			entity.RecurringBillingInitialAmount = (Convert.IsDBNull(dataRow["RecurringBillingInitialAmount"]))?0:(System.Decimal?)dataRow["RecurringBillingInitialAmount"];
			entity.TaxClassID = (Convert.IsDBNull(dataRow["TaxClassID"]))?(int)0:(System.Int32?)dataRow["TaxClassID"];
			entity.MinQty = (Convert.IsDBNull(dataRow["MinQty"]))?(int)0:(System.Int32?)dataRow["MinQty"];
			entity.ReviewStateID = (Convert.IsDBNull(dataRow["ReviewStateID"]))?(int)0:(System.Int32?)dataRow["ReviewStateID"];
			entity.AffiliateUrl = (Convert.IsDBNull(dataRow["AffiliateUrl"]))?string.Empty:(System.String)dataRow["AffiliateUrl"];
			entity.IsShippable = (Convert.IsDBNull(dataRow["IsShippable"]))?false:(System.Boolean?)dataRow["IsShippable"];
			entity.AccountID = (Convert.IsDBNull(dataRow["AccountID"]))?(int)0:(System.Int32?)dataRow["AccountID"];
			entity.PortalID = (Convert.IsDBNull(dataRow["PortalID"]))?(int)0:(System.Int32?)dataRow["PortalID"];
			entity.Franchisable = (Convert.IsDBNull(dataRow["Franchisable"]))?false:(System.Boolean)dataRow["Franchisable"];
			entity.ExpirationPeriod = (Convert.IsDBNull(dataRow["ExpirationPeriod"]))?(int)0:(System.Int32?)dataRow["ExpirationPeriod"];
			entity.ExpirationFrequency = (Convert.IsDBNull(dataRow["ExpirationFrequency"]))?(int)0:(System.Int32?)dataRow["ExpirationFrequency"];
			entity.CreateDate = (Convert.IsDBNull(dataRow["CreateDate"]))?DateTime.MinValue:(System.DateTime?)dataRow["CreateDate"];
			entity.ExternalID = (Convert.IsDBNull(dataRow["ExternalID"]))?string.Empty:(System.String)dataRow["ExternalID"];
			entity.AcceptChanges();
		}
		*/
			
		#endregion Helper Functions
	}//end class

	#region VwZnodeProductsCategoriesPromotionsFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeProductsCategoriesPromotions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeProductsCategoriesPromotionsFilterBuilder : SqlFilterBuilder<VwZnodeProductsCategoriesPromotionsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesPromotionsFilterBuilder class.
		/// </summary>
		public VwZnodeProductsCategoriesPromotionsFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesPromotionsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeProductsCategoriesPromotionsFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesPromotionsFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeProductsCategoriesPromotionsFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeProductsCategoriesPromotionsFilterBuilder

	#region VwZnodeProductsCategoriesPromotionsParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeProductsCategoriesPromotions"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class VwZnodeProductsCategoriesPromotionsParameterBuilder : ParameterizedSqlFilterBuilder<VwZnodeProductsCategoriesPromotionsColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesPromotionsParameterBuilder class.
		/// </summary>
		public VwZnodeProductsCategoriesPromotionsParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesPromotionsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public VwZnodeProductsCategoriesPromotionsParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesPromotionsParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public VwZnodeProductsCategoriesPromotionsParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion VwZnodeProductsCategoriesPromotionsParameterBuilder
	
	#region VwZnodeProductsCategoriesPromotionsSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;EntityColumn&gt;"/> class
	/// that is used exclusively with a <see cref="VwZnodeProductsCategoriesPromotions"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class VwZnodeProductsCategoriesPromotionsSortBuilder : SqlSortBuilder<VwZnodeProductsCategoriesPromotionsColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the VwZnodeProductsCategoriesPromotionsSqlSortBuilder class.
		/// </summary>
		public VwZnodeProductsCategoriesPromotionsSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion VwZnodeProductsCategoriesPromotionsSortBuilder

} // end namespace
