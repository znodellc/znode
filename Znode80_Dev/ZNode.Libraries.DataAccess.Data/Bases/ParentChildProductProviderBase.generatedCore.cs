﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ParentChildProductProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ParentChildProductProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ParentChildProduct, ZNode.Libraries.DataAccess.Entities.ParentChildProductKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ParentChildProductKey key)
		{
			return Delete(transactionManager, key.ParentChildProductID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_parentChildProductID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _parentChildProductID)
		{
			return Delete(null, _parentChildProductID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentChildProductID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _parentChildProductID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeParentChildProduct_ZNodeProduct key.
		///		FK_ZNodeParentChildProduct_ZNodeProduct Description: 
		/// </summary>
		/// <param name="_parentProductID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ParentChildProduct objects.</returns>
		public TList<ParentChildProduct> GetByParentProductID(System.Int32? _parentProductID)
		{
			int count = -1;
			return GetByParentProductID(_parentProductID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeParentChildProduct_ZNodeProduct key.
		///		FK_ZNodeParentChildProduct_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentProductID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ParentChildProduct objects.</returns>
		/// <remarks></remarks>
		public TList<ParentChildProduct> GetByParentProductID(TransactionManager transactionManager, System.Int32? _parentProductID)
		{
			int count = -1;
			return GetByParentProductID(transactionManager, _parentProductID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeParentChildProduct_ZNodeProduct key.
		///		FK_ZNodeParentChildProduct_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ParentChildProduct objects.</returns>
		public TList<ParentChildProduct> GetByParentProductID(TransactionManager transactionManager, System.Int32? _parentProductID, int start, int pageLength)
		{
			int count = -1;
			return GetByParentProductID(transactionManager, _parentProductID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeParentChildProduct_ZNodeProduct key.
		///		fKZNodeParentChildProductZNodeProduct Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_parentProductID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ParentChildProduct objects.</returns>
		public TList<ParentChildProduct> GetByParentProductID(System.Int32? _parentProductID, int start, int pageLength)
		{
			int count =  -1;
			return GetByParentProductID(null, _parentProductID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeParentChildProduct_ZNodeProduct key.
		///		fKZNodeParentChildProductZNodeProduct Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_parentProductID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ParentChildProduct objects.</returns>
		public TList<ParentChildProduct> GetByParentProductID(System.Int32? _parentProductID, int start, int pageLength,out int count)
		{
			return GetByParentProductID(null, _parentProductID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeParentChildProduct_ZNodeProduct key.
		///		FK_ZNodeParentChildProduct_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ParentChildProduct objects.</returns>
		public abstract TList<ParentChildProduct> GetByParentProductID(TransactionManager transactionManager, System.Int32? _parentProductID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeParentChildProduct_ZNodeProduct1 key.
		///		FK_ZNodeParentChildProduct_ZNodeProduct1 Description: 
		/// </summary>
		/// <param name="_childProductID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ParentChildProduct objects.</returns>
		public TList<ParentChildProduct> GetByChildProductID(System.Int32? _childProductID)
		{
			int count = -1;
			return GetByChildProductID(_childProductID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeParentChildProduct_ZNodeProduct1 key.
		///		FK_ZNodeParentChildProduct_ZNodeProduct1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_childProductID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ParentChildProduct objects.</returns>
		/// <remarks></remarks>
		public TList<ParentChildProduct> GetByChildProductID(TransactionManager transactionManager, System.Int32? _childProductID)
		{
			int count = -1;
			return GetByChildProductID(transactionManager, _childProductID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeParentChildProduct_ZNodeProduct1 key.
		///		FK_ZNodeParentChildProduct_ZNodeProduct1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_childProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ParentChildProduct objects.</returns>
		public TList<ParentChildProduct> GetByChildProductID(TransactionManager transactionManager, System.Int32? _childProductID, int start, int pageLength)
		{
			int count = -1;
			return GetByChildProductID(transactionManager, _childProductID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeParentChildProduct_ZNodeProduct1 key.
		///		fKZNodeParentChildProductZNodeProduct1 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_childProductID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ParentChildProduct objects.</returns>
		public TList<ParentChildProduct> GetByChildProductID(System.Int32? _childProductID, int start, int pageLength)
		{
			int count =  -1;
			return GetByChildProductID(null, _childProductID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeParentChildProduct_ZNodeProduct1 key.
		///		fKZNodeParentChildProductZNodeProduct1 Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_childProductID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ParentChildProduct objects.</returns>
		public TList<ParentChildProduct> GetByChildProductID(System.Int32? _childProductID, int start, int pageLength,out int count)
		{
			return GetByChildProductID(null, _childProductID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeParentChildProduct_ZNodeProduct1 key.
		///		FK_ZNodeParentChildProduct_ZNodeProduct1 Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_childProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ParentChildProduct objects.</returns>
		public abstract TList<ParentChildProduct> GetByChildProductID(TransactionManager transactionManager, System.Int32? _childProductID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ParentChildProduct Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ParentChildProductKey key, int start, int pageLength)
		{
			return GetByParentChildProductID(transactionManager, key.ParentChildProductID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeParentChildProduct index.
		/// </summary>
		/// <param name="_parentProductID"></param>
		/// <param name="_childProductID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ParentChildProduct GetByParentProductIDChildProductID(System.Int32? _parentProductID, System.Int32? _childProductID)
		{
			int count = -1;
			return GetByParentProductIDChildProductID(null,_parentProductID, _childProductID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeParentChildProduct index.
		/// </summary>
		/// <param name="_parentProductID"></param>
		/// <param name="_childProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ParentChildProduct GetByParentProductIDChildProductID(System.Int32? _parentProductID, System.Int32? _childProductID, int start, int pageLength)
		{
			int count = -1;
			return GetByParentProductIDChildProductID(null, _parentProductID, _childProductID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeParentChildProduct index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentProductID"></param>
		/// <param name="_childProductID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ParentChildProduct GetByParentProductIDChildProductID(TransactionManager transactionManager, System.Int32? _parentProductID, System.Int32? _childProductID)
		{
			int count = -1;
			return GetByParentProductIDChildProductID(transactionManager, _parentProductID, _childProductID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeParentChildProduct index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentProductID"></param>
		/// <param name="_childProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ParentChildProduct GetByParentProductIDChildProductID(TransactionManager transactionManager, System.Int32? _parentProductID, System.Int32? _childProductID, int start, int pageLength)
		{
			int count = -1;
			return GetByParentProductIDChildProductID(transactionManager, _parentProductID, _childProductID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeParentChildProduct index.
		/// </summary>
		/// <param name="_parentProductID"></param>
		/// <param name="_childProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ParentChildProduct GetByParentProductIDChildProductID(System.Int32? _parentProductID, System.Int32? _childProductID, int start, int pageLength, out int count)
		{
			return GetByParentProductIDChildProductID(null, _parentProductID, _childProductID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeParentChildProduct index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentProductID"></param>
		/// <param name="_childProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ParentChildProduct GetByParentProductIDChildProductID(TransactionManager transactionManager, System.Int32? _parentProductID, System.Int32? _childProductID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeParentChildProduct index.
		/// </summary>
		/// <param name="_parentChildProductID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ParentChildProduct GetByParentChildProductID(System.Int32 _parentChildProductID)
		{
			int count = -1;
			return GetByParentChildProductID(null,_parentChildProductID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeParentChildProduct index.
		/// </summary>
		/// <param name="_parentChildProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ParentChildProduct GetByParentChildProductID(System.Int32 _parentChildProductID, int start, int pageLength)
		{
			int count = -1;
			return GetByParentChildProductID(null, _parentChildProductID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeParentChildProduct index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentChildProductID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ParentChildProduct GetByParentChildProductID(TransactionManager transactionManager, System.Int32 _parentChildProductID)
		{
			int count = -1;
			return GetByParentChildProductID(transactionManager, _parentChildProductID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeParentChildProduct index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentChildProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ParentChildProduct GetByParentChildProductID(TransactionManager transactionManager, System.Int32 _parentChildProductID, int start, int pageLength)
		{
			int count = -1;
			return GetByParentChildProductID(transactionManager, _parentChildProductID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeParentChildProduct index.
		/// </summary>
		/// <param name="_parentChildProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ParentChildProduct GetByParentChildProductID(System.Int32 _parentChildProductID, int start, int pageLength, out int count)
		{
			return GetByParentChildProductID(null, _parentChildProductID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeParentChildProduct index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_parentChildProductID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ParentChildProduct GetByParentChildProductID(TransactionManager transactionManager, System.Int32 _parentChildProductID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ParentChildProduct&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ParentChildProduct&gt;"/></returns>
		public static TList<ParentChildProduct> Fill(IDataReader reader, TList<ParentChildProduct> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ParentChildProduct c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ParentChildProduct")
					.Append("|").Append((System.Int32)reader[((int)ParentChildProductColumn.ParentChildProductID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ParentChildProduct>(
					key.ToString(), // EntityTrackingKey
					"ParentChildProduct",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ParentChildProduct();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ParentChildProductID = (System.Int32)reader[((int)ParentChildProductColumn.ParentChildProductID - 1)];
					c.ParentProductID = (reader.IsDBNull(((int)ParentChildProductColumn.ParentProductID - 1)))?null:(System.Int32?)reader[((int)ParentChildProductColumn.ParentProductID - 1)];
					c.ChildProductID = (reader.IsDBNull(((int)ParentChildProductColumn.ChildProductID - 1)))?null:(System.Int32?)reader[((int)ParentChildProductColumn.ChildProductID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ParentChildProduct entity)
		{
			if (!reader.Read()) return;
			
			entity.ParentChildProductID = (System.Int32)reader[((int)ParentChildProductColumn.ParentChildProductID - 1)];
			entity.ParentProductID = (reader.IsDBNull(((int)ParentChildProductColumn.ParentProductID - 1)))?null:(System.Int32?)reader[((int)ParentChildProductColumn.ParentProductID - 1)];
			entity.ChildProductID = (reader.IsDBNull(((int)ParentChildProductColumn.ChildProductID - 1)))?null:(System.Int32?)reader[((int)ParentChildProductColumn.ChildProductID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ParentChildProduct entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ParentChildProductID = (System.Int32)dataRow["ParentChildProductID"];
			entity.ParentProductID = Convert.IsDBNull(dataRow["ParentProductID"]) ? null : (System.Int32?)dataRow["ParentProductID"];
			entity.ChildProductID = Convert.IsDBNull(dataRow["ChildProductID"]) ? null : (System.Int32?)dataRow["ChildProductID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ParentChildProduct"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ParentChildProduct Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ParentChildProduct entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ParentProductIDSource	
			if (CanDeepLoad(entity, "Product|ParentProductIDSource", deepLoadType, innerList) 
				&& entity.ParentProductIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ParentProductID ?? (int)0);
				Product tmpEntity = EntityManager.LocateEntity<Product>(EntityLocator.ConstructKeyFromPkItems(typeof(Product), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ParentProductIDSource = tmpEntity;
				else
					entity.ParentProductIDSource = DataRepository.ProductProvider.GetByProductID(transactionManager, (entity.ParentProductID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ParentProductIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ParentProductIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductProvider.DeepLoad(transactionManager, entity.ParentProductIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ParentProductIDSource

			#region ChildProductIDSource	
			if (CanDeepLoad(entity, "Product|ChildProductIDSource", deepLoadType, innerList) 
				&& entity.ChildProductIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ChildProductID ?? (int)0);
				Product tmpEntity = EntityManager.LocateEntity<Product>(EntityLocator.ConstructKeyFromPkItems(typeof(Product), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ChildProductIDSource = tmpEntity;
				else
					entity.ChildProductIDSource = DataRepository.ProductProvider.GetByProductID(transactionManager, (entity.ChildProductID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ChildProductIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ChildProductIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductProvider.DeepLoad(transactionManager, entity.ChildProductIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ChildProductIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ParentChildProduct object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ParentChildProduct instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ParentChildProduct Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ParentChildProduct entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ParentProductIDSource
			if (CanDeepSave(entity, "Product|ParentProductIDSource", deepSaveType, innerList) 
				&& entity.ParentProductIDSource != null)
			{
				DataRepository.ProductProvider.Save(transactionManager, entity.ParentProductIDSource);
				entity.ParentProductID = entity.ParentProductIDSource.ProductID;
			}
			#endregion 
			
			#region ChildProductIDSource
			if (CanDeepSave(entity, "Product|ChildProductIDSource", deepSaveType, innerList) 
				&& entity.ChildProductIDSource != null)
			{
				DataRepository.ProductProvider.Save(transactionManager, entity.ChildProductIDSource);
				entity.ChildProductID = entity.ChildProductIDSource.ProductID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ParentChildProductChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ParentChildProduct</c>
	///</summary>
	public enum ParentChildProductChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Product</c> at ParentProductIDSource
		///</summary>
		[ChildEntityType(typeof(Product))]
		Product,
	}
	
	#endregion ParentChildProductChildEntityTypes
	
	#region ParentChildProductFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ParentChildProductColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ParentChildProduct"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ParentChildProductFilterBuilder : SqlFilterBuilder<ParentChildProductColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ParentChildProductFilterBuilder class.
		/// </summary>
		public ParentChildProductFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ParentChildProductFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ParentChildProductFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ParentChildProductFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ParentChildProductFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ParentChildProductFilterBuilder
	
	#region ParentChildProductParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ParentChildProductColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ParentChildProduct"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ParentChildProductParameterBuilder : ParameterizedSqlFilterBuilder<ParentChildProductColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ParentChildProductParameterBuilder class.
		/// </summary>
		public ParentChildProductParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ParentChildProductParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ParentChildProductParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ParentChildProductParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ParentChildProductParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ParentChildProductParameterBuilder
	
	#region ParentChildProductSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ParentChildProductColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ParentChildProduct"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ParentChildProductSortBuilder : SqlSortBuilder<ParentChildProductColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ParentChildProductSqlSortBuilder class.
		/// </summary>
		public ParentChildProductSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ParentChildProductSortBuilder
	
} // end namespace
