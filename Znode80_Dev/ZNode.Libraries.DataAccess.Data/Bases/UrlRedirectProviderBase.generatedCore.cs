﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="UrlRedirectProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class UrlRedirectProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.UrlRedirect, ZNode.Libraries.DataAccess.Entities.UrlRedirectKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.UrlRedirectKey key)
		{
			return Delete(transactionManager, key.UrlRedirectID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_urlRedirectID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _urlRedirectID)
		{
			return Delete(null, _urlRedirectID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_urlRedirectID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _urlRedirectID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.UrlRedirect Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.UrlRedirectKey key, int start, int pageLength)
		{
			return GetByUrlRedirectID(transactionManager, key.UrlRedirectID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeUrlRedirect index.
		/// </summary>
		/// <param name="_urlRedirectID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.UrlRedirect"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.UrlRedirect GetByUrlRedirectID(System.Int32 _urlRedirectID)
		{
			int count = -1;
			return GetByUrlRedirectID(null,_urlRedirectID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeUrlRedirect index.
		/// </summary>
		/// <param name="_urlRedirectID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.UrlRedirect"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.UrlRedirect GetByUrlRedirectID(System.Int32 _urlRedirectID, int start, int pageLength)
		{
			int count = -1;
			return GetByUrlRedirectID(null, _urlRedirectID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeUrlRedirect index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_urlRedirectID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.UrlRedirect"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.UrlRedirect GetByUrlRedirectID(TransactionManager transactionManager, System.Int32 _urlRedirectID)
		{
			int count = -1;
			return GetByUrlRedirectID(transactionManager, _urlRedirectID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeUrlRedirect index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_urlRedirectID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.UrlRedirect"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.UrlRedirect GetByUrlRedirectID(TransactionManager transactionManager, System.Int32 _urlRedirectID, int start, int pageLength)
		{
			int count = -1;
			return GetByUrlRedirectID(transactionManager, _urlRedirectID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeUrlRedirect index.
		/// </summary>
		/// <param name="_urlRedirectID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.UrlRedirect"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.UrlRedirect GetByUrlRedirectID(System.Int32 _urlRedirectID, int start, int pageLength, out int count)
		{
			return GetByUrlRedirectID(null, _urlRedirectID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeUrlRedirect index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_urlRedirectID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.UrlRedirect"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.UrlRedirect GetByUrlRedirectID(TransactionManager transactionManager, System.Int32 _urlRedirectID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;UrlRedirect&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;UrlRedirect&gt;"/></returns>
		public static TList<UrlRedirect> Fill(IDataReader reader, TList<UrlRedirect> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.UrlRedirect c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("UrlRedirect")
					.Append("|").Append((System.Int32)reader[((int)UrlRedirectColumn.UrlRedirectID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<UrlRedirect>(
					key.ToString(), // EntityTrackingKey
					"UrlRedirect",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.UrlRedirect();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.UrlRedirectID = (System.Int32)reader[((int)UrlRedirectColumn.UrlRedirectID - 1)];
					c.OldUrl = (System.String)reader[((int)UrlRedirectColumn.OldUrl - 1)];
					c.NewUrl = (System.String)reader[((int)UrlRedirectColumn.NewUrl - 1)];
					c.IsActive = (System.Boolean)reader[((int)UrlRedirectColumn.IsActive - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.UrlRedirect"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.UrlRedirect"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.UrlRedirect entity)
		{
			if (!reader.Read()) return;
			
			entity.UrlRedirectID = (System.Int32)reader[((int)UrlRedirectColumn.UrlRedirectID - 1)];
			entity.OldUrl = (System.String)reader[((int)UrlRedirectColumn.OldUrl - 1)];
			entity.NewUrl = (System.String)reader[((int)UrlRedirectColumn.NewUrl - 1)];
			entity.IsActive = (System.Boolean)reader[((int)UrlRedirectColumn.IsActive - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.UrlRedirect"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.UrlRedirect"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.UrlRedirect entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.UrlRedirectID = (System.Int32)dataRow["UrlRedirectID"];
			entity.OldUrl = (System.String)dataRow["OldUrl"];
			entity.NewUrl = (System.String)dataRow["NewUrl"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.UrlRedirect"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.UrlRedirect Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.UrlRedirect entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.UrlRedirect object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.UrlRedirect instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.UrlRedirect Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.UrlRedirect entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region UrlRedirectChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.UrlRedirect</c>
	///</summary>
	public enum UrlRedirectChildEntityTypes
	{
	}
	
	#endregion UrlRedirectChildEntityTypes
	
	#region UrlRedirectFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;UrlRedirectColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UrlRedirect"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UrlRedirectFilterBuilder : SqlFilterBuilder<UrlRedirectColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UrlRedirectFilterBuilder class.
		/// </summary>
		public UrlRedirectFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UrlRedirectFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UrlRedirectFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UrlRedirectFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UrlRedirectFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UrlRedirectFilterBuilder
	
	#region UrlRedirectParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;UrlRedirectColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UrlRedirect"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class UrlRedirectParameterBuilder : ParameterizedSqlFilterBuilder<UrlRedirectColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UrlRedirectParameterBuilder class.
		/// </summary>
		public UrlRedirectParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the UrlRedirectParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public UrlRedirectParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the UrlRedirectParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public UrlRedirectParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion UrlRedirectParameterBuilder
	
	#region UrlRedirectSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;UrlRedirectColumn&gt;"/> class
	/// that is used exclusively with a <see cref="UrlRedirect"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class UrlRedirectSortBuilder : SqlSortBuilder<UrlRedirectColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the UrlRedirectSqlSortBuilder class.
		/// </summary>
		public UrlRedirectSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion UrlRedirectSortBuilder
	
} // end namespace
