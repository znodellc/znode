﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SavedCartProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SavedCartProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.SavedCart, ZNode.Libraries.DataAccess.Entities.SavedCartKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SavedCartKey key)
		{
			return Delete(transactionManager, key.SavedCartID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_savedCartID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _savedCartID)
		{
			return Delete(null, _savedCartID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_savedCartID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _savedCartID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSavedCart_ZNodeCookieMapping key.
		///		FK_ZNodeSavedCart_ZNodeCookieMapping Description: 
		/// </summary>
		/// <param name="_cookieMappingID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SavedCart objects.</returns>
		public TList<SavedCart> GetByCookieMappingID(System.Int32 _cookieMappingID)
		{
			int count = -1;
			return GetByCookieMappingID(_cookieMappingID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSavedCart_ZNodeCookieMapping key.
		///		FK_ZNodeSavedCart_ZNodeCookieMapping Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cookieMappingID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SavedCart objects.</returns>
		/// <remarks></remarks>
		public TList<SavedCart> GetByCookieMappingID(TransactionManager transactionManager, System.Int32 _cookieMappingID)
		{
			int count = -1;
			return GetByCookieMappingID(transactionManager, _cookieMappingID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSavedCart_ZNodeCookieMapping key.
		///		FK_ZNodeSavedCart_ZNodeCookieMapping Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cookieMappingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SavedCart objects.</returns>
		public TList<SavedCart> GetByCookieMappingID(TransactionManager transactionManager, System.Int32 _cookieMappingID, int start, int pageLength)
		{
			int count = -1;
			return GetByCookieMappingID(transactionManager, _cookieMappingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSavedCart_ZNodeCookieMapping key.
		///		fKZNodeSavedCartZNodeCookieMapping Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cookieMappingID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SavedCart objects.</returns>
		public TList<SavedCart> GetByCookieMappingID(System.Int32 _cookieMappingID, int start, int pageLength)
		{
			int count =  -1;
			return GetByCookieMappingID(null, _cookieMappingID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSavedCart_ZNodeCookieMapping key.
		///		fKZNodeSavedCartZNodeCookieMapping Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_cookieMappingID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SavedCart objects.</returns>
		public TList<SavedCart> GetByCookieMappingID(System.Int32 _cookieMappingID, int start, int pageLength,out int count)
		{
			return GetByCookieMappingID(null, _cookieMappingID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSavedCart_ZNodeCookieMapping key.
		///		FK_ZNodeSavedCart_ZNodeCookieMapping Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_cookieMappingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SavedCart objects.</returns>
		public abstract TList<SavedCart> GetByCookieMappingID(TransactionManager transactionManager, System.Int32 _cookieMappingID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.SavedCart Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SavedCartKey key, int start, int pageLength)
		{
			return GetBySavedCartID(transactionManager, key.SavedCartID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeSavedCart index.
		/// </summary>
		/// <param name="_savedCartID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCart"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SavedCart GetBySavedCartID(System.Int32 _savedCartID)
		{
			int count = -1;
			return GetBySavedCartID(null,_savedCartID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSavedCart index.
		/// </summary>
		/// <param name="_savedCartID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCart"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SavedCart GetBySavedCartID(System.Int32 _savedCartID, int start, int pageLength)
		{
			int count = -1;
			return GetBySavedCartID(null, _savedCartID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSavedCart index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_savedCartID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCart"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SavedCart GetBySavedCartID(TransactionManager transactionManager, System.Int32 _savedCartID)
		{
			int count = -1;
			return GetBySavedCartID(transactionManager, _savedCartID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSavedCart index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_savedCartID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCart"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SavedCart GetBySavedCartID(TransactionManager transactionManager, System.Int32 _savedCartID, int start, int pageLength)
		{
			int count = -1;
			return GetBySavedCartID(transactionManager, _savedCartID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSavedCart index.
		/// </summary>
		/// <param name="_savedCartID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCart"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SavedCart GetBySavedCartID(System.Int32 _savedCartID, int start, int pageLength, out int count)
		{
			return GetBySavedCartID(null, _savedCartID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeSavedCart index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_savedCartID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCart"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.SavedCart GetBySavedCartID(TransactionManager transactionManager, System.Int32 _savedCartID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SavedCart&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SavedCart&gt;"/></returns>
		public static TList<SavedCart> Fill(IDataReader reader, TList<SavedCart> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.SavedCart c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SavedCart")
					.Append("|").Append((System.Int32)reader[((int)SavedCartColumn.SavedCartID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SavedCart>(
					key.ToString(), // EntityTrackingKey
					"SavedCart",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.SavedCart();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.SavedCartID = (System.Int32)reader[((int)SavedCartColumn.SavedCartID - 1)];
					c.CookieMappingID = (System.Int32)reader[((int)SavedCartColumn.CookieMappingID - 1)];
					c.CreatedDate = (System.DateTime)reader[((int)SavedCartColumn.CreatedDate - 1)];
					c.LastUpdatedDate = (System.DateTime)reader[((int)SavedCartColumn.LastUpdatedDate - 1)];
					c.SalesTax = (reader.IsDBNull(((int)SavedCartColumn.SalesTax - 1)))?null:(System.Decimal?)reader[((int)SavedCartColumn.SalesTax - 1)];
					c.RecurringSalesTax = (reader.IsDBNull(((int)SavedCartColumn.RecurringSalesTax - 1)))?null:(System.Decimal?)reader[((int)SavedCartColumn.RecurringSalesTax - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCart"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SavedCart"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.SavedCart entity)
		{
			if (!reader.Read()) return;
			
			entity.SavedCartID = (System.Int32)reader[((int)SavedCartColumn.SavedCartID - 1)];
			entity.CookieMappingID = (System.Int32)reader[((int)SavedCartColumn.CookieMappingID - 1)];
			entity.CreatedDate = (System.DateTime)reader[((int)SavedCartColumn.CreatedDate - 1)];
			entity.LastUpdatedDate = (System.DateTime)reader[((int)SavedCartColumn.LastUpdatedDate - 1)];
			entity.SalesTax = (reader.IsDBNull(((int)SavedCartColumn.SalesTax - 1)))?null:(System.Decimal?)reader[((int)SavedCartColumn.SalesTax - 1)];
			entity.RecurringSalesTax = (reader.IsDBNull(((int)SavedCartColumn.RecurringSalesTax - 1)))?null:(System.Decimal?)reader[((int)SavedCartColumn.RecurringSalesTax - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SavedCart"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SavedCart"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.SavedCart entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SavedCartID = (System.Int32)dataRow["SavedCartID"];
			entity.CookieMappingID = (System.Int32)dataRow["CookieMappingID"];
			entity.CreatedDate = (System.DateTime)dataRow["CreatedDate"];
			entity.LastUpdatedDate = (System.DateTime)dataRow["LastUpdatedDate"];
			entity.SalesTax = Convert.IsDBNull(dataRow["SalesTax"]) ? null : (System.Decimal?)dataRow["SalesTax"];
			entity.RecurringSalesTax = Convert.IsDBNull(dataRow["RecurringSalesTax"]) ? null : (System.Decimal?)dataRow["RecurringSalesTax"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SavedCart"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SavedCart Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SavedCart entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region CookieMappingIDSource	
			if (CanDeepLoad(entity, "CookieMapping|CookieMappingIDSource", deepLoadType, innerList) 
				&& entity.CookieMappingIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.CookieMappingID;
				CookieMapping tmpEntity = EntityManager.LocateEntity<CookieMapping>(EntityLocator.ConstructKeyFromPkItems(typeof(CookieMapping), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CookieMappingIDSource = tmpEntity;
				else
					entity.CookieMappingIDSource = DataRepository.CookieMappingProvider.GetByCookieMappingID(transactionManager, entity.CookieMappingID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CookieMappingIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CookieMappingIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CookieMappingProvider.DeepLoad(transactionManager, entity.CookieMappingIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CookieMappingIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetBySavedCartID methods when available
			
			#region SavedCartLineItemCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SavedCartLineItem>|SavedCartLineItemCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SavedCartLineItemCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SavedCartLineItemCollection = DataRepository.SavedCartLineItemProvider.GetBySavedCartID(transactionManager, entity.SavedCartID);

				if (deep && entity.SavedCartLineItemCollection.Count > 0)
				{
					deepHandles.Add("SavedCartLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SavedCartLineItem>) DataRepository.SavedCartLineItemProvider.DeepLoad,
						new object[] { transactionManager, entity.SavedCartLineItemCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.SavedCart object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.SavedCart instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SavedCart Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SavedCart entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region CookieMappingIDSource
			if (CanDeepSave(entity, "CookieMapping|CookieMappingIDSource", deepSaveType, innerList) 
				&& entity.CookieMappingIDSource != null)
			{
				DataRepository.CookieMappingProvider.Save(transactionManager, entity.CookieMappingIDSource);
				entity.CookieMappingID = entity.CookieMappingIDSource.CookieMappingID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<SavedCartLineItem>
				if (CanDeepSave(entity.SavedCartLineItemCollection, "List<SavedCartLineItem>|SavedCartLineItemCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SavedCartLineItem child in entity.SavedCartLineItemCollection)
					{
						if(child.SavedCartIDSource != null)
						{
							child.SavedCartID = child.SavedCartIDSource.SavedCartID;
						}
						else
						{
							child.SavedCartID = entity.SavedCartID;
						}

					}

					if (entity.SavedCartLineItemCollection.Count > 0 || entity.SavedCartLineItemCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SavedCartLineItemProvider.Save(transactionManager, entity.SavedCartLineItemCollection);
						
						deepHandles.Add("SavedCartLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SavedCartLineItem >) DataRepository.SavedCartLineItemProvider.DeepSave,
							new object[] { transactionManager, entity.SavedCartLineItemCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SavedCartChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.SavedCart</c>
	///</summary>
	public enum SavedCartChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>CookieMapping</c> at CookieMappingIDSource
		///</summary>
		[ChildEntityType(typeof(CookieMapping))]
		CookieMapping,
		///<summary>
		/// Collection of <c>SavedCart</c> as OneToMany for SavedCartLineItemCollection
		///</summary>
		[ChildEntityType(typeof(TList<SavedCartLineItem>))]
		SavedCartLineItemCollection,
	}
	
	#endregion SavedCartChildEntityTypes
	
	#region SavedCartFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SavedCartColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SavedCart"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SavedCartFilterBuilder : SqlFilterBuilder<SavedCartColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SavedCartFilterBuilder class.
		/// </summary>
		public SavedCartFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SavedCartFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SavedCartFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SavedCartFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SavedCartFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SavedCartFilterBuilder
	
	#region SavedCartParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SavedCartColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SavedCart"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SavedCartParameterBuilder : ParameterizedSqlFilterBuilder<SavedCartColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SavedCartParameterBuilder class.
		/// </summary>
		public SavedCartParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SavedCartParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SavedCartParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SavedCartParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SavedCartParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SavedCartParameterBuilder
	
	#region SavedCartSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SavedCartColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SavedCart"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SavedCartSortBuilder : SqlSortBuilder<SavedCartColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SavedCartSqlSortBuilder class.
		/// </summary>
		public SavedCartSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SavedCartSortBuilder
	
} // end namespace
