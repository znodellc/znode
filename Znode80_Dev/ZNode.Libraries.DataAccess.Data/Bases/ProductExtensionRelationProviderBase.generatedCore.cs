﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductExtensionRelationProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductExtensionRelationProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation, ZNode.Libraries.DataAccess.Entities.ProductExtensionRelationKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtensionRelationKey key)
		{
			return Delete(transactionManager, key.ProductExtensionRelationID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_productExtensionRelationID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _productExtensionRelationID)
		{
			return Delete(null, _productExtensionRelationID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionRelationID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _productExtensionRelationID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtensionRelation_ZNodeProductExtension key.
		///		FK_ZNodeProductExtensionRelation_ZNodeProductExtension Description: 
		/// </summary>
		/// <param name="_productExtensionId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation objects.</returns>
		public TList<ProductExtensionRelation> GetByProductExtensionId(System.Int32? _productExtensionId)
		{
			int count = -1;
			return GetByProductExtensionId(_productExtensionId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtensionRelation_ZNodeProductExtension key.
		///		FK_ZNodeProductExtensionRelation_ZNodeProductExtension Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation objects.</returns>
		/// <remarks></remarks>
		public TList<ProductExtensionRelation> GetByProductExtensionId(TransactionManager transactionManager, System.Int32? _productExtensionId)
		{
			int count = -1;
			return GetByProductExtensionId(transactionManager, _productExtensionId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtensionRelation_ZNodeProductExtension key.
		///		FK_ZNodeProductExtensionRelation_ZNodeProductExtension Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation objects.</returns>
		public TList<ProductExtensionRelation> GetByProductExtensionId(TransactionManager transactionManager, System.Int32? _productExtensionId, int start, int pageLength)
		{
			int count = -1;
			return GetByProductExtensionId(transactionManager, _productExtensionId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtensionRelation_ZNodeProductExtension key.
		///		fKZNodeProductExtensionRelationZNodeProductExtension Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productExtensionId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation objects.</returns>
		public TList<ProductExtensionRelation> GetByProductExtensionId(System.Int32? _productExtensionId, int start, int pageLength)
		{
			int count =  -1;
			return GetByProductExtensionId(null, _productExtensionId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtensionRelation_ZNodeProductExtension key.
		///		fKZNodeProductExtensionRelationZNodeProductExtension Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productExtensionId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation objects.</returns>
		public TList<ProductExtensionRelation> GetByProductExtensionId(System.Int32? _productExtensionId, int start, int pageLength,out int count)
		{
			return GetByProductExtensionId(null, _productExtensionId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtensionRelation_ZNodeProductExtension key.
		///		FK_ZNodeProductExtensionRelation_ZNodeProductExtension Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation objects.</returns>
		public abstract TList<ProductExtensionRelation> GetByProductExtensionId(TransactionManager transactionManager, System.Int32? _productExtensionId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtensionRelationKey key, int start, int pageLength)
		{
			return GetByProductExtensionRelationID(transactionManager, key.ProductExtensionRelationID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeProductExtensionRelation index.
		/// </summary>
		/// <param name="_productExtensionRelationID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation GetByProductExtensionRelationID(System.Int32 _productExtensionRelationID)
		{
			int count = -1;
			return GetByProductExtensionRelationID(null,_productExtensionRelationID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtensionRelation index.
		/// </summary>
		/// <param name="_productExtensionRelationID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation GetByProductExtensionRelationID(System.Int32 _productExtensionRelationID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductExtensionRelationID(null, _productExtensionRelationID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtensionRelation index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionRelationID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation GetByProductExtensionRelationID(TransactionManager transactionManager, System.Int32 _productExtensionRelationID)
		{
			int count = -1;
			return GetByProductExtensionRelationID(transactionManager, _productExtensionRelationID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtensionRelation index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionRelationID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation GetByProductExtensionRelationID(TransactionManager transactionManager, System.Int32 _productExtensionRelationID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductExtensionRelationID(transactionManager, _productExtensionRelationID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtensionRelation index.
		/// </summary>
		/// <param name="_productExtensionRelationID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation GetByProductExtensionRelationID(System.Int32 _productExtensionRelationID, int start, int pageLength, out int count)
		{
			return GetByProductExtensionRelationID(null, _productExtensionRelationID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtensionRelation index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionRelationID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation GetByProductExtensionRelationID(TransactionManager transactionManager, System.Int32 _productExtensionRelationID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ProductExtensionRelation&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ProductExtensionRelation&gt;"/></returns>
		public static TList<ProductExtensionRelation> Fill(IDataReader reader, TList<ProductExtensionRelation> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ProductExtensionRelation")
					.Append("|").Append((System.Int32)reader[((int)ProductExtensionRelationColumn.ProductExtensionRelationID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ProductExtensionRelation>(
					key.ToString(), // EntityTrackingKey
					"ProductExtensionRelation",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ProductExtensionRelationID = (System.Int32)reader[((int)ProductExtensionRelationColumn.ProductExtensionRelationID - 1)];
					c.ProductExtensionId = (reader.IsDBNull(((int)ProductExtensionRelationColumn.ProductExtensionId - 1)))?null:(System.Int32?)reader[((int)ProductExtensionRelationColumn.ProductExtensionId - 1)];
					c.RelatedProductExtensionId = (reader.IsDBNull(((int)ProductExtensionRelationColumn.RelatedProductExtensionId - 1)))?null:(System.Int32?)reader[((int)ProductExtensionRelationColumn.RelatedProductExtensionId - 1)];
					c.RelationType = (reader.IsDBNull(((int)ProductExtensionRelationColumn.RelationType - 1)))?null:(System.Int32?)reader[((int)ProductExtensionRelationColumn.RelationType - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation entity)
		{
			if (!reader.Read()) return;
			
			entity.ProductExtensionRelationID = (System.Int32)reader[((int)ProductExtensionRelationColumn.ProductExtensionRelationID - 1)];
			entity.ProductExtensionId = (reader.IsDBNull(((int)ProductExtensionRelationColumn.ProductExtensionId - 1)))?null:(System.Int32?)reader[((int)ProductExtensionRelationColumn.ProductExtensionId - 1)];
			entity.RelatedProductExtensionId = (reader.IsDBNull(((int)ProductExtensionRelationColumn.RelatedProductExtensionId - 1)))?null:(System.Int32?)reader[((int)ProductExtensionRelationColumn.RelatedProductExtensionId - 1)];
			entity.RelationType = (reader.IsDBNull(((int)ProductExtensionRelationColumn.RelationType - 1)))?null:(System.Int32?)reader[((int)ProductExtensionRelationColumn.RelationType - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProductExtensionRelationID = (System.Int32)dataRow["ProductExtensionRelationID"];
			entity.ProductExtensionId = Convert.IsDBNull(dataRow["ProductExtensionId"]) ? null : (System.Int32?)dataRow["ProductExtensionId"];
			entity.RelatedProductExtensionId = Convert.IsDBNull(dataRow["RelatedProductExtensionId"]) ? null : (System.Int32?)dataRow["RelatedProductExtensionId"];
			entity.RelationType = Convert.IsDBNull(dataRow["RelationType"]) ? null : (System.Int32?)dataRow["RelationType"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ProductExtensionIdSource	
			if (CanDeepLoad(entity, "ProductExtension|ProductExtensionIdSource", deepLoadType, innerList) 
				&& entity.ProductExtensionIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ProductExtensionId ?? (int)0);
				ProductExtension tmpEntity = EntityManager.LocateEntity<ProductExtension>(EntityLocator.ConstructKeyFromPkItems(typeof(ProductExtension), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductExtensionIdSource = tmpEntity;
				else
					entity.ProductExtensionIdSource = DataRepository.ProductExtensionProvider.GetByProductExtensionID(transactionManager, (entity.ProductExtensionId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductExtensionIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductExtensionIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductExtensionProvider.DeepLoad(transactionManager, entity.ProductExtensionIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductExtensionIdSource

			#region ProductExtensionRelationIDSource	
			if (CanDeepLoad(entity, "ProductExtensionRelation|ProductExtensionRelationIDSource", deepLoadType, innerList) 
				&& entity.ProductExtensionRelationIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProductExtensionRelationID;
				ProductExtensionRelation tmpEntity = EntityManager.LocateEntity<ProductExtensionRelation>(EntityLocator.ConstructKeyFromPkItems(typeof(ProductExtensionRelation), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductExtensionRelationIDSource = tmpEntity;
				else
					entity.ProductExtensionRelationIDSource = DataRepository.ProductExtensionRelationProvider.GetByProductExtensionRelationID(transactionManager, entity.ProductExtensionRelationID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductExtensionRelationIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductExtensionRelationIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductExtensionRelationProvider.DeepLoad(transactionManager, entity.ProductExtensionRelationIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductExtensionRelationIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByProductExtensionRelationID methods when available
			
			#region ProductExtensionRelation
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "ProductExtensionRelation|ProductExtensionRelation", deepLoadType, innerList))
			{
				entity.ProductExtensionRelation = DataRepository.ProductExtensionRelationProvider.GetByProductExtensionRelationID(transactionManager, entity.ProductExtensionRelationID);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductExtensionRelation' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.ProductExtensionRelation != null)
				{
					deepHandles.Add("ProductExtensionRelation",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< ProductExtensionRelation >) DataRepository.ProductExtensionRelationProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductExtensionRelation, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ProductExtensionIdSource
			if (CanDeepSave(entity, "ProductExtension|ProductExtensionIdSource", deepSaveType, innerList) 
				&& entity.ProductExtensionIdSource != null)
			{
				DataRepository.ProductExtensionProvider.Save(transactionManager, entity.ProductExtensionIdSource);
				entity.ProductExtensionId = entity.ProductExtensionIdSource.ProductExtensionID;
			}
			#endregion 
			
			#region ProductExtensionRelationIDSource
			if (CanDeepSave(entity, "ProductExtensionRelation|ProductExtensionRelationIDSource", deepSaveType, innerList) 
				&& entity.ProductExtensionRelationIDSource != null)
			{
				DataRepository.ProductExtensionRelationProvider.Save(transactionManager, entity.ProductExtensionRelationIDSource);
				entity.ProductExtensionRelationID = entity.ProductExtensionRelationIDSource.ProductExtensionRelationID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

			#region ProductExtensionRelation
			if (CanDeepSave(entity.ProductExtensionRelation, "ProductExtensionRelation|ProductExtensionRelation", deepSaveType, innerList))
			{

				if (entity.ProductExtensionRelation != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.ProductExtensionRelation.ProductExtensionRelationID = entity.ProductExtensionRelationID;
					//DataRepository.ProductExtensionRelationProvider.Save(transactionManager, entity.ProductExtensionRelation);
					deepHandles.Add("ProductExtensionRelation",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< ProductExtensionRelation >) DataRepository.ProductExtensionRelationProvider.DeepSave,
						new object[] { transactionManager, entity.ProductExtensionRelation, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductExtensionRelationChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ProductExtensionRelation</c>
	///</summary>
	public enum ProductExtensionRelationChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>ProductExtension</c> at ProductExtensionIdSource
		///</summary>
		[ChildEntityType(typeof(ProductExtension))]
		ProductExtension,
		
		///<summary>
		/// Composite Property for <c>ProductExtensionRelation</c> at ProductExtensionRelationIDSource
		///</summary>
		[ChildEntityType(typeof(ProductExtensionRelation))]
		ProductExtensionRelation,
	}
	
	#endregion ProductExtensionRelationChildEntityTypes
	
	#region ProductExtensionRelationFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductExtensionRelationColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtensionRelation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionRelationFilterBuilder : SqlFilterBuilder<ProductExtensionRelationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionRelationFilterBuilder class.
		/// </summary>
		public ProductExtensionRelationFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionRelationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionRelationFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionRelationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionRelationFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionRelationFilterBuilder
	
	#region ProductExtensionRelationParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductExtensionRelationColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtensionRelation"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionRelationParameterBuilder : ParameterizedSqlFilterBuilder<ProductExtensionRelationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionRelationParameterBuilder class.
		/// </summary>
		public ProductExtensionRelationParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionRelationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionRelationParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionRelationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionRelationParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionRelationParameterBuilder
	
	#region ProductExtensionRelationSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductExtensionRelationColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtensionRelation"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductExtensionRelationSortBuilder : SqlSortBuilder<ProductExtensionRelationColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionRelationSqlSortBuilder class.
		/// </summary>
		public ProductExtensionRelationSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductExtensionRelationSortBuilder
	
} // end namespace
