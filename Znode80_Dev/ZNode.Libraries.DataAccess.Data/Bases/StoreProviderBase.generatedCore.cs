﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="StoreProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class StoreProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Store, ZNode.Libraries.DataAccess.Entities.StoreKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.StoreKey key)
		{
			return Delete(transactionManager, key.StoreID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_storeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _storeID)
		{
			return Delete(null, _storeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_storeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _storeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeStore_ZNodeAccount key.
		///		FK_ZNodeStore_ZNodeAccount Description: 
		/// </summary>
		/// <param name="_accountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Store objects.</returns>
		public TList<Store> GetByAccountID(System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(_accountID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeStore_ZNodeAccount key.
		///		FK_ZNodeStore_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Store objects.</returns>
		/// <remarks></remarks>
		public TList<Store> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeStore_ZNodeAccount key.
		///		FK_ZNodeStore_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Store objects.</returns>
		public TList<Store> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeStore_ZNodeAccount key.
		///		fKZNodeStoreZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_accountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Store objects.</returns>
		public TList<Store> GetByAccountID(System.Int32? _accountID, int start, int pageLength)
		{
			int count =  -1;
			return GetByAccountID(null, _accountID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeStore_ZNodeAccount key.
		///		fKZNodeStoreZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_accountID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Store objects.</returns>
		public TList<Store> GetByAccountID(System.Int32? _accountID, int start, int pageLength,out int count)
		{
			return GetByAccountID(null, _accountID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeStore_ZNodeAccount key.
		///		FK_ZNodeStore_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Store objects.</returns>
		public abstract TList<Store> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Store Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.StoreKey key, int start, int pageLength)
		{
			return GetByStoreID(transactionManager, key.StoreID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SC_Store index.
		/// </summary>
		/// <param name="_storeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Store"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Store GetByStoreID(System.Int32 _storeID)
		{
			int count = -1;
			return GetByStoreID(null,_storeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Store index.
		/// </summary>
		/// <param name="_storeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Store"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Store GetByStoreID(System.Int32 _storeID, int start, int pageLength)
		{
			int count = -1;
			return GetByStoreID(null, _storeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Store index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_storeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Store"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Store GetByStoreID(TransactionManager transactionManager, System.Int32 _storeID)
		{
			int count = -1;
			return GetByStoreID(transactionManager, _storeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Store index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_storeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Store"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Store GetByStoreID(TransactionManager transactionManager, System.Int32 _storeID, int start, int pageLength)
		{
			int count = -1;
			return GetByStoreID(transactionManager, _storeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Store index.
		/// </summary>
		/// <param name="_storeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Store"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Store GetByStoreID(System.Int32 _storeID, int start, int pageLength, out int count)
		{
			return GetByStoreID(null, _storeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_Store index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_storeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Store"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Store GetByStoreID(TransactionManager transactionManager, System.Int32 _storeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Store&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Store&gt;"/></returns>
		public static TList<Store> Fill(IDataReader reader, TList<Store> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Store c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Store")
					.Append("|").Append((System.Int32)reader[((int)StoreColumn.StoreID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Store>(
					key.ToString(), // EntityTrackingKey
					"Store",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Store();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.StoreID = (System.Int32)reader[((int)StoreColumn.StoreID - 1)];
					c.PortalID = (System.Int32)reader[((int)StoreColumn.PortalID - 1)];
					c.Name = (System.String)reader[((int)StoreColumn.Name - 1)];
					c.Address1 = (reader.IsDBNull(((int)StoreColumn.Address1 - 1)))?null:(System.String)reader[((int)StoreColumn.Address1 - 1)];
					c.Address2 = (reader.IsDBNull(((int)StoreColumn.Address2 - 1)))?null:(System.String)reader[((int)StoreColumn.Address2 - 1)];
					c.Address3 = (reader.IsDBNull(((int)StoreColumn.Address3 - 1)))?null:(System.String)reader[((int)StoreColumn.Address3 - 1)];
					c.City = (reader.IsDBNull(((int)StoreColumn.City - 1)))?null:(System.String)reader[((int)StoreColumn.City - 1)];
					c.State = (reader.IsDBNull(((int)StoreColumn.State - 1)))?null:(System.String)reader[((int)StoreColumn.State - 1)];
					c.Zip = (reader.IsDBNull(((int)StoreColumn.Zip - 1)))?null:(System.String)reader[((int)StoreColumn.Zip - 1)];
					c.Phone = (reader.IsDBNull(((int)StoreColumn.Phone - 1)))?null:(System.String)reader[((int)StoreColumn.Phone - 1)];
					c.Fax = (reader.IsDBNull(((int)StoreColumn.Fax - 1)))?null:(System.String)reader[((int)StoreColumn.Fax - 1)];
					c.ContactName = (reader.IsDBNull(((int)StoreColumn.ContactName - 1)))?null:(System.String)reader[((int)StoreColumn.ContactName - 1)];
					c.AccountID = (reader.IsDBNull(((int)StoreColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)StoreColumn.AccountID - 1)];
					c.DisplayOrder = (reader.IsDBNull(((int)StoreColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)StoreColumn.DisplayOrder - 1)];
					c.ImageFile = (reader.IsDBNull(((int)StoreColumn.ImageFile - 1)))?null:(System.String)reader[((int)StoreColumn.ImageFile - 1)];
					c.Custom1 = (reader.IsDBNull(((int)StoreColumn.Custom1 - 1)))?null:(System.String)reader[((int)StoreColumn.Custom1 - 1)];
					c.Custom2 = (reader.IsDBNull(((int)StoreColumn.Custom2 - 1)))?null:(System.String)reader[((int)StoreColumn.Custom2 - 1)];
					c.Custom3 = (reader.IsDBNull(((int)StoreColumn.Custom3 - 1)))?null:(System.String)reader[((int)StoreColumn.Custom3 - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)StoreColumn.ActiveInd - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Store"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Store"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Store entity)
		{
			if (!reader.Read()) return;
			
			entity.StoreID = (System.Int32)reader[((int)StoreColumn.StoreID - 1)];
			entity.PortalID = (System.Int32)reader[((int)StoreColumn.PortalID - 1)];
			entity.Name = (System.String)reader[((int)StoreColumn.Name - 1)];
			entity.Address1 = (reader.IsDBNull(((int)StoreColumn.Address1 - 1)))?null:(System.String)reader[((int)StoreColumn.Address1 - 1)];
			entity.Address2 = (reader.IsDBNull(((int)StoreColumn.Address2 - 1)))?null:(System.String)reader[((int)StoreColumn.Address2 - 1)];
			entity.Address3 = (reader.IsDBNull(((int)StoreColumn.Address3 - 1)))?null:(System.String)reader[((int)StoreColumn.Address3 - 1)];
			entity.City = (reader.IsDBNull(((int)StoreColumn.City - 1)))?null:(System.String)reader[((int)StoreColumn.City - 1)];
			entity.State = (reader.IsDBNull(((int)StoreColumn.State - 1)))?null:(System.String)reader[((int)StoreColumn.State - 1)];
			entity.Zip = (reader.IsDBNull(((int)StoreColumn.Zip - 1)))?null:(System.String)reader[((int)StoreColumn.Zip - 1)];
			entity.Phone = (reader.IsDBNull(((int)StoreColumn.Phone - 1)))?null:(System.String)reader[((int)StoreColumn.Phone - 1)];
			entity.Fax = (reader.IsDBNull(((int)StoreColumn.Fax - 1)))?null:(System.String)reader[((int)StoreColumn.Fax - 1)];
			entity.ContactName = (reader.IsDBNull(((int)StoreColumn.ContactName - 1)))?null:(System.String)reader[((int)StoreColumn.ContactName - 1)];
			entity.AccountID = (reader.IsDBNull(((int)StoreColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)StoreColumn.AccountID - 1)];
			entity.DisplayOrder = (reader.IsDBNull(((int)StoreColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)StoreColumn.DisplayOrder - 1)];
			entity.ImageFile = (reader.IsDBNull(((int)StoreColumn.ImageFile - 1)))?null:(System.String)reader[((int)StoreColumn.ImageFile - 1)];
			entity.Custom1 = (reader.IsDBNull(((int)StoreColumn.Custom1 - 1)))?null:(System.String)reader[((int)StoreColumn.Custom1 - 1)];
			entity.Custom2 = (reader.IsDBNull(((int)StoreColumn.Custom2 - 1)))?null:(System.String)reader[((int)StoreColumn.Custom2 - 1)];
			entity.Custom3 = (reader.IsDBNull(((int)StoreColumn.Custom3 - 1)))?null:(System.String)reader[((int)StoreColumn.Custom3 - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)StoreColumn.ActiveInd - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Store"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Store"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Store entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.StoreID = (System.Int32)dataRow["StoreID"];
			entity.PortalID = (System.Int32)dataRow["PortalID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.Address1 = Convert.IsDBNull(dataRow["Address1"]) ? null : (System.String)dataRow["Address1"];
			entity.Address2 = Convert.IsDBNull(dataRow["Address2"]) ? null : (System.String)dataRow["Address2"];
			entity.Address3 = Convert.IsDBNull(dataRow["Address3"]) ? null : (System.String)dataRow["Address3"];
			entity.City = Convert.IsDBNull(dataRow["City"]) ? null : (System.String)dataRow["City"];
			entity.State = Convert.IsDBNull(dataRow["State"]) ? null : (System.String)dataRow["State"];
			entity.Zip = Convert.IsDBNull(dataRow["Zip"]) ? null : (System.String)dataRow["Zip"];
			entity.Phone = Convert.IsDBNull(dataRow["Phone"]) ? null : (System.String)dataRow["Phone"];
			entity.Fax = Convert.IsDBNull(dataRow["Fax"]) ? null : (System.String)dataRow["Fax"];
			entity.ContactName = Convert.IsDBNull(dataRow["ContactName"]) ? null : (System.String)dataRow["ContactName"];
			entity.AccountID = Convert.IsDBNull(dataRow["AccountID"]) ? null : (System.Int32?)dataRow["AccountID"];
			entity.DisplayOrder = Convert.IsDBNull(dataRow["DisplayOrder"]) ? null : (System.Int32?)dataRow["DisplayOrder"];
			entity.ImageFile = Convert.IsDBNull(dataRow["ImageFile"]) ? null : (System.String)dataRow["ImageFile"];
			entity.Custom1 = Convert.IsDBNull(dataRow["Custom1"]) ? null : (System.String)dataRow["Custom1"];
			entity.Custom2 = Convert.IsDBNull(dataRow["Custom2"]) ? null : (System.String)dataRow["Custom2"];
			entity.Custom3 = Convert.IsDBNull(dataRow["Custom3"]) ? null : (System.String)dataRow["Custom3"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Store"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Store Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Store entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AccountIDSource	
			if (CanDeepLoad(entity, "Account|AccountIDSource", deepLoadType, innerList) 
				&& entity.AccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.AccountID ?? (int)0);
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AccountIDSource = tmpEntity;
				else
					entity.AccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, (entity.AccountID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.AccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AccountIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Store object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Store instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Store Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Store entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AccountIDSource
			if (CanDeepSave(entity, "Account|AccountIDSource", deepSaveType, innerList) 
				&& entity.AccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.AccountIDSource);
				entity.AccountID = entity.AccountIDSource.AccountID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region StoreChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Store</c>
	///</summary>
	public enum StoreChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Account</c> at AccountIDSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
	}
	
	#endregion StoreChildEntityTypes
	
	#region StoreFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;StoreColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Store"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StoreFilterBuilder : SqlFilterBuilder<StoreColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StoreFilterBuilder class.
		/// </summary>
		public StoreFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the StoreFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StoreFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StoreFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StoreFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StoreFilterBuilder
	
	#region StoreParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;StoreColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Store"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class StoreParameterBuilder : ParameterizedSqlFilterBuilder<StoreColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StoreParameterBuilder class.
		/// </summary>
		public StoreParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the StoreParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public StoreParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the StoreParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public StoreParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion StoreParameterBuilder
	
	#region StoreSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;StoreColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Store"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class StoreSortBuilder : SqlSortBuilder<StoreColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the StoreSqlSortBuilder class.
		/// </summary>
		public StoreSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion StoreSortBuilder
	
} // end namespace
