﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductReviewHistoryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductReviewHistoryProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ProductReviewHistory, ZNode.Libraries.DataAccess.Entities.ProductReviewHistoryKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductReviewHistoryKey key)
		{
			return Delete(transactionManager, key.ProductReviewHistoryID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_productReviewHistoryID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _productReviewHistoryID)
		{
			return Delete(null, _productReviewHistoryID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productReviewHistoryID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _productReviewHistoryID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductReviewHistory_ZNodeAccount key.
		///		FK_ZNodeProductReviewHistory_ZNodeAccount Description: 
		/// </summary>
		/// <param name="_vendorID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductReviewHistory objects.</returns>
		public TList<ProductReviewHistory> GetByVendorID(System.Int32 _vendorID)
		{
			int count = -1;
			return GetByVendorID(_vendorID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductReviewHistory_ZNodeAccount key.
		///		FK_ZNodeProductReviewHistory_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_vendorID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductReviewHistory objects.</returns>
		/// <remarks></remarks>
		public TList<ProductReviewHistory> GetByVendorID(TransactionManager transactionManager, System.Int32 _vendorID)
		{
			int count = -1;
			return GetByVendorID(transactionManager, _vendorID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductReviewHistory_ZNodeAccount key.
		///		FK_ZNodeProductReviewHistory_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_vendorID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductReviewHistory objects.</returns>
		public TList<ProductReviewHistory> GetByVendorID(TransactionManager transactionManager, System.Int32 _vendorID, int start, int pageLength)
		{
			int count = -1;
			return GetByVendorID(transactionManager, _vendorID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductReviewHistory_ZNodeAccount key.
		///		fKZNodeProductReviewHistoryZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_vendorID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductReviewHistory objects.</returns>
		public TList<ProductReviewHistory> GetByVendorID(System.Int32 _vendorID, int start, int pageLength)
		{
			int count =  -1;
			return GetByVendorID(null, _vendorID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductReviewHistory_ZNodeAccount key.
		///		fKZNodeProductReviewHistoryZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_vendorID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductReviewHistory objects.</returns>
		public TList<ProductReviewHistory> GetByVendorID(System.Int32 _vendorID, int start, int pageLength,out int count)
		{
			return GetByVendorID(null, _vendorID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductReviewHistory_ZNodeAccount key.
		///		FK_ZNodeProductReviewHistory_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_vendorID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductReviewHistory objects.</returns>
		public abstract TList<ProductReviewHistory> GetByVendorID(TransactionManager transactionManager, System.Int32 _vendorID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductReviewHistory_ZNodeProduct key.
		///		FK_ZNodeProductReviewHistory_ZNodeProduct Description: 
		/// </summary>
		/// <param name="_productID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductReviewHistory objects.</returns>
		public TList<ProductReviewHistory> GetByProductID(System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(_productID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductReviewHistory_ZNodeProduct key.
		///		FK_ZNodeProductReviewHistory_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductReviewHistory objects.</returns>
		/// <remarks></remarks>
		public TList<ProductReviewHistory> GetByProductID(TransactionManager transactionManager, System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductReviewHistory_ZNodeProduct key.
		///		FK_ZNodeProductReviewHistory_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductReviewHistory objects.</returns>
		public TList<ProductReviewHistory> GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductReviewHistory_ZNodeProduct key.
		///		fKZNodeProductReviewHistoryZNodeProduct Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductReviewHistory objects.</returns>
		public TList<ProductReviewHistory> GetByProductID(System.Int32 _productID, int start, int pageLength)
		{
			int count =  -1;
			return GetByProductID(null, _productID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductReviewHistory_ZNodeProduct key.
		///		fKZNodeProductReviewHistoryZNodeProduct Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductReviewHistory objects.</returns>
		public TList<ProductReviewHistory> GetByProductID(System.Int32 _productID, int start, int pageLength,out int count)
		{
			return GetByProductID(null, _productID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductReviewHistory_ZNodeProduct key.
		///		FK_ZNodeProductReviewHistory_ZNodeProduct Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductReviewHistory objects.</returns>
		public abstract TList<ProductReviewHistory> GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ProductReviewHistory Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductReviewHistoryKey key, int start, int pageLength)
		{
			return GetByProductReviewHistoryID(transactionManager, key.ProductReviewHistoryID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeProductReviewHistory index.
		/// </summary>
		/// <param name="_productReviewHistoryID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewHistory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductReviewHistory GetByProductReviewHistoryID(System.Int32 _productReviewHistoryID)
		{
			int count = -1;
			return GetByProductReviewHistoryID(null,_productReviewHistoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductReviewHistory index.
		/// </summary>
		/// <param name="_productReviewHistoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewHistory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductReviewHistory GetByProductReviewHistoryID(System.Int32 _productReviewHistoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductReviewHistoryID(null, _productReviewHistoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductReviewHistory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productReviewHistoryID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewHistory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductReviewHistory GetByProductReviewHistoryID(TransactionManager transactionManager, System.Int32 _productReviewHistoryID)
		{
			int count = -1;
			return GetByProductReviewHistoryID(transactionManager, _productReviewHistoryID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductReviewHistory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productReviewHistoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewHistory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductReviewHistory GetByProductReviewHistoryID(TransactionManager transactionManager, System.Int32 _productReviewHistoryID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductReviewHistoryID(transactionManager, _productReviewHistoryID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductReviewHistory index.
		/// </summary>
		/// <param name="_productReviewHistoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewHistory"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductReviewHistory GetByProductReviewHistoryID(System.Int32 _productReviewHistoryID, int start, int pageLength, out int count)
		{
			return GetByProductReviewHistoryID(null, _productReviewHistoryID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductReviewHistory index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productReviewHistoryID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewHistory"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductReviewHistory GetByProductReviewHistoryID(TransactionManager transactionManager, System.Int32 _productReviewHistoryID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ProductReviewHistory&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ProductReviewHistory&gt;"/></returns>
		public static TList<ProductReviewHistory> Fill(IDataReader reader, TList<ProductReviewHistory> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ProductReviewHistory c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ProductReviewHistory")
					.Append("|").Append((System.Int32)reader[((int)ProductReviewHistoryColumn.ProductReviewHistoryID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ProductReviewHistory>(
					key.ToString(), // EntityTrackingKey
					"ProductReviewHistory",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ProductReviewHistory();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ProductReviewHistoryID = (System.Int32)reader[((int)ProductReviewHistoryColumn.ProductReviewHistoryID - 1)];
					c.ProductID = (System.Int32)reader[((int)ProductReviewHistoryColumn.ProductID - 1)];
					c.VendorID = (System.Int32)reader[((int)ProductReviewHistoryColumn.VendorID - 1)];
					c.Status = (System.String)reader[((int)ProductReviewHistoryColumn.Status - 1)];
					c.Reason = (reader.IsDBNull(((int)ProductReviewHistoryColumn.Reason - 1)))?null:(System.String)reader[((int)ProductReviewHistoryColumn.Reason - 1)];
					c.Description = (reader.IsDBNull(((int)ProductReviewHistoryColumn.Description - 1)))?null:(System.String)reader[((int)ProductReviewHistoryColumn.Description - 1)];
					c.Username = (System.String)reader[((int)ProductReviewHistoryColumn.Username - 1)];
					c.LogDate = (System.DateTime)reader[((int)ProductReviewHistoryColumn.LogDate - 1)];
					c.NotificationDate = (reader.IsDBNull(((int)ProductReviewHistoryColumn.NotificationDate - 1)))?null:(System.DateTime?)reader[((int)ProductReviewHistoryColumn.NotificationDate - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewHistory"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewHistory"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ProductReviewHistory entity)
		{
			if (!reader.Read()) return;
			
			entity.ProductReviewHistoryID = (System.Int32)reader[((int)ProductReviewHistoryColumn.ProductReviewHistoryID - 1)];
			entity.ProductID = (System.Int32)reader[((int)ProductReviewHistoryColumn.ProductID - 1)];
			entity.VendorID = (System.Int32)reader[((int)ProductReviewHistoryColumn.VendorID - 1)];
			entity.Status = (System.String)reader[((int)ProductReviewHistoryColumn.Status - 1)];
			entity.Reason = (reader.IsDBNull(((int)ProductReviewHistoryColumn.Reason - 1)))?null:(System.String)reader[((int)ProductReviewHistoryColumn.Reason - 1)];
			entity.Description = (reader.IsDBNull(((int)ProductReviewHistoryColumn.Description - 1)))?null:(System.String)reader[((int)ProductReviewHistoryColumn.Description - 1)];
			entity.Username = (System.String)reader[((int)ProductReviewHistoryColumn.Username - 1)];
			entity.LogDate = (System.DateTime)reader[((int)ProductReviewHistoryColumn.LogDate - 1)];
			entity.NotificationDate = (reader.IsDBNull(((int)ProductReviewHistoryColumn.NotificationDate - 1)))?null:(System.DateTime?)reader[((int)ProductReviewHistoryColumn.NotificationDate - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewHistory"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewHistory"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ProductReviewHistory entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProductReviewHistoryID = (System.Int32)dataRow["ProductReviewHistoryID"];
			entity.ProductID = (System.Int32)dataRow["ProductID"];
			entity.VendorID = (System.Int32)dataRow["VendorID"];
			entity.Status = (System.String)dataRow["Status"];
			entity.Reason = Convert.IsDBNull(dataRow["Reason"]) ? null : (System.String)dataRow["Reason"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.Username = (System.String)dataRow["Username"];
			entity.LogDate = (System.DateTime)dataRow["LogDate"];
			entity.NotificationDate = Convert.IsDBNull(dataRow["NotificationDate"]) ? null : (System.DateTime?)dataRow["NotificationDate"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewHistory"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductReviewHistory Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductReviewHistory entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region VendorIDSource	
			if (CanDeepLoad(entity, "Account|VendorIDSource", deepLoadType, innerList) 
				&& entity.VendorIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.VendorID;
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.VendorIDSource = tmpEntity;
				else
					entity.VendorIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, entity.VendorID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'VendorIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.VendorIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.VendorIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion VendorIDSource

			#region ProductIDSource	
			if (CanDeepLoad(entity, "Product|ProductIDSource", deepLoadType, innerList) 
				&& entity.ProductIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProductID;
				Product tmpEntity = EntityManager.LocateEntity<Product>(EntityLocator.ConstructKeyFromPkItems(typeof(Product), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductIDSource = tmpEntity;
				else
					entity.ProductIDSource = DataRepository.ProductProvider.GetByProductID(transactionManager, entity.ProductID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductProvider.DeepLoad(transactionManager, entity.ProductIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ProductReviewHistory object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ProductReviewHistory instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductReviewHistory Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductReviewHistory entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region VendorIDSource
			if (CanDeepSave(entity, "Account|VendorIDSource", deepSaveType, innerList) 
				&& entity.VendorIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.VendorIDSource);
				entity.VendorID = entity.VendorIDSource.AccountID;
			}
			#endregion 
			
			#region ProductIDSource
			if (CanDeepSave(entity, "Product|ProductIDSource", deepSaveType, innerList) 
				&& entity.ProductIDSource != null)
			{
				DataRepository.ProductProvider.Save(transactionManager, entity.ProductIDSource);
				entity.ProductID = entity.ProductIDSource.ProductID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductReviewHistoryChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ProductReviewHistory</c>
	///</summary>
	public enum ProductReviewHistoryChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Account</c> at VendorIDSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
		
		///<summary>
		/// Composite Property for <c>Product</c> at ProductIDSource
		///</summary>
		[ChildEntityType(typeof(Product))]
		Product,
	}
	
	#endregion ProductReviewHistoryChildEntityTypes
	
	#region ProductReviewHistoryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductReviewHistoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductReviewHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductReviewHistoryFilterBuilder : SqlFilterBuilder<ProductReviewHistoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductReviewHistoryFilterBuilder class.
		/// </summary>
		public ProductReviewHistoryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewHistoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductReviewHistoryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewHistoryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductReviewHistoryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductReviewHistoryFilterBuilder
	
	#region ProductReviewHistoryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductReviewHistoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductReviewHistory"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductReviewHistoryParameterBuilder : ParameterizedSqlFilterBuilder<ProductReviewHistoryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductReviewHistoryParameterBuilder class.
		/// </summary>
		public ProductReviewHistoryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewHistoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductReviewHistoryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewHistoryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductReviewHistoryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductReviewHistoryParameterBuilder
	
	#region ProductReviewHistorySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductReviewHistoryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductReviewHistory"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductReviewHistorySortBuilder : SqlSortBuilder<ProductReviewHistoryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductReviewHistorySqlSortBuilder class.
		/// </summary>
		public ProductReviewHistorySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductReviewHistorySortBuilder
	
} // end namespace
