﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="PaymentTokenProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class PaymentTokenProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.PaymentToken, ZNode.Libraries.DataAccess.Entities.PaymentTokenKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PaymentTokenKey key)
		{
			return Delete(transactionManager, key.PaymentTokenID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_paymentTokenID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _paymentTokenID)
		{
			return Delete(null, _paymentTokenID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentTokenID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _paymentTokenID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentToken_ZNodeAccount key.
		///		FK_ZNodePaymentToken_ZNodeAccount Description: 
		/// </summary>
		/// <param name="_accountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentToken objects.</returns>
		public TList<PaymentToken> GetByAccountID(System.Int32 _accountID)
		{
			int count = -1;
			return GetByAccountID(_accountID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentToken_ZNodeAccount key.
		///		FK_ZNodePaymentToken_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentToken objects.</returns>
		/// <remarks></remarks>
		public TList<PaymentToken> GetByAccountID(TransactionManager transactionManager, System.Int32 _accountID)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentToken_ZNodeAccount key.
		///		FK_ZNodePaymentToken_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentToken objects.</returns>
		public TList<PaymentToken> GetByAccountID(TransactionManager transactionManager, System.Int32 _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentToken_ZNodeAccount key.
		///		fKZNodePaymentTokenZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_accountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentToken objects.</returns>
		public TList<PaymentToken> GetByAccountID(System.Int32 _accountID, int start, int pageLength)
		{
			int count =  -1;
			return GetByAccountID(null, _accountID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentToken_ZNodeAccount key.
		///		fKZNodePaymentTokenZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_accountID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentToken objects.</returns>
		public TList<PaymentToken> GetByAccountID(System.Int32 _accountID, int start, int pageLength,out int count)
		{
			return GetByAccountID(null, _accountID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentToken_ZNodeAccount key.
		///		FK_ZNodePaymentToken_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentToken objects.</returns>
		public abstract TList<PaymentToken> GetByAccountID(TransactionManager transactionManager, System.Int32 _accountID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentToken_ZNodePaymentSetting key.
		///		FK_ZNodePaymentToken_ZNodePaymentSetting Description: 
		/// </summary>
		/// <param name="_paymentSettingID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentToken objects.</returns>
		public TList<PaymentToken> GetByPaymentSettingID(System.Int32 _paymentSettingID)
		{
			int count = -1;
			return GetByPaymentSettingID(_paymentSettingID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentToken_ZNodePaymentSetting key.
		///		FK_ZNodePaymentToken_ZNodePaymentSetting Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentSettingID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentToken objects.</returns>
		/// <remarks></remarks>
		public TList<PaymentToken> GetByPaymentSettingID(TransactionManager transactionManager, System.Int32 _paymentSettingID)
		{
			int count = -1;
			return GetByPaymentSettingID(transactionManager, _paymentSettingID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentToken_ZNodePaymentSetting key.
		///		FK_ZNodePaymentToken_ZNodePaymentSetting Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentSettingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentToken objects.</returns>
		public TList<PaymentToken> GetByPaymentSettingID(TransactionManager transactionManager, System.Int32 _paymentSettingID, int start, int pageLength)
		{
			int count = -1;
			return GetByPaymentSettingID(transactionManager, _paymentSettingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentToken_ZNodePaymentSetting key.
		///		fKZNodePaymentTokenZNodePaymentSetting Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_paymentSettingID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentToken objects.</returns>
		public TList<PaymentToken> GetByPaymentSettingID(System.Int32 _paymentSettingID, int start, int pageLength)
		{
			int count =  -1;
			return GetByPaymentSettingID(null, _paymentSettingID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentToken_ZNodePaymentSetting key.
		///		fKZNodePaymentTokenZNodePaymentSetting Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_paymentSettingID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentToken objects.</returns>
		public TList<PaymentToken> GetByPaymentSettingID(System.Int32 _paymentSettingID, int start, int pageLength,out int count)
		{
			return GetByPaymentSettingID(null, _paymentSettingID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodePaymentToken_ZNodePaymentSetting key.
		///		FK_ZNodePaymentToken_ZNodePaymentSetting Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentSettingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.PaymentToken objects.</returns>
		public abstract TList<PaymentToken> GetByPaymentSettingID(TransactionManager transactionManager, System.Int32 _paymentSettingID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.PaymentToken Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PaymentTokenKey key, int start, int pageLength)
		{
			return GetByPaymentTokenID(transactionManager, key.PaymentTokenID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodePaymentToken index.
		/// </summary>
		/// <param name="_paymentTokenID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentToken"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PaymentToken GetByPaymentTokenID(System.Int32 _paymentTokenID)
		{
			int count = -1;
			return GetByPaymentTokenID(null,_paymentTokenID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePaymentToken index.
		/// </summary>
		/// <param name="_paymentTokenID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentToken"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PaymentToken GetByPaymentTokenID(System.Int32 _paymentTokenID, int start, int pageLength)
		{
			int count = -1;
			return GetByPaymentTokenID(null, _paymentTokenID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePaymentToken index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentTokenID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentToken"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PaymentToken GetByPaymentTokenID(TransactionManager transactionManager, System.Int32 _paymentTokenID)
		{
			int count = -1;
			return GetByPaymentTokenID(transactionManager, _paymentTokenID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePaymentToken index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentTokenID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentToken"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PaymentToken GetByPaymentTokenID(TransactionManager transactionManager, System.Int32 _paymentTokenID, int start, int pageLength)
		{
			int count = -1;
			return GetByPaymentTokenID(transactionManager, _paymentTokenID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePaymentToken index.
		/// </summary>
		/// <param name="_paymentTokenID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentToken"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.PaymentToken GetByPaymentTokenID(System.Int32 _paymentTokenID, int start, int pageLength, out int count)
		{
			return GetByPaymentTokenID(null, _paymentTokenID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodePaymentToken index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentTokenID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentToken"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.PaymentToken GetByPaymentTokenID(TransactionManager transactionManager, System.Int32 _paymentTokenID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;PaymentToken&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;PaymentToken&gt;"/></returns>
		public static TList<PaymentToken> Fill(IDataReader reader, TList<PaymentToken> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.PaymentToken c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("PaymentToken")
					.Append("|").Append((System.Int32)reader[((int)PaymentTokenColumn.PaymentTokenID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<PaymentToken>(
					key.ToString(), // EntityTrackingKey
					"PaymentToken",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.PaymentToken();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.PaymentTokenID = (System.Int32)reader[((int)PaymentTokenColumn.PaymentTokenID - 1)];
					c.AccountID = (System.Int32)reader[((int)PaymentTokenColumn.AccountID - 1)];
					c.PaymentSettingID = (System.Int32)reader[((int)PaymentTokenColumn.PaymentSettingID - 1)];
					c.CreditCardDescription = (reader.IsDBNull(((int)PaymentTokenColumn.CreditCardDescription - 1)))?null:(System.String)reader[((int)PaymentTokenColumn.CreditCardDescription - 1)];
					c.CardAuthCode = (reader.IsDBNull(((int)PaymentTokenColumn.CardAuthCode - 1)))?null:(System.String)reader[((int)PaymentTokenColumn.CardAuthCode - 1)];
					c.CardTransactionID = (reader.IsDBNull(((int)PaymentTokenColumn.CardTransactionID - 1)))?null:(System.String)reader[((int)PaymentTokenColumn.CardTransactionID - 1)];
					c.CardExp = (reader.IsDBNull(((int)PaymentTokenColumn.CardExp - 1)))?null:(System.DateTime?)reader[((int)PaymentTokenColumn.CardExp - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentToken"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PaymentToken"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.PaymentToken entity)
		{
			if (!reader.Read()) return;
			
			entity.PaymentTokenID = (System.Int32)reader[((int)PaymentTokenColumn.PaymentTokenID - 1)];
			entity.AccountID = (System.Int32)reader[((int)PaymentTokenColumn.AccountID - 1)];
			entity.PaymentSettingID = (System.Int32)reader[((int)PaymentTokenColumn.PaymentSettingID - 1)];
			entity.CreditCardDescription = (reader.IsDBNull(((int)PaymentTokenColumn.CreditCardDescription - 1)))?null:(System.String)reader[((int)PaymentTokenColumn.CreditCardDescription - 1)];
			entity.CardAuthCode = (reader.IsDBNull(((int)PaymentTokenColumn.CardAuthCode - 1)))?null:(System.String)reader[((int)PaymentTokenColumn.CardAuthCode - 1)];
			entity.CardTransactionID = (reader.IsDBNull(((int)PaymentTokenColumn.CardTransactionID - 1)))?null:(System.String)reader[((int)PaymentTokenColumn.CardTransactionID - 1)];
			entity.CardExp = (reader.IsDBNull(((int)PaymentTokenColumn.CardExp - 1)))?null:(System.DateTime?)reader[((int)PaymentTokenColumn.CardExp - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.PaymentToken"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PaymentToken"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.PaymentToken entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.PaymentTokenID = (System.Int32)dataRow["PaymentTokenID"];
			entity.AccountID = (System.Int32)dataRow["AccountID"];
			entity.PaymentSettingID = (System.Int32)dataRow["PaymentSettingID"];
			entity.CreditCardDescription = Convert.IsDBNull(dataRow["CreditCardDescription"]) ? null : (System.String)dataRow["CreditCardDescription"];
			entity.CardAuthCode = Convert.IsDBNull(dataRow["CardAuthCode"]) ? null : (System.String)dataRow["CardAuthCode"];
			entity.CardTransactionID = Convert.IsDBNull(dataRow["CardTransactionID"]) ? null : (System.String)dataRow["CardTransactionID"];
			entity.CardExp = Convert.IsDBNull(dataRow["CardExp"]) ? null : (System.DateTime?)dataRow["CardExp"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.PaymentToken"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PaymentToken Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PaymentToken entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AccountIDSource	
			if (CanDeepLoad(entity, "Account|AccountIDSource", deepLoadType, innerList) 
				&& entity.AccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.AccountID;
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AccountIDSource = tmpEntity;
				else
					entity.AccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, entity.AccountID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.AccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AccountIDSource

			#region PaymentSettingIDSource	
			if (CanDeepLoad(entity, "PaymentSetting|PaymentSettingIDSource", deepLoadType, innerList) 
				&& entity.PaymentSettingIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.PaymentSettingID;
				PaymentSetting tmpEntity = EntityManager.LocateEntity<PaymentSetting>(EntityLocator.ConstructKeyFromPkItems(typeof(PaymentSetting), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PaymentSettingIDSource = tmpEntity;
				else
					entity.PaymentSettingIDSource = DataRepository.PaymentSettingProvider.GetByPaymentSettingID(transactionManager, entity.PaymentSettingID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PaymentSettingIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PaymentSettingIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PaymentSettingProvider.DeepLoad(transactionManager, entity.PaymentSettingIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PaymentSettingIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByPaymentTokenID methods when available
			
			#region PaymentTokenAuthorizeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PaymentTokenAuthorize>|PaymentTokenAuthorizeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PaymentTokenAuthorizeCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PaymentTokenAuthorizeCollection = DataRepository.PaymentTokenAuthorizeProvider.GetByPaymentTokenID(transactionManager, entity.PaymentTokenID);

				if (deep && entity.PaymentTokenAuthorizeCollection.Count > 0)
				{
					deepHandles.Add("PaymentTokenAuthorizeCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PaymentTokenAuthorize>) DataRepository.PaymentTokenAuthorizeProvider.DeepLoad,
						new object[] { transactionManager, entity.PaymentTokenAuthorizeCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.PaymentToken object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.PaymentToken instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.PaymentToken Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.PaymentToken entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AccountIDSource
			if (CanDeepSave(entity, "Account|AccountIDSource", deepSaveType, innerList) 
				&& entity.AccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.AccountIDSource);
				entity.AccountID = entity.AccountIDSource.AccountID;
			}
			#endregion 
			
			#region PaymentSettingIDSource
			if (CanDeepSave(entity, "PaymentSetting|PaymentSettingIDSource", deepSaveType, innerList) 
				&& entity.PaymentSettingIDSource != null)
			{
				DataRepository.PaymentSettingProvider.Save(transactionManager, entity.PaymentSettingIDSource);
				entity.PaymentSettingID = entity.PaymentSettingIDSource.PaymentSettingID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<PaymentTokenAuthorize>
				if (CanDeepSave(entity.PaymentTokenAuthorizeCollection, "List<PaymentTokenAuthorize>|PaymentTokenAuthorizeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PaymentTokenAuthorize child in entity.PaymentTokenAuthorizeCollection)
					{
						if(child.PaymentTokenIDSource != null)
						{
							child.PaymentTokenID = child.PaymentTokenIDSource.PaymentTokenID;
						}
						else
						{
							child.PaymentTokenID = entity.PaymentTokenID;
						}

					}

					if (entity.PaymentTokenAuthorizeCollection.Count > 0 || entity.PaymentTokenAuthorizeCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PaymentTokenAuthorizeProvider.Save(transactionManager, entity.PaymentTokenAuthorizeCollection);
						
						deepHandles.Add("PaymentTokenAuthorizeCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PaymentTokenAuthorize >) DataRepository.PaymentTokenAuthorizeProvider.DeepSave,
							new object[] { transactionManager, entity.PaymentTokenAuthorizeCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region PaymentTokenChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.PaymentToken</c>
	///</summary>
	public enum PaymentTokenChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Account</c> at AccountIDSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
		
		///<summary>
		/// Composite Property for <c>PaymentSetting</c> at PaymentSettingIDSource
		///</summary>
		[ChildEntityType(typeof(PaymentSetting))]
		PaymentSetting,
		///<summary>
		/// Collection of <c>PaymentToken</c> as OneToMany for PaymentTokenAuthorizeCollection
		///</summary>
		[ChildEntityType(typeof(TList<PaymentTokenAuthorize>))]
		PaymentTokenAuthorizeCollection,
	}
	
	#endregion PaymentTokenChildEntityTypes
	
	#region PaymentTokenFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;PaymentTokenColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentToken"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentTokenFilterBuilder : SqlFilterBuilder<PaymentTokenColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentTokenFilterBuilder class.
		/// </summary>
		public PaymentTokenFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentTokenFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentTokenFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentTokenFilterBuilder
	
	#region PaymentTokenParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;PaymentTokenColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentToken"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class PaymentTokenParameterBuilder : ParameterizedSqlFilterBuilder<PaymentTokenColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentTokenParameterBuilder class.
		/// </summary>
		public PaymentTokenParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public PaymentTokenParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the PaymentTokenParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public PaymentTokenParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion PaymentTokenParameterBuilder
	
	#region PaymentTokenSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;PaymentTokenColumn&gt;"/> class
	/// that is used exclusively with a <see cref="PaymentToken"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class PaymentTokenSortBuilder : SqlSortBuilder<PaymentTokenColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the PaymentTokenSqlSortBuilder class.
		/// </summary>
		public PaymentTokenSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion PaymentTokenSortBuilder
	
} // end namespace
