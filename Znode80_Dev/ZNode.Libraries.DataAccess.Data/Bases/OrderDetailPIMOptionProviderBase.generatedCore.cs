﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="OrderDetailPIMOptionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class OrderDetailPIMOptionProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption, ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOptionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOptionKey key)
		{
			return Delete(transactionManager, key.OrderDetailPIMOptionID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_orderDetailPIMOptionID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _orderDetailPIMOptionID)
		{
			return Delete(null, _orderDetailPIMOptionID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderDetailPIMOptionID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _orderDetailPIMOptionID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOptionKey key, int start, int pageLength)
		{
			return GetByOrderDetailPIMOptionID(transactionManager, key.OrderDetailPIMOptionID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeOrderDetailPIMOption index.
		/// </summary>
		/// <param name="_orderDetailPIMOptionID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption GetByOrderDetailPIMOptionID(System.Int32 _orderDetailPIMOptionID)
		{
			int count = -1;
			return GetByOrderDetailPIMOptionID(null,_orderDetailPIMOptionID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeOrderDetailPIMOption index.
		/// </summary>
		/// <param name="_orderDetailPIMOptionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption GetByOrderDetailPIMOptionID(System.Int32 _orderDetailPIMOptionID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderDetailPIMOptionID(null, _orderDetailPIMOptionID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeOrderDetailPIMOption index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderDetailPIMOptionID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption GetByOrderDetailPIMOptionID(TransactionManager transactionManager, System.Int32 _orderDetailPIMOptionID)
		{
			int count = -1;
			return GetByOrderDetailPIMOptionID(transactionManager, _orderDetailPIMOptionID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeOrderDetailPIMOption index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderDetailPIMOptionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption GetByOrderDetailPIMOptionID(TransactionManager transactionManager, System.Int32 _orderDetailPIMOptionID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderDetailPIMOptionID(transactionManager, _orderDetailPIMOptionID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeOrderDetailPIMOption index.
		/// </summary>
		/// <param name="_orderDetailPIMOptionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption GetByOrderDetailPIMOptionID(System.Int32 _orderDetailPIMOptionID, int start, int pageLength, out int count)
		{
			return GetByOrderDetailPIMOptionID(null, _orderDetailPIMOptionID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeOrderDetailPIMOption index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderDetailPIMOptionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption GetByOrderDetailPIMOptionID(TransactionManager transactionManager, System.Int32 _orderDetailPIMOptionID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;OrderDetailPIMOption&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;OrderDetailPIMOption&gt;"/></returns>
		public static TList<OrderDetailPIMOption> Fill(IDataReader reader, TList<OrderDetailPIMOption> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("OrderDetailPIMOption")
					.Append("|").Append((System.Int32)reader[((int)OrderDetailPIMOptionColumn.OrderDetailPIMOptionID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<OrderDetailPIMOption>(
					key.ToString(), // EntityTrackingKey
					"OrderDetailPIMOption",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.OrderDetailPIMOptionID = (System.Int32)reader[((int)OrderDetailPIMOptionColumn.OrderDetailPIMOptionID - 1)];
					c.OriginalOrderDetailPIMOptionID = c.OrderDetailPIMOptionID;
					c.OrderDetailLineItemID = (System.Int32)reader[((int)OrderDetailPIMOptionColumn.OrderDetailLineItemID - 1)];
					c.PIMOptionID = (System.Int32)reader[((int)OrderDetailPIMOptionColumn.PIMOptionID - 1)];
					c.OptionData = (reader.IsDBNull(((int)OrderDetailPIMOptionColumn.OptionData - 1)))?null:(System.String)reader[((int)OrderDetailPIMOptionColumn.OptionData - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption entity)
		{
			if (!reader.Read()) return;
			
			entity.OrderDetailPIMOptionID = (System.Int32)reader[((int)OrderDetailPIMOptionColumn.OrderDetailPIMOptionID - 1)];
			entity.OriginalOrderDetailPIMOptionID = (System.Int32)reader["OrderDetailPIMOptionID"];
			entity.OrderDetailLineItemID = (System.Int32)reader[((int)OrderDetailPIMOptionColumn.OrderDetailLineItemID - 1)];
			entity.PIMOptionID = (System.Int32)reader[((int)OrderDetailPIMOptionColumn.PIMOptionID - 1)];
			entity.OptionData = (reader.IsDBNull(((int)OrderDetailPIMOptionColumn.OptionData - 1)))?null:(System.String)reader[((int)OrderDetailPIMOptionColumn.OptionData - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.OrderDetailPIMOptionID = (System.Int32)dataRow["OrderDetailPIMOptionID"];
			entity.OriginalOrderDetailPIMOptionID = (System.Int32)dataRow["OrderDetailPIMOptionID"];
			entity.OrderDetailLineItemID = (System.Int32)dataRow["OrderDetailLineItemID"];
			entity.PIMOptionID = (System.Int32)dataRow["PIMOptionID"];
			entity.OptionData = Convert.IsDBNull(dataRow["OptionData"]) ? null : (System.String)dataRow["OptionData"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region OrderDetailPIMOptionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.OrderDetailPIMOption</c>
	///</summary>
	public enum OrderDetailPIMOptionChildEntityTypes
	{
	}
	
	#endregion OrderDetailPIMOptionChildEntityTypes
	
	#region OrderDetailPIMOptionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;OrderDetailPIMOptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderDetailPIMOption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderDetailPIMOptionFilterBuilder : SqlFilterBuilder<OrderDetailPIMOptionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderDetailPIMOptionFilterBuilder class.
		/// </summary>
		public OrderDetailPIMOptionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderDetailPIMOptionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderDetailPIMOptionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderDetailPIMOptionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderDetailPIMOptionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderDetailPIMOptionFilterBuilder
	
	#region OrderDetailPIMOptionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;OrderDetailPIMOptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderDetailPIMOption"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderDetailPIMOptionParameterBuilder : ParameterizedSqlFilterBuilder<OrderDetailPIMOptionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderDetailPIMOptionParameterBuilder class.
		/// </summary>
		public OrderDetailPIMOptionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderDetailPIMOptionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderDetailPIMOptionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderDetailPIMOptionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderDetailPIMOptionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderDetailPIMOptionParameterBuilder
	
	#region OrderDetailPIMOptionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;OrderDetailPIMOptionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderDetailPIMOption"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class OrderDetailPIMOptionSortBuilder : SqlSortBuilder<OrderDetailPIMOptionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderDetailPIMOptionSqlSortBuilder class.
		/// </summary>
		public OrderDetailPIMOptionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion OrderDetailPIMOptionSortBuilder
	
} // end namespace
