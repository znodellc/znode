﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="SKUProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class SKUProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.SKU, ZNode.Libraries.DataAccess.Entities.SKUKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKUKey key)
		{
			return Delete(transactionManager, key.SKUID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_sKUID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _sKUID)
		{
			return Delete(null, _sKUID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _sKUID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKU_ZNodeSKUInventory key.
		///		FK_ZNodeSKU_ZNodeSKUInventory Description: 
		/// </summary>
		/// <param name="_sKU"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKU objects.</returns>
		public TList<SKU> GetBySKU(System.String _sKU)
		{
			int count = -1;
			return GetBySKU(_sKU, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKU_ZNodeSKUInventory key.
		///		FK_ZNodeSKU_ZNodeSKUInventory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKU"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKU objects.</returns>
		/// <remarks></remarks>
		public TList<SKU> GetBySKU(TransactionManager transactionManager, System.String _sKU)
		{
			int count = -1;
			return GetBySKU(transactionManager, _sKU, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKU_ZNodeSKUInventory key.
		///		FK_ZNodeSKU_ZNodeSKUInventory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKU"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKU objects.</returns>
		public TList<SKU> GetBySKU(TransactionManager transactionManager, System.String _sKU, int start, int pageLength)
		{
			int count = -1;
			return GetBySKU(transactionManager, _sKU, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKU_ZNodeSKUInventory key.
		///		fKZNodeSKUZNodeSKUInventory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_sKU"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKU objects.</returns>
		public TList<SKU> GetBySKU(System.String _sKU, int start, int pageLength)
		{
			int count =  -1;
			return GetBySKU(null, _sKU, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKU_ZNodeSKUInventory key.
		///		fKZNodeSKUZNodeSKUInventory Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_sKU"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKU objects.</returns>
		public TList<SKU> GetBySKU(System.String _sKU, int start, int pageLength,out int count)
		{
			return GetBySKU(null, _sKU, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeSKU_ZNodeSKUInventory key.
		///		FK_ZNodeSKU_ZNodeSKUInventory Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKU"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.SKU objects.</returns>
		public abstract TList<SKU> GetBySKU(TransactionManager transactionManager, System.String _sKU, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.SKU Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKUKey key, int start, int pageLength)
		{
			return GetBySKUID(transactionManager, key.SKUID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeSKU_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByActiveInd(System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public abstract TList<SKU> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeSKU_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByExternalID(System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(null,_externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByExternalID(System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByExternalID(TransactionManager transactionManager, System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByExternalID(System.String _externalID, int start, int pageLength, out int count)
		{
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public abstract TList<SKU> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZnodeSku_ProductId index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByProductID(System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(null,_productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeSku_ProductId index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByProductID(System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(null, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeSku_ProductId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByProductID(TransactionManager transactionManager, System.Int32 _productID)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeSku_ProductId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductID(transactionManager, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeSku_ProductId index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByProductID(System.Int32 _productID, int start, int pageLength, out int count)
		{
			return GetByProductID(null, _productID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeSku_ProductId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public abstract TList<SKU> GetByProductID(TransactionManager transactionManager, System.Int32 _productID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZnodeSku_ProductId_ActiveInd index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByProductIDActiveInd(System.Int32 _productID, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByProductIDActiveInd(null,_productID, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeSku_ProductId_ActiveInd index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByProductIDActiveInd(System.Int32 _productID, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByProductIDActiveInd(null, _productID, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeSku_ProductId_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByProductIDActiveInd(TransactionManager transactionManager, System.Int32 _productID, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByProductIDActiveInd(transactionManager, _productID, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeSku_ProductId_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByProductIDActiveInd(TransactionManager transactionManager, System.Int32 _productID, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByProductIDActiveInd(transactionManager, _productID, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeSku_ProductId_ActiveInd index.
		/// </summary>
		/// <param name="_productID"></param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetByProductIDActiveInd(System.Int32 _productID, System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByProductIDActiveInd(null, _productID, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeSku_ProductId_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productID"></param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public abstract TList<SKU> GetByProductIDActiveInd(TransactionManager transactionManager, System.Int32 _productID, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeSKU_SKU_ProductID index.
		/// </summary>
		/// <param name="_sKU"></param>
		/// <param name="_productID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKU GetBySKUProductID(System.String _sKU, System.Int32 _productID)
		{
			int count = -1;
			return GetBySKUProductID(null,_sKU, _productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_SKU_ProductID index.
		/// </summary>
		/// <param name="_sKU"></param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKU GetBySKUProductID(System.String _sKU, System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetBySKUProductID(null, _sKU, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_SKU_ProductID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKU"></param>
		/// <param name="_productID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKU GetBySKUProductID(TransactionManager transactionManager, System.String _sKU, System.Int32 _productID)
		{
			int count = -1;
			return GetBySKUProductID(transactionManager, _sKU, _productID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_SKU_ProductID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKU"></param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKU GetBySKUProductID(TransactionManager transactionManager, System.String _sKU, System.Int32 _productID, int start, int pageLength)
		{
			int count = -1;
			return GetBySKUProductID(transactionManager, _sKU, _productID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_SKU_ProductID index.
		/// </summary>
		/// <param name="_sKU"></param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKU GetBySKUProductID(System.String _sKU, System.Int32 _productID, int start, int pageLength, out int count)
		{
			return GetBySKUProductID(null, _sKU, _productID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_SKU_ProductID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKU"></param>
		/// <param name="_productID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.SKU GetBySKUProductID(TransactionManager transactionManager, System.String _sKU, System.Int32 _productID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeSKU_SupplierID index.
		/// </summary>
		/// <param name="_supplierID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetBySupplierID(System.Int32? _supplierID)
		{
			int count = -1;
			return GetBySupplierID(null,_supplierID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_SupplierID index.
		/// </summary>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetBySupplierID(System.Int32? _supplierID, int start, int pageLength)
		{
			int count = -1;
			return GetBySupplierID(null, _supplierID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_SupplierID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetBySupplierID(TransactionManager transactionManager, System.Int32? _supplierID)
		{
			int count = -1;
			return GetBySupplierID(transactionManager, _supplierID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_SupplierID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetBySupplierID(TransactionManager transactionManager, System.Int32? _supplierID, int start, int pageLength)
		{
			int count = -1;
			return GetBySupplierID(transactionManager, _supplierID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_SupplierID index.
		/// </summary>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public TList<SKU> GetBySupplierID(System.Int32? _supplierID, int start, int pageLength, out int count)
		{
			return GetBySupplierID(null, _supplierID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeSKU_SupplierID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_supplierID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;SKU&gt;"/> class.</returns>
		public abstract TList<SKU> GetBySupplierID(TransactionManager transactionManager, System.Int32? _supplierID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key SC_SKU_PK index.
		/// </summary>
		/// <param name="_sKUID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKU GetBySKUID(System.Int32 _sKUID)
		{
			int count = -1;
			return GetBySKUID(null,_sKUID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_SKU_PK index.
		/// </summary>
		/// <param name="_sKUID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKU GetBySKUID(System.Int32 _sKUID, int start, int pageLength)
		{
			int count = -1;
			return GetBySKUID(null, _sKUID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_SKU_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKU GetBySKUID(TransactionManager transactionManager, System.Int32 _sKUID)
		{
			int count = -1;
			return GetBySKUID(transactionManager, _sKUID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_SKU_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKU GetBySKUID(TransactionManager transactionManager, System.Int32 _sKUID, int start, int pageLength)
		{
			int count = -1;
			return GetBySKUID(transactionManager, _sKUID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_SKU_PK index.
		/// </summary>
		/// <param name="_sKUID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.SKU GetBySKUID(System.Int32 _sKUID, int start, int pageLength, out int count)
		{
			return GetBySKUID(null, _sKUID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_SKU_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_sKUID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.SKU GetBySKUID(TransactionManager transactionManager, System.Int32 _sKUID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;SKU&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;SKU&gt;"/></returns>
		public static TList<SKU> Fill(IDataReader reader, TList<SKU> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.SKU c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("SKU")
					.Append("|").Append((System.Int32)reader[((int)SKUColumn.SKUID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<SKU>(
					key.ToString(), // EntityTrackingKey
					"SKU",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.SKU();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.SKUID = (System.Int32)reader[((int)SKUColumn.SKUID - 1)];
					c.ProductID = (System.Int32)reader[((int)SKUColumn.ProductID - 1)];
					c.SKU = (System.String)reader[((int)SKUColumn.SKU - 1)];
					c.SupplierID = (reader.IsDBNull(((int)SKUColumn.SupplierID - 1)))?null:(System.Int32?)reader[((int)SKUColumn.SupplierID - 1)];
					c.Note = (reader.IsDBNull(((int)SKUColumn.Note - 1)))?null:(System.String)reader[((int)SKUColumn.Note - 1)];
					c.WeightAdditional = (reader.IsDBNull(((int)SKUColumn.WeightAdditional - 1)))?null:(System.Decimal?)reader[((int)SKUColumn.WeightAdditional - 1)];
					c.SKUPicturePath = (reader.IsDBNull(((int)SKUColumn.SKUPicturePath - 1)))?null:(System.String)reader[((int)SKUColumn.SKUPicturePath - 1)];
					c.ImageAltTag = (reader.IsDBNull(((int)SKUColumn.ImageAltTag - 1)))?null:(System.String)reader[((int)SKUColumn.ImageAltTag - 1)];
					c.DisplayOrder = (reader.IsDBNull(((int)SKUColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)SKUColumn.DisplayOrder - 1)];
					c.RetailPriceOverride = (reader.IsDBNull(((int)SKUColumn.RetailPriceOverride - 1)))?null:(System.Decimal?)reader[((int)SKUColumn.RetailPriceOverride - 1)];
					c.SalePriceOverride = (reader.IsDBNull(((int)SKUColumn.SalePriceOverride - 1)))?null:(System.Decimal?)reader[((int)SKUColumn.SalePriceOverride - 1)];
					c.WholesalePriceOverride = (reader.IsDBNull(((int)SKUColumn.WholesalePriceOverride - 1)))?null:(System.Decimal?)reader[((int)SKUColumn.WholesalePriceOverride - 1)];
					c.RecurringBillingPeriod = (reader.IsDBNull(((int)SKUColumn.RecurringBillingPeriod - 1)))?null:(System.String)reader[((int)SKUColumn.RecurringBillingPeriod - 1)];
					c.RecurringBillingFrequency = (reader.IsDBNull(((int)SKUColumn.RecurringBillingFrequency - 1)))?null:(System.String)reader[((int)SKUColumn.RecurringBillingFrequency - 1)];
					c.RecurringBillingTotalCycles = (reader.IsDBNull(((int)SKUColumn.RecurringBillingTotalCycles - 1)))?null:(System.Int32?)reader[((int)SKUColumn.RecurringBillingTotalCycles - 1)];
					c.RecurringBillingInitialAmount = (reader.IsDBNull(((int)SKUColumn.RecurringBillingInitialAmount - 1)))?null:(System.Decimal?)reader[((int)SKUColumn.RecurringBillingInitialAmount - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)SKUColumn.ActiveInd - 1)];
					c.Custom1 = (reader.IsDBNull(((int)SKUColumn.Custom1 - 1)))?null:(System.String)reader[((int)SKUColumn.Custom1 - 1)];
					c.Custom2 = (reader.IsDBNull(((int)SKUColumn.Custom2 - 1)))?null:(System.String)reader[((int)SKUColumn.Custom2 - 1)];
					c.Custom3 = (reader.IsDBNull(((int)SKUColumn.Custom3 - 1)))?null:(System.String)reader[((int)SKUColumn.Custom3 - 1)];
					c.WebServiceDownloadDte = (reader.IsDBNull(((int)SKUColumn.WebServiceDownloadDte - 1)))?null:(System.DateTime?)reader[((int)SKUColumn.WebServiceDownloadDte - 1)];
					c.UpdateDte = (reader.IsDBNull(((int)SKUColumn.UpdateDte - 1)))?null:(System.DateTime?)reader[((int)SKUColumn.UpdateDte - 1)];
					c.ExternalID = (reader.IsDBNull(((int)SKUColumn.ExternalID - 1)))?null:(System.String)reader[((int)SKUColumn.ExternalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.SKU entity)
		{
			if (!reader.Read()) return;
			
			entity.SKUID = (System.Int32)reader[((int)SKUColumn.SKUID - 1)];
			entity.ProductID = (System.Int32)reader[((int)SKUColumn.ProductID - 1)];
			entity.SKU = (System.String)reader[((int)SKUColumn.SKU - 1)];
			entity.SupplierID = (reader.IsDBNull(((int)SKUColumn.SupplierID - 1)))?null:(System.Int32?)reader[((int)SKUColumn.SupplierID - 1)];
			entity.Note = (reader.IsDBNull(((int)SKUColumn.Note - 1)))?null:(System.String)reader[((int)SKUColumn.Note - 1)];
			entity.WeightAdditional = (reader.IsDBNull(((int)SKUColumn.WeightAdditional - 1)))?null:(System.Decimal?)reader[((int)SKUColumn.WeightAdditional - 1)];
			entity.SKUPicturePath = (reader.IsDBNull(((int)SKUColumn.SKUPicturePath - 1)))?null:(System.String)reader[((int)SKUColumn.SKUPicturePath - 1)];
			entity.ImageAltTag = (reader.IsDBNull(((int)SKUColumn.ImageAltTag - 1)))?null:(System.String)reader[((int)SKUColumn.ImageAltTag - 1)];
			entity.DisplayOrder = (reader.IsDBNull(((int)SKUColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)SKUColumn.DisplayOrder - 1)];
			entity.RetailPriceOverride = (reader.IsDBNull(((int)SKUColumn.RetailPriceOverride - 1)))?null:(System.Decimal?)reader[((int)SKUColumn.RetailPriceOverride - 1)];
			entity.SalePriceOverride = (reader.IsDBNull(((int)SKUColumn.SalePriceOverride - 1)))?null:(System.Decimal?)reader[((int)SKUColumn.SalePriceOverride - 1)];
			entity.WholesalePriceOverride = (reader.IsDBNull(((int)SKUColumn.WholesalePriceOverride - 1)))?null:(System.Decimal?)reader[((int)SKUColumn.WholesalePriceOverride - 1)];
			entity.RecurringBillingPeriod = (reader.IsDBNull(((int)SKUColumn.RecurringBillingPeriod - 1)))?null:(System.String)reader[((int)SKUColumn.RecurringBillingPeriod - 1)];
			entity.RecurringBillingFrequency = (reader.IsDBNull(((int)SKUColumn.RecurringBillingFrequency - 1)))?null:(System.String)reader[((int)SKUColumn.RecurringBillingFrequency - 1)];
			entity.RecurringBillingTotalCycles = (reader.IsDBNull(((int)SKUColumn.RecurringBillingTotalCycles - 1)))?null:(System.Int32?)reader[((int)SKUColumn.RecurringBillingTotalCycles - 1)];
			entity.RecurringBillingInitialAmount = (reader.IsDBNull(((int)SKUColumn.RecurringBillingInitialAmount - 1)))?null:(System.Decimal?)reader[((int)SKUColumn.RecurringBillingInitialAmount - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)SKUColumn.ActiveInd - 1)];
			entity.Custom1 = (reader.IsDBNull(((int)SKUColumn.Custom1 - 1)))?null:(System.String)reader[((int)SKUColumn.Custom1 - 1)];
			entity.Custom2 = (reader.IsDBNull(((int)SKUColumn.Custom2 - 1)))?null:(System.String)reader[((int)SKUColumn.Custom2 - 1)];
			entity.Custom3 = (reader.IsDBNull(((int)SKUColumn.Custom3 - 1)))?null:(System.String)reader[((int)SKUColumn.Custom3 - 1)];
			entity.WebServiceDownloadDte = (reader.IsDBNull(((int)SKUColumn.WebServiceDownloadDte - 1)))?null:(System.DateTime?)reader[((int)SKUColumn.WebServiceDownloadDte - 1)];
			entity.UpdateDte = (reader.IsDBNull(((int)SKUColumn.UpdateDte - 1)))?null:(System.DateTime?)reader[((int)SKUColumn.UpdateDte - 1)];
			entity.ExternalID = (reader.IsDBNull(((int)SKUColumn.ExternalID - 1)))?null:(System.String)reader[((int)SKUColumn.ExternalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.SKU entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.SKUID = (System.Int32)dataRow["SKUID"];
			entity.ProductID = (System.Int32)dataRow["ProductID"];
			entity.SKU = (System.String)dataRow["SKU"];
			entity.SupplierID = Convert.IsDBNull(dataRow["SupplierID"]) ? null : (System.Int32?)dataRow["SupplierID"];
			entity.Note = Convert.IsDBNull(dataRow["Note"]) ? null : (System.String)dataRow["Note"];
			entity.WeightAdditional = Convert.IsDBNull(dataRow["WeightAdditional"]) ? null : (System.Decimal?)dataRow["WeightAdditional"];
			entity.SKUPicturePath = Convert.IsDBNull(dataRow["SKUPicturePath"]) ? null : (System.String)dataRow["SKUPicturePath"];
			entity.ImageAltTag = Convert.IsDBNull(dataRow["ImageAltTag"]) ? null : (System.String)dataRow["ImageAltTag"];
			entity.DisplayOrder = Convert.IsDBNull(dataRow["DisplayOrder"]) ? null : (System.Int32?)dataRow["DisplayOrder"];
			entity.RetailPriceOverride = Convert.IsDBNull(dataRow["RetailPriceOverride"]) ? null : (System.Decimal?)dataRow["RetailPriceOverride"];
			entity.SalePriceOverride = Convert.IsDBNull(dataRow["SalePriceOverride"]) ? null : (System.Decimal?)dataRow["SalePriceOverride"];
			entity.WholesalePriceOverride = Convert.IsDBNull(dataRow["WholesalePriceOverride"]) ? null : (System.Decimal?)dataRow["WholesalePriceOverride"];
			entity.RecurringBillingPeriod = Convert.IsDBNull(dataRow["RecurringBillingPeriod"]) ? null : (System.String)dataRow["RecurringBillingPeriod"];
			entity.RecurringBillingFrequency = Convert.IsDBNull(dataRow["RecurringBillingFrequency"]) ? null : (System.String)dataRow["RecurringBillingFrequency"];
			entity.RecurringBillingTotalCycles = Convert.IsDBNull(dataRow["RecurringBillingTotalCycles"]) ? null : (System.Int32?)dataRow["RecurringBillingTotalCycles"];
			entity.RecurringBillingInitialAmount = Convert.IsDBNull(dataRow["RecurringBillingInitialAmount"]) ? null : (System.Decimal?)dataRow["RecurringBillingInitialAmount"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.Custom1 = Convert.IsDBNull(dataRow["Custom1"]) ? null : (System.String)dataRow["Custom1"];
			entity.Custom2 = Convert.IsDBNull(dataRow["Custom2"]) ? null : (System.String)dataRow["Custom2"];
			entity.Custom3 = Convert.IsDBNull(dataRow["Custom3"]) ? null : (System.String)dataRow["Custom3"];
			entity.WebServiceDownloadDte = Convert.IsDBNull(dataRow["WebServiceDownloadDte"]) ? null : (System.DateTime?)dataRow["WebServiceDownloadDte"];
			entity.UpdateDte = Convert.IsDBNull(dataRow["UpdateDte"]) ? null : (System.DateTime?)dataRow["UpdateDte"];
			entity.ExternalID = Convert.IsDBNull(dataRow["ExternalID"]) ? null : (System.String)dataRow["ExternalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.SKU"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SKU Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKU entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ProductIDSource	
			if (CanDeepLoad(entity, "Product|ProductIDSource", deepLoadType, innerList) 
				&& entity.ProductIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProductID;
				Product tmpEntity = EntityManager.LocateEntity<Product>(EntityLocator.ConstructKeyFromPkItems(typeof(Product), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductIDSource = tmpEntity;
				else
					entity.ProductIDSource = DataRepository.ProductProvider.GetByProductID(transactionManager, entity.ProductID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductProvider.DeepLoad(transactionManager, entity.ProductIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductIDSource

			#region SKUSource	
			if (CanDeepLoad(entity, "SKUInventory|SKUSource", deepLoadType, innerList) 
				&& entity.SKUSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.SKU;
				SKUInventory tmpEntity = EntityManager.LocateEntity<SKUInventory>(EntityLocator.ConstructKeyFromPkItems(typeof(SKUInventory), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SKUSource = tmpEntity;
				else
					entity.SKUSource = DataRepository.SKUInventoryProvider.GetBySKU(transactionManager, entity.SKU);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SKUSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SKUSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SKUInventoryProvider.DeepLoad(transactionManager, entity.SKUSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SKUSource

			#region SupplierIDSource	
			if (CanDeepLoad(entity, "Supplier|SupplierIDSource", deepLoadType, innerList) 
				&& entity.SupplierIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.SupplierID ?? (int)0);
				Supplier tmpEntity = EntityManager.LocateEntity<Supplier>(EntityLocator.ConstructKeyFromPkItems(typeof(Supplier), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.SupplierIDSource = tmpEntity;
				else
					entity.SupplierIDSource = DataRepository.SupplierProvider.GetBySupplierID(transactionManager, (entity.SupplierID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SupplierIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.SupplierIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.SupplierProvider.DeepLoad(transactionManager, entity.SupplierIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion SupplierIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetBySKUID methods when available
			
			#region SKUAttributeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SKUAttribute>|SKUAttributeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SKUAttributeCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SKUAttributeCollection = DataRepository.SKUAttributeProvider.GetBySKUID(transactionManager, entity.SKUID);

				if (deep && entity.SKUAttributeCollection.Count > 0)
				{
					deepHandles.Add("SKUAttributeCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SKUAttribute>) DataRepository.SKUAttributeProvider.DeepLoad,
						new object[] { transactionManager, entity.SKUAttributeCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SKUProfileEffectiveCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SKUProfileEffective>|SKUProfileEffectiveCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SKUProfileEffectiveCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SKUProfileEffectiveCollection = DataRepository.SKUProfileEffectiveProvider.GetBySkuId(transactionManager, entity.SKUID);

				if (deep && entity.SKUProfileEffectiveCollection.Count > 0)
				{
					deepHandles.Add("SKUProfileEffectiveCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SKUProfileEffective>) DataRepository.SKUProfileEffectiveProvider.DeepLoad,
						new object[] { transactionManager, entity.SKUProfileEffectiveCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region FacetProductSKUCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FacetProductSKU>|FacetProductSKUCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FacetProductSKUCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FacetProductSKUCollection = DataRepository.FacetProductSKUProvider.GetBySKUID(transactionManager, entity.SKUID);

				if (deep && entity.FacetProductSKUCollection.Count > 0)
				{
					deepHandles.Add("FacetProductSKUCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FacetProductSKU>) DataRepository.FacetProductSKUProvider.DeepLoad,
						new object[] { transactionManager, entity.FacetProductSKUCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region SKUProfileCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<SKUProfile>|SKUProfileCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'SKUProfileCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.SKUProfileCollection = DataRepository.SKUProfileProvider.GetBySKUID(transactionManager, entity.SKUID);

				if (deep && entity.SKUProfileCollection.Count > 0)
				{
					deepHandles.Add("SKUProfileCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<SKUProfile>) DataRepository.SKUProfileProvider.DeepLoad,
						new object[] { transactionManager, entity.SKUProfileCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PromotionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Promotion>|PromotionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PromotionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PromotionCollection = DataRepository.PromotionProvider.GetBySKUID(transactionManager, entity.SKUID);

				if (deep && entity.PromotionCollection.Count > 0)
				{
					deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Promotion>) DataRepository.PromotionProvider.DeepLoad,
						new object[] { transactionManager, entity.PromotionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.SKU object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.SKU instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.SKU Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.SKU entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ProductIDSource
			if (CanDeepSave(entity, "Product|ProductIDSource", deepSaveType, innerList) 
				&& entity.ProductIDSource != null)
			{
				DataRepository.ProductProvider.Save(transactionManager, entity.ProductIDSource);
				entity.ProductID = entity.ProductIDSource.ProductID;
			}
			#endregion 
			
			#region SKUSource
			if (CanDeepSave(entity, "SKUInventory|SKUSource", deepSaveType, innerList) 
				&& entity.SKUSource != null)
			{
				DataRepository.SKUInventoryProvider.Save(transactionManager, entity.SKUSource);
				entity.SKU = entity.SKUSource.SKU;
			}
			#endregion 
			
			#region SupplierIDSource
			if (CanDeepSave(entity, "Supplier|SupplierIDSource", deepSaveType, innerList) 
				&& entity.SupplierIDSource != null)
			{
				DataRepository.SupplierProvider.Save(transactionManager, entity.SupplierIDSource);
				entity.SupplierID = entity.SupplierIDSource.SupplierID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<SKUAttribute>
				if (CanDeepSave(entity.SKUAttributeCollection, "List<SKUAttribute>|SKUAttributeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SKUAttribute child in entity.SKUAttributeCollection)
					{
						if(child.SKUIDSource != null)
						{
							child.SKUID = child.SKUIDSource.SKUID;
						}
						else
						{
							child.SKUID = entity.SKUID;
						}

					}

					if (entity.SKUAttributeCollection.Count > 0 || entity.SKUAttributeCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SKUAttributeProvider.Save(transactionManager, entity.SKUAttributeCollection);
						
						deepHandles.Add("SKUAttributeCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SKUAttribute >) DataRepository.SKUAttributeProvider.DeepSave,
							new object[] { transactionManager, entity.SKUAttributeCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SKUProfileEffective>
				if (CanDeepSave(entity.SKUProfileEffectiveCollection, "List<SKUProfileEffective>|SKUProfileEffectiveCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SKUProfileEffective child in entity.SKUProfileEffectiveCollection)
					{
						if(child.SkuIdSource != null)
						{
							child.SkuId = child.SkuIdSource.SKUID;
						}
						else
						{
							child.SkuId = entity.SKUID;
						}

					}

					if (entity.SKUProfileEffectiveCollection.Count > 0 || entity.SKUProfileEffectiveCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SKUProfileEffectiveProvider.Save(transactionManager, entity.SKUProfileEffectiveCollection);
						
						deepHandles.Add("SKUProfileEffectiveCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SKUProfileEffective >) DataRepository.SKUProfileEffectiveProvider.DeepSave,
							new object[] { transactionManager, entity.SKUProfileEffectiveCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<FacetProductSKU>
				if (CanDeepSave(entity.FacetProductSKUCollection, "List<FacetProductSKU>|FacetProductSKUCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FacetProductSKU child in entity.FacetProductSKUCollection)
					{
						if(child.SKUIDSource != null)
						{
							child.SKUID = child.SKUIDSource.SKUID;
						}
						else
						{
							child.SKUID = entity.SKUID;
						}

					}

					if (entity.FacetProductSKUCollection.Count > 0 || entity.FacetProductSKUCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FacetProductSKUProvider.Save(transactionManager, entity.FacetProductSKUCollection);
						
						deepHandles.Add("FacetProductSKUCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FacetProductSKU >) DataRepository.FacetProductSKUProvider.DeepSave,
							new object[] { transactionManager, entity.FacetProductSKUCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<SKUProfile>
				if (CanDeepSave(entity.SKUProfileCollection, "List<SKUProfile>|SKUProfileCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(SKUProfile child in entity.SKUProfileCollection)
					{
						if(child.SKUIDSource != null)
						{
							child.SKUID = child.SKUIDSource.SKUID;
						}
						else
						{
							child.SKUID = entity.SKUID;
						}

					}

					if (entity.SKUProfileCollection.Count > 0 || entity.SKUProfileCollection.DeletedItems.Count > 0)
					{
						//DataRepository.SKUProfileProvider.Save(transactionManager, entity.SKUProfileCollection);
						
						deepHandles.Add("SKUProfileCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< SKUProfile >) DataRepository.SKUProfileProvider.DeepSave,
							new object[] { transactionManager, entity.SKUProfileCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Promotion>
				if (CanDeepSave(entity.PromotionCollection, "List<Promotion>|PromotionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Promotion child in entity.PromotionCollection)
					{
						if(child.SKUIDSource != null)
						{
							child.SKUID = child.SKUIDSource.SKUID;
						}
						else
						{
							child.SKUID = entity.SKUID;
						}

					}

					if (entity.PromotionCollection.Count > 0 || entity.PromotionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PromotionProvider.Save(transactionManager, entity.PromotionCollection);
						
						deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Promotion >) DataRepository.PromotionProvider.DeepSave,
							new object[] { transactionManager, entity.PromotionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region SKUChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.SKU</c>
	///</summary>
	public enum SKUChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Product</c> at ProductIDSource
		///</summary>
		[ChildEntityType(typeof(Product))]
		Product,
		
		///<summary>
		/// Composite Property for <c>SKUInventory</c> at SKUSource
		///</summary>
		[ChildEntityType(typeof(SKUInventory))]
		SKUInventory,
		
		///<summary>
		/// Composite Property for <c>Supplier</c> at SupplierIDSource
		///</summary>
		[ChildEntityType(typeof(Supplier))]
		Supplier,
		///<summary>
		/// Collection of <c>SKU</c> as OneToMany for SKUAttributeCollection
		///</summary>
		[ChildEntityType(typeof(TList<SKUAttribute>))]
		SKUAttributeCollection,
		///<summary>
		/// Collection of <c>SKU</c> as OneToMany for SKUProfileEffectiveCollection
		///</summary>
		[ChildEntityType(typeof(TList<SKUProfileEffective>))]
		SKUProfileEffectiveCollection,
		///<summary>
		/// Collection of <c>SKU</c> as OneToMany for FacetProductSKUCollection
		///</summary>
		[ChildEntityType(typeof(TList<FacetProductSKU>))]
		FacetProductSKUCollection,
		///<summary>
		/// Collection of <c>SKU</c> as OneToMany for SKUProfileCollection
		///</summary>
		[ChildEntityType(typeof(TList<SKUProfile>))]
		SKUProfileCollection,
		///<summary>
		/// Collection of <c>SKU</c> as OneToMany for PromotionCollection
		///</summary>
		[ChildEntityType(typeof(TList<Promotion>))]
		PromotionCollection,
	}
	
	#endregion SKUChildEntityTypes
	
	#region SKUFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;SKUColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKU"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUFilterBuilder : SqlFilterBuilder<SKUColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUFilterBuilder class.
		/// </summary>
		public SKUFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUFilterBuilder
	
	#region SKUParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;SKUColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKU"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class SKUParameterBuilder : ParameterizedSqlFilterBuilder<SKUColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUParameterBuilder class.
		/// </summary>
		public SKUParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the SKUParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public SKUParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the SKUParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public SKUParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion SKUParameterBuilder
	
	#region SKUSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;SKUColumn&gt;"/> class
	/// that is used exclusively with a <see cref="SKU"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class SKUSortBuilder : SqlSortBuilder<SKUColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the SKUSqlSortBuilder class.
		/// </summary>
		public SKUSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion SKUSortBuilder
	
} // end namespace
