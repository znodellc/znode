﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductReviewStateProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductReviewStateProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ProductReviewState, ZNode.Libraries.DataAccess.Entities.ProductReviewStateKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductReviewStateKey key)
		{
			return Delete(transactionManager, key.ReviewStateID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_reviewStateID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _reviewStateID)
		{
			return Delete(null, _reviewStateID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reviewStateID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _reviewStateID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ProductReviewState Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductReviewStateKey key, int start, int pageLength)
		{
			return GetByReviewStateID(transactionManager, key.ReviewStateID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeProductReviewState index.
		/// </summary>
		/// <param name="_reviewStateName"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductReviewState GetByReviewStateName(System.String _reviewStateName)
		{
			int count = -1;
			return GetByReviewStateName(null,_reviewStateName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductReviewState index.
		/// </summary>
		/// <param name="_reviewStateName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductReviewState GetByReviewStateName(System.String _reviewStateName, int start, int pageLength)
		{
			int count = -1;
			return GetByReviewStateName(null, _reviewStateName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductReviewState index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reviewStateName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductReviewState GetByReviewStateName(TransactionManager transactionManager, System.String _reviewStateName)
		{
			int count = -1;
			return GetByReviewStateName(transactionManager, _reviewStateName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductReviewState index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reviewStateName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductReviewState GetByReviewStateName(TransactionManager transactionManager, System.String _reviewStateName, int start, int pageLength)
		{
			int count = -1;
			return GetByReviewStateName(transactionManager, _reviewStateName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductReviewState index.
		/// </summary>
		/// <param name="_reviewStateName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductReviewState GetByReviewStateName(System.String _reviewStateName, int start, int pageLength, out int count)
		{
			return GetByReviewStateName(null, _reviewStateName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeProductReviewState index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reviewStateName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductReviewState GetByReviewStateName(TransactionManager transactionManager, System.String _reviewStateName, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeProductReviewState index.
		/// </summary>
		/// <param name="_reviewStateID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductReviewState GetByReviewStateID(System.Int32 _reviewStateID)
		{
			int count = -1;
			return GetByReviewStateID(null,_reviewStateID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductReviewState index.
		/// </summary>
		/// <param name="_reviewStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductReviewState GetByReviewStateID(System.Int32 _reviewStateID, int start, int pageLength)
		{
			int count = -1;
			return GetByReviewStateID(null, _reviewStateID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductReviewState index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reviewStateID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductReviewState GetByReviewStateID(TransactionManager transactionManager, System.Int32 _reviewStateID)
		{
			int count = -1;
			return GetByReviewStateID(transactionManager, _reviewStateID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductReviewState index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reviewStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductReviewState GetByReviewStateID(TransactionManager transactionManager, System.Int32 _reviewStateID, int start, int pageLength)
		{
			int count = -1;
			return GetByReviewStateID(transactionManager, _reviewStateID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductReviewState index.
		/// </summary>
		/// <param name="_reviewStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductReviewState GetByReviewStateID(System.Int32 _reviewStateID, int start, int pageLength, out int count)
		{
			return GetByReviewStateID(null, _reviewStateID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductReviewState index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_reviewStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductReviewState GetByReviewStateID(TransactionManager transactionManager, System.Int32 _reviewStateID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ProductReviewState&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ProductReviewState&gt;"/></returns>
		public static TList<ProductReviewState> Fill(IDataReader reader, TList<ProductReviewState> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ProductReviewState c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ProductReviewState")
					.Append("|").Append((System.Int32)reader[((int)ProductReviewStateColumn.ReviewStateID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ProductReviewState>(
					key.ToString(), // EntityTrackingKey
					"ProductReviewState",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ProductReviewState();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ReviewStateID = (System.Int32)reader[((int)ProductReviewStateColumn.ReviewStateID - 1)];
					c.OriginalReviewStateID = c.ReviewStateID;
					c.ReviewStateName = (System.String)reader[((int)ProductReviewStateColumn.ReviewStateName - 1)];
					c.Description = (reader.IsDBNull(((int)ProductReviewStateColumn.Description - 1)))?null:(System.String)reader[((int)ProductReviewStateColumn.Description - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ProductReviewState entity)
		{
			if (!reader.Read()) return;
			
			entity.ReviewStateID = (System.Int32)reader[((int)ProductReviewStateColumn.ReviewStateID - 1)];
			entity.OriginalReviewStateID = (System.Int32)reader["ReviewStateID"];
			entity.ReviewStateName = (System.String)reader[((int)ProductReviewStateColumn.ReviewStateName - 1)];
			entity.Description = (reader.IsDBNull(((int)ProductReviewStateColumn.Description - 1)))?null:(System.String)reader[((int)ProductReviewStateColumn.Description - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ProductReviewState entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ReviewStateID = (System.Int32)dataRow["ReviewStateID"];
			entity.OriginalReviewStateID = (System.Int32)dataRow["ReviewStateID"];
			entity.ReviewStateName = (System.String)dataRow["ReviewStateName"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductReviewState"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductReviewState Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductReviewState entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByReviewStateID methods when available
			
			#region PortalCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Portal>|PortalCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PortalCollection = DataRepository.PortalProvider.GetByDefaultProductReviewStateID(transactionManager, entity.ReviewStateID);

				if (deep && entity.PortalCollection.Count > 0)
				{
					deepHandles.Add("PortalCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Portal>) DataRepository.PortalProvider.DeepLoad,
						new object[] { transactionManager, entity.PortalCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Product>|ProductCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductCollection = DataRepository.ProductProvider.GetByReviewStateID(transactionManager, entity.ReviewStateID);

				if (deep && entity.ProductCollection.Count > 0)
				{
					deepHandles.Add("ProductCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Product>) DataRepository.ProductProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductImageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductImage>|ProductImageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductImageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductImageCollection = DataRepository.ProductImageProvider.GetByReviewStateID(transactionManager, entity.ReviewStateID);

				if (deep && entity.ProductImageCollection.Count > 0)
				{
					deepHandles.Add("ProductImageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductImage>) DataRepository.ProductImageProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductImageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ProductReviewState object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ProductReviewState instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductReviewState Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductReviewState entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Portal>
				if (CanDeepSave(entity.PortalCollection, "List<Portal>|PortalCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Portal child in entity.PortalCollection)
					{
						if(child.DefaultProductReviewStateIDSource != null)
						{
							child.DefaultProductReviewStateID = child.DefaultProductReviewStateIDSource.ReviewStateID;
						}
						else
						{
							child.DefaultProductReviewStateID = entity.ReviewStateID;
						}

					}

					if (entity.PortalCollection.Count > 0 || entity.PortalCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PortalProvider.Save(transactionManager, entity.PortalCollection);
						
						deepHandles.Add("PortalCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Portal >) DataRepository.PortalProvider.DeepSave,
							new object[] { transactionManager, entity.PortalCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Product>
				if (CanDeepSave(entity.ProductCollection, "List<Product>|ProductCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Product child in entity.ProductCollection)
					{
						if(child.ReviewStateIDSource != null)
						{
							child.ReviewStateID = child.ReviewStateIDSource.ReviewStateID;
						}
						else
						{
							child.ReviewStateID = entity.ReviewStateID;
						}

					}

					if (entity.ProductCollection.Count > 0 || entity.ProductCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductProvider.Save(transactionManager, entity.ProductCollection);
						
						deepHandles.Add("ProductCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Product >) DataRepository.ProductProvider.DeepSave,
							new object[] { transactionManager, entity.ProductCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ProductImage>
				if (CanDeepSave(entity.ProductImageCollection, "List<ProductImage>|ProductImageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductImage child in entity.ProductImageCollection)
					{
						if(child.ReviewStateIDSource != null)
						{
							child.ReviewStateID = child.ReviewStateIDSource.ReviewStateID;
						}
						else
						{
							child.ReviewStateID = entity.ReviewStateID;
						}

					}

					if (entity.ProductImageCollection.Count > 0 || entity.ProductImageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductImageProvider.Save(transactionManager, entity.ProductImageCollection);
						
						deepHandles.Add("ProductImageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductImage >) DataRepository.ProductImageProvider.DeepSave,
							new object[] { transactionManager, entity.ProductImageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductReviewStateChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ProductReviewState</c>
	///</summary>
	public enum ProductReviewStateChildEntityTypes
	{
		///<summary>
		/// Collection of <c>ProductReviewState</c> as OneToMany for PortalCollection
		///</summary>
		[ChildEntityType(typeof(TList<Portal>))]
		PortalCollection,
		///<summary>
		/// Collection of <c>ProductReviewState</c> as OneToMany for ProductCollection
		///</summary>
		[ChildEntityType(typeof(TList<Product>))]
		ProductCollection,
		///<summary>
		/// Collection of <c>ProductReviewState</c> as OneToMany for ProductImageCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductImage>))]
		ProductImageCollection,
	}
	
	#endregion ProductReviewStateChildEntityTypes
	
	#region ProductReviewStateFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductReviewStateColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductReviewState"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductReviewStateFilterBuilder : SqlFilterBuilder<ProductReviewStateColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductReviewStateFilterBuilder class.
		/// </summary>
		public ProductReviewStateFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewStateFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductReviewStateFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewStateFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductReviewStateFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductReviewStateFilterBuilder
	
	#region ProductReviewStateParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductReviewStateColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductReviewState"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductReviewStateParameterBuilder : ParameterizedSqlFilterBuilder<ProductReviewStateColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductReviewStateParameterBuilder class.
		/// </summary>
		public ProductReviewStateParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewStateParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductReviewStateParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductReviewStateParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductReviewStateParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductReviewStateParameterBuilder
	
	#region ProductReviewStateSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductReviewStateColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductReviewState"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductReviewStateSortBuilder : SqlSortBuilder<ProductReviewStateColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductReviewStateSqlSortBuilder class.
		/// </summary>
		public ProductReviewStateSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductReviewStateSortBuilder
	
} // end namespace
