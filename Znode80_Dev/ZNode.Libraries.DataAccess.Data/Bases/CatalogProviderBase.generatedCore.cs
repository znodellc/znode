﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CatalogProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CatalogProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Catalog, ZNode.Libraries.DataAccess.Entities.CatalogKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CatalogKey key)
		{
			return Delete(transactionManager, key.CatalogID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_catalogID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _catalogID)
		{
			return Delete(null, _catalogID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _catalogID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Catalog Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CatalogKey key, int start, int pageLength)
		{
			return GetByCatalogID(transactionManager, key.CatalogID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCatalog_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public TList<Catalog> GetByExternalID(System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(null,_externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCatalog_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public TList<Catalog> GetByExternalID(System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCatalog_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public TList<Catalog> GetByExternalID(TransactionManager transactionManager, System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCatalog_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public TList<Catalog> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCatalog_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public TList<Catalog> GetByExternalID(System.String _externalID, int start, int pageLength, out int count)
		{
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCatalog_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public abstract TList<Catalog> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCatalog_IsActive index.
		/// </summary>
		/// <param name="_isActive"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public TList<Catalog> GetByIsActive(System.Boolean _isActive)
		{
			int count = -1;
			return GetByIsActive(null,_isActive, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCatalog_IsActive index.
		/// </summary>
		/// <param name="_isActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public TList<Catalog> GetByIsActive(System.Boolean _isActive, int start, int pageLength)
		{
			int count = -1;
			return GetByIsActive(null, _isActive, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCatalog_IsActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_isActive"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public TList<Catalog> GetByIsActive(TransactionManager transactionManager, System.Boolean _isActive)
		{
			int count = -1;
			return GetByIsActive(transactionManager, _isActive, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCatalog_IsActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_isActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public TList<Catalog> GetByIsActive(TransactionManager transactionManager, System.Boolean _isActive, int start, int pageLength)
		{
			int count = -1;
			return GetByIsActive(transactionManager, _isActive, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCatalog_IsActive index.
		/// </summary>
		/// <param name="_isActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public TList<Catalog> GetByIsActive(System.Boolean _isActive, int start, int pageLength, out int count)
		{
			return GetByIsActive(null, _isActive, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCatalog_IsActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_isActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public abstract TList<Catalog> GetByIsActive(TransactionManager transactionManager, System.Boolean _isActive, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCatalog_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public TList<Catalog> GetByPortalID(System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(null,_portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCatalog_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public TList<Catalog> GetByPortalID(System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCatalog_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public TList<Catalog> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCatalog_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public TList<Catalog> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCatalog_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public TList<Catalog> GetByPortalID(System.Int32? _portalID, int start, int pageLength, out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCatalog_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Catalog&gt;"/> class.</returns>
		public abstract TList<Catalog> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeCatalog index.
		/// </summary>
		/// <param name="_catalogID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Catalog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Catalog GetByCatalogID(System.Int32 _catalogID)
		{
			int count = -1;
			return GetByCatalogID(null,_catalogID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCatalog index.
		/// </summary>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Catalog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Catalog GetByCatalogID(System.Int32 _catalogID, int start, int pageLength)
		{
			int count = -1;
			return GetByCatalogID(null, _catalogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCatalog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Catalog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Catalog GetByCatalogID(TransactionManager transactionManager, System.Int32 _catalogID)
		{
			int count = -1;
			return GetByCatalogID(transactionManager, _catalogID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCatalog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Catalog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Catalog GetByCatalogID(TransactionManager transactionManager, System.Int32 _catalogID, int start, int pageLength)
		{
			int count = -1;
			return GetByCatalogID(transactionManager, _catalogID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCatalog index.
		/// </summary>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Catalog"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Catalog GetByCatalogID(System.Int32 _catalogID, int start, int pageLength, out int count)
		{
			return GetByCatalogID(null, _catalogID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCatalog index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_catalogID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Catalog"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Catalog GetByCatalogID(TransactionManager transactionManager, System.Int32 _catalogID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Catalog&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Catalog&gt;"/></returns>
		public static TList<Catalog> Fill(IDataReader reader, TList<Catalog> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Catalog c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Catalog")
					.Append("|").Append((System.Int32)reader[((int)CatalogColumn.CatalogID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Catalog>(
					key.ToString(), // EntityTrackingKey
					"Catalog",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Catalog();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CatalogID = (System.Int32)reader[((int)CatalogColumn.CatalogID - 1)];
					c.Name = (System.String)reader[((int)CatalogColumn.Name - 1)];
					c.IsActive = (System.Boolean)reader[((int)CatalogColumn.IsActive - 1)];
					c.PortalID = (reader.IsDBNull(((int)CatalogColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)CatalogColumn.PortalID - 1)];
					c.ExternalID = (reader.IsDBNull(((int)CatalogColumn.ExternalID - 1)))?null:(System.String)reader[((int)CatalogColumn.ExternalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Catalog"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Catalog"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Catalog entity)
		{
			if (!reader.Read()) return;
			
			entity.CatalogID = (System.Int32)reader[((int)CatalogColumn.CatalogID - 1)];
			entity.Name = (System.String)reader[((int)CatalogColumn.Name - 1)];
			entity.IsActive = (System.Boolean)reader[((int)CatalogColumn.IsActive - 1)];
			entity.PortalID = (reader.IsDBNull(((int)CatalogColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)CatalogColumn.PortalID - 1)];
			entity.ExternalID = (reader.IsDBNull(((int)CatalogColumn.ExternalID - 1)))?null:(System.String)reader[((int)CatalogColumn.ExternalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Catalog"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Catalog"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Catalog entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CatalogID = (System.Int32)dataRow["CatalogID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.PortalID = Convert.IsDBNull(dataRow["PortalID"]) ? null : (System.Int32?)dataRow["PortalID"];
			entity.ExternalID = Convert.IsDBNull(dataRow["ExternalID"]) ? null : (System.String)dataRow["ExternalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Catalog"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Catalog Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Catalog entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PortalID ?? (int)0);
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, (entity.PortalID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCatalogID methods when available
			
			#region FacetGroupCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<FacetGroup>|FacetGroupCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'FacetGroupCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.FacetGroupCollection = DataRepository.FacetGroupProvider.GetByCatalogID(transactionManager, entity.CatalogID);

				if (deep && entity.FacetGroupCollection.Count > 0)
				{
					deepHandles.Add("FacetGroupCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<FacetGroup>) DataRepository.FacetGroupProvider.DeepLoad,
						new object[] { transactionManager, entity.FacetGroupCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PortalCatalogCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PortalCatalog>|PortalCatalogCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalCatalogCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PortalCatalogCollection = DataRepository.PortalCatalogProvider.GetByCatalogID(transactionManager, entity.CatalogID);

				if (deep && entity.PortalCatalogCollection.Count > 0)
				{
					deepHandles.Add("PortalCatalogCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PortalCatalog>) DataRepository.PortalCatalogProvider.DeepLoad,
						new object[] { transactionManager, entity.PortalCatalogCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region LuceneGlobalProductCatalogBoostCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<LuceneGlobalProductCatalogBoost>|LuceneGlobalProductCatalogBoostCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'LuceneGlobalProductCatalogBoostCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.LuceneGlobalProductCatalogBoostCollection = DataRepository.LuceneGlobalProductCatalogBoostProvider.GetByCatalogID(transactionManager, entity.CatalogID);

				if (deep && entity.LuceneGlobalProductCatalogBoostCollection.Count > 0)
				{
					deepHandles.Add("LuceneGlobalProductCatalogBoostCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<LuceneGlobalProductCatalogBoost>) DataRepository.LuceneGlobalProductCatalogBoostProvider.DeepLoad,
						new object[] { transactionManager, entity.LuceneGlobalProductCatalogBoostCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PromotionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Promotion>|PromotionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PromotionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PromotionCollection = DataRepository.PromotionProvider.GetByCatalogID(transactionManager, entity.CatalogID);

				if (deep && entity.PromotionCollection.Count > 0)
				{
					deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Promotion>) DataRepository.PromotionProvider.DeepLoad,
						new object[] { transactionManager, entity.PromotionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CategoryNodeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CategoryNode>|CategoryNodeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryNodeCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CategoryNodeCollection = DataRepository.CategoryNodeProvider.GetByCatalogID(transactionManager, entity.CatalogID);

				if (deep && entity.CategoryNodeCollection.Count > 0)
				{
					deepHandles.Add("CategoryNodeCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CategoryNode>) DataRepository.CategoryNodeProvider.DeepLoad,
						new object[] { transactionManager, entity.CategoryNodeCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Catalog object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Catalog instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Catalog Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Catalog entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<FacetGroup>
				if (CanDeepSave(entity.FacetGroupCollection, "List<FacetGroup>|FacetGroupCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(FacetGroup child in entity.FacetGroupCollection)
					{
						if(child.CatalogIDSource != null)
						{
							child.CatalogID = child.CatalogIDSource.CatalogID;
						}
						else
						{
							child.CatalogID = entity.CatalogID;
						}

					}

					if (entity.FacetGroupCollection.Count > 0 || entity.FacetGroupCollection.DeletedItems.Count > 0)
					{
						//DataRepository.FacetGroupProvider.Save(transactionManager, entity.FacetGroupCollection);
						
						deepHandles.Add("FacetGroupCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< FacetGroup >) DataRepository.FacetGroupProvider.DeepSave,
							new object[] { transactionManager, entity.FacetGroupCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PortalCatalog>
				if (CanDeepSave(entity.PortalCatalogCollection, "List<PortalCatalog>|PortalCatalogCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PortalCatalog child in entity.PortalCatalogCollection)
					{
						if(child.CatalogIDSource != null)
						{
							child.CatalogID = child.CatalogIDSource.CatalogID;
						}
						else
						{
							child.CatalogID = entity.CatalogID;
						}

					}

					if (entity.PortalCatalogCollection.Count > 0 || entity.PortalCatalogCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PortalCatalogProvider.Save(transactionManager, entity.PortalCatalogCollection);
						
						deepHandles.Add("PortalCatalogCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PortalCatalog >) DataRepository.PortalCatalogProvider.DeepSave,
							new object[] { transactionManager, entity.PortalCatalogCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<LuceneGlobalProductCatalogBoost>
				if (CanDeepSave(entity.LuceneGlobalProductCatalogBoostCollection, "List<LuceneGlobalProductCatalogBoost>|LuceneGlobalProductCatalogBoostCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(LuceneGlobalProductCatalogBoost child in entity.LuceneGlobalProductCatalogBoostCollection)
					{
						if(child.CatalogIDSource != null)
						{
							child.CatalogID = child.CatalogIDSource.CatalogID;
						}
						else
						{
							child.CatalogID = entity.CatalogID;
						}

					}

					if (entity.LuceneGlobalProductCatalogBoostCollection.Count > 0 || entity.LuceneGlobalProductCatalogBoostCollection.DeletedItems.Count > 0)
					{
						//DataRepository.LuceneGlobalProductCatalogBoostProvider.Save(transactionManager, entity.LuceneGlobalProductCatalogBoostCollection);
						
						deepHandles.Add("LuceneGlobalProductCatalogBoostCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< LuceneGlobalProductCatalogBoost >) DataRepository.LuceneGlobalProductCatalogBoostProvider.DeepSave,
							new object[] { transactionManager, entity.LuceneGlobalProductCatalogBoostCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Promotion>
				if (CanDeepSave(entity.PromotionCollection, "List<Promotion>|PromotionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Promotion child in entity.PromotionCollection)
					{
						if(child.CatalogIDSource != null)
						{
							child.CatalogID = child.CatalogIDSource.CatalogID;
						}
						else
						{
							child.CatalogID = entity.CatalogID;
						}

					}

					if (entity.PromotionCollection.Count > 0 || entity.PromotionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PromotionProvider.Save(transactionManager, entity.PromotionCollection);
						
						deepHandles.Add("PromotionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Promotion >) DataRepository.PromotionProvider.DeepSave,
							new object[] { transactionManager, entity.PromotionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CategoryNode>
				if (CanDeepSave(entity.CategoryNodeCollection, "List<CategoryNode>|CategoryNodeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CategoryNode child in entity.CategoryNodeCollection)
					{
						if(child.CatalogIDSource != null)
						{
							child.CatalogID = child.CatalogIDSource.CatalogID;
						}
						else
						{
							child.CatalogID = entity.CatalogID;
						}

					}

					if (entity.CategoryNodeCollection.Count > 0 || entity.CategoryNodeCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CategoryNodeProvider.Save(transactionManager, entity.CategoryNodeCollection);
						
						deepHandles.Add("CategoryNodeCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CategoryNode >) DataRepository.CategoryNodeProvider.DeepSave,
							new object[] { transactionManager, entity.CategoryNodeCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CatalogChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Catalog</c>
	///</summary>
	public enum CatalogChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		///<summary>
		/// Collection of <c>Catalog</c> as OneToMany for FacetGroupCollection
		///</summary>
		[ChildEntityType(typeof(TList<FacetGroup>))]
		FacetGroupCollection,
		///<summary>
		/// Collection of <c>Catalog</c> as OneToMany for PortalCatalogCollection
		///</summary>
		[ChildEntityType(typeof(TList<PortalCatalog>))]
		PortalCatalogCollection,
		///<summary>
		/// Collection of <c>Catalog</c> as OneToMany for LuceneGlobalProductCatalogBoostCollection
		///</summary>
		[ChildEntityType(typeof(TList<LuceneGlobalProductCatalogBoost>))]
		LuceneGlobalProductCatalogBoostCollection,
		///<summary>
		/// Collection of <c>Catalog</c> as OneToMany for PromotionCollection
		///</summary>
		[ChildEntityType(typeof(TList<Promotion>))]
		PromotionCollection,
		///<summary>
		/// Collection of <c>Catalog</c> as OneToMany for CategoryNodeCollection
		///</summary>
		[ChildEntityType(typeof(TList<CategoryNode>))]
		CategoryNodeCollection,
	}
	
	#endregion CatalogChildEntityTypes
	
	#region CatalogFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CatalogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Catalog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CatalogFilterBuilder : SqlFilterBuilder<CatalogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CatalogFilterBuilder class.
		/// </summary>
		public CatalogFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CatalogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CatalogFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CatalogFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CatalogFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CatalogFilterBuilder
	
	#region CatalogParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CatalogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Catalog"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CatalogParameterBuilder : ParameterizedSqlFilterBuilder<CatalogColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CatalogParameterBuilder class.
		/// </summary>
		public CatalogParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CatalogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CatalogParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CatalogParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CatalogParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CatalogParameterBuilder
	
	#region CatalogSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CatalogColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Catalog"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CatalogSortBuilder : SqlSortBuilder<CatalogColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CatalogSqlSortBuilder class.
		/// </summary>
		public CatalogSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CatalogSortBuilder
	
} // end namespace
