﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ShippingTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ShippingTypeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ShippingType, ZNode.Libraries.DataAccess.Entities.ShippingTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ShippingTypeKey key)
		{
			return Delete(transactionManager, key.ShippingTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_shippingTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _shippingTypeID)
		{
			return Delete(null, _shippingTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _shippingTypeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ShippingType Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ShippingTypeKey key, int start, int pageLength)
		{
			return GetByShippingTypeID(transactionManager, key.ShippingTypeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeShippingType_ClassName index.
		/// </summary>
		/// <param name="_className"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingType&gt;"/> class.</returns>
		public TList<ShippingType> GetByClassName(System.String _className)
		{
			int count = -1;
			return GetByClassName(null,_className, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingType_ClassName index.
		/// </summary>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingType&gt;"/> class.</returns>
		public TList<ShippingType> GetByClassName(System.String _className, int start, int pageLength)
		{
			int count = -1;
			return GetByClassName(null, _className, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingType_ClassName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_className"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingType&gt;"/> class.</returns>
		public TList<ShippingType> GetByClassName(TransactionManager transactionManager, System.String _className)
		{
			int count = -1;
			return GetByClassName(transactionManager, _className, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingType_ClassName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingType&gt;"/> class.</returns>
		public TList<ShippingType> GetByClassName(TransactionManager transactionManager, System.String _className, int start, int pageLength)
		{
			int count = -1;
			return GetByClassName(transactionManager, _className, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingType_ClassName index.
		/// </summary>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingType&gt;"/> class.</returns>
		public TList<ShippingType> GetByClassName(System.String _className, int start, int pageLength, out int count)
		{
			return GetByClassName(null, _className, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingType_ClassName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingType&gt;"/> class.</returns>
		public abstract TList<ShippingType> GetByClassName(TransactionManager transactionManager, System.String _className, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeShippingType_IsActive index.
		/// </summary>
		/// <param name="_isActive"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingType&gt;"/> class.</returns>
		public TList<ShippingType> GetByIsActive(System.Boolean _isActive)
		{
			int count = -1;
			return GetByIsActive(null,_isActive, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingType_IsActive index.
		/// </summary>
		/// <param name="_isActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingType&gt;"/> class.</returns>
		public TList<ShippingType> GetByIsActive(System.Boolean _isActive, int start, int pageLength)
		{
			int count = -1;
			return GetByIsActive(null, _isActive, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingType_IsActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_isActive"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingType&gt;"/> class.</returns>
		public TList<ShippingType> GetByIsActive(TransactionManager transactionManager, System.Boolean _isActive)
		{
			int count = -1;
			return GetByIsActive(transactionManager, _isActive, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingType_IsActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_isActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingType&gt;"/> class.</returns>
		public TList<ShippingType> GetByIsActive(TransactionManager transactionManager, System.Boolean _isActive, int start, int pageLength)
		{
			int count = -1;
			return GetByIsActive(transactionManager, _isActive, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingType_IsActive index.
		/// </summary>
		/// <param name="_isActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingType&gt;"/> class.</returns>
		public TList<ShippingType> GetByIsActive(System.Boolean _isActive, int start, int pageLength, out int count)
		{
			return GetByIsActive(null, _isActive, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeShippingType_IsActive index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_isActive"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ShippingType&gt;"/> class.</returns>
		public abstract TList<ShippingType> GetByIsActive(TransactionManager transactionManager, System.Boolean _isActive, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SC_ShippingType index.
		/// </summary>
		/// <param name="_shippingTypeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ShippingType GetByShippingTypeID(System.Int32 _shippingTypeID)
		{
			int count = -1;
			return GetByShippingTypeID(null,_shippingTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ShippingType index.
		/// </summary>
		/// <param name="_shippingTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ShippingType GetByShippingTypeID(System.Int32 _shippingTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingTypeID(null, _shippingTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ShippingType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ShippingType GetByShippingTypeID(TransactionManager transactionManager, System.Int32 _shippingTypeID)
		{
			int count = -1;
			return GetByShippingTypeID(transactionManager, _shippingTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ShippingType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ShippingType GetByShippingTypeID(TransactionManager transactionManager, System.Int32 _shippingTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingTypeID(transactionManager, _shippingTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ShippingType index.
		/// </summary>
		/// <param name="_shippingTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ShippingType GetByShippingTypeID(System.Int32 _shippingTypeID, int start, int pageLength, out int count)
		{
			return GetByShippingTypeID(null, _shippingTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ShippingType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ShippingType GetByShippingTypeID(TransactionManager transactionManager, System.Int32 _shippingTypeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ShippingType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ShippingType&gt;"/></returns>
		public static TList<ShippingType> Fill(IDataReader reader, TList<ShippingType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ShippingType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ShippingType")
					.Append("|").Append((System.Int32)reader[((int)ShippingTypeColumn.ShippingTypeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ShippingType>(
					key.ToString(), // EntityTrackingKey
					"ShippingType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ShippingType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ShippingTypeID = (System.Int32)reader[((int)ShippingTypeColumn.ShippingTypeID - 1)];
					c.ClassName = (reader.IsDBNull(((int)ShippingTypeColumn.ClassName - 1)))?null:(System.String)reader[((int)ShippingTypeColumn.ClassName - 1)];
					c.Name = (System.String)reader[((int)ShippingTypeColumn.Name - 1)];
					c.Description = (reader.IsDBNull(((int)ShippingTypeColumn.Description - 1)))?null:(System.String)reader[((int)ShippingTypeColumn.Description - 1)];
					c.IsActive = (System.Boolean)reader[((int)ShippingTypeColumn.IsActive - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ShippingType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ShippingType entity)
		{
			if (!reader.Read()) return;
			
			entity.ShippingTypeID = (System.Int32)reader[((int)ShippingTypeColumn.ShippingTypeID - 1)];
			entity.ClassName = (reader.IsDBNull(((int)ShippingTypeColumn.ClassName - 1)))?null:(System.String)reader[((int)ShippingTypeColumn.ClassName - 1)];
			entity.Name = (System.String)reader[((int)ShippingTypeColumn.Name - 1)];
			entity.Description = (reader.IsDBNull(((int)ShippingTypeColumn.Description - 1)))?null:(System.String)reader[((int)ShippingTypeColumn.Description - 1)];
			entity.IsActive = (System.Boolean)reader[((int)ShippingTypeColumn.IsActive - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ShippingType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ShippingType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ShippingType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ShippingTypeID = (System.Int32)dataRow["ShippingTypeID"];
			entity.ClassName = Convert.IsDBNull(dataRow["ClassName"]) ? null : (System.String)dataRow["ClassName"];
			entity.Name = (System.String)dataRow["Name"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.IsActive = (System.Boolean)dataRow["IsActive"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ShippingType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ShippingType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ShippingType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByShippingTypeID methods when available
			
			#region ShippingServiceCodeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ShippingServiceCode>|ShippingServiceCodeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ShippingServiceCodeCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ShippingServiceCodeCollection = DataRepository.ShippingServiceCodeProvider.GetByShippingTypeID(transactionManager, entity.ShippingTypeID);

				if (deep && entity.ShippingServiceCodeCollection.Count > 0)
				{
					deepHandles.Add("ShippingServiceCodeCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ShippingServiceCode>) DataRepository.ShippingServiceCodeProvider.DeepLoad,
						new object[] { transactionManager, entity.ShippingServiceCodeCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ShippingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Shipping>|ShippingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ShippingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ShippingCollection = DataRepository.ShippingProvider.GetByShippingTypeID(transactionManager, entity.ShippingTypeID);

				if (deep && entity.ShippingCollection.Count > 0)
				{
					deepHandles.Add("ShippingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Shipping>) DataRepository.ShippingProvider.DeepLoad,
						new object[] { transactionManager, entity.ShippingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ShippingType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ShippingType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ShippingType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ShippingType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<ShippingServiceCode>
				if (CanDeepSave(entity.ShippingServiceCodeCollection, "List<ShippingServiceCode>|ShippingServiceCodeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ShippingServiceCode child in entity.ShippingServiceCodeCollection)
					{
						if(child.ShippingTypeIDSource != null)
						{
							child.ShippingTypeID = child.ShippingTypeIDSource.ShippingTypeID;
						}
						else
						{
							child.ShippingTypeID = entity.ShippingTypeID;
						}

					}

					if (entity.ShippingServiceCodeCollection.Count > 0 || entity.ShippingServiceCodeCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ShippingServiceCodeProvider.Save(transactionManager, entity.ShippingServiceCodeCollection);
						
						deepHandles.Add("ShippingServiceCodeCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ShippingServiceCode >) DataRepository.ShippingServiceCodeProvider.DeepSave,
							new object[] { transactionManager, entity.ShippingServiceCodeCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Shipping>
				if (CanDeepSave(entity.ShippingCollection, "List<Shipping>|ShippingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Shipping child in entity.ShippingCollection)
					{
						if(child.ShippingTypeIDSource != null)
						{
							child.ShippingTypeID = child.ShippingTypeIDSource.ShippingTypeID;
						}
						else
						{
							child.ShippingTypeID = entity.ShippingTypeID;
						}

					}

					if (entity.ShippingCollection.Count > 0 || entity.ShippingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ShippingProvider.Save(transactionManager, entity.ShippingCollection);
						
						deepHandles.Add("ShippingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Shipping >) DataRepository.ShippingProvider.DeepSave,
							new object[] { transactionManager, entity.ShippingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ShippingTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ShippingType</c>
	///</summary>
	public enum ShippingTypeChildEntityTypes
	{
		///<summary>
		/// Collection of <c>ShippingType</c> as OneToMany for ShippingServiceCodeCollection
		///</summary>
		[ChildEntityType(typeof(TList<ShippingServiceCode>))]
		ShippingServiceCodeCollection,
		///<summary>
		/// Collection of <c>ShippingType</c> as OneToMany for ShippingCollection
		///</summary>
		[ChildEntityType(typeof(TList<Shipping>))]
		ShippingCollection,
	}
	
	#endregion ShippingTypeChildEntityTypes
	
	#region ShippingTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ShippingTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ShippingType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingTypeFilterBuilder : SqlFilterBuilder<ShippingTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingTypeFilterBuilder class.
		/// </summary>
		public ShippingTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingTypeFilterBuilder
	
	#region ShippingTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ShippingTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ShippingType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ShippingTypeParameterBuilder : ParameterizedSqlFilterBuilder<ShippingTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingTypeParameterBuilder class.
		/// </summary>
		public ShippingTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ShippingTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ShippingTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ShippingTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ShippingTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ShippingTypeParameterBuilder
	
	#region ShippingTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ShippingTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ShippingType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ShippingTypeSortBuilder : SqlSortBuilder<ShippingTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ShippingTypeSqlSortBuilder class.
		/// </summary>
		public ShippingTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ShippingTypeSortBuilder
	
} // end namespace
