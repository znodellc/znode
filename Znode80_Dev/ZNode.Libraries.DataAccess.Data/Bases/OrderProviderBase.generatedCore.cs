﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="OrderProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class OrderProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Order, ZNode.Libraries.DataAccess.Entities.OrderKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderKey key)
		{
			return Delete(transactionManager, key.OrderID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_orderID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _orderID)
		{
			return Delete(null, _orderID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _orderID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_Order_SC_Shipping key.
		///		FK_SC_Order_SC_Shipping Description: 
		/// </summary>
		/// <param name="_shippingID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByShippingID(System.Int32? _shippingID)
		{
			int count = -1;
			return GetByShippingID(_shippingID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_Order_SC_Shipping key.
		///		FK_SC_Order_SC_Shipping Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		/// <remarks></remarks>
		public TList<Order> GetByShippingID(TransactionManager transactionManager, System.Int32? _shippingID)
		{
			int count = -1;
			return GetByShippingID(transactionManager, _shippingID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_Order_SC_Shipping key.
		///		FK_SC_Order_SC_Shipping Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByShippingID(TransactionManager transactionManager, System.Int32? _shippingID, int start, int pageLength)
		{
			int count = -1;
			return GetByShippingID(transactionManager, _shippingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_Order_SC_Shipping key.
		///		fKSCOrderSCShipping Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_shippingID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByShippingID(System.Int32? _shippingID, int start, int pageLength)
		{
			int count =  -1;
			return GetByShippingID(null, _shippingID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_Order_SC_Shipping key.
		///		fKSCOrderSCShipping Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_shippingID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByShippingID(System.Int32? _shippingID, int start, int pageLength,out int count)
		{
			return GetByShippingID(null, _shippingID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_Order_SC_Shipping key.
		///		FK_SC_Order_SC_Shipping Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_shippingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public abstract TList<Order> GetByShippingID(TransactionManager transactionManager, System.Int32? _shippingID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodeAccount key.
		///		FK_ZNodeOrder_ZNodeAccount Description: 
		/// </summary>
		/// <param name="_referralAccountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByReferralAccountID(System.Int32? _referralAccountID)
		{
			int count = -1;
			return GetByReferralAccountID(_referralAccountID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodeAccount key.
		///		FK_ZNodeOrder_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralAccountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		/// <remarks></remarks>
		public TList<Order> GetByReferralAccountID(TransactionManager transactionManager, System.Int32? _referralAccountID)
		{
			int count = -1;
			return GetByReferralAccountID(transactionManager, _referralAccountID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodeAccount key.
		///		FK_ZNodeOrder_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralAccountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByReferralAccountID(TransactionManager transactionManager, System.Int32? _referralAccountID, int start, int pageLength)
		{
			int count = -1;
			return GetByReferralAccountID(transactionManager, _referralAccountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodeAccount key.
		///		fKZNodeOrderZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_referralAccountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByReferralAccountID(System.Int32? _referralAccountID, int start, int pageLength)
		{
			int count =  -1;
			return GetByReferralAccountID(null, _referralAccountID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodeAccount key.
		///		fKZNodeOrderZNodeAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_referralAccountID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByReferralAccountID(System.Int32? _referralAccountID, int start, int pageLength,out int count)
		{
			return GetByReferralAccountID(null, _referralAccountID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodeAccount key.
		///		FK_ZNodeOrder_ZNodeAccount Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralAccountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public abstract TList<Order> GetByReferralAccountID(TransactionManager transactionManager, System.Int32? _referralAccountID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodePaymentSetting key.
		///		FK_ZNodeOrder_ZNodePaymentSetting Description: 
		/// </summary>
		/// <param name="_paymentSettingID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByPaymentSettingID(System.Int32? _paymentSettingID)
		{
			int count = -1;
			return GetByPaymentSettingID(_paymentSettingID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodePaymentSetting key.
		///		FK_ZNodeOrder_ZNodePaymentSetting Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentSettingID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		/// <remarks></remarks>
		public TList<Order> GetByPaymentSettingID(TransactionManager transactionManager, System.Int32? _paymentSettingID)
		{
			int count = -1;
			return GetByPaymentSettingID(transactionManager, _paymentSettingID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodePaymentSetting key.
		///		FK_ZNodeOrder_ZNodePaymentSetting Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentSettingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByPaymentSettingID(TransactionManager transactionManager, System.Int32? _paymentSettingID, int start, int pageLength)
		{
			int count = -1;
			return GetByPaymentSettingID(transactionManager, _paymentSettingID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodePaymentSetting key.
		///		fKZNodeOrderZNodePaymentSetting Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_paymentSettingID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByPaymentSettingID(System.Int32? _paymentSettingID, int start, int pageLength)
		{
			int count =  -1;
			return GetByPaymentSettingID(null, _paymentSettingID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodePaymentSetting key.
		///		fKZNodeOrderZNodePaymentSetting Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_paymentSettingID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByPaymentSettingID(System.Int32? _paymentSettingID, int start, int pageLength,out int count)
		{
			return GetByPaymentSettingID(null, _paymentSettingID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodePaymentSetting key.
		///		FK_ZNodeOrder_ZNodePaymentSetting Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentSettingID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public abstract TList<Order> GetByPaymentSettingID(TransactionManager transactionManager, System.Int32? _paymentSettingID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodePaymentStatus key.
		///		FK_ZNodeOrder_ZNodePaymentStatus Description: 
		/// </summary>
		/// <param name="_paymentStatusID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByPaymentStatusID(System.Int32? _paymentStatusID)
		{
			int count = -1;
			return GetByPaymentStatusID(_paymentStatusID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodePaymentStatus key.
		///		FK_ZNodeOrder_ZNodePaymentStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentStatusID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		/// <remarks></remarks>
		public TList<Order> GetByPaymentStatusID(TransactionManager transactionManager, System.Int32? _paymentStatusID)
		{
			int count = -1;
			return GetByPaymentStatusID(transactionManager, _paymentStatusID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodePaymentStatus key.
		///		FK_ZNodeOrder_ZNodePaymentStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByPaymentStatusID(TransactionManager transactionManager, System.Int32? _paymentStatusID, int start, int pageLength)
		{
			int count = -1;
			return GetByPaymentStatusID(transactionManager, _paymentStatusID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodePaymentStatus key.
		///		fKZNodeOrderZNodePaymentStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_paymentStatusID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByPaymentStatusID(System.Int32? _paymentStatusID, int start, int pageLength)
		{
			int count =  -1;
			return GetByPaymentStatusID(null, _paymentStatusID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodePaymentStatus key.
		///		fKZNodeOrderZNodePaymentStatus Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_paymentStatusID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public TList<Order> GetByPaymentStatusID(System.Int32? _paymentStatusID, int start, int pageLength,out int count)
		{
			return GetByPaymentStatusID(null, _paymentStatusID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeOrder_ZNodePaymentStatus key.
		///		FK_ZNodeOrder_ZNodePaymentStatus Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_paymentStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Order objects.</returns>
		public abstract TList<Order> GetByPaymentStatusID(TransactionManager transactionManager, System.Int32? _paymentStatusID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Order Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderKey key, int start, int pageLength)
		{
			return GetByOrderID(transactionManager, key.OrderID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeOrder_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByAccountID(System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(null,_accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByAccountID(System.Int32? _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_AccountID index.
		/// </summary>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByAccountID(System.Int32? _accountID, int start, int pageLength, out int count)
		{
			return GetByAccountID(null, _accountID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_AccountID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public abstract TList<Order> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeOrder_OrderDate index.
		/// </summary>
		/// <param name="_orderDate"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByOrderDate(System.DateTime? _orderDate)
		{
			int count = -1;
			return GetByOrderDate(null,_orderDate, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_OrderDate index.
		/// </summary>
		/// <param name="_orderDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByOrderDate(System.DateTime? _orderDate, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderDate(null, _orderDate, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_OrderDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderDate"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByOrderDate(TransactionManager transactionManager, System.DateTime? _orderDate)
		{
			int count = -1;
			return GetByOrderDate(transactionManager, _orderDate, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_OrderDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByOrderDate(TransactionManager transactionManager, System.DateTime? _orderDate, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderDate(transactionManager, _orderDate, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_OrderDate index.
		/// </summary>
		/// <param name="_orderDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByOrderDate(System.DateTime? _orderDate, int start, int pageLength, out int count)
		{
			return GetByOrderDate(null, _orderDate, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_OrderDate index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderDate"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public abstract TList<Order> GetByOrderDate(TransactionManager transactionManager, System.DateTime? _orderDate, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeOrder_OrderStateID index.
		/// </summary>
		/// <param name="_orderStateID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByOrderStateID(System.Int32? _orderStateID)
		{
			int count = -1;
			return GetByOrderStateID(null,_orderStateID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_OrderStateID index.
		/// </summary>
		/// <param name="_orderStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByOrderStateID(System.Int32? _orderStateID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderStateID(null, _orderStateID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_OrderStateID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderStateID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByOrderStateID(TransactionManager transactionManager, System.Int32? _orderStateID)
		{
			int count = -1;
			return GetByOrderStateID(transactionManager, _orderStateID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_OrderStateID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByOrderStateID(TransactionManager transactionManager, System.Int32? _orderStateID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderStateID(transactionManager, _orderStateID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_OrderStateID index.
		/// </summary>
		/// <param name="_orderStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByOrderStateID(System.Int32? _orderStateID, int start, int pageLength, out int count)
		{
			return GetByOrderStateID(null, _orderStateID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_OrderStateID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public abstract TList<Order> GetByOrderStateID(TransactionManager transactionManager, System.Int32? _orderStateID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeOrder_PortalId index.
		/// </summary>
		/// <param name="_portalId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByPortalId(System.Int32? _portalId)
		{
			int count = -1;
			return GetByPortalId(null,_portalId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_PortalId index.
		/// </summary>
		/// <param name="_portalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByPortalId(System.Int32? _portalId, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalId(null, _portalId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_PortalId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByPortalId(TransactionManager transactionManager, System.Int32? _portalId)
		{
			int count = -1;
			return GetByPortalId(transactionManager, _portalId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_PortalId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByPortalId(TransactionManager transactionManager, System.Int32? _portalId, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalId(transactionManager, _portalId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_PortalId index.
		/// </summary>
		/// <param name="_portalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByPortalId(System.Int32? _portalId, int start, int pageLength, out int count)
		{
			return GetByPortalId(null, _portalId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_PortalId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public abstract TList<Order> GetByPortalId(TransactionManager transactionManager, System.Int32? _portalId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeOrder_PurchaseOrderNumber index.
		/// </summary>
		/// <param name="_purchaseOrderNumber"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByPurchaseOrderNumber(System.String _purchaseOrderNumber)
		{
			int count = -1;
			return GetByPurchaseOrderNumber(null,_purchaseOrderNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_PurchaseOrderNumber index.
		/// </summary>
		/// <param name="_purchaseOrderNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByPurchaseOrderNumber(System.String _purchaseOrderNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByPurchaseOrderNumber(null, _purchaseOrderNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_PurchaseOrderNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_purchaseOrderNumber"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByPurchaseOrderNumber(TransactionManager transactionManager, System.String _purchaseOrderNumber)
		{
			int count = -1;
			return GetByPurchaseOrderNumber(transactionManager, _purchaseOrderNumber, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_PurchaseOrderNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_purchaseOrderNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByPurchaseOrderNumber(TransactionManager transactionManager, System.String _purchaseOrderNumber, int start, int pageLength)
		{
			int count = -1;
			return GetByPurchaseOrderNumber(transactionManager, _purchaseOrderNumber, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_PurchaseOrderNumber index.
		/// </summary>
		/// <param name="_purchaseOrderNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByPurchaseOrderNumber(System.String _purchaseOrderNumber, int start, int pageLength, out int count)
		{
			return GetByPurchaseOrderNumber(null, _purchaseOrderNumber, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_PurchaseOrderNumber index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_purchaseOrderNumber"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public abstract TList<Order> GetByPurchaseOrderNumber(TransactionManager transactionManager, System.String _purchaseOrderNumber, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeOrder_Total index.
		/// </summary>
		/// <param name="_total"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByTotal(System.Decimal? _total)
		{
			int count = -1;
			return GetByTotal(null,_total, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_Total index.
		/// </summary>
		/// <param name="_total"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByTotal(System.Decimal? _total, int start, int pageLength)
		{
			int count = -1;
			return GetByTotal(null, _total, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_Total index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_total"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByTotal(TransactionManager transactionManager, System.Decimal? _total)
		{
			int count = -1;
			return GetByTotal(transactionManager, _total, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_Total index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_total"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByTotal(TransactionManager transactionManager, System.Decimal? _total, int start, int pageLength)
		{
			int count = -1;
			return GetByTotal(transactionManager, _total, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_Total index.
		/// </summary>
		/// <param name="_total"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public TList<Order> GetByTotal(System.Decimal? _total, int start, int pageLength, out int count)
		{
			return GetByTotal(null, _total, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeOrder_Total index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_total"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Order&gt;"/> class.</returns>
		public abstract TList<Order> GetByTotal(TransactionManager transactionManager, System.Decimal? _total, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key SC_Order_PK index.
		/// </summary>
		/// <param name="_orderID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Order"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Order GetByOrderID(System.Int32 _orderID)
		{
			int count = -1;
			return GetByOrderID(null,_orderID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_Order_PK index.
		/// </summary>
		/// <param name="_orderID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Order"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Order GetByOrderID(System.Int32 _orderID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderID(null, _orderID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_Order_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Order"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Order GetByOrderID(TransactionManager transactionManager, System.Int32 _orderID)
		{
			int count = -1;
			return GetByOrderID(transactionManager, _orderID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_Order_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Order"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Order GetByOrderID(TransactionManager transactionManager, System.Int32 _orderID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderID(transactionManager, _orderID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_Order_PK index.
		/// </summary>
		/// <param name="_orderID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Order"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Order GetByOrderID(System.Int32 _orderID, int start, int pageLength, out int count)
		{
			return GetByOrderID(null, _orderID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_Order_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Order"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Order GetByOrderID(TransactionManager transactionManager, System.Int32 _orderID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Order&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Order&gt;"/></returns>
		public static TList<Order> Fill(IDataReader reader, TList<Order> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Order c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Order")
					.Append("|").Append((System.Int32)reader[((int)OrderColumn.OrderID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Order>(
					key.ToString(), // EntityTrackingKey
					"Order",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Order();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.OrderID = (System.Int32)reader[((int)OrderColumn.OrderID - 1)];
					c.PortalId = (reader.IsDBNull(((int)OrderColumn.PortalId - 1)))?null:(System.Int32?)reader[((int)OrderColumn.PortalId - 1)];
					c.AccountID = (reader.IsDBNull(((int)OrderColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)OrderColumn.AccountID - 1)];
					c.OrderStateID = (reader.IsDBNull(((int)OrderColumn.OrderStateID - 1)))?null:(System.Int32?)reader[((int)OrderColumn.OrderStateID - 1)];
					c.ShippingID = (reader.IsDBNull(((int)OrderColumn.ShippingID - 1)))?null:(System.Int32?)reader[((int)OrderColumn.ShippingID - 1)];
					c.PaymentTypeId = (reader.IsDBNull(((int)OrderColumn.PaymentTypeId - 1)))?null:(System.Int32?)reader[((int)OrderColumn.PaymentTypeId - 1)];
					c.BillingFirstName = (reader.IsDBNull(((int)OrderColumn.BillingFirstName - 1)))?null:(System.String)reader[((int)OrderColumn.BillingFirstName - 1)];
					c.BillingLastName = (reader.IsDBNull(((int)OrderColumn.BillingLastName - 1)))?null:(System.String)reader[((int)OrderColumn.BillingLastName - 1)];
					c.BillingCompanyName = (reader.IsDBNull(((int)OrderColumn.BillingCompanyName - 1)))?null:(System.String)reader[((int)OrderColumn.BillingCompanyName - 1)];
					c.BillingStreet = (reader.IsDBNull(((int)OrderColumn.BillingStreet - 1)))?null:(System.String)reader[((int)OrderColumn.BillingStreet - 1)];
					c.BillingStreet1 = (reader.IsDBNull(((int)OrderColumn.BillingStreet1 - 1)))?null:(System.String)reader[((int)OrderColumn.BillingStreet1 - 1)];
					c.BillingCity = (reader.IsDBNull(((int)OrderColumn.BillingCity - 1)))?null:(System.String)reader[((int)OrderColumn.BillingCity - 1)];
					c.BillingStateCode = (reader.IsDBNull(((int)OrderColumn.BillingStateCode - 1)))?null:(System.String)reader[((int)OrderColumn.BillingStateCode - 1)];
					c.BillingPostalCode = (reader.IsDBNull(((int)OrderColumn.BillingPostalCode - 1)))?null:(System.String)reader[((int)OrderColumn.BillingPostalCode - 1)];
					c.BillingCountry = (reader.IsDBNull(((int)OrderColumn.BillingCountry - 1)))?null:(System.String)reader[((int)OrderColumn.BillingCountry - 1)];
					c.BillingPhoneNumber = (reader.IsDBNull(((int)OrderColumn.BillingPhoneNumber - 1)))?null:(System.String)reader[((int)OrderColumn.BillingPhoneNumber - 1)];
					c.BillingEmailId = (reader.IsDBNull(((int)OrderColumn.BillingEmailId - 1)))?null:(System.String)reader[((int)OrderColumn.BillingEmailId - 1)];
					c.CardTransactionID = (reader.IsDBNull(((int)OrderColumn.CardTransactionID - 1)))?null:(System.String)reader[((int)OrderColumn.CardTransactionID - 1)];
					c.CardAuthCode = (reader.IsDBNull(((int)OrderColumn.CardAuthCode - 1)))?null:(System.String)reader[((int)OrderColumn.CardAuthCode - 1)];
					c.CardTypeId = (reader.IsDBNull(((int)OrderColumn.CardTypeId - 1)))?null:(System.Int32?)reader[((int)OrderColumn.CardTypeId - 1)];
					c.CardExp = (reader.IsDBNull(((int)OrderColumn.CardExp - 1)))?null:(System.String)reader[((int)OrderColumn.CardExp - 1)];
					c.TaxCost = (reader.IsDBNull(((int)OrderColumn.TaxCost - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.TaxCost - 1)];
					c.ShippingCost = (reader.IsDBNull(((int)OrderColumn.ShippingCost - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.ShippingCost - 1)];
					c.SubTotal = (reader.IsDBNull(((int)OrderColumn.SubTotal - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.SubTotal - 1)];
					c.DiscountAmount = (reader.IsDBNull(((int)OrderColumn.DiscountAmount - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.DiscountAmount - 1)];
					c.Total = (reader.IsDBNull(((int)OrderColumn.Total - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.Total - 1)];
					c.OrderDate = (reader.IsDBNull(((int)OrderColumn.OrderDate - 1)))?null:(System.DateTime?)reader[((int)OrderColumn.OrderDate - 1)];
					c.Custom1 = (reader.IsDBNull(((int)OrderColumn.Custom1 - 1)))?null:(System.String)reader[((int)OrderColumn.Custom1 - 1)];
					c.Custom2 = (reader.IsDBNull(((int)OrderColumn.Custom2 - 1)))?null:(System.String)reader[((int)OrderColumn.Custom2 - 1)];
					c.AdditionalInstructions = (reader.IsDBNull(((int)OrderColumn.AdditionalInstructions - 1)))?null:(System.String)reader[((int)OrderColumn.AdditionalInstructions - 1)];
					c.Custom3 = (reader.IsDBNull(((int)OrderColumn.Custom3 - 1)))?null:(System.String)reader[((int)OrderColumn.Custom3 - 1)];
					c.TrackingNumber = (reader.IsDBNull(((int)OrderColumn.TrackingNumber - 1)))?null:(System.String)reader[((int)OrderColumn.TrackingNumber - 1)];
					c.CouponCode = (reader.IsDBNull(((int)OrderColumn.CouponCode - 1)))?null:(System.String)reader[((int)OrderColumn.CouponCode - 1)];
					c.PromoDescription = (reader.IsDBNull(((int)OrderColumn.PromoDescription - 1)))?null:(System.String)reader[((int)OrderColumn.PromoDescription - 1)];
					c.ReferralAccountID = (reader.IsDBNull(((int)OrderColumn.ReferralAccountID - 1)))?null:(System.Int32?)reader[((int)OrderColumn.ReferralAccountID - 1)];
					c.PurchaseOrderNumber = (reader.IsDBNull(((int)OrderColumn.PurchaseOrderNumber - 1)))?null:(System.String)reader[((int)OrderColumn.PurchaseOrderNumber - 1)];
					c.PaymentStatusID = (reader.IsDBNull(((int)OrderColumn.PaymentStatusID - 1)))?null:(System.Int32?)reader[((int)OrderColumn.PaymentStatusID - 1)];
					c.WebServiceDownloadDate = (reader.IsDBNull(((int)OrderColumn.WebServiceDownloadDate - 1)))?null:(System.DateTime?)reader[((int)OrderColumn.WebServiceDownloadDate - 1)];
					c.PaymentSettingID = (reader.IsDBNull(((int)OrderColumn.PaymentSettingID - 1)))?null:(System.Int32?)reader[((int)OrderColumn.PaymentSettingID - 1)];
					c.ShipDate = (reader.IsDBNull(((int)OrderColumn.ShipDate - 1)))?null:(System.DateTime?)reader[((int)OrderColumn.ShipDate - 1)];
					c.ReturnDate = (reader.IsDBNull(((int)OrderColumn.ReturnDate - 1)))?null:(System.DateTime?)reader[((int)OrderColumn.ReturnDate - 1)];
					c.SalesTax = (reader.IsDBNull(((int)OrderColumn.SalesTax - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.SalesTax - 1)];
					c.VAT = (reader.IsDBNull(((int)OrderColumn.VAT - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.VAT - 1)];
					c.GST = (reader.IsDBNull(((int)OrderColumn.GST - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.GST - 1)];
					c.PST = (reader.IsDBNull(((int)OrderColumn.PST - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.PST - 1)];
					c.HST = (reader.IsDBNull(((int)OrderColumn.HST - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.HST - 1)];
					c.Created = (reader.IsDBNull(((int)OrderColumn.Created - 1)))?null:(System.DateTime?)reader[((int)OrderColumn.Created - 1)];
					c.CreatedBy = (reader.IsDBNull(((int)OrderColumn.CreatedBy - 1)))?null:(System.String)reader[((int)OrderColumn.CreatedBy - 1)];
					c.Modified = (reader.IsDBNull(((int)OrderColumn.Modified - 1)))?null:(System.DateTime?)reader[((int)OrderColumn.Modified - 1)];
					c.ModifiedBy = (reader.IsDBNull(((int)OrderColumn.ModifiedBy - 1)))?null:(System.String)reader[((int)OrderColumn.ModifiedBy - 1)];
					c.ExternalID = (reader.IsDBNull(((int)OrderColumn.ExternalID - 1)))?null:(System.String)reader[((int)OrderColumn.ExternalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Order"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Order"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Order entity)
		{
			if (!reader.Read()) return;
			
			entity.OrderID = (System.Int32)reader[((int)OrderColumn.OrderID - 1)];
			entity.PortalId = (reader.IsDBNull(((int)OrderColumn.PortalId - 1)))?null:(System.Int32?)reader[((int)OrderColumn.PortalId - 1)];
			entity.AccountID = (reader.IsDBNull(((int)OrderColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)OrderColumn.AccountID - 1)];
			entity.OrderStateID = (reader.IsDBNull(((int)OrderColumn.OrderStateID - 1)))?null:(System.Int32?)reader[((int)OrderColumn.OrderStateID - 1)];
			entity.ShippingID = (reader.IsDBNull(((int)OrderColumn.ShippingID - 1)))?null:(System.Int32?)reader[((int)OrderColumn.ShippingID - 1)];
			entity.PaymentTypeId = (reader.IsDBNull(((int)OrderColumn.PaymentTypeId - 1)))?null:(System.Int32?)reader[((int)OrderColumn.PaymentTypeId - 1)];
			entity.BillingFirstName = (reader.IsDBNull(((int)OrderColumn.BillingFirstName - 1)))?null:(System.String)reader[((int)OrderColumn.BillingFirstName - 1)];
			entity.BillingLastName = (reader.IsDBNull(((int)OrderColumn.BillingLastName - 1)))?null:(System.String)reader[((int)OrderColumn.BillingLastName - 1)];
			entity.BillingCompanyName = (reader.IsDBNull(((int)OrderColumn.BillingCompanyName - 1)))?null:(System.String)reader[((int)OrderColumn.BillingCompanyName - 1)];
			entity.BillingStreet = (reader.IsDBNull(((int)OrderColumn.BillingStreet - 1)))?null:(System.String)reader[((int)OrderColumn.BillingStreet - 1)];
			entity.BillingStreet1 = (reader.IsDBNull(((int)OrderColumn.BillingStreet1 - 1)))?null:(System.String)reader[((int)OrderColumn.BillingStreet1 - 1)];
			entity.BillingCity = (reader.IsDBNull(((int)OrderColumn.BillingCity - 1)))?null:(System.String)reader[((int)OrderColumn.BillingCity - 1)];
			entity.BillingStateCode = (reader.IsDBNull(((int)OrderColumn.BillingStateCode - 1)))?null:(System.String)reader[((int)OrderColumn.BillingStateCode - 1)];
			entity.BillingPostalCode = (reader.IsDBNull(((int)OrderColumn.BillingPostalCode - 1)))?null:(System.String)reader[((int)OrderColumn.BillingPostalCode - 1)];
			entity.BillingCountry = (reader.IsDBNull(((int)OrderColumn.BillingCountry - 1)))?null:(System.String)reader[((int)OrderColumn.BillingCountry - 1)];
			entity.BillingPhoneNumber = (reader.IsDBNull(((int)OrderColumn.BillingPhoneNumber - 1)))?null:(System.String)reader[((int)OrderColumn.BillingPhoneNumber - 1)];
			entity.BillingEmailId = (reader.IsDBNull(((int)OrderColumn.BillingEmailId - 1)))?null:(System.String)reader[((int)OrderColumn.BillingEmailId - 1)];
			entity.CardTransactionID = (reader.IsDBNull(((int)OrderColumn.CardTransactionID - 1)))?null:(System.String)reader[((int)OrderColumn.CardTransactionID - 1)];
			entity.CardAuthCode = (reader.IsDBNull(((int)OrderColumn.CardAuthCode - 1)))?null:(System.String)reader[((int)OrderColumn.CardAuthCode - 1)];
			entity.CardTypeId = (reader.IsDBNull(((int)OrderColumn.CardTypeId - 1)))?null:(System.Int32?)reader[((int)OrderColumn.CardTypeId - 1)];
			entity.CardExp = (reader.IsDBNull(((int)OrderColumn.CardExp - 1)))?null:(System.String)reader[((int)OrderColumn.CardExp - 1)];
			entity.TaxCost = (reader.IsDBNull(((int)OrderColumn.TaxCost - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.TaxCost - 1)];
			entity.ShippingCost = (reader.IsDBNull(((int)OrderColumn.ShippingCost - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.ShippingCost - 1)];
			entity.SubTotal = (reader.IsDBNull(((int)OrderColumn.SubTotal - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.SubTotal - 1)];
			entity.DiscountAmount = (reader.IsDBNull(((int)OrderColumn.DiscountAmount - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.DiscountAmount - 1)];
			entity.Total = (reader.IsDBNull(((int)OrderColumn.Total - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.Total - 1)];
			entity.OrderDate = (reader.IsDBNull(((int)OrderColumn.OrderDate - 1)))?null:(System.DateTime?)reader[((int)OrderColumn.OrderDate - 1)];
			entity.Custom1 = (reader.IsDBNull(((int)OrderColumn.Custom1 - 1)))?null:(System.String)reader[((int)OrderColumn.Custom1 - 1)];
			entity.Custom2 = (reader.IsDBNull(((int)OrderColumn.Custom2 - 1)))?null:(System.String)reader[((int)OrderColumn.Custom2 - 1)];
			entity.AdditionalInstructions = (reader.IsDBNull(((int)OrderColumn.AdditionalInstructions - 1)))?null:(System.String)reader[((int)OrderColumn.AdditionalInstructions - 1)];
			entity.Custom3 = (reader.IsDBNull(((int)OrderColumn.Custom3 - 1)))?null:(System.String)reader[((int)OrderColumn.Custom3 - 1)];
			entity.TrackingNumber = (reader.IsDBNull(((int)OrderColumn.TrackingNumber - 1)))?null:(System.String)reader[((int)OrderColumn.TrackingNumber - 1)];
			entity.CouponCode = (reader.IsDBNull(((int)OrderColumn.CouponCode - 1)))?null:(System.String)reader[((int)OrderColumn.CouponCode - 1)];
			entity.PromoDescription = (reader.IsDBNull(((int)OrderColumn.PromoDescription - 1)))?null:(System.String)reader[((int)OrderColumn.PromoDescription - 1)];
			entity.ReferralAccountID = (reader.IsDBNull(((int)OrderColumn.ReferralAccountID - 1)))?null:(System.Int32?)reader[((int)OrderColumn.ReferralAccountID - 1)];
			entity.PurchaseOrderNumber = (reader.IsDBNull(((int)OrderColumn.PurchaseOrderNumber - 1)))?null:(System.String)reader[((int)OrderColumn.PurchaseOrderNumber - 1)];
			entity.PaymentStatusID = (reader.IsDBNull(((int)OrderColumn.PaymentStatusID - 1)))?null:(System.Int32?)reader[((int)OrderColumn.PaymentStatusID - 1)];
			entity.WebServiceDownloadDate = (reader.IsDBNull(((int)OrderColumn.WebServiceDownloadDate - 1)))?null:(System.DateTime?)reader[((int)OrderColumn.WebServiceDownloadDate - 1)];
			entity.PaymentSettingID = (reader.IsDBNull(((int)OrderColumn.PaymentSettingID - 1)))?null:(System.Int32?)reader[((int)OrderColumn.PaymentSettingID - 1)];
			entity.ShipDate = (reader.IsDBNull(((int)OrderColumn.ShipDate - 1)))?null:(System.DateTime?)reader[((int)OrderColumn.ShipDate - 1)];
			entity.ReturnDate = (reader.IsDBNull(((int)OrderColumn.ReturnDate - 1)))?null:(System.DateTime?)reader[((int)OrderColumn.ReturnDate - 1)];
			entity.SalesTax = (reader.IsDBNull(((int)OrderColumn.SalesTax - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.SalesTax - 1)];
			entity.VAT = (reader.IsDBNull(((int)OrderColumn.VAT - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.VAT - 1)];
			entity.GST = (reader.IsDBNull(((int)OrderColumn.GST - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.GST - 1)];
			entity.PST = (reader.IsDBNull(((int)OrderColumn.PST - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.PST - 1)];
			entity.HST = (reader.IsDBNull(((int)OrderColumn.HST - 1)))?null:(System.Decimal?)reader[((int)OrderColumn.HST - 1)];
			entity.Created = (reader.IsDBNull(((int)OrderColumn.Created - 1)))?null:(System.DateTime?)reader[((int)OrderColumn.Created - 1)];
			entity.CreatedBy = (reader.IsDBNull(((int)OrderColumn.CreatedBy - 1)))?null:(System.String)reader[((int)OrderColumn.CreatedBy - 1)];
			entity.Modified = (reader.IsDBNull(((int)OrderColumn.Modified - 1)))?null:(System.DateTime?)reader[((int)OrderColumn.Modified - 1)];
			entity.ModifiedBy = (reader.IsDBNull(((int)OrderColumn.ModifiedBy - 1)))?null:(System.String)reader[((int)OrderColumn.ModifiedBy - 1)];
			entity.ExternalID = (reader.IsDBNull(((int)OrderColumn.ExternalID - 1)))?null:(System.String)reader[((int)OrderColumn.ExternalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Order"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Order"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Order entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.OrderID = (System.Int32)dataRow["OrderID"];
			entity.PortalId = Convert.IsDBNull(dataRow["PortalId"]) ? null : (System.Int32?)dataRow["PortalId"];
			entity.AccountID = Convert.IsDBNull(dataRow["AccountID"]) ? null : (System.Int32?)dataRow["AccountID"];
			entity.OrderStateID = Convert.IsDBNull(dataRow["OrderStateID"]) ? null : (System.Int32?)dataRow["OrderStateID"];
			entity.ShippingID = Convert.IsDBNull(dataRow["ShippingID"]) ? null : (System.Int32?)dataRow["ShippingID"];
			entity.PaymentTypeId = Convert.IsDBNull(dataRow["PaymentTypeId"]) ? null : (System.Int32?)dataRow["PaymentTypeId"];
			entity.BillingFirstName = Convert.IsDBNull(dataRow["BillingFirstName"]) ? null : (System.String)dataRow["BillingFirstName"];
			entity.BillingLastName = Convert.IsDBNull(dataRow["BillingLastName"]) ? null : (System.String)dataRow["BillingLastName"];
			entity.BillingCompanyName = Convert.IsDBNull(dataRow["BillingCompanyName"]) ? null : (System.String)dataRow["BillingCompanyName"];
			entity.BillingStreet = Convert.IsDBNull(dataRow["BillingStreet"]) ? null : (System.String)dataRow["BillingStreet"];
			entity.BillingStreet1 = Convert.IsDBNull(dataRow["BillingStreet1"]) ? null : (System.String)dataRow["BillingStreet1"];
			entity.BillingCity = Convert.IsDBNull(dataRow["BillingCity"]) ? null : (System.String)dataRow["BillingCity"];
			entity.BillingStateCode = Convert.IsDBNull(dataRow["BillingStateCode"]) ? null : (System.String)dataRow["BillingStateCode"];
			entity.BillingPostalCode = Convert.IsDBNull(dataRow["BillingPostalCode"]) ? null : (System.String)dataRow["BillingPostalCode"];
			entity.BillingCountry = Convert.IsDBNull(dataRow["BillingCountry"]) ? null : (System.String)dataRow["BillingCountry"];
			entity.BillingPhoneNumber = Convert.IsDBNull(dataRow["BillingPhoneNumber"]) ? null : (System.String)dataRow["BillingPhoneNumber"];
			entity.BillingEmailId = Convert.IsDBNull(dataRow["BillingEmailId"]) ? null : (System.String)dataRow["BillingEmailId"];
			entity.CardTransactionID = Convert.IsDBNull(dataRow["CardTransactionID"]) ? null : (System.String)dataRow["CardTransactionID"];
			entity.CardAuthCode = Convert.IsDBNull(dataRow["CardAuthCode"]) ? null : (System.String)dataRow["CardAuthCode"];
			entity.CardTypeId = Convert.IsDBNull(dataRow["CardTypeId"]) ? null : (System.Int32?)dataRow["CardTypeId"];
			entity.CardExp = Convert.IsDBNull(dataRow["CardExp"]) ? null : (System.String)dataRow["CardExp"];
			entity.TaxCost = Convert.IsDBNull(dataRow["TaxCost"]) ? null : (System.Decimal?)dataRow["TaxCost"];
			entity.ShippingCost = Convert.IsDBNull(dataRow["ShippingCost"]) ? null : (System.Decimal?)dataRow["ShippingCost"];
			entity.SubTotal = Convert.IsDBNull(dataRow["SubTotal"]) ? null : (System.Decimal?)dataRow["SubTotal"];
			entity.DiscountAmount = Convert.IsDBNull(dataRow["DiscountAmount"]) ? null : (System.Decimal?)dataRow["DiscountAmount"];
			entity.Total = Convert.IsDBNull(dataRow["Total"]) ? null : (System.Decimal?)dataRow["Total"];
			entity.OrderDate = Convert.IsDBNull(dataRow["OrderDate"]) ? null : (System.DateTime?)dataRow["OrderDate"];
			entity.Custom1 = Convert.IsDBNull(dataRow["Custom1"]) ? null : (System.String)dataRow["Custom1"];
			entity.Custom2 = Convert.IsDBNull(dataRow["Custom2"]) ? null : (System.String)dataRow["Custom2"];
			entity.AdditionalInstructions = Convert.IsDBNull(dataRow["AdditionalInstructions"]) ? null : (System.String)dataRow["AdditionalInstructions"];
			entity.Custom3 = Convert.IsDBNull(dataRow["Custom3"]) ? null : (System.String)dataRow["Custom3"];
			entity.TrackingNumber = Convert.IsDBNull(dataRow["TrackingNumber"]) ? null : (System.String)dataRow["TrackingNumber"];
			entity.CouponCode = Convert.IsDBNull(dataRow["CouponCode"]) ? null : (System.String)dataRow["CouponCode"];
			entity.PromoDescription = Convert.IsDBNull(dataRow["PromoDescription"]) ? null : (System.String)dataRow["PromoDescription"];
			entity.ReferralAccountID = Convert.IsDBNull(dataRow["ReferralAccountID"]) ? null : (System.Int32?)dataRow["ReferralAccountID"];
			entity.PurchaseOrderNumber = Convert.IsDBNull(dataRow["PurchaseOrderNumber"]) ? null : (System.String)dataRow["PurchaseOrderNumber"];
			entity.PaymentStatusID = Convert.IsDBNull(dataRow["PaymentStatusID"]) ? null : (System.Int32?)dataRow["PaymentStatusID"];
			entity.WebServiceDownloadDate = Convert.IsDBNull(dataRow["WebServiceDownloadDate"]) ? null : (System.DateTime?)dataRow["WebServiceDownloadDate"];
			entity.PaymentSettingID = Convert.IsDBNull(dataRow["PaymentSettingID"]) ? null : (System.Int32?)dataRow["PaymentSettingID"];
			entity.ShipDate = Convert.IsDBNull(dataRow["ShipDate"]) ? null : (System.DateTime?)dataRow["ShipDate"];
			entity.ReturnDate = Convert.IsDBNull(dataRow["ReturnDate"]) ? null : (System.DateTime?)dataRow["ReturnDate"];
			entity.SalesTax = Convert.IsDBNull(dataRow["SalesTax"]) ? null : (System.Decimal?)dataRow["SalesTax"];
			entity.VAT = Convert.IsDBNull(dataRow["VAT"]) ? null : (System.Decimal?)dataRow["VAT"];
			entity.GST = Convert.IsDBNull(dataRow["GST"]) ? null : (System.Decimal?)dataRow["GST"];
			entity.PST = Convert.IsDBNull(dataRow["PST"]) ? null : (System.Decimal?)dataRow["PST"];
			entity.HST = Convert.IsDBNull(dataRow["HST"]) ? null : (System.Decimal?)dataRow["HST"];
			entity.Created = Convert.IsDBNull(dataRow["Created"]) ? null : (System.DateTime?)dataRow["Created"];
			entity.CreatedBy = Convert.IsDBNull(dataRow["CreatedBy"]) ? null : (System.String)dataRow["CreatedBy"];
			entity.Modified = Convert.IsDBNull(dataRow["Modified"]) ? null : (System.DateTime?)dataRow["Modified"];
			entity.ModifiedBy = Convert.IsDBNull(dataRow["ModifiedBy"]) ? null : (System.String)dataRow["ModifiedBy"];
			entity.ExternalID = Convert.IsDBNull(dataRow["ExternalID"]) ? null : (System.String)dataRow["ExternalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Order"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Order Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Order entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AccountIDSource	
			if (CanDeepLoad(entity, "Account|AccountIDSource", deepLoadType, innerList) 
				&& entity.AccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.AccountID ?? (int)0);
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AccountIDSource = tmpEntity;
				else
					entity.AccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, (entity.AccountID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.AccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AccountIDSource

			#region PortalIdSource	
			if (CanDeepLoad(entity, "Portal|PortalIdSource", deepLoadType, innerList) 
				&& entity.PortalIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PortalId ?? (int)0);
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIdSource = tmpEntity;
				else
					entity.PortalIdSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, (entity.PortalId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIdSource

			#region ShippingIDSource	
			if (CanDeepLoad(entity, "Shipping|ShippingIDSource", deepLoadType, innerList) 
				&& entity.ShippingIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ShippingID ?? (int)0);
				Shipping tmpEntity = EntityManager.LocateEntity<Shipping>(EntityLocator.ConstructKeyFromPkItems(typeof(Shipping), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ShippingIDSource = tmpEntity;
				else
					entity.ShippingIDSource = DataRepository.ShippingProvider.GetByShippingID(transactionManager, (entity.ShippingID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ShippingIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ShippingIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ShippingProvider.DeepLoad(transactionManager, entity.ShippingIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ShippingIDSource

			#region ReferralAccountIDSource	
			if (CanDeepLoad(entity, "Account|ReferralAccountIDSource", deepLoadType, innerList) 
				&& entity.ReferralAccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.ReferralAccountID ?? (int)0);
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ReferralAccountIDSource = tmpEntity;
				else
					entity.ReferralAccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, (entity.ReferralAccountID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ReferralAccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ReferralAccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.ReferralAccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ReferralAccountIDSource

			#region OrderIDSource	
			if (CanDeepLoad(entity, "Order|OrderIDSource", deepLoadType, innerList) 
				&& entity.OrderIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.OrderID;
				Order tmpEntity = EntityManager.LocateEntity<Order>(EntityLocator.ConstructKeyFromPkItems(typeof(Order), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.OrderIDSource = tmpEntity;
				else
					entity.OrderIDSource = DataRepository.OrderProvider.GetByOrderID(transactionManager, entity.OrderID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.OrderIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.OrderProvider.DeepLoad(transactionManager, entity.OrderIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion OrderIDSource

			#region PaymentSettingIDSource	
			if (CanDeepLoad(entity, "PaymentSetting|PaymentSettingIDSource", deepLoadType, innerList) 
				&& entity.PaymentSettingIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PaymentSettingID ?? (int)0);
				PaymentSetting tmpEntity = EntityManager.LocateEntity<PaymentSetting>(EntityLocator.ConstructKeyFromPkItems(typeof(PaymentSetting), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PaymentSettingIDSource = tmpEntity;
				else
					entity.PaymentSettingIDSource = DataRepository.PaymentSettingProvider.GetByPaymentSettingID(transactionManager, (entity.PaymentSettingID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PaymentSettingIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PaymentSettingIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PaymentSettingProvider.DeepLoad(transactionManager, entity.PaymentSettingIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PaymentSettingIDSource

			#region PaymentStatusIDSource	
			if (CanDeepLoad(entity, "PaymentStatus|PaymentStatusIDSource", deepLoadType, innerList) 
				&& entity.PaymentStatusIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PaymentStatusID ?? (int)0);
				PaymentStatus tmpEntity = EntityManager.LocateEntity<PaymentStatus>(EntityLocator.ConstructKeyFromPkItems(typeof(PaymentStatus), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PaymentStatusIDSource = tmpEntity;
				else
					entity.PaymentStatusIDSource = DataRepository.PaymentStatusProvider.GetByPaymentStatusID(transactionManager, (entity.PaymentStatusID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PaymentStatusIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PaymentStatusIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PaymentStatusProvider.DeepLoad(transactionManager, entity.PaymentStatusIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PaymentStatusIDSource

			#region OrderStateIDSource	
			if (CanDeepLoad(entity, "OrderState|OrderStateIDSource", deepLoadType, innerList) 
				&& entity.OrderStateIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.OrderStateID ?? (int)0);
				OrderState tmpEntity = EntityManager.LocateEntity<OrderState>(EntityLocator.ConstructKeyFromPkItems(typeof(OrderState), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.OrderStateIDSource = tmpEntity;
				else
					entity.OrderStateIDSource = DataRepository.OrderStateProvider.GetByOrderStateID(transactionManager, (entity.OrderStateID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderStateIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.OrderStateIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.OrderStateProvider.DeepLoad(transactionManager, entity.OrderStateIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion OrderStateIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByOrderID methods when available
			
			#region AccountPaymentCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<AccountPayment>|AccountPaymentCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountPaymentCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AccountPaymentCollection = DataRepository.AccountPaymentProvider.GetByOrderID(transactionManager, entity.OrderID);

				if (deep && entity.AccountPaymentCollection.Count > 0)
				{
					deepHandles.Add("AccountPaymentCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<AccountPayment>) DataRepository.AccountPaymentProvider.DeepLoad,
						new object[] { transactionManager, entity.AccountPaymentCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ReferralCommissionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ReferralCommission>|ReferralCommissionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ReferralCommissionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ReferralCommissionCollection = DataRepository.ReferralCommissionProvider.GetByOrderID(transactionManager, entity.OrderID);

				if (deep && entity.ReferralCommissionCollection.Count > 0)
				{
					deepHandles.Add("ReferralCommissionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ReferralCommission>) DataRepository.ReferralCommissionProvider.DeepLoad,
						new object[] { transactionManager, entity.ReferralCommissionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region OrderLineItemCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<OrderLineItem>|OrderLineItemCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderLineItemCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OrderLineItemCollection = DataRepository.OrderLineItemProvider.GetByOrderID(transactionManager, entity.OrderID);

				if (deep && entity.OrderLineItemCollection.Count > 0)
				{
					deepHandles.Add("OrderLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<OrderLineItem>) DataRepository.OrderLineItemProvider.DeepLoad,
						new object[] { transactionManager, entity.OrderLineItemCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region GiftCardHistoryCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<GiftCardHistory>|GiftCardHistoryCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'GiftCardHistoryCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.GiftCardHistoryCollection = DataRepository.GiftCardHistoryProvider.GetByOrderID(transactionManager, entity.OrderID);

				if (deep && entity.GiftCardHistoryCollection.Count > 0)
				{
					deepHandles.Add("GiftCardHistoryCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<GiftCardHistory>) DataRepository.GiftCardHistoryProvider.DeepLoad,
						new object[] { transactionManager, entity.GiftCardHistoryCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region Order
			// RelationshipType.OneToOne
			if (CanDeepLoad(entity, "Order|Order", deepLoadType, innerList))
			{
				entity.Order = DataRepository.OrderProvider.GetByOrderID(transactionManager, entity.OrderID);
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'Order' loaded. key " + entity.EntityTrackingKey);
				#endif 

				if (deep && entity.Order != null)
				{
					deepHandles.Add("Order",
						new KeyValuePair<Delegate, object>((DeepLoadSingleHandle< Order >) DataRepository.OrderProvider.DeepLoad,
						new object[] { transactionManager, entity.Order, deep, deepLoadType, childTypes, innerList }
					));
				}
			}
			#endregion 
			
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Order object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Order instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Order Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Order entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AccountIDSource
			if (CanDeepSave(entity, "Account|AccountIDSource", deepSaveType, innerList) 
				&& entity.AccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.AccountIDSource);
				entity.AccountID = entity.AccountIDSource.AccountID;
			}
			#endregion 
			
			#region PortalIdSource
			if (CanDeepSave(entity, "Portal|PortalIdSource", deepSaveType, innerList) 
				&& entity.PortalIdSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIdSource);
				entity.PortalId = entity.PortalIdSource.PortalID;
			}
			#endregion 
			
			#region ShippingIDSource
			if (CanDeepSave(entity, "Shipping|ShippingIDSource", deepSaveType, innerList) 
				&& entity.ShippingIDSource != null)
			{
				DataRepository.ShippingProvider.Save(transactionManager, entity.ShippingIDSource);
				entity.ShippingID = entity.ShippingIDSource.ShippingID;
			}
			#endregion 
			
			#region ReferralAccountIDSource
			if (CanDeepSave(entity, "Account|ReferralAccountIDSource", deepSaveType, innerList) 
				&& entity.ReferralAccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.ReferralAccountIDSource);
				entity.ReferralAccountID = entity.ReferralAccountIDSource.AccountID;
			}
			#endregion 
			
			#region OrderIDSource
			if (CanDeepSave(entity, "Order|OrderIDSource", deepSaveType, innerList) 
				&& entity.OrderIDSource != null)
			{
				DataRepository.OrderProvider.Save(transactionManager, entity.OrderIDSource);
				entity.OrderID = entity.OrderIDSource.OrderID;
			}
			#endregion 
			
			#region PaymentSettingIDSource
			if (CanDeepSave(entity, "PaymentSetting|PaymentSettingIDSource", deepSaveType, innerList) 
				&& entity.PaymentSettingIDSource != null)
			{
				DataRepository.PaymentSettingProvider.Save(transactionManager, entity.PaymentSettingIDSource);
				entity.PaymentSettingID = entity.PaymentSettingIDSource.PaymentSettingID;
			}
			#endregion 
			
			#region PaymentStatusIDSource
			if (CanDeepSave(entity, "PaymentStatus|PaymentStatusIDSource", deepSaveType, innerList) 
				&& entity.PaymentStatusIDSource != null)
			{
				DataRepository.PaymentStatusProvider.Save(transactionManager, entity.PaymentStatusIDSource);
				entity.PaymentStatusID = entity.PaymentStatusIDSource.PaymentStatusID;
			}
			#endregion 
			
			#region OrderStateIDSource
			if (CanDeepSave(entity, "OrderState|OrderStateIDSource", deepSaveType, innerList) 
				&& entity.OrderStateIDSource != null)
			{
				DataRepository.OrderStateProvider.Save(transactionManager, entity.OrderStateIDSource);
				entity.OrderStateID = entity.OrderStateIDSource.OrderStateID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();

			#region Order
			if (CanDeepSave(entity.Order, "Order|Order", deepSaveType, innerList))
			{

				if (entity.Order != null)
				{
					// update each child parent id with the real parent id (mostly used on insert)

					entity.Order.OrderID = entity.OrderID;
					//DataRepository.OrderProvider.Save(transactionManager, entity.Order);
					deepHandles.Add("Order",
						new KeyValuePair<Delegate, object>((DeepSaveSingleHandle< Order >) DataRepository.OrderProvider.DeepSave,
						new object[] { transactionManager, entity.Order, deepSaveType, childTypes, innerList }
					));
				}
			} 
			#endregion 
	
			#region List<AccountPayment>
				if (CanDeepSave(entity.AccountPaymentCollection, "List<AccountPayment>|AccountPaymentCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(AccountPayment child in entity.AccountPaymentCollection)
					{
						if(child.OrderIDSource != null)
						{
							child.OrderID = child.OrderIDSource.OrderID;
						}
						else
						{
							child.OrderID = entity.OrderID;
						}

					}

					if (entity.AccountPaymentCollection.Count > 0 || entity.AccountPaymentCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AccountPaymentProvider.Save(transactionManager, entity.AccountPaymentCollection);
						
						deepHandles.Add("AccountPaymentCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< AccountPayment >) DataRepository.AccountPaymentProvider.DeepSave,
							new object[] { transactionManager, entity.AccountPaymentCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ReferralCommission>
				if (CanDeepSave(entity.ReferralCommissionCollection, "List<ReferralCommission>|ReferralCommissionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ReferralCommission child in entity.ReferralCommissionCollection)
					{
						if(child.OrderIDSource != null)
						{
							child.OrderID = child.OrderIDSource.OrderID;
						}
						else
						{
							child.OrderID = entity.OrderID;
						}

					}

					if (entity.ReferralCommissionCollection.Count > 0 || entity.ReferralCommissionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ReferralCommissionProvider.Save(transactionManager, entity.ReferralCommissionCollection);
						
						deepHandles.Add("ReferralCommissionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ReferralCommission >) DataRepository.ReferralCommissionProvider.DeepSave,
							new object[] { transactionManager, entity.ReferralCommissionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<OrderLineItem>
				if (CanDeepSave(entity.OrderLineItemCollection, "List<OrderLineItem>|OrderLineItemCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(OrderLineItem child in entity.OrderLineItemCollection)
					{
						if(child.OrderIDSource != null)
						{
							child.OrderID = child.OrderIDSource.OrderID;
						}
						else
						{
							child.OrderID = entity.OrderID;
						}

					}

					if (entity.OrderLineItemCollection.Count > 0 || entity.OrderLineItemCollection.DeletedItems.Count > 0)
					{
						//DataRepository.OrderLineItemProvider.Save(transactionManager, entity.OrderLineItemCollection);
						
						deepHandles.Add("OrderLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< OrderLineItem >) DataRepository.OrderLineItemProvider.DeepSave,
							new object[] { transactionManager, entity.OrderLineItemCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<GiftCardHistory>
				if (CanDeepSave(entity.GiftCardHistoryCollection, "List<GiftCardHistory>|GiftCardHistoryCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(GiftCardHistory child in entity.GiftCardHistoryCollection)
					{
						if(child.OrderIDSource != null)
						{
							child.OrderID = child.OrderIDSource.OrderID;
						}
						else
						{
							child.OrderID = entity.OrderID;
						}

					}

					if (entity.GiftCardHistoryCollection.Count > 0 || entity.GiftCardHistoryCollection.DeletedItems.Count > 0)
					{
						//DataRepository.GiftCardHistoryProvider.Save(transactionManager, entity.GiftCardHistoryCollection);
						
						deepHandles.Add("GiftCardHistoryCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< GiftCardHistory >) DataRepository.GiftCardHistoryProvider.DeepSave,
							new object[] { transactionManager, entity.GiftCardHistoryCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region OrderChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Order</c>
	///</summary>
	public enum OrderChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Account</c> at AccountIDSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIdSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		
		///<summary>
		/// Composite Property for <c>Shipping</c> at ShippingIDSource
		///</summary>
		[ChildEntityType(typeof(Shipping))]
		Shipping,
		
		///<summary>
		/// Composite Property for <c>Order</c> at OrderIDSource
		///</summary>
		[ChildEntityType(typeof(Order))]
		Order,
		
		///<summary>
		/// Composite Property for <c>PaymentSetting</c> at PaymentSettingIDSource
		///</summary>
		[ChildEntityType(typeof(PaymentSetting))]
		PaymentSetting,
		
		///<summary>
		/// Composite Property for <c>PaymentStatus</c> at PaymentStatusIDSource
		///</summary>
		[ChildEntityType(typeof(PaymentStatus))]
		PaymentStatus,
		
		///<summary>
		/// Composite Property for <c>OrderState</c> at OrderStateIDSource
		///</summary>
		[ChildEntityType(typeof(OrderState))]
		OrderState,
		///<summary>
		/// Collection of <c>Order</c> as OneToMany for AccountPaymentCollection
		///</summary>
		[ChildEntityType(typeof(TList<AccountPayment>))]
		AccountPaymentCollection,
		///<summary>
		/// Collection of <c>Order</c> as OneToMany for ReferralCommissionCollection
		///</summary>
		[ChildEntityType(typeof(TList<ReferralCommission>))]
		ReferralCommissionCollection,
		///<summary>
		/// Collection of <c>Order</c> as OneToMany for OrderLineItemCollection
		///</summary>
		[ChildEntityType(typeof(TList<OrderLineItem>))]
		OrderLineItemCollection,
		///<summary>
		/// Collection of <c>Order</c> as OneToMany for GiftCardHistoryCollection
		///</summary>
		[ChildEntityType(typeof(TList<GiftCardHistory>))]
		GiftCardHistoryCollection,
	}
	
	#endregion OrderChildEntityTypes
	
	#region OrderFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;OrderColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Order"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderFilterBuilder : SqlFilterBuilder<OrderColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderFilterBuilder class.
		/// </summary>
		public OrderFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderFilterBuilder
	
	#region OrderParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;OrderColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Order"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderParameterBuilder : ParameterizedSqlFilterBuilder<OrderColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderParameterBuilder class.
		/// </summary>
		public OrderParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderParameterBuilder
	
	#region OrderSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;OrderColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Order"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class OrderSortBuilder : SqlSortBuilder<OrderColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderSqlSortBuilder class.
		/// </summary>
		public OrderSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion OrderSortBuilder
	
} // end namespace
