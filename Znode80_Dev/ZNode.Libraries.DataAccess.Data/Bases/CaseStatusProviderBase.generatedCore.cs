﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CaseStatusProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CaseStatusProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.CaseStatus, ZNode.Libraries.DataAccess.Entities.CaseStatusKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CaseStatusKey key)
		{
			return Delete(transactionManager, key.CaseStatusID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_caseStatusID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _caseStatusID)
		{
			return Delete(null, _caseStatusID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseStatusID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _caseStatusID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.CaseStatus Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CaseStatusKey key, int start, int pageLength)
		{
			return GetByCaseStatusID(transactionManager, key.CaseStatusID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_CaseStatus index.
		/// </summary>
		/// <param name="_caseStatusID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CaseStatus"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CaseStatus GetByCaseStatusID(System.Int32 _caseStatusID)
		{
			int count = -1;
			return GetByCaseStatusID(null,_caseStatusID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CaseStatus index.
		/// </summary>
		/// <param name="_caseStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CaseStatus"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CaseStatus GetByCaseStatusID(System.Int32 _caseStatusID, int start, int pageLength)
		{
			int count = -1;
			return GetByCaseStatusID(null, _caseStatusID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CaseStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseStatusID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CaseStatus"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CaseStatus GetByCaseStatusID(TransactionManager transactionManager, System.Int32 _caseStatusID)
		{
			int count = -1;
			return GetByCaseStatusID(transactionManager, _caseStatusID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CaseStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CaseStatus"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CaseStatus GetByCaseStatusID(TransactionManager transactionManager, System.Int32 _caseStatusID, int start, int pageLength)
		{
			int count = -1;
			return GetByCaseStatusID(transactionManager, _caseStatusID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CaseStatus index.
		/// </summary>
		/// <param name="_caseStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CaseStatus"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.CaseStatus GetByCaseStatusID(System.Int32 _caseStatusID, int start, int pageLength, out int count)
		{
			return GetByCaseStatusID(null, _caseStatusID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_CaseStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.CaseStatus"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.CaseStatus GetByCaseStatusID(TransactionManager transactionManager, System.Int32 _caseStatusID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;CaseStatus&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;CaseStatus&gt;"/></returns>
		public static TList<CaseStatus> Fill(IDataReader reader, TList<CaseStatus> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.CaseStatus c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("CaseStatus")
					.Append("|").Append((System.Int32)reader[((int)CaseStatusColumn.CaseStatusID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<CaseStatus>(
					key.ToString(), // EntityTrackingKey
					"CaseStatus",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.CaseStatus();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.CaseStatusID = (System.Int32)reader[((int)CaseStatusColumn.CaseStatusID - 1)];
					c.OriginalCaseStatusID = c.CaseStatusID;
					c.CaseStatusNme = (System.String)reader[((int)CaseStatusColumn.CaseStatusNme - 1)];
					c.ViewOrder = (System.Int32)reader[((int)CaseStatusColumn.ViewOrder - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CaseStatus"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CaseStatus"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.CaseStatus entity)
		{
			if (!reader.Read()) return;
			
			entity.CaseStatusID = (System.Int32)reader[((int)CaseStatusColumn.CaseStatusID - 1)];
			entity.OriginalCaseStatusID = (System.Int32)reader["CaseStatusID"];
			entity.CaseStatusNme = (System.String)reader[((int)CaseStatusColumn.CaseStatusNme - 1)];
			entity.ViewOrder = (System.Int32)reader[((int)CaseStatusColumn.ViewOrder - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.CaseStatus"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CaseStatus"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.CaseStatus entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.CaseStatusID = (System.Int32)dataRow["CaseStatusID"];
			entity.OriginalCaseStatusID = (System.Int32)dataRow["CaseStatusID"];
			entity.CaseStatusNme = (System.String)dataRow["CaseStatusNme"];
			entity.ViewOrder = (System.Int32)dataRow["ViewOrder"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.CaseStatus"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CaseStatus Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CaseStatus entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCaseStatusID methods when available
			
			#region CaseRequestCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CaseRequest>|CaseRequestCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CaseRequestCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CaseRequestCollection = DataRepository.CaseRequestProvider.GetByCaseStatusID(transactionManager, entity.CaseStatusID);

				if (deep && entity.CaseRequestCollection.Count > 0)
				{
					deepHandles.Add("CaseRequestCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CaseRequest>) DataRepository.CaseRequestProvider.DeepLoad,
						new object[] { transactionManager, entity.CaseRequestCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.CaseStatus object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.CaseStatus instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.CaseStatus Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CaseStatus entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<CaseRequest>
				if (CanDeepSave(entity.CaseRequestCollection, "List<CaseRequest>|CaseRequestCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CaseRequest child in entity.CaseRequestCollection)
					{
						if(child.CaseStatusIDSource != null)
						{
							child.CaseStatusID = child.CaseStatusIDSource.CaseStatusID;
						}
						else
						{
							child.CaseStatusID = entity.CaseStatusID;
						}

					}

					if (entity.CaseRequestCollection.Count > 0 || entity.CaseRequestCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CaseRequestProvider.Save(transactionManager, entity.CaseRequestCollection);
						
						deepHandles.Add("CaseRequestCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CaseRequest >) DataRepository.CaseRequestProvider.DeepSave,
							new object[] { transactionManager, entity.CaseRequestCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CaseStatusChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.CaseStatus</c>
	///</summary>
	public enum CaseStatusChildEntityTypes
	{
		///<summary>
		/// Collection of <c>CaseStatus</c> as OneToMany for CaseRequestCollection
		///</summary>
		[ChildEntityType(typeof(TList<CaseRequest>))]
		CaseRequestCollection,
	}
	
	#endregion CaseStatusChildEntityTypes
	
	#region CaseStatusFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CaseStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CaseStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CaseStatusFilterBuilder : SqlFilterBuilder<CaseStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CaseStatusFilterBuilder class.
		/// </summary>
		public CaseStatusFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CaseStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CaseStatusFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CaseStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CaseStatusFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CaseStatusFilterBuilder
	
	#region CaseStatusParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CaseStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CaseStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CaseStatusParameterBuilder : ParameterizedSqlFilterBuilder<CaseStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CaseStatusParameterBuilder class.
		/// </summary>
		public CaseStatusParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CaseStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CaseStatusParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CaseStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CaseStatusParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CaseStatusParameterBuilder
	
	#region CaseStatusSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CaseStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="CaseStatus"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CaseStatusSortBuilder : SqlSortBuilder<CaseStatusColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CaseStatusSqlSortBuilder class.
		/// </summary>
		public CaseStatusSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CaseStatusSortBuilder
	
} // end namespace
