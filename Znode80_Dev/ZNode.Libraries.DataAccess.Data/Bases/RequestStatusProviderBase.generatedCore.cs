﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="RequestStatusProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class RequestStatusProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.RequestStatus, ZNode.Libraries.DataAccess.Entities.RequestStatusKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RequestStatusKey key)
		{
			return Delete(transactionManager, key.RequestStatusID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_requestStatusID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _requestStatusID)
		{
			return Delete(null, _requestStatusID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestStatusID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _requestStatusID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.RequestStatus Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RequestStatusKey key, int start, int pageLength)
		{
			return GetByRequestStatusID(transactionManager, key.RequestStatusID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeRMARequestStatus index.
		/// </summary>
		/// <param name="_requestStatusID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RequestStatus"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RequestStatus GetByRequestStatusID(System.Int32 _requestStatusID)
		{
			int count = -1;
			return GetByRequestStatusID(null,_requestStatusID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMARequestStatus index.
		/// </summary>
		/// <param name="_requestStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RequestStatus"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RequestStatus GetByRequestStatusID(System.Int32 _requestStatusID, int start, int pageLength)
		{
			int count = -1;
			return GetByRequestStatusID(null, _requestStatusID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMARequestStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestStatusID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RequestStatus"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RequestStatus GetByRequestStatusID(TransactionManager transactionManager, System.Int32 _requestStatusID)
		{
			int count = -1;
			return GetByRequestStatusID(transactionManager, _requestStatusID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMARequestStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RequestStatus"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RequestStatus GetByRequestStatusID(TransactionManager transactionManager, System.Int32 _requestStatusID, int start, int pageLength)
		{
			int count = -1;
			return GetByRequestStatusID(transactionManager, _requestStatusID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMARequestStatus index.
		/// </summary>
		/// <param name="_requestStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RequestStatus"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RequestStatus GetByRequestStatusID(System.Int32 _requestStatusID, int start, int pageLength, out int count)
		{
			return GetByRequestStatusID(null, _requestStatusID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMARequestStatus index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_requestStatusID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RequestStatus"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.RequestStatus GetByRequestStatusID(TransactionManager transactionManager, System.Int32 _requestStatusID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;RequestStatus&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;RequestStatus&gt;"/></returns>
		public static TList<RequestStatus> Fill(IDataReader reader, TList<RequestStatus> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.RequestStatus c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("RequestStatus")
					.Append("|").Append((System.Int32)reader[((int)RequestStatusColumn.RequestStatusID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<RequestStatus>(
					key.ToString(), // EntityTrackingKey
					"RequestStatus",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.RequestStatus();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.RequestStatusID = (System.Int32)reader[((int)RequestStatusColumn.RequestStatusID - 1)];
					c.Name = (System.String)reader[((int)RequestStatusColumn.Name - 1)];
					c.CustomerNotification = (reader.IsDBNull(((int)RequestStatusColumn.CustomerNotification - 1)))?null:(System.String)reader[((int)RequestStatusColumn.CustomerNotification - 1)];
					c.AdminNotification = (reader.IsDBNull(((int)RequestStatusColumn.AdminNotification - 1)))?null:(System.String)reader[((int)RequestStatusColumn.AdminNotification - 1)];
					c.IsEnabled = (reader.IsDBNull(((int)RequestStatusColumn.IsEnabled - 1)))?null:(System.Boolean?)reader[((int)RequestStatusColumn.IsEnabled - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.RequestStatus"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.RequestStatus"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.RequestStatus entity)
		{
			if (!reader.Read()) return;
			
			entity.RequestStatusID = (System.Int32)reader[((int)RequestStatusColumn.RequestStatusID - 1)];
			entity.Name = (System.String)reader[((int)RequestStatusColumn.Name - 1)];
			entity.CustomerNotification = (reader.IsDBNull(((int)RequestStatusColumn.CustomerNotification - 1)))?null:(System.String)reader[((int)RequestStatusColumn.CustomerNotification - 1)];
			entity.AdminNotification = (reader.IsDBNull(((int)RequestStatusColumn.AdminNotification - 1)))?null:(System.String)reader[((int)RequestStatusColumn.AdminNotification - 1)];
			entity.IsEnabled = (reader.IsDBNull(((int)RequestStatusColumn.IsEnabled - 1)))?null:(System.Boolean?)reader[((int)RequestStatusColumn.IsEnabled - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.RequestStatus"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.RequestStatus"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.RequestStatus entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.RequestStatusID = (System.Int32)dataRow["RequestStatusID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.CustomerNotification = Convert.IsDBNull(dataRow["CustomerNotification"]) ? null : (System.String)dataRow["CustomerNotification"];
			entity.AdminNotification = Convert.IsDBNull(dataRow["AdminNotification"]) ? null : (System.String)dataRow["AdminNotification"];
			entity.IsEnabled = Convert.IsDBNull(dataRow["IsEnabled"]) ? null : (System.Boolean?)dataRow["IsEnabled"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.RequestStatus"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.RequestStatus Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RequestStatus entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByRequestStatusID methods when available
			
			#region RMARequestCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<RMARequest>|RMARequestCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RMARequestCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.RMARequestCollection = DataRepository.RMARequestProvider.GetByRequestStatusID(transactionManager, entity.RequestStatusID);

				if (deep && entity.RMARequestCollection.Count > 0)
				{
					deepHandles.Add("RMARequestCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<RMARequest>) DataRepository.RMARequestProvider.DeepLoad,
						new object[] { transactionManager, entity.RMARequestCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.RequestStatus object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.RequestStatus instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.RequestStatus Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RequestStatus entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<RMARequest>
				if (CanDeepSave(entity.RMARequestCollection, "List<RMARequest>|RMARequestCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(RMARequest child in entity.RMARequestCollection)
					{
						if(child.RequestStatusIDSource != null)
						{
							child.RequestStatusID = child.RequestStatusIDSource.RequestStatusID;
						}
						else
						{
							child.RequestStatusID = entity.RequestStatusID;
						}

					}

					if (entity.RMARequestCollection.Count > 0 || entity.RMARequestCollection.DeletedItems.Count > 0)
					{
						//DataRepository.RMARequestProvider.Save(transactionManager, entity.RMARequestCollection);
						
						deepHandles.Add("RMARequestCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< RMARequest >) DataRepository.RMARequestProvider.DeepSave,
							new object[] { transactionManager, entity.RMARequestCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region RequestStatusChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.RequestStatus</c>
	///</summary>
	public enum RequestStatusChildEntityTypes
	{
		///<summary>
		/// Collection of <c>RequestStatus</c> as OneToMany for RMARequestCollection
		///</summary>
		[ChildEntityType(typeof(TList<RMARequest>))]
		RMARequestCollection,
	}
	
	#endregion RequestStatusChildEntityTypes
	
	#region RequestStatusFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;RequestStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RequestStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RequestStatusFilterBuilder : SqlFilterBuilder<RequestStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RequestStatusFilterBuilder class.
		/// </summary>
		public RequestStatusFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RequestStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RequestStatusFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RequestStatusFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RequestStatusFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RequestStatusFilterBuilder
	
	#region RequestStatusParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;RequestStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RequestStatus"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RequestStatusParameterBuilder : ParameterizedSqlFilterBuilder<RequestStatusColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RequestStatusParameterBuilder class.
		/// </summary>
		public RequestStatusParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RequestStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RequestStatusParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RequestStatusParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RequestStatusParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RequestStatusParameterBuilder
	
	#region RequestStatusSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;RequestStatusColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RequestStatus"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class RequestStatusSortBuilder : SqlSortBuilder<RequestStatusColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RequestStatusSqlSortBuilder class.
		/// </summary>
		public RequestStatusSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion RequestStatusSortBuilder
	
} // end namespace
