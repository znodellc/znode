﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TaxRuleProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TaxRuleProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.TaxRule, ZNode.Libraries.DataAccess.Entities.TaxRuleKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TaxRuleKey key)
		{
			return Delete(transactionManager, key.TaxRuleID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_taxRuleID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _taxRuleID)
		{
			return Delete(null, _taxRuleID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxRuleID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _taxRuleID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.TaxRule Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TaxRuleKey key, int start, int pageLength)
		{
			return GetByTaxRuleID(transactionManager, key.TaxRuleID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeTaxRule_DestinationCountryCode index.
		/// </summary>
		/// <param name="_destinationCountryCode"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByDestinationCountryCode(System.String _destinationCountryCode)
		{
			int count = -1;
			return GetByDestinationCountryCode(null,_destinationCountryCode, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_DestinationCountryCode index.
		/// </summary>
		/// <param name="_destinationCountryCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByDestinationCountryCode(System.String _destinationCountryCode, int start, int pageLength)
		{
			int count = -1;
			return GetByDestinationCountryCode(null, _destinationCountryCode, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_DestinationCountryCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_destinationCountryCode"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByDestinationCountryCode(TransactionManager transactionManager, System.String _destinationCountryCode)
		{
			int count = -1;
			return GetByDestinationCountryCode(transactionManager, _destinationCountryCode, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_DestinationCountryCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_destinationCountryCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByDestinationCountryCode(TransactionManager transactionManager, System.String _destinationCountryCode, int start, int pageLength)
		{
			int count = -1;
			return GetByDestinationCountryCode(transactionManager, _destinationCountryCode, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_DestinationCountryCode index.
		/// </summary>
		/// <param name="_destinationCountryCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByDestinationCountryCode(System.String _destinationCountryCode, int start, int pageLength, out int count)
		{
			return GetByDestinationCountryCode(null, _destinationCountryCode, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_DestinationCountryCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_destinationCountryCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public abstract TList<TaxRule> GetByDestinationCountryCode(TransactionManager transactionManager, System.String _destinationCountryCode, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeTaxRule_DestinationStateCode index.
		/// </summary>
		/// <param name="_destinationStateCode"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByDestinationStateCode(System.String _destinationStateCode)
		{
			int count = -1;
			return GetByDestinationStateCode(null,_destinationStateCode, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_DestinationStateCode index.
		/// </summary>
		/// <param name="_destinationStateCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByDestinationStateCode(System.String _destinationStateCode, int start, int pageLength)
		{
			int count = -1;
			return GetByDestinationStateCode(null, _destinationStateCode, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_DestinationStateCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_destinationStateCode"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByDestinationStateCode(TransactionManager transactionManager, System.String _destinationStateCode)
		{
			int count = -1;
			return GetByDestinationStateCode(transactionManager, _destinationStateCode, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_DestinationStateCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_destinationStateCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByDestinationStateCode(TransactionManager transactionManager, System.String _destinationStateCode, int start, int pageLength)
		{
			int count = -1;
			return GetByDestinationStateCode(transactionManager, _destinationStateCode, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_DestinationStateCode index.
		/// </summary>
		/// <param name="_destinationStateCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByDestinationStateCode(System.String _destinationStateCode, int start, int pageLength, out int count)
		{
			return GetByDestinationStateCode(null, _destinationStateCode, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_DestinationStateCode index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_destinationStateCode"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public abstract TList<TaxRule> GetByDestinationStateCode(TransactionManager transactionManager, System.String _destinationStateCode, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeTaxRule_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByExternalID(System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(null,_externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByExternalID(System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByExternalID(TransactionManager transactionManager, System.String _externalID)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength)
		{
			int count = -1;
			return GetByExternalID(transactionManager, _externalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_ExternalID index.
		/// </summary>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByExternalID(System.String _externalID, int start, int pageLength, out int count)
		{
			return GetByExternalID(null, _externalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_ExternalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_externalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public abstract TList<TaxRule> GetByExternalID(TransactionManager transactionManager, System.String _externalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeTaxRule_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByPortalID(System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(null,_portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByPortalID(System.Int32 _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByPortalID(System.Int32 _portalID, int start, int pageLength, out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public abstract TList<TaxRule> GetByPortalID(TransactionManager transactionManager, System.Int32 _portalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeTaxRule_TaxClassID index.
		/// </summary>
		/// <param name="_taxClassID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByTaxClassID(System.Int32? _taxClassID)
		{
			int count = -1;
			return GetByTaxClassID(null,_taxClassID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_TaxClassID index.
		/// </summary>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByTaxClassID(System.Int32? _taxClassID, int start, int pageLength)
		{
			int count = -1;
			return GetByTaxClassID(null, _taxClassID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_TaxClassID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByTaxClassID(TransactionManager transactionManager, System.Int32? _taxClassID)
		{
			int count = -1;
			return GetByTaxClassID(transactionManager, _taxClassID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_TaxClassID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByTaxClassID(TransactionManager transactionManager, System.Int32? _taxClassID, int start, int pageLength)
		{
			int count = -1;
			return GetByTaxClassID(transactionManager, _taxClassID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_TaxClassID index.
		/// </summary>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByTaxClassID(System.Int32? _taxClassID, int start, int pageLength, out int count)
		{
			return GetByTaxClassID(null, _taxClassID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_TaxClassID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxClassID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public abstract TList<TaxRule> GetByTaxClassID(TransactionManager transactionManager, System.Int32? _taxClassID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeTaxRule_TaxRuleTypeID index.
		/// </summary>
		/// <param name="_taxRuleTypeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByTaxRuleTypeID(System.Int32? _taxRuleTypeID)
		{
			int count = -1;
			return GetByTaxRuleTypeID(null,_taxRuleTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_TaxRuleTypeID index.
		/// </summary>
		/// <param name="_taxRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByTaxRuleTypeID(System.Int32? _taxRuleTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByTaxRuleTypeID(null, _taxRuleTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_TaxRuleTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxRuleTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByTaxRuleTypeID(TransactionManager transactionManager, System.Int32? _taxRuleTypeID)
		{
			int count = -1;
			return GetByTaxRuleTypeID(transactionManager, _taxRuleTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_TaxRuleTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByTaxRuleTypeID(TransactionManager transactionManager, System.Int32? _taxRuleTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByTaxRuleTypeID(transactionManager, _taxRuleTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_TaxRuleTypeID index.
		/// </summary>
		/// <param name="_taxRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public TList<TaxRule> GetByTaxRuleTypeID(System.Int32? _taxRuleTypeID, int start, int pageLength, out int count)
		{
			return GetByTaxRuleTypeID(null, _taxRuleTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRule_TaxRuleTypeID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRule&gt;"/> class.</returns>
		public abstract TList<TaxRule> GetByTaxRuleTypeID(TransactionManager transactionManager, System.Int32? _taxRuleTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SC_TaxRule index.
		/// </summary>
		/// <param name="_taxRuleID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRule"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxRule GetByTaxRuleID(System.Int32 _taxRuleID)
		{
			int count = -1;
			return GetByTaxRuleID(null,_taxRuleID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_TaxRule index.
		/// </summary>
		/// <param name="_taxRuleID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRule"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxRule GetByTaxRuleID(System.Int32 _taxRuleID, int start, int pageLength)
		{
			int count = -1;
			return GetByTaxRuleID(null, _taxRuleID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_TaxRule index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxRuleID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRule"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxRule GetByTaxRuleID(TransactionManager transactionManager, System.Int32 _taxRuleID)
		{
			int count = -1;
			return GetByTaxRuleID(transactionManager, _taxRuleID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_TaxRule index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxRuleID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRule"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxRule GetByTaxRuleID(TransactionManager transactionManager, System.Int32 _taxRuleID, int start, int pageLength)
		{
			int count = -1;
			return GetByTaxRuleID(transactionManager, _taxRuleID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_TaxRule index.
		/// </summary>
		/// <param name="_taxRuleID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRule"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxRule GetByTaxRuleID(System.Int32 _taxRuleID, int start, int pageLength, out int count)
		{
			return GetByTaxRuleID(null, _taxRuleID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_TaxRule index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxRuleID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRule"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.TaxRule GetByTaxRuleID(TransactionManager transactionManager, System.Int32 _taxRuleID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;TaxRule&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;TaxRule&gt;"/></returns>
		public static TList<TaxRule> Fill(IDataReader reader, TList<TaxRule> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.TaxRule c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("TaxRule")
					.Append("|").Append((System.Int32)reader[((int)TaxRuleColumn.TaxRuleID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<TaxRule>(
					key.ToString(), // EntityTrackingKey
					"TaxRule",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.TaxRule();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.TaxRuleID = (System.Int32)reader[((int)TaxRuleColumn.TaxRuleID - 1)];
					c.TaxRuleTypeID = (reader.IsDBNull(((int)TaxRuleColumn.TaxRuleTypeID - 1)))?null:(System.Int32?)reader[((int)TaxRuleColumn.TaxRuleTypeID - 1)];
					c.PortalID = (System.Int32)reader[((int)TaxRuleColumn.PortalID - 1)];
					c.TaxClassID = (reader.IsDBNull(((int)TaxRuleColumn.TaxClassID - 1)))?null:(System.Int32?)reader[((int)TaxRuleColumn.TaxClassID - 1)];
					c.DestinationCountryCode = (reader.IsDBNull(((int)TaxRuleColumn.DestinationCountryCode - 1)))?null:(System.String)reader[((int)TaxRuleColumn.DestinationCountryCode - 1)];
					c.DestinationStateCode = (reader.IsDBNull(((int)TaxRuleColumn.DestinationStateCode - 1)))?null:(System.String)reader[((int)TaxRuleColumn.DestinationStateCode - 1)];
					c.CountyFIPS = (reader.IsDBNull(((int)TaxRuleColumn.CountyFIPS - 1)))?null:(System.String)reader[((int)TaxRuleColumn.CountyFIPS - 1)];
					c.Precedence = (System.Int32)reader[((int)TaxRuleColumn.Precedence - 1)];
					c.SalesTax = (reader.IsDBNull(((int)TaxRuleColumn.SalesTax - 1)))?null:(System.Decimal?)reader[((int)TaxRuleColumn.SalesTax - 1)];
					c.VAT = (reader.IsDBNull(((int)TaxRuleColumn.VAT - 1)))?null:(System.Decimal?)reader[((int)TaxRuleColumn.VAT - 1)];
					c.GST = (reader.IsDBNull(((int)TaxRuleColumn.GST - 1)))?null:(System.Decimal?)reader[((int)TaxRuleColumn.GST - 1)];
					c.PST = (reader.IsDBNull(((int)TaxRuleColumn.PST - 1)))?null:(System.Decimal?)reader[((int)TaxRuleColumn.PST - 1)];
					c.HST = (reader.IsDBNull(((int)TaxRuleColumn.HST - 1)))?null:(System.Decimal?)reader[((int)TaxRuleColumn.HST - 1)];
					c.TaxShipping = (System.Boolean)reader[((int)TaxRuleColumn.TaxShipping - 1)];
					c.InclusiveInd = (System.Boolean)reader[((int)TaxRuleColumn.InclusiveInd - 1)];
					c.Custom1 = (reader.IsDBNull(((int)TaxRuleColumn.Custom1 - 1)))?null:(System.String)reader[((int)TaxRuleColumn.Custom1 - 1)];
					c.Custom2 = (reader.IsDBNull(((int)TaxRuleColumn.Custom2 - 1)))?null:(System.String)reader[((int)TaxRuleColumn.Custom2 - 1)];
					c.Custom3 = (reader.IsDBNull(((int)TaxRuleColumn.Custom3 - 1)))?null:(System.String)reader[((int)TaxRuleColumn.Custom3 - 1)];
					c.ExternalID = (reader.IsDBNull(((int)TaxRuleColumn.ExternalID - 1)))?null:(System.String)reader[((int)TaxRuleColumn.ExternalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRule"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.TaxRule"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.TaxRule entity)
		{
			if (!reader.Read()) return;
			
			entity.TaxRuleID = (System.Int32)reader[((int)TaxRuleColumn.TaxRuleID - 1)];
			entity.TaxRuleTypeID = (reader.IsDBNull(((int)TaxRuleColumn.TaxRuleTypeID - 1)))?null:(System.Int32?)reader[((int)TaxRuleColumn.TaxRuleTypeID - 1)];
			entity.PortalID = (System.Int32)reader[((int)TaxRuleColumn.PortalID - 1)];
			entity.TaxClassID = (reader.IsDBNull(((int)TaxRuleColumn.TaxClassID - 1)))?null:(System.Int32?)reader[((int)TaxRuleColumn.TaxClassID - 1)];
			entity.DestinationCountryCode = (reader.IsDBNull(((int)TaxRuleColumn.DestinationCountryCode - 1)))?null:(System.String)reader[((int)TaxRuleColumn.DestinationCountryCode - 1)];
			entity.DestinationStateCode = (reader.IsDBNull(((int)TaxRuleColumn.DestinationStateCode - 1)))?null:(System.String)reader[((int)TaxRuleColumn.DestinationStateCode - 1)];
			entity.CountyFIPS = (reader.IsDBNull(((int)TaxRuleColumn.CountyFIPS - 1)))?null:(System.String)reader[((int)TaxRuleColumn.CountyFIPS - 1)];
			entity.Precedence = (System.Int32)reader[((int)TaxRuleColumn.Precedence - 1)];
			entity.SalesTax = (reader.IsDBNull(((int)TaxRuleColumn.SalesTax - 1)))?null:(System.Decimal?)reader[((int)TaxRuleColumn.SalesTax - 1)];
			entity.VAT = (reader.IsDBNull(((int)TaxRuleColumn.VAT - 1)))?null:(System.Decimal?)reader[((int)TaxRuleColumn.VAT - 1)];
			entity.GST = (reader.IsDBNull(((int)TaxRuleColumn.GST - 1)))?null:(System.Decimal?)reader[((int)TaxRuleColumn.GST - 1)];
			entity.PST = (reader.IsDBNull(((int)TaxRuleColumn.PST - 1)))?null:(System.Decimal?)reader[((int)TaxRuleColumn.PST - 1)];
			entity.HST = (reader.IsDBNull(((int)TaxRuleColumn.HST - 1)))?null:(System.Decimal?)reader[((int)TaxRuleColumn.HST - 1)];
			entity.TaxShipping = (System.Boolean)reader[((int)TaxRuleColumn.TaxShipping - 1)];
			entity.InclusiveInd = (System.Boolean)reader[((int)TaxRuleColumn.InclusiveInd - 1)];
			entity.Custom1 = (reader.IsDBNull(((int)TaxRuleColumn.Custom1 - 1)))?null:(System.String)reader[((int)TaxRuleColumn.Custom1 - 1)];
			entity.Custom2 = (reader.IsDBNull(((int)TaxRuleColumn.Custom2 - 1)))?null:(System.String)reader[((int)TaxRuleColumn.Custom2 - 1)];
			entity.Custom3 = (reader.IsDBNull(((int)TaxRuleColumn.Custom3 - 1)))?null:(System.String)reader[((int)TaxRuleColumn.Custom3 - 1)];
			entity.ExternalID = (reader.IsDBNull(((int)TaxRuleColumn.ExternalID - 1)))?null:(System.String)reader[((int)TaxRuleColumn.ExternalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRule"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.TaxRule"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.TaxRule entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.TaxRuleID = (System.Int32)dataRow["TaxRuleID"];
			entity.TaxRuleTypeID = Convert.IsDBNull(dataRow["TaxRuleTypeID"]) ? null : (System.Int32?)dataRow["TaxRuleTypeID"];
			entity.PortalID = (System.Int32)dataRow["PortalID"];
			entity.TaxClassID = Convert.IsDBNull(dataRow["TaxClassID"]) ? null : (System.Int32?)dataRow["TaxClassID"];
			entity.DestinationCountryCode = Convert.IsDBNull(dataRow["DestinationCountryCode"]) ? null : (System.String)dataRow["DestinationCountryCode"];
			entity.DestinationStateCode = Convert.IsDBNull(dataRow["DestinationStateCode"]) ? null : (System.String)dataRow["DestinationStateCode"];
			entity.CountyFIPS = Convert.IsDBNull(dataRow["CountyFIPS"]) ? null : (System.String)dataRow["CountyFIPS"];
			entity.Precedence = (System.Int32)dataRow["Precedence"];
			entity.SalesTax = Convert.IsDBNull(dataRow["SalesTax"]) ? null : (System.Decimal?)dataRow["SalesTax"];
			entity.VAT = Convert.IsDBNull(dataRow["VAT"]) ? null : (System.Decimal?)dataRow["VAT"];
			entity.GST = Convert.IsDBNull(dataRow["GST"]) ? null : (System.Decimal?)dataRow["GST"];
			entity.PST = Convert.IsDBNull(dataRow["PST"]) ? null : (System.Decimal?)dataRow["PST"];
			entity.HST = Convert.IsDBNull(dataRow["HST"]) ? null : (System.Decimal?)dataRow["HST"];
			entity.TaxShipping = (System.Boolean)dataRow["TaxShipping"];
			entity.InclusiveInd = (System.Boolean)dataRow["InclusiveInd"];
			entity.Custom1 = Convert.IsDBNull(dataRow["Custom1"]) ? null : (System.String)dataRow["Custom1"];
			entity.Custom2 = Convert.IsDBNull(dataRow["Custom2"]) ? null : (System.String)dataRow["Custom2"];
			entity.Custom3 = Convert.IsDBNull(dataRow["Custom3"]) ? null : (System.String)dataRow["Custom3"];
			entity.ExternalID = Convert.IsDBNull(dataRow["ExternalID"]) ? null : (System.String)dataRow["ExternalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.TaxRule"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.TaxRule Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TaxRule entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region DestinationCountryCodeSource	
			if (CanDeepLoad(entity, "Country|DestinationCountryCodeSource", deepLoadType, innerList) 
				&& entity.DestinationCountryCodeSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.DestinationCountryCode ?? string.Empty);
				Country tmpEntity = EntityManager.LocateEntity<Country>(EntityLocator.ConstructKeyFromPkItems(typeof(Country), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.DestinationCountryCodeSource = tmpEntity;
				else
					entity.DestinationCountryCodeSource = DataRepository.CountryProvider.GetByCode(transactionManager, (entity.DestinationCountryCode ?? string.Empty));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'DestinationCountryCodeSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.DestinationCountryCodeSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CountryProvider.DeepLoad(transactionManager, entity.DestinationCountryCodeSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion DestinationCountryCodeSource

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.PortalID;
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, entity.PortalID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource

			#region TaxClassIDSource	
			if (CanDeepLoad(entity, "TaxClass|TaxClassIDSource", deepLoadType, innerList) 
				&& entity.TaxClassIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.TaxClassID ?? (int)0);
				TaxClass tmpEntity = EntityManager.LocateEntity<TaxClass>(EntityLocator.ConstructKeyFromPkItems(typeof(TaxClass), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.TaxClassIDSource = tmpEntity;
				else
					entity.TaxClassIDSource = DataRepository.TaxClassProvider.GetByTaxClassID(transactionManager, (entity.TaxClassID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TaxClassIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.TaxClassIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TaxClassProvider.DeepLoad(transactionManager, entity.TaxClassIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion TaxClassIDSource

			#region TaxRuleTypeIDSource	
			if (CanDeepLoad(entity, "TaxRuleType|TaxRuleTypeIDSource", deepLoadType, innerList) 
				&& entity.TaxRuleTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.TaxRuleTypeID ?? (int)0);
				TaxRuleType tmpEntity = EntityManager.LocateEntity<TaxRuleType>(EntityLocator.ConstructKeyFromPkItems(typeof(TaxRuleType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.TaxRuleTypeIDSource = tmpEntity;
				else
					entity.TaxRuleTypeIDSource = DataRepository.TaxRuleTypeProvider.GetByTaxRuleTypeID(transactionManager, (entity.TaxRuleTypeID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TaxRuleTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.TaxRuleTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.TaxRuleTypeProvider.DeepLoad(transactionManager, entity.TaxRuleTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion TaxRuleTypeIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.TaxRule object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.TaxRule instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.TaxRule Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TaxRule entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region DestinationCountryCodeSource
			if (CanDeepSave(entity, "Country|DestinationCountryCodeSource", deepSaveType, innerList) 
				&& entity.DestinationCountryCodeSource != null)
			{
				DataRepository.CountryProvider.Save(transactionManager, entity.DestinationCountryCodeSource);
				entity.DestinationCountryCode = entity.DestinationCountryCodeSource.Code;
			}
			#endregion 
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			
			#region TaxClassIDSource
			if (CanDeepSave(entity, "TaxClass|TaxClassIDSource", deepSaveType, innerList) 
				&& entity.TaxClassIDSource != null)
			{
				DataRepository.TaxClassProvider.Save(transactionManager, entity.TaxClassIDSource);
				entity.TaxClassID = entity.TaxClassIDSource.TaxClassID;
			}
			#endregion 
			
			#region TaxRuleTypeIDSource
			if (CanDeepSave(entity, "TaxRuleType|TaxRuleTypeIDSource", deepSaveType, innerList) 
				&& entity.TaxRuleTypeIDSource != null)
			{
				DataRepository.TaxRuleTypeProvider.Save(transactionManager, entity.TaxRuleTypeIDSource);
				entity.TaxRuleTypeID = entity.TaxRuleTypeIDSource.TaxRuleTypeID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TaxRuleChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.TaxRule</c>
	///</summary>
	public enum TaxRuleChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Country</c> at DestinationCountryCodeSource
		///</summary>
		[ChildEntityType(typeof(Country))]
		Country,
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		
		///<summary>
		/// Composite Property for <c>TaxClass</c> at TaxClassIDSource
		///</summary>
		[ChildEntityType(typeof(TaxClass))]
		TaxClass,
		
		///<summary>
		/// Composite Property for <c>TaxRuleType</c> at TaxRuleTypeIDSource
		///</summary>
		[ChildEntityType(typeof(TaxRuleType))]
		TaxRuleType,
	}
	
	#endregion TaxRuleChildEntityTypes
	
	#region TaxRuleFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TaxRuleColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TaxRule"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TaxRuleFilterBuilder : SqlFilterBuilder<TaxRuleColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TaxRuleFilterBuilder class.
		/// </summary>
		public TaxRuleFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TaxRuleFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TaxRuleFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TaxRuleFilterBuilder
	
	#region TaxRuleParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TaxRuleColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TaxRule"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TaxRuleParameterBuilder : ParameterizedSqlFilterBuilder<TaxRuleColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TaxRuleParameterBuilder class.
		/// </summary>
		public TaxRuleParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TaxRuleParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TaxRuleParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TaxRuleParameterBuilder
	
	#region TaxRuleSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TaxRuleColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TaxRule"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TaxRuleSortBuilder : SqlSortBuilder<TaxRuleColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TaxRuleSqlSortBuilder class.
		/// </summary>
		public TaxRuleSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TaxRuleSortBuilder
	
} // end namespace
