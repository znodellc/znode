﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="NoteProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class NoteProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Note, ZNode.Libraries.DataAccess.Entities.NoteKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.NoteKey key)
		{
			return Delete(transactionManager, key.NoteID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_noteID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _noteID)
		{
			return Delete(null, _noteID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_noteID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _noteID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_Note_SC_Account key.
		///		FK_SC_Note_SC_Account Description: 
		/// </summary>
		/// <param name="_accountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Note objects.</returns>
		public TList<Note> GetByAccountID(System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(_accountID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_Note_SC_Account key.
		///		FK_SC_Note_SC_Account Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Note objects.</returns>
		/// <remarks></remarks>
		public TList<Note> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_Note_SC_Account key.
		///		FK_SC_Note_SC_Account Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Note objects.</returns>
		public TList<Note> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength)
		{
			int count = -1;
			return GetByAccountID(transactionManager, _accountID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_Note_SC_Account key.
		///		fKSCNoteSCAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_accountID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Note objects.</returns>
		public TList<Note> GetByAccountID(System.Int32? _accountID, int start, int pageLength)
		{
			int count =  -1;
			return GetByAccountID(null, _accountID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_Note_SC_Account key.
		///		fKSCNoteSCAccount Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_accountID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Note objects.</returns>
		public TList<Note> GetByAccountID(System.Int32? _accountID, int start, int pageLength,out int count)
		{
			return GetByAccountID(null, _accountID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_Note_SC_Account key.
		///		FK_SC_Note_SC_Account Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_accountID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.Note objects.</returns>
		public abstract TList<Note> GetByAccountID(TransactionManager transactionManager, System.Int32? _accountID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Note Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.NoteKey key, int start, int pageLength)
		{
			return GetByNoteID(transactionManager, key.NoteID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IN1 index.
		/// </summary>
		/// <param name="_caseID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Note&gt;"/> class.</returns>
		public TList<Note> GetByCaseID(System.Int32? _caseID)
		{
			int count = -1;
			return GetByCaseID(null,_caseID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IN1 index.
		/// </summary>
		/// <param name="_caseID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Note&gt;"/> class.</returns>
		public TList<Note> GetByCaseID(System.Int32? _caseID, int start, int pageLength)
		{
			int count = -1;
			return GetByCaseID(null, _caseID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IN1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Note&gt;"/> class.</returns>
		public TList<Note> GetByCaseID(TransactionManager transactionManager, System.Int32? _caseID)
		{
			int count = -1;
			return GetByCaseID(transactionManager, _caseID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IN1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Note&gt;"/> class.</returns>
		public TList<Note> GetByCaseID(TransactionManager transactionManager, System.Int32? _caseID, int start, int pageLength)
		{
			int count = -1;
			return GetByCaseID(transactionManager, _caseID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IN1 index.
		/// </summary>
		/// <param name="_caseID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Note&gt;"/> class.</returns>
		public TList<Note> GetByCaseID(System.Int32? _caseID, int start, int pageLength, out int count)
		{
			return GetByCaseID(null, _caseID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IN1 index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_caseID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Note&gt;"/> class.</returns>
		public abstract TList<Note> GetByCaseID(TransactionManager transactionManager, System.Int32? _caseID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_AccountNote index.
		/// </summary>
		/// <param name="_noteID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Note"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Note GetByNoteID(System.Int32 _noteID)
		{
			int count = -1;
			return GetByNoteID(null,_noteID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AccountNote index.
		/// </summary>
		/// <param name="_noteID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Note"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Note GetByNoteID(System.Int32 _noteID, int start, int pageLength)
		{
			int count = -1;
			return GetByNoteID(null, _noteID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AccountNote index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_noteID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Note"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Note GetByNoteID(TransactionManager transactionManager, System.Int32 _noteID)
		{
			int count = -1;
			return GetByNoteID(transactionManager, _noteID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AccountNote index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_noteID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Note"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Note GetByNoteID(TransactionManager transactionManager, System.Int32 _noteID, int start, int pageLength)
		{
			int count = -1;
			return GetByNoteID(transactionManager, _noteID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AccountNote index.
		/// </summary>
		/// <param name="_noteID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Note"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Note GetByNoteID(System.Int32 _noteID, int start, int pageLength, out int count)
		{
			return GetByNoteID(null, _noteID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_AccountNote index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_noteID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Note"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Note GetByNoteID(TransactionManager transactionManager, System.Int32 _noteID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Note&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Note&gt;"/></returns>
		public static TList<Note> Fill(IDataReader reader, TList<Note> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Note c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Note")
					.Append("|").Append((System.Int32)reader[((int)NoteColumn.NoteID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Note>(
					key.ToString(), // EntityTrackingKey
					"Note",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Note();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.NoteID = (System.Int32)reader[((int)NoteColumn.NoteID - 1)];
					c.CaseID = (reader.IsDBNull(((int)NoteColumn.CaseID - 1)))?null:(System.Int32?)reader[((int)NoteColumn.CaseID - 1)];
					c.AccountID = (reader.IsDBNull(((int)NoteColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)NoteColumn.AccountID - 1)];
					c.NoteTitle = (System.String)reader[((int)NoteColumn.NoteTitle - 1)];
					c.NoteBody = (reader.IsDBNull(((int)NoteColumn.NoteBody - 1)))?null:(System.String)reader[((int)NoteColumn.NoteBody - 1)];
					c.CreateDte = (System.DateTime)reader[((int)NoteColumn.CreateDte - 1)];
					c.CreateUser = (System.String)reader[((int)NoteColumn.CreateUser - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Note"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Note"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Note entity)
		{
			if (!reader.Read()) return;
			
			entity.NoteID = (System.Int32)reader[((int)NoteColumn.NoteID - 1)];
			entity.CaseID = (reader.IsDBNull(((int)NoteColumn.CaseID - 1)))?null:(System.Int32?)reader[((int)NoteColumn.CaseID - 1)];
			entity.AccountID = (reader.IsDBNull(((int)NoteColumn.AccountID - 1)))?null:(System.Int32?)reader[((int)NoteColumn.AccountID - 1)];
			entity.NoteTitle = (System.String)reader[((int)NoteColumn.NoteTitle - 1)];
			entity.NoteBody = (reader.IsDBNull(((int)NoteColumn.NoteBody - 1)))?null:(System.String)reader[((int)NoteColumn.NoteBody - 1)];
			entity.CreateDte = (System.DateTime)reader[((int)NoteColumn.CreateDte - 1)];
			entity.CreateUser = (System.String)reader[((int)NoteColumn.CreateUser - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Note"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Note"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Note entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.NoteID = (System.Int32)dataRow["NoteID"];
			entity.CaseID = Convert.IsDBNull(dataRow["CaseID"]) ? null : (System.Int32?)dataRow["CaseID"];
			entity.AccountID = Convert.IsDBNull(dataRow["AccountID"]) ? null : (System.Int32?)dataRow["AccountID"];
			entity.NoteTitle = (System.String)dataRow["NoteTitle"];
			entity.NoteBody = Convert.IsDBNull(dataRow["NoteBody"]) ? null : (System.String)dataRow["NoteBody"];
			entity.CreateDte = (System.DateTime)dataRow["CreateDte"];
			entity.CreateUser = (System.String)dataRow["CreateUser"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Note"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Note Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Note entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region AccountIDSource	
			if (CanDeepLoad(entity, "Account|AccountIDSource", deepLoadType, innerList) 
				&& entity.AccountIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.AccountID ?? (int)0);
				Account tmpEntity = EntityManager.LocateEntity<Account>(EntityLocator.ConstructKeyFromPkItems(typeof(Account), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.AccountIDSource = tmpEntity;
				else
					entity.AccountIDSource = DataRepository.AccountProvider.GetByAccountID(transactionManager, (entity.AccountID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.AccountIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.AccountProvider.DeepLoad(transactionManager, entity.AccountIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion AccountIDSource

			#region CaseIDSource	
			if (CanDeepLoad(entity, "CaseRequest|CaseIDSource", deepLoadType, innerList) 
				&& entity.CaseIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.CaseID ?? (int)0);
				CaseRequest tmpEntity = EntityManager.LocateEntity<CaseRequest>(EntityLocator.ConstructKeyFromPkItems(typeof(CaseRequest), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.CaseIDSource = tmpEntity;
				else
					entity.CaseIDSource = DataRepository.CaseRequestProvider.GetByCaseID(transactionManager, (entity.CaseID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CaseIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.CaseIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.CaseRequestProvider.DeepLoad(transactionManager, entity.CaseIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion CaseIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Note object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Note instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Note Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Note entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region AccountIDSource
			if (CanDeepSave(entity, "Account|AccountIDSource", deepSaveType, innerList) 
				&& entity.AccountIDSource != null)
			{
				DataRepository.AccountProvider.Save(transactionManager, entity.AccountIDSource);
				entity.AccountID = entity.AccountIDSource.AccountID;
			}
			#endregion 
			
			#region CaseIDSource
			if (CanDeepSave(entity, "CaseRequest|CaseIDSource", deepSaveType, innerList) 
				&& entity.CaseIDSource != null)
			{
				DataRepository.CaseRequestProvider.Save(transactionManager, entity.CaseIDSource);
				entity.CaseID = entity.CaseIDSource.CaseID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region NoteChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Note</c>
	///</summary>
	public enum NoteChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Account</c> at AccountIDSource
		///</summary>
		[ChildEntityType(typeof(Account))]
		Account,
		
		///<summary>
		/// Composite Property for <c>CaseRequest</c> at CaseIDSource
		///</summary>
		[ChildEntityType(typeof(CaseRequest))]
		CaseRequest,
	}
	
	#endregion NoteChildEntityTypes
	
	#region NoteFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;NoteColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Note"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class NoteFilterBuilder : SqlFilterBuilder<NoteColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the NoteFilterBuilder class.
		/// </summary>
		public NoteFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the NoteFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public NoteFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the NoteFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public NoteFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion NoteFilterBuilder
	
	#region NoteParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;NoteColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Note"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class NoteParameterBuilder : ParameterizedSqlFilterBuilder<NoteColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the NoteParameterBuilder class.
		/// </summary>
		public NoteParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the NoteParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public NoteParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the NoteParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public NoteParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion NoteParameterBuilder
	
	#region NoteSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;NoteColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Note"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class NoteSortBuilder : SqlSortBuilder<NoteColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the NoteSqlSortBuilder class.
		/// </summary>
		public NoteSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion NoteSortBuilder
	
} // end namespace
