﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="AspNetSqlCacheTablesForChangeNotificationProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class AspNetSqlCacheTablesForChangeNotificationProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification, ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotificationKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotificationKey key)
		{
			return Delete(transactionManager, key.TableName);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_tableName">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.String _tableName)
		{
			return Delete(null, _tableName);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_tableName">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.String _tableName);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotificationKey key, int start, int pageLength)
		{
			return GetByTableName(transactionManager, key.TableName, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK__AspNet_SqlCacheT__4392552C index.
		/// </summary>
		/// <param name="_tableName"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification GetByTableName(System.String _tableName)
		{
			int count = -1;
			return GetByTableName(null,_tableName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__AspNet_SqlCacheT__4392552C index.
		/// </summary>
		/// <param name="_tableName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification GetByTableName(System.String _tableName, int start, int pageLength)
		{
			int count = -1;
			return GetByTableName(null, _tableName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__AspNet_SqlCacheT__4392552C index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_tableName"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification GetByTableName(TransactionManager transactionManager, System.String _tableName)
		{
			int count = -1;
			return GetByTableName(transactionManager, _tableName, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__AspNet_SqlCacheT__4392552C index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_tableName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification GetByTableName(TransactionManager transactionManager, System.String _tableName, int start, int pageLength)
		{
			int count = -1;
			return GetByTableName(transactionManager, _tableName, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__AspNet_SqlCacheT__4392552C index.
		/// </summary>
		/// <param name="_tableName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification GetByTableName(System.String _tableName, int start, int pageLength, out int count)
		{
			return GetByTableName(null, _tableName, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK__AspNet_SqlCacheT__4392552C index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_tableName"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification GetByTableName(TransactionManager transactionManager, System.String _tableName, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;AspNetSqlCacheTablesForChangeNotification&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;AspNetSqlCacheTablesForChangeNotification&gt;"/></returns>
		public static TList<AspNetSqlCacheTablesForChangeNotification> Fill(IDataReader reader, TList<AspNetSqlCacheTablesForChangeNotification> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("AspNetSqlCacheTablesForChangeNotification")
					.Append("|").Append((System.String)reader[((int)AspNetSqlCacheTablesForChangeNotificationColumn.TableName - 1)]).ToString();
					c = EntityManager.LocateOrCreate<AspNetSqlCacheTablesForChangeNotification>(
					key.ToString(), // EntityTrackingKey
					"AspNetSqlCacheTablesForChangeNotification",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.TableName = (System.String)reader[((int)AspNetSqlCacheTablesForChangeNotificationColumn.TableName - 1)];
					c.OriginalTableName = c.TableName;
					c.NotificationCreated = (System.DateTime)reader[((int)AspNetSqlCacheTablesForChangeNotificationColumn.NotificationCreated - 1)];
					c.ChangeId = (System.Int32)reader[((int)AspNetSqlCacheTablesForChangeNotificationColumn.ChangeId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification entity)
		{
			if (!reader.Read()) return;
			
			entity.TableName = (System.String)reader[((int)AspNetSqlCacheTablesForChangeNotificationColumn.TableName - 1)];
			entity.OriginalTableName = (System.String)reader["tableName"];
			entity.NotificationCreated = (System.DateTime)reader[((int)AspNetSqlCacheTablesForChangeNotificationColumn.NotificationCreated - 1)];
			entity.ChangeId = (System.Int32)reader[((int)AspNetSqlCacheTablesForChangeNotificationColumn.ChangeId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.TableName = (System.String)dataRow["tableName"];
			entity.OriginalTableName = (System.String)dataRow["tableName"];
			entity.NotificationCreated = (System.DateTime)dataRow["notificationCreated"];
			entity.ChangeId = (System.Int32)dataRow["changeId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region AspNetSqlCacheTablesForChangeNotificationChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.AspNetSqlCacheTablesForChangeNotification</c>
	///</summary>
	public enum AspNetSqlCacheTablesForChangeNotificationChildEntityTypes
	{
	}
	
	#endregion AspNetSqlCacheTablesForChangeNotificationChildEntityTypes
	
	#region AspNetSqlCacheTablesForChangeNotificationFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;AspNetSqlCacheTablesForChangeNotificationColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AspNetSqlCacheTablesForChangeNotification"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AspNetSqlCacheTablesForChangeNotificationFilterBuilder : SqlFilterBuilder<AspNetSqlCacheTablesForChangeNotificationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AspNetSqlCacheTablesForChangeNotificationFilterBuilder class.
		/// </summary>
		public AspNetSqlCacheTablesForChangeNotificationFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AspNetSqlCacheTablesForChangeNotificationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AspNetSqlCacheTablesForChangeNotificationFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AspNetSqlCacheTablesForChangeNotificationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AspNetSqlCacheTablesForChangeNotificationFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AspNetSqlCacheTablesForChangeNotificationFilterBuilder
	
	#region AspNetSqlCacheTablesForChangeNotificationParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;AspNetSqlCacheTablesForChangeNotificationColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AspNetSqlCacheTablesForChangeNotification"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class AspNetSqlCacheTablesForChangeNotificationParameterBuilder : ParameterizedSqlFilterBuilder<AspNetSqlCacheTablesForChangeNotificationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AspNetSqlCacheTablesForChangeNotificationParameterBuilder class.
		/// </summary>
		public AspNetSqlCacheTablesForChangeNotificationParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the AspNetSqlCacheTablesForChangeNotificationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public AspNetSqlCacheTablesForChangeNotificationParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the AspNetSqlCacheTablesForChangeNotificationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public AspNetSqlCacheTablesForChangeNotificationParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion AspNetSqlCacheTablesForChangeNotificationParameterBuilder
	
	#region AspNetSqlCacheTablesForChangeNotificationSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;AspNetSqlCacheTablesForChangeNotificationColumn&gt;"/> class
	/// that is used exclusively with a <see cref="AspNetSqlCacheTablesForChangeNotification"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class AspNetSqlCacheTablesForChangeNotificationSortBuilder : SqlSortBuilder<AspNetSqlCacheTablesForChangeNotificationColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the AspNetSqlCacheTablesForChangeNotificationSqlSortBuilder class.
		/// </summary>
		public AspNetSqlCacheTablesForChangeNotificationSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion AspNetSqlCacheTablesForChangeNotificationSortBuilder
	
} // end namespace
