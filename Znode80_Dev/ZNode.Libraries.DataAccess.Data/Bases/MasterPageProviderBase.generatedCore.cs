﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="MasterPageProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class MasterPageProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.MasterPage, ZNode.Libraries.DataAccess.Entities.MasterPageKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.MasterPageKey key)
		{
			return Delete(transactionManager, key.MasterPageID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_masterPageID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _masterPageID)
		{
			return Delete(null, _masterPageID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_masterPageID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _masterPageID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.MasterPage Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.MasterPageKey key, int start, int pageLength)
		{
			return GetByMasterPageID(transactionManager, key.MasterPageID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeMasterPage index.
		/// </summary>
		/// <param name="_masterPageID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MasterPage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.MasterPage GetByMasterPageID(System.Int32 _masterPageID)
		{
			int count = -1;
			return GetByMasterPageID(null,_masterPageID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeMasterPage index.
		/// </summary>
		/// <param name="_masterPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MasterPage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.MasterPage GetByMasterPageID(System.Int32 _masterPageID, int start, int pageLength)
		{
			int count = -1;
			return GetByMasterPageID(null, _masterPageID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeMasterPage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_masterPageID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MasterPage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.MasterPage GetByMasterPageID(TransactionManager transactionManager, System.Int32 _masterPageID)
		{
			int count = -1;
			return GetByMasterPageID(transactionManager, _masterPageID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeMasterPage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_masterPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MasterPage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.MasterPage GetByMasterPageID(TransactionManager transactionManager, System.Int32 _masterPageID, int start, int pageLength)
		{
			int count = -1;
			return GetByMasterPageID(transactionManager, _masterPageID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeMasterPage index.
		/// </summary>
		/// <param name="_masterPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MasterPage"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.MasterPage GetByMasterPageID(System.Int32 _masterPageID, int start, int pageLength, out int count)
		{
			return GetByMasterPageID(null, _masterPageID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeMasterPage index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_masterPageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MasterPage"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.MasterPage GetByMasterPageID(TransactionManager transactionManager, System.Int32 _masterPageID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;MasterPage&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;MasterPage&gt;"/></returns>
		public static TList<MasterPage> Fill(IDataReader reader, TList<MasterPage> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.MasterPage c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("MasterPage")
					.Append("|").Append((System.Int32)reader[((int)MasterPageColumn.MasterPageID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<MasterPage>(
					key.ToString(), // EntityTrackingKey
					"MasterPage",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.MasterPage();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.MasterPageID = (System.Int32)reader[((int)MasterPageColumn.MasterPageID - 1)];
					c.ThemeID = (System.Int32)reader[((int)MasterPageColumn.ThemeID - 1)];
					c.Name = (System.String)reader[((int)MasterPageColumn.Name - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.MasterPage"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.MasterPage"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.MasterPage entity)
		{
			if (!reader.Read()) return;
			
			entity.MasterPageID = (System.Int32)reader[((int)MasterPageColumn.MasterPageID - 1)];
			entity.ThemeID = (System.Int32)reader[((int)MasterPageColumn.ThemeID - 1)];
			entity.Name = (System.String)reader[((int)MasterPageColumn.Name - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.MasterPage"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.MasterPage"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.MasterPage entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.MasterPageID = (System.Int32)dataRow["MasterPageID"];
			entity.ThemeID = (System.Int32)dataRow["ThemeID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.MasterPage"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.MasterPage Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.MasterPage entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByMasterPageID methods when available
			
			#region ContentPageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ContentPage>|ContentPageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ContentPageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ContentPageCollection = DataRepository.ContentPageProvider.GetByMasterPageID(transactionManager, entity.MasterPageID);

				if (deep && entity.ContentPageCollection.Count > 0)
				{
					deepHandles.Add("ContentPageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ContentPage>) DataRepository.ContentPageProvider.DeepLoad,
						new object[] { transactionManager, entity.ContentPageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region ProductCategoryCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductCategory>|ProductCategoryCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductCategoryCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductCategoryCollection = DataRepository.ProductCategoryProvider.GetByMasterPageID(transactionManager, entity.MasterPageID);

				if (deep && entity.ProductCategoryCollection.Count > 0)
				{
					deepHandles.Add("ProductCategoryCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductCategory>) DataRepository.ProductCategoryProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductCategoryCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region CategoryNodeCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<CategoryNode>|CategoryNodeCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'CategoryNodeCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.CategoryNodeCollection = DataRepository.CategoryNodeProvider.GetByMasterPageID(transactionManager, entity.MasterPageID);

				if (deep && entity.CategoryNodeCollection.Count > 0)
				{
					deepHandles.Add("CategoryNodeCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<CategoryNode>) DataRepository.CategoryNodeProvider.DeepLoad,
						new object[] { transactionManager, entity.CategoryNodeCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.MasterPage object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.MasterPage instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.MasterPage Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.MasterPage entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<ContentPage>
				if (CanDeepSave(entity.ContentPageCollection, "List<ContentPage>|ContentPageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ContentPage child in entity.ContentPageCollection)
					{
						if(child.MasterPageIDSource != null)
						{
							child.MasterPageID = child.MasterPageIDSource.MasterPageID;
						}
						else
						{
							child.MasterPageID = entity.MasterPageID;
						}

					}

					if (entity.ContentPageCollection.Count > 0 || entity.ContentPageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ContentPageProvider.Save(transactionManager, entity.ContentPageCollection);
						
						deepHandles.Add("ContentPageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ContentPage >) DataRepository.ContentPageProvider.DeepSave,
							new object[] { transactionManager, entity.ContentPageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<ProductCategory>
				if (CanDeepSave(entity.ProductCategoryCollection, "List<ProductCategory>|ProductCategoryCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductCategory child in entity.ProductCategoryCollection)
					{
						if(child.MasterPageIDSource != null)
						{
							child.MasterPageID = child.MasterPageIDSource.MasterPageID;
						}
						else
						{
							child.MasterPageID = entity.MasterPageID;
						}

					}

					if (entity.ProductCategoryCollection.Count > 0 || entity.ProductCategoryCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductCategoryProvider.Save(transactionManager, entity.ProductCategoryCollection);
						
						deepHandles.Add("ProductCategoryCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductCategory >) DataRepository.ProductCategoryProvider.DeepSave,
							new object[] { transactionManager, entity.ProductCategoryCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<CategoryNode>
				if (CanDeepSave(entity.CategoryNodeCollection, "List<CategoryNode>|CategoryNodeCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(CategoryNode child in entity.CategoryNodeCollection)
					{
						if(child.MasterPageIDSource != null)
						{
							child.MasterPageID = child.MasterPageIDSource.MasterPageID;
						}
						else
						{
							child.MasterPageID = entity.MasterPageID;
						}

					}

					if (entity.CategoryNodeCollection.Count > 0 || entity.CategoryNodeCollection.DeletedItems.Count > 0)
					{
						//DataRepository.CategoryNodeProvider.Save(transactionManager, entity.CategoryNodeCollection);
						
						deepHandles.Add("CategoryNodeCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< CategoryNode >) DataRepository.CategoryNodeProvider.DeepSave,
							new object[] { transactionManager, entity.CategoryNodeCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region MasterPageChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.MasterPage</c>
	///</summary>
	public enum MasterPageChildEntityTypes
	{
		///<summary>
		/// Collection of <c>MasterPage</c> as OneToMany for ContentPageCollection
		///</summary>
		[ChildEntityType(typeof(TList<ContentPage>))]
		ContentPageCollection,
		///<summary>
		/// Collection of <c>MasterPage</c> as OneToMany for ProductCategoryCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductCategory>))]
		ProductCategoryCollection,
		///<summary>
		/// Collection of <c>MasterPage</c> as OneToMany for CategoryNodeCollection
		///</summary>
		[ChildEntityType(typeof(TList<CategoryNode>))]
		CategoryNodeCollection,
	}
	
	#endregion MasterPageChildEntityTypes
	
	#region MasterPageFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;MasterPageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MasterPage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MasterPageFilterBuilder : SqlFilterBuilder<MasterPageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MasterPageFilterBuilder class.
		/// </summary>
		public MasterPageFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MasterPageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MasterPageFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MasterPageFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MasterPageFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MasterPageFilterBuilder
	
	#region MasterPageParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;MasterPageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MasterPage"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MasterPageParameterBuilder : ParameterizedSqlFilterBuilder<MasterPageColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MasterPageParameterBuilder class.
		/// </summary>
		public MasterPageParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MasterPageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MasterPageParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MasterPageParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MasterPageParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MasterPageParameterBuilder
	
	#region MasterPageSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;MasterPageColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MasterPage"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class MasterPageSortBuilder : SqlSortBuilder<MasterPageColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MasterPageSqlSortBuilder class.
		/// </summary>
		public MasterPageSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion MasterPageSortBuilder
	
} // end namespace
