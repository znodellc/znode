﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="MessageConfigProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class MessageConfigProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.MessageConfig, ZNode.Libraries.DataAccess.Entities.MessageConfigKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.MessageConfigKey key)
		{
			return Delete(transactionManager, key.MessageID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_messageID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _messageID)
		{
			return Delete(null, _messageID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_messageID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _messageID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeMessageConfig_ZNodeMessageType key.
		///		FK_ZNodeMessageConfig_ZNodeMessageType Description: 
		/// </summary>
		/// <param name="_messageTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.MessageConfig objects.</returns>
		public TList<MessageConfig> GetByMessageTypeID(System.Int32 _messageTypeID)
		{
			int count = -1;
			return GetByMessageTypeID(_messageTypeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeMessageConfig_ZNodeMessageType key.
		///		FK_ZNodeMessageConfig_ZNodeMessageType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_messageTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.MessageConfig objects.</returns>
		/// <remarks></remarks>
		public TList<MessageConfig> GetByMessageTypeID(TransactionManager transactionManager, System.Int32 _messageTypeID)
		{
			int count = -1;
			return GetByMessageTypeID(transactionManager, _messageTypeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeMessageConfig_ZNodeMessageType key.
		///		FK_ZNodeMessageConfig_ZNodeMessageType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_messageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.MessageConfig objects.</returns>
		public TList<MessageConfig> GetByMessageTypeID(TransactionManager transactionManager, System.Int32 _messageTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByMessageTypeID(transactionManager, _messageTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeMessageConfig_ZNodeMessageType key.
		///		fKZNodeMessageConfigZNodeMessageType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_messageTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.MessageConfig objects.</returns>
		public TList<MessageConfig> GetByMessageTypeID(System.Int32 _messageTypeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByMessageTypeID(null, _messageTypeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeMessageConfig_ZNodeMessageType key.
		///		fKZNodeMessageConfigZNodeMessageType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_messageTypeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.MessageConfig objects.</returns>
		public TList<MessageConfig> GetByMessageTypeID(System.Int32 _messageTypeID, int start, int pageLength,out int count)
		{
			return GetByMessageTypeID(null, _messageTypeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeMessageConfig_ZNodeMessageType key.
		///		FK_ZNodeMessageConfig_ZNodeMessageType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_messageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.MessageConfig objects.</returns>
		public abstract TList<MessageConfig> GetByMessageTypeID(TransactionManager transactionManager, System.Int32 _messageTypeID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.MessageConfig Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.MessageConfigKey key, int start, int pageLength)
		{
			return GetByMessageID(transactionManager, key.MessageID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeMessageConfig index.
		/// </summary>
		/// <param name="_key"></param>
		/// <param name="_portalID"></param>
		/// <param name="_localeID"></param>
		/// <param name="_messageTypeID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;MessageConfig&gt;"/> class.</returns>
		public TList<MessageConfig> GetByKeyPortalIDLocaleIDMessageTypeID(System.String _key, System.Int32 _portalID, System.Int32? _localeID, System.Int32 _messageTypeID)
		{
			int count = -1;
			return GetByKeyPortalIDLocaleIDMessageTypeID(null,_key, _portalID, _localeID, _messageTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeMessageConfig index.
		/// </summary>
		/// <param name="_key"></param>
		/// <param name="_portalID"></param>
		/// <param name="_localeID"></param>
		/// <param name="_messageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;MessageConfig&gt;"/> class.</returns>
		public TList<MessageConfig> GetByKeyPortalIDLocaleIDMessageTypeID(System.String _key, System.Int32 _portalID, System.Int32? _localeID, System.Int32 _messageTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByKeyPortalIDLocaleIDMessageTypeID(null, _key, _portalID, _localeID, _messageTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeMessageConfig index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_key"></param>
		/// <param name="_portalID"></param>
		/// <param name="_localeID"></param>
		/// <param name="_messageTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;MessageConfig&gt;"/> class.</returns>
		public TList<MessageConfig> GetByKeyPortalIDLocaleIDMessageTypeID(TransactionManager transactionManager, System.String _key, System.Int32 _portalID, System.Int32? _localeID, System.Int32 _messageTypeID)
		{
			int count = -1;
			return GetByKeyPortalIDLocaleIDMessageTypeID(transactionManager, _key, _portalID, _localeID, _messageTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeMessageConfig index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_key"></param>
		/// <param name="_portalID"></param>
		/// <param name="_localeID"></param>
		/// <param name="_messageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;MessageConfig&gt;"/> class.</returns>
		public TList<MessageConfig> GetByKeyPortalIDLocaleIDMessageTypeID(TransactionManager transactionManager, System.String _key, System.Int32 _portalID, System.Int32? _localeID, System.Int32 _messageTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByKeyPortalIDLocaleIDMessageTypeID(transactionManager, _key, _portalID, _localeID, _messageTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeMessageConfig index.
		/// </summary>
		/// <param name="_key"></param>
		/// <param name="_portalID"></param>
		/// <param name="_localeID"></param>
		/// <param name="_messageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;MessageConfig&gt;"/> class.</returns>
		public TList<MessageConfig> GetByKeyPortalIDLocaleIDMessageTypeID(System.String _key, System.Int32 _portalID, System.Int32? _localeID, System.Int32 _messageTypeID, int start, int pageLength, out int count)
		{
			return GetByKeyPortalIDLocaleIDMessageTypeID(null, _key, _portalID, _localeID, _messageTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeMessageConfig index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_key"></param>
		/// <param name="_portalID"></param>
		/// <param name="_localeID"></param>
		/// <param name="_messageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;MessageConfig&gt;"/> class.</returns>
		public abstract TList<MessageConfig> GetByKeyPortalIDLocaleIDMessageTypeID(TransactionManager transactionManager, System.String _key, System.Int32 _portalID, System.Int32? _localeID, System.Int32 _messageTypeID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeMessageConfig index.
		/// </summary>
		/// <param name="_messageID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MessageConfig"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.MessageConfig GetByMessageID(System.Int32 _messageID)
		{
			int count = -1;
			return GetByMessageID(null,_messageID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeMessageConfig index.
		/// </summary>
		/// <param name="_messageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MessageConfig"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.MessageConfig GetByMessageID(System.Int32 _messageID, int start, int pageLength)
		{
			int count = -1;
			return GetByMessageID(null, _messageID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeMessageConfig index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_messageID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MessageConfig"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.MessageConfig GetByMessageID(TransactionManager transactionManager, System.Int32 _messageID)
		{
			int count = -1;
			return GetByMessageID(transactionManager, _messageID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeMessageConfig index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_messageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MessageConfig"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.MessageConfig GetByMessageID(TransactionManager transactionManager, System.Int32 _messageID, int start, int pageLength)
		{
			int count = -1;
			return GetByMessageID(transactionManager, _messageID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeMessageConfig index.
		/// </summary>
		/// <param name="_messageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MessageConfig"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.MessageConfig GetByMessageID(System.Int32 _messageID, int start, int pageLength, out int count)
		{
			return GetByMessageID(null, _messageID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeMessageConfig index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_messageID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.MessageConfig"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.MessageConfig GetByMessageID(TransactionManager transactionManager, System.Int32 _messageID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;MessageConfig&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;MessageConfig&gt;"/></returns>
		public static TList<MessageConfig> Fill(IDataReader reader, TList<MessageConfig> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.MessageConfig c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("MessageConfig")
					.Append("|").Append((System.Int32)reader[((int)MessageConfigColumn.MessageID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<MessageConfig>(
					key.ToString(), // EntityTrackingKey
					"MessageConfig",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.MessageConfig();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.MessageID = (System.Int32)reader[((int)MessageConfigColumn.MessageID - 1)];
					c.Key = (System.String)reader[((int)MessageConfigColumn.Key - 1)];
					c.PortalID = (System.Int32)reader[((int)MessageConfigColumn.PortalID - 1)];
					c.LocaleID = (reader.IsDBNull(((int)MessageConfigColumn.LocaleID - 1)))?null:(System.Int32?)reader[((int)MessageConfigColumn.LocaleID - 1)];
					c.Description = (reader.IsDBNull(((int)MessageConfigColumn.Description - 1)))?null:(System.String)reader[((int)MessageConfigColumn.Description - 1)];
					c.Value = (reader.IsDBNull(((int)MessageConfigColumn.Value - 1)))?null:(System.String)reader[((int)MessageConfigColumn.Value - 1)];
					c.MessageTypeID = (System.Int32)reader[((int)MessageConfigColumn.MessageTypeID - 1)];
					c.PageSEOName = (reader.IsDBNull(((int)MessageConfigColumn.PageSEOName - 1)))?null:(System.String)reader[((int)MessageConfigColumn.PageSEOName - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.MessageConfig"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.MessageConfig"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.MessageConfig entity)
		{
			if (!reader.Read()) return;
			
			entity.MessageID = (System.Int32)reader[((int)MessageConfigColumn.MessageID - 1)];
			entity.Key = (System.String)reader[((int)MessageConfigColumn.Key - 1)];
			entity.PortalID = (System.Int32)reader[((int)MessageConfigColumn.PortalID - 1)];
			entity.LocaleID = (reader.IsDBNull(((int)MessageConfigColumn.LocaleID - 1)))?null:(System.Int32?)reader[((int)MessageConfigColumn.LocaleID - 1)];
			entity.Description = (reader.IsDBNull(((int)MessageConfigColumn.Description - 1)))?null:(System.String)reader[((int)MessageConfigColumn.Description - 1)];
			entity.Value = (reader.IsDBNull(((int)MessageConfigColumn.Value - 1)))?null:(System.String)reader[((int)MessageConfigColumn.Value - 1)];
			entity.MessageTypeID = (System.Int32)reader[((int)MessageConfigColumn.MessageTypeID - 1)];
			entity.PageSEOName = (reader.IsDBNull(((int)MessageConfigColumn.PageSEOName - 1)))?null:(System.String)reader[((int)MessageConfigColumn.PageSEOName - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.MessageConfig"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.MessageConfig"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.MessageConfig entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.MessageID = (System.Int32)dataRow["MessageID"];
			entity.Key = (System.String)dataRow["Key"];
			entity.PortalID = (System.Int32)dataRow["PortalID"];
			entity.LocaleID = Convert.IsDBNull(dataRow["LocaleID"]) ? null : (System.Int32?)dataRow["LocaleID"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.Value = Convert.IsDBNull(dataRow["Value"]) ? null : (System.String)dataRow["Value"];
			entity.MessageTypeID = (System.Int32)dataRow["MessageTypeID"];
			entity.PageSEOName = Convert.IsDBNull(dataRow["PageSEOName"]) ? null : (System.String)dataRow["PageSEOName"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.MessageConfig"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.MessageConfig Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.MessageConfig entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region MessageTypeIDSource	
			if (CanDeepLoad(entity, "MessageType|MessageTypeIDSource", deepLoadType, innerList) 
				&& entity.MessageTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.MessageTypeID;
				MessageType tmpEntity = EntityManager.LocateEntity<MessageType>(EntityLocator.ConstructKeyFromPkItems(typeof(MessageType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.MessageTypeIDSource = tmpEntity;
				else
					entity.MessageTypeIDSource = DataRepository.MessageTypeProvider.GetByMessageTypeID(transactionManager, entity.MessageTypeID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'MessageTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.MessageTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.MessageTypeProvider.DeepLoad(transactionManager, entity.MessageTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion MessageTypeIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.MessageConfig object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.MessageConfig instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.MessageConfig Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.MessageConfig entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region MessageTypeIDSource
			if (CanDeepSave(entity, "MessageType|MessageTypeIDSource", deepSaveType, innerList) 
				&& entity.MessageTypeIDSource != null)
			{
				DataRepository.MessageTypeProvider.Save(transactionManager, entity.MessageTypeIDSource);
				entity.MessageTypeID = entity.MessageTypeIDSource.MessageTypeID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region MessageConfigChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.MessageConfig</c>
	///</summary>
	public enum MessageConfigChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>MessageType</c> at MessageTypeIDSource
		///</summary>
		[ChildEntityType(typeof(MessageType))]
		MessageType,
	}
	
	#endregion MessageConfigChildEntityTypes
	
	#region MessageConfigFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;MessageConfigColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MessageConfig"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MessageConfigFilterBuilder : SqlFilterBuilder<MessageConfigColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MessageConfigFilterBuilder class.
		/// </summary>
		public MessageConfigFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MessageConfigFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MessageConfigFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MessageConfigFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MessageConfigFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MessageConfigFilterBuilder
	
	#region MessageConfigParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;MessageConfigColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MessageConfig"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class MessageConfigParameterBuilder : ParameterizedSqlFilterBuilder<MessageConfigColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MessageConfigParameterBuilder class.
		/// </summary>
		public MessageConfigParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the MessageConfigParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public MessageConfigParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the MessageConfigParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public MessageConfigParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion MessageConfigParameterBuilder
	
	#region MessageConfigSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;MessageConfigColumn&gt;"/> class
	/// that is used exclusively with a <see cref="MessageConfig"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class MessageConfigSortBuilder : SqlSortBuilder<MessageConfigColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the MessageConfigSqlSortBuilder class.
		/// </summary>
		public MessageConfigSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion MessageConfigSortBuilder
	
} // end namespace
