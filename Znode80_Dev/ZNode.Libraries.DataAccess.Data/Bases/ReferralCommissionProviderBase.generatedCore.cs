﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ReferralCommissionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ReferralCommissionProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ReferralCommission, ZNode.Libraries.DataAccess.Entities.ReferralCommissionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ReferralCommissionKey key)
		{
			return Delete(transactionManager, key.ReferralCommissionID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_referralCommissionID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _referralCommissionID)
		{
			return Delete(null, _referralCommissionID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralCommissionID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _referralCommissionID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeReferralCommission_ZNodeOrder key.
		///		FK_ZNodeReferralCommission_ZNodeOrder Description: 
		/// </summary>
		/// <param name="_orderID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ReferralCommission objects.</returns>
		public TList<ReferralCommission> GetByOrderID(System.Int32? _orderID)
		{
			int count = -1;
			return GetByOrderID(_orderID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeReferralCommission_ZNodeOrder key.
		///		FK_ZNodeReferralCommission_ZNodeOrder Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ReferralCommission objects.</returns>
		/// <remarks></remarks>
		public TList<ReferralCommission> GetByOrderID(TransactionManager transactionManager, System.Int32? _orderID)
		{
			int count = -1;
			return GetByOrderID(transactionManager, _orderID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeReferralCommission_ZNodeOrder key.
		///		FK_ZNodeReferralCommission_ZNodeOrder Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ReferralCommission objects.</returns>
		public TList<ReferralCommission> GetByOrderID(TransactionManager transactionManager, System.Int32? _orderID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderID(transactionManager, _orderID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeReferralCommission_ZNodeOrder key.
		///		fKZNodeReferralCommissionZNodeOrder Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ReferralCommission objects.</returns>
		public TList<ReferralCommission> GetByOrderID(System.Int32? _orderID, int start, int pageLength)
		{
			int count =  -1;
			return GetByOrderID(null, _orderID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeReferralCommission_ZNodeOrder key.
		///		fKZNodeReferralCommissionZNodeOrder Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_orderID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ReferralCommission objects.</returns>
		public TList<ReferralCommission> GetByOrderID(System.Int32? _orderID, int start, int pageLength,out int count)
		{
			return GetByOrderID(null, _orderID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeReferralCommission_ZNodeOrder key.
		///		FK_ZNodeReferralCommission_ZNodeOrder Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ReferralCommission objects.</returns>
		public abstract TList<ReferralCommission> GetByOrderID(TransactionManager transactionManager, System.Int32? _orderID, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeReferralCommission_ZnodeReferralCommissionType key.
		///		FK_ZnodeReferralCommission_ZnodeReferralCommissionType Description: 
		/// </summary>
		/// <param name="_referralCommissionTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ReferralCommission objects.</returns>
		public TList<ReferralCommission> GetByReferralCommissionTypeID(System.Int32 _referralCommissionTypeID)
		{
			int count = -1;
			return GetByReferralCommissionTypeID(_referralCommissionTypeID, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeReferralCommission_ZnodeReferralCommissionType key.
		///		FK_ZnodeReferralCommission_ZnodeReferralCommissionType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralCommissionTypeID"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ReferralCommission objects.</returns>
		/// <remarks></remarks>
		public TList<ReferralCommission> GetByReferralCommissionTypeID(TransactionManager transactionManager, System.Int32 _referralCommissionTypeID)
		{
			int count = -1;
			return GetByReferralCommissionTypeID(transactionManager, _referralCommissionTypeID, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeReferralCommission_ZnodeReferralCommissionType key.
		///		FK_ZnodeReferralCommission_ZnodeReferralCommissionType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralCommissionTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ReferralCommission objects.</returns>
		public TList<ReferralCommission> GetByReferralCommissionTypeID(TransactionManager transactionManager, System.Int32 _referralCommissionTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByReferralCommissionTypeID(transactionManager, _referralCommissionTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeReferralCommission_ZnodeReferralCommissionType key.
		///		fKZnodeReferralCommissionZnodeReferralCommissionType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_referralCommissionTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ReferralCommission objects.</returns>
		public TList<ReferralCommission> GetByReferralCommissionTypeID(System.Int32 _referralCommissionTypeID, int start, int pageLength)
		{
			int count =  -1;
			return GetByReferralCommissionTypeID(null, _referralCommissionTypeID, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeReferralCommission_ZnodeReferralCommissionType key.
		///		fKZnodeReferralCommissionZnodeReferralCommissionType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_referralCommissionTypeID"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ReferralCommission objects.</returns>
		public TList<ReferralCommission> GetByReferralCommissionTypeID(System.Int32 _referralCommissionTypeID, int start, int pageLength,out int count)
		{
			return GetByReferralCommissionTypeID(null, _referralCommissionTypeID, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZnodeReferralCommission_ZnodeReferralCommissionType key.
		///		FK_ZnodeReferralCommission_ZnodeReferralCommissionType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralCommissionTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ReferralCommission objects.</returns>
		public abstract TList<ReferralCommission> GetByReferralCommissionTypeID(TransactionManager transactionManager, System.Int32 _referralCommissionTypeID, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ReferralCommission Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ReferralCommissionKey key, int start, int pageLength)
		{
			return GetByReferralCommissionID(transactionManager, key.ReferralCommissionID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeReferralCommission index.
		/// </summary>
		/// <param name="_referralCommissionID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommission"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ReferralCommission GetByReferralCommissionID(System.Int32 _referralCommissionID)
		{
			int count = -1;
			return GetByReferralCommissionID(null,_referralCommissionID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeReferralCommission index.
		/// </summary>
		/// <param name="_referralCommissionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommission"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ReferralCommission GetByReferralCommissionID(System.Int32 _referralCommissionID, int start, int pageLength)
		{
			int count = -1;
			return GetByReferralCommissionID(null, _referralCommissionID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeReferralCommission index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralCommissionID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommission"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ReferralCommission GetByReferralCommissionID(TransactionManager transactionManager, System.Int32 _referralCommissionID)
		{
			int count = -1;
			return GetByReferralCommissionID(transactionManager, _referralCommissionID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeReferralCommission index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralCommissionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommission"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ReferralCommission GetByReferralCommissionID(TransactionManager transactionManager, System.Int32 _referralCommissionID, int start, int pageLength)
		{
			int count = -1;
			return GetByReferralCommissionID(transactionManager, _referralCommissionID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeReferralCommission index.
		/// </summary>
		/// <param name="_referralCommissionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommission"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ReferralCommission GetByReferralCommissionID(System.Int32 _referralCommissionID, int start, int pageLength, out int count)
		{
			return GetByReferralCommissionID(null, _referralCommissionID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeReferralCommission index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralCommissionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommission"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ReferralCommission GetByReferralCommissionID(TransactionManager transactionManager, System.Int32 _referralCommissionID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ReferralCommission&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ReferralCommission&gt;"/></returns>
		public static TList<ReferralCommission> Fill(IDataReader reader, TList<ReferralCommission> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ReferralCommission c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ReferralCommission")
					.Append("|").Append((System.Int32)reader[((int)ReferralCommissionColumn.ReferralCommissionID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ReferralCommission>(
					key.ToString(), // EntityTrackingKey
					"ReferralCommission",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ReferralCommission();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ReferralCommissionID = (System.Int32)reader[((int)ReferralCommissionColumn.ReferralCommissionID - 1)];
					c.ReferralCommissionTypeID = (System.Int32)reader[((int)ReferralCommissionColumn.ReferralCommissionTypeID - 1)];
					c.ReferralCommission = (System.Decimal)reader[((int)ReferralCommissionColumn.ReferralCommission - 1)];
					c.ReferralAccountID = (System.Int32)reader[((int)ReferralCommissionColumn.ReferralAccountID - 1)];
					c.OrderID = (reader.IsDBNull(((int)ReferralCommissionColumn.OrderID - 1)))?null:(System.Int32?)reader[((int)ReferralCommissionColumn.OrderID - 1)];
					c.TransactionID = (reader.IsDBNull(((int)ReferralCommissionColumn.TransactionID - 1)))?null:(System.String)reader[((int)ReferralCommissionColumn.TransactionID - 1)];
					c.Description = (reader.IsDBNull(((int)ReferralCommissionColumn.Description - 1)))?null:(System.String)reader[((int)ReferralCommissionColumn.Description - 1)];
					c.CommissionAmount = (reader.IsDBNull(((int)ReferralCommissionColumn.CommissionAmount - 1)))?null:(System.Decimal?)reader[((int)ReferralCommissionColumn.CommissionAmount - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommission"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommission"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ReferralCommission entity)
		{
			if (!reader.Read()) return;
			
			entity.ReferralCommissionID = (System.Int32)reader[((int)ReferralCommissionColumn.ReferralCommissionID - 1)];
			entity.ReferralCommissionTypeID = (System.Int32)reader[((int)ReferralCommissionColumn.ReferralCommissionTypeID - 1)];
			entity.ReferralCommission = (System.Decimal)reader[((int)ReferralCommissionColumn.ReferralCommission - 1)];
			entity.ReferralAccountID = (System.Int32)reader[((int)ReferralCommissionColumn.ReferralAccountID - 1)];
			entity.OrderID = (reader.IsDBNull(((int)ReferralCommissionColumn.OrderID - 1)))?null:(System.Int32?)reader[((int)ReferralCommissionColumn.OrderID - 1)];
			entity.TransactionID = (reader.IsDBNull(((int)ReferralCommissionColumn.TransactionID - 1)))?null:(System.String)reader[((int)ReferralCommissionColumn.TransactionID - 1)];
			entity.Description = (reader.IsDBNull(((int)ReferralCommissionColumn.Description - 1)))?null:(System.String)reader[((int)ReferralCommissionColumn.Description - 1)];
			entity.CommissionAmount = (reader.IsDBNull(((int)ReferralCommissionColumn.CommissionAmount - 1)))?null:(System.Decimal?)reader[((int)ReferralCommissionColumn.CommissionAmount - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommission"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommission"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ReferralCommission entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ReferralCommissionID = (System.Int32)dataRow["ReferralCommissionID"];
			entity.ReferralCommissionTypeID = (System.Int32)dataRow["ReferralCommissionTypeID"];
			entity.ReferralCommission = (System.Decimal)dataRow["ReferralCommission"];
			entity.ReferralAccountID = (System.Int32)dataRow["ReferralAccountID"];
			entity.OrderID = Convert.IsDBNull(dataRow["OrderID"]) ? null : (System.Int32?)dataRow["OrderID"];
			entity.TransactionID = Convert.IsDBNull(dataRow["TransactionID"]) ? null : (System.String)dataRow["TransactionID"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.CommissionAmount = Convert.IsDBNull(dataRow["CommissionAmount"]) ? null : (System.Decimal?)dataRow["CommissionAmount"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommission"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ReferralCommission Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ReferralCommission entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region OrderIDSource	
			if (CanDeepLoad(entity, "Order|OrderIDSource", deepLoadType, innerList) 
				&& entity.OrderIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.OrderID ?? (int)0);
				Order tmpEntity = EntityManager.LocateEntity<Order>(EntityLocator.ConstructKeyFromPkItems(typeof(Order), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.OrderIDSource = tmpEntity;
				else
					entity.OrderIDSource = DataRepository.OrderProvider.GetByOrderID(transactionManager, (entity.OrderID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.OrderIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.OrderProvider.DeepLoad(transactionManager, entity.OrderIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion OrderIDSource

			#region ReferralCommissionTypeIDSource	
			if (CanDeepLoad(entity, "ReferralCommissionType|ReferralCommissionTypeIDSource", deepLoadType, innerList) 
				&& entity.ReferralCommissionTypeIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ReferralCommissionTypeID;
				ReferralCommissionType tmpEntity = EntityManager.LocateEntity<ReferralCommissionType>(EntityLocator.ConstructKeyFromPkItems(typeof(ReferralCommissionType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ReferralCommissionTypeIDSource = tmpEntity;
				else
					entity.ReferralCommissionTypeIDSource = DataRepository.ReferralCommissionTypeProvider.GetByReferralCommissionTypeID(transactionManager, entity.ReferralCommissionTypeID);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ReferralCommissionTypeIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ReferralCommissionTypeIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ReferralCommissionTypeProvider.DeepLoad(transactionManager, entity.ReferralCommissionTypeIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ReferralCommissionTypeIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ReferralCommission object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ReferralCommission instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ReferralCommission Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ReferralCommission entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region OrderIDSource
			if (CanDeepSave(entity, "Order|OrderIDSource", deepSaveType, innerList) 
				&& entity.OrderIDSource != null)
			{
				DataRepository.OrderProvider.Save(transactionManager, entity.OrderIDSource);
				entity.OrderID = entity.OrderIDSource.OrderID;
			}
			#endregion 
			
			#region ReferralCommissionTypeIDSource
			if (CanDeepSave(entity, "ReferralCommissionType|ReferralCommissionTypeIDSource", deepSaveType, innerList) 
				&& entity.ReferralCommissionTypeIDSource != null)
			{
				DataRepository.ReferralCommissionTypeProvider.Save(transactionManager, entity.ReferralCommissionTypeIDSource);
				entity.ReferralCommissionTypeID = entity.ReferralCommissionTypeIDSource.ReferralCommissionTypeID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ReferralCommissionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ReferralCommission</c>
	///</summary>
	public enum ReferralCommissionChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Order</c> at OrderIDSource
		///</summary>
		[ChildEntityType(typeof(Order))]
		Order,
		
		///<summary>
		/// Composite Property for <c>ReferralCommissionType</c> at ReferralCommissionTypeIDSource
		///</summary>
		[ChildEntityType(typeof(ReferralCommissionType))]
		ReferralCommissionType,
	}
	
	#endregion ReferralCommissionChildEntityTypes
	
	#region ReferralCommissionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ReferralCommissionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ReferralCommission"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ReferralCommissionFilterBuilder : SqlFilterBuilder<ReferralCommissionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionFilterBuilder class.
		/// </summary>
		public ReferralCommissionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ReferralCommissionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ReferralCommissionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ReferralCommissionFilterBuilder
	
	#region ReferralCommissionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ReferralCommissionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ReferralCommission"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ReferralCommissionParameterBuilder : ParameterizedSqlFilterBuilder<ReferralCommissionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionParameterBuilder class.
		/// </summary>
		public ReferralCommissionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ReferralCommissionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ReferralCommissionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ReferralCommissionParameterBuilder
	
	#region ReferralCommissionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ReferralCommissionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ReferralCommission"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ReferralCommissionSortBuilder : SqlSortBuilder<ReferralCommissionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionSqlSortBuilder class.
		/// </summary>
		public ReferralCommissionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ReferralCommissionSortBuilder
	
} // end namespace
