﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductImageTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductImageTypeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ProductImageType, ZNode.Libraries.DataAccess.Entities.ProductImageTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductImageTypeKey key)
		{
			return Delete(transactionManager, key.ProductImageTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_productImageTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _productImageTypeID)
		{
			return Delete(null, _productImageTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productImageTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _productImageTypeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ProductImageType Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductImageTypeKey key, int start, int pageLength)
		{
			return GetByProductImageTypeID(transactionManager, key.ProductImageTypeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeProductImageType index.
		/// </summary>
		/// <param name="_productImageTypeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImageType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductImageType GetByProductImageTypeID(System.Int32 _productImageTypeID)
		{
			int count = -1;
			return GetByProductImageTypeID(null,_productImageTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductImageType index.
		/// </summary>
		/// <param name="_productImageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImageType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductImageType GetByProductImageTypeID(System.Int32 _productImageTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductImageTypeID(null, _productImageTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductImageType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productImageTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImageType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductImageType GetByProductImageTypeID(TransactionManager transactionManager, System.Int32 _productImageTypeID)
		{
			int count = -1;
			return GetByProductImageTypeID(transactionManager, _productImageTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductImageType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productImageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImageType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductImageType GetByProductImageTypeID(TransactionManager transactionManager, System.Int32 _productImageTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductImageTypeID(transactionManager, _productImageTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductImageType index.
		/// </summary>
		/// <param name="_productImageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImageType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductImageType GetByProductImageTypeID(System.Int32 _productImageTypeID, int start, int pageLength, out int count)
		{
			return GetByProductImageTypeID(null, _productImageTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductImageType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productImageTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImageType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductImageType GetByProductImageTypeID(TransactionManager transactionManager, System.Int32 _productImageTypeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ProductImageType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ProductImageType&gt;"/></returns>
		public static TList<ProductImageType> Fill(IDataReader reader, TList<ProductImageType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ProductImageType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ProductImageType")
					.Append("|").Append((System.Int32)reader[((int)ProductImageTypeColumn.ProductImageTypeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ProductImageType>(
					key.ToString(), // EntityTrackingKey
					"ProductImageType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ProductImageType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ProductImageTypeID = (System.Int32)reader[((int)ProductImageTypeColumn.ProductImageTypeID - 1)];
					c.OriginalProductImageTypeID = c.ProductImageTypeID;
					c.Name = (System.String)reader[((int)ProductImageTypeColumn.Name - 1)];
					c.Description = (reader.IsDBNull(((int)ProductImageTypeColumn.Description - 1)))?null:(System.String)reader[((int)ProductImageTypeColumn.Description - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImageType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductImageType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ProductImageType entity)
		{
			if (!reader.Read()) return;
			
			entity.ProductImageTypeID = (System.Int32)reader[((int)ProductImageTypeColumn.ProductImageTypeID - 1)];
			entity.OriginalProductImageTypeID = (System.Int32)reader["ProductImageTypeID"];
			entity.Name = (System.String)reader[((int)ProductImageTypeColumn.Name - 1)];
			entity.Description = (reader.IsDBNull(((int)ProductImageTypeColumn.Description - 1)))?null:(System.String)reader[((int)ProductImageTypeColumn.Description - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductImageType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductImageType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ProductImageType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProductImageTypeID = (System.Int32)dataRow["ProductImageTypeID"];
			entity.OriginalProductImageTypeID = (System.Int32)dataRow["ProductImageTypeID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductImageType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductImageType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductImageType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByProductImageTypeID methods when available
			
			#region ProductImageCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductImage>|ProductImageCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductImageCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductImageCollection = DataRepository.ProductImageProvider.GetByProductImageTypeID(transactionManager, entity.ProductImageTypeID);

				if (deep && entity.ProductImageCollection.Count > 0)
				{
					deepHandles.Add("ProductImageCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductImage>) DataRepository.ProductImageProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductImageCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ProductImageType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ProductImageType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductImageType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductImageType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<ProductImage>
				if (CanDeepSave(entity.ProductImageCollection, "List<ProductImage>|ProductImageCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductImage child in entity.ProductImageCollection)
					{
						if(child.ProductImageTypeIDSource != null)
						{
							child.ProductImageTypeID = child.ProductImageTypeIDSource.ProductImageTypeID;
						}
						else
						{
							child.ProductImageTypeID = entity.ProductImageTypeID;
						}

					}

					if (entity.ProductImageCollection.Count > 0 || entity.ProductImageCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductImageProvider.Save(transactionManager, entity.ProductImageCollection);
						
						deepHandles.Add("ProductImageCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductImage >) DataRepository.ProductImageProvider.DeepSave,
							new object[] { transactionManager, entity.ProductImageCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductImageTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ProductImageType</c>
	///</summary>
	public enum ProductImageTypeChildEntityTypes
	{
		///<summary>
		/// Collection of <c>ProductImageType</c> as OneToMany for ProductImageCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductImage>))]
		ProductImageCollection,
	}
	
	#endregion ProductImageTypeChildEntityTypes
	
	#region ProductImageTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductImageTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductImageType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductImageTypeFilterBuilder : SqlFilterBuilder<ProductImageTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductImageTypeFilterBuilder class.
		/// </summary>
		public ProductImageTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductImageTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductImageTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductImageTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductImageTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductImageTypeFilterBuilder
	
	#region ProductImageTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductImageTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductImageType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductImageTypeParameterBuilder : ParameterizedSqlFilterBuilder<ProductImageTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductImageTypeParameterBuilder class.
		/// </summary>
		public ProductImageTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductImageTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductImageTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductImageTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductImageTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductImageTypeParameterBuilder
	
	#region ProductImageTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductImageTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductImageType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductImageTypeSortBuilder : SqlSortBuilder<ProductImageTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductImageTypeSqlSortBuilder class.
		/// </summary>
		public ProductImageTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductImageTypeSortBuilder
	
} // end namespace
