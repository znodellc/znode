﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ReferralCommissionTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ReferralCommissionTypeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ReferralCommissionType, ZNode.Libraries.DataAccess.Entities.ReferralCommissionTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ReferralCommissionTypeKey key)
		{
			return Delete(transactionManager, key.ReferralCommissionTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_referralCommissionTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _referralCommissionTypeID)
		{
			return Delete(null, _referralCommissionTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralCommissionTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _referralCommissionTypeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ReferralCommissionType Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ReferralCommissionTypeKey key, int start, int pageLength)
		{
			return GetByReferralCommissionTypeID(transactionManager, key.ReferralCommissionTypeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeReferralCommissionType index.
		/// </summary>
		/// <param name="_referralCommissionTypeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommissionType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ReferralCommissionType GetByReferralCommissionTypeID(System.Int32 _referralCommissionTypeID)
		{
			int count = -1;
			return GetByReferralCommissionTypeID(null,_referralCommissionTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeReferralCommissionType index.
		/// </summary>
		/// <param name="_referralCommissionTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommissionType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ReferralCommissionType GetByReferralCommissionTypeID(System.Int32 _referralCommissionTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByReferralCommissionTypeID(null, _referralCommissionTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeReferralCommissionType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralCommissionTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommissionType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ReferralCommissionType GetByReferralCommissionTypeID(TransactionManager transactionManager, System.Int32 _referralCommissionTypeID)
		{
			int count = -1;
			return GetByReferralCommissionTypeID(transactionManager, _referralCommissionTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeReferralCommissionType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralCommissionTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommissionType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ReferralCommissionType GetByReferralCommissionTypeID(TransactionManager transactionManager, System.Int32 _referralCommissionTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByReferralCommissionTypeID(transactionManager, _referralCommissionTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeReferralCommissionType index.
		/// </summary>
		/// <param name="_referralCommissionTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommissionType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ReferralCommissionType GetByReferralCommissionTypeID(System.Int32 _referralCommissionTypeID, int start, int pageLength, out int count)
		{
			return GetByReferralCommissionTypeID(null, _referralCommissionTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeReferralCommissionType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_referralCommissionTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommissionType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ReferralCommissionType GetByReferralCommissionTypeID(TransactionManager transactionManager, System.Int32 _referralCommissionTypeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ReferralCommissionType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ReferralCommissionType&gt;"/></returns>
		public static TList<ReferralCommissionType> Fill(IDataReader reader, TList<ReferralCommissionType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ReferralCommissionType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ReferralCommissionType")
					.Append("|").Append((System.Int32)reader[((int)ReferralCommissionTypeColumn.ReferralCommissionTypeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ReferralCommissionType>(
					key.ToString(), // EntityTrackingKey
					"ReferralCommissionType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ReferralCommissionType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ReferralCommissionTypeID = (System.Int32)reader[((int)ReferralCommissionTypeColumn.ReferralCommissionTypeID - 1)];
					c.Name = (System.String)reader[((int)ReferralCommissionTypeColumn.Name - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommissionType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommissionType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ReferralCommissionType entity)
		{
			if (!reader.Read()) return;
			
			entity.ReferralCommissionTypeID = (System.Int32)reader[((int)ReferralCommissionTypeColumn.ReferralCommissionTypeID - 1)];
			entity.Name = (System.String)reader[((int)ReferralCommissionTypeColumn.Name - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommissionType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommissionType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ReferralCommissionType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ReferralCommissionTypeID = (System.Int32)dataRow["ReferralCommissionTypeID"];
			entity.Name = (System.String)dataRow["Name"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ReferralCommissionType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ReferralCommissionType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ReferralCommissionType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByReferralCommissionTypeID methods when available
			
			#region ReferralCommissionCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ReferralCommission>|ReferralCommissionCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ReferralCommissionCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ReferralCommissionCollection = DataRepository.ReferralCommissionProvider.GetByReferralCommissionTypeID(transactionManager, entity.ReferralCommissionTypeID);

				if (deep && entity.ReferralCommissionCollection.Count > 0)
				{
					deepHandles.Add("ReferralCommissionCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ReferralCommission>) DataRepository.ReferralCommissionProvider.DeepLoad,
						new object[] { transactionManager, entity.ReferralCommissionCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region AccountCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Account>|AccountCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'AccountCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.AccountCollection = DataRepository.AccountProvider.GetByReferralCommissionTypeID(transactionManager, entity.ReferralCommissionTypeID);

				if (deep && entity.AccountCollection.Count > 0)
				{
					deepHandles.Add("AccountCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Account>) DataRepository.AccountProvider.DeepLoad,
						new object[] { transactionManager, entity.AccountCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ReferralCommissionType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ReferralCommissionType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ReferralCommissionType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ReferralCommissionType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<ReferralCommission>
				if (CanDeepSave(entity.ReferralCommissionCollection, "List<ReferralCommission>|ReferralCommissionCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ReferralCommission child in entity.ReferralCommissionCollection)
					{
						if(child.ReferralCommissionTypeIDSource != null)
						{
							child.ReferralCommissionTypeID = child.ReferralCommissionTypeIDSource.ReferralCommissionTypeID;
						}
						else
						{
							child.ReferralCommissionTypeID = entity.ReferralCommissionTypeID;
						}

					}

					if (entity.ReferralCommissionCollection.Count > 0 || entity.ReferralCommissionCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ReferralCommissionProvider.Save(transactionManager, entity.ReferralCommissionCollection);
						
						deepHandles.Add("ReferralCommissionCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ReferralCommission >) DataRepository.ReferralCommissionProvider.DeepSave,
							new object[] { transactionManager, entity.ReferralCommissionCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Account>
				if (CanDeepSave(entity.AccountCollection, "List<Account>|AccountCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Account child in entity.AccountCollection)
					{
						if(child.ReferralCommissionTypeIDSource != null)
						{
							child.ReferralCommissionTypeID = child.ReferralCommissionTypeIDSource.ReferralCommissionTypeID;
						}
						else
						{
							child.ReferralCommissionTypeID = entity.ReferralCommissionTypeID;
						}

					}

					if (entity.AccountCollection.Count > 0 || entity.AccountCollection.DeletedItems.Count > 0)
					{
						//DataRepository.AccountProvider.Save(transactionManager, entity.AccountCollection);
						
						deepHandles.Add("AccountCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Account >) DataRepository.AccountProvider.DeepSave,
							new object[] { transactionManager, entity.AccountCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ReferralCommissionTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ReferralCommissionType</c>
	///</summary>
	public enum ReferralCommissionTypeChildEntityTypes
	{
		///<summary>
		/// Collection of <c>ReferralCommissionType</c> as OneToMany for ReferralCommissionCollection
		///</summary>
		[ChildEntityType(typeof(TList<ReferralCommission>))]
		ReferralCommissionCollection,
		///<summary>
		/// Collection of <c>ReferralCommissionType</c> as OneToMany for AccountCollection
		///</summary>
		[ChildEntityType(typeof(TList<Account>))]
		AccountCollection,
	}
	
	#endregion ReferralCommissionTypeChildEntityTypes
	
	#region ReferralCommissionTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ReferralCommissionTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ReferralCommissionType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ReferralCommissionTypeFilterBuilder : SqlFilterBuilder<ReferralCommissionTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionTypeFilterBuilder class.
		/// </summary>
		public ReferralCommissionTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ReferralCommissionTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ReferralCommissionTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ReferralCommissionTypeFilterBuilder
	
	#region ReferralCommissionTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ReferralCommissionTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ReferralCommissionType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ReferralCommissionTypeParameterBuilder : ParameterizedSqlFilterBuilder<ReferralCommissionTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionTypeParameterBuilder class.
		/// </summary>
		public ReferralCommissionTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ReferralCommissionTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ReferralCommissionTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ReferralCommissionTypeParameterBuilder
	
	#region ReferralCommissionTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ReferralCommissionTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ReferralCommissionType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ReferralCommissionTypeSortBuilder : SqlSortBuilder<ReferralCommissionTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ReferralCommissionTypeSqlSortBuilder class.
		/// </summary>
		public ReferralCommissionTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ReferralCommissionTypeSortBuilder
	
} // end namespace
