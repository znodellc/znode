﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="OrderStateProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class OrderStateProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.OrderState, ZNode.Libraries.DataAccess.Entities.OrderStateKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderStateKey key)
		{
			return Delete(transactionManager, key.OrderStateID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_orderStateID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _orderStateID)
		{
			return Delete(null, _orderStateID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderStateID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _orderStateID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.OrderState Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderStateKey key, int start, int pageLength)
		{
			return GetByOrderStateID(transactionManager, key.OrderStateID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key SC_OrderState_PK index.
		/// </summary>
		/// <param name="_orderStateID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderState"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderState GetByOrderStateID(System.Int32 _orderStateID)
		{
			int count = -1;
			return GetByOrderStateID(null,_orderStateID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_OrderState_PK index.
		/// </summary>
		/// <param name="_orderStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderState"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderState GetByOrderStateID(System.Int32 _orderStateID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderStateID(null, _orderStateID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_OrderState_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderStateID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderState"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderState GetByOrderStateID(TransactionManager transactionManager, System.Int32 _orderStateID)
		{
			int count = -1;
			return GetByOrderStateID(transactionManager, _orderStateID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_OrderState_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderState"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderState GetByOrderStateID(TransactionManager transactionManager, System.Int32 _orderStateID, int start, int pageLength)
		{
			int count = -1;
			return GetByOrderStateID(transactionManager, _orderStateID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_OrderState_PK index.
		/// </summary>
		/// <param name="_orderStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderState"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.OrderState GetByOrderStateID(System.Int32 _orderStateID, int start, int pageLength, out int count)
		{
			return GetByOrderStateID(null, _orderStateID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the SC_OrderState_PK index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_orderStateID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.OrderState"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.OrderState GetByOrderStateID(TransactionManager transactionManager, System.Int32 _orderStateID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;OrderState&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;OrderState&gt;"/></returns>
		public static TList<OrderState> Fill(IDataReader reader, TList<OrderState> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.OrderState c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("OrderState")
					.Append("|").Append((System.Int32)reader[((int)OrderStateColumn.OrderStateID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<OrderState>(
					key.ToString(), // EntityTrackingKey
					"OrderState",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.OrderState();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.OrderStateID = (System.Int32)reader[((int)OrderStateColumn.OrderStateID - 1)];
					c.OriginalOrderStateID = c.OrderStateID;
					c.OrderStateName = (reader.IsDBNull(((int)OrderStateColumn.OrderStateName - 1)))?null:(System.String)reader[((int)OrderStateColumn.OrderStateName - 1)];
					c.Description = (reader.IsDBNull(((int)OrderStateColumn.Description - 1)))?null:(System.String)reader[((int)OrderStateColumn.Description - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.OrderState"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.OrderState"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.OrderState entity)
		{
			if (!reader.Read()) return;
			
			entity.OrderStateID = (System.Int32)reader[((int)OrderStateColumn.OrderStateID - 1)];
			entity.OriginalOrderStateID = (System.Int32)reader["OrderStateID"];
			entity.OrderStateName = (reader.IsDBNull(((int)OrderStateColumn.OrderStateName - 1)))?null:(System.String)reader[((int)OrderStateColumn.OrderStateName - 1)];
			entity.Description = (reader.IsDBNull(((int)OrderStateColumn.Description - 1)))?null:(System.String)reader[((int)OrderStateColumn.Description - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.OrderState"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.OrderState"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.OrderState entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.OrderStateID = (System.Int32)dataRow["OrderStateID"];
			entity.OriginalOrderStateID = (System.Int32)dataRow["OrderStateID"];
			entity.OrderStateName = Convert.IsDBNull(dataRow["OrderStateName"]) ? null : (System.String)dataRow["OrderStateName"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.OrderState"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.OrderState Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderState entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByOrderStateID methods when available
			
			#region PortalCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Portal>|PortalCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PortalCollection = DataRepository.PortalProvider.GetByDefaultOrderStateID(transactionManager, entity.OrderStateID);

				if (deep && entity.PortalCollection.Count > 0)
				{
					deepHandles.Add("PortalCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Portal>) DataRepository.PortalProvider.DeepLoad,
						new object[] { transactionManager, entity.PortalCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region OrderLineItemCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<OrderLineItem>|OrderLineItemCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderLineItemCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OrderLineItemCollection = DataRepository.OrderLineItemProvider.GetByOrderLineItemStateID(transactionManager, entity.OrderStateID);

				if (deep && entity.OrderLineItemCollection.Count > 0)
				{
					deepHandles.Add("OrderLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<OrderLineItem>) DataRepository.OrderLineItemProvider.DeepLoad,
						new object[] { transactionManager, entity.OrderLineItemCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region OrderCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Order>|OrderCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'OrderCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.OrderCollection = DataRepository.OrderProvider.GetByOrderStateID(transactionManager, entity.OrderStateID);

				if (deep && entity.OrderCollection.Count > 0)
				{
					deepHandles.Add("OrderCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Order>) DataRepository.OrderProvider.DeepLoad,
						new object[] { transactionManager, entity.OrderCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.OrderState object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.OrderState instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.OrderState Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.OrderState entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Portal>
				if (CanDeepSave(entity.PortalCollection, "List<Portal>|PortalCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Portal child in entity.PortalCollection)
					{
						if(child.DefaultOrderStateIDSource != null)
						{
							child.DefaultOrderStateID = child.DefaultOrderStateIDSource.OrderStateID;
						}
						else
						{
							child.DefaultOrderStateID = entity.OrderStateID;
						}

					}

					if (entity.PortalCollection.Count > 0 || entity.PortalCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PortalProvider.Save(transactionManager, entity.PortalCollection);
						
						deepHandles.Add("PortalCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Portal >) DataRepository.PortalProvider.DeepSave,
							new object[] { transactionManager, entity.PortalCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<OrderLineItem>
				if (CanDeepSave(entity.OrderLineItemCollection, "List<OrderLineItem>|OrderLineItemCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(OrderLineItem child in entity.OrderLineItemCollection)
					{
						if(child.OrderLineItemStateIDSource != null)
						{
							child.OrderLineItemStateID = child.OrderLineItemStateIDSource.OrderStateID;
						}
						else
						{
							child.OrderLineItemStateID = entity.OrderStateID;
						}

					}

					if (entity.OrderLineItemCollection.Count > 0 || entity.OrderLineItemCollection.DeletedItems.Count > 0)
					{
						//DataRepository.OrderLineItemProvider.Save(transactionManager, entity.OrderLineItemCollection);
						
						deepHandles.Add("OrderLineItemCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< OrderLineItem >) DataRepository.OrderLineItemProvider.DeepSave,
							new object[] { transactionManager, entity.OrderLineItemCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<Order>
				if (CanDeepSave(entity.OrderCollection, "List<Order>|OrderCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Order child in entity.OrderCollection)
					{
						if(child.OrderStateIDSource != null)
						{
							child.OrderStateID = child.OrderStateIDSource.OrderStateID;
						}
						else
						{
							child.OrderStateID = entity.OrderStateID;
						}

					}

					if (entity.OrderCollection.Count > 0 || entity.OrderCollection.DeletedItems.Count > 0)
					{
						//DataRepository.OrderProvider.Save(transactionManager, entity.OrderCollection);
						
						deepHandles.Add("OrderCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Order >) DataRepository.OrderProvider.DeepSave,
							new object[] { transactionManager, entity.OrderCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region OrderStateChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.OrderState</c>
	///</summary>
	public enum OrderStateChildEntityTypes
	{
		///<summary>
		/// Collection of <c>OrderState</c> as OneToMany for PortalCollection
		///</summary>
		[ChildEntityType(typeof(TList<Portal>))]
		PortalCollection,
		///<summary>
		/// Collection of <c>OrderState</c> as OneToMany for OrderLineItemCollection
		///</summary>
		[ChildEntityType(typeof(TList<OrderLineItem>))]
		OrderLineItemCollection,
		///<summary>
		/// Collection of <c>OrderState</c> as OneToMany for OrderCollection
		///</summary>
		[ChildEntityType(typeof(TList<Order>))]
		OrderCollection,
	}
	
	#endregion OrderStateChildEntityTypes
	
	#region OrderStateFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;OrderStateColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderState"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderStateFilterBuilder : SqlFilterBuilder<OrderStateColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderStateFilterBuilder class.
		/// </summary>
		public OrderStateFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderStateFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderStateFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderStateFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderStateFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderStateFilterBuilder
	
	#region OrderStateParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;OrderStateColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderState"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class OrderStateParameterBuilder : ParameterizedSqlFilterBuilder<OrderStateColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderStateParameterBuilder class.
		/// </summary>
		public OrderStateParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the OrderStateParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public OrderStateParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the OrderStateParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public OrderStateParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion OrderStateParameterBuilder
	
	#region OrderStateSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;OrderStateColumn&gt;"/> class
	/// that is used exclusively with a <see cref="OrderState"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class OrderStateSortBuilder : SqlSortBuilder<OrderStateColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the OrderStateSqlSortBuilder class.
		/// </summary>
		public OrderStateSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion OrderStateSortBuilder
	
} // end namespace
