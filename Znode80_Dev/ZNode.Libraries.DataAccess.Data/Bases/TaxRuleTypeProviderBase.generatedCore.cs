﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="TaxRuleTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class TaxRuleTypeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.TaxRuleType, ZNode.Libraries.DataAccess.Entities.TaxRuleTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TaxRuleTypeKey key)
		{
			return Delete(transactionManager, key.TaxRuleTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_taxRuleTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _taxRuleTypeID)
		{
			return Delete(null, _taxRuleTypeID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxRuleTypeID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _taxRuleTypeID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.TaxRuleType Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TaxRuleTypeKey key, int start, int pageLength)
		{
			return GetByTaxRuleTypeID(transactionManager, key.TaxRuleTypeID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeTaxRuleType_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public TList<TaxRuleType> GetByActiveInd(System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRuleType_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public TList<TaxRuleType> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRuleType_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public TList<TaxRuleType> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRuleType_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public TList<TaxRuleType> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRuleType_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public TList<TaxRuleType> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRuleType_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public abstract TList<TaxRuleType> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeTaxRuleType_ClassName index.
		/// </summary>
		/// <param name="_className"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public TList<TaxRuleType> GetByClassName(System.String _className)
		{
			int count = -1;
			return GetByClassName(null,_className, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRuleType_ClassName index.
		/// </summary>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public TList<TaxRuleType> GetByClassName(System.String _className, int start, int pageLength)
		{
			int count = -1;
			return GetByClassName(null, _className, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRuleType_ClassName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_className"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public TList<TaxRuleType> GetByClassName(TransactionManager transactionManager, System.String _className)
		{
			int count = -1;
			return GetByClassName(transactionManager, _className, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRuleType_ClassName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public TList<TaxRuleType> GetByClassName(TransactionManager transactionManager, System.String _className, int start, int pageLength)
		{
			int count = -1;
			return GetByClassName(transactionManager, _className, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRuleType_ClassName index.
		/// </summary>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public TList<TaxRuleType> GetByClassName(System.String _className, int start, int pageLength, out int count)
		{
			return GetByClassName(null, _className, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRuleType_ClassName index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_className"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public abstract TList<TaxRuleType> GetByClassName(TransactionManager transactionManager, System.String _className, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeTaxRuleType_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public TList<TaxRuleType> GetByPortalID(System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(null,_portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRuleType_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public TList<TaxRuleType> GetByPortalID(System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRuleType_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public TList<TaxRuleType> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRuleType_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public TList<TaxRuleType> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength)
		{
			int count = -1;
			return GetByPortalID(transactionManager, _portalID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRuleType_PortalID index.
		/// </summary>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public TList<TaxRuleType> GetByPortalID(System.Int32? _portalID, int start, int pageLength, out int count)
		{
			return GetByPortalID(null, _portalID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeTaxRuleType_PortalID index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_portalID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;TaxRuleType&gt;"/> class.</returns>
		public abstract TList<TaxRuleType> GetByPortalID(TransactionManager transactionManager, System.Int32? _portalID, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeTaxRuleType index.
		/// </summary>
		/// <param name="_taxRuleTypeID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRuleType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxRuleType GetByTaxRuleTypeID(System.Int32 _taxRuleTypeID)
		{
			int count = -1;
			return GetByTaxRuleTypeID(null,_taxRuleTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTaxRuleType index.
		/// </summary>
		/// <param name="_taxRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRuleType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxRuleType GetByTaxRuleTypeID(System.Int32 _taxRuleTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByTaxRuleTypeID(null, _taxRuleTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTaxRuleType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxRuleTypeID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRuleType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxRuleType GetByTaxRuleTypeID(TransactionManager transactionManager, System.Int32 _taxRuleTypeID)
		{
			int count = -1;
			return GetByTaxRuleTypeID(transactionManager, _taxRuleTypeID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTaxRuleType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRuleType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxRuleType GetByTaxRuleTypeID(TransactionManager transactionManager, System.Int32 _taxRuleTypeID, int start, int pageLength)
		{
			int count = -1;
			return GetByTaxRuleTypeID(transactionManager, _taxRuleTypeID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTaxRuleType index.
		/// </summary>
		/// <param name="_taxRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRuleType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.TaxRuleType GetByTaxRuleTypeID(System.Int32 _taxRuleTypeID, int start, int pageLength, out int count)
		{
			return GetByTaxRuleTypeID(null, _taxRuleTypeID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeTaxRuleType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_taxRuleTypeID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRuleType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.TaxRuleType GetByTaxRuleTypeID(TransactionManager transactionManager, System.Int32 _taxRuleTypeID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;TaxRuleType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;TaxRuleType&gt;"/></returns>
		public static TList<TaxRuleType> Fill(IDataReader reader, TList<TaxRuleType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.TaxRuleType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("TaxRuleType")
					.Append("|").Append((System.Int32)reader[((int)TaxRuleTypeColumn.TaxRuleTypeID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<TaxRuleType>(
					key.ToString(), // EntityTrackingKey
					"TaxRuleType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.TaxRuleType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.TaxRuleTypeID = (System.Int32)reader[((int)TaxRuleTypeColumn.TaxRuleTypeID - 1)];
					c.ClassName = (reader.IsDBNull(((int)TaxRuleTypeColumn.ClassName - 1)))?null:(System.String)reader[((int)TaxRuleTypeColumn.ClassName - 1)];
					c.Name = (reader.IsDBNull(((int)TaxRuleTypeColumn.Name - 1)))?null:(System.String)reader[((int)TaxRuleTypeColumn.Name - 1)];
					c.Description = (reader.IsDBNull(((int)TaxRuleTypeColumn.Description - 1)))?null:(System.String)reader[((int)TaxRuleTypeColumn.Description - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)TaxRuleTypeColumn.ActiveInd - 1)];
					c.PortalID = (reader.IsDBNull(((int)TaxRuleTypeColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)TaxRuleTypeColumn.PortalID - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRuleType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.TaxRuleType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.TaxRuleType entity)
		{
			if (!reader.Read()) return;
			
			entity.TaxRuleTypeID = (System.Int32)reader[((int)TaxRuleTypeColumn.TaxRuleTypeID - 1)];
			entity.ClassName = (reader.IsDBNull(((int)TaxRuleTypeColumn.ClassName - 1)))?null:(System.String)reader[((int)TaxRuleTypeColumn.ClassName - 1)];
			entity.Name = (reader.IsDBNull(((int)TaxRuleTypeColumn.Name - 1)))?null:(System.String)reader[((int)TaxRuleTypeColumn.Name - 1)];
			entity.Description = (reader.IsDBNull(((int)TaxRuleTypeColumn.Description - 1)))?null:(System.String)reader[((int)TaxRuleTypeColumn.Description - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)TaxRuleTypeColumn.ActiveInd - 1)];
			entity.PortalID = (reader.IsDBNull(((int)TaxRuleTypeColumn.PortalID - 1)))?null:(System.Int32?)reader[((int)TaxRuleTypeColumn.PortalID - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.TaxRuleType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.TaxRuleType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.TaxRuleType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.TaxRuleTypeID = (System.Int32)dataRow["TaxRuleTypeID"];
			entity.ClassName = Convert.IsDBNull(dataRow["ClassName"]) ? null : (System.String)dataRow["ClassName"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.Description = Convert.IsDBNull(dataRow["Description"]) ? null : (System.String)dataRow["Description"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.PortalID = Convert.IsDBNull(dataRow["PortalID"]) ? null : (System.Int32?)dataRow["PortalID"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.TaxRuleType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.TaxRuleType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TaxRuleType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region PortalIDSource	
			if (CanDeepLoad(entity, "Portal|PortalIDSource", deepLoadType, innerList) 
				&& entity.PortalIDSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PortalID ?? (int)0);
				Portal tmpEntity = EntityManager.LocateEntity<Portal>(EntityLocator.ConstructKeyFromPkItems(typeof(Portal), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PortalIDSource = tmpEntity;
				else
					entity.PortalIDSource = DataRepository.PortalProvider.GetByPortalID(transactionManager, (entity.PortalID ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalIDSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PortalIDSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PortalProvider.DeepLoad(transactionManager, entity.PortalIDSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PortalIDSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByTaxRuleTypeID methods when available
			
			#region TaxRuleCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TaxRule>|TaxRuleCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TaxRuleCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TaxRuleCollection = DataRepository.TaxRuleProvider.GetByTaxRuleTypeID(transactionManager, entity.TaxRuleTypeID);

				if (deep && entity.TaxRuleCollection.Count > 0)
				{
					deepHandles.Add("TaxRuleCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TaxRule>) DataRepository.TaxRuleProvider.DeepLoad,
						new object[] { transactionManager, entity.TaxRuleCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.TaxRuleType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.TaxRuleType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.TaxRuleType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.TaxRuleType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region PortalIDSource
			if (CanDeepSave(entity, "Portal|PortalIDSource", deepSaveType, innerList) 
				&& entity.PortalIDSource != null)
			{
				DataRepository.PortalProvider.Save(transactionManager, entity.PortalIDSource);
				entity.PortalID = entity.PortalIDSource.PortalID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<TaxRule>
				if (CanDeepSave(entity.TaxRuleCollection, "List<TaxRule>|TaxRuleCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TaxRule child in entity.TaxRuleCollection)
					{
						if(child.TaxRuleTypeIDSource != null)
						{
							child.TaxRuleTypeID = child.TaxRuleTypeIDSource.TaxRuleTypeID;
						}
						else
						{
							child.TaxRuleTypeID = entity.TaxRuleTypeID;
						}

					}

					if (entity.TaxRuleCollection.Count > 0 || entity.TaxRuleCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TaxRuleProvider.Save(transactionManager, entity.TaxRuleCollection);
						
						deepHandles.Add("TaxRuleCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TaxRule >) DataRepository.TaxRuleProvider.DeepSave,
							new object[] { transactionManager, entity.TaxRuleCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region TaxRuleTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.TaxRuleType</c>
	///</summary>
	public enum TaxRuleTypeChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Portal</c> at PortalIDSource
		///</summary>
		[ChildEntityType(typeof(Portal))]
		Portal,
		///<summary>
		/// Collection of <c>TaxRuleType</c> as OneToMany for TaxRuleCollection
		///</summary>
		[ChildEntityType(typeof(TList<TaxRule>))]
		TaxRuleCollection,
	}
	
	#endregion TaxRuleTypeChildEntityTypes
	
	#region TaxRuleTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;TaxRuleTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TaxRuleType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TaxRuleTypeFilterBuilder : SqlFilterBuilder<TaxRuleTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TaxRuleTypeFilterBuilder class.
		/// </summary>
		public TaxRuleTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TaxRuleTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TaxRuleTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TaxRuleTypeFilterBuilder
	
	#region TaxRuleTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;TaxRuleTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TaxRuleType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class TaxRuleTypeParameterBuilder : ParameterizedSqlFilterBuilder<TaxRuleTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TaxRuleTypeParameterBuilder class.
		/// </summary>
		public TaxRuleTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public TaxRuleTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the TaxRuleTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public TaxRuleTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion TaxRuleTypeParameterBuilder
	
	#region TaxRuleTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;TaxRuleTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="TaxRuleType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class TaxRuleTypeSortBuilder : SqlSortBuilder<TaxRuleTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the TaxRuleTypeSqlSortBuilder class.
		/// </summary>
		public TaxRuleTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion TaxRuleTypeSortBuilder
	
} // end namespace
