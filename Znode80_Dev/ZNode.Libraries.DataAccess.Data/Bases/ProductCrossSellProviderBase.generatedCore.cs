﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductCrossSellProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductCrossSellProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ProductCrossSell, ZNode.Libraries.DataAccess.Entities.ProductCrossSellKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductCrossSellKey key)
		{
			return Delete(transactionManager, key.ProductCrossSellTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_productCrossSellTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _productCrossSellTypeId)
		{
			return Delete(null, _productCrossSellTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productCrossSellTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _productCrossSellTypeId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_ProductCrossSellType_SC_Product key.
		///		FK_SC_ProductCrossSellType_SC_Product Description: 
		/// </summary>
		/// <param name="_productId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCrossSell objects.</returns>
		public TList<ProductCrossSell> GetByProductId(System.Int32 _productId)
		{
			int count = -1;
			return GetByProductId(_productId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_ProductCrossSellType_SC_Product key.
		///		FK_SC_ProductCrossSellType_SC_Product Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCrossSell objects.</returns>
		/// <remarks></remarks>
		public TList<ProductCrossSell> GetByProductId(TransactionManager transactionManager, System.Int32 _productId)
		{
			int count = -1;
			return GetByProductId(transactionManager, _productId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_ProductCrossSellType_SC_Product key.
		///		FK_SC_ProductCrossSellType_SC_Product Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCrossSell objects.</returns>
		public TList<ProductCrossSell> GetByProductId(TransactionManager transactionManager, System.Int32 _productId, int start, int pageLength)
		{
			int count = -1;
			return GetByProductId(transactionManager, _productId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_ProductCrossSellType_SC_Product key.
		///		fKSCProductCrossSellTypeSCProduct Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCrossSell objects.</returns>
		public TList<ProductCrossSell> GetByProductId(System.Int32 _productId, int start, int pageLength)
		{
			int count =  -1;
			return GetByProductId(null, _productId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_ProductCrossSellType_SC_Product key.
		///		fKSCProductCrossSellTypeSCProduct Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_productId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCrossSell objects.</returns>
		public TList<ProductCrossSell> GetByProductId(System.Int32 _productId, int start, int pageLength,out int count)
		{
			return GetByProductId(null, _productId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_SC_ProductCrossSellType_SC_Product key.
		///		FK_SC_ProductCrossSellType_SC_Product Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCrossSell objects.</returns>
		public abstract TList<ProductCrossSell> GetByProductId(TransactionManager transactionManager, System.Int32 _productId, int start, int pageLength, out int count);
		
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductCrossSell_ZnodeProductRelationType key.
		///		FK_ZNodeProductCrossSell_ZnodeProductRelationType Description: 
		/// </summary>
		/// <param name="_relationTypeId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCrossSell objects.</returns>
		public TList<ProductCrossSell> GetByRelationTypeId(System.Int32 _relationTypeId)
		{
			int count = -1;
			return GetByRelationTypeId(_relationTypeId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductCrossSell_ZnodeProductRelationType key.
		///		FK_ZNodeProductCrossSell_ZnodeProductRelationType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_relationTypeId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCrossSell objects.</returns>
		/// <remarks></remarks>
		public TList<ProductCrossSell> GetByRelationTypeId(TransactionManager transactionManager, System.Int32 _relationTypeId)
		{
			int count = -1;
			return GetByRelationTypeId(transactionManager, _relationTypeId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductCrossSell_ZnodeProductRelationType key.
		///		FK_ZNodeProductCrossSell_ZnodeProductRelationType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_relationTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCrossSell objects.</returns>
		public TList<ProductCrossSell> GetByRelationTypeId(TransactionManager transactionManager, System.Int32 _relationTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByRelationTypeId(transactionManager, _relationTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductCrossSell_ZnodeProductRelationType key.
		///		fKZNodeProductCrossSellZnodeProductRelationType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_relationTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCrossSell objects.</returns>
		public TList<ProductCrossSell> GetByRelationTypeId(System.Int32 _relationTypeId, int start, int pageLength)
		{
			int count =  -1;
			return GetByRelationTypeId(null, _relationTypeId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductCrossSell_ZnodeProductRelationType key.
		///		fKZNodeProductCrossSellZnodeProductRelationType Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_relationTypeId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCrossSell objects.</returns>
		public TList<ProductCrossSell> GetByRelationTypeId(System.Int32 _relationTypeId, int start, int pageLength,out int count)
		{
			return GetByRelationTypeId(null, _relationTypeId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductCrossSell_ZnodeProductRelationType key.
		///		FK_ZNodeProductCrossSell_ZnodeProductRelationType Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_relationTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductCrossSell objects.</returns>
		public abstract TList<ProductCrossSell> GetByRelationTypeId(TransactionManager transactionManager, System.Int32 _relationTypeId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ProductCrossSell Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductCrossSellKey key, int start, int pageLength)
		{
			return GetByProductCrossSellTypeId(transactionManager, key.ProductCrossSellTypeId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZnodeProductCrossSell_ProductID_RelationTypeId index.
		/// </summary>
		/// <param name="_productId"></param>
		/// <param name="_relationTypeId"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCrossSell&gt;"/> class.</returns>
		public TList<ProductCrossSell> GetByProductIdRelationTypeId(System.Int32 _productId, System.Int32 _relationTypeId)
		{
			int count = -1;
			return GetByProductIdRelationTypeId(null,_productId, _relationTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeProductCrossSell_ProductID_RelationTypeId index.
		/// </summary>
		/// <param name="_productId"></param>
		/// <param name="_relationTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCrossSell&gt;"/> class.</returns>
		public TList<ProductCrossSell> GetByProductIdRelationTypeId(System.Int32 _productId, System.Int32 _relationTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByProductIdRelationTypeId(null, _productId, _relationTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeProductCrossSell_ProductID_RelationTypeId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productId"></param>
		/// <param name="_relationTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCrossSell&gt;"/> class.</returns>
		public TList<ProductCrossSell> GetByProductIdRelationTypeId(TransactionManager transactionManager, System.Int32 _productId, System.Int32 _relationTypeId)
		{
			int count = -1;
			return GetByProductIdRelationTypeId(transactionManager, _productId, _relationTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeProductCrossSell_ProductID_RelationTypeId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productId"></param>
		/// <param name="_relationTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCrossSell&gt;"/> class.</returns>
		public TList<ProductCrossSell> GetByProductIdRelationTypeId(TransactionManager transactionManager, System.Int32 _productId, System.Int32 _relationTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByProductIdRelationTypeId(transactionManager, _productId, _relationTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeProductCrossSell_ProductID_RelationTypeId index.
		/// </summary>
		/// <param name="_productId"></param>
		/// <param name="_relationTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCrossSell&gt;"/> class.</returns>
		public TList<ProductCrossSell> GetByProductIdRelationTypeId(System.Int32 _productId, System.Int32 _relationTypeId, int start, int pageLength, out int count)
		{
			return GetByProductIdRelationTypeId(null, _productId, _relationTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZnodeProductCrossSell_ProductID_RelationTypeId index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productId"></param>
		/// <param name="_relationTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;ProductCrossSell&gt;"/> class.</returns>
		public abstract TList<ProductCrossSell> GetByProductIdRelationTypeId(TransactionManager transactionManager, System.Int32 _productId, System.Int32 _relationTypeId, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_SC_ProductCrossSellType index.
		/// </summary>
		/// <param name="_productCrossSellTypeId"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCrossSell"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductCrossSell GetByProductCrossSellTypeId(System.Int32 _productCrossSellTypeId)
		{
			int count = -1;
			return GetByProductCrossSellTypeId(null,_productCrossSellTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ProductCrossSellType index.
		/// </summary>
		/// <param name="_productCrossSellTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCrossSell"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductCrossSell GetByProductCrossSellTypeId(System.Int32 _productCrossSellTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByProductCrossSellTypeId(null, _productCrossSellTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ProductCrossSellType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productCrossSellTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCrossSell"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductCrossSell GetByProductCrossSellTypeId(TransactionManager transactionManager, System.Int32 _productCrossSellTypeId)
		{
			int count = -1;
			return GetByProductCrossSellTypeId(transactionManager, _productCrossSellTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ProductCrossSellType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productCrossSellTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCrossSell"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductCrossSell GetByProductCrossSellTypeId(TransactionManager transactionManager, System.Int32 _productCrossSellTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByProductCrossSellTypeId(transactionManager, _productCrossSellTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ProductCrossSellType index.
		/// </summary>
		/// <param name="_productCrossSellTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCrossSell"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductCrossSell GetByProductCrossSellTypeId(System.Int32 _productCrossSellTypeId, int start, int pageLength, out int count)
		{
			return GetByProductCrossSellTypeId(null, _productCrossSellTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_SC_ProductCrossSellType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productCrossSellTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCrossSell"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductCrossSell GetByProductCrossSellTypeId(TransactionManager transactionManager, System.Int32 _productCrossSellTypeId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ProductCrossSell&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ProductCrossSell&gt;"/></returns>
		public static TList<ProductCrossSell> Fill(IDataReader reader, TList<ProductCrossSell> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ProductCrossSell c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ProductCrossSell")
					.Append("|").Append((System.Int32)reader[((int)ProductCrossSellColumn.ProductCrossSellTypeId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ProductCrossSell>(
					key.ToString(), // EntityTrackingKey
					"ProductCrossSell",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ProductCrossSell();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ProductCrossSellTypeId = (System.Int32)reader[((int)ProductCrossSellColumn.ProductCrossSellTypeId - 1)];
					c.ProductId = (System.Int32)reader[((int)ProductCrossSellColumn.ProductId - 1)];
					c.RelatedProductId = (System.Int32)reader[((int)ProductCrossSellColumn.RelatedProductId - 1)];
					c.DisplayOrder = (reader.IsDBNull(((int)ProductCrossSellColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)ProductCrossSellColumn.DisplayOrder - 1)];
					c.RelationTypeId = (System.Int32)reader[((int)ProductCrossSellColumn.RelationTypeId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCrossSell"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductCrossSell"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ProductCrossSell entity)
		{
			if (!reader.Read()) return;
			
			entity.ProductCrossSellTypeId = (System.Int32)reader[((int)ProductCrossSellColumn.ProductCrossSellTypeId - 1)];
			entity.ProductId = (System.Int32)reader[((int)ProductCrossSellColumn.ProductId - 1)];
			entity.RelatedProductId = (System.Int32)reader[((int)ProductCrossSellColumn.RelatedProductId - 1)];
			entity.DisplayOrder = (reader.IsDBNull(((int)ProductCrossSellColumn.DisplayOrder - 1)))?null:(System.Int32?)reader[((int)ProductCrossSellColumn.DisplayOrder - 1)];
			entity.RelationTypeId = (System.Int32)reader[((int)ProductCrossSellColumn.RelationTypeId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductCrossSell"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductCrossSell"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ProductCrossSell entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProductCrossSellTypeId = (System.Int32)dataRow["ProductCrossSellTypeId"];
			entity.ProductId = (System.Int32)dataRow["ProductId"];
			entity.RelatedProductId = (System.Int32)dataRow["RelatedProductId"];
			entity.DisplayOrder = Convert.IsDBNull(dataRow["DisplayOrder"]) ? null : (System.Int32?)dataRow["DisplayOrder"];
			entity.RelationTypeId = (System.Int32)dataRow["RelationTypeId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductCrossSell"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductCrossSell Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductCrossSell entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region ProductIdSource	
			if (CanDeepLoad(entity, "Product|ProductIdSource", deepLoadType, innerList) 
				&& entity.ProductIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.ProductId;
				Product tmpEntity = EntityManager.LocateEntity<Product>(EntityLocator.ConstructKeyFromPkItems(typeof(Product), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.ProductIdSource = tmpEntity;
				else
					entity.ProductIdSource = DataRepository.ProductProvider.GetByProductID(transactionManager, entity.ProductId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.ProductIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductProvider.DeepLoad(transactionManager, entity.ProductIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion ProductIdSource

			#region RelationTypeIdSource	
			if (CanDeepLoad(entity, "ProductRelationType|RelationTypeIdSource", deepLoadType, innerList) 
				&& entity.RelationTypeIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = entity.RelationTypeId;
				ProductRelationType tmpEntity = EntityManager.LocateEntity<ProductRelationType>(EntityLocator.ConstructKeyFromPkItems(typeof(ProductRelationType), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.RelationTypeIdSource = tmpEntity;
				else
					entity.RelationTypeIdSource = DataRepository.ProductRelationTypeProvider.GetByProductRelationTypeId(transactionManager, entity.RelationTypeId);		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'RelationTypeIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.RelationTypeIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.ProductRelationTypeProvider.DeepLoad(transactionManager, entity.RelationTypeIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion RelationTypeIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ProductCrossSell object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ProductCrossSell instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductCrossSell Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductCrossSell entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region ProductIdSource
			if (CanDeepSave(entity, "Product|ProductIdSource", deepSaveType, innerList) 
				&& entity.ProductIdSource != null)
			{
				DataRepository.ProductProvider.Save(transactionManager, entity.ProductIdSource);
				entity.ProductId = entity.ProductIdSource.ProductID;
			}
			#endregion 
			
			#region RelationTypeIdSource
			if (CanDeepSave(entity, "ProductRelationType|RelationTypeIdSource", deepSaveType, innerList) 
				&& entity.RelationTypeIdSource != null)
			{
				DataRepository.ProductRelationTypeProvider.Save(transactionManager, entity.RelationTypeIdSource);
				entity.RelationTypeId = entity.RelationTypeIdSource.ProductRelationTypeId;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductCrossSellChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ProductCrossSell</c>
	///</summary>
	public enum ProductCrossSellChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>Product</c> at ProductIdSource
		///</summary>
		[ChildEntityType(typeof(Product))]
		Product,
		
		///<summary>
		/// Composite Property for <c>ProductRelationType</c> at RelationTypeIdSource
		///</summary>
		[ChildEntityType(typeof(ProductRelationType))]
		ProductRelationType,
	}
	
	#endregion ProductCrossSellChildEntityTypes
	
	#region ProductCrossSellFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductCrossSellColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductCrossSell"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductCrossSellFilterBuilder : SqlFilterBuilder<ProductCrossSellColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductCrossSellFilterBuilder class.
		/// </summary>
		public ProductCrossSellFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductCrossSellFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductCrossSellFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductCrossSellFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductCrossSellFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductCrossSellFilterBuilder
	
	#region ProductCrossSellParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductCrossSellColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductCrossSell"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductCrossSellParameterBuilder : ParameterizedSqlFilterBuilder<ProductCrossSellColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductCrossSellParameterBuilder class.
		/// </summary>
		public ProductCrossSellParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductCrossSellParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductCrossSellParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductCrossSellParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductCrossSellParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductCrossSellParameterBuilder
	
	#region ProductCrossSellSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductCrossSellColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductCrossSell"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductCrossSellSortBuilder : SqlSortBuilder<ProductCrossSellColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductCrossSellSqlSortBuilder class.
		/// </summary>
		public ProductCrossSellSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductCrossSellSortBuilder
	
} // end namespace
