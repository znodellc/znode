﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductExtensionProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductExtensionProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ProductExtension, ZNode.Libraries.DataAccess.Entities.ProductExtensionKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtensionKey key)
		{
			return Delete(transactionManager, key.ProductExtensionID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_productExtensionID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _productExtensionID)
		{
			return Delete(null, _productExtensionID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _productExtensionID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
	
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtension_ZNodePriceList key.
		///		FK_ZNodeProductExtension_ZNodePriceList Description: 
		/// </summary>
		/// <param name="_priceListId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtension objects.</returns>
		public TList<ProductExtension> GetByPriceListId(System.Int32? _priceListId)
		{
			int count = -1;
			return GetByPriceListId(_priceListId, 0,int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtension_ZNodePriceList key.
		///		FK_ZNodeProductExtension_ZNodePriceList Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_priceListId"></param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtension objects.</returns>
		/// <remarks></remarks>
		public TList<ProductExtension> GetByPriceListId(TransactionManager transactionManager, System.Int32? _priceListId)
		{
			int count = -1;
			return GetByPriceListId(transactionManager, _priceListId, 0, int.MaxValue, out count);
		}
		
			/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtension_ZNodePriceList key.
		///		FK_ZNodeProductExtension_ZNodePriceList Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_priceListId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		///  <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtension objects.</returns>
		public TList<ProductExtension> GetByPriceListId(TransactionManager transactionManager, System.Int32? _priceListId, int start, int pageLength)
		{
			int count = -1;
			return GetByPriceListId(transactionManager, _priceListId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtension_ZNodePriceList key.
		///		fKZNodeProductExtensionZNodePriceList Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_priceListId"></param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtension objects.</returns>
		public TList<ProductExtension> GetByPriceListId(System.Int32? _priceListId, int start, int pageLength)
		{
			int count =  -1;
			return GetByPriceListId(null, _priceListId, start, pageLength,out count);	
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtension_ZNodePriceList key.
		///		fKZNodeProductExtensionZNodePriceList Description: 
		/// </summary>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="_priceListId"></param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtension objects.</returns>
		public TList<ProductExtension> GetByPriceListId(System.Int32? _priceListId, int start, int pageLength,out int count)
		{
			return GetByPriceListId(null, _priceListId, start, pageLength, out count);	
		}
						
		/// <summary>
		/// 	Gets rows from the datasource based on the FK_ZNodeProductExtension_ZNodePriceList key.
		///		FK_ZNodeProductExtension_ZNodePriceList Description: 
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_priceListId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns a typed collection of ZNode.Libraries.DataAccess.Entities.ProductExtension objects.</returns>
		public abstract TList<ProductExtension> GetByPriceListId(TransactionManager transactionManager, System.Int32? _priceListId, int start, int pageLength, out int count);
		
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ProductExtension Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtensionKey key, int start, int pageLength)
		{
			return GetByProductExtensionID(transactionManager, key.ProductExtensionID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeProductExtension index.
		/// </summary>
		/// <param name="_productExtensionID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtension"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtension GetByProductExtensionID(System.Int32 _productExtensionID)
		{
			int count = -1;
			return GetByProductExtensionID(null,_productExtensionID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtension index.
		/// </summary>
		/// <param name="_productExtensionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtension"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtension GetByProductExtensionID(System.Int32 _productExtensionID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductExtensionID(null, _productExtensionID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtension index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtension"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtension GetByProductExtensionID(TransactionManager transactionManager, System.Int32 _productExtensionID)
		{
			int count = -1;
			return GetByProductExtensionID(transactionManager, _productExtensionID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtension index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtension"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtension GetByProductExtensionID(TransactionManager transactionManager, System.Int32 _productExtensionID, int start, int pageLength)
		{
			int count = -1;
			return GetByProductExtensionID(transactionManager, _productExtensionID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtension index.
		/// </summary>
		/// <param name="_productExtensionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtension"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductExtension GetByProductExtensionID(System.Int32 _productExtensionID, int start, int pageLength, out int count)
		{
			return GetByProductExtensionID(null, _productExtensionID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeProductExtension index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productExtensionID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtension"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductExtension GetByProductExtensionID(TransactionManager transactionManager, System.Int32 _productExtensionID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ProductExtension&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ProductExtension&gt;"/></returns>
		public static TList<ProductExtension> Fill(IDataReader reader, TList<ProductExtension> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ProductExtension c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ProductExtension")
					.Append("|").Append((System.Int32)reader[((int)ProductExtensionColumn.ProductExtensionID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ProductExtension>(
					key.ToString(), // EntityTrackingKey
					"ProductExtension",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ProductExtension();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ProductExtensionID = (System.Int32)reader[((int)ProductExtensionColumn.ProductExtensionID - 1)];
					c.Name = (reader.IsDBNull(((int)ProductExtensionColumn.Name - 1)))?null:(System.String)reader[((int)ProductExtensionColumn.Name - 1)];
					c.ProductID = (reader.IsDBNull(((int)ProductExtensionColumn.ProductID - 1)))?null:(System.Int32?)reader[((int)ProductExtensionColumn.ProductID - 1)];
					c.ShortDescription = (reader.IsDBNull(((int)ProductExtensionColumn.ShortDescription - 1)))?null:(System.String)reader[((int)ProductExtensionColumn.ShortDescription - 1)];
					c.SKU = (reader.IsDBNull(((int)ProductExtensionColumn.SKU - 1)))?null:(System.String)reader[((int)ProductExtensionColumn.SKU - 1)];
					c.Price = (reader.IsDBNull(((int)ProductExtensionColumn.Price - 1)))?null:(System.Decimal?)reader[((int)ProductExtensionColumn.Price - 1)];
					c.GUID = (reader.IsDBNull(((int)ProductExtensionColumn.GUID - 1)))?null:(System.String)reader[((int)ProductExtensionColumn.GUID - 1)];
					c.ProfileID = (reader.IsDBNull(((int)ProductExtensionColumn.ProfileID - 1)))?null:(System.Int32?)reader[((int)ProductExtensionColumn.ProfileID - 1)];
					c.EffectiveBegin = (reader.IsDBNull(((int)ProductExtensionColumn.EffectiveBegin - 1)))?null:(System.DateTime?)reader[((int)ProductExtensionColumn.EffectiveBegin - 1)];
					c.EffectiveEnd = (reader.IsDBNull(((int)ProductExtensionColumn.EffectiveEnd - 1)))?null:(System.DateTime?)reader[((int)ProductExtensionColumn.EffectiveEnd - 1)];
					c.IsActive = (reader.IsDBNull(((int)ProductExtensionColumn.IsActive - 1)))?null:(System.Boolean?)reader[((int)ProductExtensionColumn.IsActive - 1)];
					c.Created = (reader.IsDBNull(((int)ProductExtensionColumn.Created - 1)))?null:(System.DateTime?)reader[((int)ProductExtensionColumn.Created - 1)];
					c.Modified = (reader.IsDBNull(((int)ProductExtensionColumn.Modified - 1)))?null:(System.DateTime?)reader[((int)ProductExtensionColumn.Modified - 1)];
					c.Quantity = (reader.IsDBNull(((int)ProductExtensionColumn.Quantity - 1)))?null:(System.Int32?)reader[((int)ProductExtensionColumn.Quantity - 1)];
					c.PriceListId = (reader.IsDBNull(((int)ProductExtensionColumn.PriceListId - 1)))?null:(System.Int32?)reader[((int)ProductExtensionColumn.PriceListId - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtension"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtension"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ProductExtension entity)
		{
			if (!reader.Read()) return;
			
			entity.ProductExtensionID = (System.Int32)reader[((int)ProductExtensionColumn.ProductExtensionID - 1)];
			entity.Name = (reader.IsDBNull(((int)ProductExtensionColumn.Name - 1)))?null:(System.String)reader[((int)ProductExtensionColumn.Name - 1)];
			entity.ProductID = (reader.IsDBNull(((int)ProductExtensionColumn.ProductID - 1)))?null:(System.Int32?)reader[((int)ProductExtensionColumn.ProductID - 1)];
			entity.ShortDescription = (reader.IsDBNull(((int)ProductExtensionColumn.ShortDescription - 1)))?null:(System.String)reader[((int)ProductExtensionColumn.ShortDescription - 1)];
			entity.SKU = (reader.IsDBNull(((int)ProductExtensionColumn.SKU - 1)))?null:(System.String)reader[((int)ProductExtensionColumn.SKU - 1)];
			entity.Price = (reader.IsDBNull(((int)ProductExtensionColumn.Price - 1)))?null:(System.Decimal?)reader[((int)ProductExtensionColumn.Price - 1)];
			entity.GUID = (reader.IsDBNull(((int)ProductExtensionColumn.GUID - 1)))?null:(System.String)reader[((int)ProductExtensionColumn.GUID - 1)];
			entity.ProfileID = (reader.IsDBNull(((int)ProductExtensionColumn.ProfileID - 1)))?null:(System.Int32?)reader[((int)ProductExtensionColumn.ProfileID - 1)];
			entity.EffectiveBegin = (reader.IsDBNull(((int)ProductExtensionColumn.EffectiveBegin - 1)))?null:(System.DateTime?)reader[((int)ProductExtensionColumn.EffectiveBegin - 1)];
			entity.EffectiveEnd = (reader.IsDBNull(((int)ProductExtensionColumn.EffectiveEnd - 1)))?null:(System.DateTime?)reader[((int)ProductExtensionColumn.EffectiveEnd - 1)];
			entity.IsActive = (reader.IsDBNull(((int)ProductExtensionColumn.IsActive - 1)))?null:(System.Boolean?)reader[((int)ProductExtensionColumn.IsActive - 1)];
			entity.Created = (reader.IsDBNull(((int)ProductExtensionColumn.Created - 1)))?null:(System.DateTime?)reader[((int)ProductExtensionColumn.Created - 1)];
			entity.Modified = (reader.IsDBNull(((int)ProductExtensionColumn.Modified - 1)))?null:(System.DateTime?)reader[((int)ProductExtensionColumn.Modified - 1)];
			entity.Quantity = (reader.IsDBNull(((int)ProductExtensionColumn.Quantity - 1)))?null:(System.Int32?)reader[((int)ProductExtensionColumn.Quantity - 1)];
			entity.PriceListId = (reader.IsDBNull(((int)ProductExtensionColumn.PriceListId - 1)))?null:(System.Int32?)reader[((int)ProductExtensionColumn.PriceListId - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtension"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtension"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ProductExtension entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProductExtensionID = (System.Int32)dataRow["ProductExtensionID"];
			entity.Name = Convert.IsDBNull(dataRow["Name"]) ? null : (System.String)dataRow["Name"];
			entity.ProductID = Convert.IsDBNull(dataRow["ProductID"]) ? null : (System.Int32?)dataRow["ProductID"];
			entity.ShortDescription = Convert.IsDBNull(dataRow["ShortDescription"]) ? null : (System.String)dataRow["ShortDescription"];
			entity.SKU = Convert.IsDBNull(dataRow["SKU"]) ? null : (System.String)dataRow["SKU"];
			entity.Price = Convert.IsDBNull(dataRow["Price"]) ? null : (System.Decimal?)dataRow["Price"];
			entity.GUID = Convert.IsDBNull(dataRow["GUID"]) ? null : (System.String)dataRow["GUID"];
			entity.ProfileID = Convert.IsDBNull(dataRow["ProfileID"]) ? null : (System.Int32?)dataRow["ProfileID"];
			entity.EffectiveBegin = Convert.IsDBNull(dataRow["EffectiveBegin"]) ? null : (System.DateTime?)dataRow["EffectiveBegin"];
			entity.EffectiveEnd = Convert.IsDBNull(dataRow["EffectiveEnd"]) ? null : (System.DateTime?)dataRow["EffectiveEnd"];
			entity.IsActive = Convert.IsDBNull(dataRow["IsActive"]) ? null : (System.Boolean?)dataRow["IsActive"];
			entity.Created = Convert.IsDBNull(dataRow["Created"]) ? null : (System.DateTime?)dataRow["Created"];
			entity.Modified = Convert.IsDBNull(dataRow["Modified"]) ? null : (System.DateTime?)dataRow["Modified"];
			entity.Quantity = Convert.IsDBNull(dataRow["Quantity"]) ? null : (System.Int32?)dataRow["Quantity"];
			entity.PriceListId = Convert.IsDBNull(dataRow["PriceListId"]) ? null : (System.Int32?)dataRow["PriceListId"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductExtension"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductExtension Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtension entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;

			#region PriceListIdSource	
			if (CanDeepLoad(entity, "PriceList|PriceListIdSource", deepLoadType, innerList) 
				&& entity.PriceListIdSource == null)
			{
				object[] pkItems = new object[1];
				pkItems[0] = (entity.PriceListId ?? (int)0);
				PriceList tmpEntity = EntityManager.LocateEntity<PriceList>(EntityLocator.ConstructKeyFromPkItems(typeof(PriceList), pkItems), DataRepository.Provider.EnableEntityTracking);
				if (tmpEntity != null)
					entity.PriceListIdSource = tmpEntity;
				else
					entity.PriceListIdSource = DataRepository.PriceListProvider.GetByPriceListID(transactionManager, (entity.PriceListId ?? (int)0));		
				
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PriceListIdSource' loaded. key " + entity.EntityTrackingKey);
				#endif 
				
				if (deep && entity.PriceListIdSource != null)
				{
					innerList.SkipChildren = true;
					DataRepository.PriceListProvider.DeepLoad(transactionManager, entity.PriceListIdSource, deep, deepLoadType, childTypes, innerList);
					innerList.SkipChildren = false;
				}
					
			}
			#endregion PriceListIdSource
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByProductExtensionID methods when available
			
			#region ProductExtensionRelationCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductExtensionRelation>|ProductExtensionRelationCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductExtensionRelationCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductExtensionRelationCollection = DataRepository.ProductExtensionRelationProvider.GetByProductExtensionId(transactionManager, entity.ProductExtensionID);

				if (deep && entity.ProductExtensionRelationCollection.Count > 0)
				{
					deepHandles.Add("ProductExtensionRelationCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductExtensionRelation>) DataRepository.ProductExtensionRelationProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductExtensionRelationCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ProductExtension object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ProductExtension instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductExtension Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductExtension entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			
			#region PriceListIdSource
			if (CanDeepSave(entity, "PriceList|PriceListIdSource", deepSaveType, innerList) 
				&& entity.PriceListIdSource != null)
			{
				DataRepository.PriceListProvider.Save(transactionManager, entity.PriceListIdSource);
				entity.PriceListId = entity.PriceListIdSource.PriceListID;
			}
			#endregion 
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<ProductExtensionRelation>
				if (CanDeepSave(entity.ProductExtensionRelationCollection, "List<ProductExtensionRelation>|ProductExtensionRelationCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductExtensionRelation child in entity.ProductExtensionRelationCollection)
					{
						if(child.ProductExtensionIdSource != null)
						{
							child.ProductExtensionId = child.ProductExtensionIdSource.ProductExtensionID;
						}
						else
						{
							child.ProductExtensionId = entity.ProductExtensionID;
						}

					}

					if (entity.ProductExtensionRelationCollection.Count > 0 || entity.ProductExtensionRelationCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductExtensionRelationProvider.Save(transactionManager, entity.ProductExtensionRelationCollection);
						
						deepHandles.Add("ProductExtensionRelationCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductExtensionRelation >) DataRepository.ProductExtensionRelationProvider.DeepSave,
							new object[] { transactionManager, entity.ProductExtensionRelationCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductExtensionChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ProductExtension</c>
	///</summary>
	public enum ProductExtensionChildEntityTypes
	{
		
		///<summary>
		/// Composite Property for <c>PriceList</c> at PriceListIdSource
		///</summary>
		[ChildEntityType(typeof(PriceList))]
		PriceList,
		///<summary>
		/// Collection of <c>ProductExtension</c> as OneToMany for ProductExtensionRelationCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductExtensionRelation>))]
		ProductExtensionRelationCollection,
	}
	
	#endregion ProductExtensionChildEntityTypes
	
	#region ProductExtensionFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductExtensionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtension"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionFilterBuilder : SqlFilterBuilder<ProductExtensionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionFilterBuilder class.
		/// </summary>
		public ProductExtensionFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionFilterBuilder
	
	#region ProductExtensionParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductExtensionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtension"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductExtensionParameterBuilder : ParameterizedSqlFilterBuilder<ProductExtensionColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionParameterBuilder class.
		/// </summary>
		public ProductExtensionParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductExtensionParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductExtensionParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductExtensionParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductExtensionParameterBuilder
	
	#region ProductExtensionSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductExtensionColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductExtension"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductExtensionSortBuilder : SqlSortBuilder<ProductExtensionColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductExtensionSqlSortBuilder class.
		/// </summary>
		public ProductExtensionSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductExtensionSortBuilder
	
} // end namespace
