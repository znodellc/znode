﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="RMAConfigurationProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class RMAConfigurationProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.RMAConfiguration, ZNode.Libraries.DataAccess.Entities.RMAConfigurationKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RMAConfigurationKey key)
		{
			return Delete(transactionManager, key.RMAConfigID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_rMAConfigID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _rMAConfigID)
		{
			return Delete(null, _rMAConfigID);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_rMAConfigID">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _rMAConfigID);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.RMAConfiguration Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RMAConfigurationKey key, int start, int pageLength)
		{
			return GetByRMAConfigID(transactionManager, key.RMAConfigID, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeRMAConfiguration index.
		/// </summary>
		/// <param name="_rMAConfigID"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMAConfiguration"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMAConfiguration GetByRMAConfigID(System.Int32 _rMAConfigID)
		{
			int count = -1;
			return GetByRMAConfigID(null,_rMAConfigID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMAConfiguration index.
		/// </summary>
		/// <param name="_rMAConfigID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMAConfiguration"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMAConfiguration GetByRMAConfigID(System.Int32 _rMAConfigID, int start, int pageLength)
		{
			int count = -1;
			return GetByRMAConfigID(null, _rMAConfigID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMAConfiguration index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_rMAConfigID"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMAConfiguration"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMAConfiguration GetByRMAConfigID(TransactionManager transactionManager, System.Int32 _rMAConfigID)
		{
			int count = -1;
			return GetByRMAConfigID(transactionManager, _rMAConfigID, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMAConfiguration index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_rMAConfigID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMAConfiguration"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMAConfiguration GetByRMAConfigID(TransactionManager transactionManager, System.Int32 _rMAConfigID, int start, int pageLength)
		{
			int count = -1;
			return GetByRMAConfigID(transactionManager, _rMAConfigID, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMAConfiguration index.
		/// </summary>
		/// <param name="_rMAConfigID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMAConfiguration"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.RMAConfiguration GetByRMAConfigID(System.Int32 _rMAConfigID, int start, int pageLength, out int count)
		{
			return GetByRMAConfigID(null, _rMAConfigID, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeRMAConfiguration index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_rMAConfigID"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.RMAConfiguration"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.RMAConfiguration GetByRMAConfigID(TransactionManager transactionManager, System.Int32 _rMAConfigID, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;RMAConfiguration&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;RMAConfiguration&gt;"/></returns>
		public static TList<RMAConfiguration> Fill(IDataReader reader, TList<RMAConfiguration> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.RMAConfiguration c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("RMAConfiguration")
					.Append("|").Append((System.Int32)reader[((int)RMAConfigurationColumn.RMAConfigID - 1)]).ToString();
					c = EntityManager.LocateOrCreate<RMAConfiguration>(
					key.ToString(), // EntityTrackingKey
					"RMAConfiguration",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.RMAConfiguration();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.RMAConfigID = (System.Int32)reader[((int)RMAConfigurationColumn.RMAConfigID - 1)];
					c.MaxDays = (reader.IsDBNull(((int)RMAConfigurationColumn.MaxDays - 1)))?null:(System.Int32?)reader[((int)RMAConfigurationColumn.MaxDays - 1)];
					c.DisplayName = (reader.IsDBNull(((int)RMAConfigurationColumn.DisplayName - 1)))?null:(System.String)reader[((int)RMAConfigurationColumn.DisplayName - 1)];
					c.Email = (reader.IsDBNull(((int)RMAConfigurationColumn.Email - 1)))?null:(System.String)reader[((int)RMAConfigurationColumn.Email - 1)];
					c.Address = (reader.IsDBNull(((int)RMAConfigurationColumn.Address - 1)))?null:(System.String)reader[((int)RMAConfigurationColumn.Address - 1)];
					c.ShippingDirections = (reader.IsDBNull(((int)RMAConfigurationColumn.ShippingDirections - 1)))?null:(System.String)reader[((int)RMAConfigurationColumn.ShippingDirections - 1)];
					c.EnableEmailNotification = (reader.IsDBNull(((int)RMAConfigurationColumn.EnableEmailNotification - 1)))?null:(System.Boolean?)reader[((int)RMAConfigurationColumn.EnableEmailNotification - 1)];
					c.GCExpirationPeriod = (reader.IsDBNull(((int)RMAConfigurationColumn.GCExpirationPeriod - 1)))?null:(System.Int32?)reader[((int)RMAConfigurationColumn.GCExpirationPeriod - 1)];
					c.GCNotification = (reader.IsDBNull(((int)RMAConfigurationColumn.GCNotification - 1)))?null:(System.String)reader[((int)RMAConfigurationColumn.GCNotification - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.RMAConfiguration"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.RMAConfiguration"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.RMAConfiguration entity)
		{
			if (!reader.Read()) return;
			
			entity.RMAConfigID = (System.Int32)reader[((int)RMAConfigurationColumn.RMAConfigID - 1)];
			entity.MaxDays = (reader.IsDBNull(((int)RMAConfigurationColumn.MaxDays - 1)))?null:(System.Int32?)reader[((int)RMAConfigurationColumn.MaxDays - 1)];
			entity.DisplayName = (reader.IsDBNull(((int)RMAConfigurationColumn.DisplayName - 1)))?null:(System.String)reader[((int)RMAConfigurationColumn.DisplayName - 1)];
			entity.Email = (reader.IsDBNull(((int)RMAConfigurationColumn.Email - 1)))?null:(System.String)reader[((int)RMAConfigurationColumn.Email - 1)];
			entity.Address = (reader.IsDBNull(((int)RMAConfigurationColumn.Address - 1)))?null:(System.String)reader[((int)RMAConfigurationColumn.Address - 1)];
			entity.ShippingDirections = (reader.IsDBNull(((int)RMAConfigurationColumn.ShippingDirections - 1)))?null:(System.String)reader[((int)RMAConfigurationColumn.ShippingDirections - 1)];
			entity.EnableEmailNotification = (reader.IsDBNull(((int)RMAConfigurationColumn.EnableEmailNotification - 1)))?null:(System.Boolean?)reader[((int)RMAConfigurationColumn.EnableEmailNotification - 1)];
			entity.GCExpirationPeriod = (reader.IsDBNull(((int)RMAConfigurationColumn.GCExpirationPeriod - 1)))?null:(System.Int32?)reader[((int)RMAConfigurationColumn.GCExpirationPeriod - 1)];
			entity.GCNotification = (reader.IsDBNull(((int)RMAConfigurationColumn.GCNotification - 1)))?null:(System.String)reader[((int)RMAConfigurationColumn.GCNotification - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.RMAConfiguration"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.RMAConfiguration"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.RMAConfiguration entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.RMAConfigID = (System.Int32)dataRow["RMAConfigID"];
			entity.MaxDays = Convert.IsDBNull(dataRow["MaxDays"]) ? null : (System.Int32?)dataRow["MaxDays"];
			entity.DisplayName = Convert.IsDBNull(dataRow["DisplayName"]) ? null : (System.String)dataRow["DisplayName"];
			entity.Email = Convert.IsDBNull(dataRow["Email"]) ? null : (System.String)dataRow["Email"];
			entity.Address = Convert.IsDBNull(dataRow["Address"]) ? null : (System.String)dataRow["Address"];
			entity.ShippingDirections = Convert.IsDBNull(dataRow["ShippingDirections"]) ? null : (System.String)dataRow["ShippingDirections"];
			entity.EnableEmailNotification = Convert.IsDBNull(dataRow["EnableEmailNotification"]) ? null : (System.Boolean?)dataRow["EnableEmailNotification"];
			entity.GCExpirationPeriod = Convert.IsDBNull(dataRow["GCExpirationPeriod"]) ? null : (System.Int32?)dataRow["GCExpirationPeriod"];
			entity.GCNotification = Convert.IsDBNull(dataRow["GCNotification"]) ? null : (System.String)dataRow["GCNotification"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.RMAConfiguration"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.RMAConfiguration Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RMAConfiguration entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.RMAConfiguration object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.RMAConfiguration instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.RMAConfiguration Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.RMAConfiguration entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region RMAConfigurationChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.RMAConfiguration</c>
	///</summary>
	public enum RMAConfigurationChildEntityTypes
	{
	}
	
	#endregion RMAConfigurationChildEntityTypes
	
	#region RMAConfigurationFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;RMAConfigurationColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RMAConfiguration"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RMAConfigurationFilterBuilder : SqlFilterBuilder<RMAConfigurationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RMAConfigurationFilterBuilder class.
		/// </summary>
		public RMAConfigurationFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RMAConfigurationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RMAConfigurationFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RMAConfigurationFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RMAConfigurationFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RMAConfigurationFilterBuilder
	
	#region RMAConfigurationParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;RMAConfigurationColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RMAConfiguration"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class RMAConfigurationParameterBuilder : ParameterizedSqlFilterBuilder<RMAConfigurationColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RMAConfigurationParameterBuilder class.
		/// </summary>
		public RMAConfigurationParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the RMAConfigurationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public RMAConfigurationParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the RMAConfigurationParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public RMAConfigurationParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion RMAConfigurationParameterBuilder
	
	#region RMAConfigurationSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;RMAConfigurationColumn&gt;"/> class
	/// that is used exclusively with a <see cref="RMAConfiguration"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class RMAConfigurationSortBuilder : SqlSortBuilder<RMAConfigurationColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the RMAConfigurationSqlSortBuilder class.
		/// </summary>
		public RMAConfigurationSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion RMAConfigurationSortBuilder
	
} // end namespace
