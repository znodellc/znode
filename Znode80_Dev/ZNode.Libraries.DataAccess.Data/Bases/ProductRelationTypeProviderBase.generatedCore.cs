﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="ProductRelationTypeProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class ProductRelationTypeProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.ProductRelationType, ZNode.Libraries.DataAccess.Entities.ProductRelationTypeKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductRelationTypeKey key)
		{
			return Delete(transactionManager, key.ProductRelationTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_productRelationTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.Int32 _productRelationTypeId)
		{
			return Delete(null, _productRelationTypeId);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productRelationTypeId">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.Int32 _productRelationTypeId);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.ProductRelationType Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductRelationTypeKey key, int start, int pageLength)
		{
			return GetByProductRelationTypeId(transactionManager, key.ProductRelationTypeId, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZnodeProductRelationType index.
		/// </summary>
		/// <param name="_productRelationTypeId"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductRelationType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductRelationType GetByProductRelationTypeId(System.Int32 _productRelationTypeId)
		{
			int count = -1;
			return GetByProductRelationTypeId(null,_productRelationTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZnodeProductRelationType index.
		/// </summary>
		/// <param name="_productRelationTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductRelationType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductRelationType GetByProductRelationTypeId(System.Int32 _productRelationTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByProductRelationTypeId(null, _productRelationTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZnodeProductRelationType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productRelationTypeId"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductRelationType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductRelationType GetByProductRelationTypeId(TransactionManager transactionManager, System.Int32 _productRelationTypeId)
		{
			int count = -1;
			return GetByProductRelationTypeId(transactionManager, _productRelationTypeId, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZnodeProductRelationType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productRelationTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductRelationType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductRelationType GetByProductRelationTypeId(TransactionManager transactionManager, System.Int32 _productRelationTypeId, int start, int pageLength)
		{
			int count = -1;
			return GetByProductRelationTypeId(transactionManager, _productRelationTypeId, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZnodeProductRelationType index.
		/// </summary>
		/// <param name="_productRelationTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductRelationType"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.ProductRelationType GetByProductRelationTypeId(System.Int32 _productRelationTypeId, int start, int pageLength, out int count)
		{
			return GetByProductRelationTypeId(null, _productRelationTypeId, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZnodeProductRelationType index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_productRelationTypeId"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.ProductRelationType"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.ProductRelationType GetByProductRelationTypeId(TransactionManager transactionManager, System.Int32 _productRelationTypeId, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;ProductRelationType&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;ProductRelationType&gt;"/></returns>
		public static TList<ProductRelationType> Fill(IDataReader reader, TList<ProductRelationType> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.ProductRelationType c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("ProductRelationType")
					.Append("|").Append((System.Int32)reader[((int)ProductRelationTypeColumn.ProductRelationTypeId - 1)]).ToString();
					c = EntityManager.LocateOrCreate<ProductRelationType>(
					key.ToString(), // EntityTrackingKey
					"ProductRelationType",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.ProductRelationType();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.ProductRelationTypeId = (System.Int32)reader[((int)ProductRelationTypeColumn.ProductRelationTypeId - 1)];
					c.ProductRelationCode = (System.String)reader[((int)ProductRelationTypeColumn.ProductRelationCode - 1)];
					c.ProductRelationDesc = (reader.IsDBNull(((int)ProductRelationTypeColumn.ProductRelationDesc - 1)))?null:(System.String)reader[((int)ProductRelationTypeColumn.ProductRelationDesc - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductRelationType"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductRelationType"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.ProductRelationType entity)
		{
			if (!reader.Read()) return;
			
			entity.ProductRelationTypeId = (System.Int32)reader[((int)ProductRelationTypeColumn.ProductRelationTypeId - 1)];
			entity.ProductRelationCode = (System.String)reader[((int)ProductRelationTypeColumn.ProductRelationCode - 1)];
			entity.ProductRelationDesc = (reader.IsDBNull(((int)ProductRelationTypeColumn.ProductRelationDesc - 1)))?null:(System.String)reader[((int)ProductRelationTypeColumn.ProductRelationDesc - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.ProductRelationType"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductRelationType"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.ProductRelationType entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.ProductRelationTypeId = (System.Int32)dataRow["ProductRelationTypeId"];
			entity.ProductRelationCode = (System.String)dataRow["ProductRelationCode"];
			entity.ProductRelationDesc = Convert.IsDBNull(dataRow["ProductRelationDesc"]) ? null : (System.String)dataRow["ProductRelationDesc"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.ProductRelationType"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductRelationType Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductRelationType entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByProductRelationTypeId methods when available
			
			#region ProductCrossSellCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<ProductCrossSell>|ProductCrossSellCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ProductCrossSellCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ProductCrossSellCollection = DataRepository.ProductCrossSellProvider.GetByRelationTypeId(transactionManager, entity.ProductRelationTypeId);

				if (deep && entity.ProductCrossSellCollection.Count > 0)
				{
					deepHandles.Add("ProductCrossSellCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<ProductCrossSell>) DataRepository.ProductCrossSellProvider.DeepLoad,
						new object[] { transactionManager, entity.ProductCrossSellCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.ProductRelationType object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.ProductRelationType instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.ProductRelationType Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.ProductRelationType entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<ProductCrossSell>
				if (CanDeepSave(entity.ProductCrossSellCollection, "List<ProductCrossSell>|ProductCrossSellCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(ProductCrossSell child in entity.ProductCrossSellCollection)
					{
						if(child.RelationTypeIdSource != null)
						{
							child.RelationTypeId = child.RelationTypeIdSource.ProductRelationTypeId;
						}
						else
						{
							child.RelationTypeId = entity.ProductRelationTypeId;
						}

					}

					if (entity.ProductCrossSellCollection.Count > 0 || entity.ProductCrossSellCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ProductCrossSellProvider.Save(transactionManager, entity.ProductCrossSellCollection);
						
						deepHandles.Add("ProductCrossSellCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< ProductCrossSell >) DataRepository.ProductCrossSellProvider.DeepSave,
							new object[] { transactionManager, entity.ProductCrossSellCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region ProductRelationTypeChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.ProductRelationType</c>
	///</summary>
	public enum ProductRelationTypeChildEntityTypes
	{
		///<summary>
		/// Collection of <c>ProductRelationType</c> as OneToMany for ProductCrossSellCollection
		///</summary>
		[ChildEntityType(typeof(TList<ProductCrossSell>))]
		ProductCrossSellCollection,
	}
	
	#endregion ProductRelationTypeChildEntityTypes
	
	#region ProductRelationTypeFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;ProductRelationTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductRelationType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductRelationTypeFilterBuilder : SqlFilterBuilder<ProductRelationTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductRelationTypeFilterBuilder class.
		/// </summary>
		public ProductRelationTypeFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductRelationTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductRelationTypeFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductRelationTypeFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductRelationTypeFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductRelationTypeFilterBuilder
	
	#region ProductRelationTypeParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;ProductRelationTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductRelationType"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class ProductRelationTypeParameterBuilder : ParameterizedSqlFilterBuilder<ProductRelationTypeColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductRelationTypeParameterBuilder class.
		/// </summary>
		public ProductRelationTypeParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the ProductRelationTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public ProductRelationTypeParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the ProductRelationTypeParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public ProductRelationTypeParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion ProductRelationTypeParameterBuilder
	
	#region ProductRelationTypeSortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;ProductRelationTypeColumn&gt;"/> class
	/// that is used exclusively with a <see cref="ProductRelationType"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class ProductRelationTypeSortBuilder : SqlSortBuilder<ProductRelationTypeColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the ProductRelationTypeSqlSortBuilder class.
		/// </summary>
		public ProductRelationTypeSortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion ProductRelationTypeSortBuilder
	
} // end namespace
