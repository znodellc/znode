﻿#region Using directives

using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;

using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;

#endregion

namespace ZNode.Libraries.DataAccess.Data.Bases
{	
	///<summary>
	/// This class is the base class for any <see cref="CountryProviderBase"/> implementation.
	/// It exposes CRUD methods as well as selecting on index, foreign keys and custom stored procedures.
	///</summary>
	public abstract partial class CountryProviderBaseCore : EntityProviderBase<ZNode.Libraries.DataAccess.Entities.Country, ZNode.Libraries.DataAccess.Entities.CountryKey>
	{		
		#region Get from Many To Many Relationship Functions
		#endregion	
		
		#region Delete Methods

		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to delete.</param>
		/// <returns>Returns true if operation suceeded.</returns>
		public override bool Delete(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CountryKey key)
		{
			return Delete(transactionManager, key.Code);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="_code">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public bool Delete(System.String _code)
		{
			return Delete(null, _code);
		}
		
		/// <summary>
		/// 	Deletes a row from the DataSource.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_code">. Primary Key.</param>
		/// <remarks>Deletes based on primary key(s).</remarks>
		/// <returns>Returns true if operation suceeded.</returns>
		public abstract bool Delete(TransactionManager transactionManager, System.String _code);		
		
		#endregion Delete Methods
		
		#region Get By Foreign Key Functions
		#endregion

		#region Get By Index Functions
		
		/// <summary>
		/// 	Gets a row from the DataSource based on its primary key.
		/// </summary>
		/// <param name="transactionManager">A <see cref="TransactionManager"/> object.</param>
		/// <param name="key">The unique identifier of the row to retrieve.</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <returns>Returns an instance of the Entity class.</returns>
		public override ZNode.Libraries.DataAccess.Entities.Country Get(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.CountryKey key, int start, int pageLength)
		{
			return GetByCode(transactionManager, key.Code, start, pageLength);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCountry_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Country&gt;"/> class.</returns>
		public TList<Country> GetByActiveInd(System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(null,_activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCountry_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Country&gt;"/> class.</returns>
		public TList<Country> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCountry_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Country&gt;"/> class.</returns>
		public TList<Country> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCountry_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Country&gt;"/> class.</returns>
		public TList<Country> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength)
		{
			int count = -1;
			return GetByActiveInd(transactionManager, _activeInd, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCountry_ActiveInd index.
		/// </summary>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Country&gt;"/> class.</returns>
		public TList<Country> GetByActiveInd(System.Boolean _activeInd, int start, int pageLength, out int count)
		{
			return GetByActiveInd(null, _activeInd, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCountry_ActiveInd index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_activeInd"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Country&gt;"/> class.</returns>
		public abstract TList<Country> GetByActiveInd(TransactionManager transactionManager, System.Boolean _activeInd, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key IX_ZNodeCountry_Name index.
		/// </summary>
		/// <param name="_name"></param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Country&gt;"/> class.</returns>
		public TList<Country> GetByName(System.String _name)
		{
			int count = -1;
			return GetByName(null,_name, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCountry_Name index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Country&gt;"/> class.</returns>
		public TList<Country> GetByName(System.String _name, int start, int pageLength)
		{
			int count = -1;
			return GetByName(null, _name, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCountry_Name index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Country&gt;"/> class.</returns>
		public TList<Country> GetByName(TransactionManager transactionManager, System.String _name)
		{
			int count = -1;
			return GetByName(transactionManager, _name, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCountry_Name index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Country&gt;"/> class.</returns>
		public TList<Country> GetByName(TransactionManager transactionManager, System.String _name, int start, int pageLength)
		{
			int count = -1;
			return GetByName(transactionManager, _name, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCountry_Name index.
		/// </summary>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="TList&lt;Country&gt;"/> class.</returns>
		public TList<Country> GetByName(System.String _name, int start, int pageLength, out int count)
		{
			return GetByName(null, _name, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the IX_ZNodeCountry_Name index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_name"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="TList&lt;Country&gt;"/> class.</returns>
		public abstract TList<Country> GetByName(TransactionManager transactionManager, System.String _name, int start, int pageLength, out int count);
						
		/// <summary>
		/// 	Gets rows from the datasource based on the primary key PK_ZNodeCountry index.
		/// </summary>
		/// <param name="_code"></param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Country"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Country GetByCode(System.String _code)
		{
			int count = -1;
			return GetByCode(null,_code, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCountry index.
		/// </summary>
		/// <param name="_code"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Country"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Country GetByCode(System.String _code, int start, int pageLength)
		{
			int count = -1;
			return GetByCode(null, _code, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCountry index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_code"></param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Country"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Country GetByCode(TransactionManager transactionManager, System.String _code)
		{
			int count = -1;
			return GetByCode(transactionManager, _code, 0, int.MaxValue, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCountry index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_code"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Country"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Country GetByCode(TransactionManager transactionManager, System.String _code, int start, int pageLength)
		{
			int count = -1;
			return GetByCode(transactionManager, _code, start, pageLength, out count);
		}
		
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCountry index.
		/// </summary>
		/// <param name="_code"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">out parameter to get total records for query</param>
		/// <remarks></remarks>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Country"/> class.</returns>
		public ZNode.Libraries.DataAccess.Entities.Country GetByCode(System.String _code, int start, int pageLength, out int count)
		{
			return GetByCode(null, _code, start, pageLength, out count);
		}
		
				
		/// <summary>
		/// 	Gets rows from the datasource based on the PK_ZNodeCountry index.
		/// </summary>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="_code"></param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">Number of rows to return.</param>
		/// <param name="count">The total number of records.</param>
		/// <returns>Returns an instance of the <see cref="ZNode.Libraries.DataAccess.Entities.Country"/> class.</returns>
		public abstract ZNode.Libraries.DataAccess.Entities.Country GetByCode(TransactionManager transactionManager, System.String _code, int start, int pageLength, out int count);
						
		#endregion "Get By Index Functions"
	
		#region Custom Methods
		
		
		#endregion

		#region Helper Functions	
		
		/// <summary>
		/// Fill a TList&lt;Country&gt; From a DataReader.
		/// </summary>
		/// <param name="reader">Datareader</param>
		/// <param name="rows">The collection to fill</param>
		/// <param name="start">Row number at which to start reading, the first row is 0.</param>
		/// <param name="pageLength">number of rows.</param>
		/// <returns>a <see cref="TList&lt;Country&gt;"/></returns>
		public static TList<Country> Fill(IDataReader reader, TList<Country> rows, int start, int pageLength)
		{
			NetTiersProvider currentProvider = DataRepository.Provider;
            bool useEntityFactory = currentProvider.UseEntityFactory;
            bool enableEntityTracking = currentProvider.EnableEntityTracking;
            LoadPolicy currentLoadPolicy = currentProvider.CurrentLoadPolicy;
			Type entityCreationFactoryType = currentProvider.EntityCreationalFactoryType;
			
			// advance to the starting row
			for (int i = 0; i < start; i++)
			{
				if (!reader.Read())
				return rows; // not enough rows, just return
			}
			for (int i = 0; i < pageLength; i++)
			{
				if (!reader.Read())
					break; // we are done
					
				string key = null;
				
				ZNode.Libraries.DataAccess.Entities.Country c = null;
				if (useEntityFactory)
				{
					key = new System.Text.StringBuilder("Country")
					.Append("|").Append((System.String)reader[((int)CountryColumn.Code - 1)]).ToString();
					c = EntityManager.LocateOrCreate<Country>(
					key.ToString(), // EntityTrackingKey
					"Country",  //Creational Type
					entityCreationFactoryType,  //Factory used to create entity
					enableEntityTracking); // Track this entity?
				}
				else
				{
					c = new ZNode.Libraries.DataAccess.Entities.Country();
				}
				
				if (!enableEntityTracking ||
					c.EntityState == EntityState.Added ||
					(enableEntityTracking &&
					
						(
							(currentLoadPolicy == LoadPolicy.PreserveChanges && c.EntityState == EntityState.Unchanged) ||
							(currentLoadPolicy == LoadPolicy.DiscardChanges && c.EntityState != EntityState.Unchanged)
						)
					))
				{
					c.SuppressEntityEvents = true;
					c.Code = (System.String)reader[((int)CountryColumn.Code - 1)];
					c.OriginalCode = c.Code;
					c.Name = (System.String)reader[((int)CountryColumn.Name - 1)];
					c.DisplayOrder = (System.Int32)reader[((int)CountryColumn.DisplayOrder - 1)];
					c.ActiveInd = (System.Boolean)reader[((int)CountryColumn.ActiveInd - 1)];
					c.EntityTrackingKey = key;
					c.AcceptChanges();
					c.SuppressEntityEvents = false;
				}
				rows.Add(c);
			}
		return rows;
		}		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Country"/> object from the <see cref="IDataReader"/>.
		/// </summary>
		/// <param name="reader">The <see cref="IDataReader"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Country"/> object to refresh.</param>
		public static void RefreshEntity(IDataReader reader, ZNode.Libraries.DataAccess.Entities.Country entity)
		{
			if (!reader.Read()) return;
			
			entity.Code = (System.String)reader[((int)CountryColumn.Code - 1)];
			entity.OriginalCode = (System.String)reader["Code"];
			entity.Name = (System.String)reader[((int)CountryColumn.Name - 1)];
			entity.DisplayOrder = (System.Int32)reader[((int)CountryColumn.DisplayOrder - 1)];
			entity.ActiveInd = (System.Boolean)reader[((int)CountryColumn.ActiveInd - 1)];
			entity.AcceptChanges();
		}
		
		/// <summary>
		/// Refreshes the <see cref="ZNode.Libraries.DataAccess.Entities.Country"/> object from the <see cref="DataSet"/>.
		/// </summary>
		/// <param name="dataSet">The <see cref="DataSet"/> to read from.</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Country"/> object.</param>
		public static void RefreshEntity(DataSet dataSet, ZNode.Libraries.DataAccess.Entities.Country entity)
		{
			DataRow dataRow = dataSet.Tables[0].Rows[0];
			
			entity.Code = (System.String)dataRow["Code"];
			entity.OriginalCode = (System.String)dataRow["Code"];
			entity.Name = (System.String)dataRow["Name"];
			entity.DisplayOrder = (System.Int32)dataRow["DisplayOrder"];
			entity.ActiveInd = (System.Boolean)dataRow["ActiveInd"];
			entity.AcceptChanges();
		}
		#endregion 
		
		#region DeepLoad Methods
		/// <summary>
		/// Deep Loads the <see cref="IEntity"/> object with criteria based of the child 
		/// property collections only N Levels Deep based on the <see cref="DeepLoadType"/>.
		/// </summary>
		/// <remarks>
		/// Use this method with caution as it is possible to DeepLoad with Recursion and traverse an entire object graph.
		/// </remarks>
		/// <param name="transactionManager"><see cref="TransactionManager"/> object</param>
		/// <param name="entity">The <see cref="ZNode.Libraries.DataAccess.Entities.Country"/> object to load.</param>
		/// <param name="deep">Boolean. A flag that indicates whether to recursively save all Property Collection that are descendants of this instance. If True, saves the complete object graph below this object. If False, saves this object only. </param>
		/// <param name="deepLoadType">DeepLoadType Enumeration to Include/Exclude object property collections from Load.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Country Property Collection Type Array To Include or Exclude from Load</param>
		/// <param name="innerList">A collection of child types for easy access.</param>
	    /// <exception cref="ArgumentNullException">entity or childTypes is null.</exception>
	    /// <exception cref="ArgumentException">deepLoadType has invalid value.</exception>
		public override void DeepLoad(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Country entity, bool deep, DeepLoadType deepLoadType, System.Type[] childTypes, DeepSession innerList)
		{
			if(entity == null)
				return;
			
			//used to hold DeepLoad method delegates and fire after all the local children have been loaded.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
			// Deep load child collections  - Call GetByCode methods when available
			
			#region ShippingCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<Shipping>|ShippingCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'ShippingCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.ShippingCollection = DataRepository.ShippingProvider.GetByDestinationCountryCode(transactionManager, entity.Code);

				if (deep && entity.ShippingCollection.Count > 0)
				{
					deepHandles.Add("ShippingCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<Shipping>) DataRepository.ShippingProvider.DeepLoad,
						new object[] { transactionManager, entity.ShippingCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region PortalCountryCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<PortalCountry>|PortalCountryCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'PortalCountryCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.PortalCountryCollection = DataRepository.PortalCountryProvider.GetByCountryCode(transactionManager, entity.Code);

				if (deep && entity.PortalCountryCollection.Count > 0)
				{
					deepHandles.Add("PortalCountryCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<PortalCountry>) DataRepository.PortalCountryProvider.DeepLoad,
						new object[] { transactionManager, entity.PortalCountryCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			#region TaxRuleCollection
			//Relationship Type One : Many
			if (CanDeepLoad(entity, "List<TaxRule>|TaxRuleCollection", deepLoadType, innerList)) 
			{
				#if NETTIERS_DEBUG
				System.Diagnostics.Debug.WriteLine("- property 'TaxRuleCollection' loaded. key " + entity.EntityTrackingKey);
				#endif 

				entity.TaxRuleCollection = DataRepository.TaxRuleProvider.GetByDestinationCountryCode(transactionManager, entity.Code);

				if (deep && entity.TaxRuleCollection.Count > 0)
				{
					deepHandles.Add("TaxRuleCollection",
						new KeyValuePair<Delegate, object>((DeepLoadHandle<TaxRule>) DataRepository.TaxRuleProvider.DeepLoad,
						new object[] { transactionManager, entity.TaxRuleCollection, deep, deepLoadType, childTypes, innerList }
					));
				}
			}		
			#endregion 
			
			
			//Fire all DeepLoad Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			deepHandles = null;
		}
		
		#endregion 
		
		#region DeepSave Methods

		/// <summary>
		/// Deep Save the entire object graph of the ZNode.Libraries.DataAccess.Entities.Country object with criteria based of the child 
		/// Type property array and DeepSaveType.
		/// </summary>
		/// <param name="transactionManager">The transaction manager.</param>
		/// <param name="entity">ZNode.Libraries.DataAccess.Entities.Country instance</param>
		/// <param name="deepSaveType">DeepSaveType Enumeration to Include/Exclude object property collections from Save.</param>
		/// <param name="childTypes">ZNode.Libraries.DataAccess.Entities.Country Property Collection Type Array To Include or Exclude from Save</param>
		/// <param name="innerList">A Hashtable of child types for easy access.</param>
		public override bool DeepSave(TransactionManager transactionManager, ZNode.Libraries.DataAccess.Entities.Country entity, DeepSaveType deepSaveType, System.Type[] childTypes, DeepSession innerList)
		{	
			if (entity == null)
				return false;
							
			#region Composite Parent Properties
			//Save Source Composite Properties, however, don't call deep save on them.  
			//So they only get saved a single level deep.
			#endregion Composite Parent Properties

			// Save Root Entity through Provider
			if (!entity.IsDeleted)
				this.Save(transactionManager, entity);
			
			//used to hold DeepSave method delegates and fire after all the local children have been saved.
			Dictionary<string, KeyValuePair<Delegate, object>> deepHandles = new Dictionary<string, KeyValuePair<Delegate, object>>();
	
			#region List<Shipping>
				if (CanDeepSave(entity.ShippingCollection, "List<Shipping>|ShippingCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(Shipping child in entity.ShippingCollection)
					{
						if(child.DestinationCountryCodeSource != null)
						{
							child.DestinationCountryCode = child.DestinationCountryCodeSource.Code;
						}
						else
						{
							child.DestinationCountryCode = entity.Code;
						}

					}

					if (entity.ShippingCollection.Count > 0 || entity.ShippingCollection.DeletedItems.Count > 0)
					{
						//DataRepository.ShippingProvider.Save(transactionManager, entity.ShippingCollection);
						
						deepHandles.Add("ShippingCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< Shipping >) DataRepository.ShippingProvider.DeepSave,
							new object[] { transactionManager, entity.ShippingCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<PortalCountry>
				if (CanDeepSave(entity.PortalCountryCollection, "List<PortalCountry>|PortalCountryCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(PortalCountry child in entity.PortalCountryCollection)
					{
						if(child.CountryCodeSource != null)
						{
							child.CountryCode = child.CountryCodeSource.Code;
						}
						else
						{
							child.CountryCode = entity.Code;
						}

					}

					if (entity.PortalCountryCollection.Count > 0 || entity.PortalCountryCollection.DeletedItems.Count > 0)
					{
						//DataRepository.PortalCountryProvider.Save(transactionManager, entity.PortalCountryCollection);
						
						deepHandles.Add("PortalCountryCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< PortalCountry >) DataRepository.PortalCountryProvider.DeepSave,
							new object[] { transactionManager, entity.PortalCountryCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
	
			#region List<TaxRule>
				if (CanDeepSave(entity.TaxRuleCollection, "List<TaxRule>|TaxRuleCollection", deepSaveType, innerList)) 
				{	
					// update each child parent id with the real parent id (mostly used on insert)
					foreach(TaxRule child in entity.TaxRuleCollection)
					{
						if(child.DestinationCountryCodeSource != null)
						{
							child.DestinationCountryCode = child.DestinationCountryCodeSource.Code;
						}
						else
						{
							child.DestinationCountryCode = entity.Code;
						}

					}

					if (entity.TaxRuleCollection.Count > 0 || entity.TaxRuleCollection.DeletedItems.Count > 0)
					{
						//DataRepository.TaxRuleProvider.Save(transactionManager, entity.TaxRuleCollection);
						
						deepHandles.Add("TaxRuleCollection",
						new KeyValuePair<Delegate, object>((DeepSaveHandle< TaxRule >) DataRepository.TaxRuleProvider.DeepSave,
							new object[] { transactionManager, entity.TaxRuleCollection, deepSaveType, childTypes, innerList }
						));
					}
				} 
			#endregion 
				
			//Fire all DeepSave Items
			foreach(KeyValuePair<Delegate, object> pair in deepHandles.Values)
		    {
                pair.Key.DynamicInvoke((object[])pair.Value);
		    }
			
			// Save Root Entity through Provider, if not already saved in delete mode
			if (entity.IsDeleted)
				this.Save(transactionManager, entity);
				

			deepHandles = null;
						
			return true;
		}
		#endregion
	} // end class
	
	#region CountryChildEntityTypes
	
	///<summary>
	/// Enumeration used to expose the different child entity types 
	/// for child properties in <c>ZNode.Libraries.DataAccess.Entities.Country</c>
	///</summary>
	public enum CountryChildEntityTypes
	{
		///<summary>
		/// Collection of <c>Country</c> as OneToMany for ShippingCollection
		///</summary>
		[ChildEntityType(typeof(TList<Shipping>))]
		ShippingCollection,
		///<summary>
		/// Collection of <c>Country</c> as OneToMany for PortalCountryCollection
		///</summary>
		[ChildEntityType(typeof(TList<PortalCountry>))]
		PortalCountryCollection,
		///<summary>
		/// Collection of <c>Country</c> as OneToMany for TaxRuleCollection
		///</summary>
		[ChildEntityType(typeof(TList<TaxRule>))]
		TaxRuleCollection,
	}
	
	#endregion CountryChildEntityTypes
	
	#region CountryFilterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="SqlFilterBuilder&lt;CountryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Country"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CountryFilterBuilder : SqlFilterBuilder<CountryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CountryFilterBuilder class.
		/// </summary>
		public CountryFilterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CountryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CountryFilterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CountryFilterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CountryFilterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CountryFilterBuilder
	
	#region CountryParameterBuilder
	
	/// <summary>
	/// A strongly-typed instance of the <see cref="ParameterizedSqlFilterBuilder&lt;CountryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Country"/> object.
	/// </summary>
	[CLSCompliant(true)]
	public class CountryParameterBuilder : ParameterizedSqlFilterBuilder<CountryColumn>
	{
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CountryParameterBuilder class.
		/// </summary>
		public CountryParameterBuilder() : base() { }

		/// <summary>
		/// Initializes a new instance of the CountryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		public CountryParameterBuilder(bool ignoreCase) : base(ignoreCase) { }

		/// <summary>
		/// Initializes a new instance of the CountryParameterBuilder class.
		/// </summary>
		/// <param name="ignoreCase">Specifies whether to create case-insensitive statements.</param>
		/// <param name="useAnd">Specifies whether to combine statements using AND or OR.</param>
		public CountryParameterBuilder(bool ignoreCase, bool useAnd) : base(ignoreCase, useAnd) { }

		#endregion Constructors
	}

	#endregion CountryParameterBuilder
	
	#region CountrySortBuilder
    
    /// <summary>
    /// A strongly-typed instance of the <see cref="SqlSortBuilder&lt;CountryColumn&gt;"/> class
	/// that is used exclusively with a <see cref="Country"/> object.
    /// </summary>
    [CLSCompliant(true)]
    public class CountrySortBuilder : SqlSortBuilder<CountryColumn>
    {
		#region Constructors

		/// <summary>
		/// Initializes a new instance of the CountrySqlSortBuilder class.
		/// </summary>
		public CountrySortBuilder() : base() { }

		#endregion Constructors

    }    
    #endregion CountrySortBuilder
	
} // end namespace
