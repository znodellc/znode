﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class PaymentGatewaysEndpointFixture : BaseFixture
	{
		[Test]
		public void get_payment_gateway()
		{
			var route = "paymentgateways/1";
			var response = GetModelFromEndpoint<PaymentGatewayResponse>(route);

			Assert.IsNotNull(response.PaymentGateway, "Payment gateway was null");
			Assert.IsTrue(response.PaymentGateway.GetType() == typeof(PaymentGatewayModel), "PaymentGateway type was not PaymentGatewayModel");
		}

		[Test]
		public void get_payment_gateways()
		{
			var route = "paymentgateways";
			var response = GetModelFromEndpoint<PaymentGatewayListResponse>(route);

			Assert.IsNotNull(response.PaymentGateways, "Payment gateways were null");
			Assert.IsTrue(response.PaymentGateways.GetType() == typeof(Collection<PaymentGatewayModel>), "PaymentGateways type was not Collection<PaymentGatewayModel>");
		}

		[Test]
		public void get_payment_gateways_filter_name_equals_authorize_net()
		{
			var route = "paymentgateways?filter=name~eq~authorize.net";
			var response = GetModelFromEndpoint<PaymentGatewayListResponse>(route);

			Assert.IsNotNull(response.PaymentGateways, "Payment gateways were null");
			Assert.IsTrue(response.PaymentGateways.GetType() == typeof(Collection<PaymentGatewayModel>), "PaymentGateways type was not Collection<PaymentGatewayModel>");

			foreach (var p in response.PaymentGateways)
			{
				Assert.AreEqual("authorize.net", p.Name.ToLower(), "Expected name 'authorize.net', actual name '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_payment_gateways_filter_name_not_equal_to_authorize_net()
		{
			var route = "paymentgateways?filter=name~ne~authorize.net";
			var response = GetModelFromEndpoint<PaymentGatewayListResponse>(route);

			Assert.IsNotNull(response.PaymentGateways, "Payment gateways were null");
			Assert.IsTrue(response.PaymentGateways.GetType() == typeof(Collection<PaymentGatewayModel>), "PaymentGateways type was not Collection<PaymentGatewayModel>");

			foreach (var p in response.PaymentGateways)
			{
				Assert.AreNotEqual("authorize.net", p.Name.ToLower(), "Expected name not equal to 'authorize.net'");
			}
		}

		[Test]
		public void get_payment_gateways_filter_name_contains_direct()
		{
			var route = "paymentgateways?filter=name~cn~direct";
			var response = GetModelFromEndpoint<PaymentGatewayListResponse>(route);

			Assert.IsNotNull(response.PaymentGateways, "Payment gateways were null");
			Assert.IsTrue(response.PaymentGateways.GetType() == typeof(Collection<PaymentGatewayModel>), "PaymentGateways type was not Collection<PaymentGatewayModel>");

			foreach (var p in response.PaymentGateways)
			{
				Assert.IsTrue(p.Name.ToLower().Contains("direct"), "Expected name to contain 'direct', actual name '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_payment_gateways_filter_name_starts_with_pay()
		{
			var route = "paymentgateways?filter=name~sw~pay";
			var response = GetModelFromEndpoint<PaymentGatewayListResponse>(route);

			Assert.IsNotNull(response.PaymentGateways, "Payment gateways were null");
			Assert.IsTrue(response.PaymentGateways.GetType() == typeof(Collection<PaymentGatewayModel>), "PaymentGateways type was not Collection<PaymentGatewayModel>");

			foreach (var p in response.PaymentGateways)
			{
				Assert.IsTrue(p.Name.ToLower().StartsWith("pay"), "Expected name to start with 'pay', actual name started with '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_payment_gateways_filter_name_ends_with_pro()
		{
			var route = "paymentgateways?filter=name~ew~pro";
			var response = GetModelFromEndpoint<PaymentGatewayListResponse>(route);

			Assert.IsNotNull(response.PaymentGateways, "Payment gateways were null");
			Assert.IsTrue(response.PaymentGateways.GetType() == typeof(Collection<PaymentGatewayModel>), "PaymentGateways type was not Collection<PaymentGatewayModel>");

			foreach (var p in response.PaymentGateways)
			{
				Assert.IsTrue(p.Name.ToLower().EndsWith("pro"), "Expected name to end with 'pro', actual name ended with '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_payment_gateways_page_index_0_size_5()
		{
			var route = "paymentgateways?page=index~0,size~5";
			var response = GetModelFromEndpoint<PaymentGatewayListResponse>(route);

			Assert.IsNotNull(response.PaymentGateways, "Payment gateways were null");
			Assert.IsTrue(response.PaymentGateways.GetType() == typeof(Collection<PaymentGatewayModel>), "PaymentGateways type was not Collection<PaymentGatewayModel>");
			Assert.IsTrue(response.PaymentGateways.Count == 5, "Expected count equal to 5, actual count " + response.PaymentGateways.Count);
		}

		[Test]
		public void create_update_delete_payment_type()
		{
			// Create
			var route = "paymentgateways";
			var data = GetDataFromFile("PaymentGateway-Create.json");
			var response = PostModelToEndpoint<PaymentGatewayResponse>(route, data);

			Assert.IsNotNull(response.PaymentGateway, "Payment gateway was null");
			Assert.IsTrue(response.PaymentGateway.PaymentGatewayId > 0, "Payment gateway ID was not greater than 0");
			Assert.IsTrue(response.PaymentGateway.Name == "NewPaymentGateway", "Expected name 'NewPaymentGateway', actual name '" + response.PaymentGateway.Name + "'");

			// Update
			route = "paymentgateways/" + response.PaymentGateway.PaymentGatewayId;
			data = GetDataFromFile("PaymentGateway-Update.json");
			response = PutModelToEndpoint<PaymentGatewayResponse>(route, data);

			Assert.IsNotNull(response.PaymentGateway, "Payment gateway was null");
			Assert.IsTrue(response.PaymentGateway.Name == "UpdatedPaymentGateway", "Expected name 'UpdatedPaymentGateway', actual name '" + response.PaymentGateway.Name + "'");

			// Delete
			route = "paymentgateways/" + response.PaymentGateway.PaymentGatewayId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Payment gateway was not deleted");
		}
	}
}
