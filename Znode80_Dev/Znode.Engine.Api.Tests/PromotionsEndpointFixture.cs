﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class PromotionsEndpointFixture : BaseFixture
	{
		[Test]
		public void get_promotion()
		{
			var route = "promotions/1";
			var response = GetModelFromEndpoint<PromotionResponse>(route);

			Assert.IsNotNull(response.Promotion, "Promotion was null");
			Assert.IsTrue(response.Promotion.GetType() == typeof(PromotionModel), "Promotion type was not PromotionModel");
		}

		[Test]
		public void get_promotion_expand_promotion_type()
		{
			var route = "promotions/1?expand=promotiontype";
			var response = GetModelFromEndpoint<PromotionResponse>(route);

			Assert.IsNotNull(response.Promotion.PromotionType, "Promotion type was null");
			Assert.IsTrue(response.Promotion.PromotionType.GetType() == typeof(PromotionTypeModel), "Promotion type was not PromotionTypeModel");
		}

		[Test]
		public void get_promotions()
		{
			var route = "promotions";
			var response = GetModelFromEndpoint<PromotionListResponse>(route);

			Assert.IsNotNull(response.Promotions, "Promotions were null");
			Assert.IsTrue(response.Promotions.GetType() == typeof(Collection<PromotionModel>), "Promotions type was not Collection<PromotionModel>");
		}

		[Test]
		public void get_promotions_filter_portalid_equals_null()
		{
			var route = "promotions?filter=portalid~eq~null";
			var response = GetModelFromEndpoint<PromotionListResponse>(route);

			Assert.IsNotNull(response.Promotions, "Promotions were null");
			Assert.IsTrue(response.Promotions.GetType() == typeof(Collection<PromotionModel>), "Promotions type was not Collection<PromotionModel>");
		}

		[Test]
		public void get_promotions_filter_portalid_not_equal_to_null()
		{
			var route = "promotions?filter=portalid~ne~null";
			var response = GetModelFromEndpoint<PromotionListResponse>(route);

			Assert.IsNotNull(response.Promotions, "Promotions were null");
			Assert.IsTrue(response.Promotions.GetType() == typeof(Collection<PromotionModel>), "Promotions type was not Collection<PromotionModel>");
		}

		[Test]
		public void get_promotions_filter_discount_equals_10_00()
		{
			var route = "promotions?filter=discount~eq~10.00";
			var response = GetModelFromEndpoint<PromotionListResponse>(route);

			Assert.IsNotNull(response.Promotions, "Promotions were null");
			Assert.IsTrue(response.Promotions.GetType() == typeof(Collection<PromotionModel>), "Promotions type was not Collection<PromotionModel>");

			foreach (var p in response.Promotions)
			{
				Assert.AreEqual(10.00m, p.Discount, "Expected discount 10.00, actual discount " + p.Discount);
			}
		}

		[Test]
		public void get_promotions_filter_discount_not_equal_to_10_00()
		{
			var route = "promotions?filter=discount~ne~10.00";
			var response = GetModelFromEndpoint<PromotionListResponse>(route);

			Assert.IsNotNull(response.Promotions, "Promotions were null");
			Assert.IsTrue(response.Promotions.GetType() == typeof(Collection<PromotionModel>), "Promotions type was not Collection<PromotionModel>");

			foreach (var p in response.Promotions)
			{
				Assert.AreNotEqual(10.00m, p.Discount, "Expected discount not equal to 10.00");
			}
		}

		[Test]
		public void get_promotions_filter_name_contains_test()
		{
			var route = "promotions?filter=name~cn~test";
			var response = GetModelFromEndpoint<PromotionListResponse>(route);

			Assert.IsNotNull(response.Promotions, "Promotions were null");
			Assert.IsTrue(response.Promotions.GetType() == typeof(Collection<PromotionModel>), "Promotions type was not Collection<PromotionModel>");

			foreach (var p in response.Promotions)
			{
				Assert.IsTrue(p.Name.ToLower().Contains("test"), "Expected name to contain 'test', actual name '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_promotions_filter_discount_greater_than_10_00()
		{
			var route = "promotions?filter=discount~gt~10.00";
			var response = GetModelFromEndpoint<PromotionListResponse>(route);

			Assert.IsNotNull(response.Promotions, "Promotions were null");
			Assert.IsTrue(response.Promotions.GetType() == typeof(Collection<PromotionModel>), "Promotions type was not Collection<PromotionModel>");

			foreach (var p in response.Promotions)
			{
				Assert.IsTrue(p.Discount > 10.00m, "Expected discount greater than 10.00, actual discount " + p.Discount);
			}
		}

		[Test]
		public void get_promotions_filter_discount_greater_than_or_equal_to_10_00()
		{
			var route = "promotions?filter=discount~ge~10.00";
			var response = GetModelFromEndpoint<PromotionListResponse>(route);

			Assert.IsNotNull(response.Promotions, "Promotions were null");
			Assert.IsTrue(response.Promotions.GetType() == typeof(Collection<PromotionModel>), "Promotions type was not Collection<PromotionModel>");

			foreach (var p in response.Promotions)
			{
				Assert.IsTrue(p.Discount >= 10.00m, "Expected discount greater than or equal to 10.00, actual discount " + p.Discount);
			}
		}

		[Test]
		public void get_promotions_filter_discount_less_than_10_00()
		{
			var route = "promotions?filter=discount~lt~10.00";
			var response = GetModelFromEndpoint<PromotionListResponse>(route);

			Assert.IsNotNull(response.Promotions, "Promotions were null");
			Assert.IsTrue(response.Promotions.GetType() == typeof(Collection<PromotionModel>), "Promotions type was not Collection<PromotionModel>");

			foreach (var p in response.Promotions)
			{
				Assert.IsTrue(p.Discount < 10.00m, "Expected discount less than 10.00, actual discount " + p.Discount);
			}
		}

		[Test]
		public void get_promotions_filter_discount_less_than_or_equal_to_10_00()
		{
			var route = "promotions?filter=discount~le~10.00";
			var response = GetModelFromEndpoint<PromotionListResponse>(route);

			Assert.IsNotNull(response.Promotions, "Promotions were null");
			Assert.IsTrue(response.Promotions.GetType() == typeof(Collection<PromotionModel>), "Promotions type was not Collection<PromotionModel>");

			foreach (var p in response.Promotions)
			{
				Assert.IsTrue(p.Discount <= 10.00m, "Expected discount less than or equal to 10.00, actual discount " + p.Discount);
			}
		}

		[Test]
		public void get_promotions_filter_name_starts_with_test()
		{
			var route = "promotions?filter=name~sw~test";
			var response = GetModelFromEndpoint<PromotionListResponse>(route);

			Assert.IsNotNull(response.Promotions, "Promotions were null");
			Assert.IsTrue(response.Promotions.GetType() == typeof(Collection<PromotionModel>), "Promotions type was not Collection<PromotionModel>");

			foreach (var p in response.Promotions)
			{
				Assert.IsTrue(p.Name.ToLower().StartsWith("test"), "Expected name to start with 'test', actual name started with '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_promotions_filter_name_ends_with_order()
		{
			var route = "promotions?filter=name~ew~order";
			var response = GetModelFromEndpoint<PromotionListResponse>(route);

			Assert.IsNotNull(response.Promotions, "Promotions were null");
			Assert.IsTrue(response.Promotions.GetType() == typeof(Collection<PromotionModel>), "Promotions type was not Collection<PromotionModel>");

			foreach (var p in response.Promotions)
			{
				Assert.IsTrue(p.Name.ToLower().EndsWith("order"), "Expected name to end with 'order', actual name ended with '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_promotions_page_index_0_size_5()
		{
			var route = "promotions?page=index~0,size~5";
			var response = GetModelFromEndpoint<PromotionListResponse>(route);

			Assert.IsNotNull(response.Promotions, "Promotions were null");
			Assert.IsTrue(response.Promotions.GetType() == typeof(Collection<PromotionModel>), "Promotions type was not Collection<PromotionModel>");
			Assert.IsTrue(response.Promotions.Count == 5, "Expected count equal to 5, actual count " + response.Promotions.Count);
		}

		[Test]
		public void create_update_delete_promotion()
		{
			// Create
			var route = "promotions";
			var data = GetDataFromFile("Promotion-Create.json");
			var response = PostModelToEndpoint<PromotionResponse>(route, data);

			Assert.IsNotNull(response.Promotion, "Promotion was null");
			Assert.IsTrue(response.Promotion.PromotionId > 0, "Promotion ID was not greater than 0");
			Assert.IsTrue(response.Promotion.Name == "NewPromotion", "Expected name 'NewPromotion', actual name '" + response.Promotion.Name + "'");

			// Update
			route = "promotions/" + response.Promotion.PromotionId;
			data = GetDataFromFile("Promotion-Update.json");
			response = PutModelToEndpoint<PromotionResponse>(route, data);

			Assert.IsNotNull(response.Promotion, "Promotion was null");
			Assert.IsTrue(response.Promotion.Name == "UpdatedPromotion", "Expected name 'UpdatedPromotion', actual name '" + response.Promotion.Name + "'");

			// Delete
			route = "promotions/" + response.Promotion.PromotionId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Promotion was not deleted");
		}
	}
}
