﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class StatesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_state()
		{
			var route = "states/OH";
			var response = GetModelFromEndpoint<StateResponse>(route);

			Assert.IsNotNull(response.State, "State was null");
			Assert.IsTrue(response.State.GetType() == typeof(StateModel), "State type was not StateModel");
		}

		[Test]
		public void get_states()
		{
			var route = "states";
			var response = GetModelFromEndpoint<StateListResponse>(route);

			Assert.IsNotNull(response.States, "States were null");
			Assert.IsTrue(response.States.GetType() == typeof(Collection<StateModel>), "States type was not Collection<StateModel>");
		}

		[Test]
		public void get_states_filter_code_equals_oh()
		{
			var route = "states?filter=code~eq~oh";
			var response = GetModelFromEndpoint<StateListResponse>(route);

			Assert.IsNotNull(response.States, "States were null");
			Assert.IsTrue(response.States.GetType() == typeof(Collection<StateModel>), "States type was not Collection<StateModel>");

			foreach (var s in response.States)
			{
				Assert.AreEqual("oh", s.Code.ToLower(), "Expected code 'oh', actual code '" + s.Code.ToLower() + "'");
			}
		}

		[Test]
		public void get_states_filter_code_not_equal_to_oh()
		{
			var route = "states?filter=code~ne~oh";
			var response = GetModelFromEndpoint<StateListResponse>(route);

			Assert.IsNotNull(response.States, "States were null");
			Assert.IsTrue(response.States.GetType() == typeof(Collection<StateModel>), "States type was not Collection<StateModel>");

			foreach (var s in response.States)
			{
				Assert.AreNotEqual("oh", s.Code.ToLower(), "Expected code not equal to 'oh'");
			}
		}

		[Test]
		public void get_states_filter_name_contains_south()
		{
			var route = "states?filter=name~cn~south";
			var response = GetModelFromEndpoint<StateListResponse>(route);

			Assert.IsNotNull(response.States, "States were null");
			Assert.IsTrue(response.States.GetType() == typeof(Collection<StateModel>), "States type was not Collection<StateModel>");

			foreach (var s in response.States)
			{
				Assert.IsTrue(s.Name.ToLower().Contains("south"), "Expected name to contain 'south', actual name '" + s.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_states_filter_name_starts_with_south()
		{
			var route = "states?filter=name~sw~south";
			var response = GetModelFromEndpoint<StateListResponse>(route);

			Assert.IsNotNull(response.States, "States were null");
			Assert.IsTrue(response.States.GetType() == typeof(Collection<StateModel>), "States type was not Collection<StateModel>");

			foreach (var s in response.States)
			{
				Assert.IsTrue(s.Name.ToLower().StartsWith("south"), "Expected name to start with 'south', actual name '" + s.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_states_filter_name_ends_with_dakota()
		{
			var route = "states?filter=name~ew~dakota";
			var response = GetModelFromEndpoint<StateListResponse>(route);

			Assert.IsNotNull(response.States, "States were null");
			Assert.IsTrue(response.States.GetType() == typeof(Collection<StateModel>), "States type was not Collection<StateModel>");

			foreach (var s in response.States)
			{
				Assert.IsTrue(s.Name.ToLower().EndsWith("dakota"), "Expected name to end with 'dakota', actual name '" + s.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_states_page_index_0_size_10()
		{
			var route = "states?page=index~0,size~10";
			var response = GetModelFromEndpoint<StateListResponse>(route);

			Assert.IsNotNull(response.States, "States were null");
			Assert.IsTrue(response.States.GetType() == typeof(Collection<StateModel>), "States type was not Collection<StateModel>");
			Assert.IsTrue(response.States.Count == 10, "Expected count equal to 10, actual count " + response.States.Count);
		}

		[Test]
		public void create_update_delete_state()
		{
			// Create
			var route = "states";
			var data = GetDataFromFile("State-Create.json");
			var response = PostModelToEndpoint<StateResponse>(route, data);

			Assert.IsNotNull(response.State, "State was null");
			Assert.IsNotNullOrEmpty(response.State.Code, "State code was not empty");
			Assert.IsTrue(response.State.Name == "NewState", "Expected name 'NewState', actual name '" + response.State.Name + "'");

			// Update
			route = "states/" + response.State.Code;
			data = GetDataFromFile("State-Update.json");
			response = PutModelToEndpoint<StateResponse>(route, data);

			Assert.IsNotNull(response.State, "State was null");
			Assert.IsTrue(response.State.Name == "UpdatedState", "Expected name 'UpdatedState', actual name '" + response.State.Name + "'");

			// Delete
			route = "states/" + response.State.Code;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "State was not deleted");
		}
	}
}
