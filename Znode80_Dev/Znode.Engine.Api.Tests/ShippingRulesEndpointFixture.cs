﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class ShippingRulesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_shipping_rule()
		{
			var route = "shippingrules/1";
			var response = GetModelFromEndpoint<ShippingRuleResponse>(route);

			Assert.IsNotNull(response.ShippingRule, "Shipping rule was null");
			Assert.IsTrue(response.ShippingRule.GetType() == typeof(ShippingRuleModel), "Shipping rule type was not ShippingRuleModel");
		}

		[Test]
		public void get_shipping_rules()
		{
			var route = "shippingrules";
			var response = GetModelFromEndpoint<ShippingRuleListResponse>(route);

			Assert.IsNotNull(response.ShippingRules, "Shipping rules were null");
			Assert.IsTrue(response.ShippingRules.GetType() == typeof(Collection<ShippingRuleModel>), "ShippingRules type was not Collection<ShippingRuleModel>");
		}

		[Test]
		public void get_shipping_rules_filter_shipping_option_id_equals_9()
		{
			var route = "shippingrules?filter=shippingoptionid~eq~9";
			var response = GetModelFromEndpoint<ShippingRuleListResponse>(route);

			Assert.IsNotNull(response.ShippingRules, "Shipping rules were null");
			Assert.IsTrue(response.ShippingRules.GetType() == typeof(Collection<ShippingRuleModel>), "ShippingRules type was not Collection<ShippingRuleModel>");

			foreach (var s in response.ShippingRules)
			{
				Assert.AreEqual(9, s.ShippingOptionId, "Expected shipping option id 9, actual shipping option id " + s.ShippingOptionId);
			}
		}

		[Test]
		public void get_shipping_rules_filter_shipping_option_id_not_equal_to_9()
		{
			var route = "shippingrules?filter=shippingoptionid~ne~9";
			var response = GetModelFromEndpoint<ShippingRuleListResponse>(route);

			Assert.IsNotNull(response.ShippingRules, "Shipping rules were null");
			Assert.IsTrue(response.ShippingRules.GetType() == typeof(Collection<ShippingRuleModel>), "ShippingRules type was not Collection<ShippingRuleModel>");

			foreach (var s in response.ShippingRules)
			{
				Assert.AreNotEqual(9, s.ShippingOptionId, "Expected shipping option id not equal to 9");
			}
		}

		[Test]
		public void get_shipping_rules_page_index_0_size_1()
		{
			var route = "shippingrules?page=index~0,size~1";
			var response = GetModelFromEndpoint<ShippingRuleListResponse>(route);

			Assert.IsNotNull(response.ShippingRules, "Shipping rules were null");
			Assert.IsTrue(response.ShippingRules.GetType() == typeof(Collection<ShippingRuleModel>), "ShippingRules type was not Collection<ShippingRuleModel>");
			Assert.IsTrue(response.ShippingRules.Count == 1, "Expected count equal to 1, actual count " + response.ShippingRules.Count);
		}

		[Test]
		public void create_update_delete_shipping_rule()
		{
			// Create
			var route = "shippingrules";
			var data = GetDataFromFile("ShippingRule-Create.json");
			var response = PostModelToEndpoint<ShippingRuleResponse>(route, data);

			Assert.IsNotNull(response.ShippingRule, "Shipping rule was null");
			Assert.IsTrue(response.ShippingRule.ShippingRuleId > 0, "Shipping type ID was not greater than 0");
			Assert.IsTrue(response.ShippingRule.BaseCost == 5.00m, "Expected base cost 5.00, actual base cost " + response.ShippingRule.BaseCost);

			// Update
			route = "shippingrules/" + response.ShippingRule.ShippingRuleId;
			data = GetDataFromFile("ShippingRule-Update.json");
			response = PutModelToEndpoint<ShippingRuleResponse>(route, data);

			Assert.IsNotNull(response.ShippingRule, "Shipping rule was null");
			Assert.IsTrue(response.ShippingRule.BaseCost == 35.00m, "Expected base cost 35.00, actual base cost " + response.ShippingRule.BaseCost);

			// Delete
			route = "shippingrules/" + response.ShippingRule.ShippingRuleId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Shipping rule was not deleted");
		}
	}
}
