﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class AddressesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_address()
		{
			var route = "addresses/2";
			var response = GetModelFromEndpoint<AddressResponse>(route);

			Assert.IsNotNull(response.Address, "Address was null");
			Assert.IsTrue(response.Address.GetType() == typeof(AddressModel), "Address type was not AddressModel");
		}

		[Test]
		public void get_addresses()
		{
			var route = "addresses";
			var response = GetModelFromEndpoint<AddressListResponse>(route);

			Assert.IsNotNull(response.Addresses, "Addresses were null");
			Assert.IsTrue(response.Addresses.GetType() == typeof(Collection<AddressModel>), "Addresses type was not Collection<AddressModel>");
		}

		[Test]
		public void get_addresses_filter_statecode_equals_sd()
		{
			var route = "addresses?filter=statecode~eq~sd";
			var response = GetModelFromEndpoint<AddressListResponse>(route);

			Assert.IsNotNull(response.Addresses, "Addresses were null");
			Assert.IsTrue(response.Addresses.GetType() == typeof(Collection<AddressModel>), "Addresses type was not Collection<AddressModel>");

			foreach (var a in response.Addresses)
			{
				Assert.AreEqual("sd", a.StateCode.ToLower(), "Expected state code 'sd', actual state code " + a.StateCode.ToLower());
			}
		}

		[Test]
		public void get_addresses_filter_city_contains_fake()
		{
			var route = "addresses?filter=city~cn~fake";
			var response = GetModelFromEndpoint<AddressListResponse>(route);

			Assert.IsNotNull(response.Addresses, "Addresses were null");
			Assert.IsTrue(response.Addresses.GetType() == typeof(Collection<AddressModel>), "Addresses type was not Collection<AddressModel>");

			foreach (var a in response.Addresses)
			{
				Assert.IsTrue(a.City.ToLower().Contains("fake"), "Expected city to contain 'fake', actual city " + a.StateCode.ToLower());
			}
		}

		[Test]
		public void get_addresses_filter_city_starts_with_fake()
		{
			var route = "addresses?filter=city~sw~fake";
			var response = GetModelFromEndpoint<AddressListResponse>(route);

			Assert.IsNotNull(response.Addresses, "Addresses were null");
			Assert.IsTrue(response.Addresses.GetType() == typeof(Collection<AddressModel>), "Addresses type was not Collection<AddressModel>");

			foreach (var a in response.Addresses)
			{
				Assert.IsTrue(a.City.ToLower().StartsWith("fake"), "Expected city to start with 'fake', actual city " + a.StateCode.ToLower());
			}
		}

		[Test]
		public void get_addresses_filter_city_ends_with_city()
		{
			var route = "addresses?filter=city~ew~city";
			var response = GetModelFromEndpoint<AddressListResponse>(route);

			Assert.IsNotNull(response.Addresses, "Addresses were null");
			Assert.IsTrue(response.Addresses.GetType() == typeof(Collection<AddressModel>), "Addresses type was not Collection<AddressModel>");

			foreach (var a in response.Addresses)
			{
				Assert.IsTrue(a.City.ToLower().EndsWith("city"), "Expected city to end with 'city', actual city " + a.StateCode.ToLower());
			}
		}
	}
}
