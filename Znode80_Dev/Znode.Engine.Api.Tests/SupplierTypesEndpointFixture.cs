﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class SupplierTypesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_supplier_type()
		{
			var route = "suppliertypes/1";
			var response = GetModelFromEndpoint<SupplierTypeResponse>(route);

			Assert.IsNotNull(response.SupplierType, "Supplier type was null");
			Assert.IsTrue(response.SupplierType.GetType() == typeof(SupplierTypeModel), "Supplier type was not SupplierTypeModel");
		}

		[Test]
		public void get_supplier_types()
		{
			var route = "suppliertypes";
			var response = GetModelFromEndpoint<SupplierTypeListResponse>(route);

			Assert.IsNotNull(response.SupplierTypes, "Supplier types were null");
			Assert.IsTrue(response.SupplierTypes.GetType() == typeof(Collection<SupplierTypeModel>), "SupplierTypes type was not Collection<SupplierTypeModel>");
		}

		[Test]
		public void get_supplier_types_filter_name_equals_email()
		{
			var route = "suppliertypes?filter=name~eq~email";
			var response = GetModelFromEndpoint<SupplierTypeListResponse>(route);

			Assert.IsNotNull(response.SupplierTypes, "Supplier types were null");
			Assert.IsTrue(response.SupplierTypes.GetType() == typeof(Collection<SupplierTypeModel>), "SupplierTypes type was not Collection<SupplierTypeModel>");

			foreach (var s in response.SupplierTypes)
			{
				Assert.AreEqual("email", s.Name.ToLower(), "Expected name 'email', actual name '" + s.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_supplier_types_filter_name_not_equal_to_email()
		{
			var route = "suppliertypes?filter=name~ne~email";
			var response = GetModelFromEndpoint<SupplierTypeListResponse>(route);

			Assert.IsNotNull(response.SupplierTypes, "Supplier types were null");
			Assert.IsTrue(response.SupplierTypes.GetType() == typeof(Collection<SupplierTypeModel>), "SupplierTypes type was not Collection<SupplierTypeModel>");

			foreach (var s in response.SupplierTypes)
			{
				Assert.AreNotEqual("email", s.Name.ToLower(), "Expected name not equal to 'email'");
			}
		}

		[Test]
		public void get_supplier_types_filter_class_name_contains_znode()
		{
			var route = "suppliertypes?filter=classname~cn~znode";
			var response = GetModelFromEndpoint<SupplierTypeListResponse>(route);

			Assert.IsNotNull(response.SupplierTypes, "Supplier types were null");
			Assert.IsTrue(response.SupplierTypes.GetType() == typeof(Collection<SupplierTypeModel>), "SupplierTypes type was not Collection<SupplierTypeModel>");

			foreach (var s in response.SupplierTypes)
			{
				Assert.IsTrue(s.ClassName.ToLower().Contains("znode"), "Expected class name to contain 'znode', actual class name '" + s.ClassName.ToLower() + "'");
			}
		}

		[Test]
		public void get_supplier_types_filter_class_name_starts_with_znode()
		{
			var route = "suppliertypes?filter=classname~sw~znode";
			var response = GetModelFromEndpoint<SupplierTypeListResponse>(route);

			Assert.IsNotNull(response.SupplierTypes, "Supplier types were null");
			Assert.IsTrue(response.SupplierTypes.GetType() == typeof(Collection<SupplierTypeModel>), "SupplierTypes type was not Collection<SupplierTypeModel>");

			foreach (var s in response.SupplierTypes)
			{
				Assert.IsTrue(s.ClassName.ToLower().StartsWith("znode"), "Expected class name to start with 'znode', actual class name started with '" + s.ClassName.ToLower() + "'");
			}
		}

		[Test]
		public void get_supplier_types_filter_class_name_ends_with_service()
		{
			var route = "suppliertypes?filter=classname~ew~service";
			var response = GetModelFromEndpoint<SupplierTypeListResponse>(route);

			Assert.IsNotNull(response.SupplierTypes, "Supplier types were null");
			Assert.IsTrue(response.SupplierTypes.GetType() == typeof(Collection<SupplierTypeModel>), "SupplierTypes type was not Collection<SupplierTypeModel>");

			foreach (var s in response.SupplierTypes)
			{
				Assert.IsTrue(s.ClassName.ToLower().EndsWith("service"), "Expected class name to end with 'service', actual class name ended with '" + s.ClassName.ToLower() + "'");
			}
		}

		[Test]
		public void get_supplier_types_page_index_0_size_2()
		{
			var route = "suppliertypes?page=index~0,size~2";
			var response = GetModelFromEndpoint<SupplierTypeListResponse>(route);

			Assert.IsNotNull(response.SupplierTypes, "Supplier types were null");
			Assert.IsTrue(response.SupplierTypes.GetType() == typeof(Collection<SupplierTypeModel>), "SupplierTypes type was not Collection<SupplierTypeModel>");
			Assert.IsTrue(response.SupplierTypes.Count == 2, "Expected count equal to 2, actual count " + response.SupplierTypes.Count);
		}

		[Test]
		public void create_update_delete_supplier_type()
		{
			// Create
			var route = "suppliertypes";
			var data = GetDataFromFile("SupplierType-Create.json");
			var response = PostModelToEndpoint<SupplierTypeResponse>(route, data);

			Assert.IsNotNull(response.SupplierType, "Supplier type was null");
			Assert.IsTrue(response.SupplierType.SupplierTypeId > 0, "Supplier type ID was not greater than 0");
			Assert.IsTrue(response.SupplierType.Name == "NewSupplierType", "Expected name 'NewSupplierType', actual name '" + response.SupplierType.Name + "'");

			// Update
			route = "suppliertypes/" + response.SupplierType.SupplierTypeId;
			data = GetDataFromFile("SupplierType-Update.json");
			response = PutModelToEndpoint<SupplierTypeResponse>(route, data);

			Assert.IsNotNull(response.SupplierType, "Supplier type was null");
			Assert.IsTrue(response.SupplierType.Name == "UpdatedSupplierType", "Expected name 'UpdatedSupplierType', actual name '" + response.SupplierType.Name + "'");

			// Delete
			route = "suppliertypes/" + response.SupplierType.SupplierTypeId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Supplier type was not deleted");
		}
	}
}
