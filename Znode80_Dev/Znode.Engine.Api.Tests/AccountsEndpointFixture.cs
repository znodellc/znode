﻿using System;
using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class AccountsEndpointFixture : BaseFixture
	{
		[Test]
		public void get_account()
		{
			var route = "accounts/11521";
			var response = GetModelFromEndpoint<AccountResponse>(route);

			Assert.IsNotNull(response.Account, "Account was null");
			Assert.IsTrue(response.Account.GetType() == typeof(AccountModel), "Account type was not AccountModel");
		}

		[Test]
		public void get_account_by_userid()
		{
			var route = "accounts/8086A820-D3F6-4763-AE07-CE84AB85E2E7";
			var response = GetModelFromEndpoint<AccountResponse>(route);

			Assert.IsNotNull(response.Account, "Account was null");
			Assert.IsTrue(response.Account.GetType() == typeof(AccountModel), "Account type was not AccountModel");
			Assert.IsTrue(response.Account.UserId.ToString().ToUpper() == "8086A820-D3F6-4763-AE07-CE84AB85E2E7", "Expected user ID 8086A820-D3F6-4763-AE07-CE84AB85E2E7, actual user ID " + response.Account.UserId);
		}

		[Test]
		public void get_account_expand_accounttype()
		{
			var route = "accounts/11521?expand=accounttype";
			var response = GetModelFromEndpoint<AccountResponse>(route);

			Assert.IsNotNull(response.Account.AccountType, "Account type was null");
			Assert.IsTrue(response.Account.AccountType.GetType() == typeof(AccountTypeModel), "Account type was not AccountTypeModel");
		}

		[Test]
		public void get_account_expand_addresses()
		{
			var route = "accounts/11521?expand=addresses";
			var response = GetModelFromEndpoint<AccountResponse>(route);

			Assert.IsNotNull(response.Account.Addresses, "Addresses were null");
			Assert.IsNotEmpty(response.Account.Addresses, "Addresses were empty");
			Assert.IsTrue(response.Account.Addresses.GetType() == typeof(Collection<AddressModel>), "Addresses type was not Collection<AddressModel>");
		}

		[Test]
		public void get_account_expand_orders()
		{
			var route = "accounts/11521?expand=orders";
			var response = GetModelFromEndpoint<AccountResponse>(route);

			Assert.IsNotNull(response.Account.Orders, "Orders were null");
			Assert.IsNotEmpty(response.Account.Orders, "Orders were empty");
			Assert.IsTrue(response.Account.Orders.GetType() == typeof(Collection<OrderModel>), "Orders type was not Collection<OrderModel>");
		}

		[Test]
		public void get_account_expand_profiles()
		{
			var route = "accounts/11521?expand=profiles";
			var response = GetModelFromEndpoint<AccountResponse>(route);

			Assert.IsNotNull(response.Account.Profiles, "Profiles were null");
			Assert.IsNotEmpty(response.Account.Profiles, "Profiles were empty");
			Assert.IsTrue(response.Account.Profiles.GetType() == typeof(Collection<ProfileModel>), "Profiles type was not Collection<ProfileModel>");
		}

        [Test]
        public void get_account_expand_wishlist()
        {
            var route = "accounts/11522?expand=wishlist";
            var response = GetModelFromEndpoint<AccountResponse>(route);

            Assert.IsNotNull(response.Account.WishList, "wishlist was null");
            Assert.IsNotEmpty(response.Account.WishList, "wishlist was empty");
            Assert.IsTrue(response.Account.WishList.GetType() == typeof(Collection<WishListModel>), "was type was not Collection<wishlist>");
        }


		[Test]
		public void get_account_expand_user()
		{
			var route = "accounts/11521?expand=user";
			var response = GetModelFromEndpoint<AccountResponse>(route);

			Assert.IsNotNull(response.Account.User, "User was null");
			Assert.IsTrue(response.Account.User.GetType() == typeof(UserModel), "User type was not UserModel");
			Assert.AreEqual(response.Account.UserId, response.Account.User.UserId, "User IDs did not match");
		}

		[Test]
		public void get_accounts()
		{
			var route = "accounts";
			var response = GetModelFromEndpoint<AccountListResponse>(route);

			Assert.IsNotNull(response.Accounts, "Accounts were null");
			Assert.IsTrue(response.Accounts.GetType() == typeof(Collection<AccountModel>), "Accounts type was not Collection<AccountModel>");
		}

		[Test]
		public void get_accounts_filter_accounttypeid_equals_null()
		{
			var route = "accounts?filter=accounttypeid~eq~null";
			var response = GetModelFromEndpoint<AccountListResponse>(route);

			Assert.IsNotNull(response.Accounts, "Accounts were null");
			Assert.IsTrue(response.Accounts.GetType() == typeof(Collection<AccountModel>), "Accounts type was not Collection<AccountModel>");

			foreach (var a in response.Accounts)
			{
				Assert.IsNull(a.AccountTypeId, "Expected account type ID equal to null, actual account type ID '" + a.AccountTypeId);
			}
		}

		[Test]
		public void get_accounts_filter_accounttypeid_not_equal_to_null()
		{
			var route = "accounts?filter=accounttypeid~ne~null";
			var response = GetModelFromEndpoint<AccountListResponse>(route);

			Assert.IsNotNull(response.Accounts, "Accounts were null");
			Assert.IsTrue(response.Accounts.GetType() == typeof(Collection<AccountModel>), "Accounts type was not Collection<AccountModel>");

			foreach (var a in response.Accounts)
			{
				Assert.IsNotNull(a.AccountTypeId, "Expected account type ID not equal to null, actual account type ID '" + a.AccountTypeId);
			}
		}

		[Test]
		public void get_accounts_filter_accounttypeid_equals_0()
		{
			var route = "accounts?filter=accounttypeid~eq~0";
			var response = GetModelFromEndpoint<AccountListResponse>(route);

			Assert.IsNotNull(response.Accounts, "Accounts were null");
			Assert.IsTrue(response.Accounts.GetType() == typeof(Collection<AccountModel>), "Accounts type was not Collection<AccountModel>");

			foreach (var a in response.Accounts)
			{
				Assert.AreEqual(0, a.AccountTypeId, "Expected account type ID 0, actual account type ID '" + a.AccountTypeId);
			}
		}

		[Test]
		public void get_accounts_filter_email_contains_test()
		{
			var route = "accounts?filter=email~cn~test";
			var response = GetModelFromEndpoint<AccountListResponse>(route);

			Assert.IsNotNull(response.Accounts, "Accounts were null");
			Assert.IsTrue(response.Accounts.GetType() == typeof(Collection<AccountModel>), "Accounts type was not Collection<AccountModel>");

			foreach (var a in response.Accounts)
			{
				Assert.IsTrue(a.Email.ToLower().Contains("test"), "Expected email to contain 'test', actual email '" + a.Email);
			}
		}

		[Test]
		public void get_accounts_filter_email_starts_with_znode()
		{
			var route = "accounts?filter=email~sw~znode";
			var response = GetModelFromEndpoint<AccountListResponse>(route);

			Assert.IsNotNull(response.Accounts, "Accounts were null");
			Assert.IsTrue(response.Accounts.GetType() == typeof(Collection<AccountModel>), "Accounts type was not Collection<AccountModel>");

			foreach (var a in response.Accounts)
			{
				Assert.IsTrue(a.Email.ToLower().StartsWith("znode"), "Expected email to start with 'znode', actual email '" + a.Email);
			}
		}

		[Test]
		public void get_accounts_filter_email_ends_with_com()
		{
			var route = "accounts?filter=email~ew~com";
			var response = GetModelFromEndpoint<AccountListResponse>(route);

			Assert.IsNotNull(response.Accounts, "Accounts were null");
			Assert.IsTrue(response.Accounts.GetType() == typeof(Collection<AccountModel>), "Accounts type was not Collection<AccountModel>");

			foreach (var a in response.Accounts)
			{
				Assert.IsTrue(a.Email.ToLower().EndsWith("com"), "Expected email to end with 'com', actual email '" + a.Email);
			}
		}

		[Test]
		public void create_update_delete_account()
		{
			// Create
			var route = "accounts";
			var data = GetDataFromFile("Account-Create.json");
		    try
		    {
                var response = PostModelToEndpoint<AccountResponse>(route, data);
		    }
		    catch (Exception ex)
		    { 
		        
		        throw;
		    }
			
			
            //Assert.IsNotNull(response.Account, "Account was null");
            //Assert.IsTrue(response.Account.AccountId > 0, "Account ID was not greater than 0");
            //Assert.IsTrue(response.Account.Custom1 == "NewAccount", "Expected custom 1 'NewAccount', actual custom 1 '" + response.Account.Custom1 + "'");

            //// Update
            //route = "accounts/" + response.Account.AccountId;
            //data = GetDataFromFile("Account-Update.json");
            //response = PutModelToEndpoint<AccountResponse>(route, data);

            //Assert.IsNotNull(response.Account, "Account was null");
            //Assert.IsTrue(response.Account.Custom1 == "UpdatedAccount", "Expected custom 1 'UpdatedAccount', actual custom 1 '" + response.Account.Custom1 + "'");

            //// Delete
            //route = "accounts/" + response.Account.AccountId;
            //var result = DeleteResourceFromEndpoint(route);

			//Assert.IsTrue(result, "Account was not deleted");
		}

		[Test]
		public void login()
		{
			var route = "accounts/login?expand=profiles";
			var data = GetDataFromFile("Account-Login.json");
			var response = PostModelToEndpoint<AccountResponse>(route, data, true, false, false);

			Assert.IsNotNull(response.Account, "Account was null");
			Assert.IsTrue(response.Account.GetType() == typeof(AccountModel), "Account type was not AccountModel");
			Assert.IsFalse(response.HasError, "Response had an error");
            Assert.IsTrue(response.Account.Profiles.Count > 0, "Profiles are not returned");
			Assert.AreEqual(0, response.ErrorCode, "Expected error code 0, actual error code " + response.ErrorCode);
		}

		[Test]
		public void change_password()
		{
			// Create
			var route = "accounts";
			var data = GetDataFromFile("Account-Create.json");
			var response = PostModelToEndpoint<AccountResponse>(route, data);

			Assert.IsNotNull(response.Account, "Account was null");
			Assert.IsTrue(response.Account.AccountId > 0, "Account ID was not greater than 0");
			Assert.IsTrue(response.Account.Custom1 == "NewAccount", "Expected custom 1 'NewAccount', actual custom 1 '" + response.Account.Custom1 + "'");

			// Hold the account ID
			var accountId = response.Account.AccountId;

			// Change the password
			route = "accounts/changepassword";
			data = GetDataFromFile("Account-ChangePassword.json");
			response = PostModelToEndpoint<AccountResponse>(route, data, true, false, false);

			// Delete
			route = "accounts/" + accountId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Account was not deleted");
		}

		[Test]
		public void reset_password()
		{
			// Create
			var route = "accounts";
			var data = GetDataFromFile("Account-Create.json");
			var response = PostModelToEndpoint<AccountResponse>(route, data);

			Assert.IsNotNull(response.Account, "Account was null");
			Assert.IsTrue(response.Account.AccountId > 0, "Account ID was not greater than 0");
			Assert.IsTrue(response.Account.Custom1 == "NewAccount", "Expected custom 1 'NewAccount', actual custom 1 '" + response.Account.Custom1 + "'");

			// Hold the account ID
			var accountId = response.Account.AccountId;

			// Do the reset
			route = "accounts/resetpassword";
			data = GetDataFromFile("Account-ResetPassword.json");
			response = PostModelToEndpoint<AccountResponse>(route, data, true, false, false);

			// Delete
			route = "accounts/" + accountId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Account was not deleted");
		}
	}
}
