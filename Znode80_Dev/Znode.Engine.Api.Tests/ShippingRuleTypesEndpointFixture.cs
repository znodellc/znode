﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class ShippingRuleTypesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_shipping_rule_type()
		{
			var route = "shippingruletypes/1";
			var response = GetModelFromEndpoint<ShippingRuleTypeResponse>(route);

			Assert.IsNotNull(response.ShippingRuleType, "Shipping rule type was null");
			Assert.IsTrue(response.ShippingRuleType.GetType() == typeof(ShippingRuleTypeModel), "Shipping rule type type was not ShippingRuleTypeModel");
		}

		[Test]
		public void get_shipping_rule_types()
		{
			var route = "shippingruletypes";
			var response = GetModelFromEndpoint<ShippingRuleTypeListResponse>(route);

			Assert.IsNotNull(response.ShippingRuleTypes, "Shipping rule types were null");
			Assert.IsTrue(response.ShippingRuleTypes.GetType() == typeof(Collection<ShippingRuleTypeModel>), "ShippingRuleTypes type was not Collection<ShippingRuleTypeModel>");
		}

		[Test]
		public void get_shipping_rule_types_filter_name_equals_fixed_rate_per_item()
		{
			var route = "shippingruletypes?filter=name~eq~fixed%20rate%20per%20item";
			var response = GetModelFromEndpoint<ShippingRuleTypeListResponse>(route);

			Assert.IsNotNull(response.ShippingRuleTypes, "Shipping rule types were null");
			Assert.IsTrue(response.ShippingRuleTypes.GetType() == typeof(Collection<ShippingRuleTypeModel>), "ShippingRuleTypes type was not Collection<ShippingRuleTypeModel>");

			foreach (var s in response.ShippingRuleTypes)
			{
				Assert.AreEqual("fixed rate per item", s.Name.ToLower(), "Expected name 'fixed rate per item', actual name '" + s.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_rule_types_filter_name_not_equal_to_fixed_rate_per_item()
		{
			var route = "shippingruletypes?filter=name~ne~fixed%20rate%20per%20item";
			var response = GetModelFromEndpoint<ShippingRuleTypeListResponse>(route);

			Assert.IsNotNull(response.ShippingRuleTypes, "Shipping rule types were null");
			Assert.IsTrue(response.ShippingRuleTypes.GetType() == typeof(Collection<ShippingRuleTypeModel>), "ShippingRuleTypes type was not Collection<ShippingRuleTypeModel>");

			foreach (var s in response.ShippingRuleTypes)
			{
				Assert.AreNotEqual("fixed rate per item", s.Name.ToLower(), "Expected name not equal to 'fixed rate per item'");
			}
		}

		[Test]
		public void get_shipping_rule_types_filter_name_contains_based()
		{
			var route = "shippingruletypes?filter=name~cn~based";
			var response = GetModelFromEndpoint<ShippingRuleTypeListResponse>(route);

			Assert.IsNotNull(response.ShippingRuleTypes, "Shipping rule types were null");
			Assert.IsTrue(response.ShippingRuleTypes.GetType() == typeof(Collection<ShippingRuleTypeModel>), "ShippingRuleTypes type was not Collection<ShippingRuleTypeModel>");

			foreach (var s in response.ShippingRuleTypes)
			{
				Assert.IsTrue(s.Name.ToLower().Contains("based"), "Expected name to contain 'based', actual name '" + s.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_rule_types_filter_name_starts_with_fixed()
		{
			var route = "shippingruletypes?filter=name~sw~fixed";
			var response = GetModelFromEndpoint<ShippingRuleTypeListResponse>(route);

			Assert.IsNotNull(response.ShippingRuleTypes, "Shipping rule types were null");
			Assert.IsTrue(response.ShippingRuleTypes.GetType() == typeof(Collection<ShippingRuleTypeModel>), "ShippingRuleTypes type was not Collection<ShippingRuleTypeModel>");

			foreach (var s in response.ShippingRuleTypes)
			{
				Assert.IsTrue(s.Name.ToLower().StartsWith("fixed"), "Expected name to start with 'fixed', actual name started with '" + s.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_rule_types_filter_name_ends_with_rate()
		{
			var route = "shippingruletypes?filter=name~ew~rate";
			var response = GetModelFromEndpoint<ShippingRuleTypeListResponse>(route);

			Assert.IsNotNull(response.ShippingRuleTypes, "Shipping rule types were null");
			Assert.IsTrue(response.ShippingRuleTypes.GetType() == typeof(Collection<ShippingRuleTypeModel>), "ShippingRuleTypes type was not Collection<ShippingRuleTypeModel>");

			foreach (var s in response.ShippingRuleTypes)
			{
				Assert.IsTrue(s.Name.ToLower().EndsWith("rate"), "Expected name to end with 'rate', actual name ended with '" + s.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_rule_types_page_index_0_size_4()
		{
			var route = "shippingruletypes?page=index~0,size~4";
			var response = GetModelFromEndpoint<ShippingRuleTypeListResponse>(route);

			Assert.IsNotNull(response.ShippingRuleTypes, "Shipping rule types were null");
			Assert.IsTrue(response.ShippingRuleTypes.GetType() == typeof(Collection<ShippingRuleTypeModel>), "ShippingRuleTypes type was not Collection<ShippingRuleTypeModel>");
			Assert.IsTrue(response.ShippingRuleTypes.Count == 4, "Expected count equal to 4, actual count " + response.ShippingRuleTypes.Count);
		}

		[Test]
		public void create_update_delete_shipping_rule_type()
		{
			// Create
			var route = "shippingruletypes";
			var data = GetDataFromFile("ShippingRuleType-Create.json");
			var response = PostModelToEndpoint<ShippingRuleTypeResponse>(route, data);

			Assert.IsNotNull(response.ShippingRuleType, "Shipping rule type was null");
			Assert.IsTrue(response.ShippingRuleType.ShippingRuleTypeId > 0, "Shipping type ID was not greater than 0");
			Assert.IsTrue(response.ShippingRuleType.Name == "NewShippingRuleType", "Expected name 'NewShippingRuleType', actual name '" + response.ShippingRuleType.Name + "'");

			// Update
			route = "shippingruletypes/" + response.ShippingRuleType.ShippingRuleTypeId;
			data = GetDataFromFile("ShippingRuleType-Update.json");
			response = PutModelToEndpoint<ShippingRuleTypeResponse>(route, data);

			Assert.IsNotNull(response.ShippingRuleType, "Shipping rule type was null");
			Assert.IsTrue(response.ShippingRuleType.Name == "UpdatedShippingRuleType", "Expected name 'UpdatedShippingRuleType', actual name '" + response.ShippingRuleType.Name + "'");

			// Delete
			route = "shippingruletypes/" + response.ShippingRuleType.ShippingRuleTypeId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Shipping rule type was not deleted");
		}
	}
}
