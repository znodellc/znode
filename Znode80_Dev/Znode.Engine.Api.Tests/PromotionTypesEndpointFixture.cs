﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class PromotionTypesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_promotion_type()
		{
			var route = "promotiontypes/1";
			var response = GetModelFromEndpoint<PromotionTypeResponse>(route);

			Assert.IsNotNull(response.PromotionType, "Promotion type was null");
			Assert.IsTrue(response.PromotionType.GetType() == typeof(PromotionTypeModel), "PromotionType type was not PromotionTypeModel");
		}

		[Test]
		public void get_promotion_types()
		{
			var route = "promotiontypes";
			var response = GetModelFromEndpoint<PromotionTypeListResponse>(route);

			Assert.IsNotNull(response.PromotionTypes, "Promotion types were null");
			Assert.IsTrue(response.PromotionTypes.GetType() == typeof(Collection<PromotionTypeModel>), "PromotionTypes type was not Collection<PromotionTypeModel>");
		}

		[Test]
		public void get_promotion_types_filter_class_type_equals_cart()
		{
			var route = "promotiontypes?filter=classtype~eq~cart";
			var response = GetModelFromEndpoint<PromotionTypeListResponse>(route);

			Assert.IsNotNull(response.PromotionTypes, "Promotion types were null");
			Assert.IsTrue(response.PromotionTypes.GetType() == typeof(Collection<PromotionTypeModel>), "PromotionTypes type was not Collection<PromotionTypeModel>");

			foreach (var p in response.PromotionTypes)
			{
				Assert.AreEqual("cart", p.ClassType.ToLower(), "Expected class type 'cart', actual class type '" + p.ClassType.ToLower() + "'");
			}
		}

		[Test]
		public void get_promotion_types_filter_class_type_not_equal_to_cart()
		{
			var route = "promotiontypes?filter=classtype~ne~cart";
			var response = GetModelFromEndpoint<PromotionTypeListResponse>(route);

			Assert.IsNotNull(response.PromotionTypes, "Promotion types were null");
			Assert.IsTrue(response.PromotionTypes.GetType() == typeof(Collection<PromotionTypeModel>), "PromotionTypes type was not Collection<PromotionTypeModel>");

			foreach (var p in response.PromotionTypes)
			{
				Assert.AreNotEqual("cart", p.ClassType.ToLower(), "Expected class type not equal to 'cart'");
			}
		}

		[Test]
		public void get_promotion_types_filter_class_name_contains_percent()
		{
			var route = "promotiontypes?filter=classname~cn~percent";
			var response = GetModelFromEndpoint<PromotionTypeListResponse>(route);

			Assert.IsNotNull(response.PromotionTypes, "Promotion types were null");
			Assert.IsTrue(response.PromotionTypes.GetType() == typeof(Collection<PromotionTypeModel>), "PromotionTypes type was not Collection<PromotionTypeModel>");

			foreach (var p in response.PromotionTypes)
			{
				Assert.IsTrue(p.ClassName.ToLower().Contains("percent"), "Expected class name to contain 'percent', actual class name '" + p.ClassName.ToLower() + "'");
			}
		}

		[Test]
		public void get_promotion_types_filter_name_starts_with_amount()
		{
			var route = "promotiontypes?filter=name~sw~amount";
			var response = GetModelFromEndpoint<PromotionTypeListResponse>(route);

			Assert.IsNotNull(response.PromotionTypes, "Promotion types were null");
			Assert.IsTrue(response.PromotionTypes.GetType() == typeof(Collection<PromotionTypeModel>), "PromotionTypes type was not Collection<PromotionTypeModel>");

			foreach (var p in response.PromotionTypes)
			{
				Assert.IsTrue(p.Name.ToLower().StartsWith("amount"), "Expected name to start with 'amount', actual name started with '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_promotion_types_filter_name_ends_with_product()
		{
			var route = "promotiontypes?filter=name~ew~product";
			var response = GetModelFromEndpoint<PromotionTypeListResponse>(route);

			Assert.IsNotNull(response.PromotionTypes, "Promotion types were null");
			Assert.IsTrue(response.PromotionTypes.GetType() == typeof(Collection<PromotionTypeModel>), "PromotionTypes type was not Collection<PromotionTypeModel>");

			foreach (var p in response.PromotionTypes)
			{
				Assert.IsTrue(p.Name.ToLower().EndsWith("product"), "Expected name to end with 'product', actual name ended with '" + p.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_promotion_types_page_index_0_size_5()
		{
			var route = "promotiontypes?page=index~0,size~5";
			var response = GetModelFromEndpoint<PromotionTypeListResponse>(route);

			Assert.IsNotNull(response.PromotionTypes, "Promotion types were null");
			Assert.IsTrue(response.PromotionTypes.GetType() == typeof(Collection<PromotionTypeModel>), "PromotionTypes type was not Collection<PromotionTypeModel>");
			Assert.IsTrue(response.PromotionTypes.Count == 5, "Expected count equal to 5, actual count " + response.PromotionTypes.Count);
		}

		[Test]
		public void create_update_delete_promotion_type()
		{
			// Create
			var route = "promotiontypes";
			var data = GetDataFromFile("PromotionType-Create.json");
			var response = PostModelToEndpoint<PromotionTypeResponse>(route, data);

			Assert.IsNotNull(response.PromotionType, "Promotion type was null");
			Assert.IsTrue(response.PromotionType.PromotionTypeId > 0, "Promotion type ID was not greater than 0");
			Assert.IsTrue(response.PromotionType.Name == "NewPromotionType", "Expected name 'NewPromotionType', actual name '" + response.PromotionType.Name + "'");

			// Update
			route = "promotiontypes/" + response.PromotionType.PromotionTypeId;
			data = GetDataFromFile("PromotionType-Update.json");
			response = PutModelToEndpoint<PromotionTypeResponse>(route, data);

			Assert.IsNotNull(response.PromotionType, "Promotion type was null");
			Assert.IsTrue(response.PromotionType.Name == "UpdatedPromotionType", "Expected name 'UpdatedPromotionType', actual name '" + response.PromotionType.Name + "'");

			// Delete
			route = "promotiontypes/" + response.PromotionType.PromotionTypeId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Promotion type was not deleted");
		}
	}
}
