﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class ShippingServiceCodesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_shipping_service_code()
		{
			var route = "shippingservicecodes/1";
			var response = GetModelFromEndpoint<ShippingServiceCodeResponse>(route);

			Assert.IsNotNull(response.ShippingServiceCode, "Shipping service code was null");
			Assert.IsTrue(response.ShippingServiceCode.GetType() == typeof(ShippingServiceCodeModel), "Shipping service code type was not ShippingServiceCodeModel");
		}

		[Test]
		public void get_shipping_service_codes()
		{
			var route = "shippingservicecodes";
			var response = GetModelFromEndpoint<ShippingServiceCodeListResponse>(route);

			Assert.IsNotNull(response.ShippingServiceCodes, "Shipping service codes were null");
			Assert.IsTrue(response.ShippingServiceCodes.GetType() == typeof(Collection<ShippingServiceCodeModel>), "ShippingServiceCodes type was not Collection<ShippingServiceCodeModel>");
		}

		[Test]
		public void get_shipping_service_codes_filter_code_equals_fedex_ground()
		{
			var route = "shippingservicecodes?filter=code~eq~fedex_ground";
			var response = GetModelFromEndpoint<ShippingServiceCodeListResponse>(route);

			Assert.IsNotNull(response.ShippingServiceCodes, "Shipping service codes were null");
			Assert.IsTrue(response.ShippingServiceCodes.GetType() == typeof(Collection<ShippingServiceCodeModel>), "ShippingServiceCodes type was not Collection<ShippingServiceCodeModel>");

			foreach (var s in response.ShippingServiceCodes)
			{
				Assert.AreEqual("fedex_ground", s.Code.ToLower(), "Expected code 'fedex_ground', actual code '" + s.Code.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_service_codes_filter_code_not_equal_to_fedex_ground()
		{
			var route = "shippingservicecodes?filter=code~ne~fedex_ground";
			var response = GetModelFromEndpoint<ShippingServiceCodeListResponse>(route);

			Assert.IsNotNull(response.ShippingServiceCodes, "Shipping service codes were null");
			Assert.IsTrue(response.ShippingServiceCodes.GetType() == typeof(Collection<ShippingServiceCodeModel>), "ShippingServiceCodes type was not Collection<ShippingServiceCodeModel>");

			foreach (var s in response.ShippingServiceCodes)
			{
				Assert.AreNotEqual("fedex_ground", s.Code.ToLower(), "Expected code not equal to 'fedex_ground'");
			}
		}

		[Test]
		public void get_shipping_service_codes_filter_description_contains_worldwide()
		{
			var route = "shippingservicecodes?filter=description~cn~worldwide";
			var response = GetModelFromEndpoint<ShippingServiceCodeListResponse>(route);

			Assert.IsNotNull(response.ShippingServiceCodes, "Shipping service codes were null");
			Assert.IsTrue(response.ShippingServiceCodes.GetType() == typeof(Collection<ShippingServiceCodeModel>), "ShippingServiceCodes type was not Collection<ShippingServiceCodeModel>");

			foreach (var s in response.ShippingServiceCodes)
			{
				Assert.IsTrue(s.Description.ToLower().Contains("worldwide"), "Expected description to contain 'worldwide', actual description '" + s.Description.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_service_codes_filter_description_starts_with_ups()
		{
			var route = "shippingservicecodes?filter=description~sw~ups";
			var response = GetModelFromEndpoint<ShippingServiceCodeListResponse>(route);

			Assert.IsNotNull(response.ShippingServiceCodes, "Shipping service codes were null");
			Assert.IsTrue(response.ShippingServiceCodes.GetType() == typeof(Collection<ShippingServiceCodeModel>), "ShippingServiceCodes type was not Collection<ShippingServiceCodeModel>");

			foreach (var s in response.ShippingServiceCodes)
			{
				Assert.IsTrue(s.Description.ToLower().StartsWith("ups"), "Expected description to start with 'ups', actual description started with '" + s.Description.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_service_codes_filter_description_ends_with_air()
		{
			var route = "shippingservicecodes?filter=description~ew~air";
			var response = GetModelFromEndpoint<ShippingServiceCodeListResponse>(route);

			Assert.IsNotNull(response.ShippingServiceCodes, "Shipping service codes were null");
			Assert.IsTrue(response.ShippingServiceCodes.GetType() == typeof(Collection<ShippingServiceCodeModel>), "ShippingServiceCodes type was not Collection<ShippingServiceCodeModel>");

			foreach (var s in response.ShippingServiceCodes)
			{
				Assert.IsTrue(s.Description.ToLower().EndsWith("air"), "Expected description to end with 'air', actual description ended with '" + s.Description.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_service_codes_page_index_1_size_5()
		{
			var route = "shippingservicecodes?page=index~1,size~5";
			var response = GetModelFromEndpoint<ShippingServiceCodeListResponse>(route);

			Assert.IsNotNull(response.ShippingServiceCodes, "Shipping service codes were null");
			Assert.IsTrue(response.ShippingServiceCodes.GetType() == typeof(Collection<ShippingServiceCodeModel>), "ShippingServiceCodes type was not Collection<ShippingServiceCodeModel>");
			Assert.IsTrue(response.ShippingServiceCodes.Count == 5, "Expected count equal to 5, actual count " + response.ShippingServiceCodes.Count);
		}

		[Test]
		public void create_update_delete_shipping_service_code()
		{
			// Create
			var route = "shippingservicecodes";
			var data = GetDataFromFile("ShippingServiceCode-Create.json");
			var response = PostModelToEndpoint<ShippingServiceCodeResponse>(route, data);

			Assert.IsNotNull(response.ShippingServiceCode, "Shipping service code was null");
			Assert.IsTrue(response.ShippingServiceCode.ShippingServiceCodeId > 0, "Shipping type ID was not greater than 0");
			Assert.IsTrue(response.ShippingServiceCode.Description == "NewShippingServiceCode", "Expected description 'NewShippingServiceCode', actual description '" + response.ShippingServiceCode.Description + "'");

			// Update
			route = "shippingservicecodes/" + response.ShippingServiceCode.ShippingServiceCodeId;
			data = GetDataFromFile("ShippingServiceCode-Update.json");
			response = PutModelToEndpoint<ShippingServiceCodeResponse>(route, data);

			Assert.IsNotNull(response.ShippingServiceCode, "Shipping service code was null");
			Assert.IsTrue(response.ShippingServiceCode.Description == "UpdatedShippingServiceCode", "Expected description 'UpdatedShippingServiceCode', actual description '" + response.ShippingServiceCode.Description + "'");

			// Delete
			route = "shippingservicecodes/" + response.ShippingServiceCode.ShippingServiceCodeId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Shipping service code was not deleted");
		}
	}
}
