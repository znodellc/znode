﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Net;
using System.Text;
using NUnit.Framework;
using Newtonsoft.Json;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	public class BaseFixture
	{
		public T GetModelFromEndpoint<T>(string route, Dictionary<string, string> additionalHeaders = null) where T : BaseResponse
		{
			var endpoint = GetFullEndpoint(route);

			var req = (HttpWebRequest)WebRequest.Create(endpoint);
			req.Method = "GET";
			req.Headers.Add(GetDomainKeyHeader());
			req.Headers.Add(GetDomainNameHeader());

			if (additionalHeaders != null)
			{
				foreach (var additionalHeader in additionalHeaders)
				{
					req.Headers.Add(String.Format("{0}:{1}", additionalHeader.Key, additionalHeader.Value));
				}
			}

			using (var rsp = (HttpWebResponse)req.GetResponse())
			{
				Assert.IsTrue(rsp.ContentLength > 0, "Expected content length greater than 0, actual content length " + rsp.ContentLength);
				Assert.IsTrue(rsp.ContentType.Contains("application/json"), "Expected content type application/json, actual content type " + rsp.ContentType);
				Assert.IsTrue(rsp.StatusCode == HttpStatusCode.OK, "Expected status code " + HttpStatusCode.OK + ", actual status code " + rsp.StatusCode);

				var model = GetModelFromResponse<T>(rsp);
				Assert.IsNotNull(model, "Response model was null");
				Assert.IsTrue(model.GetType() == typeof(T), "Response model type was not T");

				return model;
			}
		}

		public T PostModelToEndpoint<T>(string route, string data, bool doAsserts = true, bool checkLocationHeader = true, bool checkCreatedStatusCode = true) where T : BaseResponse
		{
			var endpoint = GetFullEndpoint(route);
			var dataBytes = Encoding.UTF8.GetBytes(data);

			var req = (HttpWebRequest)WebRequest.Create(endpoint);
			req.Method = "POST";
			req.ContentType = "application/json";
			req.ContentLength = dataBytes.Length;
			req.Headers.Add(GetDomainKeyHeader());
			req.Headers.Add(GetDomainNameHeader());

			using (var reqStream = req.GetRequestStream())
			{
				reqStream.Write(dataBytes, 0, dataBytes.Length);

				using (var rsp = (HttpWebResponse)req.GetResponse())
				{
					if (doAsserts)
					{
						Assert.IsTrue(rsp.ContentLength > 0, "Expected content length greater than 0, actual content length " + rsp.ContentLength);
						Assert.IsTrue(rsp.ContentType.Contains("application/json"), "Expected content type application/json, actual content type " + rsp.ContentType);

						if (checkLocationHeader) Assert.IsNotNullOrEmpty(rsp.Headers["Location"], "Response did not contain a location header");
						if (checkCreatedStatusCode) Assert.IsTrue(rsp.StatusCode == HttpStatusCode.Created, "Expected status code " + HttpStatusCode.Created + ", actual status code " + rsp.StatusCode);
					}

					var model = GetModelFromResponse<T>(rsp);
					Assert.IsNotNull(model, "Response model was null");
					Assert.IsTrue(model.GetType() == typeof(T), "Response model type was not T");

					return model;
				}
			}
		}

		public T PutModelToEndpoint<T>(string route, string data) where T : BaseResponse
		{
			var endpoint = GetFullEndpoint(route);
			var dataBytes = Encoding.UTF8.GetBytes(data);

			var req = (HttpWebRequest)WebRequest.Create(endpoint);
			req.Method = "PUT";
			req.ContentType = "application/json";
			req.ContentLength = dataBytes.Length;
			req.Headers.Add(GetDomainKeyHeader());
			req.Headers.Add(GetDomainNameHeader());

			using (var reqStream = req.GetRequestStream())
			{
				reqStream.Write(dataBytes, 0, dataBytes.Length);

				using (var rsp = (HttpWebResponse)req.GetResponse())
				{
					Assert.IsTrue(rsp.ContentLength > 0, "Expected content length greater than 0, actual content length " + rsp.ContentLength);
					Assert.IsTrue(rsp.ContentType.Contains("application/json"), "Expected content type application/json, actual content type " + rsp.ContentType);
					Assert.IsTrue(rsp.StatusCode == HttpStatusCode.OK, "Expected status code 200" + HttpStatusCode.OK + ", actual status code " + rsp.StatusCode);

					var model = GetModelFromResponse<T>(rsp);
					Assert.IsNotNull(model, "Response model was null");
					Assert.IsTrue(model.GetType() == typeof(T), "Response model type was not T");

					return model;
				}
			}
		}

		public bool DeleteResourceFromEndpoint(string route)
		{
			var endpoint = GetFullEndpoint(route);

			var req = (HttpWebRequest)WebRequest.Create(endpoint);
			req.Method = "DELETE";
			req.Headers.Add(GetDomainKeyHeader());
			req.Headers.Add(GetDomainNameHeader());

			using (var rsp = (HttpWebResponse)req.GetResponse())
			{
				Assert.IsTrue(rsp.StatusCode == HttpStatusCode.NoContent, "Expected status code " + HttpStatusCode.NoContent + ", actual status code " + rsp.StatusCode);
				return true;
			}
		}

		private T GetModelFromResponse<T>(WebResponse response) where T : BaseResponse
		{
			using (var body = response.GetResponseStream())
			{
				if (body != null)
				{
					using (var stream = new StreamReader(body))
					{
						using (var jsonReader = new JsonTextReader(stream))
						{
							var jsonSerializer = new JsonSerializer();
							return jsonSerializer.Deserialize<T>(jsonReader);
						}
					}
				}
			}

			return null;
		}

		public string GetFullEndpoint(string route)
		{
			return ConfigurationManager.AppSettings["ApiRootUri"] + route;
		}

		public string GetDomainKeyHeader()
		{
			return "Znode-DomainKey: " + ConfigurationManager.AppSettings["ApiKey"];
		}

		public string GetDomainNameHeader()
		{
			return "Znode-DomainName: " + ConfigurationManager.AppSettings["DomainName"];
		}

		public string GetDataFromFile(string dataFile)
		{
			var filePath = AppDomain.CurrentDomain.BaseDirectory + "\\Data\\" + dataFile;
			var reader = new StreamReader(filePath);
			return reader.ReadToEnd();
		}
	}
}
