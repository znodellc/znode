﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class ReviewsEndpointFixture : BaseFixture
	{
		[Test]
		public void get_review()
		{
			var route = "reviews/1";
			var response = GetModelFromEndpoint<ReviewResponse>(route);

			Assert.IsNotNull(response.Review, "Review was null");
			Assert.IsTrue(response.Review.GetType() == typeof(ReviewModel), "Review type was not ReviewModel");
		}

		[Test]
		public void get_reviews()
		{
			var route = "reviews";
			var response = GetModelFromEndpoint<ReviewListResponse>(route);

			Assert.IsNotNull(response.Reviews, "Reviews were null");
			Assert.IsTrue(response.Reviews.GetType() == typeof(Collection<ReviewModel>), "Reviews type was not Collection<ReviewModel>");
		}

		[Test]
		public void get_reviews_filter_productid_equals_302()
		{
			var route = "reviews?filter=productid~eq~302";
			var response = GetModelFromEndpoint<ReviewListResponse>(route);

			Assert.IsNotNull(response.Reviews, "Reviews were null");
			Assert.IsTrue(response.Reviews.GetType() == typeof(Collection<ReviewModel>), "Reviews type was not Collection<ReviewModel>");

			foreach (var r in response.Reviews)
			{
				Assert.AreEqual(302, r.ProductId, "Expected product ID 302, actual product ID " + r.ProductId);
			}
		}

		[Test]
		public void get_reviews_filter_productid_not_equal_to_302()
		{
			var route = "reviews?filter=productid~ne~302";
			var response = GetModelFromEndpoint<ReviewListResponse>(route);

			Assert.IsNotNull(response.Reviews, "Reviews were null");
			Assert.IsTrue(response.Reviews.GetType() == typeof(Collection<ReviewModel>), "Reviews type was not Collection<ReviewModel>");

			foreach (var r in response.Reviews)
			{
				Assert.AreNotEqual(302, r.ProductId, "Expected product ID not equal to 302");
			}
		}

		[Test]
		public void get_reviews_filter_productid_greater_than_335()
		{
			var route = "reviews?filter=productid~gt~335";
			var response = GetModelFromEndpoint<ReviewListResponse>(route);

			Assert.IsNotNull(response.Reviews, "Reviews were null");
			Assert.IsTrue(response.Reviews.GetType() == typeof(Collection<ReviewModel>), "Reviews type was not Collection<ReviewModel>");

			foreach (var r in response.Reviews)
			{
				Assert.IsTrue(r.ProductId > 335, "Expected product ID greater than 335");
			}
		}

		[Test]
		public void get_reviews_filter_productid_greater_than_or_equal_to_335()
		{
			var route = "reviews?filter=productid~ge~335";
			var response = GetModelFromEndpoint<ReviewListResponse>(route);

			Assert.IsNotNull(response.Reviews, "Reviews were null");
			Assert.IsTrue(response.Reviews.GetType() == typeof(Collection<ReviewModel>), "Reviews type was not Collection<ReviewModel>");

			foreach (var r in response.Reviews)
			{
				Assert.IsTrue(r.ProductId >= 335, "Expected product ID greater than or equal to 335");
			}
		}

		[Test]
		public void get_reviews_filter_productid_less_than_335()
		{
			var route = "reviews?filter=productid~lt~335";
			var response = GetModelFromEndpoint<ReviewListResponse>(route);

			Assert.IsNotNull(response.Reviews, "Reviews were null");
			Assert.IsTrue(response.Reviews.GetType() == typeof(Collection<ReviewModel>), "Reviews type was not Collection<ReviewModel>");

			foreach (var r in response.Reviews)
			{
				Assert.IsTrue(r.ProductId < 335, "Expected product ID less than 335");
			}
		}

		[Test]
		public void get_reviews_filter_productid_less_than_or_equal_to_335()
		{
			var route = "reviews?filter=productid~le~335";
			var response = GetModelFromEndpoint<ReviewListResponse>(route);

			Assert.IsNotNull(response.Reviews, "Reviews were null");
			Assert.IsTrue(response.Reviews.GetType() == typeof(Collection<ReviewModel>), "Reviews type was not Collection<ReviewModel>");

			foreach (var r in response.Reviews)
			{
				Assert.IsTrue(r.ProductId <= 335, "Expected product ID less than or equal to 335");
			}
		}

		[Test]
		public void get_reviews_page_index_0_size_5()
		{
			var route = "reviews?page=index~0,size~5";
			var response = GetModelFromEndpoint<ReviewListResponse>(route);

			Assert.IsNotNull(response.Reviews, "Reviews were null");
			Assert.IsTrue(response.Reviews.GetType() == typeof(Collection<ReviewModel>), "Reviews type was not Collection<ReviewModel>");
			Assert.IsTrue(response.Reviews.Count == 5, "Expected count equal to 5, actual count " + response.Reviews.Count);
		}

		[Test]
		public void create_update_delete_review()
		{
			// Create
			var route = "reviews";
			var data = GetDataFromFile("Review-Create.json");
			var response = PostModelToEndpoint<ReviewResponse>(route, data);

			Assert.IsNotNull(response.Review, "Review was null");
			Assert.IsTrue(response.Review.ReviewId > 0, "Review ID was not greater than 0");
			Assert.IsTrue(response.Review.Subject == "NewReview", "Expected subject 'NewReview', actual subject '" + response.Review.Subject + "'");

			// Update
			route = "reviews/" + response.Review.ReviewId;
			data = GetDataFromFile("Review-Update.json");
			response = PutModelToEndpoint<ReviewResponse>(route, data);

			Assert.IsNotNull(response.Review, "Review was null");
			Assert.IsTrue(response.Review.Subject == "UpdatedReview", "Expected subject 'UpdatedReview', actual subject '" + response.Review.Subject + "'");

			// Delete
			route = "reviews/" + response.Review.ReviewId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Review was not deleted");
		}
	}
}
