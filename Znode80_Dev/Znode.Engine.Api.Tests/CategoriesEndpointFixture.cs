﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class CategoriesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_category()
		{
			var route = "categories/81";
			var response = GetModelFromEndpoint<CategoryResponse>(route);

			Assert.IsNotNull(response.Category, "Category was null");
			Assert.IsTrue(response.Category.GetType() == typeof(CategoryModel), "Category type was not CategoryModel");
		}

		[Test]
		public void get_category_expand_categorynodes()
		{
			var route = "categories/81?expand=categorynodes";
			var response = GetModelFromEndpoint<CategoryResponse>(route);

			Assert.IsNotNull(response.Category.CategoryNodes, "Category nodes were null");
			Assert.IsNotEmpty(response.Category.CategoryNodes, "Category nodes were empty");
			Assert.IsTrue(response.Category.CategoryNodes.GetType() == typeof(Collection<CategoryNodeModel>), "Category nodes type was not Collection<CategoryNodeModel>");
		}

		[Test]
		public void get_category_expand_productids()
		{
			var route = "categories/81?expand=productids";
			var response = GetModelFromEndpoint<CategoryResponse>(route);

			Assert.IsNotNull(response.Category.ProductIds, "Product IDs were null");
			Assert.IsNotEmpty(response.Category.ProductIds, "Product IDs were empty");
		}

		[Test]
		public void get_category_expand_subcategories()
		{
			var route = "categories/81?expand=subcategories";
			var response = GetModelFromEndpoint<CategoryResponse>(route);

			Assert.IsNotNull(response.Category.Subcategories, "Subcategories were null");
			Assert.IsNotEmpty(response.Category.Subcategories, "Subcategories were empty");
			Assert.IsTrue(response.Category.Subcategories.GetType() == typeof(Collection<CategoryModel>), "Subcategories type was not Collection<CategoryModel>");
		}

		[Test]
		public void get_categories()
		{
			var route = "categories";
			var response = GetModelFromEndpoint<CategoryListResponse>(route);

			Assert.IsNotNull(response.Categories, "Categories were null");
			Assert.IsTrue(response.Categories.GetType() == typeof(Collection<CategoryModel>), "Categories type was not Collection<CategoryModel>");
		}

		[Test]
		public void get_categories_by_catalog()
		{
			var route = "categories/catalog/1";
			var response = GetModelFromEndpoint<CategoryListResponse>(route);

			Assert.IsNotNull(response.Categories, "Categories were null");
			Assert.IsTrue(response.Categories.GetType() == typeof(Collection<CategoryModel>), "Categories type was not Collection<CategoryModel>");
		}
	}
}
