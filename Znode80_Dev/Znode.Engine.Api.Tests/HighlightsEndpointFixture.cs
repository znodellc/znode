﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class HighlightsEndpointFixture : BaseFixture
	{
		[Test]
		public void get_highlight()
		{
			var route = "highlights/2";
			var response = GetModelFromEndpoint<HighlightResponse>(route);

			Assert.IsNotNull(response.Highlight, "Highlight was null");
			Assert.IsTrue(response.Highlight.GetType() == typeof(HighlightModel), "Highlight type was not HighlightModel");
		}

		[Test]
		public void get_highlights()
		{
			var route = "highlights";
			var response = GetModelFromEndpoint<HighlightListResponse>(route);

			Assert.IsNotNull(response.Highlights, "Highlights were null");
			Assert.IsTrue(response.Highlights.GetType() == typeof(Collection<HighlightModel>), "Highlights type was not Collection<HighlightModel>");
		}

		[Test]
		public void get_highlights_filter_name_equals_free_shipping()
		{
			var route = "highlights?filter=name~eq~free%20shipping";
			var response = GetModelFromEndpoint<HighlightListResponse>(route);

			Assert.IsNotNull(response.Highlights, "Highlights were null");
			Assert.IsTrue(response.Highlights.GetType() == typeof(Collection<HighlightModel>), "Highlights type was not Collection<HighlightModel>");

			foreach (var h in response.Highlights)
			{
				Assert.AreEqual("free shipping", h.Name.ToLower(), "Expected name 'free shipping', actual name " + h.Name.ToLower());
			}
		}

		[Test]
		public void get_highlights_filter_name_not_equal_to_free_shipping()
		{
			var route = "highlights?filter=name~ne~free%20shipping";
			var response = GetModelFromEndpoint<HighlightListResponse>(route);

			Assert.IsNotNull(response.Highlights, "Highlights were null");
			Assert.IsTrue(response.Highlights.GetType() == typeof(Collection<HighlightModel>), "Highlights type was not Collection<HighlightModel>");

			foreach (var h in response.Highlights)
			{
				Assert.AreNotEqual("free shipping", h.Name.ToLower(), "Expected name not equal to 'free shipping'");
			}
		}

		[Test]
		public void get_highlights_filter_name_contains_ship()
		{
			var route = "highlights?filter=name~cn~ship";
			var response = GetModelFromEndpoint<HighlightListResponse>(route);

			Assert.IsNotNull(response.Highlights, "Highlights were null");
			Assert.IsTrue(response.Highlights.GetType() == typeof(Collection<HighlightModel>), "Highlights type was not Collection<HighlightModel>");

			foreach (var h in response.Highlights)
			{
				Assert.IsTrue(h.Name.ToLower().Contains("ship"), "Expected name to contain 'ship', actual name '" + h.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_highlights_filter_name_starts_with_bio()
		{
			var route = "highlights?filter=name~sw~bio";
			var response = GetModelFromEndpoint<HighlightListResponse>(route);

			Assert.IsNotNull(response.Highlights, "Highlights were null");
			Assert.IsTrue(response.Highlights.GetType() == typeof(Collection<HighlightModel>), "Highlights type was not Collection<HighlightModel>");

			foreach (var h in response.Highlights)
			{
				Assert.IsTrue(h.Name.ToLower().StartsWith("bio"), "Expected name to start with 'bio', actual name '" + h.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_highlights_filter_name_ends_with_certified()
		{
			var route = "highlights?filter=name~ew~certified";
			var response = GetModelFromEndpoint<HighlightListResponse>(route);

			Assert.IsNotNull(response.Highlights, "Highlights were null");
			Assert.IsTrue(response.Highlights.GetType() == typeof(Collection<HighlightModel>), "Highlights type was not Collection<HighlightModel>");

			foreach (var h in response.Highlights)
			{
				Assert.IsTrue(h.Name.ToLower().EndsWith("certified"), "Expected name to end with 'certified', actual name '" + h.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_highlights_page_index_0_size_3()
		{
			var route = "highlights?page=index~0,size~3";
			var response = GetModelFromEndpoint<HighlightListResponse>(route);

			Assert.IsNotNull(response.Highlights, "Highlights were null");
			Assert.IsTrue(response.Highlights.GetType() == typeof(Collection<HighlightModel>), "Highlights type was not Collection<HighlightModel>");
			Assert.IsTrue(response.Highlights.Count == 3, "Expected count equal to 3, actual count " + response.Highlights.Count);
		}

		[Test]
		public void create_update_delete_highlight()
		{
			// Create
			var route = "highlights";
			var data = GetDataFromFile("Highlight-Create.json");
			var response = PostModelToEndpoint<HighlightResponse>(route, data);

			Assert.IsNotNull(response.Highlight, "Highlight was null");
			Assert.IsTrue(response.Highlight.HighlightId > 0, "Highlight ID was not greater than 0");
			Assert.IsTrue(response.Highlight.Name == "NewHighlight", "Expected name 'NewHighlight', actual name '" + response.Highlight.Name + "'");

			// Update
			route = "highlights/" + response.Highlight.HighlightId;
			data = GetDataFromFile("Highlight-Update.json");
			response = PutModelToEndpoint<HighlightResponse>(route, data);

			Assert.IsNotNull(response.Highlight, "Highlight was null");
			Assert.IsTrue(response.Highlight.Name == "UpdatedHighlight", "Expected name 'UpdatedHighlight', actual name '" + response.Highlight.Name + "'");

			// Delete
			route = "highlights/" + response.Highlight.HighlightId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Highlight was not deleted");
		}
	}
}
