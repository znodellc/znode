﻿using NUnit.Framework;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class SearchEndpointsFixture : BaseFixture
	{
		[Test]
		public void Search_TypeAheadTest()
		{
			var route = "search/suggested";
			var data = GetDataFromFile("Search-TypeAhead.json");
			var response = PostModelToEndpoint<SuggestedSearchListResponse>(route, data, false, false, false);

			Assert.IsNotNull(response.SuggestedSearchResults, "Search results were null");
			Assert.IsTrue(response.SuggestedSearchResults.Count == 6, "Expected extra products returned");
		}

		[Test]
		public void Search_ProductNameTest()
		{
			var route = "search/keyword";
			var data = GetDataFromFile("Search-ProductName.json");
			var response = PostModelToEndpoint<KeywordSearchResponse>(route, data, false, false, false);

			Assert.IsNotNull(response.Search.Products, "Product was null");
			Assert.IsTrue(response.Search.Products.Count == 1, "Expected extra products returned");
		}

		[Test]
		public void Search_ProductNameWithExtrnalIdTest()
		{
			var route = "search/keyword";
			var data = GetDataFromFile("Search-ProductNameExternalId.json");
			var response = PostModelToEndpoint<KeywordSearchResponse>(route, data, false, false, false);

			Assert.IsNotNull(response.Search.Products, "Product was null");
			Assert.IsTrue(response.Search.Products.Count == 1, "Expected extra products returned");
			Assert.IsTrue(response.Search.Products[0].Id == "qazwsx", "Expected extra products returned");
		}

		[Test]
		public void Search_CategoryNameWithExapndsTest()
		{
			var route = "search/keyword?expand=categories,facets";
			var data = GetDataFromFile("Search_CategoryName.json");
			var response = PostModelToEndpoint<KeywordSearchResponse>(route, data, false, false, false);

			Assert.IsNotNull(response.Search.Products, "Product was null");
			Assert.IsTrue(response.Search.Products.Count == 13, "Products count not matching");
			Assert.IsNotNull(response.Search.Categories, "Category was null");
			Assert.IsNotNull(response.Search.Facets, "Facet was null");
		}

		[Test]
		public void Search_CategoryNameWithSortTest()
		{
			var route = "search/keyword?sort=name~asc";
			var data = GetDataFromFile("Search_CategoryName.json");
			var response = PostModelToEndpoint<KeywordSearchResponse>(route, data, false, false, false);

			Assert.IsNotNull(response.Search.Products, "Product was null");
			Assert.IsTrue(response.Search.Products.Count == 13, "Products count not matching");

			route = "search/keyword?sort=name~desc";
			response = PostModelToEndpoint<KeywordSearchResponse>(route, data, false, false, false);

			Assert.IsNotNull(response.Search.Products, "Product was null");
			Assert.IsTrue(response.Search.Products.Count == 13, "Products count not Matching");
		}
	}
}
