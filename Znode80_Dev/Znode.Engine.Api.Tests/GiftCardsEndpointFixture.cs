﻿using NUnit.Framework;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class GiftCardsEndpointFixture : BaseFixture
	{
		[Test]
		public void create_update_delete_gift_card()
		{
			// Create
			var route = "giftcards";
			var data = GetDataFromFile("GiftCard-Create.json");
			var response = PostModelToEndpoint<GiftCardResponse>(route, data);

			Assert.IsNotNull(response.GiftCard, "Gift card was null");
			Assert.IsTrue(response.GiftCard.GiftCardId > 0, "Gift card ID was not greater than 0");
			Assert.IsTrue(response.GiftCard.Name == "NewGiftCard", "Expected name 'NewGiftCard', actual name '" + response.GiftCard.Name + "'");

			// Update
			route = "giftcards/" + response.GiftCard.GiftCardId;
			data = GetDataFromFile("GiftCard-Update.json");
			response = PutModelToEndpoint<GiftCardResponse>(route, data);

			Assert.IsNotNull(response.GiftCard, "Gift card was null");
			Assert.IsTrue(response.GiftCard.Name == "UpdatedGiftCard", "Expected name 'UpdatedGiftCard', actual name '" + response.GiftCard.Name + "'");

			// Delete
			route = "giftcards/" + response.GiftCard.GiftCardId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Gift card was not deleted");
		}
	}
}
