﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class TaxRulesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_tax_rule()
		{
			var route = "taxrules/18";
			var response = GetModelFromEndpoint<TaxRuleResponse>(route);

			Assert.IsNotNull(response.TaxRule, "Tax rule was null");
			Assert.IsTrue(response.TaxRule.GetType() == typeof(TaxRuleModel), "TaxRule type was not TaxRuleModel");
		}

		[Test]
		public void get_tax_rules()
		{
			var route = "taxrules";
			var response = GetModelFromEndpoint<TaxRuleListResponse>(route);

			Assert.IsNotNull(response.TaxRules, "Tax rules were null");
			Assert.IsTrue(response.TaxRules.GetType() == typeof(Collection<TaxRuleModel>), "TaxRules type was not Collection<TaxRuleModel>");
		}

		[Test]
		public void get_tax_rules_filter_portal_id_equals_1()
		{
			var route = "taxrules?filter=portalid~eq~1";
			var response = GetModelFromEndpoint<TaxRuleListResponse>(route);

			Assert.IsNotNull(response.TaxRules, "Tax rules were null");
			Assert.IsTrue(response.TaxRules.GetType() == typeof(Collection<TaxRuleModel>), "TaxRules type was not Collection<TaxRuleModel>");

			foreach (var t in response.TaxRules)
			{
				Assert.AreEqual(1, t.PortalId, "Expected portal ID 1, actual portal ID " + t.PortalId);
			}
		}

		[Test]
		public void get_tax_rules_filter_portal_id_not_equal_to_1()
		{
			var route = "taxrules?filter=portalid~ne~1";
			var response = GetModelFromEndpoint<TaxRuleListResponse>(route);

			Assert.IsNotNull(response.TaxRules, "Tax rules were null");
			Assert.IsTrue(response.TaxRules.GetType() == typeof(Collection<TaxRuleModel>), "TaxRules type was not Collection<TaxRuleModel>");

			foreach (var t in response.TaxRules)
			{
				Assert.AreNotEqual(1, t.PortalId, "Expected portal ID not equal to 1");
			}
		}

		[Test]
		public void get_tax_rules_page_index_0_size_1()
		{
			var route = "taxrules?page=index~0,size~1";
			var response = GetModelFromEndpoint<TaxRuleListResponse>(route);

			Assert.IsNotNull(response.TaxRules, "Tax rules were null");
			Assert.IsTrue(response.TaxRules.GetType() == typeof(Collection<TaxRuleModel>), "TaxRules type was not Collection<TaxRuleModel>");
			Assert.IsTrue(response.TaxRules.Count == 1, "Expected count equal to 1, actual count " + response.TaxRules.Count);
		}

		[Test]
		public void create_update_delete_tax_rule()
		{
			// Create
			var route = "taxrules";
			var data = GetDataFromFile("TaxRule-Create.json");
			var response = PostModelToEndpoint<TaxRuleResponse>(route, data);

			Assert.IsNotNull(response.TaxRule, "Tax rule was null");
			Assert.IsTrue(response.TaxRule.TaxRuleId > 0, "Tax rule ID was not greater than 0");
			Assert.IsTrue(response.TaxRule.CountryCode == "US", "Expected country code 'US', actual country code '" + response.TaxRule.CountryCode + "'");

			// Update
			route = "taxrules/" + response.TaxRule.TaxRuleId;
			data = GetDataFromFile("TaxRule-Update.json");
			response = PutModelToEndpoint<TaxRuleResponse>(route, data);

			Assert.IsNotNull(response.TaxRule, "Tax rule was null");
			Assert.IsTrue(response.TaxRule.CountryCode == "CA", "Expected country code 'CA', actual country code '" + response.TaxRule.CountryCode + "'");

			// Delete
			route = "taxrules/" + response.TaxRule.TaxRuleId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Tax rule was not deleted");
		}
	}
}
