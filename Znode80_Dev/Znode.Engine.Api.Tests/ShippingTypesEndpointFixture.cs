﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class ShippingTypesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_shipping_type()
		{
			var route = "shippingtypes/1";
			var response = GetModelFromEndpoint<ShippingTypeResponse>(route);

			Assert.IsNotNull(response.ShippingType, "Shipping type was null");
			Assert.IsTrue(response.ShippingType.GetType() == typeof(ShippingTypeModel), "Shipping type was not ShippingTypeModel");
		}

		[Test]
		public void get_shipping_types()
		{
			var route = "shippingtypes";
			var response = GetModelFromEndpoint<ShippingTypeListResponse>(route);

			Assert.IsNotNull(response.ShippingTypes, "Shipping types were null");
			Assert.IsTrue(response.ShippingTypes.GetType() == typeof(Collection<ShippingTypeModel>), "ShippingTypes type was not Collection<ShippingTypeModel>");
		}

		[Test]
		public void get_shipping_types_filter_name_equals_fedex()
		{
			var route = "shippingtypes?filter=name~eq~fedex";
			var response = GetModelFromEndpoint<ShippingTypeListResponse>(route);

			Assert.IsNotNull(response.ShippingTypes, "Shipping types were null");
			Assert.IsTrue(response.ShippingTypes.GetType() == typeof(Collection<ShippingTypeModel>), "ShippingTypes type was not Collection<ShippingTypeModel>");

			foreach (var s in response.ShippingTypes)
			{
				Assert.AreEqual("fedex", s.Name.ToLower(), "Expected name 'fedex', actual name '" + s.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_types_filter_name_not_equal_to_fedex()
		{
			var route = "shippingtypes?filter=name~ne~fedex";
			var response = GetModelFromEndpoint<ShippingTypeListResponse>(route);

			Assert.IsNotNull(response.ShippingTypes, "Shipping types were null");
			Assert.IsTrue(response.ShippingTypes.GetType() == typeof(Collection<ShippingTypeModel>), "ShippingTypes type was not Collection<ShippingTypeModel>");

			foreach (var s in response.ShippingTypes)
			{
				Assert.AreNotEqual("fedex", s.Name.ToLower(), "Expected name not equal to 'fedex'");
			}
		}

		[Test]
		public void get_shipping_types_filter_class_name_contains_znode()
		{
			var route = "shippingtypes?filter=classname~cn~znode";
			var response = GetModelFromEndpoint<ShippingTypeListResponse>(route);

			Assert.IsNotNull(response.ShippingTypes, "Shipping types were null");
			Assert.IsTrue(response.ShippingTypes.GetType() == typeof(Collection<ShippingTypeModel>), "ShippingTypes type was not Collection<ShippingTypeModel>");

			foreach (var s in response.ShippingTypes)
			{
				Assert.IsTrue(s.ClassName.ToLower().Contains("znode"), "Expected class name to contain 'znode', actual class name '" + s.ClassName.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_types_filter_class_name_starts_with_znode()
		{
			var route = "shippingtypes?filter=classname~sw~znode";
			var response = GetModelFromEndpoint<ShippingTypeListResponse>(route);

			Assert.IsNotNull(response.ShippingTypes, "Shipping types were null");
			Assert.IsTrue(response.ShippingTypes.GetType() == typeof(Collection<ShippingTypeModel>), "ShippingTypes type was not Collection<ShippingTypeModel>");

			foreach (var s in response.ShippingTypes)
			{
				Assert.IsTrue(s.ClassName.ToLower().StartsWith("znode"), "Expected class name to start with 'znode', actual class name started with '" + s.ClassName.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_types_filter_class_name_ends_with_fedex()
		{
			var route = "shippingtypes?filter=classname~ew~fedex";
			var response = GetModelFromEndpoint<ShippingTypeListResponse>(route);

			Assert.IsNotNull(response.ShippingTypes, "Shipping types were null");
			Assert.IsTrue(response.ShippingTypes.GetType() == typeof(Collection<ShippingTypeModel>), "ShippingTypes type was not Collection<ShippingTypeModel>");

			foreach (var s in response.ShippingTypes)
			{
				Assert.IsTrue(s.ClassName.ToLower().EndsWith("fedex"), "Expected class name to end with 'fedex', actual class name ended with '" + s.ClassName.ToLower() + "'");
			}
		}

		[Test]
		public void get_shipping_types_page_index_0_size_4()
		{
			var route = "shippingtypes?page=index~0,size~4";
			var response = GetModelFromEndpoint<ShippingTypeListResponse>(route);

			Assert.IsNotNull(response.ShippingTypes, "Shipping types were null");
			Assert.IsTrue(response.ShippingTypes.GetType() == typeof(Collection<ShippingTypeModel>), "ShippingTypes type was not Collection<ShippingTypeModel>");
			Assert.IsTrue(response.ShippingTypes.Count == 4, "Expected count equal to 4, actual count " + response.ShippingTypes.Count);
		}

		[Test]
		public void create_update_delete_shipping_type()
		{
			// Create
			var route = "shippingtypes";
			var data = GetDataFromFile("ShippingType-Create.json");
			var response = PostModelToEndpoint<ShippingTypeResponse>(route, data);

			Assert.IsNotNull(response.ShippingType, "Shipping type was null");
			Assert.IsTrue(response.ShippingType.ShippingTypeId > 0, "Shipping type ID was not greater than 0");
			Assert.IsTrue(response.ShippingType.Name == "NewShippingType", "Expected name 'NewShippingType', actual name '" + response.ShippingType.Name + "'");

			// Update
			route = "shippingtypes/" + response.ShippingType.ShippingTypeId;
			data = GetDataFromFile("ShippingType-Update.json");
			response = PutModelToEndpoint<ShippingTypeResponse>(route, data);

			Assert.IsNotNull(response.ShippingType, "Shipping type was null");
			Assert.IsTrue(response.ShippingType.Name == "UpdatedShippingType", "Expected name 'UpdatedShippingType', actual name '" + response.ShippingType.Name + "'");

			// Delete
			route = "shippingtypes/" + response.ShippingType.ShippingTypeId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Shipping type was not deleted");
		}
	}
}
