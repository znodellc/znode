﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class TaxRuleTypesEndpointFixture : BaseFixture
	{
		[Test]
		public void get_tax_rule_type()
		{
			var route = "taxruletypes/1";
			var response = GetModelFromEndpoint<TaxRuleTypeResponse>(route);

			Assert.IsNotNull(response.TaxRuleType, "Tax rule type was null");
			Assert.IsTrue(response.TaxRuleType.GetType() == typeof(TaxRuleTypeModel), "TaxRuleType type was not TaxRuleTypeModel");
		}

		[Test]
		public void get_tax_rule_types()
		{
			var route = "taxruletypes";
			var response = GetModelFromEndpoint<TaxRuleTypeListResponse>(route);

			Assert.IsNotNull(response.TaxRuleTypes, "Tax rule types were null");
			Assert.IsTrue(response.TaxRuleTypes.GetType() == typeof(Collection<TaxRuleTypeModel>), "TaxRuleTypes type was not Collection<TaxRuleTypeModel>");
		}

		[Test]
		public void get_tax_rule_types_filter_name_equals_sales_tax()
		{
			var route = "taxruletypes?filter=name~eq~sales%20tax";
			var response = GetModelFromEndpoint<TaxRuleTypeListResponse>(route);

			Assert.IsNotNull(response.TaxRuleTypes, "Tax rule types were null");
			Assert.IsTrue(response.TaxRuleTypes.GetType() == typeof(Collection<TaxRuleTypeModel>), "TaxRuleTypes type was not Collection<TaxRuleTypeModel>");

			foreach (var t in response.TaxRuleTypes)
			{
				Assert.AreEqual("sales tax", t.Name.ToLower(), "Expected name 'sales tax', actual name '" + t.Name.ToLower() + "'");
			}
		}

		[Test]
		public void get_tax_rule_types_filter_name_not_equal_to_sales_tax()
		{
			var route = "taxruletypes?filter=name~ne~sales%20tax";
			var response = GetModelFromEndpoint<TaxRuleTypeListResponse>(route);

			Assert.IsNotNull(response.TaxRuleTypes, "Tax rule types were null");
			Assert.IsTrue(response.TaxRuleTypes.GetType() == typeof(Collection<TaxRuleTypeModel>), "TaxRuleTypes type was not Collection<TaxRuleTypeModel>");

			foreach (var t in response.TaxRuleTypes)
			{
				Assert.AreNotEqual("sales tax", t.Name.ToLower(), "Expected name not equal to 'sales tax'");
			}
		}

		[Test]
		public void get_tax_rule_types_filter_class_name_contains_znode()
		{
			var route = "taxruletypes?filter=classname~cn~znode";
			var response = GetModelFromEndpoint<TaxRuleTypeListResponse>(route);

			Assert.IsNotNull(response.TaxRuleTypes, "Tax rule types were null");
			Assert.IsTrue(response.TaxRuleTypes.GetType() == typeof(Collection<TaxRuleTypeModel>), "TaxRuleTypes type was not Collection<TaxRuleTypeModel>");

			foreach (var t in response.TaxRuleTypes)
			{
				Assert.IsTrue(t.ClassName.ToLower().Contains("znode"), "Expected class name to contain 'znode', actual class name '" + t.ClassName.ToLower() + "'");
			}
		}

		[Test]
		public void get_tax_rule_types_filter_class_name_starts_with_znode()
		{
			var route = "taxruletypes?filter=classname~sw~znode";
			var response = GetModelFromEndpoint<TaxRuleTypeListResponse>(route);

			Assert.IsNotNull(response.TaxRuleTypes, "Tax rule types were null");
			Assert.IsTrue(response.TaxRuleTypes.GetType() == typeof(Collection<TaxRuleTypeModel>), "TaxRuleTypes type was not Collection<TaxRuleTypeModel>");

			foreach (var t in response.TaxRuleTypes)
			{
				Assert.IsTrue(t.ClassName.ToLower().StartsWith("znode"), "Expected class name to start with 'znode', actual class name started with '" + t.ClassName.ToLower() + "'");
			}
		}

		[Test]
		public void get_tax_rule_types_filter_class_name_ends_with_tax()
		{
			var route = "taxruletypes?filter=classname~ew~tax";
			var response = GetModelFromEndpoint<TaxRuleTypeListResponse>(route);

			Assert.IsNotNull(response.TaxRuleTypes, "Tax rule types were null");
			Assert.IsTrue(response.TaxRuleTypes.GetType() == typeof(Collection<TaxRuleTypeModel>), "TaxRuleTypes type was not Collection<TaxRuleTypeModel>");

			foreach (var t in response.TaxRuleTypes)
			{
				Assert.IsTrue(t.ClassName.ToLower().EndsWith("tax"), "Expected class name to end with 'tax', actual class name ended with '" + t.ClassName.ToLower() + "'");
			}
		}

		[Test]
		public void get_tax_rule_types_page_index_0_size_1()
		{
			var route = "taxruletypes?page=index~0,size~1";
			var response = GetModelFromEndpoint<TaxRuleTypeListResponse>(route);

			Assert.IsNotNull(response.TaxRuleTypes, "Tax rule types were null");
			Assert.IsTrue(response.TaxRuleTypes.GetType() == typeof(Collection<TaxRuleTypeModel>), "TaxRuleTypes type was not Collection<TaxRuleTypeModel>");
			Assert.IsTrue(response.TaxRuleTypes.Count == 1, "Expected count equal to 1, actual count " + response.TaxRuleTypes.Count);
		}

		[Test]
		public void create_update_delete_tax_rule_type()
		{
			// Create
			var route = "taxruletypes";
			var data = GetDataFromFile("TaxRuleType-Create.json");
			var response = PostModelToEndpoint<TaxRuleTypeResponse>(route, data);

			Assert.IsNotNull(response.TaxRuleType, "Tax rule type was null");
			Assert.IsTrue(response.TaxRuleType.TaxRuleTypeId > 0, "TaxRuleType ID was not greater than 0");
			Assert.IsTrue(response.TaxRuleType.Name == "NewTaxRuleType", "Expected name 'NewTaxRuleType', actual name '" + response.TaxRuleType.Name + "'");

			// Update
			route = "taxruletypes/" + response.TaxRuleType.TaxRuleTypeId;
			data = GetDataFromFile("TaxRuleType-Update.json");
			response = PutModelToEndpoint<TaxRuleTypeResponse>(route, data);

			Assert.IsNotNull(response.TaxRuleType, "Tax rule type was null");
			Assert.IsTrue(response.TaxRuleType.Name == "UpdatedTaxRuleType", "Expected name 'UpdatedTaxRuleType', actual name '" + response.TaxRuleType.Name + "'");

			// Delete
			route = "taxruletypes/" + response.TaxRuleType.TaxRuleTypeId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Tax rule type was not deleted");
		}
	}
}
