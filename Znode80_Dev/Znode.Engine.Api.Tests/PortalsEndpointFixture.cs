﻿using System.Collections.ObjectModel;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Tests
{
	[TestFixture]
	public class PortalsEndpointFixture : BaseFixture
	{
		[Test]
		public void get_portal()
		{
			var route = "portals/1";
			var response = GetModelFromEndpoint<PortalResponse>(route);

			Assert.IsNotNull(response.Portal, "Portal was null");
			Assert.IsTrue(response.Portal.GetType() == typeof(PortalModel), "Portal type was not PortalModel");
		}

		[Test]
		public void get_portal_expand_portalcountries()
		{
			var route = "portals/1?expand=portalcountries";
			var response = GetModelFromEndpoint<PortalResponse>(route);

			Assert.IsNotNull(response.Portal.PortalCountries, "Portal countries were null");
			Assert.IsNotEmpty(response.Portal.PortalCountries, "Portal countries were empty");
			Assert.IsTrue(response.Portal.PortalCountries.GetType() == typeof(Collection<PortalCountryModel>), "PortalCountries type was not Collection<PortalCountryModel>");
		}

        [Test]
        public void get_portal_expand_Catalogs()
        {
            var route = "portals/1?expand=catalogs";
            var response = GetModelFromEndpoint<PortalResponse>(route);

            Assert.IsNotNull(response.Portal.Catalogs, "Portal catalogs were null");
            Assert.IsNotEmpty(response.Portal.Catalogs, "Portal catalogs were empty");
            Assert.IsTrue(response.Portal.Catalogs[0].CatalogId > 0, "Catalog Id is 0");
        }

		[Test]
		public void get_portals()
		{
			var route = "portals";
			var response = GetModelFromEndpoint<PortalListResponse>(route);

			Assert.IsNotNull(response.Portals, "Portals were null");
			Assert.IsTrue(response.Portals.GetType() == typeof(Collection<PortalModel>), "Portals type was not Collection<PortalModel>");
		}

		[Test]
		public void get_portals_filter_externalid_equals_null()
		{
			var route = "portals?filter=externalid~eq~null";
			var response = GetModelFromEndpoint<PortalListResponse>(route);

			Assert.IsNotNull(response.Portals, "Portals were null");
			Assert.IsTrue(response.Portals.GetType() == typeof(Collection<PortalModel>), "Portals type was not Collection<PortalModel>");
		}

		[Test]
		public void get_portals_filter_storename_not_equal_to_null()
		{
			var route = "portals?filter=storename~ne~null";
			var response = GetModelFromEndpoint<PortalListResponse>(route);

			Assert.IsNotNull(response.Portals, "Portals were null");
			Assert.IsTrue(response.Portals.GetType() == typeof(Collection<PortalModel>), "Portals type was not Collection<PortalModel>");
		}

		[Test]
		public void get_portals_filter_storename_equals_fine_foods()
		{
			var route = "portals?filter=storename~eq~fine%20foods";
			var response = GetModelFromEndpoint<PortalListResponse>(route);

			Assert.IsNotNull(response.Portals, "Portals were null");
			Assert.IsTrue(response.Portals.GetType() == typeof(Collection<PortalModel>), "Portals type was not Collection<PortalModel>");

			foreach (var p in response.Portals)
			{
				Assert.AreEqual("fine foods", p.StoreName.ToLower(), "Expected store name 'fine foods', actual store name '" + p.StoreName.ToLower() + "'");
			}
		}

		[Test]
		public void get_portals_filter_storename_not_equal_to_fine_foods()
		{
			var route = "portals?filter=storename~ne~fine%20foods";
			var response = GetModelFromEndpoint<PortalListResponse>(route);

			Assert.IsNotNull(response.Portals, "Portals were null");
			Assert.IsTrue(response.Portals.GetType() == typeof(Collection<PortalModel>), "Portals type was not Collection<PortalModel>");

			foreach (var p in response.Portals)
			{
				Assert.AreNotEqual("fine foods", p.StoreName.ToLower(), "Expected store name not equal to 'fine foods'");
			}
		}

		[Test]
		public void get_portals_filter_storename_contains_cheese()
		{
			var route = "portals?filter=storename~cn~cheese";
			var response = GetModelFromEndpoint<PortalListResponse>(route);

			Assert.IsNotNull(response.Portals, "Portals were null");
			Assert.IsTrue(response.Portals.GetType() == typeof(Collection<PortalModel>), "Portals type was not Collection<PortalModel>");

			foreach (var p in response.Portals)
			{
				Assert.IsTrue(p.StoreName.ToLower().Contains("cheese"), "Expected store name to contain 'cheese', actual store name '" + p.StoreName.ToLower() + "'");
			}
		}

		[Test]
		public void get_portals_filter_storename_starts_with_wine()
		{
			var route = "portals?filter=storename~sw~wine";
			var response = GetModelFromEndpoint<PortalListResponse>(route);

			Assert.IsNotNull(response.Portals, "Portals were null");
			Assert.IsTrue(response.Portals.GetType() == typeof(Collection<PortalModel>), "Portals type was not Collection<PortalModel>");

			foreach (var p in response.Portals)
			{
				Assert.IsTrue(p.StoreName.ToLower().StartsWith("wine"), "Expected store name to start with 'wine', actual store name '" + p.StoreName.ToLower() + "'");
			}
		}

		[Test]
		public void get_portals_filter_storename_ends_with_wholesaler()
		{
			var route = "portals?filter=storename~ew~wholesaler";
			var response = GetModelFromEndpoint<PortalListResponse>(route);

			Assert.IsNotNull(response.Portals, "Portals were null");
			Assert.IsTrue(response.Portals.GetType() == typeof(Collection<PortalModel>), "Portals type was not Collection<PortalModel>");

			foreach (var p in response.Portals)
			{
				Assert.IsTrue(p.StoreName.ToLower().EndsWith("wholesaler"), "Expected store name to end with 'wholesaler', actual store name '" + p.StoreName.ToLower() + "'");
			}
		}

		[Test]
		public void get_portals_page_index_0_size_2()
		{
			var route = "portals?page=index~0,size~2";
			var response = GetModelFromEndpoint<PortalListResponse>(route);

			Assert.IsNotNull(response.Portals, "Portals were null");
			Assert.IsTrue(response.Portals.GetType() == typeof(Collection<PortalModel>), "Portals type was not Collection<PortalModel>");
			Assert.IsTrue(response.Portals.Count == 2, "Expected count equal to 2, actual count " + response.Portals.Count);
		}

		[Test]
		public void create_update_delete_portal()
		{
			// Create
			var route = "portals";
			var data = GetDataFromFile("Portal-Create.json");
			var response = PostModelToEndpoint<PortalResponse>(route, data);

			Assert.IsNotNull(response.Portal, "Portal was null");
			Assert.IsTrue(response.Portal.PortalId > 0, "Portal ID was not greater than 0");
			Assert.IsTrue(response.Portal.StoreName == "NewPortal", "Expected store name 'NewPortal', actual store name '" + response.Portal.StoreName + "'");

			// Update
			route = "portals/" + response.Portal.PortalId;
			data = GetDataFromFile("Portal-Update.json");
			response = PutModelToEndpoint<PortalResponse>(route, data);

			Assert.IsNotNull(response.Portal, "Portal was null");
			Assert.IsTrue(response.Portal.StoreName == "UpdatedPortal", "Expected store name 'UpdatedPortal', actual store name '" + response.Portal.StoreName + "'");

			// Delete
			route = "portals/" + response.Portal.PortalId;
			var result = DeleteResourceFromEndpoint(route);

			Assert.IsTrue(result, "Portal was not deleted");
		}
	}
}
