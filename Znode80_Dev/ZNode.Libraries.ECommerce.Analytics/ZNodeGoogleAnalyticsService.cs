using System.Text;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Analytics
{
    /// <summary>
    /// Provides Analytics code specific to Google that will put javascript on selected pages.
    /// </summary>
    public class ZNodeGoogleAnalyticsService : ZNodeAnalyticsService
    {
        #region Public Methods

        /// <summary>
        /// Gets the order receipt page analytics javascript.
        /// </summary>
        public override string OrderReceiptJavascript
        {
            get { return this.GetOrderReceiptJavascript(); }
        }
        #endregion

        #region Private Helper Methods
        /// <summary>
        /// Injects the ecommerce code in the checkout page
        /// </summary>
        /// <returns>Returns the order receipt page analytics script.</returns>
        protected string GetOrderReceiptJavascript()
        {
            StringBuilder sb = new StringBuilder(string.Empty);
            ZNodeAnalyticsData analytics = new ZNodeAnalyticsData();

            // Only output the cart information if the Google Analytics has been specified.
            if (analytics.IsOrderReceiptPage)
            {
                if (analytics.SiteWideBottomJavascript.Contains("google-analytics.com") || analytics.SiteWideTopJavascript.Contains("google-analytics.com"))
                {
                    // Build the additional Google Asynchronous Javascript for the order receipt page.                    
                    sb.Append("<script type='text/javascript'>");
                    sb.Append("_gaq.push(['_addTrans',");

                    sb.Append(CreateQuotedString(Order.OrderID.ToString()));
                    sb.Append(", ");

                    sb.Append(CreateQuotedString(Order.BillingAddress.FirstName + " " + Order.BillingAddress.LastName));
                    sb.Append(", ");

                    sb.Append(CreateQuotedString(((decimal)Order.Total).ToString("N2")));
                    sb.Append(", ");

                    sb.Append(CreateQuotedString(((decimal)Order.TaxCost).ToString("N2")));
                    sb.Append(", ");

                    sb.Append(CreateQuotedString(((decimal)Order.ShippingCost).ToString("N2")));
                    sb.Append(", ");

                    sb.Append(CreateQuotedString(Order.BillingAddress.City));
                    sb.Append(", ");

                    sb.Append(CreateQuotedString(Order.BillingAddress.StateCode));
                    sb.Append(", ");

                    sb.Append(CreateQuotedString(Order.BillingAddress.CountryCode));

                    sb.Append("]);");

                    // Append order line items
                    foreach (OrderLineItem orderLineItem in Order.OrderLineItems)
                    {
                        sb.Append(" _gaq.push(['_addItem',");

                        sb.Append(CreateQuotedString(orderLineItem.OrderID.ToString()));
                        sb.Append(", ");

                        sb.Append(CreateQuotedString(orderLineItem.SKU));
                        sb.Append(", ");

                        sb.Append(CreateQuotedString(orderLineItem.Name));
                        sb.Append(", ");

                        sb.Append(CreateQuotedString("Product"));
                        sb.Append(", ");

                        sb.Append(CreateQuotedString(((decimal)orderLineItem.Price).ToString("N2")));
                        sb.Append(", ");

                        sb.Append(CreateQuotedString(orderLineItem.Quantity.ToString()));

                        sb.Append("]); ");
                    }

                    // Submits transaction to the Analytics servers
                    sb.Append("_gaq.push(['_trackTrans']); ");

                    sb.Append("</script>");
                }

                sb.Append(analytics.OrderReceiptJavascript);
            }

            return sb.ToString();
        }
        #endregion
    }
}