﻿using System;
using System.Data;
using System.Linq;
using System.Xml;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Utilities
{
    /// <summary>
    /// RSS Writer File
    /// </summary>
    public class ZNodeRssWriter : ZNodeBusinessBase
    {
        #region Private Variables
        private int fileCount = 0;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the ZNodeRssWriter class.
        /// </summary>
        public ZNodeRssWriter()
        {
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Creates the XMLSite Map
        /// </summary>
        /// <param name="datasetValues">data set values</param>
        /// <param name="rootTag">Root Tag for the XML</param>
        /// <param name="rootTagValue">Root Tag value</param>
        /// <param name="xmlFileName">FileName of the XML</param>
        /// <returns>Returns the boolean value if it created or not.</returns>
        public int CreateXMLSiteMap(DataSet datasetValues, string rootTag, string rootTagValue, string xmlFileName)
        {           
            try
            {
                int recordsCount = datasetValues.Tables[0].Rows.Count;
                int loopCnt = 0;
                int recCnt = 0;

                // Number of records to be generated in the file.
                if (System.Configuration.ConfigurationManager.AppSettings["XMLSiteMapRecordCount"] != null)
                {
                    recCnt = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["XMLSiteMapRecordCount"].ToString());
                }

                for (; loopCnt < recordsCount;)
                {
                    string filePath = string.Format("{0}{1}_{2}{3}", ZNodeConfigManager.EnvironmentConfig.ContentPath, xmlFileName.Trim(), this.fileCount.ToString(), ".xml");
                    string[] rootTagValues = rootTagValue.Split(',');

                    // Construct the XML for the Site Map creation
                    XmlDocument requestXMLDoc = new XmlDocument();

                    requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", "UTF-8", null));

                    XmlElement urlsetElement = null;

                    urlsetElement = requestXMLDoc.CreateElement(rootTag);

                    for (int i = 0; i < rootTagValues.Count(); i++)
                    {
                        string[] values = rootTagValues[i].Split('=');

                        urlsetElement.SetAttribute(values[0], values[1]);
                    }

                    requestXMLDoc.AppendChild(urlsetElement);

                    // Loop thro the dataset values.
                    do
                    {
                        DataRow dr = datasetValues.Tables[0].Rows[loopCnt];

                        XmlElement urlElement = requestXMLDoc.CreateElement(datasetValues.Tables[0].TableName);

                        urlsetElement.AppendChild(urlElement);

                        foreach (DataColumn dc in datasetValues.Tables[0].Columns)
                        {
                            if (!string.IsNullOrEmpty(dr[dc.ColumnName].ToString()))
                            {
                                urlElement.AppendChild(this.MakeElement(requestXMLDoc, dc.ColumnName, dr[dc.ColumnName].ToString()));
                            }
                        }

                        loopCnt++;
                    }
                    while (loopCnt < recordsCount && (loopCnt + 1) % recCnt != 0);

                    ZNodeStorageManager.WriteTextStorage(requestXMLDoc.OuterXml, filePath);

                    // Increment the file count if the file has to be splitted.
                    this.fileCount++;
                }
            }
            catch (Exception)
            {
                return 0;
            }

            return this.fileCount;
        }

        /// <summary>
        /// Creates and Returns an Element with the specified tagName with the value 
        /// </summary>
        /// <param name="doc">Xml document</param>
        /// <param name="tagName">Value of Tag Name</param>
        /// <param name="tagValue">Value of TagValue</param>
        /// <returns>Returns the Xml Element</returns>
        private XmlElement MakeElement(XmlDocument doc, string tagName, string tagValue)
        {
            XmlElement elem;

            if (tagName.Contains("g:"))
            {
                elem = doc.CreateElement("g", tagName.Split(':')[1], "http://base.google.com/ns/1.0");
            }
            else
            {
                elem = doc.CreateElement(tagName);
            }

            elem.InnerText = tagValue;

            return elem;
        }

        public string GenerateGoogleSiteMapIndexFiles(int fileNameCount, string txtXMLFileName)
        {
            string rootTag = "sitemapindex";
            string roottagxmlns = "http://www.sitemaps.org/schemas/sitemap/0.9";

            // Construct the XML for the Site Map creation
            XmlDocument requestXMLDoc = new XmlDocument();

            requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", "UTF-8", null));

            XmlElement urlsetElement = null;

            urlsetElement = requestXMLDoc.CreateElement(rootTag);
            urlsetElement.SetAttribute("xmlns", roottagxmlns);

            requestXMLDoc.AppendChild(urlsetElement);
                       
            for (int i = 0; i < fileNameCount; i++)
            {
                XmlElement urlElement = requestXMLDoc.CreateElement("sitemap");
                string fileName = ZNodeStorageManager.HttpPath(string.Format("{0}{1}_{2}{3}", ZNodeConfigManager.EnvironmentConfig.ContentPath, txtXMLFileName.Trim(), i, ".xml"));
                if (fileName.StartsWith("~/")) fileName = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.Replace(System.Web.HttpContext.Current.Request.Url.AbsolutePath, string.Empty) + fileName.Substring(1);
                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "loc", fileName));
                urlElement.AppendChild(this.MakeElement(requestXMLDoc, "lastmod", DateTime.UtcNow.ToString("yyyy-MM-ddThh-mm-sszzz")));
                urlsetElement.AppendChild(urlElement);
            }

            string strSiteMapIndexFile = string.Format("{0}{1}_{2}{3}", ZNodeConfigManager.EnvironmentConfig.ContentPath, txtXMLFileName.Trim(), DateTime.UtcNow.ToString("yyyy-MM-ddThh-mm-ss"), ".xml");
            ZNodeStorageManager.WriteTextStorage(requestXMLDoc.OuterXml, strSiteMapIndexFile);

            return strSiteMapIndexFile;
        }
        #endregion
    }
}
