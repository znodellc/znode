﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace ZNode.Libraries.ECommerce.Utilities.Extensions
{
    public static class ZnodeAdoNetExtensions
    {
        public static string InferString(this DataRow dr, string columnName)
        {
            if (!dr.Table.Columns.Contains(columnName) || dr.IsNull(columnName))
            {
                return string.Empty;
            }			

            return dr[columnName].ToString();
        }

        public static int InferInt(this DataRow dr, string columnName)
        {

            if (!dr.Table.Columns.Contains(columnName) || dr.IsNull(columnName))
            {
                return 0;
            }

            return Convert.ToInt32(dr[columnName]);
        }

        public static decimal InferDecimal(this DataRow dr, string columnName)
        {
            if (!dr.Table.Columns.Contains(columnName) || dr.IsNull(columnName))
            {
                return 0;
            }

            return Convert.ToDecimal(dr[columnName]);
        }

        public static bool InferBool(this DataRow dr, string columnName)
        {
            if (!dr.Table.Columns.Contains(columnName) || dr.IsNull(columnName))
            {
                return false;
            }

            return Convert.ToBoolean(dr[columnName]);
        }


        public static decimal? InferDecimal(this DataRow dr, string columnName, decimal? defaultValue)
        {
            if (!dr.Table.Columns.Contains(columnName) || dr.IsNull(columnName))
            {
                return defaultValue;
            }

            return Convert.ToDecimal(dr[columnName]);
        }

        public static int? InferInt(this DataRow dr, string columnName, int? defaultValue)
        {

            if (!dr.Table.Columns.Contains(columnName) || dr.IsNull(columnName))
            {
                return defaultValue;
            }

            return Convert.ToInt32(dr[columnName]);
        }

        public static DateTime InferDate(this DataRow dr, string columnName)
        {
            if (!dr.Table.Columns.Contains(columnName) || dr.IsNull(columnName))
            {
                return DateTime.Today;
            }

            return Convert.ToDateTime(dr[columnName]);
		}

		#region SqaDataReader

		public static string InferString(this SqlDataReader dr, string columnName)
		{
			bool exists = dr.IsColumnExist(columnName);

			if (exists)
			{
				int ordinal = dr.GetOrdinal(columnName);

				if (!dr.IsDBNull(ordinal))
				{
					return dr[columnName].ToString();	
				}				
			}

			return string.Empty;
		}

		public static int InferInt(this SqlDataReader dr, string columnName)
		{
			bool exists = dr.IsColumnExist(columnName);

			if (exists)
			{
				int ordinal = dr.GetOrdinal(columnName);

				if (!dr.IsDBNull(ordinal))
				{
					return Convert.ToInt32(dr[columnName]);
				}
			}

			return 0;
		}

		public static decimal InferDecimal(this SqlDataReader dr, string columnName)
		{
			bool exists = dr.IsColumnExist(columnName);

			if (exists)
			{
				int ordinal = dr.GetOrdinal(columnName);

				if (!dr.IsDBNull(ordinal))
				{
					return Convert.ToDecimal(dr[columnName]);
				}
			}

			return 0;
		}

		public static bool InferBool(this SqlDataReader dr, string columnName)
		{
			bool exists = dr.IsColumnExist(columnName);

			if (exists)
			{
				int ordinal = dr.GetOrdinal(columnName);

				if (!dr.IsDBNull(ordinal))
				{
					return Convert.ToBoolean(dr[columnName]);
				}
			}

			return false;
		}


		public static decimal? InferDecimal(this SqlDataReader dr, string columnName, decimal? defaultValue)
		{
			bool exists = dr.IsColumnExist(columnName);

			if (exists)
			{
				int ordinal = dr.GetOrdinal(columnName);

				if (!dr.IsDBNull(ordinal))
				{
					return Convert.ToDecimal(dr[columnName]);
				}
			}

			return defaultValue;
		}

	    public static int? InferInt(this SqlDataReader dr, string columnName, int? defaultValue)
	    {
			bool exists = dr.IsColumnExist(columnName);

			if (exists)
			{
				int ordinal = dr.GetOrdinal(columnName);

				if (!dr.IsDBNull(ordinal))
				{
					return Convert.ToInt32(dr[columnName]);
				}
			}

			return defaultValue;
	    }

	    public static DateTime InferDate(this SqlDataReader dr, string columnName)
		{
			bool exists = dr.IsColumnExist(columnName);

			if (exists)
			{
				int ordinal = dr.GetOrdinal(columnName);

				if (!dr.IsDBNull(ordinal))
				{
					return Convert.ToDateTime(dr[columnName]);
				}
			}

		    return DateTime.MinValue;
		}

	    public static bool IsColumnExist(this SqlDataReader dr, string columnName)
	    {
		    return dr.GetSchemaTable().Rows.OfType<DataRow>().Any(x => x["ColumnName"].ToString() == columnName);
	    }

	    #endregion
	}
}