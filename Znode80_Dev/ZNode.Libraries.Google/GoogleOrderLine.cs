using System.Xml;

namespace ZNode.Libraries.Google.GoogleCallbackServices
{
    #region Order LineItem
    /// <summary>
    /// This class creates an object for an individual item identified in a 
    /// &lt;merchant-calculation-callback&gt; request. Your class that inherits 
    /// from CallbackRules.cs can access these objects to locate information 
    /// about each item in an order. See the GetTaxResult method in the 
    /// ExampleRules.cs file for an example of how your application might access 
    /// these objects.
    /// </summary>
    public class OrderLine
    {
        #region Private Variables
        private string _ItemName;
        private string _ItemDescription;
        private int _Quantity;
        private decimal _UnitPrice;
        private string _TaxTableSelector;
        private XmlNode[] _MerchantPrivateItemDataNodes = new XmlNode[] { }; 
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the OrderLine class
        /// Contains information about an individual item listed in the customer's shopping cart
        /// </summary>
        /// <param name="ItemName">Identifies the name of an item</param>
        /// <param name="ItemDescription">Contains a description of an item</param>
        /// <param name="Quantity">Identifies how many units of a particular item
        ///  are included in the customer's shopping cart.</param>
        /// <param name="UnitPrice">Identifies the cost of the item. 
        /// This tag has one required attribute, which identifies the
        /// currency of the price.</param>
        /// <param name="TaxTableSelector">identifies an alternate tax table that 
        /// should be used to calculate tax for a particular item. 
        /// The value of the &lt;tax-table-selector&gt; tag should correspond to the 
        /// value of the name attribute of an alternate-tax-table</param>
        /// <param name="MerchantPrivateItemData">Merchant Private item data</param>
        public OrderLine(string ItemName, string ItemDescription, int Quantity, decimal UnitPrice, string TaxTableSelector, string MerchantPrivateItemData)
        {
            this._ItemName = ItemName;
            this._ItemDescription = ItemDescription;
            this._Quantity = Quantity;
            this._UnitPrice = UnitPrice;
            this._TaxTableSelector = TaxTableSelector;

            if (this.MerchantPrivateItemData != null)
            {
                this._MerchantPrivateItemDataNodes = new System.Xml.XmlNode[] { };
            }
        } 
   
        /// <summary>
        /// Initializes a new instance of the OrderLine class.
        /// Contains information about an individual item
        /// listed in the customer's shopping cart
        /// </summary>
        /// <param name="ItemName">Identifies the name of an item</param>
        /// <param name="ItemDescription">Contains a description of an item</param>
        /// <param name="Quantity">Identifies how many units of a particular item
        ///  are included in the customer's shopping cart.</param>
        /// <param name="UnitPrice">Identifies the cost of the item. 
        /// This tag has one required attribute, which identifies the
        /// currency of the price.</param>
        /// <param name="TaxTableSelector">identifies an alternate tax table that 
        /// should be used to calculate tax for a particular item. 
        /// The value of the &lt;tax-table-selector&gt; tag should correspond to the 
        /// value of the name attribute of an alternate-tax-table</param>
        /// <param name="MerchantPrivateItemDataNodes">tag contains any well-formed 
        /// XML sequence that should accompany an individual item in an order. 
        /// Google Checkout will return this XML in the 
        /// &lt;merchant-calculation-callback&gt;
        /// and the &lt;new-order-notification&gt; for the order.</param>
        public OrderLine(string ItemName, string ItemDescription, int Quantity, decimal UnitPrice, string TaxTableSelector, XmlNode[] MerchantPrivateItemDataNodes)
        {
            this._ItemName = ItemName;
            this._ItemDescription = ItemDescription;
            this._Quantity = Quantity;
            this._UnitPrice = UnitPrice;
            this._TaxTableSelector = TaxTableSelector;

            if (MerchantPrivateItemDataNodes != null)
            {
                this._MerchantPrivateItemDataNodes = MerchantPrivateItemDataNodes;
            }
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the the name of an item
        /// </summary>
        public string ItemName
        {
            get { return this._ItemName; }
        }

        /// <summary>
        /// Gets a description of an item
        /// </summary>
        public string ItemDescription
        {
            get { return this._ItemDescription; }
        }

        /// <summary>
        /// Gets how many units of a particular item are included in the customer's shopping cart
        /// </summary>
        public int Quantity
        {
            get { return this._Quantity; }
        }

        /// <summary>
        /// Gets the cost of the item. 
        /// This tag has one required attribute, which identifies the
        /// currency of the price
        /// </summary>
        public decimal UnitPrice
        {
            get { return this._UnitPrice; }
        }

        /// <summary>
        /// Gets an alternate tax table that should be used to calculate tax for a particular item. 
        /// The value of the &lt;tax-table-selector&gt; tag should correspond to the 
        /// value of the name attribute of an alternate-tax-table
        /// </summary>
        public string TaxTableSelector
        {
            get { return this._TaxTableSelector; }
        }

        /// <summary>
        /// Gets for the Merchant Private Item Data that was set using the string method
        /// </summary>
        public string MerchantPrivateItemData
        {
            get
            {
                if (this._MerchantPrivateItemDataNodes != null && this._MerchantPrivateItemDataNodes.Length > 0)
                {
                    string xmlnodeData = string.Empty;

                    // what we must do is look for an xml node by the name of 
                    // MERCHANT_DATA_HIDDEN
                    foreach (XmlNode node in this._MerchantPrivateItemDataNodes)
                    {
                        if (node.Name == "MERCHANT_DATA_HIDDEN")
                        {
                            return node.InnerXml;
                        }

                        xmlnodeData += node.OuterXml;
                    }

                    // if we get this far and we have one node, just return it
                    if (this._MerchantPrivateItemDataNodes.Length == 1)
                    {
                        return this._MerchantPrivateItemDataNodes[0].OuterXml;
                    }

                    return xmlnodeData;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets an Array of <see cref="System.Xml.XmlNode" />
        /// for the Merchant Private Data
        /// </summary>
        public System.Xml.XmlNode[] MerchantPrivateDataNodes
        {
            get
            {
                if (this._MerchantPrivateItemDataNodes != null)
                {
                    return this._MerchantPrivateItemDataNodes;
                }
                else
                {
                    return new XmlNode[] { };
                }
            }
        }
        #endregion
    }
    #endregion
}
