using System;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.Google.GoogleCheckoutServices;

namespace ZNode.Libraries.Google
{
    /// <summary>
    /// Google Checkout Gateway
    /// </summary>
    public class GoogleCheckout : ZNodeBusinessBase
    {
        #region GatewayEnvironment Enum Object
        /// <summary>
        /// Determine where the messages are posted (Sandbox, Production)
        /// </summary>
        private enum EnvironmentType
        {
            /// <summary>Use the Sandbox account to post the messages</summary>
            Sandbox = 0,

            /// <summary>Use the Production account to post the messages</summary>
            Production = 1,

            /// <summary>Unknown account.</summary>
            Unknown = 2
        }
        #endregion

        #region Protected Member Variables
        /// <summary>EnvironmentType used to determine where the messages are posted (Sandbox, Production)</summary>
        private EnvironmentType _Environment = EnvironmentType.Unknown;
        private ZNodeGenericCollection<ZNodeShoppingCartItem> _CartItems = null;
        private ZNodeShoppingCart _shoppingCart = null;
        private string _ECRedirectUrl = string.Empty;
        private string _ECCancelUrl = string.Empty;
        private PaymentSetting _PaymentSetting = new PaymentSetting();
        private string CurrencyCode = ZNodeCurrencyManager.CurrencyCode();
        private ZNodeUserAccount userAccount = null;
        private CarrierCalculatedShippingOption[] _CarrierShippingOptions = null;
        #endregion

        #region Public Contructors
        /// <summary>
        /// Initializes a new instance of the GoogleCheckout class.
        /// </summary>
        /// <param name="ECRedirectUrl">Redirect URL</param>
        /// <param name="ECReturnUrl">Return URL</param>
        public GoogleCheckout(string ECRedirectUrl, string ECReturnUrl)
        {
            this._shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
            this._CartItems = this._shoppingCart.ShoppingCartItems;
            this.userAccount = ZNodeUserAccount.CurrentAccount();

            this._ECRedirectUrl = ECRedirectUrl;
            this._ECCancelUrl = ECReturnUrl;
        }
        #endregion

        #region Public properties
        /// <summary>
        /// Gets or sets the payment settings
        /// </summary>
        public PaymentSetting PaymentSetting
        {
            get { return this._PaymentSetting; }
            set { this._PaymentSetting = value; }
        }

        /// <summary>
        /// Gets or sets the cartitems
        /// </summary>
        protected ZNodeGenericCollection<ZNodeShoppingCartItem> CartItems
        {
            get { return this._CartItems; }
            set { this._CartItems = value; }
        }

        /// <summary>
        /// Gets or sets the shopping cart
        /// </summary>
        protected ZNodeShoppingCart ShoppingCart
        {
            get { return this._shoppingCart; }
            set { this._shoppingCart = value; }
        }

        /// <summary>
        /// Gets or sets the cancel url
        /// </summary>
        protected string ECCancelUrl
        {
            get { return this._ECCancelUrl; }
            set { this._ECCancelUrl = value; }
        }

        /// <summary>
        /// Gets or sets the redirect url
        /// </summary>
        protected string ECRedirectUrl
        {
            get { return this._ECRedirectUrl; }
            set { this._ECRedirectUrl = value; }
        }
        #endregion

        #region Private properties
        /// <summary>
        /// Gets the Default Profile
        /// </summary>
        private static Profile DefaultRegisteredProfile
        {
            get
            {
                Profile profile = null;
                bool check = false;

                if (profile != null)
                {
                    if (profile.ProfileID == ZNodeConfigManager.SiteConfig.DefaultRegisteredProfileID.GetValueOrDefault())
                    {
                        check = true;
                    }
                }

                if (!check)
                {
                    // Get Portal by Domain Config
                    ProfileService ProfileService = new ProfileService();
                    Domain currentDomain = ZNodeConfigManager.DomainConfig;

                    // Get DefaultRegistered Profile
                    if (currentDomain != null)
                    {
                        PortalService portalService = new PortalService();
                        Portal currentPortal = portalService.GetByPortalID(currentDomain.PortalID);
                        profile = ProfileService.GetByProfileID(currentPortal.DefaultRegisteredProfileID.GetValueOrDefault());
                    }
                }

                return profile;
            }
        }

        /// <summary>
        /// Gets the Shipping options
        /// </summary>
        private CarrierCalculatedShippingOption[] ShippingOptions
        {
            get
            {
                this._CarrierShippingOptions = null;

                int profileID = 0;

                if (this.userAccount != null)
                {
                    profileID = this.userAccount.ProfileID;
                }
                else
                {
                    Profile profile = new Profile();
                    ProfileService profileService = new ProfileService();
                    profile = DefaultRegisteredProfile;

                    profileID = profile.ProfileID;
                }

                ShippingService shipServ = new ShippingService();
                TList<Shipping> shippingList = shipServ.GetAll();

                shippingList.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.Shipping shipping) { return shipping.ActiveInd == true && (shipping.ProfileID == null || shipping.ProfileID == profileID) && (shipping.ShippingTypeID == 2 || shipping.ShippingTypeID == 3); });
                shippingList.Sort("DisplayOrder Asc");

                if (shippingList.Count == 0)
                {
                    return this._CarrierShippingOptions;
                }

                this._CarrierShippingOptions = new CarrierCalculatedShippingOption[shippingList.Count];

                ShippingServiceCodeService shippingcodeService = new ShippingServiceCodeService();
                TList<ShippingServiceCode> serviceCodeList = shippingcodeService.GetAll();

                int i = 0;
                foreach (Shipping _shipping in shippingList)
                {
                    if (
                        !this.ShoppingCart.PortalCarts.Exists(
                            x => x.AddressCarts.Exists(y => y.Shipping.ShippingID == _shipping.ShippingID)))
                        continue;
                    // Shipping Settings
                    CarrierCalculatedShippingOption shippingOption = new CarrierCalculatedShippingOption();

                    // Currently Google Checkout only supports US currency
                    shippingOption.price = new CarrierCalculatedShippingOptionPrice();
                    shippingOption.price.currency = this.CurrencyCode;

                    // hard coded carrier pick up tag and valid values for this tag are REGULAR_PICKUP, SPECIAL_PICKUP and DROP_OFF
                    shippingOption.carrierpickup = "REGULAR_PICKUP";
                    shippingOption.additionalfixedcharge = new Money();
                    shippingOption.additionalfixedcharge.currency = this.CurrencyCode;
                    shippingOption.additionalfixedcharge.Value = _shipping.HandlingCharge;
                    if (_shipping.ShippingTypeID == 3)
                    {
                        shippingOption.shippingcompany = "UPS";
                        foreach (ShippingServiceCode code in serviceCodeList)
                        {
                            if (code.Code == _shipping.ShippingCode)
                            {
                                shippingOption.shippingtype = code.Description.Replace("UPS - ", string.Empty);
                                break;
                            }
                        }
                    }
                    else if (_shipping.ShippingTypeID == 2)
                    {
                        // FedEx carrier shipping settings
                        shippingOption.shippingcompany = "FedEx";

                        if (_shipping.ShippingCode == "FEDEX_2_DAY")
                        {
                            shippingOption.shippingtype = "2Day";
                        }
                        else
                        {
                            string shippingCode = _shipping.ShippingCode.Replace("_", " ");
                            shippingCode = shippingCode.Replace("FEDEX", string.Empty);
                            shippingOption.shippingtype = shippingCode.Trim();
                        }
                    }
                    else
                    {
                        shippingOption.shippingcompany = _shipping.Description;
                        shippingOption.shippingtype = _shipping.ShippingCode;
                    }

                    shippingOption.price.Value = this._shoppingCart.Total;
                    this._CarrierShippingOptions[i++] = shippingOption;
                }

                return this._CarrierShippingOptions;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Send the Message to Google Checkout
        /// </summary>
        /// <returns>Returns the google checkout response</returns>
        public GoogleCheckoutResponse SendRequestToGoogle()
        {
            // Local Variables
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            ZNode.Libraries.Google.GoogleCheckoutServices.CheckoutShoppingCart MyCart = new ZNode.Libraries.Google.GoogleCheckoutServices.CheckoutShoppingCart();

            // Get discount amount
            decimal orderDiscount = Math.Round(this._shoppingCart.Discount, 2);
            decimal taxAmount = Math.Round(this._shoppingCart.OrderLevelTaxes, 2);
            decimal giftCardAmount = Math.Round(this._shoppingCart.GiftCardAmount, 2);

            int itemsCount = this._CartItems.Count;
            System.Guid uniqueID = System.Guid.NewGuid();

            // Adding Shopping cart Items to the request and Initialize Shopping cart object
            MyCart.shoppingcart = new ZNode.Libraries.Google.GoogleCheckoutServices.ShoppingCart();

            if (orderDiscount > 0)
            {
                itemsCount += 1;
            }

            if (taxAmount > 0)
            {
                itemsCount += 1;
            }

            if (giftCardAmount > 0)
            {
                itemsCount += 1;
            }

            // Add the items in the shopping cart to the API request.
            MyCart.shoppingcart.items = new ZNode.Libraries.Google.GoogleCheckoutServices.Item[itemsCount];

            int itemIndex = 0;
            ZNode.Libraries.Google.GoogleCheckoutServices.anyMultiple emptyItemData = new anyMultiple();

            // Loop through the shopping cart item
            foreach (ZNodeShoppingCartItem cartItem in this._CartItems)
            {
                MyCart.shoppingcart.items[itemIndex] = new ZNode.Libraries.Google.GoogleCheckoutServices.Item();
                string description = cartItem.Product.ShoppingCartDescription;
                MyCart.shoppingcart.items[itemIndex].itemname = this.EscapeXmlChars(cartItem.Product.Name);
                MyCart.shoppingcart.items[itemIndex].itemdescription = this.EscapeXmlChars(description.Replace("<br />", string.Empty));
                MyCart.shoppingcart.items[itemIndex].quantity = cartItem.Quantity;
                MyCart.shoppingcart.items[itemIndex].unitprice = new ZNode.Libraries.Google.GoogleCheckoutServices.Money();
                MyCart.shoppingcart.items[itemIndex].unitprice.currency = this.CurrencyCode;
                MyCart.shoppingcart.items[itemIndex].unitprice.Value = cartItem.UnitPrice;
                MyCart.shoppingcart.items[itemIndex].itemweight = new ItemWeight();
                MyCart.shoppingcart.items[itemIndex].itemweight.unit = ZNodeConfigManager.SiteConfig.WeightUnit.Trim(new char[] { 'S' });
                MyCart.shoppingcart.items[itemIndex].itemweight.value = double.Parse(cartItem.Product.Weight.ToString());
                
               
                if (cartItem.Product.RecurringBillingInd)
                {
                    MyCart.shoppingcart.items[itemIndex].subscription = new ZNode.Libraries.Google.GoogleCheckoutServices.Subscription();

                    MyCart.shoppingcart.items[itemIndex].subscription.period = GetRecurringDatePeriod(cartItem.Product.RecurringBillingPeriod);
                    MyCart.shoppingcart.items[itemIndex].subscription.startdate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                    MyCart.shoppingcart.items[itemIndex].subscription.recurrentitem = new ZNode.Libraries.Google.GoogleCheckoutServices.Item();
                    MyCart.shoppingcart.items[itemIndex].subscription.recurrentitem.itemname = cartItem.Product.Name.ToString();
                    MyCart.shoppingcart.items[itemIndex].subscription.recurrentitem.itemdescription = cartItem.Product.Description.ToString();
                    MyCart.shoppingcart.items[itemIndex].subscription.recurrentitem.quantity = cartItem.Quantity;
                    MyCart.shoppingcart.items[itemIndex].subscription.recurrentitem.unitprice = new ZNode.Libraries.Google.GoogleCheckoutServices.Money();
                    MyCart.shoppingcart.items[itemIndex].subscription.recurrentitem.unitprice.currency = this.CurrencyCode;
                    MyCart.shoppingcart.items[itemIndex].subscription.recurrentitem.unitprice.Value = cartItem.UnitPrice;
                    MyCart.shoppingcart.items[itemIndex].subscription.type = "google";
                }

            

                decimal productPrice = cartItem.UnitPrice;

                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                    {
                        if (addOnValue.RecurringBillingInd)
                        {
                            productPrice = productPrice +  addOnValue.RecurringBillingInitialAmount * cartItem.Quantity;
                        }
                    }
                }
                if (cartItem.Product.RecurringBillingInd )
                {
                    MyCart.shoppingcart.items[itemIndex].subscription.payments = new SubscriptionPayment[1];
                    MyCart.shoppingcart.items[itemIndex].subscription.payments[0] =
                        new ZNode.Libraries.Google.GoogleCheckoutServices.SubscriptionPayment();
                    MyCart.shoppingcart.items[itemIndex].subscription.payments[0].maximumcharge =
                        new ZNode.Libraries.Google.GoogleCheckoutServices.Money();
                    MyCart.shoppingcart.items[itemIndex].subscription.payments[0].maximumcharge.Value = productPrice;
                    MyCart.shoppingcart.items[itemIndex].subscription.payments[0].maximumcharge.currency =
                        this.CurrencyCode;
                    MyCart.shoppingcart.items[itemIndex].subscription.payments[0].times =
                        cartItem.Product.RecurringBillingTotalCycles;
                }
                emptyItemData.Any = new XmlNode[] 
                {
                    this.MakeNode(doc, "ShippingRuleTypeID", cartItem.Product.ShippingRuleTypeID.GetValueOrDefault(-1).ToString()),
                    this.MakeNode(doc, "Weight", cartItem.Product.Weight.ToString("N2")),
                    this.MakeNode(doc, "Length", cartItem.Product.Length.ToString("N2")),
                    this.MakeNode(doc, "Width", cartItem.Product.Width.ToString("N2")),
                    this.MakeNode(doc, "Height", cartItem.Product.Height.ToString("N2")),
                    this.MakeNode(doc, "TaxExempt", cartItem.Product.TaxClassID.ToString())
                };

                MyCart.shoppingcart.items[itemIndex].merchantprivateitemdata = emptyItemData;

                itemIndex++;
            }

            // Reduce the item index.
            itemIndex--;

            // Set discount info as line item
            if (orderDiscount > 0)
            {
                itemIndex++;
                MyCart.shoppingcart.items[itemIndex] = new ZNode.Libraries.Google.GoogleCheckoutServices.Item();
                MyCart.shoppingcart.items[itemIndex].itemname = "Discount";
                MyCart.shoppingcart.items[itemIndex].itemdescription = string.Empty;
                MyCart.shoppingcart.items[itemIndex].quantity = 1;
                MyCart.shoppingcart.items[itemIndex].unitprice = new ZNode.Libraries.Google.GoogleCheckoutServices.Money();
                MyCart.shoppingcart.items[itemIndex].unitprice.currency = this.CurrencyCode;
                MyCart.shoppingcart.items[itemIndex].unitprice.Value = -orderDiscount;
                MyCart.shoppingcart.items[itemIndex].itemweight = new ItemWeight();
                MyCart.shoppingcart.items[itemIndex].itemweight.unit = ZNodeConfigManager.SiteConfig.WeightUnit.Trim(new char[] { 'S' });
                MyCart.shoppingcart.items[itemIndex].itemweight.value = 0;
                emptyItemData = new anyMultiple();
                MyCart.shoppingcart.items[itemIndex].merchantprivateitemdata = emptyItemData;
            }

            // Set discount info as line item
            if (giftCardAmount > 0)
            {
                itemIndex++;
                MyCart.shoppingcart.items[itemIndex] = new ZNode.Libraries.Google.GoogleCheckoutServices.Item();
                MyCart.shoppingcart.items[itemIndex].itemname = "Gift Card";
                MyCart.shoppingcart.items[itemIndex].itemdescription = string.Empty;
                MyCart.shoppingcart.items[itemIndex].quantity = 1;
                MyCart.shoppingcart.items[itemIndex].unitprice = new ZNode.Libraries.Google.GoogleCheckoutServices.Money();
                MyCart.shoppingcart.items[itemIndex].unitprice.currency = this.CurrencyCode;
                MyCart.shoppingcart.items[itemIndex].unitprice.Value = -giftCardAmount;
                MyCart.shoppingcart.items[itemIndex].itemweight = new ItemWeight();
                MyCart.shoppingcart.items[itemIndex].itemweight.unit = ZNodeConfigManager.SiteConfig.WeightUnit.Trim(new char[] { 'S' });
                MyCart.shoppingcart.items[itemIndex].itemweight.value = 0;
                emptyItemData = new anyMultiple();
                MyCart.shoppingcart.items[itemIndex].merchantprivateitemdata = emptyItemData;
            }

            // General Settings, Check for Test mode 
            if (PaymentSetting.TestMode)
            {
                this._Environment = EnvironmentType.Sandbox; // Testing Server
            }
            else
            {
                this._Environment = EnvironmentType.Production; // Production Server
            }

            // Add the &lt;continue-shopping-url&gt; element to the API request.
            MyCart.checkoutflowsupport = new ZNode.Libraries.Google.GoogleCheckoutServices.CheckoutShoppingCartCheckoutflowsupport();

            ZNode.Libraries.Google.GoogleCheckoutServices.MerchantCheckoutFlowSupport support = new ZNode.Libraries.Google.GoogleCheckoutServices.MerchantCheckoutFlowSupport();

            support.continueshoppingurl = this._ECRedirectUrl; // Return URL  
            support.editcarturl = this._ECCancelUrl; // Cancel URL or Edit Cart URL

            // merchant private data
            System.Xml.XmlNode tempNode = this.MakeNode(doc, "ProfileID", string.Empty);

            tempNode.InnerText = uniqueID.ToString();

            emptyItemData = new anyMultiple();
            emptyItemData.Any = new XmlNode[] { tempNode };
            MyCart.shoppingcart.merchantprivatedata = emptyItemData;

            // Shipping Methods
            ZNode.Libraries.Google.GoogleCheckoutServices.MerchantCheckoutFlowSupportShippingmethods Shippingmethods = new ZNode.Libraries.Google.GoogleCheckoutServices.MerchantCheckoutFlowSupportShippingmethods();

            // Shipping Settings
            if (this.ShippingOptions != null)
            {
                ZNode.Libraries.Google.GoogleCheckoutServices.CarrierCalculatedShipping shipping = new CarrierCalculatedShipping();

                shipping.carriercalculatedshippingoptions = this._CarrierShippingOptions;

                string dimensionUnit = ZNodeConfigManager.SiteConfig.DimensionUnit;
                ShippingPackage package = new ShippingPackage();

                decimal height = 1;
                decimal width = 1;
                decimal length = 1;

                // Get Package Properties
                this.GetPackageProperties(this._shoppingCart.ShoppingCartItems, out height, out length, out width);

                package.height = new Dimension();
                package.height.unit = dimensionUnit;
                package.height.value = double.Parse(height.ToString());
                package.length = new Dimension();
                package.length.unit = dimensionUnit;
                package.length.value = double.Parse(length.ToString());

                package.width = new Dimension();
                package.width.unit = dimensionUnit;
                package.width.value = double.Parse(width.ToString());

                package.shipfrom = new AnonymousAddress();
                package.shipfrom.id = "Package";
                package.shipfrom.countrycode = ZNodeConfigManager.SiteConfig.ShippingOriginCountryCode;
                package.shipfrom.postalcode = ZNodeConfigManager.SiteConfig.ShippingOriginZipCode;
                package.shipfrom.region = ZNodeConfigManager.SiteConfig.ShippingOriginStateCode;
                package.shipfrom.city = string.Empty;

                shipping.shippingpackages = new ShippingPackage[] { package };

                if (shipping.carriercalculatedshippingoptions != null)
                {
                    Shippingmethods.Items = new object[] { shipping };
                }
                else if (this.CustomShippingOptions() != null)
                {
                    // Add Shipping Methods
                    Shippingmethods.Items = new object[] { this.CustomShippingOptions() };
                }

                support.shippingmethods = Shippingmethods;
            }

            // Set Tax info as line item
            if (taxAmount > 0)
            {
                itemIndex++;

                MyCart.shoppingcart.items[itemIndex] = new ZNode.Libraries.Google.GoogleCheckoutServices.Item();
                MyCart.shoppingcart.items[itemIndex].itemname = "TAX";
                MyCart.shoppingcart.items[itemIndex].itemdescription = string.Empty;
                MyCart.shoppingcart.items[itemIndex].quantity = 1;
                MyCart.shoppingcart.items[itemIndex].unitprice = new ZNode.Libraries.Google.GoogleCheckoutServices.Money();
                MyCart.shoppingcart.items[itemIndex].unitprice.currency = this.CurrencyCode;
                MyCart.shoppingcart.items[itemIndex].unitprice.Value = taxAmount;
                MyCart.shoppingcart.items[itemIndex].itemweight = new ItemWeight();
                MyCart.shoppingcart.items[itemIndex].itemweight.unit = ZNodeConfigManager.SiteConfig.WeightUnit.Trim(new char[] { 'S' });
                MyCart.shoppingcart.items[itemIndex].itemweight.value = 0;
                
                emptyItemData = new anyMultiple();
                MyCart.shoppingcart.items[itemIndex].merchantprivateitemdata = emptyItemData;
            }

            // Send Request to Google Server
            // Add checkout flow support type
            MyCart.checkoutflowsupport.Item = support;

            // Get Xml data
            byte[] Data = MessageHelper.Serialize(MyCart);

            ZNodeLoggingBase.LogMessage(MessageHelper.Utf8BytesToString(Data));
            
            ZNodeEncryption decrypt = new ZNodeEncryption();

            string loginId = decrypt.DecryptData(PaymentSetting.GatewayUsername);
            string password = decrypt.DecryptData(PaymentSetting.GatewayPassword);

            // Prepare web request.
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(this.GetPostUrl(loginId));
            myRequest.Method = "POST";
            myRequest.ContentLength = Data.Length;
            myRequest.Headers.Add("Authorization", string.Format("Basic {0}", this.GetAuthorization(loginId, password)));
            myRequest.ContentType = "application/xml; charset=UTF-8";
            myRequest.Accept = "application/xml; charset=UTF-8";
            myRequest.KeepAlive = false;

            // Send the data.
            using (Stream requestStream = myRequest.GetRequestStream())
            {
                requestStream.Write(Data, 0, Data.Length);
            }

            // Receive the response
            // Read the response.
            string responseXml = string.Empty;

            try
            {
                using (HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse())
                {
                    using (Stream ResponseStream = myResponse.GetResponseStream())
                    {
                        using (StreamReader ResponseStreamReader = new StreamReader(ResponseStream))
                        {
                            responseXml = ResponseStreamReader.ReadToEnd();
                        }
                    }
                }
            }
            catch (WebException WebExcp)
            {
                if (WebExcp.Response != null)
                {
                    using (HttpWebResponse HttpWResponse =
                      (HttpWebResponse)WebExcp.Response)
                    {
                        using (StreamReader sr = new StreamReader(HttpWResponse.GetResponseStream()))
                        {
                            responseXml = sr.ReadToEnd();
                        }
                    }
                }
            }

            // Parse the output string and return the response code & text
            GoogleCheckoutResponse gcr = this.ParseXML(responseXml);

            if (gcr.ResponseCode == "0")
            {
                gcr.ResponseText = uniqueID.ToString();
            }

            return gcr;
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Get the Recurring Billing DatePeriod
        /// </summary>
        private DatePeriod GetRecurringDatePeriod(string dtPeriod)
        {
            DatePeriod RecurringPeriod = new DatePeriod();

            if (dtPeriod.Equals("DAY", StringComparison.OrdinalIgnoreCase))
            {
                RecurringPeriod = DatePeriod.DAILY;
            }
            else if (dtPeriod.Equals("WEEK", StringComparison.OrdinalIgnoreCase))
            {
                RecurringPeriod = DatePeriod.WEEKLY;
            }
            else if (dtPeriod.Equals("MONTH", StringComparison.OrdinalIgnoreCase))
            {
                RecurringPeriod = DatePeriod.MONTHLY;
            }
            else if (dtPeriod.Equals("YEAR", StringComparison.OrdinalIgnoreCase))
            {
                RecurringPeriod = DatePeriod.YEARLY;
            }

            return RecurringPeriod;
        }

        /// <summary>
        /// Replaces the symbols with the hex codes
        /// </summary>
        /// <param name="doc">XML Document</param>
        /// <param name="tagName">Name of the tag</param>
        /// <param name="tagValue">Value of the tag</param>
        /// <returns>Returns the XML Node</returns>
        private XmlNode MakeNode(XmlDocument doc, string tagName, string tagValue)
        {
            System.Xml.XmlNode tempNode = doc.CreateElement(tagName);
            tempNode.InnerText = tagValue;

            return tempNode;
        }

        /// <summary>
        /// Replaces the symbols with the hex codes
        /// </summary>
        /// <param name="In">In values for the XML</param>
        /// <returns>Returns after escape XML characters</returns>
        private string EscapeXmlChars(string In)
        {
            string RetVal = In;
            RetVal = RetVal.Replace("&", "&#x26;");
            RetVal = RetVal.Replace("<", "&#x3c;");
            RetVal = RetVal.Replace(">", "&#x3e;");
            return RetVal;
        }

        /// <summary>
        /// Retreives the Merhcnat Id and Key to add this value to the Request Headers.
        /// </summary>
        /// <param name="User">Merchant Id</param>
        /// <param name="Password">Password for the user</param>
        /// <returns>Returns the Merchant ID and key for the username and password</returns>
        private string GetAuthorization(string User, string Password)
        {
            UTF8Encoding utf8encoder = new UTF8Encoding(false, true);
            return Convert.ToBase64String(utf8encoder.GetBytes(string.Format("{0}:{1}", User, Password)));
        }

        /// <summary>
        /// Returns the URL to post the input data for payment
        /// Checks for test mode and returns the URL to post data
        /// </summary>
        /// <param name="_MerchantID">Merchant ID</param>
        /// <returns>Returns the post URL</returns>
        private string GetPostUrl(string _MerchantID)
        {
            if (this._Environment == EnvironmentType.Sandbox)
            {
                return string.Format("https://sandbox.google.com/checkout/cws/v2/Merchant/{0}/request", _MerchantID);
            }
            else
            {
                return string.Format("https://checkout.google.com/cws/v2/Merchant/{0}/request", _MerchantID);
            }
        }

        /// <summary>
        /// Returns the XML returned from Google gateway
        /// </summary>
        /// <param name="ResponseXml">Response XML</param>
        /// <returns>Returns the google checkout response</returns>
        private GoogleCheckoutResponse ParseXML(string ResponseXml)
        {
            GoogleCheckoutResponse _response = new GoogleCheckoutResponse();
            bool parsed = false;

            try
            {
                if (ResponseXml.IndexOf("<checkout-redirect") > -1)
                {
                    ZNode.Libraries.Google.GoogleCheckoutServices.CheckoutRedirect _CheckoutRedirectResponse = (ZNode.Libraries.Google.GoogleCheckoutServices.CheckoutRedirect)MessageHelper.Deserialize(ResponseXml, typeof(ZNode.Libraries.Google.GoogleCheckoutServices.CheckoutRedirect));
                    _response.SerialNumber = _CheckoutRedirectResponse.serialnumber;
                    _response.ResponseCode = "0";
                    _response.ECRedirectURL = _CheckoutRedirectResponse.redirecturl;

                    parsed = true;
                }
                else if (ResponseXml.IndexOf("<request-received") > -1)
                {
                    ZNode.Libraries.Google.GoogleCheckoutServices.RequestReceivedResponse _requestReceivedResponse = (ZNode.Libraries.Google.GoogleCheckoutServices.RequestReceivedResponse)MessageHelper.Deserialize(ResponseXml, typeof(ZNode.Libraries.Google.GoogleCheckoutServices.RequestReceivedResponse));
                    _response.SerialNumber = _requestReceivedResponse.serialnumber;
                    _response.ResponseCode = "0";

                    parsed = true;
                }
                else if (ResponseXml.IndexOf("<error") > -1)
                {
                    ZNode.Libraries.Google.GoogleCheckoutServices.ErrorResponse _ErrorResponse = (ZNode.Libraries.Google.GoogleCheckoutServices.ErrorResponse)MessageHelper.Deserialize(ResponseXml, typeof(ZNode.Libraries.Google.GoogleCheckoutServices.ErrorResponse));
                    _response.ResponseCode = "-1";
                    _response.ResponseText = _ErrorResponse.errormessage + "<br>" + _ErrorResponse.warningmessages;
                    parsed = true;
                }
                else
                {
                    _response.ResponseCode = "-1";
                    _response.ResponseText = "Unable to connect with google";
                    parsed = true;
                }
            }
            catch
            {
                // let it continue
            }

            if (!parsed)
            {
            }

            return _response;
        }

        /// <summary>
        /// Sets package properties for the entire cart items or specified cart item
        /// </summary>
        /// <param name="ShoppingCartItems">Shopping Cart Items</param>
        /// <param name="Height">Height as output parameter</param>
        /// <param name="Length">length as output parameter</param>
        /// <param name="Width">Width as output parameter</param>
        private void GetPackageProperties(ZNodeGenericCollection<ZNodeShoppingCartItem> ShoppingCartItems, out decimal Height, out decimal Length, out decimal Width)
        {
            // We are going to get a very aproximate package dimension by taking the total volume of all items in the cart
            // and then getting the cube root of that volume. This will give us a minimum package size.
            // NOTE: This will under estimate the total package volume since packages will rarely be cubes!
            decimal totalVolume = 0;
            decimal totalWeight = 0;
            decimal totalValue = 0;

            bool roundweightToNextIncrement = false;
            bool useGrossValue = false;

            // Get each products dimensions,weight
            foreach (ZNodeShoppingCartItem cartItem in ShoppingCartItems)
            {
                // Get Product total weight.
                int quantity = cartItem.Quantity;
                decimal itemWeight = 0;

                if (!cartItem.Product.FreeShippingInd)
                {
                    // get dimensions
                    totalVolume += cartItem.Product.Height * cartItem.Product.Width * cartItem.Product.Length;

                    // check this property to round the weight up to the next pound (or kg). 
                    if (roundweightToNextIncrement)
                    {
                        // Round to the next highest pound.
                        itemWeight += Math.Round(cartItem.Product.Weight + 0.51m, 0);
                    }
                    else
                    {
                        // get product weight
                        itemWeight += cartItem.Product.Weight;
                    }

                    if (useGrossValue)
                    {
                        totalValue += cartItem.Product.FinalPrice * quantity; // get product value
                        // get product weight
                        itemWeight *= quantity;
                    }
                    else
                    {
                        totalValue += cartItem.Product.FinalPrice;
                    }

                    totalWeight += itemWeight;
                }

                // Get each add-ons dimentions
                foreach (ZNodeAddOn AddOn in cartItem.Product.SelectedAddOnItems.ZNodeAddOnCollection)
                {
                    foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                    {
                        itemWeight = 0;

                        if (!AddOnValue.FreeShippingInd)
                        {
                            totalVolume += AddOnValue.Height * AddOnValue.Width * AddOnValue.Length;

                            // check this property to round the weight up to the next pound (or kg). 
                            if (roundweightToNextIncrement)
                            {
                                // Round to the next highest pound.                                
                                itemWeight += Math.Round(AddOnValue.Weight + 0.51m, 0);
                            }
                            else
                            {
                                // get addon value weight
                                itemWeight += AddOnValue.Weight;
                            }

                            if (useGrossValue)
                            {
                                totalValue += AddOnValue.FinalPrice * quantity; // get product addons value
                                itemWeight *= quantity;
                            }
                            else
                            {
                                totalValue += AddOnValue.FinalPrice; // get product addons value
                            }
                        }

                        totalWeight += itemWeight;
                    }
                }
            }

            // Let's aproximate dimentions by taking the qube root of the total volume.
            decimal dimension = (decimal)Math.Pow(Convert.ToDouble(totalVolume), 1 / 3);

            Height = dimension;
            Length = dimension;
            Width = dimension;
        }

        /// <summary>
        /// Retrieves the calculated shipping
        /// </summary>
        /// <returns>Merchant Calculated shipping</returns>
        private MerchantCalculatedShipping[] CustomShippingOptions()
        {
            MerchantCalculatedShipping[] _MerchantShippingOptions = null;

            int profileID = 0;

            if (this.userAccount != null)
            {
                profileID = this.userAccount.ProfileID;
            }
            else
            {
                Profile profile = new Profile();
                ProfileService profileService = new ProfileService();
                profile = DefaultRegisteredProfile;

                profileID = profile.ProfileID;
            }

            ShippingService shipServ = new ShippingService();
            TList<Shipping> shippingList = shipServ.GetAll();

            shippingList.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.Shipping shipping) { return shipping.ActiveInd == true && (shipping.ProfileID == null || shipping.ProfileID == profileID) && !(shipping.ShippingTypeID == 2 || shipping.ShippingTypeID == 3); });
            shippingList.Sort("DisplayOrder Asc");

            if (shippingList.Count == 0)
            {
                return _MerchantShippingOptions;
            }

            _MerchantShippingOptions = new MerchantCalculatedShipping[shippingList.Count];

            ShippingServiceCodeService shippingcodeService = new ShippingServiceCodeService();
            TList<ShippingServiceCode> serviceCodeList = shippingcodeService.GetAll();

            int i = 0;
            foreach (Shipping _shipping in shippingList)
            {
                // Shipping Settings
                MerchantCalculatedShipping shippingOption = new MerchantCalculatedShipping();

                // Currently Google Checkout only supports US currency
                shippingOption.price = new MerchantCalculatedShippingPrice();
                shippingOption.price.currency = this.CurrencyCode;
                shippingOption.price.Value = _shipping.HandlingCharge;
                if (_shipping.ShippingRuleCollection.Count > 0)
                {
                    shippingOption.price.Value += _shipping.ShippingRuleCollection[0].BaseCost + (this._shoppingCart.ShoppingCartItems.Count * _shipping.ShippingRuleCollection[0].PerItemCost);
                }

                shippingOption.name = _shipping.ShippingCode;
                _MerchantShippingOptions[i++] = shippingOption;
            }

            return _MerchantShippingOptions;
        }

        #endregion
    }
}
