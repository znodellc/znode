using System.Collections;
using System.Xml;
using ZNode.Libraries.Google.GoogleCheckoutServices;

namespace ZNode.Libraries.Google.GoogleCallbackServices
{
    #region Order entity object
    /// <summary>
    /// This class contains methods that parse a 
    /// &lt;merchant-calculation-callback&gt; request and creates an object 
    /// identifying the items in the customer's shopping cart. Your class that 
    /// inherits from CallbackRules.cs will receive an object of this type to 
    /// identify the items in the customer's order.
    /// </summary>
    public class Order
    {
        #region Private Variables
        private ArrayList _OrderLines;
        private XmlNode[] _MerchantPrivateDataNodes = new XmlNode[] { }; 
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the Order class and Process a Merchant Calculation callback.
        /// </summary>
        /// <param name="Callback">The Callback message.</param>
        public Order(MerchantCalculationCallback Callback)
        {
            this._OrderLines = new ArrayList();
            for (int i = 0; i < Callback.shoppingcart.items.Length; i++)
            {
                Item ThisItem = Callback.shoppingcart.items[i];

                this._OrderLines.Add(new OrderLine(ThisItem.itemname, ThisItem.itemdescription, ThisItem.quantity, ThisItem.unitprice.Value, ThisItem.taxtableselector, ThisItem.merchantprivateitemdata.Any));
            }
        } 
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the sub total of all the items (Quantity * Price)
        /// </summary>
        public decimal ItemSubTotal
        {
            get
            {
                decimal retVal = 0;

                foreach (OrderLine Line in this)
                {
                    retVal += Line.UnitPrice * Line.Quantity;
                }

                return retVal;
            }
        }

        /// <summary>
        /// Gets the Merchant Private Data
        /// </summary>
        public string MerchantPrivateData
        {
            get
            {
                if (this._MerchantPrivateDataNodes != null && this._MerchantPrivateDataNodes.Length > 0)
                {
                    // what we must do is look for an xml node by the name of
                    // MERCHANT_DATA_HIDDEN
                    foreach (XmlNode node in this._MerchantPrivateDataNodes)
                    {
                        if (node.Name == "MERCHANT_DATA_HIDDEN")
                        {
                            return node.InnerXml;
                        }
                    }

                    // if we get this far and we have one node, just return it
                    if (this._MerchantPrivateDataNodes.Length == 1)
                    {
                        return this._MerchantPrivateDataNodes[0].OuterXml;
                    }
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets An Array of <see cref="System.Xml.XmlNode" /> 
        /// for the Merchant Private Data
        /// </summary>
        public System.Xml.XmlNode[] MerchantPrivateDataNodes
        {
            get { return this._MerchantPrivateDataNodes; }
        } 
        #endregion

        #region Public Methods
        /// <summary>
        /// Return an Iterator to process the Order Lines
        /// </summary>
        /// <returns>Returns enumerator</returns>
        public IEnumerator GetEnumerator()
        {
            return this._OrderLines.GetEnumerator();
        } 
        #endregion
    }
    #endregion
}
