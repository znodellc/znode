﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Znode.Engine.MvcAdmin.Controllers.Home;
using Znode.Plugin.Framework.Web.ViewEngine;

namespace Znode.Engine.MvcAdminNew
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_BeginRequest()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            Exception exception = null;
            try
            {
                exception = Server.GetLastError().GetBaseException();
                LogMessage(exception);
                RedirectToErrorPage(sender, exception);
            }
            catch (Exception ex)
            {
                LogMessage(ex);
                RedirectToErrorPage(sender, ex);
            }
        }

        private void LogMessage(Exception exception)
        {
            if (exception != null)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(exception);
            }
        }

        private void RedirectToErrorPage(Object sender, Exception exception)
        {
            var controller = new HomeController();
            var httpContext = ((MvcApplication)sender).Context;

            var routeData = new RouteData();
            httpContext.ClearError();
            httpContext.Response.Clear();

            SetErrorPage(exception, routeData);

            ((IController)controller).Execute(new RequestContext(new HttpContextWrapper(httpContext), routeData));
        }

        private void SetErrorPage(Exception exception, RouteData routeData)
        {
            routeData.Values.Add("controller", "home");
            routeData.Values.Add("action", "ErrorHandler");
            routeData.Values.Add("exception", exception);
        }
    }
}
