﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.Models
{
    public class ListViewModel
    {
        public int[] AssignedId { get; set; }
        public int[] UnAssignedId { get; set; }
        public IEnumerable<SelectListItem> AssignedList { get; set; }
        public IEnumerable<SelectListItem> UnAssignedList { get; set; }
    }
}