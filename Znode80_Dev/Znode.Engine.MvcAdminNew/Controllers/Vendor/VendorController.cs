﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Vendor
{
    /// <summary>
    /// This is the controller for the vendor dashboard
    /// </summary>
    public class VendorController : BaseController
    {
        
        #region Private Variables
        private readonly IVendorAccountAgent _vendorAccountAgent;
        #endregion

        #region Constructor
        public VendorController()
        {
            _vendorAccountAgent = new VendorAccountAgent();
        }
        #endregion

       
        /// <summary>
        /// This action result will redirects to the Vendor Dashboard page
        /// </summary>
        /// <returns>ActionRest view of Vendor Dashboard page</returns>
        public ActionResult VendorDashboard()
        {
            return View();
        }

        /// <summary>
        /// This action will delete the vender
        /// </summary>      
        /// <param name="venderId">To delete the vender</param>
        /// <returns>ActionResult</returns>
        public ActionResult DeleteVender(int venderId)
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult CreateVendor()
        {
            return View();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vendorAccountViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateVendor(VendorAccountViewModel vendorAccountViewModel)
        {
            return View();
        }


        [HttpGet]
        public ActionResult List()
        {
            VendorAccountListViewModel vendorAccount = _vendorAccountAgent.GetSupplier();
            if (vendorAccount.Equals(null))
            {
                return new EmptyResult();
            }
            return View("List", vendorAccount);
        }
       
    }
}