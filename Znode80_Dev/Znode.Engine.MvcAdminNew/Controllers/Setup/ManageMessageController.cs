﻿using System;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Controllers.SetUp
{
    public class ManageMessageController : Controller
    {
        #region Private Variables

        private readonly IMessageConfigAgent _messageConfigAgent;
        private readonly IPortalAgent _portalAgent;
        private int localeId = MvcAdminConstants.LocaleId;
        private string createEditView = MvcAdminConstants.CreateEditView;
        private string messageList = MvcAdminConstants.ListView;
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ManageMessageController()
        {
            _messageConfigAgent = new MessageConfigAgent();
            _portalAgent = new PortalAgent();
        }

        #endregion

        #region Action Methods

        /// <summary>
        /// Gets Message list
        /// </summary>
        /// <returns>Returns ActionResult</returns>
        public ActionResult List()
        {
            ManageMessageListViewModel list = _messageConfigAgent.GetMessageConfigs();
            return View(list);
        }

        /// <summary>
        /// Add New Message
        /// </summary>
        /// <param name="model">ManageMessageViewModel</param>
        /// <param name="PortalId">PortalId</param>
        /// <returns>Returns ActionResult</returns>
        public ActionResult Create()
        {
            var model = new ManageMessageViewModel() { Portal = _portalAgent.GetAllPortals().Portals.ToList() };
            return View(createEditView, model);
        }

        /// <summary>
        /// Adds new message to list
        /// </summary>
        /// <param name="model">ManageMessageViewModel</param>
        /// <returns>Returns ActionResult</returns>
        [HttpPost]
        public ActionResult Create(ManageMessageViewModel model, int PortalId)
        {
            if (ModelState.IsValid)
            {
                model.LocaleId = localeId;
                model.PortalId = PortalId;
                model.MessageTypeId = Convert.ToInt32(MessageType.MessageBlock);
                _messageConfigAgent.SaveMessageConfig(model);
                return RedirectToAction(messageList);
            }
            return View(createEditView, model);
        }

        /// <summary>
        /// Get Message to be edited on the basis of messageConfigId
        /// </summary>
        /// <param name="messageConfigId">messageConfigId</param>
        /// <param name="model">ManageMessageViewModel</param>
        /// <returns>Returns ActionResult</returns>
        [HttpGet]
        public ActionResult Edit(int messageConfigId)
        {
            ManageMessageViewModel model = new ManageMessageViewModel();
            if (!messageConfigId.Equals(null))
            {
                model = _messageConfigAgent.GetMessageConfig(messageConfigId);
                model.Portal = _portalAgent.GetPortals(null, null).Portals.ToList();
                return View(createEditView, model);
            }
            return View(createEditView);
        }

        /// <summary>
        /// Edit message and add edited message to the list
        /// </summary>
        /// <param name="model">ManageMessageViewModel</param>
        /// <param name="configid">configid</param>
        /// <param name="localeid">localeid</param>
        /// <param name="messageTypeId">messageTypeId</param>
        /// <returns>Returns List with edited message</returns>
        [HttpPost]
        public ActionResult Edit(ManageMessageViewModel model, int configid, int localeid, int messageTypeId, int EditportalId)
        {
            if (ModelState.IsValid)
            {
                model.MessageConfigId = configid;
                model.MessageTypeId = messageTypeId;
                model.PortalId = EditportalId;
                var status = _messageConfigAgent.UpdateMessageConfig(model.MessageConfigId, model);
            }
            return RedirectToAction(messageList);
        }

        /// <summary>
        /// Delete message by message Config Id
        /// </summary>
        /// <param name="id">integer message Config Id</param>
        /// <returns>Returns Redirect to List Action</returns>
        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (id > 0)
            {
                _messageConfigAgent.DeleteMessageConfig(id);
                return RedirectToAction("List");
            }
            return RedirectToAction("List");
        }
      
        #endregion
    }
}