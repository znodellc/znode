﻿using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.Controllers
{
    /// <summary>
    /// Setup Controller.
    /// </summary>
    public class SetupController : BaseController
    {
        #region Public Properties
        
        /// <summary>
        /// Gets the landing page of Setup.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        } 
        #endregion

    }
}
