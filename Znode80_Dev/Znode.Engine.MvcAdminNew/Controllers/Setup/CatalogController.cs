﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class CatalogController : BaseController
    {
        #region Private Variables
        private readonly ICatalogAgent _catalogAgent;
        private readonly ICategoryAgent _categoryAgent;
        private const string list = "List";
        private const string createEdit = "CreateEdit";
        private const string delete = "Delete";
        private const string copy = "Copy";
        private const string SearchCategoryView = "_SearchCategories";
        private const string CategoryPageType = "Category";
        private const string ManageAction = "Manage";
        #endregion

        #region Constructor
        public CatalogController()
        {
            _catalogAgent = new CatalogAgent();
            _categoryAgent = new CategoryAgent();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets the catalogs list.
        /// </summary>
        /// <returns>Returns the catalogs list.</returns>
        [AllowAnonymous]
        public ActionResult List(int page = 1, int recordPerPage = 10, string sort = "CatalogId", string sortDir = "ASC", string searchStore = "")
        {
            SortCollection sortCollection = new SortCollection();
            sortCollection.Add("sort", sort);
            sortCollection.Add("sortDir", sortDir);

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple("CatalogId", FilterOperators.Contains, searchStore));

            CatalogListViewModel catalogs = _catalogAgent.GetCatalogs(filters, sortCollection, page, recordPerPage);
            if (Equals(catalogs, null))
            {
                return new EmptyResult();
            }

            return ActionView(list, catalogs);
        }


        /// <summary>
        /// Manages the existing catalog.
        /// </summary>
        /// <param name="catalogId">The Id of catalog.</param>
        /// <returns>Returns view for catalog name and associated categories in edit mode.</returns>
        public ActionResult Manage(int catalogId, int page = 1, int recordPerPage = 10, string sort = "CategoryId", string sortDir = "ASC", string searchStore = "")
        {
            SortCollection sortCollection = new SortCollection();
            sortCollection.Add("sort", sort);
            sortCollection.Add("sortDir", sortDir);

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple("CategoryId", FilterOperators.Contains, searchStore));

            CatalogViewModel catalogs = new CatalogViewModel();
            catalogs = _catalogAgent.GetCatalog(catalogId);

            if (catalogs.CatalogId.Equals(catalogId))
            {
                CatalogAssociatedCategoriesListViewModel categoriesList = _categoryAgent.GetCategoryByCataLogId(catalogId, filters, sortCollection, page, recordPerPage);
                catalogs.Categories = Equals(categoriesList, null) ? null : categoriesList.AssociatedCategories;
                catalogs.RecordPerPage = categoriesList.RecordPerPage;
                catalogs.Page = categoriesList.Page;
                catalogs.TotalResults = categoriesList.TotalResults;
                catalogs.TotalPages = categoriesList.TotalPages;
            }
            return ActionView(createEdit, catalogs);
        }

        /// <summary>
        /// Creates a new catalog.
        /// </summary>
        /// <returns>Returns view of create-edit type for catalog.</returns>
        public ActionResult Add()
        {
            CatalogViewModel model = new CatalogViewModel();
            return ActionView(createEdit, model);
        }

        /// <summary>
        /// Creates a new catalog.
        /// </summary>
        /// <param name="model">The model of type CatalogViewModel.</param>
        /// <returns>Returns view of create-edit type for catalog.</returns>
        [HttpPost]
        public ActionResult Add(CatalogViewModel model)
        {
            if (!Equals(model, null))
            {
                model.IsActive = true;
                TempData[MvcAdminConstants.Notifications] = _catalogAgent.CreateCatalog(model) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);

            }
            return RedirectToAction(list);
        }

        /// <summary>
        /// Creates a copy of an existing catalog.
        /// </summary>
        /// <param name="catalogId">The Id of catalog.</param>
        /// <returns>Returns view of copy of catalog.</returns>
        public ActionResult Copy(int catalogId)
        {
            CatalogViewModel catalogs = _catalogAgent.GetCatalog(catalogId > 0 ? catalogId : 0);
            catalogs.Name = string.Format("Copy Of {0}", catalogs.Name);
            return ActionView(copy, catalogs);
        }

        /// <summary>
        /// Creates a copy of an existing catalog.
        /// </summary>
        /// <param name="model">The model of type CatalogViewModel.</param>
        /// <returns>Redirects to action List.</returns>
        [HttpPost]
        public ActionResult Copy(CatalogViewModel model)
        {
            if (!Equals(model, null))
            {
                model.IsActive = true;
                _catalogAgent.CopyCatalog(model);
            }
            return RedirectToAction(list);
        }

        /// <summary>
        /// Deletes an existing catalog.
        /// </summary>
        /// <param name="catalogId">the Id of the catalog.</param>
        /// <returns>Returns view of delete.</returns>
        public ActionResult Delete(int catalogId)
        {
            CatalogViewModel catalog = new CatalogViewModel();
            catalog = _catalogAgent.GetCatalog(catalogId);
            catalog.PreserveCategories = true;
            return View(delete, catalog);
        }

        /// <summary>
        /// Deletes an existing catalog.
        /// </summary>
        /// <param name="model">The model of type CatalogViewModel.</param>
        /// <returns>Redirects to action List.</returns>
        [HttpPost]
        public ActionResult Delete(CatalogViewModel model)
        {
            if (!Equals(model, null))
            {
                _catalogAgent.DeleteCatalog(model);
            }
            return RedirectToAction(list);

        }

        /// <summary>
        /// Associates an already created category to the catalog.
        /// </summary>
        /// <param name="catalogId">The id of the catalog.</param>
        /// <param name="categoryName">The name of the category.</param>
        /// <param name="categoryId">The id od the category.</param>
        /// <returns>Returns view.</returns>
        public ActionResult AssociateCategories(int catalogId, string categoryName = "", int categoryId = 0)
        {
            CategoryNodeViewModel model = new CategoryNodeViewModel();
            model.ParentCategoryNodes = _categoryAgent.BindParentCategoryNodes(catalogId);
            model.ThemeList = _categoryAgent.BindThemeList();
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.CSSThemeId, FilterOperators.Equals, model.ThemeId.ToString()));
            model.CssList = _categoryAgent.BindCssList(null);
            model.MasterPageList = _categoryAgent.BindMasterPageList(0, CategoryPageType);
            model.Catalog = _catalogAgent.GetCatalog(catalogId);
            model.CatalogName = model.Catalog.Name;
            model.Category = new CategoryViewModel();
            model.Category.CategoryName = categoryName;
            model.CategoryId = categoryId;
            model.CatalogId = catalogId;
            model.IsActive = true;
            model.DisplayOrder = 99;
            return View(model);
        }


        /// <summary>
        ///  Associates an already created category to the catalog.
        /// </summary>
        /// <param name="model">The model of the type CategoryNodeViewModel.</param>
        /// <returns>Retuns view.</returns>
        [HttpPost]
        public ActionResult AssociateCategories(CategoryNodeViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData[MvcAdminConstants.Notifications] = _categoryAgent.AssociateCategory(model) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            return RedirectToAction(ManageAction, new { catalogId = model.CatalogId });
        }

        /// <summary>
        /// Gets all categories.
        /// </summary>
        /// <param name="pageSize">The size of page.</param>
        /// <returns>Returns List of all categories.</returns>
        /// <param name="catalogId">The Id of the catalog.</param>
        /// <param name="categoryName">The name of category.</param>
        /// <param name="pageIndex">The index of page.</param>
        /// <param name="recordPerPage">The size of page.</param>
        /// <param name="sort">Sort collection object.</param>
        /// <param name="sortDir">Direction of Sort</param>
        /// <param name="searchStore">The search keyword.</param>
        /// <returns>Returns view.</returns>
        public ActionResult SearchCategories(int catalogId, string categoryName = "", int page = 1, int recordPerPage = 10, string sort = "CategoryId", string sortDir = "ASC", string searchStore = "")
        {
            SortCollection sortCollection = new SortCollection();
            sortCollection.Add("sort", sort);
            sortCollection.Add("sortDir", sortDir);
            TempData["CatalogId"] = catalogId;
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple("CategoryId", FilterOperators.Contains, searchStore));
            CategoryNodeViewModel model = new CategoryNodeViewModel();
            CatalogAssociatedCategoriesListViewModel categoriesList = _categoryAgent.GetAllCategories(catalogId, categoryName, filters, sortCollection, page, recordPerPage);
            model.Categories = Equals(categoriesList, null) ? null : categoriesList.AssociatedCategories;
            model.CatalogId = Convert.ToInt32(TempData["CatalogId"]);
            model.Catalog = _catalogAgent.GetCatalog(Convert.ToInt32(model.CatalogId));
            model.CatalogName = model.Catalog.Name;
            model.RecordPerPage = categoriesList.RecordPerPage;
            model.Page = categoriesList.Page;
            model.TotalResults = categoriesList.TotalResults;
            model.TotalPages = categoriesList.TotalPages;
            return ActionView(SearchCategoryView, model);
        }

        /// <summary>
        /// Bind css list to dropdown.
        /// </summary>
        /// <param name="CssThemeId">The id of theme.</param>
        /// <returns>Returns Json result.</returns>
        [HttpGet]
        public JsonResult BindCssList(int CssThemeId)
        {
            CategoryNodeViewModel model = new CategoryNodeViewModel();
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.CSSThemeId, FilterOperators.Equals, CssThemeId.ToString()));
            var list = _categoryAgent.BindCssList(filters);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Binds master page list to dropdown.
        /// </summary>
        /// <param name="CssThemeId">The id of theme.</param>
        /// <returns>Returns Json result.</returns>
        [HttpGet]
        public JsonResult BindMasterPageList(int CssThemeId)
        {
            CategoryNodeViewModel model = new CategoryNodeViewModel();
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.CSSThemeId, FilterOperators.Equals, CssThemeId.ToString()));
            var list = _categoryAgent.BindMasterPageList(CssThemeId, CategoryPageType);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}
