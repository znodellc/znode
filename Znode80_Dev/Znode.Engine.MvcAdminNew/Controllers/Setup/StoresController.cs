﻿using Resources;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    /// <summary>
    /// Store Controller
    /// </summary>
    public class StoresController : BaseController
    {
        #region Private Variables
        private readonly IPortalAgent _portalAgent;
        private readonly IDomainAgent _domainAgent;
        private const string ManageView = "Manage";
        private const string AddUrlView = "CreateEditUrl";
        private const string ListView = "List";
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for Stores Controller.
        /// </summary>
        public StoresController()
        {
            _portalAgent = new PortalAgent();
            _domainAgent = new DomainAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Deletes the specified portal
        /// </summary>
        /// <param name="portalId">Portal id</param>
        /// <returns>Redirects to list view with deleted portal.</returns>
        
        public JsonResult Delete(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!id.Equals(0))
            {
                status = _portalAgent.DeletePortal(id);
              message = status ? ZnodeResources.DeleteMessage : ZnodeResources.ErrorStoreDelete;
            }           
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Gets the main page of the store.
        /// </summary>
        /// <returns>View of the store's main page.</returns>
        public ActionResult List([ModelBinder(typeof(PageDataBinder))] FilterCollectionDataModel model)
        {
            PortalListViewModel portals = _portalAgent.GetPortals(model.Filters, model.SortCollection, model.Page, model.RecordPerPage);
            return ActionView(portals);
        }

        /// <summary>
        /// To show Manage View in Tabs
        /// </summary>
        /// <param name="id">Portal Id</param>
        /// <returns></returns>
        public ActionResult Manage(int id = 0)
        {
            TempData["PortalId"] = id;
            DomainListViewModel model = new DomainListViewModel();
            model = _domainAgent.GetDomains(id);
            return View(ManageView, model);
        }

        /// <summary>
        /// To Show Add/Edit Domain Url View.
        /// </summary>
        /// <param name="id">Portal Id</param>
        /// <returns></returns>
        public ActionResult AddUrl(int id = 0)
        {
            DomainViewModel model = new DomainViewModel();
            model.PortalId = id;
            return View(AddUrlView, model);
        }

        /// <summary>
        /// Add Domain url
        /// </summary>
        /// <param name="Model">DomainViewModel</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddUrl(DomainViewModel Model)
        {
            TempData[MvcAdminConstants.Notifications] = _domainAgent.createDomainUrl(Model) 
                ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) 
                : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            return RedirectToAction(ManageView, new { @id = Model.PortalId });
        }

        /// <summary>
        /// To show Add/edit Domain url view. 
        /// </summary>
        /// <param name="id">int Portal id</param>
        /// <param name="domainId">int domain id</param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit(int? id, int domainId)
        {
            if (!Equals(id, null))
            {
                DomainViewModel viewModel = _domainAgent.GetDomain(domainId);
                if (Equals(viewModel, null))
                {
                    return RedirectToAction(AddUrlView, new { @id = id });
                }
                return View(AddUrlView, viewModel);
            }
            return View(AddUrlView);
        }

        /// <summary>
        /// To update Domain Url data
        /// </summary>
        /// <param name="model">ProductTypesViewModel model</param>
        /// <returns>show susscess/fail message and redirect to list page</returns>
        [HttpPost]
        public ActionResult Edit(DomainViewModel ViewModel)
        {
            if (ModelState.IsValid)
        {
                TempData[MvcAdminConstants.Notifications] = _domainAgent.UpdateDomainUrl(ViewModel) 
                    ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) 
                    : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                return RedirectToAction(ManageView, new { @id = ViewModel.PortalId });
            }
            return View(ViewModel);
        }

        /// <summary>
        /// Gets Create a store page for creating a store.
        /// </summary>
        /// <returns>View for creating store.</returns>
        [HttpGet]
        public ActionResult Create()
        {
            PortalViewModel portalViewModel = new PortalViewModel();
            portalViewModel=_portalAgent.BindPortalInformation(portalViewModel);
            return View(portalViewModel);
        }

        /// <summary>
        /// Creates a portal
        /// </summary>
        /// <param name="portalViewModel"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(PortalViewModel portalViewModel)
        {
            string notification = string.Empty;
            portalViewModel.LogoPathImageName = portalViewModel.LogoPath.FileName;
            bool isPortalCreated = _portalAgent.CreatePortal(portalViewModel, out notification);

            if(isPortalCreated)
            {
                //To be redirected to manage page's first tab of Stores
               
            }
            portalViewModel = _portalAgent.BindPortalInformation(portalViewModel);
            TempData["ErrorMessage"] = notification;
            return View(portalViewModel);
        }

        /// <summary>
        /// Deletes an existing Domain Url.
        /// </summary>
        /// <param name="id">domain Id</param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult DeleteDomain(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(id, null))
            {
                status = _domainAgent.DeleteDomainUrl(id.Value);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.ErrorDeleteProductType;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }
        /// <summary>
        /// Copies a store.
        /// </summary>
        /// <param name="portalId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Copy(int portalId)
        {
            bool isPortalCopied = _portalAgent.CopyStore(portalId);
            if (!isPortalCopied)
            {
                TempData["ErrorMessage"] = ZnodeResources.ErrorCopyStore;
            }
            return RedirectToAction("List");
        }
        #endregion

        /// <summary>
        /// Gets Css list according to Theme id.
        /// </summary>
        /// <param name="CssThemeId"></param>
        /// <returns></returns>
        [HttpGet]
        public JsonResult BindCssList(int CssThemeId)
        {
            CategoryNodeViewModel model = new CategoryNodeViewModel();
            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(FilterKeys.CSSThemeId, FilterOperators.Equals, CssThemeId.ToString()));
            var list = _portalAgent.BindCssList(filters);
            return Json(list, JsonRequestBehavior.AllowGet);
        }

    }
}


