﻿using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Controllers.SetUp
{
    public class PaymentController : BaseController
    {
        #region Private Variables
        private readonly IPaymentAgent _paymentAgent;
        #endregion

        #region Constructor
        public PaymentController()
        {
            _paymentAgent = new PaymentAgent();
        }
        #endregion

        #region Public Methods
        public ActionResult PaymentOptions()
        {
            PaymentOptionListViewModel model = _paymentAgent.GetPaymentOptions();
            return View(model);
        }


        public ActionResult List()
        {
            PaymentOptionListViewModel model = _paymentAgent.GetPaymentOptions();
            return View(model);
        }

        #endregion
    }
}