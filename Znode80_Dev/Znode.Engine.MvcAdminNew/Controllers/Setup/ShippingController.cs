﻿using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.SetUp
{
    public class ShippingController : Controller
    {
        #region Private Variables

        private readonly IShippingAgent _shippingAgent;
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ShippingController()
        {
            _shippingAgent = new ShippingAgent();
        }

        #endregion
        
        #region Action Methods

        /// <summary>
        /// This function will return the list of all the Shipping Option.
        /// </summary>
        /// <returns>ActionResult view of Shipping Option list</returns>
        public ActionResult List()
        {
            //Get Shipping options
            ShippingOptionListViewModel list = _shippingAgent.GetShippingOptions();

            //Check country code, if it is null then assign to All.
            foreach (ShippingOptionViewModel shippingOption in list.ShippingOptions)
            {
                if (string.IsNullOrEmpty(shippingOption.CountryCode))
                {
                    shippingOption.CountryCode = "All";
                }
            }

            return View(list);
        }
        #endregion
    }
}