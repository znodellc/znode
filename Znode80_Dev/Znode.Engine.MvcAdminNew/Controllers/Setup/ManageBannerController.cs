﻿using System;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using Resources;

namespace Znode.Engine.MvcAdmin.Controllers.SetUp
{
    public class ManageBannerController : BaseController
    {
         #region Private Variables

        private readonly IMessageConfigAgent _messageConfigAgent;
        private readonly IPortalAgent _portalAgent;
        private int localeId = MvcAdminConstants.LocaleId;
        private string createEditView = MvcAdminConstants.CreateEditView;
        private string messageList = MvcAdminConstants.ListView;
        #endregion

        #region Public Constructors

        /// <summary>
        /// Default Constructor
        /// </summary>
        public ManageBannerController()
        {
            _messageConfigAgent = new MessageConfigAgent();
            _portalAgent = new PortalAgent();
        }

        #endregion

        #region Action Methods

        /// <summary>
        /// Gets Message list
        /// </summary>
        /// <returns>Returns ActionResult</returns>
        public ActionResult List()
        {
            ManageMessageListViewModel list = _messageConfigAgent.GetAllBanners();
            return View(list);
        }


        /// <summary>
        /// Add New Banner
        /// </summary>
        /// <param name="model">ManageMessageViewModel</param>
        /// <param name="PortalId">PortalId</param>
        /// <returns>Returns ActionResult</returns>
        public ActionResult Create()
        {
            var model = new ManageBannerViewModel();
            model.Portal = _messageConfigAgent.GetPortals();
            model.BannerKeys = _messageConfigAgent.GetAllBannerKeys();
            return View(createEditView, model);
        }

        /// <summary>
        /// Adds new Banner to list
        /// </summary>
        /// <param name="model">ManageMessageViewModel</param>
        /// <returns>Returns ActionResult</returns>
        [HttpPost]
        public ActionResult Create(ManageBannerViewModel model)
        {
            if (Equals(model.Key,null) || Equals(model.Key, MvcAdminConstants.BannerAddNewLocationValue))
            {
                model.Key = model.MessageKey;
            }
            else
            { ModelState.Remove(MvcAdminConstants.BannerMessageKey); }
            
            if (ModelState.IsValid)
            {
                if (Equals(model.Key, MvcAdminConstants.BannerDoNotShowValue))
                {
                    model.Key = string.Empty;
                    model.Value = string.Empty;
                    model.Description = string.Empty;
                    model.PageSEOName = string.Empty;
                }
                model.LocaleID = localeId;
                model.MessageTypeID = Convert.ToInt32(MessageType.Banner);
                TempData[MvcAdminConstants.Notifications] = (_messageConfigAgent.CreateBanner(model))
                                                         ? GenerateNotificationMessages(ZnodeResources.CreateBannerSuccess, NotificationType.success)
                                                         : GenerateNotificationMessages(ZnodeResources.CreateBannerError, NotificationType.error);
                return RedirectToAction(messageList);
            }
            model.Portal = _messageConfigAgent.GetPortals(model.PortalID);
            model.BannerKeys = _messageConfigAgent.GetAllBannerKeys();
            return View(createEditView, model);
        }


        /// <summary>
        /// Get Banner based on messageId for updation.
        /// </summary>
        /// <param name="messageConfigId">messageId</param>
        /// <param name="model">ManageBannerViewModel</param>
        /// <returns>Returns ActionResult</returns>
        [HttpGet]
        public ActionResult Edit(int messageId)
        {
            ManageBannerViewModel model = new ManageBannerViewModel();
            if (!messageId.Equals(null))
            {
                model = _messageConfigAgent.GetBanner(messageId);
                model.IsEditMode = true;
                return View(createEditView, model);
            }
            return View(createEditView);
        }

        /// <summary>
        /// Method Updates the Banner .
        /// </summary>
        /// <param name="model">ManageMessageViewModel</param>
        /// <returns>Return the status & redirect to List action.</returns>
        [HttpPost]
        public ActionResult Edit(ManageBannerViewModel model)
        {
            if (Equals(model.Key, MvcAdminConstants.BannerAddNewLocationValue))
            {
                model.Key = model.MessageKey;
            }
            else
            { ModelState.Remove(MvcAdminConstants.BannerMessageKey); }

            if (ModelState.IsValid)
            {
                if (Equals(model.Key, MvcAdminConstants.BannerDoNotShowValue))
                {
                    model.Key = string.Empty;
                    model.Value = string.Empty;
                    model.Description = string.Empty;
                    model.PageSEOName = string.Empty;
                }
                model.LocaleID = localeId;
                model.MessageTypeID = Convert.ToInt32(MessageType.Banner);
                TempData[MvcAdminConstants.Notifications] = (_messageConfigAgent.UpdateBanner(model.MessageID, model))
                                                        ? GenerateNotificationMessages(ZnodeResources.UpdateBannerSuccess, NotificationType.success)
                                                        : GenerateNotificationMessages(ZnodeResources.UpdateBannerError, NotificationType.error);
            }
            return RedirectToAction(messageList);
        }

        /// <summary>
        /// Delete Banner by message Config Id
        /// </summary>
        /// <param name="id">integer message Config Id</param>
        /// <returns>Returns Redirect to List Action</returns>
        [HttpPost]
        public JsonResult Delete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _messageConfigAgent.DeleteMessageConfig(id.Value);
                message = status ? ZnodeResources.DeleteBannerSuccess : ZnodeResources.DeleteBannerError;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }
        #endregion
    }
}