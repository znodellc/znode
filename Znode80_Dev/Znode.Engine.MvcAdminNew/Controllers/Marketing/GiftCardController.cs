﻿using Resources;
using System;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Marketing
{

    public class GiftCardController : BaseController
    {
        #region Private Variables
        private readonly IGiftCardAgent _GiftCardAgent;
        private string createEditView = MvcAdminConstants.CreateEditView;
        private string GiftCardListAction = MvcAdminConstants.ListView;
        #endregion

        #region Constructor
        public GiftCardController()
        {
            _GiftCardAgent = new GiftCardAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Gets the main page of the GiftCards.
        /// </summary>
        /// <returns>Returns the object of GiftCardListViewModel.</returns>
        public ActionResult List(int? page = 1, int? recordPerPage = 10)
        {
            GiftCardListViewModel list = _GiftCardAgent.GetGiftCards(page, recordPerPage);
            return View(list);
        }
                
        /// <summary>
        /// Delete GiftCard
        /// </summary>
        /// <param name="id"> int id</param>
        /// <returns>Returns to the GiftCard list view</returns>
        [HttpPost]
        public JsonResult Delete(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _GiftCardAgent.DeleteGiftCard(id);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// Create GiftCard
        /// </summary>
        /// <returns>Returns view of create-edit for GiftCard</returns>
        public ActionResult Create()
        {
            GiftCardViewModel model = new GiftCardViewModel();
            model.PortalList = _GiftCardAgent.GetAllPortals().PortalList;
            model.CardNumber = _GiftCardAgent.GetNextGiftCardNumber().CardNumber;
            return View(createEditView, model);
        }

        /// <summary>
        /// Create GiftCard
        /// </summary>
        /// <param name="model">The model of type GiftCardViewModel.</param>
        /// <returns>Returns the object of GiftCardViewModel.</returns>
        [HttpPost]
        public ActionResult Create(GiftCardViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!Equals(model.AccountId, null))
                {
                    int AccountId = Convert.ToInt32(model.AccountId);
                    if (_GiftCardAgent.IsAccountActive(AccountId))
                    {
                        bool status = _GiftCardAgent.Create(model);
                        TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                        return RedirectToAction(GiftCardListAction);
                    }
                    else
                    {
                        ModelState.AddModelError("AccountId", ZnodeResources.ValidAccountId);
                        model.PortalList = _GiftCardAgent.GetAllPortals().PortalList;
                        return View(createEditView, model);
                    }
                }
                else
                {
                    bool status = _GiftCardAgent.Create(model);
                    TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
                    return RedirectToAction(GiftCardListAction);
                }
            }
            model.PortalList = _GiftCardAgent.GetAllPortals().PortalList;
            return View(createEditView, model);
        }

        /// <summary>
        /// Edit GiftCard
        /// </summary>
        /// <param name="giftCardId">giftCardId os selected GiftCard</param>
        /// <returns>Returns view of create-edit for GiftCard and object of GiftCardViewModel</returns>
        public ActionResult Edit(int giftCardId)
        {
            if (giftCardId > 0)
            {
                GiftCardViewModel giftCard = new GiftCardViewModel();
                giftCard = _GiftCardAgent.GetGiftCard(giftCardId);
                giftCard.EnableToCustomerAccount = !Equals(giftCard.AccountId, null) ? true : false;
                giftCard.PortalList = _GiftCardAgent.GetAllPortals().PortalList;
                return View(createEditView, giftCard);
            }
            return RedirectToAction(GiftCardListAction);
        }

        /// <summary>
        /// Edit GiftCard
        /// </summary>
        /// <param name="model">The model of type GiftCardViewModel</param>
        /// <returns>Returns the GiftCards List</returns>
        [HttpPost]
        public ActionResult Edit(GiftCardViewModel model)
        {
           if (ModelState.IsValid)
            {
                if (!model.AccountId.Equals(null))
                {
                    int AccountId = Convert.ToInt32(model.AccountId);
                    if (_GiftCardAgent.IsAccountActive(AccountId))
                    {
                        bool status = _GiftCardAgent.Update(model);
                        TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                        return RedirectToAction(GiftCardListAction);
                    }
                    else
                    {
                        ModelState.AddModelError("AccountId", ZnodeResources.ValidAccountId);
                        model.PortalList = _GiftCardAgent.GetAllPortals().PortalList;
                        return View(createEditView, model);
                    }
                }
                else
                {
                    bool status = _GiftCardAgent.Update(model);
                    TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                    return RedirectToAction(GiftCardListAction);
                }
            }
            model.PortalList = _GiftCardAgent.GetAllPortals().PortalList;
            return View(createEditView, model);
        }

        #endregion
    }
}