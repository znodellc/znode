﻿using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.ViewModels;
using System;
using Resources;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.Controllers.Marketing
{
    public class FacetController : BaseController
    {
        #region Private Variables
        private readonly IFacetAgent _facetAgent;
        private readonly ICategoryAgent _categoryAgent;
        private string ListAction = "List";
        private string CreateFacetGroupView = "CreateFacetGroup";
        private string CreateFacetView = "CreateFacet";
        private string _AssociateCatagoriesPartial = "_AssociateCatagories";
        private string ManageFacetGroupAction = "ManageFacetGroup";
        private string FacetControllerName = "Facet";
        
        #endregion

        #region Constructor
        public FacetController()
        {
            _facetAgent = new FacetAgent();
            _categoryAgent = new CategoryAgent();
        }
        #endregion

        #region Public Methods
        public ActionResult List()
        {
            FacetGroupListViewModel facetGroups = _facetAgent.GetFacetGroups();
            return View(facetGroups);
        }

        [HttpGet]
        public ActionResult CreateFacetGroup()
        {
            FacetGroupViewModel model = _facetAgent.BindFacetGroupInformation();
            ViewBag.UnAssignedTitle = ZnodeResources.LabelUnAssocitedCategories;
            ViewBag.AssignedTitle = ZnodeResources.LabelAssocitedCategories;
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateFacetGroup(FacetGroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!Equals(model.AssociateCatagoriesListViewModel, null) && !Equals(model.AssociateCatagoriesListViewModel.AssignedId, null))
                {
                    TempData[MvcAdminConstants.Notifications] = (_facetAgent.CreateFacetGroup(model))
                                                         ? GenerateNotificationMessages(ZnodeResources.CreateFacetGroupSuccess, NotificationType.success)
                                                         : GenerateNotificationMessages(ZnodeResources.CreateFacetGroupError, NotificationType.error);
                    return RedirectToAction(ListAction);
                }
                else
                {
                    ViewBag.message = ZnodeResources.ErrorAssociatedCategoriesRequired;
                }
            }
            BindFacetGroupDetails(model);
            return View(model);
        }

        [HttpPost]
        public ActionResult AssignedCategoriesList(int catalogId)
        {
            FacetGroupViewModel model = _facetAgent.GetAssociatedCategories(catalogId);
            ViewBag.UnAssignedTitle = ZnodeResources.LabelUnAssocitedCategories;
            ViewBag.AssignedTitle = ZnodeResources.LabelAssocitedCategories;
            return PartialView(_AssociateCatagoriesPartial, model);
        }

        [HttpGet]
        public ActionResult ManageFacetGroup(int? itemId, string mode="")
        {
            if (Equals(mode, MvcAdminConstants.AssociatedFacet))
            {
                TempData[MvcAdminConstants.DisplayAssociatedFacet] = true;
            }
            FacetGroupViewModel model = _facetAgent.GetFacetGroup(itemId);
            model.IsEditMode = true;
            //Display the the Header titles for Associated & Un associated categories.
            ViewBag.UnAssignedTitle = ZnodeResources.LabelUnAssocitedCategories;
            ViewBag.AssignedTitle = ZnodeResources.LabelAssocitedCategories;
            return View(CreateFacetGroupView, model);
        }

        [HttpPost]
        public ActionResult ManageFacetGroup(FacetGroupViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (!Equals(model.AssociateCatagoriesListViewModel, null) && !Equals(model.AssociateCatagoriesListViewModel.AssignedId, null))
                {
                    TempData[MvcAdminConstants.Notifications] = (_facetAgent.ManageFacetGroup(model))
                                                         ? GenerateNotificationMessages(ZnodeResources.UpdateFacetGroupSuccess, NotificationType.success)
                                                         : GenerateNotificationMessages(ZnodeResources.UpdateFacetGroupError, NotificationType.error);
                    return RedirectToAction(ListAction);
                }
                else
                {
                    ViewBag.message = ZnodeResources.ErrorAssociatedCategoriesRequired;
                }
            }
            BindFacetGroupDetails(model);
            model.IsEditMode = true;
            return View(CreateFacetGroupView, model);
        }

         [HttpPost]
        public JsonResult DeleteFacetGroup(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _facetAgent.DeleteFacetGroup(id.Value);
                message = status ? ZnodeResources.DeleteFacetGroupSuccess : ZnodeResources.DeleteFacetGroupError;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        [HttpGet]
        public ActionResult CreateFacet(int? itemId)
        {
            if (!Equals(itemId, null) && itemId > 0)
            {
                FacetViewModel model = new FacetViewModel();
                model.FacetGroupID = itemId;
                return View(model);
            }
            //TODO Set Error.
            return RedirectToAction(ListAction);
        }

        [HttpPost]
        public ActionResult CreateFacet(FacetViewModel model)
        {
            if (ModelState.IsValid)
            {
                var facet = _facetAgent.CreateFacet(model);
                if(!Equals(facet, null) && facet.FacetID > 0)
                {
                    TempData[MvcAdminConstants.DisplayAssociatedFacet] = true;
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.CreateFacetSuccess, NotificationType.success);                                                        
                    return RedirectToAction(ManageFacetGroupAction, FacetControllerName, new { @itemId = facet.FacetGroupID });
                }
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.CreateFacetError, NotificationType.error);
            }
            return View(model);
        }

        [HttpGet]
        public ActionResult EditFacet(int? itemId)
        {
            if (!Equals(itemId, null) && itemId > 0)
            {
                FacetViewModel model = _facetAgent.GetFacet(itemId);
                model.IsEditMode = true;
                return View(CreateFacetView, model);
            }
            //TODO Set Error.
            return RedirectToAction(ListAction);
        }

        [HttpPost]
        public ActionResult EditFacet(FacetViewModel model)
        {
            if (ModelState.IsValid)
            {
                var facet = _facetAgent.EditFacet(model);
                if (!Equals(facet, null) && facet.FacetID > 0)
                {
                    TempData[MvcAdminConstants.DisplayAssociatedFacet] = true;
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.UpdateFacetSuccess, NotificationType.success);
                    return RedirectToAction(ManageFacetGroupAction, FacetControllerName, new { @itemId = facet.FacetGroupID });
                }
                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.UpdateFacetError, NotificationType.error);
            }
            return View(CreateFacetView,model);
        }

        [HttpPost]
        public JsonResult DeleteFacet(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _facetAgent.DeleteFacet(id.Value);
                message = status ? ZnodeResources.DeleteFacetSuccess : ZnodeResources.DeleteFacetError;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #endregion

        #region Private Methods
        private void BindFacetGroupDetails(FacetGroupViewModel model)
        {
            //Get the facet group details
            FacetGroupViewModel facetGroupModel = _facetAgent.BindFacetGroupInformation();

            //Set the selected property true, to show selected in view.
            facetGroupModel.Catalogs.ForEach(x => x.Selected = (Equals(x.Value,Convert.ToString(model.CatalogId))));
            facetGroupModel.TagControlType.ForEach(x => x.Selected = (Equals(x.Value, Convert.ToString(model.ControlTypeId))));

            model.Catalogs = facetGroupModel.Catalogs;
            model.TagControlType = facetGroupModel.TagControlType;

            //Gets associated categories based on catalog id.
            model.AssociateCatagoriesListViewModel = _facetAgent.BindAssociatedCategories((int)model.CatalogId);

            //Bind the Assocated Facets.
            model.AssociatedFacets = ((int)model.FacetGroupId > 0) ? _facetAgent.GetFacetGroup((int)model.FacetGroupId).AssociatedFacets : new FacetListViewModel();

            //Display the the Header titles for Associated & Un associated categories.
            ViewBag.UnAssignedTitle = ZnodeResources.LabelUnAssocitedCategories;
            ViewBag.AssignedTitle = ZnodeResources.LabelAssocitedCategories;
        }
        #endregion

    }
}