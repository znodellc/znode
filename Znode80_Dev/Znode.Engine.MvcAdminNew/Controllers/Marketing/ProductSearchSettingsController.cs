﻿using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.ViewModels;
using System;
using Resources;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
namespace Znode.Engine.MvcAdmin.Controllers.Marketing
{
    public class ProductSearchSettingsController : BaseController
    {
        public ActionResult ProductSearchSetting()
        {
            ProductSearchSettingsViewModel productSeachSettingModel = new ProductSearchSettingsViewModel();

            return View(productSeachSettingModel);
        }
    }
}