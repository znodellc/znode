﻿using Resources;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class PromotionController : BaseController
    {
        #region Private Variables
        private readonly IPromotionAgent _promotionAgent;
        private readonly IProductAgent _productAgent;
        private readonly IPortalAgent _portalAgent;
        private readonly IManufacturersAgent _manufacturersAgent;
        private string createEditView = MvcAdminConstants.CreateEditView;
        private string promotionListAction = MvcAdminConstants.ListView;       
        #endregion

        #region Public Constructor
        /// <summary>
        /// Constructor for Promotion
        /// </summary>
        public PromotionController()
        {
            _promotionAgent = new PromotionAgent();
            _productAgent = new ProductAgent();
            _portalAgent = new PortalAgent();
            _manufacturersAgent = new ManufacturersAgent();
        } 
        #endregion

        #region ActionMethods
        /// <summary>
        /// This function will return the list of all the Promotions.
        /// </summary>
        /// <returns>ActionResult view of promotion list</returns>
        public ActionResult List()
        {
            PromotionsListViewModel list = _promotionAgent.GetPromotions();
            return View(promotionListAction,list);
        }

        /// <summary>
        /// To show add new Promotion page
        /// </summary>
        /// <returns></returns>   
        public ActionResult Create()
        {
            PromotionsViewModel promotion = new PromotionsViewModel();
            var viewModel = _promotionAgent.GetPromotionInformation(promotion);            
            return View(createEditView, viewModel);
        }
        
        /// <summary>
        /// To save Promotion details
        /// </summary>
        /// <param name="model">PromotionviewModel</param>
        /// <returns>returns promotion view model to the view</returns>
        [HttpPost]
        public ActionResult Create(PromotionsViewModel model, int var = 0)
        {
            if (ModelState.IsValid)
            {
                var status = _promotionAgent.SavePromotion(model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            return RedirectToAction(promotionListAction);
        }

        /// <summary>
        /// To Show edit page by id
        /// </summary>
        /// <returns>edit promotion</returns>
        [HttpGet]
        public ActionResult Edit(int promotionId)
        {
            if (!promotionId.Equals(null))
            {
                PromotionsViewModel promotion = _promotionAgent.GetPromotionById(promotionId);
                var viewModel = _promotionAgent.GetPromotionInformation(promotion);
                return View(createEditView, viewModel);
            }            
            return View(createEditView);
        }

        /// <summary>
        /// To update Promotion data
        /// </summary>
        /// <param name="model">Promotion view Model</param>
        /// <returns>returns promotion view model to the view</returns>
        [HttpPost]
        public ActionResult Edit(PromotionsViewModel model)
        {
            if (ModelState.IsValid)
            {
                var status = _promotionAgent.UpdatePromotion(model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
            }
            return RedirectToAction(promotionListAction);
        }

        /// <summary>
        /// This action method delete the promotion
        /// </summary>
        /// <param name="keyword">integer Promotion Id</param>
        /// <returns>Returns partial view of promotions list</returns>
        [HttpPost]
        public ActionResult Delete(int id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _promotionAgent.DeletePromotion(id);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        /// <summary>
        /// To get Product Details
        /// </summary>
        /// <param name="promotion">Promotion view Model</param>
        /// <returns>promotion model in json format</returns>
        public ActionResult GetProduct(PromotionsViewModel promotion = null)
        {
            if (Equals(promotion, null))
            {
                promotion = new PromotionsViewModel();
            }
            var viewModel = _promotionAgent.GetPromotionInformation(promotion);
            return Json(viewModel, JsonRequestBehavior.AllowGet);
        }
      
        #endregion
    }
}