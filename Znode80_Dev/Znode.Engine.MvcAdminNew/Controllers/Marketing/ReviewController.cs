﻿using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcAdmin.Models;
using Resources;

namespace Znode.Engine.MvcAdmin.Controllers.Marketing
{
    public class ReviewController : BaseController
    {
        #region Private Variables
        private readonly IReviewAgent _reviewAgent;
        private string CustomerReviewsAction = "List";
        #endregion

        #region Constructor
        public ReviewController()
        {
            _reviewAgent = new ReviewAgent();
        }
        #endregion

        #region Public Methods
        public ActionResult List() 
        {
            ReviewViewModel reviews = _reviewAgent.GetReviews();
            return View(reviews);
        }

        public ActionResult ChangeReviewStatus(int reviewId)
        {
            ReviewItemViewModel review = _reviewAgent.GetReview(reviewId);
            return View(review);
        }

        [HttpPost]
        public ActionResult ChangeReviewStatus(ReviewItemViewModel model)
        {
            TempData[MvcAdminConstants.Notifications] = (_reviewAgent.UpdateReviewStatus(model))
                                                       ? GenerateNotificationMessages(ZnodeResources.UpdateReviewStatusSuccess, NotificationType.success)
                                                       : GenerateNotificationMessages(ZnodeResources.UpdateReviewStatusError, NotificationType.error);
            return RedirectToAction(CustomerReviewsAction);
        }


        public ActionResult EditReview(int reviewId)
        {
            if (reviewId > 0)
            {
                ReviewItemViewModel review = _reviewAgent.GetReview(reviewId);
                if (Equals(review, null))
                {
                    return RedirectToAction(CustomerReviewsAction);
                }
                return View(review);
            }
            return RedirectToAction(CustomerReviewsAction);
        }

        [HttpPost]
        public ActionResult EditReview(ReviewItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData[MvcAdminConstants.Notifications] = (_reviewAgent.UpdateReview(model))
                                                        ? GenerateNotificationMessages(ZnodeResources.UpdateReviewSuccess, NotificationType.success)
                                                        : GenerateNotificationMessages(ZnodeResources.UpdateReviewError, NotificationType.error);
                return RedirectToAction(CustomerReviewsAction);
            }
            return View(model);
        }

        public ActionResult DeleteReview(int reviewId)
        {
            if (reviewId > 0)
            {
                ReviewItemViewModel review = _reviewAgent.GetReview(reviewId);
                return View(review);
            }
            return RedirectToAction(CustomerReviewsAction);
        }

        [HttpPost]
        public JsonResult DeleteReview(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _reviewAgent.DeleteReview(id.Value);
                message = status ? ZnodeResources.DeleteReviewSuccess : ZnodeResources.DeleteReviewError;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }
        #endregion
    }
}