﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.Controllers.Dashboard
{
    /// <summary>
    /// This controller will encapsulate all the methods necessary for the dashboard page
    /// </summary>
    public class DashboardController : Controller
    {
        /// <summary>
        /// Show the Dashboard view
        /// </summary>
        /// <returns>ActionResult to display view</returns>
        public ActionResult Dashboard()
        {
            return View();
        }
    }
}
