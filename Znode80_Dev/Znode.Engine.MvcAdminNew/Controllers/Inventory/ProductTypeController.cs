﻿using Resources;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Inventory
{
    /// <summary>
    /// Product Type Controller
    /// </summary>
    public class ProductTypeController : BaseController
    {
        #region Private Variables
        private readonly IProductTypeAgent _productTypeAgent;
        private readonly IProductTypeAttributeAgent _productTypeAttributeAgent;
        private readonly IAttributeTypesAgent _attributeTypeAgent;
        private string createEditView = MvcAdminConstants.CreateEditView;
        private string productTypeListAction = MvcAdminConstants.ListView;
        private const string attributeTypeList = "AttributesList";
        #endregion

        #region Public Constructor
        /// <summary>
        /// Constructor for ProductTypeController.
        /// </summary>
        /// <returns></returns>
        public ProductTypeController()
        {
            _productTypeAgent = new ProductTypeAgent();
            _productTypeAttributeAgent = new ProductTypeAttributeAgent();
            _attributeTypeAgent = new AttributeTypesAgent();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get List of Product Types.
        /// </summary>
        /// <returns>Show Add new productType page</returns>
        public ActionResult List()
        {
            ProductTypeListViewModel List = _productTypeAgent.GetProductTypes();
            return View(List);
        }

        /// <summary>
        /// Create New Product Type.
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            return View(createEditView);
        }

        /// <summary>
        /// To save Product Type details
        /// </summary>
        /// <param name="model">ProductTypesViewModel</param>
        /// <returns>show add new page</returns>
        [HttpPost]
        public ActionResult Create(ProductTypeViewModel ViewModel)
        {
            TempData[MvcAdminConstants.Notifications] = _productTypeAgent.CreateProductType(ViewModel) 
                ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) 
                : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            return RedirectToAction(productTypeListAction);
        }

        /// <summary>
        /// To Show edit page by productTypeId
        /// </summary>
        /// <param name="id">productTypeId</param>
        /// <returns>Edit Page</returns>
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (!Equals(id, null))
            {
                ProductTypeViewModel viewModel = _productTypeAgent.GetProductType(id.Value);
                if (Equals(viewModel, null))
                {
                    return RedirectToAction(productTypeListAction);
                }
                return View(createEditView, viewModel);

            }
            return View(productTypeListAction);
        }

        /// <summary>
        /// To update ProductTypes data
        /// </summary>
        /// <param name="model">ProductTypesViewModel model</param>
        /// <returns>show susscess/fail message and redirect to list page</returns>
        [HttpPost]
        public ActionResult Edit(ProductTypeViewModel ViewModel)
        {
            if (ModelState.IsValid)
            {
                TempData[MvcAdminConstants.Notifications] = _productTypeAgent.UpdateProductType(ViewModel) 
                    ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) 
                    : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
                return RedirectToAction(productTypeListAction);
            }
            return View(ViewModel);
        }

        /// <summary>
        /// Deletes an existing Product Type.
        /// </summary>
        /// <param name="id">nullable int productTypeId</param>
        /// <returns>Returns json and Success/Fail Message</returns>
        public JsonResult Delete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(id, null))
            {
                status = _productTypeAgent.DeleteProductType(id.Value);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.ErrorDeleteProductType;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #region Attribute Associated with Product Type Methods.

        /// <summary>
        /// Get List for Attributes associated with selected Product Type.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="page"></param>
        /// <param name="recordPerPage"></param>
        /// <param name="sort"></param>
        /// <param name="sortDir"></param>
        /// <param name="searchStore"></param>
        /// <returns>show susscess/fail message and redirect to list page</returns>
        public ActionResult AttributesList(int? id, int page = 1, int recordPerPage = 10, string sort = MvcAdminConstants.productTypeId, string sortDir = MvcAdminConstants.sortDirAsc, string searchStore = "")
        {
            SortCollection sortCollection = new SortCollection();
            sortCollection.Add(MvcAdminConstants.sort, sort);
            sortCollection.Add(MvcAdminConstants.sortDir, sortDir);

            FilterCollection filters = new FilterCollection();
            filters.Add(new FilterTuple(MvcAdminConstants.productTypeId, FilterOperators.Contains, searchStore));
            ProductTypeViewModel productType = new ProductTypeViewModel();
            productType = _productTypeAgent.GetProductType(id.Value);

            if (Equals(productType.ProductTypeId,id))
            {
                ProductTypeAssociatedAttributeTypesListViewModel attributeTypeList = _productTypeAttributeAgent.GetAttributesByProductTypeId(id.Value, filters, sortCollection, page, recordPerPage);
                bool status = _productTypeAttributeAgent.IsProductAssociatedProductType(id.Value);

                if (status)
                {
                    productType.IsSuccess = true;
                }
                
                productType.attributeTypes = Equals(attributeTypeList, null) ? null : attributeTypeList.AssociatedAttributes;
                productType.RecordPerPage = attributeTypeList.RecordPerPage;
                productType.Page = attributeTypeList.Page;
                productType.TotalResults = attributeTypeList.TotalResults;
                productType.TotalPages = attributeTypeList.TotalPages;
            }
            return View(productType);
        }

        /// <summary>
        /// To show list of Attributes associated with Product Type.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ActionResult AddAttribute(int? id)
        {
            AttributeTypesViewModel attributeType = _attributeTypeAgent.GetAttributeTypeList();
            attributeType.ProductTypeId = id.Value;
            if (Equals(attributeType,null))
            {
                return new EmptyResult();
            }
            return View(attributeType);
        }

        /// <summary>
        /// To Save Attribute Associated with Product Type.
        /// </summary>
        /// <param name="productTypeId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult CreateAttribute(int? productTypeId, AttributeTypesViewModel model)
        {
            if (!Equals(model.AttributeTypeId, null))
            {

                ProductTypeAssociatedAttributeTypesViewModel addAttributes = new ProductTypeAssociatedAttributeTypesViewModel();
                
                if (!Equals(productTypeId.Value, null))
                {
                    addAttributes.ProductTypeId = productTypeId.Value;
                    addAttributes.AttributeTypeId = model.AttributeTypeId;
                    bool status = _productTypeAttributeAgent.CreateProductType(addAttributes);
                    TempData[MvcAdminConstants.Notifications] = status 
                        ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) 
                        : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.info);
                }
            }
            return RedirectToAction(attributeTypeList, new { id = productTypeId });
        }

        /// <summary>
        /// To delete Attribute associated with Product Type Id.
        /// </summary>
        /// <param name="id">nullable int AttributeId</param>
        /// <param name="productTypeId">int attributeTypeId</param>
        /// <returns>show susscess/fail message and redirect to list page</returns>
        [HttpPost]
        public JsonResult DeleteAttribute(int? id, int productTypeId)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();

            if (!Equals(id, null))
            {
                //To Check is Product Type associated with Product.
                var associate = _productTypeAttributeAgent.IsProductAssociatedProductType(productTypeId);
                if (!associate)
                {
                    status = _productTypeAttributeAgent.DeleteAttributeType(id.Value);
                }
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.ErrorDeleteProductTypeAssociatedProduct ;
            }
            return Json(new { sucess = status, message = message, id = productTypeId, isFadeOut = isFadeOut });
        } 
        #endregion
        #endregion
    }
}