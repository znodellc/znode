﻿using Resources;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Controllers
{
    public class AttributeTypesController : BaseController
    {
        #region Private Variables

        private readonly IAttributeTypesAgent _attributeTypesAgent;
        private readonly IAttributesAgent _attributesAgent;
        private string createEditView = MvcAdminConstants.CreateEditView;
        private string attributeTypeListAction = MvcAdminConstants.ListView;
        private string attributeValueListAction = "AttributeValueList";
        private string createEditAttributeView = "CreateEditAttribute";
        private int localeId = MvcAdminConstants.LocaleId;

        #endregion

        #region Public Constructors

        /// <summary>
        /// Constructor for the AttributeTypes
        /// </summary>
        public AttributeTypesController()
        {
            _attributeTypesAgent = new AttributeTypesAgent();
            _attributesAgent = new ProductAttributesAgent();
        }

        #endregion

        #region Action Methods
        public ActionResult List()
        {
            AttributeTypesListViewModel attributeTyp = _attributeTypesAgent.GetAttributesType();
            if (Equals(attributeTyp, null))
            {
                return new EmptyResult();
            }
            return View(attributeTypeListAction, attributeTyp);
        }

        /// <summary>
        /// To show add new Attribute Type page
        /// </summary>
        /// <returns>Show Add new attributetype page</returns>
        [HttpGet]
        public ActionResult Create()
        {
            return View(createEditView);
        }

        /// <summary>
        /// To save Attribute Type details
        /// </summary>
        /// <param name="model">AttributeTypesViewModel</param>
        /// <returns>show add new page</returns>
        [HttpPost]
        public ActionResult Create(AttributeTypesViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.LocaleId = localeId;
                TempData[MvcAdminConstants.Notifications] = _attributeTypesAgent.SaveAttributeType(model) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.info);
            }
            return RedirectToAction(attributeTypeListAction);
        }

        /// <summary>
        /// To Show edit page by id
        /// </summary>
        /// <param name="id">nullabe int attributeTypeId</param>
        /// <returns>Edit Page</returns>
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (!Equals(id, null))
            {
                AttributeTypesViewModel model = _attributeTypesAgent.GetAttributeTypeById(id.Value);
                return View(createEditView, model);
            }
            return View(createEditView);
        }

        /// <summary>
        /// To update AttributeTypes data
        /// </summary>
        /// <param name="model">AttributeTypesViewModel model</param>
        /// <returns>show susscess/fail message and redirect to list page</returns>
        [HttpPost]
        public ActionResult Edit(AttributeTypesViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData[MvcAdminConstants.Notifications] = _attributeTypesAgent.UpdateAttributeType(model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.info);
            }
            return RedirectToAction(attributeTypeListAction);
        }

        /// <summary>
        ///  To delete AttributeTypes by AttributeTypeId
        /// </summary>
        /// <param name="id">nullable int AttributeTypeId</param>
        /// <returns>show susscess/fail message and redirect to list page</returns>
        [HttpPost]
        public JsonResult Delete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _attributeTypesAgent.DeleteAttributeType(id.Value);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #region Attribute Values
        /// <summary>
        /// To show AttributeValue list page by AttributeTypeId
        /// </summary>
        /// <param name="id">nullable int AttributeTypeId</param>
        /// <returns>AttributeValue list page</returns>
        [HttpGet]
        public ActionResult AttributeValueList(int? id)
        {
            if (!Equals(id, null))
            {
                AttributesListViewModel model = _attributesAgent.GetAttributesByAttributeTypeId(id.Value);
                return View(attributeValueListAction, model);
            }
            return View(attributeValueListAction);
        }

        /// <summary>
        /// To show add new Attribute value page 
        /// </summary>
        /// <param name="id">nullable int AttributeTypeId</param>
        /// <returns>show add new page</returns>
        [HttpGet]
        public ActionResult CreateAttribute(int? id)
        {
            if (!id.Equals(null))
            {
                AttributesViewModel model = new AttributesViewModel();
                model.AttributeTypeId = id.Value;
                return View(createEditAttributeView, model);
            }
            return View(createEditAttributeView);
        }

        /// <summary>
        /// To save Attribute value details
        /// </summary>
        /// <param name="model">AttributesViewModel</param>
        /// <returns>show susscess/fail message and redirect to list page</returns>
        [HttpPost]
        public ActionResult CreateAttribute(AttributesViewModel model)
        {
            if (ModelState.IsValid)
            {
               TempData[MvcAdminConstants.Notifications] = _attributesAgent.SaveAttributes(model) ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.info);
            }
            return RedirectToAction(attributeValueListAction, new { id = model.AttributeTypeId });
        }

        /// <summary>
        /// To show update Attribute value page by AttributeId
        /// </summary>
        /// <param name="id">nullable int AttributeId</param>
        /// <returns>show update page</returns>
        [HttpGet]
        public ActionResult EditAttribute(int? id)
        {
            if (!id.Equals(null))
            {
                AttributesViewModel model = _attributesAgent.GetAttributesByAttributeId(id.Value);
                return View(createEditAttributeView, model);
            }
            return View(attributeValueListAction);
        }

        /// <summary>
        /// To update Attribute value details
        /// </summary>
        /// <param name="model">AttributesViewModel</param>
        /// <returns>show susscess/fail message and redirect to list page</returns>
        [HttpPost]
        public ActionResult EditAttribute(AttributesViewModel model)
        {
            if (ModelState.IsValid)
            {
                TempData[MvcAdminConstants.Notifications] = _attributesAgent.UpdateAttributes(model) ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.info);
            }
            return RedirectToAction(attributeValueListAction, new { id = model.AttributeTypeId });
        }

        /// <summary>
        /// To delete Attribute by AttributeId
        /// </summary>
        /// <param name="id">nullable int AttributeId</param>
        /// <returns>show susscess/fail message and redirect to list page</returns>
        [HttpPost]
        public JsonResult DeleteAttribute(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                status = _attributesAgent.DeleteAttributes(id.Value);
                message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }
        #endregion

        #endregion
    }
}