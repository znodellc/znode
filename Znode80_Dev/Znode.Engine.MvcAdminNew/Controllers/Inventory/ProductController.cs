﻿using Resources;
using System.IO;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class ProductController : BaseController
    {
        #region Private Variables
        private readonly IProductAgent _productAgent;
        private string createEditView = MvcAdminConstants.CreateEditView;
        private string ProductTagsPartialView = "_ProductTags";
        private string ProductFacetsPartialView = "_ProductFacets";
        private string ProductAssociateFacetsPartialView = "_AssociatedFacets";
        #endregion

        #region Constructor
        public ProductController()
        {
            _productAgent = new ProductAgent();
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// To show product List
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult List()
        {
            ProductListViewModel products = _productAgent.GetProducts();
            if (Equals(products, null))
            {
                return new EmptyResult();
            }
            return View("List", products);
        }

        /// <summary>
        /// To show add new product detail page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Create()
        {
            ProductViewModel model = new ProductViewModel();
            _productAgent.BindProductPageDropdown(model, PortalId.Value);
            model.ShippingRate = 0;
            return View(createEditView, model);
        }

        /// <summary>
        /// To save product details
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.IsActive = true;
                model.ReviewStateId = (int)ZNodeProductReviewState.Approved;
                model.FreeShipping = Equals(model.FreeShipping, null) ? false : true;
                model.ShippingRate = Equals(model.ShippingRuleTypeId, 3) ? null : model.ShippingRate;
                bool status = _productAgent.IsSkuExist(model.Sku, model.ProductId);
                if (status)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorSkuAlreadyExist, NotificationType.error);
                    _productAgent.BindProductPageDropdown(model, PortalId.Value);
                    return View(createEditView, model);
                }
                status = _productAgent.SaveProduct(model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            return RedirectToAction(MvcAdminConstants.ManageView, new { id = model.ProductId });
        }

        /// <summary>
        /// To manage product details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Edit(int? id)
        {
            if (!Equals(id, null))
            {
                ProductViewModel model = _productAgent.GetProduct(id.Value);
                _productAgent.BindProductPageDropdown(model, PortalId.Value);
                return View(createEditView, model);
            }
            return View(createEditView);
        }

        [HttpPost]
        public ActionResult Edit(ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.IsActive = true;
                model.ReviewStateId = (int)ZNodeProductReviewState.Approved;
                model.FreeShipping = Equals(model.FreeShipping, null) ? false : true;
                if (Equals(model.FreeShipping, true))
                {
                    model.ShippingRuleTypeId = 0;
                    model.ShippingRate = null;
                }
                bool status = _productAgent.IsSkuExist(model.Sku, model.ProductId);
                if (status)
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorSkuAlreadyExist, NotificationType.error);
                    _productAgent.BindProductPageDropdown(model, PortalId.Value);
                    return View(createEditView, model);
                }
                status = _productAgent.UpdateProduct(model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            return RedirectToAction(MvcAdminConstants.ManageView, new { id = model.ProductId });
        }

        /// <summary>
        /// To edit product details
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult Manage(int? id)
        {
            if (!Equals(id, null))
            {
                ProductViewModel model = _productAgent.GetProduct(id.Value);
                return View(MvcAdminConstants.ManageView, model);
            }
            return View(MvcAdminConstants.ManageView);
        }


        /// <summary>
        /// Gets the product tags based on product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetProductTags(int productId)
        {
            ProductTagsViewModel model = _productAgent.GetProductTags(productId);
            if (Equals(model.ProductId, 0))
            {
                model.ProductId = productId;
            }
            return ActionView(ProductTagsPartialView, model);
        }

        /// <summary>
        /// Method Insert/Update/Delete the product tags.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult ProductTags(ProductTagsViewModel model)
        {
            ModelState.Remove("ProductTags");
            if (!string.IsNullOrEmpty(model.ProductTags))
            {
                model.ProductTags = model.ProductTags.Trim();
            }
            ProductTagsViewModel productTagModel = _productAgent.GetProductTags(model.ProductId);

            return (!string.IsNullOrEmpty(productTagModel.ProductTags))
                ? (!string.IsNullOrEmpty(model.ProductTags))
                //update Tags
                    ? ProudctTagsHandler(model, productTagModel, ZnodeResources.UpdateTagSuccess, ZnodeResources.UpdateTagError, ZnodeCRUDMode.Update)
                //Delete Tags
                    : ProudctTagsHandler(new ProductTagsViewModel { ProductId = model.ProductId }, productTagModel, ZnodeResources.DeleteTagSuccess, ZnodeResources.DeleteTagError, ZnodeCRUDMode.Delete)
                : (!string.IsNullOrEmpty(model.ProductTags))
                //Insert Tags
                    ? ProudctTagsHandler(model, productTagModel, ZnodeResources.CreateTagSuccess, ZnodeResources.CreateTagError, ZnodeCRUDMode.Insert)
                //Required Validation 
                    : ProudctTagsHandler(new ProductTagsViewModel { ProductId = model.ProductId }, productTagModel, string.Empty, ZnodeResources.RequiredProductTags, ZnodeCRUDMode.Error);
        }

        /// <summary>
        /// Gets the product Facets based on product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        [HttpGet]
        public ActionResult GetProductFacets(int productId)
        {
            ProductTagsViewModel model = _productAgent.GetProductTags(productId);
            if (Equals(model.ProductId, 0))
            {
                model.ProductId = productId;
            }
            return ActionView(ProductFacetsPartialView, model);
        }

        [HttpGet]
        public ActionResult GetAssociatedFacets(int productId)
        {
            ProductFacetsViewModel model = _productAgent.GetAssociatedFacetGroup(productId);
            model.ProductId = productId;
            return ActionView(ProductAssociateFacetsPartialView, model);
        }


        public ActionResult AssociatedFacets(ProductFacetsViewModel model)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            var selectedFacets = _productAgent.GetAssociatedFacetGroup(model.ProductId).SelectedFacets;
            if (Equals(selectedFacets, null) && !(Equals(model.PostedFacets, null)))
            {
                //Insert Associated Facets.
                status = _productAgent.BindAssociatedFacets(model);
            }
            else
            {
                if (!Equals(selectedFacets, null) && selectedFacets.Count > 0)
                {
                    //Insert or Delete Associated Facets.
                    status = _productAgent.BindAssociatedFacets(model);
                }
                else
                {
                    //Error for Required associated facets.
                    message = ZnodeResources.RequiredAssocateFacets;
                }
            }
            ProductFacetsViewModel modelfacet = _productAgent.GetAssociatedFacetGroup(model.ProductId);
            modelfacet.ProductId = model.ProductId;
            var ResponseData = RenderRazorViewToString(ProductAssociateFacetsPartialView, modelfacet);
            return Json(new { sucess = status, message = message, Response = ResponseData, isFadeOut = isFadeOut });
        }


        [HttpGet]
        public ActionResult GetFacetsByFacetGroupId(int productId, int facetGroupId)
        {
            ProductFacetsViewModel model = new ProductFacetsViewModel();
            if (productId > 0 && facetGroupId > 0)
            {
                model = _productAgent.GetAssociatedFacetGroup(productId);
            }
            return ActionView("_FacetCheckBoxList", model);
        }

        #endregion

        #region Private Method

        /// <summary>
        /// To get Product Attributes list by ProductTypeId
        /// </summary>
        /// <param name="productTypeId">int ProductTypeId</param>
        /// <returns>json collection of attributes list and its values</returns>
        [HttpGet]
        public JsonResult GetProductAttributesByProductTypeId(int? productTypeId)
        {
            var result = _productAgent.GetProductAttributesByProductTypeId(productTypeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private ActionResult ProudctTagsHandler(ProductTagsViewModel model, ProductTagsViewModel productTagModel, string msgSuccess, string msgError, ZnodeCRUDMode mode)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            model.TagId = productTagModel.TagId;
            status = (Equals(mode, ZnodeCRUDMode.Update))
                ? _productAgent.UpdateProductTags(model)
                : (Equals(mode, ZnodeCRUDMode.Delete))
                    ? _productAgent.DeleteProductTags(productTagModel.TagId)
                    : (Equals(mode, ZnodeCRUDMode.Insert))
                        ? _productAgent.InsertProductTags(model)
                        : false;

            message = status ? msgSuccess : msgError;
            var ResponseData = RenderRazorViewToString(ProductTagsPartialView, model);
            return Json(new { sucess = status, message = message, Response = ResponseData, isFadeOut = isFadeOut });
        }

        #endregion
  
    }
}
