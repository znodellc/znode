﻿using Resources;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers.Inventory
{
    public class HighlightController : BaseController
    {
        #region Private Variables
        private string createEditView = MvcAdminConstants.CreateEditView;
        private string HighlightListAction = MvcAdminConstants.ListView;
        private readonly IHighlightAgent _highlightAgent;
        private int localeId = MvcAdminConstants.LocaleId;
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public HighlightController()
        {
            _highlightAgent = new HighlightAgent();
        }
        #endregion

        #region public Action Methods

        /// <summary>
        /// Gets List Of Highlight
        /// </summary>
        /// <returns>ActionResult</returns>
        public ActionResult List()
        {
            HighlightsListViewModel list = _highlightAgent.GetHighlights();
            list.HighlightTypeList = _highlightAgent.GetHighlightTypes().HighlightTypeList;
            return View(HighlightListAction, list);
        }

        /// <summary>
        /// Create highlight
        /// </summary>
        /// <returns>ActionResult</returns>
        public ActionResult Create()
        {
            HighlightViewModel model = new HighlightViewModel();
            model.HighlightTypeList = _highlightAgent.GetHighlightTypes().HighlightTypeList;
            return View(createEditView, model);
        }

        /// <summary>
        /// Create Highlight
        /// </summary>
        /// <param name="model">HighlightViewModel model</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public ActionResult Create(HighlightViewModel model)
        {
            if (ModelState.IsValid)
            {
                SetHighlightProperty(model);
                bool status = _highlightAgent.CreateHighlight(model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.SaveMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.SaveErrorMessage, NotificationType.error);
            }
            return RedirectToAction(HighlightListAction);
        }

        /// <summary>
        /// Edit Existing Highlight
        /// </summary>
        /// <param name="id">int? highlightId</param>
        /// <returns>ActionResult</returns>
        public ActionResult Edit(int? id)
        {
            HighlightViewModel model = null;
            if (!Equals(id, null) && id > 0)
            {
                model = _highlightAgent.GetHighlight(int.Parse(id.ToString()));
                model.HighlightTypeList = _highlightAgent.GetHighlightTypes().HighlightTypeList;
                model.IsEditMode = true;
            }
            return View(createEditView, model);
        }

        /// <summary>
        ///  Edit Existing Highlight
        /// </summary>
        /// <param name="model">HighlightViewModel model</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public ActionResult Edit(HighlightViewModel model)
        {
            if (ModelState.IsValid)
            {
                SetHighlightProperty(model);
                var status = _highlightAgent.UpdateHighlight(model);
                TempData[MvcAdminConstants.Notifications] = status ? GenerateNotificationMessages(ZnodeResources.UpdateMessage, NotificationType.success) : GenerateNotificationMessages(ZnodeResources.UpdateErrorMessage, NotificationType.error);
            }
            return RedirectToAction(HighlightListAction);
        }

        /// <summary>
        /// Delete existing highlight
        /// </summary>
        /// <param name="id">int highlightId</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public JsonResult Delete(int? id)
        {
            bool status = false;
            string message = string.Empty;
            bool isFadeOut = CheckIsFadeOut();
            if (!Equals(null, id))
            {
                if (_highlightAgent.CheckAssociatedProduct(id.Value))
                {
                    message = ZnodeResources.DeleteHighlightErrorMessage;
                }
                else
                {
                    status = _highlightAgent.DeleteHighlight(id.Value);
                    message = status ? ZnodeResources.DeleteMessage : ZnodeResources.DeleteErrorMessage;
                }
            }
            return Json(new { sucess = status, message = message, id = id, isFadeOut = isFadeOut });
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Sets the property of Highlight
        /// </summary>
        /// <param name="model">HighlightViewModel model</param>
        private void SetHighlightProperty(HighlightViewModel model)
        {
            if (!Equals(model.Image, null))
            {
                model.ImageFile = (!Equals(model.Image, null)) ? model.Image.FileName : null;
            }
            else
            {
                model.ImageFile = model.ImageFile;
            }
            model.ImageFile = model.NoImage ? null : model.ImageFile;
            model.LocaleId = localeId;
            model.ShowPopup = (model.Description != null) ? true : false;
            model.HyperlinkNewWindow = (model.Hyperlink != null) ? true : false;
        }
        #endregion
    }
}