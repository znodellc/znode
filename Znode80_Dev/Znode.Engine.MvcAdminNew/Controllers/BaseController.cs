﻿using Elmah;
using Newtonsoft.Json;
using Resources;
using System;
using System.Configuration;
using System.IO;
using System.Web.Mvc;
using System.Web.Routing;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Models;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class BaseController : Controller
    {
        #region Public variable
        public const string Notifications = "Notifications";
        #endregion

        #region Property
        public int? PortalId { get; set; }
        #endregion

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.ControllerContext = new ControllerContext(requestContext, this);
            PortalId = PortalAgent.CurrentPortal.PortalId;
        }


        #region ActionView
        public ActionResult ActionView()
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View();
        }

        public ActionResult ActionView(IView view)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View(view);
        }

        public ActionResult ActionView(object model)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView(model);
            }
            return View(model);
        }

        public ActionResult ActionView(string viewName)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView(viewName);
            }
            return View(viewName);
        }

        public ActionResult ActionView(IView view, object model)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View(view, model);
        }

        public ActionResult ActionView(string viewName, object model)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView(viewName, model);
            }
            return View(viewName, model);
        }

        public ActionResult ActionView(string viewName, string masterName)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View(viewName, masterName);
        }

        public ActionResult ActionView(string viewName, string masterName, object model)
        {
            if (Request.IsAjaxRequest())
            {
                return PartialView();
            }
            return View(viewName, masterName, model);
        }
        #endregion

        #region Protected Method

        /// <summary>
        /// To show Notification message 
        /// </summary>
        /// <param name="message">string message to show on page</param>
        /// <param name="type">enum type of message</param>
        /// <param name="isFadeOut">bool isFadeOut true/false</param>
        /// <param name="fadeOutMilliSeconds">int fadeOutMilliSeconds</param>
        /// <returns>string Json format of message box</returns>
        protected string GenerateNotificationMessages(string message, NotificationType type, int fadeOutMilliSeconds = 5000)
        {
            MessageBoxModel msgObj = new MessageBoxModel();
            msgObj.Message = message;
            msgObj.Type = type.ToString();
            msgObj.IsFadeOut = CheckIsFadeOut();
            msgObj.FadeOutMilliSeconds = fadeOutMilliSeconds;
            return JsonConvert.SerializeObject(msgObj);
        }
        #endregion

        #region public Method

        /// <summary>
        /// To get IsFadeOut status from web config file, 
        /// if NotificationMessagesIsFadeOut key not found in config then it will returns false 
        /// </summary>
        /// <returns>return true/false</returns>
        public bool CheckIsFadeOut()
        {
            bool isFadeOut = false;
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["NotificationMessagesIsFadeOut"]))
            {
                isFadeOut = Convert.ToBoolean(ConfigurationManager.AppSettings["NotificationMessagesIsFadeOut"]);
            }
            else
            {
                //To do : need to log this in log file after common log functionality is ready.
                //ZNodeLogging.LogMessage(ZnodeResources.ConfigKeyNotificationMessagesIsFadeOutMissing);
            }
            return isFadeOut;
        }

        /// <summary>
        /// Log error to Elmah
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="contextualMessage"></param>
        public void ExceptionHandler(Exception ex, string contextualMessage = null)
        {
            if (contextualMessage != null)
            {
                var annotatedException = new Exception(contextualMessage, ex);
                Elmah.ErrorSignal.FromCurrentContext().Raise(annotatedException);
            }
            else
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            ModelState.AddModelError(string.Empty, ZnodeResources.GenericErrorMessage);
        }

        //Method converts the Partial view result to string.
        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        #endregion
    }
}
