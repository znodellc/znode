﻿using System.Net;
using System.Web.Mvc;
using System.Web;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;
using Resources;
using System;
using System.Configuration;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class AccountController : BaseController
    {
        #region Private Variables
        private readonly IAccountAgent _accountAgent;
        private readonly IAuthenticationAgent _authenticationAgent;
        LoginViewModel model;
        private string DashBoardControllerName = "Dashboard";
        private string DashBoardAction = "Dashboard";
        private string LoginAction = "Login";
        private string ResetPasswordAction = "ResetPassword";
        private string ForgotPasswordAction = "ForgotPassword";
        private string ResetAdminDetailsAction = "ResetAdminDetails";
        private string LoginView = "Login";
        private string ResetPasswordView = "ResetPassword";
        private string ForgotPasswordView = "ForgotPassword";
        private string ChangePasswordView = "ChangePassword";
        #endregion

        #region Constructor
        public AccountController()
        {
            _accountAgent = new AccountAgent();
            _authenticationAgent = new AuthenticationAgent();
        }
        #endregion

        #region Public Methods

        #region Get/Post action for Login
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                _accountAgent.Logout();
            }
            //Get user name from cookies
            GetLoginRememberMeCookie();
            return View(LoginView, model);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                //Login the user.
               
                var loginviewModel = _accountAgent.Login(model);

                if (!loginviewModel.HasError)
                {
                    //Set the Authentication cookie.
                    _authenticationAgent.SetAuthCookie(model.Username, model.RememberMe);

                    //TODO//Remember me
                    if (checked(model.RememberMe) == true)
                    {
                        SaveLoginRememberMeCookie(model.Username);
                    }
                    return ValidateUserRole(model);
                }
                //TODO
                if (loginviewModel != null && loginviewModel.IsResetPassword)
                {
                    if (!string.IsNullOrEmpty(loginviewModel.Username) && !string.IsNullOrEmpty(loginviewModel.PasswordResetToken))
                    {
                        return RedirectToAction(ResetPasswordAction, new { passwordToken = loginviewModel.PasswordResetToken, userName = loginviewModel.Username });
                    }
                    return RedirectToAction(ResetPasswordAction);
                }
                if (loginviewModel != null && loginviewModel.IsResetAdmin)
                {
                    return RedirectToAction(ResetAdminDetailsAction);
                }

                TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(loginviewModel.ErrorMessage, Znode.Engine.MvcAdmin.Models.NotificationType.error) ;
                return View(LoginView, loginviewModel);
            }
            return View(LoginView, model);
        }

      
        #endregion

        #region Post action for Logout
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                _accountAgent.Logout();
            }
            return RedirectToAction(LoginAction);
        }

         #endregion

        #region Get/Post Action for ResetAdminDetails
        [HttpGet]
        public ActionResult ResetAdminDetails()
        {
            if (!_accountAgent.CheckAccountKey())
            {
                return RedirectToAction(LoginAction);
            }
            return View();
        }
       
        [HttpPost]
        public ActionResult ResetAdminDetails(ResetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                var resetPasswordModel = _accountAgent.ResetPassword(model);
                if (!resetPasswordModel.HasError)
                {
                    var loginModel = new LoginViewModel()
                    {
                        Username = model.Username,
                        Password = model.Password,
                    };
                    var accountViewModel = _accountAgent.Login(loginModel);

                    if (!accountViewModel.HasError)
                    {
                        Session.Add("SuccessMessage", resetPasswordModel.SuccessMessage);
                        _authenticationAgent.SetAuthCookie(loginModel.Username, true);
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(resetPasswordModel.SuccessMessage, Znode.Engine.MvcAdmin.Models.NotificationType.success);
                        return RedirectToAction(DashBoardAction, DashBoardControllerName);
                    }
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(resetPasswordModel.ErrorMessage, Znode.Engine.MvcAdmin.Models.NotificationType.error);
                }
            }
            return View();
        }


        #endregion

        #region Get/Post Action for ResetPassword
        [AllowAnonymous]
        [HttpGet]
        public ActionResult ResetPassword(string passwordToken, string userName)
        {

            passwordToken = WebUtility.UrlDecode(passwordToken);
            userName = WebUtility.UrlDecode(userName);
            var resetPassword = new ChangePasswordViewModel();
            resetPassword.UserName = userName;
            resetPassword.PasswordToken = passwordToken;

            //Set ResetPasword flag, use to hide Old Password field in View.
            resetPassword.IsResetPassword = true;
            ResetPasswordStatusTypes enumstatus;

            enumstatus = _accountAgent.CheckResetPasswordLinkStatus(resetPassword);
            switch (enumstatus)
            {
                case ResetPasswordStatusTypes.Continue:
                    {
                        return View(ResetPasswordView, resetPassword);
                    }
                case ResetPasswordStatusTypes.LinkExpired:
                    {
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ResetPasswordLinkError, Znode.Engine.MvcAdmin.Models.NotificationType.error); 
                        return RedirectToAction(ForgotPasswordAction);
                    }
                case ResetPasswordStatusTypes.TokenMismatch:
                    {
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ResetPasswordLinkError, Znode.Engine.MvcAdmin.Models.NotificationType.error); 
                        return RedirectToAction(ForgotPasswordAction);
                    }
                case ResetPasswordStatusTypes.NoRecord:
                    {
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ResetPasswordLinkError, Znode.Engine.MvcAdmin.Models.NotificationType.error); 
                        return RedirectToAction(ForgotPasswordAction);
                    }
                default:
                    {
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ResetPasswordLinkError, Znode.Engine.MvcAdmin.Models.NotificationType.error); 
                        return RedirectToAction(ForgotPasswordAction);
                    }
            }
        }

         [AllowAnonymous]
        [HttpPost]
        public ActionResult ResetPassword(ChangePasswordViewModel model)
        {
            ModelState.Remove("OldPassword");
            model.IsResetPassword = true;
            if (ModelState.IsValid)
            {
                var changepasswordmodel = _accountAgent.ChangePassword(model);

                if (!model.HasError)
                {
                    var loginModel = new LoginViewModel()
                    {
                        Username = model.UserName,
                        Password = model.NewPassword,
                    };
                    var accountViewModel = _accountAgent.Login(loginModel);

                    if (!accountViewModel.HasError)
                    {
                        Session.Add("SuccessMessage", changepasswordmodel.SuccessMessage);
                        _authenticationAgent.SetAuthCookie(loginModel.Username, true);
                        TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(changepasswordmodel.SuccessMessage, Znode.Engine.MvcAdmin.Models.NotificationType.success);
                        return RedirectToAction(DashBoardAction, DashBoardControllerName);
                    }
                }
                else
                {
                    TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(changepasswordmodel.ErrorMessage, Znode.Engine.MvcAdmin.Models.NotificationType.error);
                }
            }
            return View(ResetPasswordView, model);
        }




        #endregion

        #region Get/Post Action for Forgot Password
        [AllowAnonymous]
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            return View();
        }
        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgotPassword(AccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                model = _accountAgent.ForgotPassword(model);
                TempData[MvcAdminConstants.Notifications] = (model.HasError)
                    ? GenerateNotificationMessages(model.ErrorMessage, Znode.Engine.MvcAdmin.Models.NotificationType.error)
                    : GenerateNotificationMessages(model.SuccessMessage, Znode.Engine.MvcAdmin.Models.NotificationType.info);
            }
            return View(ForgotPasswordView, model);
        }
        #endregion

        #region Get/Post for Change Password
        [Authorize]
        [HttpGet]
        public ActionResult Changepassword()
        {
            return View(ChangePasswordView, new ChangePasswordViewModel());
        }

        [Authorize]
        [HttpPost]
        public ActionResult Changepassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.UserName = User.Identity.Name;
                model = _accountAgent.ChangePassword(model);

                TempData[MvcAdminConstants.Notifications] = (model.HasError)
                    ? GenerateNotificationMessages(model.ErrorMessage, Znode.Engine.MvcAdmin.Models.NotificationType.error)
                    : GenerateNotificationMessages(model.SuccessMessage, Znode.Engine.MvcAdmin.Models.NotificationType.success);
                if (!model.HasError)
                {
                    return RedirectToAction(DashBoardAction, DashBoardControllerName);
                }
            }
            return View(ChangePasswordView, model);
        }

        #endregion

        #endregion

        #region Private Methods
        /// <summary>
        /// Remember me function for logged in user on remember me selection
        /// This function is used to save user name in cookies.
        /// </summary>
        /// <param name="userId">string userId</param>
        private void SaveLoginRememberMeCookie(string userId)
        {
            //Check if the browser support cookies 
            if ((HttpContext.Request.Browser.Cookies))
            {
                HttpCookie cookieLoginRememberMe = new HttpCookie(MvcAdminConstants.LoginCookieNameValue);
                cookieLoginRememberMe.Values[MvcAdminConstants.LoginCookieNameValue] = userId;
                cookieLoginRememberMe.Expires = DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]));
                HttpContext.Response.Cookies.Add(cookieLoginRememberMe);
                cookieLoginRememberMe.HttpOnly = true;
            }
        }

        /// <summary>
        /// Remember me function for logged in user on remember me selection
        /// This method is used to get user name value from cookies.
        /// </summary>
        private void GetLoginRememberMeCookie()
        {
            if ((HttpContext.Request.Browser.Cookies))
            {
                if (!Equals(HttpContext.Request.Cookies[MvcAdminConstants.LoginCookieNameValue], null))
                {
                    HttpCookie cookieRememberMe = HttpContext.Request.Cookies[MvcAdminConstants.LoginCookieNameValue];
                    if (!Equals(cookieRememberMe, null))
                    {
                        var loginName = HttpUtility.HtmlEncode(cookieRememberMe.Values[MvcAdminConstants.LoginCookieNameValue]);
                        model = new LoginViewModel();
                        model.Username = loginName;
                        model.RememberMe = true;
                    }
                }
            }
        }

        private ActionResult ValidateUserRole(LoginViewModel model)
        {
            if (_accountAgent.IsUserInRole(model.Username, "ORDER APPROVER") && !_accountAgent.IsUserInRole(model.Username, "ADMIN"))
                //TODO
                //Response.Redirect("~/Secure/Orders/OrderManagement/ViewOrders/Default.aspx", true);
                return RedirectToAction("Index", "Test");

            if ((_accountAgent.IsUserInRole(model.Username.Trim(), "REVIEWER") || _accountAgent.IsUserInRole(model.Username.Trim(), "REVIEWER MANAGER")) && !_accountAgent.IsUserInRole(model.Username, "ADMIN"))
            {
                //TODO
                // Response.Redirect("~/Secure/Vendors/VendorProducts/Default.aspx", true);
                return RedirectToAction("Index", "Test");
            }

            // if user is an admin, then redirect to the admin dashboard
            if (_accountAgent.IsUserInRole(model.Username.Trim(), "ADMIN") && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
            {
                return RedirectToAction(DashBoardAction, DashBoardControllerName);
            }
            else if (_accountAgent.IsUserInRole(model.Username.Trim(), "FRANCHISE") && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
            {
                //TODO
                //Response.Redirect("~/franchiseAdmin/secure/default.aspx", true);
                return RedirectToAction("Index", "Test");
            }
            else if (_accountAgent.IsUserInRole(model.Username.Trim(), "CUSTOMER SERVICE REP") || _accountAgent.IsUserInRole(model.Username.Trim(), "CATALOG EDITOR") || _accountAgent.IsUserInRole(model.Username.Trim(), "EXECUTIVE") || _accountAgent.IsUserInRole(model.Username.Trim(), "ORDER ONLY") || _accountAgent.IsUserInRole(model.Username.Trim(), "SEO") || _accountAgent.IsUserInRole(model.Username.Trim(), "CONTENT EDITOR") && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
            {
                //TODO
                //Response.Redirect("~/secure/default.aspx", true);
                return RedirectToAction("Index", "Test");
            }
            TempData[MvcAdminConstants.Notifications] = GenerateNotificationMessages(ZnodeResources.ErrorAccessDenied, Znode.Engine.MvcAdmin.Models.NotificationType.error);
            _authenticationAgent.RedirectFromLoginPage(model.Username, false);
            return RedirectToAction(LoginAction);
        }

        #endregion
    }

}
