﻿using System.Web;
using System.Web.Optimization;

namespace Znode.Engine.MvcAdminNew
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery")
                .Include("~/Scripts/jquery-{version}.js")
                .Include("~/Scripts/jquery-dialog.min.js")); 

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                              "~/Scripts/jquery.unobtrusive*",
                              "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Content/bootstrap-3.3.1/js/bootstrap.min.js",
                      "~/Content/bootstrap-3.3.1/js/datepicker.js",
                      "~/Content/bootstrap-3.3.1/js/nicescroll.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundle/CustomJs")               
                .Include("~/Scripts/CustomScripts/gluten.js")                
                .Include("~/Scripts/CustomScripts/commonhelper.js")
                .Include("~/Scripts/CustomScripts/global.js")
                .Include("~/Scripts/CustomScripts/Manufacturers.js")
                .Include("~/Scripts/CustomScripts/Facet.js")
                .Include("~/Scripts/CustomScripts/api.js"));

            bundles.Add(new ScriptBundle("~/bundle/GridPagerJs")
            .Include("~/Scripts/jurl.js")
            .Include("~/Scripts/CustomScripts/GridPager.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap-3.3.1/css/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/TwoListView").Include(
                     "~/Scripts/TwoListView.js"));

            bundles.Add(new ScriptBundle("~/bundles/CommonHelper").Include(
                    "~/Scripts/CustomScripts/commonhelper.js"));

            bundles.Add(new ScriptBundle("~/bundle/Highlight").Include(
                "~/Scripts/CustomScripts/Highlight.js"));

            bundles.Add(new ScriptBundle("~/bundles/Product").Include(
                     "~/Scripts/CustomScripts/Product.js"));

            bundles.Add(new ScriptBundle("~/bundles/AttributeType").Include(
                     "~/Scripts/CustomScripts/AttributeType.js"));

            bundles.Add(new ScriptBundle("~/bundle/Promotion").Include(
                "~/Scripts/CustomScripts/Promotion.js"));

            bundles.Add(new ScriptBundle("~/bundles/portal").Include(
                    "~/Scripts/CustomScripts/Portals.js"));

            bundles.Add(new ScriptBundle("~/bundle/AssociateCategory").Include(
                "~/Scripts/CustomScripts/AssociateCategory.js"));

        }
    }
}

