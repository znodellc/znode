﻿using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Libraries.Helpers.Attributes;

namespace Znode.Engine.MvcAdminNew
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
        //    filters.Add(new ElmahHandledErrorLoggerFilter());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
