﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Znode.Engine.MvcAdminNew.Startup))]
namespace Znode.Engine.MvcAdminNew
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
