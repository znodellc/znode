﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.Extensions
{
    public static class StringExtension
    {
        // Extension method; the first parameter takes the "this" keyword and specifies the type for which the method is defined
        public static string FirstCharToUpper(this string input)
        {
            if (String.IsNullOrEmpty(input))
                throw new ArgumentException("Input string is null");

            return input.First().ToString().ToUpper() + String.Join("", input.Skip(1));
        }
        /// <summary>
        /// Converts string value to int
        /// </summary>
        /// <param name="stringValue">string value to convert</param>
        /// <returns>int value</returns>
        public static int ConvertToInt(this string stringValue)
        {
            int intValue;
            int.TryParse(stringValue, out intValue);
            return intValue;
        }
    }
}