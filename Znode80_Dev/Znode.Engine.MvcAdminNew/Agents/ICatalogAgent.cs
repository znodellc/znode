﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface ICatalogAgent
    {
        /// <summary>
        /// Gets the list of catalogs.      
        /// </summary>
        /// <param name="filters">Collection of filter</param>
        /// <param name="sortCollection">Collection of Sort</param>
        /// <param name="pageIndex">Index of page</param>
        /// <param name="recordPerPage">Records to be shown per page</param>
        /// <returns>Returns the list of catalogs.</returns>
        CatalogListViewModel GetCatalogs(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Gets the list of catalogs by catalogId.
        /// </summary>
        /// <param name="catalogId">The Id of catalog.</param>
        /// <returns>Returns the list of catalogs by catalogId.</returns>
        CatalogViewModel GetCatalog(int catalogId);

        /// <summary>
        /// Creates a new catalog.
        /// </summary>
        /// <param name="model">The model of type CatalogViewModel.</param>
        /// <returns>Returns true or false.</returns>
        bool CreateCatalog(CatalogViewModel model);

        /// <summary>
        /// Creates a copy of an existing catalog.
        /// </summary>
        /// <param name="model">The model of type CatalogViewModel.</param>
        /// <returns>Returns true or false.</returns>
        bool CopyCatalog(CatalogViewModel model);

        /// <summary>
        /// Deletes an existing catalog.
        /// </summary>
        /// <param name="model">The model of type CatalogViewModel.</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteCatalog(CatalogViewModel model);
    }
}
