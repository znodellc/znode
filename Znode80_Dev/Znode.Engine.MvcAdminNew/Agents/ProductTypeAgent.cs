﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ProductTypeAgent : BaseAgent, IProductTypeAgent
    {
        #region Private Variabls
        private readonly IProductTypeClient _ProductTypeClient;
        private readonly IProductsClient _productsClient;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for ProductTypeAgent
        /// </summary>
        public ProductTypeAgent()
        {
            _ProductTypeClient = new ProductTypeClient();
            _productsClient = new ProductsClient();
        } 
        #endregion

        #region Public Methods
        
        public ProductTypeListViewModel GetProductTypes()
        {
            var List = _ProductTypeClient.GetProductTypes(null, null, null);
            return ProductTypeViewModelMap.ToListViewModel(List.ProductTypes);
        }

        public ProductTypeViewModel GetProductType(int productTypeId)
        {
            ProductTypeViewModel productTypeViewModel = ProductTypeViewModelMap.ToViewModel(_ProductTypeClient.GetProductType(productTypeId));
            return productTypeViewModel;

        }

        public bool CreateProductType(ProductTypeViewModel viewModel)
        {
            ProductTypeModel model = ProductTypeViewModelMap.ToModel(viewModel);
            var productType = _ProductTypeClient.CreateProductType(model);

            return (productType.ProductTypeId > 0) ? true : false;
        }

        public bool DeleteProductType(int productTypeId)
        {
            var count = _productsClient.GetProducts(null,new FilterCollection(){new FilterTuple(FilterKeys.ProductTypeId, FilterOperators.Equals, productTypeId.ToString())}, null);
            return count.Products == null ? _ProductTypeClient.DeleteProductType(productTypeId) : false;
        }

        public bool UpdateProductType(ProductTypeViewModel viewModel)
        {
            var productType = _ProductTypeClient.UpdateProductType(viewModel.ProductTypeId, ProductTypeViewModelMap.ToModel(viewModel));
            return Equals(productType, null);
        }
        #endregion
    }
}