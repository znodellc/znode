﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Extensions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class FacetAgent: BaseAgent, IFacetAgent
    {
        #region Private Variables
        private readonly IFacetsClient _facetClient;
        private readonly ICatalogsClient _catalogClient;
        private readonly ICategoriesClient _categoriesClient; 
        #endregion

        #region Constructor
        public FacetAgent()
        {
            _facetClient = GetClient<FacetsClient>();
            _catalogClient = GetClient<CatalogsClient>();
            _categoriesClient = GetClient<CategoriesClient>();
        }
        #endregion

        #region Public Methods
        public FacetGroupListViewModel GetFacetGroups()
        {
            FacetGroupListViewModel facetGroupModel = new FacetGroupListViewModel();
            //Gets list of facet groups.
            var facetGroupList = _facetClient.GetFacetGroups(new ExpandCollection(), Filters, new SortCollection());

            if (!Equals(facetGroupList,null) && !Equals(facetGroupList.FacetGroups,null) && facetGroupList.FacetGroups.Count > 0)
            {
                facetGroupModel = FacetGroupViewModelMap.ToViewModels(facetGroupList.FacetGroups);
            }
            //Gets the list of all catalogs.
            var catalogs =_catalogClient.GetCatalogs(Filters, new SortCollection());
            facetGroupModel.GetCatalogs = FacetGroupViewModelMap.GetSelectListItem(catalogs.Catalogs,MvcAdminConstants.LabelCatalogId, MvcAdminConstants.LabelName, true);
            return facetGroupModel;
        }

        public FacetGroupViewModel BindFacetGroupInformation()
        {
            FacetGroupViewModel model = new FacetGroupViewModel();
            //Gets the list of all catalogs.
            model.Catalogs = FacetGroupViewModelMap.GetSelectListItem(_catalogClient.GetCatalogs(Filters, new SortCollection()).Catalogs, MvcAdminConstants.LabelCatalogId, MvcAdminConstants.LabelName);

            //Gets the list of facet group control type.
            model.TagControlType = FacetGroupViewModelMap.GetSelectListItem(_facetClient.GetFacetControlTypes(new ExpandCollection(), Filters, new SortCollection()).FacetControlTypes, MvcAdminConstants.LabelControlTypeID, MvcAdminConstants.LabelControlName);


            int catalogId = model.Catalogs[0].Value.ConvertToInt();

            //Bind categories based on the first catalog id from the catalog list.
            model.AssociateCatagoriesListViewModel = BindAssociatedCategories(catalogId);
            return model;
        }

        public FacetGroupViewModel GetAssociatedCategories(int catalogId)
        {
            FacetGroupViewModel model = new FacetGroupViewModel();

            //Bind categories based on selected catalog id.
            model.AssociateCatagoriesListViewModel = BindAssociatedCategories(catalogId);
            return model;
        }

        public bool CreateFacetGroup(FacetGroupViewModel model)
        {
            //Insert the facet group.
            var facetGroup = _facetClient.CreateFacetGroup(FacetGroupViewModelMap.ToModel(model));
            if (facetGroup.FacetGroupID > 0)
            {
                //Inserts the facet group categories for the newly created facet group.
                return _facetClient.InsertFacetGroupCategory(FacetGroupViewModelMap.ToListModel(model,facetGroup.FacetGroupID));
            }
            return false;
        }

        public FacetGroupViewModel GetFacetGroup(int? facetGroupId)
        {
            FacetGroupViewModel facetGroupModel = new FacetGroupViewModel();
           
            //Gets list of facet groups.
            var facetGroup = _facetClient.GetFacetGroup((int)facetGroupId, new ExpandCollection { ExpandKeys.Categories, ExpandKeys.Facets });

            if (!Equals(facetGroup, null))
            {
                facetGroupModel = FacetGroupViewModelMap.ToViewModel(facetGroup);
                facetGroupModel.AssociateCatagoriesListViewModel = BindAssociatedCategories((int)facetGroup.CatalogID, facetGroup.FacetGroupCategories);
                facetGroupModel.AssociatedFacets = BindAssociatedFacets(facetGroup.FacetGroupFacets);
            }
           
            return facetGroupModel;
        }

        public bool ManageFacetGroup(FacetGroupViewModel model)
        {
            try
            {
                var facetGroup = _facetClient.ManageFacetGroup(model.FacetGroupId, FacetGroupViewModelMap.ToModel(model));

                if (facetGroup.FacetGroupID > 0)
                {
                    //Delete the associated categories for the Facet group.
                    if (_facetClient.DeleteFacetGroupCategoryByFacetGroupId(facetGroup.FacetGroupID))
                    {
                        //Inserts the facet group categories for the newly created facet group.
                        return _facetClient.InsertFacetGroupCategory(FacetGroupViewModelMap.ToListModel(model, facetGroup.FacetGroupID));
                    }
                    return false;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool DeleteFacetGroup(int facetGroupId)
        {
            return  _facetClient.DeleteFacetGroup(facetGroupId);
        }

        public FacetViewModel CreateFacet(FacetViewModel model)
        {
            //Insert the facet details.
            var facet = _facetClient.CreateFacet(FacetGroupViewModelMap.ToModel(model));
            if (!Equals(facet, null) && facet.FacetID > 0)
            {
                model.FacetID=facet.FacetID;
                return model;
            }
            return null;
        }

        public FacetViewModel GetFacet(int? facetId)
        {
            FacetViewModel model = new FacetViewModel();
            var facet = _facetClient.GetFacet((int)facetId);
            if (!Equals(facet, null))
            {
                model = FacetGroupViewModelMap.ToViewModel(facet);
            }
            return model;
        }

        public FacetViewModel EditFacet(FacetViewModel model)
        {
            var facet = _facetClient.EditFacet(model.FacetID, FacetGroupViewModelMap.ToModel(model));

            if (!Equals(facet, null) && facet.FacetID > 0)
            {
                model = FacetGroupViewModelMap.ToViewModel(facet);
            }
            return model;
        }

        public bool DeleteFacet(int? facetId)
        {
            return _facetClient.DeleteFacet((int)facetId);
        }
        #endregion

        #region Private Methods
        public ListViewModel BindAssociatedCategories(int catalogId, Collection<FacetGroupCategoryModel> facetGroupCategories = null)
        {
            ListViewModel listViewModel = new ListViewModel();
            string categoryIds = string.Empty;

            if (!Equals(facetGroupCategories, null) && facetGroupCategories.Count() > 0)
            {
                //Convert Category ids in comma seperated string format.
                categoryIds = (string.Join(",", facetGroupCategories.Select(x => x.CategoryID.ToString()).ToArray()));

                //Gets All the catagories based on catalog id.
                List<SelectListItem> lstitems = FacetGroupViewModelMap.GetSelectListItem(_categoriesClient.GetCategoriesByCatalog(catalogId, new ExpandCollection(), Filters, new SortCollection()).Categories, MvcAdminConstants.LabelCategoryId, MvcAdminConstants.LabelName);
                var categoryIdsArray = categoryIds.Split(',');

                //Filter the categories, to remove already assigned categories.
                listViewModel.UnAssignedList = (from item in lstitems
                                                where !categoryIdsArray.Contains(item.Value)
                                                select item).ToList();

                //Gets the details of Associate Category.
                listViewModel.AssignedList = (string.IsNullOrEmpty(categoryIds)) 
                    ? Enumerable.Empty<SelectListItem>()
                    : (Equals(categoryIdsArray.Count(), 1))
                            ?FacetGroupViewModelMap.ToListItem(_categoriesClient.GetCategory(Convert.ToInt32(categoryIds)))
                            : FacetGroupViewModelMap.GetSelectListItem(_categoriesClient.GetCategoriesByCategoryIds(categoryIds, new ExpandCollection(), Filters, new SortCollection(), null, null).Categories, MvcAdminConstants.LabelCategoryId, MvcAdminConstants.LabelName);
                listViewModel.AssignedList.All(x => x.Selected = true);
                return listViewModel;
            }

            //Method gets associated categories based on the selected catalog id.
            listViewModel.UnAssignedList = (catalogId > 0)
                ? FacetGroupViewModelMap.GetSelectListItem(_categoriesClient.GetCategoriesByCatalog(catalogId, new ExpandCollection(), Filters, new SortCollection()).Categories, MvcAdminConstants.LabelCategoryId, MvcAdminConstants.LabelName)
                : Enumerable.Empty<SelectListItem>();
            listViewModel.AssignedList = Enumerable.Empty<SelectListItem>();
            return listViewModel;
        }

        //Method Binds the Associated Facets for the facet group.
        private FacetListViewModel BindAssociatedFacets(Collection<FacetModel> associatedFacets)
        {
            FacetListViewModel model = new FacetListViewModel();
            model.Facets = FacetGroupViewModelMap.ToViewModel(associatedFacets);
            return model;
        }
        #endregion
    }
}