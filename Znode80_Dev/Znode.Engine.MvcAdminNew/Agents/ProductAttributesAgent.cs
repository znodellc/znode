﻿using Znode.Engine.Api.Client;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.Api.Client.Filters;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ProductAttributesAgent : BaseAgent, IAttributesAgent
    {
        #region Private Variabls
        private readonly IProductAttributesClient _productAttributesClient;
        #endregion

        #region Constructors
        public ProductAttributesAgent()
        {
            _productAttributesClient = GetClient<ProductAttributesClient>();
        }
        #endregion

        #region Public Methods

        public AttributesListViewModel GetAttributes()
        {
            var list = _productAttributesClient.GetAttributes(null, null);
            return AttributesViewModelMap.ToListViewModel(list.Attributes);
        }

        public AttributesListViewModel GetAttributesByAttributeTypeId(int attributeTypeId)
        {
            var list = _productAttributesClient.GetAttributesByAttributeTypeId(attributeTypeId);
            return AttributesViewModelMap.ToListViewModel(list.Attributes);
        }

        public AttributesViewModel GetAttributesByAttributeId(int attributeId)
        {
            var attributes = _productAttributesClient.GetAttributes(attributeId);

            if (attributes == null)
            {
                return null;
            }
            var attributeViewModel = AttributesViewModelMap.ToViewModel(attributes);
            return attributeViewModel;
        }

        public bool SaveAttributes(AttributesViewModel model)
        {
            var attribute = _productAttributesClient.CreateAttributes(AttributesViewModelMap.ToModel(model));
            return (attribute.AttributeId > 0) ? true : false;
        }

        public bool UpdateAttributes(AttributesViewModel model)
        {
            var attribute = _productAttributesClient.UpdateAttributes(model.AttributeId, AttributesViewModelMap.ToModel(model));
            return (attribute.AttributeId > 0) ? true : false;
        }

        public bool DeleteAttributes(int attributeId)
        {
            return _productAttributesClient.DeleteAttributes(attributeId); ;
        }

        #endregion
    }
}