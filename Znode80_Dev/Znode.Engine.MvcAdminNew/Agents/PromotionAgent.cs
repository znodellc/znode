﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;


namespace Znode.Engine.MvcAdmin.Agents
{
    public class PromotionAgent : BaseAgent, IPromotionAgent
    {
        #region Private members
        private readonly IPromotionsClient _promotionsClient;
        private readonly IPortalsClient _portalsClient;
        private readonly IPromotionTypesClient _promotionTypeClient;
        private readonly IManufacturersClient _manufacturerClient;
        private readonly ICatalogsClient _catalogClient;
        private readonly ICategoriesClient _catagoriesClient;
        private readonly IProductsClient _productsClient;
        private readonly IProfilesClient _profileClient;
        private readonly ISkusClient _skuClient;
        #endregion

        #region Constructor

        /// <summary>
        /// Constructor for the Promotions
        /// </summary>
        public PromotionAgent()
        {
            _promotionsClient = GetClient<PromotionsClient>();
            _portalsClient = GetClient<PortalsClient>();
            _promotionTypeClient = GetClient<PromotionTypesClient>();
            _manufacturerClient = GetClient<ManufacturersClient>();
            _catalogClient = GetClient<CatalogsClient>();
            _catagoriesClient = GetClient<CategoriesClient>();
            _productsClient = GetClient<ProductsClient>();
            _profileClient = GetClient<ProfilesClient>();
            _skuClient = GetClient<SkusClient>();
        } 
        #endregion

        #region Public methods
        
        public PromotionsListViewModel GetPromotions()
        {
            var list = _promotionsClient.GetPromotions(new ExpandCollection{ ExpandKeys.Portal, ExpandKeys.Profiles, ExpandKeys.PromotionType }, null, null);           
            return PromotionViewModelMap.ToListViewModel(list.Promotions);            
        }
        
        public PromotionsViewModel GetPromotionById(int promotionId)
        {
            var promotionModel = _promotionsClient.GetPromotion(promotionId, new ExpandCollection { ExpandKeys.Portal, ExpandKeys.Profiles, ExpandKeys.PromotionType });

            if (!Equals(promotionModel, null))
            {
                return PromotionViewModelMap.ToViewModel(promotionModel);
            }
            return null;
        }

        public bool SavePromotion(PromotionsViewModel model)
        {
            _promotionsClient.CreatePromotion(PromotionViewModelMap.ToModel(model));
            return true;
        }

        public bool UpdatePromotion(PromotionsViewModel model)
        {
            var promotion = _promotionsClient.UpdatePromotion(model.PromotionId, PromotionViewModelMap.ToModel(model));               
            return true;
        }

        public bool DeletePromotion(int promotionId)
        {
            return _promotionsClient.DeletePromotion(promotionId);
        }

        public PromotionsViewModel GetPromotionInformation(PromotionsViewModel promotionsViewModel)
        {
            //Gets the list of all portals.
            promotionsViewModel.PortalList = PromotionViewModelMap.ToListItems(_portalsClient.GetPortals(Expands, Filters, new SortCollection()).Portals);

            //Gets the list of all promotionTypes.
            promotionsViewModel.PromotionTypeList = PromotionViewModelMap.ToListItems(_promotionTypeClient.GetPromotionTypes(Filters, new SortCollection()).PromotionTypes);

            //Gets the list of all manufactures.
            promotionsViewModel.ManufacturerList = PromotionViewModelMap.ToListItems(_manufacturerClient.GetManufacturers(Filters, new SortCollection()).Manufacturers);

            //Gets the list of all catalogs.
            promotionsViewModel.CatalogList = PromotionViewModelMap.ToListItems(_catalogClient.GetCatalogs(Filters, new SortCollection()).Catalogs, false);

            //Gets the list of all categories.
            promotionsViewModel.CategoryList = PromotionViewModelMap.ToListItems(_catagoriesClient.GetCategories(Expands, Filters, new SortCollection()).Categories);

            //Gets the list of all products.
            promotionsViewModel.ProductList = PromotionViewModelMap.ToListViewModel(_productsClient.GetProducts(Expands, Filters, new SortCollection()).Products);

            //Gets the list of all profiles.
            promotionsViewModel.ProfileList = PromotionViewModelMap.ToListItems(_profileClient.GetProfiles(Expands, Filters, new SortCollection()).Profiles);
            
            return promotionsViewModel;
        }
        #endregion
    }
}
