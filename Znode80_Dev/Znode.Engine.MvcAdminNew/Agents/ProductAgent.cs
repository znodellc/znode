﻿using System.Collections.Generic;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.Api.Client.Expands;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ProductAgent : BaseAgent, IProductAgent
    {
        #region Private Variables

        private readonly IProductsClient _productsClient;
        private readonly IProductTypeClient _productTypeClient;
        private readonly IManufacturersClient _manufacturersClient;
        private readonly ISuppliersClient _suppliersClient;
        private readonly IPromotionTypesClient _promotionTypesClient;
        private readonly ITaxClassesClient _taxClassesClient;
        private readonly IShippingRuleTypesClient _shippingRuleTypesClient;
        private readonly ISkusClient _skuClient;
        #endregion

        #region Constructor

        public ProductAgent()
        {
            _productsClient = GetClient<ProductsClient>();
            _productTypeClient = GetClient<ProductTypeClient>();
            _manufacturersClient = GetClient<ManufacturersClient>();
            _suppliersClient = GetClient<SuppliersClient>();
            _promotionTypesClient = GetClient<PromotionTypesClient>();
            _taxClassesClient = GetClient<TaxClassesClient>();
            _shippingRuleTypesClient = GetClient<ShippingRuleTypesClient>();
            _skuClient = GetClient<SkusClient>();
        }

        #endregion

        #region Public Methods

        public ProductListViewModel GetProducts()
        {
            var productList = _productsClient.GetProducts(null, null, null);
            if (productList != null && productList.Products != null)
            {
                return ProductViewModelMap.ToListViewModel(productList.Products);
            }
            return null;
        }

        public ProductViewModel GetProduct(int productId)
        {
            var product = _productsClient.GetProductDetailsByProductId(productId);
            if (Equals(product, null))
            {
                return null;
            }
            var productViewModel = ProductViewModelMap.ToViewModel(product.Products[0]);

            return productViewModel;
        }

        public bool IsSkuExist(string Sku, int productId)
        {
            var skuCount = _skuClient.GetSkus(null, new FilterCollection()
                                                            {
                                                                new FilterTuple(FilterKeys.Sku, FilterOperators.Equals,Sku),
                                                                 new FilterTuple(FilterKeys.ProductId, FilterOperators.NotEquals,productId.ToString())
                                                            }, null);

            return Equals(skuCount.Skus, null) ? false : true;
        }

        public bool SaveProduct(ProductViewModel model)
        {
            var product = _productsClient.CreateProduct(ProductViewModelMap.ToModel(model));
            if (product.ProductId > 0)
            {
                model.ProductId = product.ProductId;
                var sku = _skuClient.CreateSku(SkuViewModelMap.ToModel(model));
                if (!string.IsNullOrEmpty(model.ProductAttributes))
                {
                    var skuAttribute = _skuClient.AddSkuAttribute(sku.SkuId, model.ProductAttributes);
                }
            }
            return true;
        }

        public bool UpdateProduct(ProductViewModel model)
        {
            var product = _productsClient.UpdateProduct(model.ProductId, ProductViewModelMap.ToModel(model));
            if (product.ProductId > 0)
            {
                var sku = _skuClient.UpdateSku(model.SkuId, SkuViewModelMap.ToModel(model));
                if (!string.IsNullOrEmpty(model.ProductAttributes))
                {
                    var skuAttribute = _skuClient.AddSkuAttribute(sku.SkuId, model.ProductAttributes);
                }
            }
            return true;
        }

        public bool DeleteProduct(int productId)
        {
            var product = _productsClient.DeleteProduct(productId);
            return true;
        }

        public ProductViewModel BindProductPageDropdown(ProductViewModel model, int portalId)
        {
            //to sort by name
            var sort = new SortCollection();
            sort.Add(SortKeys.DisplayOrder, SortDirections.Ascending);
            model.ProductTypeList = ProductViewModelMap.ToListItems(_productTypeClient.GetProductTypes(null, null, sort).ProductTypes);

            //to sort by name
            sort = new SortCollection();
            sort.Add(SortKeys.Name, SortDirections.Ascending);

            model.ManufacturerList = ProductViewModelMap.ToListItems(_manufacturersClient.GetManufacturers(null, sort).Manufacturers);
            model.SupplierList = ProductViewModelMap.ToListItems(_suppliersClient.GetSuppliers(null, null, sort).Suppliers);
            model.TaxClassList = ProductViewModelMap.ToListItems(_taxClassesClient.GetActiveTaxClassByPortalId(portalId).TaxClasses);

            //to sort by name
            sort = new SortCollection();
            sort.Add(SortKeys.ShippingRuleId, SortDirections.Ascending);
            model.ShippingRuleTypeList = ProductViewModelMap.ToListItems(_shippingRuleTypesClient.GetShippingRuleTypes(null, sort).ShippingRuleTypes);
            model.ExpirationFrequencyList = ProductViewModelMap.ToListItems();

            return model;
        }

        public List<AttributeTypeValueViewModel> GetProductAttributesByProductTypeId(int? productTypeId)
        {
            var list = ProductViewModelMap.ToListViewModel(_productTypeClient.GetProductAttributesByProductTypeId(productTypeId.Value));
            return list;
        }


        public ProductTagsViewModel GetProductTags(int productId)
        {
            var productTags = _productsClient.GetProductTags(productId);
            return ProductTagsViewModelMap.ToViewModel(productTags);
        }

        public bool InsertProductTags(ProductTagsViewModel model)
        {
            var productTags = _productsClient.CreateProductTags(ProductTagsViewModelMap.ToModel(model));
            return productTags.TagId > 0;
            
        }

        public bool UpdateProductTags(ProductTagsViewModel model)
        {
            var productTags = _productsClient.UpdateProductTags(model.TagId, ProductTagsViewModelMap.ToModel(model));
            return productTags.TagId > 0;
        }

        public bool DeleteProductTags(int tagId)
        {
            return _productsClient.DeleteProductTags(tagId);
        }

        public ProductFacetsViewModel GetAssociatedFacetGroup(int productId)
        {
            var productFacets = _productsClient.GetAssociatedFacets(productId);
            return ProductFacetsViewModelMap.ToViewModel(productFacets);
        }

        public bool BindAssociatedFacets(ProductFacetsViewModel model)
        {
            return _productsClient.BindAssociatedFacets(ProductFacetsViewModelMap.ToModel(model));
        }

        #endregion


    }
}