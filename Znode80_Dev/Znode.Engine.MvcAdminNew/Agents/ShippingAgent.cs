﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ShippingAgent : BaseAgent , IShippingAgent
    {
        #region Private Variabls

        private readonly IShippingOptionsClient _shippingOptionsClient;
        private readonly IShippingTypesClient _shippingTypeClient;
        #endregion

        #region Constructors
        
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public ShippingAgent()
        {
            _shippingOptionsClient = GetClient<ShippingOptionsClient>();
            _shippingTypeClient = GetClient<ShippingTypesClient>();
        }

        #endregion

        #region Public Methods
        
        public ShippingOptionListViewModel GetShippingOptions()
        {
            var list = _shippingOptionsClient.GetShippingOptions(new ExpandCollection { ExpandKeys.ShippingType }, new Api.Client.Filters.FilterCollection { }, new Api.Client.Sorts.SortCollection());
            return ShippingOptionViewModelMap.ToListViewModel(list.ShippingOptions);
        }
        #endregion
    }
}