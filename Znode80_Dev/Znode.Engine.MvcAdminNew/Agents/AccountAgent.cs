﻿using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Resources;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class AccountAgent : BaseAgent, IAccountAgent
    {
        #region Private Variables
        private readonly IAccountsClient _accountClient;
        private readonly IEnvironmentConfigClient _environmentConfigClient;
        #endregion

        #region Constructor
        public AccountAgent()
        {
            _accountClient = GetClient<AccountsClient>();
            _environmentConfigClient = GetClient<EnvironmentConfigClient>();
        }
        #endregion

        #region Public Methods
        #region Login method
        /// <summary>
        ///  Login based on the username and password
        /// </summary>
        /// <param name="model">Login details as Login View Model</param>
        /// <returns>Success or not</returns>
        public LoginViewModel Login(LoginViewModel model)
        {
            AccountModel accountModel;
            LoginViewModel loginViewModel;
            try
            {
                accountModel = _accountClient.Login(AccountViewModelMap.ToLoginModel(model), GetExpands());

                //In case user requested for Reset password,
                if (accountModel.User != null && !string.IsNullOrEmpty(accountModel.User.PasswordToken))
                {
                    loginViewModel = AccountViewModelMap.ToLoginViewModel(accountModel);
                    loginViewModel.IsResetPassword = true;
                    loginViewModel.HasError = true;
                    return loginViewModel;
                }
                // Check user profiles.
                if (!accountModel.Profiles.Any())
                {
                    return new LoginViewModel() { HasError = true };
                }

                SaveInSession(MvcAdminConstants.UserAccountSessionKey, AccountViewModelMap.ToAccountViewModel(accountModel));

                //Saves the value from ZNode.Libraries.Framework.Business.ZNodeEnvironmentConfig in session.
                EnvironmentConfigModel environmentConfigModel=_environmentConfigClient.GetEnvironmentConfig();
                SaveInSession(MvcAdminConstants.EnvironmentConfigKey, environmentConfigModel);

                return AccountViewModelMap.ToLoginViewModel(accountModel);

            }
            catch (ZnodeException ex)
            {
                if (ex.ErrorCode == 1)
                {
                    accountModel = _accountClient.GetAccountByUser(model.Username);
                    SaveInSession(MvcAdminConstants.UserAccountSessionKey, accountModel);
                    SaveInSession(MvcAdminConstants.ErrorCodeSessionKey, ex.ErrorCode);
                    loginViewModel = new LoginViewModel();
                    loginViewModel.IsResetAdmin = true;
                    loginViewModel.HasError = true;
                    loginViewModel.ErrorMessage = ex.ErrorMessage;
                    return loginViewModel;
                }
                //TODO
                //if (ex.ErrorCode == 2)
                //{
                //    loginViewModel = new LoginViewModel();
                //    loginViewModel.IsResetPassword = true;
                //    loginViewModel.HasError = true;
                //    loginViewModel.ErrorMessage = ex.ErrorMessage;
                //    return loginViewModel;
                //}
                //if (ex.ErrorCode == ErrorCodes.LoginFailed || ex.ErrorCode == ErrorCodes.AccountLocked)
                //{
                //    loginViewModel = new LoginViewModel();
                //    loginViewModel.HasError = true;
                //    loginViewModel.ErrorMessage = ex.ErrorMessage;

                //    return loginViewModel;
                //}
            }

            return new LoginViewModel() { HasError = true, ErrorMessage = ZnodeResources.InvalidUserNamePassword };

        }

        #endregion

        #region Logout
        /// <summary>
        /// Logout the user
        /// </summary>
        public void Logout()
        {
            FormsAuthentication.SignOut();
            HttpContext.Current.Session.Abandon();
        }
        #endregion

        public bool IsUserInRole(string userName, string roleName)
        {
            AccountModel accountModel;
            try
            {
                accountModel = _accountClient.CheckUserRole(new AccountModel { UserName = userName, RoleName = roleName });
                return accountModel.IsUserInRole;
            }
            catch (ZnodeException ex)
            {
                return false;
            }
        }


        #endregion

        public bool CheckAccountKey()
        {
            var accountViewModel = GetFromSession<AccountModel>(MvcAdminConstants.UserAccountSessionKey);
            return (!Equals(accountViewModel, null));    
        }

        public ResetPasswordModel ResetPassword(ResetPasswordModel model)
        {
            try
            {
                if (!Equals(model, null))
                {
                    var accountViewModel = GetFromSession<AccountModel>(MvcAdminConstants.UserAccountSessionKey);
                    if (Equals(accountViewModel, null))
                    {
                        return new ResetPasswordModel() { HasError = true, ErrorMessage = ZnodeResources.ErrorResetAdminDetails };
                    }
                    var errorCode = GetFromSession<int?>(MvcAdminConstants.ErrorCodeSessionKey);

                    var accountModel = AccountViewModelMap.ToAccountModel(model);
                    accountModel.AccountId = accountViewModel.AccountId;
                    accountModel.UserId = accountViewModel.UserId;
                    accountModel.ErrorCode = errorCode.ToString();
                    var registerModel = _accountClient.ResetAdminDetails(accountModel);                   
                    return new ResetPasswordModel{ SuccessMessage = ZnodeResources.SuccessResetAdminDetails};
                }
            }
            catch (ZnodeException exception)
            {
                if (Equals(exception.ErrorCode, ErrorCodes.UserNameUnavailable))
                {
                    return new ResetPasswordModel() { HasError = true, ErrorMessage = string.Format(ZnodeResources.UserNameUnavailable, model.Username)};
                }
            }
            return new ResetPasswordModel() { HasError = true, ErrorMessage = ZnodeResources.ErrorResetAdminDetails };

        }

        /// <summary>
        /// Forgot Password method.
        /// </summary>
        /// <param name="model">AccountViewModel </param>
        /// <returns>Returns fogot password details.</returns>
        public AccountViewModel ForgotPassword(AccountViewModel model)
        {
            if (!Equals(model, null))
            {
                try
                {
                    var accountModel = _accountClient.GetAccountByUser(model.UserName.Trim(), new ExpandCollection { ExpandKeys.User });

                    if (!model.HasError && !Equals(accountModel,null))
                    {
                        if (IsUserInRole(model.UserName.Trim(), "Admin"))
                        {
                            if (Equals(model.EmailAddress, accountModel.Email))
                            {
                                model.BaseUrl = GetDomainUrl();
                                model = ResetPassword(model);
                                model.UserId = accountModel.User.UserId;
                                model.AccountId = accountModel.AccountId;
                                return model;
                            }
                            else
                            {
                                model.HasError = true;
                                model.ErrorMessage = ZnodeResources.ValidEmailAddress;
                                return model;
                            }
                        }
                        else
                        {
                            model.HasError = true;
                            model.ErrorMessage = ZnodeResources.ErrorAccessDenied;
                            return model;
                        }
                    }
                }
                catch (ZnodeException)
                {
                    model.HasError = true;
                    model.ErrorMessage = ZnodeResources.InvalidAccountInformation;
                }
            }

            return model;
        }

        /// <summary>
        /// This function will check the Reset Password Link current status.
        /// </summary>
        /// <param name="model">ChangePasswordViewModel</param>
        /// <returns>Returns Status of Reset Password Link.</returns>
        public ResetPasswordStatusTypes CheckResetPasswordLinkStatus(ChangePasswordViewModel model)
        {
            try
            {
                string errorCode = string.Empty;
                var data = _accountClient.CheckResetPasswordLinkStatus(AccountViewModelMap.ToChangePasswordModel(model));
                return ResetPasswordStatusTypes.NoRecord;
            }
            catch (ZnodeException ex)
            {
                switch (ex.ErrorCode)
                {
                    case ErrorCodes.ResetPasswordContinue:
                        return ResetPasswordStatusTypes.Continue;
                    case ErrorCodes.ResetPasswordLinkExpired:
                        return ResetPasswordStatusTypes.LinkExpired;
                    case ErrorCodes.ResetPasswordNoRecord:
                        return ResetPasswordStatusTypes.NoRecord;
                    case ErrorCodes.ResetPasswordTokenMismatch:
                        return ResetPasswordStatusTypes.TokenMismatch;
                    default:
                        return ResetPasswordStatusTypes.NoRecord;
                }
            }
        }
        
        /// <summary>
        /// Used to change the password
        /// </summary>
        /// <param name="model">Old password and new password as Change password view model</param>
        /// <returns>Return true or false if password is changed or not</returns>
        public ChangePasswordViewModel ChangePassword(ChangePasswordViewModel model)
        {
            try
            {
                _accountClient.ChangePassword(AccountViewModelMap.ToChangePasswordModel(model));

                return new ChangePasswordViewModel
                {
                    SuccessMessage = ZnodeResources.SuccessPasswordChanged,
                };
            }
            catch (ZnodeException)
            {
                return new ChangePasswordViewModel
                {
                    HasError = true,
                    ErrorMessage = ZnodeResources.ErrorChangePassword,
                };
            }
        }


        #region Private Methods
        #region GetExpands
        /// <summary>
        /// Returns the Expands needed for the account agent.
        /// </summary>
        /// <returns></returns>
        private ExpandCollection GetExpands()
        {
            return new ExpandCollection()
                {
                    ExpandKeys.Addresses,
                    ExpandKeys.Profiles,
                    ExpandKeys.WishLists,
                    ExpandKeys.Orders,
                    ExpandKeys.OrderLineItems,
                    ExpandKeys.User,
                    ExpandKeys.GiftCardHistory
                };
        }
        #endregion

        #region GetDomainUrl
        private string GetDomainUrl()
        {
            return (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority))) ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) : string.Empty;
        }
        #endregion

          /// <summary>
        /// Reset password method
        /// </summary>
        /// <param name="model">AccountViewModel</param>
        /// <returns>Returns password changed successfully or not</returns>
        private AccountViewModel ResetPassword(AccountViewModel model)
        {
            try
            {
                _accountClient.ResetPassword(AccountViewModelMap.ToAccountModel(model));
                model.SuccessMessage = Resources.ZnodeResources.SuccessResetPassword;
            }
            catch (ZnodeException ex)
            {
                model.HasError = true;
                model.ErrorMessage = ex.ErrorMessage;
            }
            return model;
        }
        #endregion
    }
}