﻿using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IDomainAgent
    {
        /// <summary>
        /// Get all the domains from specified portal id
        /// </summary>
        /// <param name="portalId">portal id</param>
        /// <returns>gets list of domains</returns>
        DomainListViewModel GetDomains(int portalId);

        /// <summary>
        /// Create New Domain Url.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool createDomainUrl(DomainViewModel model);

        /// <summary>
        /// Get Existing Domain by portal id.
        /// </summary>
        /// <param name="portalId">portal id.</param>
        /// <returns>Returns DomainViewModel</returns>
        DomainViewModel GetDomain(int portalId);

        /// <summary>
        /// Update Existing Domain url.
        /// </summary>
        /// <param name="viewModel">DomainViewModel</param>
        /// <returns>Returns bool.</returns>
        bool UpdateDomainUrl(DomainViewModel viewModel);

        /// <summary>
        /// Delete Existing Domain Url by domain Id.
        /// </summary>
        /// <param name="domainId">domain Id.</param>
        /// <returns>Returns bool</returns>
        bool DeleteDomainUrl(int domainId);
    }
}
