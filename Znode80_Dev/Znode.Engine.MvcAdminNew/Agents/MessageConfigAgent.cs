﻿using Znode.Engine.Api.Client;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcDemo.Maps;
using System.Web.Mvc;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.MvcAdmin.Helpers;
using Resources;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class MessageConfigAgent : BaseAgent, IMessageConfigAgent
    {
        #region Private Variables

        private readonly IMessageConfigsClient _messageConfigClient;
        private readonly IPortalsClient _portalClient;
        private readonly IPortalAgent _portalAgent;
        #endregion

        #region Constructors

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public MessageConfigAgent()
        {
            _messageConfigClient = GetClient<MessageConfigsClient>();
            _portalClient = GetClient<PortalsClient>();
            _portalAgent = new PortalAgent();
        }

        #endregion

        #region Public Methods
       
        public ManageMessageListViewModel GetMessageConfigs()
        {
            //Get Message
            var _messageList = _messageConfigClient.GetMessageConfigs(null, null, null);

            //Get Portals
            var storeList = _portalClient.GetPortals(null, null, null);
            
            ManageMessageListViewModel manageViewModels = ManageMessageViewModelMap.ToListViewModel(_messageList.MessageConfigs);

            foreach (var manageMessageViewModel in manageViewModels.ManageMessage)
            {
                if (manageMessageViewModel.PortalId > 0)
                    manageMessageViewModel.PortalName = this.GetPortalName(manageMessageViewModel.PortalId);
            }

            manageViewModels.Portal = PortalViewModelMap.ToListViewModel(storeList,null,null).Portals;

            return manageViewModels;
        }       
       
        public ManageMessageViewModel GetMessageConfig(int messageConfigId)
        {
            ManageMessageViewModel manageMessageViewModel = ManageMessageViewModelMap.ToViewModel(_messageConfigClient.GetMessageConfig(messageConfigId));
            return manageMessageViewModel;
        }
       
        public bool DeleteMessageConfig(int messageConfigId)
        {
            return _messageConfigClient.DeleteMessageConfig(messageConfigId);
        }
        
        public bool SaveMessageConfig(ManageMessageViewModel model)
        {
            var message = _messageConfigClient.CreateMessageConfig(ManageMessageViewModelMap.ToModel(model));
            return (message.MessageConfigId > 0) ? true : false;
        }
       
        public bool UpdateMessageConfig(int messageConfigId,ManageMessageViewModel model)
        {
            var message = _messageConfigClient.UpdateMessageConfig(model.MessageConfigId, ManageMessageViewModelMap.ToModel(model));
            return true;
        }

        public List<SelectListItem> GetAllBannerKeys()
        {
            List<SelectListItem> lstBannerKeys=new List<SelectListItem>();

            //Set the filter condition for banner.
            Filters = new FilterCollection { { FilterKeys.MessageTypeId, FilterOperators.Equals, ((int)MessageType.Banner).ToString() } };

            //Get all the banners.
            var messageList = _messageConfigClient.GetMessageConfigs(null, Filters, null);

            if (!Equals(messageList, null) && !Equals(messageList.MessageConfigs, null) && messageList.MessageConfigs.Count > 0)
            {
                //Gets the distinct keys from all banner to be display in dropdown.
                var bannerKeys = (from item in messageList.MessageConfigs
                                  where !string.IsNullOrEmpty(item.Key)
                                  select item.Key).Distinct().ToList();

                lstBannerKeys = (from item in bannerKeys
                                 select new SelectListItem
                                 {
                                     Text = item,
                                     Value = item,
                                 }).ToList();
            }
            //Insert default items in Location drop down. 
            if (lstBannerKeys.Count > 0)
            {
                lstBannerKeys.Insert(0, new SelectListItem { Text = ZnodeResources.LabelBannerDoNotShowText, Value = MvcAdminConstants.BannerDoNotShowValue });
                lstBannerKeys.Add(new SelectListItem { Text = ZnodeResources.LabelBannerAddNewLocationText, Value = MvcAdminConstants.BannerAddNewLocationValue });
            }
            
            return lstBannerKeys;
        }


        public ManageMessageListViewModel GetAllBanners()
        {
            //Set the Filter
            Filters = new FilterCollection { { FilterKeys.MessageTypeId, FilterOperators.Equals, ((int)MessageType.Banner).ToString() } };

            //Get All the Banners
            var messageList = _messageConfigClient.GetMessageConfigs(null, Filters, null);

            if (Equals(messageList.MessageConfigs, null)) return new ManageMessageListViewModel();
           
            //Get Portals to display in dropdown
            var storeList = _portalClient.GetPortals(null, null, null);

            ManageMessageListViewModel manageViewModels = ManageMessageViewModelMap.ToListViewModel(messageList.MessageConfigs);

            //Filter store list based on Portal id to bind store name in list grid.
            if (!Equals(manageViewModels, null) && !Equals(manageViewModels.ManageMessage, null))
            {
                foreach (var manageMessageViewModel in manageViewModels.ManageMessage)
                {
                    if (manageMessageViewModel.PortalId > 0 && !Equals(storeList.Portals, null) && storeList.Portals.Count > 0)
                        manageMessageViewModel.PortalName = storeList.Portals.ToList().Find(x => Equals(x.PortalId, manageMessageViewModel.PortalId)).StoreName;
                }
                manageViewModels.Portal = PortalViewModelMap.ToListViewModel(storeList, null, null).Portals;
            }
            return manageViewModels;
        }


        public bool CreateBanner(ManageBannerViewModel model)
        {
            //Create the banner & return its details.
            var message = _messageConfigClient.CreateMessageConfig(ManageBannerViewModelMap.ToModel(model));
            return (!Equals(message, null) && message.MessageConfigId > 0);
        }

        public ManageBannerViewModel GetBanner(int messageId)
        {
            //Get the banner based on messageId.
            
            ManageBannerViewModel model = ManageBannerViewModelMap.ToViewModel(_messageConfigClient.GetMessageConfig(messageId));
            
            //Bind all portals.
            model.Portal = GetPortals(model.PortalID);

            //Bind all banner keys, if exists.
            model.BannerKeys = GetAllBannerKeys();
            model.BannerKeys.All(x => x.Selected = Equals(x.Value, model.Key));
            return model;
        }

        public bool UpdateBanner(int messageId, ManageBannerViewModel model)
        {
            //Updates the banner & return its details.
            var banner = _messageConfigClient.UpdateMessageConfig(messageId, ManageBannerViewModelMap.ToModel(model));
            return (!Equals(banner,null) && banner.MessageConfigId > 0);
        }

        public List<SelectListItem> GetPortals(int portalId = 0)
        {
            //Get the portals & Map with view model.
            return ManageBannerViewModelMap.GetSelectListItem(_portalAgent.GetPortals(null, null).Portals.ToList(), portalId);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Get portal name
        /// </summary>
        /// <param name="portalId">portalId</param>
        /// <returns>Returns portalName</returns>
        private string GetPortalName(int portalId)
        {
            string portalName = _portalClient.GetPortal(portalId).StoreName;
            return portalName;
        } 

        #endregion
    }
}