﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for the Manufacturer or brands
    /// </summary>
    public interface IManufacturersAgent
    {
        ManufacturersListViewModel GetManufacturers(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);
        ManufacturersViewModel GetManufacturerByID(int manufacturerID);
        bool SaveManufacturer(ManufacturersViewModel model);
        bool UpdateManufacturer(ManufacturersViewModel model);
        bool DeleteManufactuere(int manufacturerID);
    }
}