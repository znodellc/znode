﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class SupplierAgent : BaseAgent, ISupplierAgent
    {
        #region Private Variables

        private readonly ISuppliersClient _suppliersClient;
        private readonly IAccountsClient _accountClient;
        private readonly IAddressesClient _addressClient;

        #endregion

        #region Constructor

        public SupplierAgent()
        {
            _suppliersClient = GetClient<SuppliersClient>();
            _accountClient = GetClient<AccountsClient>();
            _addressClient = GetClient<AddressesClient>();
        }

        #endregion

        #region Public Methods
        public bool CreateVendorAccount(VendorAccountViewModel vendorAccountViewModel)
        {
            if (vendorAccountViewModel != null)
            {
                _accountClient.CreateAccount(VendorViewModelMap.ToModel(vendorAccountViewModel));
                SupplierViewModel supplierViewModel = new SupplierViewModel();
                supplierViewModel.ContactEmail = vendorAccountViewModel.Email;
            }
            return true;
        }

        public bool DeleteVender(int VenderId)
        {
            var product = _suppliersClient.DeleteSupplier(VenderId);
            return true;
        } 
        #endregion
    }
}