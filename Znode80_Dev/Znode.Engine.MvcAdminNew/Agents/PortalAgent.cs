﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Extensions;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcDemo.Maps;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class PortalAgent : BaseAgent, IPortalAgent
    {
        #region Private Variables
        private readonly IPortalsClient _portalsClient;
        private readonly IDomainsClient _domainClient;
        private readonly ICatalogsClient _catalogClient;
        private readonly IThemeClient _themeClient;
        private readonly ICSSClient _cssClient;
        private readonly IOrderStateClient _orderStateClient;
        private readonly IProductReviewStateClient _productReviewStateClient;
        private readonly ILocaleClient _localeClient;
        private readonly IPortalCountriesClient _portalCountriesClient;
        private readonly IPortalCatalogsClient _portalCatalogsClient;
        private readonly IContentPageClient _contentPageClient;
        private readonly IMessageConfigsClient _messageConfigClient;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for portal agent.
        /// </summary>
        public PortalAgent()
        {
            _portalsClient = GetClient<PortalsClient>();
            _domainClient = GetClient<DomainsClient>();
            _catalogClient = GetClient<CatalogsClient>();
            _themeClient = GetClient<ThemeClient>();
            _cssClient = GetClient<CSSClient>();
            _orderStateClient = GetClient<OrderStateClient>();
            _productReviewStateClient = GetClient<ProductReviewStateClient>();
            _localeClient = GetClient<LocaleClient>();
            _portalCountriesClient = GetClient<PortalCountriesClient>();
            _portalCatalogsClient = GetClient<PortalCatalogsClient>();
            _contentPageClient = GetClient<ContentPageClient>();
            _messageConfigClient = GetClient<MessageConfigsClient>();
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the current portal.
        /// </summary>
        public static PortalViewModel CurrentPortal
        {
            get
            {
                if (HttpContext.Current.Cache[HttpContext.Current.Request.Url.Authority] == null)
                {
                    var agent = new PortalAgent();
                    var model = agent.GetCurrentPortal();

                    if (model != null)
                    {
                        HttpContext.Current.Cache.Insert(HttpContext.Current.Request.Url.Authority, model, null, DateTime.Now.AddMinutes(5),
                                                         Cache.NoSlidingExpiration);
                    }
                }

                return HttpContext.Current.Cache[HttpContext.Current.Request.Url.Authority] as PortalViewModel;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Deletes a portal specified by the portal id.
        /// </summary>
        /// <param name="portalId">Portal Id.</param>
        /// <returns>Bool value whether the Portal is deleted or not.</returns>
        public bool DeletePortal(int portalId)
        {
            return _portalsClient.DeletePortalByPortalId(portalId);
        }

        /// <summary>
        /// Gets the list of portals.
        /// </summary>
        /// <param name="filters">Filter to be applied on the Portal List.</param>
        /// <param name="sortCollection">Sorting of the portal List.</param>
        /// <param name="pageIndex">Start page index of the portal.</param>
        /// <param name="recordPerPage">Records count per page.</param>
        /// <returns></returns>
        public PortalListViewModel GetPortals(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            PortalListViewModel portalListViewModel = new PortalListViewModel();
            var list = _portalsClient.GetPortals(new ExpandCollection { ExpandKeys.Catalog, ExpandKeys.PortalCountries }, filters, sortCollection, pageIndex - 1, recordPerPage);

            string sort = string.Empty;
            string sortDir = string.Empty;

            if (!Equals(sortCollection, null))
            {
                sort = sortCollection["sort"];
                sortDir = sortCollection["sortDir"];

                if (!Equals(list.Portals, null))
                {
                    list.Portals = (from item in list.Portals
                                    select item).AsQueryable().OrderBy(sort, sortDir, null).ToList().ToCollection();
                }
            }

            portalListViewModel = PortalViewModelMap.ToListViewModel(list, pageIndex, recordPerPage);

            foreach (var portal in portalListViewModel.Portals)
            {
                portal.DomainName = this.GetPortalDomain(portal.PortalId);
            }


            return portalListViewModel;
        }

        /// <summary>
        /// Get all portals
        /// </summary>
        /// <returns>Returns list of portals</returns>
        public PortalListViewModel GetAllPortals()
        {
            PortalListViewModel portalListViewModel = new PortalListViewModel();
            var list = _portalsClient.GetPortals(null, null, null);

            if (!Equals(list.Portals, null) || list.Portals.Count > 0)
            {
                portalListViewModel = PortalViewModelMap.ToListViewModel(list, null, null);

                foreach (var portal in portalListViewModel.Portals)
                {
                    portal.DomainName = this.GetPortalDomain(portal.PortalId);
                }
            }

            return portalListViewModel;
        }

        /// <summary>
        /// Binds the portal informations.
        /// </summary>
        /// <returns>returns select list items.</returns>
        public PortalViewModel BindPortalInformation(PortalViewModel portalViewModel)
        {
            portalViewModel.Catalogs = PortalViewModelMap.ToSelectListItems(_catalogClient.GetCatalogs(Filters, new SortCollection()).Catalogs);
            portalViewModel.Themes = PortalViewModelMap.ToSelectListItems(_themeClient.GetThemes(Filters, new SortCollection()).Themes);
            portalViewModel.CSS = this.BindCssList();
            portalViewModel.OrderState = PortalViewModelMap.ToSelectListItems(_orderStateClient.GetOrderStates(Expands, Filters, new SortCollection()).OrderStates);
            portalViewModel.ProductReviewState = PortalViewModelMap.ToSelectListItems(_productReviewStateClient.GetProductReviewStates(Expands, Filters, new SortCollection()).ProductReviewStates);
            portalViewModel.DefaultReviewStates = this.GetCustomerReviewState();
            portalViewModel.Locales = PortalViewModelMap.ToSelectListItems(_localeClient.GetLocales(Expands, Filters, new SortCollection()).Locales);
            return portalViewModel;
        }

        /// <summary>
        /// Creates portal.
        /// </summary>
        /// <param name="portalViewModel">View model from the view.</param>
        /// <param name="notificationMessage">notification message.</param>
        /// <returns></returns>
        public bool CreatePortal(PortalViewModel portalViewModel, out string notificationMessage)
        {
            notificationMessage = string.Empty;
            bool isPortalCreated = false;
            int localeId = 0;

            EnvironmentConfigModel environmentConfig = GetFromSession<EnvironmentConfigModel>(MvcAdminConstants.EnvironmentConfigKey);

            if (Equals(environmentConfig, null) || !environmentConfig.MultiStoreAdminEnabled)
            {
                notificationMessage = "This functionality is not available in the Single Store edition. Please upgrade your software to the Multi-Store edition to enable this functionality.";
                return false;
            }

            this.GetFedexKeys(portalViewModel);

            if (portalViewModel.LocaleId.Equals(null))
            {
                localeId = Helpers.MvcAdminConstants.LocaleId;
            }
            else
            {
                localeId = portalViewModel.LocaleId;
            }

            this.SetDefaultValues(portalViewModel);

            this.SetValuesFromExistingPortal(portalViewModel);

            string logofile = portalViewModel.LogoPathImageName;
            string logoPath = string.Empty;
            if (!Equals(portalViewModel.LogoPathImageName, null) && !Equals(environmentConfig, null))
            {
                logoPath = this.GetLogoPath(portalViewModel.LogoPathImageName, environmentConfig.OriginalImagePath);
                if (string.IsNullOrEmpty(logoPath))
                {
                    notificationMessage = "Select a valid jpg, gif or png image.";
                    return false;
                }
                else
                {
                    portalViewModel.LogoPathImageName = logoPath;
                }
            }
            else
            {
                notificationMessage = "Please select Logo image.";
                return false;
            }

            PortalViewModel portal = PortalViewModelMap.ToViewModel(_portalsClient.CreatePortal(PortalViewModelMap.ToModel(portalViewModel)));
            isPortalCreated = portal.PortalId > 0 ? true : false;
            portalViewModel.PortalId = portal.PortalId;

            if (isPortalCreated)
            {
                if (!Equals(environmentConfig, null))
                {
                    this.UpdateLogoPath(portalViewModel, logofile, environmentConfig.OriginalImagePath);
                }
                else
                {
                    return false;
                }
                isPortalCreated = !Equals(_portalsClient.UpdatePortal(portalViewModel.PortalId, PortalViewModelMap.ToModel(portalViewModel)), null) ? true : false;

                isPortalCreated = this.CreatePortalCountry(portalViewModel, isPortalCreated);

                this.DeleteExistingPortalCatalogMapping(portalViewModel);

                PortalCatalogModel portalCatalogModel = this.CreatePortalCatalog(portalViewModel, localeId);

                this.SetDefaultContentPage(portalViewModel, portalCatalogModel);

                _portalsClient.CreateMessage(portalViewModel.PortalId, portalViewModel.LocaleId);

                return true;
            }
            return false;
        }

        /// <summary>
        /// Copy stores.
        /// </summary>
        /// <param name="portalId">Portal id of the store to be copied.</param>
        /// <returns>Bool Value if the store is copied or not.</returns>
        public bool CopyStore(int portalId)
        {
            if (portalId > 0)
            {
                return _portalsClient.CopyStore(portalId);
            }
            return false;
        }

        public List<SelectListItem> BindCssList(FilterCollection filters = null, SortCollection sortCollection = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (!Equals(filters, null))
            {
                CSSListModel cssModel = _cssClient.GetCSSs(null, filters, sortCollection);
                return PortalViewModelMap.ToSelectListItems(cssModel.CSSs);
            }
            return list;
        }
        #endregion

        #region Private Method
        /// <summary>
        /// Gets the current portal.
        /// </summary>
        /// <returns>PortalViewModel</returns>
        private PortalViewModel GetCurrentPortal()
        {
            var list =
                _domainClient.GetDomains(
                    new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.DomainName, FilterOperators.Equals,
                                            HttpContext.Current.Request.Url.Authority)
                        }, new SortCollection());

            if (list.Domains.Any())
            {
                var portal = _portalsClient.GetPortal(list.Domains.First().PortalId, new ExpandCollection() { ExpandKeys.PortalCatalogs, ExpandKeys.Catalogs });
                return PortalViewModelMap.ToViewModel(portal);
            }
            return null;
        }

        /// <summary>
        /// Gets domain according to portal id
        /// </summary>
        /// <param name="id">portal id</param>
        /// <returns>protal domain's name</returns>
        private string GetPortalDomain(int id)
        {
            string portalUrl = string.Empty;
            DomainAgent domainAgent = new DomainAgent();
            DomainListViewModel domainList = domainAgent.GetDomains(id);
            if (!Equals(domainList, null))
            {
                foreach (var domain in domainList.Domains)
                {
                    if (domain.IsActive)
                    {
                        portalUrl = domain.DomainName;
                        break;
                    }
                }
            }
            return portalUrl;
        }

        /// <summary>
        /// Get List of select list items for customer review states.
        /// </summary>
        /// <returns>List of Select List items.</returns>
        private List<SelectListItem> GetCustomerReviewState()
        {
            List<SelectListItem> customerReviewState = new List<SelectListItem>(){
                new SelectListItem(){Text="Publish Immediately", Value="A"},
                new SelectListItem(){Text="Do Not Publish. Require Moderator Approval",Value="N"}
            };
            return customerReviewState;
        }

        /// <summary>
        /// Gets default fedex keys.
        /// </summary>
        /// <param name="portalViewModel">portalViewModel</param>
        private void GetFedexKeys(PortalViewModel portalViewModel)
        {
            // Get Fedex Key and Password from Existing Portal
            DataSet ds = _portalsClient.GetFedexKeys();
            if (!Equals(ds, null) && !Equals(ds.Tables, null) && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                portalViewModel.FedExCspKey = ds.Tables[0].Rows[0]["FedExCSPKey"].ToString();
                portalViewModel.FedExCspPassword = ds.Tables[0].Rows[0]["FedExCSPPassword"].ToString();
                portalViewModel.FedExClientProductVersion = ds.Tables[0].Rows[0]["FedExClientProductVersion"].ToString();
                portalViewModel.FedExClientProductId = ds.Tables[0].Rows[0]["FedExClientProductId"].ToString();
            }
        }

        /// <summary>
        /// Sets the default value from existing portal.
        /// </summary>
        /// <param name="portalViewModel">portalViewModel</param>
        private void SetValuesFromExistingPortal(PortalViewModel portalViewModel)
        {
            PortalViewModel existingPortal = PortalViewModelMap.ToViewModel(_portalsClient.GetPortal(this.GetCurrentPortal().PortalId));

            if (!Equals(existingPortal, null))
            {
                portalViewModel.SmtpPort = existingPortal.SmtpPort;
                portalViewModel.WeightUnit = existingPortal.WeightUnit;
                portalViewModel.DimensionUnit = existingPortal.DimensionUnit;

                portalViewModel.CurrencyTypeId = existingPortal.CurrencyTypeId;
                portalViewModel.MaxCatalogCategoryDisplayThumbnails = existingPortal.MaxCatalogCategoryDisplayThumbnails;
                portalViewModel.MaxCatalogDisplayColumns = existingPortal.MaxCatalogDisplayColumns;

                portalViewModel.MaxCatalogDisplayItems = existingPortal.MaxCatalogDisplayItems;
                portalViewModel.MaxCatalogItemCrossSellWidth = existingPortal.MaxCatalogItemCrossSellWidth;
                portalViewModel.MaxCatalogItemLargeWidth = existingPortal.MaxCatalogItemLargeWidth;

                portalViewModel.MaxCatalogItemMediumWidth = existingPortal.MaxCatalogItemMediumWidth;
                portalViewModel.MaxCatalogItemSmallThumbnailWidth = existingPortal.MaxCatalogItemSmallThumbnailWidth;
                portalViewModel.MaxCatalogItemSmallWidth = existingPortal.MaxCatalogItemSmallWidth;

                portalViewModel.MaxCatalogItemThumbnailWidth = existingPortal.MaxCatalogItemThumbnailWidth;
                portalViewModel.ImageNotAvailablePath = existingPortal.ImageNotAvailablePath;
                portalViewModel.ShopByPriceMax = existingPortal.ShopByPriceMax;

                portalViewModel.ShopByPriceMin = existingPortal.ShopByPriceMin;
                portalViewModel.ShopByPriceIncrement = existingPortal.ShopByPriceIncrement;
            }
        }

        /// <summary>
        /// Sets default view model to the portal to be created.
        /// </summary>
        /// <param name="portalViewModel">portalViewModel</param>
        private void SetDefaultValues(PortalViewModel portalViewModel)
        {
            portalViewModel.IsActive = true;
            portalViewModel.FedExAccountNumber = string.Empty;
            portalViewModel.FedExAddInsurance = false;

            portalViewModel.FedExProductionKey = string.Empty;
            portalViewModel.FedExSecurityCode = string.Empty;
            //portalViewModel.InclusiveTax = false;

            portalViewModel.SmtpPassword = string.Empty;
            portalViewModel.SmtpServer = string.Empty;
            portalViewModel.SmtpUsername = string.Empty;

            portalViewModel.SiteWideAnalyticsJavascript = string.Empty;
            portalViewModel.SiteWideBottomJavascript = string.Empty;
            portalViewModel.SiteWideTopJavascript = string.Empty;

            portalViewModel.OrderReceiptAffiliateJavascript = string.Empty;
            portalViewModel.GoogleAnalyticsCode = string.Empty;
            portalViewModel.UpsKey = string.Empty;

            portalViewModel.UpsPassword = string.Empty;
            portalViewModel.UpsUsername = string.Empty;
            portalViewModel.MobileTheme = "Mobile";
        }

        /// <summary>
        /// Sets logo path for the portal.
        /// </summary>
        /// <param name="fileName">File name provided by the user.</param>
        /// <returns>Logo path of the Portal.</returns>
        private string GetLogoPath(string fileName, string originalImagePath = "")
        {
            // Set logo path
            string filePath = string.Empty;
            // Check for Product Image
            filePath = System.IO.Path.GetFileNameWithoutExtension(fileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(fileName);
            string fileExtension = string.Empty;
            fileExtension = System.IO.Path.GetExtension(fileName);

            if (fileName != string.Empty)
            {
                if ((fileExtension.ToLower() == ".jpeg") || (fileExtension.ToLower().Equals(".jpg")) || (fileExtension.ToLower().Equals(".png")) || (fileExtension.ToLower().Equals(".gif")))
                {
                    filePath = originalImagePath + fileName;
                }
            }
            return filePath;
        }

        /// <summary>
        /// Sets Default Content Pages for the portal.
        /// </summary>
        /// <param name="portalViewModel">portalViewModel</param>
        /// <param name="portalCatalogModel">portalCatalogModel</param>
        private void SetDefaultContentPage(PortalViewModel portalViewModel, PortalCatalogModel portalCatalogModel)
        {
            // Get Exisitng Portal Settings By portalID
            PortalViewModel existingPortal = PortalViewModelMap.ToViewModel(_portalsClient.GetPortal(this.GetCurrentPortal().PortalId));

            if (!Equals(existingPortal, null))
            {
                ContentPageListModel contentPageListModel = new ContentPageListModel();
                FilterCollection contentPageFilters = new FilterCollection();
                contentPageFilters.Add(FilterKeys.PortalId, FilterOperators.Equals, existingPortal.PortalId.ToString());
                contentPageFilters.Add(FilterKeys.LocaleId, FilterOperators.Equals, existingPortal.LocaleId.ToString());
                contentPageListModel = _contentPageClient.GetContentPages(Expands, contentPageFilters, new SortCollection());

                ContentPageModel contentPageModel = new ContentPageModel();
                foreach (ContentPageModel contentPage in contentPageListModel.ContentPages)
                {
                    contentPageModel = _contentPageClient.CopyContentPage(contentPage) as ContentPageModel;
                    contentPageModel.ContentPageID = -1;
                    contentPageModel.PortalID = portalViewModel.PortalId;
                    contentPageModel.LocaleId = portalCatalogModel.LocaleId;
                    contentPageModel.SEOURL = null;
                    contentPageModel.ThemeID = portalCatalogModel.ThemeId;
                    contentPageModel.Html = string.Empty;
                    contentPageModel.UpdatedUser = HttpContext.Current.User.Identity.Name;
                    contentPageModel.IsUrlRedirectEnabled = false;
                    contentPageModel.MappedSeoUrl = null;
                    _contentPageClient.AddPage(contentPageModel);
                }
            }
        }

        /// <summary>
        /// Creates entry for portal catalog mapping.
        /// </summary>
        /// <param name="portalViewModel"></param>
        /// <param name="localeId"></param>
        /// <returns></returns>
        private PortalCatalogModel CreatePortalCatalog(PortalViewModel portalViewModel, int localeId)
        {
            PortalCatalogModel portalCatalogModel = new PortalCatalogModel();
            // Add the new Selection
            portalCatalogModel.PortalId = portalViewModel.PortalId;
            portalCatalogModel.CatalogId = portalViewModel.CatalogId ?? 0;
            portalCatalogModel.ThemeId = portalViewModel.ThemeId;
            portalCatalogModel.CssId = portalViewModel.CSSId;
            portalCatalogModel.LocaleId = localeId;
            PortalCatalogModel portalcatalog = _portalCatalogsClient.CreatePortalCatalog(portalCatalogModel);
            return portalCatalogModel;
        }

        /// <summary>
        /// Deletes portal catalog mapping.
        /// </summary>
        /// <param name="portalViewModel"></param>
        private void DeleteExistingPortalCatalogMapping(PortalViewModel portalViewModel)
        {
            FilterCollection portalCatalogFilterCollection = new FilterCollection();
            portalCatalogFilterCollection.Add(FilterKeys.PortalId, FilterOperators.Equals, portalViewModel.PortalId.ToString());

            PortalCatalogListModel portalCatalogListModel = _portalCatalogsClient.GetPortalCatalogs(Expands, portalCatalogFilterCollection, new SortCollection());

            if (!Equals(portalCatalogListModel.PortalCatalogs, null))
            {
                foreach (PortalCatalogModel portalCatalog in portalCatalogListModel.PortalCatalogs)
                {
                    _portalCatalogsClient.DeletePortalCatalog(portalCatalog.PortalCatalogId);
                }
            }
        }

        /// <summary>
        /// Created portal country mapping.
        /// </summary>
        /// <param name="portalViewModel"></param>
        /// <param name="isPortalCreated"></param>
        /// <returns></returns>
        private bool CreatePortalCountry(PortalViewModel portalViewModel, bool isPortalCreated)
        {
            PortalCountryModel portalCountry = new PortalCountryModel();
            // Add PortalCountry List
            portalCountry.PortalId = portalViewModel.PortalId;
            portalCountry.IsBillingActive = true;
            portalCountry.IsShippingActive = true;
            portalCountry.CountryCode = "US";
            isPortalCreated = (_portalCountriesClient.CreatePortalCountry(portalCountry).PortalCountryId > 0) ? true : false;
            return isPortalCreated;
        }

        /// <summary>
        /// Updates portal's logo path.
        /// </summary>
        /// <param name="portalViewModel">New portal created.</param>
        /// <param name="logoPath"></param>
        private void UpdateLogoPath(PortalViewModel portalViewModel, string logofile, string originalImagePath = "")
        {          
            PortalViewModel store = null;
            store = PortalViewModelMap.ToViewModel(_portalsClient.GetPortal(portalViewModel.PortalId));
            portalViewModel.LogoPathImageName = originalImagePath + "Turnkey/" + portalViewModel.PortalId + "/" + logofile;
            ////portalViewModel.LogoPath.SaveAs(HelperMethods.GetImagePath(portalViewModel.LogoPathImageName));
            //byte[] imageData1 = new byte[portalViewModel.LogoPath.InputStream.Length];
            //portalViewModel.LogoPath.InputStream.Read(imageData1, 0, (int)portalViewModel.LogoPath.InputStream.Length);
            //ZNodeStorageManager.WriteBinaryStorage(imageData1, HelperMethods.GetImagePath(portalViewModel.LogoPathImageName));
        }
        #endregion
    }
}



