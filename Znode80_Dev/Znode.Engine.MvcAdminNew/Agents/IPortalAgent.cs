﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for Portal Agent
    /// </summary>
    public interface IPortalAgent
    {
        /// <summary>
        /// Delete portal on the basis of portal id.
        /// </summary>
        /// <param name="portalId">portalId</param>
        /// <returns>Returns bool</returns>
        bool DeletePortal(int portalId);

        /// <summary>
        /// Gets a list of all portal in sorted order
        /// </summary>
        /// <param name="filters">filters</param>
        /// <param name="sortCollection">sortCollection</param>
        /// <param name="pageIndex">pageIndex</param>
        /// <param name="recordPerPage">recordPerPage</param>
        /// <returns>Returns PortalListViewModel</returns>
        PortalListViewModel GetPortals(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);
                 
        /// <summary>
        /// Get all portals
        /// </summary>
        /// <returns>Returns list of portals</returns>
        PortalListViewModel GetAllPortals();    

        /// <summary>
        /// Binds the portal information.
        /// </summary>
        /// <param name="portalViewModel">portalviewmodel parameter.</param>
        /// <returns></returns>
        PortalViewModel BindPortalInformation(PortalViewModel portalViewModel);

        /// <summary>
        /// Creates portal.
        /// </summary>
        /// <param name="portalViewModel">View model from the view.</param>
        /// <param name="notificationMessage">notification message.</param>
        /// <returns></returns>
        bool CreatePortal(PortalViewModel portalViewModel, out string notificationMessage);

        /// <summary>
        /// Copy stores.
        /// </summary>
        /// <param name="portalId">Portal id of the store to be copied.</param>
        /// <returns>Bool Value if the store is copied or not.</returns>
        bool CopyStore(int portalId);

        /// <summary>
        /// Binds Css list of portal.
        /// </summary>
        /// <param name="filters">Filters to be applied on list.</param>
        /// <param name="sortCollection">Sorting to be applied on the list.</param>
        /// <returns>List of Select list items.</returns>
        List<SelectListItem> BindCssList(FilterCollection filters = null, SortCollection sortCollection = null);
    }
}
