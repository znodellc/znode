﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IReviewAgent
    {
        /// <summary>
        /// To get Customer Review list
        /// </summary>
        /// <returns>Retuns Customer Review List.</returns>
        ReviewViewModel GetReviews();

        /// <summary>
        /// Get review based on review id.
        /// </summary>
        /// <param name="reviewId"></param>
        /// <returns>Return review details</returns>
        ReviewItemViewModel GetReview(int reviewId);

        /// <summary>
        /// Method change the review status.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return true or false</returns>
        bool UpdateReviewStatus(ReviewItemViewModel model);

        /// <summary>
        /// Method update the review.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return true or false</returns>
        bool UpdateReview(ReviewItemViewModel model);

        /// <summary>
        /// Method delete the review based on id.
        /// </summary>
        /// <param name="reviewId"></param>
        /// <returns>Return true or false</returns>
        bool DeleteReview(int reviewId);
    }
}
