﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class AuthenticationAgent : AuthorizeAttribute, IAuthenticationAgent
    {
        #region Variable Declaration
        private string defaultControllerName = "Account";
        private string defaultActionName = "Login";
        #endregion

        public void SetAuthCookie(string userName, bool createPersistantCookie)
        {
            FormsAuthentication.SetAuthCookie(userName, createPersistantCookie);
        }

        public void RedirectFromLoginPage(string userName, bool createPersistantCookie)
        {
            FormsAuthentication.RedirectFromLoginPage(userName, createPersistantCookie);
        }

         #region OnAuthorization
        //Executed for Each Actions in each Controller.
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //TODO//
            // Currently code verifies the "UserAccount" key in session.
            // Will implement the functionality once all user will use the "UserAccount" key to store user data in session.
            // Also need to verify the default route action, for other user types.
            //AuthenticateUser(filterContext);
        }
        #endregion

        #region AuthenticateUser
        //Method Used to Authenticate the user.
        public void AuthenticateUser(AuthorizationContext filterContext)
        {
            var isAuthorized = base.AuthorizeCore(filterContext.HttpContext);
            //skipAuthorization get sets to true when the action has the [AllowAnonymous] attributes, If true then skip authentication.
            bool skipAuthorization = filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true)
                            || filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), inherit: true);

            if (!skipAuthorization)
            {
                if (!isAuthorized && !filterContext.HttpContext.Request.IsAuthenticated && (string.IsNullOrEmpty(filterContext.HttpContext.User.Identity.Name)) && Equals(HttpContext.Current.Session["UserAccount"],null))
                {
                    HandleUnauthorizedRequest(filterContext);
                }
            }
        }
        #endregion
       
        #region HandleUnauthorizedRequest
        //Redirect User to Index page in case the un authorized access.
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (Equals(HttpContext.Current.Session["UserAccount"],null))
            {
                filterContext.Result = new RedirectToRouteResult(
                          new RouteValueDictionary {
                        { "Controller", defaultControllerName },
                        { "Action", defaultActionName }
                });
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }

        #endregion
    }
}