﻿using Znode.Engine.Api.Client;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class AttributeTypesAgent : BaseAgent, IAttributeTypesAgent
    {
        #region Private Variables
        private readonly IAttributeTypesClient _attributeTypeClient;
        #endregion

        #region Constructors
        public AttributeTypesAgent()
        {
            _attributeTypeClient = GetClient<AttributeTypesClient>();
        }
        #endregion

        #region Public Methods

        public AttributeTypesListViewModel GetAttributesType()
        {
            var list = _attributeTypeClient.GetAttributeTypes(null, null);
            return AttributeTypesViewModelMap.ToListViewModel(list.AttributeTypes);
        }

        public AttributeTypesViewModel GetAttributeTypeById(int attributeTypeId)
        {
            var attributesType = _attributeTypeClient.GetAttributeType(attributeTypeId);

            return Equals(attributesType, null) ? null : AttributeTypesViewModelMap.ToViewModel(attributesType);
        }

        public bool SaveAttributeType(AttributeTypesViewModel model)
        {
            var attributeType = _attributeTypeClient.CreateAttributeType(AttributeTypesViewModelMap.ToModel(model));
            return (attributeType.AttributeTypeId > 0) ;
        }

        public bool UpdateAttributeType(AttributeTypesViewModel model)
        {
            var attributeType = _attributeTypeClient.UpdateAttributeType(model.AttributeTypeId, AttributeTypesViewModelMap.ToModel(model));
            return (attributeType.AttributeTypeId > 0) ;
        }

        public bool DeleteAttributeType(int attributeTypeId)
        {
            return _attributeTypeClient.DeleteAttributeType(attributeTypeId);
        }

        public AttributeTypesViewModel GetAttributeTypeList()
        {
            AttributeTypesViewModel model = new AttributeTypesViewModel();
            model.GetAttributesList = AttributeTypesViewModelMap.ToListItems(_attributeTypeClient.GetAttributeTypes(Filters, Sorts).AttributeTypes);
            return model;
        }

        #endregion
    }
}