﻿using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IGiftCardAgent
    {
        /// <summary>
        /// Get list of GiftCards
        /// </summary>
        /// <param name="pageIndex">integer pageIndex</param>
        /// <param name="pageSize">integer pageSize</param>
        /// <returns>returns GiftCardListViewModel</returns>
        GiftCardListViewModel GetGiftCards(int? pageIndex, int? pageSize);
                
        /// <summary>
        /// Delete GiftCard 
        /// </summary>
        /// <param name="giftCardId">int giftCardId</param>
        /// <returns>Returs true if GiftCard is Deleted</returns>
        bool DeleteGiftCard(int giftCardId);

        /// <summary>
        /// Return GiftCard of specific Id
        /// </summary>
        /// <param name="GiftCardId">int GiftCardId</param>
        /// <returns>returns GiftCards details of specific GiftCardId</returns>
        GiftCardViewModel GetGiftCard(int GiftCardId);

        /// <summary>
        /// Create GiftCard 
        /// </summary>
        /// <param name="model">GiftCardViewModel model</param>
        /// <returns>Returs true if GiftCard is Created</returns>
        bool Create(GiftCardViewModel model);

        /// <summary>
        /// Update GiftCard 
        /// </summary>
        /// <param name="model">GiftCardViewModel model</param>
        /// <returns>Returs true if GiftCard is updated</returns>
        bool Update(GiftCardViewModel model);

        /// <summary>
        /// Check AccountId Valid or Not
        /// </summary>
        /// <param name="accountId">int accountId</param>
        /// <returns>Returs true if Account is Active</returns>
        bool IsAccountActive(int accountId);

        /// <summary>
        /// Get Next GiftCard Number
        /// </summary>
        /// <returns>returns GiftCardNumberViewModel</returns>
        GiftCardViewModel GetNextGiftCardNumber();

        /// <summary>
        /// Get All Portals
        /// </summary>
        /// <returns>List of Portals</returns>
        GiftCardViewModel GetAllPortals();
    }
}