﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ManufacturersAgent : BaseAgent, IManufacturersAgent
    {
        #region Private Variabls
        private readonly IManufacturersClient _manufacturerClient;
        #endregion

        #region Constructors

        /// <summary>
        /// Constructor for the Manufacturer or Brands.
        /// </summary>
        public ManufacturersAgent()
        {
            _manufacturerClient = GetClient<ManufacturersClient>();
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// This function returns the list of all manufacturers
        /// </summary>
        /// <returns>ManufacturersListViewModel list of ManufacturersViewModel</returns>
        public ManufacturersListViewModel GetManufacturers(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            var list = _manufacturerClient.GetManufacturers(filters, sortCollection, pageIndex - 1, recordPerPage);
            return ManufacturersViewModelMap.ToListViewModel(list, pageIndex, recordPerPage);
        }

        /// <summary>
        /// This method will return the manufacturer based on manufacturer id
        /// </summary>
        /// <param name="manufacturerID">integer manufacturer id</param>
        /// <returns>returns edit view model of manufacturer</returns>
        public ManufacturersViewModel GetManufacturerByID(int manufacturerID)
        {
            ManufacturerModel model = _manufacturerClient.GetManufacturer(manufacturerID);
            return model != null ? ManufacturersViewModelMap.ToViewModel(model) : null;
        }

        /// <summary>
        /// This method will save the Manufacturer
        /// </summary>
        /// <param name="model">ManufacturersViewModel model</param>
        /// <returns>Returs true if manufacturer saved</returns>
        public bool SaveManufacturer(ManufacturersViewModel model)
        {
            var manufacturer = _manufacturerClient.CreateManufacturer(ManufacturersViewModelMap.ToModel(model));
            return (manufacturer.ManufacturerId > 0) ? true : false;
        }

        /// <summary>
        /// This method will update the Manufacturer
        /// </summary>
        /// <param name="model">ManufacturersViewModel model</param>
        /// <returns>Returns true if manufacturer updated</returns>
        public bool UpdateManufacturer(ManufacturersViewModel model)
        {
            ManufacturerModel updateModel = ManufacturersViewModelMap.ToModel(model);
            var manufacturer = _manufacturerClient.UpdateManufacturer(updateModel.ManufacturerId, updateModel);
            return true;
        }

        /// <summary>
        /// This method will delete the Manufacturer
        /// </summary>
        /// <param name="manufacturerID">integere ManufacturerID</param>
        /// <returns>Returs true if manufacturer deleted</returns>
        public bool DeleteManufactuere(int manufacturerID)
        {
            return _manufacturerClient.DeleteManufacturer(manufacturerID);
        }

        #endregion
    }
}