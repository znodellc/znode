﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for the Product Type Agent
    /// </summary>
    public interface IProductTypeAgent
    {
        /// <summary>
        /// Get Product Types List.
        /// </summary>
        /// <returns>Returns ProductTypeListViewModel</returns>
        ProductTypeListViewModel GetProductTypes();

        /// <summary>
        /// Get existing Product Type.
        /// </summary>
        /// <param name="productTypeId"></param>
        /// <returns>Returns ProductTypeViewModel</returns>
        ProductTypeViewModel GetProductType(int productTypeId);

        /// <summary>
        /// Create new Product Type.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns>Return bool</returns>
        bool CreateProductType(ProductTypeViewModel viewModel);

        /// <summary>
        /// Delete an existing  Product Type.
        /// </summary>
        /// <param name="productTypeId"></param>
        /// <returns>Return bool</returns>
        bool DeleteProductType(int productTypeId);

        /// <summary>
        /// Update existing Product Type.
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns>Return bool</returns>
        bool UpdateProductType(ProductTypeViewModel viewModel);
    }
}