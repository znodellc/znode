﻿using System.Linq;
using System.Web;
using System.Web.Security;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Agents
{
    public class PaymentAgent : BaseAgent, IPaymentAgent
    {
        #region Private Variables
        private readonly IPaymentOptionsClient _paymentOptionClient;
        #endregion

        #region Constructor
        public PaymentAgent()
        {
            _paymentOptionClient = GetClient<PaymentOptionsClient>();
        }
        #endregion

        #region Public Methods
        public PaymentOptionListViewModel GetPaymentOptions()
        {
            var paymentOptions = _paymentOptionClient.GetPaymentOptions(new ExpandCollection { ExpandKeys.PaymentGateway, ExpandKeys.PaymentType }, null, null);
            return PaymentOptionViewModelMap.ToListViewModel(paymentOptions.PaymentOptions);
        }

        

        #endregion



    }
}