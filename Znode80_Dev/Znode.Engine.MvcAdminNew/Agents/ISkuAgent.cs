﻿using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for the SKU
    /// </summary>
    public interface ISkuAgent
    {
        SkuViewModel GetSKUById(int skuId);
        bool SaveSKU(SkuViewModel model);
        bool UpdateSKU(SkuViewModel model);
        bool DeleteSKU(int skuId);
    }
}
