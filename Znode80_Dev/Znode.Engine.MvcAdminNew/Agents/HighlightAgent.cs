﻿using System;
using System.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class HighlightAgent : BaseAgent, IHighlightAgent
    {
        #region Private Variables
        private readonly IHighlightsClient _highlightsClient;
        private readonly IHighlightTypesClient _highlightTypesClient;
        #endregion

        #region Constructor
        /// <summary>
        /// Default Constructor.
        /// </summary>
        public HighlightAgent()
        {
            _highlightsClient = GetClient<HighlightsClient>();
            _highlightTypesClient = GetClient<HighlightTypesClient>();
        }
        #endregion

        #region Public Methods
        public HighlightsListViewModel GetHighlights(string name, string typeName)
        {
            var list = this.GetHighlights();
            HighlightsListViewModel highlights = new HighlightsListViewModel();
            highlights.Highlights = (from highlight in list.Highlights
                                     where highlight.Name.ToLower().Contains(name.ToLower())
                                     && highlight.HighlightType.Name.ToLower().Contains(typeName.ToLower())
                                     select highlight).ToList();

            highlights.HighlightTypeList = GetHighlightTypes().HighlightTypeList;
            return highlights;
        }

        public HighlightsListViewModel GetHighlights()
        {
            var list = _highlightsClient.GetHighlights(new ExpandCollection { ExpandKeys.HighlightType }, null, null);
            HighlightsListViewModel highlightViewModels = HighlightViewModelMap.ToListViewModel(list.Highlights);
            return highlightViewModels;
        }

        public HighlightViewModel GetHighlight(int? highlightId)
        {
            HighlightViewModel highlightViewModel = (highlightId.HasValue) ? HighlightViewModelMap.ToViewModel(_highlightsClient.GetHighlight(highlightId.Value)) : null;
            return highlightViewModel;
        }

        public bool CreateHighlight(HighlightViewModel model)
        {
            var highlight = _highlightsClient.CreateHighlight(HighlightViewModelMap.ToModel(model));
            return (highlight.HighlightId > 0) ? true : false;
        }

        public bool UpdateHighlight(HighlightViewModel model)
        {
            var highlight = _highlightsClient.UpdateHighlight(model.HighlightId, HighlightViewModelMap.ToModel(model));
            return (!Equals(highlight, null)) ? true : false;
        }

        public bool DeleteHighlight(int highlightId)
        {
            return _highlightsClient.DeleteHighlight(highlightId);
        }

        public HighlightViewModel GetHighlightTypes()
        {
            HighlightViewModel model = new HighlightViewModel();
            model.HighlightTypeList = HighlightViewModelMap.ToListItems(_highlightTypesClient.GetHighlightTypes(Filters, Sorts).HighlightTypes);
            return model;
        }

        public bool CheckAssociatedProduct(int highlightId)
        {
            return HighlightViewModelMap.ToViewModel(_highlightsClient.CheckAssociatedProduct(highlightId)).IsAssociatedProduct;
        }
        #endregion
    }
}