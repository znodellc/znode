﻿using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for Attributes
    /// </summary>
    interface IAttributesAgent
    {
        /// <summary>
        ///  To Get all Attributes list
        /// </summary>
        /// <returns></returns>
        AttributesListViewModel GetAttributes();       

        /// <summary>
        /// To Get all Attributes list by attributeTypeId
        /// </summary>
        /// <param name="attributeTypeId"></param>
        /// <returns></returns>
        AttributesListViewModel GetAttributesByAttributeTypeId(int attributeTypeId);
      
        /// <summary>
        /// To Get  Attribute  by attributeId
        /// </summary>
        /// <param name="attributeId"></param>
        /// <returns></returns>
        AttributesViewModel GetAttributesByAttributeId(int attributeId);
        /// <summary>
        /// To save Attributes 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool SaveAttributes(AttributesViewModel model);

        /// <summary>
        /// To update Attributes 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool UpdateAttributes(AttributesViewModel model);

        /// <summary>
        /// To delete Attributes by attributeId
        /// </summary>
        /// <param name="attributeTypeId"></param>
        /// <returns></returns>
        bool DeleteAttributes(int attributeId);
    }
}
