﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ReviewAgent : BaseAgent, IReviewAgent
    {
        #region Private Variables

        private readonly IReviewsClient _reviewsClient;

        #endregion

        #region Constructor

        public ReviewAgent()
        {
            _reviewsClient = GetClient<ReviewsClient>();
        }

        #endregion

        #region Public Methods

        public ReviewViewModel GetReviews()
        {
            //(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
           // Filters = new FilterCollection { { FilterKeys.ReviewStatus, FilterOperators.Equals, "N" } };
            var reviewList = _reviewsClient.GetReviews(new ExpandCollection { ExpandKeys.Product }, Filters, new SortCollection(), 0, 10);

            if (reviewList != null && reviewList.Reviews != null && reviewList.Reviews.Count > 0)
            {
                return ReviewViewModelMap.ToViewModels(reviewList.Reviews);
            }
            return null;
        }

        public ReviewItemViewModel GetReview(int reviewId)
        {
            var reviewModel = _reviewsClient.GetReview(reviewId, new ExpandCollection { ExpandKeys.Product });

            if (!Equals(reviewModel, null))
            {
                return ReviewViewModelMap.ToViewModel(reviewModel);
            }
            return null;
        }

        public bool UpdateReviewStatus(ReviewItemViewModel model)
        {
            //Get the Review Model to update the Review Status.
            var reviewModel = _reviewsClient.GetReview(model.ReviewId);
            if(Equals(reviewModel, null)) return false;
            reviewModel.Status = model.Status;
            
            //updates the Review Status.
            var updatedReviews =_reviewsClient.UpdateReview(model.ReviewId, reviewModel);
            return !Equals(updatedReviews, null);
        }


        public bool UpdateReview(ReviewItemViewModel model)
        {
            var updatedReviews = _reviewsClient.UpdateReview(model.ReviewId,ReviewViewModelMap.ToReviewModel(model));
            return !Equals(updatedReviews, null);
        }

        public bool DeleteReview(int reviewId)
        {
            return _reviewsClient.DeleteReview(reviewId);
        }


        #endregion
    }
}