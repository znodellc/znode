﻿using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IAccountAgent
    {
        /// <summary>
        /// This method is used to login the user.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return the login details.</returns>
        LoginViewModel Login(LoginViewModel model);

        /// <summary>
        /// This method is used to logout the user.
        /// </summary>
        void Logout();

        /// <summary>
        /// Method used to check whether the user is authorized or not.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="roleName"></param>
        /// <returns>Returns true or false</returns>
        bool IsUserInRole(string userName, string roleName);
        
        /// <summary>
        /// Method checks for the Account key in the session variable.
        /// </summary>
        /// <returns>Returns true or false.</returns>
        bool CheckAccountKey();

        /// <summary>
        /// Function used for Reset admin details for the first default login.
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <returns>Returns reset admin details status.</returns>
        ResetPasswordModel ResetPassword(ResetPasswordModel model);
        /// <summary>
        /// Function used for forgot password.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns forgot password details.</returns>
        AccountViewModel ForgotPassword(AccountViewModel model);

        /// <summary>      
        /// This function will check the Reset Password Link current status.
        /// </summary>
        /// <param name="model">ChangePasswordViewModel</param>
        /// <returns>Returns Status of Reset Password Link.</returns>
        ResetPasswordStatusTypes CheckResetPasswordLinkStatus(ChangePasswordViewModel model);

        /// <summary>
        /// Function used to Change/Reset the user password.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns the Change/Reset password details.</returns>
        ChangePasswordViewModel ChangePassword(ChangePasswordViewModel model);
    }
}
