﻿using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IPaymentAgent
    {
        /// <summary>
        /// get all payment options list
        /// </summary>
        /// <returns>returns PaymentOptionListViewModel</returns>
        PaymentOptionListViewModel GetPaymentOptions();
    }
}
