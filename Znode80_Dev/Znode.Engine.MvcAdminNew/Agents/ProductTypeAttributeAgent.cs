﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class ProductTypeAttributeAgent : BaseAgent , IProductTypeAttributeAgent
    {
        #region Public Variables
        private readonly IProductTypeAttributeClient _productTypeAttributeClient;
        private readonly IProductsClient _productsClient;
        private readonly IAttributeTypesClient _attributeTypesClient; 
        #endregion

        #region Public Constructor
        public ProductTypeAttributeAgent()
        {
            _productTypeAttributeClient = GetClient<ProductTypeAttributeClient>();
            _productsClient = GetClient<ProductsClient>();
            _attributeTypesClient = GetClient<AttributeTypesClient>();
        } 
        #endregion

        #region Public Methods
        public ProductTypeAssociatedAttributeTypesListViewModel GetAttributesByProductTypeId(int productTypeAttributeId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            ProductTypeAssociatedAttributeTypesListModel modelList = _productTypeAttributeClient.GetAttributeByProductTypeIdFromCustomService(productTypeAttributeId, filters, sortCollection, pageIndex, recordPerPage);
            
            return Equals(modelList, null) ? null : ProductTypeAttributeViewModelMap.ToListViewModel(modelList, pageIndex, recordPerPage, modelList.TotalPages, modelList.TotalResults);
        }

        public bool IsProductAssociatedProductType(int productTypeId)
        {
            var count = _productsClient.GetProducts(null, new FilterCollection() { new FilterTuple(FilterKeys.ProductTypeId, FilterOperators.Equals, productTypeId.ToString()) }, null);
            
            return Equals(count.Products, null) ? false : true;
        }

        public AttributeTypesListViewModel GetAttributeTypes()
        {
            AttributeTypesListViewModel model = new AttributeTypesListViewModel();
            model.AttributeTypeList = AttributeTypesViewModelMap.ToListItems(_attributeTypesClient.GetAttributeTypes(Filters, Sorts).AttributeTypes);
            return model;
        }

        public bool DeleteAttributes(int attributeTypeId)
        {
            return _attributeTypesClient.DeleteAttributeType(attributeTypeId);
        }

        public bool DeleteAttributeType(int attributeTypeId)
        {
            return _productTypeAttributeClient.DeleteAttributeType(attributeTypeId);
        }

        public bool CreateProductType(ProductTypeAssociatedAttributeTypesViewModel viewModel)
        {
            ProductTypeAssociatedAttributeTypesModel model = ProductTypeAttributeViewModelMap.ToModel(viewModel);
            var productType = _productTypeAttributeClient.CreateAttributeType(model);

            return (productType.ProductTypeId > 0);
        }
        #endregion
    }
}