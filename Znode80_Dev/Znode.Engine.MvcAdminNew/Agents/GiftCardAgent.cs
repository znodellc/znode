﻿using System.Linq;
using Znode.Engine.Api.Client;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using System;
namespace Znode.Engine.MvcAdmin.Agents
{
    public class GiftCardAgent : BaseAgent, IGiftCardAgent
    {
        #region Private Variables

        private readonly IGiftCardsClient _IGiftCardsClient;
        private readonly IAccountsClient _IAccountsClient;
        private readonly IPortalsClient _IPortalsClient;
        #endregion

        #region Constructor

        public GiftCardAgent()
        {
            _IGiftCardsClient = GetClient<GiftCardsClient>();
            _IAccountsClient = GetClient<AccountsClient>();
            _IPortalsClient = GetClient<PortalsClient>();
        }

        #endregion

        public GiftCardListViewModel GetGiftCards(int? pageIndex, int? pageSize)
        {
            GiftCardListViewModel GiftCards = new GiftCardListViewModel();
            var list = _IGiftCardsClient.GetGiftCards(null, null, null,pageIndex -1 ,pageSize);
            var filteredList = (from GiftCard in list.GiftCards where Convert.ToDateTime(GiftCard.ExpirationDate) > DateTime.Now select GiftCard);
            return GiftCardViewModelMap.ToListViewModel(filteredList);
        }

        public bool DeleteGiftCard(int giftCardId)
        {
            return _IGiftCardsClient.DeleteGiftCard(giftCardId);
        }

        public GiftCardViewModel GetGiftCard(int GiftCardId)
        {
            var GiftCardModel = _IGiftCardsClient.GetGiftCard(GiftCardId);

            if (!Equals(GiftCardModel, null))
            {
                return GiftCardViewModelMap.ToViewModel(GiftCardModel);
            }
            return null;
        }

        public bool Create(GiftCardViewModel model)
        {
            if (!Equals(model, null))
            {
                var value = _IGiftCardsClient.CreateGiftCard(GiftCardViewModelMap.ToModel(model));
                return true;
            }
            else
                return false;
        }

        public bool Update(GiftCardViewModel model)
        {
            var updatedGiftCard = _IGiftCardsClient.UpdateGiftCard(model.GiftCardId, GiftCardViewModelMap.ToModel(model));
            return true;
        }

        public bool IsAccountActive(int accountId)
        {
            var accounts = _IAccountsClient.GetAccount(accountId) != null ? _IAccountsClient.GetAccount(accountId) : null;

            if (!Equals(accounts, null) && accounts.AccountId.Equals(accountId))
            {
                return true;
            }
            else
                return false;
        }

        public GiftCardViewModel GetNextGiftCardNumber()
        {
            GiftCardViewModel giftCardnumber = new GiftCardViewModel();
            giftCardnumber = GiftCardViewModelMap.ToViewModel(_IGiftCardsClient.GetNextGiftCardNumber());
            return giftCardnumber;
        }

        public GiftCardViewModel GetAllPortals()
        {
            GiftCardViewModel model = new GiftCardViewModel();
            model.PortalList = GiftCardViewModelMap.ToListItems(_IPortalsClient.GetPortals(null, null,null).Portals);
            return model;
        }
    }
}