﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    ///  Interface for the Products
    /// </summary>
    public interface IProductAgent
    {
        /// <summary>
        /// To get Product list
        /// </summary>
        /// <returns></returns>
        ProductListViewModel GetProducts();

        /// <summary>
        /// To get Product details by productId
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        ProductViewModel GetProduct(int productId);

        /// <summary>
        /// To check Sku Exist
        /// </summary>
        /// <param name="Sku"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        bool IsSkuExist(string Sku, int productId);

        /// <summary>
        /// To save product details
        /// </summary>
        /// <param name="model">ProductViewModel</param>
        /// <returns> true/false </returns>
        bool SaveProduct(ProductViewModel model);

        /// <summary>
        /// To update product details
        /// </summary>
        /// <param name="model">ProductViewModel</param>
        /// <returns>true/false</returns>
        bool UpdateProduct(ProductViewModel model);

        /// <summary>
        ///  To delete product details by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>true/false</returns>
        bool DeleteProduct(int productId);

        /// <summary>
        /// To bind all dropdown of product page
        /// </summary>
        /// <param name="model"></param>
        /// <param name="portalId"></param>
        /// <returns></returns>
        ProductViewModel BindProductPageDropdown(ProductViewModel model, int portalId);

       
        List<AttributeTypeValueViewModel> GetProductAttributesByProductTypeId(int? productTypeId);

        /// <summary>
 
        /// To Get the Product tags.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns>Return product tags.</returns>
        ProductTagsViewModel GetProductTags(int productId);

        /// <summary>

        /// Inserts the Product Tags.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return true or false</returns>
        bool InsertProductTags(ProductTagsViewModel model);

        /// <summary>
       
        /// Updates the Product Tags.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return true or false</returns>
        bool UpdateProductTags(ProductTagsViewModel model);

        /// <summary>
  
        /// Deletes the Product Tags.
        /// </summary>
        /// <param name="tagId"></param>
        /// <returns>Return true or false</returns>
        bool DeleteProductTags(int tagId);

        /// <summary>
   
        /// To Get the associated facets based on product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns>Return the associated facets.</returns>
        ProductFacetsViewModel GetAssociatedFacetGroup(int productId);

        /// <summary>
    
        /// To Associated the Facets to the product.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return true or false</returns>
        bool BindAssociatedFacets(ProductFacetsViewModel model);

    }
}
