﻿using System.Collections.Generic;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IHighlightAgent
    {
        /// <summary>
        /// Get Highlight list.
        /// </summary>
        /// <returns>returns HighlightsListViewModel</returns>
        HighlightsListViewModel GetHighlights();

        /// <summary>
        ///  Get Highlight details by highlightId.
        /// </summary>
        /// <param name="highlightId">int highlightId</param>
        /// <returns>returns HighlightViewModel</returns>
        HighlightViewModel GetHighlight(int? highlightId);

        /// <summary>
        /// Save new Highlight.
        /// </summary>
        /// <param name="model">HighlightViewModel model</param>
        /// <returns>true or false</returns>
        bool CreateHighlight(HighlightViewModel model);

        /// <summary>
        /// Update Highlight.
        /// </summary>
        /// <param name="model">HighlightViewModel model</param>
        /// <returns>true or false</returns>
        bool UpdateHighlight(HighlightViewModel model);

        /// <summary>
        /// Delete Existing Highlight.
        /// </summary>
        /// <param name="highlightId">int highlightId</param>
        /// <returns>true or false</returns>
        bool DeleteHighlight(int highlightId);

        /// <summary>
        /// Get Highlight.
        /// </summary>
        /// <param name="name">string name</param>
        /// <param name="typeName">string typeName</param>
        /// <returns>returns HighlightsListViewModel</returns>
        HighlightsListViewModel GetHighlights(string name, string typeName);

        /// <summary>
        /// Get Highlight Types.
        /// </summary>
        /// <returns>returns HighlightViewModel<HighlightTypeViewModel></returns>
        HighlightViewModel GetHighlightTypes();

        /// <summary>
        /// Check the Associated product with highlight
        /// </summary>
        /// <param name="highlightId">int highlightId</param>
        /// <returns>true or false</returns>
        bool CheckAssociatedProduct(int highlightId);
    }
}
