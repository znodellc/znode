﻿using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for the AttributeType
    /// </summary>
    public interface IAttributeTypesAgent
    {
        /// <summary>
        ///  To Get all Attributes Type list
        /// </summary>
        /// <returns></returns>
        AttributeTypesListViewModel GetAttributesType();       

        /// <summary>
        /// To Get all Attributes Type list by attributeTypeId
        /// </summary>
        /// <param name="attributeTypeId"></param>
        /// <returns></returns>
        AttributeTypesViewModel GetAttributeTypeById(int attributeTypeId);

        /// <summary>
        /// To save Attributes Type 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool SaveAttributeType(AttributeTypesViewModel model);

        /// <summary>
        /// To update Attributes Type 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool UpdateAttributeType(AttributeTypesViewModel model);

        /// <summary>
        /// To delete Attributes Type by attributeTypeId
        /// </summary>
        /// <param name="attributeTypeId"></param>
        /// <returns></returns>
        bool DeleteAttributeType(int attributeTypeId);

        /// <summary>
        /// To Get All Attribute Types.
        /// </summary>
        /// <returns></returns>
        AttributeTypesViewModel GetAttributeTypeList();
    }
}
