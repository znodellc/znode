﻿using System.Collections.Generic;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface ICategoryAgent
    {

        /// <summary>
        /// Gets categories by catalog Id.
        /// </summary>
        /// <param name="catalogId">The Id of catalog.</param>
        /// <returns>Returns categories by catalog Id.</returns>
        CatalogAssociatedCategoriesListViewModel GetCategoryByCataLogId(int catalogId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Get Categories based on Catalog Id.
        /// </summary>
        /// <param name="catalogId">The id of catalog.</param>
        /// <returns>Return Categories.</returns>
        CategoryListViewModel GetCategoryByCatalogId(int catalogId);

        /// <summary>
        /// Gets all categories.
        /// </summary>
        /// <param name="catalogId">The Id of the catalog.</param>
        /// <param name="categoryName">The name of category.</param>
        /// <param name="filters">Filter collcetion object.</param>
        /// <param name="sorts">Sort collection object.</param>
        /// <param name="pageIndex">The index of page.</param>
        /// <param name="pageSize">The size of page.</param>
        /// <returns>Returns List of all categories.</returns>
        CatalogAssociatedCategoriesListViewModel GetAllCategories(int catalogId, string categoryName, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null);

        /// <summary>
        /// Binds the parent-category nodes.
        /// </summary>
        /// <param name="catalogId">The id of catalog.</param>
        /// <returns>List of parent-category nodes.</returns>
        List<SelectListItem> BindParentCategoryNodes(int catalogId);

        /// <summary>
        /// Binds theme list.
        /// </summary>
        /// <param name="filters">The filter collcetion object.</param>
        /// <param name="sortCollection">The sort collection object.</param>
        /// <returns>List of theme.</returns>
        List<SelectListItem> BindThemeList(FilterCollection filters = null, SortCollection sortCollection = null);

        /// <summary>
        /// Binds the css list.
        /// </summary>
        /// <param name="filters">The filter collcetion object.</param>
        /// <param name="sortCollection">The sort collection object.</param>
        /// <returns>List of css.</returns>
        List<SelectListItem> BindCssList(FilterCollection filters = null, SortCollection sortCollection = null);

        /// <summary>
        /// Binds the master page list.
        /// </summary>
        /// <param name="themeId">The id of theme.</param>
        /// <param name="pageType">the type of page.</param>
        /// <returns>List of master page.</returns>
        List<SelectListItem> BindMasterPageList(int themeId = 0, string pageType = "");

        /// <summary>
        /// Associates an already created category to the catalog.
        /// </summary>
        /// <param name="model">The model of type CategoryNodeViewModel.</param>
        /// <returns>Returns true or false.</returns>
        bool AssociateCategory(CategoryNodeViewModel model);
    }
}
