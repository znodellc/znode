﻿using System.Collections.Generic;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class CategoryAgent : BaseAgent, ICategoryAgent
    {
        #region Private Variables
        private readonly ICategoriesClient _categoryClient;
        private readonly ICategoryNodesClient _categoryNodeClient;
        private readonly IThemeClient _themeClient;
        private readonly ICSSClient _cssClient;
        private readonly IMasterPageClient _masterPageClient;
        #endregion

        #region Constructor
        public CategoryAgent()
        {
            _categoryClient = GetClient<CategoriesClient>();
            _categoryNodeClient = GetClient<CategoryNodesClient>();
            _themeClient = GetClient<ThemeClient>();
            _cssClient = GetClient<CSSClient>();
            _masterPageClient = GetClient<MasterPageClient>();
        }
        #endregion

        #region Public Methods
        public CatalogAssociatedCategoriesListViewModel GetCategoryByCataLogId(int catalogId, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            CatalogAssociatedCategoriesListModel modelList = _categoryClient.GetCategoryByCatalogIdFromCustomService(catalogId, filters, sortCollection, pageIndex, recordPerPage);

            return Equals(modelList, null) ? null : CategoryViewModelMap.ToListViewModel(modelList, pageIndex, recordPerPage, modelList.TotalPages, modelList.TotalResults);
        }


        public CategoryListViewModel GetCategoryByCatalogId(int catalogId)
        {
            //Gets the categories on catalog Id.
            var data = _categoryClient.GetCategoriesByCatalog(catalogId, new ExpandCollection(), Filters, new SortCollection());
            return CategoryListViewModelMap.ToViewModel(data.Categories);
        }

        public CatalogAssociatedCategoriesListViewModel GetAllCategories(int catalogId, string categoryName, FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            CatalogAssociatedCategoriesListModel modelList = _categoryClient.GetAllCategories(catalogId, categoryName, filters, sortCollection, pageIndex, recordPerPage);

            return Equals(modelList, null) ? null : CategoryViewModelMap.ToListViewModel(modelList, pageIndex, recordPerPage, modelList.TotalPages, modelList.TotalResults);
        }

        public List<SelectListItem> BindParentCategoryNodes(int catalogId)
        {
            CatalogAssociatedCategoriesListModel categoryModel = _categoryClient.GetCategoryByCatalogIdFromCustomService(catalogId, null, null, 1, 100);
            return Equals(categoryModel, null) ? null : CategoryViewModelMap.ToParentCategorySelectListItems(categoryModel.CategoryList);
        }

        public List<SelectListItem> BindThemeList(FilterCollection filters = null, SortCollection sortCollection = null)
        {
            ThemeListModel themeModel = _themeClient.GetThemes(filters, sortCollection);
            return Equals(themeModel, null) ? null : CategoryViewModelMap.ToThemeSelectListItems(themeModel.Themes);
        }

        public List<SelectListItem> BindCssList(FilterCollection filters = null, SortCollection sortCollection = null)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            if (!Equals(filters, null))
            {
                CSSListModel cssModel = _cssClient.GetCSSs(null, filters, sortCollection);
                return Equals(cssModel, null) ? null : CategoryViewModelMap.ToCssSelectListItems(cssModel.CSSs);
            }
            return list;

        }

        public List<SelectListItem> BindMasterPageList(int themeId = 0, string pageType = "")
        {
            MasterPageListModel masterPageList = _masterPageClient.GetMasterPageByThemeId(themeId, pageType);
            return Equals(masterPageList, null) ? null : CategoryViewModelMap.ToMasterPageSelectListItems(masterPageList.MasterPages);
        }

        public bool AssociateCategory(CategoryNodeViewModel model)
        {
            return Equals(_categoryNodeClient.CreateCategoryNode(CategoryViewModelMap.ToModel(model)), null) ? false : true;
        }

        #endregion
    }
}