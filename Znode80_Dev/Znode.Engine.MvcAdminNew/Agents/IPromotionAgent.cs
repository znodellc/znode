﻿using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    interface IPromotionAgent
    {
        /// <summary>
        /// Get list of all promotions
        /// </summary>
        /// <returns>list of promotions</returns>
        PromotionsListViewModel GetPromotions();

        /// <summary>
        /// Get Promotion on the basis of promotionId
        /// </summary>
        /// <param name="promotionId">promotion Id</param>
        /// <returns>Promotion related to Id</returns>
        PromotionsViewModel GetPromotionById(int promotionId);

        /// <summary>
        /// Deletes promotion from list on the basis of promotionId
        /// </summary>
        /// <param name="promotionId">promotion Id</param>
        /// <returns>Delete promotion</returns>
        bool DeletePromotion(int promotionId);

        /// <summary>
        /// Update the promtion on the basis of promotionId
        /// </summary>
        /// <param name="model">Promotion View Model</param>
        /// <returns>Update promotion</returns>
        bool UpdatePromotion(PromotionsViewModel model);

        /// <summary>
        /// Gets the list of portals, catalogs, categories & promotionType
        /// </summary>
        /// <param name="promotionsViewModel">Promotion View Model</param>
        /// <returns>This fucntion returns the Portals, catalog etc List information Of promotion </returns>
        PromotionsViewModel GetPromotionInformation(PromotionsViewModel promotionsViewModel);

        /// <summary>
        /// Create the new promotion
        /// </summary>
        /// <param name="model">Promotion View Model</param>
        /// <returns>create promotion</returns>
        bool SavePromotion(PromotionsViewModel model);
    }
}
