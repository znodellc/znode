﻿using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    /// <summary>
    /// Interface for the Shipping Agent
    /// </summary>
    public interface IShippingAgent
    {
        /// <summary>
        /// Get Shipping Options List.
        /// </summary>
        /// <returns>Returns ShippingOptionListViewModel</returns>
        ShippingOptionListViewModel GetShippingOptions();
    }
}