﻿using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class DomainAgent : BaseAgent, IDomainAgent
    {
        #region Private Variables
        private readonly IDomainsClient _domainClient;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for Domain agent.
        /// </summary>
        public DomainAgent()
        {
            _domainClient = GetClient<DomainsClient>();
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get all the domains from specified portal id
        /// </summary>
        /// <param name="portalId">portal id</param>
        /// <returns>gets list of domains</returns>
        public DomainListViewModel GetDomains(int portalId)
        {
            var list = _domainClient.GetDomains(new FilterCollection { new FilterTuple(FilterKeys.PortalId, FilterOperators.Equals, portalId.ToString()) }, null);
            return DomainViewModelMap.ToListViewModel(list.Domains);
        }

        public DomainViewModel GetDomain(int portalId)
        {
            DomainViewModel domainViewModel = DomainViewModelMap.ToViewModel(_domainClient.GetDomain(portalId));
            return domainViewModel;
        }

        public bool createDomainUrl(DomainViewModel model)
        {
            var domain = _domainClient.CreateDomain(DomainViewModelMap.ToModel(model));
            return (domain.DomainId > 0) ;
        }

        public bool UpdateDomainUrl(DomainViewModel viewModel)
        {
            var productType = _domainClient.UpdateDomain(viewModel.DomainId, DomainViewModelMap.ToModel(viewModel));
            return (viewModel.DomainId > 0);
        }

        public bool DeleteDomainUrl(int domainId)
        {
            return _domainClient.DeleteDomain(domainId);
        }
        #endregion
    }
}