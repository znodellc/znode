﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class VendorAccountAgent : BaseAgent, IVendorAccountAgent
    {
        #region Private Variables

        private readonly ISuppliersClient _vendorAccountClient;

        #endregion

        #region Constructor

        public VendorAccountAgent()
        {
            _vendorAccountClient = GetClient<SuppliersClient>();
        }

        #endregion

        //public methods
        public VendorAccountListViewModel GetSupplier()
        {
            var list = _vendorAccountClient.GetSuppliers(null, null, null);
            return SupplierViewModelMap.ToListViewModel(list.Suppliers);
        }
    }
}