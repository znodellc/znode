﻿using System.Linq;
using Znode.Engine.MvcAdmin.Extensions;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class CatalogAgent : BaseAgent, ICatalogAgent
    {
        #region Private Variables
        private readonly ICatalogsClient _catalogClient;
        #endregion

        #region Constructor
        public CatalogAgent()
        {
            _catalogClient = GetClient<CatalogsClient>();
        }
        #endregion

        #region Public Methods
       
        public CatalogListViewModel GetCatalogs(FilterCollection filters = null, SortCollection sortCollection = null, int? pageIndex = null, int? recordPerPage = null)
        {
            var catalogList = _catalogClient.GetCatalogs(filters, sortCollection, pageIndex - 1, recordPerPage);
            string sort = sortCollection["sort"];
            string sortDir = sortCollection["sortDir"];

            if (!Equals(catalogList.Catalogs, null))
            {
                catalogList.Catalogs = (from item in catalogList.Catalogs
                                        select item).AsQueryable().OrderBy(sort, sortDir,null).ToList().ToCollection();
            }

            if (!Equals(catalogList, null) && !Equals(catalogList.Catalogs, null))
            {
                return CatalogViewModelMap.ToListViewModel(catalogList, pageIndex, recordPerPage);
            }
            return null;

        }

        
        public CatalogViewModel GetCatalog(int catalogId)
        {
            CatalogModel catalogList = _catalogClient.GetCatalog(catalogId);
            return Equals(catalogList, null)? null: CatalogViewModelMap.ToViewModel(catalogList);           
        }

       
        public bool CreateCatalog(CatalogViewModel model)
        {
            CatalogModel modelSaved = _catalogClient.CreateCatalog(CatalogViewModelMap.ToModel(model));
            return Equals(modelSaved, null) ? false : true;
        }
        
        public bool CopyCatalog(CatalogViewModel model)
        {
            CatalogModel modelCopied = _catalogClient.CopyCatalog(CatalogViewModelMap.ToModel(model));
            return Equals(modelCopied, null) ? false : true;
        }

        public bool DeleteCatalog(CatalogViewModel model)
        {
            return _catalogClient.DeleteCatalog(model.CatalogId, model.PreserveCategories);
        }
        #endregion
    }
}