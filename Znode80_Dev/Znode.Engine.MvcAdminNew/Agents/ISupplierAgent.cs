﻿using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    interface ISupplierAgent
    {
        /// <summary>
        ///  To delete Vender details by productId
        /// </summary>
        /// <param name="VenderId">int VenderId</param>
        /// <returns>true/false</returns>
        bool DeleteVender(int VenderId);

        /// <summary>
        /// Creates vendor account.
        /// </summary>
        /// <param name="vendorAccountViewModel">Model of VendorAccountViewModel type.</param>
        /// <returns>Returns true or false.</returns>
        bool CreateVendorAccount(VendorAccountViewModel vendorAccountViewModel);
    }
}
