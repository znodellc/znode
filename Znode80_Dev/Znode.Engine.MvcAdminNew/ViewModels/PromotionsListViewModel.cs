﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for Promotions
    /// </summary>
    public class PromotionsListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for PromotionsListViewModel
        /// </summary>
        public PromotionsListViewModel()
        {
            Promotions = new List<PromotionsViewModel>();            
        }

        public List<PromotionsViewModel> Promotions { get; set; }
       
    }
}