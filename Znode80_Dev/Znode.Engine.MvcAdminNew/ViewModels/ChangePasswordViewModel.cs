﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Resources;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ChangePasswordViewModel : BaseViewModel
    {
        public ChangePasswordViewModel()
        {
            
        }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredPassword")]
        public string OldPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredNewPassword")]
        [RegularExpression("^(?=.*[0-9])(?=.*[a-zA-Z]).{8,}$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidPassword")]
        [MaxLength(128, ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InValidPasswordLength")]
        public string NewPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredNewConfirmPassword")]
        [Compare("NewPassword", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorPasswordMatch")]
        public string ReTypeNewPassword { get; set; }

        public string UserName { get; set; }
        public bool IsResetPassword { get; set; }
        public string PasswordToken { get; set; }
    }
}