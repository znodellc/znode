﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Libraries.Helpers.Attributes;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// view model for Create Highlight 
    /// </summary>
    public class HighlightViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for HighlightViewModel
        /// </summary>
        public HighlightViewModel()
        {

        }

        [DisplayName("Image ALT Text")]
        public string ImageAltTag { get; set; }
        public string ImageFile { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string ImageThumbnailPath { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredName")]
        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Display Text")]
        [UIHint("RichTextBox")]
        [AllowHtml]
        public string Description { get; set; }

        [DisplayName("Hyperlink")]
        public string Hyperlink { get; set; }

        public int HighlightId { get; set; }
        [DisplayName("Type")]
        public int? TypeId { get; set; }
        public int LocaleId { get; set; }

        [DisplayName("Display Order")]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredDisplayOrder")]
        public int? DisplayOrder { get; set; }
        public int? HighlightTypeId { get; set; }

        public bool ShowPopup { get; set; }
        public bool NoImage { get; set; }
        public bool DispalyText { get; set; }
        public bool DisplayHyperlink { get; set; }
        public bool IsEditMode { get; set; }

        public bool IsActive { get; set; }
        public bool IsAssociatedProduct { get; set; }
        public bool? HyperlinkNewWindow { get; set; }

        public HighlightTypeViewModel HighlightType { get; set; }

        public List<SelectListItem> HighlightTypeList { get; set; }

        [DisplayName("Select Image")]
        [FileTypeValidation(MvcAdminConstants.ImageFileTypes, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ImageFileTypeErrorMessage")]
        [FileMaxSizeValidation(MvcAdminConstants.ImageMaxFileSize, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "FileSizeExceededErrorMessage")]
        [UIHint("FileUploader")]
        public HttpPostedFileBase Image { get; set; }
    }
}