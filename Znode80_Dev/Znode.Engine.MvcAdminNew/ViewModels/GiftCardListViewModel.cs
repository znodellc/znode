﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for GiftCard
    /// </summary>
    public class GiftCardListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for GiftCard
        /// </summary>
        public GiftCardListViewModel()
        {
            GiftCards = new List<GiftCardViewModel>(); 
        }

        public List<GiftCardViewModel> GiftCards { get; set; }
    }
}
