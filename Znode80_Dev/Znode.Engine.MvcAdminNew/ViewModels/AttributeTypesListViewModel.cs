﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for AttributeType
    /// </summary>
    public class AttributeTypesListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for AttributeType
        /// </summary>
        public AttributeTypesListViewModel()
        {
            AttributeType = new List<AttributeTypesViewModel>();
            AttributeTypeList = new List<SelectListItem>();
        }

        public int AttributeTypeId { get; set; }

        public List<AttributeTypesViewModel> AttributeType { get; set; }
        public List<SelectListItem> AttributeTypeList { get; set; }
    }
}