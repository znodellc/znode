﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List view model for associated attribute with product Type.
    /// </summary>
    public class ProductTypeAttributeListViewModel : BaseViewModel 
    {
        /// <summary>
        /// Constructor for ProductTypeAttributeListViewModel
        /// </summary>
        public ProductTypeAttributeListViewModel()
        {
            ProductTypeAttributes = new List<ProductTypeAttributeViewModel>();
        }

        public List<ProductTypeAttributeViewModel> ProductTypeAttributes { get; set; }
        
    }
}