﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Resources;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for Manage Banner
    /// </summary>
    public class ManageBannerViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for Manage Banner
        /// </summary>
        public ManageBannerViewModel()
        {

        }
        #endregion

        #region Public Properties

        public int MessageID { get; set; }
        public string Key { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredMessageKey")]
        [RegularExpression("^[A-Za-z0-9-_]+$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidMessageKey")]
        public string MessageKey { get; set; }
        public string Description { get; set; }

        [UIHint("RichTextBox")]
        [AllowHtml]
        public string Value { get; set; }

        [RegularExpression("^[A-Za-z0-9-_]+$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidSEOUrl")]
        public string PageSEOName { get; set; }
        public int MessageTypeID { get; set; }
        public int PortalID { get; set; }
        public int? LocaleID { get; set; }

        public List<SelectListItem> Portal { get; set; }
        public List<SelectListItem> BannerKeys { get; set; }

        public bool IsEditMode { get; set; }

        #endregion
    }
}