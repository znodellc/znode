﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for create and edit Shipping
    /// </summary>
    public class ShippingViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ShippingViewModel 
        /// </summary>
        public ShippingViewModel()
        {

        }

        public string DisplayName { get; set; }
        public string InternalCode { get; set; }
        
        public int DisplayOrder { get; set; }
        
        public decimal BaseCost { get; set; }
        public decimal HandlingCharge { get; set; }
        public decimal PerUnitCost { get; set; }
        
        public decimal? LowerLimit { get; set; }
        public decimal? UpperLimit { get; set; }

        public bool Enable { get; set; }
    }
}