﻿namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for 301 Redirect
    /// </summary>
    public class URL301RedirectViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for URL301RedirectViewModel
        /// </summary>
        public URL301RedirectViewModel()
        {

        }

        public string URLToRedirctFrom { get; set; }
        public string URLToRedirctTo { get; set; }

        public bool EnableThisRedirection { get; set; }
    }
}