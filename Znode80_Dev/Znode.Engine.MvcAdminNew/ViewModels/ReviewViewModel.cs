﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ReviewViewModel : BaseViewModel
    {
        public ReviewViewModel()
        {
            ReviewItems = new List<ReviewItemViewModel>();
        }
        public List<ReviewItemViewModel> ReviewItems { get; set; }
    }
}