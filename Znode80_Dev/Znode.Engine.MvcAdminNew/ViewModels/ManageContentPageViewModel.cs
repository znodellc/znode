﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for Manage Content Page
    /// </summary>
    public class ManageContentPageViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for Manage Content Page 
        /// </summary>
        public ManageContentPageViewModel()
        {

        } 
        #endregion

        #region Public Properties
        public string PageName { get; set; }
        public string PageTitle { get; set; }
        public string SEOTitle { get; set; }
        public string SEOKeywords { get; set; }
        public string SEODescription { get; set; }
        public string AdditionalMetaInformation { get; set; }
        public string SEOFriendlyPageName { get; set; }
        
        #endregion
    }
}