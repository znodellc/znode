﻿using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CatalogViewModel : BaseViewModel
    {
        int localeId = 0;
        public int CatalogId { get; set; }
        public string ExternalId { get; set; }
        public bool IsActive { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredCatalogName")]
        public string Name { get; set; }
        public int? PortalId { get; set; }
        public int LocaleId
        {
            get
            {
                return localeId;
            }
            set
            {
                localeId = MvcAdminConstants.LocaleId;
            }
        }

        public bool PreserveCategories { get; set; }

        public List<CatalogAssociatedCategoriesViewModel> Categories { get; set; }
    }
}