﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for create and edit Tax
    /// </summary>
    public class TaxViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for TaxViewModel
        /// </summary>
        public TaxViewModel()
        {

        }

        public string TaxClassName { get; set; }

        public int? DisplayOrder { get; set; }

        public bool Enable { get; set; }
    }
}