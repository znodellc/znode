﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Create and Edit Supplier Account
    /// </summary>
    public class  VendorAccountViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Supplier Account View Model
        /// </summary>
        public VendorAccountViewModel()
        {

        }

        public string Name { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string CompanyName { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }

        public int VendorId { get; set; }
        public int AccountId { get; set; }
    }
}