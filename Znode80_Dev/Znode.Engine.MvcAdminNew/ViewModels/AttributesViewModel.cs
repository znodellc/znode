﻿using System.ComponentModel.DataAnnotations;
using Resources;
using Znode.Engine.MvcAdmin.Helpers;


namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class AttributesViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Attributes View Model
        /// </summary>
        public AttributesViewModel()
        {

        }       
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public int? DisplayOrder { get; set; }

        public int AttributeId { get; set; }
        public int AttributeTypeId { get; set; }

        public bool IsActive { get; set; }

        public string AttributeTypeName { get; set; }
    }
}