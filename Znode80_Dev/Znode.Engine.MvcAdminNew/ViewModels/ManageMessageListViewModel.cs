﻿using System.Collections.Generic;
using System.ComponentModel;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for MessageConfig
    /// </summary>
    public class ManageMessageListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for MessageConfigListViewModel 
        /// </summary>
        public ManageMessageListViewModel()
        {
            ManageMessage = new List<ManageMessageViewModel>();
            Portal = new List<PortalViewModel>();
        }

        public List<ManageMessageViewModel> ManageMessage { get; set; }

        public List<PortalViewModel> Portal { get; set; }

        [DisplayName("Message")]
        public string Message { get; set; }
        [DisplayName("Store Name")]
        public string StoreName { get; set; } 
    }
}