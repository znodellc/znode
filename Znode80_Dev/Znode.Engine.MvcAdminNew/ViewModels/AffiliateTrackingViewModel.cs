﻿using System;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for Affiliate Tracking
    /// </summary>
    public class AffiliateTrackingViewModel:BaseViewModel
    {
        /// <summary>
        /// Default Constructor for Affiliate Tracking
        /// </summary>
        public AffiliateTrackingViewModel()
        {

        }

        public DateTime BeginDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}