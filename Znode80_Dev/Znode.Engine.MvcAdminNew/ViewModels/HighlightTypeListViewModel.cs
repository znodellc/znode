﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for highlight type
    /// </summary>
    public class HighlightTypeListViewModel
    {
        public HighlightTypeListViewModel()
        {
            HighlightTypes = new List<HighlightTypeViewModel>();
        }

        public List<HighlightTypeViewModel> HighlightTypes { get; set; }
    }
}