﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for Edit Settings for Product
    /// </summary>
    public class ProductSettingViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ProductSettingViewModel
        /// </summary>
        public ProductSettingViewModel()
        { 
            
        }

        public string InStockMessage { get; set; }
        public string OutOfStockMessage { get; set; }
        public string BackOrderMessage { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoTitle { get; set; }
        public string SeoUrl { get; set; }

        public int ProductId { get; set; }

        public bool IsFeatured { get; set; }
        public bool IsHomepageSpecial { get; set; }
        public bool CallForPricing { get; set; }
        public bool IsFranchisable { get; set; }
        public bool AllowRecurringBilling { get; set; }

        public bool? IsNewProduct { get; set; }
    }
}