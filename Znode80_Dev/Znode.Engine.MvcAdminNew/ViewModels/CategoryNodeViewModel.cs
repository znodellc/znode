﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CategoryNodeViewModel : BaseViewModel
    {
        public CatalogViewModel Catalog { get; set; }
        public int? CatalogId { get; set; }
        public CategoryViewModel Category { get; set; }      
        public int CategoryId { get; set; }
        public int CategoryNodeId { get; set; }
        public DateTime? BeginDate { get; set; }
        public int? CssId { get; set; }
        public int? DisplayOrder { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsActive { get; set; }
        public int? MasterPageId { get; set; }
        public int? ParentCategoryNodeId { get; set; }
        public int? ThemeId { get; set; }
        public List<CatalogAssociatedCategoriesViewModel> Categories { get; set; }
        public string CatalogName { get; set; }
        public List<SelectListItem> ParentCategoryNodes { get; set; }
        public List<SelectListItem> ThemeList { get; set; }
        public List<SelectListItem> CssList { get; set; }
        public List<SelectListItem> MasterPageList { get; set; }
    }
}