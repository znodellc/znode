﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class FacetGroupListViewModel : BaseViewModel
    {
        public FacetGroupListViewModel()
        {
            FacetGroups = new List<FacetGroupViewModel>();
        }
        public List<FacetGroupViewModel> FacetGroups { get; set; }

        public List<SelectListItem> GetCatalogs { get; set; } 
    }
}