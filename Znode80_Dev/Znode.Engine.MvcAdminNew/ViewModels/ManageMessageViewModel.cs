﻿
using System.Collections.Generic;
using System.ComponentModel;
using Resources;
using System.ComponentModel.DataAnnotations;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for Manage Message
    /// </summary>
    public class ManageMessageViewModel : BaseViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for Manage Message
        /// </summary>
        public ManageMessageViewModel()
        {

        } 
        #endregion

        #region Public Properties

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredMessageKey")]
        [DisplayName("Message Key")]
        public string MessageKey { get; set; }

        [DisplayName("Description")]
        public string Description { get; set; }

        public string Value { get; set; }
        public int MessageConfigId { get; set; }
        public string PortalName { get; set; }
        public int PortalId { get; set; }
        public int MessageTypeId { get; set; }
        public int MessageId { get; set; }
        public int? LocaleId { get; set; }

        [DisplayName("Store Name")]
        public PortalViewModel StoreName { get; set; }

        public List<PortalViewModel> Portal { get; set; }

        #endregion
    }
}