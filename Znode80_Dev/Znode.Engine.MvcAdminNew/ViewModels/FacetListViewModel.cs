﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class FacetListViewModel : BaseViewModel
    {
        public FacetListViewModel()
        {
            Facets = new List<FacetViewModel>();
        }
        public List<FacetViewModel> Facets { get; set; }

    }
}