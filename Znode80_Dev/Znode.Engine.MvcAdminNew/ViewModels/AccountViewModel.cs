﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Resources;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class AccountViewModel : BaseViewModel
    {
        public int AccountId { get; set; }
        //public Collection<AddressViewModel> Addresses { get; set; }        
        public string CompanyName { get; set; }		
        public bool EmailOptIn { get; set; }
        public bool? EnableCustomerPricing { get; set; }
        public string ExternalId { get; set; }
        public bool? IsActive { get; set; }		
        public int? ProfileId { get; set; }		
        public Guid? UserId { get; set; }
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredUserName")]
        public string UserName { get; set; }
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredEmailID")]
        [EmailAddress(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidEmailAddress", ErrorMessage = "")]
        public string EmailAddress { get; set; }
        public string PasswordQuestion { get; set; }
        
        public string PasswordAnswer { get; set; }
        public bool UseWholeSalePricing { get; set; }
        //public bool IsResetPassword { get; set; }
        public string BaseUrl { get; set; }
       
    }
}