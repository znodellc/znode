﻿namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for General SEO Settings for Product, Catalog and Category
    /// </summary>
    public class GeneralSEOSettingsViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for GeneralSEOSettingsViewModel
        /// </summary>
        public GeneralSEOSettingsViewModel()
        {

        }

        public string CategoryTitle { get; set; }
        public string SEOTitle { get; set; }
        public string SEOKeywords { get; set; }
        public string SEODescription { get; set; }
        public string SEOPageName { get; set; }     
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string ProductFeature { get; set; }
        public string ProductSpecification { get; set; }
        public string AdditionalInformation { get; set; }

        public bool URL301Redirect { get; set; }
    }
}