﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CatalogAssociatedCategoriesListViewModel :BaseViewModel
    {
        public CatalogAssociatedCategoriesListViewModel()
        {
            AssociatedCategories = new List<CatalogAssociatedCategoriesViewModel>();
        }

        public List<CatalogAssociatedCategoriesViewModel> AssociatedCategories { get; set; }
    }
}