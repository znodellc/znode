﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CategoryNodeListViewModel:BaseViewModel
    {
        public CategoryNodeListViewModel()
        {
            CategoryNodeList = new List<CategoryNodeViewModel>();
        }

        public List<CategoryNodeViewModel> CategoryNodeList { get; set; }
    }
}