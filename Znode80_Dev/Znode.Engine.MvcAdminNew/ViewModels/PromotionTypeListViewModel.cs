﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PromotionTypeListViewModel
    {
        /// <summary>
        /// Constructor for PromotionsListViewModel
        /// </summary>
        public PromotionTypeListViewModel()
        {
            PromotionTypes = new List<PromotionTypeViewModel>();
        }

        public List<PromotionTypeViewModel> PromotionTypes { get; set; }   
    }
}