﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class DomainListViewModel
    {
        #region Constructor
        /// <summary>
        /// Constructor for DomainViewModel that initialises the Domains list.
        /// </summary>
        public DomainListViewModel()
        {
            Domains = new List<DomainViewModel>();
        } 
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets all Domain list.
        /// </summary>
        public List<DomainViewModel> Domains { get; set; } 
        #endregion
    }
}