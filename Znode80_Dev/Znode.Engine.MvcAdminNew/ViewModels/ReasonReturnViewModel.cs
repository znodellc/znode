﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Reason for Return order
    /// </summary>
    public class ReasonReturnViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ReasonReturnViewModel
        /// </summary>
        public ReasonReturnViewModel()
        {
        
        }

        public string  Name { get; set; }

        public int ReasonId { get; set; }

        public bool IsEnabled { get; set; }

    }
}