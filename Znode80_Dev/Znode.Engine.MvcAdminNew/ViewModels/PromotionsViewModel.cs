﻿using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Promotions
    /// </summary>
    public class PromotionsViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for PromotionsViewModel
        /// </summary>
        public PromotionsViewModel()
        {

        }

        [DisplayName("Promotion Name")]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredPromotionName")]
        public string Name { get; set; }

        public string Description { get; set; }
        public string CouponInd { get; set; }

        [DisplayName("Coupon Code")]
        public string CouponCode { get; set; }

        [DisplayName("Promotion Message")]
        public string PromotionMessage { get; set; }
        public string SKU { get; set; }
        [DisplayName("Promotion Code")]
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredPromotionCode")]
        public string PromoCode { get; set; }

        [DisplayName("Discount Product")]        
        public string DiscountProduct { get; set; }

        [DisplayName("Require Product")]
        public string RequireProduct { get; set; }

        public int PromotionId { get; set; }
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredDisplayOrder")]
        public int DisplayOrder { get; set; }
        public int PromotionTypeId { get; set; }

        [DisplayName("Available Quantity")]
        public int? CouponQuantityAvailable { get; set; }

        [DisplayName("Start Date(MM/DD/YYYY)")]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredStartDate")]
        [DisplayFormat(DataFormatString = "{0::MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime StartDate { get; set; }

        [DisplayName("End Date(MM/DD/YYYY)")]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredEndDate")]
        [DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime EndDate { get; set; }

        public bool HasCoupon { get; set; }

        public PromotionTypeViewModel PromotionType { get; set; }
        public PortalViewModel Portal { get; set; }
        public ProfileViewModel Profile { get; set; }
        public ProductListViewModel ProductList { get; set; }

        [DisplayName("Minimum Quantity")]
        public int? QuantityMinimum { get; set; }
        public int? PortalId { get; set; }
        public int? ProfileId { get; set; }
        public int? AccountId { get; set; }
        public int? CatalogId { get; set; }
        public int? CategoryId { get; set; }
        public int? SkuId { get; set; }
        public int? DiscountedProductId { get; set; }
        public int? RequiredProductId { get; set; }
        public int? ManufacturerId { get; set; }
        [DisplayName("Product Quantity")]
        public int? DiscountedProductQuantity { get; set; }        
        public int? RequiredProductMinimumQuantity { get; set; }

        [DisplayName("Discount")]
        public decimal Discount { get; set; }  
        [DisplayName("Minimum Order Amount")]
        public decimal OrderMinimum { get; set; }

        [DisplayName("Store")]
        public List<SelectListItem> PortalList { get; set; }
        [DisplayName("Require Brand")]
        public List<SelectListItem> ManufacturerList { get; set; }
        [DisplayName("Discount Type")]
        public List<SelectListItem> PromotionTypeList { get; set; }
        [DisplayName("Catalog")]
        public List<SelectListItem> CatalogList { get; set; }
        [DisplayName("Category")]
        public List<SelectListItem> CategoryList { get; set; }
        [DisplayName("Profile")]
        public List<SelectListItem> ProfileList { get; set; }
    }
}