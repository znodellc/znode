﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for Highlights
    /// </summary>
    public class HighlightsListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for HighlightsListViewModel
        /// </summary>
        public HighlightsListViewModel()
        {
            Highlights = new List<HighlightViewModel>();
            HighlightTypeList = new List<SelectListItem>();
        }

        [DisplayName("Name")]
        public string Name { get; set; }

        [DisplayName("Type")]
        public int TypeId { get; set; }

        public List<HighlightViewModel> Highlights { get; set; }
        
        public List<SelectListItem> HighlightTypeList { get; set; }
    }
}