﻿using System.ComponentModel.DataAnnotations;
using Resources;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ResetPasswordModel : BaseViewModel
    {
        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredUserName")]
        public string Username { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredPassword")]
        [RegularExpression("^(?=.*[0-9])(?=.*[a-zA-Z]).{8,}$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidPassword")]
        [MaxLength(128, ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InValidPasswordLength")]
        public string Password { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredNewConfirmPassword")]
        [Compare("Password", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorPasswordMatch")]
        public string ConfirmPassword { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredEmailID")]
        [EmailAddress(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidEmailAddress", ErrorMessage = "")]
        public string Email { get; set; }
    }
}