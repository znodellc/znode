﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for ProductType Attribute.
    /// </summary>
    public class ProductTypeAttributeViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ProductTypeAttributeViewModel
        /// </summary>
        public ProductTypeAttributeViewModel()
        { 
            
        }

        public int ProductAttributeTypeID { get; set; }
        public int ProductTypeId { get; set; }
        public int AttributeTypeId { get; set; }
    }
}