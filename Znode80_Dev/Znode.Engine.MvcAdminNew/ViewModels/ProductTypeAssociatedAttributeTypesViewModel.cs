﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for associated attributes with Product Type.
    /// </summary>
    public class ProductTypeAssociatedAttributeTypesViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ProductTypeAssociatedAttributeTypesViewModel.
        /// </summary>
        public ProductTypeAssociatedAttributeTypesViewModel()
        { 
        
        }
        public int ProductAttributeTypeID { get; set; }
        public int ProductTypeId { get; set; }
        public int AttributeTypeId { get; set; }
        public int LocaleId { get; set; }
        public int DisplayOrder { get; set; }

        public int? PortalId { get; set; }

        public string Name { get; set; }

        public bool IsPrivate { get; set; }
    }
}