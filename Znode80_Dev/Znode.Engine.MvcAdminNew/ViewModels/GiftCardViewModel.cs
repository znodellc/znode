﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Create and Edit Gift Cards
    /// </summary>
    public class GiftCardViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for GiftCardViewModel
        /// </summary>
        public GiftCardViewModel()
        {

        }
        [DisplayName("Card Number")]
        public string CardNumber { get; set; }
        
        [DisplayName("Gift Card Name")]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredGiftCardName")]
        public string Name { get; set; }    
        
        public int GiftCardId { get; set; }
        public int CreatedBy { get; set; }
        
        [DisplayName("Store Name")]
        public int PortalId { get; set; }

        [DisplayName("Account ID")]
        [RegularExpression("^[0-9]*$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidNumber")]	
        public int? AccountId { get; set; }      

        public DateTime CreateDate { get; set; }

        [DisplayName("Expiration Date(YYYY/MM/DD)")]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredExpirationDate")]
        public DateTime? ExpirationDate { get; set; }
        
        [DisplayName("Gift Card Amount")]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "RequiredGiftCardAmount")]
        [Range(0.01, 99999.00, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidAmountRange")]
        public decimal? Amount { get; set; }

        public bool EnableToCustomerAccount { get; set; }
        public List<SelectListItem> PortalList { get; set; }
    }
} 
