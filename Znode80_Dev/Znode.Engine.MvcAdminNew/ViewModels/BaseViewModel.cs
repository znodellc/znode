﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public abstract class BaseViewModel
    {

        public string ErrorMessage { get; set; }

        public bool HasError { get; set; }

        public string SuccessMessage { get; set; }

        public string Title { get; set; }

        public int Page { get; set; }

        public int TotalResults { get; set; }

        public int TotalPages { get; set; }

        public int RecordPerPage { get; set; }

        public SortCollection SortCollection { get; set; }

        public FilterCollection Filters { get; set; }

        public static string SortKey { get; set; }

        public object Clone()
        {
            var clonedObject = this.MemberwiseClone();
            return clonedObject;
        }

      
      
        
    }

}

