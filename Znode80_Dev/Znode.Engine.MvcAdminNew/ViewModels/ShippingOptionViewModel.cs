﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For Shipping Options
    /// </summary>
    public class ShippingOptionViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ShippingOptionViewModel
        /// </summary>
        public ShippingOptionViewModel()
        { 
            
        }

        public string CountryCode { get; set; }
        public string Description { get; set; }
        public string ExternalId { get; set; }
        public string ShippingCode { get; set; }

        public int ShippingId { get; set; }
        public int DisplayOrder { get; set; }
        public int ShippingOptionId { get; set; }
        public int ShippingTypeId { get; set; }

        public int? ProfileId { get; set; }

        public decimal HandlingCharge { get; set; }

        public bool IsActive { get; set; }

        public ShippingTypeModel ShippingType { get; set; }
        public ShippingRuleModel ShippingRule { get; set; }
    }
}