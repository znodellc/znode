﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Libraries.Helpers.Attributes;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For ProductViewModel
    /// </summary>
    public class ProductViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ProductViewModel
        /// </summary>
        public ProductViewModel()
        {
            
        }

        public string BundleItemsIds { get; set; }

        public string ShortDescription { get; set; }

        [UIHint("RichTextBox")]
        [AllowHtml]
        public string LongDescription { get; set; }

        [UIHint("RichTextBox")]
        [AllowHtml]
        public string FeaturesDescription { get; set; }

        [UIHint("RichTextBox")]
        [AllowHtml]
        public string ProductSpecifications { get; set; }

        [UIHint("RichTextBox")]
        [AllowHtml]
        public string ShippingInformation { get; set; }

        public string ImageAltTag { get; set; }
        public string ImageFile { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string InStockMessage { get; set; }
        public string BackOrderMessage { get; set; }
        public string OutOfStockMessage { get; set; }

        public string BrandName { get; set; }
        public string SupplierName { get; set; }
        public string TaxClassName { get; set; }
        public string ShippingRule { get; set; }
        public string ProductTypeName { get; set; }

        [Display(Name = "Product Attributes")]
        public string ProductAttributes { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string Sku { get; set; }
        public string InventoryMessage { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoTitle { get; set; }
        public string SeoPageName { get; set; }
        public string DownloadLink { get; set; }
        public string FBTProductsIds { get; set; }
        public string YMALProductsIds { get; set; }
        public string VendorName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string ProductCode { get; set; }

        public int ProductId { get; set; }
        public int ProductTypeId { get; set; }
        public int? BrandId { get; set; }
        public int? TaxClassId { get; set; }
        public int? SupplierId { get; set; }
        public int SelectedQuantity { get; set; }
        public int? ShippingRuleTypeId { get; set; }
        public int Rating { get; set; }
        public int WishListId { get; set; }
        public int? ReviewStateId { get; set; }

        public int? DisplayOrder { get; set; }
        public int? QuantityOnHand { get; set; }
        public int? ReorderLevel { get; set; }

        public int SkuId { get; set; }

        public int? ExpirationPeriod { get; set; }
        public int? ExpirationFrequency { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public int? MaxQuantity { get; set; }

        public int? MinQuantity { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public decimal? RetailPrice { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal? WholeSalePrice { get; set; }
        public decimal? Weight { get; set; }
        public decimal? Height { get; set; }
        public decimal? Width { get; set; }
        public decimal? Length { get; set; }
        public decimal? ShippingRate { get; set; }
        public bool IsActive { get; set; }
        public bool ShowAddToCart { get; set; }
        public bool ShowWishlist { get; set; }
        public bool ShowQuantity { get; set; }
        public bool IsCallForPricing { get; set; }

        public bool? TrackInventory { get; set; }
        public bool? AllowBackOrder { get; set; }
        public bool? FreeShipping { get; set; }

        [FileTypeValidation(MvcAdminConstants.ImageFileTypes, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ImageFileTypeErrorMessage")]
        [FileMaxSizeValidation(MvcAdminConstants.ImageMaxFileSize, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "FileSizeExceededErrorMessage")]
        [UIHint("FileUploader")]
        public HttpPostedFileBase ProductImage { get; set; }

        public List<SelectListItem> ProductTypeList { get; set; }
        public List<SelectListItem> ManufacturerList { get; set; }
        public List<SelectListItem> SupplierList { get; set; }
        public List<SelectListItem> TaxClassList { get; set; }
        public List<SelectListItem> ShippingRuleTypeList { get; set; }
        public List<SelectListItem> ExpirationFrequencyList { get; set; }
    }
}