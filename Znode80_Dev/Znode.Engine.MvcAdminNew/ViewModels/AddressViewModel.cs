﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Address
    /// </summary>
    public class AddressViewModel : BaseViewModel
    {
        /// <summary>
        /// Consructor for AddressViewModel
        /// </summary>
        public AddressViewModel()
        {

        }

        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string StreetAddress1 { get; set; }
        public string StreetAddress2 { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string StateCode { get; set; }
        public string CountryCode { get; set; }

        public int AccountId { get; set; }
        public int AddressId { get; set; }

        public bool IsDefaultBilling { get; set; }
        public bool IsDefaultShipping { get; set; }
        public bool UseSameAsShippingAddress { get; set; }     
    }
}