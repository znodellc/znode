﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Supplier
    /// </summary>
    public class SupplierViewModel : BaseViewModel
    {
        /// <summary>
        /// Consructor for SupplierViewModel
        /// </summary>
        public SupplierViewModel()
        {
 
        }

        public string Name { get; set; }
        public string SupplierCode { get; set; }
        public string Description { get; set; }
        public string ContactFirstName { get; set; }
        public string ContactLastName { get; set; }
        public string ContactPhoneNumber { get; set; }
        public string ContactEmail { get; set; }
        public string NotificationEmailId { get; set; }
        public string EmailNotification { get; set; }
        public string Custom1 { get; set; }
        public string Custom2 { get; set; }
        public string Custom3 { get; set; }
        public string Custom4 { get; set; }
        public string Custom5 { get; set; }

        public int SupplierId { get; set; }
        public int? DisplayOrder { get; set; }

        public bool EnableEmailNotification { get; set; }
    }
}