﻿
using Resources;
using System.ComponentModel.DataAnnotations;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model to display domains
    /// </summary>
    public class DomainViewModel
    {
        #region Public Properties

        public int DomainId { get; set; }
        public int PortalId { get; set; } 

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        //[Url(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidEmailAddress")]
        public string DomainName { get; set; }
        public string ApiKey { get; set; }

        public bool IsActive { get; set; }
        
        #endregion
    }
}