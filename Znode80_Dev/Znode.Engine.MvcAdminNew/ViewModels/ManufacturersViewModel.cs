﻿
using Resources;
using System.ComponentModel.DataAnnotations;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For add New Manufacturer 
    /// </summary>
    public class ManufacturersViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ManufacturerViewModel
        /// </summary>
        public ManufacturersViewModel()
        { 
            
        }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string Name { get; set; }
        public string Description { get; set; }
        [EmailAddress(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ValidEmailAddress", ErrorMessage="")]
        public string Email { get; set; }

        public int ManufacturerId { get; set; }

        [Display(Name="Is Active?")]
        public bool IsActive { get; set; }
    }
}