﻿using Resources;
using System.ComponentModel.DataAnnotations;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for create and edit Facet
    /// </summary>
    public class FacetViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for FacetViewModel
        /// </summary>
        public FacetViewModel()
        {

        }
        public int FacetID { get; set; }

        public int? FacetGroupID { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredFacetName")]
        [RegularExpression("^[a-zA-Z0-9 _<>]+$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidFacetName")]
        public string FacetName { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "RequiredDisplayOrder")]
        [RegularExpression("^(1|[1-9][0-9]*)$", ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "InvalidDisplayOrder")]
        public int? FacetDisplayOrder { get; set; }

        public string IconPath { get; set; }

        public bool IsEditMode { get; set; }
    }
}