﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class VendorAccountListViewModel : BaseViewModel
    {
        public VendorAccountListViewModel()
        {
            VendorAccount = new List<VendorAccountViewModel>();
        }

        public List<VendorAccountViewModel> VendorAccount { get; set; }
    }
}