﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Add Attribute Type
    /// </summary>
    public class AttributeTypesViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for AttributeTypeViewMode
        /// </summary>
        public AttributeTypesViewModel()
        {
            GetAttributesList = new List<SelectListItem>();
        }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceType = typeof(ZnodeResources), ErrorMessageResourceName = "ErrorRequired")]
        public int DisplayOrder { get; set; }

        public string Description { get; set; }

        public int AttributeTypeId { get; set; }
        public int LocaleId { get; set; }
        public int ProductTypeId { get; set; }

        public List<SelectListItem> GetAttributesList { get; set; }
    }
}