﻿using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// List View Model for Manufacturers
    /// </summary>
    public class ShippingOptionListViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ShippingOptionListViewModel
        /// </summary>
        public ShippingOptionListViewModel()
		{
            ShippingOptions = new List<ShippingOptionViewModel>();
		}
        public List<ShippingOptionViewModel> ShippingOptions { get; set; }
    }
}