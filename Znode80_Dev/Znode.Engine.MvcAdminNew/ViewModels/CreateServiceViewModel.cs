﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for create Service
    /// </summary>
    public class CreateServiceViewModel : BaseViewModel
    {
        /// <summary>
        /// Consructor for CreateServiceViewModel
        /// </summary>
        public CreateServiceViewModel()
        {
 
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string EmailId { get; set; }
        public string PhoneNumber { get; set; }
        public string DisplayName { get; set; }
        public string Message { get; set; }
    }
}