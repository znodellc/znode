﻿using Resources;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Web;
using Znode.Libraries.Helpers.Attributes;
using Znode.Engine.MvcAdmin.Helpers;  

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// Model for PortalViewModel
    /// </summary>
    public class PortalViewModel : BaseViewModel
    {
        #region public constructor 
        public PortalViewModel()
        {
            StoreName = ZnodeResources.LabelAllPortals;
        } 
        #endregion       
        #region Public Properties
        public int PortalId { get; set; }

        [Required]
        [DisplayName("Store Name")]
        public string StoreName { get; set; }

        [Required]
        public string CompanyName { get; set; }
        public bool IsCurrentPortal { get; set; }
        public bool PersistentCartEnabled { get; set; }

        
        public int DefaultAnonymousProfileId { get; set; }
        public int MaxRecentViewItemToDisplay { get; set; }
        public string DomainName { get; set; }
    
        [Required]
        [DisplayName("Select a Logo")]
        [FileTypeValidation(MvcAdminConstants.ImageFileTypes, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ImageFileTypeErrorMessage")]
        [FileMaxSizeValidation(MvcAdminConstants.ImageMaxFileSize, ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "FileSizeExceededErrorMessage")]
        [UIHint("FileUploader")]
        public HttpPostedFileBase LogoPath { get; set; }

        public string LogoPathImageName { get; set; }
        public bool UseSsl { get; set; }

        [Required]
        [RegularExpression("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidEmailAddress")]
        public string AdminEmail { get; set; }

        [Required]
        [RegularExpression("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidEmailAddress")]
        public string SalesEmail { get; set; }

        [Required]
        [RegularExpression("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$", ErrorMessageResourceType = typeof(Resources.ZnodeResources), ErrorMessageResourceName = "ValidEmailAddress")]
        public string CustomerServiceEmail { get; set; }

        [Required]
        public string CustomerServicePhoneNumber { get; set; }

        [Required]
        public string SalesPhoneNumber { get; set; }
    
        public bool? EnableAddressValidation { get; set; }
        public bool? RequireValidatedAddress { get; set; }
        public bool? EnableCustomerPricing { get; set; }
        public int? CurrencyTypeId { get; set; }
        public int? DefaultRegisteredProfileId { get; set; }
        public string DimensionUnit { get; set; }
       
        public string FedExAccountNumber { get; set; }
        public bool? FedExAddInsurance { get; set; }
        public string FedExClientProductId { get; set; }
        public string FedExClientProductVersion { get; set; }
        public string FedExCspKey { get; set; }
        public string FedExCspPassword { get; set; }
        public string FedExDropoffType { get; set; }
        public string FedExMeterNumber { get; set; }
        public string FedExPackagingType { get; set; }
        public string FedExProductionKey { get; set; }
        public string FedExSecurityCode { get; set; }
        public bool? FedExUseDiscountRate { get; set; }
        public string GoogleAnalyticsCode { get; set; }
        public string ImageNotAvailablePath { get; set; }
        public bool InclusiveTax { get; set; }
        public bool IsActive { get; set; }
        public bool IsShippingTaxable { get; set; }
        
        public string MasterPage { get; set; }
        public int MaxCatalogCategoryDisplayThumbnails { get; set; }
        public byte MaxCatalogDisplayColumns { get; set; }
        public int MaxCatalogDisplayItems { get; set; }
        public int MaxCatalogItemCrossSellWidth { get; set; }
        public int MaxCatalogItemLargeWidth { get; set; }
        public int MaxCatalogItemMediumWidth { get; set; }
        public int MaxCatalogItemSmallThumbnailWidth { get; set; }
        public int MaxCatalogItemSmallWidth { get; set; }
        public int MaxCatalogItemThumbnailWidth { get; set; }
        public string MobileTheme { get; set; }
        public string OrderReceiptAffiliateJavascript { get; set; }
        public string SeoDefaultCategoryDescription { get; set; }
        public string SeoDefaultCategoryKeyword { get; set; }
        public string SeoDefaultCategoryTitle { get; set; }
        public string SeoDefaultContentDescription { get; set; }
        public string SeoDefaultContentKeyword { get; set; }
        public string SeoDefaultContentTitle { get; set; }
        public string SeoDefaultProductDescription { get; set; }
        public string SeoDefaultProductKeyword { get; set; }
        public string SeoDefaultProductTitle { get; set; }
        public string ShippingOriginAddress1 { get; set; }
        public string ShippingOriginAddress2 { get; set; }
        public string ShippingOriginCity { get; set; }
        public string ShippingOriginCountryCode { get; set; }
        public string ShippingOriginPhoneNumber { get; set; }
        public string ShippingOriginStateCode { get; set; }
        public string ShippingOriginZipCode { get; set; }
        public int ShopByPriceIncrement { get; set; }
        public int ShopByPriceMax { get; set; }
        public int ShopByPriceMin { get; set; }
        public bool ShowAlternateImageInCategory { get; set; }
        public bool ShowSwatchInCategory { get; set; }
        public string SiteWideAnalyticsJavascript { get; set; }
        public string SiteWideBottomJavascript { get; set; }
        public string SiteWideTopJavascript { get; set; }
        public string SmtpPassword { get; set; }
        public int? SmtpPort { get; set; }
        public string SmtpServer { get; set; }
        public string SmtpUsername { get; set; }
        public int? SplashCategoryId { get; set; }
        public string SplashImageFile { get; set; }
        public string TimeZoneOffset { get; set; }
        public string UpsKey { get; set; }
        public string UpsPassword { get; set; }
        public string UpsUsername { get; set; }
        public bool? UseDynamicDisplayOrder { get; set; }
        public string WeightUnit { get; set; }

        public int? CatalogId { get; set; }
        public List<SelectListItem> Catalogs { get; set; }

        public int? ThemeId { get; set; }
        public List<SelectListItem> Themes { get; set; }

        public int? CSSId { get; set; }
        public List<SelectListItem> CSS { get; set; }

        public int? DefaultOrderStateId { get; set; }
        public List<SelectListItem> OrderState { get; set; }

        public int? DefaultProductReviewStateId { get; set; }
        public List<SelectListItem> ProductReviewState { get; set; }

        public string DefaultReviewStatus { get; set; }
        public List<SelectListItem> DefaultReviewStates { get; set; }

        public int LocaleId { get; set; }
        public List<SelectListItem> Locales { get; set; }
        #endregion

       
    }
}