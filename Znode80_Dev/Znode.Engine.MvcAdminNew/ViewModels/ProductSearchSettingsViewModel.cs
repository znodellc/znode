﻿using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Resources;
using Znode.Engine.MvcAdmin.Models;
namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductSearchSettingsViewModel : BaseViewModel
    {
        public ProductLevelSearchSettingViewModel ProductLevelSettings { get; set; }
    }

    public class ProductLevelSearchSettingViewModel : BaseViewModel
    {
        public int CatalogId { get; set; }
        public int BrandId { get; set; }
        public int ProductTypeId { get; set; }
        public int ProductCategoryId { get; set; }

        public List<SelectListItem> Catalogs { get; set; }
        public List<SelectListItem> Brands { get; set; }
        public List<SelectListItem> ProductTypes { get; set; }
        public List<SelectListItem> ProductCatagories { get; set; }
    }
}