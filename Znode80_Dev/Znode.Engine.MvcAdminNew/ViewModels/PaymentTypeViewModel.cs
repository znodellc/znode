﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PaymentTypeViewModel
    {
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public int PaymentTypeId { get; set; }
    }
}