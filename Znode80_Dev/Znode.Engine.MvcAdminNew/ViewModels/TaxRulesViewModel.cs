﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for create and edit TaxRules
    /// </summary>
    public class TaxRulesViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for TaxRuleViewModel
        /// </summary>
        public TaxRulesViewModel()
        {

        }

        public int Precedence { get; set; }
        
        public decimal? SalesTax { get; set; }
        public decimal? VATTax { get; set; }
        public decimal? GSTTax { get; set; }
        public decimal? PSTTax { get; set; }
        public decimal? HSTTax { get; set; }
        
        public bool InclusiveTax { get; set; }
    }
}