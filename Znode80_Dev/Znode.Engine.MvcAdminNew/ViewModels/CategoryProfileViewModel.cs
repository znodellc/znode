﻿using System;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CategoryProfileViewModel : BaseViewModel
    {
        public int CategoryProfileID { get; set; }
        public int CategoryID { get; set; }
        public int ProfileID { get; set; }
        public DateTime EffectiveDate { get; set; }
    }
}