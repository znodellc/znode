﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class PaymentOptionViewModel
    {
        public int DisplayOrder { get; set; }
        public bool? EnableAmericanExpress { get; set; }
        public bool? EnableDiscover { get; set; }
        public bool? EnableMasterCard { get; set; }
        public bool? EnableRecurringPayments { get; set; }
        public bool? EnableVault { get; set; }
        public bool? EnableVisa { get; set; }
        public bool IsActive { get; set; }
        public bool? IsRmaCompatible { get; set; }
        public string Partner { get; set; }
        public PaymentGatewayViewModel PaymentGateway { get; set; }
        public int? PaymentGatewayId { get; set; }
        public string PaymentGatewayPassword { get; set; }
        public string PaymentGatewayUsername { get; set; }
        public int PaymentOptionId { get; set; }
        public PaymentTypeViewModel PaymentType { get; set; }
        public int PaymentTypeId { get; set; }
        public bool PreAuthorize { get; set; }
        public int? ProfileId { get; set; }
        public string ProfileName { get; set; }
        public bool TestMode { get; set; }
        public string TransactionKey { get; set; }
        public string Vendor { get; set; }

        public PaymentOptionViewModel()
        {
            PaymentGateway = new PaymentGatewayViewModel();
            PaymentType = new PaymentTypeViewModel();
        }
    }
}