﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for create RMAConfiguration
    /// </summary>
    public class RMAConfigurationViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for RMAConfigurationViewModel
        /// </summary>
        public RMAConfigurationViewModel()
        {
 
        }

        public string DisplayName { get; set; }
        public string EmailId { get; set; }
        public string ReturnMailingAddress { get; set; }
        public string ShippingInstruction { get; set; }
        public string GiftCardNotification { get; set; }

        public int RMAConfigId { get; set; }
        public int? MaxDays { get; set; }
        public int? GiftCardExprirationPeriod { get; set; }

        public bool? EnableEmailNotification { get; set; }
    }
}