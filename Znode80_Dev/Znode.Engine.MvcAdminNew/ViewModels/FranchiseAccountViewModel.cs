﻿using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Create and Edit Franchise Account
    /// </summary>
    public class FranchiseAccountViewModel : BaseViewModel
    {      
        /// <summary>
        /// Constructor for Create and Edit Franchise Account View Model
        /// </summary>
        public FranchiseAccountViewModel()
        {

        }

        public string UserName { get; set; }
        public string StoreName { get; set; }
        public string SiteURL { get; set; }       
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }       
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }

        public int AccountId { get; set; }
        public int FranchiseNumber { get; set; }

        public HttpPostedFileBase Logo { get; set; }
    }
}