﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For Message Config in Vendor
    /// </summary>
    public class MessageConfigViewModel : BaseViewModel 
    {
        /// <summary>
        /// Constructor for MessageConfigViewModel()
        /// </summary>
        public MessageConfigViewModel()
        { 
        
        }

        public string Key { get; set; }
        public string Value { get; set; }

        public int MessageConfigId { get; set; }
    }
}