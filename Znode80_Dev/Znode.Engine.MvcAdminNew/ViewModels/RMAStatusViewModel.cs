﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for RMA Status
    /// </summary>
    public class RMAStatusViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for RMAStatusViewModel
        /// </summary>
        public RMAStatusViewModel()
        {
 
        }

        public string RequestStatusName { get; set; }

        public int RequestStatusId { get; set; }
    }
}