﻿using Resources;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class ProductViewModelMap
    {
        /// <summary>
        /// To map model to ViewModel
        /// </summary>
        /// <param name="model">ProductModel</param>
        /// <returns>returns  ProductViewModel</returns>
        public static ProductViewModel ToViewModel(ProductModel model)
        {
            var viewModel = new ProductViewModel()
            {
                ProductId = model.ProductId,
                AllowBackOrder = model.AllowBackOrder,
                BackOrderMessage = model.BackOrderMessage,
                BundleItemsIds = model.BundledProductIds,

                ImageAltTag = model.ImageAltTag,
                ImageFile = model.ImageFile,
                ImageLargePath = HelperMethods.GetImagePath(model.ImageLargePath),
                ImageMediumPath = HelperMethods.GetImagePath((model.Skus != null && model.Skus.Any() && Path.GetFileName(model.Skus.First().ImageMediumPath) != MvcAdminConstants.NoImageName) ? HelperMethods.GetImagePath(model.Skus.First().ImageMediumPath) : HelperMethods.GetImagePath(model.ImageMediumPath)),
                ImageSmallThumbnailPath = HelperMethods.GetImagePath((model.Skus != null && model.Skus.Any() && Path.GetFileName(model.Skus.First().ImageSmallThumbnailPath) != MvcAdminConstants.NoImageName) ? HelperMethods.GetImagePath(model.Skus.First().ImageSmallThumbnailPath) : HelperMethods.GetImagePath(model.ImageSmallThumbnailPath)),
                InStockMessage = model.InStockMessage,
                IsActive = model.IsActive,
                IsCallForPricing = model.CallForPricing,
                MaxQuantity = model.MaxSelectableQuantity.GetValueOrDefault(10),
                MinQuantity = model.MinSelectableQuantity.GetValueOrDefault(1),
                Name = model.Name,
                OutOfStockMessage = model.OutOfStockMessage,
                ProductCode = model.ProductNumber,
                RetailPrice = GetProductPrice(model, "retailprice"),
                SalePrice = GetProductPrice(model, "saleprice"),
                SeoDescription = model.SeoDescription,
                SeoKeywords = model.SeoKeywords,
                SeoPageName = model.SeoUrl,
                SeoTitle = model.SeoTitle,
                ProductSpecifications = Equals(model.Specifications, null) ? String.Empty : HttpUtility.HtmlDecode(model.Specifications),
                LongDescription = Equals(model.Description, null) ? String.Empty : HttpUtility.HtmlDecode(model.Description),
                FeaturesDescription = Equals(model.FeaturesDescription, null) ? String.Empty : HttpUtility.HtmlDecode(model.FeaturesDescription),
                ShippingInformation = Equals(model.AdditionalInfo, null) ? String.Empty : HttpUtility.HtmlDecode(model.AdditionalInfo),
                ShortDescription = Equals(model.ShortDescription, null) ? String.Empty : HttpUtility.HtmlDecode(model.ShortDescription),
                TrackInventory = model.TrackInventory,
                WholeSalePrice = GetProductPrice(model, "wholesaleprice"),
                Weight = model.Weight,
                Height = model.Height,
                Width = model.Width,
                Length = model.Length,
                ProductTypeId = model.ProductTypeId,
                TaxClassId = model.TaxClassId,
                ReviewStateId = model.ReviewStateId,
                DisplayOrder = model.DisplayOrder,
                ShippingRuleTypeId = model.ShippingRuleTypeId,
                DownloadLink = model.DownloadLink,
                FreeShipping = model.AllowFreeShipping,
                ExpirationPeriod = model.ExpirationPeriod,
                ExpirationFrequency = model.ExpirationFrequency,
                ShippingRate = model.ShippingRate,
                BrandName = model.ManufacturerName,
                SupplierName = Equals(model.SupplierName, null) ? ZnodeResources.LabelNotApplicable : string.IsNullOrEmpty(model.SupplierName) ? ZnodeResources.LabelNotApplicable : model.SupplierName,
                TaxClassName = Equals(model.TaxClassName, null) ? string.Empty : model.TaxClassName,
                ShippingRule = Equals(model.ShippingRule, null) ? string.Empty : model.ShippingRule,
                ProductTypeName = Equals(model.ProductTypeName, null) ? string.Empty : model.ProductTypeName,
                Sku = Equals(model.SkuName, null) ? string.Empty : model.SkuName,
                SkuId = model.SkuId,
                ProductAttributes = Equals(model.AttributeIds, null) ? string.Empty : model.AttributeIds,
                QuantityOnHand = model.QuantityOnHand,
                ReorderLevel = model.ReorderLevel,
            };

            return viewModel;
        }

        /// <summary>
        /// To map ListViewModel
        /// </summary>
        /// <param name="models">Collection<ProductModel></param>
        /// <returns>returns ProductListViewModel</returns>
        public static ProductListViewModel ToListViewModel(Collection<ProductModel> models)
        {
            ProductListViewModel list = new ProductListViewModel();
            foreach (ProductModel model in models)
            {
                list.Products.Add(ToViewModel(model));
            }
            return list;
        }

        /// <summary>
        /// Model Mapper for product
        /// </summary>
        /// <param name="viewModel">ProductViewModel</param>
        /// <returns>returns ProductModel</returns>
        public static ProductModel ToModel(ProductViewModel viewModel)
        {
            var model = new ProductModel()
            {
                ProductId = viewModel.ProductId,
                AllowBackOrder = viewModel.AllowBackOrder,
                BackOrderMessage = viewModel.BackOrderMessage,
                BundledProductIds = viewModel.BundleItemsIds,
                CallForPricing = viewModel.IsCallForPricing,
                ImageAltTag = Equals(viewModel.ImageAltTag, null) ? String.Empty : viewModel.ImageAltTag,
                ImageFile = viewModel.ImageFile,
                ImageLargePath = viewModel.ImageLargePath,
                ImageMediumPath = viewModel.ImageMediumPath,
                ImageSmallThumbnailPath = viewModel.ImageSmallThumbnailPath,
                InStockMessage = viewModel.InStockMessage,
                IsActive = viewModel.IsActive,
                MaxSelectableQuantity = viewModel.MaxQuantity,
                MinSelectableQuantity = viewModel.MinQuantity,
                Name = viewModel.Name,
                OutOfStockMessage = viewModel.OutOfStockMessage,
                ProductNumber = viewModel.ProductCode,
                RetailPrice = viewModel.RetailPrice,
                SalePrice = viewModel.SalePrice,
                WholesalePrice = viewModel.WholeSalePrice,
                SeoDescription = viewModel.SeoDescription,
                SeoKeywords = viewModel.SeoKeywords,
                SeoTitle = viewModel.SeoTitle,
                SeoUrl = viewModel.SeoPageName,
                AdditionalInfo = Equals(viewModel.ShippingInformation, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.ShippingInformation),
                Specifications = Equals(viewModel.ProductSpecifications, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.ProductSpecifications),
                Description = Equals(viewModel.LongDescription, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.LongDescription),
                FeaturesDescription = Equals(viewModel.FeaturesDescription, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.FeaturesDescription),
                ShortDescription = Equals(viewModel.ShortDescription, null) ? String.Empty : HttpUtility.HtmlEncode(viewModel.ShortDescription),
                TrackInventory = viewModel.TrackInventory,                
                ProductTypeId = viewModel.ProductTypeId,
                ShippingRate = viewModel.ShippingRate,
                TaxClassId = viewModel.TaxClassId,
                ReviewStateId = viewModel.ReviewStateId,
                DisplayOrder = viewModel.DisplayOrder,
                ShippingRuleTypeId = viewModel.ShippingRuleTypeId,
                DownloadLink = viewModel.DownloadLink,
                AllowFreeShipping = viewModel.FreeShipping,
                ExpirationPeriod = viewModel.ExpirationPeriod,
                ExpirationFrequency = viewModel.ExpirationFrequency,
                Weight = viewModel.Weight,
                Height = viewModel.Height,
                Width = viewModel.Width,
                Length = viewModel.Length
            };
            return model;
        }

        /// <summary>
        /// To map IEnumerable<ProductTypeModel> model to List<SelectListItem>
        /// </summary>
        /// <param name="model">IEnumerable<ProductTypeModel></param>
        /// <returns>List<SelectListItem></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ProductTypeModel> model)
        {
            List<SelectListItem> productTypeItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                productTypeItems = (from item in model
                                    select new SelectListItem
                                    {
                                        Text = item.Name,
                                        Value = item.ProductTypeId.ToString(),
                                    }).ToList();
            }
            return productTypeItems;
        }

        /// <summary>
        /// To map IEnumerable<ManufacturerModel> model to List<SelectListItem>
        /// </summary>
        /// <param name="model">IEnumerable<ManufacturerModel></param>
        /// <returns>returns List of SelectListItem</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ManufacturerModel> model)
        {
            List<SelectListItem> manufacturerItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                manufacturerItems = (from item in model
                                     select new SelectListItem
                                     {
                                         Text = item.Name,
                                         Value = item.ManufacturerId.ToString(),
                                     }).ToList();
            }
            return manufacturerItems;
        }

        /// <summary>
        ///  To map IEnumerable<SupplierModel> model to List<SelectListItem>
        /// </summary>
        /// <param name="model">IEnumerable<SupplierModel></param>
        /// <returns>returns List of SelectListItem</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<SupplierModel> model)
        {
            List<SelectListItem> supplierItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                supplierItems = (from item in model
                                 select new SelectListItem
                                 {
                                     Text = item.Name,
                                     Value = item.SupplierId.ToString(),
                                 }).ToList();
            }
            return supplierItems;
        }

        /// <summary>
        /// To map IEnumerable<TaxClassModel> model to List<SelectListItem>
        /// </summary>
        /// <param name="model">IEnumerable<TaxClassModel> </param>
        /// <returns>returns List of SelectListItem</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<TaxClassModel> model)
        {
            List<SelectListItem> taxClassItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                taxClassItems = (from item in model
                                 select new SelectListItem
                                 {
                                     Text = item.Name,
                                     Value = item.TaxClassId.ToString(),
                                 }).ToList();
            }
            return taxClassItems;
        }

        /// <summary>
        /// To map IEnumerable<ShippingRuleTypeModel> model to List<SelectListItem>
        /// </summary>
        /// <param name="model">IEnumerable<ShippingRuleTypeModel></param>
        /// <returns>IEnumerable<ShippingRuleTypeModel></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ShippingRuleTypeModel> model)
        {
            List<SelectListItem> shippingRuleTypeItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                shippingRuleTypeItems = (from item in model
                                         select new SelectListItem
                                         {
                                             Text = item.Name,
                                             Value = item.ShippingRuleTypeId.ToString(),
                                         }).ToList();
            }
            return shippingRuleTypeItems;
        }

        public static List<SelectListItem> ToListItems()
        {
            List<SelectListItem> expirationFrequencyItems = new List<SelectListItem>();
            expirationFrequencyItems.Add(new SelectListItem { Value = Convert.ToInt32(DurationType.Days).ToString(), Text = DurationType.Days.ToString() });
            expirationFrequencyItems.Add(new SelectListItem { Value = Convert.ToInt32(DurationType.Weeks).ToString(), Text = DurationType.Weeks.ToString() });
            expirationFrequencyItems.Add(new SelectListItem { Value = Convert.ToInt32(DurationType.Months).ToString(), Text = DurationType.Months.ToString() });
            expirationFrequencyItems.Add(new SelectListItem { Value = Convert.ToInt32(DurationType.Years).ToString(), Text = DurationType.Years.ToString() });
            return expirationFrequencyItems;
        }

        /// <summary>
        ///  To map AttributeTypeValueListModel to List of AttributeTypeValueViewModel
        /// </summary>
        /// <param name="models">List of model</param>
        /// <returns>returns list of AttributeTypeValueViewModel view model.</returns>
        public static List<AttributeTypeValueViewModel> ToListViewModel(AttributeTypeValueListModel models)
        {
            List<AttributeTypeValueViewModel> list = new List<AttributeTypeValueViewModel>();
            var dataModel = models.AttributeTypeValueList.ToList().GroupBy(x => x.AttributeTypeId)
                                      .Select(grp => new { AttributeTypeId = grp.Key, dataModel = grp.ToList() })
                                      .ToList();

            for (int counter = 0; counter < dataModel.Count; counter++)
            {
                var model = dataModel[counter];
                AttributeTypeValueViewModel items = new AttributeTypeValueViewModel();
                List<SelectListItem> listItems = new List<SelectListItem>();
                items.AttributeName = model.dataModel[0].AttributeType;
                items.AttributeTypeId = model.dataModel[0].AttributeTypeId;
                items.IsGiftCard = model.dataModel[0].IsGiftCard.Value;
                foreach (AttributeTypeValueModel lst in model.dataModel)
                {
                    listItems.Add(new SelectListItem { Value = lst.AttributeId.ToString(), Text = lst.AttributeValue });
                }
                items.AttributeValueList = listItems;
                list.Add(items);
            }

            return list;
        }

        /// <summary>
        /// To get formatted price 
        /// </summary>
        /// <param name="model">ProductModel model</param>
        /// <param name="priceType">string priceType</param>
        /// <returns>returns decimal</returns>
        private static decimal GetProductPrice(ProductModel model, string priceType)
        {
            decimal finalPrice = 0.00m;

            if (priceType.ToLower() == "originalprice")
            {
                finalPrice = model.RetailPrice.HasValue ? model.RetailPrice.Value : 0;
                return finalPrice;
            }

            if (model.PromotionPrice.HasValue)
            {
                finalPrice = model.PromotionPrice.Value;
                return finalPrice;
            }

            if (model.SalePrice.HasValue)
            {
                finalPrice = model.SalePrice.Value;
                return finalPrice;
            }

            finalPrice = model.RetailPrice.HasValue ? model.RetailPrice.Value : 0;
            return finalPrice;
        }
    }
}