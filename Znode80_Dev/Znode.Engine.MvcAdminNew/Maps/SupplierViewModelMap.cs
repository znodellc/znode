﻿using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    #region Supplier View Model Map
    /// <summary>
    /// Static class for SupplierViewModelMap
    /// </summary>
    public static class SupplierViewModelMap
    {
        /// <summary>
        /// Convert VendorAccountViewModel to SupplierModel
        /// </summary>
        /// <param name="supplierAccountViewModel">to convert to model</param>
        /// <returns>SupplierModel</returns>
        public static SupplierModel ToModel(VendorAccountViewModel supplierAccountViewModel)
        {

            return new SupplierModel()
            {
                Name = supplierAccountViewModel.Name,
                ContactFirstName = supplierAccountViewModel.FirstName,
                ContactLastName = supplierAccountViewModel.LastName,
                ContactEmail = supplierAccountViewModel.Email,
                ContactPhone = supplierAccountViewModel.PhoneNumber,

                Address = new AddressModel
                {
                    CompanyName = supplierAccountViewModel.CompanyName,
                    StreetAddress1 = supplierAccountViewModel.Street1,
                    StreetAddress2 = supplierAccountViewModel.Street2,
                    City = supplierAccountViewModel.City,
                    StateCode = supplierAccountViewModel.State,
                    PostalCode = supplierAccountViewModel.PostalCode,
                },

                Account = new AccountModel
                {
                    AccountId = supplierAccountViewModel.AccountId,
                }
            };
        }

        /// <summary>
        /// Convert SupplierModel to VendorAccountViewModel
        /// </summary>
        /// <param name="supplierModel">to convert to view model</param>
        /// <returns>VendorAccountViewModel</returns>
        public static VendorAccountViewModel ToViewModel(SupplierModel supplierModel)
        {
            return new VendorAccountViewModel()
            {
                Name = supplierModel.Name,
                FirstName = supplierModel.ContactFirstName,
                LastName = supplierModel.ContactLastName,
                Email = supplierModel.ContactEmail,
                PhoneNumber = supplierModel.ContactPhone,
                CompanyName = supplierModel.Address.CompanyName,
                Street1 = supplierModel.Address.StreetAddress1,
                Street2 = supplierModel.Address.StreetAddress2,
                City = supplierModel.Address.City,
                State = supplierModel.Address.StateCode,
                PostalCode = supplierModel.Address.PostalCode
            };
        }

    #endregion
        
        /// <summary>
        /// Convert Supplier Model to VendorAccountListViewModel
        /// </summary>
        /// <param name="models">to convert to list view model</param>
        /// <returns>VendorAccountListViewModel</returns>
        public static VendorAccountListViewModel ToListViewModel(IEnumerable<SupplierModel> models)
        {
            var viewModel = new VendorAccountListViewModel()
            {
                VendorAccount = models.ToList().Select(
                x => new VendorAccountViewModel()
                {
                    //AccountId = x.Account.AccountId,
                    FirstName = x.ContactFirstName,
                    LastName = x.ContactLastName,
                    PhoneNumber = x.ContactPhone,
                    Email = x.ContactEmail,
                    
                }).ToList()
            };

            return viewModel;
        }

    }

}
