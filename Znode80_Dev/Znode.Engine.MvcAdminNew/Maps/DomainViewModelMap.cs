﻿using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Converts domain model to domain view model.
    /// </summary>
    public class DomainViewModelMap
    {
        /// <summary>
        /// Returns view model for domain model.
        /// </summary>
        /// <param name="model">Domain Model</param>
        /// <returns>Domain View Model</returns>
        public static DomainViewModel ToViewModel(DomainModel model)
        {
            return new DomainViewModel(){
                ApiKey = model.ApiKey,
                DomainId = model.DomainId,
                DomainName = model.DomainName,
                IsActive = model.IsActive,
                PortalId = model.PortalId
            };
        }

        /// <summary>
        /// Converts Domain List View Model to view model.
        /// </summary>
        /// <param name="models">List of domain models</param>
        /// <returns>List of domain view Models</returns>
        public static DomainListViewModel ToListViewModel(IEnumerable<DomainModel>models)
        {
            if (models != null && models.Count() > 0)
            {
                var viewModel = new DomainListViewModel()
                {
                    Domains = models.ToList().Select(
                    x => new DomainViewModel()
                    {
                        ApiKey = x.ApiKey,
                        DomainId = x.DomainId,
                        DomainName = x.DomainName,
                        IsActive = x.IsActive,
                        PortalId = x.PortalId
                    }).ToList()
                };
                return viewModel;
            }
            return null;
        }

        /// <summary>
        /// Convert DomainViewModel To DomainModel.
        /// </summary>
        /// <param name="viewModel">DomainViewModel</param>
        /// <returns>Returns DomainModel</returns>
        public static DomainModel ToModel(DomainViewModel viewModel)
        {
            return new DomainModel()
            {
                DomainId = viewModel.DomainId,
                DomainName = viewModel.DomainName,
                ApiKey = Guid.NewGuid().ToString(),
                IsActive = viewModel.IsActive,
                PortalId = viewModel.PortalId
            };
        }
    }
}