﻿using System.Collections.ObjectModel;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    ///  Static Class for Vendor View Model Mapper 
    /// </summary>
    public static class VendorViewModelMap
    {
        public static AccountModel ToModel(VendorAccountViewModel VendorAccountViewModel)
        {

            if (VendorAccountViewModel == null)
            {
                return null;
            }

            return new AccountModel()
            {
                Email = VendorAccountViewModel.Email,
                UserName = VendorAccountViewModel.Name,
                RoleName="Vendor",
            };
        }

        public static VendorAccountViewModel ToViewModel(AccountModel VendorAccountViewModel)
        {
            if (VendorAccountViewModel == null)
            {
                return null;
            }

            return new VendorAccountViewModel()
            {
                Email = VendorAccountViewModel.Email,
                Name = VendorAccountViewModel.UserName,
            };
        }
    }
}