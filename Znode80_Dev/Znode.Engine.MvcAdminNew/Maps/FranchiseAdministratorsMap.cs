﻿using System.Collections.ObjectModel;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for  Franchise Administrators Mapper
    /// </summary>
    public static class FranchiseAdministratorsMap
    {
        /// <summary>
        /// Convert Franchise Administrators ViewModel to Franchise Administrators Model
        /// </summary>
        /// <param name="model">AccountModel model</param>
        /// <returns>Franchise Administrators ViewModel</returns>
        public static FranchiseAdministratorsViewModel ToViewModel(AccountModel model)
        {
            return new FranchiseAdministratorsViewModel()
            {
                Email = model.Email,
                FranchiseNumber=model.ExternalId,
                StoreName=model.CompanyName,
                
                Addresses = new Collection<AddressViewModel>(model.Addresses.Select(AddressViewModelMap.ToViewModel).OrderBy(x => x.AddressId).ToList())
            };
        }

        /// <summary>
        /// Convert Account Model to Franchise Administrators ViewModel
        /// </summary>
        /// <param name="model">FranchiseAdministratorsViewModel model</param>
        /// <returns>AccountModel</returns>
        public static AccountModel ToViewModel(FranchiseAdministratorsViewModel model)
        {
            return new AccountModel()
            {
                Email = model.Email,
                ExternalId = model.Email,
                CompanyName = model.StoreName,

                Addresses = new Collection<AddressModel>(model.Addresses.Select(AddressViewModelMap.ToModel).ToList())
            };
        }
    }
}

