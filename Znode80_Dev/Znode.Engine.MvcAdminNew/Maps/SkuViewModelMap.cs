﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class SkuViewModelMap
    {
        /// <summary>
        /// View Model Mapper for Sku
        /// </summary>
        /// <param name="model">SkuModel model</param>
        /// <returns>return SkuViewModel</returns>
        public static SkuViewModel ToViewModel(SkuModel model)
        {
            var viewModel = new SkuViewModel()
            {
                ProductId = model.ProductId,
                ImageAltTag = model.ImageAltTag,
                IsActive = model.IsActive,
                SKU = model.Sku,
                WeightAdditional = model.WeightAdditional,
                SKUImage = model.ImageFile,
                SalePriceOverride = model.SalePriceOverride,
                RecurringBillingPeriod = model.RecurringBillingPeriod,
                RecurringBillingFrequency = model.RecurringBillingFrequency,
                RecurringBillingTotalCycles = model.RecurringBillingTotalCycles,
                RecurringBillingInitialAmount = model.RecurringBillingInitialAmount
            };
            return viewModel;
        }

        /// <summary>
        /// Model Mapper for Sku
        /// </summary>
        /// <param name="viewModel">SkuViewModel</param>
        /// <returns>returns SkuModel</returns>
        public static SkuModel ToModel(SkuViewModel viewModel)
        {
            var model = new SkuModel()
            {
                SkuId = viewModel.SKUId,
                ProductId = viewModel.ProductId,
                ImageAltTag = viewModel.ImageAltTag,
                IsActive = viewModel.IsActive,
                Sku = viewModel.SKU,
                WeightAdditional = viewModel.WeightAdditional,
                ImageFile = viewModel.SKUImage,
                SalePriceOverride = viewModel.SalePriceOverride,
                RecurringBillingPeriod = viewModel.RecurringBillingPeriod,
                RecurringBillingFrequency = viewModel.RecurringBillingFrequency,
                RecurringBillingTotalCycles = viewModel.RecurringBillingTotalCycles,
                RecurringBillingInitialAmount = viewModel.RecurringBillingInitialAmount

            };

            model.Inventory = new InventoryModel();
            model.Inventory.Sku = viewModel.SKU;
            model.Inventory.QuantityOnHand = viewModel.QuantityOnHand;
            model.Inventory.ReorderLevel = viewModel.ReorderLevel;

            return model;
        }

        /// <summary>
        /// Model Mapper for Sku 
        /// </summary>
        /// <param name="viewModel">ProductViewModel</param>
        /// <returns>returns SkuModel</returns>
        public static SkuModel ToModel(ProductViewModel viewModel)
        {
            var model = new SkuModel()
            {
                ProductId = viewModel.ProductId,
                ImageAltTag = viewModel.ImageAltTag,
                IsActive = viewModel.IsActive,
                Sku = viewModel.Sku     
            };

            model.Inventory = new InventoryModel();
            model.Inventory.Sku = viewModel.Sku;
            model.Inventory.QuantityOnHand = viewModel.QuantityOnHand;
            model.Inventory.ReorderLevel = viewModel.ReorderLevel;

            return model;
        }
    }
}