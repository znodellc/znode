﻿using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class ProductTypeAttributeViewModelMap 
    {
        #region Public Methods

        /// <summary>
        /// Maps the list of ProductTypeAttributeModel model to CategoryListViewModel.
        /// </summary>
        /// <param name="models">The list of type CatalogAssociatedCategoriesModels.</param>
        /// <returns>Returns the mapped list of CatalogAssociatedCategoriesModel model to CategoryListViewModel.</returns>
        public static ProductTypeAttributeListViewModel ToListViewModel(ProductTypeAttributeListModel models, int? pageIndex, int? recordPerPage, int? totalPages, int? totalResults)
        {
            var viewModel = new ProductTypeAttributeListViewModel()
            {
                ProductTypeAttributes = models.ProductTypeAttribute.ToList().Select(
                x => new ProductTypeAttributeViewModel()
                {
                    AttributeTypeId = x.AttributeTypeId,
                    ProductAttributeTypeID = x.AttributeTypeId,
                    ProductTypeId =x.ProductTypeId
                }).ToList()
            };
            viewModel.RecordPerPage = recordPerPage ?? 10;
            viewModel.Page = pageIndex ?? 1;
            viewModel.TotalPages = Convert.ToInt32(totalPages);
            viewModel.TotalResults = Convert.ToInt32(totalResults);
            return viewModel;
        }

        /// <summary>
        /// Converts IEnumerable ProductTypeModel to ProductTypeListViewModel.
        /// </summary>
        /// <param name="models">IEnumerable ProductTypeModel</param>
        /// <returns></returns>
        public static ProductTypeAttributeListViewModel ToListViewModel(IEnumerable<ProductTypeAttributeModel> models)
        {
            var viewModel = new ProductTypeAttributeListViewModel()
            {
                ProductTypeAttributes = models.ToList().Select(
                x => new ProductTypeAttributeViewModel()
                {
                    AttributeTypeId = x.AttributeTypeId,
                    ProductTypeId = x.ProductTypeId,
                    ProductAttributeTypeID = x.ProductAttributeTypeID
                }
                ).ToList()
            };

            return viewModel;
        }

        /// <summary>
        /// Converts ProductTypeViewModel to ProductTypeModel.
        /// </summary>
        /// <param name="viewModel">ProductTypeViewModel</param>
        /// <returns>Returns ProductTypeModel</returns>
        public static ProductTypeAssociatedAttributeTypesModel ToModel(ProductTypeAssociatedAttributeTypesViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }
            return new ProductTypeAssociatedAttributeTypesModel()
            {
                ProductTypeId = viewModel.ProductTypeId,
                Name = viewModel.Name,
                AttributeTypeId = viewModel.AttributeTypeId,
                ProductAttributeTypeID = viewModel.ProductAttributeTypeID,
                DisplayOrder = viewModel.DisplayOrder
            };
        }

        /// <summary>
        /// Maps the list of ProductTypeAssociatedAttributeTypesModel model to ProductTypeAttributeListViewModel.
        /// </summary>
        /// <param name="models">The list of type ProductTypeAssociatedAttributeTypesModels.</param>
        /// <returns>Returns the mapped list of ProductTypeAssociatedAttributeTypesModel model to ProductTypeAttributeListViewModel.</returns>
        public static ProductTypeAssociatedAttributeTypesListViewModel ToListViewModel(ProductTypeAssociatedAttributeTypesListModel models, int? pageIndex, int? recordPerPage, int? totalPages, int? totalResults)
        {
            var viewModel = new ProductTypeAssociatedAttributeTypesListViewModel()
            {
                AssociatedAttributes = models.AttributeList.ToList().Select(
                x => new ProductTypeAssociatedAttributeTypesViewModel()
                {
                    ProductAttributeTypeID = x.ProductAttributeTypeID,
                    AttributeTypeId = x.AttributeTypeId,
                    ProductTypeId = x.ProductTypeId,
                    Name = x.Name,
                    DisplayOrder = x.DisplayOrder,
                    IsPrivate = x.IsPrivate,
                    LocaleId = x.LocaleId
                }).ToList()
            };
            viewModel.RecordPerPage = recordPerPage ?? 10;
            viewModel.Page = pageIndex ?? 1;
            viewModel.TotalPages = Convert.ToInt32(totalPages);
            viewModel.TotalResults = Convert.ToInt32(totalResults);
            return viewModel;
        }
        #endregion
    }
}