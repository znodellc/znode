﻿using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class PaymentOptionViewModelMap
    {
        public static PaymentOptionViewModel ToViewModel(PaymentOptionModel model)
        {
            return new PaymentOptionViewModel()
             {
                 DisplayOrder = model.DisplayOrder,
                 EnableAmericanExpress = model.EnableAmericanExpress,
                 EnableDiscover = model.EnableDiscover,
                 EnableMasterCard = model.EnableMasterCard,
                 EnableRecurringPayments = model.EnableRecurringPayments,
                 EnableVault = model.EnableVault,
                 EnableVisa = model.EnableVisa,
                 IsActive = model.IsActive,
                 IsRmaCompatible = model.IsRmaCompatible,
                 Partner = model.Partner,
                 PaymentGateway = new PaymentGatewayViewModel { Name = model.PaymentGateway.Name, PaymentGatewayId = model.PaymentGateway.PaymentGatewayId, Url = model.PaymentGateway.Url },
                 PaymentGatewayId = model.PaymentGatewayId,
                 PaymentGatewayPassword = model.PaymentGatewayPassword,
                 PaymentGatewayUsername = model.PaymentGatewayUsername,
                 PaymentOptionId = model.PaymentOptionId,
                 PaymentType = new PaymentTypeViewModel { Name = model.PaymentType.Name, Description = model.PaymentType.Description, IsActive = model.PaymentType.IsActive, PaymentTypeId = model.PaymentType.PaymentTypeId },
                 PaymentTypeId = model.PaymentTypeId,
                 PreAuthorize = model.PreAuthorize,
                 ProfileId = model.ProfileId,
                 TestMode = model.TestMode,
                 TransactionKey = model.TransactionKey,
                 Vendor = model.Vendor
             };

        }

        public static PaymentOptionListViewModel ToListViewModel(IEnumerable<PaymentOptionModel> model)
        {

            var paymetOptions = new PaymentOptionListViewModel()
            {
                PaymentOptionList = model.ToList().Select(x =>
                    new PaymentOptionViewModel()
                    {
                        DisplayOrder = x.DisplayOrder,
                        EnableAmericanExpress = x.EnableAmericanExpress,
                        EnableDiscover = x.EnableDiscover,
                        EnableMasterCard = x.EnableMasterCard,
                        EnableRecurringPayments = x.EnableRecurringPayments,
                        EnableVault = x.EnableVault,
                        EnableVisa = x.EnableVisa,
                        IsActive = x.IsActive,
                        IsRmaCompatible = x.IsRmaCompatible,
                        Partner = x.Partner,
                        PaymentGateway = ToPaymentGatewayViewModel(x.PaymentGateway),
                        PaymentGatewayId = x.PaymentGatewayId,
                        PaymentGatewayPassword = x.PaymentGatewayPassword,
                        PaymentGatewayUsername = x.PaymentGatewayUsername,
                        PaymentOptionId = x.PaymentOptionId,
                        PaymentType = ToPaymentTypeViewModel(x.PaymentType),
                        PaymentTypeId = x.PaymentTypeId,
                        PreAuthorize = x.PreAuthorize,
                        ProfileId = x.ProfileId,
                        TestMode = x.TestMode,
                        TransactionKey = x.TransactionKey,
                        Vendor = x.Vendor
                    }).ToList()
            };

            return paymetOptions;
        }


        public static PaymentGatewayViewModel ToPaymentGatewayViewModel(PaymentGatewayModel model)
        {
            if (Equals(model, null))
                return new PaymentGatewayViewModel();
            else
            {
                return new PaymentGatewayViewModel()
                {
                    Name = model.Name,
                    PaymentGatewayId = model.PaymentGatewayId,
                    Url = model.Url
                };
            }
        }

        public static PaymentTypeViewModel ToPaymentTypeViewModel(PaymentTypeModel model)
        {
            if (Equals(model, null))
                return new PaymentTypeViewModel();
            else
            {
                return new PaymentTypeViewModel()
                {
                    Description = model.Description,
                    IsActive = model.IsActive,
                    Name = model.Name,
                    PaymentTypeId = model.PaymentTypeId
                };
            }
        }
    }
}