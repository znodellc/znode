﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;


namespace Znode.Engine.MvcAdmin.Maps
{
    public static class ManageBannerViewModelMap
    {
        /// <summary>
        /// Mapper to convert ManageMessageViewModel to MessageConfigModel   
        /// </summary>
        /// <param name="paymentOptionModel">to convert to model</param>
        /// <returns>MessageConfigModel</returns>
        public static MessageConfigModel ToModel(ManageBannerViewModel model)
        {
            if (Equals(model,null))
            {
                return null;
            }
            return new MessageConfigModel()
            {
                Key = model.Key,
                Description = model.Description,
                Value = model.Value,
                MessageConfigId = model.MessageTypeID,
                PortalId = model.PortalID,
                LocaleId = model.LocaleID,
                MessageTypeId = model.MessageTypeID,
                PageSeoName=model.PageSEOName,
            };
        }

        /// <summary>
        /// Mapper to convert MessageConfigModel to ManageMessageViewModel  
        /// </summary>
        /// <param name="paymentOptionModel">to convert to view model</param>
        /// <returns>ManageMessageViewModel</returns>
        public static ManageBannerViewModel ToViewModel(MessageConfigModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }
            return new ManageBannerViewModel()
            {
                Key = model.Key,
                Description = model.Description,
                Value = model.Value,
                MessageID = model.MessageConfigId,
                PortalID = model.PortalId,
                LocaleID = model.LocaleId,
                MessageTypeID = model.MessageTypeId,
                PageSEOName = model.PageSeoName,
            };
        }

        public static List<SelectListItem> GetSelectListItem(IEnumerable<PortalViewModel> model, int portalId)
        {
            List<SelectListItem> items = new List<SelectListItem>();
            try
            {
                if (!Equals(model, null))
                {
                    items = model.Select(x => new SelectListItem
                    {
                        Value = Convert.ToString(x.PortalId),
                        Text = x.StoreName,
                        Selected = (Equals(x.PortalId,portalId)),
                    }).ToList();

                }
            }
            catch (Exception ex)
            {
                items = new List<SelectListItem>();
            }
            return items;
        }
    }
}