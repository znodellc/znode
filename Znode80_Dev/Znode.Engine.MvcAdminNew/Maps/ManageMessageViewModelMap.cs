﻿using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for ManageMessage Mapper 
    /// </summary>
    public static class ManageMessageViewModelMap
    {
        /// <summary>
        /// Mapper to convert MessageConfigModel to ManageMessageViewModel  
        /// </summary>
        /// <param name="paymentOptionModel">to convert to view model</param>
        /// <returns>ManageMessageViewModel</returns>
        public static ManageMessageViewModel ToViewModel(MessageConfigModel messageConfigModel)
        {
            if (messageConfigModel == null)
            {
                return null;
            }
            return new ManageMessageViewModel()
            {
                MessageKey = messageConfigModel.Key,
                Description = messageConfigModel.Description,
                Value = messageConfigModel.Value,
                MessageConfigId = messageConfigModel.MessageConfigId,
                PortalId = messageConfigModel.PortalId,
                LocaleId = messageConfigModel.LocaleId,
                MessageTypeId = messageConfigModel.MessageTypeId,               
            };
        }

        /// <summary>
        /// Mapper to convert ManageMessageViewModel to MessageConfigModel   
        /// </summary>
        /// <param name="paymentOptionModel">to convert to model</param>
        /// <returns>MessageConfigModel</returns>
        public static MessageConfigModel ToModel(ManageMessageViewModel manageMessageViewModel)
        {
            if (manageMessageViewModel == null)
            {
                return null;
            }
            return new MessageConfigModel()
            {
                Key = manageMessageViewModel.MessageKey,
                Description = manageMessageViewModel.Description,
                Value = manageMessageViewModel.Value,
                MessageConfigId = manageMessageViewModel.MessageConfigId,
                PortalId = manageMessageViewModel.PortalId,
                PortalName = manageMessageViewModel.PortalName,
                LocaleId = manageMessageViewModel.LocaleId,
                MessageTypeId = manageMessageViewModel.MessageTypeId
            };
        }

        /// <summary>
        /// Mapper to convert MessageConfigModel to ManageMessageListViewModel
        /// </summary>
        /// <param name="models">to convert to list view model</param>
        /// <returns>ManageMessageListViewModel</returns>
        public static ManageMessageListViewModel ToListViewModel(IEnumerable<MessageConfigModel> models)
        {
            if (models == null)
            {
                return null;
            }
            var viewModel = new ManageMessageListViewModel()
            {
                ManageMessage = models.ToList().Select(
                x => new ManageMessageViewModel()
                {
                    MessageKey = x.Key,
                    Description = x.Description,
                    Value = x.Value,
                    MessageConfigId = x.MessageConfigId,
                    PortalId = x.PortalId,
                    PortalName = x.PortalName,
                }).ToList()
            };

            return viewModel;
        }
    }
}