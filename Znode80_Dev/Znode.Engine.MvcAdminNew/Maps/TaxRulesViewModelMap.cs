﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for TaxRules Mapper
    /// </summary>
    public static class TaxRulesViewModelMap
    {
        /// <summary>
        /// Mapper to convert TaxRuleModel to TaxRulesViewModel    
        /// </summary>
        /// <param name="taxRuleModel">Object of TaxRuleModel</param>
        /// <returns>TaxRulesViewModel</returns>
        public static TaxRulesViewModel ToViewModel(TaxRuleModel taxRuleModel)
        {
            if (Equals(taxRuleModel, null))
            {
                return null;
            }

            return new TaxRulesViewModel()
            {
                Precedence = taxRuleModel.Precedence,
                SalesTax = taxRuleModel.SalesTax,
                VATTax = taxRuleModel.Vat,
                GSTTax = taxRuleModel.Gst,
                PSTTax = taxRuleModel.Pst,
                HSTTax = taxRuleModel.Hst,
                InclusiveTax = taxRuleModel.IsInclusive
            };
        }

        /// <summary>
        /// Mapper to convert TaxRulesViewModel to TaxRuleModel  
        /// </summary>
        /// <param name="taxRulesViewModel">Object of TaxRuleViewModel</param>
        /// <returns>TaxRuleModel</returns>
        public static TaxRuleModel ToModel(TaxRulesViewModel taxRulesViewModel)
        {
            if (Equals(taxRulesViewModel, null))
            {
                return null;
            }

            return new TaxRuleModel()
            {
                Precedence = taxRulesViewModel.Precedence,
                SalesTax = taxRulesViewModel.SalesTax,
                Vat = taxRulesViewModel.VATTax,
                Gst = taxRulesViewModel.GSTTax,
                Pst = taxRulesViewModel.PSTTax,
                Hst = taxRulesViewModel.HSTTax,
                IsInclusive = taxRulesViewModel.InclusiveTax
            };
        }
    }
}