﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class CatalogViewModelMap
    {
        /// <summary>
        /// Maps the Catalog Model to CatalogViewList Model.
        /// </summary>
        /// <param name="models">The model of type CatalogModel.</param>
        /// <returns>Returns the mapped Catalog Model to CatalogViewList Model.</returns>
        public static CatalogViewModel ToViewModel(CatalogModel models)
        {
            CatalogViewModel viewModel = new CatalogViewModel();
            viewModel.CatalogId = models.CatalogId;
            viewModel.Name = models.Name;
            return viewModel;
        }

        /// <summary>
        /// Maps the CatalogViewModel to CatalogModel.
        /// </summary>
        /// <param name="viewModel">The model of type CatalogViewModel.</param>
        /// <returns>Returns the mapped CatalogViewModel to CatalogModel.</returns>
        public static CatalogModel ToModel(CatalogViewModel viewModel)
        {
            CatalogModel model = new CatalogModel();
            model.CatalogId = viewModel.CatalogId;
            model.Name = viewModel.Name;
            model.IsActive = viewModel.IsActive;
            model.PreserveCategories = viewModel.PreserveCategories;
            return model;
        }

        /// <summary>
        /// Converts collection of catalog model to catalogListview model.
        /// </summary>
        /// <param name="models">Collection of catalog models.</param>
        /// <returns>CatalogListViewModel</returns>
        public static CatalogListViewModel ToListViewModel(CatalogListModel model, int? pageIndex, int? recordPerPage)
        {
            var viewModel = new CatalogListViewModel()
            {
                Catalogs = model.Catalogs.ToList().Select(
                x => new CatalogViewModel()
                {
                    CatalogId = x.CatalogId,
                    Name = x.Name,
                    IsActive = x.IsActive                    
                }).ToList()
            };
            viewModel.RecordPerPage = recordPerPage ?? 10;
            viewModel.Page = pageIndex ?? 1;
            viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
            return viewModel;
        }
    }
}