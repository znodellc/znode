﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class AccountViewModelMap
    {
        public static AccountModel ToLoginModel(LoginViewModel model)
        {
            return new AccountModel()
                {
                    User = new UserModel()
                        {
                            Username = model.Username,
                            Password = model.Password,
                            PasswordQuestion = model.PasswordQuestion,
                        }
                };
        }

        public static LoginViewModel ToLoginViewModel(AccountModel model)
        {
            return new LoginViewModel()
            {
                    Username = model.User.Username,
                    Password = model.User.Password,
                    PasswordQuestion = model.User.PasswordQuestion,
                    IsResetPassword = model.User.IsLockedOut,
                    PasswordResetToken = model.User.PasswordToken,
            };
        }

        public static AccountViewModel ToAccountViewModel(AccountModel model)
        {
            var accountViewModel = new AccountViewModel()
                {
                    AccountId = model.AccountId,
                    EmailAddress = model.User.Email,
                    UserName = model.User.Username,
                    PasswordQuestion = model.User.PasswordQuestion,
                    PasswordAnswer = model.User.PasswordAnswer,
                    UserId = model.User.UserId,
                    CompanyName = model.CompanyName,
                    EmailOptIn = model.EmailOptIn,
                    EnableCustomerPricing = model.EnableCustomerPricing,
                    ExternalId = model.ExternalId,
                    IsActive = model.IsActive,
                    ProfileId = model.Profiles.Any() ? model.Profiles.First().ProfileId : 0,
                    //UseWholeSalePricing = model.Profiles.Any() ? model.Profiles.First().UseWholesalePricing.GetValueOrDefault(false) : false,
                    //Addresses = new Collection<AddressViewModel>(model.Addresses.Select(AddressViewModelMap.ToModel).OrderBy(x => x.AddressId).ToList()),
                    //WishList = ToWishListViewModel(model.WishList),
                    //Orders = ToOrdersViewModel(model.Orders),
                };

           
            return accountViewModel;
        }
 

        public static AccountModel ToAccountModel(ResetPasswordModel model)
        {
            var accountModel = new AccountModel()
            {
                User = new UserModel()
                {
                    Username = model.Username,
                    Password = model.Password,
                    Email = model.Email,
                    IsApproved = true,
                },
                IsActive = true,
            };

            return accountModel;
        }

        public static AccountModel ToAccountModel(AccountViewModel model)
        {
            return new AccountModel()
            {
                AccountId = model.AccountId,
                Email = model.EmailAddress,
                User = new UserModel()
                {
                    Username = model.UserName,
                    Email = model.EmailAddress,
                    UserId = model.UserId.GetValueOrDefault(),
                    PasswordQuestion = model.PasswordQuestion,
                    PasswordAnswer = model.PasswordAnswer,

                },
                CompanyName = model.CompanyName,
                EmailOptIn = model.EmailOptIn,
                EnableCustomerPricing = model.EnableCustomerPricing,
                ExternalId = model.ExternalId,
                IsActive = model.IsActive,
                ProfileId = model.ProfileId,
                UserId = model.UserId,
                //Addresses = new Collection<AddressModel>(model.Addresses.Select(AddressViewModelMap.ToModel).ToList()),
                BaseUrl = model.BaseUrl,
            };
        }

        public static AccountModel ToChangePasswordModel(ChangePasswordViewModel model)
        {
            var changePasswordModel = new AccountModel()
            {
                User = new UserModel()
                {
                    Username = model.UserName,
                    Password = model.OldPassword,
                    NewPassword = model.NewPassword,
                    PasswordToken = model.PasswordToken,
                }
            };

            return changePasswordModel;
        }
    }
}