﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for Payment Mapper
    /// </summary>
    public static class PaymentViewModelMap
    {
        /// <summary>
        /// Mapper to convert PaymentOptionModel to PaymentViewModel
        /// </summary>
        /// <param name="paymentOptionModel">Object of PaymentOptionModel</param>
        /// <returns>PaymentViewModel</returns>
        public static PaymentViewModel ToViewModel(PaymentOptionModel paymentOptionModel)
        {
            if (Equals(paymentOptionModel, null))
            {
                return null;
            }

            return new PaymentViewModel()
            {
                TransactionKey = paymentOptionModel.TransactionKey,
                DisplayOrder = paymentOptionModel.DisplayOrder,
                EnableRMA = paymentOptionModel.IsRmaCompatible,
                EnableTestModel = paymentOptionModel.TestMode,
                Visa = paymentOptionModel.EnableVisa,
                MasterCard = paymentOptionModel.EnableMasterCard,
                AmericanExpress = paymentOptionModel.EnableAmericanExpress,
                Discover = paymentOptionModel.EnableDiscover,
                PreAuthorizeTransactionsWithoutCapturing = paymentOptionModel.PreAuthorize,
                Partner = paymentOptionModel.Partner,
                Vendor = paymentOptionModel.Vendor,
                Password = paymentOptionModel.PaymentGatewayPassword,
                MerchantLogin = paymentOptionModel.PaymentGatewayUsername
            };
        }

        /// <summary>
        /// Mapper to convert PaymentViewModel to PaymentOptionModel  
        /// </summary>
        /// <param name="paymentViewModel">Object of PaymentViewModel</param>
        /// <returns>PaymentOptionModel</returns>
        public static PaymentOptionModel ToModel(PaymentViewModel paymentViewModel)
        {
            if (Equals(paymentViewModel, null))
            {
                return null;
            }

            return new PaymentOptionModel()
            {
                TransactionKey = paymentViewModel.TransactionKey,
                DisplayOrder = paymentViewModel.DisplayOrder,
                IsRmaCompatible = paymentViewModel.EnableRMA,
                TestMode = paymentViewModel.EnableTestModel,
                EnableVisa = paymentViewModel.Visa,
                EnableMasterCard = paymentViewModel.MasterCard,
                EnableAmericanExpress = paymentViewModel.AmericanExpress,
                EnableDiscover = paymentViewModel.Discover,
                PreAuthorize = paymentViewModel.PreAuthorizeTransactionsWithoutCapturing,
                Partner = paymentViewModel.Partner,
                Vendor = paymentViewModel.Vendor,
                PaymentGatewayPassword = paymentViewModel.Password,
                PaymentGatewayUsername = paymentViewModel.MerchantLogin
            };
        }
    }
}