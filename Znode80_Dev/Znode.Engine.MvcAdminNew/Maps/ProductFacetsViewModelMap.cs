﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;
namespace Znode.Engine.MvcAdmin.Maps
{
    public static class ProductFacetsViewModelMap
    {
        public static ProductFacetsViewModel ToViewModel(ProductFacetModel model)
        {
            if (!Equals(model, null))
            {
                var viewModel = new ProductFacetsViewModel()
                {
                    
                    FacetGroups=ToFacetGroup(model.FacetGroups),
                    AvailableFacets=ToFacetList(model.AssociatedFacets),
                    SelectedFacets = model.AssociatedFacetsSkus.Any() ? ToSelectedFacets(model.AssociatedFacetsSkus, model.AssociatedFacets) : null,
                     
                };
                return viewModel;
            }
            return new ProductFacetsViewModel();
        }

        public static ProductFacetModel ToModel(ProductFacetsViewModel model)
        {
            ProductFacetModel facetModel = new ProductFacetModel();
            facetModel.ProductId=model.ProductId;
            facetModel.FacetIds = (Equals(model.PostedFacets, null)) ? null : model.PostedFacets.FacetIds;
            facetModel.FacetGroupId = model.FacetGroupId;
            return facetModel;
        }

        private static List<SelectListItem> ToFacetGroup(IEnumerable<FacetGroupModel> model)
        {
            List<SelectListItem> lstItems=new List<SelectListItem>();
            lstItems = (from item in model
                        select new SelectListItem
                        {
                            Text = item.FacetGroupLabel,
                            Value = item.FacetGroupID.ToString(),
                        }).ToList();
            return lstItems;

        }

        private static List<Facets> ToFacetList(IEnumerable<FacetModel> model)
        {
            List<Facets> lstItems = new List<Facets>();
            lstItems = (from item in model
                        select new Facets
                        {
                            Id=item.FacetID,
                            Name=item.FacetName,

                        }).ToList();
            return lstItems;

        }
        private static List<Facets> ToSelectedFacets(IEnumerable<FacetProductSKUModel> model, IEnumerable<FacetModel> facetmodel)
        {
            List<Facets> lstItems = new List<Facets>();
            var data = facetmodel.Where(x => model.Any(y => x.FacetID.Equals(y.FacetID))).ToList();

            foreach (var item in data)
            {
                lstItems.Add(new Facets { Id = item.FacetID, Name = item.FacetName });
            }

            return lstItems;
            
        }
      
    }
}