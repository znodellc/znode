﻿using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// View Model Mapper for Attributes
    /// </summary>
    public class AttributesViewModelMap
    {
        /// <summary>
        /// View Model Mapper for Attribute 
        /// </summary>
        /// <param name="model"> AttributeModel </param>
        /// <returns>returns AttributesViewModel</returns>
        public static AttributesViewModel ToViewModel(AttributeModel model)
        {
            return new AttributesViewModel()
            {
                AttributeId = model.AttributeId,
                AttributeTypeId = model.AttributeTypeId,
                Name = model.Name,
                DisplayOrder = model.DisplayOrder,
                IsActive = model.IsActive
            };
        }

        /// <summary>
        /// Converts Attributes model to Attributes list view model
        /// </summary>
        /// <param name="models">IEnumerable<AttributeModel></param>
        /// <returns>returns AttributesListViewModel</returns>
        public static AttributesListViewModel ToListViewModel(IEnumerable<AttributeModel> models)
        {
            if (Equals(models,null))
            {
                return null;
            }
            var viewModel = new AttributesListViewModel()
            {
                Attributes = models.ToList().Select(
                x => new AttributesViewModel()
                {
                    AttributeId = x.AttributeId,
                    AttributeTypeId = x.AttributeTypeId,
                    Name = x.Name,
                    DisplayOrder = x.DisplayOrder,
                    IsActive = x.IsActive,
                    AttributeTypeName = x.AttributeTypeName
                }).ToList()
            };

            return viewModel;
        }

        /// <summary>
        ///  Model Mapper for Attributes
        /// </summary>
        /// <param name="viewModel">AttributesViewModel</param>
        /// <returns>returns AttributeModel</returns>
        public static AttributeModel ToModel(AttributesViewModel viewModel)
        {
            return new AttributeModel()
            {
                AttributeId = viewModel.AttributeId,
                AttributeTypeId = viewModel.AttributeTypeId,
                Name = viewModel.Name,
                DisplayOrder = viewModel.DisplayOrder.Value,
                IsActive = viewModel.IsActive
            };
        }
    }
}