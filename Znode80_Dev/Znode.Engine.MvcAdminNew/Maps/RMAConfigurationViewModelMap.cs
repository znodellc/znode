﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class RMAConfigurationViewModelMap
    {

        /// <summary>
        /// Converting RMAConfiguration View Model to RMAConfiguration Model  
        /// </summary>
        /// <param name="model"></param>
        /// <returns>RMAConfigurationModel</returns>
        public static RMAConfigurationModel ToModel(RMAConfigurationViewModel model)
        {
            return new RMAConfigurationModel()
            {
                RMAConfigId = model.RMAConfigId,
                DisplayName = model.DisplayName,
                EmailId = model.EmailId,
                EnableEmailNotification = model.EnableEmailNotification,
                ReturnMailingAddress = model.ReturnMailingAddress,
                MaxDays = model.MaxDays,
                GiftCardExprirationPeriod = model.GiftCardExprirationPeriod,
                GiftCardNotification = model.GiftCardNotification,
                ShippingInstruction = model.ShippingInstruction
            };
        }

        /// <summary>
        /// Converting RMAConfiguration Model to RMAConfiguration View Model  
        /// </summary>
        /// <param name="model"></param>
        /// <returns>RMAConfigurationViewModel</returns>
        public static RMAConfigurationViewModel ToViewModel(RMAConfigurationModel model)
        {
            return new RMAConfigurationViewModel()
            {
                RMAConfigId = model.RMAConfigId,
                DisplayName = model.DisplayName,
                EmailId = model.EmailId,
                EnableEmailNotification = model.EnableEmailNotification,
                ReturnMailingAddress = model.ReturnMailingAddress,
                MaxDays = model.MaxDays,
                GiftCardExprirationPeriod = model.GiftCardExprirationPeriod,
                GiftCardNotification = model.GiftCardNotification,
                ShippingInstruction = model.ShippingInstruction
            };
        }
    }
}