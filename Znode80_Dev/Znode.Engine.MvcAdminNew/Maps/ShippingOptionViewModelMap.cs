﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class ShippingOptionViewModelMap
    {
        /// <summary>
        /// View Model Mapper for ShippingOption
        /// </summary>
        /// <param name="model">ShippingOptionModel</param>
        /// <returns></returns>
        public static ShippingOptionViewModel ToViewModel(ShippingOptionModel model)
        {
            return new ShippingOptionViewModel()
            {
                ShippingOptionId = model.ShippingOptionId,
                ShippingCode = model.ShippingCode,
                Description=model.Description,
                ProfileId = model.ProfileId,
                CountryCode = model.CountryCode,
                HandlingCharge = model.HandlingCharge,
                DisplayOrder=model.DisplayOrder,
                IsActive = model.IsActive,
                ShippingTypeId=model.ShippingTypeId,
                ShippingType=model.ShippingType,
                //ShippingRule = model.ShippingRule              
            };
        }

        /// <summary>
        /// Converts Shipping Option model to Shipping Option list view model
        /// </summary>
        /// <param name="models">IEnumerable ShippingOptionModel</param>
        /// <returns>Returns ShippingOptionListViewModel</returns>
        public static ShippingOptionListViewModel ToListViewModel(IEnumerable<ShippingOptionModel> models)
        {
            var viewModel = new ShippingOptionListViewModel()
            {
                ShippingOptions = models.ToList().Select(
                x => new ShippingOptionViewModel()
                {
                    ShippingOptionId = x.ShippingOptionId,
                    ShippingCode = x.ShippingCode,
                    ShippingType = x.ShippingType,
                    Description = x.Description,
                    ProfileId = x.ProfileId,
                    CountryCode = x.CountryCode,
                    HandlingCharge = x.HandlingCharge,
                    DisplayOrder = x.DisplayOrder,
                    IsActive = x.IsActive
                }).ToList()
            };

            return viewModel;
        }        
    }
}