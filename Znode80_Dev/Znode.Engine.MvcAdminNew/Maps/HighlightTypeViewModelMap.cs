﻿using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for HighlightType View Model Mapper 
    /// </summary>
    public static class HighlightTypeViewModelMap
    {
        /// <summary>
        /// Convert HighlightTypeModel to HighlightTypeViewModel
        /// </summary>
        /// <param name="model">to view model</param>
        /// <returns>HighlightTypeViewModel</returns>
        public static HighlightTypeViewModel ToViewModel(HighlightTypeModel model)
        {

            if (!Equals(model, null))
            {
                return new HighlightTypeViewModel()
                {
                    HighlightTypeId = model.HighlightTypeId,
                    Name = model.Name,
                    Description = model.Description,
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert HighlightTypeViewModel to HighlightTypeModel
        /// </summary>
        /// <param name="viewModel">convert to model</param>
        /// <returns>HighlightTypeModel</returns>
        public static HighlightTypeModel ToModel(HighlightTypeViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                return new HighlightTypeModel()
                {
                    HighlightTypeId = viewModel.HighlightTypeId,
                    Name = viewModel.Name,
                    Description = viewModel.Description
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert IEnumerable<HighlightTypeModel> to HighlightTypeListViewModel
        /// </summary>
        /// <param name="models">to list view model</param>
        /// <returns>HighlightTypeListViewModel</returns>
        public static HighlightTypeListViewModel ToListViewModel(IEnumerable<HighlightTypeModel> models)
        {
            if (!Equals(models, null))
            {
                return new HighlightTypeListViewModel()
                {
                    HighlightTypes = models.ToList().Select(
                    model => new HighlightTypeViewModel()
                    {
                        Name = model.Name,
                        HighlightTypeId = model.HighlightTypeId,
                        Description = model.Description
                    }
                  ).ToList()
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert HighlightTypeListModel to List<HighlightTypeViewModel> 
        /// </summary>
        /// <param name="listModel">to list view model</param>
        /// <returns>List<HighlightTypeViewModel></returns>
        public static List<HighlightTypeViewModel> ToViewModels(HighlightTypeListModel listModel)
        {
            if (!Equals(listModel, null))
            {
                var list = new List<HighlightTypeViewModel>();
                foreach (HighlightTypeModel model in listModel.HighlightTypes)
                {
                    list.Add(ToViewModel(model));
                }
                return list;
            }
            else
            {
                return null;
            }
        }
    }
}