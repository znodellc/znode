﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for URLRedirect View Model Mapper 
    /// </summary>
    public static class RedirectViewModelMap
    {
        /// <summary>
        /// Convert URL301RedirectViewModel to UrlRedirectModel 
        /// </summary>
        /// <param name="uRLRedirectViewModel">to covert to model</param>
        /// <returns>UrlRedirectModel</returns>
        public static UrlRedirectModel ToModel(URL301RedirectViewModel uRLRedirectViewModel)
        {
            if (uRLRedirectViewModel == null)
            {
                return null;
            }

            return new UrlRedirectModel()
            {
                NewUrl = uRLRedirectViewModel.URLToRedirctTo,
                OldUrl = uRLRedirectViewModel.URLToRedirctFrom,
                IsActive = uRLRedirectViewModel.EnableThisRedirection
            };
        }

        /// <summary>
        /// Convert UrlRedirectModel to URL301RedirectViewModel  
        /// </summary>
        /// <param name="urlRedirectModel">to covert to view model</param>
        /// <returns>URL301RedirectViewModel</returns>
        public static URL301RedirectViewModel ToViewModel(UrlRedirectModel urlRedirectModel)
        {
            if (urlRedirectModel == null)
            {
                return null;
            }

            return new URL301RedirectViewModel()
            {
                URLToRedirctTo = urlRedirectModel.NewUrl,
                URLToRedirctFrom = urlRedirectModel.OldUrl,
                EnableThisRedirection = urlRedirectModel.IsActive
            };
        } 

    }
}