﻿using System;
using System.Collections.Generic;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class ManufacturersViewModelMap
    {
        /// <summary>
        /// View Model Mapper for Manufacturer
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ManufacturersViewModel ToViewModel(ManufacturerModel model)
        {
            return new ManufacturersViewModel()
            {
                ManufacturerId = model.ManufacturerId,
                Description = model.Description,
                Email = model.Email,
                IsActive = model.IsActive,
                Name = model.Name,

            };
        }

        /// <summary>
        /// Converts Manufacturer model to Manufacturer list view model
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public static ManufacturersListViewModel ToListViewModel(ManufacturerListModel models, int? pageIndex, int? recordPerPage)
        {
            if (!Equals(models, null) && !Equals(models.Manufacturers, null))
            {
                var viewModel = new ManufacturersListViewModel()
                {
                    Manufacturers = models.Manufacturers.ToList().Select(
                    x => new ManufacturersViewModel()
                    {
                        ManufacturerId = x.ManufacturerId,
                        Name = x.Name,
                        Description = x.Description,
                        Email = x.Email,
                        IsActive = x.IsActive
                    }).ToList()
                };

                viewModel.RecordPerPage = recordPerPage ?? 10;
                viewModel.Page = pageIndex ?? 1;
                viewModel.TotalResults = Convert.ToInt32(models.TotalResults);

                return viewModel;
            }
            else
            {
                return new ManufacturersListViewModel();
            }
        }

        /// <summary>
        /// Model Mapper for Manufacturer
        /// </summary>
        /// <param name="model">ManufacturersViewModel model</param>
        /// <returns>returns the ManufacturerModel</returns>
        public static ManufacturerModel ToModel(ManufacturersViewModel model)
        {
            return new ManufacturerModel()
            {
                Custom1 = string.Empty,
                Custom2 = string.Empty,
                Custom3 = string.Empty,
                Description = model.Description,
                DisplayOrder = 0,
                Email = model.Email,
                EmailNotificationTemplate = string.Empty,
                IsActive = model.IsActive,
                IsDropShipper = true,
                ManufacturerId = model.ManufacturerId,
                Name = model.Name,
                PortalId = null,
                Website = string.Empty
            };
        }

    }
}