﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class ReasonReturnViewModelMap
    {
        /// <summary>
        /// Converting ReasonReturn View Model to ReasonReturn Model   
        /// </summary>
        /// <param name="model"></param>
        /// <returns>ReasonReturnModel</returns>
        public static ReasonReturnModel ToModel(ReasonReturnViewModel model)
        {
            if (model == null)
            {
                return null;
            }

            return new ReasonReturnModel()
            {
                ReasonId = model.ReasonId,
                Name = model.Name,
                IsEnabled = model.IsEnabled
            };
        }

        /// <summary>
        /// Converting ReasonReturn Model to ReasonReturn View Model   
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns>ReasonReturnViewModel</returns>
        public static ReasonReturnViewModel ToViewModel(ReasonReturnModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            return new ReasonReturnViewModel()
            {
                ReasonId = viewModel.ReasonId,
                Name = viewModel.Name,
                IsEnabled = viewModel.IsEnabled
            };
        }
    }
}