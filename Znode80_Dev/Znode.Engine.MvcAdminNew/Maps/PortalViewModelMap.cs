﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    /// <summary>
    /// Maps portal models to portal view model which is used to show on views.
    /// </summary>
    public static class PortalViewModelMap
    {
        /// <summary>
        /// Converts portal model to portal view model.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static PortalViewModel ToViewModel(PortalModel model)
        {
            if (model == null)
            {
                return null;
            }

            return new PortalViewModel()
                {
                    CompanyName = model.CompanyName,
                    PortalId = model.PortalId,
                    StoreName = model.StoreName,
                    PersistentCartEnabled = model.PersistentCartEnabled.GetValueOrDefault(false),
                    DefaultAnonymousProfileId = model.DefaultAnonymousProfileId.GetValueOrDefault(0),
                    MaxRecentViewItemToDisplay = model.MaxCatalogCategoryDisplayThumbnails,
                    LogoPathImageName = model.LogoPath,
                    UseSsl = model.UseSsl,
                    AdminEmail = model.AdminEmail,
                    SalesEmail = model.SalesEmail,
                    CustomerServiceEmail = model.CustomerServiceEmail,
                    CustomerServicePhoneNumber = model.CustomerServicePhoneNumber,
                    SalesPhoneNumber = model.SalesPhoneNumber,
                    DefaultReviewStatus = model.DefaultReviewStatus,
                    DefaultOrderStateId = model.DefaultOrderStateId,
                    EnableAddressValidation = model.EnableAddressValidation,
                    RequireValidatedAddress = model.RequireValidatedAddress,
                    EnableCustomerPricing = model.EnableCustomerPricing,
                    DefaultProductReviewStateId = model.DefaultProductReviewStateId,
                    DimensionUnit = model.DimensionUnit,
                    FedExAccountNumber = model.FedExAccountNumber,
                    FedExAddInsurance = model.FedExAddInsurance,
                    FedExClientProductId = model.FedExClientProductId,
                    FedExClientProductVersion = model.FedExClientProductVersion,
                    FedExCspKey = model.FedExCspKey,
                    FedExCspPassword = model.FedExCspPassword,
                    FedExDropoffType = model.FedExDropoffType,
                    FedExMeterNumber = model.FedExMeterNumber,
                    FedExPackagingType = model.FedExPackagingType,
                    FedExProductionKey = model.FedExProductionKey,
                    FedExSecurityCode = model.FedExSecurityCode,
                    FedExUseDiscountRate = model.FedExUseDiscountRate,
                    GoogleAnalyticsCode = model.GoogleAnalyticsCode,
                    ImageNotAvailablePath = model.ImageNotAvailablePath,
                    InclusiveTax = model.InclusiveTax,
                    IsActive = model.IsActive,
                    IsShippingTaxable = model.IsShippingTaxable,
                    LocaleId = model.LocaleId,
                    MasterPage = model.MasterPage,
                    MaxCatalogDisplayColumns = model.MaxCatalogDisplayColumns,
                    MaxCatalogDisplayItems = model.MaxCatalogDisplayItems,
                    MaxCatalogItemCrossSellWidth = model.MaxCatalogItemCrossSellWidth,
                    MaxCatalogItemLargeWidth = model.MaxCatalogItemLargeWidth,
                    MaxCatalogItemMediumWidth = model.MaxCatalogItemMediumWidth,
                    MaxCatalogItemSmallThumbnailWidth = model.MaxCatalogItemSmallThumbnailWidth,
                    MaxCatalogItemSmallWidth = model.MaxCatalogItemSmallWidth,
                    MaxCatalogItemThumbnailWidth = model.MaxCatalogItemThumbnailWidth,
                    MobileTheme = model.MobileTheme,
                    OrderReceiptAffiliateJavascript = model.OrderReceiptAffiliateJavascript,
                    SeoDefaultCategoryDescription = model.SeoDefaultCategoryDescription,
                    SeoDefaultCategoryKeyword = model.SeoDefaultCategoryKeyword,
                    SeoDefaultCategoryTitle = model.SeoDefaultCategoryTitle,
                    SeoDefaultContentDescription = model.SeoDefaultContentDescription,
                    SeoDefaultContentKeyword = model.SeoDefaultContentKeyword,
                    SeoDefaultContentTitle = model.SeoDefaultContentTitle,
                    SeoDefaultProductDescription = model.SeoDefaultProductDescription,
                    SeoDefaultProductKeyword = model.SeoDefaultProductKeyword,
                    SeoDefaultProductTitle = model.SeoDefaultProductTitle,
                    ShippingOriginAddress1 = model.ShippingOriginAddress1,
                    ShippingOriginAddress2 = model.ShippingOriginAddress2,
                    ShippingOriginCity = model.ShippingOriginCity,
                    ShippingOriginCountryCode = model.ShippingOriginCountryCode,
                    ShippingOriginPhoneNumber = model.ShippingOriginPhoneNumber,
                    ShippingOriginStateCode = model.ShippingOriginStateCode,
                    ShippingOriginZipCode = model.ShippingOriginZipCode,
                    ShopByPriceIncrement = model.ShopByPriceIncrement,
                    ShopByPriceMax = model.ShopByPriceMax,
                    ShopByPriceMin = model.ShopByPriceMin,
                    SiteWideTopJavascript = model.SiteWideTopJavascript,
                    SmtpPassword = model.SmtpPassword,
                    SmtpPort = model.SmtpPort,
                    SmtpServer = model.SmtpServer,
                    SmtpUsername = model.SmtpUsername,
                    SplashCategoryId = model.SplashCategoryId,
                    SplashImageFile = model.SplashImageFile,
                    TimeZoneOffset = model.TimeZoneOffset,
                    UpsKey = model.UpsKey,
                    UpsPassword = model.UpsPassword,
                    UpsUsername = model.UpsUsername,
                    UseDynamicDisplayOrder = model.UseDynamicDisplayOrder,
                    WeightUnit = model.WeightUnit
                    
                };
        }

        /// <summary>
        /// Converts list of portal model to portallist view model.
        /// </summary>
        /// <param name="models">IEnumerable<PortalModel></param>
        /// <returns>PortalListViewModel</returns>
        public static PortalListViewModel ToListViewModel(PortalListModel model, int? pageIndex, int? recordPerPage)
        {
            var viewModel = new PortalListViewModel();

            if (model.Portals != null)
            {
                viewModel.Portals = model.Portals.Select(
                x => new PortalViewModel()
                {
                    PortalId = x.PortalId,
                    CompanyName = x.CompanyName,
                    StoreName = x.StoreName
                }).ToList();
            }


            foreach (var portal in viewModel.Portals)
            {
                if (portal.PortalId.Equals(PortalAgent.CurrentPortal.PortalId))
                {
                    portal.IsCurrentPortal = true;

                }
            }

            viewModel.RecordPerPage = recordPerPage ?? 10;
            viewModel.Page = pageIndex ?? 1;
            viewModel.TotalResults = Convert.ToInt32(model.TotalResults);
            return viewModel;
        }

        public static PortalModel ToModel(PortalViewModel portalViewModel)
        {
            var portalModel = new PortalModel()
            {
                CompanyName=portalViewModel.CompanyName,
                PortalId = portalViewModel.PortalId,
                StoreName = portalViewModel.StoreName,
                PersistentCartEnabled = portalViewModel.PersistentCartEnabled,
                DefaultAnonymousProfileId = portalViewModel.DefaultAnonymousProfileId,
                MaxCatalogCategoryDisplayThumbnails = portalViewModel.MaxCatalogCategoryDisplayThumbnails,
                LogoPath = portalViewModel.LogoPathImageName,
                UseSsl = portalViewModel.UseSsl,
                AdminEmail = portalViewModel.AdminEmail,
                SalesEmail = portalViewModel.SalesEmail,
                CustomerServiceEmail = portalViewModel.CustomerServiceEmail,
                CustomerServicePhoneNumber = portalViewModel.CustomerServicePhoneNumber,
                SalesPhoneNumber = portalViewModel.SalesPhoneNumber,
                DefaultReviewStatus = portalViewModel.DefaultReviewStatus,
                DefaultOrderStateId = portalViewModel.DefaultOrderStateId,
                EnableAddressValidation = portalViewModel.EnableAddressValidation,
                RequireValidatedAddress = portalViewModel.RequireValidatedAddress,
                EnableCustomerPricing = portalViewModel.EnableCustomerPricing,
                DefaultProductReviewStateId = portalViewModel.DefaultProductReviewStateId,
                DimensionUnit = portalViewModel.DimensionUnit,
                FedExAccountNumber = portalViewModel.FedExAccountNumber,
                FedExAddInsurance = portalViewModel.FedExAddInsurance,
                FedExClientProductId = portalViewModel.FedExClientProductId,
                FedExClientProductVersion = portalViewModel.FedExClientProductVersion,
                FedExCspKey = portalViewModel.FedExCspKey,
                FedExCspPassword = portalViewModel.FedExCspPassword,
                FedExDropoffType = portalViewModel.FedExDropoffType,
                FedExMeterNumber = portalViewModel.FedExMeterNumber,
                FedExPackagingType = portalViewModel.FedExPackagingType,
                FedExProductionKey = portalViewModel.FedExProductionKey,
                FedExSecurityCode = portalViewModel.FedExSecurityCode,
                FedExUseDiscountRate = portalViewModel.FedExUseDiscountRate,
                GoogleAnalyticsCode = portalViewModel.GoogleAnalyticsCode,
                ImageNotAvailablePath = portalViewModel.ImageNotAvailablePath,
                InclusiveTax = portalViewModel.InclusiveTax,
                IsActive = portalViewModel.IsActive,
                IsShippingTaxable = portalViewModel.IsShippingTaxable,
                LocaleId = portalViewModel.LocaleId,
                MasterPage = portalViewModel.MasterPage,
                MaxCatalogDisplayColumns = portalViewModel.MaxCatalogDisplayColumns,
                MaxCatalogDisplayItems = portalViewModel.MaxCatalogDisplayItems,
                MaxCatalogItemCrossSellWidth = portalViewModel.MaxCatalogItemCrossSellWidth,
                MaxCatalogItemLargeWidth = portalViewModel.MaxCatalogItemLargeWidth,
                MaxCatalogItemMediumWidth = portalViewModel.MaxCatalogItemMediumWidth,
                MaxCatalogItemSmallThumbnailWidth = portalViewModel.MaxCatalogItemSmallThumbnailWidth,
                MaxCatalogItemSmallWidth = portalViewModel.MaxCatalogItemSmallWidth,
                MaxCatalogItemThumbnailWidth = portalViewModel.MaxCatalogItemThumbnailWidth,
                MobileTheme = portalViewModel.MobileTheme,
                OrderReceiptAffiliateJavascript = portalViewModel.OrderReceiptAffiliateJavascript,
                SeoDefaultCategoryDescription = portalViewModel.SeoDefaultCategoryDescription,
                SeoDefaultCategoryKeyword = portalViewModel.SeoDefaultCategoryKeyword,
                SeoDefaultCategoryTitle = portalViewModel.SeoDefaultCategoryTitle,
                SeoDefaultContentDescription = portalViewModel.SeoDefaultContentDescription,
                SeoDefaultContentKeyword = portalViewModel.SeoDefaultContentKeyword,
                SeoDefaultContentTitle = portalViewModel.SeoDefaultContentTitle,
                SeoDefaultProductDescription = portalViewModel.SeoDefaultProductDescription,
                SeoDefaultProductKeyword = portalViewModel.SeoDefaultProductKeyword,
                SeoDefaultProductTitle = portalViewModel.SeoDefaultProductTitle,
                ShippingOriginAddress1 = portalViewModel.ShippingOriginAddress1,
                ShippingOriginAddress2 = portalViewModel.ShippingOriginAddress2,
                ShippingOriginCity = portalViewModel.ShippingOriginCity,
                ShippingOriginCountryCode = portalViewModel.ShippingOriginCountryCode,
                ShippingOriginPhoneNumber = portalViewModel.ShippingOriginPhoneNumber,
                ShippingOriginStateCode = portalViewModel.ShippingOriginStateCode,
                ShippingOriginZipCode = portalViewModel.ShippingOriginZipCode,
                ShopByPriceIncrement = portalViewModel.ShopByPriceIncrement,
                ShopByPriceMax = portalViewModel.ShopByPriceMax,
                ShopByPriceMin = portalViewModel.ShopByPriceMin,
                SiteWideTopJavascript = portalViewModel.SiteWideTopJavascript,
                SmtpPassword = portalViewModel.SmtpPassword,
                SmtpPort = portalViewModel.SmtpPort,
                SmtpServer = portalViewModel.SmtpServer,
                SmtpUsername = portalViewModel.SmtpUsername,
                SplashCategoryId = portalViewModel.SplashCategoryId,
                SplashImageFile = portalViewModel.SplashImageFile,
                TimeZoneOffset = portalViewModel.TimeZoneOffset,
                UpsKey = portalViewModel.UpsKey,
                UpsPassword = portalViewModel.UpsPassword,
                UpsUsername = portalViewModel.UpsUsername,
                UseDynamicDisplayOrder = portalViewModel.UseDynamicDisplayOrder,
                WeightUnit = portalViewModel.WeightUnit
            };
            return portalModel;
        }

        /// <summary>
        /// Creates a SelectListItem List for catalogs
        /// </summary>
        /// <param name="model">Catalog Models list</param>
        /// <returns>SelectListItem List for catalogs.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<CatalogModel> model)
        {
            List<SelectListItem> catalogItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                catalogItems = (from item in model
                                select new SelectListItem
                                {
                                    Text = item.Name,
                                    Value = item.CatalogId.ToString(),
                                }).ToList();
            }
            return catalogItems;
        }

        /// <summary>
        /// Creates a SelectListItem List for Themes
        /// </summary>
        /// <param name="model">Themes Models list</param>
        /// <returns>SelectListItem List for themes.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<ThemeModel> model)
        {
            List<SelectListItem> themeItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                themeItems = (from item in model
                              select new SelectListItem
                              {
                                  Text = item.Name,
                                  Value = item.ThemeID.ToString(),
                              }).ToList();
            }

            foreach (SelectListItem item in themeItems)
            {
                if (item.Value.Equals("1"))
                {
                    item.Selected = true;
                }
            }
            return themeItems;
        }

        /// <summary>
        /// Creates a SelectListItem List for CSS
        /// </summary>
        /// <param name="model">CSS Models list</param>
        /// <returns>SelectListItem List for CSS.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<CSSModel> model)
        {
            List<SelectListItem> cssItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                cssItems = (from item in model
                            select new SelectListItem
                            {
                                Text = item.Name,
                                Value = item.CSSID.ToString(),
                            }).ToList();
            }
            return cssItems;
        }

        /// <summary>
        /// Creates a SelectListItem List for OrderStatus
        /// </summary>
        /// <param name="model">OrderStatus Models list</param>
        /// <returns>SelectListItem List for OrderStatus.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<OrderStateModel> model)
        {
            List<SelectListItem> orderStateItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                orderStateItems = (from item in model
                                   select new SelectListItem
                                   {
                                       Text = item.OrderStateName,
                                       Value = item.OrderStateID.ToString(),
                                   }).ToList();
            }
            return orderStateItems;
        }
        /// <summary>
        /// Creates a SelectListItem List for ProductReviewState
        /// </summary>
        /// <param name="model">ProductReviewState Models list</param>
        /// <returns>SelectListItem List for ProductReviewState.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<ProductReviewStateModel> model)
        {
            List<SelectListItem> productReviewStateItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                productReviewStateItems = (from item in model
                                           select new SelectListItem
                                           {
                                               Text = item.ReviewStateName,
                                               Value = item.ReviewStateID.ToString(),
                                           }).ToList();
            }
            return productReviewStateItems;
        }

        /// <summary>
        /// Creates a SelectListItem List for Locale
        /// </summary>
        /// <param name="model">Locale Models list</param>
        /// <returns>SelectListItem List for Locale.</returns>
        public static List<SelectListItem> ToSelectListItems(IEnumerable<LocaleModel> model)
        {
            List<SelectListItem> localeItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                localeItems = (from item in model
                               select new SelectListItem
                               {
                                   Text = item.LocaleCode,
                                   Value = item.LocaleID.ToString(),
                               }).ToList();
            }

            foreach (SelectListItem item in localeItems)
            {
                if (item.Text.Equals("English"))
                {
                    item.Selected = true;
                }
            }

            return localeItems;
        }
    }
  
}