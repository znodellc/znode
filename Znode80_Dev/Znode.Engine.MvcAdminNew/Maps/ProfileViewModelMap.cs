﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class ProfileViewModelMap
    {
        /// <summary>
        /// Converting Profile View Model to Profile Model   
        /// </summary>
        /// <param name="model"></param>
        /// <returns>ProfileModel</returns>
        public static ProfileModel ToModel(ProfileViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            return new ProfileModel()
            {
                ProfileId = viewModel.ProfileId,
                Name = viewModel.Name,
                Weighting = viewModel.Weighting,
                ShowPricing = viewModel.ProductPrice,
                UseWholesalePricing = viewModel.WholeSalePrice,
                TaxExempt = viewModel.TaxExempt,
                ShowOnPartnerSignup = viewModel.Affiliate
            };
        }

        /// <summary>
        /// Converting Profile Model to Profile View Model  
        /// </summary>
        /// <param name="model"></param>
        /// <returns>ProfileViewModel</returns>
        public static ProfileViewModel ToViewModel(ProfileModel model)
        {
            if (model == null)
            {
                return null;
            }

            return new ProfileViewModel()
            {
                ProfileId = model.ProfileId,
                Name = model.Name,
                Weighting = model.Weighting,
                ProductPrice = model.ShowPricing,
                WholeSalePrice = model.UseWholesalePricing,
                TaxExempt = model.TaxExempt,
                Affiliate = model.ShowOnPartnerSignup
            };
        }
    }
}