﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{

    /// <summary>
    /// Static class for MessageConfigViewModelMap
    /// </summary>
    public static class MessageConfigViewModelMap
    {
        #region Message Config View Model Map
        /// <summary>
        /// Convert MessageConfigModel to MessageConfigViewModel
        /// </summary>
        /// <param name="messageConfigModel">to convert to view model</param>
        /// <returns>MessageConfigViewModel</returns>
        public static MessageConfigViewModel ToMessageConfigViewModel(MessageConfigModel messageConfigModel)
        {
            return new MessageConfigViewModel()
            {
                MessageConfigId = messageConfigModel.MessageConfigId,
                Key = messageConfigModel.Key,
                Value = messageConfigModel.Value
            };
        }

        /// <summary>
        /// Convert MessageConfigViewModel to MessageConfigModel
        /// </summary>
        /// <param name="messageConfigViewModel">to convert to model</param>
        /// <returns>MessageConfigModel</returns>
        public static MessageConfigModel ToMessageConfigModel(MessageConfigViewModel messageConfigViewModel)
        {
            return new MessageConfigModel()
            {
                MessageConfigId = messageConfigViewModel.MessageConfigId,
                Key = messageConfigViewModel.Key,
                Value = messageConfigViewModel.Value
            };
        }
        #endregion

    }
}