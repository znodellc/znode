﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class AttributeTypesViewModelMap
    {
        /// <summary>
        /// View Model Mapper for Attribute Types
        /// </summary>
        /// <param name="model">AttributeTypeModel</param>
        /// <returns>returns AttributeTypesViewModel</returns>
        public static AttributeTypesViewModel ToViewModel(AttributeTypeModel model)
        {
            return new AttributeTypesViewModel()
            {
                AttributeTypeId = model.AttributeTypeId,
                Name = model.Name,
                DisplayOrder = model.DisplayOrder,
                Description = model.Description,
                LocaleId = model.LocaleId
            };
        }

        /// <summary>
        /// Converts AttributeTypes model to AttributeTypes list view model
        /// </summary>
        /// <param name="models">IEnumerable<AttributeTypeModel></param>
        /// <returns>returns AttributeTypesListViewModel</returns>
        public static AttributeTypesListViewModel ToListViewModel(IEnumerable<AttributeTypeModel> models)
        {
            var viewModel = new AttributeTypesListViewModel()
            {
                AttributeType = models.ToList().Select(
                x => new AttributeTypesViewModel()
                {
                    AttributeTypeId = x.AttributeTypeId,
                    Name = x.Name,
                    DisplayOrder = x.DisplayOrder,
                    Description = x.Description,
                    LocaleId = x.LocaleId
                }).ToList()
            };

            return viewModel;
        }

        /// <summary>
        /// Model Mapper for Attribute Types
        /// </summary>
        /// <param name="viewModel">AttributeTypesViewModel</param>
        /// <returns>returns AttributeTypeModel</returns>
        public static AttributeTypeModel ToModel(AttributeTypesViewModel viewModel)
        {
            return new AttributeTypeModel()
            {
                AttributeTypeId = viewModel.AttributeTypeId,
                Name = viewModel.Name,
                DisplayOrder = viewModel.DisplayOrder,
                Description = viewModel.Description,
                LocaleId = viewModel.LocaleId,
            };
        }

        /// <summary>
        /// Model Mapper for Attributes list in dropdown.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<AttributeTypeModel> model)
        {
            List<SelectListItem> attributeItems = new List<SelectListItem>();

            if (!Equals(model, null))
            {
                attributeItems = (from item in model
                                  select new SelectListItem
                                  {
                                      Text = item.Name,
                                      Value = item.AttributeTypeId.ToString(),
                                  }).ToList();
            }
            return attributeItems;
        }
    }
}