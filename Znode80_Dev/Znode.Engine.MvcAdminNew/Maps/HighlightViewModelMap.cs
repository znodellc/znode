﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for Highlight View Model Mapper 
    /// </summary>
    public static class HighlightViewModelMap
    {
        /// <summary>
        /// Convert HighlightModel to HighlightViewModel
        /// </summary>
        /// <param name="model">to covert to view model</param>
        /// <returns>HighlightViewModel</returns>
        public static HighlightViewModel ToViewModel(HighlightModel model)
        {
            if (!Equals(model, null))
            {
                return new HighlightViewModel()
                {
                    ImageAltTag = model.ImageAltTag,
                    ImageFile = model.ImageFile,
                    ImageLargePath = HelperMethods.GetImagePath(model.ImageLargePath),
                    ImageMediumPath = HelperMethods.GetImagePath(model.ImageMediumPath),
                    ImageSmallThumbnailPath = HelperMethods.GetImagePath(model.ImageSmallThumbnailPath),
                    ShowPopup = model.ShowPopup,
                    HyperlinkNewWindow = model.HyperlinkNewWindow,
                    HighlightId = model.HighlightId,
                    HighlightTypeId = model.HighlightTypeId,
                    IsActive = (model.IsActive.HasValue) ? (bool)model.IsActive : false,
                    Hyperlink = model.Hyperlink,
                    Name = model.Name,
                    DisplayOrder = model.DisplayOrder,
                    Description = model.Description,
                    HighlightType = HighlightTypeViewModelMap.ToViewModel(model.HighlightType),
                    LocaleId = model.LocaleId,
                    IsAssociatedProduct = model.IsAssociatedProduct
                };
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert HighlightViewModel to HighlightModel
        /// </summary>
        /// <param name="viewModel">to covert to model</param>
        /// <returns>HighlightModel</returns>
        public static HighlightModel ToModel(HighlightViewModel viewModel)
        {
            if (!Equals(viewModel, null))
            {
                return new HighlightModel()
                {
                    HighlightTypeId = viewModel.TypeId,
                    ImageAltTag = viewModel.ImageAltTag,
                    ImageFile = viewModel.ImageFile,
                    ImageLargePath = HelperMethods.GetImagePath(viewModel.ImageLargePath),
                    ImageMediumPath = HelperMethods.GetImagePath(viewModel.ImageMediumPath),
                    ImageSmallThumbnailPath = HelperMethods.GetImagePath(viewModel.ImageSmallThumbnailPath),
                    ShowPopup = viewModel.ShowPopup,
                    HyperlinkNewWindow = viewModel.HyperlinkNewWindow,
                    HighlightId = viewModel.HighlightId,
                    IsActive = viewModel.IsActive,
                    HighlightType = HighlightTypeViewModelMap.ToModel(viewModel.HighlightType),
                    LocaleId = viewModel.LocaleId,
                    Description = viewModel.Description,
                    Hyperlink = viewModel.Hyperlink,
                    Name = viewModel.Name,
                    DisplayOrder = viewModel.DisplayOrder,
                    IsAssociatedProduct = viewModel.IsAssociatedProduct
                };
            }
            else
            { 
                return null; 
            }
        }

        /// <summary>
        /// Convert IEnumerable<HighlightModel> to HighlightsListViewModel
        /// </summary>
        /// <param name="models">to convert to list view model</param>
        /// <returns>HighlightsListViewModel</returns>
        public static HighlightsListViewModel ToListViewModel(IEnumerable<HighlightModel> models)
        {
            if(!Equals(models,null))
            {
                var viewModel = new HighlightsListViewModel()
                {
                    Highlights = models.ToList().Select(
                    model => new HighlightViewModel()
                    {
                        TypeId=model.HighlightTypeId,
                        HighlightType = (!Equals(model.HighlightType,null)) ? HighlightTypeViewModelMap.ToViewModel(model.HighlightType) :new HighlightTypeViewModel(),
                        ImageAltTag = model.ImageAltTag,
                        ImageFile = model.ImageFile,
                        ImageLargePath = HelperMethods.GetImagePath(model.ImageLargePath),
                        ImageMediumPath = HelperMethods.GetImagePath(model.ImageMediumPath),
                        ImageSmallThumbnailPath = HelperMethods.GetImagePath(model.ImageSmallThumbnailPath),
                        HighlightTypeId = model.HighlightTypeId,
                        ShowPopup = model.ShowPopup,
                        HyperlinkNewWindow = model.HyperlinkNewWindow,
                        HighlightId = model.HighlightId,
                        IsActive = (model.IsActive.HasValue) ? (bool)model.IsActive : false,
                        Hyperlink=model.Hyperlink,
                        Name = model.Name,
                        DisplayOrder = model.DisplayOrder,
                        Description=model.Description,
                        LocaleId=model.LocaleId,
                        IsAssociatedProduct = model.IsAssociatedProduct
                    }).ToList()
                };
                return viewModel;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Convert IEnumerable<HighlightTypeModel> to List<SelectListItem>
        /// </summary>
        /// <param name="model">to convert to list item type</param>
        /// <returns>List<SelectListItem></returns>
        public static List<SelectListItem> ToListItems(IEnumerable<HighlightTypeModel> model)
        {
            List<SelectListItem> highlightTypeItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                highlightTypeItems = (from item in model
                                    select new SelectListItem
                                    {
                                        Text = item.Name,
                                        Value = item.HighlightTypeId.ToString(),
                                    }).ToList();
            }
            return highlightTypeItems;
        }
    }
}