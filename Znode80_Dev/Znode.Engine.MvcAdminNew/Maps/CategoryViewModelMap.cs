﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class CategoryViewModelMap
    {
        #region Public Methods

        /// <summary>
        /// Maps the list of CatalogAssociatedCategoriesModel model to CategoryListViewModel.
        /// </summary>
        /// <param name="models">The list of type CatalogAssociatedCategoriesModels.</param>
        /// <returns>Returns the mapped list of CatalogAssociatedCategoriesModel model to CategoryListViewModel.</returns>
        public static CatalogAssociatedCategoriesListViewModel ToListViewModel(CatalogAssociatedCategoriesListModel models, int? pageIndex, int? recordPerPage, int? totalPages, int? totalResults)
        {
            var viewModel = new CatalogAssociatedCategoriesListViewModel()
            {
                AssociatedCategories = models.CategoryList.ToList().Select(
                x => new CatalogAssociatedCategoriesViewModel()
                {
                    CategoryId = !Equals(x.CategoryId, null) ? x.CategoryId : 0,
                    CatalogId = !Equals(x.CatalogId, null) ? x.CatalogId : 0,
                    CategoryNodeId = !Equals(x.CategoryNodeId, null) ? x.CategoryNodeId : 0,
                    Name = !Equals(x.Name, null)? x.Name:string.Empty,
                    ParentCategoryNodeId = !Equals(x.ParentCategoryNodeId, null)? x.ParentCategoryNodeId:0,
                    SeoUrl = !Equals(x.SeoUrl, null)?x.SeoUrl:string.Empty,
                    ActiveInd = x.ActiveInd,
                    DisplayOrder = !Equals(x.DisplayOrder, null)?x.DisplayOrder:99,
                    CatalogName=!Equals(x.CatalogName,null)?x.CatalogName:string.Empty
                }).ToList()
            };
            viewModel.RecordPerPage = recordPerPage ?? 10;
            viewModel.Page = pageIndex ?? 1;
            viewModel.TotalPages = Convert.ToInt32(totalPages);
            viewModel.TotalResults = Convert.ToInt32(totalResults);
            return viewModel;
        }

        /// <summary>
        /// Creates a SelectListItem List for Categories
        /// </summary>
        /// <param name="model">List of CatalogAssociatedCategoriesModel.</param>
        /// <returns>SelectListItem List for categories.</returns>
        public static List<SelectListItem> ToParentCategorySelectListItems(IEnumerable<CatalogAssociatedCategoriesModel> model)
        {
            List<SelectListItem> categories = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                categories = (from item in model
                                select new SelectListItem
                                {
                                    Text = item.Name,
                                    Value = item.CategoryId.ToString(),
                                }).ToList();
            }
            return categories;
        }

        /// <summary>
        /// Creates list of selectListItem type for theme.
        /// </summary>
        /// <param name="model">List of ThemeModel.</param>
        /// <returns>SelectListItem list for theme. </returns>
        public static List<SelectListItem> ToThemeSelectListItems(IEnumerable<ThemeModel> model)
        {
            List<SelectListItem> themeList = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                themeList = (from item in model
                              select new SelectListItem
                              {
                                  Text = item.Name,
                                  Value = item.ThemeID.ToString(),
                              }).ToList();
            }
            return themeList;
        }

        /// <summary>
        /// Creates list of selectListItem type for css.
        /// </summary>
        /// <param name="model">List of CSSModel.</param>
        /// <returns>SelectListItem list for css. </returns>
        public static List<SelectListItem> ToCssSelectListItems(IEnumerable<CSSModel> model)
        {
            List<SelectListItem> cssList = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                cssList = (from item in model
                             select new SelectListItem
                             {
                                 Text = item.Name,
                                 Value = item.CSSID.ToString(),
                             }).ToList();
            }
            return cssList;
        }

        /// <summary>
        /// Creates list of selectListItem type for master page.
        /// </summary>
        /// <param name="model">List of MasterPageModel.</param>
        /// <returns>SelectListItem list for master pages.</returns>
        public static List<SelectListItem> ToMasterPageSelectListItems(IEnumerable<MasterPageModel> model)
        {
            List<SelectListItem> masterPageList = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                masterPageList = (from item in model
                           select new SelectListItem
                           {
                               Text = item.Name,
                               Value = item.MasterPageId.ToString(),
                           }).ToList();
            }
            return masterPageList;
        }

        /// <summary>
        /// Maps CategoryNodeViewModel type of object to CategoryNodeModel type.
        /// </summary>
        /// <param name="viewModel">The model of type CategoryNodeViewModel.</param>
        /// <returns>The mapped model of type CategoryNodeModel.</returns>
        public static CategoryNodeModel ToModel(CategoryNodeViewModel viewModel)
        {
            CategoryNodeModel model = new CategoryNodeModel();
            model.CatalogId = viewModel.CatalogId;
            model.CategoryId = viewModel.CategoryId;
            model.ParentCategoryNodeId = viewModel.ParentCategoryNodeId;
            model.DisplayOrder = viewModel.DisplayOrder;
            model.ThemeId = viewModel.ThemeId;
            model.MasterPageId = viewModel.MasterPageId;
            model.CssId = viewModel.CssId;
            model.IsActive = viewModel.IsActive;           
            return model;
        }

        #endregion
    }
}