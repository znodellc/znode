﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static Class for Gift Card Mapper
    /// </summary>
    public static class GiftCardViewModelMap
    {
        /// <summary>
        /// Convert GiftCardViewModel to GiftCardModel
        /// </summary>
        /// <param name="giftCardViewModel">GiftCardViewModel giftCardViewModel</param>
        /// <returns>GiftCardModel</returns>
        public static GiftCardModel ToModel(GiftCardViewModel giftCardViewModel)
        {
            if (giftCardViewModel == null)
            {
                return null;
            }

            return new GiftCardModel()
            {
                Name = giftCardViewModel.Name,
                Amount = giftCardViewModel.Amount,
                CreateDate = giftCardViewModel.CreateDate,
                ExpirationDate = giftCardViewModel.ExpirationDate,
                PortalId = giftCardViewModel.PortalId,
                AccountId = giftCardViewModel.AccountId,
                GiftCardId = giftCardViewModel.GiftCardId,
                CreatedBy = giftCardViewModel.CreatedBy,
                CardNumber = giftCardViewModel.CardNumber
            };
        }

        /// <summary>
        /// Convert GiftCardModel to GiftCardViewModel
        /// </summary>
        /// <param name="giftCardModel">GiftCardModel giftCardModel</param>
        /// <returns>GiftCardViewModel</returns>
        public static GiftCardViewModel ToViewModel(GiftCardModel giftCardModel)
        {

            if (giftCardModel == null)
            {
                return null;
            }

            return new GiftCardViewModel()
            {
                Name = giftCardModel.Name,
                Amount = giftCardModel.Amount,
                CreateDate = giftCardModel.CreateDate,
                ExpirationDate = giftCardModel.ExpirationDate,
                PortalId = giftCardModel.PortalId,
                AccountId = giftCardModel.AccountId,
                GiftCardId = giftCardModel.GiftCardId,
                CreatedBy = giftCardModel.CreatedBy,
                CardNumber = giftCardModel.CardNumber
            };
        }

        /// <summary>
        /// Converts GiftCardModel to GiftCardListViewModel
        /// </summary>
        /// <param name="models">IEnumerable</param>
        /// <returns>GiftCardListViewModel</returns>
        public static GiftCardListViewModel ToListViewModel(IEnumerable<GiftCardModel> models)
        {
            if (Equals(models, null))
            {
                return null;
            }

            return new GiftCardListViewModel()
            {
                GiftCards = models.ToList().Select(
                x => new GiftCardViewModel()
                {
                    Name = x.Name,
                    Amount = x.Amount,
                    GiftCardId = x.GiftCardId,
                    CardNumber = x.CardNumber,
                    CreateDate = x.CreateDate,
                    ExpirationDate = x.ExpirationDate
                }).ToList()
            };
        }

        /// <summary>
        /// This method returns the List of Portals
        /// </summary>
        /// <param name="model">IEnumerable PortalModel model</param>
        /// <returns>List of Portals</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<PortalModel> model)
        {
            List<SelectListItem> portalItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                portalItems = (from item in model
                               select new SelectListItem
                               {
                                   Text = item.StoreName,
                                   Value = item.PortalId.ToString()
                               }).ToList();
            }
            return portalItems;
        }

    }
}
