﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class CategoryListViewModelMap
    {
        public static CategoryListViewModel ToViewModel(IEnumerable<CategoryModel> models)
        {
            var viewModel = new CategoryListViewModel()
            {
                Categories = models.ToList().Select(x =>
                        new CategoryViewModel()
                        {
                            CategoryId = x.CategoryId,
                            CategoryName = x.Name,
                        }).ToList()
            };

            return viewModel;
        }
    }
}