﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Helpers;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class ProductTagsViewModelMap
    {
        public static ProductTagsModel ToModel(ProductTagsViewModel model)
        {
            var viewModel = new ProductTagsModel()
            {
                ProductId = model.ProductId,
                ProductTags = model.ProductTags,
                TagId = model.TagId,
            };
            return viewModel;
        }
        public static ProductTagsViewModel ToViewModel(ProductTagsModel model)
        {
            if (!Equals(model, null))
            {
                var viewModel = new ProductTagsViewModel()
                {
                    ProductId = model.ProductId,
                    ProductTags = model.ProductTags,
                    TagId = model.TagId,
                };
                return viewModel;
            }
            return new ProductTagsViewModel();
        }
    }
}