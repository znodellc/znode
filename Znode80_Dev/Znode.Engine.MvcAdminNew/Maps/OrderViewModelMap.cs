﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Mapper model for Order Section
    /// </summary>
    public static class OrderViewModelMap
    {
        /// <summary>
        /// Converting Order View Model to Order Model  
        /// </summary>
        /// <param name="viewModel"></param>
        /// <returns>OrderModel</returns>
        public static OrderModel ToOrderModel(OrderViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            return new OrderModel()
            {
                OrderId = viewModel.OrderId,
                OrderDate = viewModel.OrderDate,                
            };
        }

        /// <summary>
        /// Converting Order Model to Order View Model  
        /// </summary>
        /// <param name="model"></param>
        /// <returns>OrderViewModel</returns>
        public static OrderViewModel ToOrderViewModel(OrderModel model)
        {
            if (model == null)
            {
                return null;
            }

            return new OrderViewModel()
            {
                OrderId = model.OrderId,
                OrderDate = model.OrderDate,
            };
        }        
    }
}