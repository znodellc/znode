﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;
using Znode.Engine.MvcDemo.Maps;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class PromotionViewModelMap
    {
        /// <summary>
        /// Converting Promotion model to Promotion View Model
        /// </summary>
        /// <param name="models">The model of promotion </param>
        /// <returns>PromotionsViewModel</returns>
        public static PromotionsViewModel ToViewModel(PromotionModel model)
        {
            if (model == null)
            {
                return null;
            }

            return new PromotionsViewModel()
            {
                PromotionId = model.PromotionId,
                Name = model.Name,
               
                Discount = model.Discount,                
                DisplayOrder = model.DisplayOrder,
                StartDate = model.StartDate,
                EndDate = model.EndDate,
                CouponCode = model.CouponCode
            };
        }

        /// <summary>
        /// Converting Promotion View model to Promotion Model
        /// </summary>
        /// <param name="models">The view model of promotion</param>
        /// <returns>PromotionModel</returns>
        public static PromotionModel ToModel(PromotionsViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            return new PromotionModel()
            {
                PromotionId = viewModel.PromotionId,
                Name = viewModel.Name,
                Description = viewModel.Description,
                Discount = viewModel.Discount, 
                DisplayOrder = viewModel.DisplayOrder,
                StartDate = viewModel.StartDate,
                EndDate = viewModel.EndDate,
                PromoCode = viewModel.PromoCode,
                PromotionTypeId = viewModel.PromotionTypeId,
                PortalId = viewModel.PortalId,
                CatalogId = viewModel.CatalogId,
                CategoryId = viewModel.CategoryId,
                ProfileId = viewModel.ProfileId,
                SkuId = viewModel.SkuId,
                DiscountedProductQuantity = viewModel.DiscountedProductQuantity,
                RequiredProductMinimumQuantity = viewModel.RequiredProductMinimumQuantity,
                ManufacturerId = viewModel.ManufacturerId,
                DiscountedProductId = viewModel.DiscountedProductId,
                RequiredProductId = viewModel.RequiredProductId,
                MinimumOrderAmount = viewModel.OrderMinimum,               
                HasCoupon = viewModel.HasCoupon,
                CouponCode = viewModel.CouponCode,
                PromotionMessage = viewModel.PromotionMessage,
                CouponQuantityAvailable = viewModel.CouponQuantityAvailable
            };
        }

        /// <summary>
        /// This method returns the portals List
        /// </summary>
        /// <param name="model">The parameter is Portal model</param>
        /// <returns>list of all protals</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<PortalModel> model)
        {
            List<SelectListItem> portalItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                portalItems = (from item in model
                                 select new SelectListItem
                                 {
                                     Text = item.StoreName,
                                     Value = item.PortalId.ToString()
                                 }).ToList();
            }
            return portalItems;
        }

        /// <summary>
        /// This method returns the promotion types List
        /// </summary>
        /// <param name="model">The parameter is Promotion Type Model</param>
        /// <returns>list of all promotion types</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<PromotionTypeModel> model)
        {
            List<SelectListItem> promotionTypeItem = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                promotionTypeItem = (from item in model
                               select new SelectListItem
                               {
                                   Text = item.Name,
                                   Value = item.PromotionTypeId.ToString()
                               }).ToList();
            }
            return promotionTypeItem;
        }

        /// <summary>
        /// This method returns the manufacturers List
        /// </summary>
        /// <param name="model">The parameter is Manufacturer Model</param>
        /// <returns>list of all manufacturers</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ManufacturerModel> model)
        {
            List<SelectListItem> manufacturerItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                manufacturerItems = (from item in model
                               select new SelectListItem
                               {
                                   Text = item.Name,
                                   Value = item.ManufacturerId.ToString()
                               }).ToList();
            }
            return manufacturerItems;
        }

        /// <summary>
        /// This method returns the catalogs List
        /// </summary>
        /// <param name="model">The parameter is Catalog Model</param>
        /// <returns>list of all catalogs</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<CatalogModel> model, bool isIncludeDefaultItem)
        {
            List<SelectListItem> catalogItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                catalogItems = (from item in model
                                select new SelectListItem
                                {
                                    Text = item.Name,
                                    Value = item.CatalogId.ToString(),
                                }).ToList();
                if (isIncludeDefaultItem)
                {
                    catalogItems.Insert(0, new SelectListItem { Text = "All", Value = "0" });
                }
            }
            return catalogItems;
        }

        /// <summary>
        /// This method returns the category List
        /// </summary>
        /// <param name="model">The parameter is Catalog Model</param>
        /// <returns>list of all categories</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<CategoryModel> model)
        {
            List<SelectListItem> categoryItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                categoryItems = (from item in model
                                 select new SelectListItem
                                 {
                                     Text = item.Name,
                                     Value = item.CategoryId.ToString(),
                                 }).ToList();
            }
            return categoryItems;
        }

        /// <summary>
        /// This method returns the profile List
        /// </summary>
        /// <param name="model">The parameter is Profile Model</param>
        /// <returns>list of all profiles</returns>
        public static List<SelectListItem> ToListItems(IEnumerable<ProfileModel> model)
        {
            List<SelectListItem> profileItems = new List<SelectListItem>();
            if (!Equals(model, null))
            {
                profileItems = (from item in model
                                 select new SelectListItem
                                 {
                                     Text = item.Name,
                                     Value = item.ProfileId.ToString(),
                                 }).ToList();
            }
            return profileItems;
        }        


        /// <summary>
        /// This method returns the Promotion List Model
        /// </summary>
        /// <param name="models">IEnumerable type list of Promotion Model</param>
        /// <returns>List of type Promotion</returns>
        public static PromotionsListViewModel ToListViewModel(IEnumerable<PromotionModel> listModels)
        {

            if (listModels == null)
            {
                return null;
            }

            var viewModel = new PromotionsListViewModel()
            {
                Promotions = listModels.ToList().Select(
                x => new PromotionsViewModel()
                {
                    PromotionId = x.PromotionId,
                    Name = x.Name,
                    Portal = (Equals(x.Portal, null)) ? new PortalViewModel() : PortalViewModelMap.ToViewModel(x.Portal),
                    Profile = (Equals(x.Profile, null)) ? new ProfileViewModel() : ProfileViewModelMap.ToViewModel(x.Profile),
                    PromotionType = (Equals(x.PromotionType, null)) ? new PromotionTypeViewModel() : PromotionTypeViewModelMap.ToViewModel(x.PromotionType),
                    Discount = x.Discount, 
                    DisplayOrder = x.DisplayOrder,
                    StartDate = x.StartDate,
                    EndDate = x.EndDate,
                    CouponCode = x.CouponCode
                }).ToList()
            };
            return viewModel;            
        }

        /// <summary>
        /// This method returns the Product List Model
        /// </summary>
        /// <param name="models">IEnumerable type list of Product Model</param>
        /// <returns>List of Products</returns>
        public static ProductListViewModel ToListViewModel(IEnumerable<ProductModel> listModels)
        {

            if (listModels == null)
            {
                return null;
            }

            var viewModel = new ProductListViewModel()
            {
                Products = listModels.ToList().Select(
                x => new ProductViewModel()
                {
                    ProductId = x.ProductId,
                    Name = x.Name,
                    Sku =x.ProductNumber,                    
                    ImageSmallThumbnailPath = x.ImageSmallThumbnailPath,
                    ProductCode = x.ProductNumber,
                }).ToList()
            };
            return viewModel;
        }        
    }
}