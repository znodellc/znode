﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class PromotionTypeViewModelMap
    {
        /// <summary>
        /// Converting Promotion type model to Promotion type view model
        /// </summary>
        /// <param name="models">The model of promotion type</param>
        /// <returns>PromotionTypeViewModel</returns>
        public static PromotionTypeViewModel ToViewModel(PromotionTypeModel model)
        {
            if (model == null)
            {
                return null;
            }

            return new PromotionTypeViewModel()
            {
                ClassName = model.ClassName,
                ClassType = model.ClassType,
                Description = model.Description,
                IsActive = model.IsActive,
                Name = model.Name,
                PromotionTypeId = model.PromotionTypeId,
            };
        }

        /// <summary>
        /// Converting Promotion type view model to Promotion type model
        /// </summary>
        /// <param name="models">The view model of promotion type</param>
        /// <returns>PromotionTypeViewModel</returns>
        public static PromotionTypeModel ToModel(PromotionTypeViewModel viewModel)
        {
            if (viewModel == null)
            {
                return null;
            }

            return new PromotionTypeModel()
            {
                ClassName = viewModel.ClassName,
                ClassType = viewModel.ClassType,
                Description = viewModel.Description,
                IsActive = viewModel.IsActive,
                Name = viewModel.Name,
                PromotionTypeId = viewModel.PromotionTypeId,
            };
        }

        /// <summary>
        /// Converting Promotion type view model to Promotion type list model
        /// </summary>
        /// <param name="models">The model of promotion type</param>
        /// <returns>PromotionTypeViewModel</returns>
        public static List<PromotionTypeViewModel> ToViewModel(PromotionTypeListModel listModel)
        {
            if (listModel == null)
            {
                return null;
            }

            var list = new List<PromotionTypeViewModel>();

            foreach (PromotionTypeModel model in listModel.PromotionTypes)
            {
                list.Add(ToViewModel(model));
            }
            return list;
        }
    }
}