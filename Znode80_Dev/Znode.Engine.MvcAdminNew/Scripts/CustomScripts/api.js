﻿/*
 | Ajax API Module
 | All ajax calls should be in this module
 |
 | 2015; Znode, Inc.
*/
App.Api = (function ($, Modernizr, App) {
    var ajax = {
        settings: {
            errorAsAlert: false // True will show error as alert dialog
        },

        // Common ajax request; includes cache buster for IE
        request: function (url, method, parameters, successCallback, responseType) {
            if (!method) { method = "GET"; }
            if (!responseType) { responseType = "json"; }

            // Requests must have callback method
            if (typeof successCallback != "function") {
                ajax.errorOut("Callback is not defined. No request made.");
            } else {
                $.ajax({
                    type: method,
                    url: url,
                    data: ajax.cachestamp(parameters),
                    dataType: responseType,
                    success: function (response) {
                        _debug(response); // Always send response to console after request
                        successCallback(response);
                    },
                    error: function () {
                        ajax.errorOut("API Endpoint not available: " + url);
                    }
                });
            }
        },

        // Timestamp for cache busting
        cachestamp: function (data) {
            var d = new Date();

            if (typeof data == "string") {
                data += "&_=" + d.getTime();
            } else if (typeof data == "object") {
                data["_"] = d.getTime();
            } else {
                data = { "_": d.getTime() };
            }


            return (data);
        },

        // Error output
        errorOut: function (message) {
            console.log(message);

            if (this.settings.errorAsAlert) {
                alert(message);
            }
        }
    }

    // App API endpoints
    // Add methods here as needed for additional API calls
    // All endpoint methods are public
    var endpoints = {
        // get list of State bt CountryCode .
        getManufacturerListByKeyword: function (keyword, callbackMethod) {
            ajax.request("/Manufacturers/ManufacturerSearchList", "post", { "keyword": keyword }, callbackMethod, "html");
        },
        //Get list of Categories based on Catalog Id
        getUnAssignedCategoriesListByCatalogId: function (catalogId, callbackMethod) {
            ajax.request("/Facet/AssignedCategoriesList", "post", { "catalogId": catalogId }, callbackMethod, "html");
        },
        deleteRecordById: function (url, callbackMethod) {
            ajax.request(url, "post", {}, callbackMethod);
        },
        // Get product attributes and its values collection by ProductTypeId; Returns JSON
        getProductAttributesByProductTypeId: function (productTypeId, callbackMethod) {
            ajax.request("/Product/GetProductAttributesByProductTypeId", "get", { "productTypeId": productTypeId }, callbackMethod, "json");
        },

        GetPortalCssListByThemeId: function (cssThemeId, callbackMethod) {
            ajax.request("/Stores/BindCssList", "get", { "CssThemeId": cssThemeId }, callbackMethod, "json");
        },
        getTabDetails: function (url, queryParam, callbackMethod) {
            ajax.request(url, "get", queryParam, callbackMethod,"html");
        },
               
        getCategoryCssListByThemeId: function (cssThemeId, callbackMethod) {
            ajax.request("/Catalog/BindCssList", "get", { "CssThemeId": cssThemeId }, callbackMethod,"json");
        },
        getCategoryMasterPageListByThemeId: function (cssThemeId, callbackMethod) {
            ajax.request("/Catalog/BindMasterPageList", "get", { "CssThemeId": cssThemeId }, callbackMethod, "json");
        },

        BindFacetListByFacetGroupId: function (productId,facetGroupId, callbackMethod) {
            ajax.request("/Product/GetFacetsByFacetGroupId", "get", { "productId": productId, "facetGroupId": facetGroupId }, callbackMethod, "html");
        },
    }

    return (endpoints);

}(jQuery, Modernizr, App));

// END module