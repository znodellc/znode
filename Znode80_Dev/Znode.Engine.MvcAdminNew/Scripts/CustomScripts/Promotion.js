﻿var $j = jQuery.noConflict();

Promotion = {
    ShowHideCouponInfo: function () {             
        if ($j('#EnableToCouponCode').prop("checked") == true) {
            $j('#coupon-code').show();
            $j('#coupon-message').show();
        }
        else {
            $j('#coupon-code').hide();
            $j('#coupon-message').hide();
        }
    },

    ShowProductListPopUp: function (id) {
        $j(id).dialog({
            resizable: false,
            height: 500,
            width: 1000,
            modal: true
        });
    },

    GetPromotionProductName: function (prodId) {
        var promotion = {
            PromotionName: $j("#Name").val(),
            Description: $j("#Description").val(),
            StartDate: $j("#StartDate").val(),
            EndDate: $j("#EndDate").val(),
            PromoCode: $j("#PromoCode").val(),
            DisplayOrder: $j("#DisplayOrder").val(),
            PromotionType: $j("#ddlPromotionType option:selected").text(),
            Portal: $j("#ddlPortal option:selected").text(),
            Discount: $j("#txtDiscount").val(),
            DiscountProduct: $j("#lnkProduct_" + prodId).attr("name"),
            RequireProduct: $j("#lnkProduct_" + prodId).attr("name"),
            Manufacturer: $j("#ddlManufacturer option:selected").text(),
            SKU: $j("#lnkProduct_" + prodId).attr("sku"),
            Catalog: $j("#ddlCatalog option:selected").text(),
            Category: $j("#ddlCategory option:selected").text(),
            QuantityMinimum: $j("#QuantityMinimum").val(),
            OrderMinimum: $j("#OrderMinimum").val(),
            CouponCode: $j("#CouponCode").val(),
            PromotionMessage: $j("#PromotionMessage").val(),
            CouponQuantityAvailable: $j("#CouponQuantityAvailable").val(),
            DiscountedProductId: prodId,
            RequiredProductId: prodId
        };

        $j.ajax({
            type: 'POST',
            contentType: "application/json",
            url: '/Promotion/GetProduct',
            data: JSON.stringify(promotion),
            success: function (data) {
                $j("#DiscountProduct").val(data.DiscountProduct);
                $j("#hdnDiscountedProductId").val(data.DiscountedProductId);
                $j("#RequireProduct").val(data.RequireProduct);
                $j("#SKU").val(data.SKU);
                $j("#hdnRequireProductId").val(data.RequiredProductId);
                $j("#hdnSkuId").val(data.SkuId);

                $j("#show-product").dialog("close");
            },
            error: function () {
                alert("error")
            }
        });
    },

    GetPromotionType: function ()
    {
        var promotionTypeId = $j('#ddlPromotionType').val()

        switch (promotionTypeId) {
            case "1":
                {
                    $j("#discount-amount").show(),
                    $j("#discount-product").hide(),
                    $j("#product-sku").hide(),
                    $j("#require-product").hide(),
                    $j("#manufacturer-list").hide(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").hide(),
                    $j("#btnProduct").hide(),
                    $j("#order-minimum").show(),
                    $j("#nxtbtnProduct").hide(),
                    $j("#quantity-minimum").hide(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#lblChkBox").show()
                    break;
                }
            case "2":
                {
                    $j("#discount-amount").show(),
                    $j("#require-product").show(),
                    $j("#nxtbtnProduct").show(),
                    $j("#quantity-minimum").show(),
                    $j("#order-minimum").show(),
                    $j("#lblChkBox").show(),
                    $j("#btnProduct").hide(),
                    $j("#product-sku").hide(),
                    $j("#discount-product").hide(),
                    $j("#manufacturer-list").hide(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").hide()
                    break;
                }
            case "3":
                {
                    $j("#discount-amount").show(),
                    $j("#discount-product").hide(),
                    $j("#require-product").hide(),
                    $j("#product-sku").hide(),
                    $j("#manufacturer-list").hide(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").hide(),
                    $j("#btnProduct").hide(),
                    $j("#order-minimum").show(),
                    $j("#nxtbtnProduct").hide(),
                    $j("#quantity-minimum").hide(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#lblChkBox").show()
                    break;
                }
            case "4":
                {
                    $j("#discount-amount").show(),
                    $j("#discount-product").show(),
                    $j("#require-product").show(),
                    $j("#lblChkBox").show(),
                    $j("#quantity-minimum").show(),
                    $j("#btnProduct").show(),
                    $j("#nxtbtnProduct").show(),
                    $j("#manufacturer-list").hide(),
                    $j("#product-sku").hide(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").hide(),
                    $j("#order-minimum").hide(),
                    $j("#discounted-product-quantity").hide()
                    break;
                }
            case "5":
                {
                    $j("#discount-amount").show(),
                    $j("#discount-product").hide(),
                    $j("#require-product").hide(),
                    $j("#manufacturer-list").hide(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").hide(),
                    $j("#btnProduct").hide(),
                    $j("#order-minimum").show(),
                    $j("#nxtbtnProduct").hide(),
                    $j("#quantity-minimum").hide(),
                    $j("#product-sku").hide(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#lblChkBox").show()
                    break;
                }
            case "6":
                {
                    $j("#discount-amount").show(),
                    $j("#discount-product").hide(),
                    $j("#require-product").show(),
                    $j("#manufacturer-list").hide(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").hide(),
                    $j("#btnProduct").hide(),
                    $j("#order-minimum").show(),
                    $j("#nxtbtnProduct").show(),
                    $j("#quantity-minimum").show(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#product-sku").hide(),
                    $j("#lblChkBox").show()
                    break;
                }
            case "7":
                {
                    $j("#discount-amount").show(),
                    $j("#discount-product").hide(),
                    $j("#require-product").hide(),
                    $j("#manufacturer-list").hide(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").hide(),
                    $j("#btnProduct").hide(),
                    $j("#order-minimum").show(),
                    $j("#nxtbtnProduct").hide(),
                    $j("#quantity-minimum").hide(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#product-sku").hide(),
                    $j("#lblChkBox").show()
                    break;
                }
            case "8":
                {
                    $j("#discount-amount").show(),
                    $j("#discount-product").show(),
                    $j("#require-product").show(),
                    $j("#manufacturer-list").hide(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").hide(),
                    $j("#btnProduct").show(),
                    $j("#order-minimum").hide(),
                    $j("#nxtbtnProduct").show(),
                    $j("#quantity-minimum").show(),
                    $j("#product-sku").hide(),
                    $j("#discounted-product-quantity").show(),
                    $j("#lblChkBox").show()
                    break;
                }
            case "9":
                {
                    $j("#discount-amount").show(),
                    $j("#discount-product").hide(),
                    $j("#require-product").show(),
                    $j("#manufacturer-list").hide(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").hide(),
                    $j("#btnProduct").hide(),
                    $j("#order-minimum").hide(),
                    $j("#nxtbtnProduct").hide(),
                    $j("#quantity-minimum").hide(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#product-sku").hide(),
                    $j("#lblChkBox").hide()
                    break;
                }
            case "10":
                {
                    $j("#discount-amount").show(),
                    $j("#discount-product").hide(),
                    $j("#require-product").show(),
                    $j("#manufacturer-list").hide(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").hide(),
                    $j("#btnProduct").hide(),
                    $j("#order-minimum").hide(),
                    $j("#nxtbtnProduct").hide(),
                    $j("#quantity-minimum").hide(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#product-sku").hide(),
                    $j("#lblChkBox").hide()
                    break;
                }
            case "11":
                {
                    $j("#discount-amount").hide(),
                    $j("#discount-product").hide(),
                    $j("#require-product").hide(),
                    $j("#manufacturer-list").hide(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").hide(),
                    $j("#btnProduct").hide(),
                    $j("#order-minimum").hide(),
                    $j("#nxtbtnProduct").hide(),
                    $j("#quantity-minimum").hide(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#product-sku").hide(),
                    $j("#lblChkBox").hide(),
                    $j("#quantity-minimum").hide()
                    break;
                }
            case "12":
                {
                    break;
                }
            case "13":
                {
                    $j("#discount-amount").hide(),
                    $j("#discount-product").show(),
                    $j("#require-product").hide(),
                    $j("#manufacturer-list").hide(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").hide(),
                    $j("#btnProduct").show(),
                    $j("#order-minimum").hide(),
                    $j("#nxtbtnProduct").hide(),
                    $j("#quantity-minimum").hide(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#product-sku").show(),
                    $j("#lblChkBox").hide(),
                    $j("#coupon-message").show()
                    break;
                }
            case "14":
                {
                    $j("#discount-amount").show(),
                    $j("#discount-product").hide(),
                    $j("#require-product").hide(),
                    $j("#manufacturer-list").hide(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").show(),
                    $j("#btnProduct").hide(),
                    $j("#order-minimum").show(),
                    $j("#nxtbtnProduct").hide(),
                    $j("#quantity-minimum").show(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#product-sku").hide(),
                    $j("#lblChkBox").show()
                    break;
                }
            case "15":
                {
                    $j("#discount-amount").show(),
                    $j("#discount-product").hide(),
                    $j("#require-product").hide(),
                    $j("#manufacturer-list").hide(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").show(),
                    $j("#btnProduct").hide(),
                    $j("#order-minimum").show(),
                    $j("#nxtbtnProduct").hide(),
                    $j("#quantity-minimum").show(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#lblChkBox").show()
                    break;
                }
            case "16":
                {
                    $j("#discount-amount").show(),
                    $j("#discount-product").hide(),
                    $j("#require-product").hide(),
                    $j("#manufacturer-list").show(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").hide(),
                    $j("#btnProduct").hide(),
                    $j("#order-minimum").show(),
                    $j("#nxtbtnProduct").hide(),
                    $j("#quantity-minimum").show(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#product-sku").hide(),
                    $j("#lblChkBox").show()
                    break;
                }
            case "17":
                {
                    $j("#discount-amount").show(),
                    $j("#discount-product").hide(),
                    $j("#require-product").hide(),
                    $j("#manufacturer-list").show(),
                    $j("#catalog-list").hide(),
                    $j("#category-list").hide(),
                    $j("#btnProduct").hide(),
                    $j("#order-minimum").show(),
                    $j("#nxtbtnProduct").hide(),
                    $j("#quantity-minimum").show(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#product-sku").hide(),
                    $j("#lblChkBox").show()
                    break;
                }
            case "18":
                {
                    $j("#discount-amount").show(),
                    $j("#discount-product").hide(),
                    $j("#require-product").hide(),
                    $j("#manufacturer-list").hide(),
                    $j("#catalog-list").show(),
                    $j("#category-list").hide(),
                    $j("#btnProduct").hide(),
                    $j("#order-minimum").show(),
                    $j("#nxtbtnProduct").hide(),
                    $j("#quantity-minimum").show(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#product-sku").hide(),
                    $j("#lblChkBox").show()
                    break;
                }
            case "19":
                {
                    $j("#discount-amount").show(),
                    $j("#discount-product").hide(),
                    $j("#require-product").hide(),
                    $j("#manufacturer-list").hide(),
                    $j("#catalog-list").show(),
                    $j("#category-list").hide(),
                    $j("#btnProduct").hide(),
                    $j("#order-minimum").show(),
                    $j("#nxtbtnProduct").hide(),
                    $j("#quantity-minimum").show(),
                    $j("#discounted-product-quantity").hide(),
                    $j("#product-sku").hide(),
                    $j("#lblChkBox").show()
                    break;
                }
            default:
                    $j("#discount-amount").show(),
                    $j("#discount-product").show(),
                    $j("#require-product").show(),
                    $j("#manufacturer-list").show(),
                    $j("#catalog-list").show(),
                    $j("#category-list").show(),
                    $j("#btnProduct").show(),
                    $j("#order-minimum").show(),
                    $j("#nxtbtnProduct").show(),
                    $j("#quantity-minimum").show(),
                    $j("#discounted-product-quantity").show(),
                    $j("#product-sku").show(),
                    $j("#lblChkBox").show()
                break;
        }
    },

    ValidateAttributes: function () {
        var promotionTypeId = $j('#ddlPromotionType').val()
        if (promotionTypeId == "") {
            $j("#valDiscount").text('').removeClass("field-validation-error");
            $j("#valDiscountProduct").text('').removeClass("field-validation-error");
            $j("#valRequiredProduct").text('').removeClass("field-validation-error");
            $j("#valOrderMinimum").text('').removeClass("field-validation-error");
            $j("#valQuantityMinimum").text('').removeClass("field-validation-error");
            var discount = $j("#Discount").val();
            var discountProduct = $j("#DiscountProduct").val();
            var requireProduct = $j("#RequireProduct").val();
            var orderMinimum = $j("#OrderMinimum").val();
            var quantityMinimum = $j("#QuantityMinimum").val();
            if (discount.length < 1 || discountProduct.length < 1 || requireProduct.length < 1 || orderMinimum.length < 1 || quantityMinimum.length < 1) {
                $j("#valDiscount").text("Discount is requird.").addClass("field-validation-error");
                $j("#valRequiredProduct").text("Product is requird.").addClass("field-validation-error");
                $j("#valDiscountProduct").text("Product is required.").addClass("field-validation-error");
                $j("#valQuantityMinimum").text("Enter Minimum Quantity").addClass("field-validation-error");
                $j("#valCouponCode").text("Enter Coupon Code").addClass("field-validation-error");
                return false;
            }
            return true;
        }
        switch (promotionTypeId) {                    
            case "1":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valOrderMinimum").text('').removeClass("field-validation-error");
                    $j("#valCouponCode").text('').removeClass("field-validation-error");
                    var discount = $j("#Discount").val();
                    var orderMinimum = $j("#OrderMinimum").val();
                    var couponCode = $j("#CouponCode").val();
                    if (discount.length < 1 || orderMinimum.length < 1 || couponCode.length < 1) {
                        $j("#valDiscount").text("Discount is requird.").addClass("field-validation-error");
                        $j("#valOrderMinimum").text("Enter Minimum Order.").addClass("field-validation-error");
                        $j("#valCouponCode").text("Enter Coupon Code").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "2":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valRequiredProduct").text('').removeClass("field-validation-error");
                    $j("#valOrderMinimum").text('').removeClass("field-validation-error");
                    $j("#valQuantityMinimum").text('').removeClass("field-validation-error");
                    $j("#valCouponCode").text('').removeClass("field-validation-error");
                    var discount = $j("#DiscountProduct").val();
                    var requireProduct = $j("#RequireProduct").val();
                    var orderMinimum = $j("#OrderMinimum").val();
                    var quantityMinimum = $j("#QuantityMinimum").val();
                    var couponCode = $j("#CouponCode").val();
                    if (discount.length < 1 || requireProduct.length < 1 || orderMinimum.length < 1 || quantityMinimum.length < 1 || couponCode.length < 1) {
                        $j("#valDiscount").text("Discount Product is requird.").addClass("field-validation-error");
                        $j("#valRequiredProduct").text("Product is requird.").addClass("field-validation-error");
                        $j("#valOrderMinimum").text("Enter Minimum Order").addClass("field-validation-error");
                        $j("#valQuantityMinimum").text("Enter Minimum Quantity").addClass("field-validation-error");
                        $j("#valCouponCode").text("Enter Coupon Code").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "3":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valOrderMinimum").text('').removeClass("field-validation-error");
                    $j("#valCouponCode").text('').removeClass("field-validation-error");
                    var discount = $j("#Discount").val();
                    var orderMinimum = $j("#OrderMinimum").val();
                    var couponCode = $j("#CouponCode").val();
                    if (discount.length < 1 || orderMinimum.length < 1 || couponCode.length < 1) {
                        $j("#valDiscount").text("Discount is requird.").addClass("field-validation-error");
                        $j("#valOrderMinimum").text("Enter Minimum Order.").addClass("field-validation-error");
                        $j("#valCouponCode").text("Enter Coupon Code").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "4":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valDiscountProduct").text('').removeClass("field-validation-error");
                    $j("#valRequiredProduct").text('').removeClass("field-validation-error");
                    $j("#valQuantityMinimum").text('').removeClass("field-validation-error");
                    $j("#valCouponCode").text('').removeClass("field-validation-error");
                    var discount = $j("#Discount").val();
                    var discountProduct = $j("#DiscountProduct").val();
                    var requireProduct = $j("#RequireProduct").val();
                    var quantityMinimum = $j("#QuantityMinimum").val();
                    var couponCode = $j("#CouponCode").val();
                    if (discount.length < 1 || discountProduct.length < 1 || requireProduct.length < 1 || quantityMinimum.length < 1 || couponCode.length < 1) {
                        $j("#valDiscount").text("Discount is requird.").addClass("field-validation-error");
                        $j("#valRequiredProduct").text("Product is requird.").addClass("field-validation-error");
                        $j("#valDiscountProduct").text("Product is required.").addClass("field-validation-error");
                        $j("#valQuantityMinimum").text("Enter Minimum Quantity").addClass("field-validation-error");
                        $j("#valCouponCode").text("Enter Coupon Code").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "5":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valOrderMinimum").text('').removeClass("field-validation-error");
                    $j("#valCouponCode").text('').removeClass("field-validation-error");
                    var discount = $j("#Discount").val();
                    var orderMinimum = $j("#OrderMinimum").val();
                    var couponCode = $j("#CouponCode").val();
                    if (discount.length < 1 || orderMinimum.length < 1 || couponCode.length < 1) {
                        $j("#valDiscount").text("Discount is requird.").addClass("field-validation-error");
                        $j("#valOrderMinimum").text("Enter Minimum Order.").addClass("field-validation-error");
                        $j("#valCouponCode").text("Enter Coupon Code").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "6":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valRequiredProduct").text('').removeClass("field-validation-error");
                    $j("#valOrderMinimum").text('').removeClass("field-validation-error");
                    $j("#valQuantityMinimum").text('').removeClass("field-validation-error");
                    $j("#valCouponCode").text('').removeClass("field-validation-error");
                    var discount = $j("#DiscountProduct").val();
                    var requireProduct = $j("#RequireProduct").val();
                    var orderMinimum = $j("#OrderMinimum").val();
                    var quantityMinimum = $j("#QuantityMinimum").val();
                    var couponCode = $j("#CouponCode").val();
                    if (discount.length < 1 || requireProduct.length < 1 || orderMinimum.length < 1 || quantityMinimum.length < 1 || couponCode.length < 1) {
                        $j("#valDiscount").text("Discount Product is requird.").addClass("field-validation-error");
                        $j("#valRequiredProduct").text("Product is requird.").addClass("field-validation-error");
                        $j("#valOrderMinimum").text("Enter Minimum Order").addClass("field-validation-error");
                        $j("#valQuantityMinimum").text("Enter Minimum Quantity").addClass("field-validation-error");
                        $j("#valCouponCode").text("Enter Coupon Code").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "7":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valOrderMinimum").text('').removeClass("field-validation-error");
                    $j("#valCouponCode").text('').removeClass("field-validation-error");
                    var discount = $j("#Discount").val();
                    var orderMinimum = $j("#OrderMinimum").val();
                    var couponCode = $j("#CouponCode").val();
                    if (discount.length < 1 || orderMinimum.length < 1 || couponCode.length < 1) {
                        $j("#valDiscount").text("Discount is requird.").addClass("field-validation-error");
                        $j("#valOrderMinimum").text("Enter Minimum Order.").addClass("field-validation-error");
                        $j("#valCouponCode").text("Enter Coupon Code").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "8":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valDiscountProduct").text('').removeClass("field-validation-error");
                    $j("#valRequiredProduct").text('').removeClass("field-validation-error");
                    $j("#valQuantityMinimum").text('').removeClass("field-validation-error");
                    $j("#valCouponCode").text('').removeClass("field-validation-error");
                    var discount = $j("#Discount").val();
                    var discountProduct = $j("#DiscountProduct").val();
                    var requireProduct = $j("#RequireProduct").val();
                    var quantityMinimum = $j("#QuantityMinimum").val();
                    var couponCode = $j("#CouponCode").val();
                    if (discount.length < 1 || discountProduct.length < 1 || requireProduct.length < 1 || quantityMinimum.length < 1 || couponCode.length < 1) {
                        $j("#valDiscount").text("Discount is requird.").addClass("field-validation-error");
                        $j("#valRequiredProduct").text("Product is requird.").addClass("field-validation-error");
                        $j("#valDiscountProduct").text("Product is required.").addClass("field-validation-error");
                        $j("#valQuantityMinimum").text("Enter Minimum Quantity").addClass("field-validation-error");
                        $j("#valCouponCode").text("Enter Coupon Code").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "9":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valRequiredProduct").text('').removeClass("field-validation-error");
                    var discount = $j("#Discount").val();
                    var requireProduct = $j("#RequireProduct").val();
                    if (discount.length < 1 || requireProduct.length < 1) {
                        $j("#valDiscount").text("Discount is requird.").addClass("field-validation-error");
                        $j("#valRequiredProduct").text("Product is requird.").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "10":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valRequiredProduct").text('').removeClass("field-validation-error");
                    var discount = $j("#Discount").val();
                    var requireProduct = $j("#RequireProduct").val();
                    if (discount.length < 1 || requireProduct.length < 1) {
                        $j("#valDiscount").text("Discount is requird.").addClass("field-validation-error");
                        $j("#valRequiredProduct").text("Product is requird.").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "11":
                {
                    return true;
                    break;
                }
            case "12":
                {
                    return true;
                    break;
                }
            case "13":
                {
                    $j("#valDiscountProduct").text('').removeClass("field-validation-error");
                    var discountProduct = $j("#DiscountProduct").val();
                    if (discountProduct.length < 1) {
                        $j("#valDiscountProduct").text("Product is required.").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "14":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valQuantityMinimum").text('').removeClass("field-validation-error");
                    $j("#valOrderMinimum").text('').removeClass("field-validation-error");
                    $j("#valCouponCode").text('').removeClass("field-validation-error");
                    var discount = $j("#Discount").val();
                    var orderMinimum = $j("#OrderMinimum").val();
                    var quantityMinimum = $j("#QuantityMinimum").val();
                    var couponCode = $j("#CouponCode").val();
                    if (discount.length < 1 || orderMinimum.length < 1 || quantityMinimum.length < 1 || couponCode.length < 1) {
                        $j("#valDiscount").text("Discount is requird.").addClass("field-validation-error");
                        $j("#valOrderMinimum").text("Enter Minimum Order.").addClass("field-validation-error");
                        $j("#valQuantityMinimum").text("Enter Minimum Quantity").addClass("field-validation-error");
                        $j("#valCouponCode").text("Enter Coupon Code").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "15":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valQuantityMinimum").text('').removeClass("field-validation-error");
                    $j("#valOrderMinimum").text('').removeClass("field-validation-error");
                    $j("#valCouponCode").text('').removeClass("field-validation-error");
                    var discount = $j("#Discount").val();
                    var orderMinimum = $j("#OrderMinimum").val();
                    var quantityMinimum = $j("#QuantityMinimum").val();
                    var couponCode = $j("#CouponCode").val();
                    if (discount.length < 1 || orderMinimum.length < 1 || quantityMinimum.length < 1 || couponCode.length < 1) {
                        $j("#valDiscount").text("Discount is requird.").addClass("field-validation-error");
                        $j("#valOrderMinimum").text("Enter Minimum Order.").addClass("field-validation-error");
                        $j("#valQuantityMinimum").text("Enter Minimum Quantity").addClass("field-validation-error");
                        $j("#valCouponCode").text("Enter Coupon Code").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "16":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valQuantityMinimum").text('').removeClass("field-validation-error");
                    $j("#valOrderMinimum").text('').removeClass("field-validation-error");
                    $j("#valCouponCode").text('').removeClass("field-validation-error");
                    var discount = $j("#Discount").val();
                    var orderMinimum = $j("#OrderMinimum").val();
                    var quantityMinimum = $j("#QuantityMinimum").val();
                    var couponCode = $j("#CouponCode").val();
                    if (discount.length < 1 || orderMinimum.length < 1 || quantityMinimum.length < 1 || couponCode.length < 1) {
                        $j("#valDiscount").text("Discount is requird.").addClass("field-validation-error");
                        $j("#valOrderMinimum").text("Enter Minimum Order.").addClass("field-validation-error");
                        $j("#valQuantityMinimum").text("Enter Minimum Quantity").addClass("field-validation-error");
                        $j("#valCouponCode").text("Enter Coupon Code").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "17":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valQuantityMinimum").text('').removeClass("field-validation-error");
                    $j("#valOrderMinimum").text('').removeClass("field-validation-error");
                    $j("#valCouponCode").text('').removeClass("field-validation-error");
                    var discount = $j("#Discount").val();
                    var orderMinimum = $j("#OrderMinimum").val();
                    var quantityMinimum = $j("#QuantityMinimum").val();
                    var couponCode = $j("#CouponCode").val();
                    if (discount.length < 1 || orderMinimum.length < 1 || quantityMinimum.length < 1 || couponCode.length < 1) {
                        $j("#valDiscount").text("Discount is requird.").addClass("field-validation-error");
                        $j("#valOrderMinimum").text("Enter Minimum Order.").addClass("field-validation-error");
                        $j("#valQuantityMinimum").text("Enter Minimum Quantity").addClass("field-validation-error");
                        $j("#valCouponCode").text("Enter Coupon Code").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "18":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valQuantityMinimum").text('').removeClass("field-validation-error");
                    $j("#valOrderMinimum").text('').removeClass("field-validation-error");
                    $j("#valCouponCode").text('').removeClass("field-validation-error");
                    var discount = $j("#Discount").val();
                    var orderMinimum = $j("#OrderMinimum").val();
                    var quantityMinimum = $j("#QuantityMinimum").val();
                    var couponCode = $j("#CouponCode").val();
                    if (discount.length < 1 || orderMinimum.length < 1 || quantityMinimum.length < 1 || couponCode.length < 1) {
                        $j("#valDiscount").text("Discount is requird.").addClass("field-validation-error");
                        $j("#valOrderMinimum").text("Enter Minimum Order.").addClass("field-validation-error");
                        $j("#valQuantityMinimum").text("Enter Minimum Quantity").addClass("field-validation-error");
                        $j("#valCouponCode").text("Enter Coupon Code").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            case "19":
                {
                    $j("#valDiscount").text('').removeClass("field-validation-error");
                    $j("#valQuantityMinimum").text('').removeClass("field-validation-error");
                    $j("#valOrderMinimum").text('').removeClass("field-validation-error");
                    $j("#valCouponCode").text('').removeClass("field-validation-error");
                    var discount = $j("#Discount").val();
                    var orderMinimum = $j("#OrderMinimum").val();
                    var quantityMinimum = $j("#QuantityMinimum").val();
                    var couponCode = $j("#CouponCode").val();
                    if (discount.length < 1 || orderMinimum.length < 1 || quantityMinimum.length < 1 || couponCode.length < 1) {
                        $j("#valDiscount").text("Discount is requird.").addClass("field-validation-error");
                        $j("#valOrderMinimum").text("Enter Minimum Order.").addClass("field-validation-error");
                        $j("#valQuantityMinimum").text("Enter Minimum Quantity").addClass("field-validation-error");
                        $j("#valCouponCode").text("Enter Coupon Code").addClass("field-validation-error");
                        return false;
                    }
                    return true;
                    break;
                }
            default:
                {
                    return true;
                }
        }
    },
}
