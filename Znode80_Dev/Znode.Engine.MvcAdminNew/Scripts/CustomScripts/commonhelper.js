﻿var $j = jQuery.noConflict();
CommonHelper = {
    BindDeleteConfirmDialog: function (title, message, deleteUrl) {
        this.ShowPopUp(title, message, deleteUrl);
        return false;
    },

    ShowPopUp: function (title, message, url) {
        if ($j("BODY").find("div#popup_container").length == 0) {
            this.CreatePopUp();
        }
        $j("#popup_title").text(title);
        $j("#popup_message").text(message);

        $j("#popup_container").dialog({
            resizable: false,
            modal: true,
            show: 'explode',
            stack: true,
            buttons: {
                "OK": function () {
                    $j(this).dialog("close");
                    App.Api.deleteRecordById(url, function (response) {
                        var type = "error";
                        if (response.sucess) {
                            type = "success";
                            var rowDeleteID = "spanName_" + response.id;
                            $j("#" + rowDeleteID).closest('tr').remove();
                        }

                        CommonHelper.DisplayNotificationMessagesHelper(response.message, type, response.isFadeOut, 5000);
                    });
                },
                Cancel: function () {
                    $j(this).dialog("close");
                }
            }
        });
    },

    CreatePopUp: function () {
        var popup = '<div id="popup_container" class="ConfirmPopup" style="display:none;">' +
                        '<div class="PopupHeder ClearFix">' +
                            '<h3 id="popup_title"></h3>' +
                        '</div>' +
                        '<div id="popup_content" class="PopupContent ClearFix">' +
                          '<div id="popup_message"></div>' +
                        '</div>' +
                    '</div>';
        $j("BODY").append(popup);
    },

    LoadLayout: function () {
        this.CommonControl();
        this.DisplayNotificationMessages();
        this.CheckGiftCardCreateCheckBox();
    },
    CommonControl: function () {
        $j("#menu-toggle").click(function (e) {
            e.preventDefault();
            $j("#body").toggleClass("toggled");
        });

        $j('.sidebar-nav li .sub-menu').click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            var $parentli = $j(this).closest('li');
            $parentli.siblings('li').find('ul:visible').hide();
            $parentli.find('> ul').stop().toggle();
        });

        $('.datepicker').datepicker({ format: 'yyyy/mm/dd' });
                        
        $("[data-toggle='tooltip']").tooltip();

        //$('.panel-heading h4').on('click', function (e) {
        //    if ($(this).parents('.panel').children('.panel-collapse').hasClass('in')) {
        //        e.preventDefault();
        //        e.stopPropagation();
        //    }
        //});        
        
    },

    DisplayNotificationMessages: function () {
        var element = $j(".MessageBoxContainer");
        if (element.length) {
            var msgObj = element.data('message');
            if (msgObj !== "") {
                this.DisplayNotificationMessagesHelper(msgObj.Message, msgObj.Type, msgObj.IsFadeOut, msgObj.FadeOutMilliSeconds);
            }
        }
    },

    DisplayNotificationMessagesHelper: function (message, type, isFadeOut, fadeOutMilliSeconds) {
        var element = $j(".MessageBoxContainer");
        $j(".MessageBoxContainer").removeAttr("style");
        var closeBtnHtml = "<div><span onclick='CommonHelper.CloseMessageNotificationContainer(this);' class='right glyphicon glyphicon-remove-circle'></span></div>";
        $j(window).scrollTop(0);
        $j(document).scrollTop(0);
        if (element.length) {
            if (message !== "" && message != null) {

                element.html("<div class='MessageBox alert'><p>" + message + closeBtnHtml + "</p></div></div>");

                switch (type) {
                    case "success":
                        {
                            element.find('div').addClass('alert-success');
                            break;
                        }
                    case "error":
                        {
                            element.find('div').addClass('alert-danger');
                            break;
                        }
                    default:
                        {
                            element.find('div').addClass('alert-info');
                        }
                }

                if (isFadeOut === "" || isFadeOut == null || typeof isFadeOut === "undefined") isFadeOut = true;
                if (fadeOutMilliSeconds === "" || fadeOutMilliSeconds == null || typeof fadeOutMilliSeconds === "undefined") fadeOutMilliSeconds = 10000;

                if (isFadeOut) {
                    setTimeout(function () {
                        element.fadeOut().empty();
                    }, fadeOutMilliSeconds);
                }
            }
        }
    },

    CloseDisplayBox: function () {
        var $target = $j(".MessageBox");
        $target.slideUp('normal', function () { $target.remove(); });
    },

    CloseMessageNotificationContainer: function (messageContainer) {
        $j(messageContainer).parent("div").parent("div").parent("div").hide();
    },

    CloseMessageContainerOnTab: function () {
        $j("#MessageBoxContainerId").hide();
    },

    CheckGiftCardCreateCheckBox: function () {
        if ($j("#EnableToCustomerAccount").length > 0) {
            if ($j($j("#EnableToCustomerAccount")[0]).is(":checked")) {
                $j("#ShowAccountId").removeClass("hidden");
                $j("#AccountId").attr("required", true);
            } else {
                $j("#ShowAccountId").addClass("hidden");
                $j("#AccountId").val('');
                $j("#AccountId").removeAttr("required");
            }
        }
    },

    ShowTextBox: function () {
        if ($j('#EnableToCustomerAccount').prop("checked") == true) {
            $j('#ShowAccountId').removeClass("hidden");
            $j("#AccountId").attr("required", true);
        } else {
            $j('#ShowAccountId').addClass("hidden");
            $j("#AccountId").val('');
            $j("#AccountId").removeAttr("required");
        }
    },

    Tab_Change_Handler: function ($this) {
        var queryParam = $j($this).attr("data-queryparam");
        var targetItemId = $j($this).attr("data-targetitemid");
        var url = $j($this).attr("href");
        App.Api.getTabDetails(url, queryParam, function (response) {
            if (response.length > 0) {
                $j("#" + targetItemId).html(response);
                var activeTabclassName = "active";
                var activePanelClassName = "in active";
                $j("." + activeTabclassName).removeClass(activeTabclassName);
                $j("." + activePanelClassName).removeClass(activePanelClassName);
                $j("#" + targetItemId).addClass(activePanelClassName);
                $j($this).parent("li").addClass("active");
            }
        });
        return false;
    },
}

// Global debug
var enableDebug = true;

function _debug(s) {
    if (enableDebug) {
        if (typeof console === "undefined") {
            //console = { log: function(s){ alert(s); } }
        } else {
            console.log(s);
        }
    }
}


// In-window scrolling, uses jQuery
function scrollToTop(speed, selector) {
    if (!speed) { speed = 300; }

    if (!selector) { selector = "body"; }

    $("html, body").animate({ scrollTop: $(selector).offset().top }, speed);
}


// jQuery plugin for disabled attribute on element
(function ($) {
    $.fn.toggleDisabled = function () {
        return this.each(function () {
            var $this = $(this);
            if ($this.attr('disabled')) {
                $this.removeAttr('disabled');
            } else {
                $this.attr('disabled', 'disabled');
            }
        });
    };
})(jQuery);