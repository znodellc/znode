﻿var $j = jQuery.noConflict();
AssociateCategory = {
    BindCssList: function (cssList, masterPageList,cssThemeId) {      
        App.Api.getCategoryCssListByThemeId(cssThemeId.selectedIndex, function (response) {
            $j('#' + cssList.id).empty();          
            for (var i = 0; i < response.length; i++) {
                if (parseInt(response[i].Value) > 0)
                {
                    var defaultValue = parseInt(response[i].Value) - 1;
                    $j('#' + cssList.id).append("<option value='" + defaultValue + "'>Same as store</option>");
                    $j('#' + cssList.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
                }
                else
                {
                    $j('#' + cssList.id).append("<option value='0'>Same as store</option>");
                    $j('#' + cssList.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
                }
            }
            if (response.length < 1) {
                $j('#' + cssList.id).append("<option value=''>Same as store</option>");
            }
        });
        AssociateCategory.BindMasterPageList(masterPageList,cssThemeId.selectedIndex);
    },
    BindMasterPageList: function (masterPageList,cssThemeId) {
        App.Api.getCategoryMasterPageListByThemeId(cssThemeId, function (response) {
            $j('#' + masterPageList.id).empty();
            for (var i = 0; i < response.length; i++) {
                if (parseInt(response[i].Value) > 0) {
                    var defaultValue = parseInt(response[i].Value) - 1;
                    $j('#' + masterPageList.id).append("<option value='" + defaultValue + "'>Select Template</option>");
                    $j('#' + masterPageList.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
                }
                else
                {
                    $j('#' + masterPageList.id).append("<option value='0'>Select Template</option>");
                    $j('#' + masterPageList.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
                }
            }
            if (response.length < 1) {
                $j('#' + masterPageList.id).append("<option value=''>Select Template</option>");
            }
        });

    }   
}