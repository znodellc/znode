﻿var $j = jQuery.noConflict();

Portal = {
    BindCssList: function (ddlCss, ddlThemes) {
        var themeId=$j("#"+ddlThemes.id+" option:selected").val();
        App.Api.GetPortalCssListByThemeId(themeId, function (response) {
            $j('#' + ddlCss.id).empty();
            for (var i = 0; i < response.length; i++) {
                $j('#' + ddlCss.id).append("<option value='" + response[i].Value + "'>" + response[i].Text + "</option>");
            }
            if (response.length < 1) {
                $j('#' + ddlCss.id).append("<option value=''>Same as store</option>");
            }
        });
    }
}


