﻿var $j = jQuery.noConflict();
Highlight = {
    DisplayText: function () {
        $j('#divHyperlink').hide();
        $j('#divDisplayText').show().removeClass("hidden");
    },

    Hyperlink: function () {
        $j('#divDisplayText').hide();
        $j('#divHyperlink').show().removeClass("hidden");
    },

    UploadNewImage: function () {
        $j('#editImageSettings').show().removeClass("hidden");
        $j('#imgHighlight').removeClass("hidden");
        $j('#hdnNoImage').val(false);
    },

    KeepCurrentImage: function () {
        $j('#editImageSettings').addClass("hidden");
        $j('#imgHighlight').removeClass("hidden");
        $j('#hdnNoImage').val(false);
    },

    NoImage: function () {
        $j('#editImageSettings').addClass("hidden");
        $j('#imgHighlight').addClass("hidden");
        $j('#hdnNoImage').val(true);
    }
}
