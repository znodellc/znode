﻿var $j = jQuery.noConflict();
Product = {
    load: function () {
        var selectedItems = $j("#ProductAttributes").val();
        if (selectedItems.length > 0) {
            $j("#ProductTypeId").change();
        }
        $j("#FreeShipping").change();
        $j("#ShippingRuleTypeId").change();
    },
    GetProductAttribute: function (obj) {
        var productTypeId = $j('#' + obj.id + ' option:selected').val();
        this.BindAttributes(productTypeId);
    },
    BindAttributes: function (productTypeId) {
        $j("#divAttributes").hide();
        $j("#divGiftCards").hide();
        $j("#divProductAttributes").html('');
        App.Api.getProductAttributesByProductTypeId(productTypeId, function (response) {
            if (response.length > 0) {
                var isGiftCard = response[0].IsGiftCard;
                if (isGiftCard == 1) {
                    $j("#IsProductTypeGiftCards").val(isGiftCard);
                    $j("#divGiftCards").show();
                }
                else {
                    Product.BindAttributeList(response);
                    $j("#divAttributes").show();
                }
            }
        });
    },
    BindAttributeList: function (data) {
        for (var i = 0; i < data.length; i++) {
            var drpId = "ddlAttribute_" + data[i].AttributeTypeId;
            var ddl = '<select name="' + drpId + '" id="' + drpId + '" class="ddlAttributeList"  onchange="Product.ClearValidation(this);"><option value="">' + data[i].AttributeName + '</option>';
            for (var j = 0; j < data[i].AttributeValueList.length; j++) {
                var attributId = data[i].AttributeValueList[j].Value;
                var attributName = data[i].AttributeValueList[j].Text;
                var selected = Product.CheckSelectedAttribute(attributId);
                if (selected) {
                        ddl += '<option value="' + attributId + '"selected="selected">' + attributName + '</option>';
                    }
                    else {
                        ddl += '<option value="' + attributId + '">' + attributName + '</option>';
                    }
                }
            ddl += '</select>'
            var errId = "val" + drpId;
            ddl += '<span id="' + errId + '" class="field-validation-valid" data-valmsg-replace="true" data-valmsg-for="' + drpId + '"></span>'
            $j("#divProductAttributes").append(ddl);
        }
    },
    CheckSelectedAttribute: function (attributId) {
       var selectedItems = $j("#ProductAttributes").val();
        var arrSelectedItems = [];
        if (selectedItems.length > 0) {
            arrSelectedItems = selectedItems.split(',');
            for (var i = 0; i < arrSelectedItems.length; i++) {
                if (parseInt(arrSelectedItems[i]) == parseInt(attributId)) {
                    return true;
                }
            }
        }
        return false;
    },
    ValidateAttributes: function () {
        var attributeIds = '';
        var isValid = true;
        $j('select.ddlAttributeList').each(function () {
            var attributeId = $j('#' + $(this).attr("id") + ' option:selected').val();
            var errorId = "val" + $(this).attr("id");
            $j("#" + errorId).text('').removeClass("field-validation-error");
            if (attributeId.length > 0) {
                attributeIds += attributeId + ',';
            }
            else {
                isValid = false;
                var errmsg = $j('#' + $(this).attr("id") + ' option:selected').text() + " is required.";
                $j("#" + errorId).text('').text(errmsg).addClass("field-validation-error");
            }
        });
        if (attributeIds.length > 0) {
            $j("#ProductAttributes").val(attributeIds.slice(0, -1));
        }
        if (isValid == false) {
            return false;
        }
        $j("#valExpirationPeriod").text('').removeClass("field-validation-error");
        if ($j("#IsProductTypeGiftCards").val() == 1) {
            var expPeriod = $j("#ExpirationPeriod").val();
            if (expPeriod.length < 1) {
                $j("#valExpirationPeriod").text("Expiration Period is requird.").addClass("field-validation-error");
                return false;
            }
        }
        return true;
    },
    ClearValidation: function (ctrl) {
        var errorId = "val" + ctrl.id;
        $j("#" + errorId).text('').removeClass("field-validation-error");
    },
    ToggleShipping: function () {
        var checked = $j('#FreeShipping').prop("checked");
        if (checked == true) {
            $j("#divShipping").hide();
        }
        else {
            $j("#divShipping").show();
        }
    },
    ToggleShippingRate: function (obj) {
        var shippingTypeId = $j('#' + obj.id + ' option:selected').val();
        if (parseInt(shippingTypeId) == 3) {
            $j("#divShippingRate").show();
        }
        else {
            $j("#divShippingRate").hide();
        }
    },
    CheckSucess: function (data) {
        var type = "error";
        if (data.sucess) {
            type = "success";
        }
        document.getElementById("panel7").innerHTML = data.Response;
        try { if (data.message == "") { $j('#MessageBoxContainerId').html(''); } } catch (err) { }
        CommonHelper.DisplayNotificationMessagesHelper(data.message, type, data.isFadeOut, 5000);
    },
    BindFacetsList: function (data, productId) {
        var facetGroupId = $j(data).val();
        if (productId > 0 && facetGroupId > 0)
        {
            App.Api.BindFacetListByFacetGroupId(productId,facetGroupId, function (response) {
                _debug(response);
                $("#chklistFacets").html(response);
            });
        }
    }
}
