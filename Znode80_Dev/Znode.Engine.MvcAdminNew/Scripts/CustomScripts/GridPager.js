﻿
/*
 | Webgrid ajax update callback, paging and sorting related js.
 |
 | 2015; Znode, Inc.
*/

var WebgridHelper = {
    GridUpdateHandler: function (data) {
        $("#grid-container").html(data);
        WebgridPagerHelper.UpdateHandler();
    }
};

$(document).ready(function () {

    WebgridPagerHelper.UpdateHandler();

    var pageCount = PageCount;
    if (pageCount == 0) {
        $('#pageSizeList').attr('disabled', '');
        $('#pagerTxt').attr('disabled', '');
    }

    $(document).on("enterKey", "#pagerTxt", WebgridPagerHelper.UrlHandler);

    $('#pagerTxt').keyup(function (e) {
        if (e.keyCode == 13) {
            $(this).trigger("enterKey");
        }
    });

    $("#pagerTxt").keydown(function (event) {
        // Allow backspace, delete, tab, escape, enter
        if ($.inArray(event.keyCode, [46, 8, 9, 27, 13]) !== -1 ||
            // Allow Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
            return;
        }
        else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });

    $(document).on("change", "#pageSizeList", WebgridPagerHelper.SelectedPageSize);

    $("#grid .grid-header th a").bind('click', false).on('click', WebgridPagerHelper.SortingHandler);

});


var WebgridPagerHelper = {

    UpdateHandler: function () {

        this.SetArrows();
        this.PreviousAndNextUpdateHandler();
        this.SetPagingInput();
        this.PagingHandler();

    },

    SetArrows: function (url) {
       
        var dir = $('#dir').val();
        var col = $('#col').val();
        var sortCol = "sort=" + col;

        var header = null;
        $('th a').each(function () {
            var href = $(this).attr('href');
            if (href.indexOf(sortCol) != -1) {
                header = $(this);
                return false;
            }
        });
     
        var newUrl = new jurl(url);
        var sortDir = newUrl.getQueryParameter(SortDirFieldName);

        if (header != null) {
            if (sortDir != null && col != "" && typeof col != typeof undefined && col != null) {

                if (dir == 'Ascending') {
                    header.removeClass('Descending');
                    header.addClass('Ascending');
                    header.html(header.text() + '<img src="" alt="▲" style="padding-left:4px;"/>');
                }
                if (dir == 'Descending') {
                    header.removeClass('Ascending');
                    header.addClass('Descending');
                    header.html(header.text() + '<img src="" alt="▼" style="padding-left:4px;" />');
                }
            }
        }
    },

    PreviousAndNextUpdateHandler: function () {

        var MaxPages = PageCount;
        var currentPageNumber = PageIndex + 1;

        if (currentPageNumber <= 1) {
            $('#previousPage').on('click', false);
            $('#previousPage').addClass('DisableArrow');
        }
        else {
            $('#previousPage').off('click', false);
            $('#previousPage').removeClass('DisableArrow');
        }

        if (currentPageNumber >= MaxPages) {
            $('#nextPage').on('click', false);
            $('#nextPage').addClass('DisableArrow');
        }
        else {
            $('#nextPage').off('click', false);
            $('#nextPage').removeClass('DisableArrow');
        }
    },

    SetPagingInput: function () {

        var currentPageNumber = PageIndex + 1;
        var MaxPages = PageCount;

        $("#pagerTxt").attr('value', currentPageNumber);

        if (MaxPages == 1) {
            $("#pageCountLabel").html('Page');
        }
        else {
            $("#pageCountLabel").html("Pages");
        }
    },

    PagingHandler: function () {

        $("#nextPage").off("click");
        $("#nextPage").on('click', function (e) {
            e.preventDefault();
            var MaxPages = PageCount;
            var currentPageNumber = PageIndex + 1;
            var requestedPage = parseInt(currentPageNumber) + 1;

            var url = window.location.href;
            if (!(requestedPage <= parseInt(MaxPages))) {
                requestedPage = currentPageNumber;
            }

            var newUrl = new jurl(url);
            newUrl.setQueryParameter(RecordPerPageFieldName, PageSize);
            newUrl.setQueryParameter(PageFieldName, requestedPage);
            if (Sort != null) {
                newUrl.setQueryParameter(SortFieldName, Sort);
                newUrl.setQueryParameter(SortDirFieldName, SortDir);
            }
            WebgridPagerHelper.pagingUrlHandler(newUrl.build());
        });

        $("#previousPage").off("click");
        $("#previousPage").on('click', function (e) {

            e.preventDefault();
            var MaxPages = PageCount;
            var currentPageNumber = PageIndex + 1;
            var requestedPage = parseInt(currentPageNumber) - 1;
            var url = window.location.href;

            if (requestedPage <= 0) {
                requestedPage = currentPageNumber;
            }

            var newUrl = new jurl(url);
            newUrl.setQueryParameter(RecordPerPageFieldName, PageSize);
            newUrl.setQueryParameter(PageFieldName, requestedPage);
            if (Sort != null) {
                newUrl.setQueryParameter(SortFieldName, Sort);
                newUrl.setQueryParameter(SortDirFieldName, SortDir);
            }
            WebgridPagerHelper.pagingUrlHandler(newUrl.build());

        });

        $("#first").off('click');
        $("#first").on('click', function (e) {
            e.preventDefault();
            var currentPageNumber = PageIndex + 1;
            var requestedPage = 1;
            var url = window.location.href;

            if (requestedPage <= 0) {
                requestedPage = currentPageNumber;
            }

            var newUrl = new jurl(url);
            newUrl.setQueryParameter(RecordPerPageFieldName, PageSize);
            newUrl.setQueryParameter(PageFieldName, requestedPage);
            if (Sort != null) {
                newUrl.setQueryParameter(SortFieldName, Sort);
                newUrl.setQueryParameter(SortDirFieldName, SortDir);
            }
            WebgridPagerHelper.pagingUrlHandler(newUrl.build());
        });

        $("#last").off('click');
        $("#last").on('click', function (e) {
            e.preventDefault();
            var currentPageNumber = PageIndex + 1;
            var requestedPage = PageCount;
            var url = window.location.href;

            if (requestedPage <= 0) {
                requestedPage = currentPageNumber;
            }

            var newUrl = new jurl(url);
            newUrl.setQueryParameter(RecordPerPageFieldName, PageSize);
            newUrl.setQueryParameter(PageFieldName, requestedPage);
            if (Sort != null) {
                newUrl.setQueryParameter(SortFieldName, Sort);
                newUrl.setQueryParameter(SortDirFieldName, SortDir);
            }
            WebgridPagerHelper.pagingUrlHandler(newUrl.build());

        });
    },

    SortingHandler: function (e) {
        e.preventDefault();
        var url = e.currentTarget.attributes[1].value;

        if (!window.location.origin) {
            window.location.origin = window.location.protocol + "//" + window.location.host;
        }

        url = window.location.origin + url;
        var browserUrl = new jurl(url);
        var pageNumber = PageIndex;
        var sortDir = SortDir;
        var anchorUrl = $(this).attr('href');

        if (pageNumber != null) {
            anchorUrl = anchorUrl + "&" + PageFieldName + "=" + 1;
        }

        var absoluteUrl = window.location.origin + anchorUrl;
        var newUrl = new jurl(absoluteUrl);
        WebgridPagerHelper.pagingUrlHandler(newUrl.build());
    },

    pagingUrlHandler: function (newUrl) {
        var formData = $("#searchform").serialize();
        $.ajax({
            type: "POST",
            url: newUrl,
            data: formData,
        }).success(function (html) {
            WebgridHelper.GridUpdateHandler(html);
            WebgridPagerHelper.SetArrows(newUrl);
        });
    },

    UrlHandler: function () {
        var MaxPages = PageCount;
        var url = window.location.href;
        var requestedPage = parseInt($("#pagerTxt").val());
        if (!(requestedPage > 0 && requestedPage <= parseInt(MaxPages))) {
            requestedPage = 1;
        }

        var newUrl = new jurl(url);
        newUrl.setQueryParameter(RecordPerPageFieldName, PageSize);
        newUrl.setQueryParameter(PageFieldName, requestedPage);
        WebgridPagerHelper.pagingUrlHandler(newUrl.build());
    },

    SelectedPageSize: function () {
        var recordPerPage = $('#pageSizeList').val();
        var url = window.location.href;
        var newUrl = new jurl(url);
        newUrl.setQueryParameter(RecordPerPageFieldName, recordPerPage);
        newUrl.setQueryParameter(PageFieldName, 1);
        WebgridPagerHelper.pagingUrlHandler(newUrl.build());
    },

}



