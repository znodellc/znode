﻿var $j = jQuery.noConflict();
Banner = {
    DisplayMessageKeyTextBox: function () {
        if ($j('#ddlBannerKeys').val() == '+1')
        {
            $j('#ddlBannerKeys').hide();
            $j('#btnBannerMessageKey').show();
            $j('#txtMessagekey').show();           
        }
    },
    ClearSelection: function () {
        $j('#ddlBannerKeys').show();
        $j('#btnBannerMessageKey').hide();
        $j('#txtMessagekey').hide();
        $j('#ddlBannerKeys').val('0');
        $j('#txtMessagekey').val('');
        $j("span[data-valmsg-for='MessageKey']").html('');
    },
}
