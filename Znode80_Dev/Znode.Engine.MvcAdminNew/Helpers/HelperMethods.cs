﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Web.UI.WebControls;

namespace Znode.Engine.MvcAdmin.Helpers
{
    public static class HelperMethods
    {
        public static string GetImagePath(string image)
        {

            if (!string.IsNullOrEmpty(image))
            {
                return image.Replace("~", ConfigurationManager.AppSettings["ZnodeApiRootUri"]);
            }
            return string.Empty;
        }

        /// <summary>
        /// Get the enable/disable checkmark icon
        /// </summary>
        /// <param name="isEnabled">Indicates to icon enabled or disabled status.</param>
        /// <returns>Returns enabled/disabled checkmark icon.</returns>
        public static string GetCheckMark(bool isEnabled)
        {
            string icon = string.Empty;

            if (!Equals(isEnabled, null))
            {
                if (isEnabled == true)
                {
                    icon = "glyphicon glyphicon-ok";
                }
                else
                {
                    icon = "glyphicon glyphicon-remove";
                }
            }
            return icon;
        }

        public static string GetDomainUrl()
        {
            return (!string.IsNullOrEmpty(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority))) ? HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) : string.Empty;
        }

        public static bool IsDebugMode
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["IsDebugMode"]);
            }
        }

    }
}