﻿
namespace Znode.Engine.MvcAdmin.Helpers
{
    #region Reset Password Status Types
    /// <summary>
    /// Enum for Reset Password Status Types
    /// </summary>
    public enum ResetPasswordStatusTypes
    {
        Continue = 2001,
        LinkExpired = 2002,
        TokenMismatch = 2003,
        NoRecord = 2004,
    }
    #endregion

    #region MessageType
    /// <summary>
    /// Enum for Message Type
    /// </summary>
    public enum MessageType
    {
        MessageBlock = 1,
        Banner = 2
    }
    #endregion

    #region DurationType
    /// <summary>
    /// Enum for Duration Type
    /// </summary>
    public enum DurationType
    {
        Days = 1,
        Weeks = 7,
        Months = 30,
        Years = 365
    }
    #endregion

    #region ProductReviewState
    /// <summary>
    /// Represent the product review state enum
    /// Enumeration ID value must match with ZNodeProductReviewState table ProductReviewStateID column.
    /// </summary>
    public enum ZNodeProductReviewState
    {
        /// <summary>
        /// Product is awaiting for reviewer Aproval.
        /// </summary>
        PendingApproval = 10,

        /// <summary>
        /// Product is approved from Reviewer.
        /// </summary>
        Approved = 20,

        /// <summary>
        /// Product is declined by Reviewer.
        /// </summary>
        Declined = 30,

        /// <summary>
        /// New product added to edit.
        /// </summary>
        ToEdit = 40
    }
      #endregion

    #region Znode CRUD Mode
    public enum ZnodeCRUDMode
    {
        Insert,
        Update,
        Delete,
        Error
    }
    #endregion
}