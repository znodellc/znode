﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Helpers
{
    public class PageDataBinder: DefaultModelBinder
    {
        /// <summary>
        /// Bind Model for filter and sort results
        /// </summary>
        /// <param name="controllerContext">controllerContext</param>
        /// <param name="bindingContext">bindingContext</param>
        /// <returns>Returns FilterCollectionDataModel</returns>
        public override object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelType.IsSubclassOf(typeof(BaseViewModel)))
            {
                HttpRequestBase request = controllerContext.HttpContext.Request;

                int _page = 1;
                int _recordPerPage = 10;
                string _sortKey = string.Empty;
                string _sortDir = "ASC";

                SortCollection sortCollection = GetSortCollection(request, ref _page, ref _recordPerPage, ref _sortKey, ref _sortDir);

                return new FilterCollectionDataModel()
                {
                    Page = _page,
                    RecordPerPage = _recordPerPage,
                    SortCollection = sortCollection,
                    Filters = GetFilterCollection(request)
                };
            }
            else
            {
                return base.BindModel(controllerContext, bindingContext);
            }
        }

        /// <summary>
        /// Get sort collection from request
        /// </summary>
        /// <param name="request">Http Request</param>
        /// <param name="_page">ref _page</param>
        /// <param name="_recordPerPage">ref _recordPerPage</param>
        /// <param name="_sortKey">ref _sortKey</param>
        /// <param name="_sortDir">ref _sortDir</param>
        /// <returns>Returns SortCollection</returns>
        private SortCollection GetSortCollection(HttpRequestBase request, ref int _page, ref int _recordPerPage, ref string _sortKey, ref string _sortDir)
        {
            SortCollection sortCollection = new SortCollection();
            Dictionary<string, string> _paginationcollection = new Dictionary<string, string>();

            var queryStringCollection = request.QueryString;

            if (queryStringCollection.Count > 0)
            {
                foreach (var keys in queryStringCollection)
                {
                    if (!Equals(keys, null))
                    {
                        var kayname = keys.ToString();

                        var kayValue = queryStringCollection[kayname].ToString();

                        _paginationcollection.Add(kayname, kayValue);
                    }
                }
            }

            if (!Equals(_paginationcollection, null))
            {
                foreach (var x in _paginationcollection)
                {
                    switch (x.Key.ToLower())
                    {
                        case "page":
                            if (x.Value != "" && x.Value != null)
                                _page = int.Parse(x.Value);
                            else
                                _page = 1;
                            break;

                        case "recordperpage":
                            if (x.Value != "" && x.Value != null)
                                _recordPerPage = int.Parse(x.Value);
                            else
                                _recordPerPage = 10;
                            break;
                        case "sort":
                            if (x.Value != "" && x.Value != null)
                                _sortKey = x.Value;
                            break;
                        case "sortdir":
                            if (x.Value != "" && x.Value != null)
                                _sortDir = x.Value;
                            else
                                _sortDir = "ASC";
                            break;

                    }
                }

            }

            if (string.IsNullOrEmpty(_sortKey))
            {
                return null;
            }
            else
            {
                sortCollection.Add("sort", _sortKey);
                sortCollection.Add("sortDir", _sortDir);
            }
            return sortCollection;
        }

        /// <summary>
        /// Get Filter Collection
        /// </summary>
        /// <param name="request">Http request</param>
        /// <returns>Returns FilterCollection</returns>
        private FilterCollection GetFilterCollection(HttpRequestBase request)
        {
            FilterCollection filters = new FilterCollection();
            var formCollection = request.Form;

            foreach (var _keys in formCollection)
            {
                var kayname = _keys.ToString();
                var kayValue = formCollection[kayname].ToString();

                if (!string.IsNullOrEmpty(kayValue))
                {
                    filters.Add(new FilterTuple(kayname, FilterOperators.Contains, kayValue));
                }
            }

            return filters;
        }
    }
}