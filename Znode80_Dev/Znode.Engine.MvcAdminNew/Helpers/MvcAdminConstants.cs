﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.Helpers
{
    public class MvcAdminConstants
    {
        public const string NoImageName = "noimage.gif";
        public const string CreateEditView = "CreateEdit";
        public const string CreateView = "Create";
        public const string ListView = "List";
        public const string ManageView = "Manage";
        public const string Notifications = "Notifications";
        public const string PartialListView = "_List";
        public const string DisplayAssociatedFacet = "DisplayAssociatedFacet";
        public const string AssociatedFacet = "AssociatedFacet";
        public const string LabelCatalogId = "CatalogId";
        public const string LabelName = "Name";
        public const string LabelControlTypeID = "ControlTypeID";
        public const string LabelControlName = "ControlName";
        public const string LabelCategoryId = "CategoryId";
        public const string UserAccountSessionKey = "UserAccount";
        public const string ErrorCodeSessionKey = "ErrorCode";
        public const string LoginCookieNameValue = "loginCookie";
        public const string ImageFileTypes = "jpg,jpeg,png,bmp";
        public const string ErrorView = "Error";

        public const string BannerMessageKey = "MessageKey";
        public const string BannerDoNotShowValue = "0";
        public const string BannerAddNewLocationValue = "+1";
        public const int LocaleId = 43;
        public const int ImageMaxFileSize = 4194304;
        public const string productList = "ProductList";
        public const string productTypeId = "ProductTypeId";
        public const string sort = "sort";
        public const string sortDir = "sortDir";
        public const string sortDirAsc = "ASC";

        public const string EnvironmentConfigKey = "EnvironmentConfigKey";
        public const string ContentPageHomeKey = "home";
    }
}