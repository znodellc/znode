﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services
{
    public interface IContentPageService
    {
        /// <summary>
        /// Gets List of content Pages.
        /// </summary>
        /// <param name="expands">Expands for the content pages.</param>
        /// <param name="filters">Filters for the content page list.</param>
        /// <param name="sorts">Sorts for content page list.</param>
        /// <param name="page">Paging informtion for content page list.</param>
        /// <returns></returns>
        ContentPageListModel GetContentPages(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Creates a content page.
        /// </summary>
        /// <param name="model">Content page model to be created.</param>
        /// <returns>Content page model.</returns>
        ContentPageModel CreateContentPage(ContentPageModel model);

        /// <summary>
        /// Clones a content page.
        /// </summary>
        /// <param name="contentPageModel">Content page model to be cloned.</param>
        /// <returns>Content Page Model.</returns>
        ContentPageModel Clone(ContentPageModel contentPageModel);

        /// <summary>
        /// Adds a content page with specified parameters.
        /// </summary>
        /// <param name="contentPageModel">Content page model to be added.</param>
        /// <returns></returns>
        bool AddPage(ContentPageModel contentPageModel);
    }
}
