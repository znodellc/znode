﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using AttributeTypeRepository = ZNode.Libraries.DataAccess.Service.AttributeTypeService;

namespace Znode.Engine.Services
{
	public class AttributeTypeService : BaseService, IAttributeTypeService
	{
		private readonly AttributeTypeRepository _attributeTypeRepository;

		public AttributeTypeService()
		{
			_attributeTypeRepository = new AttributeTypeRepository();
		}

		public AttributeTypeModel GetAttributeType(int attributeTypeId)
		{
			var attributeType = _attributeTypeRepository.GetByAttributeTypeId(attributeTypeId);
			return AttributeTypeMap.ToModel(attributeType);
		}

		public AttributeTypeListModel GetAttributeTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new AttributeTypeListModel();
			var attributeTypes = new TList<AttributeType>();

			var query = new AttributeTypeQuery();
			var sortBuilder = new AttributeTypeSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			attributeTypes = _attributeTypeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var a in attributeTypes)
			{
				model.AttributeTypes.Add(AttributeTypeMap.ToModel(a));
			}

			return model;
		}

		public AttributeTypeModel CreateAttributeType(AttributeTypeModel model)
		{
			if (model == null)
			{
				throw new Exception("Attribute type model cannot be null.");
			}

			var entity = AttributeTypeMap.ToEntity(model);
			var attributeType = _attributeTypeRepository.Save(entity);
			return AttributeTypeMap.ToModel(attributeType);
		}

		public AttributeTypeModel UpdateAttributeType(int attributeTypeId, AttributeTypeModel model)
		{
			if (attributeTypeId < 1)
			{
				throw new Exception("Attribute type ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Attribute type model cannot be null.");
			}

			var attributeType = _attributeTypeRepository.GetByAttributeTypeId(attributeTypeId);
			if (attributeType != null)
			{
				// Set attributeType ID
				model.AttributeTypeId = attributeTypeId;

				var attributeTypeToUpdate = AttributeTypeMap.ToEntity(model);

				var updated = _attributeTypeRepository.Update(attributeTypeToUpdate);
				if (updated)
				{
					attributeType = _attributeTypeRepository.GetByAttributeTypeId(attributeTypeId);
					return AttributeTypeMap.ToModel(attributeType);
				}
			}

			return null;
		}

		public bool DeleteAttributeType(int attributeTypeId)
		{
			if (attributeTypeId < 1)
			{
				throw new Exception("Attribute type ID cannot be less than 1.");
			}

			var attributeType = _attributeTypeRepository.GetByAttributeTypeId(attributeTypeId);
			if (attributeType != null)
			{
				return _attributeTypeRepository.Delete(attributeType);
			}

			return false;
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, AttributeTypeQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.IsPrivate) SetQueryParameter(AttributeTypeColumn.IsPrivate, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.LocaleId) SetQueryParameter(AttributeTypeColumn.LocaleId, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Name) SetQueryParameter(AttributeTypeColumn.Name, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PortalId) SetQueryParameter(AttributeTypeColumn.PortalID, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, AttributeTypeSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.AttributeTypeId) SetSortDirection(AttributeTypeColumn.AttributeTypeId, value, sortBuilder);
					if (key == SortKeys.DisplayOrder) SetSortDirection(AttributeTypeColumn.DisplayOrder, value, sortBuilder);
					if (key == SortKeys.DisplayOrder) SetSortDirection(AttributeTypeColumn.DisplayOrder, value, sortBuilder);
					if (key == SortKeys.Name) SetSortDirection(AttributeTypeColumn.Name, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(AttributeTypeColumn column, string filterOperator, string filterValue, AttributeTypeQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(AttributeTypeColumn column, string value, AttributeTypeSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
