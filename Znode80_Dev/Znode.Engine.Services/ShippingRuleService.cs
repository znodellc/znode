﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ShippingRepository = ZNode.Libraries.DataAccess.Service.ShippingService;
using ShippingRuleRepository = ZNode.Libraries.DataAccess.Service.ShippingRuleService;
using ShippingRuleTypeRepository = ZNode.Libraries.DataAccess.Service.ShippingRuleTypeService;

namespace Znode.Engine.Services
{
	public class ShippingRuleService : BaseService, IShippingRuleService
	{
		private readonly ShippingRepository _shippingRepository;
		private readonly ShippingRuleRepository _shippingRuleRepository;
		private readonly ShippingRuleTypeRepository _shippingRuleTypeRepository;

		public ShippingRuleService()
		{
			_shippingRepository = new ShippingRepository();
			_shippingRuleRepository = new ShippingRuleRepository();
			_shippingRuleTypeRepository = new ShippingRuleTypeRepository();
		}

		public ShippingRuleModel GetShippingRule(int shippingRuleId, NameValueCollection expands)
		{
			var shippingRule = _shippingRuleRepository.GetByShippingRuleID(shippingRuleId);
			if (shippingRule != null)
			{
				GetExpands(expands, shippingRule);
			}

			return ShippingRuleMap.ToModel(shippingRule);
		}

		public ShippingRuleListModel GetShippingRules(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new ShippingRuleListModel();
			var shippingRules = new TList<ShippingRule>();
			var tempList = new TList<ShippingRule>();

			var query = new ShippingRuleQuery();
			var sortBuilder = new ShippingRuleSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			tempList = _shippingRuleRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Now go through the list and get any expands
			foreach (var shippingRule in tempList)
			{
				GetExpands(expands, shippingRule);
				shippingRules.Add(shippingRule);
			}

			// Map each item and add to the list
			foreach (var s in shippingRules)
			{
				model.ShippingRules.Add(ShippingRuleMap.ToModel(s));
			}

			return model;
		}

		public ShippingRuleModel CreateShippingRule(ShippingRuleModel model)
		{
			if (model == null)
			{
				throw new Exception("Shipping rule model cannot be null.");
			}

			var entity = ShippingRuleMap.ToEntity(model);
			var shippingRule = _shippingRuleRepository.Save(entity);
			return ShippingRuleMap.ToModel(shippingRule);
		}

		public ShippingRuleModel UpdateShippingRule(int shippingRuleId, ShippingRuleModel model)
		{
			if (shippingRuleId < 1)
			{
				throw new Exception("Shipping rule ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Shipping rule model cannot be null.");
			}

			var shippingRule = _shippingRuleRepository.GetByShippingRuleID(shippingRuleId);
			if (shippingRule != null)
			{
				// Set the shipping rule ID and map
				model.ShippingRuleId = shippingRuleId;
				var shippingRuleToUpdate = ShippingRuleMap.ToEntity(model);

				var updated = _shippingRuleRepository.Update(shippingRuleToUpdate);
				if (updated)
				{
					shippingRule = _shippingRuleRepository.GetByShippingRuleID(shippingRuleId);
					return ShippingRuleMap.ToModel(shippingRule);
				}
			}

			return null;
		}

		public bool DeleteShippingRule(int shippingRuleId)
		{
			if (shippingRuleId < 1)
			{
				throw new Exception("Shipping rule ID cannot be less than 1.");
			}

			var shippingRule = _shippingRuleRepository.GetByShippingRuleID(shippingRuleId);
			if (shippingRule != null)
			{
				return _shippingRuleRepository.Delete(shippingRule);
			}

			return false;
		}

		private void GetExpands(NameValueCollection expands, ShippingRule shippingRule)
		{
			if (expands.HasKeys())
			{
				ExpandShippingOption(expands, shippingRule);
				ExpandShippingRuleType(expands, shippingRule);
			}
		}

		private void ExpandShippingOption(NameValueCollection expands, ShippingRule shippingRule)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ShippingOption)))
			{
				var shippingOption = _shippingRepository.GetByShippingID(shippingRule.ShippingID);
				if (shippingOption != null)
				{
					shippingRule.ShippingIDSource = shippingOption;
				}
			}
		}

		private void ExpandShippingRuleType(NameValueCollection expands, ShippingRule shippingRule)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ShippingRuleType)))
			{
				var shippingRuleType = _shippingRuleTypeRepository.GetByShippingRuleTypeID(shippingRule.ShippingRuleTypeID);
				if (shippingRuleType != null)
				{
					shippingRule.ShippingRuleTypeIDSource = shippingRuleType;
				}
			}
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, ShippingRuleQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.ExternalId) SetQueryParameter(ShippingRuleColumn.ExternalID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ShippingOptionId) SetQueryParameter(ShippingRuleColumn.ShippingID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ShippingRuleTypeId) SetQueryParameter(ShippingRuleColumn.ShippingRuleTypeID, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, ShippingRuleSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.BaseCost) SetSortDirection(ShippingRuleColumn.BaseCost, value, sortBuilder);
					if (key == SortKeys.PerItemCost) SetSortDirection(ShippingRuleColumn.PerItemCost, value, sortBuilder);
					if (key == SortKeys.ShippingRuleId) SetSortDirection(ShippingRuleColumn.ShippingRuleID, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(ShippingRuleColumn column, string filterOperator, string filterValue, ShippingRuleQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(ShippingRuleColumn column, string value, ShippingRuleSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
