﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IPortalCountryService
	{
		PortalCountryModel GetPortalCountry(int portalCountryId, NameValueCollection expands);
		PortalCountryListModel GetPortalCountries(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		PortalCountryModel CreatePortalCountry(PortalCountryModel model);
		PortalCountryModel UpdatePortalCountry(int portalCountryId, PortalCountryModel model);
		bool DeletePortalCountry(int portalCountryId);
	}
}
