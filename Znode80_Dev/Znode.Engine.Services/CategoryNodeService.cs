﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using CatalogRepository = ZNode.Libraries.DataAccess.Service.CatalogService;
using CategoryRepository = ZNode.Libraries.DataAccess.Service.CategoryService;
using CategoryNodeRepository = ZNode.Libraries.DataAccess.Service.CategoryNodeService;

namespace Znode.Engine.Services
{
	public class CategoryNodeService : BaseService, ICategoryNodeService
	{
		private readonly CatalogRepository _catalogRepository;
		private readonly CategoryRepository _categoryRepository;
		private readonly CategoryNodeRepository _categoryNodeRepository;

		public CategoryNodeService()
		{
			_catalogRepository = new CatalogRepository();
			_categoryRepository = new CategoryRepository();
			_categoryNodeRepository = new CategoryNodeRepository();
		}

		public CategoryNodeModel GetCategoryNode(int categoryNodeId, NameValueCollection expands)
		{
			var categoryNode = _categoryNodeRepository.GetByCategoryNodeID(categoryNodeId);
			if (categoryNode != null)
			{
				GetExpands(expands, categoryNode);
			}

			return CategoryNodeMap.ToModel(categoryNode);
		}

		public CategoryNodeListModel GetCategoryNodes(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new CategoryNodeListModel();
			var categoryNodes = new TList<CategoryNode>();
			var tempList = new TList<CategoryNode>();

			var query = new CategoryNodeQuery();
			var sortBuilder = new CategoryNodeSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			tempList = _categoryNodeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Now go through the list and get any expands
			foreach (var categoryNode in tempList)
			{
				GetExpands(expands, categoryNode);
				categoryNodes.Add(categoryNode);
			}

			// Map each item and add to the list
			foreach (var cn in categoryNodes)
			{
				model.CategoryNodes.Add(CategoryNodeMap.ToModel(cn));
			}

			return model;
		}

	    public CategoryNodeModel CreateCategoryNode(CategoryNodeModel model)
		{
			if (model == null)
			{
				throw new Exception("Category node model cannot be null.");
			}

			var entity = CategoryNodeMap.ToEntity(model);

			var categoryNode = _categoryNodeRepository.Save(entity);
			if (categoryNode != null)
			{
				return CategoryNodeMap.ToModel(categoryNode);
			}

			return null;
		}

		public CategoryNodeModel UpdateCategoryNode(int categoryNodeId, CategoryNodeModel model)
		{
			if (categoryNodeId < 1)
			{
				throw new Exception("Category node ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Category node model cannot be null.");
			}

			var categoryNode = _categoryNodeRepository.GetByCategoryNodeID(categoryNodeId);
			if (categoryNode != null)
			{
				// Set the category node ID
				model.CategoryNodeId = categoryNodeId;

				var categoryNodeToUpdate = CategoryNodeMap.ToEntity(model);

				var updated = _categoryNodeRepository.Update(categoryNodeToUpdate);
				if (updated)
				{
					categoryNode = _categoryNodeRepository.GetByCategoryNodeID(categoryNodeId);
					if (categoryNode != null)
					{
						return CategoryNodeMap.ToModel(categoryNode);
					}
				}
			}

			return null;
		}

		public bool DeleteCategoryNode(int categoryNodeId)
		{
			if (categoryNodeId < 1)
			{
				throw new Exception("Category node ID cannot be less than 1.");
			}

			var categoryNode = _categoryNodeRepository.GetByCategoryNodeID(categoryNodeId);
			if (categoryNode != null)
			{
				return _categoryNodeRepository.Delete(categoryNode);
			}

			return false;
		}

		private void GetExpands(NameValueCollection expands, CategoryNode categoryNode)
		{
			if (expands.HasKeys())
			{
				ExpandCatalog(expands, categoryNode);
				ExpandCategory(expands, categoryNode);
			}
		}

		private void ExpandCatalog(NameValueCollection expands, CategoryNode categoryNode)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Catalog)))
			{
				if (categoryNode.CatalogID.HasValue)
				{
					var catalog = _catalogRepository.GetByCatalogID(categoryNode.CatalogID.Value);
					if (catalog != null)
					{
						categoryNode.CatalogIDSource = catalog;
					}	
				}
			}
		}

		private void ExpandCategory(NameValueCollection expands, CategoryNode categoryNode)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Category)))
			{
				var category = _categoryRepository.GetByCategoryID(categoryNode.CategoryID);
				if (category != null)
				{
					categoryNode.CategoryIDSource = category;
				}
			}
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, CategoryNodeQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.BeginDate) SetQueryParameter(CategoryNodeColumn.BeginDate, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.CatalogId) SetQueryParameter(CategoryNodeColumn.CatalogID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.CategoryId) SetQueryParameter(CategoryNodeColumn.CategoryID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.EndDate) SetQueryParameter(CategoryNodeColumn.EndDate, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsActive) SetQueryParameter(CategoryNodeColumn.ActiveInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ParentCategoryNodeId) SetQueryParameter(CategoryNodeColumn.ParentCategoryNodeID, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, CategoryNodeSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.CatalogId) SetSortDirection(CategoryNodeColumn.CatalogID, value, sortBuilder);
					if (key == SortKeys.CategoryId) SetSortDirection(CategoryNodeColumn.CategoryID, value, sortBuilder);
					if (key == SortKeys.CategoryNodeId) SetSortDirection(CategoryNodeColumn.CategoryNodeID, value, sortBuilder);
					if (key == SortKeys.DisplayOrder) SetSortDirection(CategoryNodeColumn.DisplayOrder, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(CategoryNodeColumn column, string filterOperator, string filterValue, CategoryNodeQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(CategoryNodeColumn column, string value, CategoryNodeSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
