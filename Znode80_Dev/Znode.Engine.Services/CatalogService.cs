﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using CatalogRepository = ZNode.Libraries.DataAccess.Service.CatalogService;

namespace Znode.Engine.Services
{
	public class CatalogService : BaseService, ICatalogService
	{
		private readonly CatalogRepository _catalogRepository;

		public CatalogService()
		{
			_catalogRepository = new CatalogRepository();
		}

		public CatalogModel GetCatalog(int catalogId)
		{
			var catalog = _catalogRepository.GetByCatalogID(catalogId);
			return CatalogMap.ToModel(catalog);
		}

		public CatalogListModel GetCatalogs(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new CatalogListModel();
			var catalogs = new TList<Catalog>();

			var query = new CatalogQuery();
			var sortBuilder = new CatalogSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			catalogs = _catalogRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var c in catalogs)
			{
				model.Catalogs.Add(CatalogMap.ToModel(c));
			}

			return model;
		}

        public CatalogListModel GetCatalogsByCatalogIds(string catalogIds, NameValueCollection expands, NameValueCollection sorts)
        {
            if (String.IsNullOrEmpty(catalogIds))
            {
                throw new Exception("List of catalog IDs cannot be null or empty.");
            }

            var model = new CatalogListModel();
            var catalogs = new TList<Catalog>();
            var tempList = new TList<Catalog>();

            var query = new CatalogQuery { Junction = String.Empty };
            var sortBuilder = new CatalogSortBuilder();

            var list = catalogIds.Split(',');
            foreach (var catalogId in list.Where(catalogId => !String.IsNullOrEmpty(catalogId)))
            {
	            query.BeginGroup("OR");
	            query.AppendEquals(CatalogColumn.CatalogID, catalogId);
	            query.EndGroup();
            }

            SetSorting(sorts, sortBuilder);

            // Get the initial set
            tempList = _catalogRepository.Find(query, sortBuilder);

            // Now go through the list and get any expands
            foreach (var catalog in tempList)
            {
                catalogs.Add(catalog);
            }

            // Map each item and add to the list
            foreach (var c in catalogs)
            {
                model.Catalogs.Add(CatalogMap.ToModel(c));
            }

            return model;
        }

		public CatalogModel CreateCatalog(CatalogModel model)
		{
			if (model == null)
			{
				throw new Exception("Catalog model cannot be null.");
			}

			var entity = CatalogMap.ToEntity(model);
			var catalog = _catalogRepository.Save(entity);
			return CatalogMap.ToModel(catalog);
		}

		public CatalogModel UpdateCatalog(int catalogId, CatalogModel model)
		{
			if (catalogId < 1)
			{
				throw new Exception("Catalog ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Catalog model cannot be null.");
			}

			var catalog = _catalogRepository.GetByCatalogID(catalogId);
			if (catalog != null)
			{
				// Set catalog ID
				model.CatalogId = catalogId;

				var catalogToUpdate = CatalogMap.ToEntity(model);

				var updated = _catalogRepository.Update(catalogToUpdate);
				if (updated)
				{
					catalog = _catalogRepository.GetByCatalogID(catalogId);
					return CatalogMap.ToModel(catalog);
				}
			}

			return null;
		}

		public bool DeleteCatalog(int catalogId)
		{
			if (catalogId < 1)
			{
				throw new Exception("Catalog ID cannot be less than 1.");
			}

			var catalog = _catalogRepository.GetByCatalogID(catalogId);
			if (catalog != null)
			{
				return _catalogRepository.Delete(catalog);
			}

			return false;
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, CatalogQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.ExternalId) SetQueryParameter(CatalogColumn.ExternalID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsActive) SetQueryParameter(CatalogColumn.IsActive, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Name) SetQueryParameter(CatalogColumn.Name, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PortalId) SetQueryParameter(CatalogColumn.PortalID, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, CatalogSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.CatalogId) SetSortDirection(CatalogColumn.CatalogID, value, sortBuilder);
					if (key == SortKeys.Name) SetSortDirection(CatalogColumn.Name, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(CatalogColumn column, string filterOperator, string filterValue, CatalogQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(CatalogColumn column, string value, CatalogSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}

        #region Znode Version 8.0
        /// <summary>
        /// Copies an existing catalog.
        /// </summary>
        /// <param name="model">The model of type CatalogModel.</param>
        /// <returns>Returns true or false.</returns>
        public bool CopyCatalog(CatalogModel model)
        {
            int localeId = 43;
            CatalogHelper catalogHelper = new CatalogHelper();        
            return catalogHelper.CopyCatalog(model.CatalogId, model.Name, localeId);
        }

        /// <summary>      
        /// Deletes an existing catalog.
        /// </summary>
        /// <param name="catalogId">The Id of catalog</param>
        /// <param name="preserveCategories">boolean value for preserveCategories</param>
        /// <returns>Returns true or false.</returns>
        public bool DeleteCatalogUsingCustomService(int catalogId, bool preserveCategories)
        {
            CatalogHelper catalogHelper = new CatalogHelper();          
            return catalogHelper.DeleteCatalog(catalogId, preserveCategories);
        }
        #endregion
	}
}
