﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using HighlightRepository = ZNode.Libraries.DataAccess.Service.HighlightService;
using HighlightTypeRepository = ZNode.Libraries.DataAccess.Service.HighlightTypeService;

namespace Znode.Engine.Services
{
    public class HighlightService : BaseService, IHighlightService
    {
        private readonly HighlightRepository _highlightRepository;
        private readonly HighlightTypeRepository _highlightTypeRepository;

        public HighlightService()
        {
            _highlightRepository = new HighlightRepository();
            _highlightTypeRepository = new HighlightTypeRepository();
        }

        public HighlightModel GetHighlight(int highlightId, NameValueCollection expands)
        {
            var highlight = _highlightRepository.GetByHighlightID(highlightId);
            if (highlight != null)
            {
                GetExpands(expands, highlight);
            }

            return HighlightMap.ToModel(highlight);
        }

        public HighlightListModel GetHighlights(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new HighlightListModel();
            var highlights = new TList<Highlight>();
            var tempList = new TList<Highlight>();

            var query = new HighlightQuery();
            var sortBuilder = new HighlightSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _highlightRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var highlight in tempList)
            {
                GetExpands(expands, highlight);
                highlights.Add(highlight);
            }

            // Map each item and add to the list
            foreach (var h in highlights)
            {
                model.Highlights.Add(HighlightMap.ToModel(h));
            }

            return model;
        }

        public HighlightModel CreateHighlight(HighlightModel model)
        {
            if (model == null)
            {
                throw new Exception("Highlight model cannot be null.");
            }

            var entity = HighlightMap.ToEntity(model);
            var highlight = _highlightRepository.Save(entity);
            return HighlightMap.ToModel(highlight);
        }

        public HighlightModel UpdateHighlight(int highlightId, HighlightModel model)
        {
            if (highlightId < 1)
            {
                throw new Exception("Highlight ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Highlight model cannot be null.");
            }

            var highlight = _highlightRepository.GetByHighlightID(highlightId);
            if (highlight != null)
            {
                // Set highlight ID
                model.HighlightId = highlightId;

                var highlightToUpdate = HighlightMap.ToEntity(model);

                var updated = _highlightRepository.Update(highlightToUpdate);
                if (updated)
                {
                    highlight = _highlightRepository.GetByHighlightID(highlightId);
                    return HighlightMap.ToModel(highlight);
                }
            }

            return null;
        }

        public bool DeleteHighlight(int highlightId)
        {
            if (highlightId < 1)
            {
                throw new Exception("Highlight ID cannot be less than 1.");
            }

            var highlight = _highlightRepository.GetByHighlightID(highlightId);
            if (highlight != null)
            {
                return _highlightRepository.Delete(highlight);
            }

            return false;
        }

        private void GetExpands(NameValueCollection expands, Highlight highlight)
        {
            if (expands.HasKeys())
            {
                ExpandHighlightType(expands, highlight);
            }
        }

        public HighlightModel CheckAssociateProduct(int highlightId)
        {
            HighlightModel model = new HighlightModel();
            HighlightHelper highlightHelper = new HighlightHelper();
            DataSet ds = highlightHelper.GetAssociatedProductByHighlightId(highlightId);

            if (ds != null && ds.Tables != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                model.IsAssociatedProduct = true;
            }
            else
            {
                model.IsAssociatedProduct = false;
            }

            return model;
        }

        private void ExpandHighlightType(NameValueCollection expands, Highlight highlight)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.HighlightType)))
            {
                if (highlight.HighlightTypeID.HasValue)
                {
                    var highlightType = _highlightTypeRepository.GetByHighlightTypeID(highlight.HighlightTypeID.Value);
                    if (highlightType != null)
                    {
                        highlight.HighlightTypeIDSource = highlightType;
                    }
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, HighlightQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.HighlightTypeId) SetQueryParameter(HighlightColumn.HighlightTypeID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsActive) SetQueryParameter(HighlightColumn.ActiveInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.LocaleId) SetQueryParameter(HighlightColumn.LocaleId, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Name) SetQueryParameter(HighlightColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PortalId) SetQueryParameter(HighlightColumn.PortalID, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, HighlightSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.DisplayOrder) SetSortDirection(HighlightColumn.DisplayOrder, value, sortBuilder);
                    if (key == SortKeys.HighlightId) SetSortDirection(HighlightColumn.HighlightID, value, sortBuilder);
                    if (key == SortKeys.Name) SetSortDirection(HighlightColumn.Name, value, sortBuilder);
                }
            }
        }

        private void SetQueryParameter(HighlightColumn column, string filterOperator, string filterValue, HighlightQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(HighlightColumn column, string value, HighlightSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }
    }
}
