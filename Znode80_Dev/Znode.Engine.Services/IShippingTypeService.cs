﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IShippingTypeService
	{
		ShippingTypeModel GetShippingType(int shippingTypeId);
		ShippingTypeListModel GetShippingTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		ShippingTypeModel CreateShippingType(ShippingTypeModel model);
		ShippingTypeModel UpdateShippingType(int shippingTypeId, ShippingTypeModel model);
		bool DeleteShippingType(int shippingTypeId);
	}
}
