﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IThemeService
    {
        ThemeListModel GetThemes(List<Tuple<string, string, string>> filters, NameValueCollection sorts);
    }
}
