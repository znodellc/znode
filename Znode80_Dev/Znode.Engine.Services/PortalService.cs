﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Custom;
using System.Linq;
using ZNode.Libraries.Admin;
using CatalogRepository = ZNode.Libraries.DataAccess.Service.CatalogService;
using PortalCatalogRepository = ZNode.Libraries.DataAccess.Service.PortalCatalogService;
using PortalCountryRepository = ZNode.Libraries.DataAccess.Service.PortalCountryService;
using PortalRepository = ZNode.Libraries.DataAccess.Service.PortalService;
using System.Data;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Services
{
    public class PortalService : BaseService, IPortalService
    {
        private readonly CatalogRepository _catalogRepository;
        private readonly PortalCatalogRepository _portalCatalogRepositroy;
        private readonly PortalCountryRepository _portalCountryRepository;
        private readonly PortalRepository _portalRepository;

        public PortalService()
        {
            _catalogRepository = new CatalogRepository();
            _portalCatalogRepositroy = new PortalCatalogRepository();
            _portalCountryRepository = new PortalCountryRepository();
            _portalRepository = new PortalRepository();
        }

        public PortalModel GetPortal(int portalId, NameValueCollection expands)
        {
            var portal = _portalRepository.GetByPortalID(portalId);
            if (portal != null)
            {
                GetExpands(expands, portal);
            }

            return PortalMap.ToModel(portal);
        }

        public Portal GetPortalById(int portalId)
        {
            return _portalRepository.GetByPortalID(portalId);
        }

        public PortalListModel GetPortals(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new PortalListModel();
            var portals = new TList<Portal>();
            var tempList = new TList<Portal>();

            var query = new PortalQuery();
            var sortBuilder = new PortalSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _portalRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var portal in tempList)
            {
                GetExpands(expands, portal);
                portals.Add(portal);
            }

            // Map each item and add to the list
            foreach (var p in portals)
            {
                model.Portals.Add(PortalMap.ToModel(p));
            }

            return model;
        }

        public PortalListModel GetPortalsByPortalIds(string portalIds, NameValueCollection expands, NameValueCollection sorts)
        {
            if (String.IsNullOrEmpty(portalIds))
            {
                throw new Exception("List of portal IDs cannot be null or empty.");
            }

            var model = new PortalListModel();
            var portals = new TList<Portal>();
            var tempList = new TList<Portal>();

            var query = new PortalQuery { Junction = String.Empty };
            var sortBuilder = new PortalSortBuilder();

            var list = portalIds.Split(',');
            foreach (var portalId in list.Where(portalId => !String.IsNullOrEmpty(portalId)))
            {
                query.BeginGroup("OR");
                query.AppendEquals(PortalColumn.PortalID, portalId);
                query.EndGroup();
            }

            SetSorting(sorts, sortBuilder);

            // Get the initial set
            tempList = _portalRepository.Find(query, sortBuilder);

            // Now go through the list and get any expands
            foreach (var portal in tempList)
            {
                GetExpands(expands, portal);
                portals.Add(portal);
            }

            // Map each item and add to the list
            foreach (var p in portals)
            {
                model.Portals.Add(PortalMap.ToModel(p));
            }

            return model;
        }

        public PortalModel CreatePortal(PortalModel model)
        {
            if (model == null)
            {
                throw new Exception("Portal model cannot be null.");
            }

            ////Znode Version 8.0:Starts
            //ZNodeEnvironmentConfig zNodeEnvironmentConfig=new ZNodeEnvironmentConfig();
            //model.LogoPath = zNodeEnvironmentConfig.OriginalImagePath + "Turnkey/" + model.PortalId + "/" + model.LogoPath;
            ////Znode Version 8.0:Ends

            var entity = PortalMap.ToEntity(model);
            var portal = _portalRepository.Save(entity);
            return PortalMap.ToModel(portal);
        }

        public PortalModel UpdatePortal(int portalId, PortalModel model)
        {
            if (portalId < 1)
            {
                throw new Exception("Portal ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Portal model cannot be null.");
            }

            var portal = _portalRepository.GetByPortalID(portalId);
            if (portal != null)
            {
                // Set portal ID
                model.PortalId = portalId;

                var portalToUpdate = PortalMap.ToEntity(model);

                var updated = _portalRepository.Update(portalToUpdate);
                if (updated)
                {
                    portal = _portalRepository.GetByPortalID(portalId);
                    return PortalMap.ToModel(portal);
                }
            }

            return null;
        }

        public bool DeletePortal(int portalId)
        {
            if (portalId < 1)
            {
                throw new Exception("Portal ID cannot be less than 1.");
            }

            var portal = _portalRepository.GetByPortalID(portalId);
            if (portal != null)
            {
                return _portalRepository.Delete(portal);
            }

            return false;
        }

        

        private void GetExpands(NameValueCollection expands, Portal portal)
        {
            if (expands.HasKeys())
            {
                ExpandPortalCountries(expands, portal);
                ExpandPortalCatalogs(expands, portal);
                ExpandCatalogs(expands, portal);
            }
        }

        private void ExpandPortalCountries(NameValueCollection expands, Portal portal)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.PortalCountries)))
            {
                var portalCountries = _portalCountryRepository.GetByPortalID(portal.PortalID);
                foreach (var pc in portalCountries)
                {
                    portal.PortalCountryCollection.Add(pc);
                }
            }
        }

        private void ExpandCatalogs(NameValueCollection expands, Portal portal)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Catalogs)))
            {
                var portalCatalogs = _portalCatalogRepositroy.GetByPortalID(portal.PortalID);

                var portalCatalog = portalCatalogs.Single(x => x.LocaleID == 43);

                if (portalCatalog != null)
                {
                    var catalog = _catalogRepository.GetByCatalogID(portalCatalog.CatalogID);
                    portal.CatalogCollection.Add(catalog);
                }
            }
        }

        private void ExpandPortalCatalogs(NameValueCollection expands, Portal portal)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.PortalCatalogs)))
            {
                var portalCatalogs = _portalCatalogRepositroy.GetByPortalID(portal.PortalID);

                foreach (var pc in portalCatalogs)
                {
                    portal.PortalCatalogCollection.Add(pc);
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, PortalQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.CompanyName) SetQueryParameter(PortalColumn.CompanyName, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ExternalId) SetQueryParameter(PortalColumn.ExternalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsActive) SetQueryParameter(PortalColumn.ActiveInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.LocaleId) SetQueryParameter(PortalColumn.LocaleID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.StoreName) SetQueryParameter(PortalColumn.StoreName, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, PortalSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.CompanyName) SetSortDirection(PortalColumn.CompanyName, value, sortBuilder);
                    if (key == SortKeys.PortalId) SetSortDirection(PortalColumn.PortalID, value, sortBuilder);
                    if (key == SortKeys.StoreName) SetSortDirection(PortalColumn.StoreName, value, sortBuilder);
                }
            }
        }

        private void SetQueryParameter(PortalColumn column, string filterOperator, string filterValue, PortalQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(PortalColumn column, string value, PortalSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        #region Znode Version 8.0
        
        public DataSet GetFedexKey()
        {
            StoreSettingsHelper storeSettingHelper = new StoreSettingsHelper();
            DataSet ds = storeSettingHelper.GetFedExKey();
            return ds;
        }

        public bool CreateMessages(int portalId, int localeId)
        {
            try
            {
                if (portalId < 1 || localeId < 1)
                {
                    throw new Exception("Portal ID or Locale ID cannot be less than 1.");
                }

                StoreSettingsAdmin storeSettingAdmin = new StoreSettingsAdmin();

                storeSettingAdmin.CreateMessage(portalId.ToString(), localeId.ToString());
                return true;
            }
            catch (Exception)
            {
                return false;
            }

        }

        public bool CopyStore(int portalId)
        {
            StoreSettingsAdmin storeSettingAdmin = new StoreSettingsAdmin();
            return storeSettingAdmin.CopyStoreByPortalID(portalId);
        }

        public bool DeletePortalByPortalId(int portalId)
        {
            if (portalId < 1)
            {
                throw new Exception("Portal ID cannot be less than 1.");
            }

            StoreSettingsHelper storeHelper = new StoreSettingsHelper();
            return storeHelper.DeleteByPortalId(portalId);            
        }
        #endregion
    }
}
