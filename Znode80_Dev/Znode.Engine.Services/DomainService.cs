﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using DomainRepository = ZNode.Libraries.DataAccess.Service.DomainService;

namespace Znode.Engine.Services
{
	public class DomainService : BaseService, IDomainService
	{
		private readonly DomainRepository _domainRepository;

		public DomainService()
		{
			_domainRepository = new DomainRepository();
		}

		public DomainModel GetDomain(int domainId)
		{
			var domain = _domainRepository.GetByDomainID(domainId);
			return DomainMap.ToModel(domain);
		}

		public DomainListModel GetDomains(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new DomainListModel();
			var domains = new TList<Domain>();

			var query = new DomainQuery();
			var sortBuilder = new DomainSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			domains = _domainRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var d in domains)
			{
				model.Domains.Add(DomainMap.ToModel(d));
			}

			return model;
		}

		public DomainModel CreateDomain(DomainModel model)
		{
			if (model == null)
			{
				throw new Exception("Domain model cannot be null.");
			}

			var entity = DomainMap.ToEntity(model);
			var domain = _domainRepository.Save(entity);
			return DomainMap.ToModel(domain);
		}

		public DomainModel UpdateDomain(int domainId, DomainModel model)
		{
			if (domainId < 1)
			{
				throw new Exception("Domain ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Domain model cannot be null.");
			}

			var domain = _domainRepository.GetByDomainID(domainId);
			if (domain != null)
			{
				// Set domain ID
				model.DomainId = domainId;

				var domainToUpdate = DomainMap.ToEntity(model);

				var updated = _domainRepository.Update(domainToUpdate);
				if (updated)
				{
					domain = _domainRepository.GetByDomainID(domainId);
					return DomainMap.ToModel(domain);
				}
			}

			return null;
		}

		public bool DeleteDomain(int domainId)
		{
			if (domainId < 1)
			{
				throw new Exception("Domain ID cannot be less than 1.");
			}

			var domain = _domainRepository.GetByDomainID(domainId);
			if (domain != null)
			{
				return _domainRepository.Delete(domain);
			}

			return false;
		}

		public Domain GetDomain(string domainName)
		{
			return _domainRepository.GetByDomainName(domainName.ToLower());
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, DomainQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.ApiKey) SetQueryParameter(DomainColumn.ApiKey, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.DomainName) SetQueryParameter(DomainColumn.DomainName, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsActive) SetQueryParameter(DomainColumn.IsActive, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PortalId) SetQueryParameter(DomainColumn.PortalID, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, DomainSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.DomainId) SetSortDirection(DomainColumn.DomainID, value, sortBuilder);
					if (key == SortKeys.DomainName) SetSortDirection(DomainColumn.DomainName, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(DomainColumn column, string filterOperator, string filterValue, DomainQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(DomainColumn column, string value, DomainSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
