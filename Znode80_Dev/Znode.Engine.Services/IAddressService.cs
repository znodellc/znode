﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IAddressService
	{
		AddressModel GetAddress(int addressId);
		AddressListModel GetAddresses(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		AddressModel CreateAddress(AddressModel model);
		AddressModel UpdateAddress(int addressId, AddressModel model);
		bool DeleteAddress(int addressId);
		bool AddDefaultAddresses(int accountId);
	}
}