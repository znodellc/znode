﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ProductTypeRepository = ZNode.Libraries.DataAccess.Service.ProductTypeService;

namespace Znode.Engine.Services
{
    public class ProductTypeService : BaseService, IProductTypeService
    {

        #region Private Variable
        private readonly ProductTypeRepository _productTypeRepository; 
        #endregion

        #region Public Constructor
        public ProductTypeService()
        {
            _productTypeRepository = new ProductTypeRepository();
        } 
        #endregion

        #region Public Methods
        public ProductTypeModel GetProductType(int productTypeId)
        {
            var ProductType = _productTypeRepository.GetByProductTypeId(productTypeId);
            return ProductTypeMap.ToModel(ProductType);
        }

        public ProductTypeListModel GetProductTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProductTypeListModel();
            var productTypes = new TList<ProductType>();

            var query = new ProductTypeQuery();
            var sort = new ProductTypeSortBuilder();

            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sort);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            productTypes = _productTypeRepository.Find(query, sort, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Map each item and add to the list
            foreach (var m in productTypes)
            {
                model.ProductTypes.Add(ProductTypeMap.ToModel(m));
            }

            return model;
        }

        public ProductTypeModel CreateProductType(ProductTypeModel model)
        {
            if (Equals(model , null))
            {
                throw new Exception("Product Type model cannot be null.");
            }

            var entity = ProductTypeMap.ToEntity(model);
            var productType = _productTypeRepository.Save(entity);
            return ProductTypeMap.ToModel(productType);
        }

        public ProductTypeModel UpdateProductType(int productTypeId, ProductTypeModel model)
        {
            if (productTypeId < 1)
            {
                throw new Exception("ProductType ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("ProductType model cannot be null.");
            }

            var productType = _productTypeRepository.GetByProductTypeId(productTypeId);
            if (!Equals(productType , null))
            {
                // Set manufacturer ID
                model.ProductTypeId = productTypeId;

                var productTypeToUpdate = ProductTypeMap.ToEntity(model);

                if (_productTypeRepository.Update(productTypeToUpdate))
                {
                    productType = _productTypeRepository.GetByProductTypeId(productTypeId);
                    return ProductTypeMap.ToModel(productType);
                }
            }

            return null;
        }

        public bool DeleteProductType(int productTypeId)
        {
            if (productTypeId < 1)
            {
                throw new Exception("productType ID cannot be less than 1.");
            }

            var productType = _productTypeRepository.GetByProductTypeId(productTypeId);
            if (!Equals(productType , null))
            {
                return _productTypeRepository.Delete(productType);
            }

            return false;
        }

        public AttributeTypeValueListModel GetProductAttributesByProductTypeId(int productTypeId)
        {

            var model = new AttributeTypeValueListModel();

            var key = StoredProcKeys.ProductTypeId;
            var value = productTypeId;

            Dictionary<string,int> StoredProc = new Dictionary<string, int>();
            StoredProc.Add(key, value);

            //Concate key and value and return to whereClause. 
            string whereClause = BuildWhereClause(StoredProc);

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet dataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, StoredProcKeys.SpProductTypeWiseAttributes);
            model = ProductTypeMap.ToModelList(dataSet);
            return model;
        }

        private string BuildWhereClause(Dictionary<string, int> StoredProc)
        {
            string WhereClause = string.Empty;
            int count = 0;

            foreach (KeyValuePair<string, int> pair in StoredProc)
            {
                WhereClause += StoredProcKeys.TildOperator + pair.Key + StoredProcKeys.TildEqualToOperator + pair.Value;
                if (count < StoredProc.Count - 1)
                {
                    WhereClause = WhereClause + " " + StoredProcKeys.AND + " ";
                }
                count++;
            }
            return WhereClause;
        }
        #endregion

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, ProductTypeQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.Name) SetQueryParameter(ProductTypeColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PortalId) SetQueryParameter(ProductTypeColumn.PortalID, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, ProductTypeSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.DisplayOrder) SetSortDirection(ProductTypeColumn.DisplayOrder, value, sortBuilder);
                    if (key == SortKeys.ProductTypeId) SetSortDirection(ProductTypeColumn.ProductTypeId, value, sortBuilder);
                    if (key == SortKeys.Name) SetSortDirection(ProductTypeColumn.Name, value, sortBuilder);
                }
            }
        }

        private void SetQueryParameter(ProductTypeColumn column, string filterOperator, string filterValue, ProductTypeQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(ProductTypeColumn column, string value, ProductTypeSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        } 
        #endregion
        
    }
}
