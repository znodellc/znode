﻿using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface ISearchService
	{
		KeywordSearchModel GetKeywordSearch(KeywordSearchModel model, NameValueCollection expands, NameValueCollection sorts, NameValueCollection page);
		SuggestedSearchListModel GetSearchSuggestions(SuggestedSearchModel model);
		void ReloadIndex();
	}
}