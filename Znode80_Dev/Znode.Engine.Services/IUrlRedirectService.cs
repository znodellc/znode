﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IUrlRedirectService
    {
        UrlRedirectModel GetUrlRedirect(int urlRedirectId);
        UrlRedirectListModel GetUrlRedirects(List<Tuple<string, string, string>> filters,NameValueCollection page);
        UrlRedirectModel CreateUrlRedirect(UrlRedirectModel model);
        UrlRedirectModel UpdateUrlRedirect(int urlRedirectId, UrlRedirectModel model);
        bool DeleteUrlRedirect(int urlRedirectId);
    }
}
