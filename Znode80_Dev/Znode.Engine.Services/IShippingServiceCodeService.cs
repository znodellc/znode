﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IShippingServiceCodeService
	{
		ShippingServiceCodeModel GetShippingServiceCode(int shippingServiceCodeId, NameValueCollection expands);
		ShippingServiceCodeListModel GetShippingServiceCodes(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		ShippingServiceCodeModel CreateShippingServiceCode(ShippingServiceCodeModel model);
		ShippingServiceCodeModel UpdateShippingServiceCode(int shippingServiceCodeId, ShippingServiceCodeModel model);
		bool DeleteShippingServiceCode(int shippingServiceCodeId);
	}
}
