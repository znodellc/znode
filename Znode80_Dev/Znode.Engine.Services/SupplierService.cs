﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using SupplierRepository = ZNode.Libraries.DataAccess.Service.SupplierService;
using SupplierTypeRepository = ZNode.Libraries.DataAccess.Service.SupplierTypeService;

namespace Znode.Engine.Services
{
	public class SupplierService : BaseService, ISupplierService
	{
		private readonly SupplierRepository _supplierRepository;
		private readonly SupplierTypeRepository _supplierTypeRepository;

		public SupplierService()
		{
			_supplierRepository = new SupplierRepository();
			_supplierTypeRepository = new SupplierTypeRepository();
		}

		public SupplierModel GetSupplier(int supplierId, NameValueCollection expands)
		{
			var supplier = _supplierRepository.GetBySupplierID(supplierId);
			if (supplier != null)
			{
				GetExpands(expands, supplier);
			}

			return SupplierMap.ToModel(supplier);
		}

		public SupplierListModel GetSuppliers(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new SupplierListModel();
			var suppliers = new TList<Supplier>();
			var tempList = new TList<Supplier>();

			var query = new SupplierQuery();
			var sortBuilder = new SupplierSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			tempList = _supplierRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Now go through the list and get any expands
			foreach (var supplier in tempList)
			{
				GetExpands(expands, supplier);
				suppliers.Add(supplier);
			}

			// Map each item and add to the list
			foreach (var s in suppliers)
			{
				model.Suppliers.Add(SupplierMap.ToModel(s));
			}

			return model;
		}

		public SupplierModel CreateSupplier(SupplierModel model)
		{
			if (model == null)
			{
				throw new Exception("Supplier model cannot be null.");
			}

			var entity = SupplierMap.ToEntity(model);
			var supplier = _supplierRepository.Save(entity);
			return SupplierMap.ToModel(supplier);
		}

		public SupplierModel UpdateSupplier(int supplierId, SupplierModel model)
		{
			if (supplierId < 1)
			{
				throw new Exception("Supplier ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Supplier model cannot be null.");
			}

			var supplier = _supplierRepository.GetBySupplierID(supplierId);
			if (supplier != null)
			{
				// Set the supplier ID and map
				model.SupplierId = supplierId;
				var supplierToUpdate = SupplierMap.ToEntity(model);

				var updated = _supplierRepository.Update(supplierToUpdate);
				if (updated)
				{
					supplier = _supplierRepository.GetBySupplierID(supplierId);
					return SupplierMap.ToModel(supplier);
				}
			}

			return null;
		}

		public bool DeleteSupplier(int supplierId)
		{
			if (supplierId < 1)
			{
				throw new Exception("Supplier ID cannot be less than 1.");
			}

			var supplier = _supplierRepository.GetBySupplierID(supplierId);
			if (supplier != null)
			{
				return _supplierRepository.Delete(supplier);
			}

			return false;
		}

		private void GetExpands(NameValueCollection expands, Supplier supplier)
		{
			if (expands.HasKeys())
			{
				ExpandSupplierType(expands, supplier);
			}
		}

		private void ExpandSupplierType(NameValueCollection expands, Supplier supplier)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.SupplierType)))
			{
				if (supplier.SupplierTypeID.HasValue)
				{
					var supplierType = _supplierTypeRepository.GetBySupplierTypeID(supplier.SupplierTypeID.Value);
					if (supplierType != null)
					{
						supplier.SupplierTypeIDSource = supplierType;
					}
				}
			}
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, SupplierQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.ExternalId) SetQueryParameter(SupplierColumn.ExternalID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ExternalSupplierNumber) SetQueryParameter(SupplierColumn.ExternalSupplierNo, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsActive) SetQueryParameter(SupplierColumn.ActiveInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Name) SetQueryParameter(SupplierColumn.Name, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PortalId) SetQueryParameter(SupplierColumn.PortalID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.SupplierTypeId) SetQueryParameter(SupplierColumn.SupplierTypeID, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, SupplierSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.DisplayOrder) SetSortDirection(SupplierColumn.DisplayOrder, value, sortBuilder);
					if (key == SortKeys.Name) SetSortDirection(SupplierColumn.Name, value, sortBuilder);
					if (key == SortKeys.SupplierId) SetSortDirection(SupplierColumn.SupplierID, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(SupplierColumn column, string filterOperator, string filterValue, SupplierQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(SupplierColumn column, string value, SupplierSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
