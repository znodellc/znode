﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using StateRepository = ZNode.Libraries.DataAccess.Service.StateService;

namespace Znode.Engine.Services
{
	public class StateService : BaseService, IStateService
	{
		private readonly StateRepository _stateRepository;

		public StateService()
		{
			_stateRepository = new StateRepository();
		}

		public StateModel GetState(string stateCode)
		{
			var state = _stateRepository.GetByCode(stateCode);
			return StateMap.ToModel(state);
		}

		public StateListModel GetStates(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new StateListModel();
			var states = new TList<State>();

			var query = new StateQuery();
			var sortBuilder = new StateSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			states = _stateRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var s in states)
			{
				model.States.Add(StateMap.ToModel(s));
			}

			return model;
		}

		public StateModel CreateState(StateModel model)
		{
			if (model == null)
			{
				throw new Exception("State model cannot be null.");
			}

			var entity = StateMap.ToEntity(model);
			var state = _stateRepository.Save(entity);
			return StateMap.ToModel(state);
		}

		public StateModel UpdateState(string stateCode, StateModel model)
		{
			if (String.IsNullOrEmpty(stateCode))
			{
				throw new Exception("State code cannot be null or empty.");
			}

			if (model == null)
			{
				throw new Exception("State model cannot be null.");
			}

			var state = _stateRepository.GetByCode(stateCode);
			if (state != null)
			{
				// Gotta set the original state code for the update to work
				var stateToUpdate = StateMap.ToEntity(model);
				stateToUpdate.OriginalCode = stateCode;

				var updated = _stateRepository.Update(stateToUpdate);
				if (updated)
				{
					state = _stateRepository.GetByCode(stateCode);
					return StateMap.ToModel(state);
				}
			}

			return null;
		}

		public bool DeleteState(string stateCode)
		{
			if (String.IsNullOrEmpty(stateCode))
			{
				throw new Exception("State code cannot be null or empty.");
			}

			var state = _stateRepository.GetByCode(stateCode);
			if (state != null)
			{
				return _stateRepository.Delete(state);
			}

			return false;
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, StateQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.Code) SetQueryParameter(StateColumn.Code, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.CountryCode) SetQueryParameter(StateColumn.CountryCode, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Name) SetQueryParameter(StateColumn.Name, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, StateSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.Code) SetSortDirection(StateColumn.Code, value, sortBuilder);
					if (key == SortKeys.Name) SetSortDirection(StateColumn.Name, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(StateColumn column, string filterOperator, string filterValue, StateQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(StateColumn column, string value, StateSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
