﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IReorderService
    {
        /// <summary>
        /// Znode Version 7.2.2
        /// Get the reorder Cart Items list. on the basis of orderId.
        /// Function helps to place complite order as reorder.
        /// </summary>
        /// <param name="orderId">orderId</param>
        /// <param name="expands">returnsCartItemsListModel as response </param>
        /// <returns>returns CartItemsListModel as response</returns>
        CartItemsListModel GetReorderItems(int orderId, NameValueCollection expands);

        /// <summary>
        /// Znode Version 7.2.2
        /// Get the reorder Cart Item. on the basis of orderLineItemId.
        /// Function helps to place reorder of order items.
        /// </summary>
        /// <param name="orderLineItemId">orderLineItemId</param>
        /// <param name="expands"></param>
        /// <returns>returns CartItemsModel as response</returns>
        CartItemsModel GetReorderSingleItem(int orderLineItemId, NameValueCollection expands);

        /// <summary>
        /// get product addons on the basis of product id
        /// </summary>
        /// <param name="productId">int prosuctId</param>
        /// <returns>returns set of Addons ids</returns>
        string GetProductAddons(string addonSku);

        //TODO
        CartItemsListModel GetReorderItemsList(int orderId, int orderLineItemId, bool isOrder, NameValueCollection expands);


    }
}
