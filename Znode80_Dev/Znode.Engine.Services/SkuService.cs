﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using InventoryRepository = ZNode.Libraries.DataAccess.Service.SKUInventoryService;
using ProductAttributeRepository = ZNode.Libraries.DataAccess.Service.ProductAttributeService;
using SkuRepository = ZNode.Libraries.DataAccess.Service.SKUService;
using SkuAttributeRepository = ZNode.Libraries.DataAccess.Service.SKUAttributeService;
using SupplierRepository = ZNode.Libraries.DataAccess.Service.SupplierService;
using ZNode.Libraries.DataAccess.Custom;

namespace Znode.Engine.Services
{
    public class SkuService : BaseService, ISkuService
    {
        private readonly InventoryRepository _inventoryRepository;
        private readonly ProductAttributeRepository _productAttributeRepository;
        private readonly SkuRepository _skuRepository;
        private readonly SkuAttributeRepository _skuAttributeRepository;
        private readonly SupplierRepository _supplierRepository;

        public SkuService()
        {
            _inventoryRepository = new InventoryRepository();
            _productAttributeRepository = new ProductAttributeRepository();
            _skuRepository = new SkuRepository();
            _skuAttributeRepository = new SkuAttributeRepository();
            _supplierRepository = new SupplierRepository();
        }

        public SkuModel GetSku(int skuId, NameValueCollection expands)
        {
            var sku = _skuRepository.GetBySKUID(skuId);
            if (sku != null)
            {
                GetExpands(expands, sku);
            }

            return SkuMap.ToModel(sku);
        }

        #region Znode Version 7.2.2

        public TList<SKU> GetSkuList(string strSku, NameValueCollection expands)
        {
            var skus = new TList<SKU>();
            skus = _skuRepository.GetBySKU(strSku);
            return skus;
        }
        #endregion

        public SkuListModel GetSkus(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new SkuListModel();
            var skus = new TList<SKU>();
            var tempList = new TList<SKU>();

            var query = new SKUQuery();
            var sortBuilder = new SKUSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _skuRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var sku in tempList)
            {
                GetExpands(expands, sku);
                skus.Add(sku);
            }

            // Map each item and add to the list
            foreach (var s in skus)
            {
                model.Skus.Add(SkuMap.ToModel(s));
            }

            return model;
        }

        public SkuModel CreateSku(SkuModel model)
        {
            if (model == null)
            {
                throw new Exception("SKU model cannot be null.");
            }

            if (model.Inventory == null)
            {
                throw new Exception("SKU inventory model cannot be null.");
            }

            // First save the inventory
            var inventoryEntity = InventoryMap.ToEntity(model.Inventory);
            var inventory = _inventoryRepository.Save(inventoryEntity);

            if (inventory != null)
            {
                // Now save the sku itself
                var skuEntity = SkuMap.ToEntity(model);
                var sku = _skuRepository.Save(skuEntity);
                return SkuMap.ToModel(sku);
            }

            return null;
        }

        public SkuModel UpdateSku(int skuId, SkuModel model)
        {
            if (skuId < 1)
            {
                throw new Exception("SKU ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("SKU model cannot be null.");
            }

            var sku = _skuRepository.GetBySKUID(skuId);
            if (sku != null)
            {
                // Set the sku ID and map
                model.SkuId = skuId;
                var skuToUpdate = SkuMap.ToEntity(model);

                var updated = _skuRepository.Update(skuToUpdate);
                if (updated)
                {
                    sku = _skuRepository.GetBySKUID(skuId);
                    //Znode Version 8.0 
                    //Added code block for updating inventory. - Start
                    var inventory = _inventoryRepository.GetBySKU(sku.SKU);
                    model.Inventory.InventoryId = inventory.SKUInventoryID;
                    var inventoryToUpdate = InventoryMap.ToEntity(model.Inventory);
                    var inventoryUpdated = _inventoryRepository.Update(inventoryToUpdate);
                    //Added code block for updating inventory. - End
                    return SkuMap.ToModel(sku);
                }
            }

            return null;
        }

        public bool DeleteSku(int skuId)
        {
            if (skuId < 1)
            {
                throw new Exception("SKU ID cannot be less than 1.");
            }

            var sku = _skuRepository.GetBySKUID(skuId);
            if (sku != null)
            {
                return _skuRepository.Delete(sku);
            }

            return false;
        }

        private void GetExpands(NameValueCollection expands, SKU sku)
        {
            if (expands.HasKeys())
            {
                ExpandAttributes(expands, sku);
                ExpandInventory(expands, sku);
                ExpandSupplier(expands, sku);
            }
        }

        private void ExpandAttributes(NameValueCollection expands, SKU sku)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Attributes)))
            {
                var skuAttribs = _skuAttributeRepository.GetBySKUID(sku.SKUID);
                foreach (var sa in skuAttribs)
                {
                    var prodAttrib = _productAttributeRepository.GetByAttributeId(sa.AttributeId);
                    var item = new SKUAttribute
                    {
                        AttributeId = sa.AttributeId,
                        AttributeIdSource = prodAttrib,
                        SKUAttributeID = sa.SKUAttributeID,
                        SKUID = sku.SKUID,
                        SKUIDSource = sku
                    };

                    sku.SKUAttributeCollection.Add(item);
                }
            }
        }

        private void ExpandInventory(NameValueCollection expands, SKU sku)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Inventory)))
            {
                var inventory = _inventoryRepository.GetBySKU(sku.SKU);
                if (inventory != null)
                {
                    sku.Inventory = inventory;
                }
            }
        }

        private void ExpandSupplier(NameValueCollection expands, SKU sku)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Supplier)))
            {
                if (sku.SupplierID.HasValue)
                {
                    var supplier = _supplierRepository.GetBySupplierID(sku.SupplierID.Value);
                    if (supplier != null)
                    {
                        sku.SupplierIDSource = supplier;
                    }
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, SKUQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.ExternalId) SetQueryParameter(SKUColumn.ExternalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsActive) SetQueryParameter(SKUColumn.ActiveInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ProductId) SetQueryParameter(SKUColumn.ProductID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Sku) SetQueryParameter(SKUColumn.SKU, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.SupplierId) SetQueryParameter(SKUColumn.SupplierID, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, SKUSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.DisplayOrder) SetSortDirection(SKUColumn.DisplayOrder, value, sortBuilder);
                    if (key == SortKeys.ProductId) SetSortDirection(SKUColumn.ProductID, value, sortBuilder);
                    if (key == SortKeys.Sku) SetSortDirection(SKUColumn.SKU, value, sortBuilder);
                    if (key == SortKeys.SkuId) SetSortDirection(SKUColumn.SKUID, value, sortBuilder);
                }
            }
        }

        private void SetQueryParameter(SKUColumn column, string filterOperator, string filterValue, SKUQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(SKUColumn column, string value, SKUSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        /// <summary>
        /// Znode Version 8.0
        /// To Save Sku Attribute in ZnodeSkuAttribute
        /// </summary>
        /// <param name="skuId">int skuId</param>
        /// <param name="attributeIds">string comma separated attributeId</param>
        /// <returns>return true/false</returns>
        public bool AddSkuAttribute(int skuId, string attributeIds)
        {
            if (skuId < 1)
            {
                throw new Exception("SKU ID cannot be less than 1.");
            }
            var skuHelper = new SKUHelper();
            var status = skuHelper.AddSkuAttribute(skuId, attributeIds);
            return status;
        }

    }
}
