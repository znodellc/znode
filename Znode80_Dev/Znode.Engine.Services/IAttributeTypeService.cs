﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IAttributeTypeService
	{
		AttributeTypeModel GetAttributeType(int attributeTypeId);
		AttributeTypeListModel GetAttributeTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		AttributeTypeModel CreateAttributeType(AttributeTypeModel model);
		AttributeTypeModel UpdateAttributeType(int attributeTypeId, AttributeTypeModel model);
		bool DeleteAttributeType(int attributeTypeId);
	}
}
