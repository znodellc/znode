﻿using System;
using System.Collections.Specialized;
using StructureMap;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Search.Interfaces;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;

namespace Znode.Engine.Services
{
	public class SearchService : BaseService, ISearchService
	{
		public SearchService()
		{
			ObjectFactory.Initialize(x =>
			{
				x.For<IZnodeSearchProvider>().Use<LuceneSearchProvider>();
				x.For<IZNodeSearchRequest>().Use<LuceneSearchRequest>();
			});
		}

		public KeywordSearchModel GetKeywordSearch(KeywordSearchModel model, NameValueCollection expands, NameValueCollection sorts, NameValueCollection page)
		{
			var searchProvider = ObjectFactory.GetInstance<IZnodeSearchProvider>();

			var searchRequest = KeywordSearchMap.ToZnodeRequest(model);
			searchRequest.PortalCatalogID = ZNodeCatalogManager.CatalogConfig.PortalCatalogID;

			GetExpands(expands, searchRequest);
			SetSorting(sorts, searchRequest);

			var response = searchProvider.Search(searchRequest);

			return KeywordSearchMap.ToModel(response);
		}

		public SuggestedSearchListModel GetSearchSuggestions(SuggestedSearchModel model)
		{
			var searchProvider = ObjectFactory.GetInstance<IZnodeSearchProvider>();

			var response = searchProvider.SuggestTermsFor(model.Term, model.Category, ZNodeCatalogManager.CatalogConfig.PortalCatalogID,model.ExternalIdNullCheck);

			if (response != null && response.Count > 0)
			{
				return SuggestedSearchMap.ToModel(response);
			}

			return null;
		}

		private void GetExpands(NameValueCollection expands, IZNodeSearchRequest request)
		{
			ExpandCategories(expands, request);
			ExpandFacets(expands, request);
		}

		private void ExpandCategories(NameValueCollection expands, IZNodeSearchRequest request)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Categories)))
			{
				request.GetCategoriesHeirarchy = true;
			}
		}

		private void ExpandFacets(NameValueCollection expands, IZNodeSearchRequest request)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Facets)))
			{
				request.GetFacets = true;
			}
		}

		private void SetSorting(NameValueCollection expands, IZNodeSearchRequest request)
		{
			if (expands.HasKeys())
			{
				foreach (var key in expands.AllKeys)
				{
					var value = expands.Get(key);

					if (key == SortKeys.Name)
					{
						request.SortOrder = value.ToLower() == "desc" ? 5 : 4;
					}

					if (key == SortKeys.Rating)
					{
						request.SortOrder = 6;
					}
				}
			}
		}

		public void ReloadIndex()
		{
			var searchProvider = new LuceneSearchProvider();
			searchProvider.ReloadIndex();
		}
	}
}
