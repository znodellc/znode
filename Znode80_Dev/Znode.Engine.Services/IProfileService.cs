﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IProfileService
    {
        /// <summary>
        /// Method Gets profile details.
        /// </summary>
        /// <param name="profileId"></param>
        /// <param name="expands"></param>
        /// <returns></returns>
        ProfileModel GetProfile(int profileId, NameValueCollection expands);

        /// <summary>
        /// Method Gets the profile list.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="page"></param>
        /// <returns>Return profile list.</returns>
        ProfileListModel GetProfiles(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

        /// <summary>
        /// Method Create the facet group.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return profile model.</returns>
        ProfileModel CreateProfile(ProfileModel model);

        /// <summary>
        /// Method Update the profile based on profileId.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <param name="model"></param>
        /// <returns>Return updated profile model.</returns>
        ProfileModel UpdateProfile(int profileId, ProfileModel model);

        /// <summary>
        /// Method Delete the profile based on profileId.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <returns>Return true or false.</returns>
        bool DeleteProfile(int profileId);        
    }
}
