﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface ICategoryService
	{
		CategoryModel GetCategory(int categoryId, NameValueCollection expands);
		CategoryListModel GetCategories(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		CategoryListModel GetCategoriesByCatalog(int catalogId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        CategoryListModel GetCategoriesByCatalogIds(string catalogIds, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        CategoryListModel GetCategoriesByCategoryIds(string categoryIds, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        CategoryModel CreateCategory(CategoryModel categoryModel);
		CategoryModel UpdateCategory(int categoryId, CategoryModel categoryModel);

        /// <summary>
        /// Znode Version 8.0
        /// Gets list of categories by catalog Id.
        /// </summary>
        /// <param name="catalogId">The Id of catalog.</param>
        /// <returns>Returns list of categories by catalog Id.</returns>
        CatalogAssociatedCategoriesListModel GetCategoryByCatalogId(int catalogId, NameValueCollection sorts, NameValueCollection page, out int totalRowCount);

        /// <summary>
        /// Znode Version 8.0
        /// Gets all categories.
        /// </summary>
        /// <param name="categoryName">The name of category.</param>
        /// <param name="catalogId">The id of catalog</param>
        /// <param name="sorts">Sort collection object.</param>
        /// <param name="page">Page collection object.</param>
        /// <param name="totalRowCount">Out type of total row count.</param>
        /// <returns>List of all categories.</returns>
        CatalogAssociatedCategoriesListModel GetAllCategories(string categoryName, int catalogId, NameValueCollection sorts, NameValueCollection page, out int totalRowCount);
	}
}
