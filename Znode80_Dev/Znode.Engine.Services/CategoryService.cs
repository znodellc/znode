﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using CategoriesCatalogsRepository = ZNode.Libraries.DataAccess.Service.VwZnodeCategoriesCatalogsService;
using CategoryNodeRepository = ZNode.Libraries.DataAccess.Service.CategoryNodeService;
using CategoryProfileRepository = ZNode.Libraries.DataAccess.Service.CategoryProfileService;
using CategoryRepository = ZNode.Libraries.DataAccess.Service.CategoryService;
using ProductCategoryRepository = ZNode.Libraries.DataAccess.Service.ProductCategoryService;
using System.Data;
using System.Text;
using ZNode.Libraries.DataAccess.Custom;

namespace Znode.Engine.Services
{
    public class CategoryService : BaseService, ICategoryService
    {
        private readonly CategoriesCatalogsRepository _categoriesCatalogsRepository;
        private readonly CategoryNodeRepository _categoryNodeRepository;
        private readonly CategoryProfileRepository _categoryProfileRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly ProductCategoryRepository _productCategoryRepository;

        public CategoryService()
        {
            _categoriesCatalogsRepository = new CategoriesCatalogsRepository();
            _categoryNodeRepository = new CategoryNodeRepository();
            _categoryProfileRepository = new CategoryProfileRepository();
            _categoryRepository = new CategoryRepository();
            _productCategoryRepository = new ProductCategoryRepository();
        }

        public CategoryModel GetCategory(int categoryId, NameValueCollection expands)
        {
            var category = _categoryRepository.GetByCategoryID(categoryId);
            if (category != null)
            {
                GetExpands(expands, category);
            }

            return CategoryMap.ToModel(category);
        }

        public CategoryListModel GetCategories(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CategoryListModel();
            var categories = new TList<Category>();
            var tempList = new TList<Category>();

            var query = new CategoryQuery();
            var sortBuilder = new CategorySortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _categoryRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var category in tempList)
            {
                GetExpands(expands, category);
                categories.Add(category);
            }

            // Map each item and add to the list
            foreach (var c in categories)
            {
                model.Categories.Add(CategoryMap.ToModel(c));
            }

            return model;
        }

        public CategoryListModel GetCategoriesByCatalog(int catalogId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CategoryListModel();
            var categories = new TList<Category>();
            var tempList = new VList<VwZnodeCategoriesCatalogs>();

            var query = new VwZnodeCategoriesCatalogsQuery { Junction = String.Empty };
            var sortBuilder = new VwZnodeCategoriesCatalogsSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            // Manually set the catalog ID parameter
            SetQueryParameter(VwZnodeCategoriesCatalogsColumn.CatalogID, FilterOperators.Equals, catalogId.ToString(), query);

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _categoriesCatalogsRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list, get the full category, and get any expands
            foreach (var item in tempList)
            {
                var category = _categoryRepository.GetByCategoryID(item.CategoryID);
                GetExpands(expands, category);
                categories.Add(category);
            }

            // Map each item and add to the list
            foreach (var c in categories)
            {
                model.Categories.Add(CategoryMap.ToModel(c));
            }

            return model;
        }

        public CategoryListModel GetCategoriesByCatalogIds(string catalogIds, NameValueCollection expands, List<Tuple<string, string, string>> filters,
                                                           NameValueCollection sorts, NameValueCollection page)
        {
            if (String.IsNullOrEmpty(catalogIds))
            {
                throw new Exception("List of catalog IDs cannot be null or empty.");
            }

            var model = new CategoryListModel();
            var categories = new TList<Category>();
            var tempList = new VList<VwZnodeCategoriesCatalogs>();

            var query = new VwZnodeCategoriesCatalogsQuery { Junction = String.Empty };
            var sortBuilder = new VwZnodeCategoriesCatalogsSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            var list = catalogIds.Split(',');
            foreach (var catalogId in list.Where(catalogId => !String.IsNullOrEmpty(catalogId)))
            {
                query.BeginGroup("OR");
                query.AppendEquals(VwZnodeCategoriesCatalogsColumn.CatalogID, catalogId);
                query.EndGroup();
            }

            // Manually set the catalog ID parameter
            //SetQueryParameter(VwZnodeCategoriesCatalogsColumn.CatalogID, FilterOperators.Contains, catalogIds, query);

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _categoriesCatalogsRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list, get the full category, and get any expands
            foreach (var item in tempList)
            {
                var category = _categoryRepository.GetByCategoryID(item.CategoryID);
                GetExpands(expands, category);
                categories.Add(category);
            }

            // Map each item and add to the list
            foreach (var c in categories)
            {
                model.Categories.Add(CategoryMap.ToModel(c));
            }

            return model;
        }

        public CategoryListModel GetCategoriesByCategoryIds(string categoryIds, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            if (String.IsNullOrEmpty(categoryIds))
            {
                throw new Exception("List of category IDs cannot be null or empty.");
            }

            var model = new CategoryListModel();
            var categories = new TList<Category>();
            var tempList = new TList<Category>();

            var query = new CategoryQuery { Junction = String.Empty };
            var sortBuilder = new CategorySortBuilder();

            var list = categoryIds.Split(',');
            foreach (var categoryId in list.Where(categoryId => !String.IsNullOrEmpty(categoryId)))
            {
                query.BeginGroup("OR");
                query.AppendEquals(CategoryColumn.CategoryID, categoryId);
                query.EndGroup();
            }

            SetSorting(sorts, sortBuilder);

            // Get the initial set
            tempList = _categoryRepository.Find(query, sortBuilder);

            // Now go through the list and get any expands
            foreach (var category in tempList)
            {
                GetExpands(expands, category);
                categories.Add(category);
            }

            // Map each item and add to the list
            foreach (var c in categories)
            {
                model.Categories.Add(CategoryMap.ToModel(c));
            }

            return model;
        }

        public CategoryModel CreateCategory(CategoryModel model)
        {
            if (model == null)
            {
                throw new Exception("Category model cannot be null.");
            }

            var entity = CategoryMap.ToEntity(model);

            var category = _categoryRepository.Save(entity);
            if (category != null)
            {
                return CategoryMap.ToModel(category);
            }

            return null;
        }

        public CategoryModel UpdateCategory(int categoryId, CategoryModel model)
        {
            if (categoryId < 1)
            {
                throw new Exception("Category ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Category model cannot be null.");
            }

            var category = _categoryRepository.GetByCategoryID(categoryId);
            if (category != null)
            {
                // Set the category ID
                model.CategoryId = categoryId;

                var categoryToUpdate = CategoryMap.ToEntity(model);

                var updated = _categoryRepository.Update(categoryToUpdate);
                if (updated)
                {
                    category = _categoryRepository.GetByCategoryID(categoryId);
                    if (category != null)
                    {
                        return CategoryMap.ToModel(category);
                    }
                }
            }

            return null;
        }

        private void GetExpands(NameValueCollection expands, Category category)
        {
            if (expands.HasKeys())
            {
                ExpandCategoryNodes(expands, category);
                ExpandProductIds(expands, category);
                ExpandSubcategories(expands, category);
                ExpanCategoryProfiles(expands, category);
            }
        }

        private void ExpanCategoryProfiles(NameValueCollection expands, Category category)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.CategoryProfiles)))
            {
                var categoryProfiles = _categoryProfileRepository.GetByCategoryID(category.CategoryID);

                foreach (var profile in categoryProfiles)
                {
                    category.CategoryProfileCollection.Add(profile);
                }
            }
        }

        private void ExpandCategoryNodes(NameValueCollection expands, Category category)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.CategoryNodes)))
            {
                var categoryNodes = _categoryNodeRepository.GetByCategoryID(category.CategoryID);

                foreach (var node in categoryNodes)
                {
                    category.CategoryNodeCollection.Add(node);
                }
            }
        }

        private void ExpandProductIds(NameValueCollection expands, Category category)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ProductIds)))
            {
                var productIds = String.Empty;
                var productCategories = _productCategoryRepository.GetByCategoryID(category.CategoryID);
                foreach (var pc in productCategories)
                {
                    productIds += pc.ProductID + ",";
                }

                productIds = productIds.TrimEnd(',');
                category.ProductIds = productIds;
            }
        }

        private void ExpandSubcategories(NameValueCollection expands, Category category)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Subcategories)))
            {
                var categoryNodes = _categoryNodeRepository.GetByCategoryID(category.CategoryID);

                foreach (var node in categoryNodes)
                {
                    var nodes = _categoryNodeRepository.DeepLoadByParentCategoryNodeID(node.CategoryNodeID, true, DeepLoadType.IncludeChildren, typeof(Category));

                    foreach (var catnode in nodes)
                    {
                        _categoryNodeRepository.DeepLoad(catnode, true, DeepLoadType.IncludeChildren, typeof(Category));
                        category.CategoryNodeCollection.Add(catnode);
                    }
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, CategoryQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.ExternalId) SetQueryParameter(CategoryColumn.ExternalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsVisible) SetQueryParameter(CategoryColumn.VisibleInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Name) SetQueryParameter(CategoryColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PortalId) SetQueryParameter(CategoryColumn.PortalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Title) SetQueryParameter(CategoryColumn.Title, filterOperator, filterValue, query);
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, VwZnodeCategoriesCatalogsQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.ExternalId) SetQueryParameter(VwZnodeCategoriesCatalogsColumn.ExternalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsVisible) SetQueryParameter(VwZnodeCategoriesCatalogsColumn.VisibleInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Name) SetQueryParameter(VwZnodeCategoriesCatalogsColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PortalId) SetQueryParameter(VwZnodeCategoriesCatalogsColumn.PortalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Title) SetQueryParameter(VwZnodeCategoriesCatalogsColumn.Title, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, CategorySortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.CategoryId) SetSortDirection(CategoryColumn.CategoryID, value, sortBuilder);
                    if (key == SortKeys.DisplayOrder) SetSortDirection(CategoryColumn.DisplayOrder, value, sortBuilder);
                    if (key == SortKeys.Name) SetSortDirection(CategoryColumn.Name, value, sortBuilder);
                    if (key == SortKeys.Title) SetSortDirection(CategoryColumn.Title, value, sortBuilder);
                }
            }
        }

        private void SetSorting(NameValueCollection sorts, VwZnodeCategoriesCatalogsSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.CategoryId) SetSortDirection(VwZnodeCategoriesCatalogsColumn.CategoryID, value, sortBuilder);
                    if (key == SortKeys.DisplayOrder) SetSortDirection(VwZnodeCategoriesCatalogsColumn.DisplayOrder, value, sortBuilder);
                    if (key == SortKeys.Name) SetSortDirection(VwZnodeCategoriesCatalogsColumn.Name, value, sortBuilder);
                    if (key == SortKeys.Title) SetSortDirection(VwZnodeCategoriesCatalogsColumn.Title, value, sortBuilder);
                }
            }
        }

        private void SetQueryParameter(CategoryColumn column, string filterOperator, string filterValue, CategoryQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(CategoryColumn column, string value, CategorySortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }


        #region Znode Version 8.0

        /// <summary>
        /// Gets list of categories by catalog Id.
        /// </summary>
        /// <param name="catalogId">The Id of catalog.</param>
        /// <returns>Returns list of categories by catalog Id.</returns>
        public CatalogAssociatedCategoriesListModel GetCategoryByCatalogId(int catalogId, NameValueCollection sorts, NameValueCollection page, out int totalRowCount)
        {
            var model = new CatalogAssociatedCategoriesListModel();
            var sortBuilder = new VwZnodeCategoriesCatalogsSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;           
            string whereClause = string.Format("~~CatalogId~~={0}", catalogId);

            SetPaging(page, model, out pagingStart, out pagingLength);
            SetSorting(sorts, sortBuilder);
            string orderBy = sortBuilder.ToString();
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));
          

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, "ZNodeCategory", orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);

            foreach (DataRow dr in resultDataSet.Tables[0].Rows)
            {
                dr["Name"] = ParsePath(dr["Name"].ToString(), ">");
            }
            model = CategoryMap.ToModelList(resultDataSet);
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            return model;
        }

        /// <summary>
        /// Returns a bread crumb path from a delimited string
        /// </summary>
        /// <param name="delimitedString">Delimitted string</param>
        /// <param name="separator">Path seperator.</param>
        /// <returns>Returns the parsed category path.</returns>
        private string ParsePath(string delimitedString, string separator)
        {
            StringBuilder categoryPath = new StringBuilder();

            string[] delim1 = { "||" };
            string[] pathItems = delimitedString.Split(delim1, StringSplitOptions.None);

            foreach (string pathItem in pathItems)
            {
                string[] delim2 = { "%%" };
                string[] items = pathItem.Split(delim2, StringSplitOptions.None);

                string categoryId = items.GetValue(0).ToString();
                string categoryName = items.GetValue(1).ToString();
                categoryPath.Append(categoryName);
                categoryPath.Append(string.Format(" {0} ", separator));
            }

            if (categoryPath.Length > 0)
            {
                if (categoryPath.ToString().LastIndexOf(string.Format(" {0} ", separator)) == categoryPath.ToString().Length - 3)
                {
                    categoryPath = categoryPath.Remove(categoryPath.ToString().Length - 3, 3);
                }
            }

            return categoryPath.ToString();
        }


        /// <summary>      
        /// Gets all categories.
        /// </summary>
        /// <param name="categoryName">The name of category.</param>
        /// <param name="catalogId">The id of catalog</param>
        /// <param name="sorts">Sort collection object.</param>
        /// <param name="page">Page collection object.</param>
        /// <param name="totalRowCount">Out type of total row count.</param>
        /// <returns>List of all categories.</returns>
        public CatalogAssociatedCategoriesListModel GetAllCategories(string categoryName, int catalogId, NameValueCollection sorts, NameValueCollection page, out int totalRowCount)
        {
            var model = new CatalogAssociatedCategoriesListModel();
            var sortBuilder = new VwZnodeCategoriesCatalogsSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;
            string whereClause = string.Format("~~Name~~ LIKE '%{0}%'", categoryName);

            SetPaging(page, model, out pagingStart, out pagingLength);
            SetSorting(sorts, sortBuilder);
            string orderBy = sortBuilder.ToString();
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));


            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();
            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, "ZNode_SearchCategories", orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);
            model = CategoryMap.ToModelList(resultDataSet);
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            return model;
        }

        #endregion
    }
}
