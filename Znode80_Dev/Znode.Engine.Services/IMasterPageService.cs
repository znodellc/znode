﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
   public interface IMasterPageService
    {
       MasterPageListModel GetMasterPages(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);

       MasterPageListModel GetMasterPageListByThemeId(int themeId, string pageType);
    }
}
