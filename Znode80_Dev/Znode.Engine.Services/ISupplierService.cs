﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface ISupplierService
	{
		SupplierModel GetSupplier(int supplierId, NameValueCollection expands);
		SupplierListModel GetSuppliers(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		SupplierModel CreateSupplier(SupplierModel model);
		SupplierModel UpdateSupplier(int supplierId, SupplierModel model);
		bool DeleteSupplier(int supplierId);
	}
}
