﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface ICountryService
	{
		CountryModel GetCountry(string countryCode, NameValueCollection expands);
		CountryListModel GetCountries(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		CountryModel CreateCountry(CountryModel model);
		CountryModel UpdateCountry(string countryCode, CountryModel model);
		bool DeleteCountry(string countryCode);

        /// <summary>
        /// Znode Version 7.2.2
        /// This function Get all active countries for portalId.
        /// </summary>
        /// <param name="model">int portalId</param>
        /// <returns>Returns CountryListModel</returns>
        CountryListModel GetActiveCountryByPortalId(int portalId);
	}
}
