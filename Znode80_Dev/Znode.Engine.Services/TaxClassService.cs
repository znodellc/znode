﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using TaxClassRepository = ZNode.Libraries.DataAccess.Service.TaxClassService;

namespace Znode.Engine.Services
{
    public class TaxClassService : BaseService, ITaxClassService
    {
        private readonly TaxClassRepository _taxClassRepository;

        public TaxClassService()
        {
            _taxClassRepository = new TaxClassRepository();
        }

        public TaxClassModel GetTaxClass(int taxClassId)
        {
            var taxClass = _taxClassRepository.GetByTaxClassID(taxClassId);
            return TaxClassMap.ToModel(taxClass);
        }

        public TaxClassListModel GetTaxClasses(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new TaxClassListModel();
            var taxClasses = new TList<TaxClass>();

            var query = new TaxClassQuery();
            var sortBuilder = new TaxClassSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            taxClasses = _taxClassRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Map each item and add to the list
            foreach (var t in taxClasses)
            {
                model.TaxClasses.Add(TaxClassMap.ToModel(t));
            }

            return model;
        }

        public TaxClassModel CreateTaxClass(TaxClassModel model)
        {
            if (model == null)
            {
                throw new Exception("Tax class model cannot be null.");
            }

            var entity = TaxClassMap.ToEntity(model);
            var taxClass = _taxClassRepository.Save(entity);
            return TaxClassMap.ToModel(taxClass);
        }

        public TaxClassModel UpdateTaxClass(int taxClassId, TaxClassModel model)
        {
            if (taxClassId < 1)
            {
                throw new Exception("Tax class ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Tax class model cannot be null.");
            }

            var taxClass = _taxClassRepository.GetByTaxClassID(taxClassId);
            if (taxClass != null)
            {
                // Set the tax class ID and map
                model.TaxClassId = taxClassId;
                var taxClassToUpdate = TaxClassMap.ToEntity(model);

                var updated = _taxClassRepository.Update(taxClassToUpdate);
                if (updated)
                {
                    taxClass = _taxClassRepository.GetByTaxClassID(taxClassId);
                    return TaxClassMap.ToModel(taxClass);
                }
            }

            return null;
        }

        public bool DeleteTaxClass(int taxClassId)
        {
            if (taxClassId < 1)
            {
                throw new Exception("Tax class ID cannot be less than 1.");
            }

            var taxClass = _taxClassRepository.GetByTaxClassID(taxClassId);
            if (taxClass != null)
            {
                return _taxClassRepository.Delete(taxClass);
            }

            return false;
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, TaxClassQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.ExternalId) SetQueryParameter(TaxClassColumn.ExternalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsActive) SetQueryParameter(TaxClassColumn.ActiveInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Name) SetQueryParameter(TaxClassColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PortalId) SetQueryParameter(TaxClassColumn.PortalID, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, TaxClassSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.DisplayOrder) SetSortDirection(TaxClassColumn.DisplayOrder, value, sortBuilder);
                    if (key == SortKeys.Name) SetSortDirection(TaxClassColumn.Name, value, sortBuilder);
                    if (key == SortKeys.TaxClassId) SetSortDirection(TaxClassColumn.TaxClassID, value, sortBuilder);
                }
            }
        }

        private void SetQueryParameter(TaxClassColumn column, string filterOperator, string filterValue, TaxClassQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(TaxClassColumn column, string value, TaxClassSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        #region Znode Version 8.0
        /// <summary>
        /// To get Active Tax Class by PortalId
        /// </summary>
        /// <param name="portalId">int PortalId</param>
        /// <returns>List of active tax class having portalId equal to supplied portalId or null</returns>
        public TaxClassListModel GetActiveTaxClassByPortalId(int portalId)
        {
            var taxHelper = new TaxHelper();
            TaxClassListModel list = new TaxClassListModel();
            DataSet dataset = taxHelper.GetActiveTaxClassByPortalId(portalId);
            if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dataset.Tables[0].Rows)
                {
                    TaxClassModel item = new TaxClassModel();
                    item.TaxClassId = Convert.ToInt32(dr[0]);
                    item.Name = dr[1].ToString();
                    item.DisplayOrder = Convert.ToInt32(dr[2]);
                    item.IsActive = Convert.ToBoolean(dr[3]);
                    item.PortalId = Convert.ToInt32(dr[4]);
                    item.ExternalId = dr[5].ToString();
                    list.TaxClasses.Add(item);
                }
            }
            return list;
        }

        #endregion
    }
}
