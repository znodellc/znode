﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IDomainService
	{
		DomainModel GetDomain(int domainId);
		DomainListModel GetDomains(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
	    Domain GetDomain(string domainName);
		DomainModel CreateDomain(DomainModel model);
		DomainModel UpdateDomain(int domainId, DomainModel model);
		bool DeleteDomain(int domainId);
	}
}
