﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ThemeRepository = ZNode.Libraries.DataAccess.Service.ThemeService;

namespace Znode.Engine.Services
{
    public class ThemeService : BaseService, IThemeService
    {
        #region Private Variables
        private readonly ThemeRepository _themeRepository;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for ThemeService
        /// </summary>
        public ThemeService()
        {
            _themeRepository = new ThemeRepository();
        }
        #endregion

        #region Public Methods
        // <summary>
        /// Gets the list of themes according to filters and sorts.
        /// </summary>
        /// <param name="filters">Filters to be applied.</param>
        /// <param name="sorts">Sorting of the list.</param>
        /// <returns>List of themes</returns>
        public ThemeListModel GetThemes(List<Tuple<string, string, string>> filters, NameValueCollection sorts)
        {
            var model = new ThemeListModel();
            var themes = new TList<Theme>();
            var tempList = new TList<Theme>();


            var query = new ThemeQuery();
            var sortBuilder = new ThemeSortBuilder();

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);

            // Get the initial set           
            tempList = _themeRepository.Find(query, sortBuilder);
            model.TotalResults = tempList.Count();

            // Now go through the list and get any expands
            foreach (var theme in tempList)
            {
                themes.Add(theme);
            }

            // Map each item and add to the list
            foreach (var a in themes)
            {
                model.Themes.Add(ThemeMap.ToModel(a));
            }
            return model;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Sets the filter on Theme List.
        /// </summary>
        /// <param name="filters">filter tuple containg key, operator and value.</param>
        /// <param name="query">Query releated to filter tuple.</param>
        private void SetFiltering(List<Tuple<string, string, string>> filters, ThemeQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.ThemeName) SetQueryParameter(ThemeColumn.Name, filterOperator, filterValue, query);
            }
        }

        /// <summary>
        /// Sorts the Theme List.
        /// </summary>
        /// <param name="sorts">Sort collection consisting of key and directions.</param>
        /// <param name="sortBuilder">Creates sort on the basis of sort collections.</param>
        private void SetSorting(NameValueCollection sorts, ThemeSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.ThemeId) SetSortDirection(ThemeColumn.ThemeID, value, sortBuilder);
                    if (key == SortKeys.ThemeName) SetSortDirection(ThemeColumn.Name, value, sortBuilder);
                }
            }
        }
        #endregion

    }
}
