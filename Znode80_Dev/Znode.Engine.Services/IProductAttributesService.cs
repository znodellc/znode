﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IProductAttributesService
    {
        AttributeModel GetAttribute(int attributeId);
        ProductAttributeListModel GetAttributesByAttributeTypeId(int attributeTypeId);
        ProductAttributeListModel GetAttributes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        AttributeModel CreateAttributes(AttributeModel model);
        AttributeModel UpdateAttributes(int attributeId, AttributeModel model);
        bool DeleteAttributes(int attributeId);
    }
}
