﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IHighlightService
	{
		HighlightModel GetHighlight(int highlightId, NameValueCollection expands);
		HighlightListModel GetHighlights(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		HighlightModel CreateHighlight(HighlightModel model);
		HighlightModel UpdateHighlight(int highlightId, HighlightModel model);
		bool DeleteHighlight(int highlightId);

        /// <summary>
        /// Check the Associated product with highlight and sets the property of it.
        /// </summary>
        /// <param name="highlightId">int highlightId</param>
        /// <returns>HighlightModel</returns>
        HighlightModel CheckAssociateProduct(int highlightId);
	}
}
