﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using WishListRepository = ZNode.Libraries.DataAccess.Service.WishListService;

namespace Znode.Engine.Services
{
	public class WishListService : BaseService, IWishListService
	{
		private readonly WishListRepository _wishlistRepository;

		public WishListService()
		{
			_wishlistRepository = new WishListRepository();
		}

		public WishListModel Create(WishListModel model)
		{
			model.CreateDate = DateTime.Now;
			var wishList = _wishlistRepository.Save(WishListMap.ToEntity(model));
			return WishListMap.ToModel(wishList);
		}

		public WishListModel GetWishList(int id)
		{
			var wishList = _wishlistRepository.GetByWishListID(id);
			return WishListMap.ToModel(wishList);
		}

		public WishListListModel GetWishLists(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new WishListListModel();
			var wishLists = new TList<WishList>();
			var tempList = new TList<WishList>();

			var query = new WishListQuery { Junction = String.Empty };
			var sortBuilder = new WishListSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			tempList = _wishlistRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Now go through the list and get any expands
			foreach (var wishes in tempList)
			{
				// GetExpands(expands, wishes);
				wishLists.Add(wishes);
			}

			// Map each item and add to the list
			foreach (var w in wishLists)
			{
				model.WishLists.Add(WishListMap.ToModel(w));
			}

			return model;
		}

		public WishListModel UpdateWishlist(int wishlistId, WishListModel model)
		{
			model.WishListId = wishlistId;
			if (_wishlistRepository.Update(WishListMap.ToEntity(model)))
			{
				var wishlist = _wishlistRepository.GetByWishListID(wishlistId);
				return WishListMap.ToModel(wishlist);
			}

			return null;
		}

		public bool DeleteWishlist(int wishlistId)
		{
			if (_wishlistRepository.Delete(wishlistId))
			{
				return true;
			}

			return false;
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, WishListQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.AccountId) SetQueryParameter(WishListColumn.AccountID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ProductId) SetQueryParameter(WishListColumn.ProductID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.CreateDate) SetQueryParameter(WishListColumn.CreateDte, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, WishListSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.AccountId) SetSortDirection(WishListColumn.AccountID, value, sortBuilder);
					if (key == SortKeys.CreateDate) SetSortDirection(WishListColumn.CreateDte, value, sortBuilder);
					if (key == SortKeys.ProductId) SetSortDirection(WishListColumn.ProductID, value, sortBuilder);
				}
			}
		}
	}
}
