﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IProductService
	{
		ProductModel GetProduct(int productId, NameValueCollection expands);
		ProductModel GetProductWithSku(int productId, int skuId, NameValueCollection expands);
		ProductListModel GetProducts(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		ProductListModel GetProductsByCatalog(int catalogId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		ProductListModel GetProductsByCatalogIds(string catalogIds, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		ProductListModel GetProductsByCategory(int categoryId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		ProductListModel GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		ProductListModel GetProductsByExternalIds(string externalIds, NameValueCollection expands, NameValueCollection sorts);
		ProductListModel GetProductsByHomeSpecials(int portalCatalogId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		ProductListModel GetProductsByProductIds(string productIds, NameValueCollection expands, NameValueCollection sorts);
		ProductListModel GetProductsByPromotionType(int promotionTypeId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		ProductModel CreateProduct(ProductModel model);
		ProductModel UpdateProduct(int productId, ProductModel model);
		bool DeleteProduct(int productId);
        /// <summary>
        /// Znode Version 8.0
        /// To get product information by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns product details </returns>
        ProductListModel GetProductDetailsByProductId(int productId);


        /// <summary>
        /// Znode Version 8.0
        /// Gets the Product tags of the product.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="expands"></param>
        /// <returns></returns>
        ProductTagsModel GetProductTags(int productId, NameValueCollection expands);

        /// <summary>
        /// Znode Version 8.0
        /// Insert the Product Tags for the product
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        ProductTagsModel CreateProductTag(ProductTagsModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Updates the product tags for the product.
        /// </summary>
        /// <param name="tagId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        ProductTagsModel UpdateProductTag(int tagId, ProductTagsModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Deletes the Product Tags based on tag Id.
        /// </summary>
        /// <param name="tagId"></param>
        /// <returns></returns>
        bool DeleteProductTag(int tagId);

        /// <summary>
        /// Znode Version 8.0
        /// Gets the Product Facets of the product.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="expands"></param>
        /// <returns></returns>
        ProductFacetModel GetProductFacets(int productId, NameValueCollection expands);

        /// <summary>
        /// Znode Version 8.0
        /// Binds the Associated facets to the Product.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        ProductFacetModel BindProductFacets(int productId, ProductFacetModel model);
	}
}
