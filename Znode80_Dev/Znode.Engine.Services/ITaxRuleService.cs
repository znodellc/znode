﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface ITaxRuleService
	{
		TaxRuleModel GetTaxRule(int taxRuleId, NameValueCollection expands);
		TaxRuleListModel GetTaxRules(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		TaxRuleModel CreateTaxRule(TaxRuleModel model);
		TaxRuleModel UpdateTaxRule(int taxRuleId, TaxRuleModel model);
		bool DeleteTaxRule(int taxRuleId);
	}
}
