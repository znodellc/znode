﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using CasePriorityRepository = ZNode.Libraries.DataAccess.Service.CasePriorityService;
using CaseRequestRepository = ZNode.Libraries.DataAccess.Service.CaseRequestService;
using CaseStatusRepository = ZNode.Libraries.DataAccess.Service.CaseStatusService;
using CaseTypeRepository = ZNode.Libraries.DataAccess.Service.CaseTypeService;

namespace Znode.Engine.Services
{
	public class CaseRequestService : BaseService, ICaseRequestService
	{
		private readonly CasePriorityRepository _casePriorityRepository;
		private readonly CaseRequestRepository _caseRequestRepository;
		private readonly CaseStatusRepository _caseStatusRepository;
		private readonly CaseTypeRepository _caseTypeRepository;

		public CaseRequestService()
		{
			_casePriorityRepository = new CasePriorityRepository();
			_caseRequestRepository = new CaseRequestRepository();
			_caseStatusRepository = new CaseStatusRepository();
			_caseTypeRepository = new CaseTypeRepository();
		}

		public CaseRequestModel GetCaseRequest(int caseRequestId, NameValueCollection expands)
		{
			var caseRequest = _caseRequestRepository.GetByCaseID(caseRequestId);
			if (caseRequest != null)
			{
				GetExpands(expands, caseRequest);
			}

			return CaseRequestMap.ToModel(caseRequest);
		}

		public CaseRequestListModel GetCaseRequests(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new CaseRequestListModel();
			var caseRequests = new TList<CaseRequest>();
			var tempList = new TList<CaseRequest>();

			var query = new CaseRequestQuery();
			var sortBuilder = new CaseRequestSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			tempList = _caseRequestRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Now go through the list and get any expands
			foreach (var caseRequest in tempList)
			{
				GetExpands(expands, caseRequest);
				caseRequests.Add(caseRequest);
			}

			// Map each item and add to the list
			foreach (var c in caseRequests)
			{
				model.CaseRequests.Add(CaseRequestMap.ToModel(c));
			}

			return model;
		}

		public CaseRequestModel CreateCaseRequest(CaseRequestModel model)
		{
			if (model == null)
			{
				throw new Exception("Case request model cannot be null.");
			}

			var sb = new StringBuilder();
			sb.Append(model.Title);
			sb.Append(Environment.NewLine);
			sb.Append(Environment.NewLine);
			sb.Append("Name: ");
			sb.Append(model.FirstName);
			sb.Append(" ");
			sb.Append(model.LastName);
			sb.Append(Environment.NewLine);
		    if (!model.Title.Contains("Feedback"))
		    {
		        sb.Append("Company Name:");
		        sb.Append(model.CompanyName);
		        sb.Append(Environment.NewLine);
		        sb.Append("Phone Number: ");
		        sb.Append(model.PhoneNumber);
		        sb.Append(Environment.NewLine);
		    }
		    sb.Append("Comments: ");
			sb.Append(model.Description);

			SmtpService.SendMail(model.Email, sb.Replace(Environment.NewLine, "<br />").ToString());

			// Set the create date
			model.CreateDate = DateTime.Now;

			var entity = CaseRequestMap.ToEntity(model);

			var caseRequest = _caseRequestRepository.Save(entity);
			return CaseRequestMap.ToModel(caseRequest);
		}

		public CaseRequestModel UpdateCaseRequest(int caseRequestId, CaseRequestModel model)
		{
			if (caseRequestId < 1)
			{
				throw new Exception("Case request ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Case request model cannot be null.");
			}

			var caseRequest = _caseRequestRepository.GetByCaseID(caseRequestId);
			if (caseRequest != null)
			{
				// Set case request ID
				model.CaseRequestId = caseRequestId;

				var caseRequestToUpdate = CaseRequestMap.ToEntity(model);

				var updated = _caseRequestRepository.Update(caseRequestToUpdate);
				if (updated)
				{
					caseRequest = _caseRequestRepository.GetByCaseID(caseRequestId);
					return CaseRequestMap.ToModel(caseRequest);
				}
			}

			return null;
		}

		public bool DeleteCaseRequest(int caseRequestId)
		{
			if (caseRequestId < 1)
			{
				throw new Exception("Case request ID cannot be less than 1.");
			}

			var caseRequest = _caseRequestRepository.GetByCaseID(caseRequestId);
			if (caseRequest != null)
			{
				return _caseRequestRepository.Delete(caseRequest);
			}

			return false;
		}

		private void GetExpands(NameValueCollection expands, CaseRequest caseRequest)
		{
			if (expands.HasKeys())
			{
				ExpandCasePriority(expands, caseRequest);
				ExpandCaseStatus(expands, caseRequest);
				ExpandCaseType(expands, caseRequest);
			}
		}

		private void ExpandCasePriority(NameValueCollection expands, CaseRequest caseRequest)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.CasePriority)))
			{
				var casePriority = _casePriorityRepository.GetByCasePriorityID(caseRequest.CasePriorityID);
				if (casePriority != null)
				{
					caseRequest.CasePriorityIDSource = casePriority;
				}
			}
		}

		private void ExpandCaseStatus(NameValueCollection expands, CaseRequest caseRequest)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.CaseStatus)))
			{
				var caseStatus = _caseStatusRepository.GetByCaseStatusID(caseRequest.CaseStatusID);
				if (caseStatus != null)
				{
					caseRequest.CaseStatusIDSource = caseStatus;
				}
			}
		}

		private void ExpandCaseType(NameValueCollection expands, CaseRequest caseRequest)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.CaseType)))
			{
				var caseType = _caseTypeRepository.GetByCaseTypeID(caseRequest.CaseTypeID);
				if (caseType != null)
				{
					caseRequest.CaseTypeIDSource = caseType;
				}
			}
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, CaseRequestQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.AccountId) SetQueryParameter(CaseRequestColumn.AccountID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.CasePriorityId) SetQueryParameter(CaseRequestColumn.CasePriorityID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.CaseStatusId) SetQueryParameter(CaseRequestColumn.CaseStatusID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.CaseTypeId) SetQueryParameter(CaseRequestColumn.CaseTypeID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.CreateDate) SetQueryParameter(CaseRequestColumn.CreateDte, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Email) SetQueryParameter(CaseRequestColumn.EmailID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.LastName) SetQueryParameter(CaseRequestColumn.LastName, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.OwnerAccountId) SetQueryParameter(CaseRequestColumn.OwnerAccountID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PortalId) SetQueryParameter(CaseRequestColumn.PortalID, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, CaseRequestSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.CasePriorityId) SetSortDirection(CaseRequestColumn.CasePriorityID, value, sortBuilder);
					if (key == SortKeys.CaseRequestId) SetSortDirection(CaseRequestColumn.CaseID, value, sortBuilder);
					if (key == SortKeys.CaseStatusId) SetSortDirection(CaseRequestColumn.CaseStatusID, value, sortBuilder);
					if (key == SortKeys.CaseTypeId) SetSortDirection(CaseRequestColumn.CaseTypeID, value, sortBuilder);
					if (key == SortKeys.CreateDate) SetSortDirection(CaseRequestColumn.CreateDte, value, sortBuilder);
					if (key == SortKeys.LastName) SetSortDirection(CaseRequestColumn.LastName, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(CaseRequestColumn column, string filterOperator, string filterValue, CaseRequestQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(CaseRequestColumn column, string value, CaseRequestSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
