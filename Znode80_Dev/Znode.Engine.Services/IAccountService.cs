﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IAccountService
	{
		AccountModel GetAccount(int accountId, NameValueCollection expands);
		AccountModel GetAccountByUserId(Guid userId, NameValueCollection expands);
	    AccountModel GetAccountByUsername(string username, NameValueCollection expands);
		AccountListModel GetAccounts(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
	    AccountModel CreateAccount(int portalId, AccountModel model);
		AccountModel UpdateAccount(int accountId, AccountModel model);
		bool DeleteAccount(int accountId);
        AccountModel Login(int portalId, AccountModel model, out string errorCode, NameValueCollection expand);
        AccountModel ChangePassword(int portalId, AccountModel model);
		AccountModel ResetPassword(AccountModel model);
	    Profile GetCustomerProfile(int accountId, int portalId);
        /// <summary>
        /// Znode Version 7.2.2 
        /// This function will check the Reset Password Link current status.
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <param name="errorCode"></param>
        /// <returns>Returns Reset Password Link Status i.e. Error codes</returns>
        AccountModel CheckResetPasswordLinkStatus(AccountModel model, out string errorCode);

        /// <summary>
        /// Znode Version 8.0
        /// Method Check for the user role based on role name
        /// </summary>
        /// <param name="userName">string userName</param>
        /// <param name="roleName">string roleName</param>
        /// <returns>Returns status for the user based on role name</returns>
        AccountModel CheckUserRole(string userName, string roleName);

        /// <summary>
        /// Znode Version 8.0
        /// Method used to reset Admin details in case the first default login.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return the Reset admin details</returns>
        AccountModel ResetAdminDetails(AccountModel model);

	}
}
