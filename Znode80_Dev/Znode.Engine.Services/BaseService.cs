﻿using System;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using ZNode.Libraries.DataAccess.Data;

namespace Znode.Engine.Services
{
	public abstract class BaseService
	{
		protected void SetPaging(NameValueCollection page, BaseListModel model, out int pagingStart, out int pagingLength)
		{
			// We use int.MaxValue for the paging length to ensure we always get total results back
			pagingStart = 0;
			pagingLength = int.MaxValue;

			if (page != null && page.HasKeys())
			{
				// Only do if both index and size are given
				if (!String.IsNullOrEmpty(page.Get(PageKeys.Index)) && !String.IsNullOrEmpty(page.Get(PageKeys.Size)))
				{
					var pageIndex = Convert.ToInt32(page.Get(PageKeys.Index));
					var pageSize = Convert.ToInt32(page.Get(PageKeys.Size));

					model.PageIndex = pageIndex;
					model.PageSize = pageSize;

					pagingStart = pageSize * pageIndex;
					pagingLength = pageSize;
				}
			}
			else
			{
				model.PageIndex = pagingStart;
				model.PageSize = pagingLength;
			}
		}

		protected void SetQueryParameter<TEntityColumn>(TEntityColumn column, string filterOperator, string filterValue, SqlFilterBuilder<TEntityColumn> query)
		{
			const string and = "AND";
			var value = filterValue == "null" ? null : filterValue;

			if (value == null)
			{
				// Nulls are special cases
				if (filterOperator == FilterOperators.Equals) query.AppendIsNull(and, column);
				if (filterOperator == FilterOperators.NotEquals) query.AppendIsNotNull(and, column);
			}
			else
			{
				if (filterOperator == FilterOperators.Equals) query.AppendEquals(and, column, value);
				if (filterOperator == FilterOperators.NotEquals) query.AppendNotEquals(and, column, value);
				if (filterOperator == FilterOperators.Contains) query.AppendContains(and, column, value);
				if (filterOperator == FilterOperators.GreaterThan) query.AppendGreaterThan(and, column, value);
				if (filterOperator == FilterOperators.GreaterThanOrEqual) query.AppendGreaterThanOrEqual(and, column, value);
				if (filterOperator == FilterOperators.LessThan) query.AppendLessThan(and, column, value);
				if (filterOperator == FilterOperators.LessThanOrEqual) query.AppendLessThanOrEqual(and, column, value);
				if (filterOperator == FilterOperators.StartsWith) query.AppendStartsWith(and, column, value);
				if (filterOperator == FilterOperators.EndsWith) query.AppendEndsWith(and, column, value);
			}
		}

		protected void SetSortDirection<TEntityColumn>(Enum column, string value, SqlSortBuilder<TEntityColumn> sortBuilder)
		{
			if (!String.IsNullOrEmpty(value))
			{
				var sortDirection = value.ToLower() == "desc" ? SqlSortDirection.DESC : SqlSortDirection.ASC;
				sortBuilder.Append(column, sortDirection);
			}
		}
	}
}
