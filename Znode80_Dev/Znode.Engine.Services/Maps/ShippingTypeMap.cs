﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class ShippingTypeMap
	{
		public static ShippingTypeModel ToModel(ShippingType entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new ShippingTypeModel
			{
				ClassName = entity.ClassName,
				Description = entity.Description,
				IsActive = entity.IsActive,
				Name = entity.Name,
				ShippingTypeId = entity.ShippingTypeID
			};

			return model;
		}

		public static ShippingType ToEntity(ShippingTypeModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new ShippingType
			{
				ClassName = model.ClassName,
				Description = model.Description,
				IsActive = model.IsActive,
				Name = model.Name,
				ShippingTypeID = model.ShippingTypeId
			};

			return entity;
		}
	}
}
