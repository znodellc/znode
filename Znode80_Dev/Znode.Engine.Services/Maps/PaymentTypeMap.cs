﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class PaymentTypeMap
	{
		public static PaymentTypeModel ToModel(PaymentType entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new PaymentTypeModel
			{
				Description = entity.Description,
				IsActive = entity.ActiveInd,
				Name = entity.Name,
				PaymentTypeId = entity.PaymentTypeID
			};

			return model;
		}

		public static PaymentType ToEntity(PaymentTypeModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new PaymentType
			{
				ActiveInd = model.IsActive,
				Description = model.Description,
				Name = model.Name,
				PaymentTypeID = model.PaymentTypeId
			};

			return entity;
		}
	}
}
