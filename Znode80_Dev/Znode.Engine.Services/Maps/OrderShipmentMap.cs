﻿using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
	public static class OrderShipmentMap
	{
		public static OrderShipmentModel ToModel(OrderShipment entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new OrderShipmentModel
			{
				OrderShipmentId = entity.OrderShipmentID,
				ShipName = entity.ShipName,
				ShippingOptionId = entity.ShippingID,
				ShipToCity = entity.ShipToCity,
				ShipToCompanyName = entity.ShipToCompanyName,
				ShipToCountryCode = entity.ShipToCountry,
				ShipToEmail = entity.ShipToEmailID,
				ShipToFirstName = entity.ShipToFirstName,
				ShipToLastName = entity.ShipToLastName,
				ShipToPhoneNumber = entity.ShipToPhoneNumber,
				ShipToPostalCode = entity.ShipToPostalCode,
				ShipToStateCode = entity.ShipToStateCode,
				ShipToStreetAddress1 = entity.ShipToStreet,
				ShipToStreetAddress2 = entity.ShipToStreet1
			};

			return model;
		}

		public static OrderShipment ToEntity(OrderShipmentModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new OrderShipment
			{
				OrderShipmentID = model.OrderShipmentId,
				ShipName = model.ShipName,
				ShippingID = model.ShippingOptionId,
				ShipToCity = model.ShipToCity,
				ShipToCompanyName = model.ShipToCompanyName,
				ShipToCountry = model.ShipToCountryCode,
				ShipToEmailID = model.ShipToEmail,
				ShipToFirstName = model.ShipToFirstName,
				ShipToLastName = model.ShipToLastName,
				ShipToPhoneNumber = model.ShipToPhoneNumber,
				ShipToPostalCode = model.ShipToPostalCode,
				ShipToStateCode = model.ShipToStateCode,
				ShipToStreet = model.ShipToStreetAddress1,
				ShipToStreet1 = model.ShipToStreetAddress2
			};

			return entity;
		}
	}
}
