﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class ThemeMap
    {
        public static ThemeModel ToModel(Theme entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new ThemeModel
            {
               ThemeID=entity.ThemeID,
               Name=entity.Name
            };

            return model;
        }
    }
}
