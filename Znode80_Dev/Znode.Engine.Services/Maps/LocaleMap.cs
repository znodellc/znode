﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class LocaleMap 
    {
        public static LocaleModel ToModel(Locale entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }

            var model = new LocaleModel
                {
                    LocaleID=entity.LocaleID,
                    LocaleCode=entity.LocaleCode,
                    LocaleDescription=entity.LocaleDescription,                   
                };
           
            return model;
       }
    }
}
