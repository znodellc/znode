﻿using System;
using System.Web.Security;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
	public static class UserMap
	{
		public static UserModel ToModel(MembershipUser entity)
		{
			if (entity == null)
			{
				return null;
			}

			// NOTE: MembershipUser doesn't have Password and PasswordAnswer

			var model = new UserModel
			{	
				Comment = entity.Comment,
				CreateDate = entity.CreationDate,
				Email = entity.Email,
				IsApproved = entity.IsApproved,
				IsLockedOut = entity.IsLockedOut,
				IsOnline = entity.IsOnline,
				LastActivityDate = entity.LastActivityDate,
				LastLockoutDate = entity.LastLockoutDate,
				LastLoginDate = entity.LastLoginDate,
				LastPasswordChangedDate = entity.LastPasswordChangedDate,
				PasswordQuestion = entity.PasswordQuestion,
				ProviderName = entity.ProviderName,
				UserId = (Guid)entity.ProviderUserKey,
				Username = entity.UserName
			};

			return model;
		}

		public static MembershipUser ToEntity(UserModel model)
		{
			if (model == null)
			{
				return null;
			}

			// NOTE: MembershipUser doesn't have Password and PasswordAnswer

			var entity = new MembershipUser(
				model.ProviderName,
				model.Username,
				model.UserId,
				model.Email,
				model.PasswordQuestion,
				model.Comment,
				model.IsApproved,
				model.IsLockedOut,
				model.CreateDate,
				model.LastLoginDate,
				model.LastActivityDate,
				model.LastPasswordChangedDate,
				model.LastLockoutDate
			);

			return entity;
		}
	}
}
