﻿using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
	public static class AccountTypeMap
	{
		public static AccountTypeModel ToModel(AccountType entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new AccountTypeModel
			{
				AccountTypeId = entity.AccountTypeID,
				AccountTypeName = entity.AccountTypeNme
			};

			return model;
		}

		public static AccountType ToEntity(AccountTypeModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new AccountType
			{
				AccountTypeID = model.AccountTypeId,
				AccountTypeNme = model.AccountTypeName
			};

			return entity;
		}
	}
}
