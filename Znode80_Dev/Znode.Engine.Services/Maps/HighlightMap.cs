﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services.Maps
{
	public static class HighlightMap
	{
		public static HighlightModel ToModel(Highlight entity)
		{
			if (entity == null)
			{
				return null;
			}

			var imageUtility = new ZNodeImage();

			var model = new HighlightModel
			{
				Description = entity.Description,
				DisplayOrder = entity.DisplayOrder,
				HighlightId = entity.HighlightID,
				HighlightType = HighlightTypeMap.ToModel(entity.HighlightTypeIDSource),
				HighlightTypeId = entity.HighlightTypeID,
				Hyperlink = entity.Hyperlink,
				HyperlinkNewWindow = entity.HyperlinkNewWinInd,
				ImageAltTag = entity.ImageAltTag,
				ImageFile = entity.ImageFile,
				ImageMediumPath = imageUtility.GetImageHttpPathMedium(entity.ImageFile),
				ImageLargePath = imageUtility.GetImageHttpPathLarge(entity.ImageFile),
				ImageSmallPath = imageUtility.GetImageHttpPathSmall(entity.ImageFile),
				ImageSmallThumbnailPath = imageUtility.GetImageHttpPathSmallThumbnail(entity.ImageFile),
				ImageThumbnailPath = imageUtility.GetImageHttpPathThumbnail(entity.ImageFile),
				IsActive = entity.ActiveInd,
				LocaleId = entity.LocaleId,
				Name = entity.Name,
				PortalId = entity.PortalID,
				ShortDescription = entity.ShortDescription,
				ShowPopup = entity.DisplayPopup
			};

			return model;
		}

		public static Highlight ToEntity(HighlightModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new Highlight
			{
				ActiveInd = model.IsActive,
				Description = model.Description,
				DisplayOrder = model.DisplayOrder,
				DisplayPopup = model.ShowPopup,
				HighlightID = model.HighlightId,
				HighlightTypeID = model.HighlightTypeId,
				Hyperlink = model.Hyperlink,
				HyperlinkNewWinInd = model.HyperlinkNewWindow,
				ImageAltTag = model.ImageAltTag,
				ImageFile = model.ImageFile,
				LocaleId = model.LocaleId,
				Name = model.Name,
				PortalID = model.PortalId,
				ShortDescription = model.ShortDescription
			};

			return entity;
		}
	}
}
