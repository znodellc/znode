﻿using System;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;


namespace Znode.Engine.Services.Maps
{
	class AddOnValueMap
	{	

		public static AddOnValueModel ToModel(AddOnValue entity)
		{
			if (entity == null)
			{
				return null;
			}
			
			var model = new AddOnValueModel()
					{
						AddOnValueId = entity.AddOnValueID,
						AddOnId = entity.AddOnID,
						Name = entity.Name,
						Description = entity.Description,
						SKU = entity.SKU,
						IsDefault  = entity.DefaultInd,
						QuantityOnHand =  entity.Inventory != null  && entity.Inventory.QuantityOnHand.HasValue ? entity.Inventory.QuantityOnHand.Value : 0,
						DisplayOrder = entity.DisplayOrder,
						RetailPrice = entity.RetailPrice,
						SalePrice = entity.SalePrice,
						WholesalePrice = entity.WholesalePrice,
						Weight  = entity.Weight,
						Height =  (entity.Height != null) ? entity.Height.Value : 0,
						Width = (entity.Width != null) ? entity.Width.Value : 0,
						Length = (entity.Length != null) ? entity.Length.Value : 0,
						ShippingRuleTypeId = entity.ShippingRuleTypeID,
						FreeShippingInd = entity.FreeShippingInd != null && Convert.ToBoolean(entity.FreeShippingInd),
						SupplierId = entity.SupplierID != null ? entity.SupplierID.Value : 0,
						TaxClassId = entity.TaxClassID != null ? entity.TaxClassID.Value : 0,
						//CustomText = n/a
						RecurringBillingInd = entity.RecurringBillingInd,
						RecurringBillingInstallmentInd = entity.RecurringBillingInstallmentInd,
						RecurringBillingPeriod = entity.RecurringBillingPeriod,
						RecurringBillingFrequency = entity.RecurringBillingFrequency,
						RecurringBillingTotalCycles = entity.RecurringBillingTotalCycles != null ? entity.RecurringBillingTotalCycles.Value : 0,
						RecurringBillingInitialAmount = entity.RecurringBillingInitialAmount != null ? entity.RecurringBillingInitialAmount.Value : 0,
                        
					};

			return model;
			
		}
		

		public static AddOnValue ToEntity(AddOnValueModel model)
		{
			if (model == null)
			{
				return null;
			}
			
			var entity = new AddOnValue()
				{
					AddOnID = model.AddOnId,
					Name =  model.Name,
					Description = model.Description,
					SKU = model.SKU,
					DefaultInd = model.IsDefault,
					DisplayOrder = model.DisplayOrder,
					//ImageFile = n/a,
					//ImageAltTag = n/a,
					RetailPrice = model.RetailPrice,
					SalePrice = model.SalePrice,
					WholesalePrice = model.WholesalePrice,
					RecurringBillingInd =  model.RecurringBillingInd,
					RecurringBillingInstallmentInd = model.RecurringBillingInstallmentInd,
					RecurringBillingPeriod = model.RecurringBillingPeriod,
					RecurringBillingFrequency = model.RecurringBillingFrequency,
					RecurringBillingTotalCycles = model.RecurringBillingTotalCycles,
					RecurringBillingInitialAmount = model.RecurringBillingInitialAmount,
					Weight =  model.Weight,
					Length = model.Length,
					Height = model.Height,
					Width = model.Width,
					ShippingRuleTypeID = model.ShippingRuleTypeId,
					FreeShippingInd = model.FreeShippingInd,
					//WebServiceDownloadDte =  n/a,
					//UpdateDte = n/a
					SupplierID =  model.SupplierId,
					TaxClassID = model.TaxClassId,
					//ExternalProductID = n/a
					//ExternalProductAPIID = n/a
					//ExternalID = n/a
				};
			
			return entity;
		}
	}
}
