﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public class MasterPageMap
    {
        public static MasterPageModel ToModel(MasterPage entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }
            var model = new MasterPageModel
            {
                MasterPageId = entity.MasterPageID,
                Name = entity.Name,
                ThemeId = entity.ThemeID
            };
            return model;
        }

        public static MasterPage ToEntity(MasterPageModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new MasterPage
            {
                MasterPageID = model.MasterPageId,
                Name = model.Name,
                ThemeID = model.ThemeId
            };

            return entity;
        }
    }
}
