﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class TaxRuleTypeMap
	{
		public static TaxRuleTypeModel ToModel(TaxRuleType entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new TaxRuleTypeModel
			{
				ClassName = entity.ClassName,
				Description = entity.Description,
				IsActive = entity.ActiveInd,
				Name = entity.Name,
				PortalId = entity.PortalID,
				TaxRuleTypeId = entity.TaxRuleTypeID
			};

			return model;
		}

		public static TaxRuleType ToEntity(TaxRuleTypeModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new TaxRuleType
			{
				ActiveInd = model.IsActive,
				ClassName = model.ClassName,
				Description = model.Description,
				Name = model.Name,
				PortalID = model.PortalId,
				TaxRuleTypeID = model.TaxRuleTypeId
			};

			return entity;
		}
	}
}
