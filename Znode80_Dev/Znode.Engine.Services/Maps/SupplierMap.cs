﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class SupplierMap
	{
		public static SupplierModel ToModel(Supplier entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new SupplierModel
			{
				ContactEmail = entity.ContactEmail,
				ContactFirstName = entity.ContactFirstName,
				ContactLastName = entity.ContactLastName,
				ContactPhone = entity.ContactPhone,
				Custom1 = entity.Custom1,
				Custom2 = entity.Custom2,
				Custom3 = entity.Custom3,
				Custom4 = entity.Custom4,
				Custom5 = entity.Custom5,
				Description = entity.Description,
				DisplayOrder = entity.DisplayOrder,
				EmailNotificationTemplate = entity.EmailNotificationTemplate,
				EnableEmailNotification = entity.EnableEmailNotification,
				ExternalId = entity.ExternalID,
				ExternalSupplierNumber = entity.ExternalSupplierNo,
				IsActive = entity.ActiveInd,
				Name = entity.Name,
				NotificationEmail = entity.NotificationEmailID,
				PortalId = entity.PortalID,
				SupplierId = entity.SupplierID,
				SupplierType = SupplierTypeMap.ToModel(entity.SupplierTypeIDSource),
				SupplierTypeId = entity.SupplierTypeID
			};

			return model;
		}

		public static Supplier ToEntity(SupplierModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new Supplier
			{
				ActiveInd = model.IsActive,
				ContactEmail = model.ContactEmail,
				ContactFirstName = model.ContactFirstName,
				ContactLastName = model.ContactLastName,
				ContactPhone = model.ContactPhone,
				Custom1 = model.Custom1,
				Custom2 = model.Custom2,
				Custom3 = model.Custom3,
				Custom4 = model.Custom4,
				Custom5 = model.Custom5,
				Description = model.Description,
				DisplayOrder = model.DisplayOrder,
				EmailNotificationTemplate = model.EmailNotificationTemplate,
				EnableEmailNotification = model.EnableEmailNotification,
				ExternalID = model.ExternalId,
				ExternalSupplierNo = model.ExternalSupplierNumber,
				Name = model.Name,
				NotificationEmailID = model.NotificationEmail,
				PortalID = model.PortalId,
				SupplierID = model.SupplierId,
				SupplierTypeID = model.SupplierTypeId
			};

			return entity;
		}
	}
}
