﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class ManufacturerMap
	{
		public static ManufacturerModel ToModel(Manufacturer entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new ManufacturerModel
			{
				Description = entity.Description,
				DisplayOrder = entity.DisplayOrder,
				Custom1 = entity.Custom1,
				Custom2 = entity.Custom2,
				Custom3 = entity.Custom3,
				Email = entity.EmailID,
				EmailNotificationTemplate = entity.EmailNotificationTemplate,
				IsActive = entity.ActiveInd,
				IsDropShipper = entity.IsDropShipper,
				ManufacturerId = entity.ManufacturerID,
				Name = entity.Name,
				PortalId = entity.PortalID,
				Website = entity.WebsiteLink
			};

			return model;
		}

		public static Manufacturer ToEntity(ManufacturerModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new Manufacturer
			{
				ActiveInd = model.IsActive,
				Custom1 = model.Custom1,
				Custom2 = model.Custom2,
				Custom3 = model.Custom3,
				Description = model.Description,
				DisplayOrder = model.DisplayOrder,
				EmailID = model.Email,
				EmailNotificationTemplate = model.EmailNotificationTemplate,
				IsDropShipper = model.IsDropShipper,
				ManufacturerID = model.ManufacturerId,
				Name = model.Name,
				PortalID = model.PortalId,
				WebsiteLink = model.Website
			};

			return entity;
		}
	}
}
