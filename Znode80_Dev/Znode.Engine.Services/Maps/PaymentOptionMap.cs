﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class PaymentOptionMap
	{
		public static PaymentOptionModel ToModel(PaymentSetting entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new PaymentOptionModel
			{
				DisplayOrder = entity.DisplayOrder,
				EnableAmericanExpress = entity.EnableAmex,
				EnableDiscover = entity.EnableDiscover,
				EnableMasterCard = entity.EnableMasterCard,
				EnableRecurringPayments = entity.EnableRecurringPayments,
				EnableVault = entity.EnableVault,
				EnableVisa = entity.EnableVisa,
				IsActive = entity.ActiveInd,
				IsRmaCompatible = entity.IsRMACompatible,
				Partner = entity.Partner,
				PaymentGateway = PaymentGatewayMap.ToModel(entity.GatewayTypeIDSource),
				PaymentGatewayId = entity.GatewayTypeID,
				PaymentGatewayPassword = entity.GatewayPassword,
				PaymentGatewayUsername = entity.GatewayUsername,
				PaymentOptionId = entity.PaymentSettingID,
				PaymentType = PaymentTypeMap.ToModel(entity.PaymentTypeIDSource),
				PaymentTypeId = entity.PaymentTypeID,
				PreAuthorize = entity.PreAuthorize,
				ProfileId = entity.ProfileID,
				TestMode = entity.TestMode,
				TransactionKey = entity.TransactionKey,
				Vendor = entity.Vendor
			};

			return model;
		}

		public static PaymentSetting ToEntity(PaymentOptionModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new PaymentSetting
			{
				ActiveInd = model.IsActive,
				DisplayOrder = model.DisplayOrder,
				EnableAmex = model.EnableAmericanExpress,
				EnableDiscover = model.EnableDiscover,
				EnableMasterCard = model.EnableMasterCard,
				EnableRecurringPayments = model.EnableRecurringPayments,
				EnableVault = model.EnableVault,
				EnableVisa = model.EnableVisa,
				GatewayPassword = model.PaymentGatewayPassword,
				GatewayTypeID = model.PaymentGatewayId,
				GatewayUsername = model.PaymentGatewayUsername,
				IsRMACompatible = model.IsRmaCompatible,
				Partner = model.Partner,
				PaymentSettingID = model.PaymentOptionId,
				PaymentTypeID = model.PaymentTypeId,
				PreAuthorize = model.PreAuthorize,
				ProfileID= model.ProfileId,
				TestMode = model.TestMode,
				TransactionKey = model.TransactionKey,
				Vendor = model.Vendor
			};

			return entity;
		}
	}
}
