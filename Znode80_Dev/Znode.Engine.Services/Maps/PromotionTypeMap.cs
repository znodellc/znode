﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class PromotionTypeMap
	{
		public static PromotionTypeModel ToModel(DiscountType entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new PromotionTypeModel
			{
				ClassName = entity.ClassName,
				ClassType = entity.ClassType,
				Description = entity.Description,
				IsActive = entity.ActiveInd,
				Name = entity.Name,
				PromotionTypeId = entity.DiscountTypeID
			};

			return model;
		}

		public static DiscountType ToEntity(PromotionTypeModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new DiscountType
			{
				ActiveInd = model.IsActive,
				ClassName = model.ClassName,
				ClassType = model.ClassType,
				Description = model.Description,
				DiscountTypeID = model.PromotionTypeId,
				Name = model.Name
			};

			return entity;
		}
	}
}
