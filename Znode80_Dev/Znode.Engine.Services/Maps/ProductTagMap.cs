﻿using System;
using System.Collections.Generic;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Api.Models;
using System.Linq;

namespace Znode.Engine.Services.Maps
{
    public static class ProductTagMap
    {
        public static ProductTagsModel ToModel(Tags entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }
            var model = new ProductTagsModel()
            {
                ProductId = entity.ProductID,
                ProductTags = entity.Tags,
                TagId = entity.TagID,
            };
            
            return model;
        }

        public static ProductTagsModel ToModel(TList<Tags> entity)
        {
            if (Equals(entity, null) || Equals(entity.Count, 0))
            {
                return null;
            }
            var model = new ProductTagsModel()
            {
                ProductId = entity.FirstOrDefault().ProductID,
                ProductTags = (!Equals(entity, null) && entity.Count > 0) ? entity.Select(i => i.Tags).Aggregate((i, j) => i + "," + j) : string.Empty,
                TagId = entity.FirstOrDefault().TagID,
            };

            return model;
        }

        public static Tags ToEntity(ProductTagsModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new Tags
            {
                Tags = model.ProductTags,
                ProductID = model.ProductId,
                TagID = model.TagId,
            };

            return entity;
        }
    }
}
