﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class ShippingRuleTypeMap
	{
		public static ShippingRuleTypeModel ToModel(ShippingRuleType entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new ShippingRuleTypeModel
			{
				ClassName = entity.ClassName,
				Description = entity.Description,
				IsActive = entity.ActiveInd,
				Name = entity.Name,
				ShippingRuleTypeId = entity.ShippingRuleTypeID
			};

			return model;
		}

		public static ShippingRuleType ToEntity(ShippingRuleTypeModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new ShippingRuleType
			{
				ActiveInd = model.IsActive,
				ClassName = model.ClassName,
				Description = model.Description,
				Name = model.Name,
				ShippingRuleTypeID = model.ShippingRuleTypeId
			};

			return entity;
		}
	}
}
