﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class CreditCardTypeMap
	{
		public static CreditCardTypeModel? ToModel(CreditCardType? entity)
		{
			if (entity.HasValue)
			{
				if (entity.Value == CreditCardType.Amex) return CreditCardTypeModel.AmericanExpress;
				if (entity.Value == CreditCardType.Diners) return CreditCardTypeModel.Diners;
				if (entity.Value == CreditCardType.Discover) return CreditCardTypeModel.Discover;
				if (entity.Value == CreditCardType.JCB) return CreditCardTypeModel.Jcb;
				if (entity.Value == CreditCardType.Maestro) return CreditCardTypeModel.Maestro;
				if (entity.Value == CreditCardType.MasterCard) return CreditCardTypeModel.MasterCard;
				if (entity.Value == CreditCardType.Solo) return CreditCardTypeModel.Solo;
				if (entity.Value == CreditCardType.Visa) return CreditCardTypeModel.Visa;	
			}

			return null;
		}

		public static CreditCardType? ToEntity(CreditCardTypeModel? model)
		{
			if (model.HasValue)
			{
				if (model.Value == CreditCardTypeModel.AmericanExpress) return CreditCardType.Amex;
				if (model.Value == CreditCardTypeModel.Diners) return CreditCardType.Diners;
				if (model.Value == CreditCardTypeModel.Discover) return CreditCardType.Discover;
				if (model.Value == CreditCardTypeModel.Jcb) return CreditCardType.JCB;
				if (model.Value == CreditCardTypeModel.Maestro) return CreditCardType.Maestro;
				if (model.Value == CreditCardTypeModel.MasterCard) return CreditCardType.MasterCard;
				if (model.Value == CreditCardTypeModel.Solo) return CreditCardType.Solo;
				if (model.Value == CreditCardTypeModel.Visa) return CreditCardType.Visa;
			}

			return null;
		}
	}
}
