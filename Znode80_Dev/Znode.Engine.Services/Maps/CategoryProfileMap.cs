﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
    public static class CategoryProfileMap
    {
        public static CategoryProfileModel ToModel(CategoryProfile entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new CategoryProfileModel
            {
                CategoryProfileID = entity.CategoryProfileID,
                CategoryID = entity.CategoryID,
                ProfileID =  entity.ProfileID,
                EffectiveDate =  entity.EffectiveDate
            };

            return model;
        }

        public static CategoryProfile ToEntity(CategoryProfileModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new CategoryProfile
            {
                CategoryProfileID = model.CategoryProfileID,
                CategoryID = model.CategoryID,
                ProfileID = model.ProfileID,
                EffectiveDate = model.EffectiveDate
            };

            return entity;
        }
    }
}
