﻿using System;
using System.Data;
using System.Web;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Services.Maps
{
    public static class CategoryMap
    {
        public static CategoryModel ToModel(Category entity)
        {
            if (entity == null)
            {
                return null;
            }

            var imageUtility = new ZNodeImage();

            var model = new CategoryModel
            {
                AlternateDescription = entity.AlternateDescription,
                CategoryId = entity.CategoryID,
                CategoryUrl = ZNodeSEOUrl.MakeUrlForMvc(entity.CategoryID.ToString(), SEOUrlType.Category, entity.SEOURL),
                CreateDate = entity.CreateDate,
                Custom1 = entity.Custom1,
                Custom2 = entity.Custom2,
                Custom3 = entity.Custom3,
                Description = entity.Description,
                DisplayOrder = entity.DisplayOrder,
                ExternalId = entity.ExternalID,
                ImageAltTag = entity.ImageAltTag,
                ImageFile = entity.ImageFile,
                ImageMediumPath = imageUtility.GetImageHttpPathMedium(entity.ImageFile),
                ImageLargePath = imageUtility.GetImageHttpPathLarge(entity.ImageFile),
                ImageSmallPath = imageUtility.GetImageHttpPathSmall(entity.ImageFile),
                ImageSmallThumbnailPath = imageUtility.GetImageHttpPathSmallThumbnail(entity.ImageFile),
                ImageThumbnailPath = imageUtility.GetImageHttpPathThumbnail(entity.ImageFile),
                IsVisible = entity.VisibleInd,
                Name = HttpUtility.HtmlDecode(entity.Name),
                ProductIds = entity.ProductIds,
                SeoDescription = string.IsNullOrEmpty(entity.SEODescription) ? ZNodeConfigManager.SiteConfig.SeoDefaultCategoryDescription : entity.SEODescription,
                SeoKeywords = string.IsNullOrEmpty(entity.SEOKeywords) ? ZNodeConfigManager.SiteConfig.SeoDefaultCategoryKeyword : entity.SEOKeywords,
                SeoTitle = string.IsNullOrEmpty(entity.SEOTitle) ? ZNodeConfigManager.SiteConfig.SeoDefaultCategoryTitle : entity.SEOTitle,
                SeoUrl = entity.SEOURL,
                ShortDescription = entity.ShortDescription,
                SubcategoryGridIsVisible = entity.SubCategoryGridVisibleInd,
                Title = HttpUtility.HtmlDecode(entity.Title),
                UpdateDate = entity.UpdateDate,
                //Znode version 7.2.2 To map Category Banner
                CategoryBanner = entity.CategoryBanner
            };

            foreach (var node in entity.CategoryNodeCollection)
            {
                // Add to category nodes list
                model.CategoryNodes.Add(CategoryNodeMap.ToModel(node));

                // Also add to subcategories list
                if (node.CategoryIDSource != null)
                {
                    model.Subcategories.Add(ToModel(node.CategoryIDSource));
                }
            }

            foreach (var profile in entity.CategoryProfileCollection)
            {
                model.CategoryProfiles.Add(CategoryProfileMap.ToModel(profile));
            }

            return model;
        }


        public static Category ToEntity(CategoryModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new Category
            {
                AlternateDescription = model.AlternateDescription,
                CategoryBanner = model.CategoryBanner,
                CategoryID = model.CategoryId,
                CategoryNodeCollection = new TList<CategoryNode>(),
                CreateDate = model.CreateDate,
                Description = model.Description,
                Custom1 = model.Custom1,
                Custom2 = model.Custom2,
                Custom3 = model.Custom3,
                DisplayOrder = model.DisplayOrder,
                ExternalID = model.ExternalId,
                ImageAltTag = model.ImageAltTag,
                ImageFile = model.ImageFile,
                Name = model.Name,
                SEODescription = model.SeoDescription,
                SEOKeywords = model.SeoKeywords,
                SEOTitle = model.SeoTitle,
                SEOURL = model.SeoUrl,
                ShortDescription = model.ShortDescription,
                SubCategoryGridVisibleInd = model.SubcategoryGridIsVisible,
                Title = model.Title,
                UpdateDate = model.UpdateDate,
                VisibleInd = model.IsVisible
            };

            foreach (var node in model.CategoryNodes)
            {
                entity.CategoryNodeCollection.Add(CategoryNodeMap.ToEntity(node));
            }

            return entity;
        }

        #region Znode Version 8.0
        /// <summary>
        /// Maps Dataset to list of type CatalogAssociatedCategoriesModel.
        /// </summary>
        /// <param name="result">Dataset</param>
        /// <returns>Returns mapped list of type CatalogAssociatedCategoriesModel.</returns>
        public static CatalogAssociatedCategoriesListModel ToModelList(DataSet result)
        {
            if (result == null)
            {
                return null;
            }


            var modelList = new CatalogAssociatedCategoriesListModel();
            foreach (DataRow dr in result.Tables[0].Rows)
            {
                CatalogAssociatedCategoriesModel model = new CatalogAssociatedCategoriesModel
                {
                    CategoryNodeId = dr.Table.Columns.Contains("CategoryNodeId") ? Convert.ToInt32(dr["CategoryNodeId"]) : 0,
                    CatalogId = dr.Table.Columns.Contains("CatalogId") ? Convert.ToInt32(dr["CatalogId"]) : 0,
                    CategoryId = (dr["CategoryId"] != DBNull.Value) ? Convert.ToInt32(dr["CategoryId"]) : 0,
                    Name = (dr["Name"] != DBNull.Value) ? dr["Name"].ToString() : string.Empty,
                    ParentCategoryNodeId = dr.Table.Columns.Contains("ParentCategoryNodeId") && dr["ParentCategoryNodeId"] != DBNull.Value ? Convert.ToInt32(dr["ParentCategoryNodeId"]) : 0,
                    SeoUrl = (dr["SeoUrl"] != DBNull.Value) ? dr["SeoUrl"].ToString() : string.Empty,
                    ActiveInd = dr.Table.Columns.Contains("ActiveInd") ? Convert.ToBoolean(dr["ActiveInd"]):dr.Table.Columns.Contains("VisibleInd")?Convert.ToBoolean(dr["VisibleInd"]):false,                    
                    DisplayOrder = (dr["DisplayOrder"] != DBNull.Value) ? Convert.ToInt32(dr["DisplayOrder"]) : 99,
                    CatalogName=dr.Table.Columns.Contains("CatalogName") ?dr["CatalogName"].ToString():string.Empty
                };
                modelList.CategoryList.Add(model);

            }

            return modelList;
        }
        #endregion
    }
}
