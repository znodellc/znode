﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class TierMap
	{
		public static TierModel ToModel(ProductTier entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new TierModel
			{
				Price = entity.Price,
				ProductId = entity.ProductID,
				ProductTierId = entity.ProductTierID,
				ProfileId = entity.ProfileID,
				TierEnd = entity.TierEnd,
				TierStart = entity.TierStart
			};

			return model;
		}

		public static ProductTier ToEntity(TierModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new ProductTier
			{
				Price = model.Price,
				ProductID = model.ProductId,
				ProductTierID = model.ProductTierId,
				ProfileID = model.ProfileId,
				TierEnd = model.TierEnd,
				TierStart = model.TierStart
			};

			return entity;
		}
	}
}
