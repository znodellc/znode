﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class AuditMap
	{
		public static AuditModel ToModel(SourceModificationAudit entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new AuditModel
			{
				AuditId = entity.Id,
				AuditSourceType = AuditSourceTypeMap.ToModel(entity.SourceTypeIdSource),
				AuditSourceTypeId = entity.SourceTypeId,
				AuditStatus = AuditStatusMap.ToModel(entity.StatusIdSource),
				AuditStatusId = entity.StatusId,
				AuditType = AuditTypeMap.ToModel(entity.ModificationTypeIdSource),
				AuditTypeId = entity.ModificationTypeId,
				CreateDate = entity.CreatedDate,
				CreatedBy = entity.CreatedBy,
				ExternalId = entity.ExternalId,
				ProcessedDate = entity.ProcessedDate,
				SourceId = entity.SourceId,
                Product = ProductMap.ToModel(entity.Product),
                Category = CategoryMap.ToModel(entity.Category),
                Catalog = CatalogMap.ToModel(entity.Catalog),
                Portal = PortalMap.ToModel(entity.Portal)
			};

			return model;
		}

		public static SourceModificationAudit ToEntity(AuditModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new SourceModificationAudit
			{
				CreatedDate = model.CreateDate,
				CreatedBy = model.CreatedBy,
				ExternalId = model.ExternalId,
				Id = model.AuditId,
				ModificationTypeId = model.AuditTypeId,
				ProcessedDate = model.ProcessedDate,
				SourceId = model.SourceId,
				SourceTypeId = model.AuditSourceTypeId,
				StatusId = model.AuditStatusId
			};

			return entity;
		}
	}
}
