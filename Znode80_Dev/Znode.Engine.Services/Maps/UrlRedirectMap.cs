﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class UrlRedirectMap
    {
        public static UrlRedirectModel ToModel(UrlRedirect entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new UrlRedirectModel
                {
                    IsActive = entity.IsActive,
                    NewUrl = entity.NewUrl,
                    OldUrl = entity.OldUrl,
                    UrlRedirectId = entity.UrlRedirectID

                };
            return model;
        }

        public static UrlRedirect ToEntity(UrlRedirectModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new UrlRedirect
            {
                IsActive = model.IsActive,
                NewUrl = model.NewUrl,
                OldUrl = model.OldUrl,
                UrlRedirectID = model.UrlRedirectId
            };

            return entity;
        }
    }
}
