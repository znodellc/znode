﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Entities;
using ZNodeShoppingCartItem = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem;
using ZNodeShoppingCart = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart;
using ZNodePortalCart = ZNode.Libraries.ECommerce.ShoppingCart.ZNodePortalCart;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Services.Maps
{
	public static class ShoppingCartItemMap
	{
		public static ShoppingCartItemModel ToModel(ZNodeShoppingCartItem znodeCartItem, ZNodeShoppingCart znodeCart)
		{
			if (znodeCartItem == null)
			{
				return null;
			}
            //ZNode Version 7.2.2 - Add ImageUtility to Get Image Path
            var imageUtility = new ZNodeImage();
			var model = new ShoppingCartItemModel
			{
				AddOnValuesCustomText = znodeCartItem.AddOnValuesCustomText,
				AddOnValueIds = znodeCartItem.AddOnValueIds,
				AttributeIds = znodeCartItem.AttributeIds,
				Description = znodeCartItem.PromoDescription,	  
				ExtendedPrice = znodeCartItem.ExtendedPrice,
				ExternalId = znodeCartItem.ExternalId,
				ProductId = znodeCartItem.Product.ProductID,
				Quantity = znodeCartItem.Quantity,
				ShippingCost = znodeCartItem.ShippingCost,
				ShippingOptionId = znodeCartItem.ShippingOptionId,
				Sku = znodeCartItem.Product.SelectedSKU.SKU,
				SkuId = znodeCartItem.Product.SelectedSKU.SKUID,
				CartDescription = znodeCartItem.Product.ShoppingCartDescription,
				UnitPrice = znodeCartItem.UnitPrice,
                BundleItems = ToBundleItemModel(znodeCartItem),
                MultipleShipToAddress = GetOrderShipmentForModel(znodeCartItem, znodeCart),
                TaxCost = znodeCartItem.TaxCost,
                //ZNode Version 7.2.2 - Bind SKU Image Path Url
                ImagePath = imageUtility.GetImageHttpPathLarge(znodeCartItem.Product.SelectedSKU.SKUPicturePath),
			};
			
			return model;
		}
        
		public static ZNodeShoppingCartItem ToZnodeShoppingCartItem(ShoppingCartItemModel model, AddressModel shippingAddress)
		{
			if (model == null)
			{
				return null;
			}

			var znodeCartItem = new ZNodeShoppingCartItem
			{
				AddOnValuesCustomText = model.AddOnValuesCustomText,
				AddOnValueIds = model.AddOnValueIds,
				AttributeIds = model.AttributeIds,
				ExternalId = model.ExternalId,
				Quantity = model.Quantity,
				ShippingCost = model.ShippingCost,
				ShippingOptionId = model.ShippingOptionId
			};

			var addressId = 0;

			// Cart level shipping address
			if (shippingAddress != null)
			{
				addressId = shippingAddress.AddressId;
			}

            if (model.MultipleShipToAddress != null && model.MultipleShipToAddress.Any())
            {
				foreach (var shipToAddress in model.MultipleShipToAddress)
				{
					if (shipToAddress.AddressId == 0)
					{
						shipToAddress.AddressId = addressId;
					}

					var znodeOrderShipment = new ZNodeOrderShipment(shipToAddress.AddressId, shipToAddress.Quantity, znodeCartItem.GUID,
					                                                shipToAddress.ShippingOptionId.GetValueOrDefault(0),shipToAddress.ShippingName);
					
					znodeCartItem.OrderShipments.Add(znodeOrderShipment);
				}

            }
            else
            {
		        // Cart item level shipping address
		        if (model.ShippingAddress != null)
		        {
		            addressId = model.ShippingAddress.AddressId;
		        }

		        var orderShipment = new ZNodeOrderShipment(addressId, model.Quantity, znodeCartItem.GUID);

		        znodeCartItem.OrderShipments.Add(orderShipment);
		    }

		    var productHelper = new ProductHelper();

			// Try to get by external ID
			if (model.ProductId > 0)
			{
				znodeCartItem.Product = ProductMap.ToZnodeProductBase(model.ProductId, model.Sku, model.AddOnValueIds, model.AddOnValuesCustomText);

			    znodeCartItem.Product.ZNodeBundleProductCollection = GetZnodeProductBundles(model.BundleItems);

			}
			else if (string.IsNullOrEmpty(model.ExternalId))
			{
				znodeCartItem.Product = ProductMap.ToZnodeProductBase(Convert.ToInt32(productHelper.GetProductIDByExternalID(model.ExternalId)), model.Sku, 
																		model.AddOnValueIds, model.AddOnValuesCustomText);

			    if (model.BundleItems != null)
			    {

			        foreach (var bundleItem in model.BundleItems)
			        {
			            znodeCartItem.Product.ZNodeBundleProductCollection.Add(
			                new ZNodeBundleProductEntity(
			                    ProductMap.ToZnodeProductBase(
			                        Convert.ToInt32(productHelper.GetProductIDByExternalID(bundleItem.ExternalId)),
			                        bundleItem.Sku,
			                        bundleItem.AddOnValueIds,
									bundleItem.AddOnValuesCustomText)));
			        }
			    }
			}
           
			return znodeCartItem;
		}

        private static ZNodeGenericCollection<ZNodeBundleProductEntity> GetZnodeProductBundles(IEnumerable<BundleItemModel> bundles)
	    {
            var bundleCollection = new ZNodeGenericCollection<ZNodeBundleProductEntity>();
            if (bundles != null)
            {
                foreach (var bundleItem in bundles)
                {
                    bundleCollection.Add(
                        new ZNodeBundleProductEntity(ProductMap.ToZnodeProductBase(bundleItem.ProductId, bundleItem.Sku,
                                                                                   bundleItem.AddOnValueIds,
																				   bundleItem.AddOnValuesCustomText)));
                }
            }
            return bundleCollection;
	    }

        public static Collection<BundleItemModel> ToBundleItemModel(ZNodeShoppingCartItem model)
        {
            if (model.Product.ZNodeBundleProductCollection == null)
                return null;
			
            var items = new Collection<BundleItemModel>();

            foreach (ZNodeBundleProductEntity bundleItem in model.Product.ZNodeBundleProductCollection)
            {
	            
                var itemModel = new BundleItemModel()
                    {
                       AddOnValueIds = ToAddOnValueIdArray(bundleItem.SelectedAddOns.SelectedAddOnValueIds),
                       ProductId = bundleItem.ProductID,
                       Sku = bundleItem.SelectedSKUvalue.SKU,
					   AddOnValuesCustomText = ToAddOnValueCustomTextDictionary(bundleItem.SelectedAddOns.AddOnCollection)
                    };

                items.Add(itemModel);
            }

            return items;
        }

		private static int[] ToAddOnValueIdArray(string addOnValueIdString)
		{
			return !string.IsNullOrEmpty(addOnValueIdString)
				       ? addOnValueIdString.Split(',').Select(int.Parse).ToArray()
				       : new int[0];
		}


		private static Dictionary<int, string> ToAddOnValueCustomTextDictionary(ZNodeGenericCollection<ZNodeAddOnEntity> addOnCollection)
		{
			var textboxAddOns = addOnCollection.Cast<ZNodeAddOnEntity>()
			                                  .Where(addOnEntity => addOnEntity.DisplayType.Equals("TextBox", StringComparison.OrdinalIgnoreCase)).ToList();

			if (textboxAddOns.Any())
			{
				return textboxAddOns.SelectMany(addOnEntity => addOnEntity.AddOnValueCollection.Cast<ZNodeAddOnValueEntity>())
										.ToDictionary(addOnValueEntity => addOnValueEntity.AddOnValueID, addOnValueEntity => addOnValueEntity.CustomText);
			}

			return null;
		}


		// For Multiple Ship to address mapping

        private static List<OrderShipmentModel> GetOrderShipmentForModel(ZNodeShoppingCartItem item, ZNodeShoppingCart znodeCart)
        {
            // Return to handle Multiple ship to address on client side.                        
            var resultItems = item.OrderShipments.GroupBy(x => new { x.AddressID, x.ShippingID })
                    .Select(x => new OrderShipmentModel() { AddressId = x.Key.AddressID, ShippingOptionId = x.Key.ShippingID, Quantity = x.Sum(y => y.Quantity) })
                    .ToList();
        
            return resultItems;
        }

        public static ShoppingCartItemModel ToModel(ZNodeShoppingCartItem znodeCartItem, ZNodePortalCart znodeCart)
        {
            if (znodeCartItem == null)
            {
                return null;
            }

            var model = new ShoppingCartItemModel
            {
                AddOnValuesCustomText = znodeCartItem.AddOnValuesCustomText,
                AddOnValueIds = znodeCartItem.AddOnValueIds,
                AttributeIds = znodeCartItem.AttributeIds,
                Description = znodeCartItem.PromoDescription,
                ExtendedPrice = znodeCartItem.ExtendedPrice,
                ExternalId = znodeCartItem.ExternalId,
                ProductId = znodeCartItem.Product.ProductID,
                Quantity = znodeCartItem.Quantity,
                ShippingCost = znodeCartItem.ShippingCost,
                ShippingOptionId = znodeCartItem.ShippingOptionId,
                Sku = znodeCartItem.Product.SelectedSKU.SKU,
                SkuId = znodeCartItem.Product.SelectedSKU.SKUID,
                CartDescription = znodeCartItem.Product.ShoppingCartDescription,
                UnitPrice = znodeCartItem.UnitPrice,
                BundleItems = ToBundleItemModel(znodeCartItem),
                MultipleShipToAddress = GetOrderShipmentForModel(znodeCartItem, znodeCart),
                TaxCost = znodeCartItem.TaxCost
            };

            return model;
        }


        private static List<OrderShipmentModel> GetOrderShipmentForModel(ZNodeShoppingCartItem item, ZNodePortalCart znodeCart)
        {
            // Return to handle Multiple ship to address on client side.                        
            var orderShipmentModels = item.OrderShipments.GroupBy(x => new { x.AddressID, x.ShippingID, x.ShippingName })
                    .Select(x => new OrderShipmentModel() { AddressId = x.Key.AddressID, ShippingOptionId = x.Key.ShippingID, ShippingName = x.Key.ShippingName, Quantity = x.Sum(y => y.Quantity) })
                    .ToList();

            var addressCarts = znodeCart.AddressCarts.ToList();
			
			foreach (var orderShipmentModel in orderShipmentModels)
			{
				var addressItem = addressCarts.FirstOrDefault(y => y.AddressID == orderShipmentModel.AddressId);
						if (addressItem == null) continue;
						orderShipmentModel.ShippingCost = addressItem.ShippingCost;
						orderShipmentModel.TaxCost = addressItem.TaxCost;
			}
			

            return orderShipmentModels;
        }
	}
}
