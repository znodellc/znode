﻿using System;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class CrossSellMap
	{
		public static CrossSellModel ToModel(ProductCrossSell entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new CrossSellModel
			{
				DisplayOrder = entity.DisplayOrder,
				ProductCrossSellTypeId = entity.ProductCrossSellTypeId,
				ProductId = entity.ProductId,
				RelatedProductId = entity.RelatedProductId,
				RelationTypeId = entity.RelationTypeId,
                ProductRelation = (ProductRelationCode)Enum.ToObject(typeof(ProductRelationCode), entity.RelationTypeId)
			};

			return model;
		}

		public static ProductCrossSell ToEntity(CrossSellModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new ProductCrossSell
			{
				DisplayOrder = model.DisplayOrder,
				ProductCrossSellTypeId = model.ProductCrossSellTypeId,
				ProductId = model.ProductId,
				RelatedProductId = model.RelatedProductId,
				RelationTypeId = model.RelationTypeId
			};

			return entity;
		}
	}
}
