﻿using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services.Maps
{
	public static class ProfileMap
	{
		public static ProfileModel ToModel(Profile entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new ProfileModel
			{
				EmailList = entity.EmailList,
				ExternalId = entity.DefaultExternalAccountNo,
				Name = entity.Name,
				ProfileId = entity.ProfileID,
				ShowOnPartnerSignup = entity.ShowOnPartnerSignup,
				ShowPricing = entity.ShowPricing,
				TaxClassId = entity.TaxClassID,
				TaxExempt = entity.TaxExempt,
				UseWholesalePricing = entity.UseWholesalePricing,
				Weighting = entity.Weighting
			};

			return model;
		}

		public static Profile ToEntity(ProfileModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new Profile
			{
				DefaultExternalAccountNo = model.ExternalId,
				EmailList = model.EmailList,
				Name = model.Name,
				ProfileID = model.ProfileId,
				ShowOnPartnerSignup = model.ShowOnPartnerSignup,
				ShowPricing = model.ShowPricing,
				TaxClassID = model.TaxClassId,
				TaxExempt = model.TaxExempt,
				UseWholesalePricing = model.UseWholesalePricing,
				Weighting = model.Weighting
			};

			return entity;
		}
	}
}
