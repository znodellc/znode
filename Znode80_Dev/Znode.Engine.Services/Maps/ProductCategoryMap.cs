﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class ProductCategoryMap
	{
		public static ProductCategoryModel ToModel(ProductCategory entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new ProductCategoryModel
			{
				BeginDate = entity.BeginDate,
				Category = CategoryMap.ToModel(entity.CategoryIDSource),
				CategoryId = entity.CategoryID,
				CssId = entity.CSSID,
				DisplayOrder = entity.DisplayOrder,
				EndDate = entity.EndDate,
				IsActive = entity.ActiveInd,
				MasterPageId = entity.MasterPageID,
				Product = ProductMap.ToModel(entity.ProductIDSource),
				ProductCategoryId = entity.ProductCategoryID,
				ProductId = entity.ProductID,
				ThemeId = entity.ThemeID
			};

			return model;
		}

		public static ProductCategory ToEntity(ProductCategoryModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new ProductCategory
			{
				ActiveInd = model.IsActive,
				BeginDate = model.BeginDate,
				CategoryID = model.CategoryId,
				CSSID = model.CssId,
				DisplayOrder = model.DisplayOrder,
				EndDate = model.EndDate,
				MasterPageID = model.MasterPageId,
				ProductCategoryID = model.ProductCategoryId,
				ProductID = model.ProductId,
				ThemeID = model.ThemeId
			};

			return entity;
		}
	}
}
