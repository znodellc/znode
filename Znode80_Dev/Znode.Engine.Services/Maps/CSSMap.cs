﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public class CSSMap
    {
        public static CSSModel ToModel(CSS entity)
        {
            if (entity == null)
            {
                return null;
            }
            var model = new CSSModel
                {
                    CSSID = entity.CSSID,
                    ThemeID = entity.ThemeID,
                    Name = entity.Name
                };
            return model;
        }
    }
}
