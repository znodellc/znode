﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;
using ZNode.Libraries.Search.Interfaces;

namespace Znode.Engine.Services.Maps
{
	public static class SuggestedSearchMap
	{
		public static SuggestedSearchListModel ToModel(List<TypeAheadResponse> entities)
		{
			var model = new SuggestedSearchListModel();
			if (entities != null && entities.Count > 0)
			{
				foreach (var typeAheadResponse in entities)
				{
					model.SuggestedSearchResults.Add(new SuggestedSearchModel { ProductName = typeAheadResponse.Name, CategoryName = typeAheadResponse.Category });
				}
			}

			return model;
		}
	}
}
