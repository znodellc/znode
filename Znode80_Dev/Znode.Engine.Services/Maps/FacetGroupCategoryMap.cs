﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public class FacetGroupCategoryMap
    {
        public static FacetGroupCategoryModel ToModel(FacetGroupCategory entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }
            var model = new FacetGroupCategoryModel
            {
                CategoryDisplayOrder = entity.CategoryDisplayOrder,
                CategoryID = entity.CategoryID,
                FacetGroupID = entity.FacetGroupID,
            };
            return model;
        }

        public static FacetGroupCategoryListModel ToModel(TList<FacetGroupCategory> entity)
        {
            if (Equals(entity, null))
            {
                return null;
            }
            var model = new FacetGroupCategoryListModel();
            foreach (var a in entity)
            {
                model.FacetGroupCategoryList.Add(FacetGroupCategoryMap.ToModel(a));
            }
            return model;
        }
    }
}
