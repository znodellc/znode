﻿using System;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using System.Linq;

namespace Znode.Engine.Services.Maps
{
    public static class FacetControlTypeMap
    {
        public static FacetControlTypeModel ToModel(FacetControlType entity)
        {
            if (Equals(entity,null))
            {
                return null;
            }

            var model = new FacetControlTypeModel
            {
                ControlTypeID = entity.ControlTypeID,
                ControlName=entity.ControlName,
                ControlDescription=entity.ControlDescription,
            };

            return model;
        }
    }
}
