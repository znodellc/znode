﻿using System;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using System.Linq;

namespace Znode.Engine.Services.Maps
{
    public static class AccountMap
    {
        public static AccountModel ToModel(Account entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new AccountModel
                {
                    AccountId = entity.AccountID,
                    AccountProfileCode = entity.AccountProfileCode,
                    AccountType = AccountTypeMap.ToModel(entity.AccountTypeIDSource),
                    AccountTypeId = entity.AccountTypeID,
                    CompanyName = entity.CompanyName,
                    CreateDate = entity.CreateDte,
                    CreateUser = entity.CreateUser,
                    Custom1 = entity.Custom1,
                    Custom2 = entity.Custom2,
                    Custom3 = entity.Custom3,
                    Description = entity.Description,
                    Email = entity.Email,
                    EmailOptIn = entity.EmailOptIn,
                    EnableCustomerPricing = entity.EnableCustomerPricing,
                    ExternalId = entity.ExternalAccountNo,
                    IsActive = entity.ActiveInd,
                    ParentAccountId = entity.ParentAccountID,
                    ProfileId = entity.ProfileID,
                    ReferralAccountId = entity.ReferralAccountID,
                    ReferralCommission = entity.ReferralCommission,
                    ReferralCommissionTypeId = entity.ReferralCommissionTypeID,
                    ReferralStatus = entity.ReferralStatus,
                    Source = entity.Source,
                    SubAccountLimit = entity.SubAccountLimit,
                    TaxId = entity.TaxID,
                    UpdateDate = entity.UpdateDte,
                    UpdateUser = entity.UpdateUser,
                    User = UserMap.ToModel(entity.User),
                    UserId = entity.UserID,
                    WebServiceDownloadDate = entity.WebServiceDownloadDte,
                    Website = entity.Website
                };

            foreach (var a in entity.AddressCollection)
            {
                model.Addresses.Add(AddressMap.ToModel(a));
            }

            foreach (var o in entity.OrderCollectionGetByAccountID)
            {
                model.Orders.Add(OrderMap.ToModel(o));
            }

            foreach (var ap in entity.AccountProfileCollection)
            {
                model.Profiles.Add(ProfileMap.ToModel(ap.ProfileIDSource));
            }

            foreach (var w in entity.WishListCollection)
            {
                model.WishList.Add(WishListMap.ToModel(w));
            }

            if (entity.GiftCardCollection != null)
            {
                foreach (var giftcard in entity.GiftCardCollection)
                {
                    foreach (var history in giftcard.GiftCardHistoryCollection)
                    {
                        var historyModel = GiftCardHistoryMap.ToModel(history);
                        historyModel.GiftCardNumber = giftcard.CardNumber;
                        model.GiftCardHistoryList.Add(historyModel);
                    }

                }
            }
            return model;
        }

        public static Account ToEntity(AccountModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new Account
			{
				AccountID = model.AccountId,
				AccountProfileCode = model.AccountProfileCode,
				AccountTypeID = model.AccountTypeId,
				ActiveInd = model.IsActive,
				CompanyName = model.CompanyName,
				CreateDte = model.CreateDate ?? DateTime.Now,
				CreateUser = model.CreateUser,
				Custom1 = model.Custom1,
				Custom2 = model.Custom2,
				Custom3 = model.Custom3,
				Description = model.Description,
				Email = model.Email,
				EmailOptIn = model.EmailOptIn,
				EnableCustomerPricing = model.EnableCustomerPricing,
				ExternalAccountNo = model.ExternalId,
				ParentAccountID = model.ParentAccountId,
				ProfileID = model.ProfileId,
				ReferralAccountID = model.ReferralAccountId,
				ReferralCommission = model.ReferralCommission,
				ReferralCommissionTypeID = model.ReferralCommissionTypeId,
				ReferralStatus = model.ReferralStatus,
				Source = model.Source,
				SubAccountLimit = model.SubAccountLimit,
				TaxID = model.TaxId,
				UpdateDte = model.UpdateDate,
				UpdateUser = model.UpdateUser,
				UserID = model.UserId,
				WebServiceDownloadDte = model.WebServiceDownloadDate,
				Website = model.Website,
                AddressCollection = new TList<Address>(model.Addresses.Select(AddressMap.ToEntity).ToList()),
			};

			return entity;
		}

		public static Account ToEntity(AccountModel model, Account entity)
		{
			if (model == null || entity == null)
			{
				return entity;
			}

			if (entity.AccountID == model.AccountId)
			{
				entity.AccountProfileCode = model.AccountProfileCode ?? entity.AccountProfileCode;
				entity.AccountTypeID = model.AccountTypeId ?? entity.AccountTypeID;
				entity.ActiveInd = model.IsActive ?? entity.ActiveInd;
				entity.CompanyName = model.CompanyName ?? entity.CompanyName;
				entity.CreateDte = model.CreateDate ?? entity.CreateDte;
				entity.CreateUser = model.CreateUser ?? entity.CreateUser;
				entity.Custom1 = model.Custom1 ?? entity.Custom1;
				entity.Custom2 = model.Custom2 ?? entity.Custom2;
				entity.Custom3 = model.Custom3 ?? entity.Custom3;
				entity.Description = model.Description ?? entity.Description;
				entity.Email = model.Email ?? entity.Email;
				entity.EmailOptIn = model.EmailOptIn;
				entity.EnableCustomerPricing = model.EnableCustomerPricing ?? entity.EnableCustomerPricing;
				entity.ExternalAccountNo = model.ExternalId ?? entity.ExternalAccountNo;
				entity.ParentAccountID = model.ParentAccountId ?? entity.ParentAccountID;
				entity.ProfileID = model.ProfileId ?? entity.ProfileID;
				entity.ReferralAccountID = model.ReferralAccountId ?? entity.ReferralAccountID;
				entity.ReferralCommission = model.ReferralCommission ?? entity.ReferralCommission;
				entity.ReferralCommissionTypeID = model.ReferralCommissionTypeId ?? entity.ReferralCommissionTypeID;
				entity.ReferralStatus = model.ReferralStatus ?? entity.ReferralStatus;
				entity.Source = model.Source ?? entity.Source;
				entity.SubAccountLimit = model.SubAccountLimit ?? entity.SubAccountLimit;
				entity.TaxID = model.TaxId ?? entity.TaxID;
				entity.UpdateDte = model.UpdateDate ?? entity.UpdateDte;
				entity.UpdateUser = model.UpdateUser ?? entity.UpdateUser;
				entity.UserID = model.UserId ?? entity.UserID;
				entity.WebServiceDownloadDte = model.WebServiceDownloadDate ?? entity.WebServiceDownloadDte;
				entity.Website = model.Website ?? entity.Website;
			}

			return entity;
		}
	}
}
