﻿using System;
using System.Data;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class ProductTypeAttributeMap
    {
        public static ProductTypeAttributeModel ToModel(ProductTypeAttribute entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new ProductTypeAttributeModel
            {
                AttributeTypeId = entity.AttributeTypeId,
                ProductAttributeTypeID = entity.ProductAttributeTypeID,
                ProductTypeId = entity.ProductTypeId
            };

            return model;
        }

        public static ProductTypeAttribute ToEntity(ProductTypeAttributeModel model)
        {
            if (Equals(model , null))
            {
                return null;
            }
            var entity = new ProductTypeAttribute
            {
                AttributeTypeId = model.AttributeTypeId,
                ProductAttributeTypeID = model.ProductAttributeTypeID,
                ProductTypeId = model.ProductTypeId
            };

            return entity;
        }
        
        /// <summary>
        /// Maps Dataset to list of type ProductTypeAttributeModel.
        /// </summary>
        /// <param name="result">Dataset</param>
        /// <returns>Returns mapped list of type ProductTypeAttributeModel.</returns>
        public static ProductTypeAssociatedAttributeTypesListModel ToModelList(DataSet result)
        {
            if (result == null)
            {
                return null;
            }

            var modelList = new ProductTypeAssociatedAttributeTypesListModel();
            foreach (DataRow dr in result.Tables[0].Rows)
            {
                ProductTypeAssociatedAttributeTypesModel model = new ProductTypeAssociatedAttributeTypesModel
                {
                    AttributeTypeId = Convert.ToInt32(dr["AttributeTypeId"]),
                    ProductAttributeTypeID = Convert.ToInt32(dr["ProductAttributeTypeId"]),
                    ProductTypeId = Convert.ToInt32(dr["ProductTypeId"]),
                    Name = dr["Name"].ToString(),
                    DisplayOrder = Convert.ToInt32(dr["DisplayOrder"]),
                    IsPrivate = Convert.ToBoolean(dr["IsPrivate"]),
                    LocaleId = Convert.ToInt32(dr["LocaleId"])
                };
                modelList.AttributeList.Add(model);
            }

            return modelList;
        }
    }
}
