﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class CatalogMap
	{
		public static CatalogModel ToModel(Catalog entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new CatalogModel
			{
				CatalogId = entity.CatalogID,
				ExternalId = entity.ExternalID,
				IsActive = entity.IsActive,
				Name = entity.Name,
				PortalId = entity.PortalID
			};

			return model;
		}

		public static Catalog ToEntity(CatalogModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new Catalog
			{
				CatalogID = model.CatalogId,
				ExternalID = model.ExternalId,
				IsActive = model.IsActive,
				Name = model.Name,
				PortalID = model.PortalId
			};

			return entity;
		}
	}
}
