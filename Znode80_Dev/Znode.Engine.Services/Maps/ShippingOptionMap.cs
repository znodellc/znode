﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class ShippingOptionMap
	{
		public static ShippingOptionModel ToModel(Shipping entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new ShippingOptionModel
			{
				CountryCode = entity.DestinationCountryCode,
				Description = entity.Description,
				DisplayOrder = entity.DisplayOrder,
				ExternalId = entity.ExternalID,
				HandlingCharge = entity.HandlingCharge,
				IsActive = entity.ActiveInd,
				ProfileId = entity.ProfileID,
				ShippingCode = entity.ShippingCode,
				ShippingOptionId = entity.ShippingID,
				ShippingType = ShippingTypeMap.ToModel(entity.ShippingTypeIDSource),
				ShippingTypeId = entity.ShippingTypeID
			};

			return model;
		}

		public static Shipping ToEntity(ShippingOptionModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new Shipping
			{
				ActiveInd = model.IsActive,
				Description = model.Description,
				DestinationCountryCode = model.CountryCode,
				DisplayOrder = model.DisplayOrder,
				ExternalID = model.ExternalId,
				HandlingCharge = model.HandlingCharge,
				ProfileID = model.ProfileId,
				ShippingCode = model.ShippingCode,
				ShippingID = model.ShippingOptionId,
				ShippingTypeID = model.ShippingTypeId
			};

			return entity;
		}
	}
}
