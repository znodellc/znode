﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class CategoryNodeMap
	{
		public static CategoryNodeModel ToModel(CategoryNode entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new CategoryNodeModel
			{
				Catalog = CatalogMap.ToModel(entity.CatalogIDSource),
				CatalogId = entity.CatalogID,
				Category = CategoryMap.ToModel(entity.CategoryIDSource),
				CategoryId = entity.CategoryID,
				CategoryNodeId = entity.CategoryNodeID,
				BeginDate = entity.BeginDate,
				CssId = entity.CSSID,
				DisplayOrder = entity.DisplayOrder,
				EndDate = entity.EndDate,
				IsActive = entity.ActiveInd,
				MasterPageId = entity.MasterPageID,
				ParentCategoryNodeId = entity.ParentCategoryNodeID,
				ThemeId = entity.ThemeID
			};

			return model;
		}

		public static CategoryNode ToEntity(CategoryNodeModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new CategoryNode
			{
				ActiveInd = model.IsActive,
				CatalogID = model.CatalogId,
				CategoryID = model.CategoryId,
				CategoryNodeID = model.CategoryNodeId,
				BeginDate = model.BeginDate,
				CSSID = model.CssId,
				DisplayOrder = model.DisplayOrder,
				EndDate = model.EndDate,
				MasterPageID = model.MasterPageId,
				ParentCategoryNodeID = model.ParentCategoryNodeId,
				ThemeID = model.ThemeId
			};

			return entity;
		}
	}
}
