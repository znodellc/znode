﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class DomainMap
	{
		public static DomainModel ToModel(Domain entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new DomainModel
			{
				ApiKey = entity.ApiKey,
				DomainId = entity.DomainID,
				DomainName = entity.DomainName,
				IsActive = entity.IsActive,
				PortalId = entity.PortalID
			};

			return model;
		}

		public static Domain ToEntity(DomainModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new Domain
			{
				ApiKey = model.ApiKey,
				DomainID = model.DomainId,
				DomainName = model.DomainName,
				IsActive = model.IsActive,
				PortalID = model.PortalId
			};

			return entity;
		}
	}
}
