﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class OrderStateMap
    {
        public static OrderStateModel ToModel(OrderState entity)
        {
            if (entity == null)
            {
                return null;
            }
            var model = new OrderStateModel
            {
                OrderStateID = entity.OrderStateID,
                OrderStateName = entity.OrderStateName,
                Description = entity.Description
            };
            return model;
        }
    }
}
