﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class AttributeTypeMap
	{
		public static AttributeTypeModel ToModel(AttributeType entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new AttributeTypeModel
			{
				AttributeTypeId = entity.AttributeTypeId,
				Description = entity.Description,
				DisplayOrder = entity.DisplayOrder,
				IsPrivate = entity.IsPrivate,
				LocaleId = entity.LocaleId,
				Name = entity.Name,
				PortalId = entity.PortalID
			};

			return model;
		}

		public static AttributeType ToEntity(AttributeTypeModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new AttributeType
			{
				AttributeTypeId = model.AttributeTypeId,
				Description = model.Description,
				DisplayOrder = model.DisplayOrder,
				IsPrivate = model.IsPrivate,
				LocaleId = model.LocaleId,
				Name = model.Name,
				PortalID = model.PortalId
			};

			return entity;
		}
	}
}
