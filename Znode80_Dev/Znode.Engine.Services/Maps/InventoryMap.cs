﻿using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
	public static class InventoryMap
	{
		public static InventoryModel ToModel(SKUInventory entity)
		{
			if (entity == null)
			{
				return null;
			}

			var model = new InventoryModel
			{
				InventoryId = entity.SKUInventoryID,
				QuantityOnHand = entity.QuantityOnHand,
				ReorderLevel = entity.ReOrderLevel,
				Sku = entity.SKU
			};

			foreach (var s in entity.SKUCollection)
			{
				model.Skus.Add(SkuMap.ToModel(s));
			}

			return model;
		}

		public static SKUInventory ToEntity(InventoryModel model)
		{
			if (model == null)
			{
				return null;
			}

			var entity = new SKUInventory
			{
				QuantityOnHand = model.QuantityOnHand,
				ReOrderLevel = model.ReorderLevel,
				SKU = model.Sku,
				SKUInventoryID = model.InventoryId
			};

			return entity;
		}
	}
}
