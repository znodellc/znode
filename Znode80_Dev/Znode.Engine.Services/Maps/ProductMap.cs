﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
namespace Znode.Engine.Services.Maps
{
    public static class ProductMap
    {
        public static ProductModel ToModel(Product entity)
        {
            if (entity == null)
            {
                return null;
            }

            var imageUtility = new ZNodeImage();

            var model = new ProductModel
            {
                AccountId = entity.AccountID,
                AdditionalInfo = entity.AdditionalInformation,
                AdditionalInfoLink = entity.AdditionalInfoLink,
                AdditionalInfoLinkLabel = entity.AdditionalInfoLinkLabel,
                AffiliateUrl = entity.AffiliateUrl,
                AllowBackOrder = entity.AllowBackOrder,
                AllowDropShip = entity.DropShipInd,
                AllowFreeShipping = entity.FreeShippingInd,
                AllowRecurringBilling = entity.RecurringBillingInd,
                AllowRecurringBillingInstallment = entity.RecurringBillingInstallmentInd,
                BackOrderMessage = entity.BackOrderMsg,
                BeginActiveDate = entity.BeginActiveDate,
                CallForPricing = entity.CallForPricing,
                CreateDate = entity.CreateDate,
                Custom1 = entity.Custom1,
                Custom2 = entity.Custom2,
                Custom3 = entity.Custom3,
                Description = entity.Description,
                DisplayOrder = entity.DisplayOrder,
                DownloadLink = entity.DownloadLink,
                DropShipEmailAddress = entity.DropShipEmailID,
                EndActiveDate = entity.EndActiveDate,
                ExpirationFrequency = entity.ExpirationFrequency,
                ExpirationPeriod = entity.ExpirationPeriod,
                ExternalId = entity.ExternalID,
                FeaturesDescription = entity.FeaturesDesc,
                Height = entity.Height,
                ImageAltTag = entity.ImageAltTag,
                ImageFile = entity.ImageFile,
                ImageMediumPath = imageUtility.GetImageHttpPathMedium(entity.ImageFile),
                ImageLargePath = imageUtility.GetImageHttpPathLarge(entity.ImageFile),
                ImageSmallPath = imageUtility.GetImageHttpPathSmall(entity.ImageFile),
                ImageSmallThumbnailPath = imageUtility.GetImageHttpPathSmallThumbnail(entity.ImageFile),
                ImageThumbnailPath = imageUtility.GetImageHttpPathThumbnail(entity.ImageFile),
                InStockMessage = entity.InStockMsg,
                InventoryDisplay = entity.InventoryDisplay,
                IsActive = entity.ActiveInd,
                IsCategorySpecial = entity.CategorySpecial,
                IsFeatured = entity.FeaturedInd,
                IsFranchisable = entity.Franchisable,
                IsHomepageSpecial = entity.HomepageSpecial,
                IsNewProduct = entity.NewProductInd,
                IsShippable = entity.IsShippable,
                Keywords = entity.Keywords,
                Length = entity.Length,
                Manufacturer = ManufacturerMap.ToModel(entity.ManufacturerIDSource),
                ManufacturerId = entity.ManufacturerID,
                MaxSelectableQuantity = entity.MaxQty,
                MinSelectableQuantity = entity.MinQty,
                Name = HttpUtility.HtmlDecode(entity.Name),
                OutOfStockMessage = entity.OutOfStockMsg,
                PortalId = entity.PortalID,
                ProductId = entity.ProductID,
                ProductNumber = entity.ProductNum,
                ProductType = ProductTypeMap.ToModel(entity.ProductTypeIDSource),
                ProductTypeId = entity.ProductTypeID,
                ProductUrl = ZNodeSEOUrl.MakeUrlForMvc(entity.ProductID.ToString(), SEOUrlType.Product, entity.SEOURL),
                PromotionPrice = entity.PromotionPrice,
                RecurringBillingFrequency = entity.RecurringBillingFrequency,
                RecurringBillingInitialAmount = entity.RecurringBillingInitialAmount,
                RecurringBillingPeriod = entity.RecurringBillingPeriod,
                RecurringBillingTotalCycles = entity.RecurringBillingTotalCycles,
                RetailPrice = entity.RetailPrice,
                ReviewStateId = entity.ReviewStateID,
                SalePrice = entity.SalePrice,
                SelectedSku = SkuMap.ToModel(entity.SelectedSku),
                SeoDescription = string.IsNullOrEmpty(entity.SEODescription) ? ZNodeConfigManager.SiteConfig.SeoDefaultProductDescription : entity.SEODescription,
                SeoKeywords = string.IsNullOrEmpty(entity.SEOKeywords) ? ZNodeConfigManager.SiteConfig.SeoDefaultProductKeyword : entity.SEOKeywords,
                SeoTitle = string.IsNullOrEmpty(entity.SEOTitle) ? ZNodeConfigManager.SiteConfig.SeoDefaultProductTitle : entity.SEOTitle,
                SeoUrl = entity.SEOURL,
                ShipEachItemSeparately = entity.ShipEachItemSeparately,
                ShippingRate = entity.ShippingRate,
                ShippingRuleTypeId = entity.ShippingRuleTypeID,
                ShipSeparately = entity.ShipSeparately,
                ShortDescription = entity.ShortDescription,
                Specifications = entity.Specifications,
                SupplierId = entity.SupplierID,
                TaxClassId = entity.TaxClassID,
                TieredPrice = entity.TieredPrice,
                TrackInventory = entity.TrackInventoryInd,
                UpdateDate = entity.UpdateDte,
                WebServiceDownloadDate = entity.WebServiceDownloadDte,
                Weight = entity.Weight,
                WholesalePrice = entity.WholesalePrice
            };

            foreach (var a in entity.ProductAddOnCollection)
            {
                model.AddOns.Add(AddOnMap.ToModel(a.AddOnIDSource));
            }

            foreach (var a in entity.Attributes)
            {
                model.Attributes.Add(AttributeMap.ToModel(a));
            }

            model.BundledProductIds = String.Join(",", entity.ParentChildProductCollectionGetByChildProductID.Select(bundle => bundle.ChildProductID.ToString()));

            foreach (var t in entity.ProductFrquentlyBoughtTogetherCollection)
            {
                model.FrequentlyBoughtTogether.Add(CrossSellMap.ToModel(t));
            }

            foreach (var c in entity.ProductCategoryCollection)
            {
                model.Categories.Add(CategoryMap.ToModel(c.CategoryIDSource));
            }

            foreach (var c in entity.ProductCrossSellCollection)
            {
                model.CrossSells.Add(CrossSellMap.ToModel(c));
            }

            foreach (var h in entity.ProductHighlightCollection)
            {
                model.Highlights.Add(HighlightMap.ToModel(h.HighlightIDSource));
            }

            foreach (var i in entity.ProductImageCollection)
            {
                model.Images.Add(ImageMap.ToModel(i));
            }

            foreach (var p in entity.PromotionCollection)
            {
                model.Promotions.Add(PromotionMap.ToModel(p));
            }

            foreach (var r in entity.ReviewCollection)
            {
                model.Reviews.Add(ReviewMap.ToModel(r));
            }

            foreach (var s in entity.SKUCollection)
            {
                model.Skus.Add(SkuMap.ToModel(s));
            }

            foreach (var t in entity.ProductTierCollection)
            {
                model.Tiers.Add(TierMap.ToModel(t));
            }

            foreach (var t in entity.ProductYouMayAlsoLikeCollection)
            {
                model.YouMayAlsoLike.Add(CrossSellMap.ToModel(t));
            }

            // If we have a selected SKU then create a simple base product so that
            // we can call ApplyPromotion() on it to set call for pricing properties
            if (model.SelectedSku != null)
            {
                var znodeProductBase = new ZNodeProductBase();
                znodeProductBase.ProductID = model.ProductId;
                znodeProductBase.IsPromotionApplied = false;
                znodeProductBase.SelectedSKUvalue = new ZNodeSKUEntity();
                znodeProductBase.SelectedSKUvalue.SKUID = model.SelectedSku.SkuId;
                znodeProductBase.ApplyPromotion();

                model.CallForPricing = znodeProductBase.CallForPricing;
                model.PromotionMessage = znodeProductBase.CallMessage;
            }

            return model;
        }

        public static Product ToEntity(ProductModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new Product
            {
                AccountID = model.AccountId,
                ActiveInd = model.IsActive,
                AdditionalInformation = model.AdditionalInfo,
                AdditionalInfoLink = model.AdditionalInfoLink,
                AdditionalInfoLinkLabel = model.AdditionalInfoLinkLabel,
                AffiliateUrl = model.AffiliateUrl,
                AllowBackOrder = model.AllowBackOrder,
                BackOrderMsg = model.BackOrderMessage,
                BeginActiveDate = model.BeginActiveDate,
                CallForPricing = model.CallForPricing,
                CategorySpecial = model.IsCategorySpecial,
                CreateDate = model.CreateDate,
                Custom1 = model.Custom1,
                Custom2 = model.Custom2,
                Custom3 = model.Custom3,
                Description = model.Description,
                DisplayOrder = model.DisplayOrder,
                DownloadLink = model.DownloadLink,
                DropShipEmailID = model.DropShipEmailAddress,
                DropShipInd = model.AllowDropShip,
                EndActiveDate = model.EndActiveDate,
                ExpirationFrequency = model.ExpirationFrequency,
                ExpirationPeriod = model.ExpirationPeriod,
                ExternalID = model.ExternalId,
                FeaturedInd = model.IsFeatured,
                FeaturesDesc = model.FeaturesDescription,
                Franchisable = model.IsFranchisable,
                FreeShippingInd = model.AllowFreeShipping,
                Height = model.Height,
                HomepageSpecial = model.IsHomepageSpecial,
                ImageAltTag = model.ImageAltTag,
                ImageFile = model.ImageFile,
                InStockMsg = model.InStockMessage,
                InventoryDisplay = model.InventoryDisplay,
                IsShippable = model.IsShippable,
                Keywords = model.Keywords,
                Length = model.Length,
                ManufacturerID = model.ManufacturerId,
                MaxQty = model.MaxSelectableQuantity,
                MinQty = model.MinSelectableQuantity,
                Name = model.Name,
                NewProductInd = model.IsNewProduct,
                OutOfStockMsg = model.OutOfStockMessage,
                PortalID = model.PortalId,
                ProductID = model.ProductId,
                ProductNum = model.ProductNumber,
                ProductTypeID = model.ProductTypeId,
                PromotionPrice = model.PromotionPrice,
                RecurringBillingFrequency = model.RecurringBillingFrequency,
                RecurringBillingInd = model.AllowRecurringBilling,
                RecurringBillingInitialAmount = model.RecurringBillingInitialAmount,
                RecurringBillingInstallmentInd = model.AllowRecurringBillingInstallment,
                RecurringBillingPeriod = model.RecurringBillingPeriod,
                RecurringBillingTotalCycles = model.RecurringBillingTotalCycles,
                RetailPrice = model.RetailPrice,
                ReviewStateID = model.ReviewStateId,
                SalePrice = model.SalePrice,
                SEODescription = model.SeoDescription,
                SEOKeywords = model.SeoKeywords,
                SEOTitle = model.SeoTitle,
                SEOURL = model.SeoUrl,
                ShipEachItemSeparately = model.ShipEachItemSeparately,
                ShippingRate = model.ShippingRate,
                ShippingRuleTypeID = model.ShippingRuleTypeId,
                ShipSeparately = model.ShipSeparately,
                ShortDescription = model.ShortDescription,
                Specifications = model.Specifications,
                SupplierID = model.SupplierId,
                TaxClassID = model.TaxClassId,
                TieredPrice = model.TieredPrice,
                TrackInventoryInd = model.TrackInventory,
                UpdateDte = model.UpdateDate,
                WebServiceDownloadDte = model.WebServiceDownloadDate,
                Weight = model.Weight,
                WholesalePrice = model.WholesalePrice
            };

            return entity;
        }

        // Use a database product because our cookie product being submitted only has data needed for display fullblown product
        public static ZNodeProductBase ToZnodeProductBase(int productId, string selectedSku, int[] selectedAddOnValueIds = null, Dictionary<int, string> addOnValuesCustomText = null)
        {
            var znodeProduct = ZNodeProductBase.Create(productId);

            if (!String.IsNullOrEmpty(selectedSku))
            {
                znodeProduct.SelectedSKU = ZNodeSKU.CreateBySKU(selectedSku);
            }

            if (selectedAddOnValueIds != null)
            {
                znodeProduct.SelectedAddOns = ZNodeAddOnList.CreateByProductAndAddOns(productId, string.Join(",", selectedAddOnValueIds));
                znodeProduct.SelectedAddOns.SelectedAddOnValueIds = string.Join(",", selectedAddOnValueIds);

                if (addOnValuesCustomText != null)
                {
                    foreach (var addOnValueEntity in znodeProduct.SelectedAddOns.AddOnCollection.Cast<ZNodeAddOnEntity>()
                                                                 .Where(addOnEntity => addOnEntity.DisplayType.Equals("TextBox", StringComparison.OrdinalIgnoreCase))
                                                                 .SelectMany(addOnEntity => addOnEntity.AddOnValueCollection.Cast<ZNodeAddOnValueEntity>())
                                                                 .Where(addOnValueEntity => addOnValuesCustomText.ContainsKey(addOnValueEntity.AddOnValueID)))
                    {
                        addOnValueEntity.CustomText = addOnValuesCustomText[addOnValueEntity.AddOnValueID];
                    }
                }
            }

            znodeProduct.ApplyPromotion();

            return znodeProduct;
        }

        /// <summary>
        /// Znode Version 8.0
        /// To map dataset to ProductListModel
        /// </summary>
        /// <param name="dataset">DataSet dataset</param>
        /// <returns>returns ProductListModel</returns>
        public static ProductListModel ToListModel(DataSet dataset)
        {
            var imageUtility = new ZNodeImage();
            ProductListModel list = new ProductListModel();
            foreach (DataRow dr in dataset.Tables[0].Rows)
            {
                ProductModel item = new ProductModel();
                item.ProductId = Convert.ToInt32(dr["ProductID"]);
                item.Name = dr["Name"].ToString();
                item.ShortDescription = dr["ShortDescription"].ToString();
                item.Description = dr["Description"].ToString();
                item.FeaturesDescription = dr["FeaturesDesc"].ToString();
                item.ProductNumber = dr["ProductNum"].ToString();
                item.ProductTypeId = Convert.ToInt32(dr["ProductTypeID"]);
                item.RetailPrice = Convert.ToDecimal(dr["RetailPrice"]);
                item.SalePrice = Convert.ToDecimal(dr["SalePrice"]);
                item.WholesalePrice = Convert.ToDecimal(dr["WholesalePrice"]);
                item.ImageFile = dr["ImageFile"].ToString();
                item.ImageAltTag = dr["ImageAltTag"].ToString();
                item.Weight = Convert.ToDecimal(dr["Weight"]);
                item.Length = Convert.ToDecimal(dr["Length"]);
                item.Width = Convert.ToDecimal(dr["Width"]);
                item.Height = Convert.ToDecimal(dr["Height"]);
                item.DisplayOrder = Convert.ToInt32(dr["DisplayOrder"]);
                item.IsActive = Convert.ToBoolean(dr["ActiveInd"]);
                item.CallForPricing = Convert.ToBoolean(dr["CallForPricing"]);
                item.IsHomepageSpecial = Convert.ToBoolean(dr["HomepageSpecial"]);
                item.IsCategorySpecial = Convert.ToBoolean(dr["CategorySpecial"]);
                item.ManufacturerId = Convert.ToInt32(dr["ManufacturerID"]);
                item.AdditionalInfoLink = dr["AdditionalInfoLink"].ToString();
                item.AdditionalInfoLinkLabel = dr["AdditionalInfoLinkLabel"].ToString();
                item.ShippingRuleTypeId = Convert.ToInt32(dr["ShippingRuleTypeID"]);
                item.ShippingRate = Convert.ToDecimal(dr["ShippingRate"]);
                item.DownloadLink = dr["DownloadLink"].ToString();
                item.AllowFreeShipping = Convert.ToBoolean(dr["FreeShippingInd"]);
                item.MaxSelectableQuantity = Convert.ToInt32(dr["MaxQty"]);
                item.SupplierId = Convert.ToInt32(dr["SupplierID"]);
                item.TaxClassId = Convert.ToInt32(dr["TaxClassID"]);
                item.MinSelectableQuantity = Convert.ToInt32(dr["MinQty"]);
                item.ReviewStateId = Convert.ToInt32(dr["ReviewStateID"]);
                item.PortalId = Convert.ToInt32(dr["PortalID"]);
                item.AccountId = Convert.ToInt32(dr["AccountID"]);
                item.ExpirationPeriod = Convert.ToInt32(dr["ExpirationPeriod"]);
                item.ExpirationFrequency = Convert.ToInt32(dr["ExpirationFrequency"]);
                item.ExternalId = dr["ExternalID"].ToString();
                item.SupplierName = dr["SUPPLIER NAME"].ToString();
                item.SupplierTypeName = dr["SupplierTypeName"].ToString();
                item.ManufacturerName = dr["MANUFACTURER NAME"].ToString();
                item.ProductTypeName = dr["PRODUCTTYPE NAME"].ToString();
                item.SkuName = dr["SKU"].ToString();
                item.SkuId = Convert.ToInt32(dr["SKUID"]);
                item.TaxClassName = dr["TaxClassName"].ToString();
                item.ShippingRule = dr["ShippingRuleType"].ToString();
                item.AttributeIds = dr["AttributeId"].ToString();
                item.QuantityOnHand = Convert.ToInt32(dr["QuantityOnHand"]);
                item.ReorderLevel = Convert.ToInt32(dr["ReorderLevel"]);
                item.ImageMediumPath = imageUtility.GetImageHttpPathMedium(item.ImageFile);
                item.ImageLargePath = imageUtility.GetImageHttpPathLarge(item.ImageFile);
                item.ImageSmallPath = imageUtility.GetImageHttpPathSmall(item.ImageFile);
                item.ImageSmallThumbnailPath = imageUtility.GetImageHttpPathSmallThumbnail(item.ImageFile);
                item.ImageThumbnailPath = imageUtility.GetImageHttpPathThumbnail(item.ImageFile);
                list.Products.Add(item);
            }
            return list;
        }
    }
}
