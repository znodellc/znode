﻿using System;
using System.Data;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public static class ProductTypeMap
    {
        public static ProductTypeModel ToModel(ProductType entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new ProductTypeModel
            {
                Description = entity.Description,
                DisplayOrder = entity.DisplayOrder,
                IsFranchiseable = entity.Franchisable,
                IsGiftCard = entity.IsGiftCard,
                Name = entity.Name,
                PortalId = entity.PortalID,
                ProductTypeId = entity.ProductTypeId
            };

            return model;
        }

        public static ProductType ToEntity(ProductTypeModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new ProductType
            {
                Description = model.Description,
                DisplayOrder = model.DisplayOrder,
                Franchisable = model.IsFranchiseable,
                IsGiftCard = model.IsGiftCard,
                Name = model.Name,
                PortalID = model.PortalId,
                ProductTypeId = model.ProductTypeId
            };

            return entity;
        }

        /// <summary>
        /// Maps Dataset to list of type AttributeTypeValueListModel.
        /// </summary>
        /// <param name="result">DataSet </param>
        /// <returns>returns mapped list of type AttributeTypeValueListModel</returns>
        public static AttributeTypeValueListModel ToModelList(DataSet result)
        {
            if (result == null)
            {
                return null;
            }

            var modelList = new AttributeTypeValueListModel();
            foreach (DataRow dr in result.Tables[0].Rows)
            {
                AttributeTypeValueModel model = new AttributeTypeValueModel
                {
                    AttributeId = Convert.ToInt32(dr["AttributeId"]),
                    ProductTypeId = Convert.ToInt32(dr["ProductTypeId"]),
                    AttributeTypeId = Convert.ToInt32(dr["AttributeTypeId"]),
                    DisplayOrder = Convert.ToInt32(dr["DisplayOrder"]),
                    AttributeType = dr["AttributeType"].ToString(),
                    AttributeValue = dr["Attribute"].ToString(),
                    IsGiftCard = Convert.ToInt32(dr["IsGiftCard"])
                };
                modelList.AttributeTypeValueList.Add(model);
            }

            return modelList;
        }
    }
}
