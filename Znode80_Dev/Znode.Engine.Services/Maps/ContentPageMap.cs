﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Services.Maps
{
    public class ContentPageMap
    {
        public static ContentPageModel ToModel(ContentPage entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new ContentPageModel
            {
                ContentPageID = entity.ContentPageID,
                Name = entity.Name,
                PortalID = entity.PortalID,
                Title = entity.Title,
                SEOMetaKeywords = entity.SEOMetaKeywords,
                SEOMetaDescription = entity.SEOMetaDescription,
                AllowDelete = entity.AllowDelete,
                TemplateName = entity.TemplateName,
                ActiveInd = entity.ActiveInd,
                AnalyticsCode = entity.AnalyticsCode,
                SEOURL = entity.SEOURL,
                LocaleId = entity.LocaleId,
                MetaTagAdditional = entity.MetaTagAdditional,
                ThemeID = entity.ThemeID,
                CSSID = entity.CSSID,
                MasterPageID = entity.MasterPageID,
                SEOTitle = entity.SEOTitle
            };

            return model;
        }

        public static ContentPage ToEntity(ContentPageModel model)
        {
            if (Equals(model, null))
            {
                return null;
            }

            var entity = new ContentPage
            {
                ContentPageID = model.ContentPageID,
                Name = model.Name,
                PortalID = model.PortalID,
                Title = model.Title,
                SEOMetaKeywords = model.SEOMetaKeywords,
                SEOMetaDescription = model.SEOMetaDescription,
                AllowDelete = model.AllowDelete,
                TemplateName = model.TemplateName,
                ActiveInd = model.ActiveInd,
                AnalyticsCode = model.AnalyticsCode,
                SEOURL = model.SEOURL,
                LocaleId = model.LocaleId,
                MetaTagAdditional = model.MetaTagAdditional,
                ThemeID = model.ThemeID,
                CSSID = model.CSSID,
                MasterPageID = model.MasterPageID,
                SEOTitle = model.SEOTitle
            };

            return entity;
        }
    }
}
