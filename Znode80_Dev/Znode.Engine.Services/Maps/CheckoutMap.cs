﻿using System.Linq;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;

namespace Znode.Engine.Services.Maps
{
	public static class CheckoutMap
	{
		public static ZNodeCheckout ToZnodeCheckout(ZNodeUserAccount account, ZNodeShoppingCart shoppingCart)
		{
			var znodeCheckout = new ZNodeCheckout();
			znodeCheckout.UserAccount = account;
			
			// Since there is no multi portal concept, assigning default portal cart
			znodeCheckout.ShoppingCart = shoppingCart.PortalCarts.FirstOrDefault();

		    znodeCheckout.ShoppingCart.Token = shoppingCart.Token;
            znodeCheckout.ShoppingCart.Payerid = shoppingCart.Payerid;

			return znodeCheckout;
		}
	}
}
