﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface ICategoryNodeService
	{
		CategoryNodeModel GetCategoryNode(int categoryNodeId, NameValueCollection expands);
		CategoryNodeListModel GetCategoryNodes(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        CategoryNodeModel CreateCategoryNode(CategoryNodeModel categoryNodeModel);
		CategoryNodeModel UpdateCategoryNode(int categoryNodeId, CategoryNodeModel categoryNodeModel);
		bool DeleteCategoryNode(int categoryNodeId);
	}
}
