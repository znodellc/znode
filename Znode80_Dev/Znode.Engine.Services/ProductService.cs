﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.Promotions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Engine.Taxes;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using AddOnRepository = ZNode.Libraries.DataAccess.Service.ProductAddOnService;
using AddOnValueRepository = ZNode.Libraries.DataAccess.Service.AddOnValueService;
using AttributeTypeRepository = ZNode.Libraries.DataAccess.Service.AttributeTypeService;
using CategoryRepository = ZNode.Libraries.DataAccess.Service.ProductCategoryService;
using CrossSellRepository = ZNode.Libraries.DataAccess.Service.ProductCrossSellService;
using CustomerPricingRepository = ZNode.Libraries.DataAccess.Service.CustomerPricingService;
using HighlightRepository = ZNode.Libraries.DataAccess.Service.ProductHighlightService;
using ImageRepository = ZNode.Libraries.DataAccess.Service.ProductImageService;
using ManufacturerRepository = ZNode.Libraries.DataAccess.Service.ManufacturerService;
using ParentChildProductRepository = ZNode.Libraries.DataAccess.Service.ParentChildProductService;
using ProductAttributeRepository = ZNode.Libraries.DataAccess.Service.ProductAttributeService;
using ProductCategoryRepository = ZNode.Libraries.DataAccess.Service.ProductCategoryService;
using ProductRepository = ZNode.Libraries.DataAccess.Service.ProductService;
using ProductTypeRepository = ZNode.Libraries.DataAccess.Service.ProductTypeService;
using ProductsCatalogsRepository = ZNode.Libraries.DataAccess.Service.VwZnodeProductsCatalogsService;
using ProductsCategoriesRepository = ZNode.Libraries.DataAccess.Service.VwZnodeProductsCategoriesService;
using ProductsCategoriesPromotionsRepository = ZNode.Libraries.DataAccess.Service.VwZnodeProductsCategoriesPromotionsService;
using ProductsPortalsRepository = ZNode.Libraries.DataAccess.Service.VwZnodeProductsPortalsService;
using ProductsPromotionsRepository = ZNode.Libraries.DataAccess.Service.VwZnodeProductsPromotionsService;
using PromotionRepository = ZNode.Libraries.DataAccess.Service.PromotionService;
using ReviewRepository = ZNode.Libraries.DataAccess.Service.ReviewService;
using SkuRepository = ZNode.Libraries.DataAccess.Service.SKUService;
using SkuInventoryRepository = ZNode.Libraries.DataAccess.Service.SKUInventoryService;
using SkuAttributeRepository = ZNode.Libraries.DataAccess.Service.SKUAttributeService;
using TierRepository = ZNode.Libraries.DataAccess.Service.ProductTierService;
using ZNode.Libraries.DataAccess.Custom;
using System.Data;
using TagsRepository = ZNode.Libraries.DataAccess.Service.TagsService;
using ZNode.Libraries.DataAccess.Service;
using FacetGroupRepository = ZNode.Libraries.DataAccess.Service.FacetGroupService;
using FacetsRepository = ZNode.Libraries.DataAccess.Service.FacetService;
using FacetProductSKURepository = ZNode.Libraries.DataAccess.Service.FacetProductSKUService;
using Znode.Libraries.Helpers.Extensions;

namespace Znode.Engine.Services
{
	public class ProductService : BaseService, IProductService
	{
		private readonly AddOnRepository _addOnRepository;
		private readonly AddOnValueRepository _addOnValueRepository;
		private readonly AttributeTypeRepository _attributeTypeRepository;
		private readonly CategoryRepository _categoryRepository;
		private readonly CrossSellRepository _crossSellRepository;
		private readonly CustomerPricingRepository _customerPricingRepository;
		private readonly HighlightRepository _highlightRepository;
		private readonly ImageRepository _imageRepository;
		private readonly ManufacturerRepository _manufacturerRepository;
		private readonly ParentChildProductRepository _parentChildProductRepository;
		private readonly ProductAttributeRepository _productAttributeRepository;
		private readonly ProductRepository _productRepository;
		private readonly ProductTypeRepository _productTypeRepository;
		private readonly ProductsCatalogsRepository _productsCatalogsRepository;
		private readonly ProductsCategoriesRepository _productsCategoriesRepository;
		private readonly ProductsCategoriesPromotionsRepository _productsCategoriesPromotionsRepository;
		private readonly ProductsPortalsRepository _productsPortalsRepository;
		private readonly ProductsPromotionsRepository _productsPromotionsRepository;
		private readonly PromotionRepository _promotionRepository;
		private readonly ReviewRepository _reviewRepository;
		private readonly SkuRepository _skuRepository;
		private readonly SkuInventoryRepository _skuInventoryRepository;
		private readonly SkuAttributeRepository _skuAttributeRepository;
		private readonly TierRepository _tierRepository;
        private readonly TagsRepository _tagsRepository;
        private readonly FacetGroupRepository _facetGroupRepository;
        private readonly FacetsRepository _facetsRepository;
        private readonly FacetProductSKURepository _facetProductSKURepository;

		public ProductService()
		{
			_addOnRepository = new AddOnRepository();
			_addOnValueRepository = new AddOnValueRepository();
			_attributeTypeRepository = new AttributeTypeRepository();
			_categoryRepository = new CategoryRepository();
			_crossSellRepository = new CrossSellRepository();
			_customerPricingRepository = new CustomerPricingRepository();
			_highlightRepository = new HighlightRepository();
			_imageRepository = new ImageRepository();
			_manufacturerRepository = new ManufacturerRepository();
            _parentChildProductRepository = new ParentChildProductRepository();
			_productAttributeRepository = new ProductAttributeRepository();
			_productRepository = new ProductRepository();
			_productTypeRepository = new ProductTypeRepository();
			_productsCatalogsRepository = new ProductsCatalogsRepository();
			_productsCategoriesRepository = new ProductsCategoriesRepository();
			_productsCategoriesPromotionsRepository = new ProductsCategoriesPromotionsRepository();
			_productsPortalsRepository = new ProductsPortalsRepository();
			_productsPromotionsRepository = new ProductsPromotionsRepository();
			_promotionRepository = new PromotionRepository();
			_reviewRepository = new ReviewRepository();
			_skuRepository = new SkuRepository();
			_skuInventoryRepository = new SkuInventoryRepository();
			_skuAttributeRepository = new SkuAttributeRepository();
			_tierRepository = new TierRepository();
            _tagsRepository = new TagsRepository();
            _facetGroupRepository = new FacetGroupRepository();
            _facetsRepository = new FacetsRepository();
            _facetProductSKURepository = new FacetProductSKURepository();
		}

		public ProductModel GetProduct(int productId, NameValueCollection expands)
		{
			var product = _productRepository.GetByProductID(productId);
			if (product != null)
			{
				GetExpands(expands, product);
			}

			return ProductMap.ToModel(product);
		}

		public ProductModel GetProductWithSku(int productId, int skuId, NameValueCollection expands)
		{
			var product = _productRepository.GetByProductID(productId);
			if (product != null)
			{
				var sku = _skuRepository.GetBySKUID(skuId);
				if (sku != null)
				{
					// Set selected SKU and get pricing
					product.SelectedSku = sku;
					GetPricing(product, sku);
				}
				else
				{
					throw new Exception("SKU ID " + skuId + " does not exist.");
				}

				GetExpands(expands, product);
			}

			return ProductMap.ToModel(product);
		}

		public ProductListModel GetProducts(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new ProductListModel();
			var products = new TList<Product>();
			var tempList = new TList<Product>();

			var query = new ProductQuery { Junction = String.Empty };
			var sortBuilder = new ProductSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			tempList = _productRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Now go through the list and get any expands
			foreach (var product in tempList)
			{
				GetExpands(expands, product);

				product.RetailPrice = GetFinalPrice(product.TaxClassID.GetValueOrDefault(), product.RetailPrice.GetValueOrDefault());

				if (product.SalePrice.HasValue)
				{
					product.SalePrice = GetFinalPrice(product.TaxClassID.GetValueOrDefault(), product.SalePrice.GetValueOrDefault());
				}

				products.Add(product);
			}

			// Map each item and add to the list
			foreach (var p in products)
			{
				model.Products.Add(ProductMap.ToModel(p));
			}

			return model;
		}

		public ProductListModel GetProductsByCatalog(int catalogId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new ProductListModel();
			var products = new TList<Product>();
			var tempList = new VList<VwZnodeProductsCatalogs>();

			var query = new VwZnodeProductsCatalogsQuery { Junction = String.Empty };
			var sortBuilder = new VwZnodeProductsCatalogsSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetQueryParameter(VwZnodeProductsCatalogsColumn.CatalogID, FilterOperators.Equals, catalogId.ToString(), query);

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			tempList = _productsCatalogsRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Now go through the list, get the full product, and get any expands
			foreach (var item in tempList)
			{
				var product = _productRepository.GetByProductID(item.ProductID);
				GetExpands(expands, product);
				products.Add(product);
			}

			// Map each item and add to the list
			foreach (var p in products)
			{
				model.Products.Add(ProductMap.ToModel(p));
			}

			return model;
		}

        public ProductListModel GetProductsByCatalogIds(string catalogIds, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {

            if (String.IsNullOrEmpty(catalogIds))
            {
                throw new Exception("List of catalog IDs cannot be null or empty.");
            }

            var model = new ProductListModel();
            var products = new TList<Product>();
            var tempList = new VList<VwZnodeProductsCatalogs>();

            var query = new VwZnodeProductsCatalogsQuery { Junction = String.Empty };
            var sortBuilder = new VwZnodeProductsCatalogsSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            var list = catalogIds.Split(',');
            foreach (var catalogId in list.Where(catalogId => !String.IsNullOrEmpty(catalogId)))
            {
	            query.BeginGroup("OR");
	            query.AppendEquals(VwZnodeProductsCatalogsColumn.CatalogID, catalogId);
	            query.EndGroup();
            }

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _productsCatalogsRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list, get the full product, and get any expands
            foreach (var item in tempList)
            {
                var product = _productRepository.GetByProductID(item.ProductID);
                GetExpands(expands, product);
                products.Add(product);
            }

            // Map each item and add to the list
            foreach (var p in products)
            {
                model.Products.Add(ProductMap.ToModel(p));
            }

            return model;
        }

        public ProductListModel GetProductsByCategory(int categoryId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new ProductListModel();
			var products = new TList<Product>();
			var tempList = new VList<VwZnodeProductsCategories>();

			var query = new VwZnodeProductsCategoriesQuery { Junction = String.Empty };
			var sortBuilder = new VwZnodeProductsCategoriesSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetQueryParameter(VwZnodeProductsCategoriesColumn.CategoryID, FilterOperators.Equals, categoryId.ToString(), query);

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			tempList = _productsCategoriesRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Now go through the list, get the full product, and get any expands
			foreach (var item in tempList)
			{
				var product = _productRepository.GetByProductID(item.ProductID);
				GetExpands(expands, product);
				products.Add(product);
			}

			// Map each item and add to the list
			foreach (var p in products)
			{
				model.Products.Add(ProductMap.ToModel(p));
			}

			return model;
		}

		public ProductListModel GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new ProductListModel();
			var products = new TList<Product>();
			var tempList = new VList<VwZnodeProductsCategoriesPromotions>();

			var query = new VwZnodeProductsCategoriesPromotionsQuery { Junction = String.Empty };
			var sortBuilder = new VwZnodeProductsCategoriesPromotionsSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.CategoryID, FilterOperators.Equals, categoryId.ToString(), query);
			SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.DiscountTypeID, FilterOperators.Equals, promotionTypeId.ToString(), query);

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			tempList = _productsCategoriesPromotionsRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Now go through the list, get the full product, and get any expands
			foreach (var item in tempList)
			{
				var product = _productRepository.GetByProductID(item.ProductID);
				GetExpands(expands, product);
				products.Add(product);
			}

			// Map each item and add to the list
			foreach (var p in products)
			{
				model.Products.Add(ProductMap.ToModel(p));
			}

			return model;
		}

		public ProductListModel GetProductsByExternalIds(string externalIds, NameValueCollection expands, NameValueCollection sorts)
		{
			if (String.IsNullOrEmpty(externalIds))
			{
				throw new Exception("List of product IDs cannot be null or empty.");
			}

			var model = new ProductListModel();
			var products = new TList<Product>();
			var tempList = new TList<Product>();

			var query = new ProductQuery { Junction = String.Empty };
			var sortBuilder = new ProductSortBuilder();

			var list = externalIds.Split(',');
			foreach (var externalId in list)
			{
				query.BeginGroup("OR");
				query.AppendEquals(ProductColumn.ExternalID, externalId);
				query.EndGroup();
			}

			SetSorting(sorts, sortBuilder);

			// Get the initial set
			tempList = _productRepository.Find(query, sortBuilder);

			// Now go through the list and get any expands
			foreach (var product in tempList)
			{
				GetExpands(expands, product);
				products.Add(product);
			}

			// Map each item and add to the list
			foreach (var p in products)
			{
				model.Products.Add(ProductMap.ToModel(p));
			}

			return model;
		}

		public ProductListModel GetProductsByHomeSpecials(int portalId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new ProductListModel();
			var products = new TList<Product>();
			var tempList = new VList<VwZnodeProductsPortals>();

			var query = new VwZnodeProductsPortalsQuery { Junction = String.Empty };
			var sortBuilder = new VwZnodeProductsCatalogsSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetQueryParameter(VwZnodeProductsPortalsColumn.PortalID, FilterOperators.Equals, portalId.ToString(), query);
			SetQueryParameter(VwZnodeProductsPortalsColumn.HomepageSpecial, FilterOperators.Equals, true.ToString(), query);

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			tempList = _productsPortalsRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Now go through the list, get the full product, and get any expands
			foreach (var item in tempList)
			{
				var product = _productRepository.GetByProductID(item.ProductID);
				GetExpands(expands, product);
				products.Add(product);
			}

			// Map each item and add to the list
			foreach (var p in products)
			{
				model.Products.Add(ProductMap.ToModel(p));
			}

			return model;
		}

		public ProductListModel GetProductsByProductIds(string productIds, NameValueCollection expands, NameValueCollection sorts)
		{
			if (String.IsNullOrEmpty(productIds))
			{
				throw new Exception("List of product IDs cannot be null or empty.");
			}

			var model = new ProductListModel();
			var products = new TList<Product>();
			var tempList = new TList<Product>();

			var query = new ProductQuery { Junction = String.Empty };
			var sortBuilder = new ProductSortBuilder();

			var list = productIds.Split(',');
			foreach (var productId in list.Where(productId => !String.IsNullOrEmpty(productId)))
			{
				query.BeginGroup("OR");
				query.AppendEquals(ProductColumn.ProductID, productId);
				query.EndGroup();
			}

			SetSorting(sorts, sortBuilder);

			// Get the initial set
			tempList = _productRepository.Find(query, sortBuilder);

			// Now go through the list and get any expands
			foreach (var product in tempList)
			{
				GetExpands(expands, product);
				products.Add(product);
			}

			// Map each item and add to the list
			foreach (var p in products)
			{
				model.Products.Add(ProductMap.ToModel(p));
			}

			return model;
		}

		public ProductListModel GetProductsByPromotionType(int promotionTypeId, NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new ProductListModel();
			var products = new TList<Product>();
			var tempList = new VList<VwZnodeProductsPromotions>();

			var query = new VwZnodeProductsPromotionsQuery { Junction = String.Empty };
			var sortBuilder = new VwZnodeProductsPromotionsSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetQueryParameter(VwZnodeProductsPromotionsColumn.DiscountTypeID, FilterOperators.Equals, promotionTypeId.ToString(), query);

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			tempList = _productsPromotionsRepository.Find(query, sortBuilder.ToString(), pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Now go through the list, get the full product, and get any expands
			foreach (var item in tempList)
			{
				var product = _productRepository.GetByProductID(item.ProductID);
				GetExpands(expands, product);
				products.Add(product);
			}

			// Map each item and add to the list
			foreach (var p in products)
			{
				model.Products.Add(ProductMap.ToModel(p));
			}

			return model;
		}

		public ProductModel CreateProduct(ProductModel model)
		{
			if (model == null)
			{
				throw new Exception("Product model cannot be null.");
			}

			var entity = ProductMap.ToEntity(model);

			if (entity.ProductTypeID < 1)
			{
				// Attach the default product type
				entity.ProductTypeID = _productTypeRepository.GetByName("Default").ProductTypeId;
			}

			// Set the dates
			entity.CreateDate = DateTime.Now;
			entity.UpdateDte = DateTime.Now;

			var product = _productRepository.Save(entity);
			return ProductMap.ToModel(product);
		}

		public ProductModel UpdateProduct(int productId, ProductModel model)
		{
			if (productId < 1)
			{
				throw new Exception("Product ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Product model cannot be null.");
			}

			var product = _productRepository.GetByProductID(productId);
			if (product != null)
			{
				// Set product ID and update date
				model.ProductId = productId;
				model.UpdateDate = DateTime.Now;

				var productToUpdate = ProductMap.ToEntity(model);

				var updated = _productRepository.Update(productToUpdate);
				if (updated)
				{
					product = _productRepository.GetByProductID(productId);
					return ProductMap.ToModel(product);
				}
			}

			return null;
		}

		public bool DeleteProduct(int productId)
		{
			if (productId < 1)
			{
				throw new Exception("Product ID cannot be less than 1.");
			}

			var product = _productRepository.GetByProductID(productId);
			if (product != null)
			{
				return _productRepository.Delete(product);
			}

			return false;
		}

        #region Znode Version 8.0

        /// <summary>
        /// To get product details by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns product details</returns>
        public ProductListModel GetProductDetailsByProductId(int productId)
        {
            var productHelper = new ProductHelper();
            ProductListModel list = new ProductListModel();
            DataSet dataset = productHelper.GetProductDetailsByProductId(productId);
            if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
            {
                list = ProductMap.ToListModel(dataset);
            }
            return list;
        }
        #endregion


        #region Znode Version 8.0

        public ProductTagsModel GetProductTags(int productId, NameValueCollection expands)
        {
            var productTags = _tagsRepository.GetByProductID(productId);
            return ProductTagMap.ToModel(productTags);
        }

        public ProductTagsModel CreateProductTag(ProductTagsModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Product Tag model cannot be null.");
            }

            var entity = ProductTagMap.ToEntity(model);
            var tags = _tagsRepository.Save(entity);
            return ProductTagMap.ToModel(tags);
        }

        public ProductTagsModel UpdateProductTag(int tagId, ProductTagsModel model)
        {
            if (tagId < 1)
            {
                throw new Exception("Tag ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Product Tag model cannot be null.");
            }

            var productTags = _tagsRepository.GetByProductID(model.ProductId);
            if (!Equals(productTags, null))
            {
                // Set tagId and update details.
                model.TagId = productTags.FirstOrDefault().TagID;
                var tagsToUpdate = ProductTagMap.ToEntity(model);

                var updated = _tagsRepository.Update(tagsToUpdate);
                if (updated)
                {
                    var tags = _tagsRepository.GetByTagID(productTags.FirstOrDefault().TagID);
                    return ProductTagMap.ToModel(tags);
                }
            }
            return null;
        }

        public bool DeleteProductTag(int tagId)
        {
            if (tagId < 1)
            {
                throw new Exception("Tag ID cannot be less than 1.");
            }

            var tags = _tagsRepository.GetByTagID(tagId);
            if (!Equals(tags, null))
            {
                return _tagsRepository.Delete(tags);
            }
            return false;
        }

        public ProductFacetModel GetProductFacets(int productId, NameValueCollection expands)
        {
            ProductFacetModel model = new ProductFacetModel();
            ProductHelper productHelper = new ProductHelper();
            DataSet facetData = productHelper.GetProductFacetsDetails(productId);

            model.FacetGroups = facetData.Tables[0].ToList<FacetGroupModel>().ToCollection();
            model.AssociatedFacets = facetData.Tables[1].ToList<FacetModel>().ToCollection();

            var productSKUs = _facetProductSKURepository.GetByProductID(productId);
            model.AssociatedFacetsSkus = ProductFacetsMap.ToFacetSkuModel(productSKUs);

            return model;
        }

        public ProductFacetModel BindProductFacets(int productId, ProductFacetModel model)
        {
            if (productId < 1)
            {
                throw new Exception("Product ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Product Facet model cannot be null.");
            }
            TList<FacetProductSKU> tpsList = _facetProductSKURepository.GetByProductID(productId);

            TList<Facet> facetList = _facetsRepository.GetByFacetGroupID(model.FacetGroupId);
            var selectedFacetIds= (Equals(model.FacetIds,null))? new List<int>() : model.FacetIds.Select(x => Int32.Parse(x)).ToList();

            foreach (var li in facetList)
            {
                if (selectedFacetIds.Contains(li.FacetID))
                {
                    if (tpsList.Find("FacetID", Convert.ToInt32(li.FacetID)) == null)
                    {
                        FacetProductSKU tps = new FacetProductSKU();
                        tps.FacetID = Convert.ToInt32(li.FacetID);
                        tps.ProductID = productId;
                        _facetProductSKURepository.Save(tps);
                    }
                }
                else
                {
                    FacetProductSKU tp;
                    if ((tp = tpsList.Find("FacetID", Convert.ToInt32(li.FacetID))) != null)
                    {
                        _facetProductSKURepository.Delete(tp);
                    }
                }
            }
            //TO DO, return the list of facets
            return new ProductFacetModel();
        }

        #endregion





        private decimal? GetFinalPrice(int taxClassId, decimal price)
		{
			var finalPrice = GetTaxedPrice(taxClassId, price);
			return finalPrice;
		}

		private void GetPricing(Product product, SKU sku)
		{
			// Do pricing calculations with selected SKU
			var retailPrice = GetRetailPrice(product, sku);
			var salePrice = GetSalePrice(product, sku);
			var wholesalePrice = GetWholesalePrice(product, sku);

			// Tiered pricing doesn't require a SKU
			var tieredPrice = GetTieredPrice(product);

			if (sku.RetailPriceOverride.HasValue)
			{
				sku.RetailPriceOverride = GetFinalPrice(product.TaxClassID.GetValueOrDefault(), sku.RetailPriceOverride.GetValueOrDefault());
			}

			if (sku.SalePriceOverride.HasValue)
			{
				sku.SalePriceOverride = GetFinalPrice(product.TaxClassID.GetValueOrDefault(), sku.SalePriceOverride.GetValueOrDefault());
			}

			// Once above calculations are done, product price can be calculated with SKU
			var productPrice = GetProductPrice(retailPrice, salePrice, wholesalePrice, tieredPrice, sku);

			// Now set product, final, and promotion price on the SKU
			sku.ProductPrice = productPrice;
			sku.FinalPrice = sku.ProductPrice; // GetFinalPrice(product.TaxClassID.GetValueOrDefault(), sku.ProductPrice.GetValueOrDefault());
			sku.PromotionPrice = GetPromotionPrice(product.ProductID, productPrice);

			// And finally get product price without SKU and set promotion and tiered price on the product
			productPrice = GetProductPrice(product.RetailPrice, product.SalePrice, product.WholesalePrice, tieredPrice);
			productPrice = GetFinalPrice(product.TaxClassID.GetValueOrDefault(), productPrice.GetValueOrDefault());
			product.PromotionPrice = GetPromotionPrice(product.ProductID, productPrice);
			product.TieredPrice = tieredPrice;
			
			if (!product.SKUCollection.Any())
			{
				product.RetailPrice = retailPrice;
				product.SalePrice = salePrice;
			}
		}

		private decimal? GetProductPrice(decimal? productRetailPrice, decimal? productSalePrice, decimal? productWholesalePrice, decimal? productTieredPrice)
		{
			var useWholesalePrice = UseWholesalePrice();

			var basePrice = productRetailPrice;

			if (productSalePrice.HasValue && !useWholesalePrice)
			{
				basePrice = productSalePrice.Value;
			}
			else
			{
				if (productWholesalePrice.HasValue && useWholesalePrice)
				{
					basePrice = productWholesalePrice.Value;
				}
				else if (productSalePrice.HasValue)
				{
					basePrice = productSalePrice.Value;
				}
			}

			// Check tiered price
			return productTieredPrice > 0 ? productTieredPrice : basePrice;
		}

		private decimal? GetProductPrice(decimal? productRetailPrice, decimal? productSalePrice, decimal? productWholesalePrice, decimal? productTieredPrice, SKU sku)
		{
			var useWholesalePrice = UseWholesalePrice();

			// Base/retail price
			var basePrice = productRetailPrice;
			var retailPriceOverride = sku.RetailPriceOverride;
			if (retailPriceOverride.HasValue && retailPriceOverride.Value >= 0)
			{
				basePrice = retailPriceOverride.Value;
			}

			// Sale price
			var salePrice = productSalePrice;
			var salePriceOverride = sku.SalePriceOverride;
			if (salePriceOverride.HasValue && salePriceOverride.Value >= 0)
			{
				salePrice = salePriceOverride.Value;
			}

			// Wholesale price
			var wholesalePrice = productWholesalePrice;
			var wholesalePriceOverride = sku.WholesalePriceOverride;
			if (wholesalePriceOverride.HasValue && wholesalePriceOverride.Value >= 0)
			{
				wholesalePrice = wholesalePriceOverride.Value;
			}

			// Compare sale price, wholesale price, and negotiated price
			// commented CBP price calculation 
			sku.NegotiatedPrice = null;//GetSkuNegotiatedPrice(sku);
			if (!salePrice.HasValue && !useWholesalePrice && sku.NegotiatedPrice.HasValue)
			{
				salePrice = sku.NegotiatedPrice.GetValueOrDefault();
				basePrice = salePrice.Value;
			}
			else if (salePrice.HasValue && !useWholesalePrice)
			{
				basePrice = salePrice.Value;
			}
			else
			{
				if (useWholesalePrice && wholesalePrice.HasValue)
				{
					if (sku.NegotiatedPrice.HasValue)
					{
						salePrice = sku.NegotiatedPrice.GetValueOrDefault();
						basePrice = salePrice.GetValueOrDefault();
					}
					else
					{
						basePrice = wholesalePrice.Value;
					}
				}
				else if (salePrice.HasValue)
				{
					basePrice = salePrice.Value;
				}
			}

			// Check tiered price
			return productTieredPrice > 0 ? productTieredPrice : basePrice;
		}

		private decimal? GetPromotionPrice(int productId, decimal? productPrice)
		{
			if (productPrice.HasValue)
			{
				var pricePromoManager = new ZnodePricePromotionManager();
				return pricePromoManager.PromotionalPrice(productId, productPrice.Value);
			}

			return null;
		}

		private decimal? GetRetailPrice(Product product, SKU sku)
		{
			var retailPrice = product.RetailPrice;
			var retailPriceOverride = sku.RetailPriceOverride;

			if (retailPriceOverride.HasValue && retailPriceOverride.Value >= 0)
			{
				retailPrice = retailPriceOverride.Value;
			}

			if (retailPrice.HasValue)
			{
				retailPrice = GetTaxedPrice(product.TaxClassID.GetValueOrDefault(), retailPrice.Value);
			}

			return retailPrice;
		}

		private decimal? GetSalePrice(Product product, SKU sku)
		{
			var salePrice = product.SalePrice;
			var salePriceOverride = sku.SalePriceOverride;

			if (salePriceOverride.HasValue)
			{
				salePrice = salePriceOverride.Value;
			}

			if (!salePrice.HasValue)
			{
				salePrice = GetSkuNegotiatedPrice(sku);
			}

			if (salePrice.HasValue)
			{
				salePrice = GetTaxedPrice(product.TaxClassID.GetValueOrDefault(), salePrice.Value);
			}

			return salePrice;
		}

		private decimal? GetSkuNegotiatedPrice(SKU sku)
		{
			var query = new CustomerPricingQuery();
			query.AppendEquals(CustomerPricingColumn.SKUExternalID, sku.ExternalID);

			var customerPricing = _customerPricingRepository.Find(query);
			if (customerPricing != null && customerPricing.Count > 0)
			{
				return customerPricing[0].NegotiatedPrice;
			}

			return null;
		}

		private decimal? GetTaxedPrice(int taxClassId, decimal price)
		{
			// We're leaving address null so that underneath it defaults to the shipping address on the account
			var tax = new ZnodeInclusiveTax();
			return tax.GetInclusivePrice(taxClassId, price, null);
		}

		private decimal? GetTieredPrice(Product product)
		{
			var profileId = 0;
			var tieredPrice = 0.0m;

			// Get product tiers
			var tiers = _tierRepository.GetByProductID(product.ProductID);
			if (tiers != null && tiers.Count > 0)
			{
				// Get profile
				if (HttpContext.Current.Session["ProfileCache"] != null)
				{
					var profile = (Profile)HttpContext.Current.Session["ProfileCache"];
					if (profile != null)
					{
						profileId = profile.ProfileID;
					}
				}

				// Go through each tier to set the price
				foreach (var tier in tiers)
				{
					if (1 >= tier.TierStart && tier.TierEnd >= 1)
					{
						if (tier.ProfileID == 0 || tier.ProfileID == profileId)
						{
							tieredPrice = tier.Price;
							break;
						}
					}
				}
			}

			return tieredPrice;
		}

		private decimal? GetWholesalePrice(Product product, SKU sku)
		{
			var wholesalePrice = product.WholesalePrice;
			var wholesalePriceOverride = sku.WholesalePriceOverride;

			if (wholesalePriceOverride.HasValue && wholesalePriceOverride.Value >= 0)
			{
				wholesalePrice = wholesalePriceOverride.Value;
			}

			if (wholesalePrice.HasValue)
			{
				wholesalePrice = GetTaxedPrice(product.TaxClassID.GetValueOrDefault(), wholesalePrice.Value);
			}

			return wholesalePrice;
		}

		private bool UseWholesalePrice()
		{
			if (HttpContext.Current.Session["ProfileCache"] != null)
			{
				var profile = (Profile)HttpContext.Current.Session["ProfileCache"];
				if (profile != null)
				{
					return profile.UseWholesalePricing.GetValueOrDefault(false);
				}
			}

			return false;
		}

		private void GetExpands(NameValueCollection expands, Product product)
		{
			if (expands != null && expands.HasKeys())
			{
				ExpandAddOns(expands, product);
				ExpandAttributes(expands, product);
                ExpandBundleItems(expands, product);
				ExpandCategories(expands, product);
				ExpandCrossSells(expands, product);
				ExpandFrequentlyBoughtTogether(expands, product);
				ExpandHighlights(expands, product);
				ExpandImages(expands, product);
				ExpandManufacturer(expands, product);
				ExpandProductType(expands, product);
				ExpandPromotions(expands, product);
				ExpandReviews(expands, product);
				ExpandSkus(expands, product);
				ExpandTiers(expands, product);
				ExpandYouMayAlsoLike(expands, product);
                ExpandProductTags(expands, product);
			}
		}

		private void ExpandAddOns(NameValueCollection expands, Product product)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.AddOns)))
			{
				var productAddOnList = _addOnRepository.GetByProductID(product.ProductID);
				foreach (var productAddOn in productAddOnList)
				{
					var productAddOnLoaded = _addOnRepository.DeepLoadByProductIDAddOnID(productAddOn.ProductID, productAddOn.AddOnID, true, DeepLoadType.IncludeChildren,
						new[] { typeof(AddOn) });

					var addOnValueList = _addOnValueRepository.GetByAddOnID(productAddOn.AddOnID);
					foreach (AddOnValue addOnValue in addOnValueList)
					{
						var addOnValueLoaded = _addOnValueRepository.DeepLoadByAddOnValueID(addOnValue.AddOnValueID, true, DeepLoadType.IncludeChildren, new[] { typeof(SKUInventory) });
						addOnValueLoaded.Inventory = _skuInventoryRepository.GetBySKU(addOnValueLoaded.SKU);
						productAddOnLoaded.AddOnIDSource.AddOnValueCollection.Add(addOnValueLoaded);
					}

					product.ProductAddOnCollection.Add(productAddOnLoaded);
				}
			}
		}

		private void ExpandAttributes(NameValueCollection expands, Product product)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Attributes)))
			{
				// First get the SKUs
				var skus = _skuRepository.GetByProductID(product.ProductID);
				foreach (var s in skus)
				{
					// Then get the attributes
					var skuAttribs = _skuAttributeRepository.GetBySKUID(s.SKUID);
					foreach (var sa in skuAttribs)
					{
						var prodAttrib = _productAttributeRepository.GetByAttributeId(sa.AttributeId);
						if (prodAttrib != null && !product.Attributes.Contains(prodAttrib))
						{
							// Go ahead and expand the attribute type as well
							var attribType = _attributeTypeRepository.GetByAttributeTypeId(prodAttrib.AttributeTypeId);
							if (attribType != null)
							{
								prodAttrib.AttributeType = attribType;
							}

							product.Attributes.Add(prodAttrib);
						}
					}
				}
			}
		}

		private void ExpandAttributes(NameValueCollection expands, SKU sku)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Attributes)))
			{
				var skuAttribs = _skuAttributeRepository.GetBySKUID(sku.SKUID);
				foreach (var sa in skuAttribs)
				{
					var prodAttrib = _productAttributeRepository.GetByAttributeId(sa.AttributeId);
					var item = new SKUAttribute
					{
						AttributeId = sa.AttributeId,
						AttributeIdSource = prodAttrib,
						SKUAttributeID = sa.SKUAttributeID,
						SKUID = sku.SKUID,
						SKUIDSource = sku
					};

					sku.SKUAttributeCollection.Add(item);
				}
			}
		}

        private void ExpandBundleItems(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.BundleItems)))
            {
                var productBundleList = _parentChildProductRepository.GetByParentProductID(product.ProductID);

                product.ParentChildProductCollectionGetByChildProductID = productBundleList;
            }
        }


		private void ExpandCategories(NameValueCollection expands, Product product)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Categories)))
			{
				var categories = _categoryRepository.DeepLoadByProductID(product.ProductID, true, DeepLoadType.IncludeChildren, new[] { typeof(Category) });
				foreach (var c in categories)
				{
					product.ProductCategoryCollection.Add(c);
				}
			}
		}

		private void ExpandCrossSells(NameValueCollection expands, Product product)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.CrossSells)))
			{
				var crossSells = _crossSellRepository.GetByProductId(product.ProductID);
				foreach (var c in crossSells)
				{
					product.ProductCrossSellCollection.Add(c);
				}
			}
		}

		private void ExpandFrequentlyBoughtTogether(NameValueCollection expands, Product product)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.FrequentlyBoughtTogether)))
			{
				var crossSells = _crossSellRepository.GetByProductIdRelationTypeId(product.ProductID, 4);
				foreach (var c in crossSells)
				{
					product.ProductFrquentlyBoughtTogetherCollection.Add(c);
				}
			}
		}

		private void ExpandHighlights(NameValueCollection expands, Product product)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Highlights)))
			{
				var highlights = _highlightRepository.DeepLoadByProductID(product.ProductID, true, DeepLoadType.IncludeChildren, new[] { typeof(Highlight) });
				foreach (var h in highlights)
				{
					product.ProductHighlightCollection.Add(h);
				}
			}
		}

		private void ExpandImages(NameValueCollection expands, Product product)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Images)))
			{
				var images = _imageRepository.GetByProductID(product.ProductID);
				foreach (var i in images)
				{
					product.ProductImageCollection.Add(i);
				}
			}
		}

		private void ExpandManufacturer(NameValueCollection expands, Product product)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Manufacturer)))
			{
				if (product.ManufacturerID.HasValue)
				{
					var manufacturer = _manufacturerRepository.GetByManufacturerID(product.ManufacturerID.Value);
					if (manufacturer != null)
					{
						product.ManufacturerIDSource = manufacturer;
					}
				}
			}
		}

		private void ExpandProductType(NameValueCollection expands, Product product)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ProductType)))
			{
				var type = _productTypeRepository.GetByProductTypeId(product.ProductTypeID);
				if (type != null)
				{
					product.ProductTypeIDSource = type;
				}
			}
		}

		private void ExpandPromotions(NameValueCollection expands, Product product)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Promotions)))
			{
				var promos = _promotionRepository.GetByProductID(product.ProductID);
				foreach (var p in promos)
				{
					product.PromotionCollection.Add(p);
				}
			}
		}

		private void ExpandReviews(NameValueCollection expands, Product product)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Reviews)))
			{
				var reviews = _reviewRepository.GetByProductID(product.ProductID);
				foreach (var r in reviews)
				{
					product.ReviewCollection.Add(r);
				}
			}
		}

		private void ExpandSkus(NameValueCollection expands, Product product)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Skus)))
			{
				var skus = _skuRepository.GetByProductID(product.ProductID);
				foreach (var s in skus)
				{
					ExpandAttributes(expands, s);

					// Calculate prices and add to list
					GetPricing(product, s);
					product.SKUCollection.Add(s);
				}
			}
		}

		private void ExpandTiers(NameValueCollection expands, Product product)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Tiers)))
			{
				var tiers = _tierRepository.GetByProductID(product.ProductID);
				foreach (var t in tiers)
				{
					product.ProductTierCollection.Add(t);
				}
			}
		}

		private void ExpandYouMayAlsoLike(NameValueCollection expands, Product product)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.YouMayAlsoLike)))
			{
				var crossSells = _crossSellRepository.GetByProductIdRelationTypeId(product.ProductID, 5);
				foreach (var c in crossSells)
				{
					product.ProductYouMayAlsoLikeCollection.Add(c);
				}
			}
		}

        private void ExpandProductTags(NameValueCollection expands, Product product)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ProductTags)))
            {
                TagsService tagService = new TagsService();
                TList<ZNode.Libraries.DataAccess.Entities.Tags> tagList = tagService.GetByProductID(product.ProductID);
                foreach (var c in tagList)
                {
                    product.TagsCollection.Add(c);
                }
            }
        }

		private void SetFiltering(List<Tuple<string, string, string>> filters, ProductQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.AccountId) SetQueryParameter(ProductColumn.AccountID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ExternalId) SetQueryParameter(ProductColumn.ExternalID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsActive) SetQueryParameter(ProductColumn.ActiveInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsFeatured) SetQueryParameter(ProductColumn.FeaturedInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsNew) SetQueryParameter(ProductColumn.NewProductInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ManufacturerId) SetQueryParameter(ProductColumn.ManufacturerID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Name) SetQueryParameter(ProductColumn.Name, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Number) SetQueryParameter(ProductColumn.ProductNum, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PortalId) SetQueryParameter(ProductColumn.PortalID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ProductTypeId) SetQueryParameter(ProductColumn.ProductTypeID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.RetailPrice) SetQueryParameter(ProductColumn.RetailPrice, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.SalePrice) SetQueryParameter(ProductColumn.SalePrice, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ShippingRuleTypeId) SetQueryParameter(ProductColumn.ShippingRuleTypeID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.SupplierId) SetQueryParameter(ProductColumn.SupplierID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.TaxClassId) SetQueryParameter(ProductColumn.TaxClassID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.WholesalePrice) SetQueryParameter(ProductColumn.WholesalePrice, filterOperator, filterValue, query);
			}
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, VwZnodeProductsCatalogsQuery query)
		{
			if (filters != null)
			{
				foreach (var tuple in filters)
				{
					var filterKey = tuple.Item1;
					var filterOperator = tuple.Item2;
					var filterValue = tuple.Item3;

					if (filterKey == FilterKeys.AccountId) SetQueryParameter(VwZnodeProductsCatalogsColumn.AccountID, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.ExternalId) SetQueryParameter(VwZnodeProductsCatalogsColumn.ExternalID, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.IsActive) SetQueryParameter(VwZnodeProductsCatalogsColumn.ActiveInd, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.IsFeatured) SetQueryParameter(VwZnodeProductsCatalogsColumn.FeaturedInd, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.IsNew) SetQueryParameter(VwZnodeProductsCatalogsColumn.NewProductInd, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.ManufacturerId) SetQueryParameter(VwZnodeProductsCatalogsColumn.ManufacturerID, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.Name) SetQueryParameter(VwZnodeProductsCatalogsColumn.Name, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.Number) SetQueryParameter(VwZnodeProductsCatalogsColumn.ProductNum, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.PortalId) SetQueryParameter(VwZnodeProductsCatalogsColumn.PortalID, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.ProductTypeId) SetQueryParameter(VwZnodeProductsCatalogsColumn.ProductTypeID, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.RetailPrice) SetQueryParameter(VwZnodeProductsCatalogsColumn.RetailPrice, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.SalePrice) SetQueryParameter(VwZnodeProductsCatalogsColumn.SalePrice, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.ShippingRuleTypeId) SetQueryParameter(VwZnodeProductsCatalogsColumn.ShippingRuleTypeID, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.SupplierId) SetQueryParameter(VwZnodeProductsCatalogsColumn.SupplierID, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.TaxClassId) SetQueryParameter(VwZnodeProductsCatalogsColumn.TaxClassID, filterOperator, filterValue, query);
					if (filterKey == FilterKeys.WholesalePrice) SetQueryParameter(VwZnodeProductsCatalogsColumn.WholesalePrice, filterOperator, filterValue, query);
				}
			}
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, VwZnodeProductsCategoriesQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.AccountId) SetQueryParameter(VwZnodeProductsCategoriesColumn.AccountID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ExternalId) SetQueryParameter(VwZnodeProductsCategoriesColumn.ExternalID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsActive) SetQueryParameter(VwZnodeProductsCategoriesColumn.ActiveInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsFeatured) SetQueryParameter(VwZnodeProductsCategoriesColumn.FeaturedInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsNew) SetQueryParameter(VwZnodeProductsCategoriesColumn.NewProductInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ManufacturerId) SetQueryParameter(VwZnodeProductsCategoriesColumn.ManufacturerID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Name) SetQueryParameter(VwZnodeProductsCategoriesColumn.Name, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Number) SetQueryParameter(VwZnodeProductsCategoriesColumn.ProductNum, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PortalId) SetQueryParameter(VwZnodeProductsCategoriesColumn.PortalID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ProductTypeId) SetQueryParameter(VwZnodeProductsCategoriesColumn.ProductTypeID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.RetailPrice) SetQueryParameter(VwZnodeProductsCategoriesColumn.RetailPrice, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.SalePrice) SetQueryParameter(VwZnodeProductsCategoriesColumn.SalePrice, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ShippingRuleTypeId) SetQueryParameter(VwZnodeProductsCategoriesColumn.ShippingRuleTypeID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.SupplierId) SetQueryParameter(VwZnodeProductsCategoriesColumn.SupplierID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.TaxClassId) SetQueryParameter(VwZnodeProductsCategoriesColumn.TaxClassID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.WholesalePrice) SetQueryParameter(VwZnodeProductsCategoriesColumn.WholesalePrice, filterOperator, filterValue, query);
			}
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, VwZnodeProductsCategoriesPromotionsQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.AccountId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.AccountID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ExternalId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.ExternalID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsActive) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.ActiveInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsFeatured) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.FeaturedInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsNew) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.NewProductInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ManufacturerId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.ManufacturerID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Name) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.Name, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Number) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.ProductNum, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PortalId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.PortalID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ProductTypeId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.ProductTypeID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.RetailPrice) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.RetailPrice, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.SalePrice) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.SalePrice, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ShippingRuleTypeId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.ShippingRuleTypeID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.SupplierId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.SupplierID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.TaxClassId) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.TaxClassID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.WholesalePrice) SetQueryParameter(VwZnodeProductsCategoriesPromotionsColumn.WholesalePrice, filterOperator, filterValue, query);
			}
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, VwZnodeProductsPortalsQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.AccountId) SetQueryParameter(VwZnodeProductsPortalsColumn.AccountID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ExternalId) SetQueryParameter(VwZnodeProductsPortalsColumn.ExternalID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsActive) SetQueryParameter(VwZnodeProductsPortalsColumn.ActiveInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsFeatured) SetQueryParameter(VwZnodeProductsPortalsColumn.FeaturedInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsNew) SetQueryParameter(VwZnodeProductsPortalsColumn.NewProductInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ManufacturerId) SetQueryParameter(VwZnodeProductsPortalsColumn.ManufacturerID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Name) SetQueryParameter(VwZnodeProductsPortalsColumn.Name, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Number) SetQueryParameter(VwZnodeProductsPortalsColumn.ProductNum, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PortalId) SetQueryParameter(VwZnodeProductsPortalsColumn.PortalID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ProductTypeId) SetQueryParameter(VwZnodeProductsPortalsColumn.ProductTypeID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.RetailPrice) SetQueryParameter(VwZnodeProductsPortalsColumn.RetailPrice, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.SalePrice) SetQueryParameter(VwZnodeProductsPortalsColumn.SalePrice, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ShippingRuleTypeId) SetQueryParameter(VwZnodeProductsPortalsColumn.ShippingRuleTypeID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.SupplierId) SetQueryParameter(VwZnodeProductsPortalsColumn.SupplierID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.TaxClassId) SetQueryParameter(VwZnodeProductsPortalsColumn.TaxClassID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.WholesalePrice) SetQueryParameter(VwZnodeProductsPortalsColumn.WholesalePrice, filterOperator, filterValue, query);
			}
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, VwZnodeProductsPromotionsQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.AccountId) SetQueryParameter(VwZnodeProductsPromotionsColumn.AccountID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ExternalId) SetQueryParameter(VwZnodeProductsPromotionsColumn.ExternalID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsActive) SetQueryParameter(VwZnodeProductsPromotionsColumn.ActiveInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsFeatured) SetQueryParameter(VwZnodeProductsPromotionsColumn.FeaturedInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsNew) SetQueryParameter(VwZnodeProductsPromotionsColumn.NewProductInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ManufacturerId) SetQueryParameter(VwZnodeProductsPromotionsColumn.ManufacturerID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Name) SetQueryParameter(VwZnodeProductsPromotionsColumn.Name, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Number) SetQueryParameter(VwZnodeProductsPromotionsColumn.ProductNum, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PortalId) SetQueryParameter(VwZnodeProductsPromotionsColumn.PortalID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ProductTypeId) SetQueryParameter(VwZnodeProductsPromotionsColumn.ProductTypeID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.RetailPrice) SetQueryParameter(VwZnodeProductsPromotionsColumn.RetailPrice, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.SalePrice) SetQueryParameter(VwZnodeProductsPromotionsColumn.SalePrice, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ShippingRuleTypeId) SetQueryParameter(VwZnodeProductsPromotionsColumn.ShippingRuleTypeID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.SupplierId) SetQueryParameter(VwZnodeProductsPromotionsColumn.SupplierID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.TaxClassId) SetQueryParameter(VwZnodeProductsPromotionsColumn.TaxClassID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.WholesalePrice) SetQueryParameter(VwZnodeProductsPromotionsColumn.WholesalePrice, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection expands, ProductSortBuilder sortBuilder)
		{
			if (expands != null && expands.HasKeys())
			{
				foreach (var key in expands.AllKeys)
				{
					var value = expands.Get(key);

					if (key == SortKeys.DisplayOrder) SetSortDirection(ProductColumn.DisplayOrder, value, sortBuilder);
					if (key == SortKeys.Name) SetSortDirection(ProductColumn.Name, value, sortBuilder);
					if (key == SortKeys.Number) SetSortDirection(ProductColumn.ProductNum, value, sortBuilder);
					if (key == SortKeys.ProductId) SetSortDirection(ProductColumn.ProductID, value, sortBuilder);
					if (key == SortKeys.RetailPrice) SetSortDirection(ProductColumn.RetailPrice, value, sortBuilder);
					if (key == SortKeys.SalePrice) SetSortDirection(ProductColumn.SalePrice, value, sortBuilder);
					if (key == SortKeys.WholesalePrice) SetSortDirection(ProductColumn.WholesalePrice, value, sortBuilder);
				}
			}
		}

		private void SetSorting(NameValueCollection expands, VwZnodeProductsCatalogsSortBuilder sortBuilder)
		{
			if (expands != null && expands.HasKeys())
			{
				foreach (var key in expands.AllKeys)
				{
					var value = expands.Get(key);

					if (key == SortKeys.DisplayOrder) SetSortDirection(VwZnodeProductsCatalogsColumn.DisplayOrder, value, sortBuilder);
					if (key == SortKeys.Name) SetSortDirection(VwZnodeProductsCatalogsColumn.Name, value, sortBuilder);
					if (key == SortKeys.Number) SetSortDirection(VwZnodeProductsCatalogsColumn.ProductNum, value, sortBuilder);
					if (key == SortKeys.ProductId) SetSortDirection(VwZnodeProductsCatalogsColumn.ProductID, value, sortBuilder);
					if (key == SortKeys.RetailPrice) SetSortDirection(VwZnodeProductsCatalogsColumn.RetailPrice, value, sortBuilder);
					if (key == SortKeys.SalePrice) SetSortDirection(VwZnodeProductsCatalogsColumn.SalePrice, value, sortBuilder);
					if (key == SortKeys.WholesalePrice) SetSortDirection(VwZnodeProductsCatalogsColumn.WholesalePrice, value, sortBuilder);
				}
			}
		}

		private void SetSorting(NameValueCollection expands, VwZnodeProductsCategoriesSortBuilder sortBuilder)
		{
			if (expands != null && expands.HasKeys())
			{
				foreach (var key in expands.AllKeys)
				{
					var value = expands.Get(key);

					if (key == SortKeys.DisplayOrder) SetSortDirection(VwZnodeProductsCategoriesColumn.DisplayOrder, value, sortBuilder);
					if (key == SortKeys.Name) SetSortDirection(VwZnodeProductsCategoriesColumn.Name, value, sortBuilder);
					if (key == SortKeys.Number) SetSortDirection(VwZnodeProductsCategoriesColumn.ProductNum, value, sortBuilder);
					if (key == SortKeys.ProductId) SetSortDirection(VwZnodeProductsCategoriesColumn.ProductID, value, sortBuilder);
					if (key == SortKeys.RetailPrice) SetSortDirection(VwZnodeProductsCategoriesColumn.RetailPrice, value, sortBuilder);
					if (key == SortKeys.SalePrice) SetSortDirection(VwZnodeProductsCategoriesColumn.SalePrice, value, sortBuilder);
					if (key == SortKeys.WholesalePrice) SetSortDirection(VwZnodeProductsCategoriesColumn.WholesalePrice, value, sortBuilder);
				}
			}
		}

		private void SetSorting(NameValueCollection expands, VwZnodeProductsCategoriesPromotionsSortBuilder sortBuilder)
		{
			if (expands != null && expands.HasKeys())
			{
				foreach (var key in expands.AllKeys)
				{
					var value = expands.Get(key);

					if (key == SortKeys.DisplayOrder) SetSortDirection(VwZnodeProductsCategoriesPromotionsColumn.DisplayOrder, value, sortBuilder);
					if (key == SortKeys.Name) SetSortDirection(VwZnodeProductsCategoriesPromotionsColumn.Name, value, sortBuilder);
					if (key == SortKeys.Number) SetSortDirection(VwZnodeProductsCategoriesPromotionsColumn.ProductNum, value, sortBuilder);
					if (key == SortKeys.ProductId) SetSortDirection(VwZnodeProductsCategoriesPromotionsColumn.ProductID, value, sortBuilder);
					if (key == SortKeys.RetailPrice) SetSortDirection(VwZnodeProductsCategoriesPromotionsColumn.RetailPrice, value, sortBuilder);
					if (key == SortKeys.SalePrice) SetSortDirection(VwZnodeProductsCategoriesPromotionsColumn.SalePrice, value, sortBuilder);
					if (key == SortKeys.WholesalePrice) SetSortDirection(VwZnodeProductsCategoriesPromotionsColumn.WholesalePrice, value, sortBuilder);
				}
			}
		}

		private void SetSorting(NameValueCollection expands, VwZnodeProductsPromotionsSortBuilder sortBuilder)
		{
			if (expands != null && expands.HasKeys())
			{
				foreach (var key in expands.AllKeys)
				{
					var value = expands.Get(key);

					if (key == SortKeys.DisplayOrder) SetSortDirection(VwZnodeProductsPromotionsColumn.DisplayOrder, value, sortBuilder);
					if (key == SortKeys.Name) SetSortDirection(VwZnodeProductsPromotionsColumn.Name, value, sortBuilder);
					if (key == SortKeys.Number) SetSortDirection(VwZnodeProductsPromotionsColumn.ProductNum, value, sortBuilder);
					if (key == SortKeys.ProductId) SetSortDirection(VwZnodeProductsPromotionsColumn.ProductID, value, sortBuilder);
					if (key == SortKeys.RetailPrice) SetSortDirection(VwZnodeProductsPromotionsColumn.RetailPrice, value, sortBuilder);
					if (key == SortKeys.SalePrice) SetSortDirection(VwZnodeProductsPromotionsColumn.SalePrice, value, sortBuilder);
					if (key == SortKeys.WholesalePrice) SetSortDirection(VwZnodeProductsPromotionsColumn.WholesalePrice, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(ProductColumn column, string filterOperator, string filterValue, ProductQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetQueryParameter(VwZnodeProductsPortalsColumn column, string filterOperator, string filterValue, VwZnodeProductsPortalsQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(ProductColumn column, string value, ProductSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}