﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using InventoryRepository = ZNode.Libraries.DataAccess.Service.SKUInventoryService;
using SkuRepository = ZNode.Libraries.DataAccess.Service.SKUService;

namespace Znode.Engine.Services
{
	public class InventoryService : BaseService, IInventoryService
	{
		private readonly InventoryRepository _inventoryRepository;
		private readonly SkuRepository _skuRepository;

		public InventoryService()
		{
			_inventoryRepository = new InventoryRepository();
			_skuRepository = new SkuRepository();
		}

		public InventoryModel GetInventory(int inventoryId, NameValueCollection expands)
		{
			var inventory = _inventoryRepository.GetBySKUInventoryID(inventoryId);
			if (inventory != null)
			{
				GetExpands(expands, inventory);
			}

			return InventoryMap.ToModel(inventory);
		}

		public InventoryListModel GetInventories(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new InventoryListModel();
			var inventories = new TList<SKUInventory>();
			var tempList = new TList<SKUInventory>();

			var query = new SKUInventoryQuery();
			var sortBuilder = new SKUInventorySortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			tempList = _inventoryRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Now go through the list and get any expands
			foreach (var inventory in tempList)
			{
				GetExpands(expands, inventory);
				inventories.Add(inventory);
			}

			// Map each item and add to the list
			foreach (var i in inventories)
			{
				model.Inventories.Add(InventoryMap.ToModel(i));
			}

			return model;
		}

		public InventoryModel CreateInventory(InventoryModel model)
		{
			if (model == null)
			{
				throw new Exception("Inventory model cannot be null.");
			}

			var entity = InventoryMap.ToEntity(model);
			var inventory = _inventoryRepository.Save(entity);
			return InventoryMap.ToModel(inventory);
		}

		public InventoryModel UpdateInventory(int inventoryId, InventoryModel model)
		{
			if (inventoryId < 1)
			{
				throw new Exception("Inventory ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Inventory model cannot be null.");
			}

			var inventory = _inventoryRepository.GetBySKUInventoryID(inventoryId);
			if (inventory != null)
			{
				// Set the inventory ID and map
				model.InventoryId = inventoryId;
				var inventoryToUpdate = InventoryMap.ToEntity(model);

				var updated = _inventoryRepository.Update(inventoryToUpdate);
				if (updated)
				{
					inventory = _inventoryRepository.GetBySKUInventoryID(inventoryId);
					return InventoryMap.ToModel(inventory);
				}
			}

			return null;
		}

		public bool DeleteInventory(int inventoryId)
		{
			if (inventoryId < 1)
			{
				throw new Exception("Inventory ID cannot be less than 1.");
			}

			var inventory = _inventoryRepository.GetBySKUInventoryID(inventoryId);
			if (inventory != null)
			{
				return _inventoryRepository.Delete(inventory);
			}

			return false;
		}

		private void GetExpands(NameValueCollection expands, SKUInventory inventory)
		{
			if (expands.HasKeys())
			{
				ExpandSkus(expands, inventory);
			}
		}

		private void ExpandSkus(NameValueCollection expands, SKUInventory inventory)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Skus)))
			{
				var skus = _skuRepository.GetBySKU(inventory.SKU);
				if (skus != null)
				{
					inventory.SKUCollection = skus;
				}
			}
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, SKUInventoryQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.QuantityOnHand) SetQueryParameter(SKUInventoryColumn.QuantityOnHand, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Sku) SetQueryParameter(SKUInventoryColumn.SKU, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, SKUInventorySortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.QuantityOnHand) SetSortDirection(SKUInventoryColumn.QuantityOnHand, value, sortBuilder);
					if (key == SortKeys.Sku) SetSortDirection(SKUInventoryColumn.SKU, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(SKUInventoryColumn column, string filterOperator, string filterValue, SKUInventoryQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(SKUInventoryColumn column, string value, SKUInventorySortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
