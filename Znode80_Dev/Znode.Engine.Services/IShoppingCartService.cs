﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IShoppingCartService
	{
	    ShoppingCartModel GetCartByAccount(int accountId, int portalId);
	    ShoppingCartModel GetCartByCookie(int cookieId);
        ShoppingCartModel CreateCart(int portalId, ShoppingCartModel model);
		ShoppingCartModel Calculate(int profileId, ShoppingCartModel model);
	}
}