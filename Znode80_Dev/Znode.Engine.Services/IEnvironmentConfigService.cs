﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
    public interface IEnvironmentConfigService
    {
        EnvironmentConfigModel GetEnvironmentConfig();
    }
}
