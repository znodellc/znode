﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface ITaxClassService
	{
		TaxClassModel GetTaxClass(int taxClassId);
		TaxClassListModel GetTaxClasses(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		TaxClassModel CreateTaxClass(TaxClassModel model);
		TaxClassModel UpdateTaxClass(int taxClassId, TaxClassModel model);
		bool DeleteTaxClass(int taxClassId);
        /// <summary>
        /// To get Active Tax Class by PortalId
        /// </summary>
        /// <param name="portalId">int PortalId</param>
        /// <returns>List of active tax class having portalId equal to supplied portalId or null</returns>
        TaxClassListModel GetActiveTaxClassByPortalId(int portalId);
	}
}
