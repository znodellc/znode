﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface ICatalogService
	{
		CatalogModel GetCatalog(int catalogId);
		CatalogListModel GetCatalogs(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
	    CatalogListModel GetCatalogsByCatalogIds(string catalogIds, NameValueCollection expands, NameValueCollection sorts);
		CatalogModel CreateCatalog(CatalogModel model);
		CatalogModel UpdateCatalog(int catalogId, CatalogModel model);
		bool DeleteCatalog(int catalogId);

        /// <summary>
        /// Znode Version 8.0
        /// Copies an existing catalog.
        /// </summary>
        /// <param name="model">The model of type CatalogModel.</param>
        /// <returns>Returns true or false.</returns>
        bool CopyCatalog(CatalogModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Deletes an existing catalog.
        /// </summary>
        /// <param name="catalogId">The Id of catalog</param>
        /// <param name="preserveCategories">boolean value for preserveCategories</param>
        /// <returns>Returns true or false.</returns>
        bool DeleteCatalogUsingCustomService(int catalogId,bool preserveCategories);
	}
}
