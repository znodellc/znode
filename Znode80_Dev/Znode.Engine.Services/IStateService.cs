﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IStateService
	{
		StateModel GetState(string stateCode);
		StateListModel GetStates(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		StateModel CreateState(StateModel model);
		StateModel UpdateState(string stateCode, StateModel model);
		bool DeleteState(string stateCode);
	}
}
