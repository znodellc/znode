﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ShippingTypeRepository = ZNode.Libraries.DataAccess.Service.ShippingTypeService;

namespace Znode.Engine.Services
{
	public class ShippingTypeService : BaseService, IShippingTypeService
	{
		private readonly ShippingTypeRepository _shippingTypeRepository;

		public ShippingTypeService()
		{
			_shippingTypeRepository = new ShippingTypeRepository();
		}

		public ShippingTypeModel GetShippingType(int shippingTypeId)
		{
			var shippingType = _shippingTypeRepository.GetByShippingTypeID(shippingTypeId);
			return ShippingTypeMap.ToModel(shippingType);
		}

		public ShippingTypeListModel GetShippingTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new ShippingTypeListModel();
			var shippingTypes = new TList<ShippingType>();

			var query = new ShippingTypeQuery();
			var sortBuilder = new ShippingTypeSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			shippingTypes = _shippingTypeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var s in shippingTypes)
			{
				model.ShippingTypes.Add(ShippingTypeMap.ToModel(s));
			}

			return model;
		}

		public ShippingTypeModel CreateShippingType(ShippingTypeModel model)
		{
			if (model == null)
			{
				throw new Exception("Shipping type model cannot be null.");
			}

			var entity = ShippingTypeMap.ToEntity(model);
			var shippingType = _shippingTypeRepository.Save(entity);
			return ShippingTypeMap.ToModel(shippingType);
		}

		public ShippingTypeModel UpdateShippingType(int shippingTypeId, ShippingTypeModel model)
		{
			if (shippingTypeId < 1)
			{
				throw new Exception("Shipping type ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Shipping type model cannot be null.");
			}

			var shippingType = _shippingTypeRepository.GetByShippingTypeID(shippingTypeId);
			if (shippingType != null)
			{
				// Set the shipping type ID and map
				model.ShippingTypeId = shippingTypeId;
				var shippingTypeToUpdate = ShippingTypeMap.ToEntity(model);

				var updated = _shippingTypeRepository.Update(shippingTypeToUpdate);
				if (updated)
				{
					shippingType = _shippingTypeRepository.GetByShippingTypeID(shippingTypeId);
					return ShippingTypeMap.ToModel(shippingType);
				}
			}

			return null;
		}

		public bool DeleteShippingType(int shippingTypeId)
		{
			if (shippingTypeId < 1)
			{
				throw new Exception("Shipping type ID cannot be less than 1.");
			}

			var shippingType = _shippingTypeRepository.GetByShippingTypeID(shippingTypeId);
			if (shippingType != null)
			{
				return _shippingTypeRepository.Delete(shippingType);
			}

			return false;
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, ShippingTypeQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.ClassName) SetQueryParameter(ShippingTypeColumn.ClassName, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsActive) SetQueryParameter(ShippingTypeColumn.IsActive, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Name) SetQueryParameter(ShippingTypeColumn.Name, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, ShippingTypeSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.ClassName) SetSortDirection(ShippingTypeColumn.ClassName, value, sortBuilder);
					if (key == SortKeys.Name) SetSortDirection(ShippingTypeColumn.Name, value, sortBuilder);
					if (key == SortKeys.ShippingTypeId) SetSortDirection(ShippingTypeColumn.ShippingTypeID, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(ShippingTypeColumn column, string filterOperator, string filterValue, ShippingTypeQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(ShippingTypeColumn column, string value, ShippingTypeSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
