﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ProductTypeAttributeRepository = ZNode.Libraries.DataAccess.Service.ProductTypeAttributeService;

namespace Znode.Engine.Services
{
    public class ProductTypeAttributeService : BaseService, IProductTypeAttributeService
    {
        #region Public Variables
        private readonly ProductTypeAttributeRepository _productTypeAttributeRepository;
        #endregion

        #region Public Constructor
        public ProductTypeAttributeService()
        {
            _productTypeAttributeRepository = new ProductTypeAttributeRepository();
        }
        #endregion

        #region Public Methods
        public ProductTypeAttributeModel GetAttributeType(int productTypeId)
        {
            var ProductTypeAttribute = _productTypeAttributeRepository.GetByProductAttributeTypeID(productTypeId);
            return ProductTypeAttributeMap.ToModel(ProductTypeAttribute);
        }

        public ProductTypeAttributeListModel GetAttributeTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProductTypeAttributeListModel();
            var productTypes = new TList<ProductTypeAttribute>();

            var query = new ProductTypeAttributeQuery();
            var sort = new ProductTypeAttributeSortBuilder();

            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sort);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            productTypes = _productTypeAttributeRepository.Find(query, sort, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Map each item and add to the list
            foreach (var m in productTypes)
            {
                model.ProductTypeAttribute.Add(ProductTypeAttributeMap.ToModel(m));
            }

            return model;
        }

        public ProductTypeAttributeModel CreateAttributeType(ProductTypeAttributeModel model)
        {
            if (Equals(model , null))
            {
                throw new Exception("Product Type Attribute model cannot be null.");
            }

            var entity = ProductTypeAttributeMap.ToEntity(model);
            var productType = _productTypeAttributeRepository.Save(entity);
            return ProductTypeAttributeMap.ToModel(productType);
        }

        public ProductTypeAttributeModel UpdateAttributeType(int productTypeAttributeId, ProductTypeAttributeModel model)
        {
            if (productTypeAttributeId < 1)
            {
                throw new Exception("ProductAttributeType ID cannot be less than 1.");
            }

            if (Equals(model , null))
            {
                throw new Exception("ProductTypeAttribute model cannot be null.");
            }

            var productAttributeType = _productTypeAttributeRepository.GetByProductAttributeTypeID(productTypeAttributeId);
            if (!Equals(productAttributeType , null))
            {
                // Set manufacturer ID
                model.ProductAttributeTypeID = productTypeAttributeId;

                var productTypeToUpdate = ProductTypeAttributeMap.ToEntity(model);

                if (_productTypeAttributeRepository.Update(productTypeToUpdate))
                {
                    productAttributeType = _productTypeAttributeRepository.GetByProductAttributeTypeID(productTypeAttributeId);

                    return ProductTypeAttributeMap.ToModel(productAttributeType);
                }
            }

            return null;
        }

        public bool DeleteAttributeType(int productTypeId)
        {
            if (productTypeId < 1)
            {
                throw new Exception("productType ID cannot be less than 1.");
            }

            var productTypeAttribute = _productTypeAttributeRepository.GetByProductAttributeTypeID(productTypeId);
            if (!Equals(productTypeAttribute , null))
            {
                return _productTypeAttributeRepository.Delete(productTypeAttribute);
            }

            return false;
        }
        #endregion

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, ProductTypeAttributeQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.ProductAttributeTypeID) SetQueryParameter(ProductTypeAttributeColumn.ProductAttributeTypeID, filterOperator, filterValue, query);

            }
        }

        private void SetSorting(NameValueCollection sorts, ProductTypeAttributeSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.AttributeTypeId) SetSortDirection(ProductTypeAttributeColumn.AttributeTypeId, value, sortBuilder);
                    if (key == SortKeys.ProductTypeId) SetSortDirection(ProductTypeAttributeColumn.ProductTypeId, value, sortBuilder);
                    if (key == SortKeys.ProductAttributeTypeID) SetSortDirection(ProductTypeAttributeColumn.ProductAttributeTypeID, value, sortBuilder);
                }
            }
        }

        private void SetQueryParameter(ProductTypeAttributeColumn column, string filterOperator, string filterValue, ProductTypeAttributeQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(ProductTypeAttributeColumn column, string value, ProductTypeAttributeSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }
        #endregion

        #region Znode Version 8.0
        /// <summary>
        /// Gets list of categories by catalog Id.
        /// </summary>
        /// <param name="productTypeId">The Id of catalog.</param>
        /// <returns>Returns list of categories by catalog Id.</returns>
        public ProductTypeAssociatedAttributeTypesListModel GetAttributeTypesByProductTypeId(int productTypeId, NameValueCollection sorts, NameValueCollection page, out int totalRowCount)
        {
            var model = new ProductTypeAssociatedAttributeTypesListModel();
            var sortBuilder = new VwZnodeCategoriesCatalogsSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            var key = StoredProcKeys.ProductTypeId;
            var value = productTypeId;
            
            Dictionary<string, int> StoredProc = new Dictionary<string, int>();
            StoredProc.Add(key, value);
            
            //Concate key and value and return to whereClause.
            string whereClause = BuildWhereClause(StoredProc);

            SetPaging(page, model, out pagingStart, out pagingLength);
            string orderBy = sortBuilder.ToString();
            pagingStart = Convert.ToInt32(page.Get(PageKeys.Index));

            StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper();

            DataSet resultDataSet = storedProcedureHelper.SP_DataDetailsSpConfig(whereClause, string.Empty/*HavingClause*/, StoredProcKeys.SpGetAttributeTypesBy, orderBy, pagingStart.ToString(), pagingLength.ToString(), out totalRowCount);
            model = ProductTypeAttributeMap.ToModelList(resultDataSet);
            model.TotalResults = totalRowCount;
            model.PageSize = pagingLength;
            model.PageIndex = pagingStart;
            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            return model;
        }

        private string BuildWhereClause(Dictionary<string, int> StoredProc)
        {
            string WhereClause = string.Empty;

            int count = 0;
            foreach (KeyValuePair<string, int> pair in StoredProc)
            {

                WhereClause += StoredProcKeys.TildOperator + pair.Key + StoredProcKeys.TildEqualToOperator + pair.Value;
                if (count < StoredProc.Count - 1)
                {
                    WhereClause = WhereClause + " " + StoredProcKeys.AND + " ";
                }
                count++;
            }
            return WhereClause;
        }

        /// <summary>
        /// Returns a bread crumb path from a delimited string
        /// </summary>
        /// <param name="delimitedString">Delimitted string</param>
        /// <param name="separator">Path seperator.</param>
        /// <returns>Returns the parsed category path.</returns>
        private string ParsePath(string delimitedString, string separator)
        {
            StringBuilder categoryPath = new StringBuilder();

            string[] delim1 = { "||" };
            string[] pathItems = delimitedString.Split(delim1, StringSplitOptions.None);

            foreach (string pathItem in pathItems)
            {
                string[] delim2 = { "%%" };
                string[] items = pathItem.Split(delim2, StringSplitOptions.None);

                string categoryId = items.GetValue(0).ToString();
                string categoryName = items.GetValue(1).ToString();
                categoryPath.Append(categoryName);
                categoryPath.Append(string.Format(" {0} ", separator));
            }

            if (categoryPath.Length > 0)
            {
                if (categoryPath.ToString().LastIndexOf(string.Format(" {0} ", separator)) == categoryPath.ToString().Length - 3)
                {
                    categoryPath = categoryPath.Remove(categoryPath.ToString().Length - 3, 3);
                }
            }

            return categoryPath.ToString();
        }

        /// <summary>
        /// Get Existance of Products associated with Product Type.
        /// </summary>
        /// <param name="productTypeId">Product Type Id</param>
        /// <returns>Return True / False</returns>
        public bool IsProductAssociatedProductType(int productTypeId)
        {
            ProductTypeHelper productTypeHelper = new ProductTypeHelper();
            return productTypeHelper.GetProductsByProductTypeId(productTypeId) > 0 ? true : false;
        }
        #endregion
    }
}
