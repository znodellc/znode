﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using SupplierTypeRepository = ZNode.Libraries.DataAccess.Service.SupplierTypeService;

namespace Znode.Engine.Services
{
	public class SupplierTypeService : BaseService, ISupplierTypeService
	{
		private readonly SupplierTypeRepository _supplierTypeRepository;

		public SupplierTypeService()
		{
			_supplierTypeRepository = new SupplierTypeRepository();
		}

		public SupplierTypeModel GetSupplierType(int supplierTypeId)
		{
			var supplierType = _supplierTypeRepository.GetBySupplierTypeID(supplierTypeId);
			return SupplierTypeMap.ToModel(supplierType);
		}

		public SupplierTypeListModel GetSupplierTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new SupplierTypeListModel();
			var supplierTypes = new TList<SupplierType>();

			var query = new SupplierTypeQuery();
			var sortBuilder = new SupplierTypeSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			supplierTypes = _supplierTypeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var s in supplierTypes)
			{
				model.SupplierTypes.Add(SupplierTypeMap.ToModel(s));
			}

			return model;
		}

		public SupplierTypeModel CreateSupplierType(SupplierTypeModel model)
		{
			if (model == null)
			{
				throw new Exception("Supplier type model cannot be null.");
			}

			var entity = SupplierTypeMap.ToEntity(model);
			var supplierType = _supplierTypeRepository.Save(entity);
			return SupplierTypeMap.ToModel(supplierType);
		}

		public SupplierTypeModel UpdateSupplierType(int supplierTypeId, SupplierTypeModel model)
		{
			if (supplierTypeId < 1)
			{
				throw new Exception("Supplier type ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Supplier type model cannot be null.");
			}

			var supplierType = _supplierTypeRepository.GetBySupplierTypeID(supplierTypeId);
			if (supplierType != null)
			{
				// Set the supplier type ID and map
				model.SupplierTypeId = supplierTypeId;
				var supplierTypeToUpdate = SupplierTypeMap.ToEntity(model);

				var updated = _supplierTypeRepository.Update(supplierTypeToUpdate);
				if (updated)
				{
					supplierType = _supplierTypeRepository.GetBySupplierTypeID(supplierTypeId);
					return SupplierTypeMap.ToModel(supplierType);
				}
			}

			return null;
		}

		public bool DeleteSupplierType(int supplierTypeId)
		{
			if (supplierTypeId < 1)
			{
				throw new Exception("Supplier type ID cannot be less than 1.");
			}

			var supplierType = _supplierTypeRepository.GetBySupplierTypeID(supplierTypeId);
			if (supplierType != null)
			{
				return _supplierTypeRepository.Delete(supplierType);
			}

			return false;
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, SupplierTypeQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.ClassName) SetQueryParameter(SupplierTypeColumn.ClassName, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsActive) SetQueryParameter(SupplierTypeColumn.ActiveInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Name) SetQueryParameter(SupplierTypeColumn.Name, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, SupplierTypeSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.ClassName) SetSortDirection(SupplierTypeColumn.ClassName, value, sortBuilder);
					if (key == SortKeys.Name) SetSortDirection(SupplierTypeColumn.Name, value, sortBuilder);
					if (key == SortKeys.SupplierTypeId) SetSortDirection(SupplierTypeColumn.SupplierTypeID, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(SupplierTypeColumn column, string filterOperator, string filterValue, SupplierTypeQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(SupplierTypeColumn column, string value, SupplierTypeSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
