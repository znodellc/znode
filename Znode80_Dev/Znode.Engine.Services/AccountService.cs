﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using WebMatrix.WebData;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using AccountProfileRepositry = ZNode.Libraries.DataAccess.Service.AccountProfileService;
using AccountRepository = ZNode.Libraries.DataAccess.Service.AccountService;
using AccountTypeRepository = ZNode.Libraries.DataAccess.Service.AccountTypeService;
using AddressRepositry = ZNode.Libraries.DataAccess.Service.AddressService;
using GiftCardRepository = ZNode.Libraries.DataAccess.Service.GiftCardService;
using OrderLineItemRepository = ZNode.Libraries.DataAccess.Service.OrderLineItemService;
using OrderRepositry = ZNode.Libraries.DataAccess.Service.OrderService;
using OrderShipmentRepository = ZNode.Libraries.DataAccess.Service.OrderShipmentService;
using PortalProfileRepository = ZNode.Libraries.DataAccess.Service.PortalProfileService;
using PortalRepository = ZNode.Libraries.DataAccess.Service.PortalService;
using ProfileRepositry = ZNode.Libraries.DataAccess.Service.ProfileService;
using WishListRepository = ZNode.Libraries.DataAccess.Service.WishListService;

namespace Znode.Engine.Services
{
    public class AccountService : BaseService, IAccountService
    {
        #region Private Variables
        private readonly AccountProfileRepositry _accountProfileRepository;
        private readonly AccountRepository _accountRepository;
        private readonly AccountTypeRepository _accountTypeRepository;
        private readonly AddressRepositry _addressRepository;
        private readonly GiftCardRepository _giftCardRepository;
        private readonly OrderRepositry _orderRepository;
        private readonly OrderLineItemRepository _orderLineItemRepository;
        private readonly OrderShipmentRepository _orderShipmentRepository;
        private readonly PortalRepository _portalRepository;
        private readonly PortalProfileRepository _portalProfileRepository;
        private readonly ProfileRepositry _profileRepository;
        private readonly ZNodeUserAccountBase _userAccountBase;
        private readonly WishListRepository _wishlistRepository;
        //Znode Version 7.2.2
        //Gets the membership provider details for the key "ZNodeMembershipProvider".
        private MembershipProvider _membershipProvider = Membership.Providers["ZNodeMembershipProvider"];
        #endregion

        #region Public Variables
        //Znode Version 7.2.2
        //MVC User Section, Error message for account gets locked - Start 
        //Gets the membership provider details for the key "ZNodeMembershipProvider".
        public MembershipProvider MembershipProvider
        {
            get { return _membershipProvider; }
            set { _membershipProvider = value; }
        }
        //MVC User Section, Error message for account gets locked - End
        #endregion

        public AccountService()
        {
            _accountProfileRepository = new AccountProfileRepositry();
            _accountRepository = new AccountRepository();
            _accountTypeRepository = new AccountTypeRepository();
            _addressRepository = new AddressRepositry();
            _giftCardRepository = new GiftCardRepository();
            _orderRepository = new OrderRepositry();
            _orderLineItemRepository = new OrderLineItemRepository();
            _orderShipmentRepository = new OrderShipmentRepository();
            _portalRepository = new PortalRepository();
            _portalProfileRepository = new PortalProfileRepository();
            _profileRepository = new ProfileRepositry();
            _userAccountBase = new ZNodeUserAccountBase();
            _wishlistRepository = new WishListRepository();
        }

        public AccountModel GetAccount(int accountId, NameValueCollection expands)
        {
            var account = _accountRepository.GetByAccountID(accountId);
            if (account != null)
            {
                GetExpands(expands, account);
            }

            return AccountMap.ToModel(account);
        }

        public AccountModel GetAccountByUserId(Guid userId, NameValueCollection expands)
        {
            var account = _accountRepository.GetByUserID(userId);
            if (account[0] != null)
            {
                GetExpands(expands, account[0]);
            }

            return AccountMap.ToModel(account[0]);
        }

        public AccountModel GetAccountByUsername(string username, NameValueCollection expands)
        {
            var user = Membership.GetUser(username);
            if (user == null)
            {
                throw new Exception("User " + username + " not found.");
            }

            var account = _accountRepository.GetByUserID((Guid)user.ProviderUserKey);
            if (account[0] != null)
            {
                GetExpands(expands, account[0]);
            }

            return AccountMap.ToModel(account[0]);
        }

        public AccountListModel GetAccounts(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new AccountListModel();
            var accounts = new TList<Account>();
            var tempList = new TList<Account>();

            var query = new AccountQuery();
            var sortBuilder = new AccountSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _accountRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var account in tempList)
            {
                GetExpands(expands, account);
                accounts.Add(account);
            }

            // Map each item and add to the list
            foreach (var a in accounts)
            {
                model.Accounts.Add(AccountMap.ToModel(a));
            }

            return model;
        }

        public AccountModel CreateAccount(int portalId, AccountModel model)
        {
            if (Equals(model,null))
            {
                throw new Exception("Account model cannot be null.");
            }

            if (Equals(GetDefaultRegisteredProfile(portalId), null))
            {
                throw new ZnodeException(ErrorCodes.ProfileNotPresent, "You can not register new account, please contact administrator.");
            }

            var account = AccountMap.ToEntity(model);

            var expand = new NameValueCollection { { ExpandKeys.Addresses, ExpandKeys.Addresses }, { ExpandKeys.Profiles, ExpandKeys.Profiles } };

            if (!Equals(model.User, null) && !String.IsNullOrEmpty(model.User.Username))
            {
                // Check if username is available                 
                //var usernameAvailable = IsUsernameAvailable(0, model.User.Username);
                if (!WebSecurity.UserExists(model.User.Username))
                {
                    Guid UserId;
                    string confirmationToken = CreateNewUser(model, out UserId);

                    //Set the confirmation for the user.
                    if (!Equals(confirmationToken, null))
                    {
                        WebSecurity.ConfirmAccount(confirmationToken);
                    }
                    else
                    {
                        throw new ZnodeException(ErrorCodes.MembershipError, "Could not create user.");
                    }
                    account.Email = model.User.Email;
                    account.UserID = UserId;

                    // Add registered profile information and other details 
                    UpdateRegisterdProfile(account, portalId);

                    //TODO// Log the password
                    ZNodeUserAccountBase.LogPassword((Guid)account.UserID, model.User.Password);

                    //Send Mail
                    SendMailUserAccount(model.User.Username, model.User.Password, model.User.Email);
                }
                else
                {
                    throw new ZnodeException(ErrorCodes.UserNameUnavailable, "Username " + model.User.Username + " is not available.");
                }

                expand.Add(new NameValueCollection() { { ExpandKeys.User, ExpandKeys.User } });
            }
            else
            {
                // Add anonmyous profile information
                UpdateAnonmyousProfile(account, portalId);
            }

            // Set the dates and save account
            account.CreateDte = DateTime.Now;
            account.UpdateDte = DateTime.Now;
            var savedAccount = _accountRepository.Save(account);

            // Add account profile
            var accountProfile = new AccountProfile { AccountID = savedAccount.AccountID, ProfileID = savedAccount.ProfileID };
            _accountProfileRepository.Save(accountProfile);

            // Now get back to newly created account and expand addresses and profiles by default
            var newlyCreatedAccount = _accountRepository.GetByAccountID(savedAccount.AccountID);

            GetExpands(expand, newlyCreatedAccount);

            return AccountMap.ToModel(newlyCreatedAccount);
        }

        public AccountModel UpdateAccount(int accountId, AccountModel model)
        {
            if (accountId < 1)
            {
                throw new Exception("Account ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Account model cannot be null.");
            }

            var account = _accountRepository.GetByAccountID(accountId);
            if (account != null)
            {
                // Check to update the user's email address
                if (model.User != null && account.Email != model.User.Email)
                {
                    // Update the email on the account
                    account.Email = model.User.Email;

                    // Update the email on the membership user
                    var user = Membership.GetUser(model.User.Username);
                    if (user != null)
                    {
                        user.Email = model.User.Email;
                        Membership.UpdateUser(user);
                    }
                }

                // Set the account ID
                model.AccountId = accountId;

                var accountToUpdate = AccountMap.ToEntity(model, account);

                var updated = _accountRepository.Update(accountToUpdate);
                if (updated)
                {
                    account = _accountRepository.GetByAccountID(accountId);
                    return AccountMap.ToModel(account);
                }
            }

            return null;
        }

        public bool DeleteAccount(int accountId)
        {
            if (accountId < 1)
            {
                throw new Exception("Account ID cannot be less than 1.");
            }

            var account = _accountRepository.GetByAccountID(accountId);
            if (account != null)
            {
                // Delete the addresses
                var addresses = _addressRepository.GetByAccountID(accountId);
                foreach (var a in addresses)
                {
                    _addressRepository.Delete(a.AddressID);
                }

                // Delete the account profiles
                var profiles = _accountProfileRepository.GetByAccountID(accountId);
                foreach (var p in profiles)
                {
                    _accountProfileRepository.Delete(p.AccountProfileID);
                }

                // Delete the membership user
                var username = Membership.GetUserNameByEmail(account.Email);
                Membership.DeleteUser(username, true);

                // Now delete the account itself
                return _accountRepository.Delete(account);
            }

            return false;
        }

        public AccountModel Login(int portalId, AccountModel model, out string errorCode, NameValueCollection expand)
        {
            if(Equals(model,null))
            {
                throw new Exception("Account model cannot be null.");
            }

            if (Equals(model.User,null))
            {
                throw new Exception("Account user model cannot be null.");
            }
          
            errorCode = String.Empty;

            if (!WebSecurity.UserExists(model.User.Username))
            {
                throw new Exception("Account user " + model.User.Username + " is not exists.");
            }

            if(!WebSecurity.IsConfirmed(model.User.Username))
            {
                throw new Exception("Account user " + model.User.Username + " is not confirmed.");
            }

            //var successful = _userAccountBase.Login(portalId, model.User.Username, model.User.Password);
            bool successful  = WebSecurity.Login(model.User.Username, model.User.Password, persistCookie: false);
            //Gets the user details based on username.
            var membershipUser = Membership.GetUser(model.User.Username);
            
            if (!successful)
            {
                //Znode Version 7.2.2
                //MVC User Section, Error message for account gets locked - Start 
                //Set the Error messages in case of Account gets locked. And Also display warning messages before the final last two attempts.
                CheckForLockedAccount(membershipUser);
                //MVC User Section, Error message for account gets locked - End 
            }

            if (!Equals(membershipUser,null))
            {
                var helperAccess = new AccountHelper();
                //Gets the Details of Reset Password based on User Id
                DataSet dataset = helperAccess.GetMembershipDetailsByUserId(((Guid)membershipUser.ProviderUserKey));
                if (dataset != null && dataset.Tables.Count > 0 && dataset.Tables[0].Rows.Count > 0)
                {
                    //Check for the Password Token.
                    if (!string.IsNullOrEmpty(dataset.Tables[0].Rows[0]["PASSWORDVERIFICATIONTOKEN"].ToString()))
                    {
                        model.User.PasswordToken = dataset.Tables[0].Rows[0]["PASSWORDVERIFICATIONTOKEN"].ToString();
                        errorCode = "0";
                        return model;
                    }
                }

                var accountModel = GetAccountByUserId((Guid)membershipUser.ProviderUserKey, expand);

                // filter current portal related profiles only as to identify this account related to current store.
                var protalProfiles = _portalProfileRepository.GetByPortalID(portalId);
                accountModel.Profiles = new Collection<ProfileModel>(accountModel.Profiles.Where(x => protalProfiles.Any(y => y.ProfileID == x.ProfileId)).ToList());

                //TODO
                var isError = !ZNodeUserAccountBase.CheckLastPasswordChangeDate((Guid)membershipUser.ProviderUserKey, out errorCode);
                if (!isError)
                {
                    errorCode = "0";
                }

                return accountModel;
            }

            return null;
        }

        public AccountModel ChangePassword(int portalId, AccountModel model)
        {

            if (Equals(model, null))
            {
                throw new Exception("Account model cannot be null.");
            }
            if (Equals(model.User,null))
            {
                throw new Exception("Account user model cannot be null.");
            }
            var user = Membership.GetUser(model.User.Username);
            if (Equals(user,null))
            {
                throw new Exception("User " + model.User.Username + " not found.");
            }
            bool isResetPassword = (string.IsNullOrEmpty(model.User.PasswordToken)) ? false : true;

            if (!isResetPassword && !Membership.ValidateUser(model.User.Username, model.User.Password))
            {
                throw new Exception("Password mismatch.");
            }
            if (isResetPassword && !WebSecurity.IsResetPasswordTokenValid(model.User.PasswordToken))
            {
                throw new Exception("The reset link you clicked has expired. Please request a new one.");
            }

            // Verify if the new password specified by the user is in the list of the last 4 passwords used
            var verified = (isResetPassword) ? true : ZNodeUserAccountBase.VerifyNewPassword((Guid)user.ProviderUserKey, model.User.NewPassword);
            if (verified)
            {
                // Update/Reset the password for this user
                var passwordChanged = (isResetPassword) 
                    ? WebSecurity.ResetPassword(model.User.PasswordToken, model.User.NewPassword) 
                    : user.ChangePassword(model.User.Password, model.User.NewPassword);
                if (passwordChanged)
                {
                    // Log password
                    ZNodeUserAccountBase.LogPassword((Guid)user.ProviderUserKey, model.User.NewPassword);
                    model.User.Password = model.User.NewPassword;
                    var errorCode = String.Empty;
                    return Login(portalId, model, out errorCode, new NameValueCollection());
                }
            }

            // Otherwise
            throw new Exception("Cannot use any of the four previous passwords.");
        }

        public AccountModel ResetPassword(AccountModel model)
        {
            if(Equals(model, null))
            {
                throw new Exception("Account model cannot be null.");
            }

            if (Equals(model.User, null))
            {
                throw new Exception("Account user model cannot be null.");
            }

            var membershipUser = Membership.GetUser(model.User.Username);
            if (Equals(membershipUser,null))
            {
                throw new Exception("User " + model.User.Username + " not found.");
            }

            if (!membershipUser.IsApproved)
            {
                throw new Exception("User " + model.User.Username + " is not approved.");
            }

            // Do the reset
            //var newPassword = membershipUser.ResetPassword(model.User.PasswordAnswer);
            string passwordResetToken = WebSecurity.GeneratePasswordResetToken(model.User.Username);

            //TODO// delete existing passwords as user needs to force the reset password at next login
            var helperAccess = new AccountHelper();
            helperAccess.DeletePasswordLogByUserId(((Guid)membershipUser.ProviderUserKey).ToString());

            //Znode Version 8.0
            //Reset Password Link in Email - Start
            string passwordResetUrl = string.Format("{0}/Account/ResetPassword?passwordToken={1}&userName={2}", model.BaseUrl, WebUtility.UrlEncode(passwordResetToken), WebUtility.UrlEncode(model.User.Username));
            string passwordResetLink = string.Format("<a href=\"{0}\"> here</a>", passwordResetUrl);
            //Reset Password Link in Email - End
            // Send email to the user

            var messageText = string.Empty;
            messageText += "Username: " + model.User.Username + Environment.NewLine;
            messageText += "Password Reset Link: " + passwordResetUrl + Environment.NewLine;

            var defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ResetPasswordLink_en.htm"));
            if (File.Exists(defaultTemplatePath))
            {
                var rw = new StreamReader(defaultTemplatePath);
                messageText = rw.ReadToEnd();

                var rx1 = new Regex("#UserName#", RegexOptions.IgnoreCase);
                messageText = rx1.Replace(messageText, model.User.Username);

                var rx2 = new Regex("#Link#", RegexOptions.IgnoreCase);
                messageText = rx2.Replace(messageText, passwordResetLink);

                var rx3 = new Regex("#Url#", RegexOptions.IgnoreCase);
                messageText = rx3.Replace(messageText, passwordResetUrl);
            }
            var senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            var subject = string.Format("{0}: Password Reset Confirmation", ZNodeConfigManager.SiteConfig.StoreName);

            try
            {
                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(model.User.Email, senderEmail, string.Empty, subject, messageText, true);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, "", "", null, ex.Message, null);
            }
            return model;
        }

        private bool IsUsernameAvailable(int portalId, string userName)
        {
            return _userAccountBase.IsLoginNameAvailable(portalId, userName);
        }

        private Profile GetDefaultRegisteredProfile(int portalId)
        {
            var portal = _portalRepository.GetByPortalID(portalId);
            return _profileRepository.GetByProfileID(portal.DefaultRegisteredProfileID.GetValueOrDefault());
        }

        public Profile GetCustomerProfile(int accountId, int portalId)
        {
            if (accountId == 0)
            {
                return GetDefaultAnonymousProfile(portalId);
            }

            // Account profile
            var accountHelper = new AccountHelper();
            var profileId = accountHelper.GetCustomerProfile(accountId, portalId);

            return _profileRepository.GetByProfileID(profileId);
        }

        private Profile GetDefaultAnonymousProfile(int portalId)
        {
            var portal = _portalRepository.GetByPortalID(portalId);
            return _profileRepository.GetByProfileID(portal.DefaultAnonymousProfileID.GetValueOrDefault());
        }

        private void UpdateRegisterdProfile(Account account, int portalId)
        {
            var profile = GetDefaultRegisteredProfile(portalId);
            if (profile != null)
            {
                account.ProfileID = profile.ProfileID;
            }
        }

        private void UpdateAnonmyousProfile(Account account, int portalId)
        {
            var profile = GetDefaultAnonymousProfile(portalId);
            if (profile != null)
            {
                account.ProfileID = profile.ProfileID;
            }
        }

        private void GetExpands(NameValueCollection expands, Account account)
        {
            if (expands.HasKeys())
            {
                ExpandAccountType(expands, account);
                ExpandAddresses(expands, account);
                ExpandOrders(expands, account);
                ExpandProfiles(expands, account);
                ExpandUser(expands, account);
                ExpandWishList(expands, account);
                ExpandGiftCardHistory(expands, account);
            }
        }

        private void ExpandAccountType(NameValueCollection expands, Account account)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.AccountType)))
            {
                if (account.AccountTypeID.HasValue)
                {
                    var accountType = _accountTypeRepository.GetByAccountTypeID(account.AccountTypeID.Value);
                    if (accountType != null)
                    {
                        account.AccountTypeIDSource = accountType;
                    }
                }
            }
        }

        private void ExpandAddresses(NameValueCollection expands, Account account)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Addresses)))
            {
                var addresses = _addressRepository.GetByAccountID(account.AccountID);
                foreach (var a in addresses)
                {
                    account.AddressCollection.Add(a);
                }
            }
        }

        private void ExpandOrders(NameValueCollection expands, Account account)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Orders)))
            {
                var orders = _orderRepository.GetByAccountID(account.AccountID);
                foreach (var o in orders)
                {
                    account.OrderCollectionGetByAccountID.Add(o);

                    ExpandOrderLineItems(expands, o);
                }
            }
        }

        private void ExpandOrderLineItems(NameValueCollection expands, Order order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.OrderLineItems)))
            {
                var orderLineItems = _orderLineItemRepository.DeepLoadByOrderID(order.OrderID, true, DeepLoadType.IncludeChildren, typeof(OrderLineItem));
                foreach (var item in orderLineItems)
                {
                    // Check to get the order shipment for the item
                    if (item.OrderShipmentID.HasValue)
                    {
                        var orderShipment = _orderShipmentRepository.GetByOrderShipmentID(item.OrderShipmentID.Value);
                        if (orderShipment != null)
                        {
                            item.OrderShipmentIDSource = orderShipment;
                        }
                    }
                }

                order.OrderLineItemCollection = orderLineItems;
            }
        }

        private void ExpandProfiles(NameValueCollection expands, Account account)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Profiles)))
            {
                // get all account profiles, later we filter by portal if needed.
                var accountProfiles = _accountProfileRepository.GetByAccountID(account.AccountID);
                _accountProfileRepository.DeepLoad(accountProfiles, true, DeepLoadType.IncludeChildren, typeof(Profile));

                foreach (var a in accountProfiles)
                {
                    account.AccountProfileCollection.Add(a);
                }
            }
        }

        private void ExpandUser(NameValueCollection expands, Account account)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.User)))
            {
                MembershipUser user = null;

                // First try getting the user by their guid
                if (account.UserID.HasValue)
                {
                    user = WebSecurity.GetUser(account.UserID.Value);
                }

                // Next try getting the user by their email
                if (user == null)
                {
                    var username = WebSecurity.GetUserNameByEmail(account.Email);
                    user = WebSecurity.GetUser(username);
                }

                account.User = user;
            }
        }

        private void ExpandWishList(NameValueCollection expands, Account account)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.WishList)))
            {
                var wishlist = _wishlistRepository.GetByAccountID(account.AccountID);

                foreach (var wish in wishlist)
                {
                    account.WishListCollection.Add(wish);
                }
            }
        }

        private void ExpandGiftCardHistory(NameValueCollection expands, Account account)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.GiftCardHistory)))
            {
                account.GiftCardCollection = _giftCardRepository.DeepLoadByAccountId(account.AccountID, true, DeepLoadType.IncludeChildren, typeof(List<GiftCardHistory>));
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, AccountQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.AccountTypeId) SetQueryParameter(AccountColumn.AccountTypeID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.CompanyName) SetQueryParameter(AccountColumn.CompanyName, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Email) SetQueryParameter(AccountColumn.Email, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.EmailOptIn) SetQueryParameter(AccountColumn.EmailOptIn, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.EnableCustomerPricing) SetQueryParameter(AccountColumn.EnableCustomerPricing, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ExternalId) SetQueryParameter(AccountColumn.ExternalAccountNo, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsActive) SetQueryParameter(AccountColumn.ActiveInd, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ParentAccountId) SetQueryParameter(AccountColumn.ParentAccountID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ProfileId) SetQueryParameter(AccountColumn.ProfileID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.UserId) SetQueryParameter(AccountColumn.UserID, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, AccountSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.AccountId) SetSortDirection(AccountColumn.AccountID, value, sortBuilder);
                    if (key == SortKeys.CompanyName) SetSortDirection(AccountColumn.CompanyName, value, sortBuilder);
                    if (key == SortKeys.CreateDate) SetSortDirection(AccountColumn.CreateDte, value, sortBuilder);
                }
            }
        }

        private void SetQueryParameter(AccountColumn column, string filterOperator, string filterValue, AccountQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(AccountColumn column, string value, AccountSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }

        #region Znode Version 7.2.2
        // check the Reset Password Link current status.
        public AccountModel CheckResetPasswordLinkStatus(AccountModel model, out string errorCode)
        {
            var membershipUser = Membership.GetUser(model.User.Username);
            errorCode = string.Empty;
            if (membershipUser != null)
            {
                errorCode = (WebSecurity.IsResetPasswordTokenValid(model.User.PasswordToken)) ? ErrorCodes.ResetPasswordContinue.ToString() : ErrorCodes.ResetPasswordLinkExpired.ToString();
                return new AccountModel();
            }
            errorCode = ErrorCodes.ResetPasswordNoRecord.ToString();
            return null;
        }

        //Method to check whether the user account gets locked or not & also sets the Warning message before final last two attempts.
        private void CheckForLockedAccount(MembershipUser membershipUser)
        {
            if (!Equals(membershipUser,null))
            {
                //Check whether user accoung is locked or not.
                if (membershipUser.IsLockedOut)
                {
                    throw new ZnodeUnauthorizedException(ErrorCodes.AccountLocked, "Your account has been locked due to maximum invalid login attempt. Please contact zNode support.", HttpStatusCode.Unauthorized);
                }
                else
                {
                    //Gets current failed password atttemt count for setting the message.
                    AccountHelper accountHelper = new AccountHelper();
                    int inValidAttemtCount = accountHelper.GetUserFailedPasswordAttemptCount((Guid)membershipUser.ProviderUserKey);
                    if (inValidAttemtCount > 0)
                    {
                        //Gets Maximum failed password atttemt count from web.config
                        int maxInvalidPasswordAttemptCount = _membershipProvider.MaxInvalidPasswordAttempts;

                        //Set warning error at (MaxFailureAttemptCount - 2) & (MaxFailureAttemptCount - 1) attempt.
                        if ((maxInvalidPasswordAttemptCount - inValidAttemtCount) == 2)
                        {
                            throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, "You are left with TWO more attempts. If failed, your account will get locked.", HttpStatusCode.Unauthorized);
                        }
                        else if ((maxInvalidPasswordAttemptCount - inValidAttemtCount) == 1)
                        {
                            throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, "You are left with ONE more attempt. If failed, your account will get locked.", HttpStatusCode.Unauthorized);
                        }
                        else
                        {
                            throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, "Login failed", HttpStatusCode.Unauthorized);
                        }
                    }
                    else
                    {
                        throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, "Login failed", HttpStatusCode.Unauthorized);
                    }
                }
            }
            else
            {
                throw new ZnodeUnauthorizedException(ErrorCodes.LoginFailed, "Login failed", HttpStatusCode.Unauthorized);
            }
        }


        /// <summary>
        /// Znode Version 7.2.2
        ///  This function will send acknowledge mail to new registerd user.
        ///  To inform users login name and password.
        /// </summary>
        /// <param name="loginName">string loginName</param>
        /// <param name="password">string password</param>
        /// <param name="email">string email</param>
        protected void SendMailUserAccount(string loginName, string password, string email)
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = "Welcome to " + ZNodeConfigManager.SiteConfig.StoreName;
            string customerservicephone = ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber;
            string currentCulture = string.Empty;
            string templatePath = string.Empty;
            string defaultTemplatePath = string.Empty;

            // Template selection
            currentCulture = ZNodeCatalogManager.CultureInfo;

            defaultTemplatePath = HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "UserRegistrationEmailTemplate.html"));

            templatePath = (!string.IsNullOrEmpty(currentCulture))
                ? HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "UserRegistrationEmailTemplate_" + currentCulture + ".html")
                : HttpContext.Current.Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "UserRegistrationEmailTemplate.html"));

            templatePath = defaultTemplatePath;

            StreamReader rw = new StreamReader(templatePath);
            string messageText = rw.ReadToEnd();

            Regex rx1 = new Regex("#UserName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(messageText, loginName);

            Regex rx2 = new Regex("#Password#", RegexOptions.IgnoreCase);
            messageText = rx2.Replace(messageText, password);

            Regex rx3 = new Regex("#UserID#", RegexOptions.IgnoreCase);
            messageText = rx3.Replace(messageText, loginName);

            Regex rx4 = new Regex("#CUSTOMERSERVICEPHONE#", RegexOptions.IgnoreCase);
            messageText = rx4.Replace(messageText, customerservicephone);

            Regex rx5 = new Regex("#CUSTOMERSERVICEMAIL#", RegexOptions.IgnoreCase);
            messageText = rx5.Replace(messageText, senderEmail);

            Regex RegularExpressionLogo = new Regex("#LOGO#", RegexOptions.IgnoreCase);
            messageText = RegularExpressionLogo.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);

            try
            {
                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(email, senderEmail, string.Empty, subject, messageText, true);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, loginName, "", null, ex.Message, null);
            }
        }
        #endregion

        #region Znode Version 8.0
        /// <summary>
        /// Method check for User Role
        /// </summary>
        /// <param name="userName">string userName</param>
        /// <param name="roleName">string roleName</param>
        /// <returns>Returns true or false</returns>
        public AccountModel CheckUserRole(string userName, string roleName)
        {
            AccountModel model = new AccountModel();
            model.IsUserInRole = Roles.IsUserInRole(userName, roleName);
            return model;
        }

        /// <summary>
        /// Method resets the admin details for the first default login.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns reset details.</returns>
        public AccountModel ResetAdminDetails(AccountModel model)
        {
          
            var membershipUser = Membership.GetUser(model.UserId);

            if(Equals(membershipUser,null))
            {
                throw new Exception("Account model cannot be null.");
            }
            if (Equals(model.ErrorCode, "1"))
            {
                var account = _accountRepository.GetByAccountID(model.AccountId);
                string strEmail = membershipUser.Email;
                if (Roles.IsUserInRole(membershipUser.UserName, "admin"))
                {
                    strEmail = model.User.Email.Trim();
                    account.Email = strEmail; // Set Email address
                }
                model.User.Email = strEmail;
                Guid userId;
                if (!WebSecurity.UserExists(model.User.Username))
                {
                    string confirmationToken = CreateNewUser(model, out userId);
                    if (confirmationToken != null)
                    {
                        WebSecurity.ConfirmAccount(confirmationToken);
                        string[] roles = Roles.GetRolesForUser(membershipUser.UserName);
                        if (roles.Length > 0)
                        {
                            // Associate the new user with the roles list
                            Roles.AddUsersToRoles(new string[] { model.User.Username }, roles);
                            
                        }
                        ZNodeUserAccountBase.LogPassword(userId, model.User.Password.Trim());
                        account.UserID = userId;
                        model.UserId = userId;
                        var accountToUpdate = AccountMap.ToEntity(model, account);
                        var updated = _accountRepository.Update(accountToUpdate);
                        WebSecurity.DeleteUser(membershipUser.UserName);
                    }
                    else
                    {
                        throw new ZnodeException(ErrorCodes.MembershipError, "Could not create user.");
                    }

                }
                else
                {
                    throw new ZnodeException(ErrorCodes.UserNameUnavailable, "Username " + model.User.Username + " is not available.");
                }
               

            }



            return new AccountModel();
        }
        /// <summary>
        /// Function Create new user.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="UserId"></param>
        /// <returns>Returns the confirmation token in case user created successfully.</returns>
        private static string CreateNewUser(AccountModel model, out Guid UserId)
        {
            UserId = Guid.NewGuid();
            string LoweredUserName = model.User.Username.ToLower();
            string MobileAlias = string.Empty;
            bool IsAnonymous = false;
            DateTime LastActivityDate = DateTime.Now;

            //Create the new user.
            return WebSecurity.CreateUserAndAccount(model.User.Username, model.User.Password, new
            {
                UserId,
                LoweredUserName,
                model.User.Email,
                MobileAlias,
                IsAnonymous,
                LastActivityDate,
            }, true);
        }
        #endregion
    }
}