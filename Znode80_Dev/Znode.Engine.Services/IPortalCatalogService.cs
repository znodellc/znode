﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IPortalCatalogService
	{
		PortalCatalogModel GetPortalCatalog(int portalCatalogId, NameValueCollection expands);
		PortalCatalogListModel GetPortalCatalogs(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		PortalCatalogModel CreatePortalCatalog(PortalCatalogModel model);
		PortalCatalogModel UpdatePortalCatalog(int portalCatalogId, PortalCatalogModel model);
		bool DeletePortalCatalog(int portalCatalogId);
	}
}
