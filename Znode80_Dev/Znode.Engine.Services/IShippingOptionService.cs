﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IShippingOptionService
	{
		ShippingOptionModel GetShippingOption(int shippingOptionId, NameValueCollection expands);
		ShippingOptionListModel GetShippingOptions(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		ShippingOptionModel CreateShippingOption(ShippingOptionModel model);
		ShippingOptionModel UpdateShippingOption(int shippingOptionId, ShippingOptionModel model);
		bool DeleteShippingOption(int shippingOptionId);
	}
}
