﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface ITaxRuleTypeService
	{
		TaxRuleTypeModel GetTaxRuleType(int taxRuleTypeId);
		TaxRuleTypeListModel GetTaxRuleTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		TaxRuleTypeModel CreateTaxRuleType(TaxRuleTypeModel model);
		TaxRuleTypeModel UpdateTaxRuleType(int taxRuleTypeId, TaxRuleTypeModel model);
		bool DeleteTaxRuleType(int taxRuleTypeId);
	}
}
