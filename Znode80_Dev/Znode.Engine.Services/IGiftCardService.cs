﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IGiftCardService
	{
		GiftCardModel GetGiftCard(int giftCardId, NameValueCollection expands);
		GiftCardListModel GetGiftCards(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		GiftCardModel CreateGiftCard(GiftCardModel model);
		GiftCardModel UpdateGiftCard(int giftCardId, GiftCardModel model);
		bool DeleteGiftCard(int giftCardId);
        
        /// <summary>
        /// CreateGiftCardNumber
        /// </summary>
        /// <returns>GiftCardModel</returns>
        GiftCardModel GetNextGiftCardNumber();
	}
}
