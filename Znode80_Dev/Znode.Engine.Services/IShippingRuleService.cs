﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IShippingRuleService
	{
		ShippingRuleModel GetShippingRule(int shippingRuleId, NameValueCollection expands);
		ShippingRuleListModel GetShippingRules(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		ShippingRuleModel CreateShippingRule(ShippingRuleModel model);
		ShippingRuleModel UpdateShippingRule(int shippingRuleId, ShippingRuleModel model);
		bool DeleteShippingRule(int shippingRuleId);
	}
}
