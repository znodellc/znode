﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using UrlRedirectRepository = ZNode.Libraries.DataAccess.Service.UrlRedirectService;


namespace Znode.Engine.Services
{
    /// <summary>
    /// 
    /// </summary>
    public class UrlRedirectService : BaseService, IUrlRedirectService
    {

        private readonly UrlRedirectRepository _urlRedirectRepository;

        /// <summary>
        /// 
        /// </summary>
        public UrlRedirectService()
        {
            _urlRedirectRepository = new UrlRedirectRepository();
        }

        /// <summary>
        /// Gets an Url redirect
        /// </summary>
        /// <param name="urlRedirectId"></param>
        /// <returns></returns>
        public UrlRedirectModel GetUrlRedirect(int urlRedirectId)
        {
            var urlRedirect = _urlRedirectRepository.GetByUrlRedirectID(urlRedirectId);
            return UrlRedirectMap.ToModel(urlRedirect);
        }

        /// <summary>
        /// Get Url Redirects list.
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="page"></param>
        /// <returns></returns>
        public UrlRedirectListModel GetUrlRedirects(List<Tuple<string, string, string>> filters,
                                                    NameValueCollection page)
        {
            var model = new UrlRedirectListModel();
           
            var query = new UrlRedirectQuery();
            var sortBuilder = new UrlRedirectSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            var urlRedirects = _urlRedirectRepository.Find(query, sortBuilder, pagingStart, pagingLength,
                                                                      out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }
          
            // Map each item and add to the list
            foreach (var c in urlRedirects)
            {
                model.UrlRedirects.Add(UrlRedirectMap.ToModel(c));
            }

            return model;
        }

        /// <summary>
        /// Create a new Url Redirect.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public UrlRedirectModel CreateUrlRedirect(UrlRedirectModel model)
        {
            if (model == null)
            {
                throw new Exception("Url Redirect model cannot be null.");
            }

            var entity = UrlRedirectMap.ToEntity(model);

            var urlRedirect = _urlRedirectRepository.Save(entity);
            return UrlRedirectMap.ToModel(urlRedirect);
        }

        /// <summary>
        /// Update an existing Url Redirect.
        /// </summary>
        /// <param name="urlRedirectId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public UrlRedirectModel UpdateUrlRedirect(int urlRedirectId, UrlRedirectModel model)
        {
            if (urlRedirectId < 1)
            {
                throw new Exception("Url Redirect ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Url Redirect model cannot be null.");
            }

            var urlRedirect = _urlRedirectRepository.GetByUrlRedirectID(urlRedirectId);
            if (urlRedirect != null)
            {
                model.UrlRedirectId = urlRedirectId;

                var urlRedirectToUpdate = UrlRedirectMap.ToEntity(model);

                var updated = _urlRedirectRepository.Update(urlRedirectToUpdate);
                if (updated)
                {
                    urlRedirect = _urlRedirectRepository.GetByUrlRedirectID(urlRedirectId);
                    return UrlRedirectMap.ToModel(urlRedirect);
                }
            }

            return null;
        }

        /// <summary>
        /// Delete an existing Url Redirect.
        /// </summary>
        /// <param name="urlRedirectId"></param>
        /// <returns></returns>
        public bool DeleteUrlRedirect(int urlRedirectId)
        {
            if (urlRedirectId < 1)
            {
                throw new Exception("Url Redirect ID cannot be less than 1.");
            }

            var urlRedirect = _urlRedirectRepository.GetByUrlRedirectID(urlRedirectId);
            return urlRedirect != null && _urlRedirectRepository.Delete(urlRedirect);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filters"></param>
        /// <param name="query"></param>
        private void SetFiltering(List<Tuple<string, string, string>> filters, UrlRedirectQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.UrlRedirectID)
                    SetQueryParameter(UrlRedirectColumn.UrlRedirectID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.OldUrl)
                    SetQueryParameter(UrlRedirectColumn.NewUrl, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.NewUrl)
                    SetQueryParameter(UrlRedirectColumn.OldUrl, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.IsActive)
                    SetQueryParameter(UrlRedirectColumn.IsActive, filterOperator, filterValue, query);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="column"></param>
        /// <param name="filterOperator"></param>
        /// <param name="filterValue"></param>
        /// <param name="query"></param>
        private void SetQueryParameter(UrlRedirectColumn column, string filterOperator, string filterValue, UrlRedirectQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

    }
}
