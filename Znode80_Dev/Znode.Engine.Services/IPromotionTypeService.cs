﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IPromotionTypeService
	{
		PromotionTypeModel GetPromotionType(int promotionTypeId);
		PromotionTypeListModel GetPromotionTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		PromotionTypeModel CreatePromotionType(PromotionTypeModel model);
		PromotionTypeModel UpdatePromotionType(int promotionTypeId, PromotionTypeModel model);
		bool DeletePromotionType(int promotionTypeId);
	}
}
