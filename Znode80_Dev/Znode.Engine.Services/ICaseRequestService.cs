﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface ICaseRequestService
	{
		CaseRequestModel GetCaseRequest(int caseRequestId, NameValueCollection expands);
		CaseRequestListModel GetCaseRequests(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		CaseRequestModel CreateCaseRequest(CaseRequestModel model);
		CaseRequestModel UpdateCaseRequest(int caseRequestId, CaseRequestModel model);
		bool DeleteCaseRequest(int caseRequestId);
	}
}
