﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using CSSRepository = ZNode.Libraries.DataAccess.Service.CSSService;

namespace Znode.Engine.Services
{
    public class CSSService : BaseService, ICSSService
    {
        #region Private Variables
        private readonly CSSRepository _cssRepository;
        #endregion

        public CSSService()
        {
            _cssRepository = new CSSRepository();
        }

        public CSSListModel GetCSSs(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new CSSListModel();
            var cssList = new TList<CSS>();
            var tempList = new TList<CSS>();

            var query = new CSSQuery();
            var sortBuilder = new CSSSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _cssRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var css in tempList)
            {
                GetExpands(expands, css);
                cssList.Add(css);
            }

            // Map each item and add to the list
            foreach (var a in cssList)
            {
                model.CSSs.Add(CSSMap.ToModel(a));
            }

            return model;
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, CSSQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.CSSName) SetQueryParameter(CSSColumn.Name, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.CSSThemeId) SetQueryParameter(CSSColumn.CSSID, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, CSSSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.CSSName) SetSortDirection(CSSColumn.Name, value, sortBuilder);
                }
            }
        }
        private void GetExpands(NameValueCollection expands, CSS css)
        {
            if (expands.HasKeys())
            {

            }
        }

    }
}
