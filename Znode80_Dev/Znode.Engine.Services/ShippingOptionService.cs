﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ShippingRepository = ZNode.Libraries.DataAccess.Service.ShippingService;
using ShippingTypeRepository = ZNode.Libraries.DataAccess.Service.ShippingTypeService;

namespace Znode.Engine.Services
{
	public class ShippingOptionService : BaseService, IShippingOptionService
	{
		private readonly ShippingRepository _shippingRepository;
		private readonly ShippingTypeRepository _shippingTypeRepository;

		public ShippingOptionService()
		{
			_shippingRepository = new ShippingRepository();
			_shippingTypeRepository = new ShippingTypeRepository();
		}

		public ShippingOptionModel GetShippingOption(int shippingOptionId, NameValueCollection expands)
		{
			var shippingOption = _shippingRepository.GetByShippingID(shippingOptionId);
			if (shippingOption != null)
			{
				GetExpands(expands, shippingOption);
			}

			return ShippingOptionMap.ToModel(shippingOption);
		}

		public ShippingOptionListModel GetShippingOptions(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new ShippingOptionListModel();
			var shippingOptions = new TList<Shipping>();
			var tempList = new TList<Shipping>();

			var query = new ShippingQuery();
			var sortBuilder = new ShippingSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			tempList = _shippingRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Now go through the list and get any expands
			foreach (var shippingOption in tempList)
			{
				GetExpands(expands, shippingOption);
				shippingOptions.Add(shippingOption);
			}

			// Map each item and add to the list
			foreach (var s in shippingOptions)
			{
				model.ShippingOptions.Add(ShippingOptionMap.ToModel(s));
			}

			return model;
		}

		public ShippingOptionModel CreateShippingOption(ShippingOptionModel model)
		{
			if (model == null)
			{
				throw new Exception("Shipping option model cannot be null.");
			}

			var entity = ShippingOptionMap.ToEntity(model);
			var shippingOption = _shippingRepository.Save(entity);
			return ShippingOptionMap.ToModel(shippingOption);
		}

		public ShippingOptionModel UpdateShippingOption(int shippingOptionId, ShippingOptionModel model)
		{
			if (shippingOptionId < 1)
			{
				throw new Exception("Shipping option ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Shipping option model cannot be null.");
			}

			var shippingOption = _shippingRepository.GetByShippingID(shippingOptionId);
			if (shippingOption != null)
			{
				// Set the shipping option ID and map
				model.ShippingOptionId = shippingOptionId;
				var shippingOptionToUpdate = ShippingOptionMap.ToEntity(model);

				var updated = _shippingRepository.Update(shippingOptionToUpdate);
				if (updated)
				{
					shippingOption = _shippingRepository.GetByShippingID(shippingOptionId);
					return ShippingOptionMap.ToModel(shippingOption);
				}
			}

			return null;
		}

		public bool DeleteShippingOption(int shippingOptionId)
		{
			if (shippingOptionId < 1)
			{
				throw new Exception("Shipping option ID cannot be less than 1.");
			}

			var shippingOption = _shippingRepository.GetByShippingID(shippingOptionId);
			if (shippingOption != null)
			{
				return _shippingRepository.Delete(shippingOption);
			}

			return false;
		}

		private void GetExpands(NameValueCollection expands, Shipping shippingOption)
		{
			if (expands.HasKeys())
			{
				ExpandShippingType(expands, shippingOption);
			}
		}

		private void ExpandShippingType(NameValueCollection expands, Shipping shippingOption)
		{
			if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ShippingType)))
			{
				var shippingType = _shippingTypeRepository.GetByShippingTypeID(shippingOption.ShippingTypeID);
				if (shippingType != null)
				{
					shippingOption.ShippingTypeIDSource = shippingType;
				}
			}
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, ShippingQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.CountryCode) SetQueryParameter(ShippingColumn.DestinationCountryCode, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ExternalId) SetQueryParameter(ShippingColumn.ExternalID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsActive) SetQueryParameter(ShippingColumn.ActiveInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ProfileId) SetQueryParameter(ShippingColumn.ProfileID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ShippingCode) SetQueryParameter(ShippingColumn.ShippingCode, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.ShippingTypeId) SetQueryParameter(ShippingColumn.ShippingTypeID, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, ShippingSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.CountryCode) SetSortDirection(ShippingColumn.DestinationCountryCode, value, sortBuilder);
					if (key == SortKeys.Description) SetSortDirection(ShippingColumn.Description, value, sortBuilder);
					if (key == SortKeys.DisplayOrder) SetSortDirection(ShippingColumn.DisplayOrder, value, sortBuilder);
					if (key == SortKeys.ShippingCode) SetSortDirection(ShippingColumn.ShippingCode, value, sortBuilder);
					if (key == SortKeys.ShippingOptionId) SetSortDirection(ShippingColumn.ShippingID, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(ShippingColumn column, string filterOperator, string filterValue, ShippingQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(ShippingColumn column, string value, ShippingSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
