﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IPaymentTypeService
	{
		PaymentTypeModel GetPaymentType(int paymentTypeId);
		PaymentTypeListModel GetPaymentTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		PaymentTypeModel CreatePaymentType(PaymentTypeModel model);
		PaymentTypeModel UpdatePaymentType(int paymentTypeId, PaymentTypeModel model);
		bool DeletePaymentType(int paymentTypeId);
	}
}
