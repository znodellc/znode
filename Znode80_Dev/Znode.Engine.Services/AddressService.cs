﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using AddressRepository = ZNode.Libraries.DataAccess.Service.AddressService;

namespace Znode.Engine.Services
{
	public class AddressService : BaseService, IAddressService
	{
		private readonly AddressRepository _addressRepository;

		public AddressService()
		{
			_addressRepository = new AddressRepository();
		}

		public AddressModel GetAddress(int addressId)
		{
			var address = _addressRepository.GetByAddressID(addressId);

			if (address != null)
			{
				return AddressMap.ToModel(address);
			}

			return new AddressModel();
		}

		public AddressListModel GetAddresses(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new AddressListModel();
			var addresses = new TList<Address>();

			var query = new AddressQuery();
			var sortBuilder = new AddressSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			addresses = _addressRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var a in addresses)
			{
				model.Addresses.Add(AddressMap.ToModel(a));
			}

			return model;
		}

		public AddressModel CreateAddress(AddressModel model)
		{
			if (model == null)
			{
				throw new Exception("Address model cannot be null.");
			}

			var address = AddressMap.ToEntity(model);
			_addressRepository.Save(address);
			return AddressMap.ToModel(address);
		}

		public AddressModel UpdateAddress(int addressId, AddressModel model)
		{
			if (addressId < 1)
			{
				throw new Exception("Address ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Address model cannot be null.");
			}

			var address = _addressRepository.GetByAddressID(addressId);
			if (address != null)
			{
				// Set the address ID
				model.AddressId = addressId;

				var updatedEntity = AddressMap.ToEntity(model, address);
				var resultAddress = _addressRepository.Save(updatedEntity);
				return AddressMap.ToModel(resultAddress);
			}

			return null;
		}

		public bool DeleteAddress(int addressId)
		{
			if (addressId < 1)
			{
				throw new Exception("Address ID cannot be less than 1.");
			}

			return _addressRepository.Delete(addressId);
		}

		public bool AddDefaultAddresses(int accountId)
		{
			try
			{
				var billingAddress = AddressMap.GetDefaultBillingAddress(accountId);
				_addressRepository.Save(billingAddress);

				var shippingAddress = AddressMap.GetDefaultShippingAddress(accountId);
				_addressRepository.Save(shippingAddress);
			}
			catch (Exception)
			{
				return false;
			}

			return true;
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, AddressQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.AccountId) SetQueryParameter(AddressColumn.AccountID, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.City) SetQueryParameter(AddressColumn.City, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.CompanyName) SetQueryParameter(AddressColumn.CompanyName, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.CountryCode) SetQueryParameter(AddressColumn.CountryCode, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.LastName) SetQueryParameter(AddressColumn.LastName, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PostalCode) SetQueryParameter(AddressColumn.PostalCode, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.StateCode) SetQueryParameter(AddressColumn.StateCode, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, AddressSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.AddressId) SetSortDirection(AddressColumn.AddressID, value, sortBuilder);
					if (key == SortKeys.CountryCode) SetSortDirection(AddressColumn.CountryCode, value, sortBuilder);
					if (key == SortKeys.PostalCode) SetSortDirection(AddressColumn.PostalCode, value, sortBuilder);
					if (key == SortKeys.StateCode) SetSortDirection(AddressColumn.StateCode, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(AddressColumn column, string filterOperator, string filterValue, AddressQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(AddressColumn column, string value, AddressSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
