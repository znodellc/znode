﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using AccountRepository = ZNode.Libraries.DataAccess.Service.AccountService;
using CategoryRepository = ZNode.Libraries.DataAccess.Service.CategoryService;
using DigitalAssetRepository = ZNode.Libraries.DataAccess.Service.DigitalAssetService;
using OrderLineItemRepository = ZNode.Libraries.DataAccess.Service.OrderLineItemService;
using OrderRepository = ZNode.Libraries.DataAccess.Service.OrderService;
using OrderShipmentRepository = ZNode.Libraries.DataAccess.Service.OrderShipmentService;
using PaymentSettingRepository = ZNode.Libraries.DataAccess.Service.PaymentSettingService;
using PaymentTypeRepository = ZNode.Libraries.DataAccess.Service.PaymentTypeService;
using PortalRepository = ZNode.Libraries.DataAccess.Service.PortalService;
using PortalProfileRepository = ZNode.Libraries.DataAccess.Service.PortalProfileService;
using ProfileRepository = ZNode.Libraries.DataAccess.Service.ProfileService;
using ShippingRepository = ZNode.Libraries.DataAccess.Service.ShippingService;
using ZNodeShoppingCartItem = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem;
using AddOnRepository = ZNode.Libraries.DataAccess.Service.ProductAddOnService;
using AddOnValueRepository = ZNode.Libraries.DataAccess.Service.AddOnValueService;
namespace Znode.Engine.Services
{
    public class ReorderService : BaseService, IReorderService
    {
        private readonly OrderLineItemRepository _orderLineItemRepository;
        private readonly OrderRepository _orderRepository;
        private readonly AddOnRepository _addOnRepository;
        private readonly AddOnValueRepository _addOnValueRepository;
        public ReorderService()
        {
            _orderLineItemRepository = new OrderLineItemRepository();
            _orderRepository = new OrderRepository();
            _addOnRepository = new AddOnRepository();
            _addOnValueRepository = new AddOnValueRepository();
        }


        public CartItemsListModel GetReorderItems(int orderId, NameValueCollection expands)
        {
            //Get order details by orderId
            var orderLineItems = _orderLineItemRepository.GetByOrderID(orderId);

            SkuService objSkuService = new SkuService();
            CartItemsListModel model = new CartItemsListModel();

            orderLineItems.ToList().ForEach(_Items =>
            {
                CartItemsModel CartItemViewModel = new CartItemsModel();
                CartItemViewModel.Sku = _Items.SKU;
                CartItemViewModel.Quantity = (int)_Items.Quantity;

                var skuList = objSkuService.GetSkuList(CartItemViewModel.Sku, expands);
                if (skuList != null)
                {
                    skuList.ForEach(sku =>
                    {
                        CartItemViewModel.ProductId = sku.ProductID;
                    });
                }

                if (CartItemViewModel.ProductId != 0)
                {
                    var childOrdeLineItems = _orderLineItemRepository.GetByParentOrderLineItemID(_Items.OrderLineItemID);
                    string addOnIds = string.Empty;
                    childOrdeLineItems.ForEach(childItem =>
                    {
                        if (addOnIds.Equals(string.Empty))
                        {
                            addOnIds = GetProductAddons(childItem.SKU);
                        }
                        else
                        {
                            addOnIds += "," + GetProductAddons(childItem.SKU);
                        }
                    });

                    CartItemViewModel.AddOnValueIds = addOnIds;

                    model.ReorderItems.Add(CartItemViewModel);
                }
            });

            if (model != null)
            {
                return model;
            }

            return null;
        }

        public CartItemsModel GetReorderSingleItem(int orderLineItemId, NameValueCollection expands)
        {
            //Get orderLineItems by orderLineItemId.
            var orderLineItems = _orderLineItemRepository.GetByOrderLineItemID(orderLineItemId);

            SkuService objSkuService = new SkuService();

            CartItemsModel CartItemViewModel = new CartItemsModel();
            CartItemViewModel.Sku = orderLineItems.SKU;
            CartItemViewModel.Quantity = (int)orderLineItems.Quantity;
            var SKUList = objSkuService.GetSkuList(CartItemViewModel.Sku, expands);

            if (SKUList != null)
            {
                SKUList.ForEach(sku =>
                {
                    CartItemViewModel.ProductId = sku.ProductID;
                });
            }

            if (CartItemViewModel.ProductId != 0)
            {
                var childOrdeLineItems = _orderLineItemRepository.GetByParentOrderLineItemID(orderLineItems.OrderLineItemID);
                string addOnIds = string.Empty;
                childOrdeLineItems.ForEach(childItem =>
                {
                    if (addOnIds.Equals(string.Empty))
                    {
                        addOnIds = GetProductAddons(childItem.SKU);
                    }
                    else
                    {
                        addOnIds += "," + GetProductAddons(childItem.SKU);
                    }
                });

                CartItemViewModel.AddOnValueIds = addOnIds;
            }


            if (CartItemViewModel != null)
            {
                return CartItemViewModel;
            }

            return null;
        }

        //TODO
        public CartItemsListModel GetReorderItemsList(int orderId, int orderLineItemId, bool isOrder, NameValueCollection expands)
        {
            TList<OrderLineItem> itemModel = new TList<OrderLineItem>();

            //if (isOrder)
            //{
            //    foreach (var itms in _orderLineItemRepository.GetByOrderID(orderId))
            //    {
            //        itemModel.Add(itms);
            //    }
            //}
            //else
            //{
            itemModel.Add(_orderLineItemRepository.GetByOrderLineItemID(830));
            //}

            SkuService objSkuService = new SkuService();
            CartItemsListModel model = new CartItemsListModel();
            foreach (var m in itemModel)
            {
                CartItemsModel CartItemViewModel = new CartItemsModel();
                CartItemViewModel.Sku = m.SKU;
                CartItemViewModel.Quantity = (int)m.Quantity;

                foreach (var sku in objSkuService.GetSkuList(CartItemViewModel.Sku, expands))
                {
                    CartItemViewModel.ProductId = sku.ProductID;
                }

                model.ReorderItems.Add(CartItemViewModel);
            }

            if (model != null)
            {
                return model;
            }

            return null;
        }



        public string GetProductAddons(string addonSku)
        {
            var addons = _addOnValueRepository.GetBySKU(addonSku);

            string strAddonIds = string.Empty;
            addons.ForEach(addon =>
            {

                if (strAddonIds.Equals(string.Empty))
                {
                    strAddonIds = addon.AddOnValueID.ToString();
                }
                else
                {
                    strAddonIds += "," + addon.AddOnValueID.ToString();
                }


            });

            return strAddonIds;

        }

    }
}
