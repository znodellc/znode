﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IOrderService
	{
		OrderModel GetOrder(int orderId, NameValueCollection expands);
		OrderListModel GetOrders(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		OrderModel CreateOrder(int portalId,ShoppingCartModel model);
		OrderModel UpdateOrder(int orderId, OrderModel model);
	}
}
