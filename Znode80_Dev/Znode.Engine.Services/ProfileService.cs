﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ProfileRepository = ZNode.Libraries.DataAccess.Service.ProfileService;

namespace Znode.Engine.Services
{
    public class ProfileService : BaseService, IProfileService
    {
        #region Private Variables
        private readonly ProfileRepository _profileRepository; 
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for profile service
        /// </summary>
        public ProfileService()
        {
            _profileRepository = new ProfileRepository();
        } 
        #endregion

        #region Public Methods
        public ProfileListModel GetProfiles(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ProfileListModel();
            var profiles = new TList<Profile>();
            var tempList = new TList<Profile>();

            var query = new ProfileQuery();
            var sortBuilder = new ProfileSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _profileRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (Equals(pagingLength, int.MaxValue))
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var profile in tempList)
            {
                GetExpands(expands, profile);
                profiles.Add(profile);
            }

            // Map each item and add to the list
            foreach (var a in profiles)
            {
                model.Profiles.Add(ProfileMap.ToModel(a));
            }

            return model;
        }

        public ProfileModel GetProfile(int profileId, NameValueCollection expands)
        {
            var profile = _profileRepository.GetByProfileID(profileId);
            if (!Equals(profile, null))
            {
                GetExpands(expands, profile);
            }

            return ProfileMap.ToModel(profile);
        }

        public ProfileModel CreateProfile(ProfileModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Profile model cannot be null.");
            }

            var entity = ProfileMap.ToEntity(model);
            var profile = _profileRepository.Save(entity);
            return ProfileMap.ToModel(profile);
        }

        public ProfileModel UpdateProfile(int profileId, ProfileModel model)
        {
            if (profileId < 1)
            {
                throw new Exception("Profile ID cannot be less than 1.");
            }

            if (Equals(model, null))
            {
                throw new Exception("Profile model cannot be null.");
            }

            var profile = _profileRepository.GetByProfileID(profileId);
            if (!Equals(profile, null))
            {
                // Set profileId and update details.
                model.ProfileId = profileId;
                var profileToUpdate = ProfileMap.ToEntity(model);

                var updated = _profileRepository.Update(profileToUpdate);
                if (updated)
                {
                    profile = _profileRepository.GetByProfileID(profileId);
                    return ProfileMap.ToModel(profile);
                }
            }

            return null;
        }

        public bool DeleteProfile(int profileId)
        {
            if (profileId < 1)
            {
                throw new Exception("Profile ID cannot be less than 1.");
            }

            var profile = _profileRepository.GetByProfileID(profileId);
            if (!Equals(profile, null))
            {
                return _profileRepository.Delete(profile);
            }
            return false;
        }
        #endregion

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, ProfileQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.ProfileName) SetQueryParameter(ProfileColumn.Name, filterOperator, filterValue, query);                
            }
        }

        private void SetSorting(NameValueCollection sorts, ProfileSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.ProfileId) SetSortDirection(ProfileColumn.ProfileID, value, sortBuilder);
                    if (key == SortKeys.ProfileName) SetSortDirection(ProfileColumn.Name, value, sortBuilder);
                }
            }
        }

        private void GetExpands(NameValueCollection expands, Profile profile)
        {
            
        }
        #endregion       
    }
}
