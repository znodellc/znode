﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IProductCategoryService
	{
		ProductCategoryModel GetProductCategory(int productCategoryId, NameValueCollection expands);
		ProductCategoryListModel GetProductCategories(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
        ProductCategoryModel CreateProductCategory(ProductCategoryModel productCategoryModel);
		ProductCategoryModel UpdateProductCategory(int productCategoryId, ProductCategoryModel productCategoryModel);
		bool DeleteProductCategory(int productCategoryId);
	}
}
