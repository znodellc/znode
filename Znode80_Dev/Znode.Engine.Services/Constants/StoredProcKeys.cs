﻿
namespace Znode.Engine.Services.Constants
{
    public static class StoredProcKeys
    {
        public static string ProductTypeId = "producttypeid";
        public static string SpGetAttributeTypesBy = "GetAttributeTypesBy";
        public static string SpProductTypeWiseAttributes = "ProductTypeWiseAttributes";
        public static string AND = "and";
        public static string TildOperator = "~~";
        public static string TildEqualToOperator = "~~=";
    }
}
