﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IMessageConfigService
	{
		MessageConfigModel GetMessageConfig(int messageConfigId, NameValueCollection expands);
		MessageConfigListModel GetMessageConfigs(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		MessageConfigListModel GetMessageConfigsByKeys(string keys, NameValueCollection expands, NameValueCollection sorts);
	    MessageConfigModel GetMessageConfig(string key, int portalId, int language, int messageType);
		MessageConfigModel CreateMessageConfig(MessageConfigModel model);
		MessageConfigModel UpdateMessageConfig(int messageConfigId, MessageConfigModel model);
		bool DeleteMessageConfig(int messageConfigId);
	}
}
