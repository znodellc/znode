﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IShippingRuleTypeService
	{
		ShippingRuleTypeModel GetShippingRuleType(int shippingRuleTypeId);
		ShippingRuleTypeListModel GetShippingRuleTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		ShippingRuleTypeModel CreateShippingRuleType(ShippingRuleTypeModel model);
		ShippingRuleTypeModel UpdateShippingRuleType(int shippingRuleTypeId, ShippingRuleTypeModel model);
		bool DeleteShippingRuleType(int shippingRuleTypeId);
	}
}
