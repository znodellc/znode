﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Admin;
using ContentPageRepository = ZNode.Libraries.DataAccess.Service.ContentPageService;

namespace Znode.Engine.Services
{
    public class ContentPageService : BaseService, IContentPageService
    {
        #region Private Variables
        private readonly ContentPageRepository _contentPageRepository;
        #endregion

        #region Public Methods
        public ContentPageService()
        {
            _contentPageRepository = new ContentPageRepository();
        }

        public ContentPageListModel GetContentPages(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new ContentPageListModel();
            var contentPages = new TList<ContentPage>();
            var tempList = new TList<ContentPage>();

            var query = new ContentPageQuery();
            var sortBuilder = new ContentPageSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _contentPageRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var contentPage in tempList)
            {
                GetExpands(expands, contentPage);
                contentPages.Add(contentPage);
            }

            // Map each item and add to the list
            foreach (var a in contentPages)
            {
                model.ContentPages.Add(ContentPageMap.ToModel(a));
            }

            return model;
        }

        public ContentPageModel CreateContentPage(ContentPageModel model)
        {
            if (Equals(model, null))
            {
                throw new Exception("Facet group model cannot be null.");
            }

            var entity = ContentPageMap.ToEntity(model);
            var contentPageGroup = _contentPageRepository.Save(entity);
            return ContentPageMap.ToModel(contentPageGroup);
        }

        public ContentPageModel Clone(ContentPageModel contentPageModel)
        {
            ContentPage page = ContentPageMap.ToEntity(contentPageModel);
            ContentPageModel model = ContentPageMap.ToModel(page.Clone() as ContentPage);
            return model;
        }

        public bool AddPage(ContentPageModel contentPageModel)
        {
            ContentPage contentPage = ContentPageMap.ToEntity(contentPageModel);
            ContentPageAdmin contentPageAdmin = new ContentPageAdmin();
            return contentPageAdmin.AddPage(contentPage, contentPageModel.Html, contentPageModel.PortalID, contentPageModel.LocaleId.ToString(), contentPageModel.UpdatedUser, contentPageModel.MappedSeoUrl, contentPageModel.IsUrlRedirectEnabled);
        } 
        #endregion

        #region Private Methods
        private void SetFiltering(List<Tuple<string, string, string>> filters, ContentPageQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.PortalId) SetQueryParameter(ContentPageColumn.PortalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.LocaleId) SetQueryParameter(ContentPageColumn.LocaleId, filterOperator, filterValue, query);
            }
        }
        private void SetSorting(NameValueCollection sorts, ContentPageSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.PortalId) SetSortDirection(ContentPageColumn.PortalID, value, sortBuilder);
                }
            }
        }
        private void GetExpands(NameValueCollection expands, ContentPage contentPage)
        {
            if (expands.HasKeys())
            {

            }
        } 
        #endregion
    }
}
