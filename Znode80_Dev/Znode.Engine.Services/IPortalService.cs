﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Services
{
	public interface IPortalService
	{
		PortalModel GetPortal(int portalId, NameValueCollection expands);
		PortalListModel GetPortals(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page);
		PortalListModel GetPortalsByPortalIds(string portalIds, NameValueCollection expands, NameValueCollection sorts);
		PortalModel CreatePortal(PortalModel model);
		PortalModel UpdatePortal(int portalId, PortalModel model);
        #region Znode Version 8.0
        bool DeletePortal(int portalId);
        /// <summary>
        /// Gets the Fedex keys.
        /// </summary>
        /// <returns>dataset of fedex key.</returns>
        DataSet GetFedexKey();
        /// <summary>
        /// Creates Messages for the new store.
        /// </summary>
        /// <param name="portalId">Portal Id of the new store.</param>
        /// <param name="localeId">Locale id of the new store.</param>
        /// <returns>Bool value if the messages are created or not.</returns>
        bool CreateMessages(int portalId, int localeId);

        /// <summary>
        /// Copies the store.
        /// </summary>
        /// <param name="portalId">Portal id of the store to be copied.</param>
        /// <returns>Bool value if the store is copied or not.</returns>
        bool CopyStore(int portalId);

        /// <summary>
        /// Deletes a portal by Portal Id.
        /// </summary>
        /// <param name="portalId">Id of the portal to be deleted.</param>
        /// <returns>Bool value whether the portal is deleted or not.</returns>
        bool DeletePortalByPortalId(int portalId);
        #endregion
	}
}
