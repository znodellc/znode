﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using Znode.Engine.Suppliers;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.Google;
using ZNode.Libraries.Paypal;
using AccountRepository = ZNode.Libraries.DataAccess.Service.AccountService;
using CategoryRepository = ZNode.Libraries.DataAccess.Service.CategoryService;
using DigitalAssetRepository = ZNode.Libraries.DataAccess.Service.DigitalAssetService;
using OrderLineItemRepository = ZNode.Libraries.DataAccess.Service.OrderLineItemService;
using OrderRepository = ZNode.Libraries.DataAccess.Service.OrderService;
using OrderShipmentRepository = ZNode.Libraries.DataAccess.Service.OrderShipmentService;
using PaymentSettingRepository = ZNode.Libraries.DataAccess.Service.PaymentSettingService;
using PaymentTypeRepository = ZNode.Libraries.DataAccess.Service.PaymentTypeService;
using PortalRepository = ZNode.Libraries.DataAccess.Service.PortalService;
using PortalProfileRepository = ZNode.Libraries.DataAccess.Service.PortalProfileService;
using ProfileRepository = ZNode.Libraries.DataAccess.Service.ProfileService;
using ShippingRepository = ZNode.Libraries.DataAccess.Service.ShippingService;
using ZNodeShoppingCartItem = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem;
using AddressRepository = ZNode.Libraries.DataAccess.Service.AddressService;
namespace Znode.Engine.Services
{
    public class OrderService : BaseService, IOrderService
    {
        private readonly AccountRepository _accountRepository;
        private readonly CategoryRepository _categoryRepository;
        private readonly DigitalAssetRepository _digitalAssetRepository;
        private readonly OrderLineItemRepository _orderLineItemRepository;
        private readonly OrderRepository _orderRepository;
        private readonly OrderShipmentRepository _orderShipmentRepository;
        private readonly PaymentSettingRepository _paymentSettingRepository;
        private readonly PaymentTypeRepository _paymentTypeRepository;
        private readonly PortalRepository _portalRepository;
        private readonly PortalProfileRepository _portalProfileRepository;
        private readonly ProfileRepository _profileRepository;
        private readonly ShippingRepository _shippingRepository;
        private readonly AddressRepository _addressRepository;
        public OrderService()
        {
            _accountRepository = new AccountRepository();
            _categoryRepository = new CategoryRepository();
            _digitalAssetRepository = new DigitalAssetService();
            _orderLineItemRepository = new OrderLineItemRepository();
            _orderRepository = new OrderRepository();
            _orderShipmentRepository = new OrderShipmentRepository();
            _paymentSettingRepository = new PaymentSettingService();
            _paymentTypeRepository = new PaymentTypeRepository();
            _portalRepository = new PortalRepository();
            _portalProfileRepository = new PortalProfileService();
            _profileRepository = new ProfileRepository();
            _shippingRepository = new ShippingRepository();
            _addressRepository = new AddressRepository();
        }

        public OrderModel GetOrder(int orderId, NameValueCollection expands)
        {
            var order = _orderRepository.GetByOrderID(orderId);
            if (order != null)
            {
                GetExpands(expands, order);
                return OrderMap.ToModel(order);
            }

            return null;
        }

        public OrderListModel GetOrders(NameValueCollection expands, List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
        {
            var model = new OrderListModel();
            var orders = new TList<Order>();
            var tempList = new TList<Order>();

            var query = new OrderQuery();
            var sortBuilder = new OrderSortBuilder();
            var pagingStart = 0;
            var pagingLength = 0;

            SetFiltering(filters, query);
            SetSorting(sorts, sortBuilder);
            SetPaging(page, model, out pagingStart, out pagingLength);

            // Get the initial set
            var totalResults = 0;
            tempList = _orderRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
            model.TotalResults = totalResults;

            // Check to set page size equal to the total number of results
            if (pagingLength == int.MaxValue)
            {
                model.PageSize = model.TotalResults;
            }

            // Now go through the list and get any expands
            foreach (var order in tempList)
            {
                GetExpands(expands, order);
                orders.Add(order);
            }

            // Map each item and add to the list
            foreach (var o in orders)
            {
                model.Orders.Add(OrderMap.ToModel(o));
            }

            return model;
        }

        public OrderModel CreateOrder(int portalId, ShoppingCartModel model)
        {
            if (model == null)
            {
                throw new Exception("Shopping cart model cannot be null.");
            }

            var log = new ZNodeLogging();

            ZNodeUserAccount userAccount;

            var shippingAddress = (model.MultipleShipToEnabled) ? model.ShoppingCartItems.First().ShippingAddress : model.ShippingAddress;

            // Check to create user account from model or with the default for anonymous users
            if (model.Account != null)
            {
                userAccount = new ZNodeUserAccount(AccountMap.ToEntity(model.Account));
                userAccount.SetBillingAddress(AddressMap.ToEntity(model.Payment.BillingAddress));
                userAccount.SetShippingAddress(AddressMap.ToEntity(shippingAddress));
            }
            else
            {
                userAccount = new ZNodeUserAccount();
                userAccount.AddUserAccount();
                userAccount.SetBillingAddress(AddressMap.ToEntity(model.Payment.BillingAddress));
                userAccount.SetShippingAddress(AddressMap.ToEntity(shippingAddress));
                userAccount.EmailID = model.BillingEmail;
            }

            // Add the user account to session so that it's there when ZNodeCheckout is created
            userAccount.AddToSession(ZNodeSessionKeyType.UserAccount);

            var profileId = GetProfileIdForCheckout(portalId, userAccount);
            var isDefaultPortal = IsDefaultPortal(portalId);
            
            //Znode Version 7.2.2 Multi cart shipping - Start
            if (model.OrderShipment != null)
            {
                var multiShipModel = model.OrderShipment;
                //Multiship 
                model.ShoppingCartItems.ToList().ForEach(x =>
                {
                    x.MultipleShipToAddress.Clear();
                    int count = 1;
                    multiShipModel.ForEach(shippingModel =>
                    {
                        if (x.ProductId.Equals(int.Parse(shippingModel.productid)))
                        {
                            x.MultipleShipToAddress.Add(new OrderShipmentModel() { AddressId = int.Parse(shippingModel.addressid), Quantity = shippingModel.quantity, OrderShipmentId = count, ShippingOptionId = shippingModel.ShippingId, ShippingName = shippingModel.ShippingCode });
                            count++;

                        }
                    });


                });

                model.MultipleShipToEnabled = true;
            }
            //Multi cart shipping - End 

            // Create the shopping cart and add to session so that it's there when ZNodeCheckout is created
            var shoppingCart = ShoppingCartMap.ToZnodeShoppingCart(model);
            shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);

            // Create the checkout object
            var checkout = CheckoutMap.ToZnodeCheckout(userAccount, shoppingCart);

            // assign gift card values
            checkout.ShoppingCart.GiftCardAmount = model.GiftCardAmount;
            checkout.ShoppingCart.GiftCardMessage = model.GiftCardMessage;
            checkout.ShoppingCart.GiftCardNumber = model.GiftCardNumber;
            checkout.ShoppingCart.IsGiftCardApplied = model.GiftCardApplied;
            checkout.ShoppingCart.IsGiftCardValid = model.GiftCardValid;

            checkout.ShoppingCart.Coupon = model.Coupon;
            checkout.ShoppingCart.PortalID = portalId;
            checkout.ShoppingCart.VAT = model.Vat.GetValueOrDefault();
            checkout.ShoppingCart.HST = model.Hst.GetValueOrDefault();
            checkout.ShoppingCart.GST = model.Gst.GetValueOrDefault();
            checkout.ShoppingCart.PST = model.Pst.GetValueOrDefault();
            checkout.ShoppingCart.Payment = PaymentMap.ToZnodePayment(model.Payment, shippingAddress);
            checkout.AdditionalInstructions = model.AdditionalInstructions;
            checkout.PurchaseOrderNumber = model.PurchaseOrderNumber;
            checkout.PortalID = portalId;
            checkout.ShoppingCart.Shipping = ShippingMap.ToZnodeShipping(model.Shipping);
            checkout.ShippingID = checkout.ShoppingCart.Shipping.ShippingID;

            if (model.Total > 0)
            {
                checkout.PaymentSettingID = model.Payment.PaymentOption.PaymentOptionId;
                GetPaymentOptionForCheckout(checkout, model, profileId, isDefaultPortal, log);
            }
            else
            {
                checkout.PaymentSettingID = 0;
                checkout.ShoppingCart.Payment.PaymentName = String.Empty;
                checkout.ShoppingCart.Payment.PaymentSetting = null;
            }

            //Znode Version 7.2.2 Check order is multiple shipping - Start
            if (model.OrderShipment != null)
            {
                // Do the cart calculation			
                checkout.ShoppingCart.AddressCarts.ForEach(x =>
                    {
                        x.Shipping = string.IsNullOrEmpty(x.Shipping.ShippingName) ? new ZNodeShipping
                            {
                                ShippingID = checkout.ShoppingCart.Shipping.ShippingID,
                                ShippingName = checkout.ShoppingCart.Shipping.ShippingName
                            } : x.Shipping;
                        var address = _addressRepository.GetByAddressID(x.AddressID);
                        checkout.ShoppingCart.Payment = PaymentMap.ToZnodePayment(model.Payment, AddressMap.ToModel(address));
                        x.Payment = checkout.ShoppingCart.Payment;
                        x.Calculate();
                    });
            }
            else
            {
                // Do the cart calculation			
                checkout.ShoppingCart.AddressCarts.ForEach(x =>
                {
                    x.Shipping = string.IsNullOrEmpty(x.Shipping.ShippingName) ? new ZNodeShipping
                    {
                        ShippingID = checkout.ShoppingCart.Shipping.ShippingID,
                        ShippingName = checkout.ShoppingCart.Shipping.ShippingName
                    } : x.Shipping;
                    x.Payment = checkout.ShoppingCart.Payment;
                    x.Calculate();
                });
            }
            //Check order is multiple shipping - End

            checkout.ShoppingCart.Calculate();

            // Perform validation and start the timer
            ValidateCheckout(checkout);
            log.LogActivityTimerStart();

            // Instantiate the order fullfillment
            var order = new ZNodeOrderFulfillment();

            try
            {
                // Do pre-submit processing
                var preSubmitOrderSuccess = checkout.ShoppingCart.PreSubmitOrderProcess();
                if (preSubmitOrderSuccess)
                {
                    var paymentTypeId = model.Payment.PaymentOption.PaymentType.PaymentTypeId;

                    // Check to process PayPal Express
                    if (paymentTypeId == 3 && String.IsNullOrEmpty(model.Token))
                    {
                        CheckoutWithPayPalExpress(checkout, model, profileId, log);
                        return new OrderModel { ReceiptHtml = model.RedirectUrl, IsOffsitePayment = true };
                    }

                    // Check to process Google Checkout
                    if (paymentTypeId == 4)
                    {
                        CheckoutWithGoogleCheckout(checkout, model, profileId, log);
                    }

                    // Now submit order to add to our system, passing true flag indicating this is from the API
                    order = checkout.SubmitOrder(true);
                }
                else
                {
                    log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.OrderSubmissionFailed, null);
                    ZNodeLogging.LogMessage("Pre-submit order processing failed.");
                    throw new Exception("Pre-submit order processing failed.");
                }
            }
            catch (Exception ex)
            {
                log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.OrderSubmissionFailed, null);
                ZNodeLogging.LogMessage("General submit order error: " + ex.Message);
                throw new Exception("General submit order error: " + ex.Message);
            }

            // If checkout successful then do post-submit processing
            if (checkout.IsSuccess)
            {
                // TODO: Supposedly WorldPay logic went here, but we're not supporting it in API 2.0

                // Check to create tracking event if affiliate exists
                DoAffiliateTracking(order);

                // Do post submit processing
                PostSubmitOrder(order, checkout, model.FeedbackUrl);
                log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.OrderSubmissionSuccess, order.OrderID.ToString());
            }
            else
            {
                log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.OrderSubmissionFailed, null, null, null, null, checkout.PaymentResponseText);
                throw new Exception(checkout.PaymentResponseText);
            }

            // If we get to here we're good, so setup the expands to retrieve the newly created order
            var expands = new NameValueCollection
			{
				{ ExpandKeys.Account, ExpandKeys.Account },
				{ ExpandKeys.OrderLineItems, ExpandKeys.OrderLineItems },
				{ ExpandKeys.PaymentOption, ExpandKeys.PaymentOption },
				{ ExpandKeys.PaymentType, ExpandKeys.PaymentType },
				{ ExpandKeys.ShippingOption, ExpandKeys.ShippingOption }
			};

            // Now get the newly created order
            var newlyCreatedOrder = GetOrder(order.OrderID, expands);

            // And finally attach the receipt HTML to the order and return
            var receipt = new ZnodeReceipt(order, checkout.ShoppingCart) { FromApi = true, ApiShoppingCart = checkout.ShoppingCart, FeedbackUrl = model.FeedbackUrl };
            newlyCreatedOrder.ReceiptHtml = receipt.GetHtmlReceiptForUI();

            return newlyCreatedOrder;
        }

        public OrderModel UpdateOrder(int orderId, OrderModel model)
        {
            if (orderId < 1)
            {
                throw new Exception("Order ID cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Order model cannot be null.");
            }

            var order = _orderRepository.GetByOrderID(orderId);
            if (order != null)
            {
                // Set the order ID
                model.OrderId = orderId;

                var orderToUpdate = OrderMap.ToEntity(model);

                var updated = _orderRepository.Update(orderToUpdate);
                if (updated)
                {
                    // Update order line items
                    foreach (var item in model.OrderLineItems)
                    {
                        _orderLineItemRepository.Update(OrderLineItemMap.ToEntity(item));
                    }

                    order = _orderRepository.GetByOrderID(orderId);
                    return OrderMap.ToModel(order);
                }
            }

            return null;
        }

        private void ValidateCheckout(ZNodeCheckout checkout)
        {
            if (checkout.ShoppingCart == null)
            {
                throw new Exception("Shopping cart cannot be null.");
            }

            if (checkout.ShoppingCart.ShoppingCartItems.Count == 0)
            {
                throw new Exception("There are no items in the shopping cart.");
            }

            if (!ProfileHasPaymentOptions(checkout))
            {
                throw new Exception("Profile does not have any payment options.");
            }

            //if (!CheckoutHasValidAddress(checkout))
            //{
            //    throw new Exception("Address validation failed.");
            //}

            // Check for any error message at the portal cart level
            if (checkout.ShoppingCart.PortalCarts.Select(cart => cart.AddressCarts.Count(x => x.ErrorMessage != String.Empty)).Any(errorMessageCount => errorMessageCount > 0))
            {
                throw new Exception("Shopping cart contains errors.");
            }

            // Don't validate payment if cart total is 0
            if (checkout.ShoppingCart.Total == 0) return;

            if (checkout.ShoppingCart.Payment == null)
            {
                throw new Exception("Shopping cart payment cannot be null.");
            }

            if (checkout.ShoppingCart.Payment.PaymentSetting == null)
            {
                throw new Exception("Shopping cart payment option cannot be null.");
            }

            if (checkout.ShoppingCart.Payment.PaymentSetting.PaymentSettingID == 0)
            {
                throw new Exception("No payment option selected (payment option ID cannot be 0).");
            }
        }

        private bool ProfileHasPaymentOptions(ZNodeCheckout checkout)
        {
            var profileId = checkout.UserAccount != null ? ZNodeProfile.CurrentUserProfileId : ZNodeProfile.DefaultRegisteredProfileId;

            var paymentOptions = _paymentSettingRepository.GetAll();

            var hasPaymentOptions = checkout.ShoppingCart.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().Select(x => x.Product.PortalID).Distinct().All(x =>
            {
                var total = 0;
                var isDefaultPortal = !DataRepository.CategoryProvider.GetByPortalID(x, 0, 1, out total).Any();

                var franchisePortal = _portalRepository.GetByPortalID(x);
                profileId = franchisePortal.DefaultRegisteredProfileID.GetValueOrDefault(0);

                return paymentOptions.Any(p => ((p.ProfileID == null && isDefaultPortal) || p.ProfileID == profileId) && p.ActiveInd);
            });

            return hasPaymentOptions;
        }

        private bool CheckoutHasValidAddress(ZNodeCheckout checkout)
        {
            var isValidAddress = !checkout.ShoppingCart.IsMultipleShipToAddress ||
                                 checkout.UserAccount.CheckValidAddress(checkout.UserAccount.BillingAddress) &&
                                 checkout.ShoppingCart.PortalCarts.All(x => x.AddressCarts.All(
                                    y =>
                                    checkout.UserAccount.CheckValidAddress(checkout.UserAccount.Addresses.FirstOrDefault(z => z.AddressID == y.AddressID))
                                 ));

            return isValidAddress;
        }

        private int? GetProfileIdForCheckout(int portalId, ZNodeUserAccount userAccount)
        {
            int? profileId = null;

            if (portalId == ZNodeConfigManager.SiteConfig.PortalID)
            {
                profileId = userAccount.ProfileID;
            }
            else
            {
                var portalProfiles = _portalProfileRepository.GetByPortalID(portalId);
                var portalProfile = portalProfiles.Find(PortalProfileColumn.ProfileID, userAccount.ProfileID);

                if (portalProfile != null)
                {
                    // Use the profile ID on the user account
                    profileId = userAccount.ProfileID;
                }
                else
                {
                    // Use default registered profile ID associated with the franchise portal
                    var franchisePortal = _portalRepository.GetByPortalID(portalId);
                    if (franchisePortal != null)
                    {
                        profileId = franchisePortal.DefaultRegisteredProfileID;
                    }
                }
            }

            HttpContext.Current.Session["ProfileCache"] = _profileRepository.GetByProfileID(profileId.GetValueOrDefault(0));

            return profileId;
        }

        private bool IsDefaultPortal(int portalId)
        {
            // Check categories to determine if default portal
            int total;
            var categoryList = _categoryRepository.GetByPortalID(portalId, 0, 1, out total);
            return categoryList.Count == 0;
        }

        private void CheckoutWithPayPalExpress(ZNodeCheckout checkout, ShoppingCartModel model, int? profileId, ZNodeLogging log)
        {
            // Get payment option to give to PayPal
            var paymentOption = GetPaymentOption(profileId.GetValueOrDefault(), model.Payment.PaymentOption.PaymentType.PaymentTypeId);

            // Create the PayPal gateway
            var paypal = new PaypalGateway(paymentOption, model.ReturnUrl, model.CancelUrl, checkout.ShoppingCart)
            {
                OrderTotal = checkout.ShoppingCart.Total,
                PaymentActionTypeCode = "Sale"
            };

            // Call out to PayPal and check the response
            var response = paypal.DoPaypalExpressCheckout();
            if (response.ResponseCode != "0")
            {
                log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.SubmitPaymentFailed, response.ResponseText);
                ZNodeLogging.LogMessage("PayPal Express error: " + response.ResponseText);
                throw new Exception("PayPal Express error: " + response.ResponseText);
            }

            // If successful, set the redirect URL for clients to use
            model.RedirectUrl = response.HostUrl;
        }

        private void CheckoutWithGoogleCheckout(ZNodeCheckout checkout, ShoppingCartModel model, int? profileId, ZNodeLogging log)
        {
            // Get payment option to give to Google
            var paymentOption = GetPaymentOption(profileId.GetValueOrDefault(), model.Payment.PaymentOption.PaymentTypeId);

            // Create the Google gateway
            var google = new GoogleCheckout(model.ReturnUrl, model.CancelUrl)
            {
                PaymentSetting = paymentOption
            };

            // Call out to Google and check the response
            var response = google.SendRequestToGoogle();
            if (response.ResponseCode != "0")
            {
                log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.SubmitPaymentFailed, response.ResponseText);
                ZNodeLogging.LogMessage("Google Checkout error: " + response.ResponseText);
                throw new Exception("Google Checkout error: " + response.ResponseText);
            }

            // Get the response key
            var key = response.ResponseText;

            checkout.ShoppingCart.Payment.PaymentSetting = paymentOption;
            HttpRuntime.Cache.Add(key + "cart", checkout.ShoppingCart, null, DateTime.Now.AddHours(1), TimeSpan.FromMinutes(0), CacheItemPriority.Normal, null);

            // Check to create user account
            if (checkout.UserAccount == null)
            {
                checkout.UserAccount = new ZNodeUserAccount();
                checkout.UserAccount.AddUserAccount();
            }

            // Add user account to cache
            HttpRuntime.Cache.Add(key + "user", checkout.UserAccount, null, DateTime.Now.AddHours(1), TimeSpan.FromMinutes(0), CacheItemPriority.Normal, null);

            // Add portal ID to cache
            HttpRuntime.Cache.Add(key + "portalid", ZNodeConfigManager.SiteConfig.PortalID, null, DateTime.Now.AddHours(1), TimeSpan.FromMinutes(0), CacheItemPriority.Normal, null);

            // Pre-submit order
            checkout.ShoppingCart.PreSubmitOrderProcess();

            // If successful, set the redirect URL for clients to use
            model.RedirectUrl = response.ECRedirectURL;
        }

        private void PostSubmitOrder(ZNodeOrderFulfillment order, ZNodeCheckout checkout, string feedbackUrl)
        {
            checkout.ShoppingCart.Payment.TransactionID = order.CardTransactionID;
            checkout.ShoppingCart.Payment.AuthCode = order.CardAuthCode;
            checkout.ShoppingCart.Payment.SubscriptionID = order.Custom2;

            // Do post submit processing
            //Znode Version 7.2.2 
            //Code commented for update Inventory- Start
            //Removed this code block to update Inventory just after order submitted in database
            //checkout.ShoppingCart.PostSubmitOrderProcess();
            //Code commented for update Inventory- End

            // Invoke any supplier web services
            var supplierWebService = new ZnodeSupplierWebServiceManager(order, checkout.ShoppingCart);
            supplierWebService.InvokeWebService();

            // Send email receipts to the suppliers
            var supplierEmail = new ZnodeSupplierEmailManager(order, checkout.ShoppingCart) { FromApi = true, FeedbackUrl = feedbackUrl };
            supplierEmail.SendEmailReceipt();

            // Clear the saved cart info
            var savedCart = new ZNodeSavedCart();
            savedCart.RemoveSavedCart();

            var counter = 0;

            // Loop through the order line items
            foreach (var item in order.OrderLineItems)
            {
                var shoppingCartItem = checkout.ShoppingCart.AddressCarts.SelectMany(x => x.ShoppingCartItems.Cast<ZNodeShoppingCartItem>()).ElementAt(counter++);

                // Set product ID and quantity
                var productId = shoppingCartItem.Product.ProductID;
                var quantity = shoppingCartItem.Quantity;

                // Get digital assets and loop through them
                var digitalAssets = ZNodeDigitalAssetList.CreateByProductIdAndQuantity(productId, quantity).DigitalAssetCollection;
                foreach (var da in digitalAssets.Cast<ZNodeDigitalAsset>())
                {
                    // Get asset, set order line item ID, then update
                    var asset = _digitalAssetRepository.GetByDigitalAssetID(da.DigitalAssetID);
                    if (asset != null)
                    {
                        asset.OrderLineItemID = item.OrderLineItemID;
                        _digitalAssetRepository.Update(asset);
                    }
                }

                // TODO: Is this needed or is it for demo site only?
                // If product has digital assets, it will display it on the receipt page along with the product name
                //shoppingCartItem.Product.ZNodeDigitalAssetCollection = digitalAssets;

                // TODO: Is this needed or is it for demo site only?
                // Update display order
                //var displayOrder = new ZNodeDisplayOrder();
                //displayOrder.SetDisplayOrder(ZNodeProduct.Create(shoppingCartItem.Product.ProductID));
            }
        }

        private PaymentSetting GetPaymentOption(int profileId, int paymentTypeId)
        {
            var all = _paymentSettingRepository.GetAll();

            var list = all.FindAll(x => x.PaymentTypeID == paymentTypeId);
            list.Filter = "ActiveInd = true AND ProfileID = " + profileId;

            if (list.Count == 0)
            {
                // If nothing in the list with profile ID, try filtering without it
                list.Filter = "ActiveInd = true";
            }

            // Now sort and return the first one in the list
            list.Sort("ProfileID DESC");
            return list[0];
        }

        private void GetPaymentOptionForCheckout(ZNodeCheckout checkout, ShoppingCartModel model, int? profileId, bool isDefaultPortal, ZNodeLogging log)
        {
            // Get all payment options
            var allPaymentOptions = _paymentSettingRepository.GetAll();

            // Get active payment options for this profile ID
            var activeProfilePaymentOptions = allPaymentOptions.FindAll(pmt => pmt.ProfileID == profileId && pmt.ActiveInd);

            // Get all active payment options with no profile ID and default portal
            var allActivePaymentOptions = allPaymentOptions.FindAll(pmt => pmt.ProfileID == null && isDefaultPortal && pmt.ActiveInd);

            // Merge them together
            if (allActivePaymentOptions.Any())
            {
                activeProfilePaymentOptions.AddRange(allActivePaymentOptions.Where(x => activeProfilePaymentOptions.All(y => y.PaymentTypeID != x.PaymentTypeID)).ToList());
            }

            // Get active payment options with payment type ID
            var paymentTypeId = model.Payment.PaymentOption.PaymentType.PaymentTypeId;
            var activePaymentOptions = activeProfilePaymentOptions.FindAll(pmt => pmt.PaymentTypeID == paymentTypeId && pmt.ActiveInd);

            if (activePaymentOptions != null && activePaymentOptions.Count > 0)
            {
                checkout.ShoppingCart.Payment.PaymentSetting = activePaymentOptions[0];
                checkout.PaymentSettingID = activePaymentOptions[0].PaymentSettingID;
            }
            else
            {
                log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.OrderSubmissionFailed, null);
                ZNodeLogging.LogMessage("Multiple vendor checkout not supported, site not configured for credit card payments.");
                throw new Exception("Multiple vendor checkout not supported, site not configured for credit card payments.");
            }
        }

        private void DoAffiliateTracking(ZNodeOrderFulfillment order)
        {
            var tracking = new ZNodeTracking();
            if (tracking.AffiliateId.Length > 0)
            {
                tracking.AccountID = order.AccountID;
                tracking.OrderID = order.OrderID;
                tracking.LogTrackingEvent("Placed an order");
            }
        }

        private void GetExpands(NameValueCollection expands, Order order)
        {
            if (expands.HasKeys())
            {
                ExpandAccount(expands, order);
                ExpandOrderLineItems(expands, order);
                ExpandPaymentOption(expands, order);
                ExpandPaymentType(expands, order);
                ExpandShippingOption(expands, order);
            }
        }

        private void ExpandAccount(NameValueCollection expands, Order order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.Account)))
            {
                if (order.AccountID.HasValue)
                {
                    var account = _accountRepository.GetByAccountID(order.AccountID.Value);
                    if (account != null)
                    {
                        order.AccountIDSource = account;
                    }
                }
            }
        }

        private void ExpandOrderLineItems(NameValueCollection expands, Order order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.OrderLineItems)))
            {
                var orderLineItems = _orderLineItemRepository.DeepLoadByOrderID(order.OrderID, true, DeepLoadType.IncludeChildren, typeof(OrderLineItem));
                foreach (var item in orderLineItems)
                {
                    // Check to get the order shipment for the item
                    if (item.OrderShipmentID.HasValue)
                    {
                        var orderShipment = _orderShipmentRepository.GetByOrderShipmentID(item.OrderShipmentID.Value);
                        if (orderShipment != null)
                        {
                            item.OrderShipmentIDSource = orderShipment;
                        }
                    }
                }

                order.OrderLineItemCollection = orderLineItems;
            }
        }

        private void ExpandPaymentOption(NameValueCollection expands, Order order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.PaymentOption)))
            {
                if (order.PaymentSettingID.HasValue)
                {
                    var paymentOption = _paymentSettingRepository.GetByPaymentSettingID(order.PaymentSettingID.Value);
                    if (paymentOption != null)
                    {
                        order.PaymentSettingIDSource = paymentOption;
                    }
                }
            }
        }

        private void ExpandPaymentType(NameValueCollection expands, Order order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.PaymentType)))
            {
                if (order.PaymentTypeId.HasValue)
                {
                    var paymentType = _paymentTypeRepository.GetByPaymentTypeID(order.PaymentTypeId.Value);
                    if (paymentType != null)
                    {
                        order.PaymentType = paymentType;
                    }
                }
            }
        }

        private void ExpandShippingOption(NameValueCollection expands, Order order)
        {
            if (!String.IsNullOrEmpty(expands.Get(ExpandKeys.ShippingOption)))
            {
                if (order.ShippingID.HasValue)
                {
                    var shippingOption = _shippingRepository.GetByShippingID(order.ShippingID.Value);
                    if (shippingOption != null)
                    {
                        order.ShippingIDSource = shippingOption;
                    }
                }
            }
        }

        private void SetFiltering(List<Tuple<string, string, string>> filters, OrderQuery query)
        {
            foreach (var tuple in filters)
            {
                var filterKey = tuple.Item1;
                var filterOperator = tuple.Item2;
                var filterValue = tuple.Item3;

                if (filterKey == FilterKeys.AccountId) SetQueryParameter(OrderColumn.AccountID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.CompanyName) SetQueryParameter(OrderColumn.BillingCompanyName, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.CountryCode) SetQueryParameter(OrderColumn.BillingCountry, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Email) SetQueryParameter(OrderColumn.BillingEmailId, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.ExternalId) SetQueryParameter(OrderColumn.ExternalID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.LastName) SetQueryParameter(OrderColumn.BillingLastName, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.OrderDate) SetQueryParameter(OrderColumn.OrderDate, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.OrderStateId) SetQueryParameter(OrderColumn.OrderStateID, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PortalId) SetQueryParameter(OrderColumn.PortalId, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PostalCode) SetQueryParameter(OrderColumn.BillingPostalCode, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.PurchaseOrderNumber) SetQueryParameter(OrderColumn.PurchaseOrderNumber, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.StateCode) SetQueryParameter(OrderColumn.BillingStateCode, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.Total) SetQueryParameter(OrderColumn.Total, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.TrackingNumber) SetQueryParameter(OrderColumn.TrackingNumber, filterOperator, filterValue, query);
                if (filterKey == FilterKeys.TransactionId) SetQueryParameter(OrderColumn.CardTransactionID, filterOperator, filterValue, query);
            }
        }

        private void SetSorting(NameValueCollection sorts, OrderSortBuilder sortBuilder)
        {
            if (sorts.HasKeys())
            {
                foreach (var key in sorts.AllKeys)
                {
                    var value = sorts.Get(key);

                    if (key == SortKeys.City) SetSortDirection(OrderColumn.BillingCity, value, sortBuilder);
                    if (key == SortKeys.CompanyName) SetSortDirection(OrderColumn.BillingCompanyName, value, sortBuilder);
                    if (key == SortKeys.CountryCode) SetSortDirection(OrderColumn.BillingCountry, value, sortBuilder);
                    if (key == SortKeys.LastName) SetSortDirection(OrderColumn.BillingLastName, value, sortBuilder);
                    if (key == SortKeys.OrderDate) SetSortDirection(OrderColumn.OrderDate, value, sortBuilder);
                    if (key == SortKeys.OrderId) SetSortDirection(OrderColumn.OrderID, value, sortBuilder);
                    if (key == SortKeys.PostalCode) SetSortDirection(OrderColumn.BillingPostalCode, value, sortBuilder);
                    if (key == SortKeys.StateCode) SetSortDirection(OrderColumn.BillingStateCode, value, sortBuilder);
                    if (key == SortKeys.Total) SetSortDirection(OrderColumn.Total, value, sortBuilder);
                }
            }
        }

        private void SetQueryParameter(OrderColumn column, string filterOperator, string filterValue, OrderQuery query)
        {
            base.SetQueryParameter(column, filterOperator, filterValue, query);
        }

        private void SetSortDirection(OrderColumn column, string value, OrderSortBuilder sortBuilder)
        {
            base.SetSortDirection(column, value, sortBuilder);
        }



    }
}
