﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using Znode.Engine.Api.Models;
using Znode.Engine.Services.Constants;
using Znode.Engine.Services.Maps;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Data.Bases;
using ZNode.Libraries.DataAccess.Entities;
using TaxRuleTypeRepository = ZNode.Libraries.DataAccess.Service.TaxRuleTypeService;

namespace Znode.Engine.Services
{
	public class TaxRuleTypeService : BaseService, ITaxRuleTypeService
	{
		private readonly TaxRuleTypeRepository _taxRuleTypeRepository;

		public TaxRuleTypeService()
		{
			_taxRuleTypeRepository = new TaxRuleTypeRepository();
		}

		public TaxRuleTypeModel GetTaxRuleType(int taxRuleTypeId)
		{
			var taxRuleType = _taxRuleTypeRepository.GetByTaxRuleTypeID(taxRuleTypeId);
			return TaxRuleTypeMap.ToModel(taxRuleType);
		}

		public TaxRuleTypeListModel GetTaxRuleTypes(List<Tuple<string, string, string>> filters, NameValueCollection sorts, NameValueCollection page)
		{
			var model = new TaxRuleTypeListModel();
			var taxRuleTypes = new TList<TaxRuleType>();

			var query = new TaxRuleTypeQuery();
			var sortBuilder = new TaxRuleTypeSortBuilder();
			var pagingStart = 0;
			var pagingLength = 0;

			SetFiltering(filters, query);
			SetSorting(sorts, sortBuilder);
			SetPaging(page, model, out pagingStart, out pagingLength);

			// Get the initial set
			var totalResults = 0;
			taxRuleTypes = _taxRuleTypeRepository.Find(query, sortBuilder, pagingStart, pagingLength, out totalResults);
			model.TotalResults = totalResults;

			// Check to set page size equal to the total number of results
			if (pagingLength == int.MaxValue)
			{
				model.PageSize = model.TotalResults;
			}

			// Map each item and add to the list
			foreach (var t in taxRuleTypes)
			{
				model.TaxRuleTypes.Add(TaxRuleTypeMap.ToModel(t));
			}

			return model;
		}

		public TaxRuleTypeModel CreateTaxRuleType(TaxRuleTypeModel model)
		{
			if (model == null)
			{
				throw new Exception("Tax rule type model cannot be null.");
			}

			var entity = TaxRuleTypeMap.ToEntity(model);
			var taxRuleType = _taxRuleTypeRepository.Save(entity);
			return TaxRuleTypeMap.ToModel(taxRuleType);
		}

		public TaxRuleTypeModel UpdateTaxRuleType(int taxRuleTypeId, TaxRuleTypeModel model)
		{
			if (taxRuleTypeId < 1)
			{
				throw new Exception("Tax rule type ID cannot be less than 1.");
			}

			if (model == null)
			{
				throw new Exception("Tax rule type model cannot be null.");
			}

			var taxRuleType = _taxRuleTypeRepository.GetByTaxRuleTypeID(taxRuleTypeId);
			if (taxRuleType != null)
			{
				// Set the tax rule type ID and map
				model.TaxRuleTypeId = taxRuleTypeId;
				var taxRuleTypeToUpdate = TaxRuleTypeMap.ToEntity(model);

				var updated = _taxRuleTypeRepository.Update(taxRuleTypeToUpdate);
				if (updated)
				{
					taxRuleType = _taxRuleTypeRepository.GetByTaxRuleTypeID(taxRuleTypeId);
					return TaxRuleTypeMap.ToModel(taxRuleType);
				}
			}

			return null;
		}

		public bool DeleteTaxRuleType(int taxRuleTypeId)
		{
			if (taxRuleTypeId < 1)
			{
				throw new Exception("Tax rule type ID cannot be less than 1.");
			}

			var taxRuleType = _taxRuleTypeRepository.GetByTaxRuleTypeID(taxRuleTypeId);
			if (taxRuleType != null)
			{
				return _taxRuleTypeRepository.Delete(taxRuleType);
			}

			return false;
		}

		private void SetFiltering(List<Tuple<string, string, string>> filters, TaxRuleTypeQuery query)
		{
			foreach (var tuple in filters)
			{
				var filterKey = tuple.Item1;
				var filterOperator = tuple.Item2;
				var filterValue = tuple.Item3;

				if (filterKey == FilterKeys.ClassName) SetQueryParameter(TaxRuleTypeColumn.ClassName, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.IsActive) SetQueryParameter(TaxRuleTypeColumn.ActiveInd, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.Name) SetQueryParameter(TaxRuleTypeColumn.Name, filterOperator, filterValue, query);
				if (filterKey == FilterKeys.PortalId) SetQueryParameter(TaxRuleTypeColumn.PortalID, filterOperator, filterValue, query);
			}
		}

		private void SetSorting(NameValueCollection sorts, TaxRuleTypeSortBuilder sortBuilder)
		{
			if (sorts.HasKeys())
			{
				foreach (var key in sorts.AllKeys)
				{
					var value = sorts.Get(key);

					if (key == SortKeys.ClassName) SetSortDirection(TaxRuleTypeColumn.ClassName, value, sortBuilder);
					if (key == SortKeys.Name) SetSortDirection(TaxRuleTypeColumn.Name, value, sortBuilder);
					if (key == SortKeys.TaxRuleTypeId) SetSortDirection(TaxRuleTypeColumn.TaxRuleTypeID, value, sortBuilder);
				}
			}
		}

		private void SetQueryParameter(TaxRuleTypeColumn column, string filterOperator, string filterValue, TaxRuleTypeQuery query)
		{
			base.SetQueryParameter(column, filterOperator, filterValue, query);
		}

		private void SetSortDirection(TaxRuleTypeColumn column, string value, TaxRuleTypeSortBuilder sortBuilder)
		{
			base.SetSortDirection(column, value, sortBuilder);
		}
	}
}
