﻿namespace Znode.Engine.Tasks.Lucene.Configuration
{
	public class LuceneTaskConfiguration
	{
		public string LogFolder { get; set; }
		public string LuceneIndexLocation { get; set; }
		public int LuceneIndexInterval { get; set; }
		public string LuceneIndexReaderKey { get; set; }
		public string LuceneIndexReaderUrl { get; set; }
		public int OnStartWaitTime { get; set; }
		public string ServerName { get; set; }
		public string TransactionLogFile { get; set; }
	}
}
