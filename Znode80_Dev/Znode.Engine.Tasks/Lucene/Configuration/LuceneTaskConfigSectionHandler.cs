﻿using System;
using System.Configuration;
using System.Xml;

namespace Znode.Engine.Tasks.Lucene.Configuration
{
	public class LuceneTaskConfigSectionHandler : IConfigurationSectionHandler
	{
		public object Create(object parent, object configContext, XmlNode section)
		{
			var config = new LuceneTaskConfiguration();
			config.LogFolder = GetAttributeValue("LogFolder", section);
			config.LuceneIndexInterval = Convert.ToInt32(GetAttributeValue("LuceneIndexInterval", section)) * 1000;
			config.LuceneIndexLocation = GetAttributeValue("LuceneIndexLocation", section);
			config.LuceneIndexReaderKey = GetAttributeValue("LuceneIndexReaderKey", section);
			config.LuceneIndexReaderUrl = GetAttributeValue("LuceneIndexReaderUrl", section);
			config.OnStartWaitTime = Convert.ToInt32(GetAttributeValue("OnStartWaitTime", section)) * 1000;
			config.ServerName = GetAttributeValue("ServerName", section);
			config.TransactionLogFile = GetAttributeValue("TransactionLogFile", section);
            
			return config;
		}

		private string GetAttributeValue(string element, XmlNode section)
		{
			var node = section.SelectSingleNode(element);

			if (node == null)
			{
				throw new ConfigurationErrorsException("Lucene task configuration element '" + element + "' does not exist.");
			}

			if (node.Attributes == null || node.Attributes.Count < 0)
			{
				throw new ConfigurationErrorsException("Lucene task configuration element '" + element + "' must have a value attribute.");
			}

			if (String.IsNullOrEmpty(node.Attributes["value"].Value))
			{
				throw new ConfigurationErrorsException("The value attribute for Lucene task configuration element '" + element + "' cannot be empty.");
			}

			return node.Attributes["value"].Value;
		}
	}
}
