﻿namespace Znode.Engine.Tasks.Lucene
{
    public enum LuceneTaskProcessedStatus
    {
        InProcess = 1,
        Completed = 2,
        Failed = 3,
        Ignore = 4
    }
}