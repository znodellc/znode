﻿using Znode.Engine.Api.Models;

namespace PluginApi.Models
{
    public class PluginSettingModel : BaseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
