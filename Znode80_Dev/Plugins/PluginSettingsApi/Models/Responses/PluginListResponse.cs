﻿using System.Collections.ObjectModel;
using Znode.Engine.Api.Models.Responses;

namespace PluginApi.Models.Responses
{
    public class PluginListResponse : BaseListResponse
    {
        public Collection<PluginModel> Plugins { get; set; }
    }
}