﻿using Znode.Engine.Api.Models.Responses;

namespace PluginApi.Models.Responses
{
    public class PluginResponse : BaseResponse
    {
        public PluginModel Plugin { get; set; }
    }
}