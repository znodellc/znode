﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;

namespace PluginApi.Models
{
    public class PluginModel : BaseModel
    {
        public PluginModel()
        {
            PluginSettings = new List<PluginSettingModel>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public bool IsInstalled { get; set; }

        public List<PluginSettingModel> PluginSettings { get; set; }

        public PluginModel Sample()
        {
            return new PluginModel()
            {
                Id = 1,
                IsInstalled = true,
                Name = "SamplePlugin",
                Active = true,
                PluginSettings = new List<PluginSettingModel>()
                {
                    new PluginSettingModel() { Id = 1, Name = "timeout", Value = "30" }, 
                    new PluginSettingModel() { Id = 2, Name = "ReferenceId", Value = "12345" }
                }
            };
        }
    }
}