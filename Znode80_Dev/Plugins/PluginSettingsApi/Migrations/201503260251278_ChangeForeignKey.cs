namespace PluginSettingsApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeForeignKey : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.PluginSetting", "Plugin_Id", "dbo.Plugin");
            DropIndex("dbo.PluginSetting", new[] { "Plugin_Id" });
            RenameColumn(table: "dbo.PluginSetting", name: "Plugin_Id", newName: "PluginId");
            AlterColumn("dbo.PluginSetting", "PluginId", c => c.Int(nullable: false));
            CreateIndex("dbo.PluginSetting", "PluginId");
            AddForeignKey("dbo.PluginSetting", "PluginId", "dbo.Plugin", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PluginSetting", "PluginId", "dbo.Plugin");
            DropIndex("dbo.PluginSetting", new[] { "PluginId" });
            AlterColumn("dbo.PluginSetting", "PluginId", c => c.Int());
            RenameColumn(table: "dbo.PluginSetting", name: "PluginId", newName: "Plugin_Id");
            CreateIndex("dbo.PluginSetting", "Plugin_Id");
            AddForeignKey("dbo.PluginSetting", "Plugin_Id", "dbo.Plugin", "Id");
        }
    }
}
