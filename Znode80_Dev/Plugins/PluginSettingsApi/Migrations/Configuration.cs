using System.Data.Entity.Migrations;
using PluginSettingsApi.Data.Models;
using PluginSettingsApi.Data.Repositories;

namespace PluginSettingsApi.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<PluginConfiguratorDbContext>
    {
        public Configuration()
        {
            // these should always be set to false
            // let the Plugin.Install and Uninstall methods override these as necessary
            AutomaticMigrationsEnabled = false;
            AutomaticMigrationDataLossAllowed = false;

            // creates data migration specific to this class
            ContextKey = this.GetType().Name;
        }

        protected override void Seed(PluginConfiguratorDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            var plugin = new Plugin() { Name = "SeedTest", Active = false, Installed = false };
            plugin.PluginSettings.Add(new PluginSetting() { Name = "testName", Value = "testValue" });

            context.Plugins.AddOrUpdate(p => p.Name, plugin);
        }
    }
}
