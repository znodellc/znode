﻿using System;
using System.Linq;
using PluginSettingsApi.Data.Models;
using RefactorThis.GraphDiff;

namespace PluginSettingsApi.Data.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : IEntity
    {
        public EfRepository()
        {
        }

        public IQueryable<T> List()
        {
            throw new NotImplementedException();
        }

        public void Save(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
