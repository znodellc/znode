using System.Linq;
using PluginSettingsApi.Data.Models;

namespace PluginApi.Data.Repositories
{
    public interface IPluginRepository
    {
        IQueryable<Plugin> List();
        Plugin Save(Plugin plugin);
        bool Delete(int id);
        bool Delete(Plugin plugin);
    }
}