﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PluginApi.Models;
using PluginSettingsApi.Data.Models;

namespace PluginApi.Api.Services.Maps
{
    public static class PluginMap
    {
        public static PluginModel ToModel(Plugin entity)
        {
            if (entity == null)
            {
                return null;
            }

            var model = new PluginModel
            {
                Id = entity.Id,
                Active = entity.Active,
                IsInstalled = entity.Installed,
                Name = entity.Name,
                PluginSettings = entity.PluginSettings.Select(x => new PluginSettingModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Value = x.Value
                }).ToList()
            };

            return model;
        }

        public static Plugin ToEntity(PluginModel model)
        {
            if (model == null)
            {
                return null;
            }

            var entity = new Plugin()
            {
                Id = model.Id,
                Name = model.Name,
                Installed = model.IsInstalled,
                Active = model.Active
            };

            foreach (var setting in model.PluginSettings)
            {
                entity.PluginSettings.Add(new PluginSetting()
                {
                    Id = setting.Id,
                    Name = setting.Name,
                    Value = setting.Value,
                    PluginId = entity.Id
                });
            }

            return entity;
        }
    }
}
