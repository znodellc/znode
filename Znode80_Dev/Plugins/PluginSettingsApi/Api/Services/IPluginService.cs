﻿using System.Collections.Generic;
using PluginApi.Models;

namespace PluginSettingsApi.Api.Services
{
    public interface IPluginService
    {
        PluginModel GetPlugin(string pluginName);
        List<PluginModel> GetPlugins();
        PluginModel CreatePlugin(PluginModel model);
        PluginModel UpdatePlugin(int pluginId, PluginModel model);
        bool DeletePlugin(int pluginId);
    }
}