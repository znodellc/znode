﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using PluginApi.Api.Services.Maps;
using PluginApi.Data.Repositories;
using PluginApi.Models;
using PluginSettingsApi.Data.Models;
using PluginSettingsApi.Data.Repositories;

namespace PluginSettingsApi.Api.Services
{
    public class PluginService : IPluginService
    {
        //private readonly IRepository<PluginSetting> _pluginRepository;
        private readonly IPluginRepository _pluginRepository;

        public PluginService()
        {
            //_pluginRepository = new FakeRepository<PluginSetting>();
            //_pluginRepository = new EfRepository<PluginSetting>();
            _pluginRepository = new PluginRepository(new PluginConfiguratorDbContext());

            //_pluginRepository.Save(new PluginSetting()
            //{
            //    Plugin = new Plugin() { Name = "PluginConfigurator" },
            //    Name = "Timeout",
            //    Value = "30"
            //});
        }

        public PluginModel GetPlugin(string pluginName)
        {
            //TODO: make this query better by not filtering server-side
            var returnValue = _pluginRepository
                .List()
                .Where(x => x.Name.ToLower().Equals(pluginName.ToLower()))
                .Select(x => new PluginModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Active = x.Active,
                    IsInstalled = x.Installed,
                    PluginSettings = x.PluginSettings
                            .Select(ps => new PluginSettingModel() { Id = ps.Id, Name = ps.Name, Value = ps.Value })
                            .ToList()
                })
                .SingleOrDefault();

            return returnValue;
        }

        public List<PluginModel> GetPlugins()
        {
            //TODO: make this query better by not filtering server-side
            return _pluginRepository
                .List()
                .Select(x => new PluginModel()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Active = x.Active,
                    IsInstalled = x.Installed,
                    PluginSettings = x.PluginSettings
                        .Select(ps => new PluginSettingModel() { Id = ps.Id, Name = ps.Name, Value = ps.Value })
                        .ToList()
                })
                .ToList();
        }

        public PluginModel CreatePlugin(PluginModel model)
        {
            if (model == null)
            {
                throw new Exception("Plugin model cannot be null.");
            }

            var entity = PluginMap.ToEntity(model);
            var inserted = _pluginRepository.Save(entity);
            return PluginMap.ToModel(inserted);
        }

        public PluginModel UpdatePlugin(int pluginId, PluginModel model)
        {
            if (pluginId < 1)
            {
                throw new Exception("PluginId cannot be less than 1.");
            }

            if (model == null)
            {
                throw new Exception("Plugin model cannot be null.");
            }

            var plugin = _pluginRepository.List().SingleOrDefault(x => x.Id == pluginId);
            if (plugin != null)
            {
                var entity = PluginMap.ToEntity(model);
                var updated = _pluginRepository.Save(entity);
                return PluginMap.ToModel(updated);
            }

            return null;
        }

        public bool DeletePlugin(int pluginId)
        {
            if (pluginId < 1)
            {
                throw new Exception("Plugin Id cannot be less than 1.");
            }

            var plugin = _pluginRepository.List().SingleOrDefault(x => x.Id == pluginId);
            if (plugin != null)
            {
                return _pluginRepository.Delete(pluginId);
            }

            return false;
        }
    }
}
