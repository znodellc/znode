﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeTags' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Tags : TagsBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Tags"/> instance.
		///</summary>
		public Tags():base(){}	
		
		#endregion
	}
}
