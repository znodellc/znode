﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeReferralCommissionType' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ReferralCommissionType : ReferralCommissionTypeBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ReferralCommissionType"/> instance.
		///</summary>
		public ReferralCommissionType():base(){}	
		
		#endregion
	}
}
