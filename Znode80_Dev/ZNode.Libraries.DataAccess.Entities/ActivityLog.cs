﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeActivityLog' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class ActivityLog : ActivityLogBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="ActivityLog"/> instance.
		///</summary>
		public ActivityLog():base(){}	
		
		#endregion
	}
}
