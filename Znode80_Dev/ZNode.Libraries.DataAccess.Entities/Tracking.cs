﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeTracking' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class Tracking : TrackingBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="Tracking"/> instance.
		///</summary>
		public Tracking():base(){}	
		
		#endregion
	}
}
