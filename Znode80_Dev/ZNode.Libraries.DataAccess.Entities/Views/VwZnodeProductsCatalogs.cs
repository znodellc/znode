﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'vw_ZnodeProductsCatalogs' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class VwZnodeProductsCatalogs : VwZnodeProductsCatalogsBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="VwZnodeProductsCatalogs"/> instance.
		///</summary>
		public VwZnodeProductsCatalogs():base(){}	
		
		#endregion
	}
}
