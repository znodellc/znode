﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'vw_ZnodeProductsCategories' view. [No description found in the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class VwZnodeProductsCategories : VwZnodeProductsCategoriesBase
	{
		#region Constructors

		///<summary>
		/// Creates a new <see cref="VwZnodeProductsCategories"/> instance.
		///</summary>
		public VwZnodeProductsCategories():base(){}	
		
		#endregion
	}
}
