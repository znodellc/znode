﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeUrlRedirect' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class UrlRedirect : UrlRedirectBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="UrlRedirect"/> instance.
		///</summary>
		public UrlRedirect():base(){}	
		
		#endregion
	}
}
