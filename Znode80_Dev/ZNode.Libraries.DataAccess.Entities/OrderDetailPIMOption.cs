﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeOrderDetailPIMOption' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class OrderDetailPIMOption : OrderDetailPIMOptionBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="OrderDetailPIMOption"/> instance.
		///</summary>
		public OrderDetailPIMOption():base(){}	
		
		#endregion
	}
}
