﻿using System;
using System.ComponentModel;

namespace ZNode.Libraries.DataAccess.Entities
{
	/// <summary>
	///		The data structure representation of the 'ZNodeSupplier' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface ISupplier 
	{
		/// <summary>			
		/// SupplierID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "ZNodeSupplier"</remarks>
		System.Int32 SupplierID { get; set; }
				
		
		
		/// <summary>
		/// SupplierTypeID : 
		/// </summary>
		System.Int32?  SupplierTypeID  { get; set; }
		
		/// <summary>
		/// ExternalSupplierNo : 
		/// </summary>
		System.String  ExternalSupplierNo  { get; set; }
		
		/// <summary>
		/// Name : 
		/// </summary>
		System.String  Name  { get; set; }
		
		/// <summary>
		/// Description : 
		/// </summary>
		System.String  Description  { get; set; }
		
		/// <summary>
		/// ContactFirstName : 
		/// </summary>
		System.String  ContactFirstName  { get; set; }
		
		/// <summary>
		/// ContactLastName : 
		/// </summary>
		System.String  ContactLastName  { get; set; }
		
		/// <summary>
		/// ContactPhone : 
		/// </summary>
		System.String  ContactPhone  { get; set; }
		
		/// <summary>
		/// ContactEmail : 
		/// </summary>
		System.String  ContactEmail  { get; set; }
		
		/// <summary>
		/// NotificationEmailID : 
		/// </summary>
		System.String  NotificationEmailID  { get; set; }
		
		/// <summary>
		/// EmailNotificationTemplate : 
		/// </summary>
		System.String  EmailNotificationTemplate  { get; set; }
		
		/// <summary>
		/// EnableEmailNotification : 
		/// </summary>
		System.Boolean  EnableEmailNotification  { get; set; }
		
		/// <summary>
		/// DisplayOrder : 
		/// </summary>
		System.Int32?  DisplayOrder  { get; set; }
		
		/// <summary>
		/// ActiveInd : 
		/// </summary>
		System.Boolean  ActiveInd  { get; set; }
		
		/// <summary>
		/// Custom1 : 
		/// </summary>
		System.String  Custom1  { get; set; }
		
		/// <summary>
		/// Custom2 : 
		/// </summary>
		System.String  Custom2  { get; set; }
		
		/// <summary>
		/// Custom3 : 
		/// </summary>
		System.String  Custom3  { get; set; }
		
		/// <summary>
		/// Custom4 : 
		/// </summary>
		System.String  Custom4  { get; set; }
		
		/// <summary>
		/// Custom5 : 
		/// </summary>
		System.String  Custom5  { get; set; }
		
		/// <summary>
		/// PortalID : 
		/// </summary>
		System.Int32?  PortalID  { get; set; }
		
		/// <summary>
		/// ExternalID : 
		/// </summary>
		System.String  ExternalID  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _sKUSupplierID
		/// </summary>	
		TList<SKU> SKUCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _addOnValueSupplierID
		/// </summary>	
		TList<AddOnValue> AddOnValueCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _productSupplierID
		/// </summary>	
		TList<Product> ProductCollection {  get;  set;}	

		#endregion Data Properties

	}
}


