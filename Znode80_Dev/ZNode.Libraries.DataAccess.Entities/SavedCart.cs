﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeSavedCart' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SavedCart : SavedCartBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SavedCart"/> instance.
		///</summary>
		public SavedCart():base(){}	
		
		#endregion
	}
}
