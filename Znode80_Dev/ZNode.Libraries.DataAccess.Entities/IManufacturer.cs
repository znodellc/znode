﻿using System;
using System.ComponentModel;

namespace ZNode.Libraries.DataAccess.Entities
{
	/// <summary>
	///		The data structure representation of the 'ZNodeManufacturer' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IManufacturer 
	{
		/// <summary>			
		/// ManufacturerID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "ZNodeManufacturer"</remarks>
		System.Int32 ManufacturerID { get; set; }
				
		
		
		/// <summary>
		/// Name : 
		/// </summary>
		System.String  Name  { get; set; }
		
		/// <summary>
		/// Description : 
		/// </summary>
		System.String  Description  { get; set; }
		
		/// <summary>
		/// WebsiteLink : 
		/// </summary>
		System.String  WebsiteLink  { get; set; }
		
		/// <summary>
		/// EmailID : 
		/// </summary>
		System.String  EmailID  { get; set; }
		
		/// <summary>
		/// IsDropShipper : 
		/// </summary>
		System.Boolean?  IsDropShipper  { get; set; }
		
		/// <summary>
		/// EmailNotificationTemplate : 
		/// </summary>
		System.String  EmailNotificationTemplate  { get; set; }
		
		/// <summary>
		/// DisplayOrder : 
		/// </summary>
		System.Int32  DisplayOrder  { get; set; }
		
		/// <summary>
		/// ActiveInd : 
		/// </summary>
		System.Boolean  ActiveInd  { get; set; }
		
		/// <summary>
		/// Custom1 : 
		/// </summary>
		System.String  Custom1  { get; set; }
		
		/// <summary>
		/// Custom2 : 
		/// </summary>
		System.String  Custom2  { get; set; }
		
		/// <summary>
		/// Custom3 : 
		/// </summary>
		System.String  Custom3  { get; set; }
		
		/// <summary>
		/// PortalID : 
		/// </summary>
		System.Int32?  PortalID  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _productManufacturerID
		/// </summary>	
		TList<Product> ProductCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _promotionManufacturerID
		/// </summary>	
		TList<Promotion> PromotionCollection {  get;  set;}	

		#endregion Data Properties

	}
}


