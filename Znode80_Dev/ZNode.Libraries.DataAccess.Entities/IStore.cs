﻿using System;
using System.ComponentModel;

namespace ZNode.Libraries.DataAccess.Entities
{
	/// <summary>
	///		The data structure representation of the 'ZNodeStore' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IStore 
	{
		/// <summary>			
		/// StoreID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "ZNodeStore"</remarks>
		System.Int32 StoreID { get; set; }
				
		
		
		/// <summary>
		/// PortalID : 
		/// </summary>
		System.Int32  PortalID  { get; set; }
		
		/// <summary>
		/// Name : 
		/// </summary>
		System.String  Name  { get; set; }
		
		/// <summary>
		/// Address1 : 
		/// </summary>
		System.String  Address1  { get; set; }
		
		/// <summary>
		/// Address2 : 
		/// </summary>
		System.String  Address2  { get; set; }
		
		/// <summary>
		/// Address3 : 
		/// </summary>
		System.String  Address3  { get; set; }
		
		/// <summary>
		/// City : 
		/// </summary>
		System.String  City  { get; set; }
		
		/// <summary>
		/// State : 
		/// </summary>
		System.String  State  { get; set; }
		
		/// <summary>
		/// Zip : 
		/// </summary>
		System.String  Zip  { get; set; }
		
		/// <summary>
		/// Phone : 
		/// </summary>
		System.String  Phone  { get; set; }
		
		/// <summary>
		/// Fax : 
		/// </summary>
		System.String  Fax  { get; set; }
		
		/// <summary>
		/// ContactName : 
		/// </summary>
		System.String  ContactName  { get; set; }
		
		/// <summary>
		/// AccountID : 
		/// </summary>
		System.Int32?  AccountID  { get; set; }
		
		/// <summary>
		/// DisplayOrder : 
		/// </summary>
		System.Int32?  DisplayOrder  { get; set; }
		
		/// <summary>
		/// ImageFile : 
		/// </summary>
		System.String  ImageFile  { get; set; }
		
		/// <summary>
		/// Custom1 : 
		/// </summary>
		System.String  Custom1  { get; set; }
		
		/// <summary>
		/// Custom2 : 
		/// </summary>
		System.String  Custom2  { get; set; }
		
		/// <summary>
		/// Custom3 : 
		/// </summary>
		System.String  Custom3  { get; set; }
		
		/// <summary>
		/// ActiveInd : 
		/// </summary>
		System.Boolean  ActiveInd  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


