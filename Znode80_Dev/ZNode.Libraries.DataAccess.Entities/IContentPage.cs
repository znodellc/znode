﻿using System;
using System.ComponentModel;

namespace ZNode.Libraries.DataAccess.Entities
{
	/// <summary>
	///		The data structure representation of the 'ZNodeContentPage' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IContentPage 
	{
		/// <summary>			
		/// ContentPageID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "ZNodeContentPage"</remarks>
		System.Int32 ContentPageID { get; set; }
				
		
		
		/// <summary>
		/// Name : 
		/// </summary>
		System.String  Name  { get; set; }
		
		/// <summary>
		/// PortalID : 
		/// </summary>
		System.Int32  PortalID  { get; set; }
		
		/// <summary>
		/// Title : 
		/// </summary>
		System.String  Title  { get; set; }
		
		/// <summary>
		/// SEOTitle : 
		/// </summary>
		System.String  SEOTitle  { get; set; }
		
		/// <summary>
		/// SEOMetaKeywords : 
		/// </summary>
		System.String  SEOMetaKeywords  { get; set; }
		
		/// <summary>
		/// SEOMetaDescription : 
		/// </summary>
		System.String  SEOMetaDescription  { get; set; }
		
		/// <summary>
		/// AllowDelete : 
		/// </summary>
		System.Boolean  AllowDelete  { get; set; }
		
		/// <summary>
		/// TemplateName : 
		/// </summary>
		System.String  TemplateName  { get; set; }
		
		/// <summary>
		/// ActiveInd : 
		/// </summary>
		System.Boolean  ActiveInd  { get; set; }
		
		/// <summary>
		/// AnalyticsCode : 
		/// </summary>
		System.String  AnalyticsCode  { get; set; }
		
		/// <summary>
		/// Custom1 : 
		/// </summary>
		System.String  Custom1  { get; set; }
		
		/// <summary>
		/// Custom2 : 
		/// </summary>
		System.String  Custom2  { get; set; }
		
		/// <summary>
		/// Custom3 : 
		/// </summary>
		System.String  Custom3  { get; set; }
		
		/// <summary>
		/// SEOURL : 
		/// </summary>
		System.String  SEOURL  { get; set; }
		
		/// <summary>
		/// LocaleId : 
		/// </summary>
		System.Int32  LocaleId  { get; set; }
		
		/// <summary>
		/// MetaTagAdditional : 
		/// </summary>
		System.String  MetaTagAdditional  { get; set; }
		
		/// <summary>
		/// ThemeID : 
		/// </summary>
		System.Int32?  ThemeID  { get; set; }
		
		/// <summary>
		/// CSSID : 
		/// </summary>
		System.Int32?  CSSID  { get; set; }
		
		/// <summary>
		/// MasterPageID : 
		/// </summary>
		System.Int32?  MasterPageID  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _contentPageRevisionContentPageID
		/// </summary>	
		TList<ContentPageRevision> ContentPageRevisionCollection {  get;  set;}	

		#endregion Data Properties

	}
}


