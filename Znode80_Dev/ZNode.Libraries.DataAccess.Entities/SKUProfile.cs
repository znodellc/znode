﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeSKUProfile' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class SKUProfile : SKUProfileBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="SKUProfile"/> instance.
		///</summary>
		public SKUProfile():base(){}	
		
		#endregion
	}
}
