﻿#region Using directives

using System;

#endregion

namespace ZNode.Libraries.DataAccess.Entities
{	
	///<summary>
	/// An object representation of the 'ZNodeRMAConfiguration' table. [No description found the database]	
	///</summary>
	/// <remarks>
	/// This file is generated once and will never be overwritten.
	/// </remarks>	
	[Serializable]
	[CLSCompliant(true)]
	public partial class RMAConfiguration : RMAConfigurationBase
	{		
		#region Constructors

		///<summary>
		/// Creates a new <see cref="RMAConfiguration"/> instance.
		///</summary>
		public RMAConfiguration():base(){}	
		
		#endregion
	}
}
