﻿using System;
using System.ComponentModel;

namespace ZNode.Libraries.DataAccess.Entities
{
	/// <summary>
	///		The data structure representation of the 'ZNodeDomain' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IDomain 
	{
		/// <summary>			
		/// DomainID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "ZNodeDomain"</remarks>
		System.Int32 DomainID { get; set; }
				
		
		
		/// <summary>
		/// PortalID : 
		/// </summary>
		System.Int32  PortalID  { get; set; }
		
		/// <summary>
		/// DomainName : 
		/// </summary>
		System.String  DomainName  { get; set; }
		
		/// <summary>
		/// IsActive : 
		/// </summary>
		System.Boolean  IsActive  { get; set; }
		
		/// <summary>
		/// ApiKey : 
		/// </summary>
		System.String  ApiKey  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


