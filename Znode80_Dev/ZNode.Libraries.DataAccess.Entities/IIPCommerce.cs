﻿using System;
using System.ComponentModel;

namespace ZNode.Libraries.DataAccess.Entities
{
	/// <summary>
	///		The data structure representation of the 'ZNodeIPCommerce' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface IIPCommerce 
	{
		/// <summary>			
		/// IPCommerceID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "ZNodeIPCommerce"</remarks>
		System.Int32 IPCommerceID { get; set; }
				
		
		
		/// <summary>
		/// PortalID : 
		/// </summary>
		System.Int32?  PortalID  { get; set; }
		
		/// <summary>
		/// IPPF_Socket_Id : 
		/// </summary>
		System.String  IPPFSocketId  { get; set; }
		
		/// <summary>
		/// MerchantLogin : 
		/// </summary>
		System.String  MerchantLogin  { get; set; }
		
		/// <summary>
		/// MerchantPassword : 
		/// </summary>
		System.String  MerchantPassword  { get; set; }
		
		/// <summary>
		/// MerchantStoreId : 
		/// </summary>
		System.String  MerchantStoreId  { get; set; }
		
		/// <summary>
		/// MerchantSocketNum : 
		/// </summary>
		System.String  MerchantSocketNum  { get; set; }
		
		/// <summary>
		/// MerchantCountryCode : 
		/// </summary>
		System.String  MerchantCountryCode  { get; set; }
		
		/// <summary>
		/// MerchantName : 
		/// </summary>
		System.String  MerchantName  { get; set; }
		
		/// <summary>
		/// MerchantCustServicePhone : 
		/// </summary>
		System.String  MerchantCustServicePhone  { get; set; }
		
		/// <summary>
		/// MerchantAddress : 
		/// </summary>
		System.String  MerchantAddress  { get; set; }
		
		/// <summary>
		/// MerchantStreet1 : 
		/// </summary>
		System.String  MerchantStreet1  { get; set; }
		
		/// <summary>
		/// MerchantStreet2 : 
		/// </summary>
		System.String  MerchantStreet2  { get; set; }
		
		/// <summary>
		/// MerchantCity : 
		/// </summary>
		System.String  MerchantCity  { get; set; }
		
		/// <summary>
		/// MerchantStateProv : 
		/// </summary>
		System.String  MerchantStateProv  { get; set; }
		
		/// <summary>
		/// MerchantPostalCode : 
		/// </summary>
		System.String  MerchantPostalCode  { get; set; }
		
		/// <summary>
		/// Custom1 : 
		/// </summary>
		System.String  Custom1  { get; set; }
		
		/// <summary>
		/// Custom2 : 
		/// </summary>
		System.String  Custom2  { get; set; }
		
		/// <summary>
		/// Custom3 : 
		/// </summary>
		System.String  Custom3  { get; set; }
		
		/// <summary>
		/// Custom4 : 
		/// </summary>
		System.String  Custom4  { get; set; }
		
		/// <summary>
		/// Custom5 : 
		/// </summary>
		System.String  Custom5  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties

		#endregion Data Properties

	}
}


