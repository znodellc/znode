﻿using System;
using System.ComponentModel;

namespace ZNode.Libraries.DataAccess.Entities
{
	/// <summary>
	///		The data structure representation of the 'ZNodeLocale' table via interface.
	/// </summary>
	/// <remarks>
	/// 	This struct is generated by a tool and should never be modified.
	/// </remarks>
	public interface ILocale 
	{
		/// <summary>			
		/// LocaleID : 
		/// </summary>
		/// <remarks>Member of the primary key of the underlying table "ZNodeLocale"</remarks>
		System.Int32 LocaleID { get; set; }
				
		
		
		/// <summary>
		/// LocaleCode : 
		/// </summary>
		System.String  LocaleCode  { get; set; }
		
		/// <summary>
		/// LocaleDescription : 
		/// </summary>
		System.String  LocaleDescription  { get; set; }
			
		/// <summary>
		/// Creates a new object that is a copy of the current instance.
		/// </summary>
		/// <returns>A new object that is a copy of this instance.</returns>
		System.Object Clone();
		
		#region Data Properties


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _portalCatalogLocaleID
		/// </summary>	
		TList<PortalCatalog> PortalCatalogCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _portalLocaleID
		/// </summary>	
		TList<Portal> PortalCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _highlightLocaleId
		/// </summary>	
		TList<Highlight> HighlightCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _addOnLocaleId
		/// </summary>	
		TList<AddOn> AddOnCollection {  get;  set;}	


		/// <summary>
		///	Holds a collection of entity objects
		///	which are related to this object through the relation _attributeTypeLocaleId
		/// </summary>	
		TList<AttributeType> AttributeTypeCollection {  get;  set;}	

		#endregion Data Properties

	}
}


