﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNode.Libraries.DataAccess.Custom
{
	public enum RelationType
	{
		Addon = 1, Bundle = 2, Upgrade = 3, FBT = 4, YMAL = 5
	}
}
