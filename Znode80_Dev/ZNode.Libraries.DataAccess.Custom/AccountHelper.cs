using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// This Class manages Customer Account 
    /// </summary>
    public class AccountHelper
    {
        /// <summary>
        /// Returns a customer list for this portal.
        /// </summary>        
        /// <returns>Returns customer details dataset.</returns>
        public DataSet GetAllCustomers()
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GetAllCustomers", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Returns a payment balance list for this portal
        /// </summary>
        /// <param name="portalId">Portal Id to check.</param>
        /// <param name="referralAccountId">Referral account Id to get the commission amount details.</param>
        /// <returns>Returns the commission amount details dataset.</returns>
        public DataSet GetCommisionAmount(int portalId, string referralAccountId)
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetCommissionAmount", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object                
                adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);
                adapter.SelectCommand.Parameters.AddWithValue("@ReferralAccountId", referralAccountId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Returns a Payment Balance List for this portal
        /// </summary>
        /// <param name="portals">Comma seperated portal Ids</param>
        /// <returns>Returns the portal account dataset.</returns>
        public DataSet GetAccountByPortal(string portals)
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetAccountByPortal", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object                
                adapter.SelectCommand.Parameters.AddWithValue("@Portals", portals);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the customer profile Id.
        /// </summary>
        /// <param name="accountId">Customer account Id to get the profile Id.</param>
        /// <param name="portalId">Customer portal Id to get the profile Id.</param>
        /// <returns>Returns the customer profile Id.</returns>
        public int GetCustomerProfile(int accountId, int portalId)
        {
            int profileId = 0;

            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNode_GetCustomerProfile", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object                
                command.Parameters.AddWithValue("@AccountId", accountId);
                command.Parameters.AddWithValue("@PortalID", portalId);

                connection.Open();

                object scalarValue = command.ExecuteScalar();

                profileId = Convert.ToInt32(scalarValue);

                connection.Close();
            }

            // Return profile Id
            return profileId;
        }

        /// <summary>
        /// Delete data from Password Log based on the Account Id
        /// </summary>
        /// <param name="userId">User Id to delete the password log for the user.</param>        
        public void DeletePasswordLogByUserId(string userId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNode_DeletePasswordLogByUser", connection);
                connection.Open();

                command.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object            
                command.Parameters.AddWithValue("@UserId", userId);

                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        /// <summary>
        /// Update the LastPasswordChangedDate
        /// </summary>
        /// <param name="userId">User Id to get the user object and update the LastPasswordChangedDate field.</param>        
        public void UpdateLastPasswordChangedDate(string userId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNode_UpdateLastPasswordChangedDate", connection);
                connection.Open();

                command.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object                
                command.Parameters.AddWithValue("@UserId", userId);

                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        public string GetAccountIDByExternalID(string externalId)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var adapter = new SqlDataAdapter("ZNode_GetAccountByExternalID", connection) { SelectCommand = { CommandType = CommandType.StoredProcedure } };

                adapter.SelectCommand.Parameters.AddWithValue("@externalId", externalId);

                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                reader.Read();
                string accountId = null;

                if (reader.HasRows)
                {
                    accountId = reader[0].ToString();
                }

                connection.Close();

                return accountId;
            }
        }

        public List<string> GetAccountIDsByFilters(string query, string[,] array)
        {
            using (var connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                var accountIds = new List<string>();
                var adapter = new SqlDataAdapter(query, connection);
                for (var i = 0; i < array.GetLength(0); i++)
                {
                    if (array[i, 0].Equals("@p" + i))
                    {
                        adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], (DateTime?)Convert.ToDateTime(array[i, 1]));
                    }
                    else
                    {
                        if (array[i, 1] == null)
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], DBNull.Value);
                        }
                        else
                        {
                            adapter.SelectCommand.Parameters.AddWithValue(array[i, 0], array[i, 1]);
                        }
                    }
                }
                connection.Open();
                var reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);
                while (reader.Read())
                {
                    accountIds.Add(reader[0].ToString());
                }

                connection.Close();

                return accountIds;
            }
        }

        #region Znode Version 7.2.2

        /// <summary>
        /// Znode Version 7.2.2. 
        /// This function will Get the user failed password attempt count.
        /// </summary>
        /// <param name="userId">user Id to get the failed password Attempt count.</param>
        /// <returns>Returns the user failed password Attempt count.</returns>
        public int GetUserFailedPasswordAttemptCount(Guid userId)
        {
            int count = 0;
            // Create instance of connection and Command Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("aspnet_Membership_GetFailedPasswordAttemptCount", connection);

                // Mark the command as store procedure
                command.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object                
                command.Parameters.AddWithValue("@UserId", userId);

                connection.Open();

                object scalarValue = command.ExecuteScalar();

                count = Convert.ToInt32(scalarValue);

                connection.Close();
            }
            return count;
        }

        /// <summary>
        /// Znode Version 7.2.2. 
        /// This function will Insert Users Reset Password Details
        /// </summary>
        /// <param name="userId">user Id to of the user</param>
        /// <param name="uniqueCode">uniqueCode send over email as password token</param>
        /// <param name="resetPasswordExpDate">Link Expiration Date</param>
        /// <param name="newPassword">New generated user password</param>
        /// <returns>Returns true or false.</returns>
        public bool InsertResetPasswordSettingByUserId(string userId, string uniqueCode, DateTime resetPasswordExpDate, string newPassword)
        {
            int results = 0;
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZnodeResetPassword_Insert", connection);
                connection.Open();
                command.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object                
                command.Parameters.AddWithValue("@UserId", userId);
                command.Parameters.AddWithValue("@PasswordToken", uniqueCode);
                command.Parameters.AddWithValue("@PasswordTokenExpirationDate", resetPasswordExpDate);
                command.Parameters.AddWithValue("@Password", newPassword);

                results = command.ExecuteNonQuery();
                connection.Close();
            }
            return Convert.ToBoolean(results);
        }

        /// <summary>
        /// Znode Version 7.2.2. 
        /// This function Gets all Details of Reset Password based on User Id
        /// </summary>
        /// <param name="userId">user Id to of the user</param>
        /// <returns>Returns the DataSet object having the details</returns>
        public DataSet GetResetPasswordDetailsByUserId(string userId)
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetResetPasswordDetailsByUserId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object                
                adapter.SelectCommand.Parameters.AddWithValue("@UserId", userId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Znode Version 7.2.2. 
        /// This function delete all Details of Reset Password based on User Id
        /// </summary>
        /// <param name="userId">user Id to of the user</param>
        /// <returns>Returns void</returns>
        public void DeleteResetPasswordDetailsByUserId(string userId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNode_DeleteResetPasswordDetailsByUserId", connection);
                connection.Open();

                command.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object            
                command.Parameters.AddWithValue("@UserId", userId);

                command.ExecuteNonQuery();
                connection.Close();
            }
        }

        #endregion


        #region Znoce Version 8.0
        /// <summary>
        /// Znode Version 8.0 
        /// This function Gets Details of User based on User Id
        /// </summary>
        /// <param name="userId">user Id to of the user</param>
        /// <returns>Returns the DataSet object having the details</returns>
        public DataSet GetMembershipDetailsByUserId(Guid userId)
        {
            // Create instance of connection
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("Webpages_Membership_GetMembershipDetailsByUserId", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Add parameters to command object                
                adapter.SelectCommand.Parameters.AddWithValue("@UserId", userId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
        #endregion


        #region Public Methods related to web Service(WCF)
        /// <summary>
        /// Search accounts based on the specified fields
        /// </summary>
        /// <param name="downloadFilter">0 - All , 1 - New , 2 - Modified</param>
        /// <param name="beginDate">Account created from this date.</param>
        /// <param name="endDate">Account created to this date.</param>
        /// <param name="firstName">Customer first name to search.</param>
        /// <param name="lastName">Customer last name to search.</param>
        /// <param name="accountNumber">Account number to search.</param>
        /// <param name="companyName">Company name to search.</param>
        /// <param name="phoneNumber">Phone number to search.</param>
        /// <param name="zipCode">Zipcode to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <returns>Retuns the account details in XML format.</returns>
        public string SearchAccounts(int downloadFilter, DateTime? beginDate, DateTime? endDate, string firstName, string lastName, string accountNumber, string companyName, string zipCode, string phoneNumber, int? portalId)
        {
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create Instance of Adapter Object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_WS_GetAccountsByFilter_XML", connection);

                // Mark the Command as a Stored Procedures
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                adapter.SelectCommand.Parameters.AddWithValue("@Filter", downloadFilter);
                adapter.SelectCommand.Parameters.AddWithValue("@BeginDate", beginDate);
                adapter.SelectCommand.Parameters.AddWithValue("@EndDate", endDate);
                adapter.SelectCommand.Parameters.AddWithValue("@FirstName", firstName);
                adapter.SelectCommand.Parameters.AddWithValue("@LastName", lastName);
                adapter.SelectCommand.Parameters.AddWithValue("@AccountNumber", accountNumber);
                adapter.SelectCommand.Parameters.AddWithValue("@CompanyName", companyName);
                adapter.SelectCommand.Parameters.AddWithValue("@ZipCode", zipCode);
                adapter.SelectCommand.Parameters.AddWithValue("@Phone", phoneNumber);
                adapter.SelectCommand.Parameters.AddWithValue("@PortalID", portalId);

                connection.Open();
                SqlDataReader reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                string xmlOut = string.Empty;

                while (reader.Read())
                {
                    xmlOut = xmlOut + reader[0].ToString();
                }

                connection.Close();

                return xmlOut;
            }
        }

        /// <summary>
        /// Search accounts based on the specified filter
        /// </summary>
        /// <param name="downloadFilter">0 - All , 1 - New , 2 - Modified</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <returns>Retuns the account details in XML format.</returns>
        public string GetAccountsByFilter(int downloadFilter, int? portalId)
        {
            return this.SearchAccounts(downloadFilter, null, null, null, null, null, null, null, null, portalId);
        }
        #endregion
    }
}
