using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Represents the PortalCountryHelper class.
    /// </summary>
    public class PortalCountryHelper
    {
        /// <summary>
        /// Get the country by portal Id
        /// </summary>        
        /// <param name="portalId">Portal Id to get the associated country list.</param>
        /// <returns>Returns the portal associated country dataset.</returns>
        public DataSet GetCountriesByPortalID(int portalId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetCountriesByPortalID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@portalID", portalId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
    }
}
