using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Product CrossSell related Helper methods
    /// </summary>
    public class ProductCrossSellHelper
    {
        #region Public Methods
        /// <summary>
        /// Get related product items for a product
        /// </summary>
        /// <param name="productId">Product Id to get the product details.</param>
        /// <returns>Returns the product details dataset.</returns>
        public DataSet GetByProductID(int productId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_GetRelatedProductByProductID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;                
                adapter.SelectCommand.Parameters.AddWithValue("@Product_Id", productId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Add a new product crossSell item
        /// </summary>
        /// <param name="productId">Parent product Id.</param>
        /// <param name="relatedProductId">Relate product item to add.</param>
        /// <returns>Returns true if inserted otherwise false.</returns>
        public bool Insert(int productId, int relatedProductId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = null;

                bool isAdded = false;
                try
                {
                    // Create Instance of  Command Object
                    command = new SqlCommand("ZNODE_INSERTRELATEDPRODUCT", connection);

                    // Mark the Command as Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;

                    // add parameters
                    SqlParameter outputParam = command.Parameters.Add("@PRODUCTCROSSSELLID", SqlDbType.Int);
                    outputParam.Direction = ParameterDirection.Output;

                    command.Parameters.AddWithValue("@productID", productId);
                    command.Parameters.AddWithValue("@RelatedProductId", relatedProductId);
                   
                    // Execute the Query 
                    connection.Open();
                    command.ExecuteNonQuery();

                    connection.Close();

                    int productCrossSellId = (int)outputParam.Value;

                    if (productCrossSellId > 0)
                    {
                        isAdded = true;
                    }
                    else
                    {
                        isAdded = false;
                    }
                }
                catch 
                {
                }

                return isAdded;
            }
        }

        /// <summary>
        /// Delete a product cross sell item   
        /// </summary>
        /// <param name="relatedProductId">Related product Id.</param>
        /// <param name="productId">Parent product Id.</param>        
        /// <returns>Returns true if removed otherwise false.</returns>
        public bool RemoveProduct(int relatedProductId, int productId)
        {
            bool isRemoved = false;

            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = null;

                try
                {
                    // Create Instance of  Command Object
                    command = new SqlCommand("ZNODE_DELETERELATEDPRODUCT", connection);

                    // Mark the Command as Stored Procedure
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ProductID", productId);
                    command.Parameters.AddWithValue("@RelatedProductId", relatedProductId);

                    // Execute the Query 
                    connection.Open();
                    command.ExecuteNonQuery();
                    connection.Close();

                    isRemoved = true;
                }
                catch 
                {
                }

                // return Boolean
                return isRemoved;
            }
        }

        #endregion
    }
}
