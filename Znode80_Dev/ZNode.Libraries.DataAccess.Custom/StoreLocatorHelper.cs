using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// Store locator related Helper Functions
    /// </summary>
    public class StoreLocatorHelper
    {
        #region Public Methods

        /// <summary>
        /// Search storeList based on the search fields(Zipcode and radius)
        /// </summary>
        /// <param name="zipcode">Zip code to search</param>
        /// <param name="radius">Radion to search within.</param>
        /// <param name="city">City name to search</param>
        /// <param name="stateAbbr">State code to search.</param>
        /// <param name="areaCode">Area code to search.</param>
        /// <param name="portalId">Portal Id to search.</param>
        /// <returns>Returns the store locator search result in XML format.</returns>
        public string GetStoresByZipcode(string zipcode, int radius, string city, string stateAbbr, string areaCode, int portalId)
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlDataReader reader = null;
                System.Text.StringBuilder xmlOut = new System.Text.StringBuilder();

                try
                {
                    // Create Instance of Adapter Object
                    SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_SEARCH_STORELOCATOR", connection);

                    // Mark the Select command as Stored procedure
                    adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                    adapter.SelectCommand.Parameters.AddWithValue("@Zipcode", zipcode);
                    adapter.SelectCommand.Parameters.AddWithValue("@InRadius", radius);
                    adapter.SelectCommand.Parameters.AddWithValue("@City", city);
                    adapter.SelectCommand.Parameters.AddWithValue("@State", stateAbbr);
                    adapter.SelectCommand.Parameters.AddWithValue("@AreaCode", areaCode);
                    adapter.SelectCommand.Parameters.AddWithValue("@PortalId", portalId);

                    // Execute the command
                    connection.Open();

                    reader = adapter.SelectCommand.ExecuteReader(CommandBehavior.CloseConnection);

                    xmlOut.Append("<ZNodeStoreList>");

                    while (reader.Read())
                    {
                        xmlOut.Append(reader[0].ToString());
                    }

                    xmlOut.Append("</ZNodeStoreList>");
                }
                finally
                {
                    if (reader != null)
                    {
                        // Close the reader
                        reader.Close();
                    }

                    if (connection != null)
                    {
                        // Close the connection
                        connection.Close();
                    }
                }

                // return XML string
                return xmlOut.ToString();
            }
        }

        /// <summary>
        /// Returns Store as dataset for the given Store Name, Zip code, City and State
        /// </summary>
        /// <param name="storeName">Store name to search.</param>
        /// <param name="zipcode">Zipcode to search.</param>
        /// <param name="city">City name to search.</param>
        /// <param name="state">State code to search.</param>
        /// <returns>Returns the store search result dataset.</returns>
        public DataSet SearchStore(string storeName, string zipcode, string city, string state)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNODE_SEARCHSTORE", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Name", storeName);
                adapter.SelectCommand.Parameters.AddWithValue("@Zip", zipcode);
                adapter.SelectCommand.Parameters.AddWithValue("@City", city);
                adapter.SelectCommand.Parameters.AddWithValue("@State", state);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
        #endregion
    }
}
