﻿using System;
using System.Data;

namespace ZNode.Libraries.DataAccess.Custom
{
    /// <summary>
    /// This is helper class for StoredProcedure
    /// </summary>
    public class StoredProcedureHelper
    {
        #region Data Details Sp Config        
        public DataSet SP_DataDetailsSpConfig(string whereClause, string havingClause, string spName, string orderId, string pageIndex, string pageSize, out int totalRowCount, string strPrefix = null)
        {
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            totalRowCount = 0;
            if (strPrefix == null)
                strPrefix = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(spName))
                {
                    executeSpHelper.GetParameter("@SPName", spName, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@WhereClause", whereClause, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@HavingClause", havingClause, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@OrderBy", orderId, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@PageIndex", pageIndex, ParameterDirection.Input, SqlDbType.Int);
                    executeSpHelper.GetParameter("@PageSize", pageSize, ParameterDirection.Input, SqlDbType.Int);
                    executeSpHelper.GetParameter("@TotalRowCount", null, ParameterDirection.Output, SqlDbType.Int);
                    executeSpHelper.GetParameter("@PreFix", strPrefix, ParameterDirection.Input, SqlDbType.NVarChar);
                    var Result = executeSpHelper.GetSPResultInDataSet("ZNode_DataDetailsSpConfig");
                    totalRowCount = executeSpHelper.OutPutParamater(6); // pass out put paramater index value
                    return Result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { executeSpHelper = null; }
        }

        /// <summary>
        /// To call ZNode_DataDetailsSpConfig by sp name & where condition
        /// </summary>
        /// <param name="whereClause">string whereClause</param>
        /// <param name="spName">string spName</param>
        /// <returns>returns out data as per sp </returns>
        public DataSet SP_DataDetailsSpConfig(string whereClause, string spName)
        {
            ExecuteSpHelper executeSpHelper = new ExecuteSpHelper();
            try
            {
                if (!string.IsNullOrEmpty(spName))
                {
                    executeSpHelper.GetParameter("@SPName", spName, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@WhereClause", whereClause, ParameterDirection.Input, SqlDbType.NVarChar);
                    executeSpHelper.GetParameter("@TotalRowCount", 0, ParameterDirection.Output, SqlDbType.Int);
                    var Result = executeSpHelper.GetSPResultInDataSet("ZNode_DataDetailsSpConfig");

                    return Result;
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { executeSpHelper = null; }

        }
        #endregion
    }
}
