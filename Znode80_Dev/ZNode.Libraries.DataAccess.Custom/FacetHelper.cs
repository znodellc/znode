using System.Data;
using System.Data.SqlClient;

namespace ZNode.Libraries.DataAccess.Custom
{
    public class FacetHelper
    {
        /// <summary>
        /// Search the facet group.
        /// </summary>
        /// <param name="name">Facet group name to search.</param>
        /// <param name="catalogId">Catalog Id to search.</param>
        /// <returns>Returns the Facet group search result dataset.</returns>
        public DataSet GetFacetGroupBySearchData(string name, int catalogId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_SearchFacetGroup", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@Name", name);
                adapter.SelectCommand.Parameters.AddWithValue("@CatalogID", catalogId);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();

                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Get the categories data by facet group Id.
        /// </summary>
        /// <param name="facetGroupId">Facet group Id to get the categories.</param>
        /// <param name="catalogId">Catalog id to get the categories.</param>
        /// <param name="isFacetGroup">Indicates whether this is facet group or not.</param>
        /// <returns>Returns the categories dataset.</returns>
        public DataSet GetCategoriesByFacetGroupID(int facetGroupId, int catalogId, bool isFacetGroup)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetCategoryByFacetGroupID", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                adapter.SelectCommand.Parameters.AddWithValue("@FacetGroupId", facetGroupId);
                adapter.SelectCommand.Parameters.AddWithValue("@CatalogId", catalogId);
                adapter.SelectCommand.Parameters.AddWithValue("@IsFacetGroup", isFacetGroup);

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }

        /// <summary>
        /// Delete the categories by facet group Id.
        /// </summary>
        /// <param name="facetGroupId">Facet group Id to delete the categories.</param>
        public void DeleteCategoriesByFacetGroupID(int facetGroupId)
        {
            // Create instance of connection  Object   
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                SqlCommand command = new SqlCommand("ZNode_DeleteFacetGroupCategoryByFacetGroupID", connection);

                // Add Parameters to SPROC
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@FacetGroupId", facetGroupId);

                connection.Open();

                int rowsAffected = command.ExecuteNonQuery();

                connection.Close();
            }
        }

        /// <summary>
        /// Get the locale by associated facets.
        /// </summary>
        /// <returns>Returns the locale dataset</returns>
        public DataSet GetLocaleByAssociatedFacets()
        {
            // Create instance of connection Object
            using (SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString))
            {
                // Create instance of command object
                SqlDataAdapter adapter = new SqlDataAdapter("ZNode_GetLocaleByAssociatedFacets", connection);

                // Mark the command as store procedure
                adapter.SelectCommand.CommandType = CommandType.StoredProcedure;

                // Create and fill the dataset
                DataSet dataSet = new DataSet();
                connection.Open();
                adapter.Fill(dataSet);

                // Release the resources
                adapter.Dispose();
                connection.Close();

                // Return the datadet.
                return dataSet;
            }
        }
    }
}
