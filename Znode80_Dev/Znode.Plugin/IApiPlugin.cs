﻿using System.Web.Http;

namespace Znode.Plugin
{
    public interface IApiPlugin : IPlugin
    {
        void RegisterRoutes(HttpConfiguration config);
        void SetSampleResponses(HttpConfiguration config);
    }
}