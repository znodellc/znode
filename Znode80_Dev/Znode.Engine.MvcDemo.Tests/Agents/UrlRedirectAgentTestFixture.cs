﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Agents;

namespace Znode.Engine.MvcDemo.Tests.Agents
{
    [TestFixture]
    public class UrlRedirectAgentTestFixture : BaseFixture
    {
        [Test]
        public void Test01_GetActive301Redirects()
        {
            IUrlRedirectAgent agent =  new UrlRedirectAgent();
            UrlRedirectListModel model = agent.GetActive301Redirects();
            Assert.IsNotNull(model);
        }

        [Test]
        public void Test02_Has301Redirect()
        {
            string currentUrl = "~/product/pepper/315";
            UrlRedirectModel model = UrlRedirectAgent.Has301Redirect(currentUrl);
            Assert.IsNull(model);
        }
    }
}
