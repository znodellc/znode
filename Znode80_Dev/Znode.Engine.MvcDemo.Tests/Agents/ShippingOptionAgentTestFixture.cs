﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.ViewModels;
using NUnit.Framework;

namespace Znode.Engine.MvcDemo.Tests.Agents
{
    [TestFixture]
    public class ShippingOptionAgentTestFixture : BaseFixture
    {
        [Test]
        public void GetShippingListTest()
        {
            IShippingOptionAgent agent = new ShippingOptionAgent();
            
            var listViewModel = agent.GetShippingList();

            Assert.IsNotNull(listViewModel);

            Assert.IsTrue(listViewModel.ShippingOptions.Count > 0);
        }

        [Test]
        public void GetShippingListRetailTest()
        {
            IShippingOptionAgent agent = new ShippingOptionAgent();
            IAccountAgent accountAgent = new AccountAgent();

            var loginViewModel = new LoginViewModel();

            loginViewModel.Username = "nagaui";
            loginViewModel.Password = "password1";

            var loginModel = accountAgent.Login(loginViewModel);

            Assert.IsNotNull(loginModel);

            var listViewModel = agent.GetShippingList();

            Assert.IsNotNull(listViewModel);

            Assert.IsTrue(listViewModel.ShippingOptions.Count > 0);
            Assert.AreEqual("FLAT", listViewModel.ShippingOptions[0].ShippingCode);
            Assert.AreEqual("RETAILSHIP", listViewModel.ShippingOptions[1].ShippingCode);
        }

        [Test]
        public void GetShippingOptionTest()
        {
            IShippingOptionAgent agent = new ShippingOptionAgent();

            var viewModel = agent.GetShippingOption(9);

            Assert.IsNotNull(viewModel);

            Assert.AreEqual("FLAT", viewModel.ShippingCode);
        }
    }
}
