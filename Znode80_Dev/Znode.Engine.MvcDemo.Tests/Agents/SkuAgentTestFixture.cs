﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Agents;

namespace Znode.Engine.MvcDemo.Tests.Agents
{
    [TestFixture]
    public class SkuAgentTestFixture : BaseFixture
    {
        [Test]
        public void Test01_GetBySku()
        {
            ISkuAgent agent =  new SkuAgent();
            SkuModel model = agent.GetBySku(315, "st8979");
            Assert.IsNotNull(model);
            Assert.IsTrue(model.ProductId ==  315);
        }
            
        [Test]
        public void Test02_GetSkuInventory()
        {
            ISkuAgent agent =  new SkuAgent(); 
            SkuModel model = agent.GetSkuInventory(302, "2");
            Assert.IsNotNull(model);
            Assert.IsTrue(model.ProductId == 302);
        }
        
        [Test]
        public void Test03_GetSkuPrice()
        {
            string selectedSku = string.Empty;
            string imagePath = string.Empty;
            //ZNode Version 7.2.2 - Add imageMediumPath Out Parameter
            string imageMediumPath = string.Empty;
            ISkuAgent agent = new SkuAgent();
            decimal price = agent.GetSkusPrice(302, 2, 4.23M, out selectedSku, out imagePath, out imageMediumPath);
            Assert.IsNotNullOrEmpty(price.ToString());
        }

    }
}
