﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Tests.Agents
{
    [TestFixture]
    public class CheckoutAgentTestFixture : BaseFixture
    {
        [Test]
        public void Test01_SaveShippingAddress()
        {
            IAccountAgent accountAgent = new AccountAgent();
            ICheckoutAgent agent = new CheckoutAgent();
            
            Login();

            AddCartItem();

            var accountViewModel = accountAgent.GetAccountViewModel();

            var addressViewModel = accountAgent.GetAddress(accountViewModel.Addresses != null ? accountViewModel.Addresses[0].AddressId : 2);

            agent.SaveShippingAddess(addressViewModel);

            Assert.IsNotNull(addressViewModel);
        }

        [Test]
        public void Test02_SaveBillingAddress()
        {
            IAccountAgent accountAgent = new AccountAgent();
            ICheckoutAgent checkoutAgent = new CheckoutAgent();

            Login();

            var accountViewModel = accountAgent.GetAccountViewModel();

            var billingAddress = accountAgent.GetAddress(accountViewModel.Addresses != null ? accountViewModel.Addresses[0].AddressId : 2);
           
            var billingAddressmodel = checkoutAgent.SaveBillingAddess(billingAddress, true, true);
            Assert.IsNotNull(billingAddressmodel);
            Assert.IsTrue(billingAddressmodel.BillingAddress.AddressId > 0);
        }

        [Test]
        public void Test03_GetPaymentModel()
        {
            ICartAgent cartagent = new CartAgent();
            ICheckoutAgent checkoutAgent = new CheckoutAgent();

            Login();
            
            AddCartItem();

            var creditcardModel = new PaymentOptionViewModel()
                {
                    PaymentName = "Credit Card",
                    PaymentOptionId = 24,
                    PaymentTypeId = 1
                };
            var paymentModel = checkoutAgent.GetPaymentModel(creditcardModel);
            Assert.IsTrue(paymentModel.PaymentOptions.Count > 0);
            Assert.AreEqual("Credit Card", paymentModel.PaymentOptions[0].PaymentName);
        }

        [Test]
        public void Test04_GetAllPaymentModel()
        {
            ICheckoutAgent checkoutAgent = new CheckoutAgent();

            Login();

            AddCartItem();
            
            var paymentModel = checkoutAgent.GetPaymentModel();
            Assert.IsTrue(paymentModel.PaymentOptions.Count > 0);
            Assert.AreEqual("COD", paymentModel.PaymentOptions[1].PaymentName);
        }

        [Test]
        public void Test05_GetBillingAddress()
        {
            ICheckoutAgent checkoutAgent = new CheckoutAgent();

            Login();

            AddCartItem();

            var addressmodel = checkoutAgent.GetBillingAddress();
            Assert.IsNotNull(addressmodel);
            Assert.IsTrue(addressmodel.BillingAddress.AddressId > 0);
        }

        [Test]
        public void Test06_GetShippingAddress()
        {
            ICheckoutAgent checkoutAgent = new CheckoutAgent();

            Login();

            AddCartItem();

            var addressmodel = checkoutAgent.GetShippingAddess();
            Assert.IsNotNull(addressmodel);
            Assert.IsTrue(addressmodel.AddressId > 0);
        }
        
        [Test]
        public void Test07_GetBillingAddessFromSession()
        {
            ICheckoutAgent checkoutAgent = new CheckoutAgent();

            Login();

            AddCartItem();

            var addressmodel = checkoutAgent.GetBillingAddress();
            Assert.IsNotNull(addressmodel);
            Assert.IsTrue(addressmodel.BillingAddress.AddressId > 0);
        }

        [Test]
        public void Test08_ApplyGiftCard()
        {
            ICheckoutAgent checkoutAgent =  new CheckoutAgent();

            Login();

            AddCartItem();

            var cartViewModel = checkoutAgent.ApplyGiftCard("48NIUGQKYZ");
            Assert.IsNotNull(cartViewModel);
            Assert.IsTrue(cartViewModel.Discount > 0);
        }


        [Test]
        public void Test09_CheckoutReview()
        {
            ICartAgent cartAgent = new CartAgent();
            ICheckoutAgent checkoutAgent = new CheckoutAgent();

            Login();

            AddCartItem();
           
            var reviewOrderModel = checkoutAgent.GetCheckoutReview(9);
            Assert.IsNotNull(reviewOrderModel);
            Assert.IsTrue(reviewOrderModel.ShippingOption.ShippingId == 9);
        }

        [Test]
        public void Test10_GetAddressByAddressID()
        {
            IAccountAgent accountAgent = new AccountAgent();
            ICheckoutAgent checkoutAgent = new CheckoutAgent();

            Login();

            AddCartItem();

            var accountViewModel = accountAgent.GetAccountViewModel();

            var addressModel = accountAgent.GetAddress(accountViewModel.Addresses != null ? accountViewModel.Addresses[0].AddressId : 2);
            
            var addressViewModel = checkoutAgent.GetAddressByAddressID(addressModel);
            Assert.IsNotNull(addressViewModel);
            Assert.IsNotNull(addressViewModel.AddressId > 0);
        }

        [Test]
        public void Test11_SubmitOrder()
        {
            IAccountAgent accountAgent = new AccountAgent();
            ICheckoutAgent checkoutAgent = new CheckoutAgent();

            Login();

            AddCartItem();

            var accountViewModel = accountAgent.GetAccountViewModel();

            var addressModel = accountAgent.GetAddress(accountViewModel.Addresses != null ? accountViewModel.Addresses[0].AddressId : 2);
            

            var codModel = new PaymentOptionViewModel()
            {
                PaymentName = "COD",
                PaymentOptionId = 28,
                PaymentTypeId = 5
            };
            var orderModel = checkoutAgent.SubmitOrder(codModel, string.Empty);
            Assert.IsNotNull(orderModel);
            Assert.IsTrue(orderModel.OrderId > 0);
        }

        /// <summary>
        /// Private Method for Login
        /// </summary>
        private void Login()
        {
            IAccountAgent agent = new AccountAgent();

            var loginViewModel = new LoginViewModel
            {
                Username = "testuser1",
                Password = "password1"
            };

            agent.Login(loginViewModel);
        }

        /// <summary>
        /// Private method for Add Cart Item
        /// </summary>
        private void AddCartItem()
        {
            ICartAgent cartAgent = new CartAgent();

            CartItemViewModel model = new CartItemViewModel()
            {
                ExternalId = "cc99784e-80b1-4822-9d55-9376b89b18ff",
                ProductId = 387,
                ProductName = "Feta",
                Quantity = 1,
            };
            CartViewModel cartModel = cartAgent.Create(model);
        }
    }
}
