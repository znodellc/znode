﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Tests.Agents
{
    [TestFixture]
    public class CartAgentTestFixture : BaseFixture
    {
        [Test]
        public void AddToCartTest()
        {
            var cartModel = AddCartItem();
            Assert.IsNotNull(cartModel);
            Assert.IsTrue(cartModel.Items.Count == 1);
        }

        [Test]
        public void GetCartCountTest()
        {
            ICartAgent agent = new CartAgent();
            int cartCount = agent.GetCartCount();
            Assert.IsNotNull(cartCount);
            Assert.IsTrue(cartCount == 0);
        }

        [Test]
        public void CalculateTest()
        {
            ICartAgent agent = new CartAgent();
            var shoppingCartModel = new ShoppingCartModel()
                {
                   ShoppingCartItems = new Collection<ShoppingCartItemModel>()
                       {
                          new ShoppingCartItemModel()
                              {
                                  ExtendedPrice = 7.89M,
                                  ProductId =  387,
                                  Sku = "SFFK0989",
                                  SkuId = 59,
                                  ExternalId = "418cae19-73da-437d-946e-2eebc868bbfd",
                                  UnitPrice = 7.89M,
                              }
                       }
                };
            CartViewModel cartModel = agent.Calculate(shoppingCartModel);
            Assert.IsNotNull(cartModel);
            Assert.IsTrue(cartModel.Items.Count == 1);
        }

        [Test]
        public void RemoveItemTest()
        {
            ICartAgent agent = new CartAgent();

            var cartModel = AddCartItem();

            Assert.IsNotNull(cartModel);
            Assert.IsTrue(cartModel.Items.Count == 1);

            bool isRemoved = agent.RemoveItem(cartModel.Items[0].ExternalId);
            Assert.IsTrue(isRemoved);
        }

        [Test]
        public void ApplyCouponTest()
        {
            ICartAgent agent = new CartAgent();
            CartItemViewModel model = new CartItemViewModel()
            {
                ExternalId = "cc99784e-80b1-4822-9d55-9376b89b18ff",
                ProductId = 360,
                ProductName = "RoastedCashews",
                Quantity = 1,
            };
            CartViewModel cartModel = agent.Create(model);
            CartViewModel couponmodel = agent.ApplyCoupon("croasted");
            Assert.IsNotNull(couponmodel);
            Assert.IsNotNull(couponmodel.Discount > 0);
        }

        /// <summary>
        /// Private method for Add Cart Item
        /// </summary>
        private CartViewModel AddCartItem()
        {
            ICartAgent cartAgent = new CartAgent();

            CartItemViewModel model = new CartItemViewModel()
            {
                ExternalId = "cc99784e-80b1-4822-9d55-9376b89b18ff",
                ProductId = 387,
                ProductName = "Feta",
                Quantity = 1,
            };
            
            return cartAgent.Create(model);
        }
    }
}
