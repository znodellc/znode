﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.ViewModels;
using Znode.Engine.Api.Models;

using NUnit.Framework;

namespace Znode.Engine.MvcDemo.Tests.Agents
{
    [TestFixture]
    public class SearchAgentTestFixture : BaseFixture
    {
        [Test]
        public void GetSuggestionsTermTest()
        {
            ISearchAgent agent = new SearchAgent();

            var viewListModel = agent.GetSuggestions("Apple", string.Empty);

            Assert.IsNotNull(viewListModel);

            Assert.IsTrue(viewListModel.Count > 0);
        }

        [Test]
        public void GetSuggestionsTermCategoryTest()
        {
            ISearchAgent agent = new SearchAgent();

            var viewListModel = agent.GetSuggestions("Cashews", "Nuts");

            Assert.IsNotNull(viewListModel);

            Assert.IsTrue(viewListModel.Count > 1);
        }

        [Test]
        public void SearchSingleTermTest()
        {
            ISearchAgent agent = new SearchAgent();
            var model = new SearchRequestModel
                {
                    SearchTerm = "Apple",
                    Category = string.Empty,
                    PageNumber = 1,
                    PageSize = 5,
                    Sort = "relevance|asc",
                    RefineBy = new Collection<KeyValuePair<string, IEnumerable<string>>>()
                };

            var viewListModel = agent.Search(model);

            Assert.IsNotNull(viewListModel);

            Assert.IsTrue(viewListModel.Categories.Count == 1);
        }

        [Test]
        public void SearchCategoryTest()
        {
            ISearchAgent agent = new SearchAgent();
            var model = new SearchRequestModel
            {
                SearchTerm = string.Empty,
                Category = "Nuts",
                PageNumber = 1,
                PageSize = 5,
                Sort = "relevance|asc",
                RefineBy = new Collection<KeyValuePair<string, IEnumerable<string>>>()
            };

            var viewListModel = agent.Search(model);

            Assert.IsNotNull(viewListModel);
            Assert.IsNotNull(viewListModel.Products);

            Assert.IsTrue(viewListModel.Products.Count > 0);
        }

        [Test]
        public void SearchTextCategoryTest()
        {
            ISearchAgent agent = new SearchAgent();
            var model = new SearchRequestModel
            {
                SearchTerm = "app",
                Category = "Fruit",
                PageNumber = 1,
                PageSize = 5,
                Sort = "relevance|asc",
                RefineBy = new Collection<KeyValuePair<string, IEnumerable<string>>>()
            };

            var viewListModel = agent.Search(model);

            Assert.IsNotNull(viewListModel);
            Assert.IsNotNull(viewListModel.Products);

            Assert.AreEqual("Apple",viewListModel.Products[0].Name);
        }

        [Test]
        public void SearchSortAscTest()
        {
            ISearchAgent agent = new SearchAgent();
            var model = new SearchRequestModel
            {
                SearchTerm = string.Empty,
                Category = string.Empty,
                PageNumber = 1,
                PageSize = 5,
                Sort = "name|asc",
                RefineBy = new Collection<KeyValuePair<string, IEnumerable<string>>>()
            };

            var viewListModel = agent.Search(model);

            Assert.IsNotNull(viewListModel);
            Assert.IsNotNull(viewListModel.Products);

            Assert.AreEqual("Allium", viewListModel.Products[0].Name);
        }

        [Test]
        public void SearchSortDescTest()
        {
            ISearchAgent agent = new SearchAgent();
            var model = new SearchRequestModel
            {
                SearchTerm = string.Empty,
                Category = string.Empty,
                PageNumber = 1,
                PageSize = 5,
                Sort = "name|desc",
                RefineBy = new Collection<KeyValuePair<string, IEnumerable<string>>>()
            };

            var viewListModel = agent.Search(model);

            Assert.IsNotNull(viewListModel);
            Assert.IsNotNull(viewListModel.Products);

            Assert.AreEqual("Zucchini", viewListModel.Products[0].Name);
        }


        [Test]
        public void SearchPagingTest()
        {
            ISearchAgent agent = new SearchAgent();
            var model = new SearchRequestModel
            {
                SearchTerm = string.Empty,
                Category = string.Empty,
                PageNumber = 2,
                PageSize = 5,
                Sort = "relevance|asc",
                RefineBy = new Collection<KeyValuePair<string, IEnumerable<string>>>()
            };

            var viewListModel = agent.Search(model);

            Assert.IsNotNull(viewListModel);
            Assert.IsNotNull(viewListModel.Products);

            Assert.AreEqual("Mushroom", viewListModel.Products[0].Name);
        }

        [Test]
        public void SearchRefineByTest()
        {
            ISearchAgent agent = new SearchAgent();

            var refineBy = new Collection<KeyValuePair<string, IEnumerable<string>>>
                {
                    new KeyValuePair<string, IEnumerable<string>>("Color", new[] {"Pink"})
                };

            var model = new SearchRequestModel
            {
                SearchTerm = string.Empty,
                Category = "Flowers",
                PageNumber = 1,
                PageSize = 5,
                Sort = "relevance|asc",
                RefineBy = refineBy
            };

            var viewListModel = agent.Search(model);

            Assert.IsNotNull(viewListModel);
            Assert.IsNotNull(viewListModel.Products);

            Assert.IsTrue(viewListModel.Products.Count > 0);
            Assert.AreEqual("Allium", viewListModel.Products[0].Name);
            Assert.AreEqual("Columbine", viewListModel.Products[1].Name);
            Assert.AreEqual("Alstroemeria", viewListModel.Products[2].Name);
            Assert.AreEqual("Fresh-Cut Roses", viewListModel.Products[3].Name);
            Assert.AreEqual("Daylilies", viewListModel.Products[4].Name);
        }
    }
}
