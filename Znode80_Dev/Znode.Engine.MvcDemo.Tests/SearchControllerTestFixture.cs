﻿using System;
using System.Web.Mvc;
using NUnit.Framework;
using Znode.Engine.MvcDemo.Controllers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Tests
{
    [TestFixture]
   public class SearchControllerTestFixture 
    {

        [Test]
        public void SuggestionWithNoCategoryTest()
        {

            var controller = new SearchController();
            var result = controller.GetSuggestions("apple","");
            Assert.IsNotNull(result);
        }

        [Test]
        public void SuggestionWithCategoryTest()
        {

            var controller = new SearchController();
            var result = controller.GetSuggestions("apple", "fruit");
            Assert.IsNotNull(result);
        }

        [Test]
        public void SearchTextSingleResult()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = "apple";
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "1|asc";
            requestModel.InnerSearchKeywords = null;

            var controller = new SearchController();
            var result = controller.Index(requestModel);
           
            Assert.IsNotNull(result);
        }

        [Test]
        public void SearchTextMultipleResult()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = "app";
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "1";
            requestModel.InnerSearchKeywords = null;

            var controller = new SearchController();
            var result = controller.Index(requestModel);
            Assert.IsNotNull(result);
        }

        [Test]
        public void SearchTextCategory()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "1";
            requestModel.InnerSearchKeywords = null;

            var controller = new SearchController();
            var result = controller.Index(requestModel);
            Assert.IsNotNull(result);
        }

        [Test]
        public void SearchTextCategorySortingDesc()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "6";
            requestModel.InnerSearchKeywords = null;

            var controller = new SearchController();
            var result = controller.Index(requestModel);
            Assert.IsNotNull(result);
        }

        [Test]
        public void SearchTextCategorySortingDescPaging()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "6";
            requestModel.InnerSearchKeywords = null;

            var controller = new SearchController();
            var result = controller.Index(requestModel);
            Assert.IsNotNull(result);
        }

        [Test]
        public void SearchTextCategorySortingPriceAsc()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "4";
            requestModel.InnerSearchKeywords = null;

            var controller = new SearchController();
            var result = controller.Index(requestModel);
            Assert.IsNotNull(result);
        }

        [Test]
        public void InnerSearchCategoryTest()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = "Organic Produce";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "6";
            requestModel.InnerSearchKeywords = "Delicious||farm||";

            var controller = new SearchController();
            var result = controller.Index(requestModel);
            Assert.IsNotNull(result);
        }

        [Test]
        public void InnerSearchWithCategorySearchTerm()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = "apple";
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "6";
            requestModel.InnerSearchKeywords = "state||Fresh||";

            var controller = new SearchController();
            var result = controller.Index(requestModel);
            Assert.IsNotNull(result);
        }

        [Test]
        public void InnerSearchTermSorting()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "5";
            requestModel.InnerSearchKeywords = "delicious||";

            var controller = new SearchController();
            var result = controller.Index(requestModel);
            Assert.IsNotNull(result);
        }

        [Test]
        public void CategoryAllTest()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = string.Empty;
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "1";
            requestModel.InnerSearchKeywords = null;

            var controller = new SearchController();
            var result = controller.Index(requestModel);
            Assert.IsNotNull(result);
        }

        [Test]
        public void CategoryTest()
        {
           
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "1";
            requestModel.InnerSearchKeywords = null;

            var controller = new SearchController();
            var result = controller.Index(requestModel);
            Assert.IsNotNull(result);
        }

        [Test]
        public void CategorySearchTermTest()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = "apple";
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "1";
            requestModel.InnerSearchKeywords = null;

            var controller = new SearchController();
            var result = controller.Index(requestModel);
            Assert.IsNotNull(result);
        }
    }
}
