﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;
using MvcContrib.TestHelper;
using NUnit.Framework;
using Znode.Engine.MvcDemo.Controllers;
using Znode.Engine.MvcDemo.ViewModels;
using MvcContrib.TestHelper;
namespace Znode.Engine.MvcDemo.Tests
{
    [TestFixture]
    public class CategoryControllerTestFixture : BaseFixture
    {
       

        [Test]
        public void CategoryFacetTest()
        {
            var controller = new CategoryController();
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "1";
            var result = controller.Index(requestModel, string.Empty, "");
            
            
            
            
        }

        [Test]
        public void CategoryFeaturedProductTest()
        {
            var controller = new CategoryController();
            var result = controller.GetCategory(81);
            Assert.IsNotNull(result);
        }

        [Test]
        public void CategoryInnerSearchTermTest()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "1";
            requestModel.InnerSearchKeywords = "state||Fresh||";

            var controller = new CategoryController();
            var result = controller.Index(requestModel, string.Empty, "");
            Assert.IsNotNull(result);
        }

        [Test]
        public void CategoryPagingTest()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 2;
            requestModel.PageSize = 5;
            requestModel.Sort = "6";
            requestModel.InnerSearchKeywords = null;

            var controller = new CategoryController();
            var result = controller.Index(requestModel, string.Empty, "");
            Assert.IsNotNull(result);
        }

        [Test]
        public void CategoryProductTest()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "1";
            requestModel.InnerSearchKeywords = null;

            var controller = new CategoryController();
            var result = controller.Index(requestModel, string.Empty, "");
            Assert.IsNotNull(result);
        }

        [Test]
        public void CategorySortingDescTest()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "6";
            requestModel.InnerSearchKeywords = null;

            var controller = new CategoryController();
            var result = controller.Index(requestModel, string.Empty, "");
            Assert.IsNotNull(result);
        }

        [Test]
        public void CategorySortingPriceAsc()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "4";
            requestModel.InnerSearchKeywords = null;

            var controller = new CategoryController();
            var result = controller.Index(requestModel, string.Empty, "");
            Assert.IsNotNull(result);
        }

        [Test]
        public void CategorySortingTest()
        {
            var requestModel = new SearchRequestModel();
            requestModel.SearchTerm = string.Empty;
            requestModel.Category = "Fruit";
            requestModel.PageNumber = 1;
            requestModel.PageSize = 5;
            requestModel.Sort = "6";
            requestModel.InnerSearchKeywords = null;

            var controller = new CategoryController();
            var result = controller.Index(requestModel, string.Empty, "");
            Assert.IsNotNull(result);
        }
    }
}
