﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using NUnit.Framework;
using Znode.Engine.MvcDemo.Controllers;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Tests
{
    [TestFixture]
    public class AccountControllerTestFixture : BaseFixture
    {
       

       
        
        [Test]
        public void TestSession()
        {
            var controller = new AccountController();
            var model = new LoginViewModel();
            model.Username = "testuser01";
            model.Password = "password1";
            var result = controller.Login(model, string.Empty);
            var account = HttpContext.Current.Session[MvcDemoConstants.AccountKey];
            Assert.IsNull(HttpContext.Current.Session[MvcDemoConstants.AccountKey]);
        }
        [Test]
        public void LoginTest()
        {
            var controller = new AccountController();
            var model = new LoginViewModel();
            model.Username = "user1";
            model.Password = "password1";
            var result = controller.Login(model, string.Empty);
            Assert.IsNotNull(result);
        }

        [Test]
        public void AddWishListAfterLoginTest()
        {
            var controller = new AccountController();
            var productcontroller = new ProductController();
            var model = new LoginViewModel();
            model.Username = "user1";
            model.Password = "password1";
            var result = controller.Login(model, string.Empty);
            var wishlistresult = productcontroller.WishList(405);
            Assert.IsNotNull(wishlistresult);
        }
    }
}
