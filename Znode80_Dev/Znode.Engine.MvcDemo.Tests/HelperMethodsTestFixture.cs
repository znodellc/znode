﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using NUnit.Framework;
using Znode.Engine.MvcDemo.Controllers;
using Znode.Engine.MvcDemo.ViewModels;
namespace Znode.Engine.MvcDemo.Tests
{
    [TestFixture]
    public class HelperMethodsTestFixture
    {

        [Test]
        public void CategoryFacetTest()
        {
            string strRegex = @"(?<=src="").*?(?="")";
            Regex srcRegex = new Regex(strRegex);
            string strTargetString = @"<div class=\""user\"">as321</div>\r\n<div class=\""user\""><br></div>\r\n<div class=\""user\""><img height=\""650\"" src=""C:\\DescriptionGraphic0001v1.JPG\"" width=\""907\"" border=\""0\""></div>";

            string srcElement = srcRegex.Match(strTargetString).Value; // SRC

            string imgRegex = @"<img.*?>";

        string str =    Regex.Replace(strTargetString, imgRegex, "REPLACEMENT STRING");
        }

        [Test]
        public void ANotherTest()
        {
            string absoluteUrl = "http://api.multifront.localhost";
            string strTargetString = @"<div class=\""user\"">as321</div>\r\n<div class=\""user\""><br></div>\r\n<div class=\""user\""><img height=\""650\"" src=""C:\\DescriptionGraphic0001v1.JPG\"" width=\""907\"" border=\""0\""></div>";
            String value = Regex.Replace(strTargetString, "<(.*?)(src|href)=\"(?!http)(.*?)\"(.*?)>", "<$1$2=\"" + absoluteUrl + "$3\"$4>",
                                RegexOptions.IgnoreCase | RegexOptions.Multiline);

            // Now just make sure that there isn't a // because if
            // the original relative path started with a / then the
            // replacement above would create a //.

            string check = value.Replace(absoluteUrl + "/", absoluteUrl);
        }
    }
}
