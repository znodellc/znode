﻿using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
	public class ZnodeProductPromotionProductDetails : ZnodeProductPromotionType
    {
		public ZnodeProductPromotionProductDetails()
		{
			Name = "Product Details";
			Description = "Overrides displayed product details.";
			AvailableForFranchise = false;

			Controls.Add(ZnodePromotionControl.Store);
			Controls.Add(ZnodePromotionControl.Profile);
		}

		/// <summary>
		/// Allows you to change product details when displaying the product.
		/// </summary>
		/// <param name="product">The product on which you can change its details.</param>
        public override void ChangeDetails(ZNodeProductBaseEntity product)
        {
			// Check if promotion is already applied
			if (!product.IsPromotionApplied && product.ProductID == PromotionBag.RequiredProductId)
            {
				// Add whatever logic you want here to override the product details for this promotion. For example, if you
				// want to override the product name and description for the product in a wholesale profile, do the following:
				//
				// product.Name = product.Name + " (Wholesale)";
				// product.Description = product.Description + " (Wholesale)";

				// Ensure it's ignored if applied again
                product.IsPromotionApplied = true;
            }
        }    
    }
}
