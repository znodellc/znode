using System;
using System.Linq;
using System.Reflection;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Promotions
{
	/// <summary>
	/// Helps manage product promotions.
	/// </summary>
	public class ZnodeProductPromotionManager : ZnodePromotionManager
	{
		private ZNodeGenericCollection<IZnodeProductPromotionType> _productPromotions;

		public ZnodeProductPromotionManager()
		{
			_productPromotions = new ZNodeGenericCollection<IZnodeProductPromotionType>();

			var currentPortalId = ZNodeConfigManager.SiteConfig.PortalID;

			Profile profile = null;
			int? currentProfileId = null;

			if (HttpContext.Current.Session["ProfileCache"] != null)
			{
				profile = HttpContext.Current.Session["ProfileCache"] as Profile;
			}

			if (profile != null)
			{
				currentProfileId = profile.ProfileID;
			}

			var productPromotionsFromCache = (TList<Promotion>)ProductPromotionCache.Clone();
			ApplyPromotionsFilter(productPromotionsFromCache, currentPortalId, currentProfileId);

			// Sort the promotions, build the list, then kill the cache clone
			productPromotionsFromCache.Sort("DisplayOrder");
			BuildPromotionsList(productPromotionsFromCache, currentPortalId, currentProfileId);
			productPromotionsFromCache.Dispose();
		}

		/// <summary>
		/// Changes details for a product.
		/// </summary>
		/// <param name="product">The product whose details will be changed.</param>
		public void ChangeDetails(ZNodeProductBaseEntity product)
		{
			_productPromotions.Sort("Precedence");

			foreach (ZnodeProductPromotionType promo in _productPromotions)
			{
				promo.ChangeDetails(product);
			}
		}

		private void ApplyPromotionsFilter(TList<Promotion> promotionsFromCache, int? currentPortalId, int? currentProfileId)
		{
			promotionsFromCache.ApplyFilter(
				promo => (promo.ProfileID == currentProfileId || promo.ProfileID == null) &&
					     (promo.PortalID == currentPortalId || promo.PortalID == null) &&
						 promo.DiscountTypeIDSource.ClassType.Equals("PRODUCT", StringComparison.OrdinalIgnoreCase));
		}

		private void BuildPromotionsList(TList<Promotion> promotionsFromCache, int? currentPortalId, int? currentProfileId)
		{
			foreach (var promotion in promotionsFromCache)
			{
				var promoBag = BuildPromotionBag(promotion, currentPortalId, currentProfileId);
				AddPromotionType(promotion, promoBag);
			}
		}

		private ZnodePromotionBag BuildPromotionBag(Promotion promotion, int? currentPortalId, int? currentProfileId)
		{
			var promoBag = new ZnodePromotionBag();
			promoBag.PortalId = currentPortalId;
			promoBag.ProfileId = currentProfileId;
			promoBag.Discount = promotion.Discount;
			promoBag.MinimumOrderAmount = promotion.OrderMinimum.GetValueOrDefault(0);
			promoBag.RequiredProductId = promotion.ProductID.GetValueOrDefault(0);
			promoBag.RequiredProductMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
			promoBag.DiscountedProductId = promotion.PromotionProductID.GetValueOrDefault(0);
			promoBag.DiscountedProductQuantity = promotion.PromotionProductQty.GetValueOrDefault(1);
			promoBag.RequiredBrandId = promotion.ManufacturerID;
			promoBag.RequiredBrandMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
			promoBag.RequiredCatalogId = promotion.CatalogID;
			promoBag.RequiredCatalogMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
			promoBag.RequiredCategoryId = promotion.CategoryID;
			promoBag.RequiredCategoryMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);

			if (promotion.CouponInd)
			{
				promoBag.Coupon = GetCoupon(promotion);
			}

			if (!String.IsNullOrEmpty(promotion.PromotionMessage) && !promotion.CouponInd)
			{
				if (promoBag.Coupon == null)
				{
					promoBag.Coupon = new ZnodeCoupon();
				}

				promoBag.Coupon.PromotionMessage = promotion.PromotionMessage;
			}

			return promoBag;
		}

		private void AddPromotionType(Promotion promotion, ZnodePromotionBag promotionBag)
		{
			var availablePromoTypes = GetAvailablePromotionTypes();
			foreach (var t in availablePromoTypes.Where(t => t.ClassName == promotion.DiscountTypeIDSource.ClassName))
			{
				try
				{
					var promoTypeAssembly = Assembly.GetAssembly(t.GetType());
					var productPromo = (IZnodeProductPromotionType)promoTypeAssembly.CreateInstance(t.ToString());

					if (productPromo != null)
					{
						productPromo.Precedence = promotion.DisplayOrder;
						productPromo.Bind(promotionBag);
						_productPromotions.Add(productPromo);
					}
				}
				catch (Exception ex)
				{
					ZNodeLogging.LogMessage("Error while instantiating promotion type: " + t);
					ZNodeLogging.LogMessage(ex.ToString());
				}
			}
		}
	}
}
