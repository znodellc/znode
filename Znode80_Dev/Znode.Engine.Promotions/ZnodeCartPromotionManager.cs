using System;
using System.Linq;
using System.Reflection;
using System.Web;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Promotions
{
	/// <summary>
	/// Helps manage cart promotions.
	/// </summary>
	public class ZnodeCartPromotionManager : ZnodePromotionManager
	{
		private ZNodeShoppingCart _shoppingCart;
		private ZNodeGenericCollection<IZnodeCartPromotionType> _cartPromotions;

		/// <summary>
		/// Throws a NotImplementedException because this class requires a shopping cart to work.
		/// </summary>
		public ZnodeCartPromotionManager()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Instantiates all the promotions that are required for the current shopping cart.
		/// </summary>
		/// <param name="shoppingCart">The current shopping cart.</param>
		/// <param name="profileId">The current profile ID, if any.</param>
		public ZnodeCartPromotionManager(ZNodeShoppingCart shoppingCart, int? profileId)
		{
			_shoppingCart = shoppingCart;
			_cartPromotions = new ZNodeGenericCollection<IZnodeCartPromotionType>();

			var currentPortalId = ZNodeConfigManager.SiteConfig.PortalID;

			Profile profile = null;
			int? currentProfileId = null;

			if (!profileId.HasValue)
			{
				if (HttpContext.Current.Session["ProfileCache"] != null)
				{
					profile = HttpContext.Current.Session["ProfileCache"] as Profile;
				}

				if (profile != null)
				{
					currentProfileId = profile.ProfileID;
				}
			}
			else
			{
				currentProfileId = profileId;
			}

			// Check for default portal
			int total;
			var categoryService = new CategoryService();
			var categoryList = categoryService.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID, 0, 1, out total);
			var isDefaultPortal = categoryList.Count == 0;

			var cartPromotionsFromCache = (TList<Promotion>)CartPromotionCache.Clone();
			ApplyPromotionsFilter(cartPromotionsFromCache, currentPortalId, currentProfileId, isDefaultPortal);

			// Sort the promotions, build the list, then kill the cache clone
			cartPromotionsFromCache.Sort("DisplayOrder");
			BuildPromotionsList(cartPromotionsFromCache, currentPortalId, currentProfileId);
			cartPromotionsFromCache.Dispose();
		}

		/// <summary>
		/// Calculates the promotion and updates the shopping cart.
		/// </summary>
		public void Calculate()
		{
			// No sense in continuing without a shopping cart
			if (_shoppingCart == null)
				return;

			_shoppingCart.OrderLevelDiscount = 0;
			_shoppingCart.Shipping.ShippingDiscount = 0;
			_shoppingCart.CouponApplied = false;
			_shoppingCart.ClearPreviousErrorMessges();
			GetAmountsForCartItemsAndAddOns();
			CalculateEachPromotion();
			CheckForInvalidCoupon();
		}

		/// <summary>
		/// Process anything that must be done before the order is submitted.
		/// </summary>
		/// <returns>True if everything is good for submitting the order; otherwise, false.</returns>
		public bool PreSubmitOrderProcess()
		{
			var allPreConditionsOk = true;

			foreach (ZnodeCartPromotionType promo in _cartPromotions)
			{
				// Make sure all pre-conditions are good before letting the customer check out
				allPreConditionsOk &= promo.PreSubmitOrderProcess();
			}

			return allPreConditionsOk;
		}

		/// <summary>
		/// Process anything that must be done after the order is submitted.
		/// </summary>
		public void PostSubmitOrderProcess()
		{
			foreach (ZnodeCartPromotionType promo in _cartPromotions)
			{
				promo.PostSubmitOrderProcess();
			}

			var promotionService = new PromotionService();

			if (_shoppingCart.CouponApplied)
			{
				var query = new PromotionQuery();
				query.Append(PromotionColumn.CouponCode, _shoppingCart.Coupon);

				// Find promotions with the coupon code
				var promotions = promotionService.Find(query.GetParameters());

				foreach (var promo in promotions)
				{
					if (promo.CouponQuantityAvailable.GetValueOrDefault(0) > 0)
					{
						// Reduce the quantity of available coupons
						promo.CouponQuantityAvailable -= 1;
						promotionService.Update(promo);
					}
				}
			}
		}

		private void GetAmountsForCartItemsAndAddOns()
		{
			var pricePromoManager = new ZnodePricePromotionManager();

			foreach (ZNodeShoppingCartItem cartItem in _shoppingCart.ShoppingCartItems)
			{
				cartItem.PromotionalPrice = pricePromoManager.PromotionalPrice(cartItem.Product, cartItem.TieredPricing);
				cartItem.ExtendedPriceDiscount = 0;
				cartItem.Product.DiscountAmount = 0;

				foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
				{
					foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
					{
						addOnValue.DiscountAmount = 0;
					}
				}
			}
		}

		private void CalculateEachPromotion()
		{
			_cartPromotions.Sort("Precedence");

			foreach (ZnodeCartPromotionType promo in _cartPromotions)
			{
				promo.Calculate();
			}

			if (_shoppingCart.CouponApplied)
			{
				_shoppingCart.ClearPreviousErrorMessges();
				_shoppingCart.AddPromoDescription = _shoppingCart.CouponMessage;
			}
		}

		private void CheckForInvalidCoupon()
		{
			if (!_shoppingCart.CouponApplied && !_shoppingCart.CouponValid && !String.IsNullOrEmpty(_shoppingCart.Coupon))
			{
				_shoppingCart.AddPromoDescription = "Invalid coupon code " + _shoppingCart.Coupon;
			}
		}

		private void ApplyPromotionsFilter(TList<Promotion> promotionsFromCache, int? currentPortalId, int? currentProfileId, bool isDefaultPortal)
		{
			if (String.IsNullOrEmpty(_shoppingCart.Coupon))
			{
				promotionsFromCache.ApplyFilter(
					promo => (promo.ProfileID == currentProfileId || promo.ProfileID == null && isDefaultPortal) &&
							 (promo.PortalID == currentPortalId || promo.PortalID == null) &&
							 (promo.CouponInd == false) &&
							 promo.DiscountTypeIDSource.ClassType.Equals("CART", StringComparison.OrdinalIgnoreCase));
			}
			else
			{
				promotionsFromCache.ApplyFilter(
					promo => (promo.ProfileID == currentProfileId || promo.ProfileID == null && isDefaultPortal) &&
							 (promo.PortalID == currentPortalId || promo.PortalID == null) &&
							 (promo.CouponInd == false || promo.CouponCode == _shoppingCart.Coupon) &&
							 promo.DiscountTypeIDSource.ClassType.Equals("CART", StringComparison.OrdinalIgnoreCase));
			}
		}

		private void BuildPromotionsList(TList<Promotion> promotionsFromCache, int? currentPortalId, int? currentProfileId)
		{
			foreach (var promotion in promotionsFromCache)
			{
				var promoBag = BuildPromotionBag(promotion, currentPortalId, currentProfileId);
				AddPromotionType(promotion, promoBag);
			}
		}

		private ZnodePromotionBag BuildPromotionBag(Promotion promotion, int? currentPortalId, int? currentProfileId)
		{
			var promoBag = new ZnodePromotionBag();
			promoBag.PortalId = currentPortalId;
			promoBag.ProfileId = currentProfileId;
			promoBag.Discount = promotion.Discount;
			promoBag.MinimumOrderAmount = promotion.OrderMinimum.GetValueOrDefault(0);
			promoBag.RequiredProductId = promotion.ProductID.GetValueOrDefault(0);
			promoBag.RequiredProductMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
			promoBag.DiscountedProductId = promotion.PromotionProductID.GetValueOrDefault(0);
			promoBag.DiscountedProductQuantity = promotion.PromotionProductQty.GetValueOrDefault(1);
			promoBag.RequiredBrandId = promotion.ManufacturerID;
			promoBag.RequiredBrandMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
			promoBag.RequiredCatalogId = promotion.CatalogID;
			promoBag.RequiredCatalogMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);
			promoBag.RequiredCategoryId = promotion.CategoryID;
			promoBag.RequiredCategoryMinimumQuantity = promotion.QuantityMinimum.GetValueOrDefault(1);

			if (promotion.CouponInd)
			{
				promoBag.Coupon = GetCoupon(promotion);
			}

			return promoBag;
		}

		private void AddPromotionType(Promotion promotion, ZnodePromotionBag promotionBag)
		{
			var availablePromoTypes = GetAvailablePromotionTypes();
			foreach (var t in availablePromoTypes.Where(t => t.ClassName == promotion.DiscountTypeIDSource.ClassName))
			{
				try
				{
					var promoTypeAssembly = Assembly.GetAssembly(t.GetType());
					var cartPromo = (IZnodeCartPromotionType)promoTypeAssembly.CreateInstance(t.ToString());

					if (cartPromo != null)
					{
						cartPromo.Precedence = promotion.DisplayOrder;
						cartPromo.Bind(_shoppingCart, promotionBag);
						_cartPromotions.Add(cartPromo);
					}
				}
				catch (Exception ex)
				{
					ZNodeLogging.LogMessage("Error while instantiating promotion type: " + t);
					ZNodeLogging.LogMessage(ex.ToString());
				}
			}
		}
	}
}