using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
	public class ZnodePricePromotionPercentOffProduct : ZnodePricePromotionType
	{
		public ZnodePricePromotionPercentOffProduct()
		{
			Name = "Percent Off Displayed Product Price";
			Description = "Displays a discounted price for the product based on the percent off.";
			AvailableForFranchise = false;

			Controls.Add(ZnodePromotionControl.Store);
			Controls.Add(ZnodePromotionControl.Profile);
			Controls.Add(ZnodePromotionControl.DiscountPercent);
			Controls.Add(ZnodePromotionControl.RequiredProduct);
		}

		/// <summary>
		/// Calculates the percent off the product price based on the promotion discount.
		/// </summary>
		/// <param name="product">The product to discount.</param>
		/// <param name="currentPrice">The current price of the product.</param>
		/// <returns>Returns the promotional price for the product.</returns>
		public override decimal PromotionalPrice(ZNodeProductBaseEntity product, decimal currentPrice)
		{
			return PromotionalPrice(product.ProductID, currentPrice);
		}

		/// <summary>
		/// Calculates the percent off the product price based on the promotion discount.
		/// </summary>
		/// <param name="productId">The ID of the product to discount.</param>
		/// <param name="currentPrice">The current price of the product.</param>
		/// <returns>Returns the promotional price for the product.</returns>
		public override decimal PromotionalPrice(int productId, decimal currentPrice)
		{
			var discountedPrice = currentPrice;

			if (productId == PromotionBag.RequiredProductId)
			{
				discountedPrice -= currentPrice * (PromotionBag.Discount / 100);
			}

			if (discountedPrice < 0)
			{
				discountedPrice = 0;
			}

			return discountedPrice;
		}
	}
}
