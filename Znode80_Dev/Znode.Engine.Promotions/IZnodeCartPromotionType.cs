﻿using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Promotions
{
	/// <summary>
	/// This is the interface for all shopping cart promotion types.
	/// </summary>
	public interface IZnodeCartPromotionType : IZnodePromotionType
	{
		void Bind(ZNodeShoppingCart shoppingCart, ZnodePromotionBag promotionBag);
		void Calculate();
		bool PreSubmitOrderProcess();
		void PostSubmitOrderProcess();
	}
}
