using System;
using System.Web;
using System.Xml.Serialization;
using ZNode.Libraries.ECommerce.Entities;
using Znode.Engine.Taxes;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the properties and methods of a product AddOn values.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeAddOnValue : ZNodeAddOnValueEntity
    {
        /// <summary>
        /// Initializes a new instance of the ZNodeAddOnValue class
        /// </summary>
        public ZNodeAddOnValue()
        {
        }

        /// <summary>
        /// Initializes a new instance of the ZNodeAddOnValue class
        /// </summary>
        /// <param name="addOnValueEntity">ZNodeAddOnValueEntity object.</param>
        public ZNodeAddOnValue(ZNodeAddOnValueEntity addOnValueEntity)
        {
            this.AddOnId = addOnValueEntity.AddOnId;
            this.AddOnValueID = addOnValueEntity.AddOnValueID;
            this.Description = addOnValueEntity.Description;
            this.DiscountAmount = addOnValueEntity.DiscountAmount;
            this.DisplayOrder = addOnValueEntity.DisplayOrder;
            this.FreeShippingInd = addOnValueEntity.FreeShippingInd;
            this.GST = addOnValueEntity.GST;
            this.Height = addOnValueEntity.Height;
            this.HST = addOnValueEntity.HST;
            this.IsDefault = addOnValueEntity.IsDefault;
            this.Length = addOnValueEntity.Length;
            this.Name = HttpContext.Current.Server.HtmlDecode(addOnValueEntity.Name);
            //this.Price = addOnValueEntity.Price;
            this.PST = addOnValueEntity.PST;
            this.QuantityOnHand = addOnValueEntity.QuantityOnHand;
            this.RecurringBillingFrequency = addOnValueEntity.RecurringBillingFrequency;
            this.RecurringBillingInd = addOnValueEntity.RecurringBillingInd;
            this.RecurringBillingInitialAmount = addOnValueEntity.RecurringBillingInitialAmount;
            this.RecurringBillingInstallmentInd = addOnValueEntity.RecurringBillingInstallmentInd;
            this.RecurringBillingPeriod = addOnValueEntity.RecurringBillingPeriod;
            this.RecurringBillingTotalCycles = addOnValueEntity.RecurringBillingTotalCycles;
            this.RetailPrice = addOnValueEntity.RetailPrice;
            this.SalePrice = addOnValueEntity.SalePrice;
            this.SalesTax = addOnValueEntity.SalesTax;
            this.ShippingCost = addOnValueEntity.ShippingCost;
            this.ShippingRuleTypeID = addOnValueEntity.ShippingRuleTypeID;
            this.SKU = addOnValueEntity.SKU;
            this.SupplierID = addOnValueEntity.SupplierID;
            this.TaxClassID = addOnValueEntity.TaxClassID;
            this.VAT = addOnValueEntity.VAT;
            this.Weight = addOnValueEntity.Weight;
            this.WholesalePrice = addOnValueEntity.WholesalePrice;
            this.Width = addOnValueEntity.Width;
            this.CustomText = addOnValueEntity.CustomText;
        }

        #region Protected Properties
        /// <summary>
        /// Gets the inclisive tax rate.
        /// </summary>
        [XmlIgnore()]
        protected new decimal InclusiveTaxRate
        {
            get
            {
                decimal? _InclusiveTaxRate = null;
                int taxClassId = 0;

                if (!_InclusiveTaxRate.HasValue && taxClassId > 0)
                {
                    ZnodeInclusiveTax _InclusiveTax = new ZnodeInclusiveTax();
                    _InclusiveTaxRate = _InclusiveTax.GetInclusiveTaxRate(taxClassId);
                }

                return _InclusiveTaxRate.GetValueOrDefault(0);
            }
        }
        #endregion
    }
}
