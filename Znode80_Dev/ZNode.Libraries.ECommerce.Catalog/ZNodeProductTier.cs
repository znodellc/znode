using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Entities;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents a product tier price
    /// </summary>    
    [Serializable()]
    [XmlRoot()]
    public class ZNodeProductTier : ZNode.Libraries.ECommerce.Entities.ZNodeProductTierEntity
    {
       
    }
}
