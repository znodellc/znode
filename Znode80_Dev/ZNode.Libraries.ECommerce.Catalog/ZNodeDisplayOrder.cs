﻿using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the ZNodeDisplayOrder class.
    /// </summary>
    public class ZNodeDisplayOrder
    {        
        /// <summary>
        /// Set the new display order based on viewed.
        /// </summary>
        /// <param name="product">Product to update its display order.</param>
        public void SetDisplayOrder(ZNodeProduct product)
        {
            if (ZNodeConfigManager.SiteConfig.UseDynamicDisplayOrder == true)
            {
                ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
                productHelper.UpdateDisplayOrder(ZNodeCatalogManager.CatalogConfig.CatalogID, product.ProductID, product.DisplayOrder);
            }
        }
    }
}
