using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the ZNodeCatalogHelper class.
    /// </summary>
    public class ZNodeCatalogHelper : ZNodeBusinessBase
    {
        /// <summary>
        /// Get Image path based on theme
        /// </summary>
        /// <param name="imageFileName">Catalog theme image file name.</param>
        /// <returns>Returns the catalog image relative path.</returns>
        public static string GetCatalogImagePath(string imageFileName)
        {
            return "~/themes/" + ZNodeCatalogManager.Theme + "/Images/" + imageFileName + ".gif";
        }
    }
}
