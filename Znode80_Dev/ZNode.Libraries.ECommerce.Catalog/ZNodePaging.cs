﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Text;
using ZNode.Libraries.Framework.Business;
using System.Web.UI.WebControls;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the ZNodePaging class.
    /// </summary>
    public class ZNodePaging
    {
        /// <summary>
        /// Initializes a new instance of the ZNodePaging class
        /// </summary>
        public ZNodePaging()
        {           
        }

        /// <summary>
        /// Get the paged products.
        /// </summary>
        /// <param name="productTable">Datatable that contains preloaded data.</param>
        /// <param name="orderBy">Sort the the product by field</param>
        /// <param name="orderDirection">Sort the product by direction (ASC or DESC)</param>
        /// <param name="pageIndex">Row number at which to start reading.</param>
        /// <param name="pageSize">Number of rows to return.</param>
        /// <param name="orderOption">Order by option.</param>
        /// <returns>Returns the sorted ZNodeProductList object.</returns>
        public static ZNodeProductList GetProducts(DataTable productTable, string orderBy, string orderDirection, int pageIndex, int pageSize, int orderOption)
        {
            ZNodeProductList productList = null;

	        if (pageIndex > 0 && productTable != null)
	        {
		        pageIndex = pageIndex - 1;

		        if (pageSize == -1)
		        {
			        pageSize = productTable.Rows.Count;
		        }

		        int pageLowerBound = pageSize*pageIndex;
		        int pageUpperBound = pageLowerBound + pageSize;

		        int totalPageCount = 0;
		        int totalRecordCount = 0;

		        if (productTable.Rows.Count > 0)
		        {
			        var dv = new DataView(productTable);
			        dv.Sort = orderBy + " " + orderDirection;

			        string columnName = "RetailPriceDESC";

			        if (orderOption != 2)
			        {
				        columnName = orderBy;
			        }

			        var foundRows =
				        dv.ToTable().Select(columnName + " > " + pageLowerBound + " AND " + columnName + " <= " + pageUpperBound);

			        var sb = new StringBuilder();

			        foreach (DataRow dr in foundRows)
			        {
				        sb.Append(dr["ProductID"].ToString());
				        sb.Append(",");
			        }


                    var externalAccountNo = string.Empty;
			        if (UserAccount.ZNodeUserAccount.CurrentAccount() != null &&
			            ZNodeConfigManager.SiteConfig.EnableCustomerPricing.GetValueOrDefault())
                        externalAccountNo = UserAccount.ZNodeUserAccount.CurrentAccount().ExternalAccountNo;
					 
					int profileId = 0;
					profileId = ZNodeProfile.CurrentUserProfileId;
					productList = ZnodeCatalogFactory.GetProductDetailsByProductIds(sb.ToString(), externalAccountNo, profileId);

					totalRecordCount = productTable.Rows.Count;
					totalPageCount = totalRecordCount % pageSize == 0
										 ? totalRecordCount / pageSize
										 : ((totalRecordCount / pageSize) + 1);

					productList.TotalPageCount = totalPageCount;
					productList.TotalRecordCount = totalRecordCount;
		        }


		        if (productList != null && productList.ZNodeProductCollection != null &&
		            productList.ZNodeProductCollection.Count > 0)
		        {
			        productList.ApplyPromotion();
			        productList.ZNodeProductCollection = productList.ZNodeProductCollection.Sort("RowIndex");
		        }		       
	        }

	        return productList;
        }



        /// <summary>
        /// Get the paged products.
        /// </summary>
        /// <param name="productIds">List that contains preloaded data.</param>
        /// <param name="orderBy">Sort the the product by field</param>
        /// <param name="orderDirection">Sort the product by direction (ASC or DESC)</param>
        /// <param name="pageIndex">Row number at which to start reading.</param>
        /// <param name="pageSize">Number of rows to return.</param>
        /// <param name="orderOption">Order by option.</param>
        /// <returns>Returns the sorted ZNodeProductList object.</returns>
        public static ZNodeProductList GetProducts(List<int> productIds, string orderBy, string orderDirection, int pageIndex, int pageSize, int orderOption)
        {
            ZNodeProductList productList = null;
            var collection = new ZNodeGenericCollection<ZNodeProductBase>();
          
            if (pageIndex > 0 && productIds != null)
            {
                pageIndex = pageIndex - 1;

                if (pageSize == -1)
                {
                    pageSize = productIds.Count;
                }

                int pageLowerBound = pageSize * pageIndex;
                int pageUpperBound = (pageLowerBound + pageSize) - pageLowerBound;

                int totalPageCount = 0;
                int totalRecordCount = 0;

                if (productIds.Count > 0)
                {
                    StringBuilder sb = new StringBuilder();
                    if (orderBy == "FinalPrice")
                    {
                        foreach (var item in productIds)
                        {
                            sb.Append(item.ToString());
                            sb.Append(",");
                        }
                    }
                    else
                    {
                        foreach (var item in productIds.Skip(pageLowerBound).Take(pageUpperBound))
                        {
                            sb.Append(item.ToString());
                            sb.Append(",");
                        }
                    }

                    ZNode.Libraries.DataAccess.Custom.ProductHelper productHelper = new ZNode.Libraries.DataAccess.Custom.ProductHelper();

                    var externalAccountNo = string.Empty;
                    if (ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount() != null && ZNodeConfigManager.SiteConfig.EnableCustomerPricing.GetValueOrDefault() && ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount().EnableCustomerPricing)
                        externalAccountNo = ZNode.Libraries.ECommerce.UserAccount.ZNodeUserAccount.CurrentAccount().ExternalAccountNo;

	                int profileId = 0;
					profileId = ZNodeProfile.CurrentUserProfileId;
					productList = ZnodeCatalogFactory.GetProductDetailsByProductIds(sb.ToString(), externalAccountNo, profileId);

					totalRecordCount = productIds.Count;
					totalPageCount = totalRecordCount % pageSize == 0 ? totalRecordCount / pageSize : ((totalRecordCount / pageSize) + 1);

					productList.TotalPageCount = totalPageCount;
					productList.TotalRecordCount = totalRecordCount;

				}

			
	        
		            
				if (productList != null && productList.ZNodeProductCollection != null &&
					productList.ZNodeProductCollection.Count > 0)
				{
					productList.ApplyPromotion();
					productList.ZNodeProductCollection = productList.ZNodeProductCollection.Sort("RowIndex");
				}		     

                if (!string.IsNullOrEmpty(orderBy) && !string.IsNullOrEmpty(orderDirection))
                {

                    var productCollection = productList.ZNodeProductCollection.Cast<ZNodeProductBase>();
                    if (orderDirection == "ASC")
                    {
                        productCollection = productCollection.OrderBy(x => x.CallForPricing).ThenBy(x => x.FinalPrice).ToList();
                    }
                    else
                    {
                        productCollection = productCollection.OrderBy(x => x.CallForPricing).ThenByDescending(x => x.FinalPrice);
                    }
                    productCollection.Skip(pageLowerBound)
                    .Take(pageUpperBound).ToList()
                    .ForEach(x => collection.Add(x));

                    productList.ZNodeProductCollection = collection;
                }
            }

            return productList;
        }
    }
}
