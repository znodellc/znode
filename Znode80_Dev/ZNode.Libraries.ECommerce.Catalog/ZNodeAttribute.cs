using System;
using System.Web;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the properties and methods of a ZNodeAttribute.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeAttribute : ZNodeBusinessBase
    {
        #region Private Member Variables
        private int _AttributeId;
        private int _AttributeTypeId;
        private string _Name = string.Empty;
        private string _ExternalId = string.Empty;
        private int _DisplayOrder;
        private bool _IsActive;
        private int _OldAttributeId;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the ZNodeAttribute class
        /// </summary>
        public ZNodeAttribute()
        {
			SkuAttributes = new ZNodeGenericCollection<ZNodeSkuAttribute>();
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Product Attribute Id
        /// </summary>
        [XmlElement()]
        public int AttributeId
        {
            get { return this._AttributeId; }
            set { this._AttributeId = value; }
        }

        /// <summary>
        /// Gets or sets the Product AttributeTypeId
        /// </summary>
        [XmlElement()]
        public int AttributeTypeId
        {
            get { return this._AttributeTypeId; }
            set { this._AttributeTypeId = value; }
        }

        /// <summary>
        /// Gets or sets the Product Attribute Name
        /// </summary>
        [XmlElement()]
        public string Name
        {
            get { return HttpContext.Current.Server.HtmlDecode(this._Name); }
            set { this._Name = HttpContext.Current.Server.HtmlDecode(value); }
        }

        /// <summary>
        /// Gets or sets the Product Attribute ExternalId
        /// </summary>
        [XmlElement()]
        public string ExternalId
        {
            get { return this._ExternalId; }
            set { this._ExternalId = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the attribute is IsActive or not.
        /// </summary>
        [XmlElement()]
        public bool IsActive
        {
            get { return this._IsActive; }
            set { this._IsActive = value; }
        }

        /// <summary>
        /// Gets or sets the Product Attribute display order
        /// </summary>
        [XmlElement()]
        public int DisplayOrder
        {
            get { return this._DisplayOrder; }
            set { this._DisplayOrder = value; }
        }

        /// <summary>
        /// Gets or sets the OldAttributeId property
        /// </summary>
        [XmlElement()]
        public int OldAttributeId
        {
            get { return this._OldAttributeId; }
            set { this._OldAttributeId = value; }
        }

		[XmlElement("ZNodeSkuAttribute")]
		public ZNodeGenericCollection<ZNodeSkuAttribute> SkuAttributes { get; set; }
		#endregion
	}

	/// <summary>
	/// Represents the properties and methods of a ZNodeAttribute.
	/// </summary>
	[Serializable()]
	[XmlRoot()]
	public class ZNodeSkuAttribute : ZNodeBusinessBase
	{
		[XmlElement()]
		public int SKUID { get; set; }
	}
}
