using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    #region ZNodeProductType
    /// <summary>
    /// Represents the properties and methods of a ZNodeProductType.
    /// </summary>
    [XmlRoot()]
    public class ZNodeProductType : ZNodeBusinessBase
    {
        #region Private Member Variables
        private int _ProductTypeId;
        private int _PortalId;
        private string _Name = string.Empty;
        private string _Description = string.Empty;
        private int _DisplayOrder;
        private string _AttributeValue = string.Empty;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the ZNodeProductType class
        /// </summary>
        public ZNodeProductType()
        {
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the ProductTypeId
        /// </summary>
        [XmlElement()]
        public int ProductTypeId
        {
            get { return this._ProductTypeId; }
            set { this._ProductTypeId = value; }
        }

        /// <summary>
        /// Gets or sets the site portalId
        /// </summary>
        [XmlElement()]
        public int PortalId
        {
            get { return this._PortalId; }
            set { this._PortalId = value; }
        }

        /// <summary>
        /// Gets or sets the name for this ProductType
        /// </summary>
        [XmlElement()]
        public string Name
        {
            get { return this._Name; }
            set { this._Name = value; }
        }

        /// <summary>
        /// Gets or sets the description for this ProductType
        /// </summary>
        [XmlElement()]
        public string Description
        {
            get { return this._Description; }
            set { this._Description = value; }
        }

        /// <summary>
        /// Gets or sets the display order value for this ProductType
        /// </summary>
        [XmlElement()]
        public int DisplayOrder
        {
            get { return this._DisplayOrder; }
            set { this._DisplayOrder = value; }
        }

        #endregion
    }
    #endregion
}
