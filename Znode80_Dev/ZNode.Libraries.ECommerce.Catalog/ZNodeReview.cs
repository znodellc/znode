using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represents the properties and methods of a product reviews.
    /// </summary>
    [Serializable()]
    [XmlRoot()]
    public class ZNodeReview
    {
        #region Private Member Variables
        private int _ReviewId;
        private int _ProductId;
        private int _AccountId;
        private string _Subject = string.Empty;
        private string _Pros = string.Empty;
        private string _Cons = string.Empty;
        private string _Comments = string.Empty;
        private string _CreateUser = string.Empty;        
        private int _Rating;
        private string _Status = "N";
        private string _UserLocation = string.Empty;
        private DateTime _CreateDate;
        private string _Custom1 = String.Empty;
        private string _Custom2 = String.Empty;
        private string _Custom3 = String.Empty;
        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the ZNodeReview class
        /// </summary>
        public ZNodeReview()
        {
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the customer Review id
        /// </summary>
        [XmlElement()]
        public int ReviewID
        {
            get { return this._ReviewId; }
            set { this._ReviewId = value; }
        }

        /// <summary>
        /// Gets or sets the product id
        /// </summary>
        [XmlElement()]
        public int ProductID
        {
            get { return this._ProductId; }
            set { this._ProductId = value; }
        }

        /// <summary>
        /// Gets or sets the account id
        /// </summary>
        [XmlElement()]
        public int AccountID
        {
            get { return this._AccountId; }
            set { this._AccountId = value; }
        }

        /// <summary>
        /// Gets or sets the review subject
        /// </summary>
        [XmlElement()]
        public string Subject
        {
            get { return this._Subject; }
            set { this._Subject = value; }
        }

        /// <summary>
        /// Gets or sets the review pros
        /// </summary>
        [XmlElement()]
        public string Pros
        {
            get { return this._Pros; }
            set { this._Pros = value; }
        }

        /// <summary>
        /// Gets or sets the review cons
        /// </summary>
        [XmlElement()]
        public string Cons
        {
            get { return this._Cons; }
            set { this._Cons = value; }
        }

        /// <summary>
        /// Gets or sets the review comments
        /// </summary>
        [XmlElement()]
        public string Comments
        {
            get { return this._Comments; }
            set { this._Comments = value; }
        }

        /// <summary>
        /// Gets or sets the review rating
        /// </summary>
        [XmlElement()]
        public int Rating
        {
            get { return this._Rating; }
            set { this._Rating = value; }
        }

        /// <summary>
        /// Gets or sets the status (Like A -Active , I - In Active, N - New)
        /// </summary>
        [XmlElement()]
        public string Status
        {
            get { return this._Status; }
            set { this._Status = value; }
        }

        /// <summary>
        /// Gets or sets the review created user nick name
        /// </summary>
        [XmlElement()]
        public string CreateUser
        {
            get { return this._CreateUser; }
            set { this._CreateUser = value; }
        }

        /// <summary>
        /// Gets or sets the created user location
        /// </summary>
        [XmlElement()]
        public string UserLocation
        {
            get { return this._UserLocation; }
            set { this._UserLocation = value; }
        }

        /// <summary>
        /// Gets or sets the review created date
        /// </summary>
        [XmlElement()]
        public DateTime CreateDate
        {
            get { return this._CreateDate; }
            set { this._CreateDate = value; }
        }
        
        /// <summary>
        /// Gets or sets the review Custom1
        /// </summary>
        [XmlElement()]
        public string Custom1
        {
            get { return this._Custom1; }
            set { this._Custom1 = value; }
        }

        /// <summary>
        /// Gets or sets the review Custom2
        /// </summary>
        [XmlElement()]
        public string Custom2
        {
            get { return this._Custom2; }
            set { this._Custom2 = value; }
        }

        /// <summary>
        /// Gets or sets the review Custom3
        /// </summary>
        [XmlElement()]
        public string Custom3
        {
            get { return this._Custom3; }
            set { this._Custom3 = value; }
        }
        #endregion
    }
}
