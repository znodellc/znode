using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Catalog
{
    /// <summary>
    /// Represens the ZNodeProfile class
    /// </summary>
    public class ZNodeProfile : ZNodeBusinessBase
    {
        #region Private Member Variables
        private Profile _Profile = null;
        #endregion      
        
        #region Public Constructors
        /// <summary>
        /// Initializes a new instance of the ZNodeProfile class
        /// </summary>
        public ZNodeProfile()
        {
            this._Profile = CurrentUserProfile;
        }
        #endregion

        #region Public Static Properties            

        /// <summary>
        /// Gets the current user profile.
        /// </summary>
        public static Profile CurrentUserProfile
        {
            get
            {
                Profile profile = null;

                if (System.Web.HttpContext.Current.Session["ProfileCache"] != null)
                {
                    profile = (ZNode.Libraries.DataAccess.Entities.Profile)System.Web.HttpContext.Current.Session["ProfileCache"];
                }

                if (profile == null)
                {
                    profile = DefaultAnonymousProfile;

                    if (profile != null)
                    {
                        System.Web.HttpContext.Current.Session["ProfileCache"] = profile;
                    }
                    else 
                    {
                        // If not exists, then get Default profile
                        profile = DefaultRegisteredProfile;
                    }

                    if (profile != null)
                    {
                        System.Web.HttpContext.Current.Session["ProfileCache"] = profile;
                    }
                }

                return profile;
            }
        }
        
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the current user profile Id
        /// </summary>
        public static int CurrentUserProfileId
        {
            get
            {
                Profile profile = CurrentUserProfile;

                if (profile != null)
                {
                    return profile.ProfileID;
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets default registered profile.
        /// </summary>
        public static int DefaultRegisteredProfileId
        {
            get
            {
                Profile profile = DefaultRegisteredProfile;

                if (profile != null)
                {
                    return profile.ProfileID;
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets the profile name
        /// </summary>
        public static string ProfileName
        {
            get
            {
                Profile profile = CurrentUserProfile;

                if (profile != null)
                {
                    return profile.Name;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Gets a value indicating whether to show or hide the price.
        /// </summary>
        public bool ShowPrice
        {
            get
            {
                if (this._Profile != null)
                {
                    return this._Profile.ShowPricing;
                }

                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether to show or hide the add to cart button.
        /// </summary>
        public bool ShowAddToCart
        {
            get
            {
                if (this._Profile != null)
                {
                    return this._Profile.ShowPricing;
                }

                return true;
            }
        }

        /// <summary>
        /// Gets a value indicating whether to use whoelsale price or not.
        /// </summary>
        public bool UseWholesalePricing
        {
            get
            {
                if (this._Profile != null)
                {
                    return this._Profile.UseWholesalePricing.GetValueOrDefault(false);
                }

                return false;
            }
        }

        /// <summary>
        /// Gets a default anonymous profile.
        /// </summary>
        private static Profile DefaultAnonymousProfile
        {
            get
            {
                Profile profile = null;

                // Get Portal by Domain Config
                ProfileService ProfileService = new ProfileService();
                Domain currentDomain = ZNodeConfigManager.DomainConfig;

                // Get DefaultRegistered Profile
                if (currentDomain != null)
                {
                    PortalService portalService = new PortalService();
                    Portal currentPortal = portalService.GetByPortalID(currentDomain.PortalID);
                    profile = ProfileService.GetByProfileID(currentPortal.DefaultAnonymousProfileID.GetValueOrDefault());
                }

                if (profile != null)
                {
                    System.Web.HttpContext.Current.Session["ProfileCache"] = profile;
                }

                return profile;
            }
        }

        /// <summary>
        /// Gets the default registered profile.
        /// </summary>
        private static Profile DefaultRegisteredProfile
        {
            get
            {
                Profile profile = null;
                bool check = false;

                if (System.Web.HttpContext.Current.Session["ProfileCache"] != null)
                {
                    profile = (ZNode.Libraries.DataAccess.Entities.Profile)System.Web.HttpContext.Current.Session["ProfileCache"];
                }

                if (profile != null)
                {
                    if (profile.ProfileID == ZNodeConfigManager.SiteConfig.DefaultRegisteredProfileID.GetValueOrDefault())
                    {
                        check = true;
                    }
                }

                if (!check)
                {
                    // Get Portal by Domain Config
                    ProfileService ProfileService = new ProfileService();
                    Domain currentDomain = ZNodeConfigManager.DomainConfig;

                    // Get DefaultRegistered Profile
                    if (currentDomain != null)
                    {
                        PortalService portalService = new PortalService();
                        Portal currentPortal = portalService.GetByPortalID(currentDomain.PortalID);
                        profile = ProfileService.GetByProfileID(currentPortal.DefaultRegisteredProfileID.GetValueOrDefault());
                    }
                }

                return profile;
            }
        }
        #endregion
    }
}
