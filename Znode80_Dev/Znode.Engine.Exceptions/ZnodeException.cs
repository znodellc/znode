﻿using System;
using System.Net;

namespace Znode.Engine.Exceptions
{
	public class ZnodeException : Exception
	{
		public int? ErrorCode { get; private set; }
		public string ErrorMessage { get; private set; }
		public HttpStatusCode StatusCode { get; private set; }

		/// <summary>
		/// Creates a new ZnodeException.
		/// </summary>
		public ZnodeException()
		{
		}

		/// <summary>
		/// Creates a new ZnodeException.
		/// </summary>
		/// <param name="errorCode">The error code.</param>
		/// <param name="errorMessage">The error message.</param>
		public ZnodeException(int? errorCode, string errorMessage)
		{
			ErrorCode = errorCode;
			ErrorMessage = errorMessage;
		}

		/// <summary>
		/// Creates a new ZnodeException.
		/// </summary>
		/// <param name="errorCode">The error code.</param>
		/// <param name="errorMessage">The error message.</param>
		/// <param name="statusCode">The HTTP status code.</param>
		public ZnodeException(int? errorCode, string errorMessage, HttpStatusCode statusCode)
		{
			ErrorCode = errorCode;
			ErrorMessage = errorMessage;
			StatusCode = statusCode;
		}
	}
}
