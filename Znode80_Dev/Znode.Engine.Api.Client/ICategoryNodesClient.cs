﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ICategoryNodesClient : IBaseClient
	{
		CategoryNodeModel GetCategoryNode(int categoryNodeId);
		CategoryNodeModel GetCategoryNode(int categoryNodeId, ExpandCollection expands);
		CategoryNodeListModel GetCategoryNodes(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		CategoryNodeListModel GetCategoryNodes(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		CategoryNodeModel CreateCategoryNode(CategoryNodeModel model);
		CategoryNodeModel UpdateCategoryNode(int categoryNodeId, CategoryNodeModel model);
		bool DeleteCategoryNode(int categoryNodeId);
	}
}
