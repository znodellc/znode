﻿using System;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
	public interface IAccountsClient : IBaseClient
	{
		AccountModel GetAccount(int accountId);
		AccountModel GetAccount(int accountId, ExpandCollection expands);
		AccountModel GetAccountByUser(Guid userId);
		AccountModel GetAccountByUser(Guid userId, ExpandCollection expands);
		AccountModel GetAccountByUser(string username);
		AccountModel GetAccountByUser(string username, ExpandCollection expands);
		AccountListModel GetAccounts(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		AccountListModel GetAccounts(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		AccountModel CreateAccount(AccountModel model);
		AccountModel UpdateAccount(int accountId, AccountModel models);
		bool DeleteAccount(int accountId);
		AccountModel Login(AccountModel model);
		AccountModel Login(AccountModel model, ExpandCollection expandss);
		AccountModel ResetPassword(AccountModel models);
		AccountModel ChangePassword(AccountModel model);

        /// <summary>
        /// Znode Version 7.2.2
        /// This function will check the Reset Password Link current status.
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <returns>Returns Status of Reset Password Link.</returns>
        AccountModel CheckResetPasswordLinkStatus(AccountModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Method Check for the user role based on role name.
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <returns>Returns status for the user based on role name</returns>
        AccountModel CheckUserRole(AccountModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Function used for Reset admin details for the first default login.
        /// </summary>
        /// <param name="model">AccountModel</param>
        /// <returns>Returns reset admin details status.</returns>
        AccountModel ResetAdminDetails(AccountModel model);

	}
}
