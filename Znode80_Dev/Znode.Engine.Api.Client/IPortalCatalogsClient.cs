﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPortalCatalogsClient : IBaseClient
	{
		PortalCatalogModel GetPortalCatalog(int portalCatalogId);
		PortalCatalogModel GetPortalCatalog(int portalCatalogId, ExpandCollection expands);
		PortalCatalogListModel GetPortalCatalogs(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		PortalCatalogListModel GetPortalCatalogs(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		PortalCatalogModel CreatePortalCatalog(PortalCatalogModel model);
		PortalCatalogModel UpdatePortalCatalog(int portalCatalogId, PortalCatalogModel model);
		bool DeletePortalCatalog(int portalCatalogId);
	}
}
