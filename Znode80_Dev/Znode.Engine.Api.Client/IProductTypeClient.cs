﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IProductTypeClient : IBaseClient
    {
        /// <summary>
        /// Get Product Type List.
        /// </summary>
        /// <param name="productTypeId"></param>
        /// <returns>Returns ProductTypeModel.</returns>
        ProductTypeModel GetProductType(int productTypeId);

        /// <summary>
        /// Get Product Type List.
        /// </summary>
        /// <param name="productTypeId">asd</param>
        /// <param name="expands"></param>
        /// <returns>Returns ProductTypeModel.</returns>
        ProductTypeModel GetProductType(int productTypeId, ExpandCollection expands);

        /// <summary>
        /// Get Product Types List.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <returns>Returns ProductTypeListModel.</returns>
        ProductTypeListModel GetProductTypes(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Get Product Types List.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>Returns ProductTypeListModel.</returns>
        ProductTypeListModel GetProductTypes(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Create New Product Type.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns ProductTypeModel.</returns>
        ProductTypeModel CreateProductType(ProductTypeModel model);

        /// <summary>
        /// Update Existing Product Type.
        /// </summary>
        /// <param name="productTypeId">Int</param>
        /// <param name="model">ProductTypeModel</param>
        /// <returns></returns>
        ProductTypeModel UpdateProductType(int productTypeId, ProductTypeModel model);

        /// <summary>
        /// Delete existing product Type.
        /// </summary>
        /// <param name="productTypeId">int</param>
        /// <returns>Returns bool true/false</returns>
        bool DeleteProductType(int productTypeId);

        /// <summary>
        /// To get list of Product Attributes and its values by productTypeId
        /// </summary>
        /// <param name="productTypeId">int productTypeId</param>
        /// <returns>returns AttributeTypeValueListModel </returns>
        AttributeTypeValueListModel GetProductAttributesByProductTypeId(int productTypeId);
    }
}
