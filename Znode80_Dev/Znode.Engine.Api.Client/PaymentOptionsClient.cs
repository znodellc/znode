﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class PaymentOptionsClient : BaseClient, IPaymentOptionsClient
	{
		public PaymentOptionModel GetPaymentOption(int paymentOptionId)
		{
			return GetPaymentOption(paymentOptionId, null);
		}

		public PaymentOptionModel GetPaymentOption(int paymentOptionId, ExpandCollection expands)
		{
			var endpoint = PaymentOptionsEndpoint.Get(paymentOptionId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<PaymentOptionResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.PaymentOption;
		}

		public PaymentOptionListModel GetPaymentOptions(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetPaymentOptions(expands, filters, sorts, null, null);
		}

		public PaymentOptionListModel GetPaymentOptions(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = PaymentOptionsEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<PaymentOptionListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new PaymentOptionListModel { PaymentOptions = (response == null) ? null : response.PaymentOptions };
			list.MapPagingDataFromResponse(response);
			return list;
		}

		public PaymentOptionModel CreatePaymentOption(PaymentOptionModel model)
		{
			var endpoint = PaymentOptionsEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<PaymentOptionResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.PaymentOption;
		}

		public PaymentOptionModel UpdatePaymentOption(int paymentOptionId, PaymentOptionModel model)
		{
			var endpoint = PaymentOptionsEndpoint.Update(paymentOptionId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<PaymentOptionResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.PaymentOption;
		}

		public bool DeletePaymentOption(int paymentOptionId)
		{
			var endpoint = PaymentOptionsEndpoint.Delete(paymentOptionId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<PaymentOptionResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
