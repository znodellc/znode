﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class ReviewsClient : BaseClient, IReviewsClient
	{
		public ReviewModel GetReview(int reviewId)
		{
			return GetReview(reviewId, null);
		}

		public ReviewModel GetReview(int reviewId, ExpandCollection expands)
		{
			var endpoint = ReviewsEndpoint.Get(reviewId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ReviewResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Review;
		}

		public ReviewListModel GetReviews(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetReviews(expands, filters, sorts, null, null);
		}

		public ReviewListModel GetReviews(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = ReviewsEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ReviewListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new ReviewListModel { Reviews = (response == null) ? null: response.Reviews };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public ReviewModel CreateReview(ReviewModel model)
		{
			var endpoint = ReviewsEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<ReviewResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.Review;
		}

		public ReviewModel UpdateReview(int reviewId, ReviewModel model)
		{
			var endpoint = ReviewsEndpoint.Update(reviewId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<ReviewResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.Review;
		}

		public bool DeleteReview(int reviewId)
		{
			var endpoint = ReviewsEndpoint.Delete(reviewId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<ReviewResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
