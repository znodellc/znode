﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class ProfilesClient : BaseClient, IProfilesClient
    {
        public ProfileModel GetProfile(int profileId)
        {
            return GetProfile(profileId, null);
        }

        public ProfileModel GetProfile(int profileId, ExpandCollection expands)
        {
            var endpoint = ProfilesEndpoint.Get(profileId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProfileResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return Equals(response, null) ? null : response.Profile;
        }

        public Models.ProfileListModel GetProfiles(Expands.ExpandCollection expands, Filters.FilterCollection filters, Sorts.SortCollection sorts)
        {
            return GetProfiles(expands, filters, sorts, null, null);
        }

        public Models.ProfileListModel GetProfiles(Expands.ExpandCollection expands, Filters.FilterCollection filters, Sorts.SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProfilesEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProfileListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProfileListModel { Profiles = (Equals(response,null)) ? null : response.Profiles };
            list.MapPagingDataFromResponse(response);

            return list;
        }        

        public ProfileModel CreateProfile(ProfileModel model)
        {
            var endpoint =ProfilesEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ProfileResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.Profile;
        }

        public ProfileModel UpdateProfile(int profileId, ProfileModel model)
        {
            var endpoint = ProfilesEndpoint.Update(profileId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ProfileResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.Profile;
        }

        public bool DeleteProfile(int profileId)
        {
            var endpoint = ProfilesEndpoint.Delete(profileId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProfileResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }
    }
}
