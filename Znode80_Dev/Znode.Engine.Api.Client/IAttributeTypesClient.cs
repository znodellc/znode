﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IAttributeTypesClient : IBaseClient
	{
		AttributeTypeModel GetAttributeType(int attributeTypeId);
		AttributeTypeListModel GetAttributeTypes(FilterCollection filters, SortCollection sorts);
		AttributeTypeModel CreateAttributeType(AttributeTypeModel model);
		AttributeTypeModel UpdateAttributeType(int attributeTypeId, AttributeTypeModel model);
		bool DeleteAttributeType(int attributeTypeId);
	}
}
