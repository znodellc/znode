﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class ShoppingCartsClient : BaseClient, IShoppingCartsClient
	{
		public ShoppingCartModel GetCartByAccount(int accountId)
		{
			var endpoint = ShoppingCartsEndpoint.GetByAccount(accountId);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ShoppingCartResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.ShoppingCart;
		}

		public ShoppingCartModel GetCartByCookie(int cookieId)
		{
			var endpoint = ShoppingCartsEndpoint.GetByCookie(cookieId);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ShoppingCartResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.ShoppingCart;
		}

		public ShoppingCartModel CreateCart(ShoppingCartModel model)
		{
			var endpoint = ShoppingCartsEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<ShoppingCartResponse>(endpoint, JsonConvert.SerializeObject(model), status);
            // TBD needs to creat new API method to remove items 
            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.Created, HttpStatusCode.InternalServerError };

            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.ShoppingCart;
		}

		public ShoppingCartModel Calculate(ShoppingCartModel model)
		{
			var endpoint = ShoppingCartsEndpoint.Calculate();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<ShoppingCartResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.ShoppingCart;
		}
	}
}
