﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Api.Models.Extensions;

namespace Znode.Engine.Api.Client
{
    public class CSSClient : BaseClient, ICSSClient
    {
        public CSSListModel GetCSSs(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetCSSs(expands, filters, sorts, null, null);
        }
        public CSSListModel GetCSSs(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CSSEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CSSListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CSSListModel { CSSs = (response == null) ? null : response.CSSs };
            list.MapPagingDataFromResponse(response);

            return list;
        }
    }
}
