﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IMasterPageClient : IBaseClient
    {
        /// <summary>
        /// Method Returns the MasterPages.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <returns>Returns the MasterPages</returns>
        MasterPageListModel GetMasterPages(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Method Returns the MasterPages.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>Returns the MasterPages</returns>
        MasterPageListModel GetMasterPages(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Method Returns the MasterPages by theme Id
        /// </summary>
        /// <param name="themeId"></param>
        /// <param name="pageType"></param>
        /// <returns>Returns the MasterPages</returns>
        MasterPageListModel GetMasterPageByThemeId(int themeId, string pageType);
    }
}
