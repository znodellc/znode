﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IProductAttributesClient : IBaseClient
    {
        AttributeModel GetAttributes(int attributeId);
        ProductAttributeListModel GetAttributes(FilterCollection filters, SortCollection sorts);
        AttributeModel CreateAttributes(AttributeModel model);
        AttributeModel UpdateAttributes(int attributeId, AttributeModel model);
        bool DeleteAttributes(int attributeId);
        ProductAttributeListModel GetAttributesByAttributeTypeId(int attributeTypeId);
    }
}
