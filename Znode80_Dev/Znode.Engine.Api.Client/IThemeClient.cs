﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
namespace Znode.Engine.Api.Client
{
    public interface IThemeClient : IBaseClient
    {
        ThemeListModel GetThemes(FilterCollection filters, SortCollection sorts);
    }
}
