﻿using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IProductsClient : IBaseClient
	{
		ProductModel GetProduct(int productId);
		ProductModel GetProduct(int productId, ExpandCollection expands);
		ProductModel GetProductWithSku(int productId, int skuId);
		ProductListModel GetProducts(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		ProductListModel GetProducts(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		ProductListModel GetProductsByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		ProductListModel GetProductsByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        ProductListModel GetProductsByCatalogIds(string catalogIds, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		ProductListModel GetProductsByCategory(int categoryId, ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		ProductListModel GetProductsByCategory(int categoryId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		ProductListModel GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		ProductListModel GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		ProductListModel GetProductsByExternalIds(string externalIds, ExpandCollection expands, SortCollection sorts);
		ProductListModel GetProductsByHomeSpecials(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		ProductListModel GetProductsByProductIds(string productIds, ExpandCollection expands, SortCollection sorts);
        ProductListModel GetProductsByProductIds(string productIds, ExpandCollection expands, SortCollection sorts, int? pageIndex, int? pageSize);
		ProductListModel GetProductsByPromotionType(int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		ProductListModel GetProductsByPromotionType(int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        ProductListModel GetProductsByQuery(string query);
		ProductModel CreateProduct(ProductModel model);
		ProductModel UpdateProduct(int productId, ProductModel model);
		bool DeleteProduct(int productId);

        Task<ProductListModel> GetProductsByProductIdsAsync(string productIds, int accountId, ExpandCollection expands, SortCollection sorts);
        Task<ProductListModel> GetProductsByHomeSpecialsAsync(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Znode Version 8.0
        /// Get the Product Tags based on product Id.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns>Return product tags</returns>
        ProductTagsModel GetProductTags(int productId);

        /// <summary>
        /// Znode Version 8.0
        /// Create the Product tags for the Product
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        ProductTagsModel CreateProductTags(ProductTagsModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Updates the Product tags for the Product
        /// </summary>
        /// <param name="tagId"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        ProductTagsModel UpdateProductTags(int tagId, ProductTagsModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Deletes the Product tags based on product id.
        /// </summary>
        /// <param name="tagId"></param>
        /// <returns></returns>
        bool DeleteProductTags(int tagId);

        /// <summary>
        /// Znode Version 8.0
        /// To Get the Product associated facets.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        ProductFacetModel GetAssociatedFacets(int productId);

        /// <summary>
        ///  Znode Version 8.0
        /// To Associcate the Facets to the Product.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        bool BindAssociatedFacets(ProductFacetModel model);
        

        /// <summary>
        /// Znode Version 8.0
        /// To get product details by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns product details</returns>
        ProductListModel GetProductDetailsByProductId(int productId);
	}
}
