﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPromotionTypesClient : IBaseClient
	{
		PromotionTypeModel GetPromotionType(int promotionTypeId);
		PromotionTypeListModel GetPromotionTypes(FilterCollection filters, SortCollection sorts);
		PromotionTypeListModel GetPromotionTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		PromotionTypeModel CreatePromotionType(PromotionTypeModel model);
		PromotionTypeModel UpdatePromotionType(int promotionTypeId, PromotionTypeModel model);
		bool DeletePromotionType(int promotionTypeId);
	}
}
