﻿using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class ThemeClient : BaseClient, IThemeClient
    {
        public ThemeListModel GetThemes(FilterCollection filters, SortCollection sorts)
        {
            var endpoint = ThemeEndPoint.List();
            endpoint += BuildEndpointQueryString(null,filters, sorts,null,null);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ThemeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ThemeListModel { Themes = (response == null) ? null : response.Themes };
            list.MapPagingDataFromResponse(response);

            return list;
        }
    }
}
