﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IHighlightsClient : IBaseClient
	{
		HighlightModel GetHighlight(int highlightId);
		HighlightModel GetHighlight(int highlightId, ExpandCollection expands);
		HighlightListModel GetHighlights(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		HighlightListModel GetHighlights(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		HighlightModel CreateHighlight(HighlightModel model);
		HighlightModel UpdateHighlight(int highlightId, HighlightModel model);
		bool DeleteHighlight(int highlightId);

        /// <summary>
        ///  Znode Version 8.0
        ///  Check the Associated product with highlight and sets the property of it.
        /// </summary>
        /// <returns>HighlightModel</returns>
        HighlightModel CheckAssociatedProduct(int highlightId);
	}
}
