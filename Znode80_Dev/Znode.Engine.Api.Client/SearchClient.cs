﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class SearchClient : BaseClient, ISearchClient
	{
		public SearchClient() { }

		public SearchClient(int accountId)
		{
			AccountId = accountId;
		}

		public SuggestedSearchListModel GetTypeAheadResponse(SuggestedSearchModel model)
		{
			var endpoint = SearchEndpoint.Suggested();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<SuggestedSearchListResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			//if (status.HasError || status.StatusCode != HttpStatusCode.OK || status.StatusCode != HttpStatusCode.NotFound)
			//{
			//	throw new ZnodeException(status.ErrorCode, status.ErrorMessage, status.StatusCode);
			//}

            var list = new SuggestedSearchListModel { SuggestedSearchResults = (response == null) ? null : response.SuggestedSearchResults };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public KeywordSearchModel Search(KeywordSearchModel model, ExpandCollection expands, SortCollection sorts)
		{
			var endpoint = SearchEndpoint.Keyword();
			endpoint += BuildEndpointQueryString(expands, null, sorts, null, null);

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<KeywordSearchResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			//if (status.HasError || status.StatusCode != HttpStatusCode.OK || (status.StatusCode != HttpStatusCode.OK && status.StatusCode != HttpStatusCode.NotFound) )
			//{
			//	throw new ZnodeException(status.ErrorCode, status.ErrorMessage, status.StatusCode);
			//}

			return response != null ? response.Search : new KeywordSearchModel { Products = new Collection<SearchProductModel>() };
		}
	}
}
