﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IShippingTypesClient : IBaseClient
	{
		ShippingTypeModel GetShippingType(int shippingTypeId);
		ShippingTypeListModel GetShippingTypes(FilterCollection filters, SortCollection sorts);
		ShippingTypeListModel GetShippingTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		ShippingTypeModel CreateShippingType(ShippingTypeModel model);
		ShippingTypeModel UpdateShippingType(int shippingTypeId, ShippingTypeModel model);
		bool DeleteShippingType(int shippingTypeId);
	}
}
