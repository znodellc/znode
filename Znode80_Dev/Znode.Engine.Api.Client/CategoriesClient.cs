﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class CategoriesClient : BaseClient, ICategoriesClient
    {
        public CategoryModel GetCategory(int categoryId)
        {
            return GetCategory(categoryId, null);
        }

        public CategoryModel GetCategory(int categoryId, ExpandCollection expands)
        {
            var endpoint = CategoriesEndpoint.Get(categoryId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.Category;
        }

        public CategoryListModel GetCategories(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetCategories(expands, filters, sorts, null, null);
        }

        public CategoryListModel GetCategories(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CategoriesEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CategoryListModel { Categories = (response == null) ? null : response.Categories };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CategoryListModel GetCategoriesByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetCategoriesByCatalog(catalogId, expands, filters, sorts, null, null);
        }

        public CategoryListModel GetCategoriesByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CategoriesEndpoint.ListByCatalog(catalogId);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CategoryListModel { Categories = (response == null) ? null : response.Categories };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CategoryListModel GetCategoriesByCatalogIds(string catalogIds, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CategoriesEndpoint.ListByCatalogIds(catalogIds);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryListResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            var list = new CategoryListModel { Categories = (response == null) ? null : response.Categories };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CategoryListModel GetCategoriesByCategoryIds(string categoryIds, ExpandCollection expands, FilterCollection filters,
                                                            SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CategoriesEndpoint.ListByCategoryIds(categoryIds);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryListResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            var list = new CategoryListModel { Categories = (response == null) ? null : response.Categories };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public CategoryModel CreateCategory(CategoryModel model)
        {
            var endpoint = CategoriesEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<CategoryResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (response == null) ? null : response.Category;
        }

        public CategoryModel UpdateCategory(int categoryId, CategoryModel model)
        {
            var endpoint = CategoriesEndpoint.Update(categoryId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<CategoryResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response == null) ? null : response.Category;
        }

        #region Znode Version 8.0
       
        //Gets the categories associated with a specific catalog.
        public CatalogAssociatedCategoriesListModel GetCategoryByCatalogIdFromCustomService(int catalogId, FilterCollection filters=null,
                                                            SortCollection sorts=null, int? pageIndex=1, int? pageSize=10)
        {
            return catalogId > 0 ? GetCategoryByIdCustomService(catalogId, filters,sorts, pageIndex, pageSize) : null;
        }

        private CatalogAssociatedCategoriesListModel GetCategoryByIdCustomService(int catalogId, FilterCollection filters,
                                                            SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = CategoriesEndpoint.GetCategoriesByCatalogId(catalogId);
            endpoint += BuildEndpointQueryString(null, filters,sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.CatalogAssociatedCategories;
        }

        public CatalogAssociatedCategoriesListModel GetAllCategories(int catalogId, string categoryName, FilterCollection filters,
                                                            SortCollection sorts, int? pageIndex, int? pageSize)
        {
            categoryName = categoryName + "_" + catalogId.ToString();
            var endpoint = CategoriesEndpoint.GetAllCategories(categoryName);
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<CategoryListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.CatalogAssociatedCategories;
        }

        #endregion
    }
}
