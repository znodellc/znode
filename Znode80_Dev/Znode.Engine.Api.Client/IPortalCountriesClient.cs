﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPortalCountriesClient : IBaseClient
	{
		PortalCountryModel GetPortalCountry(int portalCountryId);
		PortalCountryModel GetPortalCountry(int portalCountryId, ExpandCollection expands);
		PortalCountryListModel GetPortalCountries(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		PortalCountryListModel GetPortalCountries(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		PortalCountryModel CreatePortalCountry(PortalCountryModel model);
		PortalCountryModel UpdatePortalCountry(int portalCountryId, PortalCountryModel model);
		bool DeletePortalCountry(int portalCountryId);
	}
}
