﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using System.Data;
using System;

namespace Znode.Engine.Api.Client
{
    public class PortalsClient : BaseClient, IPortalsClient
    {
        public PortalModel GetPortal(int portalId)
        {
            return GetPortal(portalId, null);
        }

        public PortalModel GetPortal(int portalId, ExpandCollection expands)
        {
            var endpoint = PortalsEndpoint.Get(portalId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PortalResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.Portal;
        }

        public PortalListModel GetPortals(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetPortals(expands, filters, sorts, null, null);
        }

        public PortalListModel GetPortals(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = PortalsEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PortalListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new PortalListModel { Portals = (response == null) ? null : response.Portals };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public PortalListModel GetPortalsByPortalIds(string portalIds, ExpandCollection expands)
        {
            var endpoint = PortalsEndpoint.ListByPortalIds(portalIds);
            endpoint += BuildEndpointQueryString(expands, null, null, null, null);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PortalListResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            var list = new PortalListModel { Portals = response.Portals };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public PortalModel CreatePortal(PortalModel model)
        {
            var endpoint = PortalsEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<PortalResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (response == null) ? null : response.Portal;
        }

        public PortalModel UpdatePortal(int portalId, PortalModel model)
        {
            var endpoint = PortalsEndpoint.Update(portalId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<PortalResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response == null) ? null : response.Portal;
        }

        public bool DeletePortal(int portalId)
        {
            var endpoint = PortalsEndpoint.Delete(portalId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<PortalResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        #region Znode Version 8.0
        public DataSet GetFedexKeys()
        {
            var endpoint = PortalsEndpoint.GetFedexKeys();
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<FedexKeysResponse>(endpoint, status);
            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            DataSet ds = response.fedexkeys;
            return ds;
        }

        public void CreateMessage(int portalId, int localeId)
        {
            var endpoint = PortalsEndpoint.CreateMessage(portalId, localeId);
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PortalResponse>(endpoint, status);
            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
        }

        public bool CopyStore(int portalId)
        {
            var endpoint = PortalsEndpoint.CopyStore(portalId);
            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<PortalResponse>(endpoint, status);
            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            return response.IsStoreCopied;
        }

        public bool DeletePortalByPortalId(int portalId)
        {
            try
            {
                var endpoint = PortalsEndpoint.DeletePortalByPortalId(portalId);

                var status = new ApiStatus();
                var deleted = DeleteResourceFromEndpoint<PortalResponse>(endpoint, status);

                CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

                return deleted;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}
