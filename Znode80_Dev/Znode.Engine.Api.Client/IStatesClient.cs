﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IStatesClient : IBaseClient
	{
		StateModel GetState(string stateCode);
		StateListModel GetStates(FilterCollection filters, SortCollection sorts);
		StateListModel GetStates(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		StateModel CreateState(StateModel model);
		StateModel UpdateState(string stateCode, StateModel model);
		bool DeleteState(string stateCode);
	}
}
