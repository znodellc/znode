﻿using System;
using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class AccountsClient : BaseClient, IAccountsClient
	{
		public AccountModel GetAccount(int accountId)
		{
			return GetAccount(accountId, null);
		}

		public AccountModel GetAccount(int accountId, ExpandCollection expands)
		{
			var endpoint = AccountsEndpoint.Get(accountId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<AccountResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Account;
		}

		public AccountModel GetAccountByUser(Guid userId)
		{
			return GetAccountByUser(userId, null);
		}

		public AccountModel GetAccountByUser(Guid userId, ExpandCollection expands)
		{
			var endpoint = AccountsEndpoint.GetByUserId(userId.ToString());
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<AccountResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Account;
		}

		public AccountModel GetAccountByUser(string username)
		{
			return GetAccountByUser(username, null);
		}

		public AccountModel GetAccountByUser(string username, ExpandCollection expands)
		{
			var endpoint = AccountsEndpoint.GetByUsername(username);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<AccountResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Account;
		}

		public AccountListModel GetAccounts(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetAccounts(expands, filters, sorts, null, null);
		}

		public AccountListModel GetAccounts(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = AccountsEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<AccountListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new AccountListModel { Accounts = (response == null) ? null : response.Accounts };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public AccountModel CreateAccount(AccountModel model)
		{
			var endpoint = AccountsEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.Account;
		}

		public AccountModel UpdateAccount(int accountId, AccountModel model)
		{
			var endpoint = AccountsEndpoint.Update(accountId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.Account;
		}

		public bool DeleteAccount(int accountId)
		{
			var endpoint = AccountsEndpoint.Delete(accountId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<AccountResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}

		public AccountModel Login(AccountModel model)
		{
			return Login(model, null);
		}

		public AccountModel Login(AccountModel model, ExpandCollection expands)
		{
			var endpoint = AccountsEndpoint.Login();
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.Unauthorized };
			CheckStatusAndThrow<ZnodeUnauthorizedException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Account;
		}

		public AccountModel ResetPassword(AccountModel model)
		{
			var endpoint = AccountsEndpoint.ResetPassword();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.Account;
		}

		public AccountModel ChangePassword(AccountModel model)
		{
			var endpoint = AccountsEndpoint.ChangePassword();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.Account;
		}
        #region Znode Version 7.2.2
        //Function checks reset password link status.
        public AccountModel CheckResetPasswordLinkStatus(AccountModel model)
        {
            var endpoint = AccountsEndpoint.CheckResetPasswordLinkStatus();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response == null) ? null : response.Account;
        }
        #endregion

        #region Znode Version 8.0
        //Function checks for the User Role.
        public AccountModel CheckUserRole(AccountModel model)
        {
            var endpoint = AccountsEndpoint.CheckUserRole();
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response == null) ? null : response.Account;
        }

        //Function used for Reset admin details for the first default login.
        public AccountModel ResetAdminDetails(AccountModel model)
        {
            var endpoint = AccountsEndpoint.ResetAdminDetails();
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<AccountResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (response == null) ? null : response.Account;
        }

        #endregion
    }
}

    