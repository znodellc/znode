﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IHighlightTypesClient : IBaseClient
	{
		HighlightTypeModel GetHighlightType(int highlightTypeId);
		HighlightTypeListModel GetHighlightTypes(FilterCollection filters, SortCollection sorts);
		HighlightTypeListModel GetHighlightTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		HighlightTypeModel CreateHighlightType(HighlightTypeModel model);
		HighlightTypeModel UpdateHighlightType(int highlightTypeId, HighlightTypeModel model);
		bool DeleteHighlightType(int highlightTypeId);
	}
}
