﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
	public interface IAddressesClient : IBaseClient
	{
		AddressModel GetAddress(int addressId, bool refreshCache = false);
		AddressListModel GetAddresses(FilterCollection filters, SortCollection sorts, bool refreshCache = false);
		AddressListModel GetAddresses(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize, bool refreshCache = false);
		AddressModel CreateAddress(AddressModel model);
		AddressModel UpdateAddress(int addressId, AddressModel model);
		bool DeleteAddress(int addressId);
	}
}
