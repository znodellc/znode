﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IContentPageClient : IBaseClient
    {
        ContentPageListModel GetContentPages(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
        ContentPageListModel GetContentPages(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        ContentPageModel CreateContentPage(ContentPageModel model);
        object CopyContentPage(ContentPageModel model);
        /// <summary>
        /// Adds a content page.
        /// </summary>
        /// <param name="model">Content page model.</param>
        /// <returns>Bool value if contentpage is added or not.</returns>
        bool AddPage(ContentPageModel model);
    }
}
