﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class WishListsClient : BaseClient, IWishListsClient
	{
		public WishListModel GetWishList(int wishlistId)
		{
			var endpoint = WishListsEnpoint.Get(wishlistId);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<WishListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.WishList;
		}

		public WishListListModel GetWishLists(FilterCollection filters, SortCollection sorts)
		{
			return GetWishLists(filters, sorts, null, null);
		}

		public WishListListModel GetWishLists(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = WishListsEnpoint.List();
			endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<WishListsListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new WishListListModel { WishLists = (response == null) ? null : response.WishLists };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public WishListModel CreateWishList(WishListModel model)
		{
			var endpoint = WishListsEnpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<WishListResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.WishList;
		}

		public WishListModel UpdateWishList(int wishlistId, WishListModel model)
		{
			var endpoint = WishListsEnpoint.Update(wishlistId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<WishListResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.WishList;
		}

		public bool DeleteWishList(int wishlistId)
		{
			var endpoint = WishListsEnpoint.Delete(wishlistId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<WishListResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
