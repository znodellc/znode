﻿using System;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IFacetsClient : IBaseClient
    {
        /// <summary>
        /// Method gets the Facet group details.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <returns>Returns facet group details.</returns>
        FacetGroupModel GetFacetGroup(int facetGroupId);

        /// <summary>
        /// Method gets the Facet group details.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <param name="expands"></param>
        /// <returns>Returns facet group details.</returns>
        FacetGroupModel GetFacetGroup(int facetGroupId, ExpandCollection expands);

        /// <summary>
        /// Method Returns the Facet Groups.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <returns>Returns the facet groups</returns>
        FacetGroupListModel GetFacetGroups(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Method Returns the Facet Groups.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>Returns the facet groups</returns>
        FacetGroupListModel GetFacetGroups(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Method Returns the Facet Control Types.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <returns>Returns Facet Control Types.</returns>
        FacetControlTypeListModel GetFacetControlTypes(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Method Creates the facet group.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return the facet group.</returns>
        FacetGroupModel CreateFacetGroup(FacetGroupModel model);

        /// <summary>
        /// Method Updates the Facet group.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <param name="model"></param>
        /// <returns>Returns Updated facet group.</returns>
        FacetGroupModel UpdateFacetGroup(int facetGroupId, FacetGroupModel model);

        /// <summary>
        /// Method Deletes the facet group based on Id.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <returns>Returns true or false.</returns>
        bool DeleteFacetGroup(int facetGroupId);

        /// <summary>
        /// Method Create the facet group category.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns true or false.</returns>
        bool InsertFacetGroupCategory(FacetGroupCategoryListModel model);

        /// <summary>
        /// Method Delete the associated facet group categories based on faced group Id.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <returns>Returns true or false.</returns>
        bool DeleteFacetGroupCategoryByFacetGroupId(int facetGroupId);

        /// <summary>
        /// Method Updates the facet group details.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return the updated facet group.</returns>
        FacetGroupModel ManageFacetGroup(int facetGroupId, FacetGroupModel model);

        /// <summary>
        /// Method Creates the facet.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return the facet model.</returns>
        FacetModel CreateFacet(FacetModel model);


        /// <summary>
        /// Method gets the Facet details.
        /// </summary>
        /// <param name="facetId"></param>
        /// <returns>Returns facet details.</returns>
        FacetModel GetFacet(int facetId);

        /// <summary>
        /// Method gets the Facet details.
        /// </summary>
        /// <param name="facetId"></param>
        /// <param name="expands"></param>
        /// <returns>Returns facet details.</returns>
        FacetModel GetFacet(int facetId, ExpandCollection expands);


        /// <summary>
        /// Method Updates the facet details.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return the updated facet</returns>
        FacetModel EditFacet(int facetId, FacetModel model);

        /// <summary>
        /// Method Deletes the facet details.
        /// </summary>
        /// <param name="facetId"></param>
        /// <returns>Return true or false</returns>
        bool DeleteFacet(int facetId);
    }
}
