﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
	public interface IReviewsClient : IBaseClient
	{
		ReviewModel GetReview(int reviewId);
		ReviewModel GetReview(int reviewId, ExpandCollection expands);
		ReviewListModel GetReviews(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		ReviewListModel GetReviews(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		ReviewModel CreateReview(ReviewModel model);
		ReviewModel UpdateReview(int reviewId, ReviewModel model);
		bool DeleteReview(int reviewId);
	}
}
