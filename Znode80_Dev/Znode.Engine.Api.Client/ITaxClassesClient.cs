﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ITaxClassesClient : IBaseClient
	{
		TaxClassModel GetTaxClass(int taxClassId);
		TaxClassListModel GetTaxClasses(FilterCollection filters, SortCollection sorts);
		TaxClassListModel GetTaxClasses(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		TaxClassModel CreateTaxClass(TaxClassModel model);
		TaxClassModel UpdateTaxClass(int taxClassId, TaxClassModel model);
		bool DeleteTaxClass(int taxClassId);

        /// <summary>
        /// Znode Version 8.0
        /// To  Get Active Tax Class By PortalId
        /// </summary>
        /// <param name="portalId">int portalId</param>
        /// <returns>List of Tax Classes</returns>
        TaxClassListModel GetActiveTaxClassByPortalId(int portalId);
	}
}
