﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class ShippingOptionsClient : BaseClient, IShippingOptionsClient
	{
		public ShippingOptionModel GetShippingOption(int shippingOptionId)
		{
			return GetShippingOption(shippingOptionId, null);
		}

		public ShippingOptionModel GetShippingOption(int shippingOptionId, ExpandCollection expands)
		{
			var endpoint = ShippingOptionsEndpoint.Get(shippingOptionId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ShippingOptionResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.ShippingOption;
		}

		public ShippingOptionListModel GetShippingOptions(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetShippingOptions(expands, filters, sorts, null, null);
		}

		public ShippingOptionListModel GetShippingOptions(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = ShippingOptionsEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ShippingOptionListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new ShippingOptionListModel {ShippingOptions = (response == null) ?  null :  response.ShippingOptions};
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public ShippingOptionModel CreateShippingOption(ShippingOptionModel model)
		{
			var endpoint = ShippingOptionsEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<ShippingOptionResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.ShippingOption;
		}

		public ShippingOptionModel UpdateShippingOption(int shippingOptionId, ShippingOptionModel model)
		{
			var endpoint = ShippingOptionsEndpoint.Update(shippingOptionId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<ShippingOptionResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.ShippingOption;
		}

		public bool DeleteShippingOption(int shippingOptionId)
		{
			var endpoint = ShippingOptionsEndpoint.Delete(shippingOptionId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<ShippingOptionResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
