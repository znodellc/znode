﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class CategoryNodesClient : BaseClient, ICategoryNodesClient
	{
		public CategoryNodeModel GetCategoryNode(int categoryNodeId)
		{
			return GetCategoryNode(categoryNodeId, null);
		}

		public CategoryNodeModel GetCategoryNode(int categoryNodeId, ExpandCollection expands)
		{
			var endpoint = CategoryNodesEndpoint.Get(categoryNodeId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<CategoryNodeResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.CategoryNode;
		}

		public CategoryNodeListModel GetCategoryNodes(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetCategoryNodes(expands, filters, sorts, null, null);
		}

		public CategoryNodeListModel GetCategoryNodes(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = CategoryNodesEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<CategoryNodeListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new CategoryNodeListModel { CategoryNodes = (response == null) ? null : response.CategoryNodes };
			list.MapPagingDataFromResponse(response);

			return list;
		}
        
        public CategoryNodeModel CreateCategoryNode(CategoryNodeModel model)
		{
			var endpoint = CategoryNodesEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<CategoryNodeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.CategoryNode;
		}

		public CategoryNodeModel UpdateCategoryNode(int categoryNodeId, CategoryNodeModel model)
		{
			var endpoint = CategoryNodesEndpoint.Update(categoryNodeId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<CategoryNodeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.CategoryNode;
		}

		public bool DeleteCategoryNode(int categoryNodeId)
		{
			var endpoint = CategoryNodesEndpoint.Delete(categoryNodeId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<CategoryNodeResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
