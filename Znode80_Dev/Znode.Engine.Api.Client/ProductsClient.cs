﻿using System.Collections.ObjectModel;
using System.Net;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class ProductsClient : BaseClient, IProductsClient
	{
		public ProductsClient() { }

		public ProductsClient(int accountId)
		{
			AccountId = accountId;
		}

		public ProductModel GetProduct(int productId)
		{
			return GetProduct(productId, null);
		}

		public ProductModel GetProduct(int productId, ExpandCollection expands)
		{
			var endpoint = ProductsEndpoint.Get(productId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ProductResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Product;
		}

		public ProductModel GetProductWithSku(int productId, int skuId)
		{
			return GetProductWithSku(productId, skuId, null);
		}

		public ProductModel GetProductWithSku(int productId, int skuId, ExpandCollection expands)
		{
			var endpoint = ProductsEndpoint.GetWithSku(productId, skuId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ProductResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Product;
		}

		public ProductListModel GetProducts(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetProducts(expands, filters, sorts, null, null);
		}

		public ProductListModel GetProducts(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = ProductsEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new ProductListModel { Products = (response == null) ? null : response.Products };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public ProductListModel GetProductsByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetProductsByCatalog(catalogId, expands, filters, sorts, null, null);
		}

		public ProductListModel GetProductsByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = ProductsEndpoint.ListByCatalog(catalogId);
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new ProductListModel { Products = (response == null) ? null : response.Products };
			list.MapPagingDataFromResponse(response);

			return list;
		}

        public ProductListModel GetProductsByCatalogIds(string catalogIds, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.ListByCatalogIds(catalogIds);
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (response == null) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        
        public ProductListModel GetProductsByCategory(int categoryId, ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetProductsByCategory(categoryId, expands, filters, sorts, null, null);
		}

		public ProductListModel GetProductsByCategory(int categoryId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = ProductsEndpoint.ListByCategory(categoryId);
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new ProductListModel { Products = (response == null) ? null : response.Products };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public ProductListModel GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetProductsByCategoryByPromotionType(categoryId, promotionTypeId, expands, filters, sorts, null, null);
		}

		public ProductListModel GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = ProductsEndpoint.ListByCategoryByPromotionType(categoryId, promotionTypeId);
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new ProductListModel { Products = (response == null) ? null : response.Products };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public ProductListModel GetProductsByExternalIds(string externalIds, ExpandCollection expands, SortCollection sorts)
		{
			var endpoint = ProductsEndpoint.ListByExternalIds(externalIds);
			endpoint += BuildEndpointQueryString(expands, null, sorts, null, null);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new ProductListModel { Products = (response == null) ? null : response.Products };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public ProductListModel GetProductsByHomeSpecials(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = ProductsEndpoint.ListByHomeSpecials();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new ProductListModel { Products = (response == null) ? null : response.Products };
			list.MapPagingDataFromResponse(response);

			return list;
		}

        public async Task<ProductListModel> GetProductsByHomeSpecialsAsync(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.ListByHomeSpecials();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = await GetResourceFromEndpointAsync<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (response == null) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

		public ProductListModel GetProductsByProductIds(string productIds, ExpandCollection expands, SortCollection sorts)
		{
			var endpoint = ProductsEndpoint.ListByProductIds(productIds);
			endpoint += BuildEndpointQueryString(expands, null, sorts, null, null);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new ProductListModel { Products = (response == null) ? null : response.Products };
			list.MapPagingDataFromResponse(response);

			return list;
		}

        public async Task<ProductListModel> GetProductsByProductIdsAsync(string productIds, int accountId, ExpandCollection expands, SortCollection sorts)
        {
            AccountId = accountId;
            var endpoint = ProductsEndpoint.ListByProductIds(productIds);
            endpoint += BuildEndpointQueryString(expands, null, sorts, null, null);

            var status = new ApiStatus();
            var response = await GetResourceFromEndpointAsync<ProductListResponse>(endpoint, status);
            
            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (response == null) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        public ProductListModel GetProductsByProductIds(string productIds, ExpandCollection expands, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductsEndpoint.ListByProductIds(productIds);
            endpoint += BuildEndpointQueryString(expands, null, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ProductListModel { Products = (response == null) ? null : response.Products };
            list.MapPagingDataFromResponse(response);

            return list;
        }
        
        public ProductListModel GetProductsByPromotionType(int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetProductsByPromotionType(promotionTypeId, expands, filters, sorts, null, null);
		}

		public ProductListModel GetProductsByPromotionType(int promotionTypeId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = ProductsEndpoint.ListByPromotionType(promotionTypeId);
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new ProductListModel { Products = (response == null) ? null : response.Products };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public ProductListModel GetProductsByQuery(string query)
		{
			var endpoint = ProductsEndpoint.ListByQuery(query);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new ProductListModel { Products = (response == null) ? null : response.Products };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public ProductModel CreateProduct(ProductModel model)
		{
			var endpoint = ProductsEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<ProductResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.Product;
		}

		public ProductModel UpdateProduct(int productId, ProductModel model)
		{
			var endpoint = ProductsEndpoint.Update(productId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<ProductResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.Product;
		}

		public bool DeleteProduct(int productId)
		{
			var endpoint = ProductsEndpoint.Delete(productId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<ProductResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}


        #region Znode Version 8.0
        public ProductTagsModel GetProductTags(int productId)
		{
            return GetProductTags(productId, null);
		}

        public ProductTagsModel GetProductTags(int productId, ExpandCollection expands)
		{
			var endpoint = ProductsEndpoint.GetProductTags(productId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ProductTagResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (Equals(response, null)) ? null : response.ProductTag;
		}
        
        public ProductTagsModel CreateProductTags(ProductTagsModel model)
        {
            var endpoint = ProductsEndpoint.CreateProductTag();
            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ProductTagResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response,null)) ? null : response.ProductTag;
        }

        public ProductTagsModel UpdateProductTags(int tagId, ProductTagsModel model)
        {
            var endpoint = ProductsEndpoint.UpdateProductTag(tagId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ProductTagResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (Equals(response, null)) ? null : response.ProductTag;
        }

        public bool DeleteProductTags(int tagId)
        {
            var endpoint = ProductsEndpoint.DeleteProductTag(tagId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProductTagResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public ProductFacetModel GetAssociatedFacets(int productId)
        {
            return GetAssociatedFacets(productId, null);
        }

        public ProductFacetModel GetAssociatedFacets(int productId, ExpandCollection expands)
        {
            var endpoint = ProductsEndpoint.GetProductAssociatedFacets(productId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductFacetsResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (Equals(response, null)) ? null : response.ProductFacets;
        }

        public bool BindAssociatedFacets(ProductFacetModel model)
        {
            var endpoint = ProductsEndpoint.UpdateProductFacets(model.ProductId);

            var status = new ApiStatus();
            var response = PutResourceToEndpoint<ProductFacetsResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

            return (!Equals(response, null));
        }
        

        /// <summary>
        /// 
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public ProductListModel GetProductDetailsByProductId(int productId)
        {
            var endpoint = ProductsEndpoint.GetProductDetailsByProductId(productId);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.ProductList;
        }
        #endregion
    }
}
