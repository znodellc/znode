﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class ShoppingCartsEndpoint : BaseEndpoint
	{
        public static string GetByAccount(int accountId)
        {
            return String.Format("{0}/shoppingcarts/account/{1}", ApiRoot,accountId);
        }

        public static string GetByCookie(int cookieId)
        {
            return String.Format("{0}/shoppingcarts/cookie/{1}", ApiRoot, cookieId);
        }

		public static string Create()
		{
			return String.Format("{0}/shoppingcarts", ApiRoot);
		}

		public static string Calculate()
		{
			return String.Format("{0}/shoppingcarts/calculate", ApiRoot);
		}
	}
}
