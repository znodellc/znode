﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class StatesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/states", ApiRoot);
		}

		public static string Delete(string stateCode)
		{
			return String.Format("{0}/states/{1}", ApiRoot, stateCode);
		}

		public static string Get(string stateCode)
		{
			return String.Format("{0}/states/{1}", ApiRoot, stateCode);
		}

		public static string List()
		{
			return String.Format("{0}/states", ApiRoot);
		}

		public static string Update(string stateCode)
		{
			return String.Format("{0}/states/{1}", ApiRoot, stateCode);
		}
	}
}
