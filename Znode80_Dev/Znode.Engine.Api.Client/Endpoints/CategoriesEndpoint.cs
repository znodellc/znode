﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class CategoriesEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/categories", ApiRoot);
        }

        public static string Delete(int categoryId)
        {
            return String.Format("{0}/categories/{1}", ApiRoot, categoryId);
        }

        public static string Get(int categoryId)
        {
            return String.Format("{0}/categories/{1}", ApiRoot, categoryId);
        }

        public static string List()
        {
            return String.Format("{0}/categories", ApiRoot);
        }

        public static string ListByCatalog(int catalogId)
        {
            return String.Format("{0}/categories/catalog/{1}", ApiRoot, catalogId);
        }

        public static string ListByCatalogIds(string catalogIds)
        {
            return String.Format("{0}/categories/catalog/{1}", ApiRoot, catalogIds);
        }

        public static string ListByCategoryIds(string categoryIds)
        {
            return String.Format("{0}/categories/{1}", ApiRoot, categoryIds);
        }

        public static string Update(int categoryId)
        {
            return String.Format("{0}/categories/{1}", ApiRoot, categoryId);
        }

        public static string GetCategoriesByCatalogId(int catalogId)
        {
            return String.Format("{0}/categories/getcategoriesbycatalogid/{1}", ApiRoot, catalogId);
        }

        public static string GetAllCategories(string categoryName)
        {
            return String.Format("{0}/categories/getallcategories/{1}", ApiRoot, categoryName);           
        }
    }
}
