﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class AccountsEndpoint : BaseEndpoint
	{
		public static string ChangePassword()
		{
			return String.Format("{0}/accounts/changepassword", ApiRoot);
		}

		public static string Create()
		{
			return String.Format("{0}/accounts", ApiRoot);
		}

		public static string Delete(int accountId)
		{
			return String.Format("{0}/accounts/{1}", ApiRoot, accountId);
		}

		public static string Get(int accountId)
		{
			return String.Format("{0}/accounts/{1}", ApiRoot, accountId);
		}

		public static string GetByUserId(string userId)
		{
			return String.Format("{0}/accounts/{1}", ApiRoot, userId);
		}

        public static string GetByUsername(string username)
        {
            return String.Format("{0}/accounts/{1}", ApiRoot, username);
        }

		public static string List()
		{
			return String.Format("{0}/accounts", ApiRoot);
		}

		public static string Login()
		{
			return String.Format("{0}/accounts/login", ApiRoot);
		}

		public static string ResetPassword()
		{
			return String.Format("{0}/accounts/resetpassword", ApiRoot);
		}

		public static string Update(int accountId)
		{
			return String.Format("{0}/accounts/{1}", ApiRoot, accountId);
		}

        public static string CheckResetPasswordLinkStatus()
        {
            return String.Format("{0}/accounts/checkresetpasswordlinkstatus", ApiRoot);
        }

        public static string CheckUserRole()
        {
            return String.Format("{0}/accounts/checkuserrole", ApiRoot);
        }
        public static string ResetAdminDetails()
        {
            return String.Format("{0}/accounts/resetadmindetails", ApiRoot);
        }
	}
}
