﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ProfilesEndpoint : BaseEndpoint
    {
        public static string Get(int profileId)
        {
            return String.Format("{0}/profiles/{1}", ApiRoot, profileId);
        }

        public static string List()
        {
            return String.Format("{0}/profiles", ApiRoot);
        }

        public static string Create()
        {
            return String.Format("{0}/profiles", ApiRoot);
        }

        public static string Delete(int profileId)
        {
            return String.Format("{0}/profiles/{1}", ApiRoot, profileId);
        }

        public static string Update(int profileId)
        {
            return String.Format("{0}/profiles/{1}", ApiRoot, profileId);
        }
    }
}
