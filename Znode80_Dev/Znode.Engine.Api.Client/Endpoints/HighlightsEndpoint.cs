﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class HighlightsEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/highlights", ApiRoot);
		}

		public static string Delete(int highlightId)
		{
			return String.Format("{0}/highlights/{1}", ApiRoot, highlightId);
		}

		public static string Get(int highlightId)
		{
			return String.Format("{0}/highlights/{1}", ApiRoot, highlightId);
		}

		public static string List()
		{
			return String.Format("{0}/highlights", ApiRoot);
		}

		public static string Update(int highlightId)
		{
			return String.Format("{0}/highlights/{1}", ApiRoot, highlightId);
		}

        public static string CheckAssociatedProduct(int highlightId)
        {
            return String.Format("{0}/highlights/checkassociatedproduct/{1}", ApiRoot, highlightId);
        }
	}
}
