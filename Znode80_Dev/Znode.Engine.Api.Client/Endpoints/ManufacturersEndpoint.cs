﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class ManufacturersEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/manufacturers", ApiRoot);
		}

		public static string Delete(int manufacturerId)
		{
			return String.Format("{0}/manufacturers/{1}", ApiRoot, manufacturerId);
		}

		public static string Get(int manufacturerId)
		{
			return String.Format("{0}/manufacturers/{1}", ApiRoot, manufacturerId);
		}

		public static string List()
		{
			return String.Format("{0}/manufacturers", ApiRoot);
		}

		public static string Update(int manufacturerId)
		{
			return String.Format("{0}/manufacturers/{1}", ApiRoot, manufacturerId);
		}
	}
}
