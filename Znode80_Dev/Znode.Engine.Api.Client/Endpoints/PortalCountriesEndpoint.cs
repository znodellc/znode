﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class PortalCountriesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/portalcountries", ApiRoot);
		}

		public static string Delete(int portalCountryId)
		{
			return String.Format("{0}/portalcountries/{1}", ApiRoot, portalCountryId);
		}

		public static string Get(int portalCountryId)
		{
			return String.Format("{0}/portalcountries/{1}", ApiRoot, portalCountryId);
		}

		public static string List()
		{
			return String.Format("{0}/portalcountries", ApiRoot);
		}

		public static string Update(int portalCountryId)
		{
			return String.Format("{0}/portalcountries/{1}", ApiRoot, portalCountryId);
		}
	}
}
