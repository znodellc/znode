﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class ContentPageEndpoint:BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/contentpages", ApiRoot);
        }
        public static string Create()
        {
            return String.Format("{0}/contentpages", ApiRoot);
        }
        public static string CopyContentPage()
        {
            return String.Format("{0}/copycontentpage", ApiRoot);
        }
        public static string AddPage()
        {
            return String.Format("{0}/addpage", ApiRoot);
        }
    }
}
