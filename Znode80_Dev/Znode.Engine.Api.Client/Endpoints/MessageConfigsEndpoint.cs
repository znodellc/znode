﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class MessageConfigsEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/messageconfigs", ApiRoot);
		}

		public static string Delete(int messageConfigId)
		{
			return String.Format("{0}/messageconfigs/{1}", ApiRoot, messageConfigId);
		}

		public static string Get(int messageConfigId)
		{
			return String.Format("{0}/messageconfigs/{1}", ApiRoot, messageConfigId);
		}

        public static string GetByKey(string key)
        {
            return String.Format("{0}/messageconfigs/Key/{1}", ApiRoot, key);
        }

		public static string List()
		{
			return String.Format("{0}/messageconfigs", ApiRoot);
		}

		public static string ListByKeys(string keys)
		{
			return String.Format("{0}/messageconfigs/keys/{1}", ApiRoot, keys);
		}

		public static string Update(int messageConfigId)
		{
			return String.Format("{0}/messageconfigs/{1}", ApiRoot, messageConfigId);
		}
	}
}
