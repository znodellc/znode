﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class OrderStateEndpoint : BaseEndpoint
    {
        public static string List()
        {
            return String.Format("{0}/orderstates", ApiRoot);
        }
    }
}
