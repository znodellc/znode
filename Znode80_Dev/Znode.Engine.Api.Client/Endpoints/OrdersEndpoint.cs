﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class OrdersEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/orders", ApiRoot);
		}

		public static string Get(int orderId)
		{
			return String.Format("{0}/orders/{1}", ApiRoot, orderId);
		}

		public static string List()
		{
			return String.Format("{0}/orders", ApiRoot);
		}

		public static string Update(int orderId)
		{
			return String.Format("{0}/orders/{1}", ApiRoot, orderId);
		}
	}
}
