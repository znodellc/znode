﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class ShippingRuleTypesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/shippingruletypes", ApiRoot);
		}

		public static string Delete(int shippingRuleTypeId)
		{
			return String.Format("{0}/shippingruletypes/{1}", ApiRoot, shippingRuleTypeId);
		}

		public static string Get(int shippingRuleTypeId)
		{
			return String.Format("{0}/shippingruletypes/{1}", ApiRoot, shippingRuleTypeId);
		}

		public static string List()
		{
			return String.Format("{0}/shippingruletypes", ApiRoot);
		}

		public static string Update(int shippingRuleTypeId)
		{
			return String.Format("{0}/shippingruletypes/{1}", ApiRoot, shippingRuleTypeId);
		}
	}
}
