﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class CategoryNodesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/categorynodes", ApiRoot);
		}

		public static string Delete(int categoryNodeId)
		{
			return String.Format("{0}/categorynodes/{1}", ApiRoot, categoryNodeId);
		}

		public static string Get(int categoryNodeId)
		{
			return String.Format("{0}/categorynodes/{1}", ApiRoot, categoryNodeId);
		}

		public static string List()
		{
			return String.Format("{0}/categorynodes", ApiRoot);
		}

		public static string Update(int categoryNodeId)
		{
			return String.Format("{0}/categorynodes/{1}", ApiRoot, categoryNodeId);
		}
	}
}
