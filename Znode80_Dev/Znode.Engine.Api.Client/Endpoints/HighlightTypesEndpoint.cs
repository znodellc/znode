﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class HighlightTypesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/highlighttypes", ApiRoot);
		}

		public static string Delete(int highlightTypeId)
		{
			return String.Format("{0}/highlighttypes/{1}", ApiRoot, highlightTypeId);
		}

		public static string Get(int highlightTypeId)
		{
			return String.Format("{0}/highlighttypes/{1}", ApiRoot, highlightTypeId);
		}

		public static string List()
		{
			return String.Format("{0}/highlighttypes", ApiRoot);
		}

		public static string Update(int highlightTypeId)
		{
			return String.Format("{0}/highlighttypes/{1}", ApiRoot, highlightTypeId);
		}
	}
}
