﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class ShippingOptionsEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/shippingoptions", ApiRoot);
		}

		public static string Delete(int shippingOptionId)
		{
			return String.Format("{0}/shippingoptions/{1}", ApiRoot, shippingOptionId);
		}

		public static string Get(int shippingOptionId)
		{
			return String.Format("{0}/shippingoptions/{1}", ApiRoot, shippingOptionId);
		}

		public static string List()
		{
			return String.Format("{0}/shippingoptions", ApiRoot);
		}

		public static string Update(int shippingOptionId)
		{
			return String.Format("{0}/shippingoptions/{1}", ApiRoot, shippingOptionId);
		}
	}
}
