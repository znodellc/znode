﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class CountriesEndpoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/countries", ApiRoot);
        }

        public static string Delete(string countryCode)
        {
            return String.Format("{0}/countries/{1}", ApiRoot, countryCode);
        }

        public static string Get(string countryCode)
        {
            return String.Format("{0}/countries/{1}", ApiRoot, countryCode);
        }

        public static string List()
        {
            return String.Format("{0}/countries", ApiRoot);
        }

        public static string Update(string countryCode)
        {
            return String.Format("{0}/countries/{1}", ApiRoot, countryCode);
        }

        /// <summary>
        /// Znode Version 7.2.2
        /// This function Get all active countries for portalId.
        /// </summary>
        /// <param name="portalId">int portalId </param>
        /// <returns>returns end point url</returns>
        public static string GetActiveCountryByPortalId(int portalId)
        {
            return String.Format("{0}/countries/getactivecountrybyportalid/{1}", ApiRoot, portalId);
        }
    }
}
