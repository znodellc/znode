﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class ProductsEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/products", ApiRoot);
		}

		public static string Delete(int productId)
		{
			return String.Format("{0}/products/{1}", ApiRoot, productId);
		}

		public static string Get(int productId)
		{
			return String.Format("{0}/products/{1}", ApiRoot, productId);
		}

        public static string GetProductDetailsByProductId(int productId)
        {
            return String.Format("{0}/products/getproductdetailsbyproductid/{1}", ApiRoot, productId);
        }

		public static string GetWithSku(int productId, int skuId)
		{
			return String.Format("{0}/products/{1}/{2}", ApiRoot, productId, skuId);
		}

		public static string List()
		{
			return String.Format("{0}/products", ApiRoot);
		}

		public static string ListByCatalog(int catalogId)
		{
			return String.Format("{0}/products/catalog/{1}", ApiRoot, catalogId);
		}

        public static string ListByCatalogIds(string catalogIds)
        {
            return String.Format("{0}/products/catalog/{1}", ApiRoot, catalogIds);
        }
        
        public static string ListByCategory(int categoryId)
		{
			return String.Format("{0}/products/category/{1}", ApiRoot, categoryId);
		}

		public static string ListByCategoryByPromotionType(int categoryId, int promotionTypeId)
		{
			return String.Format("{0}/products/category/{1}/promotiontype/{2}", ApiRoot, categoryId, promotionTypeId);
		}

		public static string ListByExternalIds(string externalIds)
		{
			return String.Format("{0}/products/externalids/{1}", ApiRoot, externalIds);
		}

		public static string ListByHomeSpecials()
		{
			return String.Format("{0}/products/homespecials", ApiRoot);
		}

		public static string ListByProductIds(string productIds)
		{
			return String.Format("{0}/products/{1}", ApiRoot, productIds);
		}

		public static string ListByPromotionType(int promotionTypeId)
		{
			return String.Format("{0}/products/promotiontype/{1}", ApiRoot, promotionTypeId);
		}

		public static string Update(int productId)
		{
			return String.Format("{0}/products/{1}", ApiRoot, productId);
		}

        public static string GetProductTags(int productId)
        {
            return String.Format("{0}/producttags/{1}", ApiRoot, productId);
        }

        public static string CreateProductTag()
        {
            return String.Format("{0}/producttags", ApiRoot);
        }
        public static string UpdateProductTag(int tagId)
        {
            return String.Format("{0}/producttags/{1}", ApiRoot, tagId);
        }
        public static string DeleteProductTag(int tagId)
        {
            return String.Format("{0}/producttags/{1}", ApiRoot, tagId);
        }
        public static string GetProductAssociatedFacets(int productId)
        {
            return String.Format("{0}/productfacets/{1}", ApiRoot, productId);
        }
        public static string UpdateProductFacets(int productId)
        {
            return String.Format("{0}/productfacets/{1}", ApiRoot, productId);
        }
	}
}
