﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client.Endpoints
{
    public class UrlRedirectEndPoint : BaseEndpoint
    {
        public static string Create()
        {
            return String.Format("{0}/urlredirects", ApiRoot);
        }

        public static string Delete(int domainId)
        {
            return String.Format("{0}/urlredirects/{1}", ApiRoot, domainId);
        }

        public static string Get(int domainId)
        {
            return String.Format("{0}/urlredirects/{1}", ApiRoot, domainId);
        }

        public static string List()
        {
            return String.Format("{0}/urlredirects", ApiRoot);
        }

        public static string Update(int domainId)
        {
            return String.Format("{0}/urlredirects/{1}", ApiRoot, domainId);
        }
    }
}
