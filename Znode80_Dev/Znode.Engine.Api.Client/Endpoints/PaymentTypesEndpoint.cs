﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class PaymentTypesEndpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/paymenttypes", ApiRoot);
		}

		public static string Delete(int paymentTypeId)
		{
			return String.Format("{0}/paymenttypes/{1}", ApiRoot, paymentTypeId);
		}

		public static string Get(int paymentTypeId)
		{
			return String.Format("{0}/paymenttypes/{1}", ApiRoot, paymentTypeId);
		}

		public static string List()
		{
			return String.Format("{0}/paymenttypes", ApiRoot);
		}

		public static string Update(int paymentTypeId)
		{
			return String.Format("{0}/paymenttypes/{1}", ApiRoot, paymentTypeId);
		}
	}
}
