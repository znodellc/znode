﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class WishListsEnpoint : BaseEndpoint
	{
		public static string Create()
		{
			return String.Format("{0}/wishlists", ApiRoot);
		}

		public static string Delete(int wishlistId)
		{
			return String.Format("{0}/wishlists/{1}", ApiRoot, wishlistId);
		}

		public static string Get(int wishListId)
		{
			return String.Format("{0}/wishlists/{1}", ApiRoot, wishListId);
		}

		public static string List()
		{
			return String.Format("{0}/wishlists", ApiRoot);
		}

		public static string Update(int wishListId)
		{
			return String.Format("{0}/wishlists/{1}", ApiRoot, wishListId);
		}
	}
}
