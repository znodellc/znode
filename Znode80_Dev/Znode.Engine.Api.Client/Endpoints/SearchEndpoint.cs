﻿using System;

namespace Znode.Engine.Api.Client.Endpoints
{
	public class SearchEndpoint : BaseEndpoint
	{
		public static string Keyword()
		{
			return String.Format("{0}/search/keyword", ApiRoot);
		}

		public static string Suggested()
		{
			return String.Format("{0}/search/suggested", ApiRoot);
		}
	}
}
