﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IEnvironmentConfigClient:IBaseClient
    {
        /// <summary>
        /// Gets EnvironmentConfig values.
        /// </summary>
        /// <returns>Returns model containing environmentConfig value.</returns>
        EnvironmentConfigModel GetEnvironmentConfig();
    }
}
