﻿using System.Data;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPortalsClient : IBaseClient
	{
		PortalModel GetPortal(int portalId);
		PortalModel GetPortal(int portalId, ExpandCollection expands);
		PortalListModel GetPortals(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		PortalListModel GetPortals(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        PortalListModel GetPortalsByPortalIds(string portalIds, ExpandCollection expands);
		PortalModel CreatePortal(PortalModel model);
		PortalModel UpdatePortal(int portalId, PortalModel model);
		bool DeletePortal(int portalId);
        /// <summary>
        /// Gets default fedex keys.
        /// </summary>
        /// <returns>Dataset of fedex keys.</returns>
        DataSet GetFedexKeys();

        /// <summary>
        /// Creates default messages for new portal.
        /// </summary>
        /// <param name="portalId">Portal id of the portal for which messages are to be created.</param>
        /// <param name="localeId">Locale id of the portal.</param>
        void CreateMessage(int portalId, int localeId);

        /// <summary>
        /// Copies a store.
        /// </summary>
        /// <param name="portalId">Portal ID of the portal which is to be copied.</param>
        /// <returns>Bool value if the store is copied or not.</returns>
        bool CopyStore(int portalId);       

        /// <summary>
        /// Deletes portal by portal id.
        /// </summary>
        /// <param name="portalId">Portal id of the portal to be deleted.</param>
        /// <returns>Bool value if the Portal is deleted or not.</returns>
        bool DeletePortalByPortalId(int portalId);
	}
}
