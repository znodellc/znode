﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ITaxRuleTypesClient : IBaseClient
	{
		TaxRuleTypeModel GetTaxRuleType(int taxRuleTypeId);
		TaxRuleTypeListModel GetTaxRuleTypes(FilterCollection filters, SortCollection sorts);
		TaxRuleTypeListModel GetTaxRuleTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		TaxRuleTypeModel CreateTaxRuleType(TaxRuleTypeModel model);
		TaxRuleTypeModel UpdateTaxRuleType(int taxRuleTypeId, TaxRuleTypeModel model);
		bool DeleteTaxRuleType(int taxRuleTypeId);
	}
}
