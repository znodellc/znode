﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IPromotionsClient : IBaseClient
	{
		PromotionModel GetPromotion(int promotionId);
		PromotionModel GetPromotion(int promotionId, ExpandCollection expands);
		PromotionListModel GetPromotions(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		PromotionListModel GetPromotions(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		PromotionModel CreatePromotion(PromotionModel model);
		PromotionModel UpdatePromotion(int promotionId, PromotionModel model);
		bool DeletePromotion(int promotionId);
	}
}
