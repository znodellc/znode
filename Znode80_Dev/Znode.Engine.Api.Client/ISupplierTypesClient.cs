﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ISupplierTypesClient : IBaseClient
	{
		SupplierTypeModel GetSupplierType(int supplierTypeId);
		SupplierTypeListModel GetSupplierTypes(FilterCollection filters, SortCollection sorts);
		SupplierTypeListModel GetSupplierTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		SupplierTypeModel CreateSupplierType(SupplierTypeModel model);
		SupplierTypeModel UpdateSupplierType(int supplierTypeId, SupplierTypeModel model);
		bool DeleteSupplierType(int supplierTypeId);
	}
}
