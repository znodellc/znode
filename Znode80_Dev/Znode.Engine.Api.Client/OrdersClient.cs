﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class OrdersClient : BaseClient, IOrdersClient
	{
		public OrderModel GetOrder(int orderId)
		{
			return GetOrder(orderId, null);
		}

		public OrderModel GetOrder(int orderId, ExpandCollection expands)
		{
			var endpoint = OrdersEndpoint.Get(orderId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<OrderResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Order;
		}

		public OrderListModel GetOrders(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetOrders(expands, filters, sorts, null, null);
		}

		public OrderListModel GetOrders(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = OrdersEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<OrderListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			var list = new OrderListModel { Orders = (response == null) ? null : response.Orders };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public OrderModel CreateOrder(ShoppingCartModel model)
		{
			var endpoint = OrdersEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<OrderResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.Created };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Order;
		}

		public OrderModel UpdateOrder(int orderId, OrderModel model)
		{
			var endpoint = OrdersEndpoint.Update(orderId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<OrderResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.Order;
		}
	}
}
