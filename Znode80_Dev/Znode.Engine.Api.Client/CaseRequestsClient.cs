﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class CaseRequestsClient : BaseClient, ICaseRequestsClient
	{
		public CaseRequestModel GetCaseRequest(int caseRequestId)
		{
			return GetCaseRequest(caseRequestId, null);
		}

		public CaseRequestModel GetCaseRequest(int caseRequestId, ExpandCollection expands)
		{
			var endpoint = CaseRequestsEndpoint.Get(caseRequestId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<CaseRequestResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.CaseRequest;
		}

		public CaseRequestListModel GetCaseRequests(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetCaseRequests(expands, filters, sorts, null, null);
		}

		public CaseRequestListModel GetCaseRequests(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = CaseRequestsEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<CaseRequestListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);
            
			var list = new CaseRequestListModel { CaseRequests = (response == null) ? null : response.CaseRequests };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public CaseRequestModel CreateCaseRequest(CaseRequestModel model)
		{
			var endpoint = CaseRequestsEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<CaseRequestResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.CaseRequest;
		}

		public CaseRequestModel UpdateCaseRequest(int caseRequestId, CaseRequestModel model)
		{
			var endpoint = CaseRequestsEndpoint.Update(caseRequestId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<CaseRequestResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.CaseRequest;
		}

		public bool DeleteCaseRequest(int caseRequestId)
		{
			var endpoint = CaseRequestsEndpoint.Delete(caseRequestId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<CaseRequestResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
