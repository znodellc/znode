﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Client
{
    public interface IBaseClient
    {

        int AccountId { get; set; }
        bool RefreshCache { get; set; }

    }
}
