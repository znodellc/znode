﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IShippingOptionsClient : IBaseClient
	{
		ShippingOptionModel GetShippingOption(int shippingOptionId);
		ShippingOptionModel GetShippingOption(int shippingOptionId, ExpandCollection expands);
		ShippingOptionListModel GetShippingOptions(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		ShippingOptionListModel GetShippingOptions(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		ShippingOptionModel CreateShippingOption(ShippingOptionModel model);
		ShippingOptionModel UpdateShippingOption(int shippingOptionId, ShippingOptionModel model);
		bool DeleteShippingOption(int shippingOptionId);
	}
}
