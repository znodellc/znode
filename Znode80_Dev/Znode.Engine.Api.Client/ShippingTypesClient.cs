﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class ShippingTypesClient : BaseClient, IShippingTypesClient
	{
		public ShippingTypeModel GetShippingType(int shippingTypeId)
		{
			var endpoint = ShippingTypesEndpoint.Get(shippingTypeId);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ShippingTypeResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.ShippingType;
		}

		public ShippingTypeListModel GetShippingTypes(FilterCollection filters, SortCollection sorts)
		{
			return GetShippingTypes(filters, sorts, null, null);
		}

		public ShippingTypeListModel GetShippingTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = ShippingTypesEndpoint.List();
			endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<ShippingTypeListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ShippingTypeListModel { ShippingTypes = (response == null) ? null : response.ShippingTypes };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public ShippingTypeModel CreateShippingType(ShippingTypeModel model)
		{
			var endpoint = ShippingTypesEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<ShippingTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.ShippingType;
		}

		public ShippingTypeModel UpdateShippingType(int shippingTypeId, ShippingTypeModel model)
		{
			var endpoint = ShippingTypesEndpoint.Update(shippingTypeId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<ShippingTypeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.ShippingType;
		}

		public bool DeleteShippingType(int shippingTypeId)
		{
			var endpoint = ShippingTypesEndpoint.Delete(shippingTypeId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<ShippingTypeResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
