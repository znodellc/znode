﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class InventoryClient : BaseClient, IInventoryClient
	{
		public InventoryModel GetInventory(int inventoryId)
		{
			return GetInventory(inventoryId, null);
		}

		public InventoryModel GetInventory(int inventoryId, ExpandCollection expands)
		{
			var endpoint = InventoryEndpoint.Get(inventoryId);
			endpoint += BuildEndpointQueryString(expands);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<InventoryResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Inventory;
		}

		public InventoryListModel GetInventories(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
		{
			return GetInventories(expands, filters, sorts, null, null);
		}

		public InventoryListModel GetInventories(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = InventoryEndpoint.List();
			endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<InventoryListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new InventoryListModel { Inventories = (response == null) ? null : response.Inventories };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public InventoryModel CreateInventory(InventoryModel model)
		{
			var endpoint = InventoryEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<InventoryResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.Inventory;
		}

		public InventoryModel UpdateInventory(int inventoryId, InventoryModel model)
		{
			var endpoint = InventoryEndpoint.Update(inventoryId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<InventoryResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.Inventory;
		}

		public bool DeleteInventory(int inventoryId)
		{
			var endpoint = InventoryEndpoint.Delete(inventoryId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<InventoryResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
