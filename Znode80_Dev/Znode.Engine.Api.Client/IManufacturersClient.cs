﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IManufacturersClient : IBaseClient
	{
		ManufacturerModel GetManufacturer(int manufacturerId);
		ManufacturerListModel GetManufacturers(FilterCollection filters, SortCollection sorts);
		ManufacturerListModel GetManufacturers(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		ManufacturerModel CreateManufacturer(ManufacturerModel model);
		ManufacturerModel UpdateManufacturer(int manufacturerId, ManufacturerModel model);
		bool DeleteManufacturer(int manufacturerId);
	}
}
