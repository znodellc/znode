﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Api.Models.Extensions;
using Newtonsoft.Json;

namespace Znode.Engine.Api.Client
{
    public class ContentPageClient:BaseClient,IContentPageClient
    {
        /// <summary>
        /// List of Content Pages.
        /// </summary>
        /// <param name="expands">Expand values for content pages.</param>
        /// <param name="filters">Filter values for content pages.</param>
        /// <param name="sorts">Sort parameters for content pages.</param>
        /// <returns>List of Content page models.</returns>
        public ContentPageListModel GetContentPages(ExpandCollection expands, FilterCollection filters, SortCollection sorts)
        {
            return GetContentPages(expands, filters, sorts, null, null);
        }

        /// <summary>
        /// List of content pages.
        /// </summary>
        /// <param name="expands">Expand values for content pages.</param>
        /// <param name="filters">Filter values for content pages.</param>
        /// <param name="sorts">Sort parameters for content pages.</param>
        /// <param name="pageIndex">Start page index of content pages.</param>
        /// <param name="pageSize">Page size of content page list.</param>
        /// <returns></returns>
        public ContentPageListModel GetContentPages(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ContentPageEndpoint.List();
            endpoint += BuildEndpointQueryString(expands, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ContentPageListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new ContentPageListModel { ContentPages = (response == null) ? null : response.ContentPages };
            list.MapPagingDataFromResponse(response);

            return list;
        }

        /// <summary>
        /// Creates a content page.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ContentPageModel CreateContentPage(ContentPageModel model)
        {
            var endpoint = ContentPageEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ContentPageResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.ContentPage;
        }

        /// <summary>
        /// Copies a content Page.
        /// </summary>
        /// <param name="model">Content Page model.</param>
        /// <returns>Object.</returns>
        public object CopyContentPage(ContentPageModel model)
        {
            var endpoint = ContentPageEndpoint.CopyContentPage();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ContentPageResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (Equals(response, null)) ? null : response.ContentPageCopy;
        }

       
        public bool AddPage(ContentPageModel model)
        {
            var endpoint = ContentPageEndpoint.AddPage();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ContentPageResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return response.IsContentPageAdded;
        }
    }
}
