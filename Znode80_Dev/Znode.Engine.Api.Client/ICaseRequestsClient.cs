﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ICaseRequestsClient : IBaseClient
	{
		CaseRequestModel GetCaseRequest(int caseRequestId);
		CaseRequestModel GetCaseRequest(int caseRequestId, ExpandCollection expands);
		CaseRequestListModel GetCaseRequests(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		CaseRequestListModel GetCaseRequests(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		CaseRequestModel CreateCaseRequest(CaseRequestModel model);
		CaseRequestModel UpdateCaseRequest(int caseRequestId, CaseRequestModel model);
		bool DeleteCaseRequest(int caseRequestId);
	}
}
