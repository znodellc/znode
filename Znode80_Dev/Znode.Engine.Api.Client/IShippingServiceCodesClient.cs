﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IShippingServiceCodesClient : IBaseClient
	{
		ShippingServiceCodeModel GetShippingServiceCode(int shippingServiceCodeId);
		ShippingServiceCodeModel GetShippingServiceCode(int shippingServiceCodeId, ExpandCollection expands);
		ShippingServiceCodeListModel GetShippingServiceCodes(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		ShippingServiceCodeListModel GetShippingServiceCodes(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		ShippingServiceCodeModel CreateShippingServiceCode(ShippingServiceCodeModel model);
		ShippingServiceCodeModel UpdateShippingServiceCode(int shippingServiceCodeId, ShippingServiceCodeModel model);
		bool DeleteShippingServiceCode(int shippingServiceCodeId);
	}
}
