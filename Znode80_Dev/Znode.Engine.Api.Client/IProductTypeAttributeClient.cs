﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IProductTypeAttributeClient 
    {
        /// <summary>
        /// Get Attribute Types by Attribute Id.
        /// </summary>
        /// <param name="attributeTypeId">Attribute Id.</param>
        /// <returns>Returns ProductTypeAttributeModel</returns>
        ProductTypeAttributeModel GetAttributeType(int attributeTypeId);

        /// <summary>
        /// Get List of Attribute Types by Product Type Attribute Id.
        /// </summary>
        /// <param name="ProductTypeAttributeId"></param>
        /// <param name="expands"></param>
        /// <returns>Returns ProductTypeAttributeModel</returns>
        ProductTypeAttributeModel GetAttributeType(int ProductTypeAttributeId, ExpandCollection expands);

        /// <summary>
        /// Create Attribute Type Associated With Product Type.
        /// </summary>
        /// <param name="model">ProductTypeAssociatedAttributeTypesModel</param>
        /// <returns>Returns ProductTypeAttributeModel</returns>
        ProductTypeAttributeModel CreateAttributeType(ProductTypeAssociatedAttributeTypesModel model);

        /// <summary>
        /// Delete Existing Attribute Type.
        /// </summary>
        /// <param name="attributeTypeId">Attribute Type Id</param>
        /// <returns>Returns bool.</returns>
        bool DeleteAttributeType(int attributeTypeId);

        /// <summary>
        /// Gets Attribute Types associated with specific Product Type.
        /// </summary>
        /// <param name="ProductTypeId">The Id of Product Type.</param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>Returns list of Attributes associated with a Product Type.</returns>
        ProductTypeAssociatedAttributeTypesListModel GetAttributeByProductTypeIdFromCustomService(int ProductTypeId, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
    }
}
