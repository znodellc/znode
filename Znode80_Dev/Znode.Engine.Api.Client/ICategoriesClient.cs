﻿using System.Collections.Generic;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ICategoriesClient : IBaseClient
	{
		CategoryModel GetCategory(int categoryId);
		CategoryModel GetCategory(int categoryId, ExpandCollection expands);
		CategoryListModel GetCategories(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		CategoryListModel GetCategories(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		CategoryListModel GetCategoriesByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		CategoryListModel GetCategoriesByCatalog(int catalogId, ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        CategoryListModel GetCategoriesByCatalogIds(string catalogIds, ExpandCollection expands,
                                                    FilterCollection filters, SortCollection sorts, int? pageIndex,
                                                    int? pageSize);
        CategoryListModel GetCategoriesByCategoryIds(string categoryIds, ExpandCollection expands,
                                                    FilterCollection filters, SortCollection sorts, int? pageIndex,
                                                    int? pageSize);
        CategoryModel CreateCategory(CategoryModel model);
		CategoryModel UpdateCategory(int categoryId, CategoryModel model);

        /// <summary>
        /// Znode Version 8.0
        /// Gets categories associated with specific catalog.
        /// </summary>
        /// <param name="catalogId">The Id of catalog.</param>
        /// <returns>Returns list of categories associated with a catalog.</returns>
        CatalogAssociatedCategoriesListModel GetCategoryByCatalogIdFromCustomService(int catalogId,  FilterCollection filters=null,
                                                            SortCollection sorts=null, int? pageIndex=1, int? pageSize=10);

        /// <summary>
        /// Znode Version 8.0
        /// Gets all categories.
        /// </summary>
        /// <param name="catalogId">The Id of the catalog.</param>
        /// <param name="categoryName">The name of category.</param>
        /// <param name="filters">Filter collcetion object.</param>
        /// <param name="sorts">Sort collection object.</param>
        /// <param name="pageIndex">The index of page.</param>
        /// <param name="pageSize">The size of page.</param>
        /// <returns>Returns List of all categories.</returns>
        CatalogAssociatedCategoriesListModel GetAllCategories(int catalogId, string categoryName, FilterCollection filters,
                                                            SortCollection sorts, int? pageIndex, int? pageSize);
	}
}
