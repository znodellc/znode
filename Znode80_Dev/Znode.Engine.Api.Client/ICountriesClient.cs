﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ICountriesClient : IBaseClient
	{
		CountryModel GetCountry(string countryCode);
		CountryModel GetCountry(string countryCode, ExpandCollection expands);
		CountryListModel GetCountries(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		CountryListModel GetCountries(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		CountryModel CreateCountry(CountryModel model);
		CountryModel UpdateCountry(string countryCode, CountryModel model);
		bool DeleteCountry(string countryCode);


        /// <summary>
        /// Znode Version 7.2.2
        /// This function Get all active countries for portalId.
        /// </summary>
        /// <param name="model">int portalId</param>
        /// <returns>Returns CountryListModel</returns>
        CountryListModel GetActiveCountryByPortalId(int portalId);
	}
}
