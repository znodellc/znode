﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IShoppingCartsClient : IBaseClient
	{
		ShoppingCartModel GetCartByAccount(int accountId);
		ShoppingCartModel GetCartByCookie(int cookieId);
		ShoppingCartModel CreateCart(ShoppingCartModel model);
		ShoppingCartModel Calculate(ShoppingCartModel model);
	}
}