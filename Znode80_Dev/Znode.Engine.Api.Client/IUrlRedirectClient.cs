﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IUrlRedirectClient : IBaseClient
    {
        UrlRedirectListModel Get301Redirects(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
    }
}
