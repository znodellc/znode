﻿using System.Collections.ObjectModel;
using System.Net;
using Newtonsoft.Json;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
	public class DomainsClient : BaseClient, IDomainsClient
	{
		public DomainModel GetDomain(int domainId)
		{
			var endpoint = DomainsEndpoint.Get(domainId);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<DomainResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

			return (response == null) ? null : response.Domain;
		}

		public DomainListModel GetDomains(FilterCollection filters, SortCollection sorts)
		{
			return GetDomains(filters, sorts, null, null);
		}

		public DomainListModel GetDomains(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize)
		{
			var endpoint = DomainsEndpoint.List();
			endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

			var status = new ApiStatus();
			var response = GetResourceFromEndpoint<DomainListResponse>(endpoint, status);

			var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
			CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            var list = new DomainListModel { Domains = (response == null) ? null : response.Domains };
			list.MapPagingDataFromResponse(response);

			return list;
		}

		public DomainModel CreateDomain(DomainModel model)
		{
			var endpoint = DomainsEndpoint.Create();

			var status = new ApiStatus();
			var response = PostResourceToEndpoint<DomainResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

			return (response == null) ? null : response.Domain;
		}

		public DomainModel UpdateDomain(int domainId, DomainModel model)
		{
			var endpoint = DomainsEndpoint.Update(domainId);

			var status = new ApiStatus();
			var response = PutResourceToEndpoint<DomainResponse>(endpoint, JsonConvert.SerializeObject(model), status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.OK);

			return (response == null) ? null : response.Domain;
		}

		public bool DeleteDomain(int domainId)
		{
			var endpoint = DomainsEndpoint.Delete(domainId);

			var status = new ApiStatus();
			var deleted = DeleteResourceFromEndpoint<DomainResponse>(endpoint, status);

			CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

			return deleted;
		}
	}
}
