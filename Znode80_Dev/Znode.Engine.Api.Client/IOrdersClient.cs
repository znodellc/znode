﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IOrdersClient : IBaseClient
	{
		OrderModel GetOrder(int orderId);
		OrderModel GetOrder(int orderId, ExpandCollection expands);
		OrderListModel GetOrders(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		OrderListModel GetOrders(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        OrderModel CreateOrder(ShoppingCartModel model);
		OrderModel UpdateOrder(int orderId, OrderModel model);
	}
}
