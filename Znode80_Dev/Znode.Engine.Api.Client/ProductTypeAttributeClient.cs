﻿using Newtonsoft.Json;
using System.Collections.ObjectModel;
using System.Net;
using Znode.Engine.Api.Client.Endpoints;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Client
{
    public class ProductTypeAttributeClient : BaseClient , IProductTypeAttributeClient
    {
        public ProductTypeAttributeModel GetAttributeType(int attributeTypeId)
        {
            return GetAttributeType(attributeTypeId, null);
        }

        public ProductTypeAttributeModel GetAttributeType(int ProductTypeAttributeId, ExpandCollection expands)
        {
            var endpoint = ProductTypeAttributeEndpoint.Get(ProductTypeAttributeId);
            endpoint += BuildEndpointQueryString(expands);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductTypeAttributeResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.ProductTypeAttribute;
        }

        public ProductTypeAttributeModel CreateAttributeType(ProductTypeAssociatedAttributeTypesModel model)
        {
            var endpoint = ProductTypeAttributeEndpoint.Create();

            var status = new ApiStatus();
            var response = PostResourceToEndpoint<ProductTypeAttributeResponse>(endpoint, JsonConvert.SerializeObject(model), status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.Created);

            return (response == null) ? null : response.ProductTypeAttribute;
        }

        public bool DeleteAttributeType(int attributeTypeId)
        {
            var endpoint = ProductTypeAttributeEndpoint.Delete(attributeTypeId);

            var status = new ApiStatus();
            var deleted = DeleteResourceFromEndpoint<ProductTypeAttributeResponse>(endpoint, status);

            CheckStatusAndThrow<ZnodeException>(status, HttpStatusCode.NoContent);

            return deleted;
        }

        public ProductTypeAssociatedAttributeTypesListModel GetAttributeByProductTypeIdFromCustomService(int ProductTypeId, FilterCollection filters,
                                                            SortCollection sorts, int? pageIndex, int? pageSize)
        {
            return ProductTypeId > 0 ? GetAttributeTypesByProductTypeIdService(ProductTypeId, filters, sorts, pageIndex, pageSize) : null;
        }

        public ProductTypeAssociatedAttributeTypesListModel GetAttributeTypesByProductTypeIdService(int ProductTypeId, FilterCollection filters,
                                                            SortCollection sorts, int? pageIndex, int? pageSize)
        {
            var endpoint = ProductTypeAttributeEndpoint.GetAttributeTypesByProductTypeId(ProductTypeId);
            endpoint += BuildEndpointQueryString(null, filters, sorts, pageIndex, pageSize);

            var status = new ApiStatus();
            var response = GetResourceFromEndpoint<ProductTypeAttributeListResponse>(endpoint, status);

            var expectedStatusCodes = new Collection<HttpStatusCode> { HttpStatusCode.OK, HttpStatusCode.NotFound };
            CheckStatusAndThrow<ZnodeException>(status, expectedStatusCodes);

            return (response == null) ? null : response.ProductTypeAssociatedAttributeTypes;
        }
    }
}
