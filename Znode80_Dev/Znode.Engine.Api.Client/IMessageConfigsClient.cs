﻿using System.Threading.Tasks;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IMessageConfigsClient : IBaseClient
	{
		MessageConfigModel GetMessageConfig(int messageConfigId);
		MessageConfigModel GetMessageConfig(int messageConfigId, ExpandCollection expands);
        MessageConfigModel GetMessageConfigByKey(string key, ExpandCollection expands);
		MessageConfigListModel GetMessageConfigs(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		MessageConfigListModel GetMessageConfigs(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		MessageConfigListModel GetMessageConfigsByKeys(string keys, ExpandCollection expands, SortCollection sorts);
		MessageConfigModel CreateMessageConfig(MessageConfigModel model);
		MessageConfigModel UpdateMessageConfig(int messageConfigId, MessageConfigModel model);
		bool DeleteMessageConfig(int messageConfigId);

        Task<MessageConfigModel> GetMessageConfigByKeyAsync(string key, ExpandCollection expands);
	}
}
