﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ITaxRulesClient : IBaseClient
	{
		TaxRuleModel GetTaxRule(int taxRuleId);
		TaxRuleModel GetTaxRule(int taxRuleId, ExpandCollection expands);
		TaxRuleListModel GetTaxRules(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		TaxRuleListModel GetTaxRules(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		TaxRuleModel CreateTaxRule(TaxRuleModel model);
		TaxRuleModel UpdateTaxRule(int taxRuleId, TaxRuleModel model);
		bool DeleteTaxRule(int taxRuleId);
	}
}
