﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IProductCategoriesClient : IBaseClient
	{
		ProductCategoryModel GetProductCategory(int productCategoryId);
		ProductCategoryModel GetProductCategory(int productCategoryId, ExpandCollection expands);
		ProductCategoryListModel GetProductCategories(ExpandCollection expands, FilterCollection filters, SortCollection sorts);
		ProductCategoryListModel GetProductCategories(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
        ProductCategoryListModel GetProductCategoriesByQuery(string query);
		ProductCategoryModel CreateProductCategory(ProductCategoryModel model);
		ProductCategoryModel UpdateProductCategory(int productCategoryId, ProductCategoryModel model);
		bool DeleteProductCategory(int productCategoryId);
	}
}
