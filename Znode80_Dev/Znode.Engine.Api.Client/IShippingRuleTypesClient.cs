﻿using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IShippingRuleTypesClient : IBaseClient
	{
		ShippingRuleTypeModel GetShippingRuleType(int shippingRuleTypeId);
		ShippingRuleTypeListModel GetShippingRuleTypes(FilterCollection filters, SortCollection sorts);
		ShippingRuleTypeListModel GetShippingRuleTypes(FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);
		ShippingRuleTypeModel CreateShippingRuleType(ShippingRuleTypeModel model);
		ShippingRuleTypeModel UpdateShippingRuleType(int shippingRuleTypeId, ShippingRuleTypeModel model);
		bool DeleteShippingRuleType(int shippingRuleTypeId);
	}
}
