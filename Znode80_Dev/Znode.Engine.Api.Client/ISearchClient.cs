﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface ISearchClient : IBaseClient
	{
		SuggestedSearchListModel GetTypeAheadResponse(SuggestedSearchModel model);
		KeywordSearchModel Search(KeywordSearchModel model, ExpandCollection expands, SortCollection sort);
	}
}
