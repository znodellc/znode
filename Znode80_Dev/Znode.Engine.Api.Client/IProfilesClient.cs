﻿using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Client
{
    public interface IProfilesClient : IBaseClient
    {
        /// <summary>
        /// Method gets the Profile details.
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns>Returns profile details.</returns>
        ProfileModel GetProfile(int profileId);

        /// <summary>
        /// Method gets the Profile details.
        /// </summary>
        /// <param name="profileId"></param>
        /// <param name="expands"></param>
        /// <returns>Returns profile details.</returns>
        ProfileModel GetProfile(int profileId, ExpandCollection expands);

        /// <summary>
        /// Method Returns the Profiles.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <returns>Returns the profiles</returns>
        ProfileListModel GetProfiles(ExpandCollection expands, FilterCollection filters, SortCollection sorts);

        /// <summary>
        /// Method Returns the Profiles.
        /// </summary>
        /// <param name="expands"></param>
        /// <param name="filters"></param>
        /// <param name="sorts"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns>Returns the profiles</returns>
        ProfileListModel GetProfiles(ExpandCollection expands, FilterCollection filters, SortCollection sorts, int? pageIndex, int? pageSize);

        /// <summary>
        /// Method Creates the Profile.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return the profile.</returns>
        ProfileModel CreateProfile(ProfileModel model);

        /// <summary>
        /// Method Updates the Profile.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <param name="model"></param>
        /// <returns>Returns Updated profile.</returns>
        ProfileModel UpdateProfile(int profileId, ProfileModel model);

        /// <summary>
        /// Method Deletes the profile based on Id.
        /// </summary>
        /// <param name="facetGroupId"></param>
        /// <returns>Returns true or false.</returns>
        bool DeleteProfile(int profileId);
    }
}
