﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class CatalogAgent : BaseAgent, ICatalogAgent
    {
        #region Private Variables
        private readonly ICatalogsClient _catalogClient; 
        #endregion

        #region Constructor
        public CatalogAgent()
        {
            _catalogClient = GetClient<CatalogsClient>();
        } 
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets the List of Catalogs
        /// </summary>
        /// <returns></returns>
        public CatalogListViewModel GetCatalogs()
        {
            CatalogListModel catalogList = _catalogClient.GetCatalogs(null, null);

            if (catalogList != null && catalogList.Catalogs != null)
            {
                return CatalogListViewModelMap.ToViewModel(catalogList.Catalogs);
            }
            return null;

        }

        /// <summary>
        /// Gets Catalog by catalog Id
        /// </summary>
        /// <param name="catalogId">int</param>
        /// <returns></returns>
        public CatalogListViewModel GetCatalog(int catalogId)
        {
            CatalogModel catalogList = _catalogClient.GetCatalog(catalogId);

            if (catalogList != null)
            {
                return CatalogListViewModelMap.ToViewModel(catalogList);
            }
            return null;

        } 
        #endregion
    }
}