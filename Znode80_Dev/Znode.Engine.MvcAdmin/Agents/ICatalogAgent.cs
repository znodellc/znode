﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface ICatalogAgent
    {
        /// <summary>
        /// Gets the list of catalogs
        /// </summary>
        /// <returns></returns>
        CatalogListViewModel GetCatalogs();

        /// <summary>
        /// Gets the list of catalogs by catalogId
        /// </summary>
        /// <param name="catalogId"></param>
        /// <returns></returns>
        CatalogListViewModel GetCatalog(int catalogId);
    }
}
