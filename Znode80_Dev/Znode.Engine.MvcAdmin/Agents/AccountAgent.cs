﻿using System.Linq;
using System.Web;
using System.Web.Security;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class AccountAgent : BaseAgent, IAccountAgent
    {
        #region Private Variables
        private readonly IAccountsClient _accountClient;

        #endregion

        #region Constructor
        public AccountAgent()
        {
            _accountClient = GetClient<AccountsClient>();
        }
        #endregion

        #region Public Methods
        #region Login method
        /// <summary>
        ///      based on the username and password
        /// </summary>
        /// <param name="model">Login details as Login View Model</param>
        /// <returns>Success or not</returns>
        public LoginViewModel Login(LoginViewModel model)
        {
            AccountModel accountModel;
            LoginViewModel loginViewModel;
            try
            {
                accountModel = _accountClient.Login(AccountViewModelMap.ToLoginModel(model), GetExpands());

                // Check user profiles.
                if (!accountModel.Profiles.Any())
                {
                    return new LoginViewModel() { HasError = true };
                }

                SaveInSession("UserAccount", AccountViewModelMap.ToAccountViewModel(accountModel));

                return AccountViewModelMap.ToLoginViewModel(accountModel);

            }
            catch (ZnodeException ex)
            {
                //if (ex.ErrorCode == 2)
                //{
                //    loginViewModel = new LoginViewModel();
                //    loginViewModel.IsResetPassword = true;
                //    loginViewModel.HasError = true;
                //    loginViewModel.ErrorMessage = ex.ErrorMessage;
                //    return loginViewModel;
                //}
                //if (ex.ErrorCode == ErrorCodes.LoginFailed || ex.ErrorCode == ErrorCodes.AccountLocked)
                //{
                //    loginViewModel = new LoginViewModel();
                //    loginViewModel.HasError = true;
                //    loginViewModel.ErrorMessage = ex.ErrorMessage;

                //    return loginViewModel;
                //}
            }

            return new LoginViewModel() { HasError = true, ErrorMessage = "Login Failed" };

        }

        #endregion

        #region Logout
        /// <summary>
        /// Logout the user
        /// </summary>
        public void Logout()
        {
            FormsAuthentication.SignOut();
            HttpContext.Current.Session.Abandon();
        }
        #endregion

        public bool IsUserInRole(string userName, string roleName)
        {
            AccountModel accountModel;
            try
            {
                accountModel = _accountClient.CheckUserRole(new AccountModel { UserName = userName, RoleName = roleName });
                return accountModel.IsUserInRole;
            }
            catch (ZnodeException ex)
            {
                return false;
            }
        }


        #endregion

        #region Private Methods
        #region GetExpands
        /// <summary>
        /// Returns the Expands needed for the account agent.
        /// </summary>
        /// <returns></returns>
        private ExpandCollection GetExpands()
        {
            return new ExpandCollection()
                {
                    ExpandKeys.Addresses,
                    ExpandKeys.Profiles,
                    ExpandKeys.WishLists,
                    ExpandKeys.Orders,
                    ExpandKeys.OrderLineItems,
                    ExpandKeys.User,
                    ExpandKeys.GiftCardHistory
                };
        }
        #endregion
        #endregion
    }
}