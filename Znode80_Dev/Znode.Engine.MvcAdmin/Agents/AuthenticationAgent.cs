﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class AuthenticationAgent : IAuthenticationAgent
    {


        public void SetAuthCookie(string userName, bool createPersistantCookie)
        {
            FormsAuthentication.SetAuthCookie(userName, createPersistantCookie);
        }

        public void RedirectFromLoginPage(string userName, bool createPersistantCookie)
        {
            FormsAuthentication.RedirectFromLoginPage(userName, createPersistantCookie);
        }
    }
}