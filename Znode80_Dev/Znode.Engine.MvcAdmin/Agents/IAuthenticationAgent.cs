﻿namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IAuthenticationAgent
    {
        void SetAuthCookie(string userName, bool createPersistantCookie);
        void RedirectFromLoginPage(string userName, bool createPersistantCookie);
    }
}