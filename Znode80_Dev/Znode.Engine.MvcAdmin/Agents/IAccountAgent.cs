﻿using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IAccountAgent
    {
        /// <summary>
        /// This method is used to login the user.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Return the login details.</returns>
        LoginViewModel Login(LoginViewModel model);

        /// <summary>
        /// This method is used to logout the user.
        /// </summary>
        void Logout();

        /// <summary>
        /// Method used to check whether the user is authorized or not.
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="roleName"></param>
        /// <returns>Returns true or false</returns>
        bool IsUserInRole(string userName, string roleName);
    }
}
