﻿using System.Linq;
using System.Web;
using System.Web.Security;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcAdmin.Maps;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public class PromotionsAgent : BaseAgent, IPromotionsAgent
    {
        #region Private Variables
        private readonly IPromotionsClient _promotionsClient;
        #endregion

        #region Constructor
        public PromotionsAgent()
        {
            _promotionsClient = GetClient<PromotionsClient>();
        }
        #endregion

        #region Public Methods

        public PromotionsViewModel GetPromotions(int promotionId)
        {
            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Profiles);
          
            var promotion = _promotionsClient.GetPromotion(promotionId);

            if (promotion == null)
            {
                return null;
            }

            return null;
        }


        public bool SavePromotions(PromotionsViewModel model)
        {
            return true;
        }

        public bool UpdatePromotions(PromotionsViewModel model)
        {
            return true;
        }

        public bool DeletePromotions(int promotionId)
        {
            return true;
        }

        #endregion
    }
}