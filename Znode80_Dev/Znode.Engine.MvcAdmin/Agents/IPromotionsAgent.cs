﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Agents
{
    public interface IPromotionsAgent
    {
        PromotionsViewModel GetPromotions(int promotionId);
        bool SavePromotions(PromotionsViewModel model);
        bool UpdatePromotions(PromotionsViewModel model);
        bool DeletePromotions(int promotionId);
    }
}
