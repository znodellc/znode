﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class CatalogController : BaseController
    {
        #region Private Variables
        private readonly ICatalogAgent _catalogAgent;
        //private readonly ICatagoryAgent _categoryAgent; 
        #endregion


        #region Constructor
        public CatalogController()
        {
            _catalogAgent = new CatalogAgent();
            //_categoryAgent = new CategoryAgent();
        } 
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets the Catalogs List
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public ActionResult GetCatalogs()
        {
            CatalogListViewModel catalogs = _catalogAgent.GetCatalogs();
            if (catalogs.Equals(null))
            {
                return new EmptyResult();
            }

            return View("CatalogList", catalogs);
        }

        //Below code is yet to be completed so commented it.

        //public ActionResult ManageCatalog(int catalogId)
        //{
        //    CatalogListViewModel catalogs = _catalogAgent.GetCatalog(catalogId);

        //    if (catalogs.CatalogId.Equals(catalogId))
        //    {
        //        CategoryListViewModel categoriesList = _categoryAgent.GetCategory(catalogId);
        //        if (categoriesList != null)
        //        {
        //            catalogs.Categories = categoriesList.Categories;
        //        }
        //    }
        //    return View("AddCatalog",catalogs);

        //} 
        #endregion

    }
}
