﻿using System.Web.Mvc;

namespace Znode.Engine.MvcAdmin.Controllers
{
    /// <summary>
    /// This controller will encapsulate all the methods necessary for the dashboard page
    /// </summary>
    public class DashboardController : BaseController
    {
        /// <summary>
        /// Show the Dashboard view
        /// </summary>
        /// <returns>ActionResult to display view</returns>
        public ActionResult Dashboard()
        {
            return View();
        }

    }
}
