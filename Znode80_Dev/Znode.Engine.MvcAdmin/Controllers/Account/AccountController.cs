﻿using System.Web.Mvc;
using Znode.Engine.MvcAdmin.Agents;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class AccountController : BaseController
    {
        #region Private Variables
        private readonly IAccountAgent _accountAgent;
        private readonly IAuthenticationAgent _authenticationAgent;
        #endregion

        #region Constructor
        public AccountController()
        {
            _accountAgent = new AccountAgent();
            _authenticationAgent = new AuthenticationAgent();
        }
        #endregion

        #region Get/Post action for Login
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                _accountAgent.Logout();
            }
            //TODO
            //Get user name from cookies
            //GetLoginRememberMeCookie();
            return View("Login");
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                //Login the user.
                var loginviewModel = _accountAgent.Login(model);

                if (!loginviewModel.HasError)
                {
                    //Set the Authentication cookie.
                    _authenticationAgent.SetAuthCookie(model.Username, model.RememberMe);

                    //TODO//Remember me
                    if (checked(model.RememberMe) == true)
                    {
                        //SaveLoginRememberMeCookie(model.Username);
                    }

                    if (_accountAgent.IsUserInRole(model.Username, "ORDER APPROVER") && !_accountAgent.IsUserInRole(model.Username, "ADMIN"))
                        //TODO
                        //Response.Redirect("~/Secure/Orders/OrderManagement/ViewOrders/Default.aspx", true);
                        return RedirectToAction("Index", "Test");

                    if ((_accountAgent.IsUserInRole(model.Username.Trim(), "REVIEWER") || _accountAgent.IsUserInRole(model.Username.Trim(), "REVIEWER MANAGER")) && !_accountAgent.IsUserInRole(model.Username, "ADMIN"))
                    {
                        //TODO
                       // Response.Redirect("~/Secure/Vendors/VendorProducts/Default.aspx", true);
                        return RedirectToAction("Index", "Test");
                    }

                    // if user is an admin, then redirect to the admin dashboard
                    if (_accountAgent.IsUserInRole(model.Username.Trim(), "ADMIN") && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
                    {
                        return RedirectToAction("Dashboard", "Dashboard");
                    }
                    else if (_accountAgent.IsUserInRole(model.Username.Trim(), "FRANCHISE") && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
                    {
                        //TODO
                        //Response.Redirect("~/franchiseAdmin/secure/default.aspx", true);
                        return RedirectToAction("Index", "Test");
                    }
                    else if (_accountAgent.IsUserInRole(model.Username.Trim(), "CUSTOMER SERVICE REP") || _accountAgent.IsUserInRole(model.Username.Trim(), "CATALOG EDITOR") || _accountAgent.IsUserInRole(model.Username.Trim(), "EXECUTIVE") || _accountAgent.IsUserInRole(model.Username.Trim(), "ORDER ONLY") || _accountAgent.IsUserInRole(model.Username.Trim(), "SEO") || _accountAgent.IsUserInRole(model.Username.Trim(), "CONTENT EDITOR") && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
                    {
                        //TODO
                        //Response.Redirect("~/secure/default.aspx", true);
                        return RedirectToAction("Index", "Test");                       
                    }
                    _authenticationAgent.RedirectFromLoginPage(model.Username, false);
                    return RedirectToAction("Login");
                }
                //TODO
                if (loginviewModel != null && loginviewModel.IsResetPassword)
                {
                    return View("ResetPassword");
                }

                return View("Login", loginviewModel);
            }

            return View("Login", model);
        }

        #endregion

        #region Post action for Logout
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Logout()
        {
            _accountAgent.Logout();
            return RedirectToAction("Login");
        }

         #endregion
    }
}
