﻿using System.Web.Mvc;
using System.Web.Routing;
using Znode.Engine.MvcAdmin.Agents;

namespace Znode.Engine.MvcAdmin.Controllers
{
    public class BaseController : Controller
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            this.ControllerContext = new ControllerContext(requestContext, this);
        }
    }
}
