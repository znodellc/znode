﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// VIew Model for a list of tags to associate with your product. 
    /// </summary>
    public class TagsViewModelcs : BaseViewModel
    {
        /// <summary>
        /// Constructor For TagsViewModelcs
        /// </summary>
        /// 
        public TagsViewModelcs()
        { 
        
        }

        public string Tags { get; set; }

        public int TagId { get; set; }
        public int ProductId { get; set; }
        
    }
}