﻿namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for create and edit Facet Group
    /// </summary>
    public class FacetGroupViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for FacetGroupViewModel
        /// </summary>
        public FacetGroupViewModel()
        {

        }

        public string FacetGroupName { get; set; }

        public int DisplayOrder { get; set; }
        public int CatalogId { get; set; }
    }
}