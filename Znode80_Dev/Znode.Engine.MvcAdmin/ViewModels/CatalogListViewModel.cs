﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class CatalogListViewModel :BaseViewModel
    {
        public int CatalogId { get; set; }
        public string ExternalId { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public int? PortalId { get; set; }
        public Collection<CatalogModel> Catalogs { get; set; }
        public Collection<CategoryModel> Categories { get; set; }

    }
}