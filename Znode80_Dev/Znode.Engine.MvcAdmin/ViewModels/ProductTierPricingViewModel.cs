﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Add Pricing Tier for Product
    /// </summary>
    public class ProductTierPricingViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ProductTierPricingViewModel
        /// </summary>
        public ProductTierPricingViewModel()
        { 
        
        }

        public int ProductTierId { get; set; }
        public int ProductId { get; set; }
        public int TierStart { get; set; }
        public int TierEnd { get; set; }

        public int? ProfileId { get; set; }
        
        public decimal Price { get; set; }
    }
}