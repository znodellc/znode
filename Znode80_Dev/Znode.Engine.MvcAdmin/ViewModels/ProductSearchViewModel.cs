﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For Search Products
    /// </summary>
    public class ProductSearchViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor For ProductSearchViewModel
        /// </summary>
        /// 
        public ProductSearchViewModel()
        { 
        
        }

        public string ProductName { get; set; }
        public string ProductNumber { get; set; }
        public string Sku { get; set; }
        public string ProductCategory { get; set; }
    }
}