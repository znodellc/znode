﻿using System;
using System.Collections.Generic;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Promotions
    /// </summary>
    public class PromotionsViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for PromotionsViewModel
        /// </summary>
        public PromotionsViewModel()
        {

        }
         
        public string Name { get; set; }
        public string Description { get; set; }
        public string CouponInd { get; set; }
        public string CouponCode { get; set; }
        public string PromotionMessage { get; set; }
        public decimal OrderMinimum { get; set; }
        public string QuantityMinimum { get; set; }
        public string DisplayOrder { get; set; }
        public string PromoCode { get; set; }

        public int PortalId { get; set; }
        public int CouponQuantityAvailable { get; set; }
        public int ProfileId { get; set; }
        public int DiscountTypeId { get; set; }

        public int? PromotionId { get; set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public decimal Discount { get; set; }
    }
}