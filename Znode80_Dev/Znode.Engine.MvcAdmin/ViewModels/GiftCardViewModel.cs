﻿using System;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Create and Edit Gift Cards
    /// </summary>
    public class GiftCardViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for GiftCardViewModel
        /// </summary>
        public GiftCardViewModel()
        {

        }

        public string CardNumber { get; set; }
        public string Name { get; set; }    
        
        public int GiftCardId { get; set; }
        public int CreatedBy { get; set; }
        public int PortalId { get; set; }
      
        public int? OrderLineItemId { get; set; }      

        public DateTime CreateDate { get; set; }
        public DateTime? ExpirationDate { get; set; }

        public decimal? Amount { get; set; }        
    }
}
