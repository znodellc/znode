﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For Add New Product Type
    /// </summary>
    public class ProductTypeViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for ProductTypeViewModel
        /// </summary>
        public ProductTypeViewModel()
        { 
        
        }

        public string Name { get; set; }
        public string Description { get; set; }

        public int ProductTypeId { get; set; }
        public int DisplayOrder { get; set; }
        
        public bool IsFranchisable { get; set; }
    }
}