﻿using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for create and edit Category
    /// </summary>
    public class CategoryViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for CategoryViewModel
        /// </summary>
        public CategoryViewModel()
        {

        }
        public string CategoryName { get; set; }
        public string CategoryTitle { get; set; }
        public string ImageAltText { get; set; }
        public string SeoTitle { get; set; }
        public string SeoKeyword { get; set; }
        public string SeoDescription { get; set; }
        public string SeoFriendlyPageName { get; set; }
        public string ShortDescription { get; set; }
        public string LongDescription { get; set; }
        public string AdditionalDescription { get; set; }
        public string CategoryBanner { get; set; }
        public string ImagePath { get; set; }

        public int? DisplayOrder { get; set; }
        
        public bool ChildCategories { get; set; }
        
        public HttpPostedFileBase SelectImage { get; set; }
    }
}