﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For Add Alternate Image for Product
    /// </summary>
    public class ProductAlternateImageViewModel : BaseViewModel
    {
        /// <summary>
        /// Construtor for ProductAlternateImageViewModel
        /// </summary>
        /// 
        public ProductAlternateImageViewModel()
        {

        }

        public int ProductImageId { get; set; }
        public int ProductId { get; set; }

        public int? DisplayOrder { get; set; }

        public string Title { get; set; }
        public string ImageAltText { get; set; }

        public bool ShowOnCategoryPage { get; set; }
    }
}