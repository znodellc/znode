﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model for Reason return 
    /// </summary>
    public class ReasonReturnViewModel : BaseViewModel
    {
        /// <summary>
        /// Consructor for ReasonReturnViewModel
        /// </summary>
        public ReasonReturnViewModel()
        {
 
        }
        public string  Name { get; set; }

        public int ReasonId { get; set; }

        public bool IsEnabled { get; set; }

    }
}