﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    public class ProductCreateEditViewModel : BaseViewModel
    {
        
        public string BundleItemsIds { get; set; }
        public string ShortDescription { get; set; }
        public string Description { get; set; }
        public string ImageAltTag { get; set; }
        public string ImageFile { get; set; }
        public string ImageLargePath { get; set; }
        public string ImageMediumPath { get; set; }
        public string ImageSmallThumbnailPath { get; set; }
        public string InStockMessage { get; set; }
        public string BackOrderMessage { get; set; }
        public string OutOfStockMessage { get; set; }
        public string Name { get; set; }
        public string Sku { get; set; }
        public string InventoryMessage { get; set; }
        public string SeoDescription { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoTitle { get; set; }
        public string SeoPageName { get; set; }
        public string DownloadLink { get; set; }
        public string FBTProductsIds { get; set; }
        public string YMALProductsIds { get; set; }
        public string VendorName { get; set; }
        public string ProductCode { get; set; }

        public int ProductId { get; set; }
        public int ProductTypeId { get; set; }
        public int BrandId { get; set; }
        public int TaxClassId { get; set; }
        public int SupplierId { get; set; }
        public int SelectedQuantity { get; set; }
        public int ShippingRuleTypeId { get; set; }
        public int Rating { get; set; }
        public int WishListID { get; set; }


        public int? DisplayOrder { get; set; }
        public int? QuantityOnHand { get; set; }
        public int? ReorderLevel { get; set; }
        public int? MaxQuantity { get; set; }
        public int? MinQuantity { get; set; }

        public decimal Price { get; set; }
        public decimal OriginalPrice { get; set; }
        
        public decimal? RetailPrice { get; set; }
        public decimal? SalePrice { get; set; }
        public decimal? WholeSalePrice { get; set; }
        public decimal? Weight { get; set; }
        public decimal? Height { get; set; }
        public decimal? Width { get; set; }
        public decimal? Length { get; set; }

        public bool IsActive { get; set; }
        public bool ShowAddToCart { get; set; }
        public bool ShowWishlist { get; set; }
        public bool ShowQuantity { get; set; }
        public bool IsCallForPricing { get; set; }

        public bool? TrackInventory { get; set; }
        public bool? AllowBackOrder { get; set; }
        public bool? FreeShipping { get; set; }

        public HttpPostedFileBase ProductImage { get; set; }

        //public List<ProductTypeViewModel> ProductTypeList { get; set; }
        //public List<ProductBrandTypeViewModel> ProductBrandList { get; set; }
        //public List<TaxClassViewModel> TaxList { get; set; }
        //public List<ShippingRuleTypeViewModel> ShippingCostList { get; set; }
        //public List<SupplierViewModel> SupplierNameList { get; set; }
    }
}