﻿using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// view model for Create Highlight 
    /// </summary>
    public class HighlightViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for HighlightViewModel
        /// </summary>
        public HighlightViewModel()
        { 

        }

        public string Name { get; set; }
        public string ImageAltTag { get; set; }
        public string Description { get; set; }

        public int MyProperty { get; set; }
        
        public int? DisplayOrder { get; set; }

        public bool? IsActive { get; set; }

        public HttpPostedFileBase HighlightImage { get; set; }
             
    }
}