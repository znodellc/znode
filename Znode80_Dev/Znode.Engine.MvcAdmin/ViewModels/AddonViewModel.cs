﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Add Product Add-On
    /// </summary>
    public class AddonViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for AddonViewModel
        /// </summary>

        public AddonViewModel()
        { 
        
        }

        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string InStockMessage { get; set; }
        public string OutOfStockMessage { get; set; }
        public string BackOrderMessage { get; set; }

        public int DisplayOrder { get; set; }
        public int AddOnId { get; set; }
    }
}