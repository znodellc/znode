﻿using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for create and edit Store
    /// </summary>
    public class StoreViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for StoreViewModel
        /// </summary>
        public StoreViewModel()
        {

        }

        public string StoreName { get; set; }
        public string BrandName { get; set; }
        public string SalesDepartmentEmail { get; set; }
        public string SalesDepartmentPhoneNumber { get; set; }
        public string CustomerServiceEmail { get; set; }
        public string CustomerServicePhoneNumber { get; set; }
        public string AdministratorsEmail { get; set; }

        public int? StoreId { get; set; }
        
        public bool Security { get; set; }
        public bool IncludeTaxesInProductPrice { get; set; }
        public bool EnablePersistentCart  { get; set; }
        public bool EnableAddressValidation   { get; set; }
        public bool RequireValidatedAddress { get; set; }
        public bool EnableCustomerBasedPricing { get; set; }
        public bool RequireManualApprovalOfEveryOrder { get; set; }
        
        public HttpPostedFileBase Logo { get; set; }
    }
}