﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for create Profile
    /// </summary>
    public class ProfileViewModel : BaseViewModel
    {
        /// <summary>
        /// Consructor for ProfileViewModel
        /// </summary>
        public ProfileViewModel()
        {
            
        }

        public string Name { get; set; }

        public int ProfileId { get; set; }        

        public decimal? Weighting { get; set; }

        public bool ProductPrice { get; set; }
        public bool? WholeSalePrice { get; set; }
        public bool Affiliate { get; set; }
        public bool TaxExempt { get; set; }
    }
}