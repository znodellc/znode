﻿using System;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View model create order
    /// </summary>
    public class OrderViewModel : BaseViewModel
    {
        /// <summary>
        /// Consructor for OrderViewModel
        /// </summary>
        public OrderViewModel()
        {
 
        }

        public string StoreName { get; set; }
        public string OrderStatus { get; set; }
        public string PaymentStatus { get; set; }
        public string PaymentType { get; set; }
        public string Name { get; set; }

        public int OrderId { get; set; }

        public decimal Amount { get; set; }

        public DateTime? OrderDate { get; set; }
    }
}