﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model For add New Brand 
    /// </summary>
    public class BrandViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for BrandViewModel
        /// </summary>
        public BrandViewModel()
        { 
            
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }

        public int BrandId { get; set; }

        public bool IsActive { get; set; }
    }
}