﻿
namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// View Model for Add Attribute Type
    /// </summary>
    public class AttributeTypeViewMode : BaseViewModel
    {
        /// <summary>
        /// Constructor for AttributeTypeViewMode
        /// </summary>
        public AttributeTypeViewMode()
        { 
        
        }

        public string Name { get; set; }

        public int AttributeTypeId { get; set; }
        public int DisplayOrder { get; set; }
    }
}