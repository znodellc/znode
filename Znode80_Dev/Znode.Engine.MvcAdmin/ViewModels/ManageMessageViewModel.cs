﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for Manage Message
    /// </summary>
    public class ManageMessageViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Manage Message
        /// </summary>
        public ManageMessageViewModel()
        {

        }

        public string MessageKey { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
    }
}