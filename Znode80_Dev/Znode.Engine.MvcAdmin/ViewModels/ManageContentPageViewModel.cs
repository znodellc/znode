﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcAdmin.ViewModels
{
    /// <summary>
    /// ViewModel for Manage Content Page
    /// </summary>
    public class ManageContentPageViewModel : BaseViewModel
    {
        /// <summary>
        /// Constructor for Manage Content Page 
        /// </summary>
        public ManageContentPageViewModel()
        {

        }

        public string PageName { get; set; }
        public string PageTitle { get; set; }
        public string SEOTitle { get; set; }
        public string SEOKeywords { get; set; }
        public string SEODescription { get; set; }
        public string AdditionalMetaInformation { get; set; }
        public string SEOFriendlyPageName { get; set; }
        
    }
}