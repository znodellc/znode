﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class CatalogListViewModelMap
    {
        #region Public Methods
        /// <summary>
        /// Maps the collection of Catalog model to CatalogListViewModel
        /// </summary>
        /// <param name="models">Collection<CatalogModel></param>
        /// <returns></returns>
        public static CatalogListViewModel ToViewModel(Collection<CatalogModel> models)
        {
            CatalogListViewModel viewModel = new CatalogListViewModel();
            viewModel.Catalogs = models;
            return viewModel;
        }

        /// <summary>
        /// Maps the Catalog Model to CatalogViewList Model
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public static CatalogListViewModel ToViewModel(CatalogModel models)
        {
            CatalogListViewModel viewModel = new CatalogListViewModel();
            viewModel.CatalogId = models.CatalogId;
            viewModel.Name = models.Name;
            return viewModel;
        }
        #endregion

    }
}