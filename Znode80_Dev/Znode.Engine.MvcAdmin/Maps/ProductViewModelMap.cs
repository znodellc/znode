﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class ProductViewModelMap
    {

        #region HighLight view
        public static HighlightModel ToHighlightModel(HighlightViewModel model)
        {
            return new HighlightModel
            {
                HighlightId = model.HighlightId,
                Name = model.Name,
                Description = model.Description,
                DisplayOrder = model.DisplayOrder,
                ImageAltTag = model.ImageAltTag,
                IsActive = model.IsActive,
            };
        }

        public static HighlightViewModel ToHighlightViewModel(HighlightModel model)
        {
            return new HighlightViewModel
            {
                HighlightId = model.HighlightId,
                Description = model.Description,
                Name = model.Name,
                DisplayOrder = model.DisplayOrder,
                ImageAltTag = model.ImageAltTag,
            };
        }
        #endregion

        #region AddOns View
        public static AddonViewModel ToAddonViewModel(AddOnModel model)
        {
            return new AddonViewModel
            {
                AddOnId = model.AddOnId,
                Description = model.Description,
                DisplayOrder = model.DisplayOrder,
                InStockMessage = model.InStockMessage,
                BackOrderMessage = model.BackOrderMessage,
                OutOfStockMessage = model.OutOfStockMessage,
                Name = model.Name
            };
        }
        public static AddOnModel ToAddOnModel(AddonViewModel model)
        {
            return new AddOnModel
            {
                AddOnId = model.AddOnId,
                Description = model.Description,
                DisplayOrder = model.DisplayOrder,
                InStockMessage = model.InStockMessage,
                BackOrderMessage = model.BackOrderMessage,
                OutOfStockMessage = model.OutOfStockMessage,
                Name = model.Name
            };
        }
        #endregion

        #region Product Pricing Tier
        public static ProductTierPricingViewModel ToProductTierPricingViewModel(TierModel model)
        {
            return new ProductTierPricingViewModel
            {
                ProfileId = model.ProfileId,
                ProductId = model.ProductId,
                ProductTierId = model.ProductTierId,
                Price = model.Price,
                TierStart = model.TierStart,
                TierEnd = model.TierEnd,

            };
        }
        public static TierModel ToTierModel(ProductTierPricingViewModel model)
        {
            return new TierModel
            {
                ProfileId = model.ProfileId,
                ProductId = model.ProductId,
                ProductTierId = model.ProductTierId,
                Price = model.Price,
                TierStart = model.TierStart,
                TierEnd = model.TierEnd,

            };
        }
        #endregion

        #region Product Setting
        public static ProductSettingViewModel ToProductSettingViewModel(ProductModel model)
        {
            return new ProductSettingViewModel
            {
                ProductId = model.ProductId,
                AllowRecurringBilling = model.AllowRecurringBilling,
                BackOrderMessage = model.BackOrderMessage,
                CallForPricing = model.CallForPricing,
                InStockMessage = model.InStockMessage,
                IsFeatured = model.IsFeatured,
                IsHomepageSpecial = model.IsHomepageSpecial,
                IsNewProduct = model.IsNewProduct,
                OutOfStockMessage = model.OutOfStockMessage,
                SeoDescription = model.SeoDescription,
                SeoTitle = model.SeoTitle,
                SeoUrl = model.SeoUrl,

            };
        }

        public static ProductModel ToProductModel(ProductSettingViewModel model)
        {
            return new ProductModel
            {
                ProductId = model.ProductId,
                AllowRecurringBilling = model.AllowRecurringBilling,
                BackOrderMessage = model.BackOrderMessage,
                CallForPricing = model.CallForPricing,
                InStockMessage = model.InStockMessage,
                IsFeatured = model.IsFeatured,
                IsHomepageSpecial = model.IsHomepageSpecial,
                IsNewProduct = model.IsNewProduct,
                OutOfStockMessage = model.OutOfStockMessage,
                SeoDescription = model.SeoDescription,
                SeoTitle = model.SeoTitle,
                SeoUrl = model.SeoUrl,

            };
        }
        #endregion

        #region Product Type
        public static ProductTypeViewModel ToProductTypeViewModel(ProductTypeModel model)
        {
            return new ProductTypeViewModel
            {
                ProductTypeId = model.ProductTypeId,
                Description = model.Description,
                DisplayOrder = model.DisplayOrder,
                IsFranchisable = model.IsFranchiseable,
                Name = model.Name,

            };
        }

        public static ProductTypeModel ToProductTypeModel(ProductTypeViewModel model)
        {
            return new ProductTypeModel
            {
                ProductTypeId = model.ProductTypeId,
                Description = model.Description,
                DisplayOrder = model.DisplayOrder,
                IsFranchiseable = model.IsFranchisable,
                Name = model.Name,

            };
        }
        #endregion

        #region Tags
        public static TagsViewModel ToTagsViewModel(TagsModel model)
        {
            return new TagsViewModel
            {
                TagId = model.TagId,
                ProductId = model.ProductId,
                Tags = model.Tags,
            };
        }
        public static TagsModel ToTagsModel(TagsViewModel model)
        {
            return new TagsModel
            {
                TagId = model.TagId,
                ProductId = model.ProductId,
                Tags = model.Tags,
            };
        }
        #endregion

        #region Brand
        public static BrandViewModel ToBrandViewModel(ManufacturerModel model)
        {
            return new BrandViewModel
            {
                BrandId = model.ManufacturerId,
                Description = model.Description,
                Email = model.Email,
                IsActive = model.IsActive,
                Name = model.Name,
            };
        }

        public static ManufacturerModel ToManufacturerModel(BrandViewModel model)
        {
            return new ManufacturerModel
            {
                ManufacturerId = model.BrandId,
                Description = model.Description,
                Email = model.Email,
                IsActive = model.IsActive,
                Name = model.Name,
            };
        }
        #endregion

        #region Attribute Type
        public static AttributeTypeViewModel ToAttributeTypeViewModel(AttributeModel model)
        {
            return new AttributeTypeViewModel
            {
                Name = model.Name,
                AttributeTypeId = model.AttributeTypeId,
                DisplayOrder = model.DisplayOrder,
            };
        }

        public static AttributeModel ToAttributeTypeViewModel(AttributeTypeViewModel model)
        {
            return new AttributeModel
            {
                Name = model.Name,
                AttributeTypeId = model.AttributeTypeId,
                DisplayOrder = model.DisplayOrder,
            };
        } 
        #endregion
    }

}