﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class AccountViewModelMap
    {
        public static AccountModel ToLoginModel(LoginViewModel model)
        {
            return new AccountModel()
                {
                    User = new UserModel()
                        {
                            Username = model.Username,
                            Password = model.Password,
                            PasswordQuestion = model.PasswordQuestion,
                        }
                };
        }

        public static LoginViewModel ToLoginViewModel(AccountModel model)
        {
            return new LoginViewModel()
            {
                    Username = model.User.Username,
                    Password = model.User.Password,
                    PasswordQuestion = model.User.PasswordQuestion,
                    IsResetPassword = model.User.IsLockedOut,
                
            };
        }

        public static AccountViewModel ToAccountViewModel(AccountModel model)
        {
            var accountViewModel = new AccountViewModel()
                {
                    AccountId = model.AccountId,
                    EmailAddress = model.User.Email,
                    UserName = model.User.Username,
                    PasswordQuestion = model.User.PasswordQuestion,
                    PasswordAnswer = model.User.PasswordAnswer,
                    UserId = model.User.UserId,
                    CompanyName = model.CompanyName,
                    EmailOptIn = model.EmailOptIn,
                    EnableCustomerPricing = model.EnableCustomerPricing,
                    ExternalId = model.ExternalId,
                    IsActive = model.IsActive,
                    ProfileId = model.Profiles.Any() ? model.Profiles.First().ProfileId : 0,
                    //UseWholeSalePricing = model.Profiles.Any() ? model.Profiles.First().UseWholesalePricing.GetValueOrDefault(false) : false,
                    //Addresses = new Collection<AddressViewModel>(model.Addresses.Select(AddressViewModelMap.ToModel).OrderBy(x => x.AddressId).ToList()),
                    //WishList = ToWishListViewModel(model.WishList),
                    //Orders = ToOrdersViewModel(model.Orders),
                };

            //if (accountViewModel.Orders.Any() && model.GiftCardHistoryList != null)
            //{
            //    accountViewModel.Orders.ToList().ForEach(x =>
            //        {
            //            var giftCardHistoryModel = model.GiftCardHistoryList.FirstOrDefault(y => y.OrderId == x.OrderId);
            //            if (giftCardHistoryModel != null)
            //                x.GiftCardAmount = -giftCardHistoryModel.TransactionAmount;
            //        });
            //}

            return accountViewModel;
        }
        //public static ReviewViewModel ToReviewViewModel(Collection<ReviewModel> model)
        //{
        //    var reviewModel = new ReviewViewModel();
        //    reviewModel.Items = new Collection<ReviewItemViewModel>(
        //        model.Select(x => new ReviewItemViewModel()
        //        {
        //            ReviewId = x.ReviewId,
        //            Rating = x.Rating,
        //            AccountId = x.AccountId,
        //            Comments = WebUtility.HtmlDecode(x.Comments),
        //            CreateDate = x.CreateDate.GetValueOrDefault().ToShortDateString(),
        //            CreateUser = x.CreateUser,
        //            ProductId = x.ProductId,
        //            Status =  x.Status,
        //            Subject = x.Subject,
        //            Duration = GetReviewDuration(x.CreateDate.GetValueOrDefault()),
        //            UserLocation = x.UserLocation,
        //        }).ToList());

        //    return reviewModel;
        //}

        //public static ReviewModel ToReviewViewModel(ReviewItemViewModel model)
        //{
        //    return new ReviewModel()
        //        {
        //            AccountId = model.AccountId,
        //            Comments =  model.Comments,
        //            Cons = string.Empty,
        //            CreateDate = Convert.ToDateTime(model.CreateDate),
        //            CreateUser =  model.CreateUser,
        //            Custom1 =  string.Empty,
        //            Custom2 =  string.Empty,
        //            Custom3 =  string.Empty,
        //            Product =  null,
        //            Status =  MvcDemoConstants.DefaultReviewStatus,
        //            Subject = model.Subject,
        //            Rating =  model.Rating,
        //            UserLocation =  model.UserLocation,
        //            ProductId =  model.ProductId,
        //            Pros =  string.Empty,
        //        };
        //}
       
        //public static AccountModel ToAccountModel(AccountViewModel model)
        //{
        //    return new AccountModel()
        //    {
        //        AccountId = model.AccountId,
        //        Email = model.EmailAddress,
        //        User = new UserModel() { 
        //            Username = model.UserName,
        //            Email = model.EmailAddress,
        //            UserId = model.UserId.GetValueOrDefault(),
        //            PasswordQuestion = model.PasswordQuestion,
        //            PasswordAnswer = model.PasswordAnswer,
        //        },
        //        CompanyName = model.CompanyName,
        //        EmailOptIn = model.EmailOptIn,
        //        EnableCustomerPricing = model.EnableCustomerPricing,
        //        ExternalId = model.ExternalId,
        //        IsActive = model.IsActive,            
        //        ProfileId = model.ProfileId,
        //        UserId = model.UserId,
        //        Addresses = new Collection<AddressModel>(model.Addresses.Select(AddressViewModelMap.ToModel).ToList()),
        //        //Znode Version 7.2.2
        //        //Map BaseUrl to MVC Domain Url for Reseting Password - Start
        //        BaseUrl = model.BaseUrl,
        //        //Map BaseUrl to MVC Domain Url for Reseting Password - End
        //    };
        //}

        //public static AccountModel ToAccountModel(AccountViewModel model, ChangePasswordViewModel passwordModel)
        //{
        //    return new AccountModel()
        //    {
        //        AccountId = model.AccountId,
        //        Email = model.EmailAddress,
        //        User = new UserModel()
        //        {
        //            Username = model.UserName,
        //            Email = model.EmailAddress,
        //            UserId = model.UserId.GetValueOrDefault(),
        //            PasswordQuestion = model.PasswordQuestion,
        //            PasswordAnswer = model.PasswordAnswer,
        //            Password = passwordModel.OldPassword,
        //            NewPassword = passwordModel.NewPassword,
        //        },
        //        CompanyName = model.CompanyName,
        //        EmailOptIn = model.EmailOptIn,
        //        EnableCustomerPricing = model.EnableCustomerPricing,
        //        ExternalId = model.ExternalId,
        //        IsActive = model.IsActive,
        //        ProfileId = model.ProfileId,
        //        UserId = model.UserId,
        //        Addresses = new Collection<AddressModel>(model.Addresses.Select(AddressViewModelMap.ToModel).ToList()),
        //    };
        //}

        //public static Collection<OrdersViewModel> ToOrdersViewModel(Collection<OrderModel> model)
        //{
        //    var ordersModel = new Collection<OrdersViewModel>(model.Select(ToOrderViewModel).OrderByDescending(x => x.OrderId).ToList());
            
        //    return ordersModel;
        //}

        //public static OrdersViewModel ToOrderViewModel(OrderModel model)
        //{
        //    if (model.OrderId == 0)
        //    {
        //        return null;
        //    }
        //    return new OrdersViewModel()
        //        {
        //            OrderId = model.OrderId,
        //            OrderDate = model.OrderDate.GetValueOrDefault().ToShortDateString(),
        //            OrderTotal = model.Total.GetValueOrDefault(0),
        //            PaymentMethod = (model.PaymentTypeId != null) ? ((EnumPaymentType)model.PaymentTypeId).ToString(): string.Empty,
        //            AccountId = model.AccountId,
        //            SubTotal = model.SubTotal.GetValueOrDefault(0),
        //            Discount = -model.DiscountAmount.GetValueOrDefault(0),
        //            Tax = model.TaxCost.GetValueOrDefault(0),
        //            Shipping = model.ShippingCost.GetValueOrDefault(0),
        //            OrderLineItems = ToOrderLineItemViewModel(model.OrderLineItems),
        //            BillingAddress = AddressViewModelMap.ToModel(model),
        //            ShippingAddress = AddressViewModelMap.ToModel(model.OrderLineItems.First().OrderShipment),
        //        };
        //}

        //public static Collection<OrderLineItemViewModel> ToOrderLineItemViewModel(Collection<OrderLineItemModel> model)
        //{
        //    var orderLineItemModel =
        //        new Collection<OrderLineItemViewModel>(model.Select(x => new OrderLineItemViewModel()
        //            {
        //                OrderLineItemId   = x.OrderLineItemId,
        //                Quantity = x.Quantity.GetValueOrDefault(0),
        //                Price = x.Price.GetValueOrDefault(0),
        //                Total = (x.Quantity * x.Price).GetValueOrDefault(0),
        //                Discount = x.DiscountAmount.GetValueOrDefault(0),
        //                Tax = x.SalesTax.GetValueOrDefault(0),
        //                Shipping = x.ShippingCost.GetValueOrDefault(0),
        //                Name = x.Name,
        //                Description=x.Description,
        //                ParentOrderLineItemId = x.ParentOrderLineItemId,
        //                OrderLineItemRelationshipTypeId = x.OrderLineItemRelationshipTypeId
        //            }).ToList());

        //    foreach (var orderLineItemViewModel in orderLineItemModel.Where(x => x.ParentOrderLineItemId == null))
        //    {
        //        var childLineItems = orderLineItemModel.Where(x => x.ParentOrderLineItemId == orderLineItemViewModel.OrderLineItemId);
        //        if (childLineItems.Any())
        //        {
        //            orderLineItemViewModel.Price += childLineItems.Sum(x => x.Price);
        //            orderLineItemViewModel.Total += childLineItems.Sum(x => x.Total);
        //        }
        //    }

        //    return new Collection<OrderLineItemViewModel>(orderLineItemModel.Where(y => y.ParentOrderLineItemId == null).ToList());
        //}

        //public static AccountModel ToSignUpModel(RegisterViewModel model)
        //{
        //    var signUpModel = new AccountModel()
        //        {
        //           User = new UserModel()
        //               {
        //                   Username = model.UserName,
        //                   Password = model.Password,
        //                   Email = model.EmailAddress,
        //                   PasswordQuestion = model.SelectedSecurityQuestion,
        //                   PasswordAnswer = model.SecurityAnswer,
        //                   IsApproved = true,                           
        //               },
        //               IsActive = true,
        //               EmailOptIn = model.EmailOptIn
                       
        //        };

        //    return signUpModel;
        //}
        
        //public static WishListViewModel ToWishListViewModel(Collection<WishListModel> wishListModel)
        //{
        //    var wishListView = new WishListViewModel();
        //   wishListView.Items = new Collection<WishListItemViewModel>(
        //       wishListModel.Select(x => new WishListItemViewModel()
        //    {
        //        ProductId = x.ProductId,
        //        AccountId = x.AccountId,
        //        CreateDate = x.CreateDate,
        //        Custom = x.Custom,
        //        WishListId = x.WishListId,
        //    }).ToList());

        //    return wishListView;
        //}
        //public static WishListItemViewModel ToWishListViewModel(WishListModel wishListModel)
        //{
        //    var wishListView = new WishListItemViewModel();
        //    wishListView.AccountId = wishListModel.AccountId;
        //    wishListView.CreateDate = wishListModel.CreateDate;

        //    return new WishListItemViewModel()
        //        {
        //            ProductId = wishListModel.ProductId,
        //            AccountId = wishListModel.AccountId,
        //            CreateDate = wishListModel.CreateDate,
        //            Custom = wishListModel.Custom,
        //            WishListId = wishListModel.WishListId,
        //        };
        //}

        //public static WishListModel ToWishListModel(int accountId, int productId)
        //{
        //    var wishList = new WishListModel()
        //    {
        //        ProductId = productId,
        //        AccountId = accountId,
        //        CreateDate = DateTime.Today,
        //        Custom = string.Empty,
        //    };

        //    return wishList;
        //}

        //public static AccountModel ToChangePasswordModel(ChangePasswordViewModel model)
        //{
        //    var changePasswordModel = new AccountModel()
        //        {
        //            User = new UserModel()
        //                {
        //                    Username = model.UserName,
        //                    Password = model.OldPassword,
        //                    NewPassword = model.NewPassword,
        //                    PasswordToken=model.PasswordToken,
        //                }
        //        };

        //    return changePasswordModel;
        //}
    }
}