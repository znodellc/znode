﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Mapper model for Address
    /// </summary>
    public class AddressViewModelMap
    {
        /// <summary>
        /// Mapping of Address Model to Address View Model
        /// </summary>
        /// <param name="model">Object of Address View Model</param>
        /// <returns>Address Model</returns>
        public static AddressModel ToModel(AddressViewModel model)
        {
            return new AddressModel()
            {
                AccountId = model.AccountId,
                AddressId = model.AddressId,
                City = model.City,
                CompanyName = model.CompanyName,
                FirstName = model.FirstName,
                IsDefaultBilling = model.IsDefaultBilling,
                IsDefaultShipping = model.IsDefaultShipping,
                LastName = model.LastName,
                Name = model.Name,
                MiddleName = model.MiddleName,
                PhoneNumber = model.PhoneNumber,
                PostalCode = model.PostalCode,
                StateCode = model.StateCode,
                StreetAddress1 = model.StreetAddress1,
                StreetAddress2 = model.StreetAddress2,
                CountryCode = model.CountryCode,
            };
        }

        /// <summary>
        /// Mapping of Address View Model to Address Model 
        /// </summary>
        /// <param name="model">Object of Address Model</param>
        /// <returns>Address View Model</returns>
        public static AddressViewModel ToViewModel(AddressModel model)
        {
            return new AddressViewModel()
            {
                AccountId = model.AccountId,
                AddressId = model.AddressId,
                City = model.City,
                CompanyName = model.CompanyName,
                FirstName = model.FirstName,
                IsDefaultBilling = model.IsDefaultBilling,
                IsDefaultShipping = model.IsDefaultShipping,
                LastName = model.LastName,
                Name = model.Name,
                MiddleName = model.MiddleName,
                PhoneNumber = model.PhoneNumber,
                PostalCode = model.PostalCode,
                StateCode = model.StateCode,
                StreetAddress1 = model.StreetAddress1,
                StreetAddress2 = model.StreetAddress2,
                CountryCode = model.CountryCode,
            };             
        }
    }
}