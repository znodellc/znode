﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public class SetupViewModelMap
    {
        /// <summary>
        /// Mapper to convert CategoryModel to CategoryViewModel 
        /// </summary>
        /// <param name="categoryModel"></param>
        /// <returns>CategoryViewModel</returns>
        public static CategoryViewModel ToCategoryViewModel(CategoryModel categoryModel)
        {
            return new CategoryViewModel()
            {
                CategoryName = categoryModel.Name,
                CategoryTitle = categoryModel.Title,
                ImageAltText = categoryModel.ImageAltTag,
                SeoTitle = categoryModel.SeoTitle,
                SeoKeyword = categoryModel.SeoKeywords,
                SeoDescription = categoryModel.SeoDescription,
                ShortDescription = categoryModel.ShortDescription,
                CategoryBanner = categoryModel.CategoryBanner,
                ChildCategories = categoryModel.SubcategoryGridIsVisible,
                DisplayOrder = categoryModel.DisplayOrder,
                ImagePath = categoryModel.ImageFile,
                SeoFriendlyPageName = categoryModel.SeoUrl
            };
        }

       /// <summary>
        /// Mapper to convert CategoryViewModel to CategoryModel
       /// </summary>
       /// <param name="categoryViewModel"></param>
       /// <returns>CategoryModel</returns>
        public static CategoryModel ToCategoryModel(CategoryViewModel categoryViewModel)
        {
            return new CategoryModel()
            {
                Name = categoryViewModel.CategoryName,
                Title = categoryViewModel.CategoryTitle,
                ImageAltTag = categoryViewModel.ImageAltText,
                SeoTitle = categoryViewModel.SeoTitle,
                SeoKeywords = categoryViewModel.SeoKeyword,
                SeoDescription = categoryViewModel.SeoDescription,
                ShortDescription = categoryViewModel.ShortDescription,
                CategoryBanner = categoryViewModel.CategoryBanner,
                SubcategoryGridIsVisible = categoryViewModel.ChildCategories,
                DisplayOrder = categoryViewModel.DisplayOrder,
                ImageFile = categoryViewModel.ImagePath,
                SeoUrl=categoryViewModel.SeoFriendlyPageName,

            };
        }            

        /// <summary>
        /// Mapper to convert PaymentOptionModel to PaymentViewModel
        /// </summary>
        /// <param name="paymentOptionModel"></param>
        /// <returns>PaymentViewModel</returns>
        public static PaymentViewModel ToPaymentViewModel(PaymentOptionModel paymentOptionModel)
        {
            return new PaymentViewModel()
            {
                TransactionKey = paymentOptionModel.TransactionKey,
                DisplayOrder = paymentOptionModel.DisplayOrder,
                EnableRMA = paymentOptionModel.IsRmaCompatible,
                EnableTestModel = paymentOptionModel.TestMode,
                Visa = paymentOptionModel.EnableVisa,
                MasterCard = paymentOptionModel.EnableMasterCard,
                AmericanExpress = paymentOptionModel.EnableAmericanExpress,
                Discover = paymentOptionModel.EnableDiscover,
                PreAuthorizeTransactionsWithoutCapturing = paymentOptionModel.PreAuthorize,
                Partner = paymentOptionModel.Partner,
                Vendor = paymentOptionModel.Vendor,
                Password = paymentOptionModel.PaymentGatewayPassword,
                MerchantLogin = paymentOptionModel.PaymentGatewayUsername
            };
        }

        /// <summary>
        /// Mapper to convert PaymentViewModel to PaymentOptionModel  
        /// </summary>
        /// <param name="paymentOptionModel"></param>
        /// <returns>PaymentOptionModel</returns>
        public static PaymentOptionModel ToPaymentOptionModel(PaymentViewModel paymentViewModel)
        {
            return new PaymentOptionModel()
            {
                TransactionKey = paymentViewModel.TransactionKey,
                DisplayOrder = paymentViewModel.DisplayOrder,
                IsRmaCompatible = paymentViewModel.EnableRMA,
                TestMode = paymentViewModel.EnableTestModel,
                EnableVisa = paymentViewModel.Visa,
                EnableMasterCard = paymentViewModel.MasterCard,
                EnableAmericanExpress = paymentViewModel.AmericanExpress,
                EnableDiscover = paymentViewModel.Discover,
                PreAuthorize = paymentViewModel.PreAuthorizeTransactionsWithoutCapturing,
                Partner = paymentViewModel.Partner,
                Vendor = paymentViewModel.Vendor,
                PaymentGatewayPassword = paymentViewModel.Password,
                PaymentGatewayUsername = paymentViewModel.MerchantLogin
            };
        }

        /// <summary>
        /// Mapper to convert MessageConfigModel to ManageMessageViewModel  
        /// </summary>
        /// <param name="paymentOptionModel"></param>
        /// <returns>ManageMessageViewModel</returns>
        public static ManageMessageViewModel ToManageMessageViewModel(MessageConfigModel messageConfigModel)
        {
            return new ManageMessageViewModel()
            {
                MessageKey = messageConfigModel.Key,
                Description = messageConfigModel.Description,
                Value = messageConfigModel.Value
            };
        }

        /// <summary>
        /// Mapper to convert ManageMessageViewModel to MessageConfigModel   
        /// </summary>
        /// <param name="paymentOptionModel"></param>
        /// <returns>MessageConfigModel</returns>
        public static MessageConfigModel ToMessageConfigModel(ManageMessageViewModel manageMessageViewModel)
        {
            return new MessageConfigModel()
            {
                Key = manageMessageViewModel.MessageKey,
                Description = manageMessageViewModel.Description,
                Value = manageMessageViewModel.Value
            };
        }

        /// <summary>
        /// Mapper to convert TaxClassModel to TaxViewModel   
        /// </summary>
        /// <param name="paymentOptionModel"></param>
        /// <returns>TaxViewModel</returns>
        public static TaxViewModel ToTaxViewModel(TaxClassModel taxClassModel)
        {
            return new TaxViewModel()
            {
                TaxClassName = taxClassModel.Name,
                DisplayOrder = taxClassModel.DisplayOrder,
                Enable = taxClassModel.IsActive
            };
        }

        /// <summary>
        /// Mapper to convert TaxViewModel to TaxClassModel    
        /// </summary>
        /// <param name="paymentOptionModel"></param>
        /// <returns>TaxClassModel</returns>
        public static TaxClassModel ToTaxClassModel(TaxViewModel taxViewModel)
        {
            return new TaxClassModel()
            {
                Name = taxViewModel.TaxClassName,
                DisplayOrder = taxViewModel.DisplayOrder,
                IsActive = taxViewModel.Enable
            };
        }

        /// <summary>
        /// Mapper to convert TaxRuleModel to TaxRulesViewModel    
        /// </summary>
        /// <param name="paymentOptionModel"></param>
        /// <returns>TaxRulesViewModel</returns>
        public static TaxRulesViewModel ToTaxRulesViewModel(TaxRuleModel taxRuleModel)
        {
            return new TaxRulesViewModel()
            {
                Precedence=taxRuleModel.Precedence,
                SalesTax=taxRuleModel.SalesTax,
                VATTax=taxRuleModel.Vat,
                GSTTax=taxRuleModel.Gst,
                PSTTax=taxRuleModel.Pst,
                HSTTax =taxRuleModel.Hst,
                InclusiveTax=taxRuleModel.IsInclusive
            };
        }

        /// <summary>
        /// Mapper to convert TaxRulesViewModel to TaxRuleModel  
        /// </summary>
        /// <param name="paymentOptionModel"></param>
        /// <returns>TaxRuleModel</returns>
        public static TaxRuleModel ToTaxRuleModel(TaxRulesViewModel taxRulesViewModel)
        {
            return new TaxRuleModel()
            {
                Precedence = taxRulesViewModel.Precedence,
                SalesTax = taxRulesViewModel.SalesTax,
                Vat = taxRulesViewModel.VATTax,
                Gst = taxRulesViewModel.GSTTax,
                Pst = taxRulesViewModel.PSTTax,
                Hst = taxRulesViewModel.HSTTax,
                IsInclusive = taxRulesViewModel.InclusiveTax
            };
        }

        /// <summary>
        /// Mapper to convert ShippingOptionModel to ShippingViewModel  
        /// </summary>
        /// <param name="paymentOptionModel"></param>
        /// <returns>ShippingViewModel</returns>
        public static ShippingViewModel ToShippingViewModel(ShippingOptionModel shippingOptionModel)
        {
            return new ShippingViewModel()
            {
                DisplayName = shippingOptionModel.Description,
                InternalCode = shippingOptionModel.ShippingCode,
                DisplayOrder = shippingOptionModel.DisplayOrder,
                HandlingCharge = shippingOptionModel.HandlingCharge,
                Enable = shippingOptionModel.IsActive,

                BaseCost = shippingOptionModel.ShippingRule.BaseCost,
                PerUnitCost = shippingOptionModel.ShippingRule.PerItemCost,
                LowerLimit = shippingOptionModel.ShippingRule.LowerLimit,
                UpperLimit = shippingOptionModel.ShippingRule.UpperLimit
            };
        }

        /// <summary>
        /// Mapper to convert ShippingViewModel to ShippingOptionModel  
        /// </summary>
        /// <param name="paymentOptionModel"></param>
        /// <returns>ShippingOptionModel</returns>
        public static ShippingOptionModel ToShippingOptionModel(ShippingViewModel shippingViewModel)
        {
            return new ShippingOptionModel()
            {
                Description = shippingViewModel.DisplayName,
                ShippingCode = shippingViewModel.InternalCode,
                DisplayOrder = shippingViewModel.DisplayOrder,
                HandlingCharge = shippingViewModel.HandlingCharge,
                IsActive = shippingViewModel.Enable,

                ShippingRule = new ShippingRuleModel()
                {
                    BaseCost = shippingViewModel.BaseCost,
                    PerItemCost = shippingViewModel.PerUnitCost,
                    LowerLimit = shippingViewModel.LowerLimit,
                    UpperLimit = shippingViewModel.UpperLimit,
                }
            };
        }
    }
}