﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    /// <summary>
    /// Static class for Supplier/Vendor Mapper
    /// </summary>
    public static class SupplierViewModelMap
    {
        /// <summary>
        /// Convert SupplierAccountViewModel to SupplierModel
        /// </summary>
        /// <param name="supplierAccountViewModel">to convert to model</param>
        /// <returns>SupplierModel</returns>
        public static SupplierModel ToSupplierModel(SupplierAccountViewModel supplierAccountViewModel)
        {

            return new SupplierModel()
            {
                Name = supplierAccountViewModel.Name,
                ContactFirstName = supplierAccountViewModel.FirstName,
                ContactLastName = supplierAccountViewModel.LastName,
                ContactEmail = supplierAccountViewModel.Email,
                ContactPhone = supplierAccountViewModel.PhoneNumber,

                Address = new AddressModel
                {
                    CompanyName = supplierAccountViewModel.CompanyName,
                    StreetAddress1 = supplierAccountViewModel.Street1,
                    StreetAddress2 = supplierAccountViewModel.Street2,
                    City = supplierAccountViewModel.City,
                    StateCode = supplierAccountViewModel.State,
                    PostalCode = supplierAccountViewModel.PostalCode,


                },
                Account = new AccountModel
                {
                    AccountId = supplierAccountViewModel.AccountId,
                }
            };
        }

        /// <summary>
        /// Convert SupplierModel to SupplierAccountViewModel
        /// </summary>
        /// <param name="supplierModel">to convert to view model</param>
        /// <returns>SupplierAccountViewModel</returns>
        public static SupplierAccountViewModel ToSupplierAccountViewModel(SupplierModel supplierModel)
        {
            return new SupplierAccountViewModel()
            {
                Name=supplierModel.Name,
                FirstName=supplierModel.ContactFirstName,
                LastName=supplierModel.ContactLastName,
                Email=supplierModel.ContactEmail,
                PhoneNumber=supplierModel.ContactPhone,
               CompanyName=supplierModel.Address.CompanyName,
               Street1=supplierModel.Address.StreetAddress1,
               Street2=supplierModel.Address.StreetAddress2,
               City=supplierModel.Address.City,
               State=supplierModel.Address.StateCode,
               PostalCode=supplierModel.Address.PostalCode
            };
        } 
    }
}
