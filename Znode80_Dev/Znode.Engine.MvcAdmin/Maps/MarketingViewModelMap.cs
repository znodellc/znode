﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcAdmin.ViewModels;

namespace Znode.Engine.MvcAdmin.Maps
{
    public static class MarketingViewModelMap
    {
        #region Gigt Card View Model Map

        public static GiftCardModel ToGiftCardModel(GiftCardViewModel giftCardViewModel)
        {
            return new GiftCardModel()
            {
                Name = giftCardViewModel.Name,
                Amount = giftCardViewModel.Amount,
                CreateDate = giftCardViewModel.CreateDate,
                ExpirationDate = giftCardViewModel.ExpirationDate,
                PortalId = giftCardViewModel.PortalId,
                OrderLineItemId = giftCardViewModel.OrderLineItemId,
                GiftCardId = giftCardViewModel.GiftCardId,
                CreatedBy = giftCardViewModel.CreatedBy,
                CardNumber = giftCardViewModel.CardNumber
            };
        }

        public static GiftCardViewModel ToGiftCardViewModel(GiftCardModel giftCardModel)
        {
            return new GiftCardViewModel()
            {
                Name = giftCardModel.Name,
                Amount = giftCardModel.Amount,
                CreateDate = giftCardModel.CreateDate,
                ExpirationDate = giftCardModel.ExpirationDate,
                PortalId = giftCardModel.PortalId,
                OrderLineItemId = giftCardModel.OrderLineItemId,
                GiftCardId = giftCardModel.GiftCardId,
                CreatedBy = giftCardModel.CreatedBy,
                CardNumber = giftCardModel.CardNumber
            };
        } 

        #endregion

        #region Review View Model Map
        public static ReviewModel ToReviewModel(ProductReviewViewModel reviewViewModel)
        {
            return new ReviewModel()
            {
                ReviewId = reviewViewModel.ReviewId,
                Status = reviewViewModel.Status,
                Subject = reviewViewModel.HeadLine,
                UserLocation = reviewViewModel.UserLocation,
                Comments = reviewViewModel.Comments,
                CreateDate = reviewViewModel.CreateDate,
                CreateUser = reviewViewModel.CreateUser,
                Product = new ProductModel
                {
                    Name = reviewViewModel.ProductName
                }
            };
        }

        public static ProductReviewViewModel ToReviewViewModel(ReviewModel reviewModel)
        {
            return new ProductReviewViewModel()
            {
                ProductName = reviewModel.Product.Name,
                ReviewId = reviewModel.ReviewId,
                Status = reviewModel.Status,
                HeadLine = reviewModel.Subject,
                UserLocation = reviewModel.UserLocation,
                Comments = reviewModel.Comments,
                CreateDate = reviewModel.CreateDate,
                CreateUser = reviewModel.CreateUser,
            };
        } 
        #endregion

        #region Search Facet View Model Map
        public static SearchFacetModel ToSearchFacetModel(FacetViewModel facetViewModel)
        {
            return new SearchFacetModel()
            {
                AttributeName = facetViewModel.FacetValue
            };
        }

        public static FacetViewModel ToSearchFacetModel(SearchFacetModel searchFacetModel)
        {
            return new FacetViewModel()
            {
                FacetValue = searchFacetModel.AttributeName
            };
        } 
        #endregion

        public static FacetGroupViewModel ToFacaetGroupViewModel()
        {
            return null;
        }
    }
}