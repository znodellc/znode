using System;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// Gateway Response
    /// </summary>
    public class GatewayResponse : ZNodePaymentBase
    {
        #region Private Member Instance Variables
        
        /// <summary>
        /// Represents the Gateway Response code as a string.
        /// </summary>
        private string _ResponseCode = string.Empty;
        
        /// <summary>
        ///  Represents the Gateway Response code as a string.
        /// </summary>
        private string _ResponseText = string.Empty;
        
        /// <summary>
        /// Represents the Gateway Response transaction id as a string.
        /// </summary>
        private string _TransactionId;
        
        /// <summary>
        /// Represents the Gateway Response transaction id as a string.
        /// </summary>
        private string _ProfileId;
        
        /// <summary>
        /// Represents the Gateway Response code as boolean value
        /// </summary>
        private bool _IsSuccess = false;    
        
        /// <summary>
        /// Represents the Gateway AVSResponse code as a string.
        /// </summary>
        private string _AVSResponsecode = string.Empty;
        
        /// <summary>
        /// Represents the Gateway CCVResponse code as a string.
        /// </summary>
        private string _CCVResponsecode = string.Empty;
        
        /// <summary>
        /// Respresents the Gateway Unique transaction reference number
        /// </summary>
        private string _ReferenceNumber = string.Empty;
       
        /// <summary>
        /// Represents the Paypal Gateway express checkout token 
        /// </summary>        
        private string _Token = string.Empty;
        
        /// <summary>
        ///  Represents the Paypal Gateway express checkout payerID
        /// </summary>
        private string _PayerID = string.Empty;
        
        /// <summary>
        /// Represents the Paypal payer address
        /// </summary>
        private Address _CustomerAddress = new Address();
        
        /// <summary>
        /// Represents the ApprovalCode from the gateway
        /// </summary>
        private string _ApprovalCode = string.Empty;       

        // Retry Logic Response.

        /// <summary>
        /// Represents the RetryTrace number.
        /// </summary>
        private string _Retrytrace = string.Empty;
        
        /// <summary>
        /// Represents the number of requests processed by the Gateway with the same retryTrace number. 
        /// </summary>
        private string _RetryAttempcount = string.Empty;
        
        /// <summary>
        /// Represents the Last retry date.
        /// </summary>
        private string _LastRetryDate = string.Empty;
        
        /// <summary>
        /// Represents Express Checkout redirect URL
        /// </summary>
        private string _RedirectURL = string.Empty;

        /// <summary>
        /// Represents the Word Pay Header cookie
        /// </summary>
        private string _WorldPayHeaderCookie = string.Empty;
        
        /// <summary>
        /// Represents the Issuer post data
        /// </summary>
        private string _IssuerPostData = string.Empty;

        /// <summary>
        /// Represents the Echo Data
        /// </summary>
        private string _EchoData = string.Empty;

        /// <summary>
        /// Represents the world pay post data
        /// </summary>
        private string _worldpayPostData = string.Empty;

        /// <summary>
        /// PayFlow Pro Card Auth Code
        /// </summary>
        private string _cardAuthCode = string.Empty;

        /// <summary>
        /// Represents the card type id
        /// </summary>
        private string _cardTypeId = string.Empty;

        /// <summary>
        /// Represents the card number
        /// </summary>
        private string _cardNumber = string.Empty;

        /// <summary>
        /// To get the raw data from CyberSource
        /// </summary>
        private string _GatewayResponseData = string.Empty;

        /// <summary>
        /// Represents the transaction type
        /// </summary>
        private string _TransactionType = string.Empty;

        /// <summary>
        /// Represents the subscription id
        /// </summary>
        private string _SubscriptionID = string.Empty;

        /// <summary>
        /// Represents the Payment Profile Id
        /// </summary>
        private string _PaymentProfileID = string.Empty;

        /// <summary>
        /// Represents the Shipping Address id
        /// </summary>
        private string _ShippingAddressID = string.Empty;

        #endregion

        #region Public Instance properties

        /// <summary>
        /// Gets or sets the Gateway Response code
        /// </summary>
        public string ResponseCode
        {
            get { return this._ResponseCode; }
            set { this._ResponseCode = value; }
        }

        /// <summary>
        /// Gets or sets the Gateway Response Text
        /// </summary>
        public string ResponseText
        {
            get { return this._ResponseText; }
            set { this._ResponseText = value; }
        }

        /// <summary>
        /// Gets or sets the Gateway TransactionID
        /// </summary>
        public string TransactionId
        {
            get { return this._TransactionId; }
            set { this._TransactionId = value; }
        }

        /// <summary>
        /// Gets or sets the payment profile Id
        /// </summary>
        public string ProfileId
        {
            get { return this._ProfileId; }
            set { this._ProfileId = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Gateway Response is success or not
        /// </summary>
        public bool IsSuccess
        {
            get { return this._IsSuccess; }
            set { this._IsSuccess = value; }
        }

        /// <summary>
        /// Gets or sets the Gateway AVSResponse Code.
        /// </summary>
        public string AVSResponseCode
        {
            get { return this._AVSResponsecode; }
            set { this._AVSResponsecode = value; }
        }

        /// <summary>
        /// Gets or sets the Gateway CCVResponse Code.
        /// </summary>
        public string CCVResponsecode
        {
            get { return this._CCVResponsecode; }
            set { this._CCVResponsecode = value; }
        }

        /// <summary>
        /// Gets or sets the Gateway Transaction Reference number.
        /// </summary>
        public string ReferenceNumber
        {
            get { return this._ReferenceNumber; }
            set { this._ReferenceNumber = value; }
        }

        /// <summary>
        ///  Gets or sets the Paypal Gateway token
        /// </summary>
        public string PaypalECtoken
        {
            get { return this._Token; }
            set { this._Token = value; }
        }

        /// <summary>
        ///  Gets or sets the Paypal Gateway payer ID
        /// </summary>
        public string PaypalPayerID
        {
            get { return this._PayerID; }
            set { this._PayerID = value; }
        }

        /// <summary>
        /// Gets or sets the Paypal-payer address
        /// </summary>
        public Address PayerAddress
        {
            get { return this._CustomerAddress; }
            set { this._CustomerAddress = value; }
        }

        /// <summary>
        /// Gets or sets the approval code
        /// </summary>
        public string ApprovalCode
        {
            get { return this._ApprovalCode; }
            set { this._ApprovalCode = value; }
        }        

        // Retry Logic 

        /// <summary>
        /// Gets or sets the Retry trace number.
        /// </summary>
        public string RetryTrace
        {
            get { return this._Retrytrace; }
            set { this._Retrytrace = value; }
        }

        /// <summary>
        /// Gets or sets the RetryAttempCount
        /// </summary>
        public string RetryAttemptCount
        {
            get { return this._RetryAttempcount; }
            set { this._RetryAttempcount = value; }
        }

        /// <summary>
        /// Gets or sets the Last RetryDate.
        /// </summary>
        public string LastRetryDate
        {
            get { return this._LastRetryDate; }
            set { this._LastRetryDate = value; }
        }

        /// <summary>
        /// Gets or sets the EC Redirect URL.
        /// </summary>
        public string ECRedirectURL
        {
            get { return this._RedirectURL; }
            set { this._RedirectURL = value; }
        }

        /// <summary>
        /// Gets or sets the Worldpay Request header cookie.
        /// </summary>
        public string WorldPayHeaderCookie
        {
            get { return this._WorldPayHeaderCookie; }
            set { this._WorldPayHeaderCookie = value; }
        }

        /// <summary>
        /// Gets or sets the Worldpay IssuerUrl data
        /// </summary>
        public string IssuerPostData
        {
            get { return this._IssuerPostData; }
            set { this._IssuerPostData = value; }
        }

        /// <summary>
        /// Gets or sets the Worldpay echodata
        /// </summary>
        public string EchoData
        {
            get { return this._EchoData; }
            set { this._EchoData = value; }
        }

        /// <summary>
        /// Gets or sets the parequest from Worldpay response
        /// </summary>
        public string WorldPayPostData
        {
            get { return this._worldpayPostData; }
            set { this._worldpayPostData = value; }
        }

        /// <summary>
        /// Gets or sets the card auth code
        /// </summary>
        public string CardAuthCode
        {
            get { return this._cardAuthCode; }
            set { this._cardAuthCode = value; }
        }

        /// <summary>
        /// Gets or sets the card type id
        /// </summary>
        public string CardTypeID
        {
            get { return this._cardTypeId; }
            set { this._cardTypeId = value; }
        }

        /// <summary>
        /// Gets or sets the card number
        /// </summary>
        public string CardNumber
        {
            get { return this._cardNumber; }
            set { this._cardNumber = value; }
        }

        /// <summary>
        /// Gets or sets the To save the raw response of CyberSource
        /// </summary>
        public string GatewayResponseData
        {
            get { return this._GatewayResponseData; }
            set { this._GatewayResponseData = value; }
        }

        /// <summary>
        /// Gets or sets the Gateway Transaction Type
        /// </summary>
        public string TransactionType
        {
            get { return this._TransactionType; }
            set { this._TransactionType = value; }
        }

        /// <summary>
        /// Gets or sets the Subscription id
        /// </summary>
        public string SubscriptionID
        {
            get { return this._SubscriptionID; }
            set { this._SubscriptionID = value; }
        }

        /// <summary>
        /// Gets or sets the Payment Profile id
        /// </summary>
        public string PaymentProfileID
        {
            get { return this._PaymentProfileID; }
            set { this._PaymentProfileID = value; }
        }

        /// <summary>
        /// Gets or sets the shipping address id
        /// </summary>
        public string ShippingAddressID
        {
            get { return this._ShippingAddressID; }
            set { this._ShippingAddressID = value; }
        }
     #endregion
    }
}
