using System;
using System.Text.RegularExpressions;
using com.paypal.sdk.profiles;
using com.paypal.sdk.services;
using com.paypal.soap.api;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// PayPal Gateway
    /// </summary>
    public class GatewayPaypal : ZNodePaymentBase
    {
        #region Protected Member Variables
        /// <summary>
        /// Read only Instance object for Call Services 
        /// </summary>
        private readonly CallerServices caller;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the GatewayPaypal class.
        /// </summary>
        /// <param name="Gateway">Gateway Info</param>
        public GatewayPaypal(GatewayInfo Gateway)
        {
            this.caller = new CallerServices();
            this.caller.APIProfile = CreateAPIProfile(Gateway.GatewayLoginID, Gateway.GatewayPassword, Gateway.TransactionKey, Gateway.TestMode);
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Submit payment to paypal direct payment gateway
        /// </summary>
        /// <param name="GatewayInfo">Gateway Info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCardInfo">Credit Card Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse SubmitPayment(GatewayInfo GatewayInfo, Address BillingAddress, Address ShippingAddress, CreditCard CreditCardInfo)
        {
            // Local variables
            // Create the response object
            GatewayResponse _response = new GatewayResponse();

            // Create the request object
            DoDirectPaymentRequestType directPayRequest = new DoDirectPaymentRequestType();

            // Create the request details object
            directPayRequest.DoDirectPaymentRequestDetails = new DoDirectPaymentRequestDetailsType();

            // Get local machine ip address
            directPayRequest.DoDirectPaymentRequestDetails.IPAddress = GatewayInfo.CustomerIPAddress;

            // CreditCard Info
            directPayRequest.DoDirectPaymentRequestDetails.MerchantSessionId = System.Guid.NewGuid().ToString();
            directPayRequest.DoDirectPaymentRequestDetails.PaymentAction = (PaymentActionCodeType)Enum.Parse(typeof(PaymentActionCodeType), GatewayInfo.PaymentActionCodeType);
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard = new CreditCardDetailsType();
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CreditCardNumber = CreditCardInfo.CardNumber;
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CreditCardType = this.GetCreditCardType(CreditCardInfo.CardNumber);            
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CVV2 = CreditCardInfo.CardSecurityCode;
            string[] expirationDate = CreditCardInfo.CreditCardExp.Split(new char[] { '/' });
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.ExpMonth = int.Parse(expirationDate[0]);
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.ExpYear = int.Parse(expirationDate[1]);
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.ExpMonthSpecified = true;
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.ExpYearSpecified = true;

            // PaymentDetails 
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CardOwner = new PayerInfoType();
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CardOwner.Payer = BillingAddress.EmailId;
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CardOwner.PayerID = string.Empty;            
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CardOwner.Address = new AddressType();
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CardOwner.Address.Street1 = BillingAddress.Street1;
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CardOwner.Address.Street2 = BillingAddress.Street2;
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CardOwner.Address.CityName = BillingAddress.City;
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CardOwner.Address.StateOrProvince = BillingAddress.StateCode;
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CardOwner.Address.PostalCode = BillingAddress.PostalCode;
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CardOwner.Address.CountryName = BillingAddress.CountryCode;
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CardOwner.Address.Country = (CountryCodeType)Enum.Parse(typeof(CountryCodeType), BillingAddress.CountryCode);             
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CardOwner.Address.CountrySpecified = true;

            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CardOwner.PayerName = new PersonNameType();
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CardOwner.PayerName.FirstName = BillingAddress.FirstName;
            directPayRequest.DoDirectPaymentRequestDetails.CreditCard.CardOwner.PayerName.LastName = BillingAddress.LastName;
            directPayRequest.DoDirectPaymentRequestDetails.PaymentDetails = new PaymentDetailsType();
            directPayRequest.DoDirectPaymentRequestDetails.PaymentDetails.OrderTotal = new BasicAmountType();

            // NOTE: The only currency supported by the Direct Payment API at this time is US dollars (USD).
            directPayRequest.DoDirectPaymentRequestDetails.PaymentDetails.OrderTotal.currencyID = (CurrencyCodeType)Enum.Parse(typeof(CurrencyCodeType), GatewayInfo.CurrencyCode);
            directPayRequest.DoDirectPaymentRequestDetails.PaymentDetails.OrderTotal.Value = CreditCardInfo.Amount.ToString();

            DoDirectPaymentResponseType directPayResponse = (DoDirectPaymentResponseType)this.caller.Call("DoDirectPayment", directPayRequest);

            if (directPayResponse.Ack == AckCodeType.Success || directPayResponse.Ack == AckCodeType.SuccessWithWarning)
            {
                _response.ResponseText = directPayResponse.Ack.ToString();
                _response.TransactionId = directPayResponse.TransactionID;
                _response.CardAuthCode = directPayResponse.TransactionID;
                _response.IsSuccess = true;
            }
            else
            {
                _response.ResponseText = directPayResponse.Errors[0].LongMessage;
                _response.ResponseCode = directPayResponse.Errors[0].ErrorCode;
            }

            return _response;
        }

        /// <summary>
        /// Submit payment to paypal direct payment gateway
        /// </summary>
        /// <param name="GatewayInfo">Gateway Info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCardInfo">Credit Card Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse SubmitRecurringPayment(GatewayInfo GatewayInfo, Address BillingAddress, Address ShippingAddress, CreditCard CreditCardInfo, RecurringBillingInfo RecurringBillingInfo)
        {
            // Local variables
            // Create the response object
            GatewayResponse _response = new GatewayResponse();
            CreateRecurringPaymentsProfileReq request = new CreateRecurringPaymentsProfileReq();

          

            request.CreateRecurringPaymentsProfileRequest = new CreateRecurringPaymentsProfileRequestType();
            request.CreateRecurringPaymentsProfileRequest.Version = "51.0";
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails = new CreateRecurringPaymentsProfileRequestDetailsType();
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.RecurringPaymentsProfileDetails = new RecurringPaymentsProfileDetailsType();
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.RecurringPaymentsProfileDetails.BillingStartDate = DateTime.Now;
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.RecurringPaymentsProfileDetails.ProfileReference = RecurringBillingInfo.ProfileName;
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.RecurringPaymentsProfileDetails.SubscriberName = BillingAddress.FirstName;

            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails = new ScheduleDetailsType();
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod = new BillingPeriodDetailsType();
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.Description = "Subscription";

            BasicAmountType amount = new BasicAmountType();
            amount.Value = RecurringBillingInfo.InitialAmount.ToString();

            amount.currencyID = CurrencyCodeType.USD;
            
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.Amount = amount;
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.BillingFrequency = Convert.ToInt32(RecurringBillingInfo.Frequency);
            if (RecurringBillingInfo.Period == "DAY")
                request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.BillingPeriod = BillingPeriodType.Day;
            if (RecurringBillingInfo.Period == "WEEK")
                request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.BillingPeriod = BillingPeriodType.Week;
            if (RecurringBillingInfo.Period == "MONTH")
                request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.BillingPeriod = BillingPeriodType.Month;
            if (RecurringBillingInfo.Period == "YEAR")
                request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.BillingPeriod = BillingPeriodType.Year;

            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.ScheduleDetails.PaymentPeriod.TotalBillingCycles = RecurringBillingInfo.TotalCycles;




            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard = new CreditCardDetailsType();
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CreditCardNumber = CreditCardInfo.CardNumber;
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CreditCardType = this.GetCreditCardType(CreditCardInfo.CardNumber);
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CVV2 = CreditCardInfo.CardSecurityCode;
            string[] expirationDate = CreditCardInfo.CreditCardExp.Split(new char[] { '/' });
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.ExpMonth = int.Parse(expirationDate[0]);
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.ExpYear = int.Parse(expirationDate[1]);
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.ExpMonthSpecified = true;
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.ExpYearSpecified = true;

            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CardOwner = new PayerInfoType();
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CardOwner.Payer = BillingAddress.EmailId;
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CardOwner.PayerID = string.Empty;
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CardOwner.Address = new AddressType();
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CardOwner.Address.Street1 = BillingAddress.Street1;
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CardOwner.Address.Street2 = BillingAddress.Street2;
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CardOwner.Address.CityName = BillingAddress.City;
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CardOwner.Address.StateOrProvince = BillingAddress.StateCode;
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CardOwner.Address.PostalCode = BillingAddress.PostalCode;
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CardOwner.Address.CountryName = BillingAddress.CountryCode;
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CardOwner.Address.Country = (CountryCodeType)Enum.Parse(typeof(CountryCodeType), BillingAddress.CountryCode);
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CardOwner.Address.CountrySpecified = true;

            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CardOwner.PayerName = new PersonNameType();
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CardOwner.PayerName.FirstName = BillingAddress.FirstName;
            request.CreateRecurringPaymentsProfileRequest.CreateRecurringPaymentsProfileRequestDetails.CreditCard.CardOwner.PayerName.LastName = BillingAddress.LastName;

      
            CreateRecurringPaymentsProfileResponseType recurringResponse = (CreateRecurringPaymentsProfileResponseType)this.caller.Call("CreateRecurringPaymentsProfile", request.CreateRecurringPaymentsProfileRequest);

            if (recurringResponse.Ack == AckCodeType.Success || recurringResponse.Ack == AckCodeType.SuccessWithWarning)
            {
                _response.ResponseText = recurringResponse.Ack.ToString();
                _response.TransactionId = recurringResponse.CorrelationID;
                _response.CardAuthCode = recurringResponse.CreateRecurringPaymentsProfileResponseDetails.ProfileID;
                _response.IsSuccess = true;
            }
            else
            {
                _response.ResponseText = recurringResponse.Errors[0].LongMessage;
                _response.ResponseCode = recurringResponse.Errors[0].ErrorCode;
            }

            return _response;
        }
       

       
        #endregion

        #region Private Methods
        /// <summary>
        /// Initiate Merchant details for an API
        /// </summary>
        /// <param name="apiUsername">API Username</param>
        /// <param name="apiPassword">API Password</param>
        /// <param name="signature">Signature of the merchant</param>
        /// <param name="IsTestMode">Test mode or not</param>
        /// <returns>Returns the profile</returns>
        private static IAPIProfile CreateAPIProfile(string apiUsername, string apiPassword, string signature, bool IsTestMode)
        {
            IAPIProfile profile = ProfileFactory.createSignatureAPIProfile();
            profile.APIUsername = apiUsername;
            profile.APIPassword = apiPassword;
            profile.APISignature = signature;
            profile.Subject = string.Empty;

            if (!IsTestMode)
            {
                // Test mode is not enabled,then set Profile Environment as "live" - Production Server
                profile.Environment = "live";
            }
            else
            {
                profile.Environment = "sandbox";
            }

            return profile;
        }

        /// <summary>
        /// Returns the card type and also it will ensure that the card is a valid length for the card type. If the
        /// card type isn't recognised it will return Solo by default.               
        /// </summary>
        /// <param name="cardNumber">Card number</param>
        /// <returns>Returns the credit card type</returns>
        private CreditCardTypeType GetCreditCardType(string cardNumber)
        {            
            // AMEX -- 34 or 37 -- 15 length
            if (Regex.IsMatch(cardNumber, "^(34|37)") && (15 == cardNumber.Length))
            {
                return CreditCardTypeType.Amex;
            }
            else if (Regex.IsMatch(cardNumber, "^(51|52|53|54|55)") && (16 == cardNumber.Length))
            {
                // MasterCard -- 51 through 55 -- 16 length
                return CreditCardTypeType.MasterCard;
            }
            else if (Regex.IsMatch(cardNumber, "^(4)") && (13 == cardNumber.Length || 16 == cardNumber.Length))
            {
                // VISA -- 4 -- 13 and 16 length
                return CreditCardTypeType.Visa;
            }
            else if (Regex.IsMatch(cardNumber, "^(6011)") && (16 == cardNumber.Length))
            {
                // Discover -- 6011 -- 16 length
                return CreditCardTypeType.Discover;
            }
            else
            {
                // Card type wasn't recognised, provided Unknown is in the CardTypes property, then return Solo
                return CreditCardTypeType.Solo;
            }
        }
        #endregion
    }
}
