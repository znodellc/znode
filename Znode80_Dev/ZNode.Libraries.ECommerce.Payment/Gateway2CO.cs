﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// Represents the Gateway 2CO
    /// </summary>
    public class Gateway2CO
    {
        private string serverURL = System.Configuration.ConfigurationManager.AppSettings["TwoCOCheckoutPath"];

        /// <summary>
        /// Submits credit card payment to a 2Checkout gateway
        /// </summary>
        /// <param name="Gateway">2CO PaymentSetting object</param>
        /// <param name="returnURL">Return URL after payment</param>
        /// <param name="insURL">INS Handler URL for payment notification</param>
        /// <returns>Returns true if payment is success else false</returns>
        public bool SubmitPayment(ZNode.Libraries.DataAccess.Entities.PaymentSetting Gateway, string returnURL, string insURL, ZNodeShoppingCart cart = null)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            if (cart == null)
            {
                cart = (ZNodeShoppingCart) ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
            }

            Dictionary<string, string> parameters = new Dictionary<string, string>();

            // Specify Vendor / Gateway information
            parameters.Add("sid", encrypt.DecryptData(Gateway.GatewayUsername));

            if (Gateway.TestMode)
            {
                parameters.Add("demo", "Y");
            }

            if (!string.IsNullOrEmpty(returnURL))
            {
                parameters.Add("return_url", returnURL);
            }

            if (!string.IsNullOrEmpty(insURL))
            {
                parameters.Add("x_receipt_link_url", insURL);
            }

            // paymentsetting id, for future INS reference via custom parameter
            parameters.Add("payment_setting_id", Gateway.PaymentSettingID.ToString());

            // send mode parameter to process PaymentInsHandler page.
            parameters.Add("mode", "2CO");

            // Specify no option selection on 2CO Cart page.
            parameters.Add("fixed", "Y");

            // Specify Customer information if he is logged in already.
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                ZNodeUserAccount user = ZNodeUserAccount.CurrentAccount();                

                parameters.Add("first_name", user.BillingAddress.FirstName);
                parameters.Add("last_name", user.BillingAddress.LastName);
                parameters.Add("street_address", user.BillingAddress.Street);
                parameters.Add("street_address2", user.BillingAddress.Street1);
                parameters.Add("city", user.BillingAddress.City);
                parameters.Add("state", user.BillingAddress.StateCode);
                parameters.Add("zip", user.BillingAddress.PostalCode);
                parameters.Add("country", user.BillingAddress.CountryCode);
                parameters.Add("email", user.EmailID);
                parameters.Add("phone", user.BillingAddress.PhoneNumber);

                parameters.Add("ship_name", string.Concat(user.ShippingAddress.FirstName, " ", user.ShippingAddress.LastName));
                parameters.Add("ship_street_address", user.ShippingAddress.Street);
                parameters.Add("ship_street_address2", user.ShippingAddress.Street1);
                parameters.Add("ship_city", user.ShippingAddress.City);
                parameters.Add("ship_state", user.ShippingAddress.StateCode);
                parameters.Add("ship_zip", user.ShippingAddress.PostalCode);
                parameters.Add("ship_country", user.ShippingAddress.CountryCode);

                parameters.Add("billing_addressId", user.BillingAddress.AddressID.ToString());
                parameters.Add("shipping_addressId", user.ShippingAddress.AddressID.ToString());
            }

            int itemNum = 0;
            for (int i = 0; i < cart.ShoppingCartItems.Count; i++)
            {

                // Set Product Name.
                if (cart.ShoppingCartItems[i].Product != null)
                {
                    parameters.Add(string.Format("li_{0}_name", itemNum), cart.ShoppingCartItems[i].Product.Name);
                    parameters.Add(string.Format("li_{0}_product_id", itemNum), cart.ShoppingCartItems[i].Product.ProductNum);
                    parameters.Add(string.Format("li_{0}_type", itemNum), "product");                    
                }

                // Set puchase quantity.
                parameters.Add(string.Format("li_{0}_quantity", itemNum), cart.ShoppingCartItems[i].Quantity.ToString());

                // Set product is shippable.
                string shippable = "Y";
                if (cart.ShoppingCartItems[i].Product.FreeShippingInd)
                {
                    shippable = "N";
                }
                parameters.Add(string.Format("li_{0}_tangible", itemNum), shippable);

                // Set the product price
                parameters.Add(string.Format("li_{0}_price", itemNum), cart.ShoppingCartItems[i].Product.FinalPrice.ToString("F2"));

                // Set the Options
                int addOnItemNum = 0;
                AddOnValueService aService = new AddOnValueService();
                foreach (ZNodeAddOnEntity AddOn in cart.ShoppingCartItems[i].Product.SelectedAddOns.AddOnCollection)
                {
                    foreach (ZNodeAddOnValueEntity addOnVal in AddOn.AddOnValueCollection) //Add-On value collection
                    {
                        AddOnValue addOnValue = aService.DeepLoadByAddOnValueID(Convert.ToInt32(addOnVal.AddOnValueID), true, DeepLoadType.IncludeChildren, typeof(AddOn));
                        parameters.Add(string.Format("li_{0}_option_{1}_name", itemNum, addOnItemNum), AddOn.Name);
                        parameters.Add(string.Format("li_{0}_option_{1}_value", itemNum, addOnItemNum), addOnValue.Name);
                        parameters.Add(string.Format("li_{0}_option_{1}_surcharge", itemNum, addOnItemNum), addOnValue.RetailPrice.ToString("F2"));
                        addOnItemNum = addOnItemNum + 1;
                    }
                }

                itemNum = itemNum + 1;
            }

            if (cart.Discount > 0)
            {
                parameters.Add(string.Format("li_{0}_name", itemNum), string.Format("Coupon: {0}", cart.Coupon));
                parameters.Add(string.Format("li_{0}_type", itemNum), "coupon");
                parameters.Add(string.Format("li_{0}_price", itemNum), cart.Discount.ToString("F2"));
                itemNum = itemNum + 1;
            }

            if (cart.TaxCost > 0)
            {
                parameters.Add(string.Format("li_{0}_name", itemNum), "TAX");
                parameters.Add(string.Format("li_{0}_type", itemNum), "tax");
                parameters.Add(string.Format("li_{0}_price", itemNum), cart.TaxCost.ToString("F2"));
                itemNum = itemNum + 1;
            }

            if (cart.Shipping != null && cart.ShippingCost > 0)
            {
                parameters.Add(string.Format("li_{0}_name", itemNum), cart.Shipping.ShippingName);
                parameters.Add(string.Format("li_{0}_type", itemNum), "shipping");
                parameters.Add(string.Format("li_{0}_price", itemNum), cart.ShippingCost.ToString("F2"));
            }
                       
            // create temporary HTML form to post the parameters to 2CO site.
            HttpResponse resp = HttpContext.Current.Response;

            resp.Clear();
            resp.Write("<html>");
            resp.Write("<head><title>Processing Payment...</title></head>");
            resp.Write("<body onLoad=\"document.forms['gateway_form'].submit();\">");
            resp.Write(string.Format("<form method=\"POST\" name=\"gateway_form\" action=\"{0}\">", this.serverURL));
            resp.Write("<p><h2 style=\"text-align:center;\">Please wait, your order is being processed and you will be redirected to the payment website.</h2></p>");
            
            // create hidden input form fields
            string value;
            foreach (string key in parameters.Keys)
            {
                bool status = parameters.TryGetValue(key, out value);
                resp.Write(string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"/>", key, value));
            }
            
            resp.Write("<p style=\"text-align:center;\"><br/><br/>If you are not automatically redirected to payment website within 5 seconds...<br/><br/>");
            resp.Write("<input type=\"submit\" value=\"Click Here\"></p>");
            resp.Write("</form>");
            resp.Write("</body></html>");
            resp.End();
            return true;
        }        
        
        /// <summary>
        /// Claculates MD5 hash
        /// </summary>
        /// <param name="input">Input for calculating the Crypto MD5 hash</param>
        /// <returns>Calculate MD5 hash</returns>
        public string CalculateMD5hash(string input)
        {
            MD5CryptoServiceProvider CryptoService = new MD5CryptoServiceProvider();
            byte[] data = CryptoService.ComputeHash(Encoding.Default.GetBytes(input));
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                stringBuilder.Append(data[i].ToString("X2"));
            }

            return stringBuilder.ToString().ToUpper();
        }
    }
}
