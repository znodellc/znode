using System;
using System.Collections.Generic;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// Processes different payment types
    /// </summary>
    [Serializable()]
    public class ZNodePayment : ZNode.Libraries.ECommerce.Entities.ZNodePayment
    {
        #region Member Variables
        private ZNodeOrder _Order = new ZNodeOrder();
        private string _CurrencyCode = ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.CurrencyCode();
        private IEnumerable<ZNodeShoppingCartItem> _addressCartItems = null;
        private bool _fromApi = false;
        public string Token { get; set; }
        public string PayerId { get; set; }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the currency code
        /// </summary>
        public string CurrencyCode
        {
            get { return this._CurrencyCode; }
            set { this._CurrencyCode = value; }
        }

        /// <summary>
        /// Gets or sets the order
        /// </summary>
        public ZNodeOrder Order
        {
            get { return this._Order; }
            set { this._Order = value; }
        }

        public IEnumerable<ZNodeShoppingCartItem> AddressCartItems
        {
            get { return this._addressCartItems; }
            set { this._addressCartItems = value; }
        }
        /// <summary>
        /// Gets or Sets MultipleShipping Address is enabled
        /// </summary>
        public bool IsMultipleShipToAddress { get; set; }

        #endregion

        #region Public and Private Methods
        /// <summary>
        /// This method orchestrates the payment submission process
        /// </summary>
        /// <returns>Returns the payment response</returns>
        public ZNodePaymentResponse SubmitPayment(bool fromApi = false)
        {
            _fromApi = fromApi;

            // submit card payment
            if (PaymentSetting.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.CREDIT_CARD)
            {
                ZNodePaymentResponse response = this.SubmitCreditCardPayment();

                // Check 
                if (response.IsSuccess && this.IsRecurringBillingExists)
                {
                    bool subscriptionResult = true;
                    string responseText = "This gateway does not support all of the billing periods that you have set on products. Please be sure to update the billing period on all your products.";
                    int counter = 0;
                    int index = 0;

                    // Loop through the Order Line Items
                    foreach (OrderLineItem orderLineItem in this._Order.OrderLineItems)
                    {
                        ZNodeShoppingCartItem cartItem = ShoppingCart.ShoppingCartItems[counter++];

                        decimal productPrice = (cartItem.TieredPricing + cartItem.ShippingCost + cartItem.TaxCost) * cartItem.Quantity;

                        if (cartItem.Product.RecurringBillingInd && subscriptionResult)
                        {
                            RecurringBillingInfo recurringBilling = new RecurringBillingInfo();
                            recurringBilling.InitialAmount = Math.Round(cartItem.Product.RecurringBillingInitialAmount, 2);
                            recurringBilling.InstallmentInd = cartItem.Product.RecurringBillingInstallmentInd;
                            recurringBilling.Period = cartItem.Product.RecurringBillingPeriod;
                            recurringBilling.TotalCycles = cartItem.Product.RecurringBillingTotalCycles;
                            recurringBilling.Frequency = cartItem.Product.RecurringBillingFrequency;
                            recurringBilling.Amount = Math.Round(productPrice, 2);
                            recurringBilling.ProfileName = cartItem.Product.SKU + " - " + orderLineItem.OrderLineItemID.ToString();
                            recurringBilling.InvoiceNo = orderLineItem.OrderID + " - " + cartItem.Product.SKU;
                            ZNodeSubscriptionResponse val = this.SubmitRecurringBilling(recurringBilling);

                            if (val.IsSuccess)
                            {
                                val.ReferenceId = orderLineItem.OrderLineItemID;
                                response.RecurringBillingSubscriptionResponse.Add(val);
                            }
                            else
                            {
                                subscriptionResult &= false;
                                responseText = val.ResponseText + val.ResponseCode;
                                break;
                            }
                        }

                        index = 0;
                        foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                        {


                            foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                            {
                                if (addOnValue.RecurringBillingInd && subscriptionResult && addOnValue.FinalPrice > 0)
                                {
                                    productPrice = addOnValue.FinalPrice * cartItem.Quantity;

                                    RecurringBillingInfo recurringBilling = new RecurringBillingInfo();
                                    recurringBilling.InitialAmount = Math.Round(addOnValue.RecurringBillingInitialAmount, 2);
                                    recurringBilling.InstallmentInd = addOnValue.RecurringBillingInstallmentInd;
                                    recurringBilling.Period = addOnValue.RecurringBillingPeriod;
                                    recurringBilling.TotalCycles = addOnValue.RecurringBillingTotalCycles;
                                    recurringBilling.Frequency = addOnValue.RecurringBillingFrequency;
                                    recurringBilling.Amount = Math.Round(productPrice, 2);
                                    recurringBilling.ProfileName = addOnValue.SKU + " - " + orderLineItem.OrderLineItemID.ToString();
                                    recurringBilling.InvoiceNo = orderLineItem.OrderID + " - " + addOnValue.SKU;

                                    ZNodeSubscriptionResponse val = this.SubmitRecurringBilling(recurringBilling);

                                    if (val.IsSuccess)
                                    {
                                        val.ParentLineItemId = orderLineItem.OrderLineItemID;
                                        val.ReferenceId = orderLineItem.OrderLineItemCollection[index].OrderLineItemID;
                                        response.RecurringBillingSubscriptionResponse.Add(val);
                                    }
                                    else
                                    {
                                        subscriptionResult &= false;
                                        responseText = val.ResponseText + val.ResponseCode;
                                        break;
                                    }
                                }
                                index++;
                                if (!subscriptionResult)
                                {
                                    break;
                                }


                            }
                        }
                    }

                    // if any subscription fails, then revert all the transactions
                    if (!subscriptionResult)
                    {
                        foreach (ZNodeSubscriptionResponse subscriptionResp in response.RecurringBillingSubscriptionResponse)
                        {
                            this.CancelSubscription(subscriptionResp.SubscriptionId);
                        }

                        if (!string.IsNullOrEmpty(response.TransactionId))
                        {
                            // Revert Value
                            this.RefundPayment(response.TransactionId, NonRecurringItemsTotalAmount);
                        }

                        response = new ZNodePaymentResponse();
                        response.ResponseCode = "-1";
                        response.ResponseText = responseText;
                    }
                }

                return response;
            }
            else if (PaymentSetting.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PURCHASE_ORDER)
            {
                // submit purchase order
                return this.SubmitPurchaseOrderPayment();
            }
            else if (PaymentSetting.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL)
            {               
                // submit paypal express checkout order
                return this.SubmitPaypalExpressPayment(ShoppingCart);
            }
            else if (PaymentSetting.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT)
            {
                // submit Google express checkout order
                return this.SubmitGoogleExpressPayment();
            }
            else if (PaymentSetting.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.COD)
            {
                // submit COD (Charge on delivery)
                return this.SubmitCODPayment();
            }
            else if (PaymentSetting.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.TwoCO)
            {
                // submit 2CO Payment
                return this.Submit2COPayment();
            }
            else
            {
                ZNodePaymentResponse pr = new ZNodePaymentResponse();
                return pr;
            }
        }

        // Credit Card Payment Submission

        /// <summary>
        /// Submits Credit Card Payment 
        /// </summary>
        /// <returns>Returns the Payment response</returns>
        public ZNodePaymentResponse SubmitCreditCardPayment()
        {
            // Construct a response object using the gateway response received
            ZNodePaymentResponse PaymentResponse = new ZNodePaymentResponse();

            if (NonRecurringItemsTotalAmount > 0)
            {
                if (this.IsRecurringBillingExists)
                {
                    // Transaction Info
                    CreditCard.OrderID = this.Order.OrderID;
                    CreditCard.Amount = Math.Round(NonRecurringItemsTotalAmount, 2);
                    CreditCard.ShippingCharge = 0;
                    CreditCard.SubTotal = 0;
                    CreditCard.TaxCost = 0;
                }
                else
                {
                    // Transaction Info
                    CreditCard.OrderID = this.Order.OrderID;
                    CreditCard.Amount = Math.Round(this.Order.Total, 2);
                    CreditCard.ShippingCharge = Math.Round(this.Order.ShippingCost, 2);
                    CreditCard.SubTotal = Math.Round(this.Order.SubTotal, 2);
                    CreditCard.TaxCost = Math.Round(this.Order.TaxCost, 2);
                }

                // Description
                foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
                {
                    CreditCard.Description += cartItem.Product.ShoppingCartDescription;
                }

                // Get Gateway Info From Payment Settings
                GatewayInfo gatewayInfo = new GatewayInfo();
                ZNodeEncryption decrypt = new ZNodeEncryption();
                gatewayInfo.GatewayLoginID = decrypt.DecryptData(PaymentSetting.GatewayUsername);
                gatewayInfo.GatewayPassword = decrypt.DecryptData(PaymentSetting.GatewayPassword);

                if (PaymentSetting.GatewayTypeID.Value == (int)GatewayType.PAYPAL)
                {
                    // paypal
                    gatewayInfo.TransactionKey = PaymentSetting.TransactionKey;
                }
                else
                {
                    gatewayInfo.TransactionKey = decrypt.DecryptData(PaymentSetting.TransactionKey);
                }

                gatewayInfo.Vendor = PaymentSetting.Vendor; // for Verisign Payflow Pro
                gatewayInfo.Partner = PaymentSetting.Partner;
                gatewayInfo.TestMode = PaymentSetting.TestMode;
                gatewayInfo.PreAuthorize = PaymentSetting.PreAuthorize;
                gatewayInfo.CurrencyCode = this.CurrencyCode;

                // set gateway type
                if (PaymentSetting.GatewayTypeID.HasValue && Enum.IsDefined(typeof(GatewayType), PaymentSetting.GatewayTypeID))
                {
                    // Retrieves the name of the gateway name
                    string gatewayName = Enum.GetName(typeof(GatewayType), PaymentSetting.GatewayTypeID.Value);
                    gatewayInfo.Gateway = (GatewayType)Enum.Parse(typeof(GatewayType), gatewayName);
                }

                // set billing address
                ZNode.Libraries.ECommerce.Entities.Address gatewayBillingAddress = new ZNode.Libraries.ECommerce.Entities.Address();
                gatewayBillingAddress.City = BillingAddress.City;
                gatewayBillingAddress.CompanyName = BillingAddress.CompanyName;
                gatewayBillingAddress.CountryCode = BillingAddress.CountryCode;
                gatewayBillingAddress.EmailId = this._Order.Email;
                gatewayBillingAddress.FirstName = BillingAddress.FirstName;
                gatewayBillingAddress.LastName = BillingAddress.LastName;
                gatewayBillingAddress.MiddleName = BillingAddress.MiddleName;
                gatewayBillingAddress.PhoneNumber = BillingAddress.PhoneNumber;
                gatewayBillingAddress.PostalCode = BillingAddress.PostalCode;
                gatewayBillingAddress.StateCode = BillingAddress.StateCode;
                gatewayBillingAddress.Street1 = BillingAddress.Street;
                gatewayBillingAddress.Street2 = BillingAddress.Street1;

                // set shipping address
                ZNode.Libraries.ECommerce.Entities.Address gatewayShippingAddress = new ZNode.Libraries.ECommerce.Entities.Address();
                gatewayShippingAddress.City = ShippingAddress.City;
                gatewayShippingAddress.CompanyName = ShippingAddress.CompanyName;
                gatewayShippingAddress.CountryCode = ShippingAddress.CountryCode;
                gatewayShippingAddress.EmailId = this._Order.Email;
                gatewayShippingAddress.FirstName = ShippingAddress.FirstName;
                gatewayShippingAddress.LastName = ShippingAddress.LastName;
                gatewayShippingAddress.MiddleName = ShippingAddress.MiddleName;
                gatewayShippingAddress.PhoneNumber = ShippingAddress.PhoneNumber;
                gatewayShippingAddress.PostalCode = ShippingAddress.PostalCode;
                gatewayShippingAddress.StateCode = ShippingAddress.StateCode;
                gatewayShippingAddress.Street1 = ShippingAddress.Street;
                gatewayShippingAddress.Street2 = ShippingAddress.Street1;

                GatewayResponse resp = new GatewayResponse();

                // Check Selected GatewayType 
                switch (gatewayInfo.Gateway)
                {
                    case GatewayType.CUSTOM:// If Custom gateway is selected

                        CustomGateway _submitToGateway = new CustomGateway();
                        resp = _submitToGateway.SubmitPayment(gatewayInfo, gatewayBillingAddress, gatewayShippingAddress, CreditCard);
                        break;

                    case GatewayType.AUTHORIZE: // if Authorize.Net

                        GatewayAuthorize _SubmitToAuthorizeNet = new GatewayAuthorize();
                        if (UseToken)
                        {
                            throw new NotImplementedException("Authorize Tokens have not been implemented");
                        }
                        else
                        {
                            resp = _SubmitToAuthorizeNet.SubmitPayment(gatewayInfo, gatewayBillingAddress, gatewayShippingAddress, CreditCard, IsMultipleShipToAddress);
                        }

                        break;

                    case GatewayType.PAYMENTECH: // if Payment tech orbital gateway

                        GatewayOrbital _SubmitToPaymentechOrbital = new GatewayOrbital();
                        resp = _SubmitToPaymentechOrbital.SubmitPayment(gatewayInfo, gatewayBillingAddress, gatewayShippingAddress, CreditCard);
                        break;

                    case GatewayType.PAYPAL: // if paypal direct payment

                        GatewayPaypal _SubmitToPaypal = new GatewayPaypal(gatewayInfo);
                        resp = _SubmitToPaypal.SubmitPayment(gatewayInfo, gatewayBillingAddress, gatewayShippingAddress, CreditCard);
                        break;

                    case GatewayType.VERISIGN:
                        GatewayPayFlowPro _submitToPayflowPro = new GatewayPayFlowPro();

                        // Token  based Payment
                        if (UseToken)
                        {
                            this.PopulateGatewayToken(gatewayInfo.Gateway);
                            resp = _submitToPayflowPro.SubmitPayment(gatewayBillingAddress, gatewayShippingAddress, gatewayInfo, GatewayToken, this.Order.Total, this.Order.OrderID);
                        }
                        else
                        {
                            // Standard Credit Card Payment
                            // ExpiredDate should follow the following date format "0107" (which represents Jan,2007)
                            string originalExpDate = CreditCard.CreditCardExp;
                            string expDate = CreditCard.CreditCardExp.Remove(CreditCard.CreditCardExp.IndexOf("/"), 3);
                            CreditCard.CreditCardExp = expDate;
                            resp = _submitToPayflowPro.SubmitPayment(gatewayBillingAddress, gatewayShippingAddress, gatewayInfo, CreditCard);

                            // Reset original expiration date for Token insert
                            CreditCard.CreditCardExp = originalExpDate;

                            // Payment Succeeded, Save the Card Token
                            if (resp.IsSuccess && this.SaveCardData)
                            {
                                this.TransactionID = resp.TransactionId;
                                this.AuthCode = resp.CardAuthCode;
                                this.SaveGatewayToken();
                            }
                        }

                        break;

                    case GatewayType.CARDSTREAM:
                        GatewayCardStream _submitToCardStream = new GatewayCardStream();
                        resp = _submitToCardStream.SubmitPayment(gatewayInfo, gatewayBillingAddress, gatewayShippingAddress, CreditCard);
                        break;

                    case GatewayType.WORLDPAY:
                        GatewayWorldPay _submitToWorldpay = new GatewayWorldPay();
                        CreditCard.TransactionID = ShoppingCart.Payment.SessionId;
                        CreditCard.CardHolderName = BillingAddress.FirstName + BillingAddress.LastName;
                        string returnURL = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Response.ApplyAppPathModifier("WorldPayNotification.aspx");
                        gatewayInfo.Is3DSecure = ShoppingCart.Payment.Is3DSecure;
                        gatewayInfo.PaRequestURL = returnURL;
                        gatewayInfo.BrowserAcceptHeader = "text/html";
                        if (!_fromApi)
                            gatewayInfo.BrowserUserAgentHeader =
                                HttpContext.Current.Request.ServerVariables["HTTP_USER_AGENT"].ToString();
                        else
                            gatewayInfo.BrowserUserAgentHeader =
                                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/32.0.1700.76 Safari/537.36";
                        if (!gatewayInfo.Is3DSecure)
                        {
                            resp = _submitToWorldpay.SubmitPayment(gatewayInfo, gatewayBillingAddress, gatewayShippingAddress, CreditCard);
                            PaymentResponse.RedirectURL = resp.ECRedirectURL;
                            PaymentResponse.EchoData = resp.EchoData;
                            PaymentResponse.IssuerPostData = resp.IssuerPostData;
                            PaymentResponse.WorldPayHeaderCookie = resp.WorldPayHeaderCookie;
                        }
                        else
                        {
                            gatewayInfo.WorldPayEchoData = ShoppingCart.Payment.WorldPayEchodata;
                            gatewayInfo.WorldPayIssuerResponse = ShoppingCart.Payment.WorldPayPostData;
                            gatewayInfo.HeaderCookies = ShoppingCart.Payment.WorldPayHeaderCookie;
                            resp = _submitToWorldpay.Submit3DSecurePayment(gatewayInfo, gatewayBillingAddress, gatewayShippingAddress, CreditCard);
                        }

                        break;

                    case GatewayType.CYBERSOURCE: // if CyberSource
                        GatewayCyberSource _SubmitToCyberSource = new GatewayCyberSource();
                        resp = _SubmitToCyberSource.SubmitPayment(gatewayInfo, gatewayBillingAddress, gatewayShippingAddress, CreditCard);
                        PaymentResponse.GatewayResponseData = resp.GatewayResponseData;
                        break;

                    default:

                        resp.ResponseCode = "Error :";
                        resp.ResponseText = "Unsupported gateway selected. Please select some other gateway option to process your payment.";
                        break;
                }

                PaymentResponse.IsSuccess = resp.IsSuccess;
                PaymentResponse.ResponseCode = resp.ResponseCode;
                PaymentResponse.ResponseText = resp.ResponseText;
                PaymentResponse.TransactionId = resp.TransactionId;
                PaymentResponse.CardAuthorizationCode = resp.CardAuthCode;
                PaymentResponse.CardType = resp.CardTypeID;
                PaymentResponse.CardNumber = resp.CardNumber;
                PaymentResponse.ProfileID = resp.ProfileId;
                PaymentResponse.PaymentProfileID = resp.PaymentProfileID;
                PaymentResponse.ShippingAddressID = resp.ShippingAddressID;

                if (resp.IsSuccess)
                {
                    if (PaymentSetting.PreAuthorize)
                    {
                        PaymentResponse.PaymentStatus = ZNodePaymentStatus.CREDIT_AUTHORIZED;
                    }
                    else
                    {
                        PaymentResponse.PaymentStatus = ZNodePaymentStatus.CREDIT_CAPTURED;
                    }
                }
                else
                {
                    PaymentResponse.PaymentStatus = ZNodePaymentStatus.CREDIT_DECLINED;
                }
            }
            else
            {
                PaymentResponse.IsSuccess = true;
                PaymentResponse.ResponseCode = "0";
            }

            return PaymentResponse;
        }

        /// <summary>
        /// Submits Credit Card Payment 
        /// </summary>
        /// <param name="TransactionId">Transaction id</param>
        /// <param name="Amount">Amount to refund</param>
        public void RefundPayment(string TransactionId, decimal Amount)
        {
            // Get Gateway Info From Payment Settings
            GatewayInfo gatewayInfo = new GatewayInfo();
            ZNodeEncryption decrypt = new ZNodeEncryption();
            gatewayInfo.GatewayLoginID = decrypt.DecryptData(PaymentSetting.GatewayUsername);
            gatewayInfo.GatewayPassword = decrypt.DecryptData(PaymentSetting.GatewayPassword);

            if (PaymentSetting.GatewayTypeID.Value == (int)GatewayType.PAYPAL)
            {
                // Paypal
                gatewayInfo.TransactionKey = PaymentSetting.TransactionKey;
            }
            else
            {
                gatewayInfo.TransactionKey = decrypt.DecryptData(PaymentSetting.TransactionKey);
            }

            gatewayInfo.Vendor = PaymentSetting.Vendor; // for Verisign Payflow Pro
            gatewayInfo.Partner = PaymentSetting.Partner;
            gatewayInfo.TestMode = PaymentSetting.TestMode;
            gatewayInfo.PreAuthorize = PaymentSetting.PreAuthorize;
            gatewayInfo.CurrencyCode = this.CurrencyCode;

            // Set gateway type
            if (PaymentSetting.GatewayTypeID.HasValue && Enum.IsDefined(typeof(GatewayType), PaymentSetting.GatewayTypeID))
            {
                // Retrieves the name of the gateway name
                string gatewayName = Enum.GetName(typeof(GatewayType), PaymentSetting.GatewayTypeID.Value);
                gatewayInfo.Gateway = (GatewayType)Enum.Parse(typeof(GatewayType), gatewayName);
            }

            // Transaction Info
            CreditCard.OrderID = this.Order.OrderID;
            CreditCard.Amount = Amount;
            CreditCard.TransactionID = TransactionId;

            GatewayResponse resp = new GatewayResponse();

            // Check Selected GatewayType 
            switch (gatewayInfo.Gateway)
            {
                case GatewayType.CUSTOM: // If Custom gateway is selected

                    CustomGateway _submitToGateway = new CustomGateway();
                    resp = _submitToGateway.RefundPayment(gatewayInfo, CreditCard);
                    break;

                case GatewayType.AUTHORIZE: // if Authorize.Net

                    GatewayAuthorize _SubmitToAuthorizeNet = new GatewayAuthorize();
                    resp = _SubmitToAuthorizeNet.RefundPayment(gatewayInfo, CreditCard);
                    break;

                case GatewayType.VERISIGN:
                    GatewayPayFlowPro _submitToPayflowPro = new GatewayPayFlowPro();

                    resp = _submitToPayflowPro.RefundPayment(gatewayInfo, CreditCard);
                    break;

                case GatewayType.CYBERSOURCE:
                    GatewayCyberSource _SubmitToCyberSource = new GatewayCyberSource();
                    gatewayInfo.TransactionType = Enum.GetName(typeof(ZNodePaymentStatus), this.Order.PaymentStatusID.Value);
                    CreditCard.CardDataToken = this.Order.CardAuthCode;
                    resp = _SubmitToCyberSource.RefundPayment(gatewayInfo, CreditCard);
                    break;

                default:

                    resp.ResponseCode = "Error :";
                    resp.ResponseText = "Unsupported gateway selected. Please select some other gateway option to process your payment.";
                    break;
            }
        }

        // Purchase Order Submission

        /// <summary>
        /// Submit purchase order payment 
        /// </summary>
        /// <returns>PO Payment response</returns>
        public ZNodePaymentResponse SubmitPurchaseOrderPayment()
        {
            ZNodePaymentResponse PurchaseOrderPaymentResponse = new ZNodePaymentResponse();
            PurchaseOrderPaymentResponse.IsSuccess = true;
            PurchaseOrderPaymentResponse.TransactionId = null;
            PurchaseOrderPaymentResponse.PaymentStatus = ZNodePaymentStatus.PO_PENDING;

            // submit payment based on payment settings
            return PurchaseOrderPaymentResponse;
        }

        // Charge on Delivery Submission

        /// <summary>
        /// Submit purchase order payment 
        /// </summary>
        /// <returns>Returns the COD Payment Response</returns>
        public ZNodePaymentResponse SubmitCODPayment()
        {
            ZNodePaymentResponse CODPaymentResponse = new ZNodePaymentResponse();
            CODPaymentResponse.IsSuccess = true;
            CODPaymentResponse.TransactionId = null;
            CODPaymentResponse.PaymentStatus = ZNodePaymentStatus.COD_PENDING;

            // submit payment based on payment settings
            return CODPaymentResponse;
        }

        // 2CO Submission

        /// <summary>
        /// Submit purchase order payment 
        /// </summary>
        /// <returns>2CO Payment response</returns>
        public ZNodePaymentResponse Submit2COPayment()
        {
            ZNodePaymentResponse TwoCOPaymentResponse = new ZNodePaymentResponse();
            Gateway2COResponse gatewayResponse = new Gateway2COResponse();
            TwoCOPaymentResponse.IsSuccess = true;
            TwoCOPaymentResponse.TransactionId = gatewayResponse.SaleId;
            TwoCOPaymentResponse.CardAuthorizationCode = gatewayResponse.InvoiceId;
            TwoCOPaymentResponse.PaymentStatus = gatewayResponse.GetPaymentStatus();

            // submit payment based on payment settings
            return TwoCOPaymentResponse;
        }


        // Paypal Express Checkout Order Submission

        /// <summary>
        /// Submit Paypal payment order 
        /// </summary>
        /// <returns>Returns Paypal payment response</returns>
        public ZNodePaymentResponse SubmitPaypalExpressPayment(ZNodeShoppingCart portalCart)
        {
            ZNodePaymentResponse PaypalPaymentResponse = new ZNodePaymentResponse();

            ZNode.Libraries.Paypal.PaypalGateway paypal = new ZNode.Libraries.Paypal.PaypalGateway(PaymentSetting, string.Empty, string.Empty, portalCart);
            paypal.OrderTotal = ShoppingCart.Total;
            paypal.OrderLineItems = this.Order.OrderLineItems;

            if (_fromApi)
            {
                Token = this.ShoppingCart.Token;
                PayerId = this.ShoppingCart.Payerid;
            }
            else
            {
                Token = HttpContext.Current.Request.QueryString.Get("token");
                PayerId = HttpContext.Current.Request.QueryString.Get("payerid");
            }

            PaypalPaymentResponse = paypal.DoExpressCheckoutPayment(Token, PayerId);

            return PaypalPaymentResponse;
        }

        /// <summary>
        /// Submit Google Order
        /// </summary>
        /// <returns>Returns Google Order response</returns>
        public ZNodePaymentResponse SubmitGoogleExpressPayment()
        {
            ZNodePaymentResponse paymentResponse = new ZNodePaymentResponse();
            paymentResponse.PaymentStatus = ZNodePaymentStatus.CREDIT_PENDING;
            paymentResponse.IsSuccess = true;
            return paymentResponse;
        }

        /// <summary>
        /// Get the payment setting id by token id
        /// </summary>
        /// <param name="TokenId">Token id of the gateway</param>
        /// <returns>Payment Settings id</returns>
        public int GetPaymentSettingIdByTokenId(int TokenId)
        {
            int PaymentSettingId = 0;
            PaymentTokenService service = new PaymentTokenService();
            PaymentToken token = service.GetByPaymentTokenID(TokenId);
            PaymentSettingId = token.PaymentSettingID;
            return PaymentSettingId;
        }

        /// <summary>
        /// Delete the gateway token
        /// </summary>
        /// <param name="tokenId">Gateway Token id</param>
        /// <returns>Returns true if deleted else false</returns>
        public bool DeleteGatewayToken(int tokenId)
        {
            PaymentTokenService Service = new PaymentTokenService();

            return Service.Delete(tokenId);
        }

        /// <summary>
        /// Save the gateway token
        /// </summary>
        public void SaveGatewayToken()
        {
            // We will need to include Gateway specific logic in this method eventually, at that time it may make sense to begin using the gatewaytoken.cs entity
            PaymentTokenService serv = new PaymentTokenService();
            PaymentToken savepaymentmethod = new PaymentToken();
            savepaymentmethod.AccountID = this._Order.AccountID;
            savepaymentmethod.PaymentSettingID = this.PaymentSetting.PaymentSettingID;
            string CardNumber = string.Empty;
            CardNumber = this.CreditCard.CardNumber.Trim();
            CardNumber = CardNumber.Substring((CardNumber.Length - 4), 4);
            savepaymentmethod.CreditCardDescription = CardNumber;
            savepaymentmethod.CardTransactionID = this.TransactionID;
            savepaymentmethod.CardAuthCode = this.AuthCode;
            savepaymentmethod.CardExp = DateTime.Parse(this.CreditCard.CreditCardExp);
            serv.Insert(savepaymentmethod);
        }

        /// <summary>
        /// Populate the gateway token
        /// </summary>
        /// <param name="gateway">Gateway Info</param>
        private void PopulateGatewayToken(GatewayType gateway)
        {
            PaymentTokenService Service = new PaymentTokenService();
            switch (gateway)
            {
                case GatewayType.AUTHORIZE:

                    GatewayTokenAuthorize AuthorizeToken = new GatewayTokenAuthorize();

                    PaymentToken authorizedbToken = Service.GetByPaymentTokenID(TokenId);
                    AuthorizeToken.TransactionID = authorizedbToken.CardTransactionID;
                    this.GatewayToken = AuthorizeToken;
                    break;

                case GatewayType.VERISIGN:
                    GatewayTokenPayFlowPro PayflowToken = new GatewayTokenPayFlowPro();
                    PaymentToken payflowdbToken = Service.GetByPaymentTokenID(TokenId);
                    PayflowToken.TransactionID = payflowdbToken.CardTransactionID;
                    this.GatewayToken = PayflowToken;
                    break;

                default:
                    throw new NotImplementedException("The selected gateway is not available for token based payments");
            }
        }

        // Recurring Billing Submission

        /// <summary>
        /// Submits Recurring billing Subscription
        /// </summary>
        /// <param name="RBSubscriptionInfo">RB Subscription Info</param>
        /// <returns>Returns subscription response</returns>
        public ZNodeSubscriptionResponse SubmitRecurringBilling(RecurringBillingInfo RBSubscriptionInfo)
        {
            // Construct a response object using the gateway response received
            ZNodeSubscriptionResponse PaymentResponse = new ZNodeSubscriptionResponse();

            // Transaction Info
            CreditCard.OrderID = this.Order.OrderID;
            CreditCard.Amount = 0;
            CreditCard.ShippingCharge = 0;
            CreditCard.SubTotal = 0;
            CreditCard.TaxCost = 0;

            // Get Gateway Info From Payment Settings
            GatewayInfo gatewayInfo = new GatewayInfo();
            ZNodeEncryption decrypt = new ZNodeEncryption();
            gatewayInfo.GatewayLoginID = decrypt.DecryptData(PaymentSetting.GatewayUsername);
            gatewayInfo.GatewayPassword = decrypt.DecryptData(PaymentSetting.GatewayPassword);

            if (PaymentSetting.GatewayTypeID.Value == (int)GatewayType.PAYPAL)
            {
                // paypal
                gatewayInfo.TransactionKey = PaymentSetting.TransactionKey;
            }
            else
            {
                gatewayInfo.TransactionKey = decrypt.DecryptData(PaymentSetting.TransactionKey);
            }

            gatewayInfo.Vendor = PaymentSetting.Vendor; // for Verisign Payflow Pro
            gatewayInfo.Partner = PaymentSetting.Partner;
            gatewayInfo.TestMode = PaymentSetting.TestMode;
            gatewayInfo.PreAuthorize = PaymentSetting.PreAuthorize;
            gatewayInfo.CurrencyCode = this.CurrencyCode;

            // set gateway type
            if (PaymentSetting.GatewayTypeID.HasValue && Enum.IsDefined(typeof(GatewayType), PaymentSetting.GatewayTypeID))
            {
                // Retrieves the name of the gateway name
                string gatewayName = Enum.GetName(typeof(GatewayType), PaymentSetting.GatewayTypeID.Value);
                gatewayInfo.Gateway = (GatewayType)Enum.Parse(typeof(GatewayType), gatewayName);
            }

            // set billing address
            ZNode.Libraries.ECommerce.Entities.Address gatewayBillingAddress = new ZNode.Libraries.ECommerce.Entities.Address();
            gatewayBillingAddress.City = BillingAddress.City;
            gatewayBillingAddress.CompanyName = BillingAddress.CompanyName;
            gatewayBillingAddress.CountryCode = BillingAddress.CountryCode;
            gatewayBillingAddress.EmailId = this._Order.Email;
            gatewayBillingAddress.FirstName = BillingAddress.FirstName;
            gatewayBillingAddress.LastName = BillingAddress.LastName;
            gatewayBillingAddress.MiddleName = BillingAddress.MiddleName;
            gatewayBillingAddress.PhoneNumber = BillingAddress.PhoneNumber;
            gatewayBillingAddress.PostalCode = BillingAddress.PostalCode;
            gatewayBillingAddress.StateCode = BillingAddress.StateCode;
            gatewayBillingAddress.Street1 = BillingAddress.Street;
            gatewayBillingAddress.Street2 = BillingAddress.Street1;

            // set shipping address
            ZNode.Libraries.ECommerce.Entities.Address gatewayShippingAddress = new ZNode.Libraries.ECommerce.Entities.Address();
            gatewayShippingAddress.City = ShippingAddress.City;
            gatewayShippingAddress.CompanyName = ShippingAddress.CompanyName;
            gatewayShippingAddress.CountryCode = ShippingAddress.CountryCode;
            gatewayShippingAddress.EmailId = this._Order.Email;
            gatewayShippingAddress.FirstName = ShippingAddress.FirstName;
            gatewayShippingAddress.LastName = ShippingAddress.LastName;
            gatewayShippingAddress.MiddleName = ShippingAddress.MiddleName;
            gatewayShippingAddress.PhoneNumber = ShippingAddress.PhoneNumber;
            gatewayShippingAddress.PostalCode = ShippingAddress.PostalCode;
            gatewayShippingAddress.StateCode = ShippingAddress.StateCode;
            gatewayShippingAddress.Street1 = ShippingAddress.Street;
            gatewayShippingAddress.Street2 = ShippingAddress.Street1;

            GatewayResponse resp = new GatewayResponse();

            // Check Selected GatewayType 
            switch (gatewayInfo.Gateway)
            {
                case GatewayType.CUSTOM:// If Custom gateway is selected

                    CustomGateway _submitToGateway = new CustomGateway();
                    resp = _submitToGateway.SubmitRecurringBillingSubscription(gatewayInfo, gatewayBillingAddress, gatewayShippingAddress, CreditCard, RBSubscriptionInfo);
                    break;

                case GatewayType.AUTHORIZE: // if Authorize.Net

                    GatewayAuthorize _SubmitToAuthorizeNet = new GatewayAuthorize();
                    resp = _SubmitToAuthorizeNet.SubmitRecurringBillingSubscription(gatewayBillingAddress, CreditCard, gatewayInfo, RBSubscriptionInfo);
                    break;

                case GatewayType.VERISIGN:
                    GatewayPayFlowPro _submitToPayflowPro = new GatewayPayFlowPro();

                    if (CreditCard.CreditCardExp.Length != 4)
                    {
                        // ExpiredDate should follow the following date format "0107" (which represents Jan,2007)
                        string expDate = CreditCard.CreditCardExp.Remove(CreditCard.CreditCardExp.IndexOf("/"), 3);
                        CreditCard.CreditCardExp = expDate;
                    }

                    resp = _submitToPayflowPro.SubmitRecurringBillingSubscription(gatewayBillingAddress, gatewayShippingAddress, gatewayInfo, CreditCard, RBSubscriptionInfo);
                    break;

                case GatewayType.WORLDPAY:

                    GatewayWorldPay _SubmitToWorldPay = new GatewayWorldPay();
                    resp = _SubmitToWorldPay.SubmitRecurringBillingSubscription(true, gatewayInfo, gatewayBillingAddress, gatewayShippingAddress, CreditCard, RBSubscriptionInfo);
                    break;

                case GatewayType.CYBERSOURCE:
                    GatewayCyberSource _SubmitToCyberSource = new GatewayCyberSource();
                    resp = _SubmitToCyberSource.SubmitRecurringBillingSubscription(gatewayInfo, gatewayBillingAddress, gatewayShippingAddress, CreditCard, RBSubscriptionInfo);
                    break;
                case GatewayType.PAYMENTECH:
                    GatewayOrbital _SubmitToPaymentech = new GatewayOrbital();
                    // resp = _SubmitToPaymentech.SubmitRecurringPayment(gatewayInfo, gatewayBillingAddress, gatewayShippingAddress, CreditCard, RBSubscriptionInfo);
                    break;

                case GatewayType.PAYPAL:
                    GatewayPaypal _SubmitToPaypal = new GatewayPaypal(gatewayInfo);
                    resp = _SubmitToPaypal.SubmitRecurringPayment(gatewayInfo, gatewayBillingAddress, gatewayShippingAddress, CreditCard, RBSubscriptionInfo);
                    break;


                default:

                    resp.ResponseCode = "Error :";
                    resp.ResponseText = "Unsupported gateway selected. Please select some other gateway option to process your payment.";
                    break;
            }

            PaymentResponse.IsSuccess = resp.IsSuccess;
            PaymentResponse.ResponseCode = resp.ResponseCode;
            PaymentResponse.ResponseText = resp.ResponseText;
            PaymentResponse.SubscriptionId = resp.TransactionId; // SubscriptionId            

            if (gatewayInfo.Gateway == GatewayType.VERISIGN)
            {
                PaymentResponse.SubscriptionId = resp.ProfileId;
            }

            return PaymentResponse;
        }

        /// <summary>
        /// Cancel the subscription
        /// </summary>
        /// <param name="SubscriptionId">Subscription id</param>
        private void CancelSubscription(string SubscriptionId)
        {
            // Get Gateway Info From Payment Settings
            GatewayInfo gatewayInfo = new GatewayInfo();
            ZNodeEncryption decrypt = new ZNodeEncryption();
            gatewayInfo.GatewayLoginID = decrypt.DecryptData(PaymentSetting.GatewayUsername);
            gatewayInfo.GatewayPassword = decrypt.DecryptData(PaymentSetting.GatewayPassword);

            if (PaymentSetting.GatewayTypeID.Value == (int)GatewayType.PAYPAL)
            {
                // paypal
                gatewayInfo.TransactionKey = PaymentSetting.TransactionKey;
            }
            else
            {
                gatewayInfo.TransactionKey = decrypt.DecryptData(PaymentSetting.TransactionKey);
            }

            gatewayInfo.Vendor = PaymentSetting.Vendor; // for Verisign Payflow Pro
            gatewayInfo.Partner = PaymentSetting.Partner;
            gatewayInfo.TestMode = PaymentSetting.TestMode;
            gatewayInfo.CurrencyCode = this.CurrencyCode;

            // set gateway type
            if (PaymentSetting.GatewayTypeID.HasValue && Enum.IsDefined(typeof(GatewayType), PaymentSetting.GatewayTypeID))
            {
                // Retrieves the name of the gateway name
                string gatewayName = Enum.GetName(typeof(GatewayType), PaymentSetting.GatewayTypeID.Value);
                gatewayInfo.Gateway = (GatewayType)Enum.Parse(typeof(GatewayType), gatewayName);
            }

            GatewayResponse resp = new GatewayResponse();

            // Check Selected GatewayType 
            switch (gatewayInfo.Gateway)
            {
                case GatewayType.CUSTOM: // If Custom gateway is selected

                    CustomGateway _submitToGateway = new CustomGateway();
                    resp = _submitToGateway.CancelRecurringBillingSubscription(SubscriptionId, gatewayInfo);
                    break;

                case GatewayType.AUTHORIZE: // if Authorize.Net

                    GatewayAuthorize _SubmitToAuthorizeNet = new GatewayAuthorize();
                    resp = _SubmitToAuthorizeNet.CancelRecurringBillingSubscription(gatewayInfo, SubscriptionId);
                    break;

                case GatewayType.VERISIGN:

                    GatewayPayFlowPro _submitToPayflowPro = new GatewayPayFlowPro();
                    resp = _submitToPayflowPro.CancelRecurringBilling(SubscriptionId, gatewayInfo);
                    break;

                default:

                    resp.ResponseCode = "Error :";
                    resp.ResponseText = "Unsupported gateway selected. Please select some other gateway option to process your payment.";
                    break;
            }
        }

        #endregion
    }
}