using System;
using System.Net;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// CardStream Gateway
    /// </summary>
    public class GatewayCardStream : ZNodePaymentBase
    {
        /// <summary>
        /// Submits credit card payment to a Cardstream gateway
        /// </summary>
        /// <param name="Gateway">Gateway info</param>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="CreditCard">Credit Card</param>
        /// <returns>Gateway Response</returns>
        public GatewayResponse SubmitPayment(GatewayInfo Gateway, Address BillingAddress, Address ShippingAddress, CreditCard CreditCard)
        {
            GatewayResponse PaymentGatewayResponse = new GatewayResponse();

            // Your payment gateway code here
            string CSMerchantID = Gateway.GatewayLoginID;
            string CSPassword = Gateway.GatewayPassword;
            WebClient objRequest = new WebClient();
            System.Collections.Specialized.NameValueCollection objInf = new System.Collections.Specialized.NameValueCollection(24);
            System.Collections.Specialized.NameValueCollection objRetInf = new System.Collections.Specialized.NameValueCollection(17);
            byte[] objRetBytes;
            string[] objRetVals;

            // CSMerchantID and CSPassword to be passed by Gateway class during normal operation
            objInf.Add("VPMerchantID", CSMerchantID);
            objInf.Add("VPMerchantPassword", CSPassword);

            // Gateway Response settings            
            objInf.Add("VPMessageType", "ESALE_KEYED"); // Payment type - ESALE_KEYED is Internet sale where cardholder and merchant are not present
            objInf.Add("VPDispatch", "NOW"); // Payment taken when NOW for immediate or LATER for deferred

            int currencyCode = (int)((ISOCurrencyCode)Enum.Parse(typeof(ISOCurrencyCode), Gateway.CurrencyCode));
            objInf.Add("VPCurrencyCode", currencyCode.ToString()); // Needs to be in the ISO format, so 826 is for Sterling

            // Billing Address 
            objInf.Add("VPCardName", BillingAddress.FirstName + " " + BillingAddress.LastName);            
            objInf.Add("VPBillingHouseNumber", BillingAddress.Street1.Trim(char.Parse(" "))); // Needs to be numeric only
            int numLength = BillingAddress.Street1.Trim(char.Parse(" ")).Length;
            objInf.Add("VPBillingStreet", BillingAddress.Street1.Substring(numLength, BillingAddress.Street1.Length - numLength) + " " + BillingAddress.Street2); // alphanumeric street details
            objInf.Add("VPBillingCity", BillingAddress.City);
            objInf.Add("VPBillingState", BillingAddress.StateCode);
            objInf.Add("VPBillingPostCode", BillingAddress.PostalCode);
            objInf.Add("VPBillingCountry", BillingAddress.CountryCode);
            objInf.Add("VPBillingPhoneNumber", BillingAddress.PhoneNumber);
            objInf.Add("VPBillingEmail", BillingAddress.EmailId);

            // Invoice Information
            objInf.Add("VPOrderDesc", "Description of Order");
            objInf.Add("VPTransactionUnique", CreditCard.OrderID.ToString());

            // Credit Card Details 
            objInf.Add("VPCardNumber", CreditCard.CardNumber);
            string month = CreditCard.CreditCardExp.Substring(0, 2);
            string year = CreditCard.CreditCardExp.Substring(3, 2);
            objInf.Add("VPExpiryDateMM", month); // Needs to be split into Month and Year
            objInf.Add("VPExpiryDateYY", year); // Needs to be split into Month and Year
            objInf.Add("VPCV2", CreditCard.CardSecurityCode); // Authorisation code of the card (CCV)                      
            objInf.Add("VPAmount", Convert.ToString(Math.Round((CreditCard.Amount * 100), 0))); // Needs converting to base currency so 64.54 is 6454

            // Maestro/Switch Support                       
            //// objInf.Add("VPIssueNumber", CreditCard.IssueNumber); // IssueNumber - Format 0-99
            //// objInf.Add("VPStartDateMM", CreditCard.StartDateMonth); // StartDateMonth - Format 01-12
            //// objInf.Add("VPStartDateYY", CreditCard.StartDateYear); // StartDateYear - Format 00-99

            try
            {
                // Actual Server 
                objRequest.BaseAddress = "https://www.cardstream.com/merchantsecure/cardstream/VPDirect.cfm";

                // Post the values into Cardstream Server
                objRetBytes = objRequest.UploadValues(objRequest.BaseAddress, "POST", objInf);
                objRetVals = System.Text.Encoding.ASCII.GetString(objRetBytes).Split("&".ToCharArray());
                
                string[] ObjRet = objRetVals[0].Split("=".ToCharArray());

                // Transaction Approved if the Response code is 00
                if (ObjRet[1] == "00")
                {
                    // Set Response code and Response Text
                    PaymentGatewayResponse.ResponseCode = "Response Code: " + objRetVals[0].Trim(char.Parse("=")) + "<br>";
                    PaymentGatewayResponse.ResponseText = "Response Reason Code : Success";
                    PaymentGatewayResponse.ReferenceNumber = "Auth Reference :" + objRetVals[2].Trim(char.Parse(":"));
                    PaymentGatewayResponse.CardAuthCode = objRetVals[2].Trim(char.Parse(":"));
                    PaymentGatewayResponse.IsSuccess = true;
                }
                else
                {
                    PaymentGatewayResponse.IsSuccess = false;

                    // Set Response code and Response Text
                    PaymentGatewayResponse.ResponseCode = "Response Code: " + objRetVals[0].Trim(char.Parse("=")) + "<br>";
                    if (ObjRet[1] == "05")
                    {
                        PaymentGatewayResponse.ResponseText = "Response Reason Code : " + objRetVals[2].Trim(char.Parse("=")) + "<br>";
                        PaymentGatewayResponse.ReferenceNumber = "Auth Reference : Declined";
                    }
                    else
                    {
                        PaymentGatewayResponse.ResponseText = "Response Reason Code : " + objRetVals[2].Trim(char.Parse("=")) + " " + objRetVals[1].Trim(char.Parse("=")) + "<br>";
                        PaymentGatewayResponse.ReferenceNumber = "Auth Reference :" + objRetVals[2].Trim(char.Parse(":"));
                    }
                }
            }
            catch (Exception)
            {
                PaymentGatewayResponse.IsSuccess = false;
            }

            // Return Payment GatewayResponse
            return PaymentGatewayResponse;
        }
    }
}
