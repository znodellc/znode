using System;
using System.Text;
using PayPal.Payments.Common.Utility;
using PayPal.Payments.DataObjects;
using PayPal.Payments.Transactions;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace ZNode.Libraries.ECommerce.Payment
{
    /// <summary>
    /// PayFlow Pro payment gateway
    /// </summary>
    public class GatewayPayFlowPro : ZNodePaymentBase
    {
        #region Private Variables
        private string orderId;
        #endregion

        #region Constructors
        /// <summary>
        /// Initializes a new instance of the GatewayPayFlowPro class.
        /// </summary>
        public GatewayPayFlowPro() 
        {
        }        
        #endregion

        #region Public Methods
        // Credit Card Payment

        /// <summary>
        /// Populate the authorization transaction from the order details.
        /// Submits credit card payment to a PayFlow pro gateway
        /// </summary>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="_gatewayInfo">_gateway Info</param>
        /// <param name="_creditCardInfo">_creditCard Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse SubmitPayment(ZNode.Libraries.ECommerce.Entities.Address BillingAddress, ZNode.Libraries.ECommerce.Entities.Address ShippingAddress, GatewayInfo _gatewayInfo, ZNode.Libraries.ECommerce.Entities.CreditCard _creditCardInfo)
        {
            this.orderId = PayflowUtility.RequestId;

            // Populate the Billing address details.
            PayPal.Payments.DataObjects.BillTo Bill = new PayPal.Payments.DataObjects.BillTo();
            Bill.FirstName = BillingAddress.FirstName;
            Bill.LastName = BillingAddress.LastName;
            Bill.Street = BillingAddress.Street1 + BillingAddress.Street2;
            Bill.City = BillingAddress.City;
            Bill.Zip = BillingAddress.PostalCode;
            Bill.State = BillingAddress.StateCode;

            // Populate the Shipping address details.
            PayPal.Payments.DataObjects.ShipTo Ship = new PayPal.Payments.DataObjects.ShipTo();
            Ship.ShipToFirstName = ShippingAddress.FirstName;
            Ship.ShipToLastName = ShippingAddress.LastName;
            Ship.ShipToStreet = ShippingAddress.Street1 + ShippingAddress.Street2;
            Ship.ShipToCity = ShippingAddress.City;
            Ship.ShipToZip = ShippingAddress.PostalCode;
            Ship.ShipToState = ShippingAddress.StateCode;

            // Populate the invoice
            PayPal.Payments.DataObjects.Invoice invoice = new PayPal.Payments.DataObjects.Invoice();
            invoice.BillTo = Bill;
            invoice.ShipTo = Ship;
            invoice.InvNum = _creditCardInfo.OrderID.ToString();
            invoice.Amt = new Currency(_creditCardInfo.Amount, _gatewayInfo.CurrencyCode);

            // Populate the Credit Card details.            
            string CardExpireDate = _creditCardInfo.CreditCardExp;
            PayPal.Payments.DataObjects.CreditCard Card = new PayPal.Payments.DataObjects.CreditCard(_creditCardInfo.CardNumber, CardExpireDate);
            
            // Set Credit Card Secutiry Code          
            Card.Cvv2 = _creditCardInfo.CardSecurityCode;

            // Create the Tender.
            PayPal.Payments.DataObjects.CardTender tender = new PayPal.Payments.DataObjects.CardTender(Card);
            
            // Merchant Account Details
            // Note that this code takes the following fields as parameters.1.UserId, 2.Vendor Name, 3.Partner name,4. password
            PayPal.Payments.DataObjects.UserInfo userInfo = new PayPal.Payments.DataObjects.UserInfo(_gatewayInfo.GatewayLoginID, _gatewayInfo.Vendor, _gatewayInfo.Partner, _gatewayInfo.GatewayPassword);
            
            // Set Post URL
            _gatewayInfo.GatewayURL = this.SetPostURL(_gatewayInfo.TestMode);

            // Connection Settomgs
            PayPal.Payments.DataObjects.PayflowConnectionData connectionData = new PayPal.Payments.DataObjects.PayflowConnectionData(_gatewayInfo.GatewayURL, 443, null, 0, null, null);

            // Create the transaction.
            PayPal.Payments.DataObjects.Response RespAuth;

            if (!string.IsNullOrEmpty(_creditCardInfo.TransactionID))
            {
                // Create a new Tender - Base Tender data object and set the Tender Type to "C".
                // We do not pass any payment device
                BaseTender baseTender = new BaseTender("C", null);
                if (_gatewayInfo.PreAuthorize)
                {
                    // Create a new Reference Transaction.
                    ReferenceTransaction refTrans = new ReferenceTransaction("A", _creditCardInfo.TransactionID, userInfo, connectionData, invoice, baseTender, this.orderId);

                    // Submit the Transaction
                    RespAuth = refTrans.SubmitTransaction();
                }
                else
                {
                    // Create a new Reference Transaction.
                    ReferenceTransaction refTrans = new ReferenceTransaction("S", _creditCardInfo.TransactionID, userInfo, connectionData, invoice, baseTender, this.orderId);

                    // Submit the Transaction
                    RespAuth = refTrans.SubmitTransaction();
                }
            }
            else
            {
                if (_gatewayInfo.PreAuthorize)
                {
                    AuthorizationTransaction authTrans = new AuthorizationTransaction(userInfo, connectionData, invoice, tender, this.orderId);
                    RespAuth = authTrans.SubmitTransaction();
                }
                else
                {
                    SaleTransaction saleTrans = new SaleTransaction(userInfo, connectionData, invoice, tender, this.orderId);
                    RespAuth = saleTrans.SubmitTransaction();
                }
            }  

            GatewayResponse Response = new GatewayResponse();

            if (RespAuth.TransactionResponse.Result == 0)
            {
                Response.ResponseCode = "0";
                Response.IsSuccess = true;                
                Response.ResponseText = RespAuth.TransactionResponse.RespMsg;
                Response.TransactionId = RespAuth.TransactionResponse.Pnref;
                Response.CardAuthCode = RespAuth.TransactionResponse.AuthCode;
            }          
            else
            {
                // If result code is greater than 0 (Zero), the transaction is discarded 
                // by the Payflow server. The reason why the transaction is discarded is 
                // evident by the result code value and therefore, you should look at this 
                // result code and decide if 
                // 1. The customer has given some wrong inputs,
                // 2. It's a fraudulent transaction.
                // 3. There's a problem with your merchant account credentials etc.
                StringBuilder sb = new StringBuilder();
                sb.Append("Response Description : " + RespAuth.TransactionResponse.RespMsg.ToString());

                Response.ResponseCode = "Response Code : " + RespAuth.TransactionResponse.Result.ToString() + "<br>";
                 
                Response.ResponseText = sb.ToString();
            }

            return Response;
        }

        /// <summary>
        /// Populate the authorization transaction from the order details.
        /// Submits credit card payment to a PayFlow pro gateway
        /// </summary>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="_gatewayInfo">Gateway Info</param>
        /// <param name="Token">Gateway Token</param>
        /// <param name="OrderTotal">Order Total</param>
        /// <param name="OrderId">Order Id for payment</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse SubmitPayment(ZNode.Libraries.ECommerce.Entities.Address BillingAddress, ZNode.Libraries.ECommerce.Entities.Address ShippingAddress, GatewayInfo _gatewayInfo, ZNode.Libraries.ECommerce.Entities.GatewayToken Token, decimal OrderTotal, int OrderId)
        {
            if (Token is GatewayTokenPayFlowPro)
            {
                GatewayTokenPayFlowPro PayFlowToken = (GatewayTokenPayFlowPro)Token;
                this.orderId = PayflowUtility.RequestId;

                // Populate the invoice
                PayPal.Payments.DataObjects.Invoice invoice = new PayPal.Payments.DataObjects.Invoice();
                invoice.InvNum = OrderId.ToString();
                invoice.Amt = new Currency(OrderTotal, _gatewayInfo.CurrencyCode);

                // Merchant Account Details
                // Note that this code takes the following fields as parameters.1.UserId, 2.Vendor Name, 3.Partner name,4. password
                PayPal.Payments.DataObjects.UserInfo userInfo = new PayPal.Payments.DataObjects.UserInfo(_gatewayInfo.GatewayLoginID, _gatewayInfo.Vendor, _gatewayInfo.Partner, _gatewayInfo.GatewayPassword);

                // Set Post URL
                _gatewayInfo.GatewayURL = this.SetPostURL(_gatewayInfo.TestMode);

                // Connection Settomgs
                PayPal.Payments.DataObjects.PayflowConnectionData connectionData = new PayPal.Payments.DataObjects.PayflowConnectionData(_gatewayInfo.GatewayURL, 443, null, 0, null, null);

                // Create the transaction.
                PayPal.Payments.DataObjects.Response RespAuth;

                // Create a new Tender - Base Tender data object and set the Tender Type to "C".
                // We do not pass any payment device
                BaseTender baseTender = new BaseTender("C", null);

                if (_gatewayInfo.PreAuthorize)
                {
                    // Create a new Reference Transaction.
                    ReferenceTransaction refTrans = new ReferenceTransaction("A", PayFlowToken.TransactionID, userInfo, connectionData, invoice, baseTender, this.orderId);

                    // Submit the Transaction
                    RespAuth = refTrans.SubmitTransaction();
                }
                else
                {
                    // Create a new Reference Transaction.
                    ReferenceTransaction refTrans = new ReferenceTransaction("S", PayFlowToken.TransactionID, userInfo, connectionData, invoice, baseTender, this.orderId);

                    // Submit the Transaction
                    RespAuth = refTrans.SubmitTransaction();
                }

                GatewayResponse Response = new GatewayResponse();

                if (RespAuth.TransactionResponse.Result == 0)
                {
                    Response.ResponseCode = "0";
                    Response.IsSuccess = true;
                    Response.ResponseText = RespAuth.TransactionResponse.RespMsg;
                    Response.TransactionId = RespAuth.TransactionResponse.Pnref;
                    Response.CardAuthCode = RespAuth.TransactionResponse.AuthCode;
                }
                else
                {
                    // If result code is greater than 0 (Zero), the transaction is discarded 
                    // by the Payflow server. The reason why the transaction is discarded is 
                    // evident by the result code value and therefore, you should look at this 
                    // result code and decide if 
                    // 1. The customer has given some wrong inputs,
                    // 2. It's a fraudulent transaction.
                    // 3. There's a problem with your merchant account credentials etc.
                    StringBuilder sb = new StringBuilder();
                    sb.Append("Response Description : " + RespAuth.TransactionResponse.RespMsg.ToString());

                    Response.ResponseCode = "Response Code : " + RespAuth.TransactionResponse.Result.ToString() + "<br>";

                    Response.ResponseText = sb.ToString();
                }

                return Response;
            }
            else
            {
                throw new InvalidOperationException("Wrong Gateway Token Type provided.");
            }
        }

        /// <summary>
        /// Void payment
        /// </summary>
        /// <param name="GatewayInfo">Gateway Info</param>
        /// <param name="CreditCardInfo">CreditCard Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse VoidPayment(GatewayInfo GatewayInfo, ZNode.Libraries.ECommerce.Entities.CreditCard CreditCardInfo)
        {
            // Populate the invoice
            PayPal.Payments.DataObjects.Invoice Inv = new PayPal.Payments.DataObjects.Invoice();
            Inv.InvNum = CreditCardInfo.OrderID.ToString();
            Inv.Amt = new Currency(CreditCardInfo.Amount, GatewayInfo.CurrencyCode);

            // Populate the Credit Card details.            
            string CardExpireDate = CreditCardInfo.CreditCardExp;
            PayPal.Payments.DataObjects.CreditCard Card = new PayPal.Payments.DataObjects.CreditCard(CreditCardInfo.CardNumber, CardExpireDate);

            // Set Credit Card Secutiry Code          
            Card.Cvv2 = CreditCardInfo.CardSecurityCode;

            // Merchant Account Details
            // Note that this code takes the following fields as parameters.1.UserId, 2.Vendor Name, 3.Partner name,4. password
            PayPal.Payments.DataObjects.UserInfo userInfo = new PayPal.Payments.DataObjects.UserInfo(GatewayInfo.GatewayLoginID, GatewayInfo.Vendor, GatewayInfo.Partner, GatewayInfo.GatewayPassword);

            // Set Post URL
            GatewayInfo.GatewayURL = this.SetPostURL(GatewayInfo.TestMode);

            // Connection Settomgs
            PayPal.Payments.DataObjects.PayflowConnectionData connectionData = new PayPal.Payments.DataObjects.PayflowConnectionData(GatewayInfo.GatewayURL, 443, null, 0, null, null);

            // void transaction.
            PayPal.Payments.Transactions.VoidTransaction voidtrans = new VoidTransaction(CreditCardInfo.TransactionID, userInfo, connectionData, PayflowUtility.RequestId);

            PayPal.Payments.DataObjects.Response RespAuth = voidtrans.SubmitTransaction();

            GatewayResponse Response = new GatewayResponse();

            if (RespAuth.TransactionResponse.Result == 0)
            {
                Response.ResponseCode = "0";
                Response.IsSuccess = true;
                Response.ResponseText = RespAuth.TransactionResponse.RespMsg;
                Response.TransactionId = RespAuth.TransactionResponse.Pnref;
            }
            else
            {
                // If result code is greater than 0 (Zero), the transaction is discarded 
                // by the Payflow server. The reason why the transaction is discarded is 
                // evident by the result code value and therefore, you should look at this 
                // result code and decide if 
                // 1. The customer has given some wrong inputs,
                // 2. It's a fraudulent transaction.
                // 3. There's a problem with your merchant account credentials etc.
                StringBuilder sb = new StringBuilder();
                sb.Append("Response Description : " + RespAuth.TransactionResponse.RespMsg.ToString());

                Response.ResponseCode = "Response Code : " + RespAuth.TransactionResponse.Result.ToString() + "<br>";

                Response.ResponseText = sb.ToString();
            }

            return Response;
        }

        /// <summary>
        /// Void payment
        /// </summary>
        /// <param name="GatewayInfo">Gateway Info</param>
        /// <param name="CreditCardInfo">CreditCard Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse RefundPayment(GatewayInfo GatewayInfo, ZNode.Libraries.ECommerce.Entities.CreditCard CreditCardInfo)
        {
            // Populate the invoice
            PayPal.Payments.DataObjects.Invoice Inv = new PayPal.Payments.DataObjects.Invoice();
            Inv.InvNum = CreditCardInfo.OrderID.ToString();
            Inv.Amt = new Currency(CreditCardInfo.Amount, GatewayInfo.CurrencyCode);

            // Populate the Credit Card details.            
            string CardExpireDate = CreditCardInfo.CreditCardExp;
            PayPal.Payments.DataObjects.CreditCard Card = new PayPal.Payments.DataObjects.CreditCard(CreditCardInfo.CardNumber, CardExpireDate);

            // Set Credit Card Secutiry Code          
            Card.Cvv2 = CreditCardInfo.CardSecurityCode;

            // Create the Tender.
            PayPal.Payments.DataObjects.CardTender Tender = new PayPal.Payments.DataObjects.CardTender(Card);

            // Merchant Account Details
            // Note that this code takes the following fields as parameters.1.UserId, 2.Vendor Name, 3.Partner name,4. password
            PayPal.Payments.DataObjects.UserInfo userInfo = new PayPal.Payments.DataObjects.UserInfo(GatewayInfo.GatewayLoginID, GatewayInfo.Vendor, GatewayInfo.Partner, GatewayInfo.GatewayPassword);

            // Set Post URL
            GatewayInfo.GatewayURL = this.SetPostURL(GatewayInfo.TestMode);

            // Connection Settomgs
            PayPal.Payments.DataObjects.PayflowConnectionData connectionData = new PayPal.Payments.DataObjects.PayflowConnectionData(GatewayInfo.GatewayURL, 443, null, 0, null, null);

            // void transaction.
            PayPal.Payments.Transactions.CreditTransaction credittrans = new CreditTransaction(CreditCardInfo.TransactionID, userInfo, connectionData, PayflowUtility.RequestId);

            PayPal.Payments.DataObjects.Response RespAuth = credittrans.SubmitTransaction();

            GatewayResponse Response = new GatewayResponse();

            if (RespAuth.TransactionResponse.Result == 0)
            {
                Response.ResponseCode = "0";
                Response.IsSuccess = true;
                Response.ResponseText = RespAuth.TransactionResponse.RespMsg;
                Response.TransactionId = RespAuth.TransactionResponse.Pnref;
            }           
            else
            {
                // If result code is greater than 0 (Zero), the transaction is discarded 
                // by the Payflow server. The reason why the transaction is discarded is 
                // evident by the result code value and therefore, you should look at this 
                // result code and decide if 
                // 1. The customer has given some wrong inputs,
                // 2. It's a fraudulent transaction.
                // 3. There's a problem with your merchant account credentials etc.
                StringBuilder sb = new StringBuilder();
                sb.Append("Response Description : " + RespAuth.TransactionResponse.RespMsg.ToString());

                Response.ResponseCode = "Response Code : " + RespAuth.TransactionResponse.Result.ToString() + "<br>";

                Response.ResponseText = sb.ToString();
            }

            return Response;
        }

        /// <summary>
        /// Capture previously authorized transaction
        /// </summary>
        /// <param name="_gatewayInfo">Gateway Info</param>
        /// <param name="_creditCardInfo">Credit Card Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse CapturePayment(GatewayInfo _gatewayInfo, ZNode.Libraries.ECommerce.Entities.CreditCard _creditCardInfo)
        {
            // Populate the invoice
            PayPal.Payments.DataObjects.Invoice Inv = new PayPal.Payments.DataObjects.Invoice();

            // Populate the Credit Card details.            
            string CardExpireDate = _creditCardInfo.CreditCardExp;
            PayPal.Payments.DataObjects.CreditCard Card = new PayPal.Payments.DataObjects.CreditCard(_creditCardInfo.CardNumber, CardExpireDate);

            // Set Credit Card Secutiry Code          
            Card.Cvv2 = _creditCardInfo.CardSecurityCode;

            // Create the Tender.
            PayPal.Payments.DataObjects.CardTender Tender = new PayPal.Payments.DataObjects.CardTender(Card);

            // Merchant Account Details
            // Note that this code takes the following fields as parameters.1.UserId, 2.Vendor Name, 3.Partner name,4. password
            PayPal.Payments.DataObjects.UserInfo userInfo = new PayPal.Payments.DataObjects.UserInfo(_gatewayInfo.GatewayLoginID, _gatewayInfo.Vendor, _gatewayInfo.Partner, _gatewayInfo.GatewayPassword);

            // Set Post URL
            _gatewayInfo.GatewayURL = this.SetPostURL(_gatewayInfo.TestMode);

            // Connection Settomgs
            PayPal.Payments.DataObjects.PayflowConnectionData connectionData = new PayPal.Payments.DataObjects.PayflowConnectionData(_gatewayInfo.GatewayURL, 443, null, 0, null, null);

            // capture transaction.
            PayPal.Payments.Transactions.CaptureTransaction capturetrans = new CaptureTransaction(_creditCardInfo.TransactionID, userInfo, connectionData, PayflowUtility.RequestId);

            PayPal.Payments.DataObjects.Response RespAuth = capturetrans.SubmitTransaction();

            GatewayResponse Response = new GatewayResponse();

            if (RespAuth.TransactionResponse.Result == 0)
            {
                Response.ResponseCode = "0";
                Response.IsSuccess = true;
                Response.ResponseText = RespAuth.TransactionResponse.RespMsg;
                Response.TransactionId = RespAuth.TransactionResponse.Pnref;
            }           
            else
            {
                // If result code is greater than 0 (Zero), the transaction is discarded 
                // by the Payflow server. The reason why the transaction is discarded is 
                // evident by the result code value and therefore, you should look at this 
                // result code and decide if 
                // 1. The customer has given some wrong inputs,
                // 2. It's a fraudulent transaction.
                // 3. There's a problem with your merchant account credentials etc.
                StringBuilder sb = new StringBuilder();
                sb.Append("Response Description : " + RespAuth.TransactionResponse.RespMsg.ToString());

                Response.ResponseCode = "Response Code : " + RespAuth.TransactionResponse.Result.ToString() + "<br>";

                Response.ResponseText = sb.ToString();
            }

            return Response;
        }

        // Recurring Billing Subscription

        /// <summary>
        /// Billing Period
        /// </summary>
        /// <param name="Unit">Unit for billing</param>
        /// <param name="Frequency">Frequency for the billing</param>
        /// <returns>Returns the recurring billing unit</returns>
        public RecurringBillingUnit BillingPeriod(string Unit, string Frequency)
        {
            if (Unit.Equals("DAY", StringComparison.OrdinalIgnoreCase))
            {
                if (Frequency.Equals("7", StringComparison.OrdinalIgnoreCase))
                {
                    return RecurringBillingUnit.WEEK;
                }
                else if (Frequency.Equals("14", StringComparison.OrdinalIgnoreCase))
                {
                    return RecurringBillingUnit.BIWK;
                }
                else if (Frequency.Equals("28", StringComparison.OrdinalIgnoreCase))
                {
                    return RecurringBillingUnit.FRWK;
                }
            }
            else if (Unit.Equals("WEEK", StringComparison.OrdinalIgnoreCase))
            {
                return RecurringBillingUnit.WEEK;
            }
            else if (Unit.Equals("MONTH", StringComparison.OrdinalIgnoreCase))
            {
                return RecurringBillingUnit.MONT;
            }
            else if (Unit.Equals("YEAR", StringComparison.OrdinalIgnoreCase))
            {
                return RecurringBillingUnit.YEAR;
            }

            return RecurringBillingUnit.NULL;
        }

         /// <summary>
        /// Submit the recurring billing subscription
        /// </summary>
        /// <param name="BillingAddress">Billing Address</param>
        /// <param name="ShippingAddress">Shipping Address</param>
        /// <param name="GatewayInfo">Gateway Info</param>
        /// <param name="CreditCardInfo">Credit Card Info</param>
        /// <param name="RecurringBillingInfo">Recurring Billing info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse SubmitRecurringBillingSubscription(ZNode.Libraries.ECommerce.Entities.Address BillingAddress, ZNode.Libraries.ECommerce.Entities.Address ShippingAddress, GatewayInfo GatewayInfo, ZNode.Libraries.ECommerce.Entities.CreditCard CreditCardInfo, RecurringBillingInfo RecurringBillingInfo)
        {
            GatewayResponse gatewayResponse = new GatewayResponse();

            // Populate the Billing address details.
            BillTo Bill = new BillTo();
            Bill.FirstName = BillingAddress.FirstName;
            Bill.LastName = BillingAddress.LastName;
            Bill.Street = BillingAddress.Street1 + BillingAddress.Street2;
            Bill.City = BillingAddress.City;
            Bill.Zip = BillingAddress.PostalCode;
            Bill.State = BillingAddress.StateCode;

            // Populate the Shipping address details.
            ShipTo Ship = new ShipTo();
            Ship.ShipToFirstName = ShippingAddress.FirstName;
            Ship.ShipToLastName = ShippingAddress.LastName;
            Ship.ShipToStreet = ShippingAddress.Street1 + ShippingAddress.Street2;
            Ship.ShipToCity = ShippingAddress.City;
            Ship.ShipToZip = ShippingAddress.PostalCode;
            Ship.ShipToState = ShippingAddress.StateCode;

            // Populate the invoice
            Invoice invoice = new PayPal.Payments.DataObjects.Invoice();
            invoice.BillTo = Bill;
            invoice.ShipTo = Ship;
            invoice.InvNum = CreditCardInfo.OrderID.ToString();
            invoice.Amt = new Currency(CreditCardInfo.Amount, GatewayInfo.CurrencyCode);

            // Populate the Credit Card details.            
            string CardExpireDate = CreditCardInfo.CreditCardExp;
            PayPal.Payments.DataObjects.CreditCard Card = new PayPal.Payments.DataObjects.CreditCard(CreditCardInfo.CardNumber, CardExpireDate);

            // Set Credit Card Secutiry Code          
            Card.Cvv2 = CreditCardInfo.CardSecurityCode;

            // Create the Tender.
            PayPal.Payments.DataObjects.CardTender tender = new PayPal.Payments.DataObjects.CardTender(Card);
            tender.ChkNum = "1";

            // Merchant Account Details
            // Note that this code takes the following fields as parameters.1.UserId, 2.Vendor Name, 3.Partner name,4. password
            PayPal.Payments.DataObjects.UserInfo userInfo = new PayPal.Payments.DataObjects.UserInfo(GatewayInfo.GatewayLoginID, GatewayInfo.Vendor, GatewayInfo.Partner, GatewayInfo.GatewayPassword);

            // Set Post URL
            GatewayInfo.GatewayURL = this.SetPostURL(GatewayInfo.TestMode);

            // Connection Settings
            PayflowConnectionData connectionData = new PayPal.Payments.DataObjects.PayflowConnectionData(GatewayInfo.GatewayURL, 443, null, 0, null, null);

            // Recurring Billing settings
            RecurringInfo subscriptionInfo = new RecurringInfo();
            subscriptionInfo.ProfileName = RecurringBillingInfo.ProfileName;

            invoice.Amt = new Currency(RecurringBillingInfo.InitialAmount, GatewayInfo.CurrencyCode);

            RecurringBillingUnit payPeriod = this.BillingPeriod(RecurringBillingInfo.Period, RecurringBillingInfo.Frequency);
            subscriptionInfo.PayPeriod = payPeriod.ToString();

            subscriptionInfo.Term = long.Parse(RecurringBillingInfo.TotalCycles.ToString());
            subscriptionInfo.OptionalTrx = "A"; // Authorization for validating the account information
            DateTime startDate = DateTime.Today.Date.AddDays(1);

            if (RecurringBillingInfo.InitialAmount > 0)
            {
                subscriptionInfo.OptionalTrxAmt = new Currency(RecurringBillingInfo.InitialAmount, GatewayInfo.CurrencyCode);

                long term = (long)payPeriod;
                startDate = DateTime.Today.Date.AddDays(term);
            }

            // Format: MMDDYYYY            
            subscriptionInfo.Start = startDate.ToString("MMddyyyy");
            
            // Create the transaction.
            PayPal.Payments.DataObjects.Response RespAuth;

            RecurringAddTransaction recurringBilling = new RecurringAddTransaction(userInfo, connectionData, invoice, tender, subscriptionInfo, PayflowUtility.RequestId);
            
            // Submit request transaction
            RespAuth = recurringBilling.SubmitTransaction();

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogObject(typeof(TransactionResponse), RespAuth.TransactionResponse);
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogObject(typeof(RecurringResponse), RespAuth.RecurringResponse);

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(RespAuth.ResponseString);

            if (RespAuth.TransactionResponse.Result == 0)
            {
                gatewayResponse.ResponseCode = "0";
                gatewayResponse.IsSuccess = true;
                gatewayResponse.ResponseText = RespAuth.TransactionResponse.RespMsg;
                gatewayResponse.ProfileId = RespAuth.RecurringResponse.ProfileId;
                gatewayResponse.ApprovalCode = RespAuth.RecurringResponse.RPRef;
                gatewayResponse.TransactionId = RespAuth.RecurringResponse.TrxPNRef;
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Response Description : " + RespAuth.TransactionResponse.RespMsg.ToString());

                gatewayResponse.ResponseCode = "Response Code : " + RespAuth.TransactionResponse.Result.ToString() + "<br>";

                gatewayResponse.ResponseText = sb.ToString();
            }

            return gatewayResponse;
        }
      
        /// <summary>
        /// Cancel the recurring billing subscription
        /// </summary>
        /// <param name="SubscriptionId">Subsciption Id</param>
        /// <param name="GatewayInfo">Gateway Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse CancelRecurringBilling(string SubscriptionId, GatewayInfo GatewayInfo)
        {
            GatewayResponse gatewayResponse = new GatewayResponse();
            
            RecurringInfo subscriptionInfo = new RecurringInfo();
            subscriptionInfo.OrigProfileId = SubscriptionId;

            // Merchant Account Details
            // Note that this code takes the following fields as parameters.1.UserId, 2.Vendor Name, 3.Partner name,4. password
            PayPal.Payments.DataObjects.UserInfo userInfo = new PayPal.Payments.DataObjects.UserInfo(GatewayInfo.GatewayLoginID, GatewayInfo.Vendor, GatewayInfo.Partner, GatewayInfo.GatewayPassword);

            // Set Post URL
            GatewayInfo.GatewayURL = this.SetPostURL(GatewayInfo.TestMode);

            // Connection Settomgs
            PayflowConnectionData connectionData = new PayPal.Payments.DataObjects.PayflowConnectionData(GatewayInfo.GatewayURL, 443, null, 0, null, null);

            RecurringCancelTransaction subscriptionCancelTransaction = new RecurringCancelTransaction(userInfo, connectionData, subscriptionInfo, PayflowUtility.RequestId);
            
            // Create the transaction.
            PayPal.Payments.DataObjects.Response responseTransaction;
            responseTransaction = subscriptionCancelTransaction.SubmitTransaction();

            if (responseTransaction.TransactionResponse.Result == 0)
            {
                gatewayResponse.ResponseCode = "0";
                gatewayResponse.IsSuccess = true;
                gatewayResponse.ResponseText = responseTransaction.TransactionResponse.RespMsg;
                gatewayResponse.TransactionId = responseTransaction.TransactionResponse.Pnref;
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Response Description : " + responseTransaction.TransactionResponse.RespMsg.ToString());

                gatewayResponse.ResponseCode = "Response Code : " + responseTransaction.TransactionResponse.Result.ToString() + "<br>";

                gatewayResponse.ResponseText = sb.ToString();
            }

            return gatewayResponse;
        }

        /// <summary>
        /// Cancel Recurring Billing
        /// </summary>
        /// <param name="SubscriptionId">Subscription Id</param>
        /// <param name="SubScriptionInfo">Subscription Info</param>
        /// <param name="GatewayInfo">Gateway Info</param>
        /// <returns>Returns the gateway response</returns>
        public GatewayResponse CancelRecurringBilling(string SubscriptionId, RecurringBillingInfo SubScriptionInfo, GatewayInfo GatewayInfo)
        {
            GatewayResponse gatewayResponse = new GatewayResponse();

            // Merchant Account Details
            // Note that this code takes the following fields as parameters.1.UserId, 2.Vendor Name, 3.Partner name,4. password
            PayPal.Payments.DataObjects.UserInfo userInfo = new PayPal.Payments.DataObjects.UserInfo(GatewayInfo.GatewayLoginID, GatewayInfo.Vendor, GatewayInfo.Partner, GatewayInfo.GatewayPassword);

            // Set Post URL
            GatewayInfo.GatewayURL = this.SetPostURL(GatewayInfo.TestMode);

            // Connection Settomgs
            PayflowConnectionData connectionData = new PayPal.Payments.DataObjects.PayflowConnectionData(GatewayInfo.GatewayURL, 443, null, 0, null, null);

            RecurringInfo subscriptionInfo = new RecurringInfo();

            subscriptionInfo.PayPeriod = this.BillingPeriod(SubScriptionInfo.Period, SubScriptionInfo.Frequency).ToString();
            subscriptionInfo.Term = long.Parse(SubScriptionInfo.TotalCycles.ToString());
            subscriptionInfo.Start = System.DateTime.Today.ToString();

            // Create the transaction.
            PayPal.Payments.DataObjects.Response responseTransaction;
            RecurringCancelTransaction subscriptionCancelTransaction = new RecurringCancelTransaction(userInfo, connectionData, subscriptionInfo, PayflowUtility.RequestId);
            responseTransaction = subscriptionCancelTransaction.SubmitTransaction();

            if (responseTransaction.TransactionResponse.Result == 0)
            {
                gatewayResponse.ResponseCode = "0";
                gatewayResponse.IsSuccess = true;
                gatewayResponse.ResponseText = responseTransaction.TransactionResponse.RespMsg;
                gatewayResponse.TransactionId = responseTransaction.TransactionResponse.Pnref;
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("Response Description : " + responseTransaction.TransactionResponse.RespMsg.ToString());

                gatewayResponse.ResponseCode = "Response Code : " + responseTransaction.TransactionResponse.Result.ToString() + "<br>";

                gatewayResponse.ResponseText = sb.ToString();
            }

            return gatewayResponse;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Set the post URL
        /// </summary>
        /// <param name="IsTestMode">Test mode or not</param>
        /// <returns>Returns the post url</returns>
        private string SetPostURL(bool IsTestMode)
        {
            if (IsTestMode)
            {
                return "pilot-payflowpro.paypal.com/transaction";                
            }

            return "payflowpro.paypal.com/transaction";
        }
        #endregion
    }
}
