﻿using System.Web.Http;
using System.Web.Routing;

namespace Znode.Engine.Api.App_Start
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Uncomment the following line of code to enable query support for actions with an IQueryable or IQueryable<T> return type.
            // To avoid processing unexpected or malicious queries, use the validation settings on QueryableAttribute to validate incoming queries.
            // For more information, visit http://go.microsoft.com/fwlink/?LinkId=279712.
            //config.EnableQuerySupport();

            // To disable tracing in your application, please comment out or remove the following line of code
            // For more information, refer to: http://www.asp.net/web-api
            //config.EnableSystemDiagnosticsTracing();

            // Remove XML formatter
            config.Formatters.Remove(config.Formatters.XmlFormatter);

            //config.Routes.MapHttpRoute(
            //	name: "DefaultApi",
            //	routeTemplate: "{controller}/{id}",
            //	defaults: new { id = RouteParameter.Optional }
            //);

            // Account routes
            config.Routes.MapHttpRoute("account-get", "accounts/{accountId}", new { controller = "accounts", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), accountId = @"^\d+$" });
            config.Routes.MapHttpRoute("account-getbyuserid", "accounts/{userId}", new { controller = "accounts", action = "getbyuserid" }, new { httpMethod = new HttpMethodConstraint("GET"), userId = @"^[A-Za-z0-9]{8}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}-[A-Za-z0-9]{12}$" });
            config.Routes.MapHttpRoute("account-getbyusername", "accounts/{username}", new { controller = "accounts", action = "getbyusername" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("account-list", "accounts", new { controller = "accounts", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("account-create", "accounts", new { controller = "accounts", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-update", "accounts/{accountId}", new { controller = "accounts", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("account-delete", "accounts/{accountId}", new { controller = "accounts", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("account-login", "accounts/login", new { controller = "accounts", action = "login" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-resetpassword", "accounts/resetpassword", new { controller = "accounts", action = "resetpassword" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-changepassword", "accounts/changepassword", new { controller = "accounts", action = "changepassword" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-checkresetpasswordlinkstatus", "accounts/checkresetpasswordlinkstatus", new { controller = "accounts", action = "checkresetpasswordlinkstatus" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-checkuserrole", "accounts/checkuserrole", new { controller = "accounts", action = "checkuserrole" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("account-resetadmindetails", "accounts/resetadmindetails", new { controller = "accounts", action = "resetadmindetails" }, new { httpMethod = new HttpMethodConstraint("POST") });

            // Address routes
            config.Routes.MapHttpRoute("address-get", "addresses/{addressId}", new { controller = "addresses", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("address-list", "addresses", new { controller = "addresses", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("address-create", "addresses", new { controller = "addresses", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("address-update", "addresses/{addressId}", new { controller = "addresses", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("address-delete", "addresses/{addressId}", new { controller = "addresses", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Attribute type routes
            config.Routes.MapHttpRoute("attributetype-get", "attributetypes/{attributeTypeId}", new { controller = "attributetypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("attributetype-list", "attributetypes", new { controller = "attributetypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("attributetype-create", "attributetypes", new { controller = "attributetypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("attributetype-update", "attributetypes/{attributeTypeId}", new { controller = "attributetypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("attributetype-delete", "attributetypes/{attributeTypeId}", new { controller = "attributetypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Audit routes
            config.Routes.MapHttpRoute("audit-get", "audits/{auditId}", new { controller = "audits", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("audit-list", "audits", new { controller = "audits", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("audit-create", "audits", new { controller = "audits", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("audit-update", "audits/{auditId}", new { controller = "audits", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("audit-delete", "audits/{auditId}", new { controller = "audits", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Case request routes
            config.Routes.MapHttpRoute("caserequest-get", "caserequests/{caseRequestId}", new { controller = "caserequests", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("caserequest-list", "caserequests", new { controller = "caserequests", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("caserequest-create", "caserequests", new { controller = "caserequests", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("caserequest-update", "caserequests/{caseRequestId}", new { controller = "caserequests", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("caserequest-delete", "caserequests/{caseRequestId}", new { controller = "caserequests", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Catalog routes
            config.Routes.MapHttpRoute("catalog-get", "catalogs/{catalogId}", new { controller = "catalogs", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), catalogId = @"^\d+$" });
            config.Routes.MapHttpRoute("catalog-list", "catalogs", new { controller = "catalogs", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("catalog-listbycatalogids", "catalogs/{catalogIds}", new { controller = "catalogs", action = "listbycatalogids" }, new { httpMethod = new HttpMethodConstraint("GET"), catalogIds = @"^(\d+,?)+$" });
            config.Routes.MapHttpRoute("catalog-create", "catalogs", new { controller = "catalogs", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("catalog-update", "catalogs/{catalogId}", new { controller = "catalogs", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("catalog-delete", "catalogs/{catalogId}", new { controller = "catalogs", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("catalog-copycatalog", "catalogs", new { controller = "catalogs", action = "copycatalog" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("catalog-deletecatalog", "catalogs/{catalogId}/{preserveCategories}", new { controller = "catalogs", action = "deletecatalog" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Category routes 
            config.Routes.MapHttpRoute("category-get", "categories/{categoryId}", new { controller = "categories", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), categoryId = @"^\d+$" });
            config.Routes.MapHttpRoute("category-list", "categories", new { controller = "categories", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("catalog-listbycategoryids", "categories/{categoryIds}", new { controller = "categories", action = "listbycategoryids" }, new { httpMethod = new HttpMethodConstraint("GET"), categoryIds = @"^(\d+,?)+$" });
            config.Routes.MapHttpRoute("category-listbycatalog", "categories/catalog/{catalogId}", new { controller = "categories", action = "listbycatalog" }, new { httpMethod = new HttpMethodConstraint("GET"), catalogId = @"^\d+$" });
            config.Routes.MapHttpRoute("category-listbycatalogids", "categories/catalog/{catalogIds}", new { controller = "categories", action = "listbycatalogids" }, new { httpMethod = new HttpMethodConstraint("GET"), catalogIds = @"^(\d+,?)+$" });
            config.Routes.MapHttpRoute("category-create", "categories", new { controller = "categories", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("category-update", "categories/{categoryId}", new { controller = "categories", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("category-getcategoriesbycatalogid", "categories/getcategoriesbycatalogid/{catalogId}", new { controller = "categories", action = "getcategoriesbycatalogid" }, new { httpMethod = new HttpMethodConstraint("GET"), catalogId = @"^\d+$" });
            config.Routes.MapHttpRoute("category-getallcategories", "categories/getallcategories/{categoryName}", new { controller = "categories", action = "getallcategories" }, new { httpMethod = new HttpMethodConstraint("GET") });


            // Category node routes
            config.Routes.MapHttpRoute("categorynode-get", "categorynodes/{categoryNodeId}", new { controller = "categorynodes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), categoryNodeId = @"^\d+$" });
            config.Routes.MapHttpRoute("categorynode-list", "categorynodes", new { controller = "categorynodes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("categorynode-create", "categorynodes", new { controller = "categorynodes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("categorynode-update", "categorynodes/{categoryNodeId}", new { controller = "categorynodes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("categorynode-delete", "categorynodes/{categoryNodeId}", new { controller = "categorynodes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Content Page Routes
            config.Routes.MapHttpRoute("contentpage-list", "contentpages", new { controller = "contentpage", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("contentpage-create", "contentpages", new { controller = "contentpage", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("contentpage-copy", "copycontentpage", new { controller = "contentpage", action = "copycontentpage" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("contentpage-addpage", "addpage", new { controller = "contentpage", action = "addpage" }, new { httpMethod = new HttpMethodConstraint("POST") });

            // Country routes
            config.Routes.MapHttpRoute("country-get", "countries/{countryCode}", new { controller = "countries", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("country-list", "countries", new { controller = "countries", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("country-getactivecountrybyportalid", "countries/getactivecountrybyportalid/{portalId}", new { controller = "countries", action = "getactivecountrybyportalid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("country-create", "countries", new { controller = "countries", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("country-update", "countries/{countryCode}", new { controller = "countries", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("country-delete", "countries/{countryCode}", new { controller = "countries", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // CSS routes
            config.Routes.MapHttpRoute("css-list", "csslist", new { controller = "css", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Domain routes
            config.Routes.MapHttpRoute("domain-get", "domains/{domainId}", new { controller = "domains", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("domain-list", "domains", new { controller = "domains", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("domain-create", "domains", new { controller = "domains", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("domain-update", "domains/{domainId}", new { controller = "domains", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("domain-delete", "domains/{domainId}", new { controller = "domains", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //EnvironmentConfig Routes
            config.Routes.MapHttpRoute("environmentconfig-get", "environmentconfig", new { controller = "environmentconfig", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Facet routes
            config.Routes.MapHttpRoute("facet-get", "facet/{facetId}", new { controller = "facetgroups", action = "getfacet" }, new { httpMethod = new HttpMethodConstraint("GET"), facetId = @"^\d+$" });
            config.Routes.MapHttpRoute("facet-create", "facet", new { controller = "facetgroups", action = "createfacet" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("facet-update", "facet/{facetid}", new { controller = "facetgroups", action = "updatefacet" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("facet-delete", "facet/{facetid}", new { controller = "facetgroups", action = "deletefacet" }, new { httpMethod = new HttpMethodConstraint("DELETE") });


            // Facet Groups routes
            config.Routes.MapHttpRoute("facetgroup-get", "facetgroups/{facetGroupId}", new { controller = "facetgroups", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), facetGroupId = @"^\d+$" });
            config.Routes.MapHttpRoute("facetgroup-list", "facetgroups", new { controller = "facetgroups", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("facetgroup-create", "facetgroups", new { controller = "facetgroups", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("facetgroup-update", "facetgroups/{facetgroupid}", new { controller = "facetgroups", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("facetgroup-delete", "facetgroups/{facetgroupid}", new { controller = "facetgroups", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("facetcontroltypes-list", "facetcontroltypes", new { controller = "facetgroups", action = "getcontroltypelist" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("facetgroupcategory-create", "facetgroupcategory", new { controller = "facetgroups", action = "insertfacetgroupcategory" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("facetgroupcategory-delete", "facetgroupcategory/{facetgroupid}", new { controller = "facetgroups", action = "deletefacetgroupcategory" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Gift card routes
            config.Routes.MapHttpRoute("giftcard-get", "giftcards/{giftCardId}", new { controller = "giftcards", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("giftcard-list", "giftcards", new { controller = "giftcards", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("giftcard-create", "giftcards", new { controller = "giftcards", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("giftcard-update", "giftcards/{giftCardId}", new { controller = "giftcards", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("giftcard-delete", "giftcards/{giftCardId}", new { controller = "giftcards", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("giftcard-getnextgiftcardnumber", "giftcardnumber", new { controller = "giftcards", action = "getnextgiftcardnumber" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Highlight routes
            config.Routes.MapHttpRoute("highlight-get", "highlights/{highlightId}", new { controller = "highlights", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("highlight-list", "highlights", new { controller = "highlights", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("highlight-create", "highlights", new { controller = "highlights", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("highlight-update", "highlights/{highlightId}", new { controller = "highlights", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("highlight-delete", "highlights/{highlightId}", new { controller = "highlights", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("highlight-checkassociatedproduct", "highlights/checkassociatedproduct/{highlightId}", new { controller = "highlights", action = "checkassociatedproduct" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Highlight type routes
            config.Routes.MapHttpRoute("highlighttype-get", "highlighttypes/{highlightTypeId}", new { controller = "highlighttypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("highlighttype-list", "highlighttypes", new { controller = "highlighttypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("highlighttype-create", "highlighttypes", new { controller = "highlighttypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("highlighttype-update", "highlighttypes/{highlightTypeId}", new { controller = "highlighttypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("highlighttype-delete", "highlighttypes/{highlightTypeId}", new { controller = "highlighttypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Inventory routes
            config.Routes.MapHttpRoute("inventory-get", "inventory/{inventoryId}", new { controller = "inventory", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("inventory-list", "inventory", new { controller = "inventory", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("inventory-create", "inventory", new { controller = "inventory", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("inventory-update", "inventory/{inventoryId}", new { controller = "inventory", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("inventory-delete", "inventory/{inventoryId}", new { controller = "inventory", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });


            // Locale routes
            config.Routes.MapHttpRoute("locale-list", "locales", new { controller = "locale", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Manufacturer routes
            config.Routes.MapHttpRoute("manufacturer-get", "manufacturers/{manufacturerId}", new { controller = "manufacturers", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("manufacturer-list", "manufacturers", new { controller = "manufacturers", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("manufacturer-create", "manufacturers", new { controller = "manufacturers", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("manufacturer-update", "manufacturers/{manufacturerId}", new { controller = "manufacturers", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("manufacturer-delete", "manufacturers/{manufacturerId}", new { controller = "manufacturers", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // MasterPage routes
            config.Routes.MapHttpRoute("masterpage-list", "masterpages", new { controller = "masterpage", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("masterpage-getmasterpagebythemeid", "masterpages/getmasterpagebythemeid/{themeId}/{pageType}", new { controller = "masterpage", action = "getmasterpagebythemeid" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // MessageConfig routes
            config.Routes.MapHttpRoute("messageconfig-get", "messageconfigs/{messageConfigId}", new { controller = "messageconfigs", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("messageconfig-list", "messageconfigs", new { controller = "messageconfigs", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("messageconfig-listbykeys", "messageconfigs/keys/{keys}", new { controller = "messageconfigs", action = "listbykeys" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("messageconfig-getbykey", "messageconfigs/key/{key}", new { controller = "messageconfigs", action = "GetByKey" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("messageconfig-create", "messageconfigs", new { controller = "messageconfigs", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("messageconfig-update", "messageconfigs/{messageConfigId}", new { controller = "messageconfigs", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("messageconfig-delete", "messageconfigs/{messageConfigId}", new { controller = "messageconfigs", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Order routes
            config.Routes.MapHttpRoute("order-get", "orders/{orderId}", new { controller = "orders", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("order-list", "orders", new { controller = "orders", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("order-create", "orders", new { controller = "orders", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("order-update", "orders/{orderId}", new { controller = "orders", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });

            //Order States routes
            config.Routes.MapHttpRoute("orderstate-list", "orderstates", new { controller = "orderstate", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Znode Version 7.2.2
            //Reorder routes
            config.Routes.MapHttpRoute("reorder-items", "reorder/{orderId}", new { controller = "reorder", action = "getitems" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("reorder-singleitems", "reordersingle/{orderLineItemIds}", new { controller = "reordersingle", action = "getsingleitem" }, new { httpMethod = new HttpMethodConstraint("GET") });
            //config.Routes.MapHttpRoute("reorder-singleitems", "reorder/{orderLineItemIds}", new { controller = "reorder", action = "getsingleitem" }, new { httpMethod = new HttpMethodConstraint("GET") });
            //config.Routes.MapHttpRoute("reorder-itemsList", "reorder/getreorderlist/{orderId}/{orderLineItemId}/{isOrder}", new { controller = "reorder", action = "getreorderlist" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Payment gateway routes
            config.Routes.MapHttpRoute("paymentgateway-get", "paymentgateways/{paymentGatewayId}", new { controller = "paymentgateways", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("paymentgateway-list", "paymentgateways", new { controller = "paymentgateways", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("paymentgateway-create", "paymentgateways", new { controller = "paymentgateways", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("paymentgateway-update", "paymentgateways/{paymentGatewayId}", new { controller = "paymentgateways", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("paymentgateway-delete", "paymentgateways/{paymentGatewayId}", new { controller = "paymentgateways", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Payment option routes
            config.Routes.MapHttpRoute("paymentoption-get", "paymentoptions/{paymentOptionId}", new { controller = "paymentoptions", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("paymentoption-list", "paymentoptions", new { controller = "paymentoptions", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("paymentoption-create", "paymentoptions", new { controller = "paymentoptions", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("paymentoption-update", "paymentoptions/{paymentOptionId}", new { controller = "paymentoptions", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("paymentoption-delete", "paymentoptions/{paymentOptionId}", new { controller = "paymentoptions", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Payment type routes
            config.Routes.MapHttpRoute("paymenttype-get", "paymenttypes/{paymentTypeId}", new { controller = "paymenttypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("paymenttype-list", "paymenttypes", new { controller = "paymenttypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("paymenttype-create", "paymenttypes", new { controller = "paymenttypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("paymenttype-update", "paymenttypes/{paymentTypeId}", new { controller = "paymenttypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("paymenttype-delete", "paymenttypes/{paymentTypeId}", new { controller = "paymenttypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Portal routes
            config.Routes.MapHttpRoute("portal-get", "portals/{portalId}", new { controller = "portals", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), portalId = @"^\d+$" });
            config.Routes.MapHttpRoute("portal-list", "portals", new { controller = "portals", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portal-listbyportalids", "portals/{portalIds}", new { controller = "portals", action = "listbyportalids" }, new { httpMethod = new HttpMethodConstraint("GET"), portalIds = @"^(\d+,?)+$" });
            config.Routes.MapHttpRoute("portal-create", "portals", new { controller = "portals", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("portal-update", "portals/{portalId}", new { controller = "portals", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("portal-delete", "portals/{portalId}", new { controller = "portals", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("portal-getfedexkeys", "getfedexkey", new { controller = "portals", action = "getfedexkey" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portal-createmessage", "createmessages/{portalId}/{localeId}", new { controller = "portals", action = "createmessage" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portal-copy", "copystore/{portalId}", new { controller = "portals", action = "copystore" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portal-deletebyid", "deleteportalbyportalid/{portalId}", new { controller = "portals", action = "deletebyportalid" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Portal catalog routes
            config.Routes.MapHttpRoute("portalcatalog-get", "portalcatalogs/{portalCatalogId}", new { controller = "portalcatalogs", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), portalCatalogId = @"^\d+$" });
            config.Routes.MapHttpRoute("portalcatalog-list", "portalcatalogs", new { controller = "portalcatalogs", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portalcatalog-create", "portalcatalogs", new { controller = "portalcatalogs", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("portalcatalog-update", "portalcatalogs/{portalCatalogId}", new { controller = "portalcatalogs", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("portalcatalog-delete", "portalcatalogs/{portalCatalogId}", new { controller = "portalcatalogs", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Portal country routes
            config.Routes.MapHttpRoute("portalcountry-get", "portalcountries/{portalCountryId}", new { controller = "portalcountries", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portalcountry-list", "portalcountries", new { controller = "portalcountries", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("portalcountry-create", "portalcountries", new { controller = "portalcountries", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("portalcountry-update", "portalcountries/{portalCountryId}", new { controller = "portalcountries", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("portalcountry-delete", "portalcountries/{portalCountryId}", new { controller = "portalcountries", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Product routes
            config.Routes.MapHttpRoute("product-get", "products/{productId}", new { controller = "products", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("product-getproductdetailsbyproductid", "products/getproductdetailsbyproductid/{productId}", new { controller = "products", action = "getproductdetailsbyproductid" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("product-getwithsku", "products/{productId}/{skuId}", new { controller = "products", action = "getwithsku" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("product-list", "products", new { controller = "products", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-listbycatalog", "products/catalog/{catalogId}", new { controller = "products", action = "listbycatalog" }, new { httpMethod = new HttpMethodConstraint("GET"), catalogId = @"^\d+$" });
            config.Routes.MapHttpRoute("product-listbycatalogids", "products/catalog/{catalogIds}", new { controller = "products", action = "listbycatalogids" }, new { httpMethod = new HttpMethodConstraint("GET"), catalogIds = @"^(\d+,?)+$" });
            config.Routes.MapHttpRoute("product-listbycategory", "products/category/{categoryId}", new { controller = "products", action = "listbycategory" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-listbycategorybypromotiontype", "products/category/{categoryId}/promotiontype/{promotionTypeId}", new { controller = "products", action = "listbycategorybypromotiontype" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-listbyexternalids", "products/externalids/{externalIds}", new { controller = "products", action = "listbyexternalids" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-listbyhomespecials", "products/homespecials", new { controller = "products", action = "listbyhomespecials" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-listbyproductids", "products/{productIds}", new { controller = "products", action = "listbyproductids" }, new { httpMethod = new HttpMethodConstraint("GET"), productIds = @"^(\d+,?)+$" });
            config.Routes.MapHttpRoute("product-listbypromotiontype", "products/promotiontype/{promotionTypeId}", new { controller = "products", action = "listbypromotiontype" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("product-create", "products", new { controller = "products", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("product-update", "products/{productId}", new { controller = "products", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("product-delete", "products/{productId}", new { controller = "products", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Attribute routes
            config.Routes.MapHttpRoute("productattribute-get", "productattributes/{attributeId}", new { controller = "productattributes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productattribute-getattributesbyattributetypeid", "productattributes/getattributesbyattributetypeid/{attributeTypeId}", new { controller = "productattributes", action = "getattributesbyattributetypeid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productattribute-list", "productattributes", new { controller = "productattributes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productattribute-create", "productattributes", new { controller = "productattributes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("productattribute-update", "productattributes/{attributeId}", new { controller = "productattributes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("productattribute-delete", "productattributes/{attributeId}", new { controller = "productattributes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Product category routes
            config.Routes.MapHttpRoute("productcategory-get", "productcategories/{productCategoryId}", new { controller = "productcategories", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), productCategoryId = @"^\d+$" });
            config.Routes.MapHttpRoute("productcategory-list", "productcategories", new { controller = "productcategories", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("productcategory-create", "productcategories", new { controller = "productcategories", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("productcategory-update", "productcategories/{productCategoryId}", new { controller = "productcategories", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("productcategory-delete", "productcategories/{productCategoryId}", new { controller = "productcategories", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Product Facets routes
            config.Routes.MapHttpRoute("productfacets-get", "productfacets/{productId}", new { controller = "products", action = "getproductfacets" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("productfacets-update", "productfacets/{productId}", new { controller = "products", action = "bindproductfacets" }, new { httpMethod = new HttpMethodConstraint("PUT") });


            //Product Tags routes
            config.Routes.MapHttpRoute("producttags-get", "producttags/{productId}", new { controller = "products", action = "getproducttag" }, new { httpMethod = new HttpMethodConstraint("GET"), productId = @"^\d+$" });
            config.Routes.MapHttpRoute("producttags-create", "producttags", new { controller = "products", action = "createproducttag" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("producttags-update", "producttags/{tagId}", new { controller = "products", action = "updateproducttag" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("producttags-delete", "producttags/{tagId}", new { controller = "products", action = "deleteproducttag" }, new { httpMethod = new HttpMethodConstraint("DELETE") });


            // Product Type routes
            config.Routes.MapHttpRoute("producttype-get", "producttype/{productTypeId}", new { controller = "producttype", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("producttype-list", "producttype", new { controller = "producttype", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("producttype-getproductattributesbyproducttypeid", "producttype/getproductattributesbyproducttypeid/{productTypeId}", new { controller = "producttype", action = "getproductattributesbyproducttypeid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("producttype-create", "producttypes", new { controller = "producttype", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("producttype-update", "producttype/{productTypeId}", new { controller = "producttype", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("producttype-delete", "producttypes/{productTypeId}", new { controller = "producttype", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Product Type Attribute routes
            config.Routes.MapHttpRoute("producttypeattribute-get", "producttypeattributs/{productTypeAttributId}", new { controller = "categories", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), attributeId = @"^\d+$" });
            config.Routes.MapHttpRoute("producttypeattribute-create", "producttypeattributs", new { controller = "ProductTypeAttribute", action = "Create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("producttypeattribute-getattributetypesbyproducttypeid", "producttypeattribute/getattributetypesbyproducttypeid/{productTypeId}", new { controller = "producttypeattribute", action = "getattributetypesbyproducttypeid" }, new { httpMethod = new HttpMethodConstraint("GET"), productTypeId = @"^\d+$" });
            config.Routes.MapHttpRoute("producttypeattribute-isproductassociatedproducttype", "producttypeattribute/{productTypeId}", new { controller = "producttypeattribute", action = "IsProductAssociatedProductType" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("producttypeattribute-delete", "producttypeattribute/{productTypeAttributId}", new { controller = "producttypeattribute", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Product Review State routes
            config.Routes.MapHttpRoute("productreviewstate-list", "productreviewstates", new { controller = "productreviewstate", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });

            //Profile route
            config.Routes.MapHttpRoute("profile-get", "profiles/{profileId}", new { controller = "profile", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET"), profileId = @"^\d+$" });
            config.Routes.MapHttpRoute("profile-list", "profiles", new { controller = "profile", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("profile-create", "profiles", new { controller = "profile", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("profile-update", "profiles/{facetgroupid}", new { controller = "profile", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("profile-delete", "profiles/{facetgroupid}", new { controller = "profile", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Promotion routes
            config.Routes.MapHttpRoute("promotion-get", "promotions/{promotionId}", new { controller = "promotions", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("promotion-list", "promotions", new { controller = "promotions", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("promotion-create", "promotions", new { controller = "promotions", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("promotion-update", "promotions/{promotionId}", new { controller = "promotions", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("promotion-delete", "promotions/{promotionId}", new { controller = "promotions", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Promotion type routes
            config.Routes.MapHttpRoute("promotiontype-get", "promotiontypes/{promotionTypeId}", new { controller = "promotiontypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("promotiontype-list", "promotiontypes", new { controller = "promotiontypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("promotiontype-create", "promotiontypes", new { controller = "promotiontypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("promotiontype-update", "promotiontypes/{promotionTypeId}", new { controller = "promotiontypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("promotiontype-delete", "promotiontypes/{promotionTypeId}", new { controller = "promotiontypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Review routes
            config.Routes.MapHttpRoute("review-get", "reviews/{reviewId}", new { controller = "reviews", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("review-list", "reviews", new { controller = "reviews", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("review-create", "reviews", new { controller = "reviews", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("review-update", "reviews/{reviewId}", new { controller = "reviews", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("review-delete", "reviews/{reviewId}", new { controller = "reviews", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Search routes
            config.Routes.MapHttpRoute("search-keyword", "search/keyword", new { controller = "search", action = "keyword" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("search-suggested", "search/suggested", new { controller = "search", action = "suggested" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("search-reloadindex", "search/reloadindex", new { controller = "search", action = "reloadindex" }, new { httpMethod = new HttpMethodConstraint("POST") });

            // Shipping option routes
            config.Routes.MapHttpRoute("shippingoption-get", "shippingoptions/{shippingOptionId}", new { controller = "shippingoptions", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingoption-list", "shippingoptions", new { controller = "shippingoptions", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingoption-create", "shippingoptions", new { controller = "shippingoptions", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("shippingoption-update", "shippingoptions/{shippingOptionId}", new { controller = "shippingoptions", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("shippingoption-delete", "shippingoptions/{shippingOptionId}", new { controller = "shippingoptions", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Shipping rule routes
            config.Routes.MapHttpRoute("shippingrule-get", "shippingrules/{shippingRuleId}", new { controller = "shippingrules", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingrule-list", "shippingrules", new { controller = "shippingrules", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingrule-create", "shippingrules", new { controller = "shippingrules", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("shippingrule-update", "shippingrules/{shippingRuleId}", new { controller = "shippingrules", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("shippingrule-delete", "shippingrules/{shippingRuleId}", new { controller = "shippingrules", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Shipping rule type routes
            config.Routes.MapHttpRoute("shippingruletype-get", "shippingruletypes/{shippingRuleTypeId}", new { controller = "shippingruletypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingruletype-list", "shippingruletypes", new { controller = "shippingruletypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingruletype-create", "shippingruletypes", new { controller = "shippingruletypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("shippingruletype-update", "shippingruletypes/{shippingRuleTypeId}", new { controller = "shippingruletypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("shippingruletype-delete", "shippingruletypes/{shippingRuleTypeId}", new { controller = "shippingruletypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Shipping service code routes
            config.Routes.MapHttpRoute("shippingservicecode-get", "shippingservicecodes/{shippingServiceCodeId}", new { controller = "shippingservicecodes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingservicecode-list", "shippingservicecodes", new { controller = "shippingservicecodes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingservicecode-create", "shippingservicecodes", new { controller = "shippingservicecodes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("shippingservicecode-update", "shippingservicecodes/{shippingServiceCodeId}", new { controller = "shippingservicecodes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("shippingservicecode-delete", "shippingservicecodes/{shippingServiceCodeId}", new { controller = "shippingservicecodes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Shipping type routes
            config.Routes.MapHttpRoute("shippingtype-get", "shippingtypes/{shippingTypeId}", new { controller = "shippingtypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingtype-list", "shippingtypes", new { controller = "shippingtypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shippingtype-create", "shippingtypes", new { controller = "shippingtypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("shippingtype-update", "shippingtypes/{shippingTypeId}", new { controller = "shippingtypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("shippingtype-delete", "shippingtypes/{shippingTypeId}", new { controller = "shippingtypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Shopping cart routes
            config.Routes.MapHttpRoute("shoppingcart-getbyaccount", "shoppingcarts/account/{accountId}", new { controller = "shoppingcarts", action = "getbyaccount" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shoppingcart-getbycookie", "shoppingcarts/cookie/{cookieId}", new { controller = "shoppingcarts", action = "getbycookie" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("shoppingcart-create", "shoppingcarts", new { controller = "shoppingcarts", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("shoppingcart-calculate", "shoppingcarts/calculate", new { controller = "shoppingcarts", action = "calculate" }, new { httpMethod = new HttpMethodConstraint("POST") });

            // SKU routes
            config.Routes.MapHttpRoute("sku-get", "skus/{skuId}", new { controller = "skus", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("sku-list", "skus", new { controller = "skus", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("sku-create", "skus", new { controller = "skus", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("sku-update", "skus/{skuId}", new { controller = "skus", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("sku-delete", "skus/{skuId}", new { controller = "skus", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });
            config.Routes.MapHttpRoute("sku-addskuattribute", "skus/addskuattribute/{skuId}/{attributeIds}", new { controller = "skus", action = "addskuattribute" }, new { httpMethod = new HttpMethodConstraint("POST") });

            // State routes
            config.Routes.MapHttpRoute("state-get", "states/{stateCode}", new { controller = "states", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("state-list", "states", new { controller = "states", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("state-create", "states", new { controller = "states", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("state-update", "states/{stateCode}", new { controller = "states", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("state-delete", "states/{stateCode}", new { controller = "states", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Supplier routes
            config.Routes.MapHttpRoute("supplier-get", "suppliers/{supplierId}", new { controller = "suppliers", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("supplier-list", "suppliers", new { controller = "suppliers", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("supplier-create", "suppliers", new { controller = "suppliers", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("supplier-update", "suppliers/{supplierId}", new { controller = "suppliers", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("supplier-delete", "suppliers/{supplierId}", new { controller = "suppliers", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Supplier type routes
            config.Routes.MapHttpRoute("suppliertype-get", "suppliertypes/{supplierTypeId}", new { controller = "suppliertypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("suppliertype-list", "suppliertypes", new { controller = "suppliertypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("suppliertype-create", "suppliertypes", new { controller = "suppliertypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("suppliertype-update", "suppliertypes/{supplierTypeId}", new { controller = "suppliertypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("suppliertype-delete", "suppliertypes/{supplierTypeId}", new { controller = "suppliertypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Tax class routes
            config.Routes.MapHttpRoute("taxclass-get", "taxclasses/{taxClassId}", new { controller = "taxclasses", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("taxclass-list", "taxclasses", new { controller = "taxclasses", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("taxclass-getactivetaxclassbyportalid", "taxclasses/getactivetaxclassbyportalid/{portalId}", new { controller = "taxclasses", action = "getactivetaxclassbyportalid" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("taxclass-create", "taxclasses", new { controller = "taxclasses", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("taxclass-update", "taxclasses/{taxClassId}", new { controller = "taxclasses", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("taxclass-delete", "taxclasses/{taxClassId}", new { controller = "taxclasses", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Tax rule routes
            config.Routes.MapHttpRoute("taxrule-get", "taxrules/{taxRuleId}", new { controller = "taxrules", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("taxrule-list", "taxrules", new { controller = "taxrules", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("taxrule-create", "taxrules", new { controller = "taxrules", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("taxrule-update", "taxrules/{taxRuleId}", new { controller = "taxrules", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("taxrule-delete", "taxrules/{taxRuleId}", new { controller = "taxrules", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Tax rule type routes
            config.Routes.MapHttpRoute("taxruletype-get", "taxruletypes/{taxRuleTypeId}", new { controller = "taxruletypes", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("taxruletype-list", "taxruletypes", new { controller = "taxruletypes", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("taxruletype-create", "taxruletypes", new { controller = "taxruletypes", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("taxruletype-update", "taxruletypes/{taxRuleTypeId}", new { controller = "taxruletypes", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("taxruletype-delete", "taxruletypes/{taxRuleTypeId}", new { controller = "taxruletypes", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //Theme routes
            config.Routes.MapHttpRoute("theme-list", "themes", new { controller = "theme", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });

            // Url Redirect routes
            config.Routes.MapHttpRoute("urlredirect-get", "urlredirects/{urlRedirectId}", new { controller = "urlredirects", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("urlredirect-list", "urlredirects", new { controller = "urlredirects", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("urlredirect-create", "urlredirects", new { controller = "urlredirects", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("urlredirect-update", "urlredirects/{urlRedirectId}", new { controller = "urlredirects", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("urlredirect-delete", "urlredirects/{urlRedirectId}", new { controller = "urlredirects", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            // Wishlist routes
            config.Routes.MapHttpRoute("wishlist-get", "wishlists/{wishListId}", new { controller = "wishlists", action = "get" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("wishlist-list", "wishlists", new { controller = "wishlists", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });
            config.Routes.MapHttpRoute("wishlist-create", "wishlists", new { controller = "wishlists", action = "create" }, new { httpMethod = new HttpMethodConstraint("POST") });
            config.Routes.MapHttpRoute("wishlist-update", "wishlists/{wishListId}", new { controller = "wishlists", action = "update" }, new { httpMethod = new HttpMethodConstraint("PUT") });
            config.Routes.MapHttpRoute("wishlist-delete", "wishlists/{wishListId}", new { controller = "wishlists", action = "delete" }, new { httpMethod = new HttpMethodConstraint("DELETE") });

            //// Facet Groups routes
            //config.Routes.MapHttpRoute("facetgroup-list", "facetgroups", new { controller = "facetgroups", action = "list" }, new { httpMethod = new HttpMethodConstraint("GET") });


            // add routes from plugins
            //PluginManager.Current.GetPlugins().ForEach(x =>
            //{
            //    x.
            //});

        }
    }
}
