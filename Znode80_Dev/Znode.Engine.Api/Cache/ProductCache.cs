﻿using ZNode.Libraries.Framework.Business;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Cache
{
	public class ProductCache : BaseCache, IProductCache
	{
		private readonly IProductService _service;

		public ProductCache(IProductService productService)
		{
			_service = productService;
		}

		public string GetProduct(int productId, string routeUri, string routeTemplate)
		{
			var accountId = UpdateCacheForProfile();

			var data = GetFromCache(routeUri + accountId);
			if (data == null)
			{
				var product = _service.GetProduct(productId, Expands);

				if (product != null)
				{
					var response = new ProductResponse { Product = product };
					data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetProductWithSku(int productId, int skuId, string routeUri, string routeTemplate)
		{
			var accountId = UpdateCacheForProfile();

			var data = GetFromCache(routeUri + accountId);
			if (data == null)
			{
				var product = _service.GetProductWithSku(productId, skuId, Expands);
				if (product != null)
				{
					var response = new ProductResponse { Product = product };
					data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetProducts(string routeUri, string routeTemplate)
		{
			var accountId = UpdateCacheForProfile();

			var data = GetFromCache(routeUri + accountId);
			if (data == null)
			{
				var list = _service.GetProducts(Expands, Filters, Sorts, Page);
				if (list.Products.Count > 0)
				{
					var response = new ProductListResponse { Products = list.Products };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetProductsByCatalog(int catalogId, string routeUri, string routeTemplate)
		{
			var accountId = UpdateCacheForProfile();

			var data = GetFromCache(routeUri + accountId);
			if (data == null)
			{
				var list = _service.GetProductsByCatalog(catalogId, Expands, Filters, Sorts, Page);
				if (list.Products.Count > 0)
				{
					var response = new ProductListResponse { Products = list.Products };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
				}
			}

			return data;
		}

        public string GetProductsByCatalogIds(string catalogIds, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (data == null)
            {
                var list = _service.GetProductsByCatalogIds(catalogIds, Expands, Filters, Sorts, Page);
                if (list.Products.Count > 0)
                {
                    var response = new ProductListResponse { Products = list.Products };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }
        
        public string GetProductsByCategory(int categoryId, string routeUri, string routeTemplate)
		{
			var accountId = UpdateCacheForProfile();

			var data = GetFromCache(routeUri + accountId);
			if (data == null)
			{
				var list = _service.GetProductsByCategory(categoryId, Expands, Filters, Sorts, Page);
				if (list.Products.Count > 0)
				{
					var response = new ProductListResponse { Products = list.Products };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, string routeUri, string routeTemplate)
		{
			var accountId = UpdateCacheForProfile();

			var data = GetFromCache(routeUri + accountId);
			if (data == null)
			{
				var list = _service.GetProductsByCategoryByPromotionType(categoryId, promotionTypeId, Expands, Filters, Sorts, Page);
				if (list.Products.Count > 0)
				{
					var response = new ProductListResponse { Products = list.Products };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetProductsByExternalIds(string externalIds, string routeUri, string routeTemplate)
		{
			var accountId = UpdateCacheForProfile();

			var data = GetFromCache(routeUri + accountId);
			if (data == null)
			{
				var list = _service.GetProductsByExternalIds(externalIds, Expands, Sorts);
				if (list.Products.Count > 0)
				{
					var response = new ProductListResponse { Products = list.Products };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetProductsByHomeSpecials(string routeUri, string routeTemplate)
		{
			var accountId = UpdateCacheForProfile();

			var data = GetFromCache(routeUri + accountId);
			if (data == null)
			{
				var list = _service.GetProductsByHomeSpecials(ZNodeConfigManager.SiteConfig.PortalID, Expands, Filters, Sorts, Page);
				if (list.Products.Count > 0)
				{
					var response = new ProductListResponse { Products = list.Products };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetProductsByProductIds(string productIds, string routeUri, string routeTemplate)
		{
			var accountId = UpdateCacheForProfile();

			var data = GetFromCache(routeUri + accountId);
			if (data == null)
			{
				var list = _service.GetProductsByProductIds(productIds, Expands, Sorts);
				if (list.Products.Count > 0)
				{
					var response = new ProductListResponse { Products = list.Products };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetProductsByPromotionType(int promotionTypeId, string routeUri, string routeTemplate)
		{
			var accountId = UpdateCacheForProfile();

			var data = GetFromCache(routeUri + accountId);
			if (data == null)
			{
				var list = _service.GetProductsByPromotionType(promotionTypeId, Expands, Filters, Sorts, Page);
				if (list.Products.Count > 0)
				{
					var response = new ProductListResponse { Products = list.Products };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
				}
			}

			return data;
		}

        #region Znode Version 8.0
        /// <summary>
        /// Method Gets all the Tags based on Product Id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns> Return Product Tags.</returns>
        public string GetProductTags(int productId, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data,null))
            {
                var productTags = _service.GetProductTags(productId, Expands);

                if (!Equals(productTags,null))
                {
                    var response = new ProductTagResponse { ProductTag = productTags };
                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }
        /// <summary>
        /// Method Gets all the Facets based on Product Id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns> Return Product Facets.</returns>
        public string GetProductFacets(int productId, string routeUri, string routeTemplate)
        {
            var accountId = UpdateCacheForProfile();

            var data = GetFromCache(routeUri + accountId);
            if (Equals(data, null))
            {
                var productFacets = _service.GetProductFacets(productId, Expands);

                if (!Equals(productFacets, null))
                {
                    var response = new ProductFacetsResponse { ProductFacets = productFacets };
                    data = InsertIntoCache(routeUri + accountId, routeTemplate, response);
                }
            }

            return data;
        }


        /// <summary>
        /// To get product details by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns product details</returns>
        public ProductListModel GetProductDetailsByProductId(int productId)
        {
            ProductListModel list = _service.GetProductDetailsByProductId(productId);

            if (list.Products.Count > 0)
            {
                var response = new ProductListResponse { ProductList = list };
                response.MapPagingDataFromModel(list);
            }
            return list;
        }
        #endregion
	}
}