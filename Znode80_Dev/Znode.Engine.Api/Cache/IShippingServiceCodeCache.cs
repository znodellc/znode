﻿namespace Znode.Engine.Api.Cache
{
	public interface IShippingServiceCodeCache
	{
		string GetShippingServiceCode(int shippingServiceCodeId, string routeUri, string routeTemplate);
		string GetShippingServiceCodes(string routeUri, string routeTemplate);
	}
}