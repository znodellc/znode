﻿
namespace Znode.Engine.Api.Cache
{
    public interface IProfileCache
    {
        /// <summary>
        /// Method returns Profile details.
        /// </summary>
        /// <param name="profileId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return profile details.</returns>
        string GetProfile(int profileId, string routeUri, string routeTemplate);

        /// <summary>
        /// Method returns Profile List.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return the Profile list.</returns>
        string GetProfiles(string routeUri, string routeTemplate);
    }
}
