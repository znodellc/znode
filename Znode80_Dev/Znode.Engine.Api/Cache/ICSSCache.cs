﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Cache
{
    public interface ICSSCache
    {
        string GetCSSs(string routeUri, string routeTemplate);
    }
}
