﻿namespace Znode.Engine.Api.Cache
{
	public interface ICaseRequestCache
	{
		string GetCaseRequest(int caseRequestId, string routeUri, string routeTemplate);
		string GetCaseRequests(string routeUri, string routeTemplate);
	}
}