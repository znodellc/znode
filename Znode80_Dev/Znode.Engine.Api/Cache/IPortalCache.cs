﻿namespace Znode.Engine.Api.Cache
{
	public interface IPortalCache
	{
		string GetPortal(int portalId, string routeUri, string routeTemplate);
		string GetPortals(string routeUri, string routeTemplate);
		string GetPortalsByPortalIds(string portalIds, string routeUri, string routeTemplate);
	}
}
