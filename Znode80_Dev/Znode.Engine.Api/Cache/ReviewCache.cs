﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class ReviewCache : BaseCache, IReviewCache
	{
		private readonly IReviewService _service;

		public ReviewCache(IReviewService reviewService)
		{
			_service = reviewService;
		}

		public string GetReview(int reviewId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var review = _service.GetReview(reviewId, Expands);
				if (review != null)
				{
					var response = new ReviewResponse { Review = review };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetReviews(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetReviews(Expands, Filters, Sorts, Page);
				if (list.Reviews.Count > 0)
				{
					var response = new ReviewListResponse { Reviews = list.Reviews };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}