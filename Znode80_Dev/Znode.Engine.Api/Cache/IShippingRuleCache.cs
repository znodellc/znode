﻿namespace Znode.Engine.Api.Cache
{
	public interface IShippingRuleCache
	{
		string GetShippingRule(int shippingRuleId, string routeUri, string routeTemplate);
		string GetShippingRules(string routeUri, string routeTemplate);
	}
}