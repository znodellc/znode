﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Engine.Api.Models.Extensions;

namespace Znode.Engine.Api.Cache
{
    public class ProductReviewStateCache : BaseCache, IProductReviewStateCache
    {
        private readonly IProductReviewStateService _service;

        public ProductReviewStateCache(IProductReviewStateService productReviewStateService)
        {
            _service = productReviewStateService;
        }
        public string GetProductReviewStates(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetProductReviewStates(Expands, Filters, Sorts, Page);
                if (list.ProductReviewStates.Count > 0)
                {
                    var response = new ProductReviewStateListResponse { ProductReviewStates = list.ProductReviewStates };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
    }
}