﻿using System;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class AccountCache : BaseCache, IAccountCache
	{
		private readonly IAccountService _service;

		public AccountCache(IAccountService accountService)
		{
			_service = accountService;
		}

		public string GetAccount(int accountId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var account = _service.GetAccount(accountId, Expands);
				if (account != null)
				{
					var response = new AccountResponse { Account = account };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetAccountByUserId(Guid userId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var account = _service.GetAccountByUserId(userId, Expands);
				if (account != null)
				{
					var response = new AccountResponse { Account = account };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetAccountByUsername(string username, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var account = _service.GetAccountByUsername(username, Expands);
				if (account != null)
				{
					var response = new AccountResponse { Account = account };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetAccounts(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetAccounts(Expands, Filters, Sorts, Page);
				if (list.Accounts.Count > 0)
				{
					var response = new AccountListResponse { Accounts = list.Accounts };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public AccountModel Login(int portalId, AccountModel model, out string errorCode)
		{
			return _service.Login(portalId, model, out errorCode, Expands);
		}
	}
}