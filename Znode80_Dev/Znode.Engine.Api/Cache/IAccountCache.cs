﻿using System;
using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Cache
{
	public interface IAccountCache
	{
		string GetAccount(int accountId, string routeUri, string routeTemplate);
		string GetAccountByUserId(Guid userId, string routeUri, string routeTemplate);
	    string GetAccountByUsername(string username, string routeUri, string routeTemplate);
		string GetAccounts(string routeUri, string routeTemplate);
        AccountModel Login(int portalId, AccountModel model, out string errorCode);
	}
}