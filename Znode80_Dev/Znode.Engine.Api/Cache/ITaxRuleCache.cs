﻿namespace Znode.Engine.Api.Cache
{
	public interface ITaxRuleCache
	{
		string GetTaxRule(int taxRuleId, string routeUri, string routeTemplate);
		string GetTaxRules(string routeUri, string routeTemplate);
	}
}