﻿namespace Znode.Engine.Api.Cache
{
	public interface IStateCache
	{
		string GetState(string stateCode, string routeUri, string routeTemplate);
		string GetStates(string routeUri, string routeTemplate);
	}
}