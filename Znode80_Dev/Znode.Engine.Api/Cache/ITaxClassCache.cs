﻿namespace Znode.Engine.Api.Cache
{
	public interface ITaxClassCache
	{
		string GetTaxClass(int taxClassId, string routeUri, string routeTemplate);
		string GetTaxClasses(string routeUri, string routeTemplate);
	}
}