﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class CategoryCache : BaseCache, ICategoryCache
    {
        private readonly ICategoryService _service;

        public CategoryCache(ICategoryService categoryService)
        {
            _service = categoryService;
        }

        public string GetCategory(int categoryId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var category = _service.GetCategory(categoryId, Expands);
                if (category != null)
                {
                    var response = new CategoryResponse { Category = category };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetCategories(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetCategories(Expands, Filters, Sorts, Page);
                if (list.Categories.Count > 0)
                {
                    var response = new CategoryListResponse { Categories = list.Categories };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetCategoriesByCatalog(int catalogId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetCategoriesByCatalog(catalogId, Expands, Filters, Sorts, Page);
                if (list.Categories.Count > 0)
                {
                    var response = new CategoryListResponse { Categories = list.Categories };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetCategoriesByCatalogIds(string catalogIds, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetCategoriesByCatalogIds(catalogIds, Expands, Filters, Sorts, Page);
                if (list.Categories.Count > 0)
                {
                    var response = new CategoryListResponse { Categories = list.Categories };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        public string GetCategoriesByCategoryIds(string categoryIds, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetCategoriesByCategoryIds(categoryIds, Expands, Filters, Sorts, Page);
                if (list.Categories.Count > 0)
                {
                    var response = new CategoryListResponse { Categories = list.Categories };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

        #region Znode Version 8.0
        public CatalogAssociatedCategoriesListModel GetCategoriesByCatalogIdUsingCustomService(int catalogId, out int totalRowCount, string routeUri, string routeTemplate)
        {
            CatalogAssociatedCategoriesListModel list = _service.GetCategoryByCatalogId(catalogId, Sorts, Page, out totalRowCount);

            if (!Equals(list,null) &&  list.CategoryList.Count > 0)
            {
                var response = new CategoryListResponse { CatalogAssociatedCategories = list };
                response.MapPagingDataFromModel(list);
            }
            return list;
        }

        public CatalogAssociatedCategoriesListModel GetAllCategories(string categoryName, int catalogId, out int totalRowCount, string routeUri, string routeTemplate)
        {
            CatalogAssociatedCategoriesListModel list = _service.GetAllCategories(categoryName, catalogId, Sorts, Page, out totalRowCount);
            if (!Equals(list,null) && list.CategoryList.Count > 0)
            {
                var response = new CategoryListResponse { CatalogAssociatedCategories = list };
                response.MapPagingDataFromModel(list);
            }
            return list;
        }

        #endregion
    }
}