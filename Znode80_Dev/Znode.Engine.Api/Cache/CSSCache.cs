﻿using System;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class CSSCache : BaseCache, ICSSCache
    {
        private readonly ICSSService _service;

        public CSSCache(ICSSService cssService)
        {
            _service = cssService;
        }
        public string GetCSSs(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetCSSs(Expands, Filters, Sorts, Page);
                if (list.CSSs.Count > 0)
                {
                    var response = new CSSListResponse { CSSs = list.CSSs };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }
    }
}