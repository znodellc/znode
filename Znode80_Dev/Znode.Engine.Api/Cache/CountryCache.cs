﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class CountryCache : BaseCache, ICountryCache
	{
		private readonly ICountryService _service;

		public CountryCache(ICountryService countryService)
		{
			_service = countryService;
		}

		public string GetCountry(string countryCode, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var country = _service.GetCountry(countryCode, Expands);
				if (country != null)
				{
					var response = new CountryResponse { Country = country };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetCountries(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetCountries(Expands, Filters, Sorts, Page);
				if (list.Countries.Count > 0)
				{
					var response = new CountryListResponse { Countries = list.Countries };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}