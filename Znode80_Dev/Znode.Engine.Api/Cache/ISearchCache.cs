﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;

namespace Znode.Engine.Api.Cache
{
	public interface ISearchCache
	{
		KeywordSearchResponse GetKeywordSearch(KeywordSearchModel model, string routeUri, string routeTemplate);
		SuggestedSearchListResponse GetSearchSuggestions(SuggestedSearchModel model, string routeUri, string routeTemplate);
		void ReloadIndex();
	}
}