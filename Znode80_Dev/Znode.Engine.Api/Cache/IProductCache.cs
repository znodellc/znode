﻿using Znode.Engine.Api.Models;
namespace Znode.Engine.Api.Cache
{
	public interface IProductCache
	{
		string GetProduct(int productId, string routeUri, string routeTemplate);
		string GetProductWithSku(int productId, int skuId, string routeUri, string routeTemplate);
		string GetProducts(string routeUri, string routeTemplate);
		string GetProductsByCatalog(int catalogId, string routeUri, string routeTemplate);
	    string GetProductsByCatalogIds(string catalogIds, string routeUri, string routeTemplate);
		string GetProductsByCategory(int categoryId, string routeUri, string routeTemplate);
		string GetProductsByCategoryByPromotionType(int categoryId, int promotionTypeId, string routeUri, string routeTemplate);
		string GetProductsByExternalIds(string externalIds, string routeUri, string routeTemplate);
		string GetProductsByHomeSpecials(string routeUri, string routeTemplate);
		string GetProductsByProductIds(string productIds, string routeUri, string routeTemplate);
		string GetProductsByPromotionType(int promotionTypeId, string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 8.0
        /// To get product details by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <param name="routeUri">string routeUri</param>
        /// <param name="routeTemplate">string routeTemplate</param>
        /// <returns>returns product details</returns>
        ProductListModel GetProductDetailsByProductId(int productId);

        /// <summary>
        /// Znode Version 8.0.
        /// Method Return the Product tags, based on product Id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return Product Tags.</returns>
        string GetProductTags(int productId, string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 8.0.
        /// Method Return the Product Facets Details, based on product Id.
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetProductFacets(int productId, string routeUri, string routeTemplate);
	}
}