﻿namespace Znode.Engine.Api.Cache
{
	public interface ICatalogCache
	{
		string GetCatalog(int catalogId, string routeUri, string routeTemplate);
		string GetCatalogs(string routeUri, string routeTemplate);
        string GetCatalogsByCatalogIds(string catalogIds, string routeUri, string routeTemplate);
    }
}