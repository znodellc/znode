﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class ProductCategoryCache : BaseCache, IProductCategoryCache
	{
		private readonly IProductCategoryService _service;

		public ProductCategoryCache(IProductCategoryService productCategoryService)
		{
			_service = productCategoryService;
		}

		public string GetProductCategory(int productCategoryId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var productCategory = _service.GetProductCategory(productCategoryId, Expands);
				if (productCategory != null)
				{
					var response = new ProductCategoryResponse { ProductCategory = productCategory };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetProductCategories(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetProductCategories(Expands, Filters, Sorts, Page);
				if (list.ProductCategories.Count > 0)
				{
					var response = new ProductCategoryListResponse { ProductCategories = list.ProductCategories };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}