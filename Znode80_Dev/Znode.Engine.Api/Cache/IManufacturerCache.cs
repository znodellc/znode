﻿namespace Znode.Engine.Api.Cache
{
	public interface IManufacturerCache
	{
		string GetManufacturer(int manufacturerId, string routeUri, string routeTemplate);
		string GetManufacturers(string routeUri, string routeTemplate);
	}
}