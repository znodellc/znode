﻿namespace Znode.Engine.Api.Cache
{
	public interface IInventoryCache
	{
		string GetInventory(int inventoryId, string routeUri, string routeTemplate);
		string GetInventories(string routeUri, string routeTemplate);
	}
}