﻿using Znode.Engine.Api.Models;
namespace Znode.Engine.Api.Cache
{
    public interface ICategoryCache
    {
        string GetCategory(int categoryId, string routeUri, string routeTemplate);
        string GetCategories(string routeUri, string routeTemplate);
        string GetCategoriesByCatalog(int catalogId, string routeUri, string routeTemplate);
        string GetCategoriesByCatalogIds(string catalogIds, string routeUri, string routeTemplate);
        string GetCategoriesByCategoryIds(string categoryIds, string routeUri, string routeTemplate);

        /// <summary>
        /// Znode Version 8.0
        /// Gets the category list by catalog Id using custom service.       
        /// </summary>
        /// <param name="catalogId">catalogId</param>
        /// <param name="whereClause">whereClause</param>
        /// <param name="havingClause">havingClause</param>
        /// <param name="orderBy">orderBy</param>
        /// <param name="totalRowCount">totalRowCount</param>
        /// <param name="routeUri">routeUri</param>
        /// <param name="routeTemplate">routeTemplate</param>
        /// <returns></returns>
        CatalogAssociatedCategoriesListModel GetCategoriesByCatalogIdUsingCustomService(int catalogId, out int totalRowCount, string routeUri, string routeTemplate);

        /// <summary>
        ///  Znode Version 8.0
        /// Gets all categories.  
        /// </summary>
        /// <param name="categoryName">The name of category.</param>
        /// <param name="catalogId">The Id of catalog.</param>
        /// <param name="totalRowCount">Out type of totalo row count.</param>
        /// <param name="routeUri">route URI.</param>
        /// <param name="routeTemplate">Route Template.</param>
        /// <returns></returns>
        CatalogAssociatedCategoriesListModel GetAllCategories(string categoryName, int catalogId, out int totalRowCount, string routeUri, string routeTemplate);
    }
}