﻿using Znode.Engine.Api.Models;

namespace Znode.Engine.Api.Cache
{
    /// <summary>
    /// Interface for Product Type Attribute Cache
    /// </summary>
    public interface IProductTypeAttributeCache
    {
        /// <summary>
        /// Get Attribute Type
        /// </summary>
        /// <param name="productTypeId">Product Type Id</param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetAttributeType(int productTypeId, string routeUri, string routeTemplate);

        /// <summary>
        /// Get Attribute Types
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        string GetAttributeTypes(string routeUri, string routeTemplate);

        /// <summary>
        /// Gets the attribute Type list by productType Id using custom service.
        /// </summary>
        /// <param name="productTypeId"></param>
        /// <param name="totalRowCount"></param>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        ProductTypeAssociatedAttributeTypesListModel GetAttributeTypeByProductTypeIdUsingCustomService(int productTypeId, out int totalRowCount, string routeUri, string routeTemplate);
    }
}