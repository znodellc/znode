﻿using Znode.Engine.Api.Models;
namespace Znode.Engine.Api.Cache
{
    public interface IProductAttributesCache
    {
        string GetAttribute(int attributeId, string routeUri, string routeTemplate);
        string GetAttributes(string routeUri, string routeTemplate);
        ProductAttributeListModel GetAttributesByAttributeTypeId(int attributeTypeId);
    }
}
