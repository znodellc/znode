﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class CaseRequestCache : BaseCache, ICaseRequestCache
	{
		private readonly ICaseRequestService _service;

		public CaseRequestCache(ICaseRequestService caseRequestService)
		{
			_service = caseRequestService;
		}

		public string GetCaseRequest(int caseRequestId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var caseRequest = _service.GetCaseRequest(caseRequestId, Expands);
				if (caseRequest != null)
				{
					var response = new CaseRequestResponse { CaseRequest = caseRequest };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetCaseRequests(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetCaseRequests(Expands, Filters, Sorts, Page);
				if (list.CaseRequests.Count > 0)
				{
					var response = new CaseRequestListResponse { CaseRequests = list.CaseRequests };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}