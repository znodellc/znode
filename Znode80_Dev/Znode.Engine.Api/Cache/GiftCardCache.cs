﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class GiftCardCache : BaseCache, IGiftCardCache
	{
		private readonly IGiftCardService _service;

		public GiftCardCache(IGiftCardService giftCardService)
		{
			_service = giftCardService;
		}

		public string GetGiftCard(int giftCardId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var giftCard = _service.GetGiftCard(giftCardId, Expands);
				if (giftCard != null)
				{
					var response = new GiftCardResponse { GiftCard = giftCard };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetGiftCards(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetGiftCards(Expands, Filters, Sorts, Page);
				if (list.GiftCards.Count > 0)
				{
					var response = new GiftCardListResponse { GiftCards = list.GiftCards };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}