﻿namespace Znode.Engine.Api.Cache
{
	public interface IShippingRuleTypeCache
	{
		string GetShippingRuleType(int shippingRuleTypeId, string routeUri, string routeTemplate);
		string GetShippingRuleTypes(string routeUri, string routeTemplate);
	}
}