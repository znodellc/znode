﻿namespace Znode.Engine.Api.Cache
{
	public interface IProductCategoryCache
	{
		string GetProductCategory(int productCategoryId, string routeUri, string routeTemplate);
		string GetProductCategories(string routeUri, string routeTemplate);
    }
}