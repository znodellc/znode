﻿namespace Znode.Engine.Api.Cache
{
	public interface IShippingTypeCache
	{
		string GetShippingType(int shippingTypeId, string routeUri, string routeTemplate);
		string GetShippingTypes(string routeUri, string routeTemplate);
	}
}