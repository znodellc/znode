﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Engine.Api.Models.Extensions;

namespace Znode.Engine.Api.Cache
{
    public class OrderStateCache : BaseCache, IOrderStateCache
    {
        private readonly IOrderStateService _service;
        public OrderStateCache(IOrderStateService orderStateService)
        {
            _service = orderStateService;
        }

        public string GetOrderStates(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetOrderStates(Expands, Filters, Sorts, Page);
                if (list.OrderStates.Count > 0)
                {
                    var response = new OrderStateListResponse { OrderStates = list.OrderStates };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }
    }
}