﻿namespace Znode.Engine.Api.Cache
{
	public interface IHighlightCache
	{
		string GetHighlight(int highlightId, string routeUri, string routeTemplate);
		string GetHighlights(string routeUri, string routeTemplate);
	}
}