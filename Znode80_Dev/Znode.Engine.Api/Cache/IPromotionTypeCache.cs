﻿namespace Znode.Engine.Api.Cache
{
	public interface IPromotionTypeCache
	{
		string GetPromotionType(int promotionTypeId, string routeUri, string routeTemplate);
		string GetPromotionTypes(string routeUri, string routeTemplate);
	}
}