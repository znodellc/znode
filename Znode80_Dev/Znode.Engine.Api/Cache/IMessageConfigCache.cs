﻿namespace Znode.Engine.Api.Cache
{
	public interface IMessageConfigCache
	{
		string GetMessageConfig(int messageConfigId, string routeUri, string routeTemplate);
	    string GetMessageConfigByKey(string key, int portalId, string routeUri, string routeTemplate);
		string GetMessageConfigs(string routeUri, string routeTemplate);
		string GetMessageConfigsByKeys(string keys, string routeUri, string routeTemplate);
	}
}