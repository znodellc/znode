﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class ProductTypeAttributeCache : BaseCache , IProductTypeAttributeCache
    {
        #region Private Variable
        private readonly IProductTypeAttributeService _service; 
        #endregion

        #region Public Methods
        public ProductTypeAttributeCache(IProductTypeAttributeService productTypeAttributeService)
        {
            _service = productTypeAttributeService;
        }

        public string GetAttributeType(int productTypeId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data , null))
            {
                var attributeType = _service.GetAttributeType(productTypeId);
                if (!Equals(attributeType , null))
                {
                    var response = new ProductTypeAttributeResponse { ProductTypeAttribute = attributeType };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetAttributeTypes(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data ,null))
            {
                var list = _service.GetAttributeTypes(Filters, Sorts, Page);
                if (list.ProductTypeAttribute.Count > 0)
                {
                    var response = new ProductTypeAttributeListResponse { ProductTypeAttributes = list.ProductTypeAttribute };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public ProductTypeAssociatedAttributeTypesListModel GetAttributeTypeByProductTypeIdUsingCustomService(int productTypeId, out int totalRowCount, string routeUri, string routeTemplate)
        {
            ProductTypeAssociatedAttributeTypesListModel list = _service.GetAttributeTypesByProductTypeId(productTypeId, Sorts, Page, out totalRowCount);

            if (list.AttributeList.Count > 0)
            {
                var response = new ProductTypeAttributeListResponse { ProductTypeAssociatedAttributeTypes = list };
                response.MapPagingDataFromModel(list);
            }
            return list;
        } 
        #endregion
    }
}