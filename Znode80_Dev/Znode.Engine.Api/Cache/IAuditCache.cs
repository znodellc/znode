﻿namespace Znode.Engine.Api.Cache
{
	public interface IAuditCache
	{
		string GetAudit(int auditId, string routeUri, string routeTemplate);
		string GetAudits(string routeUri, string routeTemplate);
	}
}