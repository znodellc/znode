﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class PaymentGatewayCache : BaseCache, IPaymentGatewayCache
	{
		private readonly IPaymentGatewayService _service;

		public PaymentGatewayCache(IPaymentGatewayService paymentGatewayService)
		{
			_service = paymentGatewayService;
		}

		public string GetPaymentGateway(int paymentGatewayId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var paymentGateway = _service.GetPaymentGateway(paymentGatewayId);
				if (paymentGateway != null)
				{
					var response = new PaymentGatewayResponse { PaymentGateway = paymentGateway };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetPaymentGateways(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetPaymentGateways(Filters, Sorts, Page);
				if (list.PaymentGateways.Count > 0)
				{
					var response = new PaymentGatewayListResponse { PaymentGateways = list.PaymentGateways };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}