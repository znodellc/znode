﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Engine.Api.Models.Extensions;

namespace Znode.Engine.Api.Cache
{
    public class ContentPageCache : BaseCache, IContentPageCache
    {
        private readonly IContentPageService _service;

        public ContentPageCache(IContentPageService contentPageService)
        {
            _service = contentPageService;
        }

        public string GetContentPages(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetContentPages(Expands, Filters, Sorts, Page);
                if (list.ContentPages.Count > 0)
                {
                    var response = new ContentPageListResponse { ContentPages = list.ContentPages };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

    }
}