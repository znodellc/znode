﻿namespace Znode.Engine.Api.Cache
{
	public interface IShippingOptionCache
	{
		string GetShippingOption(int shippingOptionId, string routeUri, string routeTemplate);
		string GetShippingOptions(string routeUri, string routeTemplate);
	}
}