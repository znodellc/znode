﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class StateCache : BaseCache, IStateCache
	{
		private readonly IStateService _service;

		public StateCache(IStateService stateService)
		{
			_service = stateService;
		}

		public string GetState(string stateCode, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var state = _service.GetState(stateCode);
				if (state != null)
				{
					var response = new StateResponse { State = state };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetStates(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetStates(Filters, Sorts, Page);
				if (list.States.Count > 0)
				{
					var response = new StateListResponse { States = list.States };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}