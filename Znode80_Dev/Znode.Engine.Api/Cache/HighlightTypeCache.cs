﻿using System.Linq.Expressions;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class HighlightTypeCache : BaseCache, IHighlightTypeCache
	{
		private readonly IHighlightTypeService _service;

		public HighlightTypeCache(IHighlightTypeService highlightTypeService)
		{
			_service = highlightTypeService;
		}

		public string GetHighlightType(int highlightTypeId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var highlightType = _service.GetHighlightType(highlightTypeId);
				if (highlightType != null)
				{
					var response = new HighlightTypeResponse { HighlightType = highlightType };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetHighlightTypes(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetHighlightTypes(Filters, Sorts, Page);
				if (list.HighlightTypes.Count > 0)
				{
					var response = new HighlightTypeListResponse { HighlightTypes = list.HighlightTypes };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}