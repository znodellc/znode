﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class ThemeCache : BaseCache, IThemeCache
    {
        #region Private Variables
        private readonly IThemeService _service; 
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor for ThemeCache.
        /// </summary>
        /// <param name="themeService"></param>
        public ThemeCache(IThemeService themeService)
        {
            _service = themeService;
        } 
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the list of themes.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns></returns>
        public string GetThemes(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetThemes(Filters, Sorts);
                if (list.Themes.Count > 0)
                {
                    var response = new ThemeListResponse { Themes = list.Themes };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        } 
        #endregion

    }
}