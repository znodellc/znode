﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Znode.Engine.Api.Cache
{
   public interface ILocaleCache
    {
        /// <summary>
        /// Method returns Locale List.
        /// </summary>
        /// <param name="routeUri"></param>
        /// <param name="routeTemplate"></param>
        /// <returns>Return the Locale list.</returns>
        string GetLocales(string routeUri, string routeTemplate);
    }
}
