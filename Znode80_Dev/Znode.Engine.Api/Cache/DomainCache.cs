﻿using System;
using System.Web;
using Znode.Engine.Api.Models.Extensions;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class DomainCache : BaseCache, IDomainCache
	{
		private readonly IDomainService _service;

		public DomainCache(IDomainService domainService)
		{
			_service = domainService;
		}

		public string GetDomain(int domainId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var domain = _service.GetDomain(domainId);
				if (domain != null)
				{
					var response = new DomainResponse { Domain = domain };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public Domain GetDomain(string domainName)
		{
			var data = (Domain)HttpRuntime.Cache.Get(domainName);
			if (data == null)
			{
				var domain = _service.GetDomain(domainName);
				if (domain != null)
				{
					HttpRuntime.Cache.Insert(domainName, domain, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromSeconds(20));
				}
				return domain;
			}

			return data;
		}

		public string GetDomains(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetDomains(Filters, Sorts, Page);
				if (list.Domains.Count > 0)
				{
					var response = new DomainListResponse { Domains = list.Domains };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}