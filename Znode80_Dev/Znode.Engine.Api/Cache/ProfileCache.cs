﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class ProfileCache : BaseCache, IProfileCache
    {
        #region Private Variables
        private readonly IProfileService _service; 
        #endregion

        #region Constructor
        /// <summary>
        /// constructor for profile service
        /// </summary>
        /// <param name="profileService"></param>
        public ProfileCache(IProfileService profileService)
        {
            _service = profileService;
        } 
        #endregion

        #region Public Methods

        public string GetProfile(int profileId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var profile = _service.GetProfile(profileId, Expands);
                if (!Equals(profile, null))
                {
                    var response = new ProfileResponse { Profile = profile };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetProfiles(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (Equals(data, null))
            {
                var list = _service.GetProfiles(Expands, Filters, Sorts, Page);
                if (list.Profiles.Count > 0)
                {
                    var response = new ProfileListResponse { Profiles = list.Profiles };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        } 
        #endregion        
    }
}