﻿
namespace Znode.Engine.Api.Cache
{
    public interface IMasterPageCache
    {
        string GetMasterPages(string routeUri, string routeTemplate);
    }
}
