﻿namespace Znode.Engine.Api.Cache
{
	public interface IOrderCache
	{
		string GetOrder(int orderId, string routeUri, string routeTemplate);
		string GetOrders(string routeUri, string routeTemplate);
	}
}