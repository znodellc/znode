﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class OrderCache : BaseCache, IOrderCache
	{
		private readonly IOrderService _service;

		public OrderCache(IOrderService orderService)
		{
			_service = orderService;
		}

		public string GetOrder(int orderId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var order = _service.GetOrder(orderId, Expands);
				if (order != null)
				{
					var response = new OrderResponse { Order = order };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetOrders(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetOrders(Expands, Filters, Sorts, Page);
				if (list.Orders.Count > 0)
				{
					var response = new OrderListResponse { Orders = list.Orders };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}