﻿namespace Znode.Engine.Api.Cache
{
	public interface IPortalCountryCache
	{
		string GetPortalCountry(int portalCountryId, string routeUri, string routeTemplate);
		string GetPortalCountries(string routeUri, string routeTemplate);
	}
}