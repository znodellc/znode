﻿namespace Znode.Engine.Api.Cache
{
	public interface IPaymentOptionCache
	{
		string GetPaymentOption(int paymentOptionId, string routeUri, string routeTemplate);
		string GetPaymentOptions(string routeUri, string routeTemplate);
	}
}