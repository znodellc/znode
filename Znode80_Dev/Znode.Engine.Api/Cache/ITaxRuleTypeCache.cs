﻿namespace Znode.Engine.Api.Cache
{
	public interface ITaxRuleTypeCache
	{
		string GetTaxRuleType(int taxRuleTypeId, string routeUri, string routeTemplate);
		string GetTaxRuleTypes(string routeUri, string routeTemplate);
	}
}