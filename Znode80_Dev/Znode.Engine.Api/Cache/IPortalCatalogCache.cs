﻿namespace Znode.Engine.Api.Cache
{
	public interface IPortalCatalogCache
	{
		string GetPortalCatalog(int portalCatalogId, string routeUri, string routeTemplate);
		string GetPortalCatalogs(string routeUri, string routeTemplate);
    }
}