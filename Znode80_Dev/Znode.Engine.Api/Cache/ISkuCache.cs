﻿namespace Znode.Engine.Api.Cache
{
	public interface ISkuCache
	{
		string GetSku(int skuId, string routeUri, string routeTemplate);
		string GetSkus(string routeUri, string routeTemplate);
	}
}