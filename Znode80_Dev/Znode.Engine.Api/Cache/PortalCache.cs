﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class PortalCache : BaseCache, IPortalCache
	{
		private readonly IPortalService _service;

		public PortalCache(IPortalService portalService)
		{
			_service = portalService;
		}

		public string GetPortal(int portalId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var portal = _service.GetPortal(portalId, Expands);
				if (portal != null)
				{
					var response = new PortalResponse { Portal = portal };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetPortals(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetPortals(Expands, Filters, Sorts, Page);
				if (list.Portals.Count > 0)
				{
					var response = new PortalListResponse { Portals = list.Portals };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetPortalsByPortalIds(string portalIds, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetPortalsByPortalIds(portalIds, Expands, Sorts);
				if (list.Portals.Count > 0)
				{
					var response = new PortalListResponse { Portals = list.Portals };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}