﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class MasterPageCache : BaseCache, IMasterPageCache
    {
        private readonly IMasterPageService _service;

        public MasterPageCache(IMasterPageService masterPageService)
		{
            _service = masterPageService;
		}

        public string GetMasterPages(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetMasterPages(Expands,Filters, Sorts, Page);
                if (list.MasterPages.Count > 0)
                {
                    var response = new MasterPageListResponse { MasterPages = list.MasterPages };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }

            return data;
        }

    }
}