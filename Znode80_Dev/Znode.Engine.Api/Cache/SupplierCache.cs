﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class SupplierCache : BaseCache, ISupplierCache
	{
		private readonly ISupplierService _service;

		public SupplierCache(ISupplierService supplierService)
		{
			_service = supplierService;
		}

		public string GetSupplier(int supplierId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var supplier = _service.GetSupplier(supplierId, Expands);
				if (supplier != null)
				{
					var response = new SupplierResponse { Supplier = supplier };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetSuppliers(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetSuppliers(Expands, Filters, Sorts, Page);
				if (list.Suppliers.Count > 0)
				{
					var response = new SupplierListResponse { Suppliers = list.Suppliers };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}