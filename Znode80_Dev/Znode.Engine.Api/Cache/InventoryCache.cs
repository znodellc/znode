﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class InventoryCache : BaseCache, IInventoryCache
	{
		private readonly IInventoryService _service;

		public InventoryCache(IInventoryService inventoryService)
		{
			_service = inventoryService;
		}

		public string GetInventory(int inventoryId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var inventory = _service.GetInventory(inventoryId, Expands);
				if (inventory != null)
				{
					var response = new InventoryResponse { Inventory = inventory };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetInventories(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetInventories(Expands, Filters, Sorts, Page);
				if (list.Inventories.Count > 0)
				{
					var response = new InventoryListResponse { Inventories = list.Inventories };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}