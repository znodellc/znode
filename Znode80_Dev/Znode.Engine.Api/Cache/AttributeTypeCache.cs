﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class AttributeTypeCache : BaseCache, IAttributeTypeCache
	{
		private readonly IAttributeTypeService _service;

		public AttributeTypeCache(IAttributeTypeService attributeTypeService)
		{
			_service = attributeTypeService;
		}

		public string GetAttributeType(int attributeTypeId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var attributeType = _service.GetAttributeType(attributeTypeId);
				if (attributeType != null)
				{
					var response = new AttributeTypeResponse { AttributeType = attributeType };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetAttributeTypes(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetAttributeTypes(Filters, Sorts, Page);
				if (list.AttributeTypes.Count > 0)
				{
					var response = new AttributeTypeListResponse { AttributeTypes = list.AttributeTypes };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}