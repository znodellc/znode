﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class ManufacturerCache : BaseCache, IManufacturerCache
	{
		private readonly IManufacturerService _service;

		public ManufacturerCache(IManufacturerService manufacturerService)
		{
			_service = manufacturerService;
		}

		public string GetManufacturer(int manufacturerId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var manufacturer = _service.GetManufacturer(manufacturerId);
				if (manufacturer != null)
				{
					var response = new ManufacturerResponse { Manufacturer = manufacturer };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetManufacturers(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetManufacturers(Filters, Sorts, Page);
				if (list.Manufacturers.Count > 0)
				{
					var response = new ManufacturerListResponse { Manufacturers = list.Manufacturers };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}