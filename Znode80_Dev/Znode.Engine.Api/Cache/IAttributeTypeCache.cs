﻿namespace Znode.Engine.Api.Cache
{
	public interface IAttributeTypeCache
	{
		string GetAttributeType(int attributeTypeId, string routeUri, string routeTemplate);
		string GetAttributeTypes(string routeUri, string routeTemplate);
	}
}