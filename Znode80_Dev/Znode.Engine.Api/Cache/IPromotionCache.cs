﻿namespace Znode.Engine.Api.Cache
{
	public interface IPromotionCache
	{
		string GetPromotion(int promotionId, string routeUri, string routeTemplate);
		string GetPromotions(string routeUri, string routeTemplate);
	}
}