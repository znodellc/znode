﻿namespace Znode.Engine.Api.Cache
{
	public interface ICategoryNodeCache
	{
		string GetCategoryNode(int categoryNodeId, string routeUri, string routeTemplate);
		string GetCategoryNodes(string routeUri, string routeTemplate);
    }
}