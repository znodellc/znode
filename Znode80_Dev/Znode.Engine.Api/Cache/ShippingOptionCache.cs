﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class ShippingOptionCache : BaseCache, IShippingOptionCache
	{
		private readonly IShippingOptionService _service;

		public ShippingOptionCache(IShippingOptionService shippingOptionService)
		{
			_service = shippingOptionService;
		}

		public string GetShippingOption(int shippingOptionId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var shippingOption = _service.GetShippingOption(shippingOptionId, Expands);
				if (shippingOption != null)
				{
					var response = new ShippingOptionResponse { ShippingOption = shippingOption };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetShippingOptions(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetShippingOptions(Expands, Filters, Sorts, Page);
				if (list.ShippingOptions.Count > 0)
				{
					var response = new ShippingOptionListResponse { ShippingOptions = list.ShippingOptions };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}