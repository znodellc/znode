﻿using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
    public class ProductTypeCache : BaseCache, IProductTypeCache
    {
        private readonly IProductTypeService _service;

        public ProductTypeCache(IProductTypeService productTypeService)
        {
            _service = productTypeService;
        }

        public string GetProductType(int productTypeId, string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var productType = _service.GetProductType(productTypeId);
                if (productType != null)
                {
                    var response = new ProductTypeResponse { ProductType = productType };
                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public string GetProductTypes(string routeUri, string routeTemplate)
        {
            var data = GetFromCache(routeUri);
            if (data == null)
            {
                var list = _service.GetProductTypes(Filters, Sorts, Page);
                if (list.ProductTypes.Count > 0)
                {
                    var response = new ProductTypeListResponse { ProductTypes = list.ProductTypes };
                    response.MapPagingDataFromModel(list);

                    data = InsertIntoCache(routeUri, routeTemplate, response);
                }
            }
            return data;
        }

        public AttributeTypeValueListModel GetProductAttributesByProductTypeId(int productTypeId)
        {
            AttributeTypeValueListModel list = _service.GetProductAttributesByProductTypeId(productTypeId);

            if (list.AttributeTypeValueList.Count > 0)
            {
                var response = new ProductTypeListResponse { AttributeTypeValues = list };
                response.MapPagingDataFromModel(list);
            }
            return list;
        } 
    }
}