﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class AuditCache : BaseCache, IAuditCache
	{
		private readonly IAuditService _service;

		public AuditCache(IAuditService auditService)
		{
			_service = auditService;
		}

		public string GetAudit(int auditId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var audit = _service.GetAudit(auditId, Expands);
				if (audit != null)
				{
					var response = new AuditResponse { Audit = audit };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetAudits(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetAudits(Expands, Filters, Sorts, Page);
				if (list.Audits.Count > 0)
				{
					var response = new AuditListResponse { Audits = list.Audits };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}