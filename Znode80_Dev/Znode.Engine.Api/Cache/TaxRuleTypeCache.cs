﻿using Znode.Engine.Api.Models.Extensions;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Cache
{
	public class TaxRuleTypeCache : BaseCache, ITaxRuleTypeCache
	{
		private readonly ITaxRuleTypeService _service;

		public TaxRuleTypeCache(ITaxRuleTypeService taxRuleTypeService)
		{
			_service = taxRuleTypeService;
		}

		public string GetTaxRuleType(int taxRuleTypeId, string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var taxRuleType = _service.GetTaxRuleType(taxRuleTypeId);
				if (taxRuleType != null)
				{
					var response = new TaxRuleTypeResponse { TaxRuleType = taxRuleType };
					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}

		public string GetTaxRuleTypes(string routeUri, string routeTemplate)
		{
			var data = GetFromCache(routeUri);
			if (data == null)
			{
				var list = _service.GetTaxRuleTypes(Filters, Sorts, Page);
				if (list.TaxRuleTypes.Count > 0)
				{
					var response = new TaxRuleTypeListResponse { TaxRuleTypes = list.TaxRuleTypes };
					response.MapPagingDataFromModel(list);

					data = InsertIntoCache(routeUri, routeTemplate, response);
				}
			}

			return data;
		}
	}
}