﻿namespace Znode.Engine.Api.Cache
{
	public interface ISupplierTypeCache
	{
		string GetSupplierType(int supplierTypeId, string routeUri, string routeTemplate);
		string GetSupplierTypes(string routeUri, string routeTemplate);
	}
}