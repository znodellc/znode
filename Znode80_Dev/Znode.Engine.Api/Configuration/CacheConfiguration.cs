﻿using System.Collections.ObjectModel;

namespace Znode.Framework.Api.Configuration
{
	public class CacheConfiguration
	{
		public Collection<CacheRoute> CacheRoutes { get; set; }
		public bool Enabled { get; set; }

		public CacheConfiguration()
		{
			CacheRoutes = new Collection<CacheRoute>();
		}
	}
}
