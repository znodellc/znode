﻿using Znode.Framework.Api.Attributes;

namespace Znode.Engine.Api.Attributes
{
	public class SortSalesTaxAttribute : SortAttribute
	{
		public override string Name { get { return "salesTax"; } }
		public override string Description { get { return "This will sort the response by sales tax."; } }
	}
}