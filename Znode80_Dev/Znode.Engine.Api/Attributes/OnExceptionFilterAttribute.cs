﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http.Filters;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;

namespace Znode.Engine.Api.Attributes
{
	public class OnExceptionFilterAttribute : ExceptionFilterAttribute
	{
		private readonly MediaTypeHeaderValue _jsonMediaType = new MediaTypeHeaderValue("application/json");

		public override void OnException(HttpActionExecutedContext context)
		{
			// For generic exceptions
			var znodeEx = context.Exception as ZnodeException;
			if (znodeEx != null)
			{
				var data = new BaseResponse
				{
					HasError = true,
					ErrorCode = znodeEx.ErrorCode,
					ErrorMessage = znodeEx.ErrorMessage
				};
                Exception newEx = new Exception(znodeEx.ErrorMessage, znodeEx);
                Elmah.ErrorSignal.FromCurrentContext().Raise(newEx);
                context.Response = context.Request.CreateResponse(HttpStatusCode.InternalServerError, data, _jsonMediaType);
			}

			// For unauthorized exceptions
			var znodeUnauthorizedEx = context.Exception as ZnodeUnauthorizedException;
			if (znodeUnauthorizedEx != null)
			{
				var data = new BaseResponse
				{
					HasError = true,
					ErrorCode = znodeUnauthorizedEx.ErrorCode,
					ErrorMessage = znodeUnauthorizedEx.ErrorMessage
				};
                context.Response = context.Request.CreateResponse(HttpStatusCode.Unauthorized, data, _jsonMediaType);
			}
			else
			{
				context.Response = context.Request.CreateErrorResponse(HttpStatusCode.InternalServerError, context.Exception.Message, context.Exception);
			}
            
		    //Elmah.ErrorSignal.FromCurrentContext().Raise(context.Exception);
		}
	}
}