﻿using System.Web.Mvc;

namespace Znode.Engine.Api.Areas.PluginDashboard
{
    public class PluginDashboardAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "PluginDashboard";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            context.MapRoute(
                "PluginDashboard_default",
                "PluginDashboard/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
