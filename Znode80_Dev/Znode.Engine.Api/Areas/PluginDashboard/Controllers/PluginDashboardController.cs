﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Plugin;
using Znode.Plugin.Framework.Domain.Services;
using Znode.Plugin.Framework.Models;

namespace Znode.Engine.Api.Areas.PluginDashboard.Controllers
{
    public class PluginDashboardController : Controller
    {
        private readonly PluginService<IApiPlugin> _pluginService;

        public PluginDashboardController()
        {
            _pluginService = new PluginService<IApiPlugin>();
        }

        public ActionResult Index()
        {
            var model = _pluginService.GetPlugins();

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(ConfigurationModel model)
        {
            if (!ModelState.IsValid)
            {
                return Index();
            }

            // save the settings 

            return Index();
        }

        public ActionResult Uninstall(string name)
        {
            _pluginService.Uninstall(name);

            var model = _pluginService.GetPlugins();
            return View("Index", model);
        }

        public ActionResult Install(string name)
        {
            _pluginService.Install(name);

            var model = _pluginService.GetPlugins();
            return View("Index", model);
        }
    }
}
