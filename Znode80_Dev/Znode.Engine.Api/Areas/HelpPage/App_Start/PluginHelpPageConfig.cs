using System.Web.Http;
using Znode.Plugin;
using Znode.Plugin.Framework;

namespace Znode.Engine.Api.Areas.HelpPage.App_Start
{
    public class PluginHelpPageConfig
    {
        public void Register(HttpConfiguration config)
        {
            foreach (var plugin in PluginManager<IApiPlugin>.Current.GetPlugins())
            {
                plugin.SetSampleResponses(config);
            }
        }
    }
}