﻿using System;
using System.Web.Mvc;
using Znode.Engine.Api.Areas.Activate.Models;
using Znode.Engine.Services;

namespace Znode.Engine.Api.Areas.Activate.Controllers
{
	public class ActivateController : Controller
	{
		private ILicenseService _service;

		public ActivateController()
		{
			_service = new LicenseService();
		}

		[HttpGet]
		public ActionResult Index()
		{
			if (_service.ValidateLicense())
			{
				// License is valid
				// return ("");
			}

			return View();
		}

		public ActionResult LicenseInformation()
		{
			return View("LicenseInformation", new ActivatePageApiModel());
		}

		[HttpPost]
		public ActionResult Create(ActivatePageApiModel model)
		{
			if (model.LicenseType != "FreeTrial" && string.IsNullOrEmpty(model.SerialNumber))
			{
				ModelState.AddModelError("SerialNumber", "Serial number is required");
			}

			if (ModelState.IsValid)
			{
				var message = String.Empty;
				if (_service.InstallLicense(model.LicenseType, model.SerialNumber, model.FullName, model.Email, out message))
				{
					// Confirmation page 
					model.Message = "Your Znode license has been successfully activated.";
				}
				else
				{
					model.ErrorMessage = "Failed to activate license. Additional info: " + message;
				}
			}

			return View("LicenseInformation", model);
		}
	}
}
