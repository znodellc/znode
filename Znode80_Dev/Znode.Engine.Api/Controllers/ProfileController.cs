﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
    /// <summary>
    /// Api Controller for profile 
    /// </summary>
    public class ProfileController : BaseController
    {
        #region Private Variables
        private readonly IProfileService _service;
        private readonly IProfileCache _cache; 
        #endregion
        
        #region Controller
        /// <summary>
        /// Api controller for Profile
        /// </summary>
        public ProfileController()
        {
            _service = new ProfileService();
            _cache = new ProfileCache(_service);
        } 
        #endregion

        #region Action methods
        /// <summary>
        /// Gets the profile By ProfileId.
        /// </summary>
        /// <param name="facetGroupId">The ID of the profile.</param>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage Get(int profileId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProfile(profileId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProfileResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Gets a list of Profiles.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;
            try
            {
                var data = _cache.GetProfiles(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProfileListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Creates a new profile.
        /// </summary>
        /// <param name="model">The model of the profile.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] ProfileModel model)
        {
            HttpResponseMessage response;

            try
            {
                var profile = _service.CreateProfile(model);
                if (!Equals(profile, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + profile.ProfileId;

                    response = CreateCreatedResponse(new ProfileResponse { Profile = profile });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Updates an existing profile by profile Id.
        /// </summary>
        /// <param name="productId">The ID of the profile.</param>
        /// <param name="model">The model of the profile.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int profileId, [FromBody] ProfileModel model)
        {
            HttpResponseMessage response;

            try
            {
                var profile = _service.UpdateProfile(profileId, model);
                response = profile != null ? CreateOKResponse(new ProfileResponse { Profile = profile }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        }

        /// <summary>
        /// Deletes an existing profile by profile Id.
        /// </summary>
        /// <param name="productId">The ID of the profile.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int profileId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteProfile(profileId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProfileResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }
            return response;
        } 
        #endregion
    }
}
