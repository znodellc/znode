﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
    public class LocaleController : BaseController
    {
       private readonly ILocaleService _service;
        private readonly ILocaleCache _cache;

        public LocaleController()
        {
            _service = new LocaleService();
            _cache = new LocaleCache(_service);
        }

        /// <summary>
        /// Gets a list of Locales.
        /// </summary>
        /// <returns></returns>
        //[ExpandAccountType, ExpandAddresses, ExpandOrders, ExpandProfiles, ExpandWishList]
        //[FilterAccountTypeId, FilterCompanyName, FilterEmail, FilterEmailOptIn, FilterEnableCustomerPricing, FilterExternalId, FilterIsActive, FilterParentAccountId, FilterProfileId, FilterUserId]
        //[SortAccountId, SortCompanyName, SortCreateDate]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetLocales(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<LocaleListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new LocaleListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

    }
}
