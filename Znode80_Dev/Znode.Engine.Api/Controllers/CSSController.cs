﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
    public class CSSController : BaseController
    {
        private readonly ICSSService _service;
        private readonly ICSSCache _cache;

        public CSSController()
        {
            _service = new CSSService();
            _cache = new CSSCache(_service);
        }

        /// <summary>
        /// Gets a list of CSS.
        /// </summary>
        /// <returns>List of CSS</returns>       
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCSSs(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<CSSListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CSSListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
