﻿using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
    public class SearchController : BaseController
    {
        private readonly ISearchService _service;
        private readonly ISearchCache _cache;

        public SearchController()
        {
            _service = new SearchService();
            _cache = new SearchCache(_service);
        }

        /// <summary>
        /// Performs keyword search.
        /// </summary>
        /// <param name="model">The model of the keyword search.</param>
        /// <returns></returns>
        [Attributes.ExpandCategories, Attributes.ExpandFacets]
        [Attributes.SortName, Attributes.SortRating, Attributes.SortRelevance]
        [HttpPost]
        public HttpResponseMessage Keyword([FromBody] KeywordSearchModel model)
        {
            HttpResponseMessage response;

            var data = _cache.GetKeywordSearch(model, RouteUri, RouteTemplate);
            if (data != null)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, data, MediaTypeFormatter);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
            }



            return response;
        }

        /// <summary>
        /// Performs suggested search, to be used for type-ahead search.
        /// </summary>
        /// <param name="model">The model of the suggested search.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Suggested([FromBody] SuggestedSearchModel model)
        {
            HttpResponseMessage response;


            var data = _cache.GetSearchSuggestions(model, RouteUri, RouteTemplate);
            if (data != null)
            {
                response = Request.CreateResponse(HttpStatusCode.OK, data, MediaTypeFormatter);
            }
            else
            {
                response = Request.CreateResponse(HttpStatusCode.NotFound);
            }



            return response;
        }

        /// <summary>
        /// Reloads the search index.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage ReloadIndex()
        {
            HttpResponseMessage response;

            _cache.ReloadIndex();
            response = CreateOKResponse();

            return response;
        }
    }
}
