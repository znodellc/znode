﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
    /// <summary>
    /// Content Page Controller.
    /// </summary>
    public class ContentPageController : BaseController
    {
        private readonly IContentPageService _service;
        private readonly IContentPageCache _cache;
        
        /// <summary>
        /// Constructor for content page controller.
        /// </summary>
        public ContentPageController()
        {
            _service = new ContentPageService();
            _cache = new ContentPageCache(_service);
        }

        /// <summary>
        /// Gets a list of ContentPage.
        /// </summary>
        /// <returns>List of ContentPage</returns>       
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetContentPages(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ContentPageListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ContentPageListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new Content Page.
        /// </summary>
        /// <param name="model">The model of the Content Page.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] ContentPageModel model)
        {
            HttpResponseMessage response;

            try
            {
                var contentPage = _service.CreateContentPage(model);
                if (contentPage != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + contentPage.ContentPageID;

                    response = CreateCreatedResponse(new ContentPageResponse { ContentPage = contentPage });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ContentPageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Copies a Content Page.
        /// </summary>
        /// <param name="model">The model of the Content Page.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage CopyContentPage([FromBody] ContentPageModel model)
        {
            HttpResponseMessage response;

            try
            {
                ContentPageModel contentPage = _service.Clone(model) as ContentPageModel;
                if (contentPage != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + contentPage.ContentPageID;

                    response = CreateCreatedResponse(new ContentPageResponse { ContentPageCopy = contentPage });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ContentPageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Adds a Content Page.
        /// </summary>
        /// <param name="model">The model of the Content Page.</param>
        /// <returns>HttpResponseMessage</returns>
        [HttpPost]
        public HttpResponseMessage AddPage([FromBody] ContentPageModel model)
        {
            HttpResponseMessage response;

            try
            {
                bool isContentPageAdded = _service.AddPage(model);

                var uri = Request.RequestUri;
                var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + model.ContentPageID;

                response = CreateCreatedResponse(new ContentPageResponse { IsContentPageAdded = isContentPageAdded });
                response.Headers.Add("Location", location);
            }
            catch (Exception ex)
            {
                var data = new ContentPageResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
