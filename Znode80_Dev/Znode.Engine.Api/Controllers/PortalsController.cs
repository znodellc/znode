﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
    public class PortalsController : BaseController
    {
        private readonly IPortalService _service;
        private readonly IPortalCache _cache;

        public PortalsController()
        {
            _service = new PortalService();
            _cache = new PortalCache(_service);
        }

        /// <summary>
        /// Gets a portal.
        /// </summary>
        /// <param name="portalId">The ID of the portal.</param>
        /// <returns></returns>
        [Attributes.ExpandPortalCountries]
        [Attributes.ExpandCatalogs]
        [HttpGet]
        public HttpResponseMessage Get(int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPortal(portalId, RouteUri, RouteTemplate);
                if (data != null)
                {
                    response = CreateOKResponse<PortalResponse>(data);
                }
                else
                {
                    response = CreateNotFoundResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of portals.
        /// </summary>
        /// <returns></returns>
        [Attributes.ExpandCatalogs, Attributes.ExpandPortalCountries]
        [Attributes.FilterCompanyName, Attributes.FilterStoreName, Attributes.FilterExternalId, Attributes.FilterIsActive, Attributes.FilterLocaleId]
        [Attributes.SortCompanyName, Attributes.SortPortalId, Attributes.SortStoreName]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPortals(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<PortalListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of portals for the specified portal IDs.
        /// </summary>
        /// <param name="portalIds">The comma-separated IDs of the portals.</param>
        /// <returns></returns>
        [Attributes.ExpandCatalogs, Attributes.ExpandPortalCountries]
        [Attributes.SortCompanyName, Attributes.SortPortalId, Attributes.SortStoreName]
        [HttpGet]
        public HttpResponseMessage ListByPortalIds(string portalIds)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetPortalsByPortalIds(portalIds, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<PortalListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new portal.
        /// </summary>
        /// <param name="model">The model of the portal.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] PortalModel model)
        {
            HttpResponseMessage response;

            try
            {
                var portal = _service.CreatePortal(model);
                if (portal != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + portal.PortalId;

                    response = CreateCreatedResponse(new PortalResponse { Portal = portal });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing portal.
        /// </summary>
        /// <param name="portalId">The ID of the portal.</param>
        /// <param name="model">The model of the portal.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int portalId, [FromBody] PortalModel model)
        {
            HttpResponseMessage response;

            try
            {
                var portal = _service.UpdatePortal(portalId, model);
                response = portal != null ? CreateOKResponse(new PortalResponse { Portal = portal }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing portal.
        /// </summary>
        /// <param name="portalId">The ID of the portal.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeletePortal(portalId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #region Znode Version 8.0
        /// <summary>
        /// Gets Fedex keys for a portal.
        /// </summary>       
        /// <returns>Returns the Fedex keys of the portal.</returns>
        [HttpGet]
        public HttpResponseMessage GetFedexKey()
        {
            HttpResponseMessage response;

            try
            {
                var data = _service.GetFedexKey();
                response = data != null ? CreateOKResponse(new FedexKeysResponse { fedexkeys = data }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new FedexKeysResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates messages for the newly created portal.
        /// </summary>
        /// <param name="portalId">Portal Id of portal</param>
        /// <param name="localeId">Locale id for the portl.</param>
        /// <returns>HttpResponsMessage.</returns>
        [HttpGet]
        public HttpResponseMessage CreateMessage(int portalId, int localeId)
        {
            HttpResponseMessage response;

            try
            {
               var isMessagesCreated = _service.CreateMessages(portalId, localeId);

                var uri = Request.RequestUri;
                var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + portalId;

                response = isMessagesCreated ? CreateOKResponse(new PortalResponse { IsMessagesCreated = isMessagesCreated }) : CreateInternalServerErrorResponse();

            }
            catch (Exception ex)
            {

                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse();
            }
            return response;
        }

        /// <summary>
        /// Copies a store.
        /// </summary>
        /// <param name="portalId">Portal id of the store to be copied.</param>
        /// <returns>HttpResponseMessage.</returns>
        [HttpGet]
        public HttpResponseMessage CopyStore(int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var isPortalCopied = _service.CopyStore(portalId);

                var uri = Request.RequestUri;
                var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + portalId;

                response = isPortalCopied ? CreateOKResponse(new PortalResponse { IsStoreCopied = isPortalCopied }) : CreateInternalServerErrorResponse();

            }
            catch (Exception ex)
            {

                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse();
            }
            return response;
        }

        /// <summary>
        /// Deletes a portal specified by Portal Id.
        /// </summary>
        /// <param name="portalId">Portal id of the portal to be deleted.</param>
        /// <returns>HttpResponseMessage.</returns>
        [HttpDelete]
        public HttpResponseMessage DeleteByPortalId(int portalId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeletePortalByPortalId(portalId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new PortalResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion
    }
}