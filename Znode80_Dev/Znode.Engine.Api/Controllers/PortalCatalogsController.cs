﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
	public class PortalCatalogsController : BaseController
	{
		private readonly IPortalCatalogService _service;
		private readonly IPortalCatalogCache _cache;

		public PortalCatalogsController()
		{
			_service = new PortalCatalogService();
			_cache = new PortalCatalogCache(_service);
		}

		/// <summary>
		/// Gets a portal catalog.
		/// </summary>
		/// <param name="portalCatalogId">The ID of the portal catalog.</param>
		/// <returns></returns>
		[Attributes.ExpandCatalog, Attributes.ExpandPortal]
		[HttpGet]
		public HttpResponseMessage Get(int portalCatalogId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetPortalCatalog(portalCatalogId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<PortalCatalogResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new PortalCatalogResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of portal catalogs.
		/// </summary>
		/// <returns></returns>
		[Attributes.ExpandCatalog, Attributes.ExpandPortal]
		[Attributes.FilterCatalogId, Attributes.FilterLocaleId, Attributes.FilterPortalId]
		[Attributes.SortCatalogId, Attributes.SortPortalCatalogId, Attributes.SortPortalId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetPortalCatalogs(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<PortalCatalogListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new PortalCatalogListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new portal catalog.
		/// </summary>
		/// <param name="model">The model of the portal catalog.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] PortalCatalogModel model)
		{
			HttpResponseMessage response;

			try
			{
				var portalCatalog = _service.CreatePortalCatalog(model);
				if (portalCatalog != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + portalCatalog.PortalCatalogId;

					response = CreateCreatedResponse(new PortalCatalogResponse { PortalCatalog = portalCatalog });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new PortalCatalogResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing portal catalog.
		/// </summary>
		/// <param name="portalCatalogId">The ID of the portal catalog.</param>
		/// <param name="model">The model of the portal catalog.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int portalCatalogId, [FromBody] PortalCatalogModel model)
		{
			HttpResponseMessage response;

			try
			{
				var portalCatalog = _service.UpdatePortalCatalog(portalCatalogId, model);
				response = portalCatalog != null ? CreateOKResponse(new PortalCatalogResponse { PortalCatalog = portalCatalog }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new PortalCatalogResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing portal catalog.
		/// </summary>
		/// <param name="portalCatalogId">The ID of the portal catalog.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int portalCatalogId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeletePortalCatalog(portalCatalogId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new PortalCatalogResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}