﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
    public class ThemeController : BaseController
    {
        #region Private Variables
        private readonly IThemeService _service;
        private readonly IThemeCache _cache;
        #endregion

        #region Constructor
        /// <summary>
        /// Constuctor for ThemeController
        /// </summary>
        public ThemeController()
        {
            _service = new ThemeService();
            _cache = new ThemeCache(_service);
        }
        #endregion

        #region Public methods
        /// <summary>
        /// Gets the list of Themes.
        /// </summary>
        /// <returns>List of themes.</returns>
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetThemes(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ThemeListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ThemeListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        #endregion
    }
}
