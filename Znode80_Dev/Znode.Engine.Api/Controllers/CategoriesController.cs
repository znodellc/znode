﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
    public class CategoriesController : BaseController
    {
        private readonly ICategoryService _service;
        private readonly ICategoryCache _cache;

        public CategoriesController()
        {
            _service = new CategoryService();
            _cache = new CategoryCache(_service);
        }

        /// <summary>
        /// Gets a category.
        /// </summary>
        /// <param name="categoryId">The ID of the category.</param>
        /// <returns></returns>
        [Attributes.ExpandCategoryNodes, Attributes.ExpandProductIds, Attributes.ExpandSubcategories]
        [HttpGet]
        public HttpResponseMessage Get(int categoryId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategory(categoryId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<CategoryResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of categories.
        /// </summary>
        /// <returns></returns>
        [Attributes.ExpandCategoryNodes, Attributes.ExpandProductIds, Attributes.ExpandSubcategories]
        [Attributes.FilterExternalId, Attributes.FilterIsVisible, Attributes.FilterName, Attributes.FilterPortalId, Attributes.FilterTitle]
        [Attributes.SortCategoryId, Attributes.SortDisplayOrder, Attributes.SortName, Attributes.SortTitle]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategories(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<CategoryListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of products for the specified external IDs.
        /// </summary>
        /// <param name="categoryIds">The comma-seperated category IDs of the categories.</param>
        /// <returns></returns>
        [Attributes.ExpandCategoryNodes, Attributes.ExpandProductIds, Attributes.ExpandSubcategories]
        [Attributes.FilterExternalId, Attributes.FilterIsVisible, Attributes.FilterName, Attributes.FilterPortalId, Attributes.FilterTitle]
        [Attributes.SortCategoryId, Attributes.SortDisplayOrder, Attributes.SortName, Attributes.SortTitle]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ListByCategoryIds(string categoryIds)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategoriesByCategoryIds(categoryIds, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<CategoryListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of categories for a specific catalog.
        /// </summary>
        /// <param name="catalogId">The ID of the catalog.</param>
        /// <returns></returns>
        [Attributes.ExpandCategoryNodes, Attributes.ExpandProductIds, Attributes.ExpandSubcategories]
        [Attributes.FilterExternalId, Attributes.FilterIsVisible, Attributes.FilterName, Attributes.FilterPortalId, Attributes.FilterTitle]
        [Attributes.SortCategoryId, Attributes.SortDisplayOrder, Attributes.SortName, Attributes.SortTitle]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ListByCatalog(int catalogId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategoriesByCatalog(catalogId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<CategoryListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of products for the specified external IDs.
        /// </summary>
        /// <param name="catalogIds">The comma-seperated external IDs of the products.</param>
        /// <returns></returns>
        [Attributes.ExpandCategoryNodes, Attributes.ExpandProductIds, Attributes.ExpandSubcategories]
        [Attributes.FilterExternalId, Attributes.FilterIsVisible, Attributes.FilterName, Attributes.FilterPortalId, Attributes.FilterTitle]
        [Attributes.SortCategoryId, Attributes.SortDisplayOrder, Attributes.SortName, Attributes.SortTitle]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ListByCatalogIds(string catalogIds)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetCategoriesByCatalogIds(catalogIds, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<CategoryListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new category.
        /// </summary>
        /// <param name="model">The model of the category.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] CategoryModel model)
        {
            HttpResponseMessage response;

            try
            {
                var category = _service.CreateCategory(model);
                if (category != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + category.CategoryId;

                    response = CreateCreatedResponse(new CategoryResponse { Category = category });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new CategoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing category.
        /// </summary>
        /// <param name="categoryId">The ID of the category.</param>
        /// <param name="model">The model of the category.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int categoryId, [FromBody] CategoryModel model)
        {
            HttpResponseMessage response;

            try
            {
                var category = _service.UpdateCategory(categoryId, model);
                response = category != null ? CreateOKResponse(new CategoryResponse { Category = category }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #region Znode Version 8.0
        /// <summary>
        /// Gets categories by catalog Id.
        /// </summary>
        /// <param name="catalogId">The Id of catalog.</param>
        /// <returns>Returns categories by catalog Id.</returns>
        [PageIndex, PageSize]
        [Attributes.SortCategoryId]
        [HttpGet]
        public HttpResponseMessage GetCategoriesByCatalogId(int catalogId, int totalRowCount = 0)
        {
            HttpResponseMessage response;

            try
            {
                CatalogAssociatedCategoriesListModel category = _cache.GetCategoriesByCatalogIdUsingCustomService(catalogId, out totalRowCount, RouteUri, RouteTemplate);

                response = !Equals(category, null) ? CreateOKResponse(new CategoryListResponse { CatalogAssociatedCategories = category, TotalResults = totalRowCount }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets all categories.
        /// </summary>
        /// <param name="categoryName">Name of category.</param>
        /// <returns>Returns all categories.</returns>
        [HttpGet]
        public HttpResponseMessage GetAllCategories(string categoryName)
        {
            int totalRowCount = 0;
            string[] splitarr = categoryName.Split('_');
            categoryName = splitarr[0];
            int catalogId = Convert.ToInt32(splitarr[1]);
            HttpResponseMessage response;

            try
            {
                CatalogAssociatedCategoriesListModel category = _cache.GetAllCategories(categoryName, catalogId, out totalRowCount, RouteUri, RouteTemplate);

                response = !Equals(category, null) ? CreateOKResponse(new CategoryListResponse { CatalogAssociatedCategories = category, TotalResults = totalRowCount }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new CategoryResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion
    }
}
