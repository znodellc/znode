﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
	public class PortalCountriesController : BaseController
	{
		private readonly IPortalCountryService _service;
		private readonly IPortalCountryCache _cache;

		public PortalCountriesController()
		{
			_service = new PortalCountryService();
			_cache = new PortalCountryCache(_service);
		}

		/// <summary>
		/// Gets a portal country.
		/// </summary>
		/// <param name="portalCountryId">The ID of the portal country.</param>
		/// <returns></returns>
		[Attributes.ExpandCountry]
		[HttpGet]
		public HttpResponseMessage Get(int portalCountryId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetPortalCountry(portalCountryId,  RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<PortalCountryResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new PortalCountryResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of portal countries.
		/// </summary>
		/// <returns></returns>
		[Attributes.ExpandCountry]
		[Attributes.FilterCountryCode, Attributes.FilterIsBillingActive, Attributes.FilterIsShippingActive, Attributes.FilterPortalId]
		[Attributes.SortCountryCode, Attributes.SortPortalCountryId]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetPortalCountries(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<PortalCountryListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new PortalCountryListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new portal country.
		/// </summary>
		/// <param name="model">The model of the portal country.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] PortalCountryModel model)
		{
			HttpResponseMessage response;

			try
			{
				var portalCountry = _service.CreatePortalCountry(model);
				if (portalCountry != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + portalCountry.PortalCountryId;

					response = CreateCreatedResponse(new PortalCountryResponse { PortalCountry = portalCountry });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new PortalCountryResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing portal country.
		/// </summary>
		/// <param name="portalCountryId">The ID of the portal country.</param>
		/// <param name="model">The model of the portal country.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int portalCountryId, [FromBody] PortalCountryModel model)
		{
			HttpResponseMessage response;

			try
			{
				var portalCountry = _service.UpdatePortalCountry(portalCountryId, model);
				response = portalCountry != null ? CreateOKResponse(new PortalCountryResponse { PortalCountry = portalCountry }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new PortalCountryResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing portal country.
		/// </summary>
		/// <param name="portalCountryId">The ID of the portal country.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int portalCountryId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeletePortalCountry(portalCountryId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new PortalCountryResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}