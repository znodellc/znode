﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Api.Models;
using Znode.Engine.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
	public class OrdersController : BaseController
	{
		private readonly IOrderService _service;
		private readonly IOrderCache _cache;

		public OrdersController()
		{
			_service = new OrderService();
			_cache = new OrderCache(_service);
		}

		/// <summary>
		/// Gets an order.
		/// </summary>
		/// <param name="orderId">The ID of the order.</param>
		/// <returns></returns>
		[Attributes.ExpandAccount, Attributes.ExpandOrderLineItems, Attributes.ExpandPaymentOption, Attributes.ExpandPaymentType, Attributes.ExpandShippingOption]
		[HttpGet]
		public HttpResponseMessage Get(int orderId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetOrder(orderId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<OrderResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new OrderResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of orders.
		/// </summary>
		/// <returns></returns>
		[Attributes.ExpandAccount, Attributes.ExpandOrderLineItems, Attributes.ExpandPaymentOption, Attributes.ExpandPaymentType, Attributes.ExpandShippingOption]
		[Attributes.FilterAccountId, Attributes.FilterCompanyName, Attributes.FilterCountryCode, Attributes.FilterEmail, Attributes.FilterExternalId, Attributes.FilterLastName, Attributes.FilterOrderDate, Attributes.FilterOrderStateId, Attributes.FilterPortalId, Attributes.FilterPostalCode, Attributes.FilterPurchaseOrderNumber, Attributes.FilterStateCode, Attributes.FilterTotal, Attributes.FilterTrackingNumber, Attributes.FilterTransactionId]
		[Attributes.SortCity, Attributes.SortCompanyName, Attributes.SortCountryCode, Attributes.SortLastName, Attributes.SortOrderDate, Attributes.SortOrderId, Attributes.SortPostalCode, Attributes.SortStateCode, Attributes.SortTotal]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetOrders(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<OrderListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new OrderListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new order.
		/// </summary>
		/// <param name="model">The model of the order.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] ShoppingCartModel model)
		{
			HttpResponseMessage response;

			try
			{
				var order = _service.CreateOrder(PortalId,model);
				if (order != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + order.OrderId;

                    if (order.IsOffsitePayment)
                    {
                        response = CreateOKResponse(new OrderResponse { Order = order });
                    }
                    else
                    {
                        response = CreateCreatedResponse(new OrderResponse { Order = order });
                    }

					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new OrderResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing order.
		/// </summary>
		/// <param name="orderId">The ID of the order.</param>
		/// <param name="model">The model of the order.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int orderId, [FromBody] OrderModel model)
		{
			HttpResponseMessage response;

			try
			{
				var order = _service.UpdateOrder(orderId, model);
				response = order != null ? CreateOKResponse(new OrderResponse { Order = order }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new OrderResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}
