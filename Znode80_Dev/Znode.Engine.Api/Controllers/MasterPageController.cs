﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Services;
using Znode.Engine.Api.Models.Responses;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
    public class MasterPageController : BaseController
    {
        private readonly IMasterPageService _service;
        private readonly IMasterPageCache _cache;

        public MasterPageController()
		{
			_service = new MasterPageService();
			_cache = new MasterPageCache(_service);
		}

        /// <summary>
        /// Gets a list of Master Page.
        /// </summary>
        /// <returns>List of Master Page</returns>       
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetMasterPages(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<MasterPageListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new MasterPageListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of Master Page.
        /// </summary>
        /// <returns>List of Master Page</returns>       
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage GetMasterPageByThemeId(int themeId, string pageType)
        {
            HttpResponseMessage response;
            
            try
            {
                var data = _service.GetMasterPageListByThemeId(themeId, pageType);
                response = data != null ? CreateOKResponse(new MasterPageListResponse{ MasterPages = data.MasterPages}) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new MasterPageListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
    }
}
