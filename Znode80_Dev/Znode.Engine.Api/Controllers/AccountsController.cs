﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
	public class AccountsController : BaseController
	{
		private readonly IAccountService _service;
		private readonly IAccountCache _cache;

		public AccountsController()
		{
			_service = new AccountService();
			_cache = new AccountCache(_service);
		}

		/// <summary>
		/// Gets an account.
		/// </summary>
		/// <param name="accountId">The ID of the account.</param>
		/// <returns></returns>
		[Attributes.ExpandAccountType, Attributes.ExpandAddresses, Attributes.ExpandOrders, Attributes.ExpandProfiles, Attributes.ExpandUser, Attributes.ExpandWishList]
		[HttpGet]
		public HttpResponseMessage Get(int accountId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetAccount(accountId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<AccountResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets an account.
		/// </summary>
		/// <param name="userId">The user ID (guid) of the account.</param>
		/// <returns></returns>
		[Attributes.ExpandAccountType, Attributes.ExpandAddresses, Attributes.ExpandOrders, Attributes.ExpandProfiles, Attributes.ExpandUser, Attributes.ExpandWishList]
		[HttpGet]
		public HttpResponseMessage GetByUserId(Guid userId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetAccountByUserId(userId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<AccountResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets an account.
		/// </summary>
		/// <param name="username">The user name of the account.</param>
		/// <returns></returns>
		[Attributes.ExpandAccountType, Attributes.ExpandAddresses, Attributes.ExpandOrders, Attributes.ExpandProfiles, Attributes.ExpandUser, Attributes.ExpandWishList]
		[HttpGet]
		public HttpResponseMessage GetByUsername(string username)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetAccountByUsername(username, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<AccountResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of accounts.
		/// </summary>
		/// <returns></returns>
		[Attributes.ExpandAccountType, Attributes.ExpandAddresses, Attributes.ExpandOrders, Attributes.ExpandProfiles, Attributes.ExpandWishList]
		[Attributes.FilterAccountTypeId, Attributes.FilterCompanyName, Attributes.FilterEmail, Attributes.FilterEmailOptIn, Attributes.FilterEnableCustomerPricing, Attributes.FilterExternalId, Attributes.FilterIsActive, Attributes.FilterParentAccountId, Attributes.FilterProfileId, Attributes.FilterUserId]
		[Attributes.SortAccountId, Attributes.SortCompanyName, Attributes.SortCreateDate]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetAccounts(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<AccountListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new AccountListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

	    /// <summary>
	    /// Creates a new account.
	    /// </summary>
	    /// <param name="model">The model of the account.</param>
	    /// <returns></returns>
	    [HttpPost]
	    public HttpResponseMessage Create([FromBody] AccountModel model)
	    {
	        HttpResponseMessage response;
	        try
	        {
	            var account = _service.CreateAccount(PortalId, model);
	            if (account != null)
	            {
	                var uri = Request.RequestUri;
	                var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + account.AccountId;

	                response = CreateCreatedResponse(new AccountResponse {Account = account});
	                response.Headers.Add("Location", location);
	            }
	            else
	            {
	                response = CreateInternalServerErrorResponse();
	            }
	        }
            catch (ZnodeException ex)
            {
                var data = new AccountResponse { HasError = true, ErrorCode = ex.ErrorCode, ErrorMessage = ex.ErrorMessage };
                response = CreateInternalServerErrorResponse(data);
            }
	        catch (Exception ex)
	        {
	            var data = new AccountResponse {HasError = true, ErrorMessage = ex.Message};
	            response = CreateInternalServerErrorResponse(data);
	        }

	        return response;
	    }

	    /// <summary>
		/// Updates an existing account.
		/// </summary>
		/// <param name="accountId">The ID of the account.</param>
		/// <param name="model">The model of the account.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int accountId, [FromBody] AccountModel model)
		{
			HttpResponseMessage response;

			try
			{
				var account = _service.UpdateAccount(accountId, model);
				response = account != null ? CreateOKResponse(new AccountResponse { Account = account }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing account.
		/// </summary>
		/// <param name="accountId">The ID of the account.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int accountId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteAccount(accountId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Logs in an existing user.
		/// </summary>
		/// <param name="model">The model of the account.</param>
		/// <returns></returns>
		[Attributes.ExpandAccountType, Attributes.ExpandAddresses, Attributes.ExpandOrders, Attributes.ExpandProfiles, Attributes.ExpandWishList]
		[HttpPost]
		public HttpResponseMessage Login([FromBody] AccountModel model)
		{
			HttpResponseMessage response;

			string errorCode;
			var account = _cache.Login(PortalId, model, out errorCode);
			if (account != null)
			{
				var data = new AccountResponse { Account = account, ErrorCode = Convert.ToInt32(errorCode), HasError = errorCode != "0" };
				response = CreateOKResponse(data);
			}
			else
			{
				var data = new AccountResponse { Account = null, ErrorCode = 10002, HasError = true, ErrorMessage = "Login Unsucessfull" };
				response = CreateUnauthorizedResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Resets the password for an existing user.
		/// </summary>
		/// <param name="model">The model of the account.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage ResetPassword([FromBody] AccountModel model)
		{
			HttpResponseMessage response;

			try
			{
				var account = _service.ResetPassword(model);
				response = account != null ? CreateOKResponse(new AccountResponse { Account = account }) : null;
				//CreateUnauthorizedResponse();
			}
			catch (Exception ex)
			{
				var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Changes the password of an existing user.
		/// </summary>
		/// <param name="model">The model of the account.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage ChangePassword([FromBody] AccountModel model)
		{
			HttpResponseMessage response;

			try
			{
				var account = _service.ChangePassword(PortalId, model);
				response = account != null ? CreateOKResponse(new AccountResponse { Account = account }) : null;
				//CreateUnauthorizedResponse();
			}
			catch (Exception ex)
			{
				var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

        #region Znode Version 7.2.2
        /// <summary>
        /// Checks Reset Password Link status.
        /// </summary>
        /// <param name="model">The model of the account.</param>
        /// <returns>Returns the link status</returns>
        [HttpPost]
        public HttpResponseMessage CheckResetPasswordLinkStatus([FromBody] AccountModel model)
        {
            HttpResponseMessage response;
            string errorCode;
            try
            {
                var account = _service.CheckResetPasswordLinkStatus(model, out errorCode);
                response = account != null ? CreateOKResponse(new AccountResponse { Account = account, ErrorCode = Convert.ToInt32(errorCode), HasError = errorCode != "0" }) : null;
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion

        #region Znode Version 8.0
        /// <summary>
        /// Check for the user role based on role name.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns status for the user based on role name</returns>
        [HttpPost]
        public HttpResponseMessage CheckUserRole([FromBody] AccountModel model)
        {
            HttpResponseMessage response;
            try
            {
                var data = _service.CheckUserRole(model.UserName,model.RoleName);
                response = data != null ? CreateOKResponse<AccountResponse>(new AccountResponse { Account = data }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        

        /// <summary>
        /// Method used to Reset Admin Details in case of first default admin.
        /// </summary>
        /// <param name="model"></param>
        /// <returns>Returns status for reset of admin details</returns>
        [HttpPost]
        public HttpResponseMessage ResetAdminDetails([FromBody] AccountModel model)
        {
            HttpResponseMessage response;
            try
            {
                var data = _service.ResetAdminDetails(model);
                response = data != null ? CreateOKResponse<AccountResponse>(new AccountResponse { Account = data }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion
    }
}
