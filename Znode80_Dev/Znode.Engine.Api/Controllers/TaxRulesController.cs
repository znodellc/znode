﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
	public class TaxRulesController : BaseController
	{
		private readonly ITaxRuleService _service;
		private readonly ITaxRuleCache _cache;

		public TaxRulesController()
		{
			_service = new TaxRuleService();
			_cache = new TaxRuleCache(_service);
		}

		/// <summary>
		/// Gets a tax rule.
		/// </summary>
		/// <param name="taxRuleId">The ID of the tax rule.</param>
		/// <returns></returns>
		[Attributes.ExpandTaxClass, Attributes.ExpandTaxRuleType]
		[HttpGet]
		public HttpResponseMessage Get(int taxRuleId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetTaxRule(taxRuleId,  RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<TaxRuleResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new TaxRuleResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of tax rules.
		/// </summary>
		/// <returns></returns>
		[Attributes.ExpandTaxClass, Attributes.ExpandTaxRuleType]
		[Attributes.FilterCountryCode, Attributes.FilterExternalId, Attributes.FilterPortalId, Attributes.FilterStateCode, Attributes.FilterTaxClassId, Attributes.FilterTaxRuleTypeId]
		[Attributes.SortCountryCode, Attributes.SortGst, Attributes.SortHst, Attributes.SortPrecedence, Attributes.SortPst, Attributes.SortSalesTax, Attributes.SortStateCode, Attributes.SortTaxRuleId, Attributes.SortVat]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetTaxRules(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<TaxRuleListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new TaxRuleListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new tax rule.
		/// </summary>
		/// <param name="model">The model of the tax rule.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] TaxRuleModel model)
		{
			HttpResponseMessage response;

			try
			{
				var taxRule = _service.CreateTaxRule(model);
				if (taxRule != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + taxRule.TaxRuleId;

					response = CreateCreatedResponse(new TaxRuleResponse { TaxRule = taxRule });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new TaxRuleResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing tax rule.
		/// </summary>
		/// <param name="taxRuleId">The ID of the tax rule.</param>
		/// <param name="model">The model of the tax rule.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int taxRuleId, [FromBody] TaxRuleModel model)
		{
			HttpResponseMessage response;

			try
			{
				var taxRule = _service.UpdateTaxRule(taxRuleId, model);
				response = taxRule != null ? CreateOKResponse(new TaxRuleResponse { TaxRule = taxRule }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new TaxRuleResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing tax rule.
		/// </summary>
		/// <param name="taxRuleId">The ID of the tax rule.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int taxRuleId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteTaxRule(taxRuleId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new TaxRuleResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}