﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
    public class GiftCardsController : BaseController
    {
        private readonly IGiftCardService _service;
        private readonly IGiftCardCache _cache;

        public GiftCardsController()
        {
            _service = new GiftCardService();
            _cache = new GiftCardCache(_service);
        }

        /// <summary>
        /// Gets a gift card.
        /// </summary>
        /// <param name="giftCardId">The ID of the gift card.</param>
        /// <returns></returns>
        [Attributes.ExpandGiftCardHistory, Attributes.ExpandOrderLineItem]
        [HttpGet]
        public HttpResponseMessage Get(int giftCardId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetGiftCard(giftCardId, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<GiftCardResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new GiftCardResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets a list of gift cards.
        /// </summary>
        /// <returns></returns>
        [Attributes.ExpandGiftCardHistory, Attributes.ExpandOrderLineItem]
        [Attributes.FilterAccountId, Attributes.FilterAmount, Attributes.FilterCardNumber, Attributes.FilterCreateDate, Attributes.FilterCreatedBy, Attributes.FilterExpirationDate, Attributes.FilterName, Attributes.FilterOrderLineItemId, Attributes.FilterPortalId]
        [Attributes.SortAmount, Attributes.SortCardNumber, Attributes.SortCreateDate, Attributes.SortExpirationDate, Attributes.SortGiftCardId, Attributes.SortName]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage List()
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetGiftCards(RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<GiftCardListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new GiftCardListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Creates a new gift card.
        /// </summary>
        /// <param name="model">The model of the gift card.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage Create([FromBody] GiftCardModel model)
        {
            HttpResponseMessage response;

            try
            {
                var giftCard = _service.CreateGiftCard(model);
                if (giftCard != null)
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + giftCard.GiftCardId;

                    response = CreateCreatedResponse(new GiftCardResponse { GiftCard = giftCard });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new GiftCardResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing gift card.
        /// </summary>
        /// <param name="giftCardId">The ID of the gift card.</param>
        /// <param name="model">The model of the gift card.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage Update(int giftCardId, [FromBody] GiftCardModel model)
        {
            HttpResponseMessage response;

            try
            {
                var giftCard = _service.UpdateGiftCard(giftCardId, model);
                response = giftCard != null ? CreateOKResponse(new GiftCardResponse { GiftCard = giftCard }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new GiftCardResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing gift card.
        /// </summary>
        /// <param name="giftCardId">The ID of the gift card.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage Delete(int giftCardId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteGiftCard(giftCardId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new GiftCardResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        /// <summary>
        /// This method will generate a new GiftCard Number
        /// </summary>
        /// <returns>Returns Next Generated GiftCard Number</returns>
        [HttpGet]
        public HttpResponseMessage GetNextGiftCardNumber()
        {
            HttpResponseMessage response;

            try
            {
                var GiftCardNumber = _service.GetNextGiftCardNumber();
                response = GiftCardNumber != null ? CreateOKResponse(new GiftCardResponse { GiftCard = GiftCardNumber }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var GiftCardNumber = new GiftCardResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(GiftCardNumber);
            }

            return response;
        }
    }
}