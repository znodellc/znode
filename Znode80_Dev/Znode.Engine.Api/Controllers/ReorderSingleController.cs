﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Exceptions;
using Znode.Engine.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;


namespace Znode.Engine.Api.Controllers
{
    public class ReorderSingleController : BaseController
    {
        private readonly IReorderService _service;
        private readonly IReorderCache _cache;

        public ReorderSingleController()
        {
            _service = new ReorderService();
            _cache = new ReorderCache(_service);
        }

        #region Znode Version 7.2.2

        /// <summary>
        /// Znode Version 7.2.2
        ///Get reorder single item.
        ///This function helps to reorder single items from the order
        /// </summary>
        /// <param name="orderLineItemIds">orderLineItemIds</param>
        /// <returns>returns HttpResponseMessage</returns>
        [Attributes.ExpandAccountType, Attributes.ExpandAddresses, Attributes.ExpandOrders, Attributes.ExpandProfiles, Attributes.ExpandUser, Attributes.ExpandWishList]
        [HttpGet]
        public HttpResponseMessage GetSingleitem(int orderLineItemIds)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetReorderSingleItem(orderLineItemIds, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<AccountResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new AccountResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion
    }
}
