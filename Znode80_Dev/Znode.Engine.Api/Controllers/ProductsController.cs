﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
	public class ProductsController : BaseController
	{
		private readonly IProductService _service;
		private readonly IProductCache _cache;

		public ProductsController()
		{
			_service = new ProductService();
			_cache = new ProductCache(_service);
		}

		/// <summary>
		/// Gets a product.
		/// </summary>
		/// <param name="productId">The ID of the product.</param>
		/// <returns></returns>
		[Attributes.ExpandAddOns, Attributes.ExpandAttributes, Attributes.ExpandCategories, Attributes.ExpandCrossSells, Attributes.ExpandFrequentlyBoughtTogether, Attributes.ExpandHighlights, Attributes.ExpandImages, Attributes.ExpandManufacturer, Attributes.ExpandProductType, Attributes.ExpandPromotions, Attributes.ExpandReviews, Attributes.ExpandSkus, Attributes.ExpandTiers, Attributes.ExpandYouMayAlsoLike, Attributes.ExpandProductTags]
		[HttpGet]
		public HttpResponseMessage Get(int productId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetProduct(productId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ProductResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a product with a selected SKU.
		/// </summary>
		/// <param name="productId">The ID of the product.</param>
		/// <param name="skuId">The ID of the selected SKU.</param>
		/// <returns></returns>
		[Attributes.ExpandAddOns, Attributes.ExpandAttributes, Attributes.ExpandCategories, Attributes.ExpandCrossSells, Attributes.ExpandFrequentlyBoughtTogether, Attributes.ExpandHighlights, Attributes.ExpandImages, Attributes.ExpandManufacturer, Attributes.ExpandProductType, Attributes.ExpandPromotions, Attributes.ExpandReviews, Attributes.ExpandSkus, Attributes.ExpandTiers, Attributes.ExpandYouMayAlsoLike]
		[HttpGet]
		public HttpResponseMessage GetWithSku(int productId, int skuId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetProductWithSku(productId, skuId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ProductResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of products.
		/// </summary>
		/// <returns></returns>
		[Attributes.ExpandAddOns, Attributes.ExpandAttributes, Attributes.ExpandCategories, Attributes.ExpandCrossSells, Attributes.ExpandFrequentlyBoughtTogether, Attributes.ExpandHighlights, Attributes.ExpandImages, Attributes.ExpandManufacturer, Attributes.ExpandProductType, Attributes.ExpandPromotions, Attributes.ExpandReviews, Attributes.ExpandSkus, Attributes.ExpandTiers, Attributes.ExpandYouMayAlsoLike]
		[Attributes.FilterAccountId, Attributes.FilterExternalId, Attributes.FilterIsActive, Attributes.FilterIsFeatured, Attributes.FilterIsNew, Attributes.FilterManufacturerId, Attributes.FilterName, Attributes.FilterNumber, Attributes.FilterPortalId, Attributes.FilterProductTypeId, Attributes.FilterRetailPrice, Attributes.FilterSalePrice, Attributes.FilterShippingRuleTypeId, Attributes.FilterSupplierId, Attributes.FilterTaxClassId, Attributes.FilterWholesalePrice]
		[Attributes.SortDisplayOrder, Attributes.SortName, Attributes.SortNumber, Attributes.SortProductId, Attributes.SortRetailPrice, Attributes.SortSalePrice, Attributes.SortWholesalePrice]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetProducts(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of products for a specific catalog.
		/// </summary>
		/// <param name="catalogId">The ID of the catalog.</param>
		/// <returns></returns>
		[Attributes.ExpandAddOns, Attributes.ExpandAttributes, Attributes.ExpandCategories, Attributes.ExpandCrossSells, Attributes.ExpandFrequentlyBoughtTogether, Attributes.ExpandHighlights, Attributes.ExpandImages, Attributes.ExpandManufacturer, Attributes.ExpandProductType, Attributes.ExpandPromotions, Attributes.ExpandReviews, Attributes.ExpandSkus, Attributes.ExpandTiers, Attributes.ExpandYouMayAlsoLike]
		[Attributes.FilterAccountId, Attributes.FilterExternalId, Attributes.FilterIsActive, Attributes.FilterIsFeatured, Attributes.FilterIsNew, Attributes.FilterManufacturerId, Attributes.FilterName, Attributes.FilterNumber, Attributes.FilterPortalId, Attributes.FilterProductTypeId, Attributes.FilterRetailPrice, Attributes.FilterSalePrice, Attributes.FilterShippingRuleTypeId, Attributes.FilterSupplierId, Attributes.FilterTaxClassId, Attributes.FilterWholesalePrice]
		[Attributes.SortDisplayOrder, Attributes.SortName, Attributes.SortNumber, Attributes.SortProductId, Attributes.SortRetailPrice, Attributes.SortSalePrice, Attributes.SortWholesalePrice]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage ListByCatalog(int catalogId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetProductsByCatalog(catalogId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

        /// <summary>
        /// Gets a list of products for a specific catalog.
        /// </summary>
        /// <param name="catalogId">The ID of the catalog.</param>
        /// <returns></returns>
        [Attributes.ExpandAddOns, Attributes.ExpandAttributes, Attributes.ExpandCategories, Attributes.ExpandCrossSells, Attributes.ExpandFrequentlyBoughtTogether, Attributes.ExpandHighlights, Attributes.ExpandImages, Attributes.ExpandManufacturer, Attributes.ExpandProductType, Attributes.ExpandPromotions, Attributes.ExpandReviews, Attributes.ExpandSkus, Attributes.ExpandTiers, Attributes.ExpandYouMayAlsoLike]
        [Attributes.FilterAccountId, Attributes.FilterExternalId, Attributes.FilterIsActive, Attributes.FilterIsFeatured, Attributes.FilterIsNew, Attributes.FilterManufacturerId, Attributes.FilterName, Attributes.FilterNumber, Attributes.FilterPortalId, Attributes.FilterProductTypeId, Attributes.FilterRetailPrice, Attributes.FilterSalePrice, Attributes.FilterShippingRuleTypeId, Attributes.FilterSupplierId, Attributes.FilterTaxClassId, Attributes.FilterWholesalePrice]
        [Attributes.SortDisplayOrder, Attributes.SortName, Attributes.SortNumber, Attributes.SortProductId, Attributes.SortRetailPrice, Attributes.SortSalePrice, Attributes.SortWholesalePrice]
        [PageIndex, PageSize]
        [HttpGet]
        public HttpResponseMessage ListByCatalogIds(string catalogIds)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductsByCatalogIds(catalogIds, RouteUri, RouteTemplate);
                response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }
        
        /// <summary>
		/// Gets a list of products for a specific category.
		/// </summary>
		/// <param name="categoryId">The ID of the category.</param>
		/// <returns></returns>
		[Attributes.ExpandAddOns, Attributes.ExpandAttributes, Attributes.ExpandCategories, Attributes.ExpandCrossSells, Attributes.ExpandFrequentlyBoughtTogether, Attributes.ExpandHighlights, Attributes.ExpandImages, Attributes.ExpandManufacturer, Attributes.ExpandProductType, Attributes.ExpandPromotions, Attributes.ExpandReviews, Attributes.ExpandSkus, Attributes.ExpandTiers, Attributes.ExpandYouMayAlsoLike]
		[Attributes.FilterAccountId, Attributes.FilterExternalId, Attributes.FilterIsActive, Attributes.FilterIsFeatured, Attributes.FilterIsNew, Attributes.FilterManufacturerId, Attributes.FilterName, Attributes.FilterNumber, Attributes.FilterPortalId, Attributes.FilterProductTypeId, Attributes.FilterRetailPrice, Attributes.FilterSalePrice, Attributes.FilterShippingRuleTypeId, Attributes.FilterSupplierId, Attributes.FilterTaxClassId, Attributes.FilterWholesalePrice]
		[Attributes.SortDisplayOrder, Attributes.SortName, Attributes.SortNumber, Attributes.SortProductId, Attributes.SortRetailPrice, Attributes.SortSalePrice, Attributes.SortWholesalePrice]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage ListByCategory(int categoryId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetProductsByCategory(categoryId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of products for a specific category and promotion type.
		/// </summary>
		/// <param name="categoryId">The ID of the category.</param>
		/// <param name="promotionTypeId">The ID of the promotion type.</param>
		/// <returns></returns>
		[Attributes.ExpandAddOns, Attributes.ExpandAttributes, Attributes.ExpandCategories, Attributes.ExpandCrossSells, Attributes.ExpandFrequentlyBoughtTogether, Attributes.ExpandHighlights, Attributes.ExpandImages, Attributes.ExpandManufacturer, Attributes.ExpandProductType, Attributes.ExpandPromotions, Attributes.ExpandReviews, Attributes.ExpandSkus, Attributes.ExpandTiers, Attributes.ExpandYouMayAlsoLike]
		[Attributes.FilterAccountId, Attributes.FilterExternalId, Attributes.FilterIsActive, Attributes.FilterIsFeatured, Attributes.FilterIsNew, Attributes.FilterManufacturerId, Attributes.FilterName, Attributes.FilterNumber, Attributes.FilterPortalId, Attributes.FilterProductTypeId, Attributes.FilterRetailPrice, Attributes.FilterSalePrice, Attributes.FilterShippingRuleTypeId, Attributes.FilterSupplierId, Attributes.FilterTaxClassId, Attributes.FilterWholesalePrice]
		[Attributes.SortDisplayOrder, Attributes.SortName, Attributes.SortNumber, Attributes.SortProductId, Attributes.SortRetailPrice, Attributes.SortSalePrice, Attributes.SortWholesalePrice]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage ListByCategoryByPromotionType(int categoryId, int promotionTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetProductsByCategoryByPromotionType(categoryId, promotionTypeId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of products for the specified external IDs.
		/// </summary>
		/// <param name="externalIds">The comma-seperated external IDs of the products.</param>
		/// <returns></returns>
		[Attributes.ExpandAddOns, Attributes.ExpandAttributes, Attributes.ExpandCategories, Attributes.ExpandCrossSells, Attributes.ExpandFrequentlyBoughtTogether, Attributes.ExpandHighlights, Attributes.ExpandImages, Attributes.ExpandManufacturer, Attributes.ExpandProductType, Attributes.ExpandPromotions, Attributes.ExpandReviews, Attributes.ExpandSkus, Attributes.ExpandTiers, Attributes.ExpandYouMayAlsoLike]
		[Attributes.SortDisplayOrder, Attributes.SortName, Attributes.SortNumber, Attributes.SortProductId, Attributes.SortRetailPrice, Attributes.SortSalePrice, Attributes.SortWholesalePrice]
		[HttpGet]
		public HttpResponseMessage ListByExternalIds(string externalIds)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetProductsByExternalIds(externalIds, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of products designated as home page specials.
		/// </summary>
		/// <returns></returns>
		[Attributes.ExpandAddOns, Attributes.ExpandAttributes, Attributes.ExpandCategories, Attributes.ExpandCrossSells, Attributes.ExpandFrequentlyBoughtTogether, Attributes.ExpandHighlights, Attributes.ExpandImages, Attributes.ExpandManufacturer, Attributes.ExpandProductType, Attributes.ExpandPromotions, Attributes.ExpandReviews, Attributes.ExpandSkus, Attributes.ExpandTiers, Attributes.ExpandYouMayAlsoLike]
		[Attributes.FilterAccountId, Attributes.FilterExternalId, Attributes.FilterIsActive, Attributes.FilterIsFeatured, Attributes.FilterIsNew, Attributes.FilterManufacturerId, Attributes.FilterName, Attributes.FilterNumber, Attributes.FilterPortalId, Attributes.FilterProductTypeId, Attributes.FilterRetailPrice, Attributes.FilterSalePrice, Attributes.FilterShippingRuleTypeId, Attributes.FilterSupplierId, Attributes.FilterTaxClassId, Attributes.FilterWholesalePrice]
		[Attributes.SortDisplayOrder, Attributes.SortName, Attributes.SortNumber, Attributes.SortProductId, Attributes.SortRetailPrice, Attributes.SortSalePrice, Attributes.SortWholesalePrice]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage ListByHomeSpecials()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetProductsByHomeSpecials(RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of products for the specified product IDs.
		/// </summary>
		/// <param name="productIds">The comma-separated IDs of the products.</param>
		/// <returns></returns>
		[Attributes.ExpandAddOns, Attributes.ExpandAttributes, Attributes.ExpandCategories, Attributes.ExpandCrossSells, Attributes.ExpandFrequentlyBoughtTogether, Attributes.ExpandHighlights, Attributes.ExpandImages, Attributes.ExpandManufacturer, Attributes.ExpandProductType, Attributes.ExpandPromotions, Attributes.ExpandReviews, Attributes.ExpandSkus, Attributes.ExpandTiers, Attributes.ExpandYouMayAlsoLike]
		[Attributes.SortDisplayOrder, Attributes.SortName, Attributes.SortNumber, Attributes.SortProductId, Attributes.SortRetailPrice, Attributes.SortSalePrice, Attributes.SortWholesalePrice]
		[HttpGet]
		public HttpResponseMessage ListByProductIds(string productIds)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetProductsByProductIds(productIds, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of products for a specific promotion type.
		/// </summary>
		/// <param name="promotionTypeId">The ID of the promotion type.</param>
		/// <returns></returns>
		[Attributes.ExpandAddOns, Attributes.ExpandAttributes, Attributes.ExpandCategories, Attributes.ExpandCrossSells, Attributes.ExpandFrequentlyBoughtTogether, Attributes.ExpandHighlights, Attributes.ExpandImages, Attributes.ExpandManufacturer, Attributes.ExpandProductType, Attributes.ExpandPromotions, Attributes.ExpandReviews, Attributes.ExpandSkus, Attributes.ExpandTiers, Attributes.ExpandYouMayAlsoLike]
		[Attributes.FilterAccountId, Attributes.FilterExternalId, Attributes.FilterIsActive, Attributes.FilterIsFeatured, Attributes.FilterIsNew, Attributes.FilterManufacturerId, Attributes.FilterName, Attributes.FilterNumber, Attributes.FilterPortalId, Attributes.FilterProductTypeId, Attributes.FilterRetailPrice, Attributes.FilterSalePrice, Attributes.FilterShippingRuleTypeId, Attributes.FilterSupplierId, Attributes.FilterTaxClassId, Attributes.FilterWholesalePrice]
		[Attributes.SortDisplayOrder, Attributes.SortName, Attributes.SortNumber, Attributes.SortProductId, Attributes.SortRetailPrice, Attributes.SortSalePrice, Attributes.SortWholesalePrice]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage ListByPromotionType(int promotionTypeId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetProductsByPromotionType(promotionTypeId, RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<ProductListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new product.
		/// </summary>
		/// <param name="model">The model of the product.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] ProductModel model)
		{
			HttpResponseMessage response;

			try
			{
				var product = _service.CreateProduct(model);
				if (product != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + product.ProductId;

					response = CreateCreatedResponse(new ProductResponse { Product = product });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing product.
		/// </summary>
		/// <param name="productId">The ID of the product.</param>
		/// <param name="model">The model of the product.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int productId, [FromBody] ProductModel model)
		{
			HttpResponseMessage response;

			try
			{
				var product = _service.UpdateProduct(productId, model);
				response = product != null ? CreateOKResponse(new ProductResponse { Product = product }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing product.
		/// </summary>
		/// <param name="productId">The ID of the product.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int productId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteProduct(productId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new ProductResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

        #region Znode Version 8.0
        /// <summary>       
        /// To get product details by productId
        /// </summary>
        /// <param name="productId">int productId</param>
        /// <returns>returns ProductDetails info for manage/edit</returns>
        public HttpResponseMessage GetProductDetailsByProductId(int productId)
        {
            HttpResponseMessage response;
            try
            {
                ProductListModel productList = _cache.GetProductDetailsByProductId(productId);
                response = !Equals(productList, null) ? CreateOKResponse(new ProductListResponse { ProductList = productList }) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductListResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets the product tags based on product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public HttpResponseMessage GetProductTag(int productId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetProductTags(productId, RouteUri, RouteTemplate);
				response = (!Equals(data,null)) ? CreateOKResponse<ProductTagResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
                var data = new ProductTagResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}


        /// <summary>
        /// Creates a new product tags.
        /// </summary>
        /// <param name="model">The model of the product tags.</param>
        /// <returns></returns>
        [HttpPost]
        public HttpResponseMessage CreateProductTag([FromBody] ProductTagsModel model)
        {
            HttpResponseMessage response;

            try
            {
                var productTag = _service.CreateProductTag(model);
                if (!Equals(productTag, null))
                {
                    var uri = Request.RequestUri;
                    var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + productTag.ProductId;

                    response = CreateCreatedResponse(new ProductTagResponse { ProductTag = productTag });
                    response.Headers.Add("Location", location);
                }
                else
                {
                    response = CreateInternalServerErrorResponse();
                }
            }
            catch (Exception ex)
            {
                var data = new ProductTagResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Updates an existing product tags.
        /// </summary>
        /// <param name="productId">The ID of the product.</param>
        /// <param name="model">The model of the product tags.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage UpdateProductTag(int tagId, [FromBody] ProductTagsModel model)
        {
            HttpResponseMessage response;

            try
            {
                var productTags = _service.UpdateProductTag(tagId, model);
                response = (!Equals(productTags, null)) ? CreateOKResponse(new ProductTagResponse { ProductTag = productTags }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTagResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Deletes an existing product tags.
        /// </summary>
        /// <param name="productId">The ID of the product.</param>
        /// <returns></returns>
        [HttpDelete]
        public HttpResponseMessage DeleteProductTag(int tagId)
        {
            HttpResponseMessage response;

            try
            {
                var deleted = _service.DeleteProductTag(tagId);
                response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTagResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Gets the product Facets based on product id.
        /// </summary>
        /// <param name="productId"></param>
        /// <returns></returns>
        public HttpResponseMessage GetProductFacets(int productId)
        {
            HttpResponseMessage response;

            try
            {
                var data = _cache.GetProductFacets(productId, RouteUri, RouteTemplate);
                response = (!Equals(data, null)) ? CreateOKResponse<ProductFacetsResponse>(data) : CreateNotFoundResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductFacetsResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        /// <summary>
        /// Bind the Associated facets to the product
        /// </summary>
        /// <param name="productId">The ID of the product.</param>
        /// <param name="model">The model of the product tags.</param>
        /// <returns></returns>
        [HttpPut]
        public HttpResponseMessage BindProductFacets(int productId, [FromBody] ProductFacetModel model)
        {
            HttpResponseMessage response;

            try
            {
                var productFacets = _service.BindProductFacets(productId, model);
                response = (!Equals(productFacets, null)) ? CreateOKResponse(new ProductFacetsResponse { ProductFacets = productFacets }) : CreateInternalServerErrorResponse();
            }
            catch (Exception ex)
            {
                var data = new ProductTagResponse { HasError = true, ErrorMessage = ex.Message };
                response = CreateInternalServerErrorResponse(data);
            }

            return response;
        }

        #endregion

	}
}