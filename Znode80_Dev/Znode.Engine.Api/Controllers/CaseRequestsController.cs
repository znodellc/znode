﻿using System;
using System.Net.Http;
using System.Web.Http;
using Znode.Engine.Api.Cache;
using Znode.Engine.Api.Models;
using Znode.Engine.Api.Models.Responses;
using Znode.Engine.Services;
using Znode.Framework.Api.Attributes;
using Znode.Framework.Api.Controllers;

namespace Znode.Engine.Api.Controllers
{
	public class CaseRequestsController : BaseController
	{
		private readonly ICaseRequestService _service;
		private readonly ICaseRequestCache _cache;

		public CaseRequestsController()
		{
			_service = new CaseRequestService();
			_cache = new CaseRequestCache(_service);
		}

		/// <summary>
		/// Gets a case request.
		/// </summary>
		/// <param name="caseRequestId">The ID of the case request.</param>
		/// <returns></returns>
		[Attributes.ExpandCasePriority, Attributes.ExpandCaseStatus, Attributes.ExpandCaseType]
		[HttpGet]
		public HttpResponseMessage Get(int caseRequestId)
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetCaseRequest(caseRequestId,  RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<CaseRequestResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new CaseRequestResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Gets a list of case requests.
		/// </summary>
		/// <returns></returns>
		[Attributes.ExpandCasePriority, Attributes.ExpandCaseStatus, Attributes.ExpandCaseType]
		[Attributes.FilterAccountId, Attributes.FilterCasePriorityId, Attributes.FilterCaseStatusId, Attributes.FilterCaseTypeId, Attributes.FilterCreateDate, Attributes.FilterEmail, Attributes.FilterLastName, Attributes.FilterOwnerAccountId, Attributes.FilterPortalId]
		[Attributes.SortCasePriorityId, Attributes.SortCaseRequestId, Attributes.SortCaseStatusId, Attributes.SortCaseTypeId, Attributes.SortCreateDate, Attributes.SortLastName]
		[PageIndex, PageSize]
		[HttpGet]
		public HttpResponseMessage List()
		{
			HttpResponseMessage response;

			try
			{
				var data = _cache.GetCaseRequests( RouteUri, RouteTemplate);
				response = data != null ? CreateOKResponse<CaseRequestListResponse>(data) : CreateNotFoundResponse();
			}
			catch (Exception ex)
			{
				var data = new CaseRequestListResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Creates a new case request.
		/// </summary>
		/// <param name="model">The model of the case request.</param>
		/// <returns></returns>
		[HttpPost]
		public HttpResponseMessage Create([FromBody] CaseRequestModel model)
		{
			HttpResponseMessage response;

			try
			{
				var caseRequest = _service.CreateCaseRequest(model);
				if (caseRequest != null)
				{
					var uri = Request.RequestUri;
					var location = uri.Scheme + "://" + uri.Host + uri.AbsolutePath + "/" + caseRequest.CaseRequestId;

					response = CreateCreatedResponse(new CaseRequestResponse { CaseRequest = caseRequest });
					response.Headers.Add("Location", location);
				}
				else
				{
					response = CreateInternalServerErrorResponse();
				}
			}
			catch (Exception ex)
			{
				var data = new CaseRequestResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Updates an existing case request.
		/// </summary>
		/// <param name="caseRequestId">The ID of the case request.</param>
		/// <param name="model">The model of the case request.</param>
		/// <returns></returns>
		[HttpPut]
		public HttpResponseMessage Update(int caseRequestId, [FromBody] CaseRequestModel model)
		{
			HttpResponseMessage response;

			try
			{
				var caseRequest = _service.UpdateCaseRequest(caseRequestId, model);
				response = caseRequest != null ? CreateOKResponse(new CaseRequestResponse { CaseRequest = caseRequest }) : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new CaseRequestResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}

		/// <summary>
		/// Deletes an existing case request.
		/// </summary>
		/// <param name="caseRequestId">The ID of the case request.</param>
		/// <returns></returns>
		[HttpDelete]
		public HttpResponseMessage Delete(int caseRequestId)
		{
			HttpResponseMessage response;

			try
			{
				var deleted = _service.DeleteCaseRequest(caseRequestId);
				response = deleted ? CreateNoContentResponse() : CreateInternalServerErrorResponse();
			}
			catch (Exception ex)
			{
				var data = new CaseRequestResponse { HasError = true, ErrorMessage = ex.Message };
				response = CreateInternalServerErrorResponse(data);
			}

			return response;
		}
	}
}
