﻿using System.Collections.Generic;
using System.Linq;
using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Suppliers
{
	public static class ZnodeSupplierTypeExtensions
	{
		/// <summary>
		/// Extension method that converts an IZnodeSupplierType list to an IZnodeProviderType list.
		/// </summary>
		/// <param name="list">The list of IZnodeSupplierType items.</param>
		/// <returns>A list of IZnodeProviderType items.</returns>
		public static List<IZnodeProviderType> ToProviderTypeList(this List<IZnodeSupplierType> list)
		{
			return list.Cast<IZnodeProviderType>().ToList();
		}
	}
}