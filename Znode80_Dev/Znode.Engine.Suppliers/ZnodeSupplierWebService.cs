using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;

namespace Znode.Engine.Suppliers
{
	public class ZnodeSupplierWebService : ZnodeSupplierWebServiceType
	{
		public ZnodeSupplierWebService()
		{
			Name = "Web Service";
			Description = "Invokes a web service call of the supplier.";

			Controls.Add(ZnodeSupplierControl.DisplayOrder);
		}

		/// <summary>
		/// Invokes an external web service of the supplier.
		/// </summary>
		/// <param name="order">The order being processed.</param>
		/// <param name="shoppingCart">The current shopping cart.</param>
		/// <param name="supplier">The supplier that has the web service call.</param>
		/// <returns>True if the web service call was successful; otherwise, false.</returns>
		public override bool InvokeWebService(ZNodeOrderFulfillment order, ZNodeShoppingCart shoppingCart, ZnodeSupplier supplier)
		{
			// This is just a dummy placeholder. To create your own supplier web service functionality, derive from this class
			// and implement it as required by the supplier's web service. It could be REST, WCF, ASMX, XML-RPC, or anything
			// else that constitutes a web service.

			return true;
		}
	}
}
