﻿using System.Collections.ObjectModel;
using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Suppliers
{
	/// <summary>
	/// This is the root interface for all supplier types.
	/// </summary>
	public interface IZnodeSupplierType : IZnodeProviderType
	{
		Collection<ZnodeSupplierControl> Controls { get; }
	}
}
