﻿using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;

namespace Znode.Engine.Suppliers
{
	/// <summary>
	/// Provides the base implementation for all supplier web service types.
	/// </summary>
	public abstract class ZnodeSupplierWebServiceType : ZnodeSupplierType, IZnodeSupplierWebServiceType
	{
		public virtual bool InvokeWebService(ZNodeOrderFulfillment order, ZNodeShoppingCart shoppingCart, ZnodeSupplier supplier)
		{
			// Nothing to see here, override this method in a derived class
			return true;
		}
	}
}
