<%@ Page Language="C#" MasterPageFile="~/Activate/AdminThemes/Standard/edit.master" AutoEventWireup="true" Inherits="Znode.Engine.Demo.ContinueTrial" Title="Znode Trial" Codebehind="continuetrial.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" Runat="Server">
	<div class="License">
    <h1>Znode Trial - <%=DaysRemaining %></h1>
    <div><img src="~/Activate/AdminThemes/Images/clear.gif" runat="server" width="1" height="20" alt=""/></div>  
        
    <div class="Status" style=" margin-bottom: 30px;">
        You are seeing this message because your store is running in the trial mode. This message will not appear in the fully
        registered version.
    </div>
        
      
    <div class="ActionLink" style="margin-left:50px; margin-bottom:20px;"><b><img id="Img3" alt="" src="~/Activate/AdminThemes/Images/400-right.gif" runat="server" border="0" align="absmiddle"  />  <a id="A2" href="~/" runat="server">Continue with Trial</a></b></div> 
        
        
    <div class="ActionLink" style="margin-left:50px; margin-bottom:20px;"><b><img id="Img4" alt=""  src="~/Activate/AdminThemes/Images/400-right.gif" runat="server" border="0" align="absmiddle"  />  <a id="A4" href="~/" runat="server">Go to Store </a></b></div> 
        
      
    <div class="ActionLink" style="margin-left:50px; margin-bottom:100px;"><b><img id="Img5" alt="" src="~/Activate/AdminThemes/Images/400-right.gif" runat="server" border="0" align="absmiddle"  />  <a id="A1" href="~/activate/default.aspx" runat="server">Activate your License</a></b></div> 
        

    <div>Issues or questions? Email us at support@znode.com. <a id="A3" href="http://www.znode.com/buy" target="_blank">Purchase licenses from znode.com</a></div>
</div> 
</asp:Content>

