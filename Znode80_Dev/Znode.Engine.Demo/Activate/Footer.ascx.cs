using System;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Footer user control class
    /// </summary>
    public partial class Activate_Footer : System.Web.UI.UserControl
    {
        #region Member Variables
        private string _FooterCopyrightText = string.Empty;
        #endregion

        /// <summary>
        /// Gets or sets the footer copyright text.
        /// </summary>
        public string FooterCopyrightText
        {
            get { return _FooterCopyrightText; }
            set { _FooterCopyrightText = value; }
        }

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["FooterCopyrightText"] != null)
            {
                this.FooterCopyrightText = System.Configuration.ConfigurationManager.AppSettings["FooterCopyrightText"].ToString();
            }
            else
            {
                this.FooterCopyrightText = "Powered by Znode. &copy;Copyright 2013, <a href=\"http://www.znode.com\" target=\"_blank\">Znode Inc</a>, All Rights Reserved.<br />Multifront v7.0";
            }
        }
        #endregion
    }
}