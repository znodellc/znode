<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Demo.Activate_Footer" CodeBehind="Footer.ascx.cs" %>

<div class="FooterPane">
    <div class="BackgroundLeft"></div>
    <div class="BackgroundMiddle">
        <div class="ZnodeLogo"><a href="http://www.znode.com" target="_blank">
            <img style="border: 0px; vertical-align: middle;" src="~/images/logo/znode_logo_small.gif" alt="Znode" runat="server" /></a></div>
        <div class="CopyrightText"><%= FooterCopyrightText %></div>
    </div>
    <div class="BackgroundRight"></div>
</div>
