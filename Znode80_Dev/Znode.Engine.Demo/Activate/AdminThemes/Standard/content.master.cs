using System;
using System.Web;
using System.Web.UI.WebControls;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Common;

namespace  Znode.Engine.Demo
{
    public partial class Activate_AdminThemes_Standard_content : ZNodeAdminTemplate
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ViewStateUserKey = Session.SessionID;

            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {
                    string inURL = Request.Url.ToString();
                    string outURL = inURL.ToLower().Replace("http://", "https://");

                    Response.Redirect(outURL);
				}
			}					
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected new void Page_Load(object sender, EventArgs e)
        {
            if (Page.Header != null)
            {
                // Add the page title to the header element
                this.Page.Header.Title = "Store Management";
            }
            
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
        }

        /// <summary>
        /// Raised after the user clicks the logout link and the logout process is complete.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void LoginStatus1_LoggingOut(object sender, LoginCancelEventArgs e)
        {
            //Clearing a per-user cache of objects here.
            Session.Remove(ZNode.Libraries.Framework.Business.ZNodeSessionKeyType.UserAccount.ToString());
            Session["ProfileCache"] = null; // Reset profile cache
        }

        /// <summary>
        /// Raised after the user clicks the logout link and the logout process is complete.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void uxLoginStatus_LoggedOut(object sender, EventArgs e)
        {
            Session.Remove(ZNode.Libraries.Framework.Business.ZNodeSessionKeyType.UserAccount.ToString());
            Session["ProfileCache"] = null; // Reset profile cache

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.UserLogout, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "User Logout", "User Logout");
            Response.Redirect("~/Default.aspx");
        }


        /// <summary>
        /// Raised after the user clicks the logout link and the logout process is complete.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AdminUserLoginStatus_LoggedOut(object sender, EventArgs e)
        {
            Response.Redirect("~/default.aspx");
        }        

        protected string SelectedStyle(object noteItem)
        {
            if (noteItem is SiteMapNode)
            {
                var item = noteItem as SiteMapNode;

                if (item.Url.ToLower() == Request.Url.AbsolutePath.ToLower())
                {                    
                    return "selected";
                }

                foreach (var subItem in item.ChildNodes)
                {
                    if ((subItem as SiteMapNode).Url.ToLower() == Request.Url.AbsolutePath.ToLower())
                    {                        
                        return "selected";
                    }
                }
            }

            return string.Empty;
        }
    }
}