<%@ Page Language="C#" MasterPageFile="~/Activate/AdminThemes/Standard/activate.master"
    AutoEventWireup="true" Inherits="Znode.Engine.Demo.Admin_Activate" Title="Activate your Znode License"
    CodeBehind="default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <asp:ScriptManager ID="scriptmanager1" runat="server">
    </asp:ScriptManager>
    <div class="License">
        <!-- License Activation Intro panel -->
        <asp:Panel ID="pnlIntro" runat="server" Visible="true">
            <h1>
                Activate your License</h1>
            <div>
                <img src="~/Activate/AdminThemes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" /></div>
            You are about to activate your license.
            <div style="margin-bottom: 20px; margin-top: 20px;">
                If you are activating a Free Trial then proceed with the activation. You will have
                the option of creating an activation without using a license key.
            </div>
            <div style="margin-bottom: 20px;">
                If you are using a purchased license, then you should be aware that this activation
                can only be used on <strong>one</strong> CPU.
                <br />
                    
            </div>
            <div>
                <img src="~/Activate/AdminThemes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" />&nbsp;</div>
            <div>
                <asp:CheckBox ID="chkIntro" runat="server" Text='' OnCheckedChanged="chkIntro_CheckedChanged" /></div>
            <div>
                <img src="~/Activate/AdminThemes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" /></div>
            <div>
                <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error"></asp:Label></div>
            <div>
                <img src="~/Activate/AdminThemes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" /></div>
            <div>
                <asp:Button ID="btnProceedToActivation" runat="server" Text="Proceed With Activation"
                    CausesValidation="true" OnClick="btnProceedToActivation_Click" /></div>
            <div>
                <img src="~/Activate/AdminThemes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" /></div>
        </asp:Panel>
        <!-- ACTIVATE LICENSE -->
        <asp:Panel ID="pnlLicenseActivate" runat="server" Visible="false">
            <h1>
                Activate your License</h1>
            <div>
                <img src="~/Activate/AdminThemes/Images/clear.gif" width="1" runat="server" height="10"
                    alt="" /></div>
            <b>Please Note:</b>
            <ul>
                <li>You are activating your license to this Server. Be sure this is what you want to
                    do!</li>
                <li>Once your key is registered you will not be able to use it for another Server.</li>
                <li>Choosing the 30 Day Free Trial will not count to your license activation.</li>
                <li>You must have Read+Write+Modify permissions for the Network Service account on
                    the <b><asp:Label ID="lblLicensePath" Text="Data" runat="server"></asp:Label></b> folder before activating.</li>
            </ul>
            <div>
                <img src="~/Activate/AdminThemes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" /></div>
            <div>
                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></div>
            <div>
                <img src="~/Activate/AdminThemes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" /></div>
            <div class="ClearBoth">
            </div>
            <div class="FormView">
                <div class="FieldStyle">
                    Select License
                </div>
                <div class="ValueStyle">
                    <asp:RadioButton ID="chkMarketPlace" GroupName="lic" Checked="false" Text="Marketplace"
                        runat="server" OnCheckedChanged="chkFreeTrial_CheckedChanged" AutoPostBack="true"
                        Visible="false" /><br />
                    <asp:RadioButton ID="chkSerLicense" GroupName="lic" Checked="false" Text="MultiFront"
                        runat="server" OnCheckedChanged="chkFreeTrial_CheckedChanged" AutoPostBack="true"
                        Visible="false" /><br />
                         <asp:RadioButton ID="chkServices" GroupName="lic" Checked="false" Text="Store Multiplier"
                        runat="server" OnCheckedChanged="chkFreeTrial_CheckedChanged" AutoPostBack="true"
                        Visible="false" /><br />
                    <asp:RadioButton ID="chkSingleStoreLicense" GroupName="lic" Checked="false" Text="MultiFront Single Store Edition<br />"
                        runat="server" OnCheckedChanged="chkFreeTrial_CheckedChanged" AutoPostBack="true"
                        Visible="false" />
                    <asp:RadioButton ID="chkFreeTrial" GroupName="lic" Checked="false" Text="30 Day Free Trial"
                        runat="server" OnCheckedChanged="chkFreeTrial_CheckedChanged" AutoPostBack="true" />
                </div>
                <asp:Panel ID="pnlSerial" runat="server" Visible="false">
                    <div class="FieldStyle">
                        Serial Number
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtSerialNumber" runat="server" Width="300" CausesValidation="true"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="SerialReqd" runat="server" ControlToValidate="txtSerialNumber"
                            ErrorMessage="Serial number is required." ToolTip="Serial number is required."
                            Display="Dynamic" CssClass="Error">Serial number is required.</asp:RequiredFieldValidator>
                    </div>
                </asp:Panel>
                <div class="FieldStyle">
                    Full Name
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtName" runat="server" Width="200" CausesValidation="true"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                        ErrorMessage="Full name is required." Display="Dynamic" CssClass="Error">Full name is required.</asp:RequiredFieldValidator>
                </div>
                <div class="FieldStyle">
                    Email Address
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtEmail" runat="server" Width="200" CausesValidation="true"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="Email is required." Display="Dynamic" CssClass="Error">Email is required.</asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEmail"
                        CssClass="Error" ErrorMessage="Enter Valid Email Address" ToolTip="Enter Valid Email Address."
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"></asp:RegularExpressionValidator>
                </div>                
                <div class="ValueStyle">
                    <textarea runat="server" id="txtEULA" style="width: 760px; height: 300px;"></textarea>
                </div>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="FieldStyle">
                <asp:CheckBox ID="chkEULA" runat="server" Text="I have read and agreed to the software license agreement (EULA) above" />
            </div>
            <br />
            <br />
            <div class="FieldStyle">
                <asp:Button ID="btnActivateLicense" runat="server" Text="Click to Activate License"
                    CausesValidation="true" OnClick="btnActivateLicense_Click" />
            </div>
            <div>
                <img src="~/Activate/AdminThemes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" />
            </div>
        </asp:Panel>
        <!-- CONFIRM -->
        <asp:Panel ID="pnlConfirm" runat="server" Visible="false">
            <h1>
                Confirmation</h1>
            <p>
                <asp:Label ID="lblConfirm" runat="server"></asp:Label></p>
            <div>
                <img id="Img1" src="~/Activate/AdminThemes/Images/clear.gif" runat="server" width="1" height="20"
                    alt="" /></div>
            <a id="A1" href="~/" runat="server">Go to Store &raquo;</a>
            <div>
                <img id="Img2" src="~/Activate/AdminThemes/Images/clear.gif" runat="server" width="1" height="20"
                    alt="" /></div>
        </asp:Panel>
        <asp:Panel ID="pnlSingleStore" runat="server" Style="display: none;" CssClass="LicenseConfirmationPopupStyle">
            <div>
                <h4 class="SubTitle">
                    Domain Confirmation</h4>
            </div>
            <div class="ConfirmationText">
                <asp:Label ID="lblSingleFrontActivationMsg" runat="server"></asp:Label>
            </div>
            <div class="ImageButtons">
                <asp:Button ID="btnActivateSingleFront" CssClass="Size75" runat="server" Text="Confirm"
                    OnClick="btnActivateSingleFront_click" />&nbsp;&nbsp;
                <asp:ImageButton ID="btnCancelConfirmation" ImageUrl="~/Activate/AdminThemes/Images/button_cancel_highlight.gif"
                    runat="server" />
            </div>
        </asp:Panel>
    </div>
    <div class="ClearBoth">
    </div>
    <div>
        <asp:Button ID="btnhidden" runat="server" Style="display: none;" />
        <ajaxToolKit:ModalPopupExtender ID="mdlPopup" CancelControlID="btnCancelConfirmation"
            runat="server" TargetControlID="btnhidden" PopupControlID="pnlSingleStore" BackgroundCssClass="modalBackground" />
    </div>
</asp:Content>
