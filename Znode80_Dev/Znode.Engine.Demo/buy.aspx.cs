﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Buy Page class.
    /// </summary>
    public partial class Buy : CommonPageBase
    {
        #region Protected Member Variables
        private string sku = string.Empty;
        private string _productNum = string.Empty;
        private int productID = 0;
        private int _quantity = 1;
        private ZNodeProduct _product = null;
        #endregion

        #region General Events
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected override void Page_Load(object sender, EventArgs e)
        {
            // Get product sku from querystring 
            if (Request.Params["sku"] != null)
            {
                this.sku = Request.Params["sku"];
            }

            // Get product num # from querystring 
            if (Request.QueryString["product_num"] != null)
            {
                this._productNum = Request.QueryString["product_num"];
            }

            // Get quantity from querystring 
            if (Request.Params["quantity"] != null)
            {
                if (!int.TryParse(Request.Params["quantity"], out this._quantity))
                {
                    this._quantity = 1;
                }
            }

            if (this.sku.Length == 0 && this._productNum.Length == 0)
            {
                Response.Redirect("~/error.aspx");
                return;
            }

            if (!Page.IsPostBack)
            {
                ZNodeSKU productSKU = new ZNodeSKU();

                if (this.sku.Length > 0)
                {
                    productSKU = ZNodeSKU.CreateBySKU(this.sku);

                    if (productSKU != null && productSKU.SKUID > 0)
                    {
                        this._product = ZNodeProduct.Create(productSKU.ProductID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                    }
                }
                else if (this._productNum.Length > 0)
                {
                    ProductService productService = new ProductService();
                    ProductQuery filters = new ProductQuery();
                    filters.AppendEquals(ProductColumn.ProductNum, this._productNum);
                    TList<Product> productList = productService.Find(filters.GetParameters());

                    if (productList != null)
                    {
                        if (productList.Count == 0)
                        {
                            // If SKUID or Invalid SKU is Zero then Redirected to default page                        
                            Response.Redirect("~/error.aspx");
                        }
                    }
                    else
                    {
                        // If SKUID or Invalid SKU is Zero then Redirected to default page                    
                        Response.Redirect("~/error.aspx");
                    }

                    this.productID = productList[0].ProductID;

                    this._product = ZNodeProduct.Create(this.productID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                }
                else
                {
                    // If SKUID or Invalid SKU is Zero then Redirected to default page
                    Response.Redirect("~/error.aspx");
                }

                // Check product attributes count
                if (this._product.ZNodeAttributeTypeCollection.Count > 0 && productSKU.SKUID == 0)
                {
                    Response.Redirect(this._product.ViewProductLink);
                }

                // Loop through the product addons
                foreach (ZNodeAddOn _addOn in this._product.ZNodeAddOnCollection)
                {
                    if (!_addOn.OptionalInd)
                    {
                        Response.Redirect(this._product.ViewProductLink);
                    }
                }

                if (this._product == null)
                {
                    // If SKUID or Invalid SKU is Zero then Redirected to default page
                    Response.Redirect("~/error.aspx");
                }

                // Create shopping cart item
                ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                item.Product = new ZNodeProductBase(this._product);
                item.Quantity = this._quantity;

                // Add product to cart
                ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                // If shopping cart is null, create in session
                if (shoppingCart == null)
                {
                    shoppingCart = new ZNodeShoppingCart();
                    shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                }

                // Add item to cart
                if (shoppingCart.AddToCart(item))
                {
                    string link = "~/shoppingcart.aspx";
                    Response.Redirect(link);
                }
                else
                {
                    // If Product is out of Stock - Redirected to Product Details  page                
                    Response.Redirect("~/error.aspx");
                }
            }
        }
        #endregion
    }
}
