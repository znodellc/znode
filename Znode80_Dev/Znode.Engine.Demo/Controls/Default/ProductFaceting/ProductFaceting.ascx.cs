using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.Tagging;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using Category = ZNode.Libraries.DataAccess.Entities.Category;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Product Faceting user control class.
    /// </summary>
	public partial class Controls_Default_ProductFaceting_ProductFaceting : System.Web.UI.UserControl
    {
        #region Member Variables

        private ZNodeSEOUrl _SeoUrl = new ZNodeSEOUrl();
        private int _CategoryId = 0;
        private int ControlTypeID = 0;
        private string _SelectedFacetValues = string.Empty;

        // Global Faceting Class declartion
        private ZNodeFacets znodeFacet = new ZNodeFacets();
        private ZNodeCategory category = new ZNodeCategory();

        // Global dataset declaration
        private DataSet facetedDataSet = new DataSet();

        #endregion

        // Public Event Handler
        public event System.EventHandler SelectedIndexChanged;

        #region Properties
        /// <summary>
        /// Gets or sets the Selected facet values
        /// </summary>
        public string SelectedFacetValues
        {
            get
            {
                return this._SelectedFacetValues;
            }

            set
            {
                this._SelectedFacetValues = value;
            }
        }

        /// <summary>
        /// Gets or sets the Category Id
        /// </summary>
        public int CategoryId
        {
            get
            {
                return this._CategoryId;
            }

            set
            {
                this._CategoryId = value;
            }
        }

        /// <summary>
		///  Gets a Faceted DataSet
        /// </summary>
        public DataSet FacetedDataSet
        {
            get
            {
                return this.facetedDataSet;
            }
        }

        #endregion

        #region Bind Facets
        /// <summary>
        /// Bind the Product Facets in the leftside page.
        /// </summary>
        public void BindFacets()
        {
            // Retrieve the facets details based on specific category
            this.facetedDataSet = this.znodeFacet.RetrieveFacetsByCategoryID(this._CategoryId, this.SelectedFacetValues, ZNodeCatalogManager.CatalogConfig.CatalogID);
            
            // Add the hierarchical relationship to the dataset for FacetGroup tables.        
            this.facetedDataSet.Relations.Add("FacetGroupRelation", this.facetedDataSet.Tables[0].Columns["FacetGroupId"], this.facetedDataSet.Tables[1].Columns["FacetGroupId"]);

            // Start to add the dynamic controls here
            ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='ProductTagging'>"));

            pnlProductFacetingTitle.Visible = false;
           
            if (this.facetedDataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow FacetGroup in this.facetedDataSet.Tables[0].Rows)
                {
                    // FACET GROUP                
                    if (FacetGroup["ControlTypeID"].ToString() != string.Empty)
                    {
                        this.ControlTypeID = int.Parse(FacetGroup["ControlTypeID"].ToString());
                    }

                    DataRow[] childrows = FacetGroup.GetChildRows("FacetGroupRelation");

                    if (childrows != null && childrows.Length > 0)
                    {
                        pnlProductFacetingTitle.Visible = true;

                        switch ((ZNodeFacetControlType)this.ControlTypeID)
                        {
                            case ZNodeFacetControlType.LABEL:
                                // Label method here
                                this.BindLabelControl(FacetGroup);
                                break;
                            case ZNodeFacetControlType.RADIOBUTTON:
                                // RadioButton method here
                                this.BindRadioButton(FacetGroup);
                                break;
                            case ZNodeFacetControlType.ICONS:
                                this.BindICons(FacetGroup);
                                break;
                            default:
                                // Dropdown method here
                                this.BindDropdown(FacetGroup);
                                break;
                        }
                    }
                }
            }

            ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));

            if (!string.IsNullOrEmpty(this.SelectedFacetValues))
            {
                this.BindSelectedFacets(this.SelectedFacetValues);
            }
        }

        /// <summary>
        /// Get the list of the Products
        /// </summary>
        public void GetProductList()
        {
            // Serialize the object
            if (this.SelectedFacetValues.Length > 0)
            {
                Session.Add("ProductList", this.znodeFacet.RetrieveProductsByFacetIds(this.SelectedFacetValues, this._CategoryId));
            }
        }

        #endregion
        #region Page_Load

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get categoryId from querystring  
            if (Request.Params["zcid"] != null)
            {
                this._CategoryId = int.Parse(Request.Params["zcid"]);
            }

            if (!Page.IsPostBack)
            {
                divSelectedTitle.Visible = false;
            }

            if (this._CategoryId != 0)
            {
                if (this.facetedDataSet != null && Page.IsPostBack)
                {
                    if (Session["FacetedValues"] != null)
                    {
                        this.SelectedFacetValues = Session["FacetedValues"].ToString();
                    }
                }
              
                ControlsPlaceHolder.Controls.Clear();
                this.BindFacets();
            }
        }
        #endregion

        #region Events

        /// <summary>
        /// Event Handler for the dynamic control DropDownList
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Controls_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Get Selected Facet Values
            this.SelectedFacetValues = this.GetSelectedFacets();

            if (this.SelectedIndexChanged != null)
            {
                this.SelectedIndexChanged(sender, e);
            }
        }

        /// <summary>
        /// Event Handler for the dynamic control LinkButton
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void OnClick_linkbuttonChanged(object sender, EventArgs e)
        {
            LinkButton lnkBtnFacetControl = sender as LinkButton;
            StringBuilder facetLinkValues = new StringBuilder();

            if (Session["FacetedValues"] != null)
            {
                this.SelectedFacetValues = Session["FacetedValues"].ToString() + "," + lnkBtnFacetControl.CommandArgument.ToString();
            }
            else
            {
                this.SelectedFacetValues = lnkBtnFacetControl.CommandArgument.ToString();
            }

            Session["FacetedValues"] = this.SelectedFacetValues;

            string qstr = this.ConstructQueryStringFromSessionValues(Session["FacetedValues"].ToString());

            if (!string.IsNullOrEmpty(qstr))
            {
                Response.Redirect(qstr, true);
            }

            ControlsPlaceHolder.Controls.Clear();

            this.BindFacets();

            if (this.SelectedIndexChanged != null)
            {
                this.SelectedIndexChanged(sender, e);
            }
        }

        /// <summary>
        /// Returns current selected addons from the list
        /// </summary>
        /// <returns>Returns the selected addons</returns>
        private string GetSelectedFacets()
        {
            StringBuilder facetValues = new StringBuilder();

            // Dynamic Controls 
            DropDownList ddlistControl = new DropDownList();
            TreeView treeviewFacetControl = new TreeView();
            RadioButtonList rbtnlistFacetControl = new RadioButtonList();
            CheckBoxList chkBoxListFacetControl = new CheckBoxList();

            if (this.facetedDataSet.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow facetGroup in this.facetedDataSet.Tables[0].Rows)
                {
                    if (facetGroup["ControlTypeID"].ToString() != string.Empty)
                    {
                        this.ControlTypeID = int.Parse(facetGroup["ControlTypeID"].ToString());
                    }

                    switch ((ZNodeFacetControlType)this.ControlTypeID)
                    {
                        case ZNodeFacetControlType.RADIOBUTTON: // RadioButtonList Control 

                            rbtnlistFacetControl = (RadioButtonList)ControlsPlaceHolder.FindControl("ctrlRadionButtonFacet" + facetGroup["FacetGroupId"].ToString());

                            if (facetValues.Length > 0)
                            {
                                facetValues.Append(",");
                            }

                            if (rbtnlistFacetControl != null)
                            {
                                if (rbtnlistFacetControl.SelectedIndex > -1)
                                {
                                    int selvalue = int.Parse(rbtnlistFacetControl.SelectedValue);

                                    if (selvalue > 0)
                                    {
                                        if (facetValues.Length > 0)
                                        {
                                            facetValues.Append(",");
                                        }

                                        rbtnlistFacetControl.SelectedItem.Selected = true;

                                        facetValues.Append(selvalue.ToString());
                                    }
                                }
                            }

                            break;

                        case ZNodeFacetControlType.ICONS: // CheckBoxList Control
                        default: // Dropdown Method here

							ddlistControl = (DropDownList)ControlsPlaceHolder.FindControl("ctrldropdownFacet" + facetGroup["FacetGroupId"].ToString());

                            if (ddlistControl != null)
                            {
                                if (ddlistControl.SelectedIndex != 0)
                                {
                                    int selvalue = int.Parse(ddlistControl.SelectedValue);

                                    if (selvalue > 0)
                                    {
                                        if (facetValues.Length > 0)
                                        {
                                            facetValues.Append(",");
                                        }

                                        facetValues.Append(selvalue.ToString());
                                    }
                                }
                            }

                            break;
                    }  // End Switch      
                }
            }

            this.SelectedFacetValues = facetValues.ToString();

			if (Session["FacetedValues"] != null)
            {
                this.SelectedFacetValues = Session["FacetedValues"].ToString() + "," + facetValues;
            }

            Session["FacetedValues"] = this.SelectedFacetValues;

            string qstr = this.ConstructQueryStringFromSessionValues(Session["FacetedValues"].ToString());

            if (!string.IsNullOrEmpty(qstr))
            {
                Response.Redirect(qstr, true);
            }

            ControlsPlaceHolder.Controls.Clear();

            // return the seleceted Facet Values.
            return this._SelectedFacetValues;
        }

        #endregion

        #region Bind Methods

        private void ClearControls()
        {
            ControlsPlaceHolder.Controls.Clear();
        }

        #endregion

        #region Dynamic Control Bind Methods

        /// <summary>
        /// Represents the BindIcons Method
        /// </summary>
        /// <param name="FacetGroupRow">The DataRow of Facet Group</param>
        private void BindICons(DataRow facetGroupRow)
        {
            // Start to add the dynamic controls here
            ControlsPlaceHolder.Controls.Add(new LiteralControl("<div>"));

            // String Builder
            StringBuilder labelFacetValues = new StringBuilder();

            // Add controls to the place holder
            Literal litrl = new Literal();
            litrl.Text = "<span class='IconsTitle'>" + facetGroupRow["FacetGroupLabel"].ToString() + "</span>";
            ControlsPlaceHolder.Controls.Add(litrl);
            ZNodeImage znodeImage = new ZNodeImage();
            foreach (DataRow facet in facetGroupRow.GetChildRows("FacetGroupRelation"))
            {
                int FacetProductCount = int.Parse(facet["ProductCount"].ToString());

                if (FacetProductCount > 0)
                {
                    LinkButton linkBtnFacetName = new LinkButton();
					linkBtnFacetName.ID = "ctrllnkBtnFacet" + facet["FacetId"].ToString();
                    linkBtnFacetName.Click += new EventHandler(this.OnClick_linkbuttonChanged);

                    if (facet["IconPath"] != System.DBNull.Value && facet["IconPath"].ToString() != string.Empty)
                    {
                        linkBtnFacetName.Text = "<img style=\"border:none; vertical-align: middle;\" src=\"" + znodeImage.GetImageHttpPathSmallThumbnail(facet["IconPath"].ToString()).Replace("~/", string.Empty) + "\" />" + "(" + facet["FacetProductCount"].ToString() + ")";
                    }
                    else
                    {
                        linkBtnFacetName.Text = facet["FacetName"].ToString() + " (" + facet["FacetProductCount"].ToString() + ")";
                    }

                    linkBtnFacetName.CommandArgument = facet["FacetId"].ToString();
                    linkBtnFacetName.CssClass = "linkBtnTagText";

                    ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='ValueStyle'>"));
                    ControlsPlaceHolder.Controls.Add(linkBtnFacetName);
                    ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
                }
            }

            ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
        }

        /// <summary>
        /// Bind the Dropdown control for facets
        /// </summary>
        /// <param name="facetGroupRow">Facet Group Data Row</param>
        private void BindDropdown(DataRow facetGroupRow)
        {
            // Dropdownlist
            DropDownList ddlFacetlist = new DropDownList();
            ddlFacetlist.Items.Add(new ListItem(facetGroupRow["FacetGroupLabel"].ToString(), "0"));
            ddlFacetlist.AppendDataBoundItems = true;
            ddlFacetlist.ID = "ctrldropdownFacet" + facetGroupRow["FacetGroupId"].ToString();
            ddlFacetlist.AutoPostBack = true;
            ddlFacetlist.CssClass = "dropdownText";
            ddlFacetlist.SelectedIndexChanged += new EventHandler(this.Controls_SelectedIndexChanged);
            ddlFacetlist.SelectedIndex = 0;

            foreach (DataRow facet in facetGroupRow.GetChildRows("FacetGroupRelation"))
            {
                int FacetProductCount = int.Parse(facet["FacetProductCount"].ToString());

                if (FacetProductCount > 0)
                {
                    // FACET                             
                    ListItem li1 = new ListItem(facet["FacetName"].ToString() + " (" + facet["FacetProductCount"].ToString() + ")", facet["FacetId"].ToString());
                    ddlFacetlist.Items.Add(li1);
                }
            }

            ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='ValueStyle'>"));
            ControlsPlaceHolder.Controls.Add(ddlFacetlist);
            ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
        }

        /// <summary>
        /// Bind the Label control
        /// </summary>
        /// <param name="facetGroupRow">Facet Group Data Row</param>
        private void BindLabelControl(DataRow facetGroupRow)
        {
            // Start to add the dynamic controls here
            ControlsPlaceHolder.Controls.Add(new LiteralControl("<div>"));

            // String Builder
            StringBuilder labelFacetValues = new StringBuilder();

            // Add controls to the place holder
            Literal litrl = new Literal();
            litrl.Text = "<span class='LinksTitle'>" + facetGroupRow["FacetGroupLabel"].ToString() + "</span>";
            ControlsPlaceHolder.Controls.Add(litrl);

            foreach (DataRow facet in facetGroupRow.GetChildRows("FacetGroupRelation"))
            {
                int FacetProductCount = int.Parse(facet["FacetProductCount"].ToString());

                if (FacetProductCount > 0)
                {
                    LinkButton linkBtnFacetName = new LinkButton();
                    linkBtnFacetName.ID = "ctrllnkBtnFacet" + facet["FacetId"].ToString();
                    linkBtnFacetName.Click += new EventHandler(this.OnClick_linkbuttonChanged);

                    linkBtnFacetName.Text = facet["FacetName"].ToString() + " (" + facet["FacetProductCount"].ToString() + ")";
                    linkBtnFacetName.CommandArgument = facet["FacetId"].ToString();
                    linkBtnFacetName.CssClass = "linkBtnTagText";

                    ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='ValueStyle'>"));
                    ControlsPlaceHolder.Controls.Add(linkBtnFacetName);
                    ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
                }
            }

            ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
        }

        /// <summary>
        /// Bind the RadioButtonList control
        /// </summary>
        /// <param name="facetGroupRow">Facet Group Data Row</param>
        private void BindRadioButton(DataRow facetGroupRow)
        {
            // Start to add the dynamic controls here
            ControlsPlaceHolder.Controls.Add(new LiteralControl("<div>"));

            // RadioButtonList
            RadioButtonList rbtnlistControl = new RadioButtonList();
			rbtnlistControl.ID = "ctrlRadionButtonFacet" + facetGroupRow["FacetGroupId"].ToString();
            rbtnlistControl.AutoPostBack = true;
            rbtnlistControl.SelectedIndexChanged += new EventHandler(this.Controls_SelectedIndexChanged);
            rbtnlistControl.CssClass = "RadioButton";

            // Set Vertical view
            rbtnlistControl.RepeatDirection = RepeatDirection.Vertical;

            // Add controls to the place holder
            Literal litrl = new Literal();
            litrl.Text = "<span class='RadioButtonListTitle'>" + facetGroupRow["FacetGroupLabel"].ToString() + "</span>";
            ControlsPlaceHolder.Controls.Add(litrl);

            foreach (DataRow facet in facetGroupRow.GetChildRows("FacetGroupRelation"))
            {
                int FacetProductCount = int.Parse(facet["FacetProductCount"].ToString());

                if (FacetProductCount > 0)
                {
                    // FACET                              
                    ListItem li1 = new ListItem(facet["FacetName"].ToString() + " (" + facet["FacetProductCount"].ToString() + ")", facet["FacetId"].ToString());
                    rbtnlistControl.Items.Add(li1);
                }
            }

            ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='ValueStyle'>"));
            ControlsPlaceHolder.Controls.Add(rbtnlistControl); // radioButton list control
            ControlsPlaceHolder.Controls.Add(new LiteralControl("</div></div>"));
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Construct QueryString key name.
        /// </summary>
        /// <param name="squeryname">The Query name</param>
        /// <returns>Returns the Query name</returns>
        private string QKeyName(string squeryname)
        {
            return squeryname.Replace(" ", "_") + "=";
        }

        /// <summary>
        /// Construct QueryString value.
        /// </summary>
        /// <param name="squeryvalue">Query value</param>
        /// <returns>Returns the Query value</returns>
        private string QKeyValue(string squeryvalue)
        {
            return Server.UrlEncode(squeryvalue).Replace("%e2%80%99", "'");
        }

        /// <summary>
        /// Construct the query string from session values
        /// </summary>
        /// <param name="facetValues">Facet Values</param>
        /// <returns>Returns the querystring values</returns>
        private string ConstructQueryStringFromSessionValues(string facetValues)
        {
            string qstring = string.Empty;

			var facetService = new ZNode.Libraries.DataAccess.Service.FacetService();
			var facetGroupService = new ZNode.Libraries.DataAccess.Service.FacetGroupService();

            string[] keyValues = facetValues.ToString().Split(',');

            foreach (string sessionkey in keyValues)
            {
                if (!string.IsNullOrEmpty(sessionkey))
                {
					ZNode.Libraries.DataAccess.Entities.Facet facet = facetService.GetByFacetID(Convert.ToInt32(sessionkey));

                    if (facet != null)
                    {
						qstring = qstring + (qstring.Trim().Length == 0 ? string.Empty : "&") + this.QKeyName(facetGroupService.GetByFacetGroupID(facet.FacetGroupID.Value).FacetGroupLabel) + this.QKeyValue(facet.FacetName);
                    }
                }
            }

            string qurl = string.Empty;

            if (qstring.Trim().Length > 0)
            {
                string url = this.GetPageURL();
                qurl = url + ((url.IndexOf('?') > 0) ? "&" : "?") + qstring.Trim();
            }

            return qurl;
        }

        /// <summary>
        /// Gets the Page URL
        /// </summary>
        /// <returns>Returns the Page Url</returns>
        private string GetPageURL()
        {
            string url;
            string query;
            string zcid = string.Empty;

            if (Request.UrlReferrer == null)
            {
                url = Request.Url.ToString();
                query = Request.Url.Query;
                zcid = "?zcid=" + Request.QueryString["zcid"];
            }
            else
            {
                url = Request.UrlReferrer.ToString();
                query = Request.UrlReferrer.Query;
            }

            if (HttpContext.Current.Items["SeoUrlFound"] == null)
            {
                zcid = "?zcid=" + Request.QueryString["zcid"];
            }

            if (query.Length > 0)
            {
                url = url.Substring(0, url.IndexOf('?'));
            }

            return url + zcid.Trim();
        }

        /// <summary>
        /// Bind the Label control
        /// </summary>
        /// <param name="selectedFacetValues">Selected Facet Values</param>
        private void BindSelectedFacets(string selectedFacetValues)
        {
            ZNodeFacets Faceting = new ZNodeFacets();
            DataSet facetNameDataset = new DataSet();
            bool IsClearNeed = false;

            // Clear the controls
            phSelectedFacets.Controls.Clear();

            // String Builder
            StringBuilder labelFacetValues = new StringBuilder();

            facetNameDataset = Faceting.GetBreadCrumbsByFacetIds(this._SelectedFacetValues);
            facetNameDataset.Tables[0].PrimaryKey = new DataColumn[] { facetNameDataset.Tables[0].Columns["FacetId"] };

			ZNode.Libraries.DataAccess.Entities.FacetGroup tg;
			var tgs = new ZNode.Libraries.DataAccess.Service.FacetGroupService();
            Literal literal1 = null;
            divSelectedTitle.Visible = false;

            foreach (string facets in this._SelectedFacetValues.Split(','))
            {
                if (!string.IsNullOrEmpty(facets))
                {
                    DataRow facet = facetNameDataset.Tables[0].Rows.Find(facets);

                    if (facet != null)
                    {
                        divSelectedTitle.Visible = true;
                        IsClearNeed = true;

                        // Start to add the dynamic controls here
                        phSelectedFacets.Controls.Add(new LiteralControl("<div>"));

						tg = tgs.GetByFacetGroupID(int.Parse(Convert.ToString(facet["FacetGroupId"])));

                        literal1 = new Literal();
						literal1.Text = "<span class=\"SelectedTagText\">" + tg.FacetGroupLabel + ":</span><span class=\"SelectedTagValue\">" + facet["FacetName"].ToString() + "</span><br />";

                        HyperLink linkBtnFacetName = new HyperLink();
                        linkBtnFacetName.ID = "breadcrumblnkBtnTag" + facet["FacetId"].ToString();
                        linkBtnFacetName.Text = String.Format("[{0}]", this.GetLocalResourceObject("Remove").ToString());
                        linkBtnFacetName.CssClass = "linkBtnFacetText";
                        linkBtnFacetName.Attributes["style"] = "font-size:7pt;";
                        linkBtnFacetName.NavigateUrl = this.ConstructQueryStringFromSessionValues(Convert.ToInt32(facet["FacetId"]));

                        phSelectedFacets.Controls.Add(literal1);
                        phSelectedFacets.Controls.Add(linkBtnFacetName);
                        phSelectedFacets.Controls.Add(new LiteralControl("</div><br />"));
                    }
                }
            }

            if (IsClearNeed)
            {
                // Start to add the dynamic controls here
                phSelectedFacets.Controls.Add(new LiteralControl("<div>"));

                HyperLink linkBtnFacetName = new HyperLink();
                linkBtnFacetName.ID = "breadcrumblnkBtnTag0";
                linkBtnFacetName.Text = this.GetLocalResourceObject("ClearAllSelections").ToString();
                linkBtnFacetName.CssClass = "linkBtnFacetText";
                linkBtnFacetName.Style.Add("font-size", "7pt");
                linkBtnFacetName.NavigateUrl = this.ConstructQueryStringFromSessionValues(0);

                phSelectedFacets.Controls.Add(linkBtnFacetName);
                phSelectedFacets.Controls.Add(new LiteralControl("</div>"));
            }
        }

        #region Selected Facets

        /// <summary>
        /// Construct the query string from session values
        /// </summary>
        /// <param name="facetId">The value of facet Id</param>
        /// <returns>Returns the Query String From Session Values</returns>
        private string ConstructQueryStringFromSessionValues(int facetId)
        {
            string qstring = string.Empty;

			var facetService = new ZNode.Libraries.DataAccess.Service.FacetService();
			var facetGroupService = new ZNode.Libraries.DataAccess.Service.FacetGroupService();

                string[] keyValues = Session["FacetedValues"].ToString().Split(',');

                foreach (string sessionkey in keyValues)
                {
                    if (!string.IsNullOrEmpty(sessionkey))
                    {
                        if (sessionkey == facetId.ToString() || facetId == 0)
                        {
                            continue;
                        }

						Facet facet = facetService.GetByFacetID(Convert.ToInt32(sessionkey));

                        if (facet != null)
                        {
							qstring = qstring + (qstring.Trim().Length == 0 ? string.Empty : "&") + this.QKeyName(facetGroupService.GetByFacetGroupID(facet.FacetGroupID.Value).FacetGroupLabel) + Server.UrlEncode(facet.FacetName);
                        }
                    }
                }

            string url = this.GetCurrentPageURL();

            string qurl = url;

            if (qstring.Trim().Length > 0)
            {
                qurl = url + ((url.IndexOf('?') > 0) ? "&" : "?") + qstring.Trim();
            }

            return qurl;
        }

        private string IsSEOUrl()
        {
            string zcid = string.Empty;

            if (Request.QueryString["zcid"] != null)
            {
                Session["BreadCrumzcid"] = Request.QueryString["zcid"];
                zcid = Request.QueryString["zcid"].ToString();
            }
            else if (Session["BreadCrumzcid"] != null)
            {
                zcid = Session["BreadCrumzcid"].ToString();
            }
            else if (Request.QueryString["zpid"] != null)
            {
                ZNode.Libraries.DataAccess.Service.ProductCategoryService service = new ZNode.Libraries.DataAccess.Service.ProductCategoryService();
                TList<ProductCategory> pcategories = service.GetByProductID(Convert.ToInt32(Request.QueryString["zpid"]));
                if (pcategories.Count > 0)
                {
                    zcid = pcategories[0].CategoryID.ToString();
                }
            }
            else
            {
                zcid = "0";
            }

            ZNode.Libraries.DataAccess.Service.CategoryService cservice = new ZNode.Libraries.DataAccess.Service.CategoryService();
            Category category = cservice.GetByCategoryID(Convert.ToInt32(zcid));
            if (category != null)
            {
                string seourl = string.Empty;
                if (category.SEOURL != null)
                {
                    seourl = category.SEOURL;
                }

                return ZNodeSEOUrl.MakeURL(category.CategoryID.ToString(), SEOUrlType.Category, seourl);
            }

            return ZNodeSEOUrl.MakeURL("0", SEOUrlType.Category, string.Empty);
        }

        /// <summary>
        /// Gets the Current Page URL
        /// </summary>
        /// <returns>Returns the Current Page URL</returns>
        private string GetCurrentPageURL()
        {
            return this.IsSEOUrl();
        }

        #endregion

        #endregion
    }
}