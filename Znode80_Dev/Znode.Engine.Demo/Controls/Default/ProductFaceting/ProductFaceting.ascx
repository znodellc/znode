<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_ProductFaceting_ProductFaceting" Codebehind="ProductFaceting.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>

<div class="ProductTagging">   
     <div class="Title" runat="server" id="divSelectedTitle"><ZNode:CustomMessage id="CustomMessage2" MessageKey="ProductTaggingSelectedTitle" runat="server"></ZNode:CustomMessage></div>  
     <asp:PlaceHolder runat="server" ID="phSelectedFacets"></asp:PlaceHolder> 
     <asp:Panel ID="pnlProductFacetingTitle" runat="server" Visible="false" EnableViewState="false">
         <div class="Title"><ZNode:CustomMessage id="CustomMessage1" EnableViewState="false" MessageKey="ProductTaggingTitle" runat="server"></ZNode:CustomMessage></div>
     </asp:Panel>
     <asp:PlaceHolder runat="server" ID="ControlsPlaceHolder"></asp:PlaceHolder> 
</div>