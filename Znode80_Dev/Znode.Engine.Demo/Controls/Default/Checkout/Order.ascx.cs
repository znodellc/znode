using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

/// <summary>
/// Checkout order information
/// </summary>
namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Order user control class.
    /// </summary>
    public partial class Controls_Default_Checkout_Order : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeShoppingCart _shoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        private ZNodePortalCart _portalCart;
        private int _PortalID;
        ZNodeUserAccount userAccount = (ZNodeUserAccount)ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount);
        #endregion

        public event System.EventHandler ShippingSelectedIndexChanged;
        public event System.EventHandler RemoveChanged;

        #region Public Properties
        public int PortalID
        {
            get { return this._PortalID; }
            set { this._PortalID = value; }
        }
        #endregion

        #region Public Methods

        public string GetValue(string value, string FieldName)
        {
            // Get Shopping Cart Item
            ZNodeShoppingCartItem item = this._shoppingCart.GetItem(value);
            if (item != null)
            {
                switch (FieldName)
                {
                    case "ViewProductLink":
                        return item.Product.ViewProductLink;
                    case "ImageAltTag":
                        return item.Product.ImageAltTag;
                    case "ThumbnailImageFilePath":
                        return item.Product.ThumbnailImageFilePath;
                    case "Name":
                        return item.Product.Name;
                    case "ShoppingCartDescription":
                        return item.Product.ShoppingCartDescription;
                    case "ProductNum":
                        return item.Product.ProductNum;
                }

            }
            return string.Empty;

        }

        public int GetQuantity(object item, int addressId)
        {
            if (item is ZNodeShoppingCartItem)
            {
                var cartItem = item as ZNodeShoppingCartItem;
                return cartItem.OrderShipments.Where(x => x.AddressID == addressId).Sum(x => x.Quantity);
            }

            return 0;
        }

        /// <summary>
        /// Represents the BindGrid method
        /// </summary>
        public void BindGrid()
        {
            this._portalCart = this._shoppingCart.PortalCarts.FirstOrDefault(x => x.PortalID == PortalID);
            var portalCart  = this._portalCart;
            rptShiping.DataSource = portalCart != null ? portalCart.AddressCarts : null;
            rptShiping.DataBind();

            var userAccount = ZNodeUserAccount.CurrentAccount();

            if (userAccount != null)
            {
                this._shoppingCart.Payment.BillingAddress = userAccount.BillingAddress;

                // Check Sale Tax rate
                if (this._shoppingCart.TaxRate > 0)
                {
                    TaxPct.Text = "(" + this._shoppingCart.TaxRate + "%)";
                }
                else
                {
                    TaxPct.Text = string.Empty;
                }
            }
        }

        public void BindShipping(DropDownList ddlShipping, string destinationShippingCode, int selectedValue = 0)
        {

            DataSet ds = new DataSet();
            DataView dv = new DataView();

            if (userAccount != null)
            {
                // Find is default store, so we include all profiles.
                CategoryService categoryService = new CategoryService();
                int total;
                TList<Category> categoryList = categoryService.GetByPortalID(this.PortalID, 0, 1, out total);
                bool IsDefaultPortal = categoryList.Count == 0;

                ShippingService shipServ = new ShippingService();
                var shippingList = shipServ.GetAll();
                shippingList.Sort("DisplayOrder Asc");

                int profileID = 0;
                if (userAccount != null)
                {
                    profileID = userAccount.ProfileID;
                }
                else
                {
                    // Get Default Registered profileId
                    profileID = ZNodeProfile.DefaultRegisteredProfileId;
                }

                if (!IsDefaultPortal)
                {
                    // If franchise store then hide the "All profiles" associated shipping option.
                    PortalProfileService ppservice = new PortalProfileService();
                    TList<PortalProfile> portalProfiles = ppservice.GetByPortalID(this.PortalID);
                    portalProfiles.ApplyFilter(delegate(PortalProfile pProfile) { return pProfile.ProfileID != profileID; });

                    if (portalProfiles.Count > 0)
                    {
                        profileID = portalProfiles[0].ProfileID;
                    }

                    shippingList.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.Shipping shipping)
                    {
                        return shipping.ActiveInd == true && shipping.ProfileID == profileID && (shipping.DestinationCountryCode == destinationShippingCode || string.IsNullOrEmpty(shipping.DestinationCountryCode))
                                                                                                                 ;
                    });
                }
                else
                {
                    // If site admin store then show all profiles.                   
                    PortalProfileService ppservice = new PortalProfileService();
                    TList<PortalProfile> portalProfiles = ppservice.GetByPortalID(this.PortalID);
                    portalProfiles.ApplyFilter(delegate(PortalProfile pProfile) { return pProfile.ProfileID == profileID; });

                    if (portalProfiles.Count == 0)
                    {
                        profileID = 0;
                    }

                    // If site admin store then show all profiles.
                    shippingList.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.Shipping shipping) { return (shipping.ActiveInd == true) && (shipping.ProfileID == profileID || shipping.ProfileID == null) && (shipping.DestinationCountryCode == destinationShippingCode || string.IsNullOrEmpty(shipping.DestinationCountryCode)); });

                }

                ds = shippingList.ToDataSet(false);
                dv = new DataView(ds.Tables[0]);

              
                if (dv.ToTable().Rows.Count > 0)
                {
                    foreach (DataRow dr in dv.ToTable().Rows)
                    {
	                    string description = Server.HtmlDecode(dr["Description"].ToString());
                        System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("&reg;");
                        description = regex.Replace(description, "�");
                        ListItem li = new ListItem(description, dr["ShippingID"].ToString());
                        ddlShipping.Items.Add(li);
                    }

                    ddlShipping.SelectedIndex = 0;
                }
            }

            if (Request.Form[ddlShipping.UniqueID] != null)
            {
                int shipId = 0;
                if (int.TryParse(Request.Form[ddlShipping.UniqueID], out shipId))
                {
                    ListItem li = ddlShipping.Items.FindByValue(shipId.ToString());
                    if (li != null)
                    {
                        ddlShipping.SelectedIndex = ddlShipping.Items.IndexOf(li);
                    }
                }
            }
            else
            {
                var item = ddlShipping.Items.FindByValue(selectedValue.ToString());
                if (item != null)
                    ddlShipping.SelectedIndex = ddlShipping.Items.IndexOf(item);
            }
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        ///  Page Pre Render Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();
            if (userAccount != null)
            {
                if (this.ShippingSelectedIndexChanged != null)
                {
                    // Triggers parent page event to hide or show the payment section
                    this.ShippingSelectedIndexChanged(sender, e);
                }
            
                BindTotals();
            }
        }

        /// <summary>
        /// Event triggered when shipping option is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstShipping_SelectedIndexChanged(object sender, EventArgs e)
        {
            var ddl = sender as DropDownList;

            this.SetShippingSettings(ddl, this._portalCart.AddressCarts.Find(x => x.AddressID == Convert.ToInt32(ddl.ID.Replace("lstShipping_", ""))));

            if (this.ShippingSelectedIndexChanged != null)
            {
                // Triggers parent page event to hide or show the payment section
                this.ShippingSelectedIndexChanged(sender, e);
            }
        }

        protected void rptShiping_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var uxCart = (GridView)e.Item.FindControl("uxCart");
                var addressitem = e.Item.DataItem as ZNodeMultipleAddressCart;
				var taxTotal = (Label)e.Item.FindControl("lblTaxCost");
				var taxTotalText = (Label)e.Item.FindControl("lblTaxCostText");
                var ddlShipping = (DropDownList)e.Item.FindControl("lstShipping");
                var shipping = (Label)e.Item.FindControl("lblShipping");
				var subTotal = (Label)e.Item.FindControl("lblSubtotal");
                var shippingText = (Label)e.Item.FindControl("lblShippingText");
	            var subTotalText = (Label) e.Item.FindControl("lblSubtotalText");
                var shippingTo = (Label)e.Item.FindControl("lblShippto");
                var lblShippingAddress = (Label)e.Item.FindControl("lblShippingAddress");
                var lblErrorMessage = (Label)e.Item.FindControl("lblErrorMessage");
                HtmlAnchor linkChange = (HtmlAnchor)e.Item.FindControl("lnkChange");
                linkChange.HRef = "~/Address.aspx?ItemId=" + addressitem.AddressID + "&returnurl=checkout.aspx&cartID=" + HttpUtility.UrlEncode(addressitem.AddressCartID.ToString());
                ddlShipping.ID = "lstShipping_" + addressitem.AddressID;

				var divShipping = (HtmlGenericControl)e.Item.FindControl("ShipSection");

                if (ddlShipping.Items.Count == 0)
                {
                    var shippingAddress =
                        this.userAccount.Addresses.FirstOrDefault(x => x.AddressID == addressitem.AddressID);

                    string countryCode = shippingAddress!=null?shippingAddress.CountryCode:string.Empty;
                    int shippingId = 0;
                    if (addressitem.Shipping != null)
                        shippingId = addressitem.Shipping.ShippingID;
                    this.BindShipping(ddlShipping, countryCode, shippingId);

                    divShipping.Visible = ddlShipping.Items.Count > 0;
                    
                    this.SetShippingSettings(ddlShipping, addressitem);

                    if (this.ShippingSelectedIndexChanged != null)
                    {
                        // Triggers parent page event to hide or show the payment section
                        this.ShippingSelectedIndexChanged(sender, e);
                    }
                }

                if (this._portalCart.AddressCarts.Count > 1)
                {
					shippingText.Visible = addressitem.ShippingCost > 0 ? true : false;
                    shippingTo.Visible = true;
                    lblShippingAddress.Visible = true;
                    linkChange.Visible = true;
					shipping.Visible = addressitem.ShippingCost > 0 ? true : false;
					subTotal.Visible = true;
	                subTotalText.Visible = true;
					taxTotal.Visible = addressitem.TaxCost > 0 ? true : false;
					taxTotalText.Visible = addressitem.TaxCost > 0 ? true : false;
                    shipping.Text = addressitem.ShippingCost.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
					subTotal.Text = addressitem.SubTotal.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
					taxTotal.Text = addressitem.TaxCost.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                }
                else
                {
                    linkChange.Visible = false;
                }

                if (!string.IsNullOrEmpty(addressitem.ErrorMessage))
                {
                    lblErrorMessage.Text = addressitem.ErrorMessage;
                    shippingText.Visible = false;
                    shipping.Visible = false;
                }
                uxCart.DataSource = addressitem.ShoppingCartItems;
                uxCart.ShowHeader = e.Item.ItemIndex == 0;
                uxCart.DataBind();
            }
        }

        /// <summary>
        /// Grid command event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxCart_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (this._portalCart == null)
            {
                this._portalCart = this._shoppingCart.PortalCarts.FirstOrDefault(x => x.PortalID == PortalID);
            }

            // The remove link was clicked
            if (e.CommandName.Equals("remove"))
            {
                string[] guidAddressId = e.CommandArgument.ToString().Split('|');


                bool status = this._portalCart.RemoveAddressCartItem(guidAddressId[1], guidAddressId[0]);

                if (status)
                {

                    if (this._shoppingCart.ShoppingCartItems.Count == 0)
                        Response.Redirect("~/Shoppingcart.aspx");

                    // Triggers parent page event
                    this.BindGrid();
                    if (this.RemoveChanged != null)
                    {
                        // Triggers parent page event to hide or show the payment section
                        this.RemoveChanged(sender, e);
                    }
                }

            }
        }

        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #endregion

        #region Private Methods

        //Working on this 
        /// <summary>
        ///  Represents the BindTotals method
        /// </summary>
        private void BindTotals()
        {
            // Bind totals
			lblTotalShipping.Text = GetShippingTotal().ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
			
            SubTotal.Text = this._portalCart.SubTotal.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            Discount.Text = "-" + this._portalCart.Discount.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            lblGiftCardAmount.Text = "-" + this._portalCart.GiftCardAmount.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
			Tax.Text = GetTaxTotal().ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();

            
            Total.Text = this._portalCart.Total.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            rowDiscount.Visible = this._portalCart.Discount > 0 ? true : false;
            divGiftCardAmount.Visible = this._portalCart.GiftCardAmount > 0 ? true : false;
            rowTax.Visible = this._portalCart.TaxCost > 0 ? true : false;
            rowTotalShipping.Visible = this._portalCart.ShippingCost > 0 ? true : false;
        }

        private decimal GetShippingTotal()
        {
			_portalCart.Calculate();
            _portalCart.AddressCarts.ForEach(x => { SetPaymentControlData(x); x.Calculate(); });
			return _portalCart.ShippingCost;
        }

		private decimal GetTaxTotal()
		{
			_portalCart.Calculate();
			_portalCart.AddressCarts.ForEach(x => { SetPaymentControlData(x); x.Calculate(); });
			return _portalCart.AddressCarts.Sum(x => x.OrderLevelTaxes);
		}

        /// <summary>
        /// Bind data to payment control
        /// </summary>
        protected void SetPaymentControlData(ZNodeMultipleAddressCart addressCart)
        {
            ZNodePayment Payment = new ZNodePayment();
            Payment.BillingAddress = userAccount.BillingAddress;
            Payment.ShippingAddress = this.userAccount.Addresses.FirstOrDefault(x => x.AddressID == addressCart.AddressID);
            addressCart.Payment = (ZNodePayment)Payment;
        }

        /// <summary>
        /// Re-calculate shipping cost
        /// </summary>
        private void SetShippingSettings(DropDownList ddlShipping, ZNodeMultipleAddressCart objShoppingCart)
        {
            //var objShoppingCart = this._portalCart.AddressCarts.Find(x => x.AddressID == addressId);			

            if (ddlShipping.Items.Count > 0)
            {
                int shippingID = int.Parse(ddlShipping.SelectedValue);

                if (objShoppingCart != null)
                {
                    // Set shipping name in shopping cart object
                    objShoppingCart.Shipping.ShippingName = ddlShipping.SelectedItem.Text;
                    objShoppingCart.Shipping.ShippingID = shippingID;
					SetPaymentControlData(objShoppingCart);
                    objShoppingCart.Calculate();
                }
                else
                {
                    // Set shipping name in shopping cart object
                    this._shoppingCart.Shipping.ShippingName = ddlShipping.SelectedItem.Text;
                    this._shoppingCart.Shipping.ShippingID = shippingID;
                    this._shoppingCart.Calculate();
                }
            }
        }

        /// <summary>
        /// Bind the user account list.
        /// </summary>
        /// <param name="addressId">Address ID</param>
        /// <returns>Returns the Address Text</returns>
        public string GetAddress(string addressId)
        {
            string addressText = string.Empty;

            Address address = userAccount.Addresses.FirstOrDefault(x => x.AddressID == Convert.ToInt32(addressId));
            if (address != null)
            {
                addressText = address.ToString().Replace("<BR>", ",");
            }

            return addressText;
        }
        #endregion
    }
}