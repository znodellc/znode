<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Checkout_OrderReceipt"
    CodeBehind="OrderReceipt.ascx.cs" %>
<div style="padding-bottom: 30px;">
    <img src="<%=BrowserFingerprintImageUrl %>" height="1" width="1" />
    <input type="hidden" id="OrderID" value="<%=OrderID %>" />
    <input type="hidden" id="OrderTotal" value="<%=OrderTotal %>" />
    <input type="hidden" id="ShippingTotal" value="<%=ShippingTotal %>" />
    <input type="hidden" id="TaxTotal" value="<%=TaxTotal %>" />
    <%=ReceiptTemplate%>
</div>
