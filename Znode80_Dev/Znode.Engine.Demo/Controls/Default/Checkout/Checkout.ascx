<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Checkout_Checkout"
	CodeBehind="Checkout.ascx.cs" %>
<%@ Register Src="StepTracker.ascx" TagName="StepTracker" TagPrefix="uc6" %>
<%@ Register Src="Order.ascx" TagName="Order" TagPrefix="uc4" %>
<%@ Register Src="Payment.ascx" TagName="Payment" TagPrefix="uc5" %>
<%@ Register Src="Address.ascx" TagName="Address" TagPrefix="uc3" %>
<%@ Register Src="OrderReceipt.ascx" TagName="OrderReceipt" TagPrefix="Order" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>
<div class="Checkout">
	<%--Disable the submit button and then call the server side button code.--%>

	<script type="text/javascript">
	    function submitOrder(ctrlId, btdId) {            
			if (Page_ClientValidate('')) {
				document.getElementById(ctrlId).disabled = true;
				if (!IsIE9()) __doPostBack(btdId, '');
				return true;
			}
			return true;
		}

		function IsIE9() {
			var rv = -1; // Return value assumes failure.
			if (navigator.appName == 'Microsoft Internet Explorer') {
				var ua = navigator.userAgent;
				var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
				if (re.exec(ua) != null)
					rv = parseFloat(RegExp.$1);
			}
			return parseInt(rv) == 9;
		}	

	</script>

	<asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
			<ProgressTemplate>
				<asp:Panel ID="pnlBackGround" runat="server" CssClass="popup-div-background">
					<div id="divFeedback" runat="server" class="popup-div-front">
						<span class="Processingtext">Processing ...</span>
						<img id="ImgProcess" src="~/themes/Default/Images/ajax-loader.gif" alt="Processing" runat="Server" />
					</div>
				</asp:Panel>
			</ProgressTemplate>
    </asp:UpdateProgress>
	
	<asp:UpdatePanel ID="UpdatePanel1" runat="server">
		
		<ContentTemplate>
<asp:Panel ID="pnlCheckout" runat="server" DefaultButton="SubmitButton">
			<div class="PageTitle">
				<asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtSecureCheckout"></asp:Localize>
			</div>
			<div class="Steps">
				<uc6:StepTracker ID="uxStepTracker" runat="server" />
			</div>
			<div style="padding-top: 2px; padding-bottom: 2px;">
				<asp:Label CssClass="Error" ID="lblError" runat="server" EnableViewState="False"
					meta:resourcekey="lblErrorResource1"></asp:Label>
			</div>
			<p class="Review">
				<asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtReview">
				</asp:Localize>
			</p>
			<asp:Repeater ID="rptCart" runat="server" OnItemDataBound="RptCart_ItemDataBound">
				<ItemTemplate>
					<div>
						<uc4:Order ID="uxOrder" runat="server" PortalID='<%# DataBinder.Eval(Container.DataItem, "Product.PortalID") %>' />
					</div>
					<div>
						<uc1:spacer ID="Spacer1" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
							runat="server"></uc1:spacer>
					</div>
				</ItemTemplate>
			</asp:Repeater>
			<div>
				<uc1:spacer ID="Spacer1" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
					runat="server"></uc1:spacer>
			</div>
			<div>
				<uc5:Payment ID="uxPayment" runat="server" />
			</div>
			<table align="left">
				<tr>
					<td style="padding-bottom: 10px;">
						<img id="Img1" src="~/themes/Default/Images/Submit_Arrow.gif" alt="Submit" runat="Server" />
					</td>
					<td>
						<div class="ProceedtoNextButton">
							<asp:LinkButton ID="SubmitButton" OnClick="OnSubmitOrder" runat="server" CssClass="Button"
								meta:resourcekey="SubmitButtonResource1">Submit Order</asp:LinkButton>
						</div>
					</td>
				</tr>
				<tr>
					<td>

						<div>
							<uc1:spacer ID="Spacer2" EnableViewState="false" SpacerHeight="1" SpacerWidth="10"
								runat="server"></uc1:spacer>
						</div>
					</td>
				</tr>
			</table>
	</asp:Panel>
		</ContentTemplate>
	</asp:UpdatePanel>

</div>
