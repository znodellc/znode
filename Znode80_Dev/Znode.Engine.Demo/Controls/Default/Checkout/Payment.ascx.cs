using System;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using EnumPaymentType = ZNode.Libraries.ECommerce.Entities.PaymentType;

/// <summary>
/// Checkout Payment
/// </summary>
namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Payment user control class.
    /// </summary>
    public partial class Controls_Default_Checkout_Payment : System.Web.UI.UserControl
    {
        #region Member Variables
        private PaymentSetting _paymentSetting;
        private ZNodeUserAccount _userAccount;
        private ZNodeShoppingCart shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        private ZNodePayment _payment;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        private bool _IsMultipleVendor = false;
        #endregion

        public event System.EventHandler EditAddressClicked;

        public event System.EventHandler PayPalClicked;

        public event System.EventHandler GoogleCheckoutClicked;

        public event System.EventHandler TwoCheckoutClicked;

        public event System.EventHandler MultipleAddressClicked;

        #region Public Properties

        public int BillingAddressId
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether to check the items in the shopping cart are from Multiple vendors. If multiple vendors, we will show only Credit Card payment type.
        /// </summary>
        public bool IsMultipleVendorShoppingCart
        {
            get
            {
                return this._IsMultipleVendor;
            }

            set
            {
                this._IsMultipleVendor = value;
                pnlGiftCard.Visible = !this._IsMultipleVendor;
            }
        }

        /// <summary>
        /// Sets a value indicating whether the Payment section on the final checkout page
        /// </summary>
        public bool ShowPaymentSection
        {
            set { pnlPayment.Visible = value; }
        }

        /// <summary>
        /// Gets or sets the Payment Object
        /// </summary>
        public ZNodePayment Payment
        {
            get
            {
                // Code for Paypal payment
                if (this.PaymentType == ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL
                    || this.PaymentType == ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT
                    || this.PaymentType == ZNode.Libraries.ECommerce.Entities.PaymentType.TwoCO)
                {
                    ZNodePayment _paypalpayment = new ZNodePayment();
                    this._payment = _paypalpayment;
                }

                // Check payment type            
                if (this.PaymentType == EnumPaymentType.CREDIT_CARD)
                {
                    ZNodePayment _creditCardPayment = new ZNodePayment();
                    if ((!string.IsNullOrEmpty(lstPaymentType.SelectedValue)) && lstPaymentType.SelectedItem.Text.Contains("Use card ending in"))
                    {
                        _creditCardPayment.UseToken = true;
                        _creditCardPayment.TokenId = Convert.ToInt32(lstPaymentType.SelectedValue);
                    }
                    else
                    {
                        _creditCardPayment.UseToken = false;

                        _creditCardPayment.CreditCard.CardNumber = txtCreditCardNumber.Text.Trim();
                        _creditCardPayment.CreditCard.CreditCardExp = lstMonth.SelectedValue + "/" + lstYear.SelectedValue;
                        _creditCardPayment.CreditCard.CardSecurityCode = txtCVV.Text.Trim();
                        if (chkSaveCardData.Checked)
                        {
                            _creditCardPayment.SaveCardData = true;
                        }
                    }

                    this._payment = _creditCardPayment;
                }
                else if (this.PaymentType == EnumPaymentType.PURCHASE_ORDER)
                {
                    ZNodePayment _purchaseOrderPayment = new ZNodePayment();
                    this._payment = _purchaseOrderPayment;
                }
                else if (this.PaymentType == EnumPaymentType.COD)
                {
                    ZNodePayment _chargeOnDeliveryPayment = new ZNodePayment();
                    this._payment = _chargeOnDeliveryPayment;
                }

                // Set payment Name to display it on the order receipt
                if (this.PaymentType == EnumPaymentType.PAYPAL || this.PaymentType == EnumPaymentType.GOOGLE_CHECKOUT || this.PaymentType == EnumPaymentType.TwoCO)
                {
                    PaymentTypeService paymentTypeService = new PaymentTypeService();
                    ZNode.Libraries.DataAccess.Entities.PaymentType pt = paymentTypeService.GetByPaymentTypeID((int)this.PaymentType);
                    if (pt != null)
                    {
                        this._payment.PaymentName = pt.Name;
                    }
                }
                else if (lstPaymentType.Items.Count > 0)
                {
                    this._payment.PaymentName = lstPaymentType.SelectedItem.Text;
                }

                return this._payment;
            }

            set
            {
                this._payment = value;

                lblBillingAddress.Text = this._payment.BillingAddress.ToString();

                lblShippingAddress.Text = this._payment.ShippingAddress.ToString();
            }
        }

        /// <summary>
        /// Gets the payment type selected
        /// </summary>
        public EnumPaymentType PaymentType
        {
            get
            {
                EnumPaymentType pt = EnumPaymentType.CREDIT_CARD;

                if (Session["PaymentType"] != null)
                {
                    pt = (EnumPaymentType)Enum.ToObject(typeof(EnumPaymentType), Convert.ToInt32(Session["PaymentType"]));
                }
                else if (lstPaymentType.Items.Count > 0 && !lstPaymentType.SelectedItem.Text.Contains("Use card ending in"))
                {
                    PaymentSettingService _pmtServ = new PaymentSettingService();
                    ZNode.Libraries.DataAccess.Entities.PaymentSetting pmtSetting = _pmtServ.DeepLoadByPaymentSettingID(int.Parse(lstPaymentType.SelectedValue), true, DeepLoadType.IncludeChildren, typeof(ZNode.Libraries.DataAccess.Entities.PaymentType));
                    int _paymentTypeID = pmtSetting.PaymentTypeID;
                    if (_paymentTypeID == (int)EnumPaymentType.CREDIT_CARD)
                    {
                        pt = EnumPaymentType.CREDIT_CARD;
                    }
                    else if (_paymentTypeID == (int)EnumPaymentType.PURCHASE_ORDER)
                    {
                        pt = EnumPaymentType.PURCHASE_ORDER;
                    }
                    else if (_paymentTypeID == (int)EnumPaymentType.COD)
                    {
                        pt = EnumPaymentType.COD;
                    }
                    else if (_paymentTypeID == (int)EnumPaymentType.TwoCO)
                    {
                        pt = EnumPaymentType.TwoCO;
                    }
                }

                return pt;
            }
        }

        /// <summary>
        /// Gets or sets the payment settingId associated with the payment type selected
        /// </summary>
        public int PaymentSettingID
        {
            get
            {
                int paymentSettingId = 0;

                if (this.PaymentType == ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL || this.PaymentType == ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT)
                {
                    // Paypal or Google checkout payment setting selected
                    if (!string.IsNullOrEmpty(hfPaymentSettingID.Value))
                    {
                        paymentSettingId = Convert.ToInt32(hfPaymentSettingID.Value);
                    }
                }
                else if (this.PaymentType == ZNode.Libraries.ECommerce.Entities.PaymentType.TwoCO)
                {
                    if (!string.IsNullOrEmpty(hfPaymentSettingID.Value))
                    {
                        paymentSettingId = Convert.ToInt32(hdn2COpaymentSettingID.Value);
                    }
                }
                else if (!string.IsNullOrEmpty(lstPaymentType.SelectedValue) && lstPaymentType.SelectedItem.Text.Contains("Use card ending in"))
                {
                    paymentSettingId = this.Payment.GetPaymentSettingIdByTokenId(Convert.ToInt32(lstPaymentType.SelectedValue));
                }
                else if (lstPaymentType.Items.Count > 0)
                {
                    paymentSettingId = int.Parse(lstPaymentType.SelectedValue);
                }

                return paymentSettingId;
            }

            set
            {
                lstPaymentType.SelectedValue = value.ToString();
            }
        }

        /// <summary>
        /// Gets the PO number if purchase Order payment CreateCustomerProfile selected
        /// </summary>
        public string PurchaseOrderNumber
        {
            get
            {
                return txtPONumber.Text.Trim();
            }
        }

        /// <summary>
        /// Gets the special additional instructions
        /// </summary>
        public string AdditionalInstructions
        {
            get
            {
                return Server.HtmlEncode(txtAdditionalInstructions.Text.Trim());
            }
        }

        /// <summary>
        /// Gets a value indicating whether Save Credit Card data checkbox is checked or not
        /// </summary>
        public bool SaveCardData
        {
            get
            {
                return chkSaveCardData.Checked;
            }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Set Cvv File path dynamically
        /// </summary>
        /// <returns>Returns the Cvv Path</returns>
        public string GetCvvPath()
        {
            string JavaScript = "javascript:popupWin=window.open('Controls/default/Cvv/cvv.htm','EIN','scrollbars,resizable,width=515,height=300,left=50,top=50');popupWin.focus();";

            CVVhyperlink.NavigateUrl = JavaScript;

            return JavaScript;
        }

        /// <summary>
        /// Binds the payment types
        /// </summary>
        public void BindPaymentTypeData(bool loadDropdown = false)
        {
            this._userAccount = (ZNodeUserAccount)ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount);

            // Find is default store, so we include all profiles.
            CategoryService categoryService = new CategoryService();
            int total;
            TList<Category> categoryList = categoryService.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID, 0, 1, out total);
            bool IsDefaultPortal = categoryList.Count == 0;

            int profileID = 0;
            if (this._userAccount != null)
            {
                profileID = this._userAccount.ProfileID;
            }
            else
            {
                // Get Default Registered profileId
                profileID = ZNodeProfile.DefaultRegisteredProfileId;
            }

            PaymentSettingService _pmtServ = new PaymentSettingService();
            ZNode.Libraries.DataAccess.Entities.TList<PaymentSetting> _pmtSetting = _pmtServ.GetAll();

            // Show or hide google express checkout button
            if (!this.IsMultipleVendorShoppingCart)
            {
                PortalProfileService ps = new PortalProfileService();
                TList<PortalProfile> portalProfiles = ps.GetByProfileID(profileID);
                if (!ZNodeShoppingCart.CurrentShoppingCart().IsMultipleShipToAddress)
                {
                    this._paymentSetting =
                        _pmtSetting.Find(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                            {
                                return (pmt.ProfileID == profileID || (pmt.ProfileID == null && IsDefaultPortal)) &&
                                       pmt.PaymentTypeID ==
                                       (int)ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT &&
                                       pmt.ActiveInd == true;
                            });

                    if (this._paymentSetting != null)
                    {
                        string merchantID = this._paymentSetting.GatewayUsername;
                        hfPaymentSettingID.Value = this._paymentSetting.PaymentSettingID.ToString();
                        ZNodeEncryption decrypt = new ZNodeEncryption();
                        GoogleSubmitButton.ImageUrl = string.Format(GoogleSubmitButton.ImageUrl,
                                                                    decrypt.DecryptData(merchantID));
                        pnlGoogleCheckout.Visible = true;
                    }
                }
                // Check paypal express configured.
                PaymentSetting paypalPaymentSetting = _pmtSetting.Find(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                {
                    return (pmt.ProfileID == profileID || (pmt.ProfileID == null && IsDefaultPortal)) &&
                        pmt.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL &&
                        pmt.ActiveInd == true;
                });

                if (paypalPaymentSetting != null && paypalPaymentSetting.ActiveInd)
                {
                    hfPaymentSettingID.Value = paypalPaymentSetting.PaymentSettingID.ToString();
                    pnlPaypal.Visible = true;
                }

                PaymentSetting _2COpaymentSetting = _pmtSetting.Find(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                {
                    return (pmt.ProfileID == profileID || (pmt.ProfileID == null && IsDefaultPortal)) &&
                        pmt.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.TwoCO &&
                        pmt.ActiveInd == true;
                });
                // Show/hide 2CO checkout button                    
                if (_2COpaymentSetting != null && !ZNodeShoppingCart.CurrentShoppingCart().IsMultipleShipToAddress)
                {
                    hdn2COpaymentSettingID.Value = _2COpaymentSetting.PaymentSettingID.ToString();
                    pnl2CO.Visible = true;
                }

            }

            if (this._userAccount != null)
            {
                BillingAddressId = this._userAccount.BillingAddress.AddressID;
                var billingAddress =
                        this._userAccount.Addresses.FirstOrDefault(x => x.AddressID == BillingAddressId);
                lblBillingAddress.Text = billingAddress.ToString();

                if (ZNodeShoppingCart.CurrentShoppingCart().IsMultipleShipToAddress)
                {
                    lnkMultipleShipping.Visible = false;
                    lnkEditShipping.Visible = false;
                    lnkView.Visible = true;
                    lblShippingAddress.Text = "You are shipping to Multiple Addresses";
                }
                else
                {
                    lblShippingAddress.Text = this._userAccount.ShippingAddress.ToString();
                    lnkEditShipping.Visible = true;
                }
            }

            if (lstPaymentType.Items.Count == 0 || loadDropdown)
            {
                lstPaymentType.Items.Clear();
                profileID = ZNodeProfile.CurrentUserProfileId;
                PortalProfileService ps = new PortalProfileService();
                TList<PortalProfile> portalProfiles = ps.GetByProfileID(profileID);

                if (this.IsMultipleVendorShoppingCart)
                {
                    _pmtSetting.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                    {
                        return (pmt.ProfileID == profileID || (pmt.ProfileID == null && IsDefaultPortal)) &&
                               (pmt.PaymentTypeID == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.CREDIT_CARD) &&
                               pmt.ActiveInd == true;
                    });

                }
                else
                {
                    var _profilePayments = _pmtSetting.FindAll(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                    {
                        return (pmt.ProfileID == profileID) &&
                            (pmt.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL &&
                            pmt.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT &&
                            pmt.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.TwoCO) &&
                            pmt.ActiveInd == true;
                    });

                    var allProfilePayments = _pmtSetting.FindAll(delegate(ZNode.Libraries.DataAccess.Entities.PaymentSetting pmt)
                    {
                        return (pmt.ProfileID == null && IsDefaultPortal) &&
                            (pmt.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL &&
                            pmt.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT &&
                            pmt.PaymentTypeID != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.TwoCO) &&
                            pmt.ActiveInd == true;
                    });

                    if (allProfilePayments.Any())
                    {
                        _profilePayments.AddRange(allProfilePayments.Where(x => !_profilePayments.Any(y => y.PaymentTypeID == x.PaymentTypeID)).ToList());
                    }

                    _pmtSetting = _profilePayments;
                }

                _pmtServ.DeepLoad(_pmtSetting, true, DeepLoadType.IncludeChildren, typeof(ZNode.Libraries.DataAccess.Entities.PaymentType));
                _pmtSetting.Sort("DisplayOrder");

                // If the customer has choosen saved card option
                if (this._userAccount != null)
                {
                    PaymentTokenService serv = new PaymentTokenService();
                    TList<PaymentToken> list = serv.GetByAccountID(this._userAccount.AccountID);

                    foreach (PaymentToken token in list)
                    {
                        ListItem scardli = new ListItem();
                        scardli.Text = "Use card ending in " + token.CreditCardDescription;
                        scardli.Value = token.PaymentTokenID.ToString();

                        lstPaymentType.Items.Add(scardli);
                    }
                }

                foreach (PaymentSetting _pmt in _pmtSetting)
                {
                    ListItem li = new ListItem();
                    li.Text = _pmt.PaymentTypeIDSource.Name;
                    li.Value = _pmt.PaymentSettingID.ToString();

                    lstPaymentType.Items.Add(li);

                    if (_pmt.PaymentTypeID == (int)EnumPaymentType.CREDIT_CARD)
                    {
                        imgAmex.Visible = (bool)_pmt.EnableAmex;
                        imgMastercard.Visible = (bool)_pmt.EnableMasterCard;
                        imgVisa.Visible = (bool)_pmt.EnableVisa;
                        imgDiscover.Visible = (bool)_pmt.EnableDiscover;
                    }
                }



                // Select first item
                if (lstPaymentType.Items.Count > 0)
                {
                    lstPaymentType.Items[0].Selected = true;

                    // Show appropriate payment control
                    this.SetPaymentControl();
                }
                else
                {
                    if (this.IsMultipleVendorShoppingCart)
                    {
                        pnlPayment.Visible = false;
                        ErrorPanel.Visible = true;

                        StringBuilder productName = new StringBuilder();
                        ZNodeShoppingCart cartItems = new ZNodeShoppingCart();
                        if (shoppingCart != null)
                        {
                            foreach (ZNodeShoppingCartItem cartItem in shoppingCart.ShoppingCartItems)
                            {
                                if (cartItem.Product.PortalID != ZNodeConfigManager.SiteConfig.PortalID)
                                {
                                    ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                                    item.Product = cartItem.Product;
                                    productName.Append(item.Product.Name.ToString());
                                    productName.Append(" ,");
                                }
                            }
                            productName = productName.Remove(productName.Length - 1, 1);
                        }

                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("General Checkout Exception - UnableToProcessRequest : Users purchasing items from multiple vendors, but the site is not configured for Credit Card payment");

                        lblError.Text = "The following product above have not been set up correctly by the vendor : " + productName + "<br>" + this.GetLocalResourceObject("NotSupported").ToString();
                        return;
                    }
                }
            }

            this.SetPaymentControl();
        }

        /// <summary>
        /// Shows the appropriate payment control based on the option selected
        /// </summary>
        public void SetPaymentControl()
        {
            PaymentSettingService _pmtServ = new PaymentSettingService();
            ZNode.Libraries.DataAccess.Entities.PaymentSetting pmtSetting = _pmtServ.DeepLoadByPaymentSettingID(int.Parse(lstPaymentType.SelectedValue), true, DeepLoadType.IncludeChildren, typeof(ZNode.Libraries.DataAccess.Entities.PaymentType));

            // Hide Check here to save... checkbox. This will be set to true if supported below.
            chkSaveCardData.Visible = false;

            if (!string.IsNullOrEmpty(lstPaymentType.SelectedValue) && lstPaymentType.SelectedItem.Text.Contains("Use card ending in"))
            {
                // Hide purchase order panel
                pnlPurchaseOrder.Visible = false;

                // Show credit card panel
                pnlCreditCard.Visible = false;

                // Show Remove Credit Card link button
                lnkRemoveCardData.Visible = true;

                // Uncheck the Save Credit Card data
                chkSaveCardData.Checked = false;

                // Remove the Purchase Order number
                txtPONumber.Text = string.Empty;

                //Znode Version 7.2.2
                // Hide PayPal panel - Start
                pnlPaypal.Visible = false;
                // Hide PayPal panel - End

                return;
            }

            int paymentTypeId = pmtSetting.PaymentTypeID;

            if (paymentTypeId == (int)EnumPaymentType.CREDIT_CARD)
            {
                // Show credit card panel
                pnlCreditCard.Visible = true;

                //Znode Version 7.2.2
                // Show PayPal panel if payment setting of PayPal is ACtive - Start
                if (this.IsPayPalActive((int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL))
                {
                    pnlPaypal.Visible = true;
                }
                // Show PayPal panel if payment setting of PayPal is ACtive - End

                if (pmtSetting.GatewayTypeID == (int)GatewayType.VERISIGN)
                {
                    chkSaveCardData.Visible = true;
                }

                // Hide purchase order panel
                pnlPurchaseOrder.Visible = false;

                // Hide Remove Credit Card link button
                lnkRemoveCardData.Visible = false;

                // Uncheck the Save Credit Card data
                chkSaveCardData.Checked = false;
            }
            else if (paymentTypeId == (int)EnumPaymentType.PURCHASE_ORDER)
            {
                // Hide credit card panel
                pnlCreditCard.Visible = false;

                // Show purchase order panel
                pnlPurchaseOrder.Visible = true;

                // Hide Remove Credit Card link button
                lnkRemoveCardData.Visible = false;

                // Uncheck the Save Credit Card data
                chkSaveCardData.Checked = false;

                //Znode Version 7.2.2
                // Hide PayPal panel - Start
                pnlPaypal.Visible = false;
                // Hide PayPal panel - End
            }
            else if (paymentTypeId == (int)EnumPaymentType.COD)
            {
                // Hide credit card panel
                pnlCreditCard.Visible = false;

                // Hide purchase order panel
                pnlPurchaseOrder.Visible = false;

                // Hide Remove Credit Card link button
                lnkRemoveCardData.Visible = false;

                // Uncheck the Save Credit Card data
                chkSaveCardData.Checked = false;

                //Znode Version 7.2.2
                // Hide PayPal panel - Start
                pnlPaypal.Visible = false;
                // Hide PayPal panel - End
            }
        }

        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Payment type changed event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetPaymentControl();
        }

        /// <summary>
        /// Raises the edit address event to the parent control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LnkEditBilling_Click(object sender, EventArgs e)
        {
            if (this.EditAddressClicked != null)
            {
                this.EditAddressClicked(sender, e);
            }
        }

        /// <summary>
        /// Raises the edit address event to the parent control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LnkEditShipping_Click(object sender, EventArgs e)
        {
            if (this.EditAddressClicked != null)
            {
                this.EditAddressClicked(sender, e);
            }
        }

        /// <summary>
        /// Raises the edit address event to the parent control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void lnkMultipleShipping_Click(object sender, EventArgs e)
        {
            if (this.MultipleAddressClicked != null)
            {
                this.MultipleAddressClicked(sender, e);
            }
        }

        /// <summary>
        /// Paypal click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Paypal_Click(object sender, ImageClickEventArgs e)
        {
            if (this.PayPalClicked != null)
            {
                Session["PaymentType"] = (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL;
                this.PayPalClicked(sender, e);
                Session["PaymentType"] = null;
            }
        }
        /// <summary>
        /// Google Submitbutton click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GoogleSubmitButton_Click(object sender, ImageClickEventArgs e)
        {
            if (this.GoogleCheckoutClicked != null)
            {
                Session["PaymentType"] = (int)ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT;
                this.GoogleCheckoutClicked(sender, e);

                Session["PaymentType"] = null;
            }
        }

        /// <summary>
        /// Raises the edit address event to the parent control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void TwoCheckoutSubmitButton_Click(object sender, EventArgs e)
        {
            if (this.TwoCheckoutClicked != null)
            {
                Session["PaymentType"] = (int)ZNode.Libraries.ECommerce.Entities.PaymentType.TwoCO;
                this.TwoCheckoutClicked(sender, e);
                Session["PaymentType"] = null;
            }
        }

        /// <summary>
        /// To remove the saved credit card info
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LnkRemoveCardData_Click(object sender, EventArgs e)
        {
            this.Payment.DeleteGatewayToken(Convert.ToInt32(lstPaymentType.SelectedValue));
            lstPaymentType.Items.Clear();
            this.BindPaymentTypeData();
        }

        /// <summary>
        /// Event raised when AddGiftCard button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnApplyGiftCard_Click(object sender, EventArgs e)
        {
            var znodePortalCart = this.shoppingCart.PortalCarts.FirstOrDefault();
            if (znodePortalCart == null) return;

            var giftCardAdded = znodePortalCart.AddGiftCard(txtGiftCardNumber.Text);

            if (giftCardAdded)
            {
                ZNodeShoppingCart.CurrentShoppingCart().IsGiftCardApplied = znodePortalCart.IsGiftCardApplied;
                ZNodeShoppingCart.CurrentShoppingCart().IsGiftCardValid = znodePortalCart.IsGiftCardValid;
            }

            txtGiftCardNumber.Text = znodePortalCart.GiftCardNumber;
            lblGiftCardMessage.Text = znodePortalCart.GiftCardMessage;
        }

        #endregion

        #region Page_Load & Page_PreRender

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (this.shoppingCart.PortalCarts.Any(x => x.IsGiftCardApplied) && this.shoppingCart.PortalCarts.Any(x => x.IsGiftCardValid))
            {
                pnlPayment.Visible = true;
                pnlGiftCardNumber.Visible = true;
            }

            if (this.shoppingCart.PortalCarts.Sum(x => x.Total) <= 0)
            {
                pnlCreditCard.Visible = false;
                pnlCOD.Visible = false;
                pnlIPCCreditCard.Visible = false;
                pnlPurchaseOrder.Visible = false;
                PaymentTypePanel.Visible = false;
                pnl2CO.Visible = false;
            }
            else
            {
                PaymentTypePanel.Visible = true;
                this.BindPaymentTypeData();
            }

            if (this.shoppingCart.PortalCarts.Any(x => x.IsGiftCardApplied) && this.shoppingCart.PortalCarts.Sum(x => x.Total) == 0)
            {
                pnlPayment.Visible = true;
                pnlGoogleCheckout.Visible = false;
                pnlPaypal.Visible = false;
                pnl2CO.Visible = false;
                pnlGiftCardNumber.Visible = true;
            }

            if (shoppingCart.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().Sum(x => x.Quantity) > 1)
            {
                if (ZNodeShoppingCart.CurrentShoppingCart().IsMultipleShipToAddress)
                {
                    lnkMultipleShipping.Visible = false;
                }
                else
                {
                    lnkMultipleShipping.Visible = true;
                }
            }

            if (this._userAccount == null || !this._userAccount.UserID.HasValue)
            {
                lnkMultipleShipping.Visible = false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (lstPaymentType.Items.Count == 0)
            {
                this.BindPaymentTypeData();
            }

            if (lstYear.Items.Count < 1)
            {
                this.BindYearList();
            }

            // Set Image Path
            imgAmex.Src = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/card_amex.gif";
            imgDiscover.Src = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/card_discover.gif";
            imgMastercard.Src = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/card_mastercard.gif";
            imgVisa.Src = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/card_visa.gif";

            CVVhyperlink.Attributes.Add("OnClientClick", this.GetCvvPath() + "return false");

            if (IsPostBack)
            {
                if (this.PaymentType == EnumPaymentType.CREDIT_CARD)
                {
                    pnlCreditCard.Visible = true;
                    Localize3.Visible = true;
                    txtCreditCardNumber.Visible = true;
                    Localize4.Visible = true;
                    lstMonth.Visible = true;
                    lstYear.Visible = true;
                    Localize6.Visible = true;
                    txtCVV.Visible = true;
                    CVVhyperlink.Visible = true;
                }

                if (!string.IsNullOrEmpty(lstPaymentType.SelectedValue) && lstPaymentType.SelectedItem.Text.Contains("Use card ending in"))
                {
                    pnlPurchaseOrder.Visible = false;
                    pnlCreditCard.Visible = false;
                    lnkRemoveCardData.Visible = true;
                    chkSaveCardData.Checked = false;
                    txtPONumber.Text = string.Empty;

                    return;
                }
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Binds the expiration year list based on current year
        /// </summary>
        private void BindYearList()
        {
            ListItem defaultItem = new ListItem("-- Year --", "0");
            lstYear.Items.Add(defaultItem);
            defaultItem.Selected = true;

            int currentYear = System.DateTime.Now.Year;
            int counter = 25;

            do
            {
                string itemtext = currentYear.ToString();

                lstYear.Items.Add(new ListItem(itemtext));

                currentYear = currentYear + 1;

                counter = counter - 1;
            }
            while (counter > 0);
        }

        /// <summary>
        /// Returns an instance of the PaymentSetting  class.
        /// </summary>
        /// <param name="paymentTypeID">Payment Type ID</param>
        /// <returns>Returns the Payment Setting</returns>
        private PaymentSetting GetByPaymentTypeId(int paymentTypeID)
        {
            PaymentSettingService _pmtServ = new PaymentSettingService();
            int? profileID = 0;
            if (this._userAccount != null)
            {
                // Get loggedIn user profile
                profileID = this._userAccount.ProfileID;
            }

            TList<PaymentSetting> list = _pmtServ.GetByPaymentTypeID(paymentTypeID);
            list.Filter = "ActiveInd = true AND ProfileID = " + profileID;

            if (list.Count == 0)
            {
                // Get All Profiles payment setting
                list.Filter = "ActiveInd = true";
            }

            return list[0];
        }

        /// <summary>
        /// Znode Version V.2.2
        /// To check PayPal Payment setting are active or Not for user.
        /// </summary>
        /// <param name="paymentTypeID">paymentTypeID for checking PayPal Account is Active. </param>
        /// <returns>PayPal Account Active status.</returns>
        private bool IsPayPalActive(int paymentTypeID)
        {
            PaymentSettingService _pmtServ = new PaymentSettingService();
            int? profileID = 0;
          
            if (this._userAccount != null)
            {
                // Get loggedIn user profile
                profileID = this._userAccount.ProfileID;
            }

            TList<PaymentSetting> list = _pmtServ.GetByPaymentTypeID(paymentTypeID);
            list.Filter = "ActiveInd = true AND ProfileID = " + profileID;

            if (list.Count == 0)
            {
                return false;
            }

            return true;
        }


        #endregion
    }
}