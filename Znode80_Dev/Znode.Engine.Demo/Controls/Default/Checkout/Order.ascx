<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Checkout_Order" CodeBehind="Order.ascx.cs" %>
<div class="ShoppingCart Form">
    <div class="HeaderStyle">
        <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtOrderInformation"></asp:Localize>
    </div>
    <div>
        <asp:Repeater ID="rptShiping" runat="server" OnItemDataBound="rptShiping_ItemDataBound">
            <ItemTemplate>
                <asp:Label runat="server" ID="lblErrorMessage" CssClass="Error"></asp:Label>
                <asp:GridView ID="uxCart" runat="server" AutoGenerateColumns="False" EmptyDataText="Shopping Cart Empty"
                    GridLines="None" CellPadding="4" CssClass="Grid" ShowHeader="false"
                    meta:resourcekey="uxCartResource1" OnRowCommand="UxCart_RowCommand">
                    <Columns>
                        <asp:TemplateField HeaderText="QTY" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="70px"
                            meta:resourcekey="TemplateFieldResource1">
                            <ItemTemplate>
                                <div>
                                    <%# DataBinder.Eval(Container.DataItem, "Quantity") %>
                                </div>
                                <div class="Mylink">
                                    <asp:LinkButton ID="ibRemoveLineItem" runat="server"
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "GUID").ToString() + "|" + DataBinder.Eval(((RepeaterItem)((GridView)((GridViewRow)Container).NamingContainer).Parent).DataItem, "AddressID").ToString() %>'
                                        CommandName="remove" Text="Remove" ToolTip="Remove this item" CausesValidation="false" />
                                </div>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="70px">
                            <ItemTemplate>
                                <a enableviewstate="false" id="A1" href='<%# GetValue(DataBinder.Eval(Container.DataItem, "GUID").ToString(),"ViewProductLink")  %>'
                                    runat="server">
                                    <img id="Img1" enableviewstate="false" alt='<%# GetValue(DataBinder.Eval(Container.DataItem, "GUID").ToString(),"ImageAltTag")%>'
                                        border='0' src='<%# GetValue(DataBinder.Eval(Container.DataItem, "GUID").ToString(),"ThumbnailImageFilePath") %>'
                                        runat="server" />
                                </a>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle CssClass="QTY" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ITEM" HeaderStyle-HorizontalAlign="Left"
                            meta:resourcekey="TemplateFieldResource2" ItemStyle-Width="350px">
                            <ItemTemplate>
                                <div class="ProductName">
                                    <%# DataBinder.Eval(Container.DataItem, "Product.Name").ToString()%>
                                </div>
                                <div class="Description">
                                    <div>
                                        Item#
                                <%# DataBinder.Eval(Container.DataItem, "Product.ProductNum").ToString()%>
                                    </div>
                                    <div>
                                        <%# DataBinder.Eval(Container.DataItem, "Product.ShoppingCartDescription").ToString()%>
                                    </div>
                                </div>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="PRICE" HeaderStyle-HorizontalAlign="Left"
                            meta:resourcekey="TemplateFieldResource3" ItemStyle-Width="100px">
                            <ItemTemplate>
                                <div class="Description">
                                    <%# DataBinder.Eval(Container.DataItem, "UnitPrice", "{0:c}") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()%>
                                </div>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TOTAL" HeaderStyle-HorizontalAlign="Right"
                            ItemStyle-HorizontalAlign="Right" meta:resourcekey="TemplateFieldResource4" ItemStyle-Width="100px">
                            <ItemTemplate>
                                <div class="Description">
                                    <%# (GetQuantity(Container.DataItem, (int)DataBinder.Eval(((GridViewRow)Container).NamingContainer.Parent, "DataItem.AddressId")) * (decimal)DataBinder.Eval(Container.DataItem, "UnitPrice")).ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()%>
                                </div>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Right"></ItemStyle>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="Footer" />
                    <RowStyle CssClass="Row" />
                    <HeaderStyle CssClass="Header" />
                    <AlternatingRowStyle CssClass="AlternatingRow" />
                </asp:GridView>
                <div>
                    <div class="ShoppingCart Form" id="divMultipleShipping">
                        <div class="ShippingSection">
                            <div>
								<div class="ShipAddressSection">
									<div class="ShipToText">
										&nbsp;
									</div>
									<div id="div1" style="width: 10%; float: left;" runat="server">
										&nbsp;
									</div>
									<div class="SubtotalText">
										<asp:Label ID="lblSubtotalText" runat="server" EnableViewState="False" Text="Subtotal" Visible="false"></asp:Label>
										<div class="ShippingCostText">
											<asp:Label ID="lblSubtotal" runat="server" EnableViewState="False"
												Visible="false"></asp:Label>
										</div>
									</div>
								</div>

								<div class="ShipAddressSection">
									<div class="ShipBy" id="ShipSection" runat="server">
										<b>
											<asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtShipBy"></asp:Localize></b>
										<span>
											<asp:DropDownList ID="lstShipping" runat="server" AutoPostBack="True"
												OnSelectedIndexChanged="LstShipping_SelectedIndexChanged"
												meta:resourcekey="lstShippingResource1">
											</asp:DropDownList></span>
										<div class="Error">
											<asp:Literal ID="uxErrorMsg" EnableViewState="False" runat="server"
												meta:resourcekey="uxErrorMsgResource1"></asp:Literal>
										</div>
									</div>
									<div id="div2" style="width: 10%; float: left;" runat="server">
									
									</div>
									<div class="ShipText">
										<asp:Label ID="lblTaxCostText" runat="server" EnableViewState="False" Text="Tax" Visible="false"></asp:Label>
										<div class="ShippingCostText">
											<asp:Label ID="lblTaxCost" runat="server" EnableViewState="False" Visible="false"></asp:Label>
										</div>
									</div>
								</div>
                              
                                <div class="ShipAddressSection">
                                    <div class="ShipToText">
                                        <div id="ShipAddressSection" runat="server">
                                            <b>
                                                <asp:Label ID="lblShippto" runat="server" EnableViewState="False" Text="Ship To" Visible="false" ForeColor="#494949"></asp:Label></b>
                                            <asp:Label ID="lblShippingAddress" runat="server" Text='<%#GetAddress(DataBinder.Eval(Container.DataItem, "AddressID").ToString())%>' Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                    <div id="divChange" style="width: 10%; float: left;" runat="server">
                                        <div class="Mylink">
                                            <a id="lnkChange" runat="server">Edit</a>
                                        </div>
                                    </div>
                                    <div class="ShipText">
                                        <asp:Label ID="lblShippingText" runat="server" EnableViewState="False" Text="Shipping" Visible="false"></asp:Label>
                                        <div style="float: right;">
                                            <asp:Label ID="lblShipping" runat="server" EnableViewState="False"
                                                meta:resourcekey="ShippingResource2" Visible="false"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="clear: both;"></div>
                <hr style="background-color: #808080;" />
            </ItemTemplate>
        </asp:Repeater>
        <div style="clear: both;"></div>
    </div>
    <div>
        <div class="ShoppingCart Form">
            <div class="TableContainer">
                <div class="RightContent" align="right">
                    <div class="TotalBox">
                        <br />
                        <div class="Row Clear" style="height: 10px;">
                            <div class="LeftContent">
                                <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtsubtotal"></asp:Localize>
                            </div>
                            <div class="LeftContent FieldValue">
                                <asp:Label ID="SubTotal" runat="server" EnableViewState="False"
                                    meta:resourcekey="SubTotalResource1"></asp:Label>
                            </div>
                        </div>
                        <div class="Row Clear" runat="server" id="rowDiscount" visible="false" style="height: 10px;">
                            <div class="LeftContent">
                                <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtDiscount"></asp:Localize>
                            </div>
                            <div class="LeftContent FieldValue">
                                <asp:Label ID="Discount" runat="server" EnableViewState="False"
                                    meta:resourcekey="DiscountResource1"></asp:Label>
                            </div>
                        </div>
                        <div class="Row Clear" runat="server" id="rowTax" visible="false" style="height: 10px;">
                            <div class="LeftContent">
                                <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtTax"></asp:Localize>
                                <asp:Label ID="TaxPct" runat="server" EnableViewState="False"
                                    meta:resourcekey="TaxPctResource1"></asp:Label>
                            </div>
                            <div class="LeftContent FieldValue">
                                <asp:Label ID="Tax" runat="server" EnableViewState="False"
                                    meta:resourcekey="TaxResource1"></asp:Label>
                            </div>
                        </div>
                        <div class="Row Clear" runat="server" id="rowTotalShipping" enableviewstate="false" visible="true" style="height: 10px;">
                            <div class="LeftContent">
                                <asp:Localize ID="Localize6" runat="server" meta:resourceKey="txtShipping"></asp:Localize>
                            </div>
                            <div class="LeftContent FieldValue">
                                <asp:Label ID="lblTotalShipping" runat="server" EnableViewState="False"
                                    meta:resourcekey="ShippingResource2"></asp:Label>
                            </div>
                        </div>
                        <div class="Row Clear" runat="server" id="divGiftCardAmount" visible="false" style="height: 10px;">
                            <div class="LeftContent">
                                <asp:Localize ID="Localize7" runat="server" meta:resourceKey="lblGiftCard"></asp:Localize>
                            </div>
                            <div class="LeftContent FieldValue">
                                <asp:Label ID="lblGiftCardAmount" runat="server" EnableViewState="False"></asp:Label>
                            </div>
                        </div>
                        <div class="Row Clear bold" style="height: 10px;">
                            <div class="LeftContent">
                                <b>
                                    <asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:CommonCaption, Total%>" /></b>
                            </div>
                            <div class="LeftContent FieldValue">
                                <b>
                                    <asp:Label ID="Total" runat="server" EnableViewState="False"
                                        meta:resourcekey="TotalResource2"></asp:Label></b>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="Clear"></div>
            </div>
        </div>
    </div>
</div>