﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultipleAddressCart.ascx.cs" Inherits="Znode.Engine.Demo.Controls_Default_Checkout_MultipleAddressCart" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/common/spacer.ascx" %>

<div>
    <asp:Repeater ID="rptCartItems" runat="server" OnItemDataBound="rptCartItems_ItemDataBound">
        <HeaderTemplate>
            <div class="MultiShipHeader">
                <div class="QTY">
                    QTY
                </div>
                <div class="PdtImage">
                    Product
                </div>
                <div class="Product">
                    &nbsp;
                </div>
                <div class="ShipTo">Ship To:</div>
            </div>
            <div style="clear:both;"></div>
        </HeaderTemplate>
        <ItemTemplate>
            <asp:GridView class="TableContainer" ID="uxCart" runat="server" AutoGenerateColumns="False"
                EmptyDataText="Shopping Cart Empty" GridLines="None" CellPadding="4" OnRowCommand="UxCart_RowCommand"
                OnRowDataBound="UxCart_RowDataBound" CssClass="Grid" ShowHeader="false">
                <Columns>
                    <asp:TemplateField HeaderText="QTY" HeaderStyle-HorizontalAlign="Left"  ItemStyle-Width="120px">
                        <ItemTemplate>
                            <asp:DropDownList ID="uxQty" runat="server" >
                            </asp:DropDownList>
                            <asp:HiddenField ID="GUID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ItemGUID").ToString() %>' />
                            <asp:HiddenField ID="SLNO" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "SlNo").ToString() %>' />
                            <div><uc1:Spacer ID="Spacer100" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer></div>
                            <div class="Mylink">
                                <asp:LinkButton ID="ibRemoveLineItem" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SlNo").ToString() %>'
                                    CommandName="remove" Text="Remove" ToolTip="Remove this item" />
                            </div>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100px">
                        <ItemTemplate>
                            <a enableviewstate="false" id="A1" href='<%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"ViewProductLink")  %>'
                                runat="server">
                                <img id="Img1" enableviewstate="false" alt='<%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"ImageAltTag")%>'
                                    border='0' src='<%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"ThumbnailImageFilePath") %>'
                                    runat="server" />
                            </a>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle CssClass="QTY" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="350px">
                        <ItemTemplate>
                            <div>
                                <a enableviewstate="false" id="A2" href='<%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"ViewProductLink") %>'
                                    runat="server">
                                    <%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"Name")%></a>
                            </div>
                            <div class="Description" enableviewstate="false">
                                Item#
                        <%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"ProductNum")%><br>
                                <%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"ShoppingCartDescription")%>
                            </div>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ship to" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlShippingAddress" runat="server"></asp:DropDownList>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    </asp:TemplateField>

                </Columns>
                <FooterStyle CssClass="Footer" />
                <RowStyle CssClass="Row" />
                <HeaderStyle CssClass="Header" />
                <AlternatingRowStyle CssClass="AlternatingRow" />
            </asp:GridView>
        </ItemTemplate>
    </asp:Repeater>
    <uc1:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    <div class="Error">
        <asp:Label ID="lblError" runat="server"></asp:Label>
    </div>
    <div class="UpdatedQuantity">
        Changed Quantities ?          
            <asp:LinkButton ID="lnkUpdate" runat="server" AlternateText="Update" CssClass="Button" OnClick="lnkUpdate_Click">Update</asp:LinkButton>
        <br />
    </div>
</div>

