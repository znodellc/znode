using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Xsl;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.DataAccess.Entities;
using Znode.Engine.Suppliers;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Order Receipt user control.
    /// </summary>
    public partial class Controls_Default_Checkout_OrderReceipt : System.Web.UI.UserControl
    {
        #region Private Variables
        private string _siteName = string.Empty;
        private string _receiptText = string.Empty;
        private ZNodeOrderFulfillment _Order = null;
        private List<ZNodeOrderFulfillment> _Orders = new List<ZNodeOrderFulfillment>();
        private string _customerServiceEmail = string.Empty;
        private string _customerServicePhoneNumber = string.Empty;
        private string _ReceiptTemplate = string.Empty;

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the receipt template.
        /// </summary>
        public string ReceiptTemplate
        {
            get { return _ReceiptTemplate; }
            set { _ReceiptTemplate = value; }
        }

        /// <summary>
        ///  Gets or sets the ZNodeOrderFulfillment object
        /// </summary>
        public ZNodeOrderFulfillment Order
        {
            get
            {
                return this._Order;
            }

            set
            {
                this._Order = value;
            }
        }

        public string SiteName
        {
            get
            {
                return this._siteName;
            }

            set
            {
                this._siteName = value;
            }
        }

        public string ReceiptText
        {
            get
            {
                return this._receiptText;
            }

            set
            {
                this._receiptText = value;
            }
        }

        public string CustomerServiceEmail
        {
            get
            {
                return this._customerServiceEmail;
            }

            set
            {
                this._customerServiceEmail = value;
            }
        }

        public string CustomerServicePhoneNumber
        {
            get
            {
                return this._customerServicePhoneNumber;
            }

            set
            {
                this._customerServicePhoneNumber = value;
            }
        }

        /// <summary>
        /// Gets the customer feedback url
        /// </summary>
        public string FeedBackUrl
        {
            get
            {
                StringBuilder url = new StringBuilder();
                url.Append(Request.Url.GetLeftPart(UriPartial.Authority));
                if (Request.ApplicationPath.EndsWith("/"))
                {
                    url.Append(Request.ApplicationPath);
                }
                else
                {
                    url.Append(Request.ApplicationPath).Append("/");
                }

                url.Append("CustomerFeedback.aspx");

				if (HttpContext.GetGlobalResourceObject("CommonCaption", "FeedbackURL") == null)
				{
					return "<a href=" + url.ToString() + ">Click here to give us feedback on your ordering experience.</a>";
				}
				else
				{
					return "<a href=" + url.ToString() + ">"+ HttpContext.GetGlobalResourceObject("CommonCaption", "FeedbackURL") + "</a>";
				}
            }
        }

        /// <summary>
        /// Gets the current order Id.
        /// </summary>
        public string OrderID
        {
            get { return this._Order.OrderID.ToString(); }
        }

        /// <summary>
        /// Gets the order total amount.
        /// </summary>
        public string OrderTotal
        {
            get { return this._Order.Total.ToString("N2"); }
        }

        /// <summary>
        /// Gets the order shipping total amount.
        /// </summary>
        public string ShippingTotal
        {
            get { return this._Order.ShippingCost.ToString("N2"); }
        }

        /// <summary>
        /// Gets the order tax total amount.
        /// </summary>
        public string TaxTotal
        {
            get { return this._Order.SalesTax.ToString("N2"); }
        }

        /// <summary>
        /// Gets tht listrak browser fingerprint image url.
        /// </summary>
        public string BrowserFingerprintImageUrl
        {
            get
            {
                ZNodeAnalytics analytics = new ZNodeAnalytics();
                analytics.Bind();
                return analytics.AnalyticsData.BrowserFingerprintImageUrl;
            }
        }

        #endregion

        #region Page Events

        /// <summary>
        ///  Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (Session["OrderDetail"] is List<ZNodeOrderFulfillment>)
            {
                this._Orders = Session["OrderDetail"] as List<ZNodeOrderFulfillment>;
            }
            else if (Session["OrderDetail"] is ZNodeOrderFulfillment)
            {
                this._Order = Session["OrderDetail"] as ZNodeOrderFulfillment;
            }

            if (this._Order == null && this._Orders.Count > 0)
            {
                this._Order = this._Orders[0];
            }

            if (this._Order != null)
            {
                // Override the default page analytics so we can include the Receipt Page specific info.
                ZNodeAnalytics analytics = new ZNodeAnalytics();
                analytics.AnalyticsData.IsOrderReceiptPage = true;
                analytics.AnalyticsData.Order = this._Order;
                analytics.Bind();
            }
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
			
        }

		/// <summary>
		/// Page PreRender Event
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void Page_PreRender(object sender, EventArgs e)
		{
			if (Session["OrderDetail"] == null)
			{
				Response.Redirect("~/default.aspx");
			}

			StringBuilder receiptTemplates = new StringBuilder();
			foreach (ZNodeOrderFulfillment order in this._Orders)
			{
				// Generate dynamic receipt
				ZnodeReceipt receipt = new ZnodeReceipt(order, this.FeedBackUrl);
				receiptTemplates.Append(receipt.GetHtmlReceiptForUI());
				receiptTemplates.Append("<br />");
			}

			this._ReceiptTemplate = receiptTemplates.ToString();
			//Store Session values before Abandon
			ZNodeUserAccount useracct = (ZNodeUserAccount)Session[ZNodeSessionKeyType.UserAccount.ToString()];
			Account account = (Account)Session["AccountObject"];
			Profile profile = (Profile)Session["ProfileCache"];

			// Empty cart 
			Session.Abandon();

			//Restore Session values
			Session.Add(ZNodeSessionKeyType.UserAccount.ToString(), useracct);
			HttpContext.Current.Session["ProfileCache"] = profile;
			Session.Add("AccountObject", account);

            //Znode Version 7.2.2
            //Change Description - Start 
            //Remove Shopping Cart details from session. & Set the ItemCartCount value to 0 after the order placed.

            Session.Remove(ZNodeSessionKeyType.ShoppingCart.ToString());
            UserControl cartItemCount = (UserControl)this.Page.Master.Master.FindControl("CART_ITEM_COUNT");
            Label lblCartCount = (Label)cartItemCount.FindControl("lblCartItemCount");
            lblCartCount.Text = "0";
            //Change Description - End 

			// Empty cart & logoff user
			//Session.Abandon();
			//FormsAuthentication.SignOut();

			if (this.Page.Title != null)
			{
				ZNodeResourceManager resourceManager = new ZNodeResourceManager();
				this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "PageTitle");
			}
            
		}
        #endregion
    }
}