using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Suppliers;
using EnumPaymentType = ZNode.Libraries.ECommerce.Entities.PaymentType;


namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Checkout user control class.
    /// </summary>
    public partial class Controls_Default_Checkout_Checkout : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeCheckout _checkout;
        private PaymentSetting _paymentSetting;
        private ZNodeShoppingCart _shoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        private ZNodeUserAccount _userAccount;
        private DateTime currentdate = System.DateTime.Now.Date;
        private TList<PaymentSetting> _pmtSetting;
        #endregion

        #region Page Load

        /// <summary>
        /// Bind the submit button javascript code.
        /// </summary>
        /// <param name="btnSubmit">Submit button control.</param>
        /// <param name="lblPaymentResponse">Label control that contains error message.</param>        
        public void BindSubmitButtonScript(LinkButton btnSubmit)
        {
            StringBuilder script = new StringBuilder();

            script.Append("if (typeof(Page_ClientValidate) == 'function') { ");
            script.Append("if (Page_ClientValidate('" + btnSubmit.ValidationGroup + "') == false) { return false; }} ");

            // Clear the error message if any
            script.Append("document.getElementById('" + btnSubmit.ClientID + "').style.display='none';");
            script.Append("document.getElementById('" + btnSubmit.ClientID + "').parentElement.innerHTML += '<font size=4px>Processing...</font>';");

            // Bind the button submit event
            Page page = HttpContext.Current.Handler as Page;
            script.Append(page.ClientScript.GetPostBackEventReference(btnSubmit, string.Empty));

            script.Append("return false;");

            // Attach the disable script event to Submit button control
            SubmitButton.Attributes.Add("onclick", script.ToString());
        }

        /// <summary>
        ///  Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (this._shoppingCart == null)
            {
                Response.Redirect("~/shoppingcart.aspx");
                return;
            }

            // get objects from session
            this._checkout = new ZNodeCheckout();

            if (this._checkout.UserAccount != null)
            {
                // Add the checkout page listrack tracking script.
                ZNodeAnalytics analytics = new ZNodeAnalytics();
                analytics.AnalyticsData.IsCheckoutPage = true;
                analytics.AnalyticsData.UserAccount = this._checkout.UserAccount;
                analytics.Bind();
            }
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            uxStepTracker.Step = 2;

            if (!this.IsPostBack)
            {
                BindSubmitButtonScript(SubmitButton);
            }

            if (Session["IsAddressValidated"] == null || Convert.ToBoolean(Session["IsAddressValidated"]) == false)
            {
                // User skipped the address validation by type url in address bar. Redirect to the address page.
                Response.Redirect("~/EditAddress.aspx");
            }

            // registers the event for the payment (child) control
            this.uxPayment.EditAddressClicked += new EventHandler(this.UxPayment_EditAddressClicked);
            this.uxPayment.MultipleAddressClicked += new EventHandler(this.UxPayment_MultipleAddressClicked);
            this.uxPayment.PayPalClicked += new EventHandler(this.UxPayment_PayPalClicked);
            this.uxPayment.GoogleCheckoutClicked += new EventHandler(this.UxPayment_GoogleCheckoutClicked);
            this.uxPayment.TwoCheckoutClicked += new EventHandler(this.UxPayment_2COClicked);

            try
            {
                int profileID = 0;

                // Check user Session
                if (this._checkout.UserAccount != null)
                {
                    profileID = ZNodeProfile.CurrentUserProfileId;
                }
                else
                {
                    // Get Default Registered profileId
                    profileID = ZNodeProfile.DefaultRegisteredProfileId;
                }

                if (profileID > 0)
                {
                    PaymentSettingService _pmtServ = new PaymentSettingService();
                    _pmtSetting = _pmtServ.GetAll();

                    var hasPayments = _shoppingCart.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().Select(x => x.Product.PortalID).Distinct().All(x =>
                        {
                            int total = 0;
                            var isDefaultPortal = !ZNode.Libraries.DataAccess.Data
                                 .DataRepository
                                 .CategoryProvider
                                 .GetByPortalID(
                                     x, 0, 1,
                                     out total).Any();

                            PortalService portalService = new PortalService();
                            Portal otherFranchisePortal = portalService.GetByPortalID(x);
                            profileID = otherFranchisePortal.DefaultRegisteredProfileID.GetValueOrDefault(0);

                            return
                                _pmtSetting.Any(
                                    pmt =>
                                    ((pmt.ProfileID == null && isDefaultPortal) || pmt.ProfileID == profileID) && pmt.ActiveInd == true);
                        });

                    // check whether this profile has payment options
                    if (!hasPayments)
                    {
                        Response.Redirect("~/shoppingcart.aspx?ReturnURL=checkout.aspx&ErrorMsg=paymenterror");
                        return;
                    }
                }
                else
                {
                    Response.Redirect("~/shoppingcart.aspx?ReturnURL=checkout.aspx&ErrorMsg=1");
                    return;
                }
            }
            catch (Exception exception)
            {
                throw new ApplicationException("No payment options have been setup for this profile in the database. Please use the admin to setup valid payment options first. Additional Information: " + exception.Message);
            }

            // check if shopping cart is empty
            if (this._checkout.ShoppingCart == null)
            {
                Response.Redirect("~/shoppingcart.aspx");
                return;
            }
            else
            {
                if (this._checkout.ShoppingCart.Count == 0)
                {
                    throw new ApplicationException("Checkout failed since there are no items in the shopping cart.");
                }

                if (!Page.IsPostBack)
                {
                    // Display promotion description and error message
                    this.Calculate();
                }
            }

            this.BindCartItems();

           // this.GetControlData();

            if (this.Page.Title != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "PageTitle");
            }
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Represents the GetImageFilePath function
        /// </summary>    
        /// <returns>Returns the Image File Path</returns>
        protected string GetImageFilePath()
        {
            return ResolveUrl(ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("submit_arrow.gif"));
        }

        /// <summary>
        /// Bind data to payment control
        /// </summary>
        protected void BindPaymentControlData()
        {
            ZNodePayment Payment = new ZNodePayment();
            Payment.BillingAddress = this._checkout.UserAccount.BillingAddress;
            Payment.ShippingAddress = this._checkout.UserAccount.ShippingAddress;
            uxPayment.Payment = Payment;
            this._checkout.ShoppingCart.Payment = (ZNodePayment)Payment;
        }

        /// <summary>
        /// Refreshes all the data in the checkout object using the data collected from the forms
        /// </summary>
        protected void GetControlData()
        {
            this._checkout.ShoppingCart.Payment = uxPayment.Payment;
            this._checkout.ShoppingCart.Payment.BillingAddress = this._checkout.UserAccount.BillingAddress;
            this._checkout.ShoppingCart.Payment.ShippingAddress = this._checkout.UserAccount.ShippingAddress;
            this._checkout.AdditionalInstructions = uxPayment.AdditionalInstructions;
            this._checkout.UserAccount.BillingAddress = this._checkout.UserAccount.BillingAddress;
            this._checkout.UserAccount.ShippingAddress = this._checkout.UserAccount.ShippingAddress;
            this._checkout.ShoppingCart.Payment.SaveCardData = uxPayment.SaveCardData;
            this._checkout.PaymentSettingID = uxPayment.PaymentSettingID;
            this._checkout.PurchaseOrderNumber = uxPayment.PurchaseOrderNumber;
            this._checkout.AdditionalInstructions = uxPayment.AdditionalInstructions;
        }

        /// <summary>
        /// Google checkout event raised by payment control. Do the submit order with google checkout payment method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Arguments</param>
        protected void UxPayment_GoogleCheckoutClicked(object sender, EventArgs e)
        {
            this.OnSubmitOrder(sender, e);
        }

        /// <summary>
        /// PayPal clicked event raised by payment control Do the submit order with PayPal payment method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxPayment_PayPalClicked(object sender, EventArgs e)
        {
            this.OnSubmitOrder(sender, e);
        }

        /// <summary>
        /// 2CO clicked event raised by payment control Do the submit order with PayPal payment method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxPayment_2COClicked(object sender, EventArgs e)
        {
            this.On2COCheckout(sender, e);
        }

        /// <summary>
        /// Repeater Control Item Data bound event.
        /// </summary>
        /// <param name="sender">repeater object</param>
        /// <param name="e">event args</param>
        protected void RptCart_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                ZNodeShoppingCartItem item = (ZNodeShoppingCartItem)e.Item.DataItem;
                Controls_Default_Checkout_Order ShopingCart = (Controls_Default_Checkout_Order)e.Item.FindControl("uxOrder");
                ShopingCart.ShippingSelectedIndexChanged += new EventHandler(this.UxOrder_SelectedIndexChanged);
                ShopingCart.RemoveChanged += new EventHandler(UxOrder_RemoveChanged);
                ShopingCart.PortalID = item.Product.PortalID;
                ShopingCart.BindGrid();
                // ShopingCart.BindShipping();
            }
        }

        #endregion

        #region Submit Order & Payment

        /// <summary>
        /// Event triggered when the 2CO checkout button is clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void On2COCheckout(object sender, EventArgs e)
        {
            this.GetControlData();
            string thankyouPage = "PaymentINSHandler.aspx?mode=2co";

            string insURL = Request.Url.GetLeftPart(UriPartial.Authority) + Response.ApplyAppPathModifier(thankyouPage);
            string returnURL = insURL;

            // Get payment setting object for 2co payment type
            PaymentSetting paymentSetting = this.GetByPaymentTypeId((int)ZNode.Libraries.ECommerce.Entities.PaymentType.TwoCO);

            ZNode.Libraries.ECommerce.Payment.Gateway2CO twoCO = new ZNode.Libraries.ECommerce.Payment.Gateway2CO();
            twoCO.SubmitPayment(paymentSetting, returnURL, insURL, _checkout.ShoppingCart);
        }

        /// <summary>
        /// Event triggered when the submit order button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void OnSubmitOrder(object sender, EventArgs e)
        {
            if (this._userAccount == null)
            {
                _userAccount = (ZNodeUserAccount)ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount);
            }
            bool isValidAddress = false;
            isValidAddress = _shoppingCart.IsMultipleShipToAddress ?
                                _userAccount.CheckValidAddress(this._checkout.UserAccount.BillingAddress) &&
                                _shoppingCart.PortalCarts.All(x => x.AddressCarts.All(
                                        y => _userAccount.CheckValidAddress(this._checkout.UserAccount.Addresses.FirstOrDefault(z => z.AddressID == y.AddressID)))) : true;

            if (!isValidAddress)
            {
                // User skipped the address validation by type url in address bar. Redirect to the address page.
                lblError.Text = this.GetLocalResourceObject("AddressValidationFailed").ToString();
                return;
            }

            if (_shoppingCart.PortalCarts.Select(portalcart => portalcart.AddressCarts.Where(x => x.ErrorMessage != string.Empty).Count())
                        .Any(errorMessageCount => errorMessageCount > 0))
            {
                return;
            }

            bool IsDefaultPortal = false;
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();

            // Get vendor specific cart.
            System.Collections.Generic.List<int> portalIds = this.GetShoppingCartVendors();
            List<ZNodeOrderFulfillment> orders = new List<ZNodeOrderFulfillment>();
            foreach (ZNodePortalCart portalCart in this._shoppingCart.PortalCarts)
            {
                int portalId = portalCart.PortalID;
                int? profileID = null;

                // Find is default store, so we include all profiles.
                CategoryService categoryService = new CategoryService();
                int total;
                TList<Category> categoryList = categoryService.GetByPortalID(portalId, 0, 1, out total);
                IsDefaultPortal = categoryList.Count == 0;

                if (portalId == ZNodeConfigManager.SiteConfig.PortalID)
                {
                    profileID = this._checkout.UserAccount.ProfileID;
                }
                else
                {
                    PortalProfileService ps = new PortalProfileService();
                    TList<PortalProfile> portalProfiles = ps.GetByPortalID(portalId);

                    PortalProfile portalProfile = portalProfiles.Find(PortalProfileColumn.ProfileID, this._checkout.UserAccount.ProfileID);

                    if (portalProfile != null)
                    {
                        // Assign profile Id only for franchise store.
                        profileID = this._checkout.UserAccount.ProfileID;
                    }
                    else
                    {
                        PortalService portalService = new PortalService();
                        Portal otherFranchisePortal = portalService.GetByPortalID(portalId);
                        profileID = otherFranchisePortal.DefaultRegisteredProfileID;
                    }
                }

                this._checkout.PortalID = portalId;
                this._checkout.ShoppingCart = portalCart;

                // Get data from user controls
                this.GetControlData();

                // No payment method selected.
                if (this._checkout.PaymentSettingID == 0)
                {
                    lblError.Text = this.GetLocalResourceObject("PaymentNotSelected").ToString();
                    return;
                }

                int paymentTypeID = (int)uxPayment.PaymentType;

                TList<PaymentSetting> paymentSettings = (TList<PaymentSetting>)_pmtSetting.Clone();

                var _profilePayments = paymentSettings.FindAll(pmt => pmt.ProfileID == profileID && pmt.ActiveInd == true);

                var allProfilePayments = paymentSettings.FindAll(pmt => pmt.ProfileID == null && IsDefaultPortal && pmt.ActiveInd == true);

                if (allProfilePayments.Any())
                {
                    _profilePayments.AddRange(allProfilePayments.Where(x => !_profilePayments.Any(y => y.PaymentTypeID == x.PaymentTypeID)).ToList());
                }

                paymentSettings = _profilePayments.FindAll(pmt => pmt.PaymentTypeID == paymentTypeID && pmt.ActiveInd == true);

                if (paymentSettings != null && paymentSettings.Count > 0)
                {
                    this._checkout.ShoppingCart.Payment.PaymentSetting = paymentSettings[0];
                    this._checkout.PaymentSettingID = paymentSettings[0].PaymentSettingID;
                }
                else
                {
                    log.LogActivityTimerEnd(5003, null);

                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("General Checkout Exception - UnableToProcessRequest : Users purchasing items from multiple vendors, but the site is not configured for Credit Card payment");

                    lblError.Text = this.GetLocalResourceObject("MultivendorNotSupported").ToString();
                    return;
                }

                ZNodeOrderFulfillment order = new ZNodeOrderFulfillment();

                try
                {
                    log.LogActivityTimerStart();

                    if (this._checkout.ShoppingCart.PreSubmitOrderProcess())
                    {
                        if (uxPayment.PaymentType == EnumPaymentType.PAYPAL)
                        {
                            string returnURL = Request.Url.GetLeftPart(UriPartial.Authority) + Response.ApplyAppPathModifier("PaymentINSHandler.aspx");
                            string cancelURL = returnURL.Replace("PaymentINSHandler.aspx", "shoppingcart.aspx");
                            returnURL += "?mode=Paypal";

                            this._shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                            if (this._shoppingCart != null)
                            {
                                // Get payment setting object for paypal payment type
                                this._paymentSetting = this.GetByPaymentTypeId((int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL);

                                ZNode.Libraries.Paypal.PaypalGateway paypal =
                                    new ZNode.Libraries.Paypal.PaypalGateway(this._paymentSetting, returnURL, cancelURL, this._checkout.ShoppingCart);

                                paypal.OrderTotal = this._shoppingCart.Total;
                                paypal.PaymentActionTypeCode = "Sale";
                                ZNode.Libraries.Paypal.PaypalResponse response = paypal.DoPaypalExpressCheckout();

                                if (response.ResponseCode != "0")
                                {
                                    //Znode Version 7.2.2
                                    //To check Shipping address match with paypal address - Start
                                    //If address does not match it will return error code 10736
                                    //Then we show user friendly message at PAYMENT INFORMATION section
                                    if (response.ResponseCode.Equals("10736"))
                                    {
                                        Label lblPayPalError = (Label)this.uxPayment.FindControl("lblPaypalError");
                                        lblPayPalError.Text = response.ResponseText;
                                    }
                                    else
                                    {
                                        lblError.Text = response.ResponseText;
                                    }
                                    //To check Shipping address match with paypal address - End
                                    return;
                                }
                                else
                                {
                                    // Redirect to paypal server
                                    Response.Redirect(response.HostUrl, true);
                                }
                            }
                        }
                        else if (uxPayment.PaymentType == EnumPaymentType.GOOGLE_CHECKOUT)
                        {
                            bool isSuccess = this.DoGoogleCheckout();
                            if (!isSuccess)
                            {
                                return;
                            }
                        }

                        order = (ZNodeOrderFulfillment)this._checkout.SubmitOrder();
                        orders.Add(order);
                    }
                    else
                    {
                        // display error page
                        lblError.Text = this._checkout.ShoppingCart.ErrorMessage + "<br>" + this._checkout.ShoppingCart.PromoDescription;
                        return;
                    }
                }
                catch (ZNode.Libraries.ECommerce.Entities.ZNodePaymentException ex)
                {
                    log.LogActivityTimerEnd(5003, Request.UserHostAddress.ToString(), null, null, null, ex.Message);

                    // display payment error message
                    lblError.Text = ex.Message;
                    return;
                }
                catch (Exception exc)
                {
                    log.LogActivityTimerEnd(5003, null);

                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("General Checkout Exception - UnableToProcessRequest : " + exc.Message);

                    // display error page
                    lblError.Text = Resources.CommonCaption.UnableToProcessRequest;
                    return;
                }

                // Post order submission
                if (this._checkout.IsSuccess && orders.Count > 0)
                {
                    log.LogActivityTimerEnd(5002, orders[0].OrderID.ToString());

                    // Worldpay
                    if (!string.IsNullOrEmpty(this._checkout.ECRedirectURL))
                    {
                        if (!string.IsNullOrEmpty(this._checkout.ShoppingCart.Payment.WorldPayPostData))
                        {
                            this.DoUrlPost(this._checkout.ECRedirectURL);
                        }
                        else
                        {
                            Response.Redirect(this._checkout.ECRedirectURL);
                        }
                    }

                    // Make an entry in tracking event for placing an Order 
                    ZNodeTracking tracker = new ZNodeTracking();
                    if (tracker.AffiliateId.Length > 0)
                    {
                        tracker.AccountID = order.AccountID;
                        tracker.OrderID = order.OrderID;
                        tracker.LogTrackingEvent("Placed an Order");
                    }

                    this.PostSubmitOrder(order);
                }
                else
                {
                    log.LogActivityTimerEnd(5001, null, null, null, null, this._checkout.PaymentResponseText);
                    lblError.Text = this._checkout.PaymentResponseText;
                    return;
                }
            }

            // Create session for Order and Shopping cart
            ZNodeShoppingCart shopping = new ZNodeShoppingCart();
            Session.Add("OrderDetail", orders);

            ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            // If shopping cart is null, create in session
            if (shoppingCart == null)
            {
                shoppingCart = new ZNodeShoppingCart();
                shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
            }

            // Clear the payment type session.
            Session["PaymentType"] = null;
            Session["IsAddressValidated"] = null;

            Response.Redirect("~/OrderReceipt.aspx");
        }

        /// <summary>
        /// Do google express checkout.
        /// </summary>
        /// <returns>Returns whether Google Checkout can be processed or not </returns>
        protected bool DoGoogleCheckout()
        {
            string returnURL = Request.Url.GetLeftPart(UriPartial.Authority) + Response.ApplyAppPathModifier("Notification.aspx");
            string cancelURL = returnURL.Replace("Notification.aspx", "shoppingcart.aspx");
            returnURL += "?mode=Google";

            // Get payment setting object for google payment type
            this._shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
            this._userAccount = (ZNodeUserAccount)ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount);
            if (this._shoppingCart != null)
            {
                this._paymentSetting = this.GetByPaymentTypeId((int)ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT);

                ZNode.Libraries.Google.GoogleCheckout google = new ZNode.Libraries.Google.GoogleCheckout(returnURL, cancelURL);
                google.PaymentSetting = this._paymentSetting;

                ZNode.Libraries.Google.GoogleCheckoutResponse response = google.SendRequestToGoogle();

                if (response.ResponseCode != "0")
                {
                    // Display error page
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage("General Checkout Exception - UnableToProcessRequest : " + response.ResponseText);
                    lblError.Text = Resources.CommonCaption.UnableToProcessRequest;
                    return false;
                }
                else
                {
                    // Get the GUID key
                    string serialNo = response.ResponseText;

                    this._shoppingCart.Payment.PaymentSetting = this._paymentSetting;
                    HttpRuntime.Cache.Add(serialNo + "cart", this._shoppingCart, null, DateTime.Now.AddHours(1), System.TimeSpan.FromMinutes(0), System.Web.Caching.CacheItemPriority.Normal, null);

                    // Add user account into the cache.
                    if (this._userAccount == null)
                    {
                        this._userAccount = new ZNodeUserAccount();
                        this._userAccount.AddUserAccount();
                    }

                    HttpRuntime.Cache.Add(serialNo + "user", this._userAccount, null, DateTime.Now.AddHours(1), System.TimeSpan.FromMinutes(0), System.Web.Caching.CacheItemPriority.Normal, null);

                    // Add portal id to the cache.
                    HttpRuntime.Cache.Add(serialNo + "portalid", ZNodeConfigManager.SiteConfig.PortalID, null, DateTime.Now.AddHours(1), System.TimeSpan.FromMinutes(0), System.Web.Caching.CacheItemPriority.Normal, null);

                    this._shoppingCart.PreSubmitOrderProcess();

                    Response.Redirect(response.ECRedirectURL);
                }
            }

            return true;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Shipping option list selected index changed Event raised by the order control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxOrder_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._checkout.ShoppingCart.Total == 0)
            {
                // Hide payment section on the final checkout page
                uxPayment.ShowPaymentSection = false;
            }
            else
            {
                // Display payment section
                uxPayment.ShowPaymentSection = true;
            }
        }
        private void UxOrder_RemoveChanged(object sender, EventArgs e)
        {
            BindCartItems(true);
        }


        /// <summary>
        /// Edit address Event raised by the payment control - move to first step
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxPayment_EditAddressClicked(object sender, EventArgs e)
        {
            if (ZNodeShoppingCart.CurrentShoppingCart().IsMultipleShipToAddress)
            {
                int addressId = uxPayment.BillingAddressId;
                Response.Redirect("Address.aspx?ItemId=" + addressId + "&isBilling=1&returnurl=checkout.aspx");

            }
            else
            {
                Response.Redirect("EditAddress.aspx");
            }

        }
        /// <summary>
        /// Edit address Event raised by the payment control - move to first step
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxPayment_MultipleAddressClicked(object sender, EventArgs e)
        {
            Response.Redirect("CheckoutMultipleAddress.aspx");
        }
        private void BindCartItems(bool loadPayment = false)
        {
            System.Collections.Generic.List<int> list = new System.Collections.Generic.List<int>();
            List<ZNodeShoppingCartItem> cartItems = new List<ZNodeShoppingCartItem>();
            if (this._shoppingCart != null)
            {

                foreach (ZNodeShoppingCartItem cartItem in this._shoppingCart.ShoppingCartItems)
                {
                    int portalId = 0;
                    if (cartItem.Product.PortalID > 0)
                    {
                        portalId = cartItem.Product.PortalID;
                    }

                    if (!list.Contains(portalId))
                    {
                        list.Add(portalId);
                        cartItems.Add(cartItem);

                    }
                }
            }

            rptCart.DataSource = cartItems;
            rptCart.DataBind();

            if (list.Count > 1 || list.Find(delegate(int portalId) { return portalId != ZNodeConfigManager.SiteConfig.PortalID; }) > 0)
            {
                uxPayment.IsMultipleVendorShoppingCart = true;
                uxPayment.BindPaymentTypeData(loadPayment);

                Localize2.Text = this.GetLocalResourceObject("txtMVReviewText").ToString();
            }
            else
            {
                uxPayment.IsMultipleVendorShoppingCart = false;
                uxPayment.BindPaymentTypeData(loadPayment);
            }
        }

        private System.Collections.Generic.List<int> GetShoppingCartVendors()
        {
            System.Collections.Generic.List<int> list = new System.Collections.Generic.List<int>();
            if (this._shoppingCart != null)
            {
                foreach (ZNodeShoppingCartItem cartItem in this._shoppingCart.ShoppingCartItems)
                {
                    int portalId = 0;
                    if (cartItem.Product.PortalID > 0)
                    {
                        portalId = cartItem.Product.PortalID;
                    }

                    if (!list.Contains(portalId))
                    {
                        list.Add(portalId);
                    }
                }
            }

            return list;
        }



        #endregion

        #region Post Submit Order - Set Digital Asset

        /// <summary>
        /// Post Order Submission CreateCustomerProfile
        /// </summary>
        /// <param name="order">Order information</param>
        private void PostSubmitOrder(ZNodeOrderFulfillment order)
        {
            this._checkout.ShoppingCart.Payment.TransactionID = order.CardTransactionID;
            this._checkout.ShoppingCart.Payment.AuthCode = order.CardAuthCode;
            this._checkout.ShoppingCart.Payment.SubscriptionID = order.Custom2;

            //Znode Version 7.2.2 
            //Code commented for update Inventory- Start
            //Removed this code block to update Inventory just after order submitted in database
            //this._checkout.ShoppingCart.PostSubmitOrderProcess();
            //Code commented for update Inventory- End

            // Clear the savedcart info
            ZNodeSavedCart savedCart = new ZNodeSavedCart();
            savedCart.RemoveSavedCart();

            int Counter = 0;
            DigitalAssetService digitalAssetService = new DigitalAssetService();

            // Loop through the Order Line Items
            foreach (OrderLineItem orderLineItem in order.OrderLineItems)
            {
                var shoppingCartItem = this._checkout.ShoppingCart.AddressCarts
                                .SelectMany(x => x.ShoppingCartItems.Cast<ZNodeShoppingCartItem>()).ElementAt(Counter++);

                // Set quantity ordered
                int qty = shoppingCartItem.Quantity;

                // Set product id
                int productId = shoppingCartItem.Product.ProductID;
                var AssignedDigitalAssets = new ZNodeGenericCollection<ZNodeDigitalAsset>();

                // get Digital assets for productid and quantity
                AssignedDigitalAssets = ZNodeDigitalAssetList.CreateByProductIdAndQuantity(productId, qty).DigitalAssetCollection;

                // Loop through the digital asset retrieved for this product
                foreach (var digitalAsset in AssignedDigitalAssets.Cast<ZNodeDigitalAsset>())
                {
                    var entity = digitalAssetService.GetByDigitalAssetID(digitalAsset.DigitalAssetID);

                    // Set OrderLineitemId property
                    entity.OrderLineItemID = orderLineItem.OrderLineItemID;

                    // Update digital asset to the database
                    digitalAssetService.Update(entity);
                }

                // Set retrieved digital asset collection to shopping product object
                // if product has digital assets, it will display it on the receipt page along with the product name
                shoppingCartItem.Product.ZNodeDigitalAssetCollection = AssignedDigitalAssets;

                // Update Display Order
                var displayOrder = new ZNode.Libraries.ECommerce.Catalog.ZNodeDisplayOrder();
                displayOrder.SetDisplayOrder(ZNode.Libraries.ECommerce.Catalog.ZNodeProduct.Create(shoppingCartItem.Product.ProductID));
            }

            // Invoke any supplier web services
            var supplierWebService = new ZnodeSupplierWebServiceManager(order, this._checkout.ShoppingCart);
            supplierWebService.InvokeWebService();

            // Send email receipts to the suppliers
            var supplierEmail = new ZnodeSupplierEmailManager(order, this._checkout.ShoppingCart);
            supplierEmail.SendEmailReceipt();
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Calculate tax, shipping cost
        /// </summary>
        private void Calculate()
        {
            // If next button was clicked on the address step then set payment billing address
            if (!string.IsNullOrEmpty(this._checkout.ShoppingCart.ErrorMessage))
            {
                lblError.Text = this._checkout.ShoppingCart.ErrorMessage;
            }
        }

        /// <summary>
        /// Returns an instance of the PaymentSetting  class.
        /// </summary>
        /// <param name="paymentTypeID">Payment Type Id</param>
        /// <returns>Returns PaymentSetting</returns>
        private PaymentSetting GetByPaymentTypeId(int paymentTypeID)
        {
            int? profileID = 0;
            if (this._userAccount != null)
            {
                // Get loggedIn user profile
                profileID = this._userAccount.ProfileID;
            }

            TList<PaymentSetting> list = _pmtSetting.FindAll(x => x.PaymentTypeID == paymentTypeID);
            list.Filter = "ActiveInd = true AND ProfileID = " + profileID;

            if (list.Count == 0)
            {
                // Get All Profiles payment setting
                list.Filter = "ActiveInd = true";
            }

            list.Sort("ProfileID DESC");

            return list[0];
        }

        private void DoUrlPost(string url)
        {
            Uri requestUrl = new Uri(HttpUtility.HtmlEncode(url));

            NameValueCollection queryCollection = new NameValueCollection();

            foreach (string vp in Regex.Split(url.Substring(url.LastIndexOf("?") + 1), "&"))
            {
                int indx = vp.IndexOf('=');
                queryCollection.Add(vp.Substring(0, indx), vp.Substring(indx + 1));
            }

            // Create temporary HTML form to post the parameters to 2CO site.            
            StringBuilder resp = new StringBuilder();
            resp.Append("<html>");
            resp.Append("<head><title>Processing Payment...</title></head>");
            resp.Append("<body onLoad=\"document.forms['gateway_form'].submit();\">");
            resp.Append(string.Format("<form method=\"POST\" name=\"gateway_form\" action=\"{0}\">", requestUrl.AbsoluteUri.Replace(requestUrl.Query.Substring(requestUrl.Query.LastIndexOf("?")), string.Empty)));
            resp.Append("<p><h2 style=\"text-align:center;\">Please wait, your order is being processed and you will be redirected to the payment website.</h2></p>");

            // Create hidden input form fields
            string value;
            foreach (string key in queryCollection.Keys)
            {
                value = queryCollection.GetValues(key)[0];
                resp.Append(string.Format("<input type=\"hidden\" name=\"{0}\" value=\"{1}\"/>", key, value));
            }

            resp.Append("<p style=\"text-align:center;\"><br/><br/>If you are not automatically redirected to payment website within 5 seconds...<br/><br/>");
            resp.Append("<input type=\"submit\" value=\"Click Here\"></p>");
            resp.Append("</form>");
            resp.Append("</body></html>");
            Response.Clear();
            Response.Write(resp.ToString());
            Response.End();
        }

        #endregion
    }
}