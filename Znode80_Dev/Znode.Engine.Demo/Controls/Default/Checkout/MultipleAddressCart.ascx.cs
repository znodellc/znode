﻿using System;
using System.Collections;
using System.Linq;
using System.Web.UI.WebControls;
using Znode.Engine.Promotions;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the ShoppingCart user control class.
    /// </summary>
    public partial class Controls_Default_Checkout_MultipleAddressCart : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeShoppingCart _ShoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
      
        #endregion

        #region Public Events
        public event System.EventHandler CartItem_RemoveLinkClicked;
        #endregion

        #region Public Methods
     
        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string GetValue(string value, string FieldName)
        {
            // Get Shopping Cart Item
            ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(value);
            if (item != null)
            {
                switch (FieldName)
                {
                    case "ViewProductLink":
                        return item.Product.ViewProductLink;
                    case "ImageAltTag":
                        return item.Product.ImageAltTag;
                    case "ThumbnailImageFilePath":
                        return item.Product.ThumbnailImageFilePath;
                    case "Name":
                        return item.Product.Name;
                    case "ShoppingCartDescription":
                        return item.Product.ShoppingCartDescription;
                    case "ProductNum":
                        return item.Product.ProductNum;
                }
                
            }
            return string.Empty;

        }        
        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {          
            if (this._ShoppingCart != null)
            {                    
                rptCartItems.DataSource = this._ShoppingCart.ShoppingCartItems;
                rptCartItems.DataBind();
            }
        }

       
        /// <summary>
        /// Represents the Update Order shipment
        /// </summary>
        /// <returns></returns>
        public bool UpdateOrderShipment(bool updateFlag = true)
        {
            lblError.Text = string.Empty;
            foreach (RepeaterItem repeateritem in rptCartItems.Items)
            {
                GridView gvCart = repeateritem.FindControl("uxCart") as GridView;

                foreach (GridViewRow row in gvCart.Rows)
                {
                    HiddenField hdnSlNo = row.Cells[0].FindControl("SLNO") as HiddenField;
                    HiddenField hdnGUID = row.Cells[0].FindControl("GUID") as HiddenField;
                    DropDownList ddlQty = row.Cells[0].FindControl("uxQty") as DropDownList;
                    DropDownList ddlAddress = row.Cells[3].FindControl("ddlShippingAddress") as DropDownList;
                    string GUID = hdnGUID.Value;
                    int SelectedQty = Convert.ToInt32(ddlQty.Text);

                    // Get Shopping cart item using GUID value
                    ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(GUID);
                    int Qty = (item.Quantity - 1) + SelectedQty;

                    if (Qty == 0)
                    {
                        this._ShoppingCart.RemoveFromCart(GUID);

                        // Remove the item from SavedCart Info
                        ZNodeSavedCart savedCart = new ZNodeSavedCart();
                        savedCart.RemoveSavedCartLineItem(item.Product.SelectedSKU.SKUID);
                    }
                    else
                    {

                        if (!CheckInventory(item, Qty))
                        {
                            lblError.Text = item.Product.Name + " - Unable to update this item.";
                            return false;
                        }
                    }

                    ZNode.Libraries.ECommerce.Entities.ZNodeOrderShipment ordershipment =
                        new ZNode.Libraries.ECommerce.Entities.ZNodeOrderShipment();
                    ordershipment.SlNo = hdnSlNo.Value;
                    ordershipment.Quantity = SelectedQty;
                    ordershipment.ItemGUID = GUID;
                    ordershipment.AddressID = Convert.ToInt32(ddlAddress.SelectedValue);

                    if (!_ShoppingCart.UpdateShipment(ordershipment) && updateFlag)
                    {
                        lblError.ForeColor = System.Drawing.Color.Red;
                        lblError.Text = item.Product.Name + " - Unable to update this item (Maximum quantity reached).";
                        return false;
                    }
                    if (updateFlag)
                    {
                        lblError.ForeColor = System.Drawing.Color.Green;
                        lblError.Text =
                            "Quantities updated successfully. Please update the address and click Continue to proceed checkout.";
                    }
                }
            }
            return true;
        }

     
        #endregion

        #region Private Methods

        /// <summary>
        /// Load Cart items gridview control data
        /// </summary>
        /// <param name="row"></param>
        private void LoadGridViewData(GridViewRow row)
        {
            HiddenField hdnGUID = row.FindControl("GUID") as HiddenField;
            string GUID = hdnGUID.Value;

            HiddenField hdnSlNo = row.FindControl("SLNO") as HiddenField;
            string SlNo = hdnSlNo.Value;

            // Get Shopping cart item using GUID value
            ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(GUID);
            DropDownList ddlQty = row.FindControl("uxQty") as DropDownList;
            DropDownList ddlAddress = row.FindControl("ddlShippingAddress") as DropDownList;

            var ordershipment = _ShoppingCart.GetOrderShipment(SlNo);
            if (ordershipment != null && ordershipment.AddressID > 0)
            {
                ddlAddress.SelectedValue = ordershipment.AddressID.ToString();
            }

            int minQty = 1;
            // If Max quantity is not set in admin , set it to 10
            int maxQty = item.Product.MaxQty == 0 ? 10 : item.Product.MaxQty;


            ArrayList quantityList = new ArrayList();

            for (int itemIndex = minQty; itemIndex <= maxQty; itemIndex++)
            {
                quantityList.Add(itemIndex);
            }

            // Bind Quantity drop down list
            ddlQty.DataSource = quantityList;
            ddlQty.DataBind();

            ddlQty.SelectedValue = minQty.ToString();

            if (ordershipment != null && ordershipment.Quantity > 0)
            {
                ddlQty.SelectedValue = ordershipment.Quantity.ToString();
            }
        }
        /// <summary>
        /// Check inventory for the given shopping cart item with selected quantity
        /// </summary>
        /// <param name="item"></param>
        /// <param name="selectedQuantity"></param>
        /// <returns></returns>
        private bool CheckInventory(ZNodeShoppingCartItem item, int selectedQuantity)
        {
            if (item.Product.QuantityOnHand < selectedQuantity && !item.Product.AllowBackOrder && item.Product.TrackInventoryInd)
            {
                return false;
            }
            foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductBaseEntity _bundleProduct in item.Product.ZNodeBundleProductCollection)
            {
                if (_bundleProduct.QuantityOnHand < selectedQuantity && !_bundleProduct.AllowBackOrder && _bundleProduct.TrackInventoryInd)
                {
                    return false;
                }
            }
            if (item.Product.SelectedAddOns.SelectedAddOnValueIds.Length > 0 && item.Product.SelectedAddOns.AddOnCollection.Cast<ZNodeAddOn>().Any(y => y.TrackInventoryInd))
            {

                // Loop through the Selected addons for each Shopping cartItem
                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnEntity AddOn in item.Product.SelectedAddOns.AddOnCollection)
                {
                    // Loop through the Add-on values for each Add-on
                    foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                    {
                        // Check for quantity on hand and back-order,track inventory settings
                        if (AddOnValue.QuantityOnHand < selectedQuantity && !AddOn.AllowBackOrder && AddOn.TrackInventoryInd)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Bind address names.
        /// </summary>
        /// <param name="isBillingAddress">isBillingAddress value either true or false</param>
        private void BindAddressNames(DropDownList ddlShippingAddress)
        {
            TList<Address> addressList = null;
            if (Session["AddressList"] == null)
            {
                addressList = DataRepository.AddressProvider.GetByAccountID(ZNodeUserAccount.CurrentAccount().AccountID);
				(addressList = addressList.FindAll(x => ZNodeUserAccount.CurrentAccount().CheckValidAddress(x))).FindAll(x => x.IsDefaultShipping == true).ForEach(y => y.Name = y.Name + " (default)");
                Session["AddressList"] = addressList;
            }
            else
            {
                addressList = Session["AddressList"] as TList<Address>;
            }
            
			int defaultAddressId = (addressList.Find(x => x.IsDefaultShipping == true) ??  addressList.FirstOrDefault()).AddressID;

            ddlShippingAddress.DataTextField = "Name";
            ddlShippingAddress.DataValueField = "AddressID";
            ddlShippingAddress.DataSource = addressList;
            ddlShippingAddress.DataBind();

            ddlShippingAddress.SelectedIndex = ddlShippingAddress.Items.IndexOf(ddlShippingAddress.Items.FindByValue(defaultAddressId.ToString()));
        }


        #endregion

        #region Page Events

        /// <summary>
        ///  Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (this._ShoppingCart != null)
            {
                // Add the shopping cart page listrack tracking script.
                ZNodeAnalytics analytics = new ZNodeAnalytics();
                analytics.AnalyticsData.IsShoppingCartPage = true;
                analytics.AnalyticsData.ShoppingCart = this._ShoppingCart;
                analytics.Bind();
            }
        }
            

        /// <summary>
        ///  Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                Session["AddressList"] = null;
        }
        
        #endregion

        #region General Events


        /// <summary>
        /// Event is raised when update button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void lnkUpdate_Click(object sender, EventArgs e)
        {
            UpdateOrderShipment();
        }

        #endregion

        #region Repeater & Gridview Event

        /// <summary>
        /// Repeater control Item data bound event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptCartItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var cartitem = e.Item.DataItem as ZNodeShoppingCartItem;

                var gvShipment = (GridView)e.Item.FindControl("uxCart");
                gvShipment.DataSource = cartitem.OrderShipments;
                gvShipment.DataBind();

                gvShipment.Rows.Cast<GridViewRow>().ToList().ForEach(x => LoadGridViewData(x));           

            }
        }
        /// <summary>
        /// Grid command event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxCart_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (this._ShoppingCart != null)
            {
                // The remove link was clicked
                if (e.CommandName.Equals("remove"))
                {
                    lblError.Text = string.Empty;
                    string SlNo = e.CommandArgument.ToString();

                    UpdateOrderShipment(false);
                    bool status = this._ShoppingCart.RemoveShipmentItem(SlNo);
                    
                    if(!status)
                    lblError.Text = "The product has a minimum quantity purchase level. Unable to remove the item.";
                    
                    if (status && this.CartItem_RemoveLinkClicked != null)
                    {
                        this.CartItem_RemoveLinkClicked(sender, e);
                    }
                }
            }
        }

       
        /// <summary>
        /// Cart items row databound event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UxCart_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this._ShoppingCart != null)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Retrieve the dropdownlist control from the first column
                    DropDownList qty = e.Row.Cells[0].FindControl("uxQty") as DropDownList;                  
                    DropDownList ShippingAddress = e.Row.Cells[3].FindControl("ddlShippingAddress") as DropDownList;
                    BindAddressNames(ShippingAddress);                   
                }
            }
        }

        #endregion

      
     
    }
}