using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using System.Linq;

/// <summary>
/// Address update during checkout
/// </summary>
namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Address user control class.
    /// </summary>
    public partial class Controls_Default_Checkout_Address : System.Web.UI.UserControl
    {
        #region Private Variables
        private Address _billingAddress = new Address();
        private Address _shippingAddress = new Address();
        private int _accountID = 0;
        private int _billingAddressID = 0;
        private int _shippingAddressID = 0;
        private string _email = string.Empty;
        private ZNodeCheckout checkout;
        private ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();
		private ZNodeShoppingCart _shoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets accountobject with addresses
        /// </summary>
        public Address BillingAddress
        {
            get
            {
                // Get fields
                if (ddlBillingAddressName.SelectedItem != null)
                {
                    this._billingAddress.AddressID = Convert.ToInt32(ddlBillingAddressName.SelectedItem.Value);
                }

                this._billingAddress.FirstName = Server.HtmlEncode(txtBillingFirstName.Text);
                this._billingAddress.LastName = Server.HtmlEncode(txtBillingLastName.Text);
                this._billingAddress.CompanyName = Server.HtmlEncode(txtBillingCompanyName.Text);
                this._billingAddress.Street = Server.HtmlEncode(txtBillingStreet1.Text);
                this._billingAddress.Street1 = Server.HtmlEncode(txtBillingStreet2.Text);
                this._billingAddress.City = Server.HtmlEncode(txtBillingCity.Text);
                this._billingAddress.StateCode = Server.HtmlEncode(txtBillingState.Text.ToUpper());
                this._billingAddress.PostalCode = Server.HtmlEncode(txtBillingPostalCode.Text);
                this._billingAddress.CountryCode = lstBillingCountryCode.SelectedValue;
                this._billingAddress.PhoneNumber = Server.HtmlEncode(txtBillingPhoneNumber.Text);

                return this._billingAddress;
            }

            set
            {
                this._billingAddress = value;

                // Set field values
                txtBillingFirstName.Text = Server.HtmlDecode(this._billingAddress.FirstName);
                txtBillingLastName.Text = Server.HtmlDecode(this._billingAddress.LastName);
                txtBillingCompanyName.Text = Server.HtmlDecode(this._billingAddress.CompanyName);
                txtBillingStreet1.Text = Server.HtmlDecode(this._billingAddress.Street);
                txtBillingStreet2.Text = Server.HtmlDecode(this._billingAddress.Street1);
                txtBillingCity.Text = Server.HtmlDecode(this._billingAddress.City);
                txtBillingState.Text = Server.HtmlDecode(this._billingAddress.StateCode);
                txtBillingPostalCode.Text = Server.HtmlDecode(this._billingAddress.PostalCode);

                if (!string.IsNullOrEmpty(this._billingAddress.CountryCode))
                {
                    lstBillingCountryCode.SelectedValue = this._billingAddress.CountryCode;
                }

                txtBillingPhoneNumber.Text = Server.HtmlDecode(this._billingAddress.PhoneNumber);
            }
        }

        /// <summary>
        /// Gets or sets accountobject with addresses
        /// </summary>
        public Address ShippingAddress
        {
            get
            {
                if (chkSameAsBilling.Checked)
                {
                    if (ddlShippingAddressName.SelectedItem != null)
                    {
                        this._shippingAddress.AddressID = Convert.ToInt32(ddlBillingAddressName.SelectedItem.Value);
                    }

                    this._shippingAddress.FirstName = Server.HtmlEncode(txtBillingFirstName.Text);
                    this._shippingAddress.LastName = Server.HtmlEncode(txtBillingLastName.Text);
                    this._shippingAddress.CompanyName = Server.HtmlEncode(txtBillingCompanyName.Text);
                    this._shippingAddress.Street = Server.HtmlEncode(txtBillingStreet1.Text);
                    this._shippingAddress.Street1 = Server.HtmlEncode(txtBillingStreet2.Text);
                    this._shippingAddress.City = Server.HtmlEncode(txtBillingCity.Text);
                    this._shippingAddress.StateCode = Server.HtmlEncode(txtBillingState.Text.ToUpper());
                    this._shippingAddress.PostalCode = Server.HtmlEncode(txtBillingPostalCode.Text);
                    this._shippingAddress.CountryCode = lstBillingCountryCode.SelectedValue;
                    this._shippingAddress.PhoneNumber = Server.HtmlEncode(txtBillingPhoneNumber.Text);
                }
                else
                {
                    if (ddlShippingAddressName.SelectedItem != null)
                    {
                        this._shippingAddress.AddressID = Convert.ToInt32(ddlShippingAddressName.SelectedItem.Value);
                    }

                    this._shippingAddress.FirstName = Server.HtmlEncode(txtShippingFirstName.Text);
                    this._shippingAddress.LastName = Server.HtmlEncode(txtShippingLastName.Text);
                    this._shippingAddress.CompanyName = Server.HtmlEncode(txtShippingCompanyName.Text);
                    this._shippingAddress.Street = Server.HtmlEncode(txtShippingStreet1.Text);
                    this._shippingAddress.Street1 = Server.HtmlEncode(txtShippingStreet2.Text);
                    this._shippingAddress.City = Server.HtmlEncode(txtShippingCity.Text);
                    this._shippingAddress.PostalCode = Server.HtmlEncode(txtShippingPostalCode.Text);
                    this._shippingAddress.CountryCode = lstShippingCountryCode.SelectedValue;
                    this._shippingAddress.StateCode = Server.HtmlEncode(txtShippingState.Text.ToUpper());
                    this._shippingAddress.PhoneNumber = Server.HtmlEncode(txtShippingPhoneNumber.Text);
                }

                return this._shippingAddress;
            }

            set
            {
                this._shippingAddress = value;

                // Set field values
                txtShippingFirstName.Text = Server.HtmlDecode(this._shippingAddress.FirstName);
                txtShippingLastName.Text = Server.HtmlDecode(this._shippingAddress.LastName);
                txtShippingCompanyName.Text = Server.HtmlDecode(this._shippingAddress.CompanyName);
                txtShippingStreet1.Text = Server.HtmlDecode(this._shippingAddress.Street);
                txtShippingStreet2.Text = Server.HtmlDecode(this._shippingAddress.Street1);
                txtShippingCity.Text = Server.HtmlDecode(this._shippingAddress.City);
                txtShippingState.Text = Server.HtmlDecode(this._shippingAddress.StateCode);
                txtShippingPostalCode.Text = Server.HtmlDecode(this._shippingAddress.PostalCode);

                if (!string.IsNullOrEmpty(this._shippingAddress.CountryCode))
                {
                    lstShippingCountryCode.SelectedValue = this._shippingAddress.CountryCode;
                }

                txtShippingPhoneNumber.Text = Server.HtmlDecode(this._shippingAddress.PhoneNumber);
            }
        }

        /// <summary>
        /// Gets or sets the Account Id.
        /// </summary>
        public int AccountID
        {
            get { return this._accountID; }

            set { this._accountID = value; }
        }

        /// <summary>
        /// Gets or sets the billing address
        /// </summary>
        public int BillingAddressID
        {
            get
            {
                // Get fields
                if (ddlBillingAddressName.SelectedItem != null)
                {
                    this._billingAddressID = Convert.ToInt32(ddlBillingAddressName.SelectedItem.Value);
                }

                return this._billingAddressID;
            }

            set
            {
                this._billingAddressID = value;
            }
        }

        /// <summary>
        /// Gets or sets the shipping address
        /// </summary>
        public int ShippingAddressID
        {
            get
            {
                // If billing and shipping address is different then get shipping address Id from dropdown list. 
                if (ddlShippingAddressName.SelectedItem != null && !chkSameAsBilling.Checked)
                {
                    this._shippingAddressID = Convert.ToInt32(ddlShippingAddressName.SelectedItem.Value);
                }
                else if (ddlBillingAddressName.SelectedItem != null)
                {
                    // If shipping address is same as billing address selected.
                    this._shippingAddressID = Convert.ToInt32(ddlBillingAddressName.SelectedItem.Value);
                }

                return this._shippingAddressID;
            }

            set
            {
                this._shippingAddressID = value;
            }
        }

        /// <summary>
        /// Gets or sets the Account email address.
        /// </summary>
        public string Email
        {
            get
            {
                this._email = txtEmailAddress.Text;
                return this._email;
            }

            set
            {
                this._email = value;
                txtEmailAddress.Text = value;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        ///  Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {

            if (this.userAccount != null)
            {
                // Add the shopping cart page listrack tracking script.
                ZNodeAnalytics analytics = new ZNodeAnalytics();
                analytics.AnalyticsData.UserAccount = this.userAccount;
                analytics.AnalyticsData.IsAddressPage = true;
                analytics.Bind();
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
			if (this.userAccount == null && this._shoppingCart == null)
			{
				Response.Redirect("~/default.aspx");
			}

            // Get checkout object from session
            this.checkout = new ZNodeCheckout();

            if (this.userAccount != null)
            {
                this.AccountID = this.userAccount.AccountID;
				this.Page.Master.Master.FindControl("SearchandLinks").Visible = true;
				this.Page.Master.Master.FindControl("Menu").Visible = true;
            }

            if (Request.QueryString["ErrorMsg"] != null && Session["IsAddressValidated"] == null)
            {
                lblError.Text = this.GetLocalResourceObject("AddressValidationFailed").ToString();
            }

            uxStepTracker.Step = 1;

            if (!Page.IsPostBack)
            {
                this.BindCountry();

                if (this.userAccount == null)
                {
                    // For anonymous user hide the address name
                    divBillingAddressName.Visible = false;
                    divShippingAddressName.Visible = false;
                }
                else
                {
                    txtEmailAddress.Text = this.userAccount.EmailID;

                    // Load billing address names
                    this.BindAddressNames(true);

                    // Load shipping address names.
                    this.BindAddressNames(false);
                }

                chkSameAsBilling.Checked = false;
                if (this.userAccount != null && userAccount.BillingAddress != null && userAccount.ShippingAddress != null)
                {
                    if (userAccount.BillingAddress.AddressID == userAccount.ShippingAddress.AddressID)
                    {
                        // Both billing and shipping address are same.
                        chkSameAsBilling.Checked = true;
                        pnlShipping.Visible = false;
                    }
                    else
                    {
                        chkSameAsBilling.Checked = false;
                        pnlShipping.Visible = true;
                    }
                }

                // If checkout without registration or account not yet have email then display the email address column.
                if (this.userAccount == null || !this.userAccount.UserID.HasValue || string.IsNullOrEmpty(this.userAccount.EmailID))
                {
                    // Add the listrack email capture script only if user account null.
                    if (this.userAccount == null)
                    {
                        ZNodeAnalytics analytics = new ZNodeAnalytics();
                        analytics.AnalyticsData.EmailCaptureContorlId = txtEmailAddress.ClientID;
                        analytics.Bind();
                        string emailCaptureScript = analytics.AnalyticsData.EmailCaptureScript;
                        if (!Page.ClientScript.IsStartupScriptRegistered("EmailCapture"))
                        {
                            Page.ClientScript.RegisterStartupScript(GetType(), "EmailCapture", emailCaptureScript.ToString());
                        }
                    }

                    divEmailAddress.Visible = true;
                }
                else
                {
                    divEmailAddress.Visible = false;
                }

                this.BindAddress();
            }
        }

        #endregion

        #region Protected Methods and Events
        protected void ChkSameAsBilling_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSameAsBilling.Checked)
            {
                pnlShipping.Visible = false;
            }
            else
            {
                AddressService addressService = new AddressService();
                Address address = null;
                TList<Address> addressList = new TList<Address>();
                if (this.userAccount != null)
                {
                    addressList = addressService.GetByAccountID(this.userAccount.AccountID);
                    if (addressList.Count == 1)
                    {
                        // Create new shipping address from existing billing address.
                        address = (Address)addressList[0].Clone();
                        address.AddressID = 0;
                        address.Name = "Default Shipping Address";
                        address.IsDefaultBilling = false;
                        address.IsDefaultShipping = true;
                        addressService.Insert(address);

                        // Clear previous default shipping address.
                        foreach (Address currentItem in addressList)
                        {
                            currentItem.IsDefaultShipping = false;
                        }

                        addressService.Update(addressList);

                        this.BindAddressNames(false);
                    }
                    else
                    {
                        if (this.userAccount.ShippingAddress != null && this.userAccount.ShippingAddress.AddressID > 0)
                        {
                            // Load user selected shipping address from session shipping address.
                            address = addressService.GetByAddressID(this.userAccount.ShippingAddress.AddressID);
                        }
                        else
                        {
                            // Load user's default shipping address from account.
                            AccountAdmin accountAdmin = new AccountAdmin();
                            address = accountAdmin.GetDefaultShippingAddress(this.userAccount.AccountID);
                        }
                    }

                    if (address != null)
                    {
                        this.LoadAddress(address.AddressID, false);
                    }
                }

                pnlShipping.Visible = true;
            }
        }

        protected void DdlAddressName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            int addressId = Convert.ToInt32(ddl.SelectedValue);
            if (addressId == 0)
            {
                Response.Redirect("Address.aspx?returnurl=EditAddress.aspx");
            }
            else
            {
                if (ddl.ID.ToLower().Contains("billing"))
                {
                    this.LoadAddress(addressId, true);
                }
                else
                {
                    this.LoadAddress(addressId, false);
                }
            }
        }

		protected void btnProceedToNext_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            else
            {
                this.ProceedToCheckout();
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Load the selected address
        /// </summary>
        /// <param name="addressId">Address Id</param>
        /// <param name="isBillingAddress">IsBilling address (true/false)</param>
        private void LoadAddress(int addressId, bool isBillingAddress)
        {
            AddressService addressService = new AddressService();
            Address address = addressService.GetByAddressID(addressId);

            txtEmailAddress.Text = this.Email;
            if (address != null)
            {
                if (isBillingAddress)
                {
                    this._billingAddress = address;
                    ddlBillingAddressName.SelectedIndex = ddlBillingAddressName.Items.IndexOf(ddlBillingAddressName.Items.FindByValue(addressId.ToString()));
                    txtBillingFirstName.Text = address.FirstName;
                    txtBillingLastName.Text = address.LastName;
                    txtBillingCompanyName.Text = address.CompanyName;
                    txtBillingStreet1.Text = address.Street;
                    txtBillingStreet2.Text = address.Street1;
                    txtBillingCity.Text = address.City;
                    txtBillingState.Text = address.StateCode;
                    txtBillingPostalCode.Text = address.PostalCode;

                    if (address.CountryCode != null && address.CountryCode.Length > 0)
                    {
                        lstBillingCountryCode.SelectedValue = address.CountryCode;
                    }

                    txtBillingPhoneNumber.Text = address.PhoneNumber;
                }
                else
                {
                    this._shippingAddress = address;

                    ddlShippingAddressName.SelectedIndex = ddlShippingAddressName.Items.IndexOf(ddlShippingAddressName.Items.FindByValue(addressId.ToString()));
                    txtShippingFirstName.Text = address.FirstName;
                    txtShippingLastName.Text = address.LastName;
                    txtShippingCompanyName.Text = address.CompanyName;
                    txtShippingStreet1.Text = address.Street;
                    txtShippingStreet2.Text = address.Street1;
                    txtShippingCity.Text = address.City;
                    txtShippingState.Text = address.StateCode;
                    txtShippingPostalCode.Text = address.PostalCode;
                    if (address.CountryCode != null && address.CountryCode.Length > 0)
                    {
                        lstShippingCountryCode.SelectedValue = address.CountryCode;
                    }

                    txtShippingPhoneNumber.Text = address.PhoneNumber;
                }
            }
        }

        /// <summary>
        /// Binds country drop-down list
        /// </summary>
        private void BindCountry()
        {
            // PortalCountry
            ZNode.Libraries.DataAccess.Custom.PortalCountryHelper portalCountryHelper = new ZNode.Libraries.DataAccess.Custom.PortalCountryHelper();
            DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

            DataView BillingView = new DataView(countryDs.Tables[0]);
            BillingView.RowFilter = "BillingActive = 1";

            DataView ShippingView = new DataView(countryDs.Tables[0]);
            ShippingView.RowFilter = "ShippingActive = 1";

            lstBillingCountryCode.DataSource = BillingView;
            lstBillingCountryCode.DataTextField = "Name";
            lstBillingCountryCode.DataValueField = "Code";
            lstBillingCountryCode.DataBind();
            ListItem bblistItem = lstBillingCountryCode.Items.FindByValue("US");
            if (bblistItem != null)
            {
                lstBillingCountryCode.SelectedIndex = lstBillingCountryCode.Items.IndexOf(bblistItem);
            }

            lstShippingCountryCode.DataSource = ShippingView;
            lstShippingCountryCode.DataTextField = "Name";
            lstShippingCountryCode.DataValueField = "Code";
            lstShippingCountryCode.DataBind();
            ListItem sslistItem = lstShippingCountryCode.Items.FindByValue("US");
            if (sslistItem != null)
            {
                lstShippingCountryCode.SelectedIndex = lstShippingCountryCode.Items.IndexOf(sslistItem);
            }

            // This will pre-select the billing and shipping address country code
            ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();
            if (_userAccount != null)
            {
                ListItem blistItem = lstBillingCountryCode.Items.FindByValue(_userAccount.BillingAddress.CountryCode);
                if (blistItem != null)
                {
                    lstBillingCountryCode.SelectedIndex = lstBillingCountryCode.Items.IndexOf(blistItem);
                }

                ListItem slistItem = lstShippingCountryCode.Items.FindByValue(_userAccount.ShippingAddress.CountryCode);
                if (slistItem != null)
                {
                    lstShippingCountryCode.SelectedIndex = lstShippingCountryCode.Items.IndexOf(slistItem);
                }
            }
        }

        /// <summary>
        /// Bind address names.
        /// </summary>
        /// <param name="isBillingAddress">isBillingAddress value either true or false</param>
        private void BindAddressNames(bool isBillingAddress)
        {
            AddressService addressService = new AddressService();
            TList<Address> addressList = addressService.GetByAccountID(this.AccountID);
           
            int defaultAddressId = 0;

            if (isBillingAddress)
            {
                // If account has no billing address then hide the billing address name
                divBillingAddressName.Visible = addressList.Count == 0 ? false : true;
                foreach (Address currentItem in addressList)
                {
                    if (currentItem.IsDefaultBilling)
                    {
                        currentItem.Name = currentItem.Name + " (default)";
                        defaultAddressId = currentItem.AddressID;
                    }
                }

                ddlBillingAddressName.DataTextField = "Name";
                ddlBillingAddressName.DataValueField = "AddressID";
                ddlBillingAddressName.DataSource = addressList;
                ddlBillingAddressName.DataBind();

                this.LoadAddress(userAccount.BillingAddress.AddressID, true);

                ddlBillingAddressName.SelectedIndex = ddlBillingAddressName.Items.IndexOf(ddlBillingAddressName.Items.FindByValue(userAccount.BillingAddress.AddressID.ToString()));
                this.BillingAddressID = defaultAddressId;
            }
            else
            {
                // If account has no shipping address then hide the shipping address name
                divShippingAddressName.Visible = addressList.Count == 0 ? false : true;
                foreach (Address currentItem in addressList)
                {
                    if (currentItem.IsDefaultShipping)
                    {
                        currentItem.Name = currentItem.Name + " (default)";
                        defaultAddressId = currentItem.AddressID;
                    }
                }

                ddlShippingAddressName.DataTextField = "Name";
                ddlShippingAddressName.DataValueField = "AddressID";
                ddlShippingAddressName.DataSource = addressList;
                ddlShippingAddressName.DataBind();

                this.LoadAddress(userAccount.ShippingAddress.AddressID, false);

                ddlShippingAddressName.SelectedIndex = ddlShippingAddressName.Items.IndexOf(ddlShippingAddressName.Items.FindByValue(userAccount.ShippingAddress.AddressID.ToString()));
                this.ShippingAddressID = defaultAddressId;
            }

            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                ddlBillingAddressName.Visible = false;
                ddlShippingAddressName.Visible = false;
                divBillingAddressName.Visible = false;
                divShippingAddressName.Visible = false;
            }
        }

        #endregion

        /// <summary>
        /// Bind the customer billing and shipping address.
        /// </summary>
        private void BindAddress()
        {
            if (this.checkout.UserAccount != null)
            {
                // Set the account Id.
                this.AccountID = this.checkout.UserAccount.AccountID;
                this.Email = this.checkout.UserAccount.EmailID;

                // if no address was previously entered
                if (this.BillingAddress.FirstName.Length == 0)
                {
                    this.BillingAddress = new Address();
                    this.ShippingAddress = new Address();

                    this.BillingAddress = this.checkout.UserAccount.BillingAddress;
                    this.ShippingAddress = this.checkout.UserAccount.ShippingAddress;
                }
            }
        }
        public void validateCountry(object source, ServerValidateEventArgs args)
        {
            if (chkSameAsBilling.Checked)
            {
                ZNode.Libraries.DataAccess.Custom.PortalCountryHelper portalCountryHelper = new ZNode.Libraries.DataAccess.Custom.PortalCountryHelper();
                DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

                DataTable BillingView = countryDs.Tables[0];
                DataRow[] drBillingView = BillingView.Select("CountryCode='" + this.lstBillingCountryCode.Text + "'");
                foreach (DataRow drView in drBillingView)
                {
                    if (drView["ShippingActive"].ToString() != "True")
                    {
                        args.IsValid = false;
                        valCountry.ErrorMessage = "Country has No Shipping Facility";
                    }
                    else
                    {
                        args.IsValid = true;
                    }
                }

            }
        }
        /// <summary>




        protected void lstBillingCountryCode_TextChanged(object sender, EventArgs e)
        {
            if (chkSameAsBilling.Checked)
            {
                ZNode.Libraries.DataAccess.Custom.PortalCountryHelper portalCountryHelper = new ZNode.Libraries.DataAccess.Custom.PortalCountryHelper();
                DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

                DataTable BillingView = countryDs.Tables[0];
                DataRow[] drBillingView = BillingView.Select("CountryCode='" + this.lstBillingCountryCode.Text + "'");
                foreach (DataRow drView in drBillingView)
                {
                    if (drView["ShippingActive"].ToString() != "True")
                    {
                        valCountry.ErrorMessage = "Country has No Shipping Facility";
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Validate and save the customer address and move to checkout page.
        /// </summary>
        private void ProceedToCheckout()
        {
            // Validate billing address statecode
            if (this.BillingAddress.CountryCode == "US" && this.BillingAddress.StateCode.Trim().Length != 2)
            {
                return;
            }

            // Validate shipping address statecode
            if (this.ShippingAddress.CountryCode == "US" && this.ShippingAddress.StateCode.Trim().Length != 2)
            {
                return;
            }

            // set address objects
            if (this.checkout.UserAccount != null)
            {
                AddressService addressService = new AddressService();
                Address billingAddress = new Address();
                Address shippingAddress = new Address();

                int billingAddressId = this.BillingAddressID;
                int shippingAddressId = this.ShippingAddressID;

				TList<Address> addressList = this.checkout.UserAccount.Addresses;

                // Update or Add billing address
                billingAddress = this.BillingAddress;
                if (billingAddressId > 0)
                {
                    billingAddress = this.checkout.UserAccount.Addresses.Find(x => x.AddressID == billingAddressId);
                    billingAddress.FirstName = this.BillingAddress.FirstName;
                    billingAddress.LastName = this.BillingAddress.LastName;
                    billingAddress.CompanyName = this.BillingAddress.CompanyName;
                    billingAddress.Street = this.BillingAddress.Street;
                    billingAddress.Street1 = this.BillingAddress.Street1;
                    billingAddress.City = this.BillingAddress.City;
                    billingAddress.StateCode = this.BillingAddress.StateCode;
                    billingAddress.PostalCode = this.BillingAddress.PostalCode;
                    billingAddress.CountryCode = this.BillingAddress.CountryCode;
                    billingAddress.PhoneNumber = this.BillingAddress.PhoneNumber;
	                billingAddress.AddressID = this.BillingAddress.AddressID;
	                //addressService.Update(billingAddress);
                }
                else
                {
                    // Clear all previous default billing address flag.
                    foreach (Address currentItem in addressList)
                    {
                        currentItem.IsDefaultBilling = false;
                    }                    

                    billingAddress.Name = "Default Billing";
                    billingAddress.AccountID = this.checkout.UserAccount.AccountID;
                    billingAddress.IsDefaultBilling = true;
                    //addressService.Insert(billingAddress);
					addressList.Add(billingAddress);
                }

                this.BillingAddress = billingAddress;
                billingAddressId = billingAddress.AddressID;

                // Update or Add shipping address
                shippingAddress = this.ShippingAddress;
                if (shippingAddressId > 0)
                {
					shippingAddress = this.checkout.UserAccount.Addresses.Find(x => x.AddressID == shippingAddressId);
                    shippingAddress.FirstName = this.ShippingAddress.FirstName;
                    shippingAddress.LastName = this.ShippingAddress.LastName;
                    shippingAddress.CompanyName = this.ShippingAddress.CompanyName;
                    shippingAddress.Street = this.ShippingAddress.Street;
                    shippingAddress.Street1 = this.ShippingAddress.Street1;
                    shippingAddress.City = this.ShippingAddress.City;
                    shippingAddress.StateCode = this.ShippingAddress.StateCode;
                    shippingAddress.PostalCode = this.ShippingAddress.PostalCode;
                    shippingAddress.CountryCode = this.ShippingAddress.CountryCode;
                    shippingAddress.PhoneNumber = this.ShippingAddress.PhoneNumber;
					shippingAddress.AddressID = this.ShippingAddress.AddressID;
                    //addressService.Update(shippingAddress);
                }
                else
                {
                    // Clear all previous default billing address flag.
                    foreach (Address currentItem in addressList)
                    {
                        currentItem.IsDefaultShipping = false;
                    }

                    //addressService.Update(addressList);

                    shippingAddress.Name = "Default Shipping";
                    shippingAddress.AccountID = this.checkout.UserAccount.AccountID;
                    shippingAddress.IsDefaultShipping = true;
                    //addressService.Insert(shippingAddress);
					addressList.Add(shippingAddress);
                }

                this.ShippingAddress = shippingAddress;
                shippingAddressId = shippingAddress.AddressID;

				addressService.Save(addressList);

				// Set address in session
				if (billingAddressId > 0)
				{
					this.checkout.UserAccount.SetBillingAddress(billingAddress);
				}

				if (shippingAddressId > 0)
				{
					this.checkout.UserAccount.SetShippingAddress(shippingAddress);
				}

                this.checkout.UserAccount.CompanyName = billingAddress.CompanyName;
               
                HttpContext.Current.Session["DefaultShippingAddressId"] = shippingAddressId;
                ZNodeShoppingCart shoppingcart = ZNodeShoppingCart.CurrentShoppingCart();
                if (shoppingcart != null)
                    shoppingcart.ResetShippingAddress(0);
                AccountAdmin accountAdmin = new AccountAdmin();
                Account account = accountAdmin.GetByAccountID(this.checkout.UserAccount.AccountID);
                if (account != null)
                {
                    account.Email = this.Email;
                    this.checkout.UserAccount.EmailID = account.Email;
                    accountAdmin.Update(account);
                }

                // Update online account email address
                if (this.checkout.UserAccount.UserID.HasValue)
                {
                    MembershipUser user = Membership.GetUser();

                    if (user != null)
                    {
                        user.Email = this.checkout.UserAccount.EmailID;
                        Membership.UpdateUser(user);
                    }
                }
            }
            else
            {
                // If no address was previously entered
                if (this.BillingAddress.FirstName.Length > 0)
                {
                    this.checkout.UserAccount = new ZNodeUserAccount();
                    this.checkout.UserAccount.BillingAddress = this.BillingAddress;
                    this.checkout.UserAccount.ShippingAddress = this.ShippingAddress;
                    this.checkout.UserAccount.EmailID = this.Email;
                    this.checkout.UserAccount.ProfileID = ZNodeProfile.CurrentUserProfile.ProfileID;

                    // Affiliate Settings
                    ZNodeTracking tracking = new ZNodeTracking();
                    string referralAffiliate = tracking.AffiliateId;
                    int referralAccountId = 0;

                    if (!string.IsNullOrEmpty(referralAffiliate))
                    {
                        if (int.TryParse(referralAffiliate, out referralAccountId))
                        {
                            ZNode.Libraries.DataAccess.Service.AccountService accountServ = new ZNode.Libraries.DataAccess.Service.AccountService();
                            ZNode.Libraries.DataAccess.Entities.Account account = accountServ.GetByAccountID(referralAccountId);

                            // Affiliate account exists
                            if (account != null)
                            {
                                if (account.ReferralStatus == "A")
                                {
                                    this.checkout.UserAccount.ReferralAccountId = account.AccountID;
                                }
                            }
                        }
                    }

                    // Add new contact
                    this.checkout.UserAccount.AddUserAccount();
                    this.BillingAddress = this.BillingAddress;
                    AddressService addressService = new AddressService();
                    if (chkSameAsBilling.Checked)
                    {
                        // If both address are same then create single address entity                    
                        this.BillingAddress.Name = "Default Address";
                        this.BillingAddress.IsDefaultBilling = true;
                        this.BillingAddress.IsDefaultShipping = true;
                        this.BillingAddress.AccountID = this.checkout.UserAccount.AccountID;
						this.checkout.UserAccount.Addresses.Add(this.BillingAddress);

                        // Both address are same
                        this.BillingAddress = this.checkout.UserAccount.BillingAddress;
                        this.ShippingAddress = this.checkout.UserAccount.BillingAddress;
                    }
                    else
                    {
                        // Insert billing address
                        this.BillingAddress.Name = "Default Billing Address";
                        this.BillingAddress.IsDefaultBilling = true;
                        this.BillingAddress.AccountID = this.checkout.UserAccount.AccountID;
                        this.checkout.UserAccount.Addresses.Add(this.BillingAddress);
                        this.checkout.UserAccount.SetBillingAddress(this.BillingAddress);

                        // Insert shipping address
                        this.ShippingAddress = this.ShippingAddress;
                        this.ShippingAddress.Name = "Default Shipping Address";
                        this.ShippingAddress.IsDefaultShipping = true;
                        this.ShippingAddress.AccountID = this.checkout.UserAccount.AccountID;
						this.checkout.UserAccount.Addresses.Add(this.ShippingAddress);
                        this.checkout.UserAccount.SetShippingAddress(this.ShippingAddress);

                        this.BillingAddress = this.checkout.UserAccount.BillingAddress;
                        this.ShippingAddress = this.checkout.UserAccount.ShippingAddress;
                    }

	                addressService.Save(this.checkout.UserAccount.Addresses);

                    HttpContext.Current.Session["DefaultShippingAddressId"] = this.ShippingAddress.AddressID;
                    ZNodeShoppingCart shoppingcart = ZNodeShoppingCart.CurrentShoppingCart();
                    if (shoppingcart != null)
                        shoppingcart.ResetShippingAddress(0);

                    this.checkout.UserAccount.SetBillingAddress(this.BillingAddress);
                    this.checkout.UserAccount.SetShippingAddress(this.ShippingAddress);

                    AccountProfile accountProfile = new AccountProfile();
                    accountProfile.AccountID = this.checkout.UserAccount.AccountID;
                    accountProfile.ProfileID = ZNodeProfile.CurrentUserProfile.ProfileID;
                    AccountProfileService accountProfileServ = new AccountProfileService();
                    accountProfileServ.Insert(accountProfile);

                    // This block enforces user logn/registration.
                    // Do not allow user to go to any other checkout step unless he is logged-in
                    this.checkout.UserAccount.AddToSession(ZNodeSessionKeyType.UserAccount);

                    // Get Profile entity for logged-in user profileId
                    ZNode.Libraries.DataAccess.Service.ProfileService profileService = new ZNode.Libraries.DataAccess.Service.ProfileService();
                    ZNode.Libraries.DataAccess.Entities.Profile profile = profileService.GetByProfileID(this.checkout.UserAccount.ProfileID);

                    // Hold this profile object in the session state
                    HttpContext.Current.Session["ProfileCache"] = profile;
                }
            }


            // Validate the customer shipping address if address validation enabled.
            if (ZNodeConfigManager.SiteConfig.EnableAddressValidation != null ||
                ZNodeConfigManager.SiteConfig.EnableAddressValidation == true)
            {
                AccountAdmin _accountAdmin = new AccountAdmin();
                bool isAddressValid = _accountAdmin.IsAddressValid(this.ShippingAddress);

                // Do not allow the customer to go to next page if valid shipping address required is enabled. 
                if (!isAddressValid && ZNodeConfigManager.SiteConfig.RequireValidatedAddress != null &&
                    ZNodeConfigManager.SiteConfig.RequireValidatedAddress == true)
                {
                    lblError.Text = this.GetLocalResourceObject("AddressValidationFailed").ToString();

                    // Clear the session value
                    Session["IsAddressValidated"] = null;
                    return;
                }
            }
            Session["AddressList"] = null;
            Session["IsAddressValidated"] = "true";

			_shoppingCart.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().ToList().ForEach(x => _shoppingCart.LoadShipmentData(x));
			this._shoppingCart.ResetPortalCart();
            Response.Redirect("Checkout.aspx");
        }

        protected void lnkAddress_Click(object sender, EventArgs e)
        {
            Response.Redirect("Address.aspx?returnurl=EditAddress.aspx");
        }
    }
}