<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Checkout_Notification" Codebehind="Notification.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>

<div>
    <!-- Confirmation panel -->
    <asp:Panel ID="pnlConfirmation" runat="server" Visible="False" 
        meta:resourcekey="pnlConfirmationResource1">
        <div id="Notification">
            <ZNode:spacer SpacerWidth="15" SpacerHeight="15" ID="Spacer3" runat="server" />
            <div class="Text"><asp:Label id='lblMsg' runat="server" 
                    meta:resourcekey="lblMsgResource1"></asp:Label></div>        
            <ZNode:spacer ID="Spacer2" runat="server" SpacerWidth="1" SpacerHeight="10" />
            <div class="BackLink"><a href="~/" id="HomeLink" runat="server"><<  <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtReturntoHome"></asp:Localize></a></div>
        </div>
    </asp:Panel>    
</div>
