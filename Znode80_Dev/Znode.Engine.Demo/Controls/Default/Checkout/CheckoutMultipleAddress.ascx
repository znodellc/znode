﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Checkout_CheckoutMultipleAddress" CodeBehind="CheckoutMultipleAddress.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="Spacer" TagPrefix="uc1" %>
<%@ Register Src="MultipleAddressCart.ascx" TagName="ShoppingCart" TagPrefix="ZNode" %>


<div class="ShoppingCart">
    <div class="BreadCrumb">
        <span class="PromoText">
            <ZNode:CustomMessage ID="HomeCustomMessage5" EnableViewState="false" MessageKey="StoreSpecials" runat="server" />
        </span>
    </div>
    <hr class="Horizontal" />
</div>
<!--[if lte IE 7]>
	    <div id="ie7css">
	    <![endif]-->
<div class="ShoppingCart">
    <div style="float: left; width: 40%;">
        <div class="PageTitleNew">
            Select shipping address for each item            
        </div>
    </div>
    <div style="padding: 10px;" class="Mylink">
        <a ID="addressLink" runat="server" href="">Add/Edit Address</a>
    </div>
    <div class="Clear"></div>
    <div class="Error">
        <asp:Label ID="uxMsg" EnableViewState="false" runat="server"></asp:Label>
    </div>
    <asp:Panel ID="pnlShoppingCart" runat="server">
        <div class="Error">
            <asp:Label ID="uxErrorMsg" runat="server" ></asp:Label>
        </div>
        <div>
            <ZNode:ShoppingCart ID="uxCart" runat="server" />
        </div>
        <div class="CheckoutBoxNew">
            <asp:LinkButton ID="Checkout1" runat="server" AlternateText="Check Out" OnClick="Checkout_Click"
                ValidationGroup="groupCart" CssClass="checkout">Continue</asp:LinkButton>
            <br />
        </div>
    </asp:Panel>
</div>
<div style="clear: both;"></div>
<uc1:Spacer ID="Spacer13" SpacerHeight="100" SpacerWidth="3" runat="server"></uc1:Spacer>

<!--[if lte IE 7]>
	     </div>
	     <![endif]-->
