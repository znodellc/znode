<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Checkout_Payment"
    CodeBehind="Payment.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>
<%@ Register TagPrefix="ZNode" Assembly="ZNode.Libraries.ECommerce.Catalog" Namespace="ZNode.Libraries.ECommerce.Catalog" %>
<asp:PlaceHolder runat="server" ID="ErrorPanel" Visible="false">
    <div class="Form">
        <div class="HeaderStyle">
            <asp:Localize ID="Localize16" runat="server" meta:resourceKey="txtPaymentInformation"></asp:Localize>
        </div>
        <div class="clear">
            <asp:Label CssClass="Error" ID="lblError" runat="server" EnableViewState="False"
                meta:resourcekey="lblErrorResource1"></asp:Label>
        </div>
    </div>
</asp:PlaceHolder>
<script type="text/javascript">
    function clearErrorMessage() {
        document.getElementById("ctl00_ctl00_MainContent_uxCheckout_uxPayment_lblPaypalError").innerHTML = "";
    }
</script>
<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <Triggers>
        <asp:PostBackTrigger ControlID="TwoCheckoutSubmitButton" runat="server" />
    </Triggers>
    <ContentTemplate>

        <asp:Panel ID="pnlAddress" runat="server" meta:resourcekey="pnlAddressResource1">
            <div class="Form">
                <div class="HeaderStyle">
                    <asp:Localize ID="Localize18" runat="server" meta:resourceKey="txtAddress"></asp:Localize>
                </div>

                <div>
                    <table style="width: 100%;">
                        <tr>
                            <td style="vertical-align: top;">
                                <div>
                                    <div class="FieldStyle" style="text-align: left;">
                                        <asp:Localize ID="Localize20" runat="server" meta:resourceKey="txtShippingAddress"></asp:Localize>
                                        <asp:LinkButton ID="lnkEditShipping" runat="server" Text="Change" OnClick="LnkEditShipping_Click"
                                            CausesValidation="False" meta:resourcekey="txtEditBillingShippingAddress"></asp:LinkButton>
                                        <asp:LinkButton ID="lnkMultipleShipping" runat="server" Text="Ship to Multiple Addresses" OnClick="lnkMultipleShipping_Click"
                                            CausesValidation="False" Visible="false"></asp:LinkButton>
                                    </div>
                                    <div class="LeftContent" style="text-align: left; width: 100%">
                                        <asp:Label ID="lblShippingAddress" runat="server"></asp:Label>
                                        <a href="~/CheckoutMultipleAddress.aspx" id="lnkView" runat="server" visible="false">View</a>
                                        <br />

                                    </div>
                                </div>

                            </td>
                            <td class="PaymentContent">

                                <div>
                                    <div class="FieldStyle" style="text-align: left;">
                                        <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtBillingAddress"></asp:Localize>
                                        <asp:LinkButton ID="lnkEditBilling" runat="server" Text="Change" OnClick="LnkEditBilling_Click"
                                            CausesValidation="False" meta:resourcekey="txtEditBillingShippingAddress"></asp:LinkButton>
                                    </div>
                                    <div>
                                        <asp:Label ID="lblBillingAddress" runat="server"></asp:Label>
                                        <br />

                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>

                    <uc1:spacer ID="Spacer33" EnableViewState="false" SpacerHeight="10" SpacerWidth="5"
                        runat="server"></uc1:spacer>

                </div>
                <div class="Clear"></div>



            </div>
        </asp:Panel>
        <div style="clear: both;"></div>

        <asp:Panel ID="pnlPayment" runat="server" meta:resourcekey="pnlPaymentResource1">
            <div class="Form">
                <div class="HeaderStyle">
                    <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtPaymentInformation"></asp:Localize>
                </div>
                <div class="Row Clear"></div>
                <table style="width: 100%;">
                    <tr>
                        <td class="PaymentContent">
                            <div>

                                <asp:Panel ID="pnlPaymentType" runat="server">

                                    <asp:PlaceHolder runat="server" ID="PaymentTypePanel">

                                        <div class="FieldStyle LeftContent" style="text-align: left;">
                                            <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtPaymentMethod"></asp:Localize>
                                        </div>
                                        <div class="LeftContent">
                                            <asp:DropDownList ID="lstPaymentType" runat="server" OnSelectedIndexChanged="LstPaymentType_SelectedIndexChanged"
                                                AutoPostBack="True">
                                            </asp:DropDownList>
                                        </div>

                                        <div class="FieldStyle LeftContent">
                                        </div>
                                        <div class="LeftContent ValueField">
                                            <asp:LinkButton ID="lnkRemoveCardData" runat="server" Text="Remove this card" OnClick="LnkRemoveCardData_Click"
                                                Visible="false"></asp:LinkButton>
                                        </div>


                                    </asp:PlaceHolder>
                                </asp:Panel>

                            </div>
                            <div>

                                <asp:Panel ID="pnlPurchaseOrder" runat="server" Visible="False" meta:resourcekey="pnlPurchaseOrderResource1">
                                    <div class="Row Clear">
                                        <div class="FieldStyle LeftContent" style="text-align: left;">
                                            <asp:Localize ID="Localize7" runat="server" meta:resourceKey="txtPONumber"></asp:Localize>
                                        </div>
                                        <div class="LeftContent">
                                            <asp:TextBox ID="txtPONumber" runat="server" Width="130px" Columns="30" MaxLength="100"
                                                AutoCompleteType="Disabled" meta:resourcekey="txtPONumberResource1"></asp:TextBox>
                                            <div>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" ControlToValidate="txtPONumber"
                                                    runat="server" Display="Dynamic" CssClass="Error" meta:resourcekey="Requiredfieldvalidator1Resource1"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                    </div>
                                </asp:Panel>

                            </div>


                            <%--Credit Card Payment Section--%>
                            <asp:Panel ID="pnlCreditCard" runat="server" Visible="false">

                                <div class="FieldStyle LeftContent">
                                    <asp:Localize ID="Localize11" runat="server" meta:resourceKey="rdbPaypal"></asp:Localize>
                                </div>

                                <div style="clear: both">
                                    <div class="FieldStyle LeftContent" style="text-align: left;">
                                        <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtCardNumber"></asp:Localize>
                                    </div>
                                    <div class="LeftContent" style="width: 60%;">
                                        <div>

                                            <div>
                                                <asp:TextBox ID="txtCreditCardNumber" runat="server" Width="130px" Columns="30" MaxLength="20"
                                                    autocomplete="off" meta:resourcekey="txtCreditCardNumberResource1" ValidationGroup="SubmitOrder" onblur="clearErrorMessage()"></asp:TextBox>&nbsp;&nbsp;
                       
                               <img id="imgVisa" src="~/Controls/Images/card_visa.gif" runat="server"
                                   align="absmiddle" />
                                                <img id="imgMastercard" runat="server" align="absmiddle"
                                                    src="~/Controls/Images/card_mastercard.gif"></img>
                                                <img id="imgAmex" runat="server" align="absmiddle" src="~/Controls/Images/card_amex.gif"></img>
                                                <img id="imgDiscover" runat="server" align="absmiddle" src="~/Controls/Images/card_discover.gif"></img>


                                            </div>


                                        </div>


                                        <div>
                                            <asp:RequiredFieldValidator ID="req1" runat="server" ControlToValidate="txtCreditCardNumber"
                                                Display="Dynamic" meta:resourcekey="req1Resource1"></asp:RequiredFieldValidator>
                                        </div>
                                        <div>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCreditCardNumber"
                                                Display="Dynamic" meta:resourcekey="RegularExpressionValidator1Resource1"></asp:RegularExpressionValidator>
                                        </div>
                                        <div>
                                            <ZNode:ZNodeCreditCardValidator ID="MyValidator" runat="server" AcceptedCardTypes="All, Unknown"
                                                ControlToValidate="txtCreditCardNumber" Display="Dynamic" meta:resourcekey="MyValidatorResource1"
                                                ValidateCardType="True"></ZNode:ZNodeCreditCardValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="Row Clear">
                                    <div class="FieldStyle LeftContent" style="text-align: left;">
                                        <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtExpirationDate"></asp:Localize>
                                    </div>
                                    <div class="LeftContent" style="width: 60%;">
                                        <div style="float: left;">
                                            <div>
                                                <asp:DropDownList ID="lstMonth" runat="server" meta:resourcekey="lstMonthResource1">
                                                    <asp:ListItem Value="0" meta:resourcekey="ListItemResource1">-- Month --</asp:ListItem>
                                                    <asp:ListItem Value="01" meta:resourcekey="ListItemResource2">Jan</asp:ListItem>
                                                    <asp:ListItem Value="02" meta:resourcekey="ListItemResource3">Feb</asp:ListItem>
                                                    <asp:ListItem Value="03" meta:resourcekey="ListItemResource4">Mar</asp:ListItem>
                                                    <asp:ListItem Value="04" meta:resourcekey="ListItemResource5">Apr</asp:ListItem>
                                                    <asp:ListItem Value="05" meta:resourcekey="ListItemResource6">May</asp:ListItem>
                                                    <asp:ListItem Value="06" meta:resourcekey="ListItemResource7">Jun</asp:ListItem>
                                                    <asp:ListItem Value="07" meta:resourcekey="ListItemResource8">Jul</asp:ListItem>
                                                    <asp:ListItem Value="08" meta:resourcekey="ListItemResource9">Aug</asp:ListItem>
                                                    <asp:ListItem Value="09" meta:resourcekey="ListItemResource10">Sep</asp:ListItem>
                                                    <asp:ListItem Value="10" meta:resourcekey="ListItemResource11">Oct</asp:ListItem>
                                                    <asp:ListItem Value="11" meta:resourcekey="ListItemResource12">Nov</asp:ListItem>
                                                    <asp:ListItem Value="12" meta:resourcekey="ListItemResource13">Dec</asp:ListItem>
                                                </asp:DropDownList>
                                                <div>
                                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" ControlToValidate="lstMonth"
                                                        runat="server" Display="Dynamic" InitialValue="0" ErrorMessage="Select Month"></asp:RequiredFieldValidator>
                                                </div>
                                            </div>
                                        </div>

                                        <div>
                                            <asp:DropDownList ID="lstYear" runat="server" meta:resourcekey="lstYearResource1">
                                            </asp:DropDownList>
                                            <div>
                                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" ControlToValidate="lstYear"
                                                    runat="server" Display="Dynamic" InitialValue="0" ErrorMessage="Select Year"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="Row Clear">
                                    <div class="FieldStyle LeftContent" style="text-align: left;">
                                        <asp:Localize ID="Localize6" runat="server" meta:resourceKey="txtSecurityCode"></asp:Localize>
                                    </div>
                                    <div class="LeftContent" style="width: 60%;">
                                        <asp:TextBox ID="txtCVV" runat="server" Width="30px" Columns="30" MaxLength="4" autocomplete="off"
                                            meta:resourcekey="txtCVVResource1" onblur="clearErrorMessage()"></asp:TextBox>&nbsp;&nbsp;
                    <asp:HyperLink ID="CVVhyperlink" EnableViewState="false" runat="server" Text="help"
                        meta:resourcekey="CVVhyperlinkResource1"></asp:HyperLink>
                                        <div>
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" ControlToValidate="txtCVV"
                                                runat="server" Display="Dynamic" meta:resourcekey="Requiredfieldvalidator2Resource1"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtCVV"
                                                Display="Dynamic" meta:resourcekey="RegularExpressionValidator2Resource1"></asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="Row Clear">
                                    <div class="FieldStyle LeftContent">
                                    </div>
                                    <div class="ChkField">
                                        <asp:CheckBox ID="chkSaveCardData" Visible="true" runat="server" Text=" Keep this credit card on file for future purchases" />
                                    </div>
                                </div>
                                <div class="Row Clear">
                                    <div style="padding-top: 2px; padding-bottom: 2px;">
                                        <asp:Label CssClass="Error" ID="lblPaypalError" runat="server" EnableViewState="False"
                                            meta:resourcekey="lblErrorResource1"></asp:Label>
                                    </div>
                                </div>
                            </asp:Panel>

                        </td>
                        <td style="vertical-align: top;">
                            <div>
                                <asp:HiddenField ID="hfPaymentSettingID" Value="0" runat="server" />
                                <%--Paypal Payment Section--%>
                                <asp:Panel ID="pnlPaypal" runat="server" Visible="false">                                    
                                    <div class="PaymentSection" style="min-width:100px;">                                        
                                        <p style="float:right;">
                                        <asp:ImageButton runat="server" EnableViewState="false" CausesValidation="false"
                                            ID="Paypal" ImageUrl="~/Themes/Default/Images/paypal.png"
                                            AlternateText="Acceptance Mark" ImageAlign="AbsMiddle" OnClick="Paypal_Click" />
                                        </p>
                                        <p style="color: green;width:145px; ">
                                            <asp:Localize ID="Localize21" runat="server" meta:resourceKey="lblPayPalInfoNote"></asp:Localize>
                                        </p>
                                        <br />

                                    </div>
                                </asp:Panel>
                                <div class="Clear">
                                </div>
                                <%-- Google Checkout Payment Section --%>
                                <asp:Panel ID="pnlGoogleCheckout" runat="server" Visible="false">
                                    <div class="FieldStyle LeftContent">
                                        <asp:Localize ID="Localize15" runat="server"></asp:Localize>
                                    </div>
                                    <div class="PaymentSection">
                                        <asp:ImageButton EnableViewState="true" ID="GoogleSubmitButton" CausesValidation="false"
                                            ImageUrl="https://checkout.google.com/buttons/checkout.gif?merchant_id={0}&w=160&h=43&style=white&variant=text&loc=en_US"
                                            runat="server" ImageAlign="AbsMiddle" OnClick="GoogleSubmitButton_Click" />
                                        <br />
                                    </div>
                                </asp:Panel>
                                <div class="Clear">
                                </div>
                                <%-- 2Checkout Payment Section --%>
                                <asp:HiddenField ID="hdn2COpaymentSettingID" Value="0" runat="server" />
                                <asp:Panel ID="pnl2CO" runat="server" Visible="false">
                                    <div class="FieldStyle LeftContent">
                                        <asp:Localize ID="Localize17" runat="server"></asp:Localize>
                                    </div>
                                    <div class="PaymentSection">
                                        <asp:LinkButton EnableViewState="true" ID="TwoCheckoutSubmitButton" Text="Pay via 2CO"
                                            OnClick="TwoCheckoutSubmitButton_Click" runat="server" CssClass="Button" CausesValidation="False" />
                                        <br />
                                    </div>
                                </asp:Panel>

                            </div>
                        </td>
                    </tr>

                </table>
                <asp:Panel ID="pnlCOD" runat="server" Visible="False" meta:resourcekey="pnlCODResource1">
                    <asp:Localize ID="Localize8" runat="server" meta:resourceKey="txtHint1"></asp:Localize>
                </asp:Panel>
                <asp:Panel ID="pnlIPCCreditCard" runat="server" Visible="False" meta:resourcekey="pnlIPCCreditCardResource1">
                    <asp:Localize ID="Localize9" runat="server" meta:resourceKey="txtTBD"></asp:Localize>
                </asp:Panel>
                <div class="Clear">
                </div>

                <asp:Panel ID="pnlGiftCard" runat="server" meta:resourcekey="pnlGiftCardResource1" DefaultButton="btnApplyGiftCard">
                    <div class="Form">
                        <div class="HeaderStyle">
                            <asp:Localize ID="Localize19" runat="server" meta:resourceKey="txtGiftCard"></asp:Localize>
                        </div>

                        <div class="FieldStyle LeftContent">
                        </div>
                        <div class="LeftContent">
                            <uc1:spacer ID="Spacer3" EnableViewState="false" SpacerHeight="10" SpacerWidth="5"
                                runat="server"></uc1:spacer>
                        </div>

                        <div style="clear: both;"></div>

                        <asp:Panel ID="GiftCardPanel" runat="server">
                            <div class="Form" style="padding-bottom: 0px;">
                                <!-- Gift Card Section -->
                                <asp:Panel ID="pnlGiftCardNumber" runat="server">
                                    <div>
                                        <div class="FieldStyle LeftContent" style="text-align: left;">
                                            <asp:Localize ID="Localize13" runat="server" meta:resourceKey="lblGiftCardNumber"></asp:Localize>
                                        </div>

                                        <div class="ValueStyle LeftContent">
                                            <asp:TextBox ID="txtGiftCardNumber" runat="server" Width="130"
                                                Columns="30" MaxLength="100" ValidationGroup="GiftCard"></asp:TextBox>
                                        </div>
                                        <div class="ValueStyle LeftContent">
                                            <asp:LinkButton ID="btnApplyGiftCard" runat="server" AlternateText="Apply" CssClass="Button"
                                                ValidationGroup="GiftCard" OnClick="BtnApplyGiftCard_Click">
                                                <asp:Localize ID="Localize14" runat="server" meta:resourceKey="lblApply"></asp:Localize>
                                            </asp:LinkButton>
                                        </div>
                                        <div class="FieldStyle LeftContent" style="clear: both; text-align: left; padding-bottom: 0px;">
                                            &nbsp;
                                        </div>
                                        <div class="ValueStyle LeftContent" style="text-align: left; width: auto;">
                                            <asp:Label ID="lblGiftCardMessage" runat="server" CssClass="Error" Text=""></asp:Label>
                                        </div>

                                    </div>
                                </asp:Panel>
                            </div>
                        </asp:Panel>
                    </div>
                </asp:Panel>

                <div class="GiftcardSpace">
                    <uc1:spacer ID="Spacer9" EnableViewState="false" SpacerHeight="2" SpacerWidth="5"
                        runat="server" />
                    <div class="HeaderStyle">
                        <asp:Localize ID="Localize10" runat="server" meta:resourceKey="txtHint2"></asp:Localize>
                    </div>
                    <div>
                        <uc1:spacer ID="Spacer15" EnableViewState="false" SpacerHeight="15" SpacerWidth="5"
                            runat="server" />
                        <asp:TextBox Columns="45" Rows="3" ID="txtAdditionalInstructions" runat="server"
                            TextMode="MultiLine" meta:resourcekey="txtAdditionalInstructionsResource1"></asp:TextBox>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
