<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Checkout_Address"
    CodeBehind="Address.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<%@ Register Src="StepTracker.ascx" TagName="StepTracker" TagPrefix="uc6" %>
<asp:UpdatePanel runat="server" ID="upCheckoutAddress">
    <ContentTemplate>
        <div class="Checkout">
            <div class="PageTitle">
                <asp:Localize ID="Localize17" runat="server" meta:resourceKey="txtSecureCheckout"></asp:Localize></div>
            <div class="Overview">
                <asp:Localize ID="Localize27" runat="server" meta:resourceKey="txtOverView">
                </asp:Localize>
            </div>
            <div class="Steps">
                <uc6:StepTracker ID="uxStepTracker" runat="server" />
            </div>
            <div class="Error">
                <asp:Localize ID="lblError" runat="server"></asp:Localize>
            </div>
		<asp:Panel runat="server" ID="pnlCheckoutAddress" DefaultButton="btnProceedToNext">
            <div class="Form Row Clear">
                <div class="LeftContent" style="width: 45%;">
                    <div class="FormTitle">
                        <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtBillingAddress"></asp:Localize></div>
                    <div class="Form">
                        <br />
                        <div class="Row Clear" runat="server" id="divBillingAddressName">
                            <div class="FieldStyle AddressLeftContent">
                                <asp:Localize ID="Localize25" runat="server" Text="<%$ Resources:CommonCaption, AddressName %>"></asp:Localize>
                            </div>
                            <div class="AddressLeftContent">
                                <asp:DropDownList ID="ddlBillingAddressName" Width="180px" runat="server" AutoPostBack="True"
                                    OnSelectedIndexChanged="DdlAddressName_SelectedIndexChanged"></asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator16" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired %>"
                                    ControlToValidate="ddlBillingAddressName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                    &nbsp;&nbsp;<asp:LinkButton runat="server" Text="Add Address" ID="lnkAddress" OnClick="lnkAddress_Click"></asp:LinkButton>
                            </div> 
                            
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle AddressLeftContent">
                            <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtFirstName"></asp:Localize>
                        </div>
                        <div class="AddressLeftContent">
                            <asp:TextBox ID="txtBillingFirstName" runat="server" Width="130px" Columns="30" MaxLength="100"
                                meta:resourcekey="txtBillingFirstNameResource1"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="req1" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired %>"
                                        ControlToValidate="txtBillingFirstName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle AddressLeftContent">
                            <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtLastName"></asp:Localize>
                        </div>
                        <div class="AddressLeftContent">
                            <asp:TextBox ID="txtBillingLastName" runat="server" Width="130px" Columns="30" MaxLength="100"
                                meta:resourcekey="txtBillingLastNameResource1"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" ErrorMessage="<%$ Resources:CommonCaption, LastNameRequired  %>"
                                        ControlToValidate="txtBillingLastName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle AddressLeftContent">
                            <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtCompanyName"></asp:Localize>
                        </div>
                        <div class="AddressLeftContent">
                            <asp:TextBox ID="txtBillingCompanyName" runat="server" Width="130px" Columns="30"
                                MaxLength="100" meta:resourcekey="txtBillingCompanyNameResource1"></asp:TextBox>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle AddressLeftContent">
                            <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:CommonCaption, Street1 %>"></asp:Localize>
                        </div>
                        <div class="AddressLeftContent">
                            <asp:TextBox ID="txtBillingStreet1" runat="server" Width="130px" Columns="30" MaxLength="100"
                                meta:resourcekey="txtBillingStreet1Resource1"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" ErrorMessage="<%$ Resources:CommonCaption, Street1Required %>"
                                        ControlToValidate="txtBillingStreet1" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle AddressLeftContent">
                            <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:CommonCaption, Street2 %>"></asp:Localize>
                        </div>
                        <div class="AddressLeftContent">
                            <asp:TextBox ID="txtBillingStreet2" runat="server" Width="130px" Columns="30" MaxLength="100"
                                meta:resourcekey="txtBillingStreet2Resource1"></asp:TextBox>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle AddressLeftContent">
                            <asp:Localize ID="Localize24" runat="server" meta:resourceKey="txtCity"></asp:Localize>
                        </div>
                        <div class="AddressLeftContent">
                            <asp:TextBox ID="txtBillingCity" runat="server" Width="130px" Columns="30" MaxLength="100"
                                meta:resourcekey="txtBillingCityResource1"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" ErrorMessage="<%$ Resources:CommonCaption, CityRequired %>"
                                        ControlToValidate="txtBillingCity" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle AddressLeftContent">
                            <asp:Localize ID="Localize9" runat="server" meta:resourceKey="txtState"></asp:Localize>
                        </div>
                        <div class="AddressLeftContent">
                            <asp:TextBox ID="txtBillingState" runat="server" Width="30px" Columns="10" MaxLength="2"
                                meta:resourcekey="txtBillingStateResource1"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" ErrorMessage="<%$ Resources:CommonCaption, StateRequired %>"
                                        ControlToValidate="txtBillingState" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle AddressLeftContent">
                            <asp:Localize ID="Localize10" runat="server" meta:resourceKey="txtPostalCode"></asp:Localize>
                        </div>
                        <div class="AddressLeftContent">
                            <asp:TextBox ID="txtBillingPostalCode" runat="server" Width="130px" Columns="30"
                                MaxLength="20" meta:resourcekey="txtBillingPostalCodeResource1"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" ErrorMessage="<%$ Resources:CommonCaption, PostalCodeRequired  %>"
                                        ControlToValidate="txtBillingPostalCode" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle AddressLeftContent">
                            <asp:Localize ID="Localize11" runat="server" meta:resourceKey="txtCountry"></asp:Localize>
                        </div>
                        <div class="AddressLeftContent">
                            <asp:DropDownList ID="lstBillingCountryCode" Width="180px" runat="server" 
                                AutoPostBack="false"  meta:resourcekey="lstBillingCountryCodeResource1" 
                                ontextchanged="lstBillingCountryCode_TextChanged" CausesValidation="true">
                            </asp:DropDownList>
                                              <div>
                            <asp:CustomValidator  ID="valCountry" ControlToValidate="lstBillingCountryCode"  OnServerValidate="validateCountry"
   Display="Dynamic" CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, CountryCodeRequired %>"
    runat="server"/>
                            </div>
                    </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle AddressLeftContent">
                            <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtPhoneNumber"></asp:Localize>
                        </div>
                        <div class="AddressLeftContent">
                            <asp:TextBox ID="txtBillingPhoneNumber" runat="server" Width="130px" Columns="30"
                                MaxLength="100" meta:resourcekey="txtBillingPhoneNumberResource1"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" ErrorMessage="<%$ Resources:CommonCaption, PhoneNoRequired  %>"
                                        ControlToValidate="txtBillingPhoneNumber" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                        </div>
                    </div>
                    <div class="Row Clear" runat="server" id="divEmailAddress">
                        <div class="FieldStyle AddressLeftContent">
                            <asp:Localize ID="Localize6" runat="server" meta:resourceKey="Email"></asp:Localize>
                        </div>
                        <div class="AddressLeftContent">
                            <asp:TextBox ID="txtEmailAddress" runat="server" Width="130px" Columns="30" MaxLength="100"></asp:TextBox><div>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator17" runat="server" Width="450px"
                                    ControlToValidate="txtEmailAddress" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:CommonCaption, EmailAddressRequired%>'>
                                </asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revEmail" runat="server" Width="10px" ControlToValidate="txtEmailAddress"
                                    ErrorMessage='<%$ Resources:CommonCaption, EmailValidErrorMessage %>' Display="Dynamic"
                                    ValidationExpression='<%$ Resources:CommonCaption, EmailValidationExpression%>'
                                    CssClass="Error">
                                </asp:RegularExpressionValidator></div>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle AddressLeftContent">
                        </div>
                        <div>
                            <asp:CheckBox ID="chkSameAsBilling" runat="server" Font-Bold="True" Text="<%$ Resources:CommonCaption, AddressAreSame %>"
                                OnCheckedChanged="ChkSameAsBilling_CheckedChanged" AutoPostBack="True" />
                        </div>
                    </div>
                </div>
                <div class="LeftContent" style="width: 10%">
                    &nbsp;</div>
                <div class="LeftContent" style="width: 45%;">
                    <asp:Panel ID="pnlShipping" runat="server" meta:resourcekey="pnlShippingResource1">
                        <div class="FormTitle">
                            <asp:Localize ID="Localize12" runat="server" meta:resourceKey="txtShippingAddress"></asp:Localize></div>
                        <br />
                        <div class="Form">
                            <div class="Row Clear" runat="server" id="divShippingAddressName">
                                <div class="FieldStyle LeftContent">
                                    <asp:Localize ID="Localize26" runat="server" Text="<%$ Resources:CommonCaption, AddressName %>"></asp:Localize>
                                </div>
                                <div class="LeftContent">
                                    <asp:DropDownList ID="ddlShippingAddressName" runat="server" AutoPostBack="True"
                                        Width="180px" OnSelectedIndexChanged="DdlAddressName_SelectedIndexChanged">
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvShippingAddress" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired %>"
                                        ControlToValidate="ddlShippingAddressName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent">
                                    <asp:Localize ID="Localize13" runat="server" meta:resourceKey="txtFirstName"></asp:Localize>
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtShippingFirstName" runat="server" Width="130px" Columns="30"
                                        MaxLength="100" meta:resourcekey="txtShippingFirstNameResource1"></asp:TextBox><div>
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired  %>"
                                                ControlToValidate="txtShippingFirstName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent">
                                    <asp:Localize ID="Localize14" runat="server" meta:resourceKey="txtLastName"></asp:Localize>
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtShippingLastName" runat="server" Width="130px" Columns="30" MaxLength="100"
                                        meta:resourcekey="txtShippingLastNameResource1"></asp:TextBox><div>
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" ErrorMessage="<%$ Resources:CommonCaption, LastNameRequired  %>"
                                                ControlToValidate="txtShippingLastName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent">
                                    <asp:Localize ID="Localize15" runat="server" meta:resourceKey="txtCompanyName"></asp:Localize>
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtShippingCompanyName" runat="server" Width="130px" Columns="30"
                                        MaxLength="100" meta:resourcekey="txtShippingCompanyNameResource1"></asp:TextBox>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent">
                                    <asp:Localize ID="Localize18" runat="server" Text="<%$ Resources:CommonCaption, Street1 %>"></asp:Localize>
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtShippingStreet1" runat="server" Width="130px" Columns="30" MaxLength="100"
                                        meta:resourcekey="txtShippingStreet1Resource1"></asp:TextBox><div>
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" ErrorMessage="<%$ Resources:CommonCaption, Street1Required %>"
                                                ControlToValidate="txtShippingStreet1" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent">
                                    <asp:Localize ID="Localize19" runat="server" Text="<%$ Resources:CommonCaption, Street2 %>"></asp:Localize>
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtShippingStreet2" runat="server" Width="130px" Columns="30" MaxLength="100"
                                        meta:resourcekey="txtShippingStreet2Resource1"></asp:TextBox>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent">
                                    <asp:Localize ID="Localize20" runat="server" meta:resourceKey="txtCity"></asp:Localize>
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtShippingCity" runat="server" Width="130px" Columns="30" MaxLength="100"
                                        meta:resourcekey="txtShippingCityResource1"></asp:TextBox><div>
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" ErrorMessage="<%$ Resources:CommonCaption, CityRequired %>"
                                                ControlToValidate="txtShippingCity" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent">
                                    <asp:Localize ID="Localize21" runat="server" meta:resourceKey="txtState"></asp:Localize>
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtShippingState" runat="server" Width="30px" Columns="10" MaxLength="2"
                                        meta:resourcekey="txtShippingStateResource1"></asp:TextBox><div>
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" ErrorMessage="<%$ Resources:CommonCaption, StateRequired %>"
                                                ControlToValidate="txtShippingState" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent">
                                    <asp:Localize ID="Localize22" runat="server" meta:resourceKey="txtPostalCode"></asp:Localize>
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtShippingPostalCode" runat="server" Width="130px" Columns="30"
                                        MaxLength="20" meta:resourcekey="txtShippingPostalCodeResource1"></asp:TextBox><div>
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator14" ErrorMessage="<%$ Resources:CommonCaption, PostalCodeRequired  %>"
                                                ControlToValidate="txtShippingPostalCode" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent">
                                    <asp:Localize ID="Localize23" runat="server" meta:resourceKey="txtCountry"></asp:Localize>
                                </div>
                                <div class="LeftContent">
                                    <asp:DropDownList ID="lstShippingCountryCode" Width="180px" runat="server" meta:resourcekey="lstShippingCountryCodeResource1">
                                    </asp:DropDownList>
                                    <div>
                                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator15" ErrorMessage="<%$ Resources:CommonCaption, CountryCodeRequired %>"
                                            ControlToValidate="lstShippingCountryCode" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent">
                                    <asp:Localize ID="Localize16" runat="server" meta:resourceKey="txtPhoneNumber"></asp:Localize>
                                </div>
                                <div class="LeftContent">
                                    <asp:TextBox ID="txtShippingPhoneNumber" runat="server" Width="130px" Columns="30"
                                        MaxLength="100" meta:resourcekey="txtShippingPhoneNumberResource1"></asp:TextBox><div>
                                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" ErrorMessage="<%$ Resources:CommonCaption, PhoneNoRequired  %>"
                                                ControlToValidate="txtShippingPhoneNumber" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="LeftContent" style="width: 10%">
                    &nbsp;</div>
            <ZNode:Spacer ID="Spacer1" EnableViewState="false" SpacerHeight="5" SpacerWidth="5" runat="server"></ZNode:Spacer>
            <div style="text-align:right">
				<asp:Button ID="btnProceedToNext" runat="server"  onclick="btnProceedToNext_Click" CssClass="ProceedtoNextButton" Text="Proceed to Next Step"></asp:Button>	
            </div>
      
</asp:Panel>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
