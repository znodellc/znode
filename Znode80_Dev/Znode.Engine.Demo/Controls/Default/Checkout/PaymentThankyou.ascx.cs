﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.Paypal;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the PaymentThankyou user control class.
    /// </summary>
    public partial class Controls_Default_Checkout_PaymentThankyou : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private string _paymentGateway = string.Empty;
        private string _token = string.Empty;
        private string _payerID = string.Empty;
        private ZNodeShoppingCart _shoppingCart = null;
        private ZNodeUserAccount _userAccount = null;
        private PaymentSetting _paymentSetting;
        #endregion

        #region Protected Properties
        /// <summary>
        /// Gets the unique token (i.e transaction id)
        /// </summary>
        protected string Token
        {
            get
            {
                if (this._token.Length == 0)
                {
                    this._token = this.Request.QueryString.Get("token");
                }

                return this._token;
            }
        }

        /// <summary>
        /// Gets the gateway type like google,paypal
        /// </summary>
        protected string Mode
        {
            get
            {
                return this.Request["mode"];
            }
        }

        /// <summary>
        /// Gets the unique Payer id
        /// </summary>
        protected string PayerId
        {
            get
            {
                if (this._payerID.Length == 0)
                {
                    this._payerID = this.Request.QueryString.Get("payerid");
                }

                return this._payerID;
            }
        }

        #endregion

        #region Page Load Event

        /// <summary>
        /// Page Load Event    
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this._paymentGateway = this.Mode;

                if (this._paymentGateway.Length > 0)
                {
                    if (this._paymentGateway.Equals("Paypal"))
                    {
                        if (HttpContext.Current.Session[ZNodeSessionKeyType.ShoppingCart.ToString()] != null)
                        {
                            this._shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
                        }

                        if (this._shoppingCart == null)
                        {
                            Response.Redirect("~/");
                        }

                        try
                        {
                            this._paymentSetting = this.GetByPaymentTypeId((int)ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL);
                            PaypalGateway paypal = new PaypalGateway(this._paymentSetting);
                            paypal.OrderTotal = this._shoppingCart.Total;
                            ZNode.Libraries.ECommerce.Entities.ZNodePaymentResponse response = paypal.DoExpressCheckoutPayment(this.Token, this.PayerId);
                            lblMsg.Text = this.GetLocalResourceObject("TrackByPaypal").ToString();

                            if (response.ResponseCode != "0")
                            {
                                lblMsg.Text = Resources.CommonCaption.OrderSubmitFailed;
                            }
                        }
                        catch
                        {
                            lblMsg.Text = Resources.CommonCaption.OrderSubmitFailed;
                        }
                    }
                    else if (this._paymentGateway.Equals("2CO"))
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (string key in Request.Form.Keys)
                        {
                            sb.AppendFormat("\n{0}={1}", key, Request.Form[key]);
                        }

                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(sb.ToString());
                        pnlConfirmation.Visible = true;
                        lblMsg.Text = this.GetLocalResourceObject("TrackBy2CO").ToString();
                    }
                    else
                    {
                        pnlConfirmation.Visible = true;
                        lblMsg.Text = this.GetLocalResourceObject("TrackByGoogle").ToString();
                    }
                }
                else
                {
                    Response.Redirect("~/");
                }

                // Empty cart & logoff user
                Session.Abandon();
                FormsAuthentication.SignOut();
            }
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Returns an instance of the PaymentSetting  class.
        /// </summary>
        /// <param name="paymentTypeID">Payment Type Id</param>
        /// <returns>Returns the Payment Setting</returns>
        private PaymentSetting GetByPaymentTypeId(int paymentTypeID)
        {
            PaymentSettingService _pmtServ = new PaymentSettingService();
            int? profileID = 0;

            if (HttpContext.Current.Session[ZNodeSessionKeyType.UserAccount.ToString()] != null)
            {
                this._userAccount = ZNodeUserAccount.CurrentAccount();
            }

            if (this._userAccount != null)
            {
                // Get loggedIn user profile
                profileID = this._userAccount.ProfileID; 
            }

            TList<PaymentSetting> list = _pmtServ.GetByPaymentTypeID(paymentTypeID);
            list.Filter = "ActiveInd = true AND ProfileID = " + profileID;

            if (list.Count == 0)
            {
                // Get All Profiles payment setting
                list.Filter = "ActiveInd = true AND ProfileID = null";
            }

            return list[0];
        }
        #endregion
    }
}