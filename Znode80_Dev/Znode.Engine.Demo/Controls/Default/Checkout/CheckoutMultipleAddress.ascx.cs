﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Cart user control class.
    /// </summary>
    public partial class Controls_Default_Checkout_CheckoutMultipleAddress : System.Web.UI.UserControl
    {
        #region Member Variables
        private ZNodeShoppingCart shoppingCart;
        private ZNodeUserAccount userAccount;
        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            // Registers the event for the shopping cart (child) control.
            this.uxCart.CartItem_RemoveLinkClicked += new EventHandler(this.CartItem_RemoveLinkClicked);
            this.Page.Title = "Ship to Multiple Address";
        }

        /// <summary>
        /// On Pre Render Method
        /// </summary>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            if (this.userAccount == null)
            {
                this.userAccount = ZNodeUserAccount.CurrentAccount();
            }

            if (this.shoppingCart != null)
            {
                this.Bind();
				addressLink.HRef = "~/Address.aspx?itemid=" + userAccount.BillingAddress.AddressID + "&returnurl=CheckoutMultipleAddress.aspx";
            }
            else
            {
                pnlShoppingCart.Visible = false;
                Response.Redirect("~/Shoppingcart.aspx");
            }         

        }

        /// <summary>
        /// Checkout button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Checkout_Click(object sender, EventArgs e)
        {
            if (uxCart.UpdateOrderShipment())
            {

                ZNodeShoppingCart ShoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
                string link = "~/Checkout.aspx";
                if (ShoppingCart == null)
                {
                    link = "~/Shoppingcart.aspx";
                }
             
                Response.Redirect(link);
            }
           
        }


        /// <summary>
        /// Fired when child Control Remove link triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CartItem_RemoveLinkClicked(object sender, EventArgs e)
        {
            string cartCount = "0";

            ZNodeShoppingCart ShoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);

            if (ShoppingCart != null)
            {
                cartCount = ShoppingCart.ShoppingCartItems.Count.ToString();
            }
            else
            {
                cartCount = "0";
            }

            UserControl cartItemCount = (UserControl)this.Page.Master.Master.FindControl("CART_ITEM_COUNT");
            Label lblCartCount = (Label)cartItemCount.FindControl("lblCartItemCount");
            lblCartCount.Text = cartCount.ToString();
        }

        #endregion

        #region Methods
        /// <summary>
        /// Bind All Controls
        /// </summary>
        protected void Bind()
        {
            // Show/hide cart
            if (this.shoppingCart.Count > 0)
            {
                pnlShoppingCart.Visible = true;
                uxMsg.Text = string.Empty;
            }
            else
            {
                Response.Redirect("~/Shoppingcart.aspx");
            }

            // Bind grid
            uxCart.Bind();
        }
        #endregion

    }
}