using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

/// <summary>
/// Checkout Step Tracker
/// </summary>
namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the StepTracker user control class.
    /// </summary>
    public partial class Controls_Default_Checkout_StepTracker : System.Web.UI.UserControl
    {
        #region Private Variables
        private int _step = 0;
        private string _Step1Css;
        private string _Step2Css;

        /// <summary>
        /// Gets or Sets the step 1 css class
        /// </summary>
        public string Step1Css
        {
            get { return _Step1Css; }
            set { _Step1Css = value; }
        }

        /// <summary>
        /// Gets or Sets the step 2 css class
        /// </summary>
        public string Step2Css
        {
            get { return _Step2Css; }
            set { _Step2Css = value; }
        }

        #endregion

        #region Public Properties
        public int Step
        {
            set
            {
                this._step = value;

                // First de-activate steps
                this.MakeAllStepsPassive();

                if (this._step == 1)
                {
                    this.Step1Css= "Active";
                }
                else if (this._step == 2)
                {
                    this.Step2Css= "Active";
                }
                else
                {
                    this.Step1Css= "Active";
                }
            }
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        #endregion

        #region Helper Functions
        private void MakeAllStepsPassive()
        {
            this.Step1Css = "Passive";
            this.Step2Css= "Passive";
        }
        #endregion
    }
}