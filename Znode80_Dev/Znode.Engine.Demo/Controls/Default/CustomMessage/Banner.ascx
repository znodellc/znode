﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Banner.ascx.cs" Inherits="Znode.Engine.Demo.Controls_Default_CustomMessage_Banner" %>

<div id="BannerItems" class="BannerItems">
    <asp:Repeater runat="server" ID="RptBanner">
        <ItemTemplate>            
            <div class="rotator" style="width:750px;">
              <asp:Label runat="server" Text='<%# HttpUtility.HtmlDecode(DataBinder.Eval(Container.DataItem, "Value").ToString()) %>'></asp:Label>            
            </div>                
        </ItemTemplate>
    </asp:Repeater>
</div>

<script type="text/javascript">
    var $j = jQuery.noConflict();

    (function($j) {
        $j.fn.fadeTransition = function(options) {
            var options = $.extend({ pauseTime: 5000, transitionTime: 2000, ignore: null, delayStart: 0, pauseNavigation: false }, options);
            var transitionObject;

            Trans = function(obj) {
                var timer = null;
                var current = 0;
                var els = (options.ignore) ? $j("> *:not(" + options.ignore + ")", obj) : $j("> *", obj);
                $j(obj).css("position", "relative");
                els.css("display", "none").css("left", "0").css("top", "0").css("position", "absolute");

                if (options.delayStart > 0) {
                    setTimeout(showFirst, options.delayStart);
                }
                else
                    showFirst();

                function showFirst() {
                    if (options.ignore) {
                        $j(options.ignore, obj).fadeOut(options.transitionTime);
                        $j(els[current]).fadeIn(options.transitionTime);
                    }
                    else {
                        $j(els[current]).css("display", "block");
                    }
                }

                function transition(next) {
                    $j(els[current]).fadeOut(options.transitionTime);
                    $j(els[next]).fadeIn(options.transitionTime);
                    current = next;
                    cue();
                };

                function cue() {
                    if ($j("> *", obj).length < 2) return false;
                    if (timer) clearTimeout(timer);
                    if (!options.pauseNavigation) {
                        timer = setTimeout(function() { transition((current + 1) % els.length | 0) }, options.pauseTime);
                    }
                };

                this.showItem = function(item) {
                    if (timer) clearTimeout(timer);
                    transition(item);
                };

                cue();
            }

            this.showItem = function(item) {
                transitionObject.showItem(item);
            };

            return this.each(function() {
                transitionObject = new Trans(this);
            });
        }

    })(jQuery);

    var page = {
        tr: null,
        init: function() {
            page.tr = $j(".BannerItems").fadeTransition({ pauseTime: 5000, transitionTime: 1000, ignore: "#introslide", delayStart: 0 });
        },

        show: function(idx) {
            if (page.tr.timer) clearTimeout(page.tr.timer);
            page.tr.showItem(idx);
        }
    };

    $j(document).ready(SetBannerHeight);

    function SetBannerHeight() {
        var bannerItems = document.getElementById("BannerItems");

        var height = 0;
        for (i = 0; i < bannerItems.childNodes.length; i++) {
            contentHeight = bannerItems.childNodes[i].offsetHeight;
            if (height < contentHeight) {
                height = contentHeight;
            }
        }

        bannerItems.style.height = height + 'px';

        page.init();
    }
</script>
