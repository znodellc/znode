using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Custom Message user control class.
    /// </summary>
    public partial class Controls_Default_CustomMessage_CustomMessage : System.Web.UI.UserControl
    {
        public string MessageKey
        {
            set
            {
                MessageConfigAdmin messageadmin = new MessageConfigAdmin();
                MessageConfig messageconfig = new MessageConfig();
                string key = value;
                if (key != null)
                {
                    messageconfig = messageadmin.GetByKeyPortalIDLocaleID(key, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

                    if (messageconfig != null)
                    {
                        lblMsg.Text = messageconfig.Value;
                    }
                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}