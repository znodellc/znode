<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_EmailFriend_EmailSignup" CodeBehind="EmailSignup.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<asp:UpdatePanel runat="server" ID="updatePnlNewsLetterSignup">
	<ContentTemplate>
<div class="NewsletterSignup">
    <div class="PageTitle">
        <uc1:CustomMessage ID="CustomMessage1" MessageKey="NewsletterSignupTitle" runat="server" />
    </div>
	
    <asp:Panel ID="pnlContact" runat="server" meta:resourcekey="pnlContactResource1" >
        <p>
            <uc1:CustomMessage ID="CustomMessage2" MessageKey="NewsletterSignupIntroText" runat="server" />
        </p>

        <div class="Form">

            <asp:Panel ID="pnlNewsLetterSignup" runat="server" DefaultButton="Button1" >

                <div class="Row">
                    <div align="right" class="FieldStyle LeftContent">
                        <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:CommonCaption, FirstName%>"></asp:Localize>
                    </div>
                    <div>
                        <asp:TextBox ID="txtFirstname" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div class="Clear">
                    <ZNode:Spacer ID="Spacer6" EnableViewState="false" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:Spacer>
                </div>
                <div class="Row">
                    <div align="right" class="FieldStyle LeftContent">   
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:CommonCaption, LastName%>"></asp:Localize>
                    </div>
                    <div>
                        <asp:TextBox ID="txtLastname" runat="server" meta:resourcekey="txtLastnameResource1"></asp:TextBox>
                    </div>
                </div>
                <div class="Clear">
                    <ZNode:Spacer ID="Spacer5" EnableViewState="false" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:Spacer>
                </div>
                <div class="Row">
                    <div align="right" class="FieldStyle LeftContent">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:CommonCaption, EmailID%>"></asp:Localize>
                    </div>
                    <div>
                        <asp:TextBox ID="txtEmail" runat="server" meta:resourcekey="txtEmailResource1"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail"
                            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired%>"
                            ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired%>"></asp:RequiredFieldValidator>

                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEmail"
                            CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired%>"
                            ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired%>" ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression%>"
                            Display="Dynamic"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="Clear">
                    <ZNode:Spacer ID="Spacer4" EnableViewState="false" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:Spacer>
                </div>
                <div>
                    <div align="right" class="FieldStyle LeftContent">
                        &nbsp;
                    </div>
                    <div>
                        <ZNode:Spacer ID="Spacer" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server" />
	                    <asp:Button  runat="server" ID="Button1" OnClick="Btnsignup_Click" Text="<%$ Resources:CommonCaption, Submit %>"
	                                 CssClass="button" meta:resourcekey="Button1Resource1"/>
                    </div>
                </div>
                <ZNode:Spacer ID="Spacer3" EnableViewState="false" SpacerHeight="20" SpacerWidth="10" runat="server" />

            </asp:Panel>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlConfirm" runat="server" Visible="False" CssClass="SuccessMsg" meta:resourcekey="pnlConfirmResource1">
        <ZNode:Spacer ID="Spacer2" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server" />
        <p>
            <uc1:CustomMessage ID="CustomMessage3" MessageKey="NewsletterSignupConfirmationIntroText"
                runat="server" />
        </p>
        <asp:Label ID="ErrorMessage" runat="server" Visible="False" meta:resourcekey="ErrorMessageResource1"></asp:Label>
        <ZNode:Spacer ID="Spacer1" EnableViewState="false" SpacerHeight="50" SpacerWidth="10" runat="server" />
    </asp:Panel>
</div>
		</ContentTemplate>
</asp:UpdatePanel>
