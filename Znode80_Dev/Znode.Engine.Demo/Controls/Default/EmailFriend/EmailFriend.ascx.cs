using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Email Friend user control class.
    /// </summary>
    public partial class Controls_Default_EmailFriend_EmailFriend : System.Web.UI.UserControl
    {
        #region Protected Variables
        private int ProductID = 0;
        private ZNodeProduct _product;
        #endregion

        #region Helper Methods
        /// <summary>
        /// Gets product link
        /// </summary>
        /// <returns></returns>
        public string ProductUrl
        {
            get
            {
                if (this._product != null)
                {
                    return this._product.ViewProductLink;
                }

                return "/product.aspx?zpid=" + this.ProductID;
            }
        }

        /// <summary>
        /// Get product Name
        /// </summary>
        /// <returns>Returns the Product Name</returns>
        public string GetProductName()
        {
            if (this._product != null)
            {
                return this._product.Name;
            }

            return string.Empty;
        }
        #endregion

        #region General Events
        /// <summary>
        /// Email Send Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void But_Send_Click(object sender, EventArgs e)
        {
            try
            {
                string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);

                StringBuilder build = new StringBuilder("<font famliy=\"Verdana\" size=\"3\" weight=\"normal\">");
                build.Append(string.Format(
                      this.GetLocalResourceObject("MailContent").ToString(),
                      "<a href='" + domainPath + Response.ApplyAppPathModifier(this.ProductUrl) + "' target='_blank'>" + this.GetProductName() + "</a>",
                      "<a href=" + domainPath + Response.ApplyAppPathModifier("default.aspx") + ">" + ZNodeConfigManager.SiteConfig.CompanyName + "</a></font>"));

                ZNodeEmail.SendEmail(Email.Text.Trim(), FromEmailID.Text.Trim(), string.Empty, "Product Link", build.ToString(), true);

                pnlConfirm.Visible = true;
                pnlEmailFriend.Visible = false;

                lblMessage.Text = this.GetLocalResourceObject("MailSent").ToString();
                lblMessage.CssClass = "SuccessMsg";
            }
            catch (Exception)
            {
                pnlConfirm.Visible = true;
                lblMessage.Text = this.GetLocalResourceObject("MailSendFailed").ToString();
                lblMessage.CssClass = "Error";
            }
        }

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Back_Click(object sender, EventArgs e)
        {
            Response.Redirect(this._product.ViewProductLink);
        }

        #endregion

        #region Page Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["zpid"] != null)
            {
                this.ProductID = int.Parse(Request.QueryString["zpid"]);
            }
            else
            {
                throw new ApplicationException("Invalid Product Id");
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._product = (ZNodeProduct)HttpContext.Current.Items["Product"];
            lblProductName.Text = this.GetProductName();

            if (this.Page.Title != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtEmailaFriend.Text");
            }
        }
        #endregion
    }
}