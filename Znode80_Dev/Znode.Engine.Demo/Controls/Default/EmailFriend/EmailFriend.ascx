<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_EmailFriend_EmailFriend" Codebehind="EmailFriend.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<asp:UpdatePanel runat="server" ID="updatePnlEmailFriend">
	<ContentTemplate>
<div class="PageTitle">
    <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtEmailaFriend"></asp:Localize></div>
<div class="Form">
    <asp:Panel ID="pnlEmailFriend" runat="server" meta:resourcekey="pnlEmailFriendResource1" DefaultButton="but_Send">
        <div class="Form">
            <div class="Row">
                <div class="FieldStyle LeftContent">
                    <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtProduct"></asp:Localize>
                </div>
                <div class="ValueField ">
                    <asp:Label ID="lblProductName" EnableViewState="false" runat="server"></asp:Label>
                </div>
            </div>
            <div class="Clear">
                <ZNode:Spacer ID="Spacer6" EnableViewState="false" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:Spacer>
            </div>
            
            <div class="Row">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="FromEmailLabel" runat="server" AssociatedControlID="FromEmailID" meta:resourceKey="txtYourEmail"></asp:Localize>
                    </asp:Label>
                </div>
                <div class="ValueField">
                    <asp:TextBox ID="FromEmailID" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="FromEmailID"
                        ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired%>" ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired%>"
                        CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="FromEmailID"
                        CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired%>"
                        ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired%>" ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression%>"
                        Display="Dynamic"></asp:RegularExpressionValidator>
                </div>
            </div>
            
             <div class="Clear">
                <ZNode:Spacer ID="Spacer3" EnableViewState="false" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:Spacer>
            </div>
            
            <div class="Row">
                <div class="FieldStyle LeftContent">
                    <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email" meta:resourceKey="txtFriendsEmail"></asp:Label>
                </div>
                <div class="ValueField">
                    <asp:TextBox ID="Email" runat="server" meta:resourceKey="EmailResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email"
                        ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired%>" ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired%>"
                        CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="Email"
                        CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired%>"
                        ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired%>" ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression%>"
                        Display="Dynamic"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="Row">
                <div class="FieldStyle LeftContent">
                </div>
                <div>
                    
					<asp:Button ID="but_Send" runat="server" CssClass="button" OnClick="But_Send_Click"
						Text="Send" meta:resourceKey="but_SendResource1"/>
                </div>
            </div>
        </div>
        
    </asp:Panel>
    <asp:Panel ID="pnlConfirm" runat="server" Visible="False" meta:resourcekey="pnlConfirmResource1">
        <asp:Label ID="lblMessage" runat="server"></asp:Label>
        <br />
        <ZNode:Spacer ID="Spacer2" EnableViewState="false" SpacerHeight="20" SpacerWidth="10"
            runat="server" />
        &lt;
        <asp:LinkButton ID="BackLink" CssClass="BackLink" runat="server" OnClick="Back_Click"
            CausesValidation="False" meta:resourceKey="txtBackToProduct">
            <ZNode:Spacer ID="Spacer1" SpacerHeight="10" EnableViewState="false" SpacerWidth="10"
                runat="server" />
        </asp:LinkButton>
    </asp:Panel>
</div>
<ZNode:Spacer ID="Spacer" SpacerHeight="1" EnableViewState="false" SpacerWidth="10"
    runat="server" />
	</ContentTemplate>
	</asp:UpdatePanel>
