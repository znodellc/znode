using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Email Signup user control class.
    /// </summary>
    public partial class Controls_Default_EmailFriend_EmailSignup : System.Web.UI.UserControl
    {
        #region Private Variable
        private string _sourceText = "Email Signup";
        #endregion

        #region Public Property
        /// <summary>
        /// Gets or sets the SourceText
        /// </summary>
        public string SourceText
        {
            get
            {
                return this._sourceText;
            }

            set
            {
                this._sourceText = value;
            }
        }
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Context.Items["Email"] != null)
                {
                    txtEmail.Text = Context.Items["Email"].ToString();
                }
            }

            if (this.Page.Title != null)
            {
                this.Page.Title = ZNodeCatalogManager.MessageConfig.GetMessage("NewsletterSignupTitle", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Event is raised when Sign Up button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Btnsignup_Click(object sender, EventArgs e)
        {
            ZNode.Libraries.Admin.AccountAdmin _UserAccountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Account _UserAccount = new ZNode.Libraries.DataAccess.Entities.Account();

            ZNode.Libraries.DataAccess.Entities.Address address = new ZNode.Libraries.DataAccess.Entities.Address();            

            _UserAccount.EmailOptIn = true;
            _UserAccount.Email = txtEmail.Text;
            _UserAccount.Source = this.SourceText;

            // Set to empty values to other billing address properties            
            address.Name = "Default Address";
            address.FirstName = txtFirstname.Text;
            address.MiddleName = string.Empty;
            address.LastName = txtLastname.Text;

            // Set to empty values to other billing address properties
            address.PhoneNumber = string.Empty;
            address.CompanyName = string.Empty;
            address.Street = string.Empty;
            address.Street1 = string.Empty;
            address.City = string.Empty;
            address.StateCode = string.Empty;
            address.PostalCode = string.Empty;
            address.CountryCode = string.Empty;
           
            _UserAccount.ProfileID = ZNodeConfigManager.SiteConfig.DefaultAnonymousProfileID;

            // Pre-set properties
            _UserAccount.UserID = null;
            _UserAccount.ActiveInd = true;
            _UserAccount.ParentAccountID = null;
            _UserAccount.AccountTypeID = 0;
            _UserAccount.CreateDte = System.DateTime.Now;
            _UserAccount.UpdateDte = System.DateTime.Now;
            _UserAccount.CreateUser = null;

            // Add New Contact
            bool Check = _UserAccountAdmin.Add(_UserAccount);

            // Add New Address
            AddressService addressService = new AddressService();
            address.AccountID = _UserAccount.AccountID;
            address.IsDefaultBilling = true;
            address.IsDefaultShipping = true;
            addressService.Insert(address);

            // Add and entry to AccountProfile.
            AccountProfileService accountProfileService = new AccountProfileService();
            AccountProfile accountProfile = new AccountProfile();
            accountProfile.ProfileID = _UserAccount.ProfileID;
            accountProfile.AccountID = _UserAccount.AccountID;
            accountProfileService.Insert(accountProfile);

            // Check Boolean Value
            if (Check)
            {
                ErrorMessage.Visible = false;
                pnlConfirm.Visible = true;
                pnlContact.Visible = false;
                return;
            }
            else
            {
                ErrorMessage.Visible = true;
                ErrorMessage.Text = Resources.CommonCaption.CreateFailed;
                pnlConfirm.Visible = true;
                pnlContact.Visible = false;
                return;
            }
        }

        #endregion
    }
}