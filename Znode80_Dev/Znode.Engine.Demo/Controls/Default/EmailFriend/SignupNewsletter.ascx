﻿<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Znode.Engine.Demo.Controls_Default_EmailFriend_SignupNewsletter" EnableViewState="false" CodeBehind="SignupNewsletter.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<asp:Panel ID="pnlNewsLetterSignup" runat="server" DefaultButton="newsletterImage"
    meta:resourcekey="pnlNewsLetterSignupResource1">
    <div class="NewsLetterSignUp">
        <div class="Title Small">
            <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtSignuptoReceive"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtNewsLetter" Width="115px" runat="server" ValidationGroup="SignUp" AutoCompleteType="Disabled"></asp:TextBox>
            <asp:ImageButton ID="newsletterImage" EnableViewState="false" ValidationGroup="SignUp" runat="server" ImageAlign="AbsMiddle"
                CssClass="ImageButton" OnClick="NewsletterImage_Click" meta:resourcekey="newsletterImageResource1" />
        </div>
    </div>
    <div>
        <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="txtNewsLetter"
            CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired%>"
            ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired%>" ValidationGroup="SignUp"></asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtNewsLetter"
            CssClass="Error"
            ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression%>"
            ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired%>"
            ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired%>"
            Display="Dynamic" ValidationGroup="SignUp"></asp:RegularExpressionValidator>

        <asp:Panel ID="pnlConfirm" runat="server" Visible="False" CssClass="SuccessMsg SuccessMsg-footer" meta:resourcekey="pnlConfirmResource1">
            <ZNode:Spacer ID="Spacer2" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server" />
            <p>
                <uc1:CustomMessage ID="CustomMessage3" MessageKey="NewsletterSignupConfirmationIntroText" runat="server" />
            </p>
            <asp:Label ID="ErrorMessage" runat="server" Visible="False" CssClass="Error" meta:resourcekey="ErrorMessageResource1"></asp:Label>
            <ZNode:Spacer ID="Spacer1" EnableViewState="false" SpacerHeight="50" SpacerWidth="10" runat="server" />
        </asp:Panel>
    </div>
</asp:Panel>
