﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Fulfillment;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the SignupNewsletter user control class.
    /// </summary>
    public partial class Controls_Default_EmailFriend_SignupNewsletter : System.Web.UI.UserControl
    {
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>

        #region Private Variable

        private string _sourceText = "Email Signup";
        #endregion
        public string SourceText
        {
            get
            {
                return this._sourceText;
            }

            set
            {
                this._sourceText = value;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string theme = ZNodeCatalogManager.Theme;

            // If category theme wise theme selected then apply the current category theme.
            if (Session["CurrentCategoryTheme"] != null)
            {
                theme = Session["CurrentCategoryTheme"].ToString();
            }


            // Set button image
            newsletterImage.ImageUrl = string.Format("~/themes/{0}/Images/search.gif", theme);
        }

        #region Events

        /// <summary>
        /// Event is raised when Newsletter Image  is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>      
        protected void NewsletterImage_Click(object sender, ImageClickEventArgs e)
        {
            NewsLetterSignUp();
        }

        #endregion

        #region Znode Version 7.2.2 - News Letter Sign up helper methods

        /// <summary>
        /// Znode version 7.2.2 
        /// This function is used for news letter sign up via valid email id. 
        /// If email id already exist then it shows error message otherwise displays success message.
        /// <summary>
        /// <returns>Email adderss </returns>
        protected void NewsLetterSignUp()
        {
            string newsLetterEmail = txtNewsLetter.Text;

            ZNode.Libraries.Admin.AccountAdmin _UserAccountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Account _UserAccount = new ZNode.Libraries.DataAccess.Entities.Account();
            ZNode.Libraries.DataAccess.Entities.Address address = new ZNode.Libraries.DataAccess.Entities.Address();

            // Check if email already exists in DB
            if (!IsUserAlreadyExist(txtNewsLetter.Text.Trim()))
            {
                _UserAccount.EmailOptIn = true;
                _UserAccount.Email = txtNewsLetter.Text;
                _UserAccount.Source = this.SourceText;

                // Set to empty values to other billing address properties            
                address.Name = "Default Address";
                address.MiddleName = string.Empty;

                // Set to empty values to other billing address properties
                address.PhoneNumber = string.Empty;
                address.CompanyName = string.Empty;
                address.Street = string.Empty;
                address.Street1 = string.Empty;
                address.City = string.Empty;
                address.StateCode = string.Empty;
                address.PostalCode = string.Empty;
                address.CountryCode = string.Empty;

                _UserAccount.ProfileID = ZNodeConfigManager.SiteConfig.DefaultAnonymousProfileID;

                // Pre-set properties
                _UserAccount.UserID = null;
                _UserAccount.ActiveInd = true;
                _UserAccount.ParentAccountID = null;
                _UserAccount.AccountTypeID = 0;
                _UserAccount.CreateDte = System.DateTime.Now;
                _UserAccount.UpdateDte = System.DateTime.Now;
                _UserAccount.CreateUser = null;

                // Add New Contact
                bool check = _UserAccountAdmin.Add(_UserAccount);

                // Add New Address
                AddressService addressService = new AddressService();
                address.AccountID = _UserAccount.AccountID;
                address.IsDefaultBilling = true;
                address.IsDefaultShipping = true;
                addressService.Insert(address);

                // Add and entry to AccountProfile.
                AccountProfileService accountProfileService = new AccountProfileService();
                AccountProfile accountProfile = new AccountProfile();
                accountProfile.ProfileID = _UserAccount.ProfileID;
                accountProfile.AccountID = _UserAccount.AccountID;
                accountProfileService.Insert(accountProfile);
                txtNewsLetter.Text = string.Empty;

                // Check Boolean Value
                if (check)
                {
                    ErrorMessage.Visible = false;
                    pnlConfirm.Visible = true;
                    return;
                }
                else
                {
                    ErrorMessage.Visible = true;
                    ErrorMessage.Text = Resources.CommonCaption.CreateFailed;
                    pnlConfirm.Visible = true;
                    return;
                }

            }
            else
            {
                ErrorMessage.Visible = true;
                ErrorMessage.Text = Resources.CommonCaption.EmailAlreadyExist;
                CustomMessage3.Visible = false;
                pnlConfirm.Visible = true;
                return;
            }

        }

        /// <summary>
        /// Znode version 7.2.2 
        /// This function is used to chek email id already exist or not. 
        /// If email id already exist then it shows error message otherwise displays success message.
        /// <summary>
        /// <returns>Email adderss </returns>
        private bool IsUserAlreadyExist(string email)
        {
            ZNode.Libraries.Admin.AccountAdmin _UserAccounts = new ZNode.Libraries.Admin.AccountAdmin();
           
            // To get all cusomer list
            TList<Account> customerList = _UserAccounts.GetAllCustomer();
          
            //To find specific email exist or not.
            Account account = customerList.Find("Email", email);
            return (account != null) ? true : false;
        }
        #endregion
    }
}
