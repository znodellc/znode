<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Home_Home" Codebehind="Home.ascx.cs" %>
<%@ Register Src="~/Controls/Default/specials/specials.ascx" TagName="Specials" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/common/HomeQuickSearch.ascx" TagName="QuickSearch" TagPrefix="ZNode" %>




<div>
    <asp:Label ID="lblHtml" runat="server" EnableViewState="false"></asp:Label>
</div>
<div class="HomePageSpecials">
    <ZNode:Specials ID="Specials" runat="server" Title="Specials"></ZNode:Specials>        
</div>   
<div class="horizontalline">&nbsp;</div> 

