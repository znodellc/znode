using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Service;

namespace Znode.Engine.Demo
{
    public partial class Controls_Default_Home_Home : System.Web.UI.UserControl
    {
        private ZNodeUserAccount _userAccount = null;


        #region Page Load
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Set HTML home page content
                lblHtml.Text = ZNodeContentManager.GetPageHTMLByName("home", ZNodeConfigManager.SiteConfig.PortalID);
            }
            if (HttpContext.Current.Session[ZNodeSessionKeyType.UserAccount.ToString()] != null)
            {
                this._userAccount = ZNodeUserAccount.CurrentAccount();
                if (this._userAccount.EnableCustomerPricing && ZNodeConfigManager.SiteConfig.EnableCustomerPricing.GetValueOrDefault(false))
                {
                    Specials.Visible = false;
                }
                else
                {
                    Specials.Visible = true;
                }
            }
            else
            {
                Specials.Visible = true;
            }

            //Znode Version 7.2.2
            //Following code is used to add canonical tags in home page under head tag while page Load - Start.
            //canonical tags are used to avoide duplicate contents(URLs) and show prefered URL in organic search. 
            if (this.Page != null)
            {
                string includeHomeCanonical = ConfigurationManager.AppSettings["IncludeHomeCanonical"];
                if (includeHomeCanonical.Equals("1"))
                {
                    string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);
                    HtmlGenericControl IncludeConanical = new HtmlGenericControl("link");
                    IncludeConanical.Attributes.Add("rel", "canonical");
                    IncludeConanical.Attributes.Add("href", domainPath.ToLower());

                    // Add a reference for canonical to the head section
                    if (this.Page.Header != null) this.Page.Header.Controls.Add(IncludeConanical);
                }
            }
            //Following code is used to add canonical tags in home page under head tag while page Load - End
        }
        #endregion
    }
}