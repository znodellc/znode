using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using System.Linq;
using Znode.Engine.Common;
using ZNode.Libraries.ECommerce.UserAccount;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Product List user control class.
    /// </summary>
    public partial class Controls_Default_Category_ProductList : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeCategory _category;
        private int _categoryId;
        private List<int> _ProductId;
        private ZNodeProfile _ProductListProfile = new ZNodeProfile();
        private ZNodeProductList _productList;
        private string _noRecordsText = string.Empty;
        private int _pageSize = 8;
        private string _selectedPageSizeText = "SelectedPageSize";
        #endregion

        #region Protected Static Member Variables
        private int totalRecords = 0;
        private int recCount = 0;
        private int ncurrentpage = 0;
        private string BuyImage = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/view_cart_bg.gif";
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the product seperator image
        /// </summary>
        public string ProductListSeparatorImage
        {
            get { return "~/themes/" + ZNodeCatalogManager.Theme + "/Images/line_seperator.gif"; }
        }

        /// <summary>
        /// Gets or sets the product list profile
        /// </summary>
        public ZNodeProfile ProductListProfile
        {
            get { return this._ProductListProfile; }
            set { this._ProductListProfile = value; }
        }

        /// <summary>
        /// Gets the page size object
        /// </summary>
        public int PageSize
        {
            get
            {
                //Znode Version 7.2.2
                //Set & Get selected page size in sesssion.   - Start 
                int pageSize = _pageSize;
                
                //Sets the Selected Page Size in session.
                if (Equals(Session[_selectedPageSizeText], null))
                {
                    Session.Add(_selectedPageSizeText, pageSize);
                }
                //Set & Get selected page size in sesssion. - End 

                string eventTarget = "__EVENTTARGET";                
                if (ddlTopPaging.SelectedIndex > 0 || ddlBottomPaging.SelectedIndex > 0)
                {
                    pageSize = (Request[eventTarget] != null && Request[eventTarget].Contains(ddlTopPaging.ID)) ? Convert.ToInt32(ddlTopPaging.SelectedValue) : Convert.ToInt32(ddlBottomPaging.SelectedValue);
                    Session[_selectedPageSizeText] = pageSize;
                }
                else if (Request.QueryString["size"] != null)
                {
                    pageSize = Convert.ToInt32(Request.QueryString["size"]);
                    Session[_selectedPageSizeText] = pageSize;
                }

                 //Znode Version 7.2.2
                //Set & Get selected page size in sesssion. - Start 
                //Add SelectedPageSize key in Session and bind the selected drop down value to the session key. And Access page value from Session.
                //Bind Session Page Size to the controls.
                if (Session[_selectedPageSizeText] != null)
                {
                    pageSize = Convert.ToInt32(Session[_selectedPageSizeText]);
                    ddlTopPaging.ClearSelection();
                    ddlTopPaging.SelectedValue = pageSize.ToString();
                    ddlBottomPaging.ClearSelection();
                    ddlBottomPaging.SelectedValue = pageSize.ToString();
                }
                //Set & Get selected page size in sesssion. - End 
                return pageSize;
            }
        }

        /// <summary>
        /// Gets or sets the TotalPages object
        /// </summary>
        public int TotalPages
        {
            get
            {
                int totalPages = 0;

                if (ViewState["TotalPages"] != null)
                {
                    totalPages = (int)ViewState["TotalPages"];
                }

                return totalPages;
            }

            set
            {
                ViewState["TotalPages"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the title for this control
        /// </summary>
        public string NoRecordsText
        {
            get
            {
                return this._noRecordsText;
            }

            set
            {
                this._noRecordsText = value;
            }
        }

        /// <summary>
        /// Gets or sets the product list passed in from the search page
        /// </summary>
        public ZNodeProductList ProductList
        {
            get
            {
                return this._productList;
            }

            set
            {
                this._productList = value;
            }
        }

        /// <summary>
        /// Gets or sets the list of ProductIDs
        /// </summary>
        public List<int> ProductID
        {
            get
            {
                return this._ProductId;
            }

            set
            {
                this._ProductId = value;
            }
        }
        /// <summary>
        /// Gets the CurrentPage object
        /// </summary>
        public int CurrentPage
        {
            get
            {
                int currentPage = 1;

                if (Request.QueryString["page"] != null)
                {
                    currentPage = Convert.ToInt32(Request.QueryString["page"]);
                }

                return currentPage;
            }
        }

        #endregion

        #region Private Properties
     
        /// <summary>
        /// Gets or sets the FirstIndex object
        /// </summary>
        private int FirstIndex
        {
            get
            {
                int firstIndex = 0;
                if (ViewState["FirstIndex"] != null)
                {
                    firstIndex = Convert.ToInt32(ViewState["FirstIndex"]);
                }

                return firstIndex;
            }

            set
            {
                ViewState["FirstIndex"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the LastIndex object
        /// </summary>
        private int LastIndex
        {
            get
            {
                int lastIndex = 0;

                if (ViewState["LastIndex"] != null)
                {
                    lastIndex = Convert.ToInt32(ViewState["LastIndex"]);
                }

                return lastIndex;
            }

            set
            {
                ViewState["LastIndex"] = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Represents the CheckFor Call for pricing message
        /// </summary>
        /// <param name="fieldValue">passed the field value </param>
        /// <returns>Returns the Call For pricing message </returns>
        public string CheckForCallForPricing(object fieldValue, object callMessage)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();
            bool Status = bool.Parse(fieldValue.ToString());
            string message = mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);

            if (callMessage != null && !string.IsNullOrEmpty(callMessage.ToString()))
            {
                message = callMessage.ToString();
            }

            if (Status)
            {
                return message;
            }
            else if (!this._ProductListProfile.ShowPrice)
            {
                return message;
            }
            else
            {
                return string.Empty;
            }
        }

        public string GetProductUrl(string ViewProductLink)
        {
            string Url = ViewProductLink;
            if (this._categoryId > 0)
            {
                // Add to session            
                Session["BreadCrumbCategoryId"] = this._categoryId;
            }
            return Url;
        }

        /// <summary>
        /// Represents the GetColoroption method
        /// </summary>
        /// <param name="AlternateProductImageCount">Alternate Product Image Count</param>
        /// <returns>Returns the Color Option</returns>
        public string GetColorCaption(int AlternateProductImageCount)
        {
            string output = string.Empty;

            if (AlternateProductImageCount > 0)
            {
                output = "COLORS ";
            }
            return output;
        }
        #endregion

        #region Bind Data

        // <summary>
        // Bind control display based on category object passed in
        // </summary>
        public void BindCategory()
        {
            if (Visible)
            {
                if (!Page.IsPostBack)
                {
                    this._productList = new ZNodeProductList();

                    // Set initial page value
                    ViewState["CurrentPage"] = 1;
                }
            }
        }

        /// <summary>
        /// Bind the Tagged Products.
        /// </summary>
        public void BindTagProducts()
        {
            ViewState["CurrentPage"] = 1;
            this.BindDataListPagedDataSource();
        }

        #endregion

        #region Paging DataList Control Events

        /// <summary>
        /// Event triggered when a DataBind Method is called for DataListProducts
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DataListProducts_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            // Find the seperator image and then remove for the last column
            if (e.Item.ItemType == ListItemType.Separator)
            {
                int lastColumnIndex = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;
                if ((e.Item.ItemIndex + 1) % lastColumnIndex == 0 && e.Item.ItemIndex != 0)
                {
                    foreach (Control ctrl in e.Item.Controls)
                    {
                        if (ctrl.GetType().ToString() == "System.Web.UI.HtmlControls.HtmlImage")
                        {
                            e.Item.Controls.Remove(ctrl);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Set the page size to selected DropDownList value size. If "SHOW ALL" selected then display all products.
        /// </summary>
        /// <param name="source">Source object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlTopPaging_SelectedIndexChanged(object source, EventArgs e)
        {
            this.NavigationToUrl(1, Convert.ToInt32(ddlTopPaging.SelectedValue), lstFilter);
        }

        /// <summary>
        /// Set the page size to selected DropDownList value size. If "SHOW ALL" selected then display all products.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlBottomPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.NavigationToUrl(1, Convert.ToInt32(ddlBottomPaging.SelectedValue), lstFilter);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (Request.QueryString["sort"] != null)
                {
                    lstFilter.SelectedValue = Request.QueryString["sort"].ToString();
                }
            }
            if (Request.Params["zcid"] != null)
            {
                int.TryParse(Request.Params["zcid"], out this._categoryId);
            }

            // Set Repeat columns to the product Data list
            DataListProducts.RepeatColumns = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;

            if (this._categoryId > 0)
            {
                // Retrieve category data from httpContext (set previously in the page_preinit event)
                this._category = (ZNodeCategory)HttpContext.Current.Items["Category"];

                // Add to session            
                Session["BreadCrumbCategoryId"] = this._categoryId;

                this.BindCategory();

                ddlTopPaging.SelectedIndex = ddlTopPaging.Items.IndexOf(ddlTopPaging.Items.FindByValue(this.PageSize.ToString()));
                ddlBottomPaging.SelectedIndex = ddlTopPaging.SelectedIndex;

                string baseUrl = this.GetNavigationUrl();
                
                this.BindDataListPagedDataSource();

                //// Do not add the & or ? in the following query string.
                hlTopPrevLink.NavigateUrl = hlBotPrevLink.NavigateUrl = string.Format("{0}page={1}&size={2}&sort={3}", baseUrl, this.CurrentPage - 1, ddlTopPaging.SelectedValue,lstFilter.SelectedValue);
                hlTopNextLink.NavigateUrl = hlBotNextLink.NavigateUrl = string.Format("{0}page={1}&size={2}&sort={3}", baseUrl, this.CurrentPage + 1, ddlTopPaging.SelectedValue,lstFilter.SelectedValue);
            }
        }

        /// <summary>
        /// Event triggered when LstFilter control is Selected Index Changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.NavigationToUrl(1, Convert.ToInt32(ddlBottomPaging.SelectedValue), lstFilter);
        }

        #endregion

        #region Paging Methods & Events
        /// <summary>
        /// Navigate to the category page with query string the specified current page and page size.
        /// </summary>
        /// <param name="currentPage">Current page index.</param>
        /// <param name="pageSize">Category page size. If Show All selected then -1 will be used.</param>
        private void NavigationToUrl(int currentPage, int pageSize,  DropDownList ddlSort)
        {
            string baseUrl = this.GetNavigationUrl();

            // Do not add the & or ? in the following query string.
            string navigateUrl = string.Format("{0}page={1}&size={2}&sort={3}", baseUrl, currentPage, pageSize,ddlSort.SelectedValue);
            Response.Redirect(navigateUrl);
        }

        /// <summary>
        /// Gets the navigation url for the previous/next navigation link
        /// </summary>
        /// <returns>Returns the navigation base url.</returns>
        private string GetNavigationUrl()
        {
            string baseUrl = string.Empty;
            if (this._category.SEOURL.Trim() == string.Empty)
            {
                StringBuilder url = new StringBuilder();
                url.Append(Request.Url.GetLeftPart(UriPartial.Authority));
                url.Append(Request.Url.AbsolutePath);

                // Append the category Url with base url
                if (Request.QueryString["zcid"] != null)
                {
                    url.Append("?zcid=").Append(Request.QueryString["zcid"]);
                }

                url.Append("&");
                baseUrl = url.ToString();
            }
            else
            {
                string rawUrl = string.Format("{0}.aspx", this._category.SEOURL);
                baseUrl = string.Format("{0}/{1}?", Request.Url.GetLeftPart(UriPartial.Authority), rawUrl);
            }

            foreach (String key in Request.QueryString.AllKeys)
            {
                if (key.ToLower().Equals("zcid") || key.ToLower().Equals("page") || key.ToLower().Equals("size") || key.ToLower().Equals("sort"))
                    continue;


                if (Request.QueryString[key].Contains(","))
                {
                    string[] facets = Request.QueryString[key].Split(',');
                    foreach (string facet in facets)
                    {
                        baseUrl = baseUrl + key + "=" + HttpUtility.UrlEncode(facet) + "&";
                    }
                }
                else
                {
                    baseUrl = baseUrl + key + "=" + HttpUtility.UrlEncode(Request.QueryString[key]) + "&";
                }
            }

            return baseUrl;
        }

        /// <summary>
        /// Bind Datalist using Paged DataSource object
        /// </summary>
        private void BindDataListPagedDataSource()
        {
            //Znode Version 7.2.2
            //Set Selected page size from Session - Start 
            //Set the page size from Session variable. If session not exists then it set the default page size.
            int selectedPageSize = (Equals(Session[_selectedPageSizeText], null)) ? _pageSize : Convert.ToInt32(Session[_selectedPageSizeText]);
            //Set Selected page size from Session - End 

            this._productList = new ZNodeProductList();
	        var sort = Request.QueryString["sort"];
          
			var cacheKey = string.Format("CategoryProductList_{0}_{1}_{2}_{3}_{4}_{5}_{6}_{7}", _category.CategoryID,
											 ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId,
                                             ZNodeProfile.CurrentUserProfileId, this.CurrentPage, selectedPageSize /* this.PageSize */, sort, 
											 string.Join("-", ProductID.OrderBy(x=> x).Select(x => x.ToString())));

	        if (ZNodeConfigManager.SiteConfig.EnableCustomerPricing.GetValueOrDefault(false) && ZNodeUserAccount.CurrentAccount()!=null &&
	            ZNodeUserAccount.CurrentAccount().EnableCustomerPricing)
	        {
				if (HttpContext.Current.Session[cacheKey] != null)
				{
					this._productList = HttpContext.Current.Session[cacheKey] as ZNodeProductList;
				}
				else
				{
					if (ProductID != null && ProductID.Any())
					{
						this._productList = ZNodePaging.GetProducts(ProductID, GetSortField(sort),
																	GetSortDirection(sort), this.CurrentPage,
                                                                    selectedPageSize /* this.PageSize */, 1);

						HttpContext.Current.Session[cacheKey] = this._productList;
					}
				}
	        }
	        else
	        {		        
		        if (HttpContext.Current.Cache[cacheKey] != null)
		        {
			        this._productList = HttpContext.Current.Cache[cacheKey] as ZNodeProductList;
		        }
		        else
		        {
			        if (ProductID != null && ProductID.Any())
			        {
						this._productList = ZNodePaging.GetProducts(ProductID, GetSortField(sort),
																	GetSortDirection(sort), this.CurrentPage,
                                                                    selectedPageSize /* this.PageSize */, 1);

						ZNodeCacheDependencyManager.Insert(cacheKey, this._productList, "ZNodeCategoryNode", "ZNodeCategory", "ZNodePortalCatalog", "ZNodeProductCategory", "ZNodeLuceneIndexServerStatus", "ZNodeProduct", "ZNodeSKU");						
			        }
		        }
	        }

	        // Assigning Datasource to the DataList.
            DataListProducts.DataSource = this._productList.ZNodeProductCollection;
            DataListProducts.DataBind();

            // Disable the data list item view state.
            foreach (DataListItem item in DataListProducts.Items)
            {
                item.EnableViewState = false;
            }

            this.ncurrentpage = this.CurrentPage;
            this.recCount = this._productList.TotalPageCount;
            this.totalRecords = this._productList.TotalRecordCount;

            if (this._productList.ZNodeProductCollection.Count == 0)
            {
                pnlProductList.Visible = false;
                ErrorMsg.Visible = true;
                ErrorMsg.Text = string.Empty;
            }
            else
            {
                pnlProductList.Visible = true;
                ErrorMsg.Visible = false;
            }

            hlTopNextLink.Enabled = hlBotNextLink.Enabled = !(this.ncurrentpage == this.recCount);
            hlTopPrevLink.Enabled = hlBotPrevLink.Enabled = !(this.ncurrentpage == 1);

            if (!this.IsPostBack)
            {
                this.DoPaging();
            }
        }

        /// <summary>
        /// Binding Paging List
        /// </summary>
        private void DoPaging()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("PageIndex");
            dt.Columns.Add("PageText");
            this.FirstIndex = this.CurrentPage - 5;
            this.LastIndex = (this.CurrentPage > 5) ? this.CurrentPage + 5 : 10;

            if (this.LastIndex > this.TotalPages)
            {
                this.LastIndex = this.TotalPages;
                this.FirstIndex = this.LastIndex - 10;
            }

            if (this.FirstIndex < 0)
            {
                this.FirstIndex = 0;
            }

            ddlTopPaging.Items.Clear();
            ddlBottomPaging.Items.Clear();

            string pagingText = this.GetLocalResourceObject("PagingText").ToString();

            // Each items seperated with & symbol
            string[] pagingItems = pagingText.Split(new char[] { '&' });
            foreach (string pagingItem in pagingItems)
            {
                if (pagingItem.Contains("|") && pagingItem.Trim().Length > 0)
                {
                    // Key value pair seperated with | symbol
                    string[] itemText = pagingItem.Split(new char[] { '|' });
                    DataRow dr = null;
                    dr = dt.NewRow();
                    dr[0] = Convert.ToInt32(itemText[1].Trim());
                    dr[1] = itemText[0].Trim();
                    dt.Rows.Add(dr);
                }
            }

            this.ddlTopPaging.DataTextField = "PageText";
            this.ddlTopPaging.DataValueField = "PageIndex";
            this.ddlTopPaging.DataSource = dt;
            this.ddlTopPaging.DataBind();

            this.ddlBottomPaging.DataTextField = "PageText";
            this.ddlBottomPaging.DataValueField = "PageIndex";
            this.ddlBottomPaging.DataSource = dt;
            this.ddlBottomPaging.DataBind();

            if (Request.QueryString["size"] != null)
            {
                int size = Convert.ToInt32(Request.QueryString["size"].ToString());
                ddlTopPaging.SelectedIndex = ddlTopPaging.Items.IndexOf(ddlTopPaging.Items.FindByValue(size.ToString()));
                ddlBottomPaging.SelectedIndex = ddlTopPaging.SelectedIndex;
            }
        }
       
        /// <summary>
        /// Get Sort Field
        /// </summary>
        private string GetSortField(string sort)
        {
            string sortOrder = string.Empty;

            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort)
                {
                    case "2": return "FinalPrice";

                    case "3": return "FinalPrice";
                }
            }
            return null;
        }

        /// <summary>
        /// Get Sort Direction
        /// </summary>
        private string GetSortDirection(string sort)
        {
            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort)
                {
                    case "2": return "ASC";

                    case "3": return "DESC";
                }
            }
            return null;
        }

        #endregion
    }
}