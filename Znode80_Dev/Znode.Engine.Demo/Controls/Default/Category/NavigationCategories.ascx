<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Category_NavigationCategories" Codebehind="NavigationCategories.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<div class="CategoryTreeView">
    <div class="Title"><uc1:CustomMessage ID="CustomMessage2" MessageKey="LeftNavigationShopByCategoryTitle" runat="server" /></div>
    <div>
        <asp:TreeView ID="ctrlNavigation" runat="server" ExpandDepth="0" CssClass="TreeView" NodeWrap="true" NodeIndent="15" ShowExpandCollapse="False" >
            <ParentNodeStyle CssClass="ParentNodeStyle" />
            <HoverNodeStyle CssClass="HoverNodeStyle" />
            <SelectedNodeStyle CssClass="SelectedNodeStyle" />
            <RootNodeStyle CssClass="RootNodeStyle" />
            <LeafNodeStyle CssClass="LeafNodeStyle" />
            <NodeStyle CssClass="NodeStyle" />
        </asp:TreeView>
    </div>
</div>