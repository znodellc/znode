﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the PagingLinks user control class.
    /// </summary>
    public partial class Controls_Default_Category_PagingLinks : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private int _PageDisplayCount = 5;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the PageDisplayCount object
        /// </summary>
        public int PageDisplayCount
        {
            get
            {
                return this._PageDisplayCount;
            }

            set
            {
                this._PageDisplayCount = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Represents the Clear Controls method
        /// </summary>
        public void ClearControls()
        {
            uxGridPaging.Controls.Clear();
        }

        /// <summary>
        /// Event triggered when Bind method is called
        /// </summary>
        /// <param name="PageCount">Total number of Pages</param>
        /// <param name="CurrentPageIndex">Current Page Index</param>
        /// <param name="PageDisplayIndex">Page Display Index</param>
        /// <param name="Keyword">Keyword to check</param>
        public void Bind(int PageCount, int CurrentPageIndex, int PageDisplayIndex, string Keyword)
        {
            uxGridPaging.Controls.Clear();

            if (PageCount > 0)
            {
                bool isLastPage = false;

                if (PageDisplayIndex == 0)
                {
                    uxGridPaging.Controls.Add(new LiteralControl("<a style='color:gray;background-color:gray;cursor:default;' href=\"javascript:doNothing();\">&laquo; Prev</a>&nbsp;"));
                }
                else
                {
                    uxGridPaging.Controls.Add(new LiteralControl("<a style='background-color:green;' href=\"javascript:doGridPaging(" + (CurrentPageIndex - 1) + "," + (CurrentPageIndex - 1) + ",'Previous','" + Keyword + "');\">&laquo; Prev</a>&nbsp;"));
                }

                int lowerBound = 0;

                if (CurrentPageIndex >= this._PageDisplayCount)
                {
                    lowerBound = this._PageDisplayCount * (CurrentPageIndex / this._PageDisplayCount);
                }

                int upperBound = lowerBound + this._PageDisplayCount;

                if (CurrentPageIndex % this._PageDisplayCount == 0)
                {
                    upperBound = CurrentPageIndex + this._PageDisplayCount;
                }

                if (upperBound >= PageCount)
                {
                    upperBound = PageCount;
                }

                for (int i = lowerBound; i < upperBound; i++)
                {
                    string cssClass = i == CurrentPageIndex ? "Active" : string.Empty;

                    if (i == CurrentPageIndex)
                    {
                        uxGridPaging.Controls.Add(new LiteralControl("<a style='cursor:pointer; text-decoration:underline;' class=\"" + cssClass + "\" href=\"javascript:doNothing()\">" + (i + 1) + "</a>"));
                    }
                    else
                    {
                        uxGridPaging.Controls.Add(new LiteralControl("<a class=\"" + cssClass + "\" href=\"javascript:doGridPaging(" + i + "," + i + ",'Paging','" + Keyword + "')\">" + (i + 1) + "</a>"));
                    }

                    uxGridPaging.Controls.Add(new LiteralControl("&nbsp;"));
                }

                if (CurrentPageIndex == PageCount - 1)
                {
                    isLastPage = true;
                }

                if (isLastPage)
                {
                    uxGridPaging.Controls.Add(new LiteralControl("<a style='color:gray;cursor:default;' href=\"javascript:doNothing();\">Next &raquo;</a>"));
                }
                else
                {
                    uxGridPaging.Controls.Add(new LiteralControl("<a href=\"javascript:doGridPaging(" + (CurrentPageIndex + 1) + "," + (CurrentPageIndex + 1) + ",'Next','" + Keyword + "');\">Next &raquo;</a>"));
                }
            }
        }
        #endregion        
    }
}