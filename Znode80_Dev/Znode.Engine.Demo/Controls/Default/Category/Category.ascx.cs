using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using System.Collections.Generic;
using ZNode.Libraries.Ecommerce.Entities;
using ZNode.Libraries.ECommerce.SEO;
using System.Text;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Category user control class.
    /// </summary>
    public partial class Controls_Default_Category_Category : System.Web.UI.UserControl
    {
        #region Member Variables
        private int _categoryId; 
        private ZNodeCategory _category;
        const string querystringparams = "category,text,page,size,sort";
        #endregion

        #region Page Load

        protected void Page_Init(object sender, EventArgs e)
        {
			
            // Get category id from querystring  
            if (Request.Params["zcid"] != null)
            {
                this._categoryId = int.Parse(Request.Params["zcid"]);
            }
            else
            {
                throw new ApplicationException("Invalid Category Request");
            }

            // Retrieve category data from httpContext
            this._category = (ZNodeCategory)HttpContext.Current.Items["Category"];          

            if (!Page.IsPostBack)
            {
                // Bind data to page
                this.Bind();
            }
            LoadIndex(HttpUtility.HtmlDecode(this._category.Name));

            Facets.UpdatePageHandler += UpdatePage;
        }
       
        #endregion
      
        #region Methods

        /// <summary>
        /// Bind All Controls to Category Data
        /// </summary>
        protected void Bind()
        {
            // Bind title
            CategoryTitle.Text = string.Format(CategoryTitle.Text, this._category.Title);
            CategoryDescription.Text = this._category.Description;
            AdditionalDescription.Text = this._category.AlternateDescription;
        }

	    public void UpdatePage(string facetquery)
	    {
		    var baseUrl = ZNodeSEOUrl.MakeURL(_category.CategoryID.ToString(), SEOUrlType.Category, _category.SEOURL) +
		                  (string.IsNullOrEmpty(_category.SEOURL) ? "&" : "?");
		    var sortOption = Request.Params["sort"];
		    string navigateUrl = null;

		    //If sorting, page can end up on Page2 which shows no results.
		    if (sortOption != null)
		    {
				//Set to page 1
			    if (uxCategoryProductList.CurrentPage > 1)
				    navigateUrl = string.Format("{0}Category={1}&text={2}&page={3}&size={4}&sort={5}", baseUrl,
				                                HttpUtility.UrlEncode(this._category.Name), string.Empty,
				                                1, uxCategoryProductList.PageSize, 1);
		    }

		    if (string.IsNullOrEmpty(navigateUrl))
			    navigateUrl = string.Format("{0}Category={1}&text={2}&page={3}&size={4}&sort={5}", baseUrl,
			                                HttpUtility.UrlEncode(this._category.Name), string.Empty,
			                                uxCategoryProductList.CurrentPage, uxCategoryProductList.PageSize, 1);

		    Response.Redirect(navigateUrl + facetquery);

	    }

	    /// <summary>
        /// Loads the data from the search engine
        /// </summary>
        /// <param name="Category"></param>
        private void LoadIndex(string Category)
        {
            int sort = (!string.IsNullOrEmpty(Request.QueryString["sort"])) ? Convert.ToInt32(Request.QueryString["sort"]) : 0;
            var cacheKey = string.Format("{0}_{1}_{2}_{3}_{4}_{5}", "CategoryPage", _category.CategoryID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, ZNodeProfile.CurrentUserProfileId, sort);
            var result = HttpContext.Current.Cache[cacheKey] as ZNodeSearchEngineResult;

            string searchText = string.Empty;
            var RefinedFacets = new List<KeyValuePair<string, IEnumerable<string>>>();

            foreach (var item in Request.QueryString.Keys)
            {
                if (item.ToString().Contains("zcid"))
                {
                    continue;
                }
                string itemString = item.ToString();
                if (!querystringparams.Contains(item.ToString().ToLower()))
                {
                    RefinedFacets.Add(new KeyValuePair<string, IEnumerable<string>>(itemString, Request.QueryString.GetValues(itemString)));
                }
            }

            // if cache is empty or if facets selection, then do search.
            if (result == null || RefinedFacets.Count > 0)
            {
				var searchResultText = (string.IsNullOrEmpty(Request.QueryString["SRT"])) ? string.Empty : Request.QueryString["SRT"];
				ZNodeSearchEngine search = new ZNodeSearchEngine();
				result = search.Search(Category, searchText, searchResultText, RefinedFacets, sort);

                // update cache for category page without facets
                if (RefinedFacets.Count == 0)
                {
                    ZNodeCacheDependencyManager.Insert(cacheKey, result, "ZNodeCategoryNode", "ZNodeCategory", "ZNodePortalCatalog", "ZNodeProductCategory", "ZNodeLuceneIndexServerStatus");
                }
            }

            if (result != null)
            {
                if (result.Facets != null)
                {
                    Facets.Facetslist = result.Facets;
                    Facets.BindFacets();
                }
                if (result.CategoryNavigation != null)
                {
                    Categories.CategoryNavigation = result.CategoryNavigation;
                }
                if (result.Ids != null)
                {
                    uxCategoryProductList.ProductID = result.Ids.Select(int.Parse).ToList();
                    uxCategoryProductList.DataBind();
                }
            }
        }
        #endregion
    }
}