<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Category_SubCategoryList" Codebehind="SubCategoryList.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
 
 <asp:Panel ID="pnlSubCategoryList" runat="server" Visible="true">
 <div class ='FeaturedCategory'>
 <div class='SubCategoryList'>
     <div class="Title"><uc1:CustomMessage id ="FeaturedCategoryListTitle" MessageKey="CategoryFeaturedCategoriesTitle" runat="server"></uc1:CustomMessage></div> 
     <asp:DataList ID="DataListSubCategories" runat="server" RepeatDirection="Vertical" >
        <ItemTemplate>
            <div class="SubCategoryListItem">                  
                <div class='CategoryLink'>
                   <a id="A1" enableviewstate="false" href='<%# DataBinder.Eval(Container.DataItem, "ViewCategoryLink").ToString()%>' runat="server">
                   <img id="Img2" enableviewstate="false"  alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag").ToString()%>' runat="server" style="border:none"  src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString()) %>' visible='<% # ShowCategoryImage(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>' />               
                   </a>
                </div>                
                <div class="CategoryLink">
                    <img id="RightArrow" enableviewstate="false" alt="" runat="server" src= '<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("right_arrow.gif") %>' /><a id="A3" enableviewstate="false" runat="server" href='<%# DataBinder.Eval(Container.DataItem, "ViewCategoryLink").ToString()%>' >
                     <asp:Label ID="lblName" EnableViewState="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Title")%>' ></asp:Label>
                    </a>                    
                </div>  
                <div class="ShortDescription" visible="false">
                   <asp:Label ID="lblShortDecription" EnableViewState="false" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShortDescription")%>'></asp:Label>                
               </div>                                                                    
            </div>                    
        </ItemTemplate>                  
    </asp:DataList>
    
    
    <asp:DataList ID="DataListParentCategory" runat="server" RepeatDirection="Horizontal">
        <ItemTemplate>
            <div class="SubCategoryListItem">                  
                <div class='CategoryLink'>
                <a enableviewstate="false" href= '<%# "~/category.aspx?zcid=" + DataBinder.Eval(Container.DataItem, "CategoryId") %>' runat="server">
                    <img id="Img2" enableviewstate="false" alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag").ToString()%>' runat="server" style="border:none"  src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString()) %>'/>                               
                </a>
                </div>                
                <div class="CategoryLink">
                     <asp:Label ID="lblName" EnableViewState="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Title")%>' ></asp:Label>
                </div>  
                <div class="ShortDescription" visible="false">
                   <asp:Label ID="lblShortDescription" EnableViewState="false"  Visible="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShortDescription")%>'></asp:Label>
               </div>                                                     
            </div>                    
        </ItemTemplate>                  
    </asp:DataList>
 </div>
 </div>
</asp:Panel> 
  