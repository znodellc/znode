<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Category_Category"
    CodeBehind="Category.ascx.cs" %>
<%@ Register Src="ProductList.ascx" TagName="CategoryProductList" TagPrefix="ZNode" %>
<%@ Register Src="SubCategoryList.ascx" TagName="SubCategoryList" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ProductViewed.ascx" TagName="RecentlyViewedProducts"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/ShoppingCart/NavigationCart.ascx" TagName="NavigationCart"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/navigation/NavigationBreadCrumbs.ascx" TagName="StoreBreadCrumbs"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/Banner.ascx" TagName="Banner"
    TagPrefix="ZNode" %>  
<%@ Register Src="~/Controls/Default/SearchEngine/LeftNavigation.ascx" TagName="LeftNavigation" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/SearchEngine/Facets.ascx" TagName="Facets" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/SearchEngine/SearchWithInSearch.ascx" TagName="SearchWithin" TagPrefix="ZNode" %>
<div id="LeftColumn">
    
      <div>
         <ZNode:SearchWithin id="SearchWithInSearch" IsCategoryPage="false" runat="server"></ZNode:SearchWithin> 

    </div>
    <ZNode:LeftNavigation id="Categories" runat="server" ShowSubCategory="true" IsCategoryPage="true"></ZNode:LeftNavigation> 
  
    <ZNode:Facets ID="Facets" runat="server" />
    <ZNode:NavigationCart runat="server" ID="uxNavigationCart" />
    <ZNode:RecentlyViewedProducts ID="uxRecentlyViewedProducts" runat="server" Title="Recently Viewed Items"
        ShowName="true" ShowReviews="false" ShowImage="true" ShowDescription="false"
        ShowPrice="true" ShowAddToCart="false" />
</div>
<div id="MiddleColumn">
    <div id="BreadCrumb">
        <ZNode:StoreBreadCrumbs ID="uxBreadCrumbs" runat="server"></ZNode:StoreBreadCrumbs>
        <span class="PromoText">
            <ZNode:CustomMessage ID="HomeCustomMessage5" MessageKey="StoreSpecials" runat="server" />
        </span>
    </div>
    <div class="CategoryDetail">
        <div>
            <h1 class="CategoryTitle"><asp:Literal ID="CategoryTitle" Text="{0}" runat="server"></asp:Literal></h1></div>
        <div class="Description">
            <asp:Label ID="CategoryDescription" runat="server"></asp:Label></div>
        <div style="clear:both; display:block; margin-left:15px;" ><ZNode:Banner runat="server" BannerKey="CategoryPage" DisplayTime="5000" TransitionTime="1000" /></div>
         <div style="clear:both; display:block;" ><ZNode:CategoryProductList ID="uxCategoryProductList" runat="server" CssClass="ProductList"
            Visible="true" Title="Products"></ZNode:CategoryProductList></div>
         <div style="clear:both; display:block;"><ZNode:SubCategoryList ID="uxSubCategoryList" runat="server" CssClass="SubCategoryList"
            Visible="true" Title="Categories"></ZNode:SubCategoryList></div>
        <div class="AlternateDescription">
            <asp:Label ID="AdditionalDescription" runat="server"></asp:Label></div>
    </div>

