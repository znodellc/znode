<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Znode.Engine.Demo.Controls_Default_Reviews_ProductReviews" Codebehind="ProductReviews.ascx.cs" %>
<div id="Review">
    <asp:UpdatePanel ID="UpdatePnlProductReviewDetails" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="Sorting">
                <span class="TitleStyle">
                   <%-- <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtReviewedBy"></asp:Localize>--%>
					<asp:Localize ID="reviewCountTitle" runat="server" meta:resourcekey="txtReviewCountTitle"></asp:Localize>
                    <asp:Label ID="CustomersCnt" runat="server" meta:resourcekey="CustomersCntResource1"></asp:Label>
                    <%--<asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtcustomer"></asp:Localize>--%>
					
                </span>&nbsp; <span>
                    <asp:DropDownList ID="RateList" runat="server" OnSelectedIndexChanged="RateList_SelectedIndexChanged"
                        CssClass="SortByDropdown" AutoPostBack="True" meta:resourcekey="RateListResource1">
                        <asp:ListItem Value="desc" Selected="True" meta:resourcekey="ListItemResource1"></asp:ListItem>
                        <asp:ListItem Text="Oldest First" Value="asc" meta:resourcekey="ListItemResource2"></asp:ListItem>
                        <asp:ListItem Text="Highest Rating First" Value="5" meta:resourcekey="ListItemResource3"></asp:ListItem>
                        <asp:ListItem Text="Lowest Rating First" Value="1" meta:resourcekey="ListItemResource4"></asp:ListItem>
                    </asp:DropDownList>
                </span>
            </div>
            <div>
                <asp:DataList ID="ReviewList" runat="server" Width="100%" meta:resourcekey="ReviewListResource1">
                    <ItemTemplate>
                        <div class="ItemStyle">
                            <div>
                                <span class="Subject">
                                    <%# DataBinder.Eval(Container.DataItem,"Subject") %></span>,<br />
                                <%# GetFormattedDate(DataBinder.Eval(Container.DataItem, "CreateDate"))%></div>
                            <div class="Content">
                                <div class="StarRating">
                                    <%# GetRatingImagePath(Eval("Rating").ToString()) %></div>
                                <div id="divPros" visible='<%# ShowProsField %>' runat="server" class="Row">
                                    <span class="FieldStyle">Pros:</span><span class="ValueStyle"><%# Eval("Pros") %></span></div>
                                <div id="divCons" visible='<%# ShowConsField %>' runat="server" class="Row">
                                    <span class="FieldStyle">Cons:</span><span class="ValueStyle"><%# DataBinder.Eval(Container.DataItem,"Cons")  %></span></div>
                                <div class="Row">
                                    <%# DataBinder.Eval(Container.DataItem,"Comments").ToString() %></div>
                                <div class="Row">
                                    By <span class="Text">
                                        <%# DataBinder.Eval(Container.DataItem,"CreateUser")%></span> from <span class="Text">
                                            <%# DataBinder.Eval(Container.DataItem, "UserLocation")%></span></div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <ItemStyle CssClass="ReviewItem" />
                </asp:DataList>
            </div>
            <div class="Paging">
                <div id="topNavigation" runat="server">
                    <asp:LinkButton ID="TopPrevLink" runat="server" OnClick="PrevRecord" Text="<%$ Resources:CommonCaption, Prev%>"></asp:LinkButton>
                    <asp:Label ID='lblPagingInfo' runat='server'></asp:Label>
                    <asp:LinkButton ID="TopNextLink" runat="server" OnClick="NextRecord" Text="<%$ Resources:CommonCaption, Next%>"></asp:LinkButton>
                </div>
            </div>
            <div class="Spacer">
                &nbsp;</div>
            <div>
                <asp:Label ID="lblErrorMsg" runat="server" meta:resourcekey="lblErrorMsgResource1"></asp:Label></div>
            <div class="Spacer">
                &nbsp;</div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
