<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Znode.Engine.Demo.Controls_Default_Reviews_CustomerReview" Codebehind="CustomerReview.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>
<div class="CustomerReview">
    <div class="PageTitle">
	  
        <asp:Localize ID="Localize9" runat="server" meta:resourceKey="txtHead"></asp:Localize> <asp:Literal ID="ltrlProductName" runat="server" Text=""></asp:Literal>
    </div>
	  <asp:Panel ID="pnlCustomerReview" runat="server" DefaultButton="btnSubmit">
    <div class="Container">
        <div class="Form">
            <div class="Row">
                <div align="right" class="FieldStyle ReviewLeftContent">
                    <asp:Localize ID="Localize3" meta:resourceKey="txtReviewHeadLine" runat="server"/></div>
                <div>
                    <asp:TextBox ID="Title" runat="server" MaxLength="50" Columns="65" 
                        meta:resourcekey="TitleResource1"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="NameRequired" runat="server" ControlToValidate="Title"
                            CssClass="Error" Display="Dynamic"  
                            meta:resourcekey="NameRequiredResource1"></asp:RequiredFieldValidator></div>
                </div>
            </div>
            <div class="Clear">
                <ZNode:spacer ID="Spacer1" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
            </div>
            <div class="Row">
                <div align="right" class="FieldStyle ReviewLeftContent">
                    <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtSelectRating"></asp:Localize></div>
                <div>
                    <asp:DropDownList ID="RatingList" runat="server" meta:resourcekey="RatingListResource1">
                    <asp:ListItem Value="5" Selected="True" meta:resourcekey="ListItemResource1"></asp:ListItem>
                    <asp:ListItem Value="4" meta:resourcekey="ListItemResource2"></asp:ListItem>
                    <asp:ListItem Value="3" meta:resourcekey="ListItemResource3"></asp:ListItem>
                    <asp:ListItem Value="2" meta:resourcekey="ListItemResource4"></asp:ListItem>
                    <asp:ListItem Value="1" meta:resourcekey="ListItemResource5"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="Clear">
                <ZNode:spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
            </div>
            <div class="Row" id="rowPros" runat="server">
                <div align="right" class="FieldStyle ReviewLeftContent">
                    <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtPros"></asp:Localize></div>
                <div>
                    <asp:TextBox ID="Pros" ValidationGroup="GroupPros" runat="server" 
                        meta:resourcekey="ProsResource1"></asp:TextBox></div>
            </div>
            <div class="Clear">
                <ZNode:spacer ID="Spacer3" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
            </div>
            <div class="Row" id="rowCons" runat="server">
                <div align="right" class="FieldStyle ReviewLeftContent">
                    <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtCons"></asp:Localize></div>
                <div>
                    <asp:TextBox ID="Cons" ValidationGroup="GroupCons" runat="server" 
                        meta:resourcekey="ConsResource1" /></div>
            </div>
            <div class="Clear">
                <ZNode:spacer ID="Spacer4" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
            </div>
            <div class="Row">
                <div align="right" class="FieldStyle ReviewLeftContent">
                    <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtYourReview"></asp:Localize></div>
                <div>
                    <asp:TextBox ID="Comments" runat="server" TextMode="MultiLine" Rows="10" 
                        Columns="50" meta:resourcekey="CommentsResource1"></asp:TextBox>
                    <div align="left" style="margin-left:170px;">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Comments"
                            CssClass="Error" Display="Dynamic" SetFocusOnError="True" meta:resourcekey="RequiredFieldValidator3Resource1"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Clear">
                <ZNode:spacer ID="Spacer5" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
            </div>
            <div class="Row">
                <div align="right" class="FieldStyle ReviewLeftContent">
                    <asp:Localize ID="Localize6" runat="server" meta:resourceKey="txtNickName"></asp:Localize></div>
                <div>
                    <asp:TextBox ID="Name" runat="server" meta:resourcekey="NameResource1"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="Name"
                            CssClass="Error" Display="Dynamic" 
                            meta:resourcekey="RequiredFieldValidator4Resource1"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Clear">
                <ZNode:spacer ID="Spacer6" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
            </div>
            <div class="Row">
                <div align="right" class="FieldStyle ReviewLeftContent">
                    <asp:Localize ID="Localize7" runat="server" meta:resourceKey="txtYourLocation"></asp:Localize></div>
                <div>
                    <asp:TextBox ID="Location" runat="server" MaxLength="100" 
                        meta:resourcekey="LocationResource1"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Location"
                            CssClass="Error" Display="Dynamic" 
                            meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Clear">
                <ZNode:spacer ID="Spacer7" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
            </div>
            <div class="Row">
                <div align="right" colspan="2" class="FailureText">
                    <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False" 
                        meta:resourcekey="ErrorMessageResource1"></asp:Literal>
                </div>
            </div>
            <div class="Row">
                <div align="right" class="ReviewLeftContent">
                    &nbsp;
                </div>
                <div>
                    <div>
                        <asp:Localize ID="Localize8" runat="server" meta:resourceKey="txtDesc"></asp:Localize></div>
                    <div class="Spacer">
                        &nbsp;</div>
                </div>
            </div>
            <div class="Clear">
                <ZNode:spacer ID="Spacer8" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:spacer>
            </div>
            <div class="Row">
                <div class="FieldStyle ReviewLeftContent">
                </div>
                <div>
					<asp:Button  ID="btnSubmit" runat="server" OnClick="BtnSubmit_Click" CssClass="button"
						Text="<%$ Resources:CommonCaption, Submit %>"/>
                    <asp:LinkButton ID="btnCancel" runat="server" OnClick="BtnCancel_Click" CssClass="Button"
                        CausesValidation="False" Text="<%$ Resources:CommonCaption, Cancel %>" />
                </div>
            </div>
        </div>
    </div>
		  </asp:Panel>

</div>
