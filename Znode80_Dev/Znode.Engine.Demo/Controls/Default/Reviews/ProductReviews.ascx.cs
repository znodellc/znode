using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Product Reviews user control class.
    /// </summary>
    public partial class Controls_Default_Reviews_ProductReviews : System.Web.UI.UserControl
    {
        #region Private Variables
        private const string ImageFilePath = "<img src='{0}' valign='absmiddle' alt='' border='0' />";
        private static int _currentPage;
        private ZNodeProduct _product;
        private int _pageSize = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayItems;
        private bool _showProsField;
        private bool _showConsField;
        private readonly string _starRatingImagePath = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/Star.gif";
        private readonly string _shadeStarImageImagePath = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/ShadeStar.gif";
      
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the catalog item object. The control will bind to the data from this object
        /// </summary>
        public ZNodeProduct Product
        {
            get
            {
                return this._product;
            }

            set
            {
                this._product = value;
            }
        }

        /// <summary>
        /// Gets or sets the paging size
        /// (i.e) The number of rows from the datasource to display per page
        /// </summary>
        public int PageSize
        {
            get
            {
                return this._pageSize;
            }

            set
            {
                this._pageSize = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the bool value display or hides the Pros text field to the end user
        /// </summary>
        public bool ShowProsField
        {
            get
            {
                return this._showProsField;
            }

            set
            {
                this._showProsField = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether bool value display or hides the Cons text field to the end user
        /// </summary>
        public bool ShowConsField
        {
            get 
            { 
                return this._showConsField; 
            }

            set 
            { 
                this._showConsField = value; 
            }
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
			var productReviews = Product.ZNodeReviewCollection;	
			var reviewsCount = productReviews.Count;

			//If we have reviews
			if (reviewsCount > 0)
			{
				//Sort reviews by newest first
				productReviews.Sort("CreateDate", SortDirection.Descending);

				var datalistPaging = new PagedDataSource
					{
						DataSource = productReviews,
						PageSize = PageSize,
						AllowPaging = true,
						CurrentPageIndex = 0
					};

				_currentPage = 0;

				//This footer text within Reviews, example: <<Previous | PAGE 1 of 1 | Next>>
				lblPagingInfo.Text = " | " + Resources.CommonCaption.Page + (_currentPage + 1) + " " + Resources.CommonCaption.of + " " + datalistPaging.PageCount.ToString(CultureInfo.InvariantCulture) + " | ";

				// Checking for enabling/disabling next/prev buttons.
				// Next/prev button will be disabled when is the last/first page of the pageobject.        
				TopNextLink.Enabled = !datalistPaging.IsLastPage;
				TopPrevLink.Enabled = !datalistPaging.IsFirstPage;

				ViewState["ReviewList"] = productReviews;
				ReviewList.DataSource = datalistPaging;
				ReviewList.DataBind();
			}
            else
			{
				//Hiding Sorting Options
				RateList.Visible = false;
				//Hiding Paging Options
                topNavigation.Visible = false;
                lblErrorMsg.Text = GetLocalResourceObject("NoReviews").ToString();
            }

			CustomersCnt.Text = reviewsCount.ToString(CultureInfo.InvariantCulture);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            if (!Page.IsPostBack)
				this.Bind();
            
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Returns star image for this rating value
        /// </summary>
        /// <param name="rate">The Rating Value</param>
        /// <returns>Returns the Star Image Path</returns>
        protected string GetRatingImagePath(string rate)
        {
            var sb = new StringBuilder();

            for (var i = 1; i <= 5; i++)
            {
                if (int.Parse(rate) >= i)
                {
                    sb.Append(string.Format(ImageFilePath, ResolveUrl(this._starRatingImagePath)));
                }
                else
                {
                    sb.Append(string.Format(ImageFilePath, ResolveUrl(this._shadeStarImageImagePath)));
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Returns the Formatted Date
        /// </summary>
        /// <param name="createdDate">Created Date</param>
        /// <returns>Returns the formatted date</returns>
        protected string GetFormattedDate(object createdDate)
        {
            var dt = DateTime.Parse(createdDate.ToString());
            return dt.ToString("m") + ", " + dt.Year;
        }

        /// <summary>
        /// Represents the Next Record Method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void NextRecord(object sender, EventArgs e)
        {
            _currentPage += 1;

            this.BindDataListPagedDataSource();
        }

        /// <summary>
        /// Represents the Previous Record method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void PrevRecord(object sender, EventArgs e)
        {
            _currentPage -= 1;

            this.BindDataListPagedDataSource();
        }

        /// <summary>
        /// Event is fired when RateList selected Index is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RateList_SelectedIndexChanged(object sender, EventArgs e)
        {
            _currentPage = 0;
            this.BindDataListPagedDataSource();
        }

        #endregion

        #region DataList Paging Methods & Events

        /// <summary>
        /// Bind Datalist using Paged DataSource object
        /// </summary>
        private void BindDataListPagedDataSource()
        {
            // Declare dataset
            var prodReviewList = new ZNodeGenericCollection<ZNodeReview>();
           
			// Retrieve dataset from Viewstate
	        if (ViewState["ReviewList"] != null)
		        prodReviewList = ViewState["ReviewList"] as ZNodeGenericCollection<ZNodeReview>;
            
			if (prodReviewList == null)
	        {
				topNavigation.Visible = false;
				lblErrorMsg.Text = GetLocalResourceObject("NoReviews").ToString();
		        return;
	        }
	
			// Check whether dataview object has more than 0 items
            if (prodReviewList.Count > 0 )
            {             
				// Filter reviews
                if (RateList.SelectedValue.Equals("desc"))
                {
					prodReviewList.Sort("CreateDate", SortDirection.Descending);
                }
                else if (RateList.SelectedValue.Equals("asc"))
                {
					prodReviewList.Sort("CreateDate", SortDirection.Ascending);
                }
                else if (RateList.SelectedValue.Equals("5"))
                {
                    prodReviewList.Sort("Rating", SortDirection.Descending);
                }
                else if (RateList.SelectedValue.Equals("1"))
                {
                    prodReviewList.Sort("Rating", SortDirection.Ascending);
                }
            }

            var datalistPaging = new PagedDataSource
	            {
		            DataSource = prodReviewList,
		            PageSize = PageSize,
		            AllowPaging = true,
		            CurrentPageIndex = _currentPage
	            };

	        lblPagingInfo.Text = " | Page " + (_currentPage + 1) + " of " + datalistPaging.PageCount + " | ";

            // Checking for enabling/disabling next/prev buttons.
            // Next/prev buton will be disabled when is the last/first page of the pageobject.        
            TopNextLink.Enabled = !datalistPaging.IsLastPage;
            TopPrevLink.Enabled = !datalistPaging.IsFirstPage;

            ReviewList.DataSource = datalistPaging;
            ReviewList.DataBind();
        }
        #endregion
    }
}