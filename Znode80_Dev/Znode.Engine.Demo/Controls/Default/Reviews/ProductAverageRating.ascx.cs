using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the ProductAverageRating user control class.
    /// </summary>
    public partial class Controls_Default_Reviews_ProductAverageRating : System.Web.UI.UserControl
    {
        #region Private Variables

        private const string _ImageFilePath = "<img src='{0}' valign='absmiddle' alt='' border='0' />";
        private bool _IsQuickWatch = false;
        private string _StarRatingImagePath = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/Star.gif";
        private string _ShadeStarImageImagePath = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/ShadeStar.gif";
        private string _ViewProductLink = string.Empty;
        private int _ReviewRating = 0;
        private int _TotalReviews = 0;
        private bool _ShowWriteReviewLink = false;
       
        #endregion

        #region Public Properties
        /// <summary>
        /// Sets or retrieves the product review collection list
        /// </summary>
        public int ReviewRating
        {
            set
            {
                this._ReviewRating = value;
                this.DisplayRating();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether QuickWatch is enabled or not
        /// </summary>
        public bool IsQuickWatch
        {
            get
            {
                return this._IsQuickWatch;
            }

            set
            {
                this._IsQuickWatch = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show Write Review Link or not
        /// </summary>
        public bool ShowWriteReviewLink
        {
            get
            {
                return this._ShowWriteReviewLink;
            }

            set
            {
                this._ShowWriteReviewLink = value;
            }
        }

        /// <summary>
        /// Sets the product total reviews
        /// </summary>
        public int TotalReviews
        {
            set
            {
                this._TotalReviews = value;
                this.DisplayRating();
            }
        }

        /// <summary>
        /// Gets or sets the product view link with mode selected
        /// </summary>
        public string ViewProductLink
        {
            get
            {
                return this._ViewProductLink;
            }

            set
            {
                this._ViewProductLink = value;
                this.DisplayRating();
            }
        }
        #endregion
       
        #region Helper Methods
        /// <summary>
        /// Show star image and total no.of reviews. 
        /// </summary>
        protected void DisplayRating()
        {
            if (this.ViewProductLink.Contains("?zpid=") && !this.ViewProductLink.Contains("mode=review"))
            {
                this.ViewProductLink += "&mode=review";
            }
            else if (!this.ViewProductLink.Contains("mode=review"))
            {
                this.ViewProductLink += "?mode=review";
            }

            if (this._IsQuickWatch)
            {
                // Product Link
                Productpath1.HRef = Productpath2.HRef = "javascript:self.parent.location ='" + ResolveUrl(this.ViewProductLink) + "'";
            }
            else
            {
                // Product Link
                Productpath1.HRef = this.ViewProductLink;

                // Product Link
                Productpath2.HRef = this.ViewProductLink;
            }

            ImageRatingStar.Controls.Clear();

            for (int i = 1; i <= 5; i++)
            {
                if (this._ReviewRating >= i)
                {
                    ImageRatingStar.Controls.Add(ParseControl(string.Format(_ImageFilePath, ResolveUrl(this._StarRatingImagePath))));
                }
                else
                {
                    ImageRatingStar.Controls.Add(ParseControl(string.Format(_ImageFilePath, ResolveUrl(this._ShadeStarImageImagePath))));
                }
            }

            // TotalRating count
            lbltotalRating.Text = this._TotalReviews.ToString() + " " + this.GetLocalResourceObject("Review").ToString();
            ReviewLink.Visible = this._ShowWriteReviewLink;
        }
        #endregion
    }
}