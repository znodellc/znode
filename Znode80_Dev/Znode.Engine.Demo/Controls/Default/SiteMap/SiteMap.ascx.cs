using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the SiteMap user control class.
    /// </summary>
    public partial class Controls_Default_SiteMap_SiteMap : System.Web.UI.UserControl
    {
        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();

            // Set SEO Properties
            ZNodeSEO seo = new ZNodeSEO();
            seo.SEOTitle = resourceManager.GetGlobalResourceObject("SiteMap", "txtSiteMap");
        }
    }
}