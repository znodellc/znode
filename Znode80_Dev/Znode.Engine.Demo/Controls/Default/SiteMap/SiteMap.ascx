<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_SiteMap_SiteMap" Codebehind="SiteMap.ascx.cs" %>
<%@ Register Src="~/Controls/Default/category/NavigationCategories.ascx" TagName="NavigationCategories" TagPrefix="ZNode" %>

<div class="SiteMap">
	<div class="PageTitle"><asp:Localize ID="Localize1" Text="<%$ Resources:SiteMap, txtSiteMap %>" runat="server"/>
</div>
	
	<div id="LeftColumn">
        <div class="ShoppingCartNavigation">
			<asp:TreeView ID="uxTreeView" runat="server" DataSourceID="SiteMapDataSource1" 
                NodeIndent="0" ShowExpandCollapse="False" 
                meta:resourcekey="uxTreeViewResource1">
				<RootNodeStyle CssClass="Title"/>
				<ParentNodeStyle CssClass="Title"/>				
				<HoverNodeStyle CssClass="HoverNodeStyle"  />
				<SelectedNodeStyle CssClass="SelectedNodeStyle" HorizontalPadding="0px" VerticalPadding="0px" />
				<LeafNodeStyle CssClass="LeafNodeStyle" />
				<NodeStyle CssClass="NodeStyle" />
			</asp:TreeView>			
			<asp:SiteMapDataSource ID="SiteMapDataSource1" runat="server" />			
		</div> 
	</div>
	
	
	<div id="MiddleColumn" style="padding-top:10px; margin-left:0px ">
		<div class="CatalogMap">
			<div  class="Title"><asp:Localize ID="Localize2" EnableViewState="false" Text="<%$ Resources:SiteMap, txtCatalogMap %>" runat="server"/></div>
			<div class="CategoryNavigation"><ZNode:NavigationCategories EnableViewState="false" ID="uxNavigationCategories" runat="server" /></div>
        </div>
    </div>
</div>