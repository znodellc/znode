<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Znode.Engine.Demo.Controls_Default_Account_AffiliateSignup" Codebehind="AffiliateSignup.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<h1>
    <uc1:CustomMessage ID="CustomMessage1" MessageKey="PartnerSignupTitle" runat="server" />
</h1>
<asp:Panel ID="pnlContact" runat="server" Visible="true">
    <p>
        <uc1:CustomMessage ID="CustomMessage2" MessageKey="PartnerSignupIntroText" runat="server" />
    </p>
    <div class="Form">
        <asp:Label ID="lblError" runat="server" Visible="false" CssClass="Error"></asp:Label>
        <div class="Row Clear"><div class="FormTitle"><asp:Localize ID="Localize6" meta:resourceKey="txtAccountLogin" runat="server" /></div></div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName"><asp:Localize ID="Localize1" meta:resourceKey="txtUserName" runat="server" /></asp:Label>
            </div>
            <div class="LeftContent">
                <asp:TextBox ID="UserName"  autocomplete="off" runat="server" Width="155px"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                        ErrorMessage="<%$ Resources:CommonCaption, UsernameRequired %>" ToolTip="<%$ Resources:CommonCaption, UsernameRequired %>" CssClass="Error"
                        Display="Dynamic"><asp:Localize ID="Localize20" meta:resourceKey="txtUserNameReq" runat="server" /></asp:RequiredFieldValidator></div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password"><asp:Localize ID="Localize2" meta:resourceKey="txtPassword" runat="server" /></asp:Label>
            </div>
            <div class="LeftContent">
                <asp:TextBox ID="Password" runat="server" TextMode="Password" Width="155px"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                        ErrorMessage="<%$ Resources:CommonCaption, PasswordRequired %>" ToolTip="<%$ Resources:CommonCaption, PasswordRequired %>" CssClass="Error"
                        Display="Dynamic"><asp:Localize ID="Localize3" meta:resourceKey="txtPasswordrequired" runat="server" /></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="PasswordRegularExpression" ControlToValidate="Password"
                        runat="server" ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression %>"
                        Display="Dynamic" CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, ValidPasswordRequired %>"></asp:RegularExpressionValidator></div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="lblSecretQuestion" runat="server" AssociatedControlID="txtEmail"><asp:Localize ID="Localize4" meta:resourceKey="txtSecretQuestion" runat="server" /></asp:Label>
            </div>
            <div class="LeftContent">
                <div>
                    <asp:DropDownList ID="ddlSecretQuestions" runat="server">
                        <asp:ListItem Enabled="true" Selected="True" Text="<%$ Resources:CommonCaption, txtFavPet %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtCityBorn %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtSchool %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtMovie %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtMotherName %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtCar %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtColor %>" />
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer"><asp:Localize ID="Localize31" meta:resourceKey="txtSecurityAnswer" runat="server" /></asp:Label>
            </div>
            <div class="LeftContent">
                <asp:TextBox ID="Answer" runat="server" Width="155px"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" ControlToValidate="Answer"
                        ErrorMessage="<%$ Resources:CommonCaption, SecurityAnswerRequired %>" ToolTip="<%$ Resources:CommonCaption, SecurityAnswerRequired %>"
                        CssClass="Error" Display="Dynamic"><asp:Localize ID="Localize5" meta:resourceKey="txtSecurityAnswerRequired" runat="server" /></asp:RequiredFieldValidator></div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent"><asp:Localize ID="Localize7" meta:resourceKey="txtSign" runat="server" /></div>
            <div class="LeftContent">
                <div class="ValueStyle">
                    <asp:DropDownList ID="lstProfile" runat="server" Width="155px">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FormTitle"><asp:Localize ID="Localize8" meta:resourceKey="txtAccInfo" runat="server" /></div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="FirstName" runat="server"><asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:CommonCaption, FirstName %>"/></asp:Label>
            </div>
            <div class="LeftContent">
                <asp:TextBox ID="txtFirstName" runat="server" Width="155px"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired  %>"
                        ControlToValidate="txtFirstName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent"><asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:CommonCaption, LastName %>"/></div>
            <div class="LeftContent">
                <asp:TextBox ID="txtLastName" runat="server" Width="155px"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" ErrorMessage="<%$ Resources:CommonCaption, LastNameRequired  %>"
                        ControlToValidate="txtLastName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent"><asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:CommonCaption, CompanyName %>"/></div>
            <div class="LeftContent">
                <asp:TextBox ID="txtCompanyName" runat="server" Width="155px"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" ErrorMessage="<%$ Resources:CommonCaption, CompanyNameRequired %>"
                        ControlToValidate="txtCompanyName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent"><asp:Localize ID="Localize12" meta:resourceKey="txtTax" runat="server" /></div>
            <div class="LeftContent">
                <asp:TextBox ID="txtTaxId" runat="server" Width="155px"></asp:TextBox>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent"><asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:CommonCaption, PhoneNumber %>"/></div>
            <div class="LeftContent">
                <asp:TextBox ID="txtBillingPhoneNumber" runat="server" Width="155px"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" ErrorMessage="<%$ Resources:CommonCaption, PhoneNoRequired  %>"
                        ControlToValidate="txtBillingPhoneNumber" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent"><asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:CommonCaption, EmailID %>"/></div>
            <div class="LeftContent">
                <asp:TextBox ID="txtEmail" runat="server" Width="155px"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired %>"
                        ControlToValidate="txtEmail" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                <div>
                    <asp:RegularExpressionValidator ID="regemailID" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired %>" Display="Dynamic" ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"
                        CssClass="Error"></asp:RegularExpressionValidator></div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent"><asp:Localize ID="Localize15" meta:resourceKey="txtStreet1" runat="server" /></div>
            <div class="LeftContent">
                <asp:TextBox ID="txtBillingStreet1" runat="server" Width="155px" Columns="30" MaxLength="100"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" ErrorMessage="<%$ Resources:CommonCaption, Street1Required %>"
                        ControlToValidate="txtBillingStreet1" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent"><asp:Localize ID="Localize16" meta:resourceKey="txtStreet2" runat="server" /></div>
            <div class="LeftContent">
                <asp:TextBox ID="txtBillingStreet2" runat="server" Width="155px" Columns="30" MaxLength="100"></asp:TextBox>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent"><asp:Localize ID="txLiveChat" runat="server" Text="<%$ Resources:CommonCaption, City %>"/></div>
            <div class="LeftContent">
                <asp:TextBox ID="txtBillingCity" runat="server" Width="155px" Columns="30" MaxLength="100"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" ErrorMessage="<%$ Resources:CommonCaption, CityRequired %>"
                        ControlToValidate="txtBillingCity" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent"><asp:Localize ID="Localize18" runat="server" Text="<%$ Resources:CommonCaption, State %>"/></div>
            <div class="LeftContent">
                <asp:TextBox ID="txtBillingState" runat="server" Width="30" Columns="10" MaxLength="2"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" ErrorMessage="<%$ Resources:CommonCaption, StateRequired %>"
                        ControlToValidate="txtBillingState" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent"><asp:Localize ID="Localize19" runat="server" Text="<%$ Resources:CommonCaption, PostalCode %>"/></div>
            <div class="LeftContent">
                <asp:TextBox ID="txtBillingPostalCode" runat="server" Width="155px" Columns="30"
                    MaxLength="10"></asp:TextBox><div>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" ErrorMessage="<%$ Resources:CommonCaption, PostalCodeRequired  %>"
                            ControlToValidate="txtBillingPostalCode" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent"><asp:Localize ID="Localize17" runat="server" Text="<%$ Resources:CommonCaption, Country %>"/></div>
            <div class="LeftContent">
                <asp:DropDownList ID="lstBillingCountryCode" Width="200" runat="server">
                </asp:DropDownList>
                <div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator13" ErrorMessage="<%$ Resources:CommonCaption, CountryCodeRequired %>"
                        ControlToValidate="lstBillingCountryCode" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FormTitle" runat="server" id="termsAndConditionTitle"><asp:Localize ID="Localize21" meta:resourceKey="txtTerm" runat="server" /></div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="Label1" runat="server"></asp:Label>
            </div>
            <div class="LeftContent">
                <div class="ValueStyle">
                    <asp:TextBox ReadOnly="true" ID="txtTerms" runat="server" TextMode="MultiLine" Rows="15"
                        Columns="80"></asp:TextBox></div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent">
            </div>
            <div class="LeftContent" style="width:60%">
                <asp:CheckBox ID="chkAccept" runat="server" Text="<%$ Resources:CommonCaption, IAccept %>" />
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent">
                &nbsp;
            </div>
            <div class="LeftContent">
                <ZNode:Spacer ID="Spacer" SpacerHeight="10" SpacerWidth="10" runat="server" />
                <asp:Button ID="btnSignup" Text="<%$ Resources:CommonCaption, Submit %>" runat="server" OnClick="Btnsignup_Click"
                    CssClass="Button" />
            </div>
        </div>
        <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="10" runat="server" />
    </div>
</asp:Panel>
<asp:Panel ID="pnlConfirm" runat="server" Visible="false" CssClass="SuccessMsg">
    <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server" />
    <div>
        <uc1:CustomMessage ID="CustomMessage3" MessageKey="PartnerSignupConfirmationIntroText"
            runat="server" />
    </div>
    <asp:Label ID="ErrorMessage" runat="server" Visible="false"></asp:Label>
    <ZNode:Spacer ID="Spacer1" SpacerHeight="200" SpacerWidth="10" runat="server" />
</asp:Panel>
