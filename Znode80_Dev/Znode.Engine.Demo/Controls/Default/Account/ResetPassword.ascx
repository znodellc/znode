<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Znode.Engine.Demo.Controls_Default_Account_ResetPassword" Codebehind="ResetPassword.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>
<div class="Form">
    <div class="PageTitle"><asp:Localize ID="Localize8" meta:resourceKey="txtResetTitle" runat="server"/></div>
    <div>
        <ZNode:spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="10" runat="server"></ZNode:spacer>
    </div>
    <!-- Reset password Panel -->
    <asp:Panel ID="pnlResetpassword" runat="server">
        <div class="HintText">
            <asp:Label ID="lblIntroMessage" runat="server"></asp:Label></div>
        <div>
            <ZNode:spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></ZNode:spacer>
        </div>
        <div>
            <ZNode:spacer SpacerWidth="4" SpacerHeight="10" ID="Spacer3" runat="server" />
        </div>
        <div class="Error">
            <asp:Literal ID="PasswordFailureText" runat="server" EnableViewState="false"></asp:Literal></div>
        <div>
            <ZNode:spacer SpacerWidth="4" SpacerHeight="10" ID="Spacer4" runat="server" />
        </div>
        <div class="Row" runat="server" id="tblRowUserId">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="UserID" runat="server" AssociatedControlID="UserName"><asp:Localize ID="Localize1" meta:resourceKey="txtUserName" runat="server"/></asp:Label></div>
            <div class="ValueStyle">
                <div>
                    <asp:TextBox ID="UserName" runat="server"  autocomplete="off" Width="155px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="UserName"
                        ErrorMessage="<%$ Resources:CommonCaption, UsernameRequired %>" ToolTip="<%$ Resources:CommonCaption, UsernameRequired %>" ValidationGroup="ChangePassword1"
                        CssClass="Error" Display="Dynamic"  meta:resourceKey="txtUserNameReq" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UserName"
                        CssClass="Error" ToolTip="<%$ Resources:CommonCaption, UsernameRequired %>"
                        ValidationExpression="[a-zA-Z0-9_-]{4,}" ValidationGroup="ChangePassword1" 
                         meta:resourceKey="RegularExpressionValidator1" />
                </div>
                <div class="HintText"><asp:Localize ID="Localize4" meta:resourceKey="txtHint" runat="server"/></div>
            </div>
        </div>
        <div class="Row" runat="server" id="pnlCurrentpassword">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword"><asp:Localize ID="Localize5" meta:resourceKey="txtCurrentPassword" runat="server"/></asp:Label></div>
            <div class="ValueStyle">
                <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password" Width="155px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                    ErrorMessage="<%$ Resources:CommonCaption, PasswordRequired %>" ToolTip="<%$ Resources:CommonCaption, PasswordRequired %>" ValidationGroup="ChangePassword1"
                    CssClass="Error" Display="Dynamic" meta:resourceKey="txtPasswordReq" />
            </div>
        </div>
        <div class="Row">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword"><asp:Localize ID="Localize7" meta:resourceKey="txtNewPassword" runat="server"/></asp:Label></div>
            <div class="ValueStyle">
                <div>
                    <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" Width="155px"></asp:TextBox>
                    <asp:RequiredFieldValidator 
                        ID="NewPasswordRequired" 
                        runat="server" 
                        ControlToValidate="NewPassword"
                        ErrorMessage="<%$ Resources:CommonCaption, NewPasswordRequired %>"
                        ToolTip="<%$ Resources:CommonCaption, NewPasswordRequired %>"
                        ValidationGroup="ChangePassword1" 
                        CssClass="Error" 
                        Display="Dynamic"/>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="NewPassword"
                        CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, ValidPasswordRequired %>"
                        SetFocusOnError="True" ToolTip="<%$ Resources:CommonCaption, ValidPasswordRequired %>"
                        ValidationExpression="<%$ Resources:CommonCaption, PasswordStrengthExpression %>" 
                        ValidationGroup="ChangePassword1" meta:resourceKey="txtInvalidPassword"/>
                </div> 
                <div class="HintText"><asp:Localize ID="Localize11" meta:resourceKey="txtHintPassword" runat="server"/></div>
            </div>
        </div>
        <div class="Row">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword"><asp:Localize ID="Localize12" meta:resourceKey="txtConfirmNewPassword" runat="server"/></asp:Label></div>
            <div class="ValueStyle">
                <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password" Width="155px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                    ErrorMessage="<%$ Resources:CommonCaption, ConfirmPasswordNewRequired %>" ToolTip="<%$ Resources:CommonCaption, ConfirmPasswordNewRequired %>"
                    ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic"
                     meta:resourceKey="txtConfirm"/>
                <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                    ControlToValidate="ConfirmNewPassword" CssClass="Error" Display="Dynamic" meta:resourceKey="txtMatch" ValidationGroup="ChangePassword1"></asp:CompareValidator>
            </div>
        </div>
         <div>
            <ZNode:spacer SpacerWidth="4" SpacerHeight="10" ID="Spacer7" runat="server" />
        </div>
        <div class="Row" runat="server" id="tblRowEmail" visible="false">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email"><asp:Localize ID="Localize14" meta:resourceKey="txtEmail" runat="server"/></asp:Label></div>
            <div class="ValueStyle">
                <asp:TextBox ID="Email" runat="server" ></asp:TextBox>
                <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email"
                    ValidationGroup="ChangePassword1" ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired %>" ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired %>"
                    CssClass="Error" Display="Dynamic" meta:resourceKey="txtEmailReq" />
                <asp:RegularExpressionValidator ID="ValidateEmail" runat="server" ControlToValidate="Email"
                    ValidationGroup="ChangePassword1" CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired %>"
                    ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired %>" ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression %>"
                    Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="Row" id="tblRowSecurityQuestion" runat="server">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="lblSecretQuestion" runat="server" AssociatedControlID="ddlSecretQuestions" meta:resourceKey="txtSecurityQ"/></asp:Label></div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlSecretQuestions" runat="server">
                    <asp:ListItem Enabled="true" Selected="True" Text="<%$ Resources:CommonCaption, txtFavPet %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtCityBorn %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtSchool %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtMovie %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtMotherName %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtCar %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtColor %>" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="Row" id="tblRowPasswordAnswer" runat="server">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer" meta:resourceKey="txtSecurityA"/></asp:Label></div>
            <div class="ValueStyle">
                <asp:TextBox ID="Answer" Width="155px" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Answer"
                    ErrorMessage="<%$ Resources:CommonCaption, SecurityAnswerRequired %>" ToolTip="<%$ Resources:CommonCaption, SecurityAnswerRequired %>"
                    ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic" meta:resourceKey="txtSecurityAReq"/>
            </div>
        </div>
        <div class="Row">
            <div class="FieldStyle LeftContent">
                <ZNode:spacer SpacerWidth="15" SpacerHeight="1" ID="Spacer6" runat="server" />
            </div>
            <div class="ValueStyle">
                <asp:LinkButton ID="ResetPasswordPushButton" runat="server" CommandName="ResetPassword"
                    Text="<%$ Resources:CommonCaption, Submit%>" ValidationGroup="ChangePassword1" CssClass="Button" Width="110px"
                    OnClick="ResetPasswordPushButton_Click" />
            </div>
        </div>
        <div>
            <ZNode:spacer SpacerWidth="15" SpacerHeight="30" ID="Spacer5" runat="server" />
        </div>
    </asp:Panel>
    <!-- Success message section -->
    <asp:Panel ID="pnlSuccess" runat="server" Visible="false">
        <div class="SuccessText"><asp:Localize ID="Localize19" meta:resourceKey="txtSuccess" runat="server"/></asp:Label>
           </div>
        <div>
            <ZNode:spacer SpacerWidth="15" SpacerHeight="25" ID="uxSpacer" runat="server" />
        </div>
        <div class="ContinueButton">
            <asp:LinkButton ID="ContinuePushButton" OnClick="ContinuePushButton_Click" runat="server"
                CausesValidation="False" CommandName="Continue" CssClass="Button" meta:resourceKey="txtContinue"/></div>
        <div>
            <ZNode:spacer SpacerWidth="15" SpacerHeight="30" ID="uxSpacerBottom" runat="server" />
        </div>
    </asp:Panel>
</div>
