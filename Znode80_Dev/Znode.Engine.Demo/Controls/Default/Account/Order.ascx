<%@ Control Language="C#" AutoEventWireup="true" EnableViewState="false"
    Inherits="Znode.Engine.Demo.Controls_Default_Account_Order" Codebehind="Order.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/common/spacer.ascx" %>
<div>
    <div class="PageTitle">
        <asp:Localize ID="Localize8" meta:resourceKey="txtOrdernumber" runat="server" />
        <%=OrderNumber%></div>
</div>
<div class="Form">
    <div class="Row Clear">
        <div class="LeftContent">
            <b>
                <asp:Localize ID="Localize1" meta:resourceKey="txtOrderDate" runat="server" /></b>
        </div>
        <div class="LeftContent">
            <%=OrderDate%>
        </div>
    </div>
    <div class="Row Clear">
        <div class="LeftContent">
            <b>
                <asp:Localize ID="Localize2" meta:resourceKey="txtOrderTotal" runat="server" /></b>
        </div>
        <div class="LeftContent">
            <%=OrderTotal%>
        </div>
    </div>
    <div class="Row Clear">
        <div class="LeftContent">
            <b><asp:Localize ID="Localize11" meta:resourceKey="txtGiftCard" runat="server" /></b>
        </div>
        <div class="LeftContent">
            <%=GiftCard%>
        </div>
    </div>
    <div class="Row Clear">
        <div class="LeftContent">
            <b>
                <asp:Localize ID="Localize3" meta:resourceKey="txtPaymentMethod" runat="server" /></b>
        </div>
        <div class="LeftContent">
            <%=PaymentMethod%>
        </div>
    </div>
    <div class="Row Clear">
        <div class="LeftContent">
            <b>
                <asp:Localize ID="Localize4" meta:resourceKey="txtPurchaseNo" runat="server" /></b>
        </div>
        <div class="LeftContent">
            <%=PurchaseOrderNumber%>
        </div>
    </div>
    <div class="Row Clear">
        <div class="LeftContent">
            <b>
                <asp:Localize ID="Localize5" meta:resourceKey="txtTransactionID" runat="server" /></b>
        </div>
        <div class="LeftContent">
            <%=TransactionID%>
        </div>
    </div>
    <div class="Row Clear">
        <div class="LeftContent">
            <b>
                <asp:Localize ID="Localize6" meta:resourceKey="txtTrackingNo" runat="server" /></b>
        </div>
        <div class="LeftContent">
            <%=TrackingNumber%>
        </div>
    </div>
    <div class="Row Clear">
        &nbsp;
    </div>
    <div class="Row Clear">
        <div class="LeftContent">
            <b>
                <asp:Localize ID="Localize7" meta:resourceKey="txtBillTo" runat="server" /></b>
        </div>
        <div class="LeftContent">
            <%=BillingAddress%>
        </div>
    </div>
    <div class="Row Clear">
        &nbsp;
    </div>
    <div class="Row Clear">
        <div class="LeftContent">
            <b>
                <asp:Localize ID="Localize9" meta:resourceKey="txtShipTo" runat="server" /></b>
        </div>
        <div class="LeftContent" style="width:auto;">
            <%=ShippingAddress%>
        </div>
    </div>
    <div>
        <div class="Clear">
            <ZNode:Spacer ID="Spacer1" SpacerHeight="30" SpacerWidth="10" runat="server"></ZNode:Spacer>
        </div>
    </div>
</div>
<div>
    <asp:UpdatePanel runat="server" ID="updatePnlReOrder" UpdateMode="Conditional">
	<ContentTemplate>
    <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
        CellPadding="4" ForeColor="#333333" GridLines="None" EnableViewState="false"
        CaptionAlign="Left" Width="100%" EnableSortingAndPagingCallbacks="False" PageSize="15"
        OnPageIndexChanging="UxGrid_PageIndexChanging" OnDataBound="UxGrid_DataBound"
        OnRowDataBound="UxGrid_RowDataBound" OnRowCommand="UxGrid_RowCommand">
        <Columns>
            <asp:BoundField DataField="Quantity" meta:resourceKey="Quantity" HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="Name" meta:resourceKey="ProductName" HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField meta:resourceKey="Description" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <%# DataBinder.Eval(Container.DataItem, "Description") %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField meta:resourceKey="Price" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <%# DataBinder.Eval(Container.DataItem, "Price", "{0:c}") %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField meta:resourceKey="LicenseKey" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <div>
                        <%# GetDigitalAssets(int.Parse(DataBinder.Eval(Container.DataItem, "OrderLineItemId").ToString()))%></div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField Visible="false" meta:resourceKey="DownloadLink" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <div>
                        <asp:HyperLink ID="HDownloadLink" runat="server" NavigateUrl='<%# GetDownloadLinkText(DataBinder.Eval(Container.DataItem, "DownloadLink"))%>'>
                            <asp:Localize ID="Localize11" meta:resourceKey="txtDownload" runat="server" />
                        </asp:HyperLink>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="LinkButton1" runat="server" CommandName="Reorder" CssClass="Button"
                        meta:resourceKey="Reorder" Visible='<%# ShowReorder(DataBinder.Eval(Container.DataItem, "ParentOrderLineItemId")) %>'></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HiddenField ID="hiddenOrderlineitemid" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "OrderLineItemId") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HiddenField ID="hiddenSKU" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "SKU") %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:HiddenField ID="hiddenProductNum" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ProductNum") %>' />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle CssClass="FooterStyle" />
        <RowStyle CssClass="RowStyle" />
        <PagerStyle CssClass="PagerStyle" />
        <HeaderStyle CssClass="HeaderStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
    </asp:GridView>
        <div>
            <br />
            <br />
        </div>
        <div>
            <asp:Label ID="lblError" runat="server" Visible="false" CssClass="Error"></asp:Label>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</div>
<div>
    <br />
    <br />
</div>

<div>
    <a id="A1" href="~/account.aspx" enableviewstate="false" runat="server">
        <asp:Localize ID="Localize10" meta:resourceKey="txtBack" runat="server" /></a>
</div>
<div>
    <ZNode:Spacer ID="Spacer5" EnableViewState="false" SpacerHeight="30" SpacerWidth="10"
        runat="server"></ZNode:Spacer>
</div>
