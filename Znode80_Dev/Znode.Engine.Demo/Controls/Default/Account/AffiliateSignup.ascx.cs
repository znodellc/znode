using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Affiliate Sign Up user control class.
    /// </summary>
    public partial class Controls_Default_Account_AffiliateSignup : System.Web.UI.UserControl
    {
        #region Private Variable
        private string _SourceText = "Affiliate Program";
        #endregion

        #region Public Property
        /// <summary>
        /// Gets or sets the SourceText
        /// </summary>
        public string SourceText
        {
            get
            {
                return this._SourceText;
            }

            set
            {
                this._SourceText = value;
            }
        }
        #endregion

        #region Page Load Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                Page.Form.DefaultFocus = UserName.ClientID;
                chkAccept.Attributes.Add("onclick", "return EnableSignupButton('" + chkAccept.ClientID + "','" + btnSignup.ClientID + "');");
                string key = string.Empty;
                key = "PartnerSignupTermsandConditions";
                txtTerms.Text = ZNodeCatalogManager.MessageConfig.GetMessage(key);
                if (txtTerms.Text.Length == 0)
                {
                    chkAccept.Visible = false;
                    txtTerms.Visible = false;
                    termsAndConditionTitle.Visible = false;
                    btnSignup.Enabled = true;
                }
                else
                {
                    chkAccept.Visible = true;
                    txtTerms.Visible = true;
                    termsAndConditionTitle.Visible = true;
                    btnSignup.Enabled = false;
                    chkAccept.Checked = false;
                }

                this.BindCountry();
                this.BindProfiles();
            }

            if (chkAccept.Checked)
            {
                btnSignup.Enabled = true;
            }

            if (this.Page != null)
            {
                this.Page.Title = ZNodeCatalogManager.MessageConfig.GetMessage("PartnerSignupTitle", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
            }
        }
        #endregion

        #region Events
        protected void Btnsignup_Click(object sender, EventArgs e)
        {
            this.RegisterUser();
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// This function registers a new user 
        /// </summary>
        private void RegisterUser()
        {
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();
            log.LogActivityTimerStart();

            // Check if loginName already exists in DB
            ZNodeUserAccount userAccount = new ZNodeUserAccount();
            if (UserName.Text.Length > 0)
            {
                if (!userAccount.IsLoginNameAvailable(ZNodeConfigManager.SiteConfig.PortalID, UserName.Text.Trim()))
                {
                    log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text, null, null, "User name already exists", null);
                    lblError.Text = Resources.CommonCaption.LoginFailed;
                    lblError.Visible = true;
                    return;
                }

                // Create user membership
                MembershipCreateStatus memStatus;
                MembershipUser user = Membership.CreateUser(UserName.Text, Password.Text, txtEmail.Text, ddlSecretQuestions.SelectedItem.Text, Answer.Text, true, out memStatus);

                if (memStatus != MembershipCreateStatus.Success)
                {
                    log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text);
                    lblError.Text = "Could not create user account. Please contact customer support.";
                    lblError.Visible = true;

                    return;
                }

                log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, UserName.Text);

                // Create the user account
                ZNodeUserAccount newUserAccount = new ZNodeUserAccount();

                newUserAccount.UserID = (Guid)user.ProviderUserKey;
                newUserAccount.CompanyName = txtCompanyName.Text;
                newUserAccount.ReferralStatus = "N";
                newUserAccount.TaxId = txtTaxId.Text;
                newUserAccount.ReferralCommission = 0;
                newUserAccount.Source = this.SourceText;
                if (lstProfile.Items.Count > 0)
                {
                    newUserAccount.ProfileID = Convert.ToInt32(lstProfile.SelectedValue);
                }

                newUserAccount.EmailOptIn = true;
                newUserAccount.EmailID = txtEmail.Text;

                try
                {
                    // Register account        
                    newUserAccount.AddUserAccount();

                    AccountAdmin accountAdmin = new AccountAdmin();
                    AddressService addressService = new AddressService();
                    Address billingAddress = accountAdmin.GetDefaultBillingAddress(newUserAccount.AccountID);
                    if (billingAddress == null)
                    {
                        billingAddress = new Address();
                        billingAddress.IsDefaultBilling = true;
                        billingAddress.AccountID = newUserAccount.AccountID;
                    }

                    Address shippingAddress = accountAdmin.GetDefaultBillingAddress(newUserAccount.AccountID);
                    if (shippingAddress == null)
                    {
                        shippingAddress = new Address();
                        shippingAddress.IsDefaultShipping = true;
                        shippingAddress.AccountID = newUserAccount.AccountID;
                    }

                    // Copy info to billing
                    billingAddress.FirstName = txtFirstName.Text;
                    billingAddress.LastName = txtLastName.Text;
                    billingAddress.CompanyName = txtCompanyName.Text;
                    billingAddress.PhoneNumber = txtBillingPhoneNumber.Text;
                    billingAddress.Street = txtBillingStreet1.Text;
                    billingAddress.Street1 = txtBillingStreet2.Text;
                    billingAddress.City = txtBillingCity.Text;
                    billingAddress.StateCode = txtBillingState.Text.ToUpper();
                    billingAddress.PostalCode = txtBillingPostalCode.Text;
                    billingAddress.CountryCode = lstBillingCountryCode.SelectedValue;
                    newUserAccount.BillingAddress = billingAddress;

                    // Copy info to shipping
                    shippingAddress.FirstName = txtFirstName.Text;
                    shippingAddress.LastName = txtLastName.Text;
                    shippingAddress.CompanyName = txtCompanyName.Text;
                    shippingAddress.PhoneNumber = txtBillingPhoneNumber.Text;
                    shippingAddress.Street = txtBillingStreet1.Text;
                    shippingAddress.Street1 = txtBillingStreet2.Text;
                    shippingAddress.City = txtBillingCity.Text;
                    shippingAddress.StateCode = txtBillingState.Text.ToUpper();
                    shippingAddress.PostalCode = txtBillingPostalCode.Text;
                    shippingAddress.CountryCode = lstBillingCountryCode.SelectedValue;

                    // Insert shipping address as default billing address.
                    if (billingAddress.AccountID > 0)
                    {
                        addressService.Update(billingAddress);
                    }
                    else
                    {
                        addressService.Insert(billingAddress);
                    }

                    // Insert shipping address as default shipping address.
                    if (shippingAddress.AccountID > 0)
                    {
                        addressService.Update(shippingAddress);
                    }
                    else
                    {
                        addressService.Insert(shippingAddress);
                    }

                    // Log password for further debugging
                    ZNodeUserAccount.LogPassword((Guid)user.ProviderUserKey, Password.Text.Trim());

                    // Update selected profile
                    if (newUserAccount.AccountID > 0 && lstProfile.SelectedIndex != -1)
                    {
                        int profileId = 0;

                        if (int.TryParse(lstProfile.SelectedValue, out profileId))
                        {
                            AccountProfile accountProfile = new AccountProfile();
                            accountProfile.AccountID = newUserAccount.AccountID;
                            accountProfile.ProfileID = profileId;
                            AccountProfileService accountProfileServ = new AccountProfileService();
                            accountProfileServ.Insert(accountProfile);
                        }
                    }

                    ErrorMessage.Visible = false;
                    pnlConfirm.Visible = true;
                    pnlContact.Visible = false;
                    return;
                }
                catch
                {
                    ErrorMessage.Visible = true;
                    ErrorMessage.Text = "Could not create new contact. Please contact customer support.";
                    pnlConfirm.Visible = true;
                    pnlContact.Visible = false;
                    if (chkAccept.Checked)
                    {
                        btnSignup.Enabled = true;
                    }
                    else
                    {
                        btnSignup.Enabled = false;
                    }

                    return;
                }
            }
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Binds country drop-down list
        /// </summary>
        private void BindCountry()
        {
            // PortalCountry
            ZNode.Libraries.DataAccess.Custom.PortalCountryHelper portalCountryHelper = new ZNode.Libraries.DataAccess.Custom.PortalCountryHelper();
            DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

            DataView billingView = new DataView(countryDs.Tables[0]);
            billingView.RowFilter = "BillingActive = 1";

            lstBillingCountryCode.DataSource = billingView;
            lstBillingCountryCode.DataTextField = "Name";
            lstBillingCountryCode.DataValueField = "Code";
            lstBillingCountryCode.DataBind();
            lstBillingCountryCode.SelectedValue = "US";
        }

        /// <summary>
        /// Binds Profile drop-down list
        /// </summary>
        private void BindProfiles()
        {
            ProfileService profileService = new ProfileService();
            ProfileQuery query = new ProfileQuery();
            query.Append(ProfileColumn.ShowOnPartnerSignup, "true");

            TList<Profile> profileList = profileService.Find(query.GetParameters());
            lstProfile.DataSource = profileList;
            lstProfile.DataTextField = "Name";
            lstProfile.DataValueField = "ProfileID";
            lstProfile.DataBind();

            foreach (Profile profile in profileList)
            {
                lstProfile.SelectedValue = profile.ProfileID.ToString();
                break;
            }
        }
        #endregion
    }
}