using System;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Order user control class.
    /// </summary>
    public partial class Controls_Default_Account_Order : System.Web.UI.UserControl
    {
        #region Private Variables
        private int _itemID;
        private ZNodeUserAccount _userAccount;
        private string _AccountPageLink = string.Empty;
        private string _OrderNumber = string.Empty;
        private string _OrderDate = string.Empty;
        private string _OrderTotal = string.Empty;
        private string _PaymentMethod = string.Empty;
        private string _PurchaseOrderNumber = string.Empty;
        private string _TransactionID = string.Empty;
        private string _TrackingNumber = string.Empty;
        private string _BillingAddress = string.Empty;
        private string _ShippingAddress = string.Empty;
        private bool _DigitalAssetExist = false;
        private bool _DownloadLinkExist = false;
        private string _DownLoadLinkText = string.Empty;
        private string _GiftCard = string.Empty;

        #endregion

        #region Public Properties     

        /// <summary>
        /// Gers or sets the account page link
        /// </summary>
        public string AccountPageLink
        {
            get { return _AccountPageLink; }
            set { _AccountPageLink = value; }
        }
       
        /// <summary>
        /// Gers or sets the order number
        /// </summary>
        public string OrderNumber
        {
            get { return _OrderNumber; }
            set { _OrderNumber = value; }
        }
        
        /// <summary>
        /// Gers or sets the order date.
        /// </summary>
        public string OrderDate
        {
            get { return Convert.ToDateTime(_OrderDate).ToString("MM/dd/yyyy hh:mm tt"); }
            set { _OrderDate = value; }
        }
       
        /// <summary>
        /// Gers or sets the order total.
        /// </summary>
        public string OrderTotal
        {
            get { return _OrderTotal; }
            set { _OrderTotal = value; }
        }
       
        /// <summary>
        /// Gers or sets the payment method.
        /// </summary>
        public string PaymentMethod
        {
            get { return _PaymentMethod; }
            set { _PaymentMethod = value; }
        }
       
        /// <summary>
        /// Gers or sets the purchase order number
        /// </summary>
        public string PurchaseOrderNumber
        {
            get { return _PurchaseOrderNumber; }
            set { _PurchaseOrderNumber = value; }
        }
      
        /// <summary>
        /// Gers or sets the tracnsaction Id.
        /// </summary>
        public string TransactionID
        {
            get { return _TransactionID; }
            set { _TransactionID = value; }
        }
        
        /// <summary>
        /// Gers or sets the tracking number.
        /// </summary>
        public string TrackingNumber
        {
            get { return _TrackingNumber; }
            set { _TrackingNumber = value; }
        }
       
        /// <summary>
        /// Gers or sets the billing address.
        /// </summary>
        public string BillingAddress
        {
            get { return _BillingAddress; }
            set { _BillingAddress = value; }
        }
       
        /// <summary>
        /// Gers or sets the shipping address.
        /// </summary>
        public string ShippingAddress
        {
            get { return _ShippingAddress; }
            set { _ShippingAddress = value; }
        }
        
        /// <summary>
        /// Gers or sets a value indicating whether the digital assets exists for the order.
        /// </summary>
        public bool DigitalAssetExist
        {
            get { return _DigitalAssetExist; }
            set { _DigitalAssetExist = value; }
        }
       
        /// <summary>
        /// Gers or sets a value indicating whether the download link exist for the order.
        /// </summary>
        public bool DownloadLinkExist
        {
            get { return _DownloadLinkExist; }
            set { _DownloadLinkExist = value; }
        }
       
        /// <summary>
        /// Gers or sets the download link text
        /// </summary>
        public string DownLoadLinkText
        {
            get { return _DownLoadLinkText; }
            set { _DownLoadLinkText = value; }
        }
        
        /// <summary>
        /// Gers or sets the gift card number
        /// </summary>
        public string GiftCard
        {
            get { return _GiftCard; }
            set { _GiftCard = value; }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Returns payment type name for this payment type id
        /// </summary>
        /// <param name="PaymentTypeId">Payment Type Id</param>
        /// <returns>Returns the Payment Type Name</returns>
        public string GetPaymentTypeName(int PaymentTypeId)
        {
            string Name = string.Empty;
            ZNode.Libraries.Admin.StoreSettingsAdmin settingsAdmin = new ZNode.Libraries.Admin.StoreSettingsAdmin();
            PaymentType entity = settingsAdmin.GetPaymentTypeById(PaymentTypeId);
            if (entity != null)
            {
                Name = entity.Name;
            }

            return Name;
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Returns all digital assets for this Order line Item
        /// </summary>
        /// <param name="OrderLineItemId">Order Line Item Id</param>
        /// <returns>Returns the Digital Asset value</returns>
        protected string GetDigitalAssets(int OrderLineItemId)
        {
            ZNode.Libraries.DataAccess.Service.DigitalAssetService _digitalAssetService = new ZNode.Libraries.DataAccess.Service.DigitalAssetService();
            ZNode.Libraries.DataAccess.Entities.TList<ZNode.Libraries.DataAccess.Entities.DigitalAsset> digitalAssetList = _digitalAssetService.GetByOrderLineItemID(OrderLineItemId);
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            foreach (ZNode.Libraries.DataAccess.Entities.DigitalAsset digitalAsset in digitalAssetList)
            {
                sb.Append(digitalAsset.DigitalAsset + "<br />");
                this.DigitalAssetExist = true;
            }

            return sb.ToString();
        }

        protected string GetDownloadLinkText(object DownLoadLinkText)
        {
            if (DownLoadLinkText == null)
            {
                return string.Empty;
            }

            return DownLoadLinkText.ToString();
        }

        protected bool ShowReorder(object ParentOrderLineItemId)
        {
            if (ParentOrderLineItemId == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.Bind();
        }

        protected void UxGrid_DataBound(object sender, EventArgs e)
        {
            if (this.DigitalAssetExist)
            {
                uxGrid.Columns[4].Visible = this.DigitalAssetExist;
            }
            else
            {
                uxGrid.Columns[4].Visible = false;
            }
        }

        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                HyperLink hlink1 = (HyperLink)e.Row.FindControl("HDownloadLink");
                string DownloadLink = hlink1.NavigateUrl.ToString();
                if (string.IsNullOrEmpty(this.DownLoadLinkText) && string.IsNullOrEmpty(DownloadLink))
                {
                    e.Row.Cells[5].Text = string.Empty;
                }
                else if (string.IsNullOrEmpty(this.DownLoadLinkText) && !string.IsNullOrEmpty(DownloadLink))
                {
                    this.DownloadLinkExist = true;
                }
            }
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (string.Compare(e.CommandName, "Reorder", true) == 0)
            {
                GridViewRow selectedRow = (e.CommandSource as LinkButton).NamingContainer as GridViewRow;  // uxGrid.Rows[index];

                // Get OrderlineItemId from Selected row
                TableCell Idcell = selectedRow.Cells[7];
                HiddenField hiddenid = (HiddenField)Idcell.Controls[1];

                int _orderlineitemid;
                if (!int.TryParse(hiddenid.Value, out _orderlineitemid))
                {
                    _orderlineitemid = 0;
                }

                // Get product SKU from Selected row
                TableCell Skucell = selectedRow.Cells[8];
                HiddenField hiddensku = (HiddenField)Skucell.Controls[1];
                string SKU = hiddensku.Value;

                // Get ProductNum from Selected row
                TableCell ProductNumcell = selectedRow.Cells[9];
                HiddenField hiddenprodnum = (HiddenField)ProductNumcell.Controls[1];
                string _productNum = hiddenprodnum.Value;

                int _quantity;

                // Get product Quantity from Selected row
                TableCell Quantitycell = selectedRow.Cells[0];
                if (!int.TryParse(Quantitycell.Text, out _quantity))
                {
                    _quantity = 1;
                }

                if ((SKU.Length == 0 && _productNum.Length == 0) || (ZNodeProfile.CurrentUserProfile.ShowPricing == false))
                {
                    lblError.Visible = true;
                    lblError.Text = "We are sorry but " + selectedRow.Cells[1].Text + " is no longer available.";
                    updatePnlReOrder.Update();
                    return;
                }

                Reorder reorder = new Reorder();
                if (reorder.ReorderItem(_orderlineitemid, _productNum, SKU, _quantity))
                {
                    string link = "~/shoppingcart.aspx";
                    Response.Redirect(link);
                }
                else
                {
                    lblError.Visible = true;
                    lblError.Text = "We are sorry but " + selectedRow.Cells[1].Text + " is no longer available.";
                    updatePnlReOrder.Update();
                    return;
                }
            }
        }
        #endregion

        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.QueryString["itemid"] != null)
            {
                this._itemID = int.Parse(Request.QueryString["itemid"]);
            }
            else
            {
                this._itemID = 0;
                Response.Redirect("account.aspx");
            }

            this.OrderNumber = this._itemID.ToString();

            // If the user has not logged in
            if (!HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/login.aspx?ReturnURL=order.aspx?itemId=" + Request.QueryString["itemid"]);
            }

            this._userAccount = ZNodeUserAccount.CurrentAccount();

            // Bind grid data
            this.Bind();

            // Account page link
            ZNodeUrl url = new ZNodeUrl();
            this.AccountPageLink = url.GetURLFromPageId("account");

            if (this.Page != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "Title");
            }

            
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Bind Order data
        /// </summary>
        private void Bind()
        {
            // First retrieve order info by orderid to check if it valid for the current account
            ZNode.Libraries.DataAccess.Service.OrderService ordServ = new OrderService();
            ZNode.Libraries.DataAccess.Entities.Order ord = ordServ.GetByOrderID(this._itemID);

            ZNode.Libraries.DataAccess.Service.OrderLineItemService oliServ = new OrderLineItemService();
            TList<OrderLineItem> orderLineItems = oliServ.DeepLoadByOrderID(this._itemID, true, DeepLoadType.IncludeChildren, typeof(OrderShipment));
            var orderShipmentList = orderLineItems.Where(x => x.OrderShipmentIDSource != null).Select(x => x.OrderShipmentIDSource).Distinct();
 
            // Account id on order matches current user's account id
            if (ord.AccountID == this._userAccount.AccountID) 
            {
                GiftCardHistoryService giftCardHistoryService = new GiftCardHistoryService();
                TList<GiftCardHistory> cardHistoryList = giftCardHistoryService.GetByOrderID(this._itemID);
                if (cardHistoryList.Count == 0)
                {
                    this.GiftCard = "0.00";
                }
                else
                {
                    this.GiftCard = "-" + cardHistoryList[0].TransactionAmount.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                }

                this.OrderDate = ord.OrderDate.ToString();
                this.OrderTotal = ((decimal)ord.Total).ToString("c");
                if (ord.PaymentTypeId.HasValue)
                {
                    string paymentTypeName = this.GetPaymentTypeName(ord.PaymentTypeId.Value);
                    this.PaymentMethod = paymentTypeName;
                }

                this.TransactionID = ord.CardTransactionID;
                this.TrackingNumber = ord.TrackingNumber;
                this.PurchaseOrderNumber = ord.PurchaseOrderNumber;

                // Print billing address info
                Address billingAddress = new Address();
                billingAddress.FirstName = ord.BillingFirstName;
                billingAddress.LastName = ord.BillingLastName;
                billingAddress.Street = ord.BillingStreet;
                billingAddress.Street1 = ord.BillingStreet1;
                billingAddress.City = ord.BillingCity;
                billingAddress.StateCode = ord.BillingStateCode;
                billingAddress.PostalCode = ord.BillingPostalCode;
                billingAddress.CountryCode = ord.BillingCountry;
                billingAddress.CompanyName = ord.BillingCompanyName;
                billingAddress.PhoneNumber = ord.BillingPhoneNumber;                
                this.BillingAddress = billingAddress.ToString();

                // Print shipping address info
                Address shippingAddress = new Address();
                StringBuilder stringBuilder = new StringBuilder();
                int shipmentCounter = 1;
                foreach (var orderShipment in orderShipmentList)
                {
                    shippingAddress.FirstName = orderShipment.ShipToFirstName;
                    shippingAddress.LastName = orderShipment.ShipToLastName;
                    shippingAddress.Street = orderShipment.ShipToStreet;
                    shippingAddress.Street1 = orderShipment.ShipToStreet1;
                    shippingAddress.City = orderShipment.ShipToCity;
                    shippingAddress.StateCode = orderShipment.ShipToStateCode;
                    shippingAddress.PostalCode = orderShipment.ShipToPostalCode;
                    shippingAddress.CountryCode = orderShipment.ShipToCountry;
                    shippingAddress.CompanyName = orderShipment.ShipToCompanyName;
                    shippingAddress.PhoneNumber = orderShipment.ShipToPhoneNumber;
                    if (orderShipmentList.Count() > 1)
                    {
                        Localize9.Text = "Ship To Multiple Address: ";
                        this.ShippingAddress = stringBuilder.Append(string.Format("<span style=\"font-weight:bold;\">Shipment #{0} - {1}</span>", shipmentCounter++, orderShipment.ShipName) + "<br> <br>").ToString();
                    }
                    this.ShippingAddress = stringBuilder.Append(shippingAddress.ToString() + "<br> <br>").ToString();
                }

                uxGrid.DataSource = orderLineItems;
                uxGrid.DataBind();
                if (this.DownloadLinkExist)
                {
                    uxGrid.Columns[5].Visible = true;
                }
                else
                {
                    uxGrid.Columns[5].Visible = false;
                }

                uxGrid.DataBind();
            }
        }
        #endregion
    }
}