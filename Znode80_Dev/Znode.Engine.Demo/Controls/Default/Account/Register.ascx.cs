using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Analytics;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Register user control class.
    /// </summary>
    public partial class Controls_Default_Account_Register : System.Web.UI.UserControl
    {
        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Check if SSL is required
            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {
                    string inURL = Request.Url.ToString();
                    string outURL = inURL.ToLower().Replace("http://", "https://");
                    Response.Redirect(outURL);
                }
            }

            if (this.Page != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtAccountTitle.Text");
            }

            if (!Page.IsPostBack)
            {
                // Add the listrack email capture script.
                ZNodeAnalytics analytics = new ZNodeAnalytics();
                analytics.AnalyticsData.EmailCaptureContorlId = Email.ClientID;
                analytics.Bind();
                string emailCaptureScript = analytics.AnalyticsData.EmailCaptureScript;
                if (!Page.ClientScript.IsStartupScriptRegistered("EmailCapture"))
                {
                    Page.ClientScript.RegisterStartupScript(GetType(), "EmailCapture", emailCaptureScript.ToString());
                }
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Event fired when Register link button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnRegister_Click(object sender, EventArgs e)
        {
            this.RegisterUser();

        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// This function registers a new user 
        /// </summary>
        private void RegisterUser()
        {
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();
            log.LogActivityTimerStart();

            int profileId = ZNodeProfile.DefaultRegisteredProfileId;

            if (profileId == 0)
            {
                ErrorMessage.Text = Resources.CommonCaption.CreateFailedNoProfileAttached;
                return;
            }

            // Check if loginName already exists in DB
            ZNodeUserAccount userAccount = new ZNodeUserAccount();
            if (!userAccount.IsLoginNameAvailable(ZNodeConfigManager.SiteConfig.PortalID, UserName.Text.Trim()))
            {
                log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text, null, null, "User name already exists", null);
                ErrorMessage.Text = Resources.CommonCaption.AlreadyExist;
                return;
            }

            // Create user membership
            MembershipCreateStatus memStatus;
            MembershipUser user = Membership.CreateUser(UserName.Text.Trim(), Password.Text.Trim(), Email.Text, ddlSecretQuestions.SelectedItem.Text, Answer.Text, true, out memStatus);

            if (memStatus != MembershipCreateStatus.Success)
            {
                log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text);
                ErrorMessage.Text = Resources.CommonCaption.CreateFailed;

                return;
            }

            log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, UserName.Text);

            // Create the user account
            ZNodeUserAccount newUserAccount = new ZNodeUserAccount();
            newUserAccount.UserID = (Guid)user.ProviderUserKey;
            newUserAccount.EmailOptIn = chkOptIn.Checked;
            newUserAccount.EmailID = Email.Text;

            // Copy info to billing
            Address billingAddress = new Address();
            newUserAccount.BillingAddress = billingAddress;

            // Copy info to shipping
            Address shippingAddress = new Address();
            newUserAccount.ShippingAddress = shippingAddress;

            // Affiliate Settings
            ZNodeTracking tracker = new ZNodeTracking();
            string referralAffiliate = tracker.AffiliateId;
            int referralAccountId = 0;

            if (!string.IsNullOrEmpty(referralAffiliate))
            {
                if (int.TryParse(referralAffiliate, out referralAccountId))
                {
                    ZNode.Libraries.DataAccess.Service.AccountService accountServ = new ZNode.Libraries.DataAccess.Service.AccountService();
                    ZNode.Libraries.DataAccess.Entities.Account account = accountServ.GetByAccountID(referralAccountId);

                    // Affiliate account exists
                    if (account != null)
                    {
                        if (account.ReferralStatus == "A")
                        {
                            newUserAccount.ReferralAccountId = account.AccountID;
                        }
                    }
                }
            }

            // Register account
            newUserAccount.AddUserAccount();

            AddressService addressService = new AddressService();
            billingAddress.Name = "Default Billing";
            billingAddress.FirstName = string.Empty;
            billingAddress.MiddleName = string.Empty;
            billingAddress.LastName = string.Empty;
            billingAddress.CompanyName = string.Empty;
            billingAddress.Street = string.Empty;
            billingAddress.Street1 = string.Empty;
            billingAddress.City = string.Empty;
            billingAddress.StateCode = string.Empty;
            billingAddress.PostalCode = string.Empty;
            billingAddress.CountryCode = string.Empty;
            billingAddress.PhoneNumber = string.Empty;
            billingAddress.IsDefaultBilling = true;
            billingAddress.IsDefaultShipping = false;
            billingAddress.AccountID = newUserAccount.AccountID;
            addressService.Insert(billingAddress);
            newUserAccount.SetBillingAddress(billingAddress);

            shippingAddress.Name = "Default Shipping";
            shippingAddress.FirstName = string.Empty;
            shippingAddress.MiddleName = string.Empty;
            shippingAddress.LastName = string.Empty;
            shippingAddress.CompanyName = string.Empty;
            shippingAddress.Street = string.Empty;
            shippingAddress.Street1 = string.Empty;
            shippingAddress.City = string.Empty;
            shippingAddress.StateCode = string.Empty;
            shippingAddress.PostalCode = string.Empty;
            shippingAddress.CountryCode = string.Empty;
            shippingAddress.PhoneNumber = string.Empty;
            shippingAddress.AccountID = newUserAccount.AccountID;
            shippingAddress.IsDefaultBilling = false;
            shippingAddress.IsDefaultShipping = true;
            addressService.Insert(shippingAddress);
            newUserAccount.SetShippingAddress(shippingAddress);

            // Add Account Profile
            AccountProfile accountProfile = new AccountProfile();
            accountProfile.AccountID = newUserAccount.AccountID;
            accountProfile.ProfileID = profileId;

            AccountProfileService accountProfileServ = new AccountProfileService();
            accountProfileServ.Insert(accountProfile);

            // Login the user
            ZNodeUserAccount userAcct = new ZNodeUserAccount();
            bool loginSuccess = userAcct.Login(ZNodeConfigManager.SiteConfig.PortalID, UserName.Text.Trim(), Password.Text.Trim());

            if (loginSuccess)
            {
                // Set current user Profile.
                userAcct.ProfileID = accountProfile.ProfileID.Value;

                // Get account and set to session
                Session.Add(ZNodeSessionKeyType.UserAccount.ToString(), userAcct);

                // Log password for further debugging
                ZNodeUserAccount.LogPassword((Guid)user.ProviderUserKey, Password.Text.Trim());

                //Send mail function calling SengMail()
                this.SendMail(UserName.Text, Password.Text, Email.Text);

                // Get Profile entity for logged-in user profileId
                ZNode.Libraries.DataAccess.Service.ProfileService _ProfileService = new ZNode.Libraries.DataAccess.Service.ProfileService();
                ZNode.Libraries.DataAccess.Entities.Profile _Profile = _ProfileService.GetByProfileID(userAcct.ProfileID);

                // Hold this profile object in the session state
                HttpContext.Current.Session["ProfileCache"] = _Profile;

                // Make an entry in tracking event for Account Creation            
                if (tracker.AffiliateId.Length > 0)
                {
                    tracker.AccountID = accountProfile.AccountID;
                    tracker.LogTrackingEvent("Created an Account");
                }

                ZNode.Libraries.ECommerce.ShoppingCart.ZNodeSavedCart savedCart = new ZNode.Libraries.ECommerce.ShoppingCart.ZNodeSavedCart();
                savedCart.Merge();


                if (Request.QueryString["ReturnUrl"] != null)
                {
                    FormsAuthentication.RedirectFromLoginPage(UserName.Text.Trim(), false);
                }
                else
                {
                    FormsAuthentication.SetAuthCookie(UserName.Text.Trim(), false);
                    Response.Redirect("~/");
                }

                ZNodeCacheDependencyManager.Remove("SpecialsNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId + HttpContext.Current.Session.SessionID);

                if (System.Web.HttpContext.Current.Session["IsCartChanged"] != null)
                {
                    Response.Redirect("~/shoppingcart.aspx", true);
                }

            }
            else
            {
                ErrorMessage.Text = this.GetLocalResourceObject("InvalidAccountInfo").ToString();
            }
        }

        /// <summary>
        /// Znode version 7.2.2 
        /// This function will send acknowledge mail to new registerd user.
        /// To inform users login name and password.
        /// <summary>
        /// <param name="loginName">Registerd user login name</param> 
        /// <param name="password">Password of registered user</param> 
        /// <param name="email">Email id of user</param> 
        protected void SendMail(string loginName, string password, string email)
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = HttpContext.GetGlobalResourceObject("CommonCaption", "SubjectWelcomeNewUser") +" " + ZNodeConfigManager.SiteConfig.StoreName;
            string customerservicephone = ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber;
            string currentCulture = string.Empty;
            string templatePath = string.Empty;
            string defaultTemplatePath = string.Empty;

            // Template selection
            currentCulture = ZNodeCatalogManager.CultureInfo;

            defaultTemplatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "UserRegistrationEmailTemplate.html"));

            templatePath = (!string.IsNullOrEmpty(currentCulture))
                ? Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "UserRegistrationEmailTemplate_" + currentCulture + ".html")
                : Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "UserRegistrationEmailTemplate.html"));

            templatePath = defaultTemplatePath;            
            
            StreamReader rw = new StreamReader(templatePath);
            string messageText = rw.ReadToEnd();

            Regex rx1 = new Regex("#UserName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(messageText, loginName);

            Regex rx2 = new Regex("#Password#", RegexOptions.IgnoreCase);
            messageText = rx2.Replace(messageText, password);

            Regex rx3 = new Regex("#UserID#", RegexOptions.IgnoreCase);
            messageText = rx3.Replace(messageText, loginName);

            Regex rx4 = new Regex("#CUSTOMERSERVICEPHONE#", RegexOptions.IgnoreCase);
            messageText = rx4.Replace(messageText, customerservicephone);

            Regex rx5 = new Regex("#CUSTOMERSERVICEMAIL#", RegexOptions.IgnoreCase);
            messageText = rx5.Replace(messageText, senderEmail);

            Regex RegularExpressionLogo = new Regex("#LOGO#", RegexOptions.IgnoreCase);
            messageText = RegularExpressionLogo.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);

            try
            {
                // Send mail
                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(email, senderEmail, string.Empty, subject, messageText, true);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, loginName, Request.UserHostAddress.ToString(), null, ex.Message, null);
            }
        }

        #endregion
    }
}