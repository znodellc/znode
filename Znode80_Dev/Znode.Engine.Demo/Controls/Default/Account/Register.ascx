<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Account_Register" Codebehind="Register.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>
<asp:UpdatePanel runat="server" ID="updatePnlRegister">
	<ContentTemplate>
<asp:Panel DefaultButton="btnRegister" ID="pnlRegister" runat="server">
<div>
    <div class="Form">
        <div class="PageTitle">
            <asp:Localize ID="Localize8" meta:resourceKey="txtAccountTitle" runat="server" /></div>
        <div class="Row Clear">
            <ZNode:spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:spacer>
        </div>
        <div class="Row Clear">
            <div align="left" colspan="2" class="FailureText">
                <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">
                    <asp:Localize ID="Localize1" meta:resourceKey="txtUserName" runat="server" /></asp:Label></div><div class="LeftContent" style="width: 500px;">
                <asp:TextBox ID="UserName" runat="server" autocomplete="off" CssClass="TextField"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server"
                        ControlToValidate="UserName"
                        ErrorMessage="<%$ Resources:CommonCaption, UsernameRequired%>"
                        ToolTip="<%$ Resources:CommonCaption, UsernameRequired%>"
                        ValidationGroup="uxCreateUserWizard" CssClass="Error"
                        Display="Dynamic">
                    </asp:RequiredFieldValidator></div></div></div><div class="Row Clear">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">
                    <asp:Localize ID="Localize3" meta:resourceKey="txtPassword"  runat="server" /></asp:Label></div><div class="LeftContent" style="width: 500px;">
                <asp:TextBox ID="Password" runat="server" TextMode="Password" autocomplete="off" CssClass="TextField"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ErrorMessage="<%$ Resources:CommonCaption, PasswordRequired%>" ToolTip="<%$ Resources:CommonCaption, PasswordRequired%>" ControlToValidate="Password" ValidationGroup="uxCreateUserWizard"
                        CssClass="Error" Display="Dynamic">
                      
                    </asp:RequiredFieldValidator><asp:RegularExpressionValidator ValidationGroup="uxCreateUserWizard" ID="PasswordRegularExpression"
                        ControlToValidate="Password" runat="server" 
                        ValidationExpression="<%$ Resources:CommonCaption, PasswordStrengthExpression%>"
                        Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, ValidPasswordRequired %>"
                        ToolTip="<%$ Resources:CommonCaption, ValidPasswordRequired %>" CssClass="Error"></asp:RegularExpressionValidator></div></div></div><div class="Row Clear">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="ConfirmPasswordLabel" runat="server" AssociatedControlID="ConfirmPassword">
                    <asp:Localize ID="Localize5" meta:resourceKey="txtConfirmPassword" runat="server" /></asp:Label></div><div class="LeftContent" style="width: 500px;">
                <asp:TextBox ID="ConfirmPassword" runat="server" TextMode="Password" autocomplete="off" CssClass="TextField"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" 
                        ErrorMessage="<%$ Resources:CommonCaption, ConfirmPasswordRequired %>"
                        ToolTip="<%$ Resources:CommonCaption, ConfirmPasswordRequired %>" 
                        runat="server" ControlToValidate="ConfirmPassword"
                        ValidationGroup="uxCreateUserWizard" CssClass="Error" Display="Dynamic">
                        </asp:RequiredFieldValidator></div><div>
                    <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                        ControlToValidate="ConfirmPassword" 
                        ErrorMessage="<%$ Resources:CommonCaption, PasswordMismatch %>"
                        ToolTip="<%$ Resources:CommonCaption, PasswordMismatch %>"
                        ValidationGroup="uxCreateUserWizard" CssClass="Error" Display="Dynamic"></asp:CompareValidator></div></div></div><div class="Row Clear">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">
                    <asp:Localize ID="Localize7" Text="<%$ Resources:CommonCaption, EmailID%>" runat="server" /></asp:Label></div><div class="LeftContent">
                <asp:TextBox ID="Email" runat="server" CssClass="TextField"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email"
                        ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired %>" ValidationGroup="uxCreateUserWizard" CssClass="Error"
                        Display="Dynamic"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="Email"
                        ValidationGroup="uxCreateUserWizard" CssClass="Error" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired %>" ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired %>"
                        ValidationExpression='<%$ Resources:CommonCaption, EmailValidationExpression %>'
                        Display="Dynamic">
                    </asp:RegularExpressionValidator></div></div></div><div class="Row Clear">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="EmailLabel0" runat="server" AssociatedControlID="Email">
                    <asp:Localize ID="Localize10" meta:resourceKey="txtConfirmEmail" runat="server" /></asp:Label></div><div class="LeftContent" style="width: 500px;">
                <asp:TextBox ID="txtConfirmEmail" runat="server" CssClass="TextField"></asp:TextBox><br /><asp:RequiredFieldValidator ID="EmailRequired0" runat="server" ControlToValidate="txtConfirmEmail"
                    ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired %>" 
                    ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired %>" 
                    ValidationGroup="uxCreateUserWizard" CssClass="Error" Display="Dynamic">
                    </asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtConfirmEmail"
                    ValidationGroup="uxCreateUserWizard" CssClass="Error" 
                    ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired %>"
                    ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired %>"
                    ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression %>"
                    Display="Dynamic">
                </asp:RegularExpressionValidator><br /><asp:CompareValidator ID="cvMailAddress" runat="server" ControlToCompare="Email"
                    ControlToValidate="txtConfirmEmail" meta:resourceKey="cvMailAddress"
                    ValidationGroup="uxCreateUserWizard" CssClass="Error" Display="Dynamic"></asp:CompareValidator></div></div><div class="Row Clear">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="lblSecretQuestion" runat="server" AssociatedControlID="Email">
                    <asp:Localize ID="Localize12" meta:resourceKey="txtSecurityQ" runat="server" /></asp:Label></div><div class="LeftContent">
                <div class="TextField">
                    <asp:DropDownList ID="ddlSecretQuestions" runat="server">
                        <asp:ListItem Enabled="true" Selected="True" Text="<%$ Resources:CommonCaption, txtFavPet %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtCityBorn %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtSchool %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtMovie %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtMotherName %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtCar %>" />
                        <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtColor %>" />
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer">
                    <asp:Localize ID="Localize13" meta:resourceKey="txtSecurityA" runat="server" /></asp:Label></div><div class="LeftContent" style="width: 500px;">
                <asp:TextBox ID="Answer" runat="server" CssClass="TextField"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="AnswerRequired" 
                        ErrorMessage="<%$ Resources:CommonCaption, SecurityAnswerRequired %>" 
                        ToolTip="<%$ Resources:CommonCaption, SecurityAnswerRequired %>" 
                        runat="server" ControlToValidate="Answer"
                        ValidationGroup="uxCreateUserWizard" CssClass="Error" Display="Dynamic">
                        </asp:RequiredFieldValidator></div></div></div><div class="Row Clear">
            <div class="FieldStyle LeftContent">
                &nbsp; </div><div class="LeftContent" style="width: 60%">
                <asp:CheckBox ID="chkOptIn" runat="server" meta:resourceKey="txtSendMail" Checked="true" />
            </div>
        </div>
        <div class="Row Clear">
            <div class="FieldStyle LeftContent">
                <ZNode:spacer ID="Spacer1" SpacerHeight="60" SpacerWidth="10" runat="server"></ZNode:spacer>
            </div>
            <div class="Register">
	            <asp:Button ID="btnRegister" runat="server"
					OnClick="BtnRegister_Click" ValidationGroup="uxCreateUserWizard" 
					 meta:resourcekey="RegisterButton" CssClass="Register"></asp:Button>
            </div>
        </div>
    </div>
</div>
</asp:Panel>
		</ContentTemplate>
	</asp:UpdatePanel>
