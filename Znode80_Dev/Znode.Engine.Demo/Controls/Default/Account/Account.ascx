<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Account_Account"
    CodeBehind="Account.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/common/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="SavedCardInfo" Src="~/Controls/Default/Account/SavedCardInfo.ascx" %>
<div class="Form">
    <div class="PageTitle">
        <asp:Localize ID="txtAccountsTitle" meta:resourceKey="txtAccountTitle" runat="server" /></div>
    <br />
    <div>
        <asp:Localize ID="Localize1" meta:resourceKey="txtAccountInfo" runat="server" /></div>
    <div>
        <uc1:Spacer ID="Spacer5" SpacerHeight="10" EnableViewState="false" SpacerWidth="10"
            runat="server"></uc1:Spacer>
    </div>
    <div>
        <div class="HeaderStyle">
            <asp:Localize ID="Localize2" meta:resourceKey="txtLogin" runat="server" /></div>
        <div>
            <uc1:Spacer ID="Spacer6" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
                runat="server"></uc1:Spacer>
        </div>
        <div class="LeftContent">
            <asp:Localize ID="Localize3" meta:resourceKey="txtLoginName" runat="server" />
            <asp:LoginName ID="LoginName1" runat="server" FormatString="<b>{0}</b>" />
        </div>
        <div class="RightContent">
            <asp:LinkButton ID="ibChangePassword" runat="server" OnClick="BtnChangePassword_Click"
                CssClass="Button Large" meta:resourceKey="txtChangePassword"></asp:LinkButton></div>
    </div>
    <div class="Clear">
        <uc1:Spacer ID="Spacer4" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
            runat="server"></uc1:Spacer>
    </div>
    <div>
        <h5>
            <asp:Localize ID="Localize4" meta:resourceKey="txtContact" runat="server" /></h5>
        <div class="LeftContent">
            <b>
                <asp:Localize ID="Localize5" meta:resourceKey="txtEmail" runat="server" /></b>
        </div>
        <div class="LeftContent">
            <asp:Label ID="lblEmail" runat="server"></asp:Label>
        </div>
        <br />
        <br />
        <div class="LeftContent Clear">
            <b>
                <asp:Localize ID="Localize14" meta:resourceKey="txtEmailOptIn" runat="server" /></b>
        </div>
        <div class="LeftContent">
            <asp:Image ID="EmailOptin" runat="server" />
        </div>
        <div class="LeftContent">
            <uc1:Spacer ID="Spacer1" EnableViewState="false" SpacerHeight="10" SpacerWidth="200"
                runat="server"></uc1:Spacer>
        </div>
        <div class="LeftContent">
            <uc1:Spacer ID="Spacer10" EnableViewState="false" SpacerHeight="10" SpacerWidth="200"
                runat="server"></uc1:Spacer>
        </div>
        <div class="RightContent">
            <asp:LinkButton ID="ibEditAddress" runat="server" OnClick="BtnEditAddress_Click"
                CssClass="Button Large" meta:resourceKey="txtEditContact"></asp:LinkButton></div>
    </div>
    <div class="Clear">
        <uc1:Spacer ID="Spacer12" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
            runat="server"></uc1:Spacer>
    </div>
    <h5>
        <asp:Localize ID="Localize6" meta:resourceKey="txtAddresses" runat="server" /></h5>
    <div>
        <uc1:Spacer ID="Spacer20" EnableViewState="false" SpacerHeight="2" SpacerWidth="10"
            runat="server"></uc1:Spacer>
        <div class="RightContent">
            <asp:LinkButton ID="lbAddNewAddress" runat="server" CssClass="Button Large" meta:resourceKey="AddNewAddress"
                OnClick="LbAddNewAddress_Click"></asp:LinkButton></div>
        <uc1:Spacer ID="Spacer22" EnableViewState="false" SpacerHeight="30" SpacerWidth="10"
            runat="server"></uc1:Spacer>
    </div>
    <div>
        <asp:GridView Width="100%" ID="gvAddress" runat="server" AutoGenerateColumns="False"
            GridLines="None" CellPadding="4" OnRowCommand="GvAddress_RowCommand" CssClass="Grid"
            AllowPaging="True" OnPageIndexChanging="GvAddress_PageIndexChanging" 
            meta:resourceKey="NoGiftCardItems" OnRowDeleting="GvAddress_RowDeleting"
            >
            <Columns>
                <asp:BoundField DataField="Name" HeaderText="<%$ Resources:CommonCaption, Name %>"
                    HeaderStyle-HorizontalAlign="Left">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:BoundField>
                <asp:TemplateField HeaderText="<%$ Resources:CommonCaption, DefaultShipping%>" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="Img11" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsDefaultShipping").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:CommonCaption, DefaultBilling %>" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="Img12" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsDefaultBilling").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:CommonCaption, Address %>" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblAddress" runat="server" Text='<%# GetAddress(DataBinder.Eval(Container.DataItem, "AddressID").ToString())%>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <asp:LinkButton ID="lbEdit" runat="server" CommandName="Edit" CssClass="Button" Text="<%$ Resources:CommonCaption, Edit %>"
                            CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AddressID")%>'></asp:LinkButton>
                        <asp:LinkButton ID="lbDelete" runat="server" CommandName="Delete" CssClass="Button"
                            Text="<%$ Resources:CommonCaption, Delete %>" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"AddressID")%>'></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <uc1:Spacer ID="Spacer223" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
            runat="server"></uc1:Spacer>
        <div>
            <asp:Label ID="lblError" CssClass="Error" runat="server" /></div>
    </div>
    <!-- Affiliate section -->
    <asp:Panel ID="pnlAffiliate" runat="server" Visible="false">
        <div>
            <div class="HeaderStyle">
                <asp:Localize ID="Localize7" meta:resourceKey="txtAffiliateInfo" runat="server" /></div>
            <div class="LeftContent">
                <b>
                    <asp:Localize ID="Localize9" meta:resourceKey="txtRefCommision" runat="server" /></b>
                :
            </div>
            <div class="LeftContent">
                <uc1:Spacer ID="Spacer15" EnableViewState="false" SpacerHeight="10" SpacerWidth="30"
                    runat="server"></uc1:Spacer>
            </div>
            <div>
                <%=ReferralCommision%></div>
            <div class="Clear">
                <uc1:Spacer ID="Spacer3" EnableViewState="false" SpacerHeight="5" SpacerWidth="10"
                    runat="server"></uc1:Spacer>
            </div>
            <div class="LeftContent">
                <b>
                    <asp:Localize ID="Localize10" meta:resourceKey="txtRefTaxID" runat="server" /></b>
                :
            </div>
            <div>
                <%=TaxId%></div>
            <div class="Clear">
                <uc1:Spacer ID="Spacer13" EnableViewState="false" SpacerHeight="5" SpacerWidth="10"
                    runat="server"></uc1:Spacer>
            </div>
            <div class="LeftContent">
                <b>
                    <asp:Localize ID="Localize11" meta:resourceKey="txtRefCommisionType" runat="server" /></b>
                :
            </div>
            <div class="LeftContent">
                <uc1:Spacer ID="Spacer17" EnableViewState="false" SpacerHeight="10" SpacerWidth="30"
                    runat="server"></uc1:Spacer>
            </div>
            <div>
                <%=ReferralCommisionType%></div>
            <div class="Clear">
                <uc1:Spacer ID="Spacer14" EnableViewState="false" SpacerHeight="5" SpacerWidth="10"
                    runat="server"></uc1:Spacer>
            </div>
            <div class="LeftContent">
                <strong>
                    <asp:Localize ID="Localize12" meta:resourceKey="txtRefTrack" runat="server" /></strong>
                :
            </div>
            <div class="LeftContent">
                <uc1:Spacer ID="Spacer18" EnableViewState="false" SpacerHeight="10" SpacerWidth="30"
                    runat="server"></uc1:Spacer>
            </div>
            <asp:HyperLink ID="hlAffiliateLink" Text="NA" runat="server" Target="_blank"></asp:HyperLink>
            <div class="Clear">
                <uc1:Spacer ID="Spacer11" EnableViewState="false" SpacerHeight="5" SpacerWidth="10"
                    runat="server"></uc1:Spacer>
            </div>
        </div>
    </asp:Panel>
    <h5>
        <asp:Localize ID="Localize8" meta:resourceKey="txtOrderHistory" runat="server" /></h5>
    <div>
        <uc1:Spacer ID="Spacer2" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
            runat="server"></uc1:Spacer>
    </div>
    <div>
        <asp:GridView Width="100%" ID="uxGrid" runat="server" AutoGenerateColumns="False"
            EmptyDataText="<%$ Resources:CommonCaption, NoOrder  %>" GridLines="None" CellPadding="4"
            OnRowCommand="UxGrid_RowCommand" CssClass="Grid" AllowPaging="True" OnPageIndexChanging="UxGrid_PageIndexChanging">
            <Columns>
                <asp:BoundField DataField="OrderID" meta:resourceKey="OrderNo" HeaderStyle-HorizontalAlign="Left">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="OrderDate" meta:resourceKey="OrderDate" DataFormatString="{0:MM/dd/yyyy hh:mm tt}" HeaderStyle-HorizontalAlign="Left">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:BoundField>
                <asp:BoundField DataField="OrderStatus" meta:resourceKey="OrderStatus" HeaderStyle-HorizontalAlign="Left">
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:BoundField>
                <asp:TemplateField meta:resourceKey="CustomerName" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "BillingFirstName").ToString() + " " + DataBinder.Eval(Container.DataItem, "BillingLastName").ToString()%>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField meta:resourceKey="TotalCost" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div>
                            <%# DataBinder.Eval(Container.DataItem, "Total", "{0:c}")%></div>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton runat="server" CommandName="View" CssClass="Button" meta:resourceKey="ViewItem"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
    </div>
    <div>
        <uc1:Spacer ID="Spacer8" EnableViewState="false" SpacerHeight="20" SpacerWidth="10"
            runat="server"></uc1:Spacer>
    </div>
    <div class="HeaderStyle">
        <asp:Localize ID="Localize13" meta:resourceKey="txtWishList" runat="server" /></div>
	<div>
		<asp:Label ID="lblMaxQntyError" runat="server" Visible="False"></asp:Label>

	</div>
    <div>
        <uc1:Spacer ID="Spacer7" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
            runat="server"></uc1:Spacer>
    </div>
	

    <div class="WishList">
        <asp:GridView CssClass="Grid" ID="uxWishList" runat="server" AutoGenerateColumns="False"
            EmptyDataText="<%$ Resources:CommonCaption, NoWishList  %>" GridLines="None"
            CellPadding="4" EnableViewState="true" AllowPaging="True" OnPageIndexChanging="UxWishList_PageIndexChanging"
            PageSize="10">
            <Columns> 
                <asp:TemplateField meta:resourceKey="Remove" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:LinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProductID")%>'
                            ID="lnkRemove" runat="server" meta:resourceKey="RemoveItem" CommandName="remove"
                            OnClick="LnkRemove_Click"></asp:LinkButton>
                    </ItemTemplate>
                    <ItemStyle Wrap="True" />
                </asp:TemplateField>
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a id="A1" href='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()%>'
                            runat="server">
                            <img id="Img1" alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' border='0'
                                src='<%# DataBinder.Eval(Container.DataItem, "ThumbnailImageFilePath")%>' runat="server" /></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField meta:resourceKey="Item" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div>
                            <a id="A2" href='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()%>'
                                runat="server">
                                <%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a></div>
                        <div class="Description">
                            Item#
                            <%# DataBinder.Eval(Container.DataItem, "ProductNum").ToString()%><br>
                            <%# DataBinder.Eval(Container.DataItem, "ShortDescription").ToString()%>
							<asp:HiddenField ID="hdnProductSKU" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "SKU")%>' Visible="False "/>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField meta:resourceKey="Price" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div class="CallForPrice">
                            <%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing")) %></div>
                        <asp:Label ID="Price" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>'
                            Visible='<%#!(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && AccountProfile.ShowPrice%>'></asp:Label></div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProductID")%>'
                            ID="btnbuy" runat="server" OnClick="Buy_Click" Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && AccountProfile.ShowAddToCart %>'
                            CssClass="Button Large" meta:resourceKey="AddToCart"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="Footer" />
            <RowStyle CssClass="Row" />
            <HeaderStyle CssClass="Header" />
            <AlternatingRowStyle CssClass="AlternatingRow" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer9" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
                runat="server"></uc1:Spacer>
        </div>
    </div>
    <div>
        <uc1:Spacer ID="Spacer19" EnableViewState="false" SpacerHeight="20" SpacerWidth="10"
            runat="server"></uc1:Spacer>
    </div>
    <h5>
        <asp:Label ID="lblGiftCardHistoryTitle" meta:resourceKey="txtGiftCardTitle" runat="server" /></h5>
    <div class="GiftCardHistory">
        <asp:GridView CssClass="Grid" ID="gvGiftCardHistory" runat="server" AutoGenerateColumns="False"
            meta:resourceKey="NoGiftCardItems" GridLines="None" CellPadding="4" AllowPaging="True"
            OnRowCommand="GvGiftCardHistory_RowCommand" OnPageIndexChanging="GvGiftCardHistory_PageIndexChanging">
            <Columns>
                <asp:BoundField ItemStyle-CssClass="GiftCardHistoryID" DataField="GiftCardHistoryID"
                    HeaderStyle-HorizontalAlign="Left" HeaderText="ID" ReadOnly="True" />
                <asp:TemplateField HeaderText="Gift Card Number" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div class="CardNumber">
                            <%# GetGiftCardNumber(DataBinder.Eval(Container.DataItem, "GiftCardID"))%></div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField ItemStyle-CssClass="TransactionDate" ItemStyle-HorizontalAlign="Center"
                    DataField="TransactionDate" HeaderText="Transaction Date" DataFormatString="{0:d}"
                    ReadOnly="True" />
                <asp:BoundField ItemStyle-CssClass="TransactionAmount" DataField="TransactionAmount"
                    DataFormatString="{0:0.00}" HeaderText="Transaction Amount" ItemStyle-HorizontalAlign="Right"
                    ReadOnly="True" />
                <asp:BoundField DataField="OrderID" HeaderText="Order #" ItemStyle-HorizontalAlign="Center"
                    ReadOnly="True" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="View" CssClass="Button"
                            meta:resourceKey="ViewItem"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="Footer" />
            <RowStyle CssClass="Row" />
            <HeaderStyle CssClass="Header" />
            <AlternatingRowStyle CssClass="AlternatingRow" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer16" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
                runat="server"></uc1:Spacer>
        </div>
    </div>
    <div>
        <uc1:Spacer ID="Spacer21" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
    </div>
    <h5>
        <asp:Localize ID="Localize141" meta:resourceKey="txtSavedCardInfo" runat="server" /></h5>
    <div class="WishList">
        <ZNode:SavedCardInfo runat="server" ID="SavedCardInfo"></ZNode:SavedCardInfo>
    </div>
</div>
