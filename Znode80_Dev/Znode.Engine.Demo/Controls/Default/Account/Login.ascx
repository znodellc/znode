<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Account_Login" CodeBehind="Login.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<%--<%@ Register Src="~/Controls/Default/common/HomeQuickSearch.ascx" TagName="QuickSearch" TagPrefix="ZNode" %>--%>

<asp:UpdatePanel runat="server" ID="updateCheckoutPanel">
	<ContentTemplate>
<div class="Loginpage">
    <div class="QuickSearchfield">
        <%-- <ZNode:QuickSearch ID="uxQuickSearch" runat="server"></ZNode:QuickSearch>--%>
    </div>
</div>
<div class="Form">
    <div class="PageTitle">
        <asp:Localize ID="Localize8" meta:resourceKey="txtSignIn" runat="server" /></div>
   <div class="Row Clear">
        <div class="LeftContent" style="width: 45%">
            <asp:Login ID="uxLogin" runat="server" MembershipProvider="ZNodeMembershipProvider"
                PasswordRecoveryText="Forgot Password?" PasswordRecoveryUrl="~/ForgotPassword.aspx"
                OnAuthenticate="UxLogin_Authenticate" DisplayRememberMe="False" FailureText="<%$ Resources:CommonCaption, LoginFailed%>"
                Width="100%" OnLoggedIn="UxLogin_LoggedIn">
                <LayoutTemplate>
                    <asp:Panel ID="CheckoutPanel" DefaultButton="ibLogin" runat="server">
                        <div class="Title">
                            <asp:Localize ID="Localize8" meta:resourceKey="txtUser" runat="server" />
                        </div>
                        <div class="Form">
                            <div class="Row Clear">
                                <ZNode:Spacer ID="Spacer8" EnableViewState="false" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div class="Row Clear"></div>
                            <div class="Error">
                                <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                            </div>
                            <div class="Row Clear">
                                <div class="FieldStyle LeftContent">
                                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">
                                        <asp:Localize ID="Localize1" meta:resourceKey="txtUserName" runat="server" /></asp:Label></div><div>
                                    <asp:TextBox ID="UserName" runat="server" autocomplete="off" CssClass="TextField"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                            ValidationGroup="uxLogin" meta:resourceKey="UserNameRequired" Display="Dynamic"
                                            CssClass="Error"></asp:RequiredFieldValidator></div></div></div><div class="Row Clear">
                                <div class="FieldStyle LeftContent">
                                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">
                                        <asp:Localize ID="Localize4" meta:resourceKey="txtPassword" runat="server" /></asp:Label></div><div>
                                    <asp:TextBox ID="Password" runat="server" autocomplete="off" CssClass="TextField" TextMode="Password"></asp:TextBox><div>
                                        <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                            CssClass="Error" Display="Dynamic" meta:resourceKey="PasswordRequired" ValidationGroup="uxLogin"></asp:RequiredFieldValidator></div></div></div><div class="Row Clear">
                                <div class="FieldStyle LeftContent">
                                </div>
                                <div class="ForgetLink">
                                    <asp:CheckBox ID="RememberMe" runat="server" Text="Remember me next time." meta:resourceKey="txtRemember" />
                                    <div>
                                        <asp:LinkButton ID="ForgotPassword" runat="Server" meta:resourceKey="txtForgot" CssClass="TextField"
                                            OnClick="ForgotPassword_Click"> SSS</asp:LinkButton></div></div></div><div class="Row Clear">
                                <div class="FieldStyle LeftContent">
                                    &nbsp; </div><ZNode:Spacer ID="Spacer1" EnableViewState="false" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                                <div class="NewButton">
	                                
                                    <asp:LinkButton ID="ibLogin" runat="server" ValidationGroup="uxLogin" CommandName="Login" Font-Overline="False" Font-Underline="False">
                                        <asp:Localize ID="Localize2" meta:resourceKey="txtLogin" runat="server" />
                                        &raquo;
                                    </asp:LinkButton>
                                </div>
                                 </div>
							<div class="Row Clear">
                                <ZNode:Spacer ID="Spacer2" EnableViewState="false" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                        </div>
                        </div>
                    </asp:Panel>
                </LayoutTemplate>
            </asp:Login>
        </div>
        <div class="LeftContent" style="width: 10%" align="center">
            <asp:Image ID="ImgSeparator" EnableViewState="false" runat="server" Height="350" />
        </div>
        <div class="LeftContent" style="width: 45%">
            <div class="Title">
                <asp:Localize ID="Localize5" meta:resourceKey="txtNewUser" runat="server" /></div>
            <div style="margin-bottom: 5px;">&nbsp;</div><div class="Form">
                <div class="CreateAccount">
                    <asp:LinkButton ID="uxCreate" EnableViewState="false" runat="server"
                        CssClass="Button" meta:resourcekey="txtCreateAcc" OnClick="uxCreate_Click"></asp:LinkButton></div><div class="RightTextalign">
                    <ZNode:Spacer ID="Spacer8" EnableViewState="false" SpacerHeight="2" SpacerWidth="3" runat="server"></ZNode:Spacer>
                    <asp:Localize ID="Localize6" meta:resourceKey="txtAccountInfo" runat="server" />
                </div>
                <asp:Panel Visible="false" ID="uxExpressCheckout" runat="server">
                    <div class="AddToCartButton">
                        <asp:LinkButton ID="uxCheckout" EnableViewState="false" runat="server"
                            CssClass="Button" meta:resourcekey="txtCheckout" OnClick="uxCheckout_Click"></asp:LinkButton></div><div class="RightTextalign">
                        <ZNode:Spacer ID="Spacer3" EnableViewState="false" SpacerHeight="2" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        <asp:Localize ID="Localize9" meta:resourceKey="txtCheck" runat="server" />
                    </div>
                </asp:Panel>
            </div>
        </div>
    </div>
</div>
		</ContentTemplate>
</asp:UpdatePanel>
<script language="javascript" type="text/javascript">

    // To handle Enter key event for Firefox.
    var b = document.getElementById('<%= uxLogin.FindControl("ibLogin").ClientID %>');
    if (b && typeof (b.click) == 'undefined') {
        b.click = function () {
            var result = true;
            if (b.onclick) result = b.onclick();
            if (typeof (result) == 'undefined' || result) {
                eval(b.getAttribute('href'));
            }
        }
    }

</script>
