﻿using System;
using System.Web.Security;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.UserAccount;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Edit Contact user control class.
    /// </summary>
    public partial class Controls_Default_Account_EditContact : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeUserAccount _userAccount;
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get the user account from session
            this._userAccount = ZNodeUserAccount.CurrentAccount();

            if (!this.IsPostBack)
            {
                this.BindContact();
            }
        }
        #endregion

        #region Event Methods
        protected void LbCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Account.aspx");
        }

		protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            this.SubmitPage();
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Bind the user contact information
        /// </summary>
        private void BindContact()
        {
            if (this._userAccount == null)
            {
                Response.Redirect("~/Default.aspx");
            }
            else
            {
                AccountService accountService = new AccountService();
                Account account = accountService.GetByAccountID(this._userAccount.AccountID);
                if (account != null)
                {
                    txtEmail.Text = account.Email;
                    chkEmailOptIn.Checked = account.EmailOptIn;
                }
            }
        }

        /// <summary>
        /// Submit the contact information
        /// </summary>
        private void SubmitPage()
        {
            AccountService accountService = new AccountService();
            Account account = accountService.GetByAccountID(this._userAccount.AccountID);
            if (account != null)
            {
                account.Email = txtEmail.Text;
                account.EmailOptIn = chkEmailOptIn.Checked;
                bool isSuccess = accountService.Update(account);

                // Update online account email address
                if (this._userAccount.UserID.HasValue)
                {
                    MembershipUser user = Membership.GetUser(this._userAccount.UserID.Value);

                    if (user != null)
                    {
                        user.Email = this._userAccount.EmailID;
                        Membership.UpdateUser(user);
                    }
                }

                // If successfully updated then redirect to account home page.
                if (isSuccess)
                {
                    Response.Redirect("Account.aspx");
                }
            }
        }
        #endregion
    }
}