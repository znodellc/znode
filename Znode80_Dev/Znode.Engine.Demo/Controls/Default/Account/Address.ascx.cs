using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using System.Linq;
using Address = ZNode.Libraries.DataAccess.Entities.Address;
using ZNodeShoppingCart = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart;
using ZNodeShoppingCartItem = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem;

namespace Znode.Engine.Demo
{
	/// <summary>
	/// Represents the Address user control class.
	/// </summary>
	public partial class Controls_Default_Account_Address : System.Web.UI.UserControl
	{
		#region Private Variables
		private ZNodeUserAccount _userAccount;
		private int accountId = 0;
		#endregion

		#region Public Properties

		public int itemId
		{
			get
			{
				int value = 0;

				if (ddlAddressName.Visible == true)
				{
					int.TryParse(ddlAddressName.SelectedValue, out value);
				}

				return value;
			}
		}
		#endregion

		#region Protected Events
		/// <summary>
		/// This event raised on clicking the button btnUpdate
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void BtnUpdate_Click(object sender, EventArgs e)
		{
			// Validate billing state code
			if ((lstBillingCountryCode.SelectedValue == "US") && (!string.IsNullOrEmpty(txtBillingState.Text.Trim())))
			{
				if (txtBillingState.Text.Trim().Length > 2 || txtBillingState.Text.Trim().Length < 2)
				{
					lblError.Text = Resources.CommonCaption.ValidateStateCode;
					return;
				}
			}
			if (chkUseAsDefaultShippingAddress.Checked)
			{
				if (!ValidateCountry())
				{
					//lblError.Text = "Country has No Shipping Facility";
					lblError.Text = this.GetLocalResourceObject("AllowShippingToCountry").ToString();
					return;

				}
			}

			this._userAccount = (ZNodeUserAccount)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.UserAccount);

			Address address = new Address();
			AddressService addressService = new AddressService();
			TList<Address> addressList = this._userAccount.Addresses;
			if (this.itemId > 0)
			{
				address = addressList.Find(x => x.AddressID == itemId);
			}

			if (!this.HasDefaultAddress(this._userAccount.AccountID, true))
			{
				lblError.Text = this.GetLocalResourceObject("DefaultBillingAddressRequired").ToString();
				return;
			}

			if (!this.HasDefaultAddress(this._userAccount.AccountID, false))
			{
				lblError.Text = this.GetLocalResourceObject("DefaultShippingAddressRequired").ToString();
				return;
			}
			Session["AddressList"] = null;
			address.AddressID = this.itemId;
			address.AccountID = this._userAccount.AccountID;
			address.Name = txtAddressName.Text;
			address.FirstName = txtBillingFirstName.Text;
			address.LastName = txtBillingLastName.Text;
			address.CompanyName = txtBillingCompanyName.Text;
			address.Street = txtBillingStreet1.Text;
			address.Street1 = txtBillingStreet2.Text;
			address.City = txtBillingCity.Text;
			address.StateCode = txtBillingState.Text.ToUpper();
			address.PostalCode = txtBillingPostalCode.Text;
			address.CountryCode = lstBillingCountryCode.SelectedValue;
			address.PhoneNumber = txtBillingPhoneNumber.Text;
			if (chkUseAsDefaultBillingAddress.Checked)
			{
				// Clear all previous default billing address flag.
				foreach (Address currentItem in addressList)
				{
					currentItem.IsDefaultBilling = false;
				}
			}

			if (chkUseAsDefaultShippingAddress.Checked)
			{
				// Clear all previous default billing address flag.
				foreach (Address currentItem in addressList)
				{
					currentItem.IsDefaultShipping = false;
				}
			}

			address.IsDefaultBilling = chkUseAsDefaultBillingAddress.Checked;
			address.IsDefaultShipping = chkUseAsDefaultShippingAddress.Checked;
         
            int newAddressID = this.itemId;
			if (this.itemId == 0 || itemId == -1)
			{
				addressList.Add(address);
			}

			addressService.Save(addressList);

            if (this.itemId == 0 || itemId == -1)
            {
                newAddressID = address.AddressID;
            }

            if (Request.RawUrl.ToLower().Contains("isbilling"))
            {
                if (Request.QueryString["isBilling"].ToString() == "1")
                {
                    this._userAccount.SetBillingAddress(address);
                }
            }
			if (chkUseAsDefaultShippingAddress.Checked && this.itemId > 0)
				HttpContext.Current.Session["DefaultShippingAddressId"] = this.itemId;

			var shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

			var addressCartId = new Guid();

		    if (shoppingCart != null)
		    {
		        if (shoppingCart.PortalCarts.Any())
		        {
		            addressCartId = shoppingCart.PortalCarts[0].AddressCarts[0].AddressCartID;
		        }

		        if (Request.QueryString["returnURL"] == "checkout.aspx")
		            if (shoppingCart.IsMultipleShipToAddress)
		            {
		                var cart =
		                    shoppingCart.PortalCarts.SelectMany(x => x.AddressCarts)
		                                .FirstOrDefault(x => x.AddressCartID == addressCartId);

		                var items = shoppingCart.ShoppingCartItems.Cast<ZNodeShoppingCartItem>()
		                                        .Where(
		                                            x =>
		                                            cart != null &&
		                                            cart.ShoppingCartItems.Cast<ZNodeShoppingCartItem>()
		                                                .Any(y => x.GUID == y.GUID))
		                                        .SelectMany(
		                                            x =>
		                                            x.OrderShipments.Where(
		                                                z =>
		                                                z.AddressID.ToString(CultureInfo.InvariantCulture) ==
		                                                Request.QueryString["itemid"]));

		                var orderShipments = items as ZNodeOrderShipment[] ?? items.ToArray();

		                if (orderShipments.Any())
		                {
		                    orderShipments.ToList().ForEach(x => x.AddressID = newAddressID);

		                    cart.AddressID = newAddressID;
		                }
		            }
		    }

		    this.RediretPage();
		}

		/// <summary>
		/// This event raised on clicking the button btnCancel
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			this.RediretPage();
		}

		/// <summary>
		/// This event raised on clicking the button btnUpdate
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void lnkAddress_Click(object sender, EventArgs e)
		{
			ddlAddressName.SelectedIndex = -1;
			ddlAddressName.Visible = false;
			lnkAddress.Visible = false;
			lblAddress.Visible = false;

			txtAddressName.Text = string.Empty;
			txtBillingFirstName.Text = string.Empty;
			txtBillingLastName.Text = string.Empty;
			txtBillingCompanyName.Text = string.Empty;
			txtBillingStreet1.Text = string.Empty;
			txtBillingStreet2.Text = string.Empty;
			txtBillingCity.Text = string.Empty;
			txtBillingState.Text = string.Empty;
			txtBillingPostalCode.Text = string.Empty;
			lstBillingCountryCode.SelectedIndex = 0;
			txtBillingPhoneNumber.Text = string.Empty;
			chkUseAsDefaultBillingAddress.Checked = false;
			chkUseAsDefaultShippingAddress.Checked = false;
		}

		/// <summary>
		/// Address Dropdown Selected index changed event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void DdlAddressName_SelectedIndexChanged(object sender, EventArgs e)
		{
			DropDownList ddl = sender as DropDownList;
			int addressId = Convert.ToInt32(ddl.SelectedValue);
			if (addressId == 0)
			{
				Response.Redirect("Address.aspx?returnurl=EditAddress.aspx");
			}
			else
			{
				this.BindAddress(accountId);
			}
		}

		/// <summary>
		/// Page Load Event
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void Page_Load(object sender, EventArgs e)
		{
			this._userAccount = ZNodeUserAccount.CurrentAccount();

			if (this._userAccount == null)
			{
				Response.Redirect("~/account.aspx");
			}

			if (Request.QueryString["ItemId"] == "-1" || string.IsNullOrEmpty(Request.QueryString["ItemId"]))
			{
				ddlAddressName.Visible = false;
				lblAddress.Visible = false;
				lnkAddress.Visible = false;
			}
			else if (Request.QueryString["ItemId"] == "-2")
			{
				ddlAddressName.Visible = true;
				lblAddress.Visible = true;
				lnkAddress.Visible = true;
			}

			if (!Page.IsPostBack)
			{
					this.BindCountry();
					accountId = this._userAccount.AccountID;

					if (Request.QueryString["ItemId"] != "-1" && !string.IsNullOrEmpty(Request.QueryString["ItemId"]))
					{
						BindAddressDropDown();
						this.BindAddress(accountId);
					}
			}

			if (this.itemId > 0)
			{
				lblAddressTitle.Text = this.GetLocalResourceObject("EditAddress.Text").ToString();
			}
			else
			{
				lblAddressTitle.Text = this.GetLocalResourceObject("AddAddress.Text").ToString();
			}

			if (this.Page != null)
			{
				ZNodeResourceManager resourceManager = new ZNodeResourceManager();
				this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtUpdateAddress.Text");
			}
		}
		#endregion

		#region Private Methods
		/// <summary>
		/// Bind the address dropdown
		/// </summary>
		private void BindAddressDropDown()
		{
			AddressService addressService = new AddressService();

			TList<ZNode.Libraries.DataAccess.Entities.Address> addressList = addressService.GetByAccountID(accountId);
			foreach (ZNode.Libraries.DataAccess.Entities.Address currentItem in addressList)
			{
				if (currentItem.IsDefaultBilling)
				{
					currentItem.Name = currentItem.Name + " (default)";
				}
			}

			ddlAddressName.DataTextField = "Name";
			ddlAddressName.DataValueField = "AddressID";
			ddlAddressName.DataSource = addressList;
			ddlAddressName.DataBind();

			ddlAddressName.SelectedIndex = ddlAddressName.Items.IndexOf(ddlAddressName.Items.FindByValue(Request.QueryString["ItemID"]));

		}

		/// <summary>
		/// Bind the address.
		/// </summary>
		private void BindAddress(int accountId)
		{
			AddressService addressService = new AddressService();

			Address address = addressService.GetByAddressID(this.itemId);
			if (address != null)
			{
				// Set field values
				txtAddressName.Text = address.Name;
				txtBillingFirstName.Text = address.FirstName;
				txtBillingLastName.Text = address.LastName;
				txtBillingCompanyName.Text = address.CompanyName;
				txtBillingStreet1.Text = address.Street;
				txtBillingStreet2.Text = address.Street1;
				txtBillingCity.Text = address.City;
				txtBillingState.Text = address.StateCode;
				txtBillingPostalCode.Text = address.PostalCode;

				ListItem blistItem = lstBillingCountryCode.Items.FindByValue(address.CountryCode);
				if (blistItem != null)
				{
					lstBillingCountryCode.SelectedIndex = lstBillingCountryCode.Items.IndexOf(blistItem);
				}

				txtBillingPhoneNumber.Text = address.PhoneNumber;
				chkUseAsDefaultBillingAddress.Checked = address.IsDefaultBilling;
				chkUseAsDefaultShippingAddress.Checked = address.IsDefaultShipping;
			}
		}

		/// <summary>
		/// Binds country drop-down list
		/// </summary>
		private void BindCountry()
		{
			// PortalCountry
			ZNode.Libraries.DataAccess.Custom.PortalCountryHelper portalCountryHelper = new ZNode.Libraries.DataAccess.Custom.PortalCountryHelper();
			DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

			DataView billingView = new DataView(countryDs.Tables[0]);
			billingView.RowFilter = "BillingActive = 1";

			DataView shippingView = new DataView(countryDs.Tables[0]);
			shippingView.RowFilter = "ShippingActive = 1";

			lstBillingCountryCode.DataSource = billingView;
			lstBillingCountryCode.DataTextField = "Name";
			lstBillingCountryCode.DataValueField = "Code";
			lstBillingCountryCode.DataBind();
			lstBillingCountryCode.SelectedValue = "US";
		}

		public bool ValidateCountry()
		{
			bool flag = false;
			ZNode.Libraries.DataAccess.Custom.PortalCountryHelper portalCountryHelper =
				new ZNode.Libraries.DataAccess.Custom.PortalCountryHelper();
			DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

			DataTable BillingView = countryDs.Tables[0];
			DataRow[] drBillingView = BillingView.Select("CountryCode='" + this.lstBillingCountryCode.Text + "'");
			foreach (DataRow drView in drBillingView)
			{
				if (drView["ShippingActive"].ToString() != "True")
				{
					flag = false;
				}
				else
				{
					flag =  true;
				}
			}
			return flag;
		}

		/// <summary>
		/// Check whether the account has default billing or shipping address.
		/// </summary>
		/// <param name="accountId">Account Id</param>
		/// <param name="isBillingAddress">True to check billing address, false to check shipping address.</param>
		/// <returns>Returns true if account has default address or false.</returns>
		private bool HasDefaultAddress(int accountId, bool isBillingAddress)
		{
			bool hasDefault = true;

			AccountAdmin accountAdmin = new AccountAdmin();
			Address address = null;
			if (isBillingAddress)
			{
				address = accountAdmin.GetDefaultBillingAddress(accountId);
				hasDefault = this.ValidateDefaultAddress(address, chkUseAsDefaultBillingAddress);
			}
			else
			{
				address = accountAdmin.GetDefaultShippingAddress(accountId);
				hasDefault = this.ValidateDefaultAddress(address, chkUseAsDefaultShippingAddress);
			}

			return hasDefault;
		}

		/// <summary>
		/// Validate account had default address.
		/// </summary>
		/// <param name="address">Address object</param>
		/// <param name="cb">Checkbox checked or unchecked</param>
		/// <returns>Returns true if account has default address.</returns>
		private bool ValidateDefaultAddress(Address address, CheckBox cb)
		{
			if (address == null && !cb.Checked)
			{
				return false;
			}
			else
			{
				if (!cb.Checked && address != null && address.AddressID == this.itemId)
				{
					return false;
				}
			}

			return true;
		}

		/// <summary>
		/// Redirects to the pages.
		/// </summary>
		private void RediretPage()
		{
			if (Request.QueryString["returnurl"] != null)
			{
				Response.Redirect(Request.QueryString["returnurl"].ToString());
			}
			else
			{
				Response.Redirect("~/account.aspx");
			}
		}
		#endregion
	}
}