<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Account_Address" Codebehind="Address.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>
<asp:UpdatePanel runat="server" ID="upAddress">
    <ContentTemplate>
           <div class="Form Search">
            <h1>
                <asp:Label ID="lblAddressTitle" runat="server"></asp:Label></h1>
            <div>
                <uc1:spacer ID="Spacer1" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
                    runat="server"></uc1:spacer>
                    <asp:Label ID="lblError" CssClass="Error" runat="server"></asp:Label><br /><br />
            </div>
            <div class="Row Clear">
                <div class="LeftContent" style="width: 100%" >
	                <asp:Panel runat="server" ID="pnlAddAddress" DefaultButton="btnUpdate">
                    <!-- valign="top">-->
                    <div class="Form" >                        
                        <div class="Row Clear">
	                         <div class="FieldStyle LeftContent" style="text-align: left">
		                        <asp:Label ID="lblAddress" runat="server" Text="Address"></asp:Label>
	                        </div>
                            <div class="LeftContent" style="width: 40%;">
	                             <asp:DropDownList ID="ddlAddressName" Width="180px" runat="server" AutoPostBack="True"
                                    OnSelectedIndexChanged="DdlAddressName_SelectedIndexChanged">
                                </asp:DropDownList>&nbsp;&nbsp;&nbsp;</asp:DropDownList>&nbsp;&nbsp;&nbsp; 
								<asp:LinkButton runat="server" Text="Add Address" ID="lnkAddress" OnClick="lnkAddress_Click"></asp:LinkButton>
							</div>
							</div>
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent" style="text-align: left">
                                <asp:Localize ID="Localize13" runat="server" Text="<%$ Resources:CommonCaption, AddressName %>" />
                                <br />
                                <small>Enter a name for this address entry so that you can easily select it later.</small>
                            </div>
                            <div class="LeftContent">
                                <asp:TextBox ID="txtAddressName" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox>
                                <div>
                                    <asp:RequiredFieldValidator ID="rfvAdressName" ErrorMessage="<%$ Resources:CommonCaption, AddressNameRequired %>"
                                        ControlToValidate="txtAddressName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent" style="text-align: left">
                                <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:CommonCaption, FirstName %>" /></div>
                            <div class="LeftContent">
                                <asp:TextBox ID="txtBillingFirstName" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="req1" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired %>"
                                        ControlToValidate="txtBillingFirstName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent" style="text-align: left">
                                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:CommonCaption, LastName %>" /></div>
                            <div class="LeftContent">
                                <asp:TextBox ID="txtBillingLastName" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" ErrorMessage="<%$ Resources:CommonCaption, LastNameRequired %>"
                                        ControlToValidate="txtBillingLastName" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent" style="text-align: left">
                                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:CommonCaption, CompanyName %>" /></div>
                            <div class="LeftContent">
                                <asp:TextBox ID="txtBillingCompanyName" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox></div>
                        </div>
                       
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent" style="text-align: left">
                                <asp:Localize ID="Localize7" meta:resourceKey="txtBStreet1" runat="server" /></div>
                            <div class="LeftContent">
                                <asp:TextBox ID="txtBillingStreet1" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" ErrorMessage="<%$ Resources:CommonCaption, Street1Required %>"
                                        ControlToValidate="txtBillingStreet1" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent" style="text-align: left">
                                <asp:Localize ID="Localize9" meta:resourceKey="txtBStreet2" runat="server" /></div>
                            <div class="LeftContent">
                                <asp:TextBox ID="txtBillingStreet2" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox></div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent" style="text-align: left">
                                <asp:Localize ID="txLiveChat" runat="server" Text="<%$ Resources:CommonCaption, City %>" /></div>
                            <div class="LeftContent">
                                <asp:TextBox ID="txtBillingCity" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" ErrorMessage="<%$ Resources:CommonCaption, CityRequired %>"
                                        ControlToValidate="txtBillingCity" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent" style="text-align: left">
                                <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:CommonCaption, State %>" /></div>
                            <div class="LeftContent">
                                <asp:TextBox ID="txtBillingState" runat="server" Width="180" Columns="10" MaxLength="2"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" ErrorMessage="<%$ Resources:CommonCaption, StateRequired %>"
                                        ControlToValidate="txtBillingState" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent" style="text-align: left">
                                <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:CommonCaption, PostalCode %>" /></div>
                            <div class="LeftContent">
                                <asp:TextBox ID="txtBillingPostalCode" runat="server" Width="180" Columns="30" MaxLength="10"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator12" ErrorMessage="<%$ Resources:CommonCaption, PostalCodeRequired %>"
                                        ControlToValidate="txtBillingPostalCode" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent" style="text-align: left">
                                <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:CommonCaption, Country %>" /></div>
                            <div class="LeftContent">
                                <asp:DropDownList ID="lstBillingCountryCode" Width="180" runat="server">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator13" ErrorMessage="<%$ Resources:CommonCaption, CountryCodeRequired %>"
                                    ControlToValidate="lstBillingCountryCode" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        
                         <div class="Row Clear">
                            <div class="FieldStyle LeftContent" style="text-align: left">
                                <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:CommonCaption, PhoneNumber %>" /></div>
                            <div class="LeftContent">
                                <asp:TextBox ID="txtBillingPhoneNumber" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" ErrorMessage="<%$ Resources:CommonCaption, PhoneNoRequired %>"
                                        ControlToValidate="txtBillingPhoneNumber" runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator></div>
                            </div>
                        </div>
                        
                        <div class="Row Clear">
                            <div>
                                <uc1:spacer ID="Spacer5" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
                                    runat="server"></uc1:spacer>
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent">
                            </div>
                            <div class="LeftContent">
                                <asp:CheckBox ID="chkUseAsDefaultBillingAddress" Width="300px" TextAlign="Right"
                                    runat="server" meta:resourceKey="txtUseAsDefaultBillingAddress" Checked="false" />
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent">
                            </div>
                            <div class="LeftContent">
                                <asp:CheckBox ID="chkUseAsDefaultShippingAddress" Width="300px" TextAlign="Right"
                                    runat="server" meta:resourceKey="txtUseAsDefaultShippingAddress" Checked="true" />
                            </div>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div>
                            <uc1:spacer ID="Spacer4" EnableViewState="false" SpacerHeight="15" SpacerWidth="10"
                                runat="server"></uc1:spacer>
                        </div>
                    </div>
                    <div class="Row Clear">
                        <div class="FieldStyle LeftContent">
                            </div>
	                    <asp:Button ID="btnUpdate" runat="server" Text="<%$ Resources:CommonCaption, Submit %>"
	                                OnClick="BtnUpdate_Click" CssClass="button"/>

                        <asp:LinkButton ID="btnCancel" runat="server" OnClick="BtnCancel_Click" CssClass="Button"
                            CausesValidation="false" Text="<%$ Resources:CommonCaption, Cancel %>" />
                    </div>
						</asp:Panel>
                </div>
            </div>
            <div class="LeftContent" style="width: 10%">
                &nbsp;</div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
