<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Account_Password" Codebehind="Password.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>
<asp:UpdatePanel runat="server" ID="updatePnlChangePassword">
<ContentTemplate>
<div class="Form">
    <div class="PageTitle">
        <asp:Localize ID="Localize2" meta:resourceKey="txtTitle" runat="server" /></div>
    <div>
        <uc1:spacer ID="Spacer1" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:spacer>
    </div>
	
    <asp:ChangePassword ID="ChangePassword1" runat="server" CancelDestinationPageUrl="~/account.aspx"
        ContinueDestinationPageUrl="~/account.aspx" DisplayUserName="False" ChangePasswordTitleText=""
        OnChangePasswordError="ChangePassword1_ChangePasswordError" OnChangingPassword="ChangePassword1_ChangingPassword">
        <SuccessTemplate>
            <asp:Panel ID="pnlConfirm" runat="server">
                <!--Succesfull Message -->
                <asp:Label ID="lblMessage" CssClass="" meta:resourceKey="txtPasswordChanged" runat="server"></asp:Label>
                <uc1:spacer ID="Spacer3" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:spacer>
                <uc1:spacer ID="Spacer2" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:spacer>
                <asp:LinkButton ID="ContinuePushButton" runat="server" CssClass="Button" CausesValidation="False"
                    CommandName="Continue" meta:resourceKey="txtContinue" />
            </asp:Panel>
        </SuccessTemplate>
        <ChangePasswordTemplate>
			<asp:Panel runat="server" DefaultButton="btnChangePassword" ID="pnlChangePassword">
            <div class="FieldStyle LeftContent">
                <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword">
                    <asp:Localize ID="Localize2" meta:resourceKey="txtCurrent" runat="server" /></asp:Label></div><div class="LeftContent" style="width: 500px;">
                <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                    ErrorMessage="<%$ Resources:CommonCaption, PasswordRequired %>" ToolTip="<%$ Resources:CommonCaption, PasswordRequired %>"
                    ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic"
                     meta:resourceKey="txtReqPassword"></asp:RequiredFieldValidator></div><div class="Clear">
                <uc1:spacer ID="Spacer4" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:spacer>
            </div>
            <div class="FieldStyle LeftContent">
                <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword">
                    <asp:Localize ID="Localize9" meta:resourceKey="txtNewPassword" runat="server" /></asp:Label></div><div>
                <asp:TextBox ID="NewPassword" runat="server" TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" 
                    ControlToValidate="NewPassword"
                    ErrorMessage="<%$ Resources:CommonCaption, NewPasswordRequired %>"
                    ToolTip="<%$ Resources:CommonCaption, NewPasswordRequired %>"
                    ValidationGroup="ChangePassword1" 
                    CssClass="Error" 
                    Display="Dynamic"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="NewPassword"
                    Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, ValidPasswordRequired %>"
                    SetFocusOnError="True" ToolTip="<%$ Resources:CommonCaption, ValidPasswordRequired %>"
                    ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,})" ValidationGroup="ChangePassword1"
                    CssClass="Error" meta:resourceKey="txtReq"  ></asp:RegularExpressionValidator></div><div class="Clear">
                <uc1:spacer ID="Spacer8" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:spacer>
            </div>
            <div class="FieldStyle LeftContent">
                <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword">
                    <asp:Localize ID="Localize4" meta:resourceKey="txtConfirm" runat="server" /></asp:Label></div><div>
                <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                    ErrorMessage="<%$ Resources:CommonCaption, ConfirmPasswordNewRequired %>" ToolTip="<%$ Resources:CommonCaption, ConfirmPasswordNewRequired %>"
                    ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic"
                     meta:resourceKey="txtConfirmPassword" ></asp:RequiredFieldValidator><asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                    ControlToValidate="ConfirmNewPassword" Display="Dynamic" 
                    ErrorMessage="<%$ Resources:CommonCaption, ConfirmPasswordNotMatch %>"
                    ValidationGroup="ChangePassword1" CssClass="Error"></asp:CompareValidator></div><div class="Clear">
                <uc1:spacer ID="Spacer5" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:spacer>
            </div>
            <div class="FieldStyle LeftContent">
                <asp:Label ID="lblSecretQuestion" runat="server" AssociatedControlID="ddlSecretQuestions">
                    <asp:Localize ID="Localize6" meta:resourceKey="txtSecurityQ" runat="server" /></asp:Label></div><div class="LeftContent">
                <asp:DropDownList ID="ddlSecretQuestions" runat="server">
                    <asp:ListItem Enabled="true" Selected="True" Text="<%$ Resources:CommonCaption, txtFavPet %>" />
                    <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtCityBorn %>" />
                    <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtSchool %>" />
                    <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtMovie %>" />
                    <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtMotherName %>" />
                    <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtCar %>" />
                    <asp:ListItem Enabled="true" Text="<%$ Resources:CommonCaption, txtColor %>" />
                </asp:DropDownList>
            </div>
            <div class="Clear">
                <uc1:spacer ID="Spacer6" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:spacer>
            </div>
            <div class="FieldStyle LeftContent">
                <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer">
                    <asp:Localize ID="Localize7" meta:resourceKey="txtSecurityA" runat="server" /></asp:Label></div><div>
                <asp:TextBox ID="Answer" Width="140px" runat="server"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Answer"
                    ErrorMessage="<%$ Resources:CommonCaption, SecurityAnswerRequired %>" ToolTip="<%$ Resources:CommonCaption, SecurityAnswerRequired %>"
                    ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic"
                    meta:resourceKey="txtSecurityAReq"  ></asp:RequiredFieldValidator></div><div class="Clear">
                <uc1:spacer ID="Spacer7" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:spacer>
            </div>
            <div class="FailureText">
                <asp:Literal ID="PasswordFailureText" runat="server" EnableViewState="False"></asp:Literal></div><div class="Clear">
                <uc1:spacer ID="Spacer9" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:spacer>
            </div>
            <div>
				<asp:Button ID="btnChangePassword" runat="server" meta:resourceKey="txtChangePassword"
				  CommandName="ChangePassword" ValidationGroup="ChangePassword1" CssClass="button large"	/>
                <asp:LinkButton ID="ibCancelButton" runat="server" meta:resourceKey="txtCancel" CausesValidation="False"
                    CssClass="Button" CommandName="Cancel"/>
            </div>
            <div class="Clear">
                <uc1:spacer ID="Spacer10" EnableViewState="false" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:spacer>
            </div>
				</asp:Panel>
        </ChangePasswordTemplate>
    </asp:ChangePassword>
		
</div>
<div>
    <uc1:spacer ID="Spacer4" EnableViewState="false" SpacerHeight="60" SpacerWidth="10" runat="server"></uc1:spacer>
</div>
	</ContentTemplate>
	</asp:UpdatePanel>
