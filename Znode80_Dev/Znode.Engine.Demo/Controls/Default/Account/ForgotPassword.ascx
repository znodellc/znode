<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Account_ForgotPassword" Codebehind="ForgotPassword.ascx.cs" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="ZNode"%>

<asp:UpdatePanel runat="server" ID="updatePnlUserName">
<ContentTemplate>
<div class="Form">
    <div class="PageTitle"><asp:Localize ID="Localize21" meta:resourceKey="txtPasswordTitle" runat="server"/></div>
    <asp:Wizard ID="PasswordRecoveryWizard" DisplaySideBar="False" runat="server" 
        OnActiveStepChanged="PasswordRecoveryWizard_ActiveStepChanged" 
        ActiveStepIndex="0">
        <WizardSteps>                
            <asp:TemplatedWizardStep runat="server" StepType="Start" AllowReturn="true">                
                <ContentTemplate>                            
                        <p><asp:Localize ID="Localize3" meta:resourceKey="txtEnterDetail" runat="server" /></p>                            
                        
                        <asp:Panel id="UserNamePanel" DefaultButton="ibSubmit" runat="server">     
                         <div class="Row Clear">                                                                                              
                            <div class="FieldStyle LeftContent">
                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName"><asp:Localize ID="Localize1" meta:resourceKey="txtUserName" runat="server" /></asp:Label>
                            </div>
                            <div class="LeftContent" style="width:500px;">
                                <asp:TextBox ID="UserName" autocomplete="off"  runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator 
                                ID="UserNameRequired" 
                                runat="server" 
                                ControlToValidate="UserName" 
                                ErrorMessage="<%$ Resources:CommonCaption, UsernameRequired %>" 
                                ToolTip="<%$ Resources:CommonCaption, UsernameRequired %>" 
                                ValidationGroup="PasswordRecovery1" 
                                CssClass="Error" 
                                Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                            </div>
                            
                            <div class="Row Clear">
                            <div class="FieldStyle LeftContent">
                                <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email"><asp:Localize ID="Localize2" meta:resourceKey="txtEmail" runat="server" /></asp:Label>
                            </div>
                            <div>
                                <asp:TextBox ID="Email" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Email"
                                ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired %>" ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired %>" CssClass="Error"  ValidationGroup="PasswordRecovery1" Display="Dynamic"></asp:RequiredFieldValidator>
                                 <asp:RegularExpressionValidator ID="regemailID" runat="server" ControlToValidate="Email"
                                    ErrorMessage="<%$ Resources:CommonCaption, EmailValidErrorMessage %>" ValidationGroup="PasswordRecovery1" Display="Dynamic" ValidationExpression='<%$ Resources:CommonCaption, EmailValidationExpression%>'
                                    CssClass="Error"></asp:RegularExpressionValidator></div>
                                </div>
                                
                            </div>
                            
                            <div class="Row Clear">
                                <div class="FailureText LeftContent Error"  style="width:450px; padding-bottom:5px;">
                                   <asp:Literal ID="FailureText" runat="server" EnableViewState="false"></asp:Literal>
                                </div>
                            </div>
                            <div class="Row Clear">
                                <div class="LeftContent"> &nbsp;</div>
                                <div class="LeftContent"> 
									<asp:Button ID="ibSubmit" runat="server" CommandName="MoveNext" ValidationGroup="PasswordRecovery1" CssClass="button" meta:resourceKey="txtSubmit"></asp:Button>
                                </div>
                            </div>
                            <div class="Clear"><ZNode:spacer id="Spacer18" SpacerHeight="10" SpacerWidth="10" runat="server"></ZNode:spacer></div>
                     </asp:Panel>
                </ContentTemplate>
                <CustomNavigationTemplate></CustomNavigationTemplate>                                       
            </asp:TemplatedWizardStep>                
            
            <asp:TemplatedWizardStep runat="server" StepType="Finish" AllowReturn="False">
                <ContentTemplate>
                    <div><ZNode:spacer ID="Spacer3" SpacerWidth="5" SpacerHeight="10" runat="server" /></div>
                    <p><asp:Localize ID="Localize11" meta:resourceKey="txtPlsAnswer" runat="server" /></p>
                    <div><ZNode:spacer ID="Spacer5" SpacerWidth="5" SpacerHeight="10" runat="server" /></div>
                    
                    <asp:Panel id="QuestionPanel" DefaultButton="FinishButton" runat="server">           
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent"><asp:Localize ID="Localize4" meta:resourceKey="txtSecretQuestion" runat="server" /></div>
                            <div class="ValueField"><asp:Literal ID="Question" runat="server"></asp:Literal></div>
                        </div>
                        <div class="Row Clear">
                            <div class="FieldStyle LeftContent"><asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer"><asp:Localize ID="Localize5" meta:resourceKey="txtSecretAnswer" runat="server" /></asp:Label></div>
                            <div class="ValueField">
                                <asp:TextBox ID="Answer" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" ControlToValidate="Answer" ErrorMessage="<%$ Resources:CommonCaption, AnswerRequired %>" ToolTip="<%$ Resources:CommonCaption, AnswerRequired %>" ValidationGroup="PasswordRecovery1" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="Row Clear">                                
                            <div class="FailureText LeftContent Error" style="width:400px;padding:5px;">                                 
                                 <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>                                 
                            </div>
                        </div>
                        <div class="Row Clear">
                            <div class="LeftContent">&nbsp;</div>
                            <div class="LeftContent">
								<asp:Button ID="FinishButton" runat="server"  CommandName="MoveComplete" ValidationGroup="PasswordRecovery1" CssClass="button"  meta:resourceKey="txtSubmit"></asp:Button>
                            </div>              
                        </div>
                    </asp:Panel>
                </ContentTemplate>
                <CustomNavigationTemplate></CustomNavigationTemplate>
            </asp:TemplatedWizardStep>                
            
            <asp:WizardStep ID="step3" runat="server" StepType="Complete">
                <ZNode:spacer SpacerWidth="15" SpacerHeight="15" ID="Spacer1" runat="server" />
                <div class="SuccessText"><asp:Literal Text="<%$ Resources:CommonCaption, PasswordSent %>" ID="Message" runat="server" EnableViewState="False"></asp:Literal></div>
                <ZNode:spacer SpacerWidth="15" SpacerHeight="10" ID="Spacer2" runat="server" />
                <div class="BackLink"><a id="BackLink" runat="server" href="~/account.aspx"><asp:Localize ID="Localize5" meta:resourceKey="txtBack" runat="server" /></a> </div>
                <div><ZNode:spacer ID="Spacer" SpacerWidth="5" SpacerHeight="25" runat="server" /> </div>
            </asp:WizardStep>      
        </WizardSteps>
                   
        <StepNextButtonStyle />
        <StartNextButtonStyle  />            
    </asp:Wizard>
    <!-- End -->
    <div><ZNode:spacer id="Spacer19" SpacerHeight="40" SpacerWidth="10" runat="server"></ZNode:spacer></div>
</div>
</ContentTemplate>
</asp:UpdatePanel>
