﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SavedCardInfo.ascx.cs" Inherits="Znode.Engine.Demo.Controls_Default_Account_SavedCardInfo" %>
<div>
    <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
        CellPadding="4" ForeColor="#333333" GridLines="None"  PageSize="15" OnRowCommand="UxGrid_RowCommand"  OnRowDeleting="UxGrid_RowDeleting"
        EmptyDataText="No cards are saved.">
        <Columns>
            <asp:BoundField HeaderText="Credit Card Number" DataField="CreditCardDescription" ItemStyle-Width="35%"
                ReadOnly="true" HeaderStyle-HorizontalAlign="Left" DataFormatString="XXXX-XXXX-XXXX-{0}" />
            <asp:BoundField DataField="CardExp" HeaderText="Exp. Date" DataFormatString="{0:MM/yy}"
                ReadOnly="true" ItemStyle-Width="50%" HeaderStyle-HorizontalAlign="Left"/>
          
            <asp:TemplateField>
                <ItemTemplate>
                      <asp:LinkButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("PaymentTokenID") %>'
                        Width="80px" Style="padding-top: 10px;"  CssClass="Button" CommandName="Delete" Text="Remove " />    
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle CssClass="Footer" />
        <RowStyle CssClass="Row"  />
        <HeaderStyle CssClass="Header"  />
        <AlternatingRowStyle CssClass="AlternatingRow" />
    </asp:GridView>

</div>
