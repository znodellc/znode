using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Forgot Password user control class.
    /// </summary>
    public partial class Controls_Default_Account_ForgotPassword : System.Web.UI.UserControl
    {
        #region Helper Methods
        /// <summary>
        /// Represents the Verifyuser method 
        /// </summary>
        protected void VerifyUser()
        {
            var loginName = (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("UserName") as TextBox).Text.Trim();
            var emailID = (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("Email") as TextBox).Text.Trim();

            var user = Membership.GetUser(loginName);

            if (user == null)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetFailed, loginName, Request.UserHostAddress.ToString(), null, "Invalid user name", null);

                (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("FailureText") as Literal).Text = GetLocalResourceObject("InvalidUsername").ToString();
                PasswordRecoveryWizard.ActiveStepIndex = 0;
                return;
            }

            var userAcct = new ZNodeUserAccount();
            //var acc = userAcct.GetByUserID(new Guid(user.ProviderUserKey.ToString()));

            if (!user.IsApproved)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetFailed, loginName, Request.UserHostAddress.ToString(), null, "Account is Inactive", null);
                (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("FailureText") as Literal).Text = GetLocalResourceObject("InvalidUsername").ToString() + "<br>" + "Contact site administrator for further assistance.";
                PasswordRecoveryWizard.MoveTo(PasswordRecoveryWizard.WizardSteps[0]);
                return;
            }

            // Verify user email id
            if (user.Email != emailID)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetFailed, loginName, Request.UserHostAddress.ToString(), null, "Invalid email", null);

                (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("FailureText") as Literal).Text = GetLocalResourceObject("InvalidUsername").ToString();
                PasswordRecoveryWizard.MoveTo(PasswordRecoveryWizard.WizardSteps[0]);
                return;
            }

            // If Security Question and Answer are not set we give them an error message. 
            if (string.IsNullOrEmpty(user.PasswordQuestion))
            {
                (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("FailureText") as Literal).Text =
                    "You are unable to reset your password because the Security Question and password have not been set yet, please contact site administrator for further assistance.";
                PasswordRecoveryWizard.MoveTo(PasswordRecoveryWizard.WizardSteps[0]);
            }
            else
            {
                (PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("Question") as Literal).Text = user.PasswordQuestion;
                PasswordRecoveryWizard.MoveTo(PasswordRecoveryWizard.WizardSteps[1]);
            }
        }

        /// <summary>
        /// Send mail to user with 
        /// </summary>
        protected void SendMail()
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = ZNodeConfigManager.SiteConfig.StoreName + " " + HttpContext.GetGlobalResourceObject("CommonCaption", "SubjectForgetPassword");
            string loginName = (PasswordRecoveryWizard.WizardSteps[0].Controls[0].FindControl("UserName") as TextBox).Text.Trim();
            string passwordAnswer = (PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("Answer") as TextBox).Text.Trim();
            string CurrentCulture = string.Empty;
            string templatePath = string.Empty;
            string defaultTemplatePath = string.Empty;

            MembershipUser user = Membership.GetUser(loginName);
            string LoginPassword = string.Empty;

            try
            {
                // Resets a user's password to a new one,automatically generated password
                LoginPassword = user.ResetPassword(passwordAnswer);

                // Log password for further debugging
                // ZNodeUserAccount.LogPassword((Guid)user.ProviderUserKey, LoginPassword);
                AccountHelper helperAccess = new AccountHelper();
                helperAccess.DeletePasswordLogByUserId(((Guid)user.ProviderUserKey).ToString());
            }
            catch (MembershipPasswordException ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetFailed, loginName, Request.UserHostAddress.ToString(), null, "Invalid secret question/answer", ex.Message);

                (PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("FailureText") as Literal).Text = GetLocalResourceObject("InvalidSecurityQuestion").ToString();
                PasswordRecoveryWizard.ActiveStepIndex = 1;
                return;
            }

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetSuccess, loginName, Request.UserHostAddress.ToString(), null, "Invalid secret question/answer", null);
            CurrentCulture = ZNodeCatalogManager.CultureInfo;
            defaultTemplatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ResetPassword_en.htm"));
            if (CurrentCulture != string.Empty)
            {
                templatePath = Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ResetPassword_" + CurrentCulture + ".htm");
            }
            else
            {
                templatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ResetPassword_en.htm"));
            }

            // Check the language specific file existence.
            if (!File.Exists(templatePath))
            {
                // Check the default template file existence.
                if (!File.Exists(defaultTemplatePath))
                {
                    (PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("FailureText") as Literal).Text = Resources.CommonCaption.GeneralError;
                    PasswordRecoveryWizard.ActiveStepIndex = 1;
                    return;
                }
                else
                {
                    // If language specific template not available then load default template.
                    templatePath = defaultTemplatePath;
                }
            }

            StreamReader rw = new StreamReader(templatePath);
            string messageText = rw.ReadToEnd();

            Regex rx1 = new Regex("#UserName#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(messageText, loginName);

            Regex rx2 = new Regex("#Password#", RegexOptions.IgnoreCase);
            messageText = rx2.Replace(messageText, LoginPassword);

            Regex rx3 = new Regex("#Url#", RegexOptions.IgnoreCase);
            messageText = rx3.Replace(messageText, "http://" + ZNodeConfigManager.DomainConfig.DomainName + "/login.aspx");

            try
            {
                // Send mail
                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(user.Email, senderEmail, String.Empty, subject, messageText, true);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordResetSuccess, loginName, Request.UserHostAddress.ToString(), null, ex.Message, null);
                (PasswordRecoveryWizard.WizardSteps[1].Controls[0].FindControl("FailureText") as Literal).Text = Resources.CommonCaption.GeneralError;
                PasswordRecoveryWizard.ActiveStepIndex = 1;
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.Page != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtPasswordTitle.Text");
            }
        }

        /// <summary>
        /// This event raised When PasswordRecovery Wizard step is chenged
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void PasswordRecoveryWizard_ActiveStepChanged(object sender, EventArgs e)
        {
            // Check active wizard step index
            if (PasswordRecoveryWizard.ActiveStepIndex == 1)
            {
                this.VerifyUser();
            }
            else if (PasswordRecoveryWizard.ActiveStepIndex == 2)
            {
                this.SendMail();
            }
        }

        #endregion
    }
}