﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditContact.ascx.cs" Inherits="Znode.Engine.Demo.Controls_Default_Account_EditContact" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/common/spacer.ascx" %>
<asp:UpdatePanel runat="server" ID="updatePnlEditContact">
	<ContentTemplate>
<div class="Form">
    <h1 id="Title">
        <asp:Localize ID="ContactInformation" meta:resourceKey="ContactInformation" runat="server" /></h1>
    <br /><br />
	<asp:Panel runat="server" ID="pnlEditContact" DefaultButton="btnSubmit">
    <div class="Row Clear">
        <div class="FieldStyle LeftContent">
            <asp:Localize ID="Localize1" meta:resourceKey="EmailTitle" runat="server" /></div>
        <div class="LeftContent">
            <asp:TextBox ID="txtEmail" Width="450px" runat="server"></asp:TextBox><br />
            <div style="width:450px;">
            <asp:RequiredFieldValidator 
                ID="RequiredFieldValidator1" 
                runat="server" 
                Width="450px"
                ValidationGroup="EditContact"
                ControlToValidate="txtEmail"
                CssClass="Error"
                Display="Dynamic"
                ErrorMessage='<%$ Resources:CommonCaption, EmailAddressRequired%>'>
                </asp:RequiredFieldValidator>            
            <asp:RegularExpressionValidator 
                ID="revEmail"                
                runat="server"
                Width="10px" 
                ControlToValidate="txtEmail"                
                ErrorMessage='<%$ Resources:CommonCaption, EmailValidErrorMessage %>'
                Display="Dynamic" 
                ValidationExpression='<%$ Resources:CommonCaption, EmailValidationExpression%>'
                ValidationGroup="EditContact"
                CssClass="Error">
           </asp:RegularExpressionValidator>
           </div>
        </div>
    </div>
    <br />
    
    <div class="Row Clear">
        <div class="FieldStyle LeftContent">
        </div>
        <div class="LeftContent" style="padding-top:20px;">
            <asp:CheckBox ID="chkEmailOptIn" meta:resourceKey="EmailOptIn" Checked="true" Width="450px"
                runat="server" />
        </div>
    </div>    
    <div class="Row Clear" style="padding-top:20px;">
        <div class="FieldStyle LeftContent">
        </div>
        <div class="LeftContent" style="width:400px;">
	        <asp:Button ID="btnSubmit" runat="server"  CssClass="button"  Style="width: 120px" ValidationGroup="EditContact" CausesValidation="true" Text="<%$ Resources:CommonCaption, Submit %>"
				onclick="BtnSubmit_Click"></asp:Button>
            <asp:LinkButton ID="lbCancel" runat="server" CssClass="Button" 
                Style="margin-left:10px; width: 120px;" 
                Text="<%$ Resources:CommonCaption, Cancel %>" onclick="LbCancel_Click"></asp:LinkButton>
        </div>
    </div>
		</asp:Panel>
    <div class="Clear">
        <uc1:Spacer ID="Spacer4" EnableViewState="false" SpacerHeight="10" SpacerWidth="10"
            runat="server"></uc1:Spacer>
    </div>
</div>
		</ContentTemplate>
	</asp:UpdatePanel>
