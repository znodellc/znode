using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Password user control class.
    /// </summary>
    public partial class Controls_Default_Account_Password : System.Web.UI.UserControl
    {
        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Retrieve the information from the database
                MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);

                if (user != null)
                {
                    // Pre-select security question
                    (ChangePassword1.ChangePasswordTemplateContainer.FindControl("ddlSecretQuestions") as DropDownList).Text = user.PasswordQuestion;
                }
            }

            if (this.Page != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtTitle.Text");
            }
        }

        /// <summary>
        /// Event fired when user changes the password
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChangePassword1_ChangePasswordError(object sender, EventArgs e)
        {
            (ChangePassword1.ChangePasswordTemplateContainer.FindControl("PasswordFailureText") as Literal).Text = "Current Password Incorrect";
        }

        /// <summary>
        /// Event fired when user changes the password
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChangePassword1_ChangingPassword(object sender, LoginCancelEventArgs e)
        {
            e.Cancel = true;
            this.ChangePassword();
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Represents the Change Password method
        /// </summary>
        protected void ChangePassword()
        {
            MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            Literal ltrl = ChangePassword1.ChangePasswordTemplateContainer.FindControl("PasswordFailureText") as Literal;
            string passwordQuestion = (ChangePassword1.ChangePasswordTemplateContainer.FindControl("ddlSecretQuestions") as DropDownList).SelectedItem.Text;
            string passwordAnswer = (ChangePassword1.ChangePasswordTemplateContainer.FindControl("Answer") as TextBox).Text.Trim();
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();

            if (user != null)
            {
                log.LogActivityTimerStart();

                // Verify if the new password specified by the user is in the list of the last 4 passwords used.
                bool ret = ZNodeUserAccount.VerifyNewPassword((Guid)user.ProviderUserKey, ChangePassword1.NewPassword);

                if (!ret)
                {
                    log.LogActivityTimerEnd(1107, HttpContext.Current.User.Identity.Name, Request.UserHostAddress.ToString(), null, "Previous password used", null);
                    ltrl.Text = this.GetLocalResourceObject("ltrl.Text").ToString();
                    return;
                }

                // Updates the password for this user
                if (user.ChangePassword(ChangePassword1.CurrentPassword, ChangePassword1.NewPassword))
                {
                    log.LogActivityTimerEnd(1106, user.UserName, Request.UserHostAddress.ToString(), null, null, null);

                    if (passwordAnswer.Length > 0)
                    {
                        // Log password for further debugging
                        ZNodeUserAccount.LogPassword((Guid)user.ProviderUserKey, ChangePassword1.NewPassword);

                        // Updates the password question and answer for this User
                        user.ChangePasswordQuestionAndAnswer(ChangePassword1.NewPassword, passwordQuestion, passwordAnswer);
                    }

                    // Redirect to account page
                    Response.Redirect("~/account.aspx");
                }
                else
                {
                    log.LogActivityTimerEnd(1107, user.UserName, Request.UserHostAddress.ToString(), null, "Current password incorrect", null);

                    // Display Error message
                    ltrl.Text = this.GetLocalResourceObject("InvalidCurrentPassword").ToString();
                }
            }
        }
        #endregion
    }
}