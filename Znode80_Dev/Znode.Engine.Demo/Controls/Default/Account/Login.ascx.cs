using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using Account = ZNode.Libraries.DataAccess.Entities.Account;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.DataAccess.Custom;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Login user control class.
    /// </summary>
    public partial class Controls_Default_Account_Login : System.Web.UI.UserControl
    {

        #region Variables
        //Znode Version 7.2.2
        //WebSite Section, Error message for account gets locked - Start 
        //Gets the membership provider details for the key "ZNodeMembershipProvider".

        #region Private Variables
        private MembershipProvider _membershipProvider = Membership.Providers["ZNodeMembershipProvider"];
        #endregion

        #region Public Variables
        public MembershipProvider MembershipProvider
        {
            get { return _membershipProvider; }
            set { _membershipProvider = value; }
        }
        #endregion

        //WebSite Section, Error message for account gets locked - End
        #endregion

        #region Public Properties
        /// <summary>
        /// Sets a value indicating whether "Remember Me Option" checkbox visible property
        /// </summary>
        public bool ShowRememberMeOption
        {
            set
            {
                uxLogin.DisplayRememberMe = value;
                (uxLogin.FindControl("RememberMe") as CheckBox).Visible = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Event fired when user authenticates
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        public void UxLogin_Authenticate(object sender, AuthenticateEventArgs e)
        {
            bool Authenticated = false;
            Authenticated = this.CustomAuthenticationMethod(uxLogin.UserName.Trim(), uxLogin.Password.Trim());
            e.Authenticated = Authenticated;
        }

        #endregion

        #region Protected Methods and Events
        protected void ForgotPassword_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/ForgotPassword.aspx");
        }

        protected void UxLogin_LoggedIn(object sender, EventArgs e)
        {
            ZNodeCacheDependencyManager.Remove("SpecialsNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId + HttpContext.Current.Session.SessionID);
			
            string returnURL = Request.QueryString["ReturnUrl"];

			ZNode.Libraries.ECommerce.ShoppingCart.ZNodeSavedCart savedCart = new ZNode.Libraries.ECommerce.ShoppingCart.ZNodeSavedCart();
			savedCart.Merge();

			ZNodeShoppingCart shoppingcart = ZNodeShoppingCart.CurrentShoppingCart();

			if (shoppingcart != null)
				shoppingcart.ResetShippingAddress(0);

            if (returnURL.Contains("zpid"))
            {
                Response.Redirect(returnURL);
                return;
            }

            if (System.Web.HttpContext.Current.Session["IsCartChanged"] != null)
            {
                Response.Redirect("~/shoppingcart.aspx", true);
            }
            if (System.Web.HttpContext.Current.Session["RedirectToCart"] != null)
            {
                Response.Redirect("~/shoppingcart.aspx", true);
            }

			Session["IsAddressValidated"] = ZNodeUserAccount.CurrentAccount().CheckoutAddress();
        }

        protected void uxCreate_Click(object sender, EventArgs e)
        {
            if (Request.RawUrl.ToLower().Contains("checkout"))
            {
                Response.Redirect("~/RegisterUser.aspx?ReturnUrl=EditAddress.aspx");
            }
			else if (Request.QueryString["zpid"] != null)
			{
				Response.Redirect("~/RegisterUser.aspx?ReturnUrl=Account.aspx?zpid=" + Request.QueryString["zpid"]);
			}
            else
            {
                Response.Redirect("~/RegisterUser.aspx?ReturnUrl=Account.aspx");
            }
        }

        protected void uxCheckout_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/EditAddress.aspx");
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // To load the image based on locale
            ImgSeparator.ImageUrl = ZNodeCatalogManager.GetImagePathByLocale("quickwatch_separator.jpg");

            // Set page title
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append(ZNodeConfigManager.SiteConfig.StoreName);
            sb.Append(" : ");
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();
            sb.Append(resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "Title"));
            Page.Title = sb.ToString();

            // Check if SSL is required
            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {
                    string inURL = Request.Url.ToString();
                    string outURL = inURL.ToLower().Replace("http://", "https://");

                    Response.Redirect(outURL);
                }
            }

			if (Request.RawUrl.ToLower().Contains("checkout"))
            {
                uxExpressCheckout.Visible = true;
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// This method overrides the default login implementation
        /// </summary>
        /// <param name="UserName">Name of the User</param>
        /// <param name="Password">User Password</param>
        /// <returns>Returns boolean value</returns>
        private bool CustomAuthenticationMethod(string UserName, string Password)
        {
            // Insert code that implements a site-specific custom 
            // Authentication method here.
            ZNodeUserAccount userAcct = new ZNodeUserAccount();
            bool loginSuccess = userAcct.Login(ZNodeConfigManager.SiteConfig.PortalID, UserName, Password);

            if (loginSuccess)
            {
                string retValue = string.Empty;
                bool isLoginEnabled = false;
                int profileId = userAcct.GetAccountPortalProfileID(userAcct.AccountID, ZNodeConfigManager.SiteConfig.PortalID);

                if (profileId == 0)
                {
                    return false;
                }

                bool status = ZNodeUserAccount.CheckLastPasswordChangeDate((Guid)userAcct.UserID, out retValue);

                AccountService acctService = new AccountService();
                Account account = acctService.GetByAccountID(userAcct.AccountID);

                // Get account and set to session
                Session.Add("AccountObject", account);

                if (!status)
                {
                    // Set Error Code to session Object
                    Session.Add("ErrorCode", retValue);

                    Response.Redirect("~/ResetPassword.aspx");
                }


                if (profileId != 0)
                {
                    ProfileService profileService = new ProfileService();
                    Profile ProfileEntity = profileService.GetByProfileID(profileId);

                    if (ProfileEntity != null)
                    {
                        // Hold this profile object in the session state
                        HttpContext.Current.Session["ProfileCache"] = ProfileEntity;

                        // Set CurrentUserProfile ProfileID
                        userAcct.ProfileID = ProfileEntity.ProfileID;

                        isLoginEnabled = true;
                    }
                }

                // Get account and set to session
                Session.Add(ZNodeSessionKeyType.UserAccount.ToString(), userAcct);

                // retrieve roles associated with this user
                bool isAdminRole = Roles.IsUserInRole(UserName, "ADMIN");

                if (!isLoginEnabled && !isAdminRole)
                {
                    loginSuccess = false;
                }


            }
            else
            {
                //Znode Version 7.2.2
                //WebSite Section, Error message for account gets locked - Start 
                //Set the Error messages in case of Account gets locked. And Also display warning messages before the final last two attempts.
                //Gets the user details based on username.
                MembershipUser membershipUser = Membership.GetUser(UserName.Trim());
               
                if (membershipUser != null)
                {
                    //Check whether user accoung is locked or not.
                    if (membershipUser.IsLockedOut)
                    {

                        uxLogin.FailureText = this.GetGlobalResourceObject("CommonCaption", "ErrorAccountLockedOut").ToString();
                    }
                    else
                    {
                        //Gets current failed password atttemt count for setting the message.
                        AccountHelper accountHelper = new AccountHelper();
                        int inValidAttemtCount = accountHelper.GetUserFailedPasswordAttemptCount((Guid)membershipUser.ProviderUserKey);
                        if (inValidAttemtCount > 0)
                        {
                            //Gets Maximum failed password atttemt count from web.config
                            int maxInvalidPasswordAttemptCount = _membershipProvider.MaxInvalidPasswordAttempts;

                            //Set warning error at (MaxFailureAttemptCount - 2) & (MaxFailureAttemptCount - 1) attempt.
                            uxLogin.FailureText = ((maxInvalidPasswordAttemptCount - inValidAttemtCount) == 2)
                                ? this.GetGlobalResourceObject("CommonCaption", "ErrorAccountLockedOutAfterTwoAttempt").ToString()
                                : ((maxInvalidPasswordAttemptCount - inValidAttemtCount) == 1)
                                    ? this.GetGlobalResourceObject("CommonCaption", "ErrorAccountLockedOutAfterOneAttempt").ToString()
                                    : this.GetGlobalResourceObject("CommonCaption", "LoginFailed").ToString();

                        }
                        else
                        {
                            uxLogin.FailureText = this.GetGlobalResourceObject("CommonCaption", "LoginFailed").ToString();
                        }
                    }
                }//WebSite Section, Error message for account gets locked - End
                else
                {
                    uxLogin.FailureText = this.GetGlobalResourceObject("CommonCaption", "LoginFailed").ToString();
                }
            }

            return loginSuccess;
        }

        #endregion
    }
}