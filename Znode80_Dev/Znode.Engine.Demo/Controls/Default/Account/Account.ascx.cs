using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.ShoppingCart;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using System.Linq;
namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Account user control class.
    /// </summary>
    public partial class Controls_Default_Account_Account : System.Web.UI.UserControl
    {
        #region Private Variables 
		private ZNodeUserAccount _userAccount;
        private ZNodeProfile _AccountProfile = new ZNodeProfile();         
        private string billingAddress = string.Empty;
        private string shippingAddress = string.Empty;
        private string _ReferralCommision = string.Empty;
        private string _TaxId = string.Empty;
        private string _ReferralCommisionType = string.Empty; 
        private int accountId = 0;
        private string buyImage = "~/themes/" + ZNodeCatalogManager.Theme + "/images/buynow.gif";
        #endregion

        /// <summary>
        /// Gets or sets the account profile
        /// </summary>
        public ZNodeProfile AccountProfile
        {
            get { return this._AccountProfile; }
            set { this._AccountProfile = value; }
        }

        /// <summary>
        /// Gets or sets the referral commission.
        /// </summary>
        public string ReferralCommision
        {
            get { return _ReferralCommision; }
            set { _ReferralCommision = value; }
        }

        /// <summary>
        /// Gets or sets the Tax Id
        /// </summary>
        public string TaxId
        {
            get { return _TaxId; }
            set { _TaxId = value; }
        }

        /// <summary>
        /// Gets or sets the referral commission type.
        /// </summary>
        public string ReferralCommisionType
        {
            get { return _ReferralCommisionType; }
            set { _ReferralCommisionType = value; }
        }

        #region Public methods

        /// <summary>
        /// Method Checks Call For Pricing message
        /// </summary>
        /// <param name="fieldValue">Field Value</param>
        /// <returns>Returns Call For Pricing message</returns>
        public string CheckForCallForPricing(object fieldValue)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();
            bool status = bool.Parse(fieldValue.ToString());

            if (status || !this.AccountProfile.ShowPrice)
            {
                return mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Get Gift Card Number 
        /// </summary>
        /// <param name="giftCardId">Gift Card ID</param>
        /// <returns>Returns gift card number.</returns>
        public string GetGiftCardNumber(object giftCardId)
        {
            int id = Convert.ToInt32(giftCardId.ToString());
            if (id > 0)
            {
                GiftCardService giftCardService = new GiftCardService();
                return giftCardService.GetByGiftCardId(id).CardNumber;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Bind the user account list.
        /// </summary>
        /// <param name="addressId">Address ID</param>
        /// <returns>Returns the Address Text</returns>
        public string GetAddress(string addressId)
        {
            string addressText = string.Empty;

            AddressService addressService = new AddressService();
            Address address = addressService.GetByAddressID(Convert.ToInt32(addressId));
            if (address != null)
            {
                addressText = address.ToString();
            }

            return addressText;
        } 
        #endregion

        #region Protected Methods and Events
        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.BindGiftCardHistory(this.accountId);
        }

        /// <summary>
        /// Bind field data
        /// </summary>
        protected void BindOrders()
        {
            // Current Portal 
            int? portalId = ZNodeConfigManager.SiteConfig.PortalID;
            ZNode.Libraries.Admin.OrderAdmin orderAdmin = new ZNode.Libraries.Admin.OrderAdmin();
            DataSet ds = orderAdmin.FindOrders(null, null, null, null, this._userAccount.AccountID.ToString(), null, null, null, null, null);
            uxGrid.DataSource = ds;
            uxGrid.DataBind();

            // Disable the view state for the selected columns only
            for (int rowIndex = 0; rowIndex <= uxGrid.Rows.Count - 1; rowIndex++)
            {
                // Do not disable viewstate for last column
                for (int columnIndex = 1; columnIndex <= uxGrid.Rows[rowIndex].Cells.Count - 2; columnIndex++)
                {
                    uxGrid.Rows[rowIndex].Cells[columnIndex].EnableViewState = false;
                }
            }
        }

        /// <summary>
        /// Bind Affiliate data
        /// </summary>
        protected void BindAffiliateDetails()
        {
            if (this._userAccount.ReferralStatus == "A" || this._userAccount.ReferralStatus == "N")
            {
                pnlAffiliate.Visible = true;
                this.ReferralCommision = this._userAccount.ReferralCommission.ToString("N");

                this.TaxId = this._userAccount.TaxId;

                if (this._userAccount.ReferralCommissionTypeId == 1)
                {
                    this.ReferralCommisionType = "Percentage";
                }
                else if (this._userAccount.ReferralCommissionTypeId == 2)
                {
                    this.ReferralCommisionType = "Amount";
                }
            }

            if (this._userAccount.ReferralStatus == "A")
            {
                string affiliateLink = "http://" + UserStoreAccess.DomainName + "/default.aspx?affiliate_id=" + this._userAccount.AccountID;
                hlAffiliateLink.Text = affiliateLink;
                hlAffiliateLink.NavigateUrl = affiliateLink;
            }
        }

        /// <summary>
        /// Bind wish list grid
        /// </summary>
        protected void BindWishList()
        {
            ZNodeProductList _productList = new ZNodeProductList();
            _productList = ZNodeProductList.GetWishListByAccountID(this._userAccount.AccountID);

            uxWishList.DataSource = _productList.ZNodeProductCollection;
            uxWishList.DataBind();
        }

        /// <summary>
        ///  Event triggered when a Edit Address button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEditAddress_Click(object sender, EventArgs e)
        {
            // Redirect to account page
            ZNodeUrl url = new ZNodeUrl();
            Response.Redirect("~/EditContact.aspx");
        }

        protected void LbAddNewAddress_Click(object sender, EventArgs e)
        {
            Response.Redirect("Address.aspx");
        }

        protected void GvAddress_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int addressId = Convert.ToInt32(e.CommandArgument);

            if (string.Compare(e.CommandName, "Edit", true) == 0)
            {
                Response.Redirect("Address.aspx?ItemID=" + addressId);
            }

            if (string.Compare(e.CommandName, "Delete", true) == 0)
            {
                this.DeleteAddress(addressId);
                ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart shoppingcart = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart.CurrentShoppingCart();
                if (shoppingcart != null)
                {
                    shoppingcart.ResetShippingAddress(addressId);
                }
            }
        }

        protected void GvAddress_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAddress.PageIndex = e.NewPageIndex;
            this.BindAddresses();
        }

        /// <summary>
        /// Row Deleting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GvAddress_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindAddresses();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // Get the values from the appropriate cell in the GridView control.
            if (string.Compare(e.CommandName, "View", true) == 0)
            {
                GridViewRow selectedRow = (e.CommandSource as LinkButton).NamingContainer as GridViewRow;  

                TableCell idcell = selectedRow.Cells[0];
                string id = idcell.Text;

                if (string.Compare(e.CommandName, "View", true) == 0)
                {
                    ZNodeUrl url = new ZNodeUrl();
                    string viewLink = "~/order.aspx?itemid=" + id;
                    Response.Redirect(viewLink);
                }
            }
        }

        protected void GvGiftCardHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (string.Compare(e.CommandName, "View", true) == 0)
            {
                GridViewRow selectedRow = (e.CommandSource as LinkButton).NamingContainer as GridViewRow;

                TableCell idcell = selectedRow.Cells[4];
                string id = idcell.Text;
                ZNodeUrl url = new ZNodeUrl();
                string viewLink = "~/order.aspx?itemid=" + id;
                Response.Redirect(viewLink);
            }
        } 

        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindOrders();
        }

        /// <summary>
        /// This event raised on clicking the button btnChangePassword
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnChangePassword_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/password.aspx");
        }

        protected void GvGiftCardHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvGiftCardHistory.PageIndex = e.NewPageIndex;
            this.BindGiftCardHistory(this.accountId);
        }

	    /// <summary>
	    /// Add To Cart button Click event
	    /// </summary>
	    /// <param name="sender">Sender object that raised the event.</param>
	    /// <param name="e">Event Argument of the object that contains event data.</param>
	    protected void Buy_Click(object sender, EventArgs e)
	    {
		    lblMaxQntyError.Text = string.Empty;
		    string link = "~/product.aspx?zpid=";

		    // Getting ProductID from the Link button
		    LinkButton but_buy = sender as LinkButton;
		    int productId = int.Parse(but_buy.CommandArgument);

		    var _product = ZNodeProduct.Create(productId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

		    if (ZNodeShoppingCart.CurrentShoppingCart() == null)
			    Response.Redirect(link + productId + "&action=addtocart");

		    var cartProductQuantityCount = ZNodeShoppingCart.CurrentShoppingCart()
		                                                    .ShoppingCartItems.Cast<ZNodeShoppingCartItem>()
		                                                    .Where(y => y.Product.ProductID == productId)
		                                                    .Sum(x => x.Quantity);
			int? quantityAvailable = _product.MaxQty == 0 ? 10 : _product.MaxQty;

			if (quantityAvailable == cartProductQuantityCount)
			{
				lblMaxQntyError.Visible = true;
				lblMaxQntyError.ForeColor = System.Drawing.Color.Red;
				lblMaxQntyError.Text = _product.Name + " - Can not add to the cart (Maximum quantity reached).";
			}

			else
			{
				Response.Redirect(link + productId + "&action=addtocart");
			}

	    }

	    /// <summary>
        /// Remove link button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LnkRemove_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            int productId = int.Parse(btn.CommandArgument);

            WishListService service = new WishListService();
            WishList wishListEntity = service.GetByProductID(productId)[0];
            service.Delete(wishListEntity);

            // Re-bind grid
            uxWishList.DataSource = new ZNodeProductList().ZNodeProductCollection;
            uxWishList.DataBind();
            this.BindWishList();
        }

        /// <summary>
        /// Occurs when one of the pager buttons is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxWishList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxWishList.PageIndex = e.NewPageIndex;
            this.BindWishList();
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // If user is an admin, then redirect to the admin dashboard

            if (Roles.IsUserInRole("ADMIN"))
            {
                Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["AdminWebsiteUrl"].ToString() + "/secure/default.aspx");
            }
            else if (Roles.IsUserInRole("FRANCHISE"))
            {
				Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["AdminWebsiteUrl"].ToString() + "franchiseadmin/secure/default.aspx");
            }
            else if (Roles.IsUserInRole("VENDOR"))
            {
                Response.Redirect(System.Configuration.ConfigurationManager.AppSettings["AdminWebsiteUrl"].ToString() + "malladmin/secure/product/list.aspx");
            }

            // Get the user account from session
            this._userAccount = ZNodeUserAccount.CurrentAccount();
            if (this._userAccount != null)
            {
                AccountService accountService = new AccountService();
                Account account = accountService.GetByAccountID(this._userAccount.AccountID);
                if (account != null)
                {
                    lblEmail.Text = account.Email;

                    // Email Opt-In
                    if (account.EmailOptIn)
                    {
                        EmailOptin.ImageUrl = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                    }
                    else
                    {
                        EmailOptin.ImageUrl = ZNode.Libraries.Admin.Helper.GetCheckMark(false);
                    }
                }
            }

            this.accountId = this._userAccount.AccountID;
            this.BindAffiliateDetails();

            this.BindAddresses();
	        IEnumerable<int> selectedProdIds = new List<int>();

            if (Request.Params["zpid"] != null)
            {
                //int.TryParse(Request.Params["zpid"], out this.productId);
	            selectedProdIds = Request.Params["zpid"].Split(',').Where(x => !string.IsNullOrEmpty(x)).Select(int.Parse);
            }

			if (selectedProdIds.Any())
	        {
		        foreach (var selectedProdId in selectedProdIds)
		        {
					WishListService service = new WishListService();
					WishListQuery query = new WishListQuery();

			        query.AppendEquals(WishListColumn.ProductID, selectedProdId.ToString());
					query.AppendEquals(WishListColumn.AccountID, this._userAccount.AccountID.ToString());
			        var wishList = service.Find(query.GetParameters());

			        if (wishList.Count == 0)
			        {
				        var wishListEntity = new WishList
					        {
								AccountID = this._userAccount.AccountID,
						        ProductID = selectedProdId,
						        CreateDte = System.DateTime.Today
					        };
				        service.Insert(wishListEntity);
			        }
		        }

				Response.Redirect("~/account.aspx");
	        }

            this.BindAddresses();
            this.BindOrders();
            this.BindWishList();

            this.accountId = this._userAccount.AccountID;

            if (this.Page != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtAccountTitle.Text");
            }
        }
        #endregion

        #region Private methods
        /// <summary>
        /// Bind the user account list.
        /// </summary>
        private void BindAddresses()
        {
            AddressService addressService = new AddressService();
            TList<Address> addressList = addressService.GetByAccountID(this.accountId);
            addressList.Sort("Name ASC");
            gvAddress.DataSource = addressList;
            gvAddress.DataBind();
        }

        /// <summary>
        /// Bind Gift Card History
        /// </summary>
        /// <param name="accountId">accountId to bind GiftCard History</param>
        private void BindGiftCardHistory(int accountId)
        {
            GiftCardAdmin giftCardAdmin = new GiftCardAdmin();
            TList<GiftCardHistory> giftCardHistoryList = giftCardAdmin.GetGiftCardHistoryByAccountID(accountId);
            if (giftCardHistoryList != null)
            {
                giftCardHistoryList.Sort("OrderID DESC");
            }

            gvGiftCardHistory.DataSource = giftCardHistoryList;
            gvGiftCardHistory.DataBind();
        }

        /// <summary>
        /// Delete the selected address.
        /// </summary>
        /// <param name="addressId">Address Id</param>
        private void DeleteAddress(int addressId)
        {
            AddressService addressService = new AddressService();
            AccountAdmin accountAdmin = new AccountAdmin();
            TList<Address> addressList = _userAccount.Addresses;
            int addressCount = addressList.Count;

            // If account has more than one address then validate for default address.
            if (addressCount > 0)
            {
                // Validate default shipping address.
                addressList.ApplyFilter(delegate(Address a) { return a.IsDefaultBilling == true && a.AddressID == addressId; });
                if (addressList.Count == 1)
                {
                    lblError.Text = "Default billing address could not be deleted. Set some other address as default billing address and then try again.";
                    return;
                }

                addressList.RemoveFilter();

                // Validate default shipping address.
                addressList.ApplyFilter(delegate(Address a) { return a.IsDefaultShipping == true && a.AddressID == addressId; });
                if (addressList.Count == 1)
                {
                    lblError.Text = "Default shipping address could not be deleted. Set some other address as default shipping address and then try again.";
                    return;
                }

                addressList.RemoveFilter();
            }

            var deleteAddress = addressList.Find(x => x.AddressID == addressId);

            if (deleteAddress != null)
            {
                addressList.Remove(deleteAddress);
                addressService.Delete(deleteAddress);
            }            
        } 

        #endregion

		
      
    }
}