using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

/// <summary>
/// Displays home page specials
/// </summary>
namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Price user control class.
    /// </summary>
    public partial class Controls_Default_Price_Price : System.Web.UI.UserControl
    {
        #region Private Variables

        private static int TotalRecords = 0;
        private static int recCount = 0;
        private static int ncurrentpage = 0;
        protected string Title = string.Empty;
        private string BuyImage = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/buynow.gif";
        private string _ProductListSeparatorImage = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/line_seperator.gif";
        protected ZNodeProfile _Profile = new ZNodeProfile();
        private ZNodeProductList _productList;
        private decimal _minimum = 0;
        private decimal _maximum = 0;
        
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Product list
        /// </summary>
        public ZNodeProductList ProductList
        {
            get
            {
                return this._productList;
            }

            set
            {
                this._productList = value;
            }
        }

        /// <summary>
        /// Gets the product seperator image
        /// </summary>
        public string ProductListSeparatorImage
        {
            get { return "~/themes/" + ZNodeCatalogManager.Theme + "/Images/line_seperator.gif"; }
        }

        /// <summary>
        /// Gets or sets the Current Page
        /// </summary>
        private int CurrentPage
        {
            get
            {
                int currentPage = 0;

                if (ViewState["CurrentPage"] != null)
                {
                    currentPage = (int)ViewState["CurrentPage"];
                }

                return currentPage;
            }

            set 
            { 
                ViewState["CurrentPage"] = value; 
            }
        }

        /// <summary>
        /// Gets or sets the FirstIndex
        /// </summary>
        private int FirstIndex
        {
            get
            {
                int firstIndex = 0;

                if (ViewState["FirstIndex"] != null)
                {
                    firstIndex = Convert.ToInt32(ViewState["FirstIndex"]);
                }

                return firstIndex;
            }
            
            set 
            { 
                ViewState["FirstIndex"] = value; 
            }
        }

        /// <summary>
        /// Gets or sets the LastIndex
        /// </summary>
        private int LastIndex
        {
            get
            {
                int lastIndex = 0;

                if (ViewState["LastIndex"] != null)
                {
                    lastIndex = Convert.ToInt32(ViewState["LastIndex"]);
                }

                return lastIndex;
            }

            set 
            { 
                ViewState["LastIndex"] = value; 
            }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Represents the GetColor option method
        /// </summary>
        /// <param name="AlternateProductImageCount">Alternate Product Image Count</param>
        /// <returns>Returns the Color Option</returns>
        public string GetColorCaption(int AlternateProductImageCount)
        {
            string output = string.Empty;

            if (AlternateProductImageCount > 0)
            {
                output = "COLORS ";
            }

            return output;
        }

        /// <summary>
        /// Represents the CheckForCallForPricing Method
        /// </summary>
        /// <param name="FieldValue">Field Value</param>
        /// <returns>Returns Call for Pricing message</returns>
        public string CheckForCallForPricing(object FieldValue, object callMessage)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();
            bool Status = bool.Parse(FieldValue.ToString());
            string message = mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);

            if (callMessage != null && !string.IsNullOrEmpty(callMessage.ToString()))
            {
                message = callMessage.ToString();
            }


            if (Status)
            {
                return message;
            }
            else if (!this._Profile.ShowPrice)
            {
                return message;
            }
            else
            {
                return string.Empty;
            }
        }

         /// <summary>
        /// Bind display based on a product list
        /// </summary>
        public void BindProducts()
        {
            if (this._productList.ZNodeProductCollection.Count == 0)
            {
                pnlProductList.Visible = false;
            }
            else
            {
                pnlProductList.Visible = true;
            }

            if (this._productList.ZNodeProductCollection.Count > 0)
            {
                // Set Viewstate object with product collection object
                ViewState["ProductList"] = this._productList;
                
                // Set initial page value
                ViewState["CurrentPage"] = 0;

                this.BindDataListPagedDataSource();
            }
        }
        #endregion

        #region DataList Paging Methods & Events
        /// <summary>
        /// Previous Link Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        public void PrevRecord(object sender, EventArgs e)
        {
            this.CurrentPage = int.Parse(ViewState["CurrentPage"].ToString());
            ViewState["CurrentPage"] = this.CurrentPage -= 1;

            this.BindDataListPagedDataSource();
        }

        /// <summary>
        /// Next Link Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        public void NextRecord(object sender, EventArgs e)
        {
            this.CurrentPage = int.Parse(ViewState["CurrentPage"].ToString());
            ViewState["CurrentPage"] = this.CurrentPage += 1;

            this.BindDataListPagedDataSource();
        }
        #endregion

        #region Protected Methods and Events

        /// <summary>
        /// Event that is raised when List control Selected Index is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindDataListPagedDataSource();
        }

        /// <summary>
        /// Set the page size to selected DropDownList value size. If "SHOW ALL" selected then display all products.
        /// </summary>
        /// <param name="source">Source object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlTopPaging_SelectedIndexChanged(object source, EventArgs e)
        {
            this.SetSelectedPageSize(Convert.ToInt32(ddlTopPaging.SelectedValue));
        }

        /// <summary>
        /// Set the page size to selected DropDownList value size. If "SHOW ALL" selected then display all products.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlBottomPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetSelectedPageSize(Convert.ToInt32(ddlBottomPaging.SelectedValue));
        }

        /// <summary>
        /// Event that is raised when DataListProducts DataBind method is called
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DataListProducts_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            // Find the seperator image and then remove for the last column
            if (e.Item.ItemType == ListItemType.Separator)
            {
                int lastColumnIndex = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;
                if ((e.Item.ItemIndex + 1) % lastColumnIndex == 0 && e.Item.ItemIndex != 0)
                {
                    foreach (Control ctrl in e.Item.Controls)
                    {
                        if (ctrl.GetType().ToString() == "System.Web.UI.HtmlControls.HtmlImage")
                        {
                            e.Item.Controls.Remove(ctrl);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// AddToCart Button is triggered, then redirect to product detail page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Buy_Click(object sender, ImageClickEventArgs e)
        {
            string link = "~/product.aspx?zpid=";

            // Getting ProductID from the image button
            ImageButton but_buy = sender as ImageButton;
            int zpid = int.Parse(but_buy.CommandArgument);

            Response.Redirect(link + zpid + "&action=addtocart");
        }

        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get product id from querystring  
            if (Request.Params["min"] != null)
            {
                this._minimum = decimal.Parse(Request.Params["min"]);
            }
            else
            {
                throw new ApplicationException("Invalid minimum price");
            }

            // Get product id from querystring  
            if (Request.Params["max"] != null)
            {
                this._maximum = decimal.Parse(Request.Params["max"]);
            }
            else
            {
                throw new ApplicationException("Invalid maximum price");
            }

            string currencyPrefix = ZNodeCurrencyManager.GetCurrencyPrefix();
            string currencySuffix = ZNodeCurrencyManager.GetCurrencySuffix();

            // Set title
            this.Title = currencyPrefix + this._minimum.ToString() + currencySuffix + " - " + currencyPrefix + this._maximum.ToString() + currencySuffix;

            if (!Page.IsPostBack)
            {
                this._productList = ZNodeProductList.GetProductsByPrice(ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, this._minimum, this._maximum);
                this.BindProducts();

                // Set Repeat columns to the product Data list
                DataListProducts.RepeatColumns = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;
            }

            if (!this.IsPostBack)
            {
                this.DoPaging();
            }
        }

        /// <summary>
        /// Page Pre Render object
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            // Update the top and bottom paging controls.
            if (this.IsPostBack)
            {
                string EventTarget = this.Page.Request.Form["__EventTarget"].Substring(this.Page.Request.Form["__EventTarget"].LastIndexOf("$") + 1);

                if (EventTarget == "ddlTopPaging")
                {
                    this.SelectDropdownValue(ddlBottomPaging, ddlTopPaging.SelectedValue);
                }
                else if (EventTarget == "ddlBottomPaging")
                {
                    this.SelectDropdownValue(ddlTopPaging, ddlBottomPaging.SelectedValue);
                }
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Bind Datalist using Paged DataSource object
        /// </summary>
        private void BindDataListPagedDataSource()
        {
            // Retrieve collection object from Viewstate
            if (ViewState["ProductList"] != null)
            {
                this._productList = (ZNodeProductList)ViewState["ProductList"];
            }

            // Filter products by Price range
            if (lstFilter.SelectedValue.Equals("1"))
            {
                this._productList.ZNodeProductCollection.Sort("FinalPrice");
            }
            else if (lstFilter.SelectedValue.Equals("2"))
            {
                this._productList.ZNodeProductCollection.Sort("FinalPrice", SortDirection.Descending);
            }
            else if (lstFilter.SelectedValue.Equals("0"))
            {
                this._productList.ZNodeProductCollection.Sort("DisplayOrder", SortDirection.Ascending);
            }

            // Creating an object for the 'PagedDataSource' for holding the data.
            PagedDataSource objPage = new PagedDataSource();

            // Assigning the datasource to the 'objPage' object.
            objPage.DataSource = this._productList.ZNodeProductCollection;

            // Enable paging
            if (this.GetPageSize() <= 0)
            {
                objPage.AllowPaging = false;
            }
            else
            {
                objPage.AllowPaging = true;
                objPage.PageSize = this.GetPageSize();
            }

            // "CurrentPage" is public static variable to hold the current page index value declared in the global section.        
            objPage.CurrentPageIndex = this.CurrentPage;
            ViewState["TotalPages"] = objPage.PageCount;
            ViewState["CurrentPage"] = this.CurrentPage;

            // Checking for enabling/disabling next/prev buttons.
            // Next/prev buton will be disabled when is the last/first page of the pageobject.
            BotNextLink.Enabled = !objPage.IsLastPage;
            TopNextLink.Enabled = !objPage.IsLastPage;
            TopPrevLink.Enabled = !objPage.IsFirstPage;
            BotPrevLink.Enabled = !objPage.IsFirstPage;

            // Set current page index
            ncurrentpage = objPage.CurrentPageIndex + 1;
            recCount = objPage.PageCount;
            TotalRecords = objPage.DataSourceCount;

            // Assigning Datasource to the DataList.
            DataListProducts.DataSource = objPage;
            DataListProducts.DataBind();

            if (!this.IsPostBack)
            {
                this.DoPaging();
            }
        }

        /// <summary>
        /// Represents the GetPageSize method
        /// </summary>
        /// <returns>Returns the PageSize</returns>
        private int GetPageSize()
        {
            if (ViewState["PageSize"] == null)
            {
                // Default page size
                return 8;
            }
            else
            {
                if (ViewState["PageSize"].ToString().ToLower() == "-1")
                {
                    // Disply all items in one page
                    return -1;
                }
                else
                {
                    // Display selected page size
                    return Convert.ToInt32(ViewState["PageSize"].ToString());
                }
            }
        }

        /// <summary>
        /// Binding Paging List
        /// </summary>
        private void DoPaging()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("PageIndex");
            dt.Columns.Add("PageText");
            this.FirstIndex = this.CurrentPage - 5;
            
            if (this.CurrentPage > 5)
            {
                this.LastIndex = this.CurrentPage + 5;
            }
            else
            {
                this.LastIndex = 10;
            }

            if (this.LastIndex > Convert.ToInt32(ViewState["TotalPages"]))
            {
                this.LastIndex = Convert.ToInt32(ViewState["TotalPages"]);
                this.FirstIndex = this.LastIndex - 10;
            }

            if (this.FirstIndex < 0)
            {
                this.FirstIndex = 0;
            }

            ddlTopPaging.Items.Clear();
            ddlBottomPaging.Items.Clear();

            string pagingText = this.GetLocalResourceObject("PagingText").ToString();

            // Each items seperated with & symbol
            string[] pagingItems = pagingText.Split(new char[] { '&' });
            foreach (string pagingItem in pagingItems)
            {
                if (pagingItem.Contains("|") && pagingItem.Trim().Length > 0)
                {
                    // Key value pair seperated with | symbol
                    string[] itemText = pagingItem.Split(new char[] { '|' });
                    DataRow dr = null;
                    dr = dt.NewRow();
                    dr[0] = Convert.ToInt32(itemText[1].Trim());
                    dr[1] = itemText[0].Trim();
                    dt.Rows.Add(dr);
                }
            }

            this.ddlTopPaging.DataTextField = "PageText";
            this.ddlTopPaging.DataValueField = "PageIndex";
            this.ddlTopPaging.DataSource = dt;
            this.ddlTopPaging.DataBind();

            this.ddlBottomPaging.DataTextField = "PageText";
            this.ddlBottomPaging.DataValueField = "PageIndex";
            this.ddlBottomPaging.DataSource = dt;
            this.ddlBottomPaging.DataBind();
        }
               
        /// <summary>
        /// Select the selected value in the both top and bottom page controls.
        /// </summary>
        /// <param name="ddlPaging">Pagin dropdown list </param>
        /// <param name="p">p represents a string value</param>
        private void SelectDropdownValue(DropDownList ddlPaging, string p)
        {
            ListItem li = ddlPaging.Items.FindByValue(p);
            if (li != null)
            {
                ddlPaging.SelectedValue = p;
            }
        }

        /// <summary>
        ///  Represents the Set Selected Page Size method
        /// </summary>
        /// <param name="selectedPageSize">Selected Page Size</param>
        private void SetSelectedPageSize(int selectedPageSize)
        {
            ViewState["PageSize"] = selectedPageSize;

            // Point to the first page if user selects the page size.
            ViewState["CurrentPage"] = 0;

            this.BindDataListPagedDataSource();
        }
        #endregion
    }
}