<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Price_NavigationPrice" Codebehind="NavigationPrice.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<asp:Panel ID="pnlProductList" runat="server" Visible="true">
    <div class="SpecialsTreeView">
        <div class="Title"><uc1:CustomMessage ID="CustomMessage2" MessageKey="LeftNavigationShopByPriceTitle" runat="server" /></div>
        <div>
            <asp:TreeView ID="ctrlNavigation" runat="server" ExpandDepth="2" CssClass="TreeView" NodeIndent="14" ShowExpandCollapse="False">
                <ParentNodeStyle CssClass="ParentNodeStyle" />
                <HoverNodeStyle CssClass="HoverNodeStyle"/>
                <SelectedNodeStyle CssClass="SelectedNodeStyle" />
                <RootNodeStyle CssClass="RootNodeStyle"/>
                <LeafNodeStyle CssClass="LeafNodeStyle"/>
                <NodeStyle CssClass="NodeStyle" />
            </asp:TreeView>
        </div>
    </div>
</asp:Panel> 