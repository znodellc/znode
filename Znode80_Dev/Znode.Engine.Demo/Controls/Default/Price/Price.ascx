<%@ control language="C#" autoeventwireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Price_Price" Codebehind="Price.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>
<%@ Register  Src="~/Controls/Default/Product/SwatchImages.ascx" TagName="Swatches" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>

 <asp:Panel ID="pnlProductList" runat="server" Visible="False"  meta:resourcekey="pnlProductListResource1">
 <script language="javascript" type="text/javascript">
     window.addEvent('domready', function () {
         SqueezeBox.assign($$('a.boxed'), {
             parse: 'rel'
         });

     });      
    </script>  
 <div class="Price">     
   <div class="Title"><h1><uc1:CustomMessage id="CustomMessage1" MessageKey="LeftNavigationShopByPriceTitle" runat="server"></uc1:CustomMessage> : <%=Title %></h1></div>
   
   <div class="ProductList">
        <div id="ProductListNavigation">
            <div class="TopPagingSection">
                <div class="Sorting">
                    <span class="Label"><asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:CommonCaption, Sort%>" /></span>
                    <asp:DropDownList ID="lstFilter" runat="server" OnSelectedIndexChanged="LstFilter_SelectedIndexChanged" AutoPostBack="True" meta:resourcekey="lstFilterResource1">
                        <asp:ListItem Value="0" meta:resourcekey="ListItemResource1">Popular Items</asp:ListItem>
                        <asp:ListItem Value="1" meta:resourcekey="ListItemResource2">Price Low to High</asp:ListItem>
                        <asp:ListItem Value="2" meta:resourcekey="ListItemResource3">Price High to Low</asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="TopPaging">
                     <asp:LinkButton ID="TopPrevLink" CssClass="Button" runat="server" OnClick="PrevRecord" Text="<<"></asp:LinkButton>
                      <span>
                        <asp:Localize ID="Localize12" runat="server" meta:resourceKey="txtPage">
                        </asp:Localize>
                        <asp:Localize ID="Localize13" runat="server" meta:resourceKey="txtOF">
                        </asp:Localize>
                     </span>
                     <asp:LinkButton ID="TopNextLink" runat="server" OnClick="NextRecord" CssClass="Button" Text=">>"></asp:LinkButton>
                     <span>| <asp:DropDownList ID="ddlTopPaging" runat="server" AutoPostBack="True" CssClass="Pagingdropdown" meta:resourcekey="ddlTopPagingResource1" OnSelectedIndexChanged="DdlTopPaging_SelectedIndexChanged"></asp:DropDownList></span>
                     <span><asp:Localize ID="Localize9" runat="server" meta:resourceKey="txtProducts"></asp:Localize><span class="SlashSeparator">/</span><asp:Literal ID="Literal1" runat="server" Text="<%$ Resources:CommonCaption, Page%>" /></span>
                </div>
            </div>
        </div>       

        <div class="PriceProductlist">
         <asp:DataList ID="DataListProducts" runat="server" RepeatDirection="Horizontal" OnItemDataBound="DataListProducts_ItemDataBound" meta:resourcekey="DataListProductsResource1">
            <ItemStyle CssClass="ItemStyle" VerticalAlign="Top" />                     
             <ItemTemplate>
                 <div class="PriceListItem">
                     <div class="DetailLink">
                        <a id="hlName" class="boxed" href='<%# ResolveUrl("~/Quickwatch.aspx?zpid=" + DataBinder.Eval(Container.DataItem, "ProductID")) %>' rel="{handler:'iframe',size:{x:700,y:500}}"><%# DataBinder.Eval(Container.DataItem, "Name").ToString() %></a>
                     </div>
                     <div class="Price">
                        <asp:Label ID="Label2"  EnableViewState="false" runat="server" meta:resourcekey="Label2Resource1" Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>'
                        Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && _Profile.ShowPrice %>'></asp:Label>
                    </div>
                    <div class="StarRating">
                        <ZNode:ProductAverageRating ID="uxProductAverageRating" runat="server" />
                    </div>     
                    <div class="Image">
                            <a id="imgProduct" class="boxed" href='<%# ResolveUrl("~/Quickwatch.aspx?zpid=" + DataBinder.Eval(Container.DataItem, "ProductID")) %>'
                                ondblclick="javascript:return false;" rel="{handler:'iframe',size:{x:700,y:500}}">
                                <img id='<%# "Img"  + DataBinder.Eval(Container.DataItem, "ProductID")%>' alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'
                                    border="0" src='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "SmallImageFilePath").ToString()) %>'
                                    style="vertical-align: bottom;"></img>
                            </a>
                        </div>
                     <div class="ProductSwatches" style='border-top: <%# GetColorCaption(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "AlternateProductImageCount"))) == "" ? "None":"solid 1px #D1D1D1" %>'>
                        <div class="ColorCaption">
                            <asp:Label ID="lblColor" EnableViewState="false" runat="server" meta:resourcekey="lblColorResource1" Text='<%# GetColorCaption(Convert.ToInt32(DataBinder.Eval(Container.DataItem, "AlternateProductImageCount"))) %>'></asp:Label>
                        </div>
                        <ZNode:Swatches ID="uxProductSwatches" runat="server" ControlType="Swatches" ProductId='<%# Convert.ToInt32(DataBinder.Eval(Container.DataItem, "AlternateProductImageCount")) > 0? DataBinder.Eval(Container.DataItem, "ProductID"):0 %>' IncludeProductImage="true" />
                     </div>
                     <div class="CallForPrice">
                        <asp:Label ID="uxCallForPricing" EnableViewState="false" runat="server" CssClass="Price" meta:resourcekey="uxCallForPricingResource1"
                            Text='<%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing"), DataBinder.Eval(Container.DataItem, "CallMessage")) %>'></asp:Label>
                     </div>
                     <div class="GrayBorder">
                        <ZNode:Spacer ID="Spacer1" EnableViewState="false"  runat="server" SpacerHeight="1" SpacerWidth="1" />
                     </div>
                    
                      <div class="BuyButton" style="width: 100%;">
                            <div style="width: 30%; float: left;">
                                <asp:LinkButton ID="btnbuy" EnableViewState="false" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>'
                                    CssClass="Button View" meta:resourcekey="btnbuyResource1" PostBackUrl='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink") %>'
                                    Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && _Profile.ShowAddToCart %>'>View � </asp:LinkButton>
                            </div>
                            <div style="width: 70%; float: right; text-align: right;">
                                <asp:Image ID="NewItemImage" EnableViewState="false" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.gif") %>'
                                    meta:resourcekey="NewItemImageResource1" Visible='<%# DataBinder.Eval(Container.DataItem, "NewProductInd") %>' />
                                &nbsp;<asp:Image  EnableViewState="false" ID="FeaturedItemImage" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("sale.gif") %>'
                                    meta:resourcekey="FeaturedItemImageResource1" Visible='<%# DataBinder.Eval(Container.DataItem, "FeaturedInd") %>' />
                            </div>
                            <div style="clear: both;">
                            </div>
                        </div>
                 </div>
             </ItemTemplate><SeparatorStyle CssClass="Separator" VerticalAlign="Top" Wrap="True" />
             <SeparatorTemplate>
                <img id="imgSeparator" enableviewstate="false" runat="server" src="<%# ProductListSeparatorImage %>"></img>
             </SeparatorTemplate>
            </asp:DataList>
        </div>

        <div class="BottomPaging">
            <div class="Paging">
                <asp:LinkButton ID="BotPrevLink" runat="server" OnClick="PrevRecord" CssClass="Button" Text="<<">
                   </asp:LinkButton>
                <span>
                    <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtPage"></asp:Localize>
                    <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtOF"></asp:Localize>
                </span>
                <asp:LinkButton ID="BotNextLink" OnClick="NextRecord" runat="server" CssClass="Button" Text=">>">                    
                    </asp:LinkButton>
                <span>|<span class="ShowText">
                    <asp:DropDownList runat="server" ID="ddlBottomPaging" CssClass="Pagingdropdown" AutoPostBack="True"
                        OnSelectedIndexChanged="DdlBottomPaging_SelectedIndexChanged" meta:resourcekey="ddlBottomPagingResource1">
                    </asp:DropDownList>
                </span></span><span>
                    <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtProducts"></asp:Localize><span
                        class="SlashSeparator">/</span>
                    <asp:Literal ID="Literal2" runat="server" Text="<%$ Resources:CommonCaption, Page%>" /></span>
            </div>
        </div>
        </div>
 </div>
 </asp:Panel>