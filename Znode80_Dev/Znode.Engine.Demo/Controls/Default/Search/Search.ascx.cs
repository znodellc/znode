using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the SiteAdmin Themes_Default_Search_Search user control class
    /// </summary>
    public partial class Controls_Default_Search_Search : System.Web.UI.UserControl
    {
        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Registers the event for the payment (child) control
            this.uxProductList.PagingSelectedIndexChanged += new EventHandler(this.UxPaging_SelectedIndexChanged);
            this.uxProductList.SortingSelectedIndexChanged += new EventHandler(this.UxSorting_SelectedIndexChanged);
            this.uxProductList.PreviousButtonClicked += new EventHandler(this.PreviousButton_Click);
            this.uxProductList.NextButtonClicked += new EventHandler(this.NextButton_Click);

            if (!Page.IsPostBack)
            {
                Page.Title = "Advanced Search";

                this.BindFields();

                // Set NULL to last viewed category when locale change.
                Session["BreadCrumzcid"] = null;

                // Retrieves the value of the keyword in the query string
                if (Request.Params["keyword"] != null)
                {
                    string keyword = Request.Params["keyword"];
                    keyword = keyword.Replace("''", string.Empty);
                    keyword = keyword.Replace("\"\"", string.Empty);

                    txtKeyword.Text = keyword;
                }

                // Retrieves the value of the categoryId in the query string
                if (Request.Params["Scid"] != null)
                {
                    lstCategories.SelectedValue = Request.Params["Scid"];
                    Session["BreadCrumzcid"] = lstCategories.SelectedValue;
                }

                // Retrieves the value of the productnum in the query string
                if (Request.Params["ProductNum"] != null)
                {
                    string prodNum = Request.Params["ProductNum"];
                    prodNum = prodNum.Replace("''", string.Empty);
                    prodNum = prodNum.Replace("\"\"", string.Empty);

                    txtProductNum.Text = prodNum;
                }

                // Retrieves the value of the sku in the query string
                if (Request.Params["sku"] != null)
                {
                    string sku = Request.Params["sku"];
                    sku = sku.Replace("''", string.Empty);
                    sku = sku.Replace("\"\"", string.Empty);

                    txtSKU.Text = sku;
                }

                // Retrieves the value of the search option in the query string
                if (Request.Params["soption"] != null)
                {
                    lstSearchOption.SelectedValue = Request.Params["soption"];
                }                

                // Search products
                this.BindSearch();                
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(string.Empty);

            if (txtKeyword.Text.Trim().Length > 0)
            {
                sb.Append("?keyword=" + Server.UrlEncode(txtKeyword.Text.Trim()));
            }

            if (txtProductNum.Text.Trim().Length > 0)
            {
                if (sb.Length == 0)
                {
                    sb.Append("?ProductNum=" + Server.UrlEncode(txtProductNum.Text.Trim()));
                }
                else
                {
                    sb.Append("&ProductNum=" + Server.UrlEncode(txtProductNum.Text.Trim()));
                }
            }

            if (txtSKU.Text.Trim().Length > 0)
            {
                if (sb.Length == 0)
                {
                    sb.Append("?sku=" + Server.UrlEncode(txtSKU.Text.Trim()));
                }
                else
                {
                    sb.Append("&sku=" + Server.UrlEncode(txtSKU.Text.Trim()));
                }
            }

            if (lstCategories.SelectedValue != "0")
            {
                if (sb.Length == 0)
                {
                    sb.Append("?category=" + lstCategories.SelectedItem.Text.Trim() + "&Scid=" + lstCategories.SelectedValue);
                }
                else
                {
                    sb.Append("&category=" + lstCategories.SelectedItem.Text.Trim() + "&Scid=" + lstCategories.SelectedValue);
                }

                Session["BreadCrumzcid"] = lstCategories.SelectedValue;
            }

            if (lstSearchOption.SelectedValue != "0")
            {
                if (sb.Length == 0)
                {
                    sb.Append("?sption=" + lstSearchOption.SelectedValue);
                }
                else
                {
                    sb.Append("&soption=" + lstSearchOption.SelectedValue);
                }
            }

            Response.Redirect("~/search.aspx" + sb.ToString());
        }

        /// <summary>
        /// Previous Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void PreviousButton_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Next Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void NextButton_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Paging Selected Index Changed Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Sorting Selected Index Changed Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxSorting_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// clears search by redirecting back to same page
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            ZNodeUrl url = new ZNodeUrl();
            string link = "~/Search.aspx";

            Response.Redirect(link);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Bind products based on search criteria
        /// </summary>
        protected void BindSearchData()
        {
            // retrieve collection object from Viewstate
            if (Session["ProductList"] == null)
            {
                this.BindSearch();
            }

            ZNodeSearch sm = new ZNodeSearch();

            ZNodeSearch productSearch = new ZNodeSearch();

            int pageSize = uxProductList.PageSize;

            if (pageSize == -1)
            {
                pageSize = uxProductList.TotalRecords;
            }

            string sortby = uxProductList.SortBy;
            string sortDirection = uxProductList.SortDirection;
            int sortOption = uxProductList.SortOption;

            if (Request.QueryString["sort"] != null)
            {
                if (Request.QueryString["sort"].Equals("0"))
                    sortby = "DisplayOrder";
                else
                    sortby = "RetailPrice";

                if (Request.QueryString["sort"].Equals("2"))
                    sortDirection = "DESC";
                else
                    sortDirection = "ASC";

             
                sortOption = Convert.ToInt32(Request.QueryString["sort"].ToString());
            }
            if (Session["ProductList"] != null)
            {
                uxProductList.ProductList = productSearch.GetProductList((DataTable)Session["ProductList"], sortby, sortDirection, uxProductList.CurrentPage, uxProductList.PageSize, sortOption);

                uxProductList.BindProducts();
            }

            if (uxProductList.ProductList.ZNodeProductCollection.Count == 0)
            {
                FailureText.Text = Resources.CommonCaption.NoDataFound;
            }
        }

        /// <summary>
        /// Bind products based on search criteria
        /// </summary>
        protected void BindSearch()
        {
            ZNodeSearch sm = new ZNodeSearch();

            int CategoryId = int.Parse(lstCategories.SelectedValue);
            int SearchOption = int.Parse(lstSearchOption.SelectedValue);

            // Log our search in the database so we can keep metrics on what is most popular.
            string search = txtKeyword.Text.Trim();

            if (search.Length > 0)
            {
                if (search.Length > 254)
                {
                    search = search.Remove(254);
                }
                
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.KeywordSearch, search.ToLower());
            }

            search = txtProductNum.Text.Trim();

            if (search.Length > 0)
            {
                if (search.Length > 254)
                {
                    search = search.Remove(254);
                }

                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Productsearch, search.ToLower().Trim());
            }

            search = txtSKU.Text.Trim();

            if (search.Length > 0)
            {
                if (search.Length > 254)
                {
                    search = search.Remove(254);
                }
                
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.SKUsearch, search.Trim().ToLower());
            }

            ZNodeSearch productSearch = new ZNodeSearch();
            uxProductList.ProductList = productSearch.FindProducts(
                                                      ZNodeConfigManager.SiteConfig.PortalID, 
                                                      ZNodeCatalogManager.CatalogConfig.CatalogID, 
                                                      txtKeyword.Text.Trim(), 
                                                      " ",
                                                      CategoryId, 
                                                      SearchOption, 
                                                      txtSKU.Text.Trim(), 
                                                      txtProductNum.Text.Trim(), 
                                                      uxProductList.CurrentPage, 
                                                      uxProductList.PageSize,
                                                      uxProductList.SortBy, 
                                                      uxProductList.SortDirection);

            uxProductList.BindProducts();

            if (uxProductList.ProductList.ZNodeProductCollection.Count == 0)
            {
                FailureText.Text = Resources.CommonCaption.NoDataFound;
            }
        }

        /// <summary>
        /// Bind selection field data
        /// </summary>
        protected void BindFields()
        {
            // Add categories 
            ZNode.Libraries.DataAccess.Custom.CategoryHelper categoryHelper = new CategoryHelper();
            DataSet ds = categoryHelper.GetRootCategoryItems(ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.CatalogConfig.CatalogID, ZNodeProfile.CurrentUserProfileId);

            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                if (Convert.ToBoolean(dr["Visibleind"].ToString()))
                {
                    ListItem li = new ListItem();
                    li.Text = dr["Name"].ToString();
                    li.Value = dr["CategoryId"].ToString();

                    lstCategories.Items.Add(li);
                }
            }
        }
        #endregion
    }
}