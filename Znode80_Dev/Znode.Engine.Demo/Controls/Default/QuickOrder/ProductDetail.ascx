<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Znode.Engine.Demo.Controls_Default_QuickOrder_ProductDetail" Codebehind="ProductDetail.ascx.cs" %>
<%@ Register Src="~/Controls/Default/Product/ProductAttributeGrid.ascx" TagName="ProductAttributeGrid"
    TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/common/spacer.ascx" %>
<div class="QuickOrder">
    <div class="ProductSearch">
        <h1>
            <div style="width: 100%; display: block;">
                <div style="width: 150px;">
                    <asp:Localize ID="txtSelectProduct" runat="server" meta:resourceKey="txtSelectProduct">
                    </asp:Localize>
                </div>
                <div class="CloseLink">
                    <asp:LinkButton ID="btnClose" CausesValidation="False" OnClientClick="_isset=-1;"
                        runat="server" OnClick="BtnClose_Click" 
                        meta:resourcekey="btnCloseResource1">
                        <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtCloseX">
                        </asp:Localize></asp:LinkButton>
                </div>
            </div>
        </h1>
        <div>
            <ZNode:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="1" runat="server"></ZNode:Spacer>
        </div>
        <asp:HiddenField ID="PageIndex" Value="1" runat="server" />
        <asp:HiddenField ID="TotalPages" Value="0" runat="server" />
        
        <!-- Product Search -->
        <asp:Panel ID="pnlSearchParams" runat="server" DefaultButton="ibSearch" 
            meta:resourcekey="pnlSearchParamsResource1">
        
                <div>
                    <div class="Row">
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtPartialProductName">
                            </asp:Localize></div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtProductName" runat="server" ValidationGroup="groupSearch" 
                                meta:resourcekey="txtProductNameResource1"></asp:TextBox></div>
                    </div>
                    <div  class="Row">
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtPartialItemNo">
                            </asp:Localize></div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtProductNum" runat="server" ValidationGroup="groupSearch" 
                                meta:resourcekey="txtProductNumResource1"></asp:TextBox></div>
                    </div>
                    <div class="Row">
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtPartialSKU">
                            </asp:Localize></div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtProductSku" runat="server" ValidationGroup="groupSearch" 
                                meta:resourcekey="txtProductSkuResource1"></asp:TextBox></div>
                    </div>
                    <div class="Row">
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize6" runat="server" meta:resourceKey="txtPartialBrand">
                            </asp:Localize></div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBrand" runat="server" ValidationGroup="groupSearch" 
                                meta:resourcekey="txtBrandResource1"></asp:TextBox></div>
                    </div>
                    <div class="Row">
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize7" runat="server" meta:resourceKey="txtPartialCategory">
                            </asp:Localize></div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtCategory" runat="server" ValidationGroup="groupSearch" 
                                meta:resourcekey="txtCategoryResource1"></asp:TextBox></div>
                    </div>
                    <div class="Row">
                        <div  class="FieldStyle">&nbsp;
                        </div>
                        <div class="ValueStyle">
                            <asp:ImageButton ID="ibSearch" ValidationGroup="grpSearch" OnClick="Search_Click"
                                runat="server" meta:resourcekey="ibSearchResource1" />
                        </div>
                    </div>
                </div>
                <div>
                    <ZNode:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="1" runat="server"></ZNode:Spacer>
                 </div>
                <div>                     
                        <asp:Label CssClass="Error" ID="lblSearhError" runat="server" 
                            EnableViewState="False" meta:resourcekey="lblSearhErrorResource1" />                    
                </div>
            
            
        </asp:Panel>
        <!-- Product List -->
        <asp:Panel ID="pnlProductList" runat="server" Visible="False" 
            meta:resourcekey="pnlProductListResource1">
            <div>
                <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="1" runat="server"></ZNode:Spacer>
            </div>
            <div class="
            ">
                <asp:Localize ID="Localize8" runat="server" meta:resourceKey="txtHint">
                </asp:Localize></div>
            <asp:GridView ID="uxGrid" CellPadding="6" CssClass="SearchGrid"
                AllowPaging="True" runat="server" AutoGenerateColumns="False" 
                GridLines="None" meta:resourcekey="uxGridResource1">
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <Columns>
                    <asp:TemplateField meta:resourcekey="TemplateFieldResource1">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkButton" runat="server" 
                                CommandArgument='<%# DataBinder.Eval(Container.DataItem,"ProductId") %>' 
                                CommandName="Select" meta:resourcekey="lnkButtonResource1" 
                                OnClick="Select_Clicked" OnClientClick="_isset=-1;" Text="Select"></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" meta:resourcekey="BoundFieldResource1">
                    <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductNum" meta:resourcekey="BoundFieldResource2">
                    <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ShortDescription" meta:resourcekey="BoundFieldResource3">
                    <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="RetailPrice" DataFormatString="{0:c}" meta:resourcekey="BoundFieldResource4">
                    <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <PagerSettings Visible="False" />
                <PagerStyle CssClass="PagerStyle" />
                <RowStyle CssClass="RowStyle" />
            </asp:GridView>
            <div class="PagingField">
                <asp:LinkButton ID="FirstPageLink" runat="server" meta:resourcekey="FirstPageLinkResource1"
                    OnClick="MoveToFirstPage" OnClientClick="_isset=-1;" Text="&laquo; <%$ Resources:CommonCaption, Prev%>"></asp:LinkButton>
                <asp:LinkButton ID="PreviousPageLink" OnClientClick="_isset=-1;" Text="&laquo; Prev"
                    runat="server" OnClick="PrevRecord" 
                    meta:resourcekey="PreviousPageLinkResource1"></asp:LinkButton>
                &nbsp;|
                <asp:Literal ID="Literal5" runat="server" Text="<%$ Resources:CommonCaption, Page%>">
                </asp:Literal> |&nbsp;
                <asp:LinkButton ID="NextPageLink" runat="server" meta:resourcekey="NextPageLinkResource1"
                    OnClick="NextRecord" OnClientClick="_isset=-1;" Text="<%$ Resources:CommonCaption, Next%> &raquo;"></asp:LinkButton>
                <asp:LinkButton ID="LastPageLink" OnClientClick="_isset=-1;" Text="Last &raquo;"
                    OnClick="MoveToLastPage" runat="server" 
                    meta:resourcekey="LastPageLinkResource1"></asp:LinkButton>
            </div>
        </asp:Panel>
        <!-- Product Detail section -->
        <asp:Panel ID="pnlProductDetails" runat="server" Visible="False" 
            meta:resourcekey="pnlProductDetailsResource1">
            <div class="ProductDetail">
                <table id="Table1" runat="server">
                    <tr runat="server">
                        <td runat="server">
                            <table class="Grid" cellpadding="0" cellspacing="0">
                                <tr class="HeaderStyle">
                                    <td>
                                        <asp:Localize ID="Localize9" runat="server" meta:resourceKey="txtQty">
                                        </asp:Localize>
                                    </td>
                                    <td>
                                        <asp:Localize ID="Localize10" runat="server" meta:resourceKey="txtOptions">
                                        </asp:Localize>
                                    </td>
                                    <td>
                                        <asp:Localize ID="Localize11" runat="server" meta:resourceKey="txtUnitPrice">
                                        </asp:Localize>
                                    </td>
                                    <td>
                                        <asp:Localize ID="Localize12" runat="server" meta:resourceKey="txtTotalPrice">
                                        </asp:Localize>
                                    </td>
                                </tr>
                                <tr class="RowStyle">
                                    <td>
                                        <asp:DropDownList ID="Qty" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Qty_SelectedIndexChanged">
                                        </asp:DropDownList>
                                        <div>
                                            <asp:RequiredFieldValidator ValidationGroup="grpCartItems" ID="qtyRequiredFieldValidator" ControlToValidate="Qty"
                                                CssClass="Error" meta:resourcekey="qtyRequiredFieldValidator" runat="server" Display="Dynamic" 
                                                SetFocusOnError="True"></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ValidationGroup="grpCartItems" ValidationExpression="<%$ Resources:CommonCaption, NumericValidatorExpression%>"
                                                ID="qtyRegularExpressionValidator" CssClass="Error" ControlToValidate="Qty" meta:resourcekey="qtyRegularExpressionValidator" runat="server"
                                                Display="Dynamic" SetFocusOnError="True"></asp:RegularExpressionValidator>
                                            <div>
                                                <asp:RangeValidator ValidationGroup="grpCartItems" ID="QuantityRangeValidator" CssClass="Error"
                                                    MinimumValue="0" ControlToValidate="Qty" meta:resourcekey="QuantityRangeValidator" MaximumValue="1"
                                                    Display="Dynamic" SetFocusOnError="True" runat="server" Type="Integer"></asp:RangeValidator></div>
                                        </div>
                                    </td>
                                    <td>
                                        <asp:PlaceHolder ID='ControlPlaceHolder' runat="server"></asp:PlaceHolder>
                                        <div>
                                            <ZNode:Spacer ID="Spacer3" SpacerHeight="1" SpacerWidth="1" runat="server"></ZNode:Spacer>
                                        </div>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblUnitPrice" runat="server" Text='&nbsp;'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTotalPrice" runat="server" Text='&nbsp;'></asp:Label>
                                    </td>
                                </tr>
                            </table>
                            <div align="right">
                                <asp:Label ID="uxStatus" CssClass="Error" EnableViewState="False" 
                                    runat="server"></asp:Label></div>
                            <div>
                                <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="5" runat="server"></ZNode:Spacer>
                            </div>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td runat="server">
                            <table width="100%" cellpadding="0" cellspacing="2">
                                <tr id="tablerow" runat="server" visible="False">
                                    <td class="OuterBorder" runat="server">
                                        <asp:Image ID="CatalogItemImage" AlternateText="NA" runat="server" />
                                    </td>
                                    <td class="OuterBorder" runat="server">
                                        <asp:Label ID="ProductDescription" runat="server"></asp:Label>
                                        <div>
                                            <ZNode:ProductAttributeGrid ID="uxProductAttributeGrid" CssClassName="SizeGrid" runat="server" />
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div>
                                            <ZNode:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="10" runat="server"></ZNode:Spacer>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="right" valign="top">
                                        <div>
                                            <asp:ImageButton ID="ibSubmit" runat="server" OnClick="BtnSubmit_Click" ValidationGroup="grpCartItems" />
                                            <asp:Button ID="ibAddAnotherProduct" Text="Add Another Product" runat="server" OnClick="BtnSubmitAnother_Click"
                                                ValidationGroup="grpCartItems" />
                                            <asp:Button ID="ibAddCancel" Text="Cancel" runat="server" OnClick="BtnCancel_Click"
                                                CausesValidation="False" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
    <!--During Update Process -->
    <asp:UpdateProgress ID="UpdateProgressFirefox" runat="server" DisplayAfter="0"
        Visible="False">
        <ProgressTemplate>
            <asp:Panel ID="Panel1" CssClass="overlay" runat="server" 
                meta:resourcekey="Panel1Resource1">
                <asp:Panel ID="Panel2" CssClass="loader" runat="server" 
                    meta:resourcekey="Panel2Resource1">
                    <div class="UpdateProgress">
                        <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtLoading">
                        </asp:Localize><img id="ImgLoading" align="absmiddle" src='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("loading.gif") %>'
                            runat="server" /> </div>
                </asp:Panel>
            </asp:Panel>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdateProgress ID="UpdateProgressIE" runat="server" DisplayAfter="0"
        Visible="False">
        <ProgressTemplate>
            <div>
                <iframe frameborder="0" src="about:blank" style="border: 0px; position: absolute;
                    z-index: 9; left: 0px; top: 0px; width: expression(this.offsetParent.scrollWidth);
                    height: expression(this.offsetParent.scrollHeight); filter: progid:DXImageTransform.Microsoft.Alpha(Opacity=75, FinishOpacity=0, Style=0, StartX=0, FinishX=100, StartY=0, FinishY=100);">
                </iframe>
                <div style="position: absolute; z-index: 10; left: expression((this.offsetParent.clientWidth/2)-(this.clientWidth/2)+this.offsetParent.scrollLeft);
                    top: expression((this.offsetParent.clientHeight/2)-(this.clientHeight/2)+this.offsetParent.scrollTop);">
                    <div class="UpdateProgress">
                        <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtLoading">
                        </asp:Localize><img id="Img1" align="absmiddle" src='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("loading.gif") %>'
                            runat="server" /> </div>
                </div>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</div>
