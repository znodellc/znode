using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Customer Feedback user control class.
    /// </summary>
    public partial class Controls_Default_Contact_CustomerFeedback : System.Web.UI.UserControl
    {
        #region Private Variable
        private string _CaseTitle = "User Feedback";
        #endregion

        #region Public Property
        /// <summary>
        /// Gets or sets the CaseTitle
        /// </summary>
        public string CaseTitle
        {
            get
            {
                return this._CaseTitle;
            }

            set
            {
                this._CaseTitle = value;
            }
        }
        #endregion

        #region Page Event

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this._CaseTitle = GetLocalResourceObject("UserFeedback").ToString();

            if (this.Page.Title != null)
            {
                this.Page.Title = this._CaseTitle;
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Event is raised when Submit button is pressed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            this.SendEmail();
            this.CreateCaseRecord();
            pnlContact.Visible = false;
            pnlConfirm.Visible = true;
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Send Email Receipt
        /// </summary>
        private void SendEmail()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(this.CaseTitle);
            sb.Append(System.Environment.NewLine);
            sb.Append(System.Environment.NewLine);
            sb.Append(GetLocalResourceObject("txtComments.Text").ToString() + ": ");
            sb.Append(Comments.Text);
            sb.Append(System.Environment.NewLine);
            sb.Append(GetLocalResourceObject("txtFirstName.Text").ToString() + ": ");
            sb.Append(txtFirstName.Text);
            sb.Append(GetLocalResourceObject("txtLastName.Text").ToString() + ": ");
            sb.Append(txtLastName.Text);            
            sb.Append(System.Environment.NewLine);
            sb.Append(Resources.CommonCaption.City + ": ");
            sb.Append(txtcity.Text);
            sb.Append(System.Environment.NewLine);
            sb.Append(Resources.CommonCaption.State + ": ");
            sb.Append(txtstate.Text);
            sb.Append(System.Environment.NewLine);
            sb.Append(Resources.CommonCaption.EmailID + ": ");
            sb.Append(txtemail.Text);
            sb.Append(System.Environment.NewLine);
            sb.Append(GetLocalResourceObject("txtPublish.Text").ToString() + " - ");
            sb.Append(chkPublish.Checked);
            sb.Append(System.Environment.NewLine);
            try
            {
                ZNodeEmail.SendEmail(ZNodeConfigManager.SiteConfig.CustomerServiceEmail, txtemail.Text, string.Empty, GetLocalResourceObject("MailSubject").ToString() + " : " + this.CaseTitle, sb.ToString(), false);
            }
            catch
            {
            }
        }

        /// <summary>
        /// Creates New Case Record
        /// </summary>
        private void CreateCaseRecord()
        {
            ZNode.Libraries.Admin.CaseAdmin _CaseAdmin = new ZNode.Libraries.Admin.CaseAdmin();
            ZNode.Libraries.DataAccess.Entities.CaseRequest CaseRecord = new ZNode.Libraries.DataAccess.Entities.CaseRequest();

            // Set Values 
            CaseRecord.Title = this.CaseTitle;
            CaseRecord.Description = Server.HtmlEncode("Comments: " + Comments.Text + "; <br>City: " + txtcity.Text + "; <br>State: " + txtstate.Text + "; <br>It is OK to share this feedback with other customers - " + chkPublish.Checked);
            CaseRecord.FirstName = Server.HtmlEncode(txtFirstName.Text);
            CaseRecord.LastName = Server.HtmlEncode(txtLastName.Text);
            CaseRecord.EmailID = Server.HtmlEncode(txtemail.Text);

            // Set Some Default Values
            CaseRecord.CasePriorityID = 3;
            CaseRecord.CaseStatusID = 1;
            CaseRecord.CreateDte = System.DateTime.Now;
            CaseRecord.PortalID = ZNodeConfigManager.SiteConfig.PortalID;
            CaseRecord.OwnerAccountID = null;
            CaseRecord.CaseOrigin = "Feedback Form";
            CaseRecord.CreateUser = System.Web.HttpContext.Current.User.Identity.Name;
            
            CaseRecord.CompanyName = string.Empty;
            CaseRecord.PhoneNumber = string.Empty;

            if (Session[ZNodeSessionKeyType.UserAccount.ToString()] != null)
            {
                ZNodeUserAccount _usrAccount = Session[ZNodeSessionKeyType.UserAccount.ToString()] as ZNodeUserAccount;
                CaseRecord.AccountID = _usrAccount.AccountID;
            }
            else
            {
                CaseRecord.AccountID = null;
            }

            // Add New Case for this Email
            _CaseAdmin.Add(CaseRecord);
        }
        #endregion
    }
}