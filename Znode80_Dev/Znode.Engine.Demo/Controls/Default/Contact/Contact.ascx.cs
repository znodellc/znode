using System;
using System.Text;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Contact user control class.
    /// </summary>
    public partial class Controls_Default_Contact_Contact : System.Web.UI.UserControl
    {
        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();

            // Set SEO Properties
            ZNodeSEO seo = new ZNodeSEO();
            seo.SEOTitle = resourceManager.GetLocalResourceObject(LocalizeTitle.TemplateControl.AppRelativeVirtualPath, "txtContactus.Text");
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }
        #endregion

        #region General Events
		protected void btnSubmit_Click(object sender, EventArgs e)
        {
            this.SendEmail();
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Send Email Receipt
        /// </summary>
        private void SendEmail()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("This message was sent using the contact us form on your website: ");
            sb.Append(System.Environment.NewLine);
            sb.Append(System.Environment.NewLine);
            sb.Append("Name: ");
            sb.Append(FirstName.Text);
            sb.Append(" ");
            sb.Append(LastName.Text);
            sb.Append(System.Environment.NewLine);
            sb.Append("Company Name:");
            sb.Append(CompanyName.Text);
            sb.Append(System.Environment.NewLine);
            sb.Append("Phone Number: ");
            sb.Append(PhoneNumber.Text);
            sb.Append(System.Environment.NewLine);
            sb.Append("Comments: ");
            sb.Append(System.Environment.NewLine);
            sb.Append(Comments.Text);

            pnlConfirm.Visible = true;
            pnlContact.Visible = false;

            try
            {
                ZNodeEmail.SendEmail(ZNodeConfigManager.SiteConfig.CustomerServiceEmail, Email.Text, string.Empty, "Feedback from Website Visitor", sb.ToString(), false);
                lblMessage.Text = this.GetLocalResourceObject("FeedbackSubmitted").ToString();
            }
            catch (Exception)
            {
                lblMessage.Text = this.GetLocalResourceObject("RequestFailed").ToString();
                lblMessage.CssClass = "Error";
                return;
            }

            this.CreateCaseRecord();
        }

        /// <summary>
        /// Creates New Case Record
        /// </summary>
        private void CreateCaseRecord()
        {
            ZNode.Libraries.Admin.CaseAdmin _CaseAdmin = new ZNode.Libraries.Admin.CaseAdmin();
            ZNode.Libraries.DataAccess.Entities.CaseRequest CaseRecord = new ZNode.Libraries.DataAccess.Entities.CaseRequest();

            // Set Values 
            CaseRecord.Title = "Contact Form Submission";
            CaseRecord.FirstName = Server.HtmlEncode(FirstName.Text);
            CaseRecord.LastName = Server.HtmlEncode(LastName.Text);
            CaseRecord.CompanyName = Server.HtmlEncode(CompanyName.Text);
            CaseRecord.PhoneNumber = Server.HtmlEncode(PhoneNumber.Text);
            CaseRecord.EmailID = Email.Text;
            CaseRecord.Description = Server.HtmlEncode(Comments.Text);

            // Set Some Default Values
            CaseRecord.CasePriorityID = 3;
            CaseRecord.CaseStatusID = 1;
            CaseRecord.CreateDte = System.DateTime.Now;
            CaseRecord.PortalID = ZNodeConfigManager.SiteConfig.PortalID;
            CaseRecord.OwnerAccountID = null;
            CaseRecord.CaseOrigin = "Contact Us Form";
            CaseRecord.CreateUser = System.Web.HttpContext.Current.User.Identity.Name;

            if (Session[ZNodeSessionKeyType.UserAccount.ToString()] != null)
            {
                ZNodeUserAccount _usrAccount = Session[ZNodeSessionKeyType.UserAccount.ToString()] as ZNodeUserAccount;
                CaseRecord.AccountID = _usrAccount.AccountID;
            }
            else
            {
                CaseRecord.AccountID = null;
            }

            // Add New Case for this Email
            _CaseAdmin.Add(CaseRecord);
        }
        #endregion
    }
}
