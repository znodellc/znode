<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Contact_Contact" CodeBehind="Contact.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>
<asp:UpdatePanel runat="server" ID="updatePnlContactus">
	<ContentTemplate>
<asp:Panel ID="pnlContact" class="ContactUs" runat="server" meta:resourcekey="pnlContactResource1">
    <div class="PageTitle">
        <asp:Localize ID="LocalizeTitle" runat="server" meta:resourceKey="txtContactus"></asp:Localize>
    </div>
    <p>
        <uc1:CustomMessage ID="CustomMessage1" runat="server" MessageKey="ContactUsIntroText"></uc1:CustomMessage>
    </p>
    <div class="Form">

        <asp:Panel ID="pnlContactus" runat="server" DefaultButton="btnSubmit">

            <div class="Row Clear">
                <div align="right" class="FieldStyle LeftContent">
                    <asp:Label ID="lblName" runat="server" AssociatedControlID="FirstName" meta:resourcekey="lblNameResource1">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:CommonCaption, FirstName%>"></asp:Localize>
                        :</asp:Label>
                </div>
                <div>
                    <asp:TextBox ID="FirstName" runat="server" MaxLength="100" meta:resourcekey="FirstNameResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="NameRequired" runat="server" ControlToValidate="FirstName"
                        CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, FirstNameRequired%>"
                        ToolTip="<%$ Resources:CommonCaption, FirstNameRequired%>"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="Row Clear">
                <div align="right" class="FieldStyle LeftContent">
                    <asp:Label ID="Label1" runat="server" AssociatedControlID="LastName" meta:resourcekey="Label1Resource1">
                        <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:CommonCaption, LastName%>"></asp:Localize>
                        :</asp:Label>
                </div>
                <div>
                    <asp:TextBox ID="LastName" runat="server" MaxLength="100" meta:resourcekey="LastNameResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="LastName"
                        CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, LastNameRequired%>"
                        ToolTip="<%$ Resources:CommonCaption, LastNameRequired %>">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="Row Clear">
                <div align="right" class="FieldStyle LeftContent">
                    <asp:Label ID="lblCompanyName" runat="server" AssociatedControlID="CompanyName">
                        <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:CommonCaption, CompanyName%>">
                        </asp:Localize>
                        :</asp:Label>
                </div>
                <div class="LeftContent">
                    <asp:TextBox ID="CompanyName" runat="server" MaxLength="100"></asp:TextBox>
                </div>
            </div>
            <div class="Row Clear">
                <div align="right" class="FieldStyle LeftContent">
                    <asp:Label ID="EmailLabel" runat="server" AssociatedControlID="Email">
                        <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:CommonCaption, EmailID%>"></asp:Localize>
                        :</asp:Label>
                </div>
                <div>
                    <asp:TextBox ID="Email" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="EmailRequired" runat="server" ControlToValidate="Email"
                        CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired%>"
                        ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired%>"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="Email"
                        CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired%>"
                        ToolTip="<%$ Resources:CommonCaption, ValidEmailRequired%>" ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression%>"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="Row Clear">
                <div align="right" class="FieldStyle LeftContent">
                    <asp:Label ID="PhoneNumberLabel" runat="server" AssociatedControlID="PhoneNumber">
                        <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:CommonCaption, PhoneNumber%>"></asp:Localize>
                        :</asp:Label>
                </div>
                <div>
                    <asp:TextBox ID="PhoneNumber" runat="server" MaxLength="20" meta:resourcekey="PhoneNumberResource1"></asp:TextBox>
                    <%--  <asp:RegularExpressionValidator id="RegularExpressionValidator7" ControlToValidate="PhoneNumber" CssClass="Error"
            ValidationExpression ="\d{3}-\d{3}-\d{4}" ErrorMessage="Enter valid phone number" Display="Dynamic"  runat="server"/>--%>
                </div>
            </div>
            <div class="Row Clear">
                <div align="right" class="FieldStyle LeftContent">
                    <asp:Label ID="CommentsLabel" runat="server" AssociatedControlID="Comments">
                        <asp:Localize ID="Localize7" runat="server" meta:resourcekey="CommentsLabel"></asp:Localize>
                        :</asp:Label>
                </div>
                <div>
                    <asp:TextBox ID="Comments" runat="server" Height="136px" TextMode="MultiLine" Width="300px"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Comments"
                            CssClass="Error" Display="Dynamic" ValidationGroup="uxCreateUserWizard" meta:resourcekey="RequiredFieldValidator2">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="Row Clear">
                <div align="left" colspan="2" class="FailureText LeftContent">
                    <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                </div>
            </div>
            <div class="LeftContent">
                &nbsp;
            </div>
            <div align="left" class="ContactUsButton">
				<asp:Button ID="btnSubmit" runat="server" OnClick="btnSubmit_Click" Text="Submit"
					 CssClass="button" meta:resourcekey="imgSubmitResource1"/>
            </div>
        </asp:Panel>

    </div>
    <div class="Clear">
        <ZNode:spacer ID="Spacer8" EnableViewState="false" SpacerHeight="20" SpacerWidth="10" runat="server"></ZNode:spacer>
    </div>
</asp:Panel>
<asp:Panel ID="pnlConfirm" runat="server" Visible="False" CssClass="SuccessMsg" meta:resourcekey="pnlConfirmResource1">
    <br />
    <br />
    <asp:Label ID="lblMessage" runat="server" Width="100%"></asp:Label>
    <div class="Clear">
        <ZNode:spacer ID="Spacer9" EnableViewState="false" SpacerHeight="20" SpacerWidth="10" runat="server"></ZNode:spacer>
    </div>
</asp:Panel>
</ContentTemplate>
</asp:UpdatePanel>
