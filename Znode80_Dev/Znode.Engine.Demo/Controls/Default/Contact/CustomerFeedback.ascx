<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Znode.Engine.Demo.Controls_Default_Contact_CustomerFeedback" Codebehind="CustomerFeedback.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<div class="Form">
    <asp:Panel ID="pnlContact" runat="server" class="Feedback" meta:resourcekey="pnlContactResource1">
        <div class="PageTitle">
            <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtHeading"></asp:Localize></div>
        <p>
            <uc1:CustomMessage ID="CustomMessage1" runat="server" MessageKey="CustomerFeedbackIntroText">
            </uc1:CustomMessage>
        </p>
        <div class="Row">
            <div align="right" class="FieldStyle LeftContent">
                <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtComments"></asp:Localize></div>
            <div>
                <asp:TextBox ID="Comments" runat="server" TextMode="MultiLine" Rows="10" Columns="50"
                    meta:resourcekey="CommentsResource1"></asp:TextBox></div>
        </div>
        <div class="Clear">
            <ZNode:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:Spacer>
        </div>
        <div class="Row">
            <div align="right" class="FieldStyle LeftContent">
                <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtFirstName"></asp:Localize></div>
            <div>
                <asp:TextBox ID="txtFirstName" runat="server" meta:resourcekey="txtFirstNameResource1"></asp:TextBox></div>
        </div>
        <div class="Row">
            <div align="right" class="FieldStyle LeftContent">
                <asp:Localize ID="Localize7" runat="server" meta:resourceKey="txtLastName"></asp:Localize></div>
            <div>
                <asp:TextBox ID="txtLastName" runat="server" meta:resourcekey="txtLastNameResource1"></asp:TextBox></div>
        </div>
        <div class="Clear">
            <ZNode:Spacer ID="Spacer4" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:Spacer>
        </div>
        <div class="Row">
            <div align="right" class="FieldStyle LeftContent">
                <asp:Localize ID="lblCity" runat="server" Text="<%$ Resources:CommonCaption, City%>">></asp:Localize></div>
            <div>
                <asp:TextBox ID="txtcity" runat="server"></asp:TextBox></div>
        </div>
        <div class="Clear">
            <ZNode:Spacer ID="Spacer5" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:Spacer>
        </div>
        <div class="Row">
            <div align="right" class="FieldStyle LeftContent">
                <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:CommonCaption, State%>"></asp:Localize></div>
            <div>
                <asp:TextBox ID="txtstate" runat="server" meta:resourcekey="txtstateResource1"></asp:TextBox></div>
        </div>
        <div class="Clear">
            <ZNode:Spacer ID="Spacer6" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:Spacer>
        </div>
        <div class="Row">
            <div align="right" class="FieldStyle LeftContent">
                <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:CommonCaption, EmailID%>"></asp:Localize></div>
            <div>
                <asp:TextBox ID="txtemail" runat="server" meta:resourcekey="txtemailResource1"></asp:TextBox>
                <asp:RegularExpressionValidator ID="regemailID" runat="server" ControlToValidate="txtemail"
                    ValidationGroup="1" ErrorMessage="<%$ Resources:CommonCaption, ValidEmailRequired%>"
                    Display="Dynamic" ValidationExpression="<%$ Resources:CommonCaption, EmailValidationExpression%>"
                    CssClass="Error"></asp:RegularExpressionValidator>
                    
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="<%$ Resources:CommonCaption, EmailAddressRequired%>"
                    ToolTip="<%$ Resources:CommonCaption, EmailAddressRequired%>" ControlToValidate="txtemail"
                    ValidationGroup="1" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="Clear">
            <ZNode:Spacer ID="Spacer7" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:Spacer>
        </div>
        <div class="Row">
            <div align="right" class="FieldStyle LeftContent">
                &nbsp;
            </div>
            <div>
                <asp:CheckBox Checked="True" ID="chkPublish" runat="server" meta:resourcekey="chkPublishResource1" /><strong>
                    <asp:Localize ID="Localize6" runat="server" meta:resourceKey="txtPublish"></asp:Localize></strong></div>
        </div>
        <div class="Clear">
            <ZNode:Spacer ID="Spacer8" SpacerHeight="1" SpacerWidth="10" runat="server"></ZNode:Spacer>
        </div>
        <div class="Row">
            <div align="right" class="FieldStyle LeftContent">
                &nbsp;
            </div>
            <div>
                <asp:LinkButton ID="Button1" runat="server" ValidationGroup="1" OnClick="BtnSubmit_Click"
                    CssClass="Button" Text="<%$ Resources:CommonCaption, Submit%>" /></div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlConfirm" runat="server" Visible="False" meta:resourcekey="pnlConfirmResource1">
        <div>
            <ZNode:Spacer ID="Spacer1" SpacerHeight="50" SpacerWidth="10" runat="server" />
        </div>
        <div>
            <uc1:CustomMessage ID="CustomMessage2" runat="server" MessageKey="CustomerFeedbackConfirmationIntroText">
            </uc1:CustomMessage>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="50" SpacerWidth="10" runat="server" />
        </div>
    </asp:Panel>
</div>
