using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.Tagging;
using ZNode.Libraries.Framework.Business;
using Category = ZNode.Libraries.DataAccess.Entities.Category;

/// <summary>
/// Displays the bread crumb menu for the multifront pages
/// </summary>
namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the NavigationBreadCrumbs user control class.
    /// </summary>
    public partial class Controls_Default_Navigation_NavigationBreadCrumbs : System.Web.UI.UserControl
    {
        #region Private Variables
        private int _categoryId = 0;
        private int _productId = 0;
        private string _breadcrumbcid = string.Empty;
        private string facetValues = string.Empty;
        private ZNodeNavigation navigation = new ZNodeNavigation();
        private ZNodeFacets Tagging = new ZNodeFacets();
        private DataSet facetNameDataset = new DataSet();
        #endregion

        #region Public properties

        public string BreadCrumbCid
        {
            get { return this._breadcrumbcid; }
            set { this._breadcrumbcid = value; }
        }

        #endregion

        #region Helper Methods

        public string IsSEOUrl()
        {
            string zcid = string.Empty;

            if (Request.QueryString["zcid"] != null)
            {
                Session["BreadCrumzcid"] = Request.QueryString["zcid"];
                zcid = Request.QueryString["zcid"].ToString();
            }
            else if (Session["BreadCrumzcid"] != null && Request.QueryString["zpid"] == null)
            {
                zcid = Session["BreadCrumzcid"].ToString();
            }
            else if (Request.QueryString["zpid"] != null)
            {
                ZNode.Libraries.DataAccess.Service.ProductCategoryService service = new ZNode.Libraries.DataAccess.Service.ProductCategoryService();
                TList<ProductCategory> pcategories = service.GetByProductID(Convert.ToInt32(Request.QueryString["zpid"]));
                if (pcategories.Count > 0)
                {
                    zcid = pcategories[0].CategoryID.ToString();
                }
            }
            else
            {
                zcid = "0";
            }

            ZNode.Libraries.DataAccess.Service.CategoryService cservice = new ZNode.Libraries.DataAccess.Service.CategoryService();
            Category category = cservice.GetByCategoryID(Convert.ToInt32(zcid));

            if (category != null)
            {
                string seourl = string.Empty;
                if (category.SEOURL != null)
                {
                    seourl = category.SEOURL;
                }

                return ZNodeSEOUrl.MakeURL(category.CategoryID.ToString(), SEOUrlType.Category, seourl);
            }

            return ZNodeSEOUrl.MakeURL("0", SEOUrlType.Category, string.Empty);
        }

        public string GetPageURL()
        {
            return this.IsSEOUrl();
        }

        public void GetFacetsBreadCrumbs()
        {
            // Default path        
            StringBuilder breadCrumbPath = new StringBuilder();
            StringBuilder queryStringPath = new StringBuilder();

            MessageConfigAdmin messageadmin = new MessageConfigAdmin();
            MessageConfig messageconfig = new MessageConfig();

            // ResolveUrl - Converts this URL into one that is usable on the requesting client.
            messageconfig = messageadmin.GetByKeyPortalIDLocaleID(ZNodeMessageKey.BreadCrumbsHomeLinkText.ToString(), ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
            if (messageconfig != null)
            {
                breadCrumbPath.Append(this.navigation.CreateLink(ResolveUrl("~/"), messageconfig.Value));
            }

            if (Request.Params["zcid"] != null)
            {
                ZNodeCategory category = (ZNodeCategory)HttpContext.Current.Items["Category"];

                // Get bread crumb string based on category
                breadCrumbPath.Append(category.CategoryPath);
            }
            else
            {
                if (Session["BreadCrumzcid"] != null)
                {
                    ZNode.Libraries.DataAccess.Service.ProductCategoryService service = new ZNode.Libraries.DataAccess.Service.ProductCategoryService();
                    ProductCategory pcategory = service.GetByProductIDCategoryID(Convert.ToInt32(Request.QueryString["zpid"]), Convert.ToInt32(Session["BreadCrumzcid"]));

                    if (pcategory != null)
                    {
                        this._categoryId = pcategory.CategoryID;
                    }
                }

                // Get bread crumb string based on category/ product
                breadCrumbPath.Append(this.navigation.GetBreadCrumbPath(this._categoryId, this._productId, " &raquo; ", ZNodeConfigManager.SiteConfig.PortalID));

                if (!string.IsNullOrEmpty(this.navigation.BreadCrumbCid))
                {
                    this._categoryId = Convert.ToInt32(this.navigation.BreadCrumbCid);

                    Session["BreadCrumzcid"] = this.navigation.BreadCrumbCid;
                }
            }
            int i=0;
            foreach (String key in Request.QueryString.AllKeys)
            {
                if (key.ToLower() == "zcid" || key.ToLower() == "page" || key.ToLower() == "size" || key.ToLower() == "sort")
                    continue;
                else
                    i++;
                
            }
            if (i == 0)
                Session["FacetedValues"] = null;

            if (Session["FacetedValues"] != null)
            {
                this.facetValues = Session["FacetedValues"].ToString();

                if (!string.IsNullOrEmpty(this.facetValues))
                {
                    // Bind Facet BreadCrumb Label
                    this.BindLabelControl(this.facetValues);
                }
            }

            // Set label to breadcrumb
            lblPath.Text = breadCrumbPath.ToString();

            // Set the last categoryId
            this._breadcrumbcid = this.navigation.BreadCrumbCid;
        }

        /// <summary>
        /// Bind the Label control
        /// </summary>
        /// <param name="_selectedFacetValues">Selected Facet Values</param>
        public void BindLabelControl(string _selectedFacetValues)
        {
            // Clear the controls
            ControlsPlaceHolder.Controls.Clear();

            // String Builder
            StringBuilder labelFacetValues = new StringBuilder();

            this.facetNameDataset = this.Tagging.GetBreadCrumbsByFacetIds(_selectedFacetValues);
            this.facetNameDataset.Tables[0].PrimaryKey = new DataColumn[] { this.facetNameDataset.Tables[0].Columns["TagId"] };

            foreach (string facets in _selectedFacetValues.Split(','))
            {
                if (!string.IsNullOrEmpty(facets))
                {
                    DataRow facetRow = this.facetNameDataset.Tables[0].Rows.Find(facets);

                    if (facetRow != null)
                    {
                        // Start to add the dynamic controls here
                        ControlsPlaceHolder.Controls.Add(new LiteralControl("<span>"));

                        Literal literal1 = new Literal();
                        literal1.Text = " &raquo; ";

                        HyperLink linkBtnFacetName = new HyperLink();
                        linkBtnFacetName.ID = "breadcrumblnkBtnFacet" + facetRow["FacetId"].ToString();
                        linkBtnFacetName.Text = facetRow["FacetName"].ToString();
                        linkBtnFacetName.CssClass = "linkBtnFacetText";
                        linkBtnFacetName.NavigateUrl = this.ConstructQueryStringFromSessionValues(facetRow["FacetId"].ToString());

                        ControlsPlaceHolder.Controls.Add(literal1);
                        ControlsPlaceHolder.Controls.Add(linkBtnFacetName);
                        ControlsPlaceHolder.Controls.Add(new LiteralControl("</span>"));
                    }
                }
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            // Get category id from querystring  
            if (Request.Params["zcid"] != null)
            {
                int.TryParse(Request.Params["zcid"], out this._categoryId);
                Session["BreadCrumzcid"] = this._categoryId;
            }
            else
            {
                if (Session["BreadCrumzcid"] != null && Request.Params["zpid"] != null)
                {
                    // Get categoryid from session
                   // this._categoryId = int.Parse(Session["BreadCrumzcid"].ToString());
                }
            }

            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                int.TryParse(Request.Params["zpid"], out this._productId);
            }

            this.GetFacetsBreadCrumbs();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Construct QueryString key name.
        /// </summary>
        /// <param name="squeryname">query name string</param>
        /// <returns>Returns the formatted Query Name</returns>
        private string QKeyName(string squeryname)
        {
            return squeryname.Replace(" ", "_") + "=";
        }

        /// <summary>
        /// Construct the query string from session values
        /// </summary>
        /// <param name="facetId">String represents the Facet Id</param>
        /// <returns>Returns the Query String from session values</returns>
        private string ConstructQueryStringFromSessionValues(string facetId)
        {
            string qstring = string.Empty;

			var facetService = new ZNode.Libraries.DataAccess.Service.FacetService();
			var facetGroupService = new ZNode.Libraries.DataAccess.Service.FacetGroupService();

            string[] keyValues = Session["FacetedValues"].ToString().Split(',');

            foreach (string sessionkey in keyValues)
            {
                if (!string.IsNullOrEmpty(sessionkey))
                {
					ZNode.Libraries.DataAccess.Entities.Facet facet = facetService.GetByFacetID(Convert.ToInt32(sessionkey));

                    if (facet != null)
                    {
						qstring = qstring + (qstring.Trim().Length == 0 ? string.Empty : "&") + this.QKeyName(facetGroupService.GetByFacetGroupID(facet.FacetGroupID.Value).FacetGroupLabel) + Server.UrlEncode(facet.FacetName);
                    }

                    if (sessionkey == facetId)
                    {
                        break;
                    }
                }
            }

            string qurl = string.Empty;

            if (qstring.Trim().Length > 0)
            {
                string url = this.GetPageURL();
                qurl = url + ((url.IndexOf('?') > 0) ? "&" : "?") + qstring.Trim();
            }

            return qurl;
        }
        #endregion
    }
}