<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Navigation_NavigationMenu" Codebehind="NavigationMenu.ascx.cs" %>

<div id="Menu">	
    <asp:Menu ID="ctrlMenu" runat="server" DynamicHorizontalOffset="0" RenderingMode="Table" 
        EnableTheming="False" Orientation="Horizontal" StaticSubMenuIndent="0px" 
        CssClass="Menu" StaticEnableDefaultPopOutImage="False" MaximumDynamicDisplayLevels="1">
        <StaticMenuStyle CssClass="StaticMenuStyle" />
        <StaticMenuItemStyle CssClass="StaticMenuItemStyle" />       
        <StaticSelectedStyle CssClass="StaticSelectedStyle" />
        <StaticHoverStyle CssClass="StaticHoverStyle" />
        <DynamicHoverStyle CssClass="DynamicHoverStyle" />
        <DynamicMenuStyle CssClass="DynamicMenuStyle" />
        <DynamicSelectedStyle CssClass="DynamicSelectedStyle" />
        <DynamicMenuItemStyle CssClass="DynamicMenuItemStyle" />
    </asp:Menu>
</div>

