<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_QuickWatch_QuickWatch" Codebehind="QuickWatch.ascx.cs" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ProductHighlights.ascx" TagName="ProductHighlights"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ProductPrice.ascx" TagName="ProductPrice"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ProductRelated.ascx" TagName="ProductRelated"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ProductAttributes.ascx" TagName="ProductAttributes"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ProductTabs.ascx" TagName="ProductTabs"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ProductAddOns.ascx" TagName="ProductAddOns"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/Product/CatalogImage.ascx" TagName="CatalogImage"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ExternalLinks.ascx" TagName="ExternalLinks"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Specials/BestSellers.ascx" TagName="BestSellers"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/SwatchImages.ascx" TagName="Swatches"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/Bundleproducts.ascx" TagName="BundleProduct" 
    TagPrefix="ZNode" %>
    
<script language="javascript" type="text/javascript">

    function ActiveTabChanged(sender, e) {
        PageLoadedEventHandler();
    }

    function detect() {
        document.cookie = 'ZNodeCookie';
        if (document.cookie.indexOf("ZNodeCookie") < 0) {
            alert("You must enable cookies in your browser settings in order to add items to your cart.");
            return false;
        }
        else {
            return true;
        }
    }
</script>

<div id="Quickwatch">
    <div id="LeftElement">
        <div id="Image">
            <ZNode:CatalogImage runat="server" ID="CatalogImage" />
        </div>
        <asp:Panel ID="pnlSwatches" runat="server" meta:resourcekey="pnlSwatchesResource1">
            <div class="ProductSwatches">
                <span class="Text">
                    <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtColorOptions">
                    </asp:Localize></span>
                <ZNode:Swatches ID="uxProductSwatches" IncludeProductImage="true" runat="server"
                    ControlType="Swatches" />
            </div>
        </asp:Panel>
        <div>
            <ZNode:ExternalLinks ID="ExternalLinks1" runat="server" IsQuickWatch="true" />
        </div>
        <div id="RelatedTabs">
            <ajaxToolKit:TabContainer OnClientActiveTabChanged="ActiveTabChanged" runat="server"
                ID="ProductTabs" CssClass="RelatedProductTabStyle" ScrollBars="Auto" ActiveTabIndex="0"
                meta:resourcekey="ProductTabsResource1">
                <ajaxToolKit:TabPanel ID="pnlProductRelated" runat="server" meta:resourcekey="pnlProductRelatedResource1">
                    <ContentTemplate>
                        <div class="CrossSell">
                            <ZNode:ProductRelated ID="uxProductRelated" IsQuickwatch="true" runat="server" ShowName="false"
                                ShowImage="true" ShowDescription="false" />
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlBestSellers" runat="server" meta:resourcekey="pnlBestSellersResource1">
                    <ContentTemplate>
                     <div class="BestSeller">
                        <ZNode:BestSellers ID="uxBestSellers" IsQuickwatch="true" runat="server" ShowName="false"
                            ShowImage="true" ShowDescription="false" />
                      </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
            </ajaxToolKit:TabContainer>
        </div>
    </div>
    <div id="RightElement">
        <div class="ProductDetail">
            <div>
                <h1 class="ProductTitle">
                <asp:Label ID="ProductTitle" Text="{0}" runat="server" meta:resourcekey="ProductTitleResource1"></asp:Label>
                </h1>
            </div>
            <div class="Description">
                <asp:Label ID="ProductDescription" runat="server" meta:resourcekey="ProductDescriptionResource1"></asp:Label></div>
            <div class="ProductNum">
                <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtItemNo"></asp:Localize>:&nbsp;#<asp:Label
                    ID="lblProductID" runat="server" meta:resourcekey="ProductIDResource1"></asp:Label></div>
            <div class="StarRating">
                <ZNode:ProductAverageRating IsQuickWatch="true" ShowWriteReviewLink="false" ID="uxReviewRating"
                    runat="server" />
                | <a id="A1" runat="server">
                    <asp:LinkButton OnClick="Review_Click" Style="text-decoration: none;" ID="Hyperlink"
                        runat="server" Text="WRITE A REVIEW" meta:resourcekey="HyperlinkResource1" /><asp:Localize
                            ID="Localize3" runat="server" meta:resourceKey="txtWriteaReview"></asp:Localize></a></div>
              <div class="StarRight"> <ZNode:ProductHighlights ID="uxHighlights" MaxDisplayColumns="9" runat="server" Visible="False" /> </div>
        </div>
        <!--Ajax Tabs-->
        <div id="Tabs">
            <ZNode:ProductTabs ID="uxProductTabs" runat="server" />
        </div>
        <asp:UpdatePanel ID="UpdatePanelAddToCart" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="OrderingOptions">
                    <ZNode:ProductAttributes ID="uxProductAttributes" runat="server" />
                    <ZNode:ProductAddOns ID="uxProductAddOns" runat="server" ShowCheckBoxesVertically="true"
                        ShowRadioButtonsVerically="true"></ZNode:ProductAddOns>
                    <asp:Panel ID="pnlQty" runat="server" Visible="False">
                        <div class="Quantity">
                            <span>
                                <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtQuantity"></asp:Localize>
                                :</span>
                            <asp:DropDownList ID="uxQty" CssClass="QuantityDropDown" runat="server" Width="136px"
                                AutoPostBack="True" OnSelectedIndexChanged="UxQty_SelectedIndexChanged" meta:resourcekey="uxQtyResource1">
                            </asp:DropDownList>
                        </div>
                    </asp:Panel>
                    <div class="StockMsg">
                        <asp:Label ID="lblstockmessage" runat="server" CssClass="InStockMsg" meta:resourcekey="lblstockmessageResource1"></asp:Label></div>
                    <div class="PriceContent">
                        <ZNode:ProductPrice ID="uxProductPrice" runat="server" />
                    </div>
                    <div class="CallForPrice">
                        <asp:Label ID="uxCallForPricing" runat="server" Text="Call For Pricing" Visible="False"
                            meta:resourcekey="uxCallForPricingResource1"></asp:Label></div>
                    <div class="StatusMsg">
                        <asp:Label ID="uxStatus" EnableViewState="False" runat="server" CssClass="OutOfStockMsg"
                            meta:resourcekey="uxStatusResource1"></asp:Label></div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div id="BundleProduct">
            <ZNode:BundleProduct ID="uxBundleProduct" runat="server" IsQuickWatch="true" />
        </div>
      
         <asp:UpdatePanel ID="pnlAddToCart" runat="server" UpdateMode="Always">
         <ContentTemplate>
            <div class="AddToCartButton">
            <asp:LinkButton ID="uxAddToCart"  EnableViewState="false" runat="server" ImageUrl="~/Themes/Default/Images/view_cart_bg.gif"
                OnClick="UxAddToCart_Click" ImageAlign="AbsMiddle" meta:resourcekey="uxAddToCartResource1"
                Text="Add to Cart &rsaquo;&rsaquo;" CssClass="Button" />
             </div>
         </ContentTemplate>
         </asp:UpdatePanel>
        <br />
    </div>
</div>

