using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the QuickWatch user control class.
    /// </summary>
    public partial class Controls_Default_QuickWatch_QuickWatch : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct Product;
        private ZNodeProfile profile = new ZNodeProfile();
        private int ProductId;
        private ZNodeShoppingCart _shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets Shopping cart quantity
        /// </summary>
        public int ShoppingCartQuantity
        {
            get
            {
                if (ViewState["Quantity"] != null)
                {
                    return (int)ViewState["Quantity"];
                }

                return 1;
            }

            set
            {
                ViewState["Quantity"] = value;
            }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Update and Bind inventory status message and product price
        /// </summary>
        /// <returns>Returns a bool value to update product status or not</returns>
        public bool UpdateProductStatus()
        {

            UpdatePanelAddToCart.Update();

            // Hide Product price
            uxProductPrice.Visible = this.profile.ShowPrice && !this.Product.CallForPricing;

            // Local Variables
            this.ShoppingCartQuantity = int.Parse(uxQty.SelectedValue);
            bool status = true;

            ZNodeSKU SKU = ZNodeSKU.CreateByProductDefault(this.Product.ProductID);

            // Check if product has add-ons
            if (this.Product.ZNodeAddOnCollection.Count > 0)
            {
                // If product has Add-Ons, then validate that all Add-Ons Values have been selected by the customer
                string addOnMessage = string.Empty;
                ZNodeAddOnList selectedAddOn = null;

                status = uxProductAddOns.ValidateAddOns(out addOnMessage, this.ShoppingCartQuantity, out selectedAddOn);

                if (!status)
                {
                    uxStatus.Visible = true;

                    // Error Message
                    uxStatus.Text = addOnMessage;
                   
                    // Hide AddToCart button
                    uxAddToCart.Visible = false;
                   
                    return status;
                }

                // Set Selected Add-on
                this.Product.SelectedAddOnItems = selectedAddOn;
            }

            // Check if product has attributes
            if (this.Product.ZNodeAttributeTypeCollection.Count > 0)
            {
                // If product has attributes then validate that all attributes have been selected by
                // the customer
                // validate attributes first - This is to verify that all required attributes have a valid selection
                // if a valid selection is not found then an error message is displayed
                string attributeMessage = string.Empty;
                string attributeList = string.Empty;
                string selectedAttributesDescription = string.Empty;

                status = uxProductAttributes.ValidateAttributes(out attributeMessage, out attributeList, out selectedAttributesDescription);

                if (!status)
                {
                    uxStatus.Visible = true;

                    uxStatus.Text = attributeMessage;

                    // Hide AddToCart button
                    uxAddToCart.Visible = false;

                    return status;
                }

                // Get a sku based on attributes selected
                SKU = ZNodeSKU.CreateByProductAndAttributes(this.Product.ProductID, attributeList);

                // Set Attributes Description to Description property
                SKU.AttributesDescription = selectedAttributesDescription;

                // Check stock    
                if (SKU.QuantityOnHand < this.ShoppingCartQuantity && (!this.Product.AllowBackOrder) && this.Product.TrackInventoryInd)
                {
                    lblstockmessage.Text = this.Product.OutOfStockMsg; // Display Out of stock message

                    lblstockmessage.CssClass = "OutOfStockMsg";
                    // Hide AddToCart button
                    uxAddToCart.Visible = false;

                    status = false;
                }
            }
            else
            {
                // If Product has no SKUs associated with this product.           
                SKU.AttributesDescription = string.Empty;

                // Check stock    
                if (this.Product.QuantityOnHand < this.ShoppingCartQuantity && (this.Product.AllowBackOrder == false) && this.Product.TrackInventoryInd)
                {
                    lblstockmessage.Text = this.Product.OutOfStockMsg; // Display Out of stock message

                    lblstockmessage.CssClass = "OutOfStockMsg";
                    
                    // Hide AddToCart button
                    uxAddToCart.Visible = false;

                    status = false;
                }
            }

            this.Product.SelectedSKU = SKU;
            this.Product.CheckSKUProfile();
            this.Product.IsPromotionApplied = false;
            this.Product.ApplyPromotion();

            // Check if product has child product (Bundle product)
            if (this.Product.ZNodeBundleProductCollection.Count > 0)
            {
                string addOnMessage = string.Empty;

                status = uxBundleProduct.ValidateAddons(out addOnMessage);

                if (!status)
                {
                    uxStatus.Visible = true;

                    // Error Message
                    uxStatus.Text = addOnMessage;

                    // Hide AddToCart button
                    uxAddToCart.Visible = false;

                    return status;
                }

                string attributeMessage = string.Empty;

                status = uxBundleProduct.ValidateAttributes(out attributeMessage);

                if (!status)
                {
                    uxStatus.Visible = true;

                    uxStatus.Text = attributeMessage;
                   
                    // Hide AddToCart button
                    uxAddToCart.Visible = false;
                    
                    return status;
                }
            }

            if (status)
            {
                status = this.CheckInventory(this.Product);

                if (this.Product.ZNodeBundleProductCollection.Count > 0 && status)
                {
                    foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductBaseEntity _bundleProduct in this.Product.ZNodeBundleProductCollection)
                    {
                        ZNodeProduct _znodeBundleProduct = ZNodeProduct.Create(_bundleProduct.ProductID);

                        _znodeBundleProduct.SelectedSKUvalue = _bundleProduct.SelectedSKUvalue;

                        status = status && this.CheckInventory(_znodeBundleProduct);
                    }
                }
            }

            // Check for 'Call for Pricing'
            if (this.Product.CallForPricing)
            {
                lblstockmessage.Visible = false;
                uxAddToCart.Visible = false;
                uxCallForPricing.Visible = true;
                if (!string.IsNullOrEmpty(Product.CallMessage))
                {
                    uxCallForPricing.Text = Product.CallMessage;
                }
                uxStatus.Visible = false;
            }
            else
            {
                lblstockmessage.Visible = true;
                uxCallForPricing.Visible = false;
            }

            // Bind selected sku product price if one exists
            uxProductPrice.Quantity = this.ShoppingCartQuantity;
            uxProductPrice.Product = this.Product;
            uxProductPrice.Bind();

            // Set product properties       
            ProductTitle.Text = string.Format(ProductTitle.Text, this.Product.Name);
            lblProductID.Text = this.Product.ProductNum;
            ProductDescription.Text = Server.HtmlDecode(this.Product.Description);

            return status;
        }

        /// <summary>
        /// Checks the Inventory for the product
        /// </summary>
        /// <param name="product">Product Instance</param>
        /// <returns>Returns the Inventory of Product</returns>
        public bool CheckInventory(ZNodeProduct product)
        {
            lblstockmessage.CssClass = "InStockMsg";

            // Don't track Inventory - Items can always be added to the cart,TrackInventory is disabled
            if (!product.TrackInventoryInd && !product.AllowBackOrder && product.QuantityOnHand > 0)
            {
                lblstockmessage.Text = product.InStockMsg;
            }
            else if (!product.TrackInventoryInd && !product.AllowBackOrder && product.QuantityOnHand <= 0)
            {
                lblstockmessage.Text = string.Empty;
            }
            else if (product.TrackInventoryInd && !product.AllowBackOrder && product.QuantityOnHand >= this.ShoppingCartQuantity)
            {
                lblstockmessage.Text = product.InStockMsg;
            }
            else if (product.AllowBackOrder && product.QuantityOnHand >= this.ShoppingCartQuantity)
            {
                // Set Inventory stock message
                lblstockmessage.Text = product.InStockMsg;
            }
            else if (product.AllowBackOrder && product.QuantityOnHand < this.ShoppingCartQuantity)
            {
                lblstockmessage.Text = product.BackOrderMsg;
            }
            else
            {
                // Display Out of stock message
                lblstockmessage.Text = product.OutOfStockMsg;

                lblstockmessage.CssClass = "OutOfStockMsg";

                // Hide AddToCart button
                uxAddToCart.Visible = false;

                return false;
            }

            return true;
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.BindDefaultValues();

            this.BindProduct();

            // Bind reference user controls
            this.BindControls();

            uxStatus.Visible = false;

            if (uxQty.SelectedIndex != -1)
            {
                this.ShoppingCartQuantity = int.Parse(uxQty.SelectedValue);
            }
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Review button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Review_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterClientScriptBlock(this, sender.GetType(), "BuyClick", "javascript:self.parent.location ='" + ResolveUrl("~/CustomerReview.aspx?zpid= " + this.Product.ProductID) + "'", true);
        }

        /// <summary>
        /// Add to cart button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxAddToCart_Click(object sender, EventArgs e)
        {
            // Reset status
            uxStatus.Text = string.Empty;

            if (this.UpdateProductStatus())
            {
                // Create shopping cart item
                ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                item.Product = new ZNodeProductBase(this.Product);
                item.Quantity = this.ShoppingCartQuantity;

                // Add product to cart
                ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                // If shopping cart is null, create in session
                if (shoppingCart == null)
                {
                    shoppingCart = new ZNodeShoppingCart();
                    shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                }

                string link = string.Empty;

                // Add item to cart
                if (shoppingCart.AddToCart(item))
                {
                    // Update Display Order
                    ZNodeDisplayOrder displayOrder = new ZNodeDisplayOrder();
                    displayOrder.SetDisplayOrder(this.Product);

                    ZNodeSavedCart.AddToSavedCart(item);
                    link = "~/shoppingcart.aspx";
                }
                else
                {
                    // Display Out of Stock message
                    uxStatus.Visible = true;
                    uxStatus.Text = this.Product.OutOfStockMsg;
                    lblstockmessage.Visible = false;
                    return;
                }

                ScriptManager.RegisterClientScriptBlock(UpdatePanelAddToCart, sender.GetType(), "BuyClick", "javascript:self.parent.location ='" + ResolveUrl(link) + "'", true);
            }
            else
            {
                // Display Out of Stock message
                uxStatus.Visible = true;

                return;
            }
        }

        /// <summary>
        /// Triggered when addtoCart is fired on the product listing page using query string
        /// </summary>
        protected void Buy_Click()
        {
            // Reset status
            uxStatus.Text = string.Empty;

            // Check product attributes count
            if (this.Product.ZNodeAttributeTypeCollection.Count > 0)
            {
                return;
            }

            // Loop through the product addons
            foreach (ZNodeAddOn _addOn in this.Product.ZNodeAddOnCollection)
            {
                if (!_addOn.OptionalInd)
                {
                    return;
                }
            }

            // Create shopping cart item
            ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
            item.Product = new ZNodeProductBase(this.Product);
            item.Quantity = 1;

            // Add product to cart
            ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            // If shopping cart is null, create in session
            if (shoppingCart == null)
            {
                shoppingCart = new ZNodeShoppingCart();
                shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
            }

            // Add item to cart
            if (shoppingCart.AddToCart(item))
            {
                // Update SavedCart items
                ZNodeSavedCart.AddToSavedCart(item);

                string link = "~/shoppingcart.aspx";
                Response.Redirect(link);
            }
            else
            {
                // Display Out of Stock message
                uxStatus.Visible = true;
                uxStatus.Text = this.Product.OutOfStockMsg;
                return;
            }
        }

        /// <summary>
        /// Quantity Drop down list index changed event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxQty_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateProductStatus();
        }

        #endregion
        
        #region Private Methods
        /// <summary>
        /// Bind all the controls to the data
        /// </summary>
        private void BindControls()
        {
            // Review star rating        
            uxReviewRating.TotalReviews = this.Product.TotalReviews;
            uxReviewRating.ReviewRating = this.Product.ReviewRating;
            uxReviewRating.ViewProductLink = this.Product.ViewProductLink;            

            // Related products        
            uxProductRelated.Product = this.Product;
            uxProductRelated.Bind();

            // Best Sellers
            uxBestSellers.BindProducts();

            // Set visible for best sellers and related products
            pnlBestSellers.Visible = uxBestSellers.HasItems;
            pnlProductRelated.Visible = uxProductRelated.HasItems;

            if (!uxProductRelated.HasItems && !uxBestSellers.HasItems)
            {
                ProductTabs.Visible = false;
            }
            else
            {
                ProductTabs.Visible = true;
            }

            // Registers the event for the payment (child) control
            this.uxProductAttributes.SelectedIndexChanged += new EventHandler(this.UxProductAttributes_SelectedIndexChanged);
            this.uxProductAddOns.SelectedIndexChanged += new EventHandler(this.UxProductAddOns_SelectedIndexChanged);
            this.uxBundleProduct.SelectedIndexChanged += new EventHandler(this.UxBundleProduct_SelectedIndexChanged);

            // Swatches
            uxProductSwatches.ProductId = this.ProductId;
            pnlSwatches.Visible = uxProductSwatches.HasImages;

            // Highlights         
            uxHighlights.Product = this.Product;
            uxHighlights.Bind();
            if (uxHighlights.Product != null)
            {
                uxHighlights.Visible = true;
            }

            // Price
            uxProductPrice.Visible = this.profile.ShowPrice && !this.Product.CallForPricing;
            pnlQty.Visible = this.profile.ShowPrice;
        }

        /// <summary>
        /// Bind a product
        /// </summary>
        private void BindProduct()
        {
            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this.ProductId = int.Parse(Request.Params["zpid"]);
            }
            else
            {
                throw new ApplicationException("Invalid Product Id");
            }

            if (!Page.IsPostBack)
            {
                // Retrieve product data
                this.Product = ZNodeProduct.Create(this.ProductId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

                // Add to http context so it can be shared with user controls
                HttpContext.Current.Items.Add("Product", this.Product);
            }
            else
            {
                this.Product = (ZNodeProduct)HttpContext.Current.Items["Product"];

                if (this.Product == null)
                {
                    // Retrieve product data
                    this.Product = ZNodeProduct.Create(this.ProductId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                }

                // Add to http context so it can be shared with user controls
                HttpContext.Current.Items.Add("Product", this.Product);
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this.Product = (ZNodeProduct)HttpContext.Current.Items["Product"];
    
            this.BindProductData();
        }

        /// <summary>
        /// Bind Default Value Method
        /// </summary>
        private void BindDefaultValues()
        {
            // This code must be placed here for SEO Url Redirect to work and fix
            // postback problem with URL rewriting
            if (this.Page.Form != null)
            {
                this.Page.Form.Action = Request.RawUrl;
            }

            // Set button image
            uxAddToCart.Attributes.Add("onclick", "return detect()");
        }

        /// <summary>
        /// Bind Product Data 
        /// </summary>
        private void BindProductData()
        {
            // Set product properties       
            ProductTitle.Text = string.Format(ProductTitle.Text, this.Product.Name);
            lblProductID.Text = this.Product.ProductNum;
            ProductDescription.Text = Server.HtmlDecode(this.Product.Description);
   
            if (!Page.IsPostBack)
            {
                this.BindStockData();

                BindQty();
            }
        }

        public void BindQty()
        {
            // Bind the MaxQuantity value to the Dropdown list
            // If Min quantity is not set in admin, set it to 1
            int minQty = this.Product.MinQty == 0 ? 1 : this.Product.MinQty;

            // If Max quantity is not set in admin , set it to 10
            int maxQty = this.Product.MaxQty == 0 ? 10 : this.Product.MaxQty;

            ArrayList quantityList = new ArrayList();

            for (int itemIndex = minQty; itemIndex <= maxQty; itemIndex++)
            {
                quantityList.Add(itemIndex);
            }

            uxQty.DataSource = quantityList;
            uxQty.DataBind();
        }

        /// <summary>
        /// Bind all the controls to the data
        /// </summary>
        private void BindStockData()
        {

            uxProductSwatches.ProductId = this.ProductId;
            pnlSwatches.Visible = uxProductSwatches.HasImages; 

            uxProductPrice.Visible = this.profile.ShowPrice && !this.Product.CallForPricing;
            pnlQty.Visible = this.profile.ShowPrice;

            bool showCartButton = this.profile.ShowAddToCart;
            
            // If out of stock and backorder disabled then hide the AddToCart button
            if (this.Product.ZNodeAttributeTypeCollection.Count > 0)
            {
                // If product has attributes,then display the AddToCart button
                showCartButton &= true;
            }
            else if (this.Product.AllowBackOrder && this.Product.QuantityOnHand <= 0)
            {
                // Allow back Order
                showCartButton &= true;
                
                // Set back order message
                lblstockmessage.Text = this.Product.BackOrderMsg;
            }
            else if (this.Product.AllowBackOrder && this.Product.QuantityOnHand > 0)
            {
                showCartButton &= true;
                
                // Set Item In-Stock message
                lblstockmessage.Text = this.Product.InStockMsg;
            }
            else if (this.Product.QuantityOnHand > 0 && this.Product.TrackInventoryInd == true && !this.Product.AllowBackOrder)
            {
                // Track Inventory
                showCartButton &= true;
                
                // Set In stock message
                lblstockmessage.Text = this.Product.InStockMsg;
            }
            else if (this.Product.AllowBackOrder == false && this.Product.TrackInventoryInd == false && this.Product.QuantityOnHand > 0)
            {
                // Don't track Inventory
                // Items can always be added to the cart,TrackInventory is disabled
                showCartButton &= true;

                // Set In stock message
                lblstockmessage.Text = this.Product.InStockMsg;
            }
            else if (this.Product.AllowBackOrder == false && this.Product.TrackInventoryInd == false && this.Product.QuantityOnHand <= 0)
            {
                // Don't track Inventory
                // Items can always be added to the cart,TrackInventory is disabled
                showCartButton &= true;
                
                // Set OutOf Stock message
                lblstockmessage.Text = string.Empty;
            }
            else
            {
                // Set OutOf Stock message
                lblstockmessage.Text = this.Product.OutOfStockMsg;
                lblstockmessage.CssClass = "OutOfStockMsg";
                showCartButton &= false;
            }

            // Add to cart button
            if (this.Product.CallForPricing)
            {
                // If call for pricing is enabled,then hide AddToCart Button
                uxCallForPricing.Text = ZNodeCatalogManager.MessageConfig.GetMessage("ProductCallForPricing");
                if (!string.IsNullOrEmpty(Product.CallMessage))
                {
                    uxCallForPricing.Text = Product.CallMessage;
                }
                showCartButton &= false;                
                lblstockmessage.Text = string.Empty;
                uxCallForPricing.Visible = !showCartButton;
            }

            uxAddToCart.Visible = showCartButton;
        
        }
              
        /// <summary>
        /// Product Addons selected Index Changed Event raised by the Product Addon control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxProductAddOns_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateProductStatus();
        }

        /// <summary>
        /// Product Attributes selected Index Changed Event raised by the Product Attributes control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxProductAttributes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            this.Product.SelectedSKU = uxProductAttributes.SelectedSKU;

            this.UpdateProductStatus();
            BindQty();
            UpdatePanelAddToCart.Update();
            ScriptManager.RegisterClientScriptBlock(UpdatePanelAddToCart, sender.GetType(), "AttributeImage", "document.getElementById('CatalogItemImage').src='" + ResolveUrl(this.Product.MediumImageFilePath) + "';", true);
            
        }

        /// <summary>
        /// Bindle Product Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxBundleProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdatePanelAddToCart.Update();

            this.UpdateProductStatus();
        }
        #endregion
    }
}