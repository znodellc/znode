using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Navigation Cart user control class.
    /// </summary>
    public partial class Controls_Default_ShoppingCart_NavigationCart : System.Web.UI.UserControl
    {
        #region Private Variables
        private string BuyImage = "~/themes/" + ZNodeCatalogManager.Theme + "/images/buynow.gif";
        private ZNodeProfile profile = new ZNodeProfile();
        private string _Title = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the title for this control
        /// </summary>
        public string Title
        {
            get
            {
                return this._Title;
            }

            set
            {
                this._Title = value;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                ZNodeShoppingCart shoppingCart = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart.CurrentShoppingCart();

                if (shoppingCart != null)
                {
                    pnlShoppingCartNavigation.Visible = false;

                    if (shoppingCart.ShoppingCartItems.Count > 0)
                    {
                        lblTotal.Text = (shoppingCart.SubTotal - shoppingCart.Discount).ToString("c"); 
                        ShoppingCartItems.DataSource = shoppingCart.ShoppingCartItems;
                        ShoppingCartItems.DataBind();

                        pnlShoppingCartNavigation.Visible = true;
                    }
                }
            }
        }

        /// <summary>
        /// Get Image Path
        /// </summary>
        /// <returns>Returns the Image path</returns>
        private string GetImageUrl()
        {
            return "~/themes/" + ZNodeCatalogManager.Theme + "/images/view_edit_cart.png";
        }
        #endregion
    }
}