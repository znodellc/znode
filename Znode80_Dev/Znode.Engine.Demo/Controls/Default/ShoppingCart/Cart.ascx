<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_ShoppingCart_Cart" Codebehind="Cart.ascx.cs" %>
<%@ Register Src="~/Controls/Default/navigation/NavigationBreadCrumbs.ascx" TagName="StoreBreadCrumbs" TagPrefix="ZNode" %>
<%--<%@ Register Src="~/Controls/Default/common/HomeQuickSearch.ascx" TagName="QuickSearch" TagPrefix="ZNode" %>--%>
<%@ Register Src="~/Controls/Default/Common/CustomerServicePhone.ascx" TagName="CustomerServicePhoneNo" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="Spacer" TagPrefix="uc1" %>
<%@ Register Src="ShoppingCart.ascx" TagName="ShoppingCart" TagPrefix="ZNode" %>
<%@ Register Src="ProductRelated.ascx" TagName="ProductRelated" TagPrefix="ZNode" %>

<div class="ShoppingCart">
    <div class="QuickSearchfield">
        <%--<ZNode:QuickSearch ID="uxQuickSearch" runat="server"></ZNode:QuickSearch>--%>
    </div>
    <div class="BreadCrumb">
        <span class="PromoText"><ZNode:CustomMessage ID="HomeCustomMessage5"  EnableViewState="false" MessageKey="StoreSpecials" runat="server" /></span>
    </div>
    <hr class="Horizontal"/>
</div>
<!--[if lte IE 7]>
	    <div id="ie7css">
	    <![endif]-->
<asp:UpdatePanel runat="server" ID="updatePnlCart">
	<ContentTemplate>
		<div class="ShoppingCart">
			<div class="PageTitle">
				<asp:Localize ID="Localize3" meta:resourceKey="txtShoppingCart" runat="server" />
			</div>
			<div class="Error">
				<asp:Label ID="uxMsg" EnableViewState="false" runat="server" meta:resourcekey="uxMsgResource1"></asp:Label>
			</div>
			<asp:Panel ID="pnlShoppingCart" runat="server" meta:resourcekey="pnlShoppingCartResource1">
				<div class="Error">
					<asp:Label ID="uxErrorMsg" runat="server" meta:resourcekey="uxErrorMsgResource1"></asp:Label>
				</div>
				<div>
					<ZNode:ShoppingCart ID="uxCart" runat="server" />
				</div>
				<div class="CheckoutBox" style="float: right;">
					<asp:LinkButton ID="ContinueShopping1" runat="server" AlternateText="Continue Shopping"
						OnClick="ContinueShopping1_Click" ValidationGroup="groupCart" CssClass="Button GrayButton"
						meta:resourcekey="ContinueShopping1Resource1" CausesValidation="false">Continue Shopping</asp:LinkButton>
					<asp:LinkButton ID="Checkout1" runat="server" AlternateText="Check Out" OnClick="Checkout_Click"
						ValidationGroup="groupCart" CssClass="checkout" meta:resourcekey="Checkout1Resource1">Proceed to Checkout</asp:LinkButton>
					<br />
				</div>
			</asp:Panel>
		</div>
	</ContentTemplate>
</asp:UpdatePanel>
<div style="clear:both;"></div>
<div style="display: none;"><ZNode:ProductRelated ID="uxProductRelatedItems" runat="server" /></div>
<uc1:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
<div class="ShoppingCart">
    <div class="ShippingText">
        <ZNode:CustomMessage ID="CustomMessage1" EnableViewState="false" MessageKey="ShoppingCartShippingText" runat="server" />
    </div>
</div>
<!--[if lte IE 7]>
	     </div>
	     <![endif]-->