<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_ShoppingCart_ProductRelated" Codebehind="ProductRelated.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>

<asp:Panel ID="pnlRelatedProduct" runat="server" Visible="false">
 
 <script language="javascript" type="text/javascript">
 		var prm = Sys.WebForms.PageRequestManager.getInstance();
		prm.add_pageLoaded(PageLoadedEventHandler);

		//
		function PageLoadedEventHandler() {
		 window.addEvents({
			 'domready': function() {
				 /* thumbnails example , div containers */
				 new SlideItMoo({
				 overallContainer: 'Cart_CrossSellItem_outer',
				 elementScrolled: 'Cart_CrossSellItem_inner',
				 thumbsContainer: 'Cart_CrossSellItem_items',
					 itemsVisible: 5,
					 elemsSlide: 1,
					 duration: 500,
					 itemsSelector: '.Cart_CrossSellItem_element',
					 showControls: 1
				 });
			 }
		 });
     }
	</script>  

	<div id="CartItemRelatedProducts">     
		<div class="Title"><uc1:CustomMessage ID="RelatedProductTitle" EnableViewState="<%# EnableViewstate %>" MessageKey="ProductRelatedProductsTitle" runat="server"></uc1:CustomMessage></div>
		<div id="Cart_CrossSellItem_outer">
		<div id="Cart_CrossSellItem_inner">
		<div id="Cart_CrossSellItem_items">	
			<asp:DataList ID="RelatedProductList" runat="server" EnableViewState="<%# EnableViewstate %>" RepeatLayout="Flow">
				<ItemTemplate>
					<div class="Cart_CrossSellItem_element">
						<div class="CartRelatedItem">
							<div class="DetailLink">
								<asp:HyperLink ID="hlName" Runat="server" CssClass='DetailLink' Text='<%# DataBinder.Eval(Container.DataItem, "Name").ToString()%>' NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()%>'></asp:HyperLink>
								- <span class="Price"><asp:Label ID="Label1" EnableViewState="<%# EnableViewstate %>" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>' Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && RelatedProductProfile.ShowPrice %>'></asp:Label>
							</div>
							<div class="ShortDescription"><asp:Label EnableViewState="<%# EnableViewstate %>" ID="ShortDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShortDescription").ToString() %>' /></div>                       
							<div class="Image">                    
								<a id="A1" enableviewstate="<%# EnableViewstate %>" href='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()%>' runat="server">
									<img id="img1" enableviewstate="<%# EnableViewstate %>" name='<%# "img"+DataBinder.Eval(Container.DataItem, "ProductID")%>' border='0' alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' src='<%#  GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>' runat="server" />
								</a>            
							</div>								
							<div class="CallForPrice"><asp:Label  EnableViewState="<%# EnableViewstate %>" ID="uxCallForPricing" runat="server" Text='<%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing")) %>' CssClass="Price"  /></div>
							<div class="StarRating"><ZNode:ProductAverageRating ID="uxProductAverageRating" TotalReviews='<%# DataBinder.Eval(Container.DataItem, "TotalReviews") %>'  ViewProductLink= '<%# DataBinder.Eval(Container.DataItem, "ViewProductLink")%>' ReviewRating ='<%# DataBinder.Eval(Container.DataItem, "ReviewRating")%>' runat="server" /></div>							
							<asp:HyperLink ID="HyperLink1" CssClass="Button View" 
                                        EnableViewState="<%# EnableViewstate %>"
                                        meta:resourcekey="btnbuyResource1"
                                        runat="server" 
                                        Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && RelatedProductProfile.ShowAddToCart %>' 
                                        NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink")%>'>View �</asp:HyperLink>
						 </div>
					</div>
				</ItemTemplate>     
				<ItemStyle CssClass="ItemStyle" VerticalAlign="Top" />				   
			</asp:DataList>
		</div>
		</div>
		</div>
	 </div>
 
</asp:Panel>