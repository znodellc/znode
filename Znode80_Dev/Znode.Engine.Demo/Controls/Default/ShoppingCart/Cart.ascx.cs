using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Cart user control class.
    /// </summary>
    public partial class Controls_Default_ShoppingCart_Cart : System.Web.UI.UserControl
    {
        #region Member Variables
        private ZNodeShoppingCart shoppingCart;
        private ZNodeUserAccount userAccount;
		private TList<PaymentSetting> _pmtSetting;
        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            if (!Page.IsPostBack)
            {
                PaymentSettingService _pmtServ = new PaymentSettingService();
				_pmtSetting = _pmtServ.GetAll();

                int profileID = 0;

                // Check user Session
                if (this.userAccount != null)
                {
                    profileID = this.userAccount.ProfileID;
                }
                else
                {
                    // Get Default Registered profileId
                    profileID = ZNodeProfile.DefaultRegisteredProfileId;
                }

                List<int> portalIds = this.GetShoppingCartVendors();
                
            }

            // Registers the event for the shopping cart (child) control.
            this.uxCart.CartItem_RemoveLinkClicked += new EventHandler(this.CartItem_RemoveLinkClicked);

            if (this.Page.Title != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtShoppingCart.Text");
            }

            // Hide the continue shopping button if request comes from mobile service.
            if (Session["cs"] != null)
            {
                ContinueShopping1.Visible = false;
            }
        }

        /// <summary>
        /// On Pre Render Method
        /// </summary>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            if (this.userAccount == null)
            {
                this.userAccount = ZNodeUserAccount.CurrentAccount();
            }

            if (this.shoppingCart != null)
            {
                this.Bind();
            }
            else
            {
                pnlShoppingCart.Visible = false;
                uxProductRelatedItems.Visible = false;
                uxMsg.Text = this.GetLocalResourceObject("CartEmpty").ToString();
            }

            if (!this.IsPostBack && System.Web.HttpContext.Current.Session["IsCartChanged"] != null)
            {
                // We have added the contents of cart from existing user's saved cart, so alert the user.
                uxMsg.Visible = true;                
                uxMsg.Text = this.GetLocalResourceObject("SavedCartText").ToString(); 
                System.Web.HttpContext.Current.Session.Remove("IsCartChanged");
            }
            if (!this.IsPostBack && System.Web.HttpContext.Current.Session["RedirectToCart"] != null)
            {
                // We have added the contents of cart from existing user's saved cart, so alert the user.
                uxMsg.Visible = true;
                uxMsg.Text = "Please check to confirm that all items and quantities are correct.";
                System.Web.HttpContext.Current.Session.Remove("RedirectToCart");
            }

        }

        /// <summary>
        /// Checkout button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Checkout_Click(object sender, EventArgs e)
        {
            ZNodeShoppingCart ShoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            if (ShoppingCart == null)
            {
                uxMsg.Text = this.GetLocalResourceObject("AddItemsToCart").ToString();
                return;
            }
            else
            {
              
                ZNodeUrl url = new ZNodeUrl();
                string link = "~/Checkout.aspx";

				if (!HttpContext.Current.User.Identity.IsAuthenticated)
				{
					link = "~/Login.aspx?ReturnURL=Checkout.aspx";
				}
				else
				{
					Session["IsAddressValidated"] = ZNodeUserAccount.CurrentAccount().CheckoutAddress();
				}
                if (uxCart.CouponCode.Length > 0)
                {
                    ShoppingCart.AddCouponCode(uxCart.CouponCode.Trim());
                }

				shoppingCart.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().ToList().ForEach(x => shoppingCart.LoadShipmentData(x));

                // Redirect to checkout page
                Response.Redirect(link);
            }
        }
        
        /// <summary>
        /// Event is raised when Continue Shopping button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ContinueShopping1_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/");
        }

        /// <summary>
        /// Fired when child Control Remove link triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CartItem_RemoveLinkClicked(object sender, EventArgs e)
        {
            string cartCount = "0";

            // Re-bind related items
            uxProductRelatedItems.Bind();

            ZNodeShoppingCart ShoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);

            if (ShoppingCart != null)
            {
                cartCount = ShoppingCart.ShoppingCartItems.Count.ToString();
            }
            else
            {
                cartCount = "0";
            }

            UserControl cartItemCount = (UserControl)this.Page.Master.Master.FindControl("CART_ITEM_COUNT");
            Label lblCartCount = (Label)cartItemCount.FindControl("lblCartItemCount");
            lblCartCount.Text = cartCount.ToString();
        }
       
        #endregion

        #region Methods
        /// <summary>
        /// Bind All Controls
        /// </summary>
        protected void Bind()
        {
            // Show/hide cart
            if (this.shoppingCart.Count > 0)
            {
                pnlShoppingCart.Visible = true;
                uxMsg.Text = string.Empty;
            }
            else
            {
                pnlShoppingCart.Visible = false;
                uxMsg.Text = this.GetLocalResourceObject("CartEmpty").ToString();
            }

            // Bind grid
            uxCart.Bind();
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Gets the Shopping Cart Vendors list
        /// </summary>
        /// <returns>Returns the Shopping Cart vendor list</returns>
        private System.Collections.Generic.List<int> GetShoppingCartVendors()
        {
            System.Collections.Generic.List<int> list = new System.Collections.Generic.List<int>();

            if (this.shoppingCart != null)
            {
                foreach (ZNodeShoppingCartItem cartItem in this.shoppingCart.ShoppingCartItems)
                {
                    int portalId = 0;
                    if (cartItem.Product.PortalID > 0)
                    {
                        portalId = cartItem.Product.PortalID;
                    }

                    if (!list.Contains(portalId))
                    {
                        list.Add(portalId);
                    }
                }
            }

            return list;
        }

        /// <summary>
        /// Returns an instance of the PaymentSetting  class.
        /// </summary>
        /// <param name="paymentTypeID">Payment Type ID</param>
        /// <returns>Returns the payment setting instance</returns>
        private PaymentSetting GetByPaymentTypeId(int paymentTypeID)
        {
            int? profileID = 0;
            if (this.userAccount != null)
            {
                // Get loggedIn user profile
                profileID = this.userAccount.ProfileID; 
            }

			TList<PaymentSetting> list = _pmtSetting.FindAll(x => x.PaymentTypeID == paymentTypeID);
            list.Filter = "ActiveInd = true AND ProfileID = " + profileID;

            if (list.Count == 0)
            {
                // Get All Profiles payment setting
                list.Filter = "ActiveInd = true";
            }

            return list[0];
        }
        #endregion
    }
}