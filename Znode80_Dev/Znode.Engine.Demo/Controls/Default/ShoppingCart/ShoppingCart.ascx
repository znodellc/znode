<%@ Control Language="C#" AutoEventWireup="true"
    Inherits="Znode.Engine.Demo.Controls_Default_ShoppingCart_ShoppingCart" Codebehind="ShoppingCart.ascx.cs" %>
<%@ Register Src="../CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>

<div>
    <asp:GridView class="TableContainer" ID="uxCart" runat="server" AutoGenerateColumns="False"
        EmptyDataText="Shopping Cart Empty" GridLines="None" CellPadding="4" OnRowCommand="Cart_RowCommand"
        OnRowDataBound="UxCart_RowDataBound" CssClass="Grid" meta:resourcekey="uxCartResource1">
        <Columns>
            <asp:TemplateField HeaderText="REMOVE ITEM" HeaderStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource1">
                <ItemTemplate>
                    <asp:ImageButton ID="ibRemoveLineItem" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "GUID").ToString() %>'
                        CommandName="remove" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("RemoveLineItem.png") %>'
                        ToolTip="Remove this item"  meta:resourcekey="ibRemoveLineItemResource1" />
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle CssClass="RemoveItem" Wrap="True" />
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource2">
                <ItemTemplate>
                    <a enableviewstate="false" id="A1" href='<%# DataBinder.Eval(Container.DataItem, "Product.ViewProductLink").ToString() %>'
                        runat="server">
                        <img id="Img1" enableviewstate="false" alt='<%# DataBinder.Eval(Container.DataItem, "Product.ImageAltTag") %>'
                            border='0' src='<%# DataBinder.Eval(Container.DataItem, "Product.ThumbnailImageFilePath") %>'
                            runat="server" />
                    </a>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ITEM" HeaderStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource3">
                <ItemTemplate>
                    <div>
                        <a enableviewstate="false" id="A2" href='<%# DataBinder.Eval(Container.DataItem, "Product.ViewProductLink").ToString() %>'
                            runat="server">
                            <%# DataBinder.Eval(Container.DataItem, "Product.Name").ToString()%></a>
                    </div>
                    <div class="Description" enableviewstate="false">
                        Item#
                        <%# DataBinder.Eval(Container.DataItem, "Product.ProductNum").ToString()%><br>
                        <%# DataBinder.Eval(Container.DataItem, "Product.ShoppingCartDescription").ToString()%></div>
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="QUANTITY" HeaderStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource4">
                <ItemTemplate>
                    <asp:DropDownList ID="uxQty" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Quantity_SelectedIndexChanged"
                        ValidationGroup="groupCart" CausesValidation="True" CssClass="Quantity" meta:resourcekey="uxQtyResource1">
                    </asp:DropDownList>
                    <asp:RangeValidator ValidationGroup="groupCart" ID="RangeValidator1" MinimumValue="0"
                        ControlToValidate="uxQty" MaximumValue='<%# CheckInventory(DataBinder.Eval(Container.DataItem, "GUID").ToString()) %>'
                        Enabled='<%# EnableStockValidator(DataBinder.Eval(Container.DataItem, "GUID").ToString()) %>'
                        SetFocusOnError="True" runat="server" Type="Integer" Display="Dynamic" meta:resourcekey="RangeValidator1Resource1"></asp:RangeValidator>
					<asp:RangeValidator ValidationGroup="groupCart" ID="RangeValidator2" MinimumValue="0"
                        ControlToValidate="uxQty" MaximumValue='<%# CheckAddOnInventory(DataBinder.Eval(Container.DataItem, "GUID").ToString()) %>'
                        Enabled='<%# EnableAddOnStockvalidator(DataBinder.Eval(Container.DataItem, "GUID").ToString()) %>'
                        SetFocusOnError="True" runat="server" Type="Integer" Display="Dynamic" meta:resourcekey="RangeValidator1Resource2"></asp:RangeValidator>
                    <asp:HiddenField ID="GUID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "GUID").ToString() %>' />
                </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="PRICE" HeaderStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource5">
                <ItemTemplate>
                      <div class="Description" >
                    <%# DataBinder.Eval(Container.DataItem, "UnitPrice","{0:c}") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()%>
               </div> 
                           </ItemTemplate>
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="TOTAL" HeaderStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource6">
                <ItemTemplate>
                     <div class="Description" >
                    <%# DataBinder.Eval(Container.DataItem, "ExtendedPrice","{0:c}") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()%>
                </div> 
                          </ItemTemplate>
                <ItemStyle HorizontalAlign="Right" />
                <HeaderStyle HorizontalAlign="Right" />
            </asp:TemplateField>
        </Columns>
        <FooterStyle CssClass="Footer" />
        <RowStyle CssClass="Row" />
        <HeaderStyle CssClass="Header" />
        <AlternatingRowStyle CssClass="AlternatingRow" />
    </asp:GridView>
</div>
<!--[if lte IE 7]>
	    <div id="ie7css">
	    <![endif]-->
<div class="Form">
    <div class="Row Clear">
        <div align="right">
            <asp:Label ID="PaymentErrorMsg" CssClass="Error" runat="server" Visible="False" meta:resourcekey="PaymentErrorMsgResource1" />
            <div>
                <asp:Label ID="lblErrorMessage" CssClass="Error" runat="server" ForeColor="Green"
                    meta:resourcekey="lblErrorMessageResource1"></asp:Label></div>
        </div>
    </div>
    <div class="Row Clear">
        <div class="TotalBox" align="right" style="width: 100%;">
            <div class="LeftContent CustomMessage" style="width: 50%; text-align: left;">
                <ZNode:CustomMessage ID="CustomMessage2" MessageKey="ShoppingCartFooterText" runat="server" />
            </div>
            <div class="Row Clear" style="float: right; margin-top: -10px;">
                <div class="ShoppingTotalContent">
                    <asp:Localize ID="Localize3" meta:resourceKey="txtSubtotal" runat="server" />
                </div>
                <div class="ShoppingTotalContent">
                    <asp:Label ID="SubTotal" EnableViewState="false" runat="server" meta:resourcekey="SubTotalResource1"></asp:Label>
                </div>
            </div>
            <div class="Row Clear SubTotal" id="tblRowDiscount" style="float: right;" runat="server" visible="true">
                <div class="ShoppingTotalContent">
                    <asp:Localize ID="Localize1" meta:resourceKey="txtDiscount" runat="server" />
                </div>
                <div class="ShoppingTotalContent">
                    <asp:Label ID="DiscountDisplay" EnableViewState="false" runat="server" meta:resourcekey="DiscountDisplayResource1"></asp:Label>
                </div>
            </div>
            <div class="Row Clear" id="tblRowTax" runat="server" visible="false" style="float: right;">
                <div class="ShoppingTotalContent">
                    <asp:Localize ID="Localize2" meta:resourceKey="txtTax" runat="server" />
                    <asp:Label ID="TaxPct" runat="server" EnableViewState="False" meta:resourcekey="TaxPctResource1"></asp:Label>
                </div>
                <div class="ShoppingTotalContent">
                    <asp:Label ID="Tax" runat="server" meta:resourcekey="TaxResource1"></asp:Label>
                </div>
            </div>
            <div class="Row Clear" id="tblRowShipping" runat="server" visible="false"
                style="float: right;">
                <div class="ShoppingTotalContent">
                    <asp:Localize ID="Localize4" meta:resourceKey="txtShipping" runat="server" />
                </div>
                <div class="ShoppingTotalContent">
                    <asp:Label ID="Shipping" runat="server"></asp:Label>
                </div>
            </div>
            <div class="Row Clear" style="float: right;">
                <div class="ShoppingTotalAmountContent">
                    <b>
                        <asp:Localize ID="Localize6" meta:resourceKey="txtTotal" runat="server" /></b>
                </div>
                <div class="ShoppingTotalAmountContent">
                    <b>
                        <asp:Label ID="Total" EnableViewState="false" runat="server" meta:resourcekey="TotalResource2"></asp:Label></b>
                </div>
            </div>
            <asp:PlaceHolder runat="server" ID="PromotionPanel">
                <div class="Clear PromotionText">
                    <asp:Label ID="lblPromoMessage" CssClass="Error" runat="server" meta:resourcekey="lblPromoMessageResource1"></asp:Label>
                </div>
                <div class="Clear CouponContent">
                    <asp:Panel ID="pnlCouponCode" runat="server" DefaultButton ="ApplyCouponGo">
                        <b>
                            <asp:Localize ID="Localize5" meta:resourceKey="txtEnterCouponCode" runat="server" /></b>&nbsp;
                    <asp:TextBox runat="server" ID="ecoupon" meta:resourcekey="ecouponResource1"></asp:TextBox>&nbsp;
						<asp:Button ID="ApplyCouponGo" runat="server" AlternateText="Apply" OnClick="Btnapply_click" Text="Apply"
							ValidationGroup="groupCart1" CssClass="button" meta:resourcekey="ApplyCouponGoResource1"></asp:Button>
                    </asp:Panel>
                </div>
            </asp:PlaceHolder>
        </div>
    </div>
</div>
<!--[if lte IE 7]>
	     </div>
	     <![endif]-->

