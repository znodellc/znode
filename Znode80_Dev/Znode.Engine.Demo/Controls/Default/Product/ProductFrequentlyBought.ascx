﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductFrequentlyBought.ascx.cs" Inherits="Znode.Engine.Demo.Controls.Default.Product.ProductFrequentlyBought" %>
<div id="Add-Ons">
	<asp:Panel ID="pnlAddOns" runat="server">
		<asp:PlaceHolder ID="ControlsPlaceHolder" runat="server"></asp:PlaceHolder>	
	</asp:Panel>
</div>

<asp:UpdatePanel runat="server" ID="productFrequentlyBought" UpdateMode="Conditional">
	<ContentTemplate>
<asp:Panel ID="pnlRelated" runat="server" Visible="true">
    <%--    <script language="javascript" type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(PageLoadedEventHandler);

        //
        function PageLoadedEventHandler() {
            var crossSellItems;
            window.addEvents({
                'domready': function () {
                    /* thumbnails example , div containers */
                    crossSellItems = new SlideItMoo({
                        overallContainer: 'CrossSellItems_outer',
                        elementScrolled: 'CrossSellItems_inner',
                        thumbsContainer: 'CrossSellItems',
                        itemsVisible: 2,
                        elemsSlide: 1,
                        duration: 300,
                        itemsSelector: '.CrossSellItem',
                        showControls: 1
                    });
                }
            });
        }
    </script>--%>

    <%--<h5><uc1:CustomMessage ID ="RelatedProductTitle" MessageKey="ProductRelatedProductsTitle" runat="server"></uc1:CustomMessage></h5>--%>
    <%--    <div class="CrossSellItem">
        <div id="CrossSellItems_outer">
            <div id="CrossSellItems_inner">
                <div id="CrossSellItems">
                </div>
            </div>
        </div>
    </div>--%>
    <h3>Frequently Bought Products</h3>

    <div class="StockMsg">
        <asp:Label ID="lblstockmessage" runat="server" CssClass="InStockMsg" meta:resourcekey="lblstockmessageResource1"></asp:Label>
    </div>
    <div class="product-wrapper">
        <asp:Label ID="uxStatus" EnableViewState="False" runat="server" CssClass="OutOfStockMsg" meta:resourcekey="uxStatusResource1"></asp:Label>
        <asp:DataList ID="DataListRelated" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" Font-Bold="False" 
            Font-Italic="False" Font-Overline="False" Font-Strikeout="False" Font-Underline="False" HorizontalAlign="Left" EnableViewState="False">
            <ItemTemplate>
                <div class="CrossSellItem">
                    <div class="Image">
                        <a id="A3" href='<%# GetViewProductPageUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>' runat="server">
                            <img id="Img2" alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' visible='true' src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>' border="0" runat='server'  />
                        </a>
                    </div>
                    <div class="DetailLink">
                        <a id="A1" href='<%# GetViewProductPageUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>'
                            visible='True' runat="server"><%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a>
                        <asp:HiddenField runat="server" ID="dlHiddenField" Value='<%# DataBinder.Eval(Container.DataItem, "ProductId").ToString()%>' />
                        <asp:CheckBox runat="server" ID="chkFrequentlyBought" Checked="True" />

                    </div>
                    <div class="NamePriceDetail">
                        <span class="Name">
                            <asp:Label ID="lblPrice" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice").ToString()%>'></asp:Label>
                        </span>
                    </div>
                    <br/>
                   <div>
						 <span class="plus">
							<asp:Literal runat="server" ID ="ltPlus">+</asp:Literal>
						</span>
					</div>
                </div>
				
            </ItemTemplate>
        </asp:DataList>
    </div>
    <div>
       <asp:Button runat="server" ID="btnAddToCart" Text="Add to Cart" OnClick="BtnAddToCartClick"  CssClass="button"  />
        <asp:Button runat="server" ID="btnAddToWishList" Text="Add to Wishlist" OnClick="BtnAddToWishListClick"  CssClass="button" /> 
    </div>
</asp:Panel>
<asp:Label runat="server" ID="lblproductId"></asp:Label>
		</ContentTemplate>
	</asp:UpdatePanel>