<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Product_ProductTabs" Codebehind="ProductTabs.ascx.cs" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductReviews.ascx" TagName="ProductReviews"
    TagPrefix="ZNode" %>
<%@ Register Src="ProductRelated.ascx" TagName="ProductRelated" TagPrefix="uc8" %>
<%@ Register Src="ProductAdditionalImages.ascx" TagName="ProductAdditionalImages"
    TagPrefix="uc9" %>
<div id="Tab">
    <ajaxToolKit:TabContainer runat="server" ID="ProductTabs" CssClass="CustomTabStyle"
        ScrollBars="Vertical" ActiveTabIndex="0" meta:resourcekey="ProductTabsResource1">
        <ajaxToolKit:TabPanel ID="pnlFeatures" runat="server">
            <HeaderTemplate>
                <asp:Localize ID="lblFeatures" runat="server" meta:resourcekey="pnlFeatures"></asp:Localize>
            </HeaderTemplate>
            <ContentTemplate>
                <div class="Features">
                    <asp:Label ID="ProductFeatureDesc" EnableViewState="false" runat="server"></asp:Label></div>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
        <ajaxToolKit:TabPanel ID="pnlCustomerReviews" runat="server">
            <HeaderTemplate>
                <asp:Localize ID="lblCustomerReviews" runat="server" meta:resourcekey="pnlCustomerReviews"></asp:Localize>
            </HeaderTemplate>
            <ContentTemplate>
                <div class="Reviews">
                    <ZNode:ProductReviews ID="uxProductReviews" runat="server" />
                </div>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
        <ajaxToolKit:TabPanel ID="pnlAdditionalInformation" runat="server">
            <HeaderTemplate>
                <asp:Localize ID="lblAdditionalInformation" runat="server" meta:resourcekey="pnlAdditionalInformation"></asp:Localize>
            </HeaderTemplate>
            <ContentTemplate>
                <div class="ShippingInfo">
                    <asp:Label ID="ProductAdditionalInformation" EnableViewState="false" runat="server" meta:resourcekey="ProductAdditionalInformationResource1"></asp:Label></div>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
    </ajaxToolKit:TabContainer>
</div>
