using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Product Zoom user control class.
    /// </summary>
    public partial class Controls_Default_Product_ProductZoom : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct _product;
        private int _productId;
        private string _ViewProductLink = string.Empty;
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this._productId = int.Parse(Request.Params["zpid"]);
            }
            else
            {
                throw new ApplicationException("Invalid Product Id");
            }

            if (!Page.IsPostBack)
            {
                 this._product = (ZNodeProduct)HttpContext.Current.Items["Product"];

                 if (this._product == null)
                 {
                     // Retrieve product data
                     this._product = ZNodeProduct.Create(this._productId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                     // Add to http context so it can be shared with user controls
                     HttpContext.Current.Items.Add("Product", this._product);
                 }
               

                // Set button image
                uxAddToCart.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/view_addtoCart.jpg";
                uxView.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/view_details.gif";
            }
            else
            {
                this._product = (ZNodeProduct)HttpContext.Current.Items["Product"];

                if (this._product == null)
                {
                    // Retrieve product data
                    this._product = ZNodeProduct.Create(this._productId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                    // Add to http context so it can be shared with user controls
                    HttpContext.Current.Items.Add("Product", this._product);
                }

               
            }

            if (this._product != null)
            {
                CatalogItemImage.ImageUrl = this._product.MediumImageFilePath;
                this._ViewProductLink = "javascript:self.parent.location='" + ResolveUrl(this._product.ViewProductLink) + "'";

                uxView.Attributes.Add("onclick", this._ViewProductLink);
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Add to cart button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxAddToCart_Click(object sender, ImageClickEventArgs e)
        {
            string pageLink = "~/buy.aspx?product_num=" + this._product.ProductNum;

            bool hasAttributes = false;

            // Check product attributes count
            hasAttributes = this._product.ZNodeAttributeTypeCollection.Count > 0;

            if (!hasAttributes)
            {
                hasAttributes &= this._product.QuantityOnHand > 0;
            }

            // Loop through the product addons
            foreach (ZNodeAddOn _addOn in this._product.ZNodeAddOnCollection)
            {
                if (!_addOn.OptionalInd)
                {
                    hasAttributes &= true;
                    break;
                }
            }

            if (hasAttributes)
            {
                pageLink = this._product.ViewProductLink;
            }

            Page.ClientScript.RegisterClientScriptBlock(sender.GetType(), "BuyClick", "javascript:self.parent.location ='" + ResolveUrl(pageLink) + "'", true);
        }
        #endregion
    }
}