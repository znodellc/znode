using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the ProductTabs user control class.
    /// </summary>
    public partial class Controls_Default_Product_ProductTabs : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct _product = new ZNodeProduct();
        private string Mode = string.Empty;
        #endregion

        #region Bind Method
        /// <summary>
        /// Represents the Bind Method
        /// </summary>
        public void Bind()
        {
            ProductAdditionalInformation.Text = Server.HtmlDecode(this._product.AdditionalInformation);
            ProductFeatureDesc.Text = Server.HtmlDecode(this._product.FeaturesDesc);

            int counter = 0;

            // This should be retrieved from the store settings in the future
            bool ShowTabsOnlyIfDataPresent = true;

            // Show tabs only if they contain any information
            if (ShowTabsOnlyIfDataPresent)
            {
                ProductTabs.Tabs[0].Visible = true;
                ProductTabs.Tabs[1].Visible = true;
                ProductTabs.Tabs[2].Visible = true;

                if (this._product.FeaturesDesc.Length == 0)
                {
                    ProductTabs.Tabs[0].Visible = false;
                    ProductTabs.Tabs[0].HeaderText = string.Empty;
                    counter++;
                }

                if (this._product.AdditionalInformation.Length == 0)
                {
                    ProductTabs.Tabs[2].Visible = false;
                    ProductTabs.Tabs[2].HeaderText = string.Empty;
                    counter++;
                }

                if (!this.Mode.Equals(string.Empty))
                {
                    if (counter == 0)
                    {
                        counter = 2;
                    }
                    else if (counter == 2)
                    {
                        counter = 1;
                    }
                    else if (counter == 1)
                    {
                        counter = 2;
                    }

                    ProductTabs.ActiveTabIndex = counter - 1;
                }
            }
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get product id from querystring  
            if (Request.Params["mode"] != null)
            {
                this.Mode = Request.Params["mode"];
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            this.Bind();
        }

        #endregion
    }
}