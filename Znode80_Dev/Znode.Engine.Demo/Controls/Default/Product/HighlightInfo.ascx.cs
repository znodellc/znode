using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the HighlightInfo user control class.
    /// </summary>
    public partial class Controls_Default_Product_HighlightInfo : System.Web.UI.UserControl
    {
        #region Protected Variables
        private int ItemId = 0;
        private int ProductId = 0;
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind the highlight data
        /// </summary>
        public void Bind()
        {
            ZNodeHighlight highlight = ZNodeHighlight.Create(this.ItemId);

            if (highlight != null)
            {
                lblTitle.Text = highlight.Name;
                HighlightDescription.Text = highlight.Description;
            }

            back.Text = this.GetLocalResourceObject("Back").ToString();
            back.NavigateUrl = "~/product.aspx?zpid=" + this.ProductId;
        }
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["zpid"] != null)
            {
                this.ProductId = int.Parse(Request.Params["zpid"].ToString());
            }

            if (Request.Params["highlightid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["highlightid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.Bind();
            }
        }
        #endregion
    }
}