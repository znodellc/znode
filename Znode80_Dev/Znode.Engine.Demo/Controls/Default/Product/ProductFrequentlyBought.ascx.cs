﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
namespace Znode.Engine.Demo.Controls.Default.Product
{
    public partial class ProductFrequentlyBought : System.Web.UI.UserControl
    {
        #region Public Properties

        /// <summary>
        /// Gets or sets the catalog item object. The control will bind to the data from this object
        /// </summary>
        public ZNodeProductBase Product { get; set; }
        #endregion

        #region Private Variables
        private StringBuilder stringBuilder = new StringBuilder();
        private string OutOfStock = string.Empty;
        #endregion

        #region Bind Data

        /// <summary>
	    /// Bind control display based on properties set
	    /// </summary>
	    public void Bind()
	    {
		    pnlAddOns.Visible = false;
		    var productAdmin = new ProductAdmin();
		    // If there is more than one record in the ZNodeCrossSellItemCollection where relation type is of FBT
		    if (Product.ZNodeCrossSellItemCollection.Count > 0)
		    {
			    var mainProduct = new ZNodeCrossSellItem
				    {
					    Name = Product.Name,
					    ImageAltTag = Product.ImageAltTag,
					    ImageFile = Product.ImageFile,
					    ProductId = Product.ProductID,
					    CallForPricing = false,
					    RetailPrice = Product.RetailPrice,
					    SalePrice = Product.SalePrice,
					    WholeSalePrice = Product.WholesalePrice
				    };

			    // Add main product to the list and only 2 of the frequently bought items. 
			    // Filter the CrossSellSelection so that they are only frequently bought together items.


			    var filteredFBT = new ZNodeGenericCollection<ZNodeCrossSellItem> {mainProduct};

			    foreach (
				    var csi in
					    Product.ZNodeCrossSellItemCollection.OfType<ZNodeCrossSellItem>()
					           .Where(csi => csi.RelationTypeId == (int) RelationType.FBT))
			    {
				    //Get Retail & Sales price for FBT product
				    var product = ZNodeProduct.Create(csi.ProductId, true);

				    // check for null in case if the product is disbaled 
				    if (product != null)
				    {
					    csi.RetailPrice = product.RetailPrice;

					    if (ZNodeUserAccount.CurrentAccount() == null)
					    {
						    csi.SalePrice = product.SalePrice;
						    //csi.WholeSalePrice = product.WholesalePrice;
					    }
					    else
					    {
						    if (product.UseWholeSalePrice && product.WholesalePrice.HasValue)
						    {
							    csi.SalePrice = product.WholesalePrice.HasValue
								                    ? product.WholesalePrice.Value
								                    : product.SalePrice.Value;
						    }
						    else
						    {
							    csi.SalePrice = product.SalePrice;
						    }
					    }

					    //Add to CrossSellItem collection
					    filteredFBT.Add(csi);

					    if (csi.AllowBackOrder && csi.QuantityOnHand <= 0)
					    {
						    lblstockmessage.Text = csi.BackOrderMsg;
					    }
				    }
			    }

			    if (Product.AllowBackOrder && Product.QuantityOnHand <= 0)
			    {
				    lblstockmessage.Text = Product.BackOrderMsg;
			    }

			    DataListRelated.DataSource = filteredFBT;
			    DataListRelated.DataBind();

			    var count = DataListRelated.Items.Count;
			    var ltPlus = (Literal) DataListRelated.Items[count - 1].FindControl("ltPlus");
			    ltPlus.Visible = false;
		    }
		    else
			    pnlRelated.Visible = false;
	    }

	    #endregion

        #region Helper Functions
		/// <summary>
		/// Get ImagePath
		/// </summary>
		/// <param name="imageFile">Image File Name</param>
		/// <returns>Returns the Image Path</returns>
		public string GetImagePath(string imageFile)
		{
			var znodeImage = new ZNodeImage();
			return znodeImage.GetImageHttpPathCrossSell(imageFile);
		}

        /// <summary>
        ///  Represents the GetViewProductImageUrl method
        /// </summary>
        /// <param name="link">Represents the Link Value</param>
        /// <returns>Returns the Path of Product Page Url</returns>
        protected string GetViewProductPageUrl(string link)
        {
            return "javascript:self.parent.location ='" + ResolveUrl(link) + "';";
        }

		/// <summary>
		/// Binds textbox control within placeholder control
		/// </summary>
		/// <param name="addOn"></param>
		protected void BindTextBoxControl(ZNodeAddOn addOn)
		{
			// Don't display list box if there is no add-on values for AddOns
			if (addOn.AddOnID > 0)
			{
				foreach (ZNodeAddOnValue addOnValue in addOn.ZNodeAddOnValueCollection)
				{
					ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='Option'>"));

					var txtBox = new TextBox
					{
						ID = "txtBoxAddOn" + addOnValue.AddOnValueID,
						CssClass = "AddOnTextBox",
						Text = addOnValue.Description,
						AutoPostBack = true
					};

					//txtBox.TextChanged += new EventHandler(this.Controls_SelectedIndexChanged);

					// Add controls to the place holder
					var lit1 = new Literal();
					lit1.Text += "<table class=\"Option\">";

					ControlsPlaceHolder.Controls.Add(lit1);

					var validator = new RegularExpressionValidator
					{
						CssClass = "AddOnValidator",
						ControlToValidate = txtBox.ID,
						ErrorMessage = "Invalid",
						Display = ValidatorDisplay.Dynamic,
						ValidationExpression = @"^[a-zA-Z0-9_.@\s]+$"
					};

					ControlsPlaceHolder.Controls.Add(validator);
					var ltrl = new Literal { Text = "<tr><td class='OptionLabel'>" + addOnValue.Name + " :</td>" };

					ControlsPlaceHolder.Controls.Add(ltrl);

					var lt = new Literal { Text = "<td class='OptionValue'>" };

					ControlsPlaceHolder.Controls.Add(lt);
					ControlsPlaceHolder.Controls.Add(txtBox);

					var literal = new Literal { Text = "</td>" };

					ControlsPlaceHolder.Controls.Add(literal);

					if (!addOn.OptionalInd)
					{
						ControlsPlaceHolder.Controls.Add(new LiteralControl("<td>"));
						var img = new Image
						{
							ID = "Image_AddOnValue" + addOnValue.AddOnValueID,
							ImageAlign = ImageAlign.AbsMiddle,
							ImageUrl = ResolveUrl(ZNodeCatalogManager.GetImagePathByLocale("Required.gif")),
							Visible = false
						};

						ControlsPlaceHolder.Controls.Add(img);
						ControlsPlaceHolder.Controls.Add(new LiteralControl("</td>"));
					}
					ControlsPlaceHolder.Controls.Add(new LiteralControl("</tr></table>"));

					ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
				}
			}
		}

		/// <summary>
		/// Retrieves default addOn list for a given product
		/// </summary>
		/// <param name="message"></param>
		/// <param name="selectedAddOnList"></param>
		/// <param name="addProductToCart"></param>
		/// <returns> boolean value and selectedAddOnList</returns>
        public bool GetDefaultAddOns(out string message, out ZNodeAddOnList selectedAddOnList, ZNodeProduct addProductToCart)
        {
			if (addProductToCart.ZNodeAddOnCollection.Count > 0)
			{
				pnlAddOns.Visible = true;
				//displays addon value/description on shoppingcart page
				foreach (ZNodeAddOn addOn in addProductToCart.ZNodeAddOnCollection)
				{
					var displayType = addOn.DisplayType;
					if (displayType == "TextBox")
						BindTextBoxControl(addOn);
				}
			}

			var addOnValues = new System.Text.StringBuilder();
            selectedAddOnList = new ZNodeAddOnList();

            foreach (ZNodeAddOn addOn in addProductToCart.ZNodeAddOnCollection)
                // Loop through the Add-on values for each Add-on
                foreach (var addOnValue in addOn.ZNodeAddOnValueCollection.Cast<ZNodeAddOnValue>().Where(addOnValue => addOnValue.IsDefault))
                {
                    if (addOnValues.Length > 0)
                        addOnValues.Append(",");

                    // Check for quantity on hand and back-order,track inventory settings
                    if (addOnValue.QuantityOnHand <= 0 && addOn.AllowBackOrder == false && addOn.TrackInventoryInd)
                    {
                        message = this.GetLocalResourceObject("OutOfStock").ToString().Replace("<name>", addOnValue.Name);
                        return false;
                    }

                    // Add to Selected Addon list for this product
                    addOnValues.Append(addOnValue.AddOnValueID.ToString());
                }

            if (addOnValues.Length > 0)
                // get a add-on values based on Add-ons selected
                selectedAddOnList = ZNodeAddOnList.CreateByProductAndAddOns(addProductToCart.ProductID, addOnValues.ToString());

			foreach (ZNodeAddOn addOn in selectedAddOnList.ZNodeAddOnCollection)
			{
				foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnValueEntity addOnvalue in addOn.AddOnValueCollection)
				{
					var textBoxControl = (TextBox)ControlsPlaceHolder.FindControl("txtBoxAddOn" + addOnvalue.AddOnValueID);

					if (textBoxControl != null)
					{
						addOnvalue.CustomText = textBoxControl.Text.Trim();
					}
				}
			}

            selectedAddOnList.SelectedAddOnValueIds = addOnValues.ToString();
			message = string.Empty;
            return true;
        }
		
        /// <summary>
        /// Update and Bind inventory status message and product price
        /// </summary>
		/// /// <param name="addProductToCart"></param>
        /// <returns>Returns the boolean to update the product status or not</returns>
        public bool UpdateProductStatus(ZNodeProduct addProductToCart)
        {
            var status = true;
            var sku = ZNodeSKU.CreateByProductDefault(addProductToCart.ProductID);

            // Check if product has add-ons
            if (addProductToCart.ZNodeAddOnCollection.Count > 0)
            {
                // If product has Add-Ons, then validate that all Add-Ons Values have been selected by the customer
                string addOnMessage;
                ZNodeAddOnList selectedAddOn = null;
                status = GetDefaultAddOns(out addOnMessage, out selectedAddOn, addProductToCart);

                // Set Selected Add-on
                addProductToCart.SelectedAddOnItems = selectedAddOn;
            }

            // Check stock    
            if (sku.QuantityOnHand < 1 && (!addProductToCart.AllowBackOrder) && addProductToCart.TrackInventoryInd)
            {
                // Display Out of stock message
                lblstockmessage.Text = addProductToCart.OutOfStockMsg;
                lblstockmessage.CssClass = "OutOfStockMsg";
                status = false;
            }

            if (addProductToCart.ZNodeBundleProductCollection.Count > 0)
            {
                string addOnMessage;
                ZNodeAddOnList selectedAddOn = null;

                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeBundleProductEntity bundleProduct in addProductToCart.ZNodeBundleProductCollection)
                {
                    var bundleProductToCart = ZNodeProduct.Create(bundleProduct.ProductID);

                    status = GetDefaultAddOns(out addOnMessage, out selectedAddOn, bundleProductToCart);

                    // Set Selected Add-on
                    bundleProduct.SelectedAddOns = selectedAddOn;
                }
            }

            addProductToCart.SelectedSKU = sku;
            //addProductToCart.CheckSKUProfile();
            addProductToCart.IsPromotionApplied = false;
            addProductToCart.ApplyPromotion();

            return status;
        }

        /// <summary>
        /// Adds to cart, using ShoppingCart & SavedCart object
        /// </summary>
        /// <param name="addProductToCart"></param>
        protected void AddtoCart(ZNodeProduct addProductToCart)
        {
            // Reset status
            uxStatus.Text = string.Empty;

            if (UpdateProductStatus(addProductToCart))
            {
                // Create shopping cart item
                var item = new ZNodeShoppingCartItem { Product = new ZNodeProductBase(addProductToCart), Quantity = 1 };

                // Add product to cart
                var shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                // If shopping cart is null, create in session
                if (shoppingCart == null)
                {
                    shoppingCart = new ZNodeShoppingCart();
                    shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                }

                // Add item to cart
                if (shoppingCart.AddToCart(item))
                {
                    //Update Display Order
                    var displayOrder = new ZNodeDisplayOrder();
                    displayOrder.SetDisplayOrder(addProductToCart);

                    ZNodeSavedCart.AddToSavedCart(item);
                }
                else
                {
                    // Display Out of Stock message
                    uxStatus.Visible = true;
                    uxStatus.Text = addProductToCart.OutOfStockMsg;
                    lblstockmessage.Visible = false;
                }
            }
            else
            {
                // Display Out of Stock message
                uxStatus.Visible = true;
            }
        }

        /// <summary>
        /// Checks for a quantity for a given product by param
        /// Also sets lblstockmessage depending on settings of product
        /// </summary>
        /// <param name="product"></param>
        /// <returns>returns boolean value</returns>
        public bool CheckInventory(ZNodeProduct product)
        {
            int newQuantity = 1;
            var existingItem = new ZNodeShoppingCartItem();

            // Create shopping cart item
            ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
            item.Product = new ZNodeProductBase(product);
            item.Quantity = 1; // Since FBT can add one item only.

            ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            if (shoppingCart == null)
            {
                existingItem.Quantity = newQuantity;
            }
            else
            {
                existingItem = shoppingCart.Exists(item);
            }

            if (existingItem != null && shoppingCart != null)
            {
                newQuantity = existingItem.Quantity + newQuantity;
            }

            lblstockmessage.CssClass = "InStockMsg";

            if(!string.IsNullOrEmpty(stringBuilder.ToString()))
            {
                stringBuilder.Append(", ");
            }
            // Don't track Inventory - Items can always be added to the cart,TrackInventory is disabled
            if (!product.TrackInventoryInd && !product.AllowBackOrder && product.QuantityOnHand > 0)
            {
                lblstockmessage.Text = StockMessage(product.InStockMsg);
            }
            else if (!product.TrackInventoryInd && !product.AllowBackOrder && product.QuantityOnHand <= 0)
            {
                stringBuilder.Append(string.Empty);
                lblstockmessage.Text = stringBuilder.ToString();
            }
            else if (product.TrackInventoryInd && !product.AllowBackOrder && product.QuantityOnHand >= newQuantity)
            {
                lblstockmessage.Text = StockMessage(product.InStockMsg);
            }
            else if (product.AllowBackOrder && product.QuantityOnHand >= newQuantity)
            {
                // Set Inventory stock message
                lblstockmessage.Text = StockMessage(product.InStockMsg);
            }
            else if (product.AllowBackOrder && product.QuantityOnHand < newQuantity)
            {
                lblstockmessage.Text = StockMessage(product.BackOrderMsg);
            }
            else
            {
                // Display Out of stock message
                stringBuilder.Append("<span style=\"color:#E54226;\">");
                stringBuilder.Append(product.Name + " is ");
                stringBuilder.Append(product.OutOfStockMsg);
                stringBuilder.Append("</span>");
                lblstockmessage.Text = stringBuilder.ToString();
                lblstockmessage.CssClass = "OutOfStockMsg";
                OutOfStock = product.OutOfStockMsg;
                // Hide AddToCart button
                btnAddToCart.Visible = false;
                return false;
            }


			// Check for the Maximum Quantity Reached or not.
			int maxQty = product.MaxQty == 0 ? 10 : product.MaxQty;

			if (newQuantity > maxQty)
			{
				lblstockmessage.Text = string.Format("Product {0} reached maximum quantity", product.Name);
				return false;
			}

            return true;
        }

        /// <summary>
        /// Finds selected productId's, checks quantity and then
        /// Calls AddToCart Method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnAddToCartClick(object sender, System.EventArgs e)
        {
            var selectedProdIds = GetListOfSelectedProducts();

            if (selectedProdIds != null && !selectedProdIds.Any())
            {
				lblstockmessage.Text = "Select at least one product";
				lblstockmessage.CssClass = "OutOfStockMsg";
                return;
            }

            bool isQtyAvailable = false;
            foreach (var prod in selectedProdIds.Select(selectedProdId => ZNodeProduct.Create(selectedProdId, false)))
            {
                isQtyAvailable = CheckInventory(prod);
            }

            if (isQtyAvailable && string.IsNullOrEmpty(OutOfStock))
            {
                foreach (var selectedProdId in selectedProdIds)
                {
                    var prod = ZNodeProduct.Create(selectedProdId, false);

                    if (!prod.CallForPricing)
                        AddtoCart(prod);
                }

                const string link = "~/shoppingcart.aspx";
                Response.Redirect(link);
            }
        }

        /// <summary>
        /// Gets product id for the ones selected in the FBT
        /// </summary>
        /// <returns></returns>
        private IEnumerable<int> GetListOfSelectedProducts()
        {
            return (from DataListItem item in DataListRelated.Items let checkbox = (CheckBox)item.FindControl("chkFrequentlyBought") 
					where checkbox.Checked 
					select (HiddenField)item.FindControl("dlHiddenField") 
					into productId 
					where productId != null 
					select int.Parse(productId.Value)).ToList();
        }

        /// <summary>
        /// Add to wish list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnAddToWishListClick(object sender, System.EventArgs e)
        {
            var userAccount = ZNodeUserAccount.CurrentAccount();
            var redirectUrl = "~/account.aspx";
            var selectedProdIds = GetListOfSelectedProducts();

            if (selectedProdIds != null && !selectedProdIds.Any())
            {
                lblstockmessage.Text = "Select at least one product";
                lblstockmessage.CssClass = "OutOfStockMsg";
                return;
            }

            if (userAccount != null)
            {
                foreach (var selectedProdId in selectedProdIds)
                {
					var service = new WishListService();
					var query = new WishListQuery();

                    query.AppendEquals(WishListColumn.ProductID, selectedProdId.ToString());
                    query.AppendEquals(WishListColumn.AccountID, userAccount.AccountID.ToString());
                    var wishList = service.Find(query.GetParameters());

                    if (wishList.Count == 0)
                    {
                        var wishListEntity = new WishList
                            {
                                AccountID = userAccount.AccountID,
                                ProductID = selectedProdId,
                                CreateDte = System.DateTime.Today
                            };
                        service.Insert(wishListEntity);
                    }
                }
            }
            else
                redirectUrl += "?zpid=" + string.Join(",",selectedProdIds);

            Response.Redirect(redirectUrl);
        }

        /// <summary>
        ///  Method to return Stock Messages for FBT Product
        /// </summary>
        /// <returns></returns>
        private string StockMessage(string message)
        {
            stringBuilder.Append("<span style=\"color:#60AF3A;\">");
            stringBuilder.Append(message);
            stringBuilder.Append("</span>");
            return stringBuilder.ToString();
        }
        #endregion
    }
}