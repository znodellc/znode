﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Bundle Products user control class.
    /// </summary>
    public partial class Controls_Default_Product_Bundleproducts : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct _Product;
        private int _ProductId = 0;
        private string _imageUrl = string.Empty;
        private bool _IsQuickWatch = false;
        #endregion

        public event System.EventHandler SelectedIndexChanged;

        /// <summary>
        /// Gets or sets a value indicating whether QuickWatch is enabled or not
        /// </summary>
        public bool IsQuickWatch
        {
            get 
            { 
                return this._IsQuickWatch; 
            }

            set 
            { 
                this._IsQuickWatch = value; 
            }
        }

        /// <summary>
        /// Gets or sets Shopping cart quantity
        /// </summary>
        public int ShoppingCartQuantity
        {
            get
            {
                if (ViewState["Quantity"] != null)
                {
                    return (int)ViewState["Quantity"];
                }

                return 1;
            }

            set
            {
                ViewState["Quantity"] = value;
            }
        }

        #region Public Methods

        /// <summary>
        /// Represents the ValidateAddons method
        /// </summary>
        /// <param name="addOnMessage">Addon Message</param>
        /// <returns>Returns the boolean value whether to validate addons or not</returns>
        public bool ValidateAddons(out string addOnMessage)
        {
            bool status = true;
            addOnMessage = string.Empty;
            string message = string.Empty;
            ZNodeProduct product = (ZNodeProduct)HttpContext.Current.Items["Product"];
            ZNodeAddOnList selectedAddOn = null;

            foreach (DataListItem item in bundleProductList.Items)
            {
                for (int idx = 0; idx < product.ZNodeBundleProductCollection.Count; idx++)
                {
                    Controls_Default_Product_ProductAddOns bundleProductAddOns = (Controls_Default_Product_ProductAddOns)item.FindControl("uxBundleProductAddOns");

                    if (product.ZNodeBundleProductCollection[idx].ProductID == bundleProductAddOns.BundleProductID)
                    {
                        message = string.Empty;

                        status = status && bundleProductAddOns.ValidateAddOns(out message, this.ShoppingCartQuantity, out selectedAddOn);

                        addOnMessage += message + "<br/>";
                    }
                }
            }

            return status;
        }

	    /// <summary>
	    /// Represents the ValidateAddons method for specific given product by parameter.
	    /// Returns status as boolean and addOnMessage which can contain error messages.
	    /// </summary>
	    /// <param name="product">Product to check AddOns</param>
	    /// <param name="selectedQty">Current selected quantity</param>
	    /// <param name="addOnMessage">Addon Message</param>
	    /// <returns>Returns the boolean value whether to validate addons or not</returns>
	    public bool ValidateAddons(ZNodeProduct product, int selectedQty, out string addOnMessage)
		{
			var status = true;
		    addOnMessage = string.Empty;

		    foreach (DataListItem item in bundleProductList.Items)
			{
				var bundleProductAddOns = item.FindControl("uxBundleProductAddOns") as Controls_Default_Product_ProductAddOns;

				if (product.ProductID == bundleProductAddOns.BundleProductID)
				{
					var message = string.Empty;

					ZNodeAddOnList selectedAddOn;
					status = status && bundleProductAddOns.ValidateAddOns(out message, selectedQty, out selectedAddOn);

					addOnMessage += message + "<br/>";
				}
			}
			return status;
		}


        /// <summary>
        /// Represents the ValidateAttributes method
        /// </summary>
        /// <param name="attributeMessage">Attribute Message</param>
        /// <returns>Returns the boolean value to indicate to validate Attributes or not</returns>
        public bool ValidateAttributes(out string attributeMessage)
        {
            bool status = true;
            attributeMessage = string.Empty;
            string message = string.Empty;
            string attributes = string.Empty;
            string attributeDescription = string.Empty;

            ZNodeProduct product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            foreach (DataListItem item in bundleProductList.Items)
            {
                for (int idx = 0; idx < product.ZNodeBundleProductCollection.Count; idx++)
                {
                    Controls_Default_Product_ProductAttributes bundleProductAttributes = (Controls_Default_Product_ProductAttributes)item.FindControl("uxBundleProductAttributes");

                    if (product.ZNodeBundleProductCollection[idx].ProductID == bundleProductAttributes.BundleProductID)
                    {
                        message = string.Empty;

                        status = status && bundleProductAttributes.ValidateAttributes(out message, out attributes, out attributeDescription);

                        attributeMessage += message + "<br/>";
                    }
                }
            }

            return status;
        }

        /// <summary>
        /// Represents the GetImageFileName method
        /// </summary>
        /// <returns>Returns the Image File Name Path</returns>
        public string GetImageFileName()
        {
            if (!this.IsQuickWatch)
            {
                return "SmallImageFilePath";
            }
            else
            {
                return "ThumbnailImageFilePath";
            }
        }

        #endregion
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this._ProductId = int.Parse(Request.Params["zpid"]);
            }
            else
            {
                throw new ApplicationException("Invalid Product Id");
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._Product = (ZNodeProduct)HttpContext.Current.Items["Product"];            
            
            this.BindBundleProducts();            
        }
    
        /// <summary>
        /// Represents the BindBundleProducts method
        /// </summary>
        protected void BindBundleProducts()
        {
            if (this._Product.ZNodeBundleProductCollection.Count > 0)
            {
               bundleProductList.DataSource = this._Product.ZNodeBundleProductCollection;
               bundleProductList.DataBind();
            }
            else 
            { 
                pnlBundle.Visible = false; 
            }
        }

        /// <summary>
        /// Get product image alternative text
        /// </summary>
        /// <param name="AltTagText">Alternate Image Tag Text</param>
        /// <param name="ProductName">Name of the Product</param>
        /// <returns>Returns the Alternate Image Tag Text</returns>
        protected string GetImageAltTag(string AltTagText, string ProductName)
        {
            if (string.IsNullOrEmpty(AltTagText))
            {
                return ProductName;
            }
            else
            {
                return AltTagText;
            }
        }

        protected void BundleProductList_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            Controls_Default_Product_ProductAddOns bundleProductAddOns = (Controls_Default_Product_ProductAddOns)e.Item.FindControl("uxBundleProductAddOns");
            bundleProductAddOns.SelectedIndexChanged += new EventHandler(this.BundleProductAddOns_SelectedIndexChanged);

            Controls_Default_Product_ProductAttributes bundleProductAttributes = (Controls_Default_Product_ProductAttributes)e.Item.FindControl("uxBundleProductAttributes");
            bundleProductAttributes.SelectedIndexChanged += new EventHandler(this.BundleProductAttributes_SelectedIndexChanged);    
        }

        /// <summary>
        /// Bundle Add-ons Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BundleProductAddOns_SelectedIndexChanged(object sender, EventArgs e)
        {
            bool status;
            string message;
            ZNodeProduct product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            foreach (DataListItem item in bundleProductList.Items)
            {
                for (int idx = 0; idx < product.ZNodeBundleProductCollection.Count; idx++)
                {
                    Controls_Default_Product_ProductAddOns bundleProductAddOns = (Controls_Default_Product_ProductAddOns)item.FindControl("uxBundleProductAddOns");
                    if (product.ZNodeBundleProductCollection[idx].ProductID == bundleProductAddOns.BundleProductID)
                    {
                        product.ZNodeBundleProductCollection[idx].SelectedAddOns = bundleProductAddOns.GetSelectedAddOns(out status, out message);
                    }
                }
            }

            this.SelectedIndexChanged(sender, e);
        }

        /// <summary>
        /// Bundle Attributes Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BundleProductAttributes_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SelectedIndexChanged(sender, e);
        }
    }
}