using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Retrieve Images user control class.
    /// </summary>
    public partial class Controls_Default_Product_RetrieveImages : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string Path = Request.QueryString["path"];

                int targetW = 50;
                int targetH = 55;
                int targetX = 2;
                int targetY = 20;

                string path = Path;
                string temp1 = Server.UrlDecode(Path);
                System.Drawing.Image imgPhoto = System.Drawing.Image.FromFile(temp1);
                Bitmap bmpPhoto = new Bitmap(targetW, targetH, PixelFormat.Format24bppRgb);
                bmpPhoto.SetResolution(80, 60);
                Graphics gfxPhoto = Graphics.FromImage(bmpPhoto);
                gfxPhoto.SmoothingMode = SmoothingMode.AntiAlias;
                gfxPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gfxPhoto.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gfxPhoto.DrawImage(imgPhoto, new Rectangle(0, 0, targetW, targetH), targetX, targetY, targetW, targetH, GraphicsUnit.Pixel);

                byte[] imageContent;
                using (MemoryStream mm = new MemoryStream())
                {
                    bmpPhoto.Save(mm, System.Drawing.Imaging.ImageFormat.Jpeg);
                    imageContent = mm.ToArray();
                }

                Response.ContentType = "images/JPEG";
                Response.BinaryWrite(imageContent);

                bmpPhoto.Save(Response.OutputStream, System.Drawing.Imaging.ImageFormat.Jpeg);
            }
            catch (SqlException SQLexc)
            {
                Response.Write("Data Retrival Failure. Error Details : " + SQLexc.ToString());
            }
        }
    }
}