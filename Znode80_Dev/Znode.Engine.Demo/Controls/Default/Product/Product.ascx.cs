using System;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using Category = ZNode.Libraries.DataAccess.Entities.Category;
using ZNodeShoppingCart = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart;
using ZNodeShoppingCartItem = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Product user control class.
    /// </summary>
    public partial class Controls_Default_Product_Product : System.Web.UI.UserControl
    {
        #region Private Variables

        private const string PostBackEventTarget = "__EVENTTARGET";
        private int _productId;        
        private ZNodeShoppingCart _shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);
        private ZNodeProduct Product;
        private ZNodeProfile Profile = new ZNodeProfile();
        private string zcid = string.Empty;

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets Shopping cart quantity
        /// </summary>
        public int ShoppingCartQuantity
        {
            get
            {
                if (ViewState["Quantity"] != null)
                {
                    return (int)ViewState["Quantity"];
                }

                return 1;
            }

            set
            {
                ViewState["Quantity"] = value;
            }
        }
       
        #endregion

        #region  Public Methods

        /// <summary>
        /// Represents the CheckInventory Method
        /// </summary>
        /// <param name="Product">Product Instance</param>
        /// <returns>Returns the Inventory stock message</returns>
        public bool CheckInventory(ZNodeProduct product)
        {
			int newQuantity = ShoppingCartQuantity;
			var existingItem = new ZNodeShoppingCartItem();

			// Create shopping cart item
			ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
			item.Product = new ZNodeProductBase(product);
			item.Quantity = ShoppingCartQuantity; 

			ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

			if (shoppingCart == null)
			{
				existingItem.Quantity = newQuantity;
			}
			else
			{
				existingItem = shoppingCart.Exists(item);
			}

			if (existingItem != null && shoppingCart != null)
			{
				newQuantity = existingItem.Quantity + newQuantity;
			}

            lblstockmessage.CssClass = "InStockMsg";

            // Don't track Inventory - Items can always be added to the cart,TrackInventory is disabled
            if (!product.TrackInventoryInd && !product.AllowBackOrder && product.QuantityOnHand > 0)
            {
                lblstockmessage.Text = product.InStockMsg;
            }
            else if (!product.TrackInventoryInd && !product.AllowBackOrder && product.QuantityOnHand <= 0)
            {
                lblstockmessage.Text = string.Empty;
            }
            else if (product.TrackInventoryInd && !product.AllowBackOrder && product.QuantityOnHand >= newQuantity)
            {
                lblstockmessage.Text = product.InStockMsg;
            }
            else if (product.AllowBackOrder && product.QuantityOnHand >= newQuantity)
            {
                // Set Inventory stock message
                lblstockmessage.Text = product.InStockMsg;
            }
            else if (product.AllowBackOrder && product.QuantityOnHand < newQuantity)
            {
                lblstockmessage.Text = product.BackOrderMsg;
            }
            else
            {
                // Display Out of stock message
                lblstockmessage.Text = product.OutOfStockMsg;
                lblstockmessage.CssClass = "OutOfStockMsg";

                // Hide AddToCart button
                uxAddToCart.Visible = false;

                return false;
            }

			// Check for the Maximum Quantity Reached or not.
			int maxQty = product.MaxQty == 0 ? 10 : product.MaxQty;

			if (newQuantity > maxQty)
			{
				lblstockmessage.Text = string.Format("Product {0} reached maximum selectable quantity", product.Name);
				uxAddToCart.Visible = false;
				return false;
			}

            return true;
        }

        /// <summary>
        /// Update and Bind inventory status message and product price
        /// </summary>
        /// <returns>Returns the boolean to update the product status or not</returns>
        public bool UpdateProductStatus()
        {
	        lblstockmessage.Text = String.Empty;
            // Update the panels
            UpdPnlOrderingOptions.Update();

            // Hide Product price
            uxProductPrice.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;
            uxAddToCart.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;

            this.ShoppingCartQuantity = int.Parse(uxQty.SelectedValue);
            bool status = true;

            ZNodeSKU SKU = ZNodeSKU.CreateByProductDefault(this.Product.ProductID);

            // Check if product has attributes
            if (this.Product.ZNodeAttributeTypeCollection.Count > 0)
            {
                // If product has attributes then validate that all attributes have been selected by
                // the customer validate attributes first - This is to verify that all required attributes have a valid selection
                // if a valid selection is not found then an error message is displayed
                string attributeMessage = string.Empty;
                string attributeList = string.Empty;
                string selectedAttributesDescription = string.Empty;

                status = uxProductAttributes.ValidateAttributes(out attributeMessage, out attributeList, out selectedAttributesDescription);

                if (!status)
                {
                    uxStatus.Visible = true;

                    uxStatus.Text = attributeMessage;
                    
                    // Hide AddToCart button
                    uxAddToCart.Visible = false;
                   
                    return status;
                }

                // Get a sku based on attributes selected
                SKU = ZNodeSKU.CreateByProductAndAttributes(this.Product.ProductID, attributeList);

                // Set Attributes Description to Description property
                SKU.AttributesDescription = selectedAttributesDescription;

                if (SKU.SKUID == 0)
                {
                    lblstockmessage.Text = "Selected options is not available, please try different options";
                    lblstockmessage.CssClass = "OutOfStockMsg";

                    // Hide AddToCart button
                    uxAddToCart.Visible = false;

                    status = false;
                }
                else // Check stock    
                if (SKU.QuantityOnHand < this.ShoppingCartQuantity && (!this.Product.AllowBackOrder) && this.Product.TrackInventoryInd)
                {
                    // Display Out of stock message
                    lblstockmessage.Text = this.Product.OutOfStockMsg;
                    lblstockmessage.CssClass = "OutOfStockMsg";
                    
                    // Hide AddToCart button
                    uxAddToCart.Visible = false;
                   
                    status = false;
                }
            }
            else
            {
                // If Product has no SKUs associated with this product.           
                SKU.AttributesDescription = string.Empty;

                // Check stock    
                if (this.Product.QuantityOnHand < this.ShoppingCartQuantity && (this.Product.AllowBackOrder == false) && this.Product.TrackInventoryInd)
                {
                    lblstockmessage.Text = this.Product.OutOfStockMsg; // Display Out of stock message

                    lblstockmessage.CssClass = "OutOfStockMsg";
                    
                    // Hide AddToCart button
                    uxAddToCart.Visible = false;

                    status = false;
                }
                else
                {
                    lblstockmessage.Text = this.Product.InStockMsg;
                    lblstockmessage.CssClass = "InStockMsg";
                }
            }

            // Check if product has add-ons
            if (this.Product.ZNodeAddOnCollection.Count > 0)
            {
                // If product has Add-Ons, then validate that all Add-Ons Values have been selected by the customer
                string addOnMessage = string.Empty;
                ZNodeAddOnList selectedAddOn = null;

                status = uxProductAddOns.ValidateAddOns(out addOnMessage, this.ShoppingCartQuantity, out selectedAddOn);

                if (!status)
                {
                    uxStatus.Visible = true;

                    // Error Message
                    uxStatus.Text = addOnMessage;

                    // Hide AddToCart button
                    uxAddToCart.Visible = false;

                    return status;
                }

                foreach (ZNodeAddOn addOn in this.Product.ZNodeAddOnCollection.Cast<ZNodeAddOn>().Where(addOn => addOn.AllowBackOrder))
                {
                    uxStatus.Visible = true;
                    uxStatus.ForeColor = System.Drawing.ColorTranslator.FromHtml("#60AF3A");
                    uxStatus.Text = addOnMessage;
                }


                // Set Selected Add-on
                this.Product.SelectedAddOnItems = selectedAddOn;
            }

            this.Product.SelectedSKU = SKU;
            this.Product.CheckSKUProfile();
            this.Product.IsPromotionApplied = false;
            this.Product.ApplyPromotion();

            // Check if product has child product (Bundle product)
            if (this.Product.ZNodeBundleProductCollection.Count > 0)
            {
				var existingCartQtySelected = 0;
				var existingCart = ZNodeShoppingCart.CurrentShoppingCart();
				if (existingCart != null) //Check used to grab quantity from CheckOut page
				{
					var existingCartItem = new ZNodeShoppingCartItem {Product = new ZNodeProductBase(Product)};
					existingCartQtySelected = existingCart.GetQuantityOrdered(existingCartItem);
				}

				foreach (ZNodeBundleProductEntity bundleProd in Product.ZNodeBundleProductCollection)
				{
					if ((bundleProd.QuantityOnHand <= 0 || bundleProd.QuantityOnHand < ShoppingCartQuantity + existingCartQtySelected) 
						&& bundleProd.AllowBackOrder == false 
						&& bundleProd.TrackInventoryInd)
					{
						// Display Out of stock message
						lblstockmessage.Text = string.Format("The bundled product: {0} - Is Out of Stock", bundleProd.Name);
						lblstockmessage.CssClass = "OutOfStockMsg";

						// Hide AddToCart button
						uxAddToCart.Visible = false;
						return false;
					}

					var bundledProduct = ZNodeProductBase.Create(bundleProd.ProductID);
					var selectedQty = ShoppingCartQuantity + existingCartQtySelected;
					
					if (bundledProduct.ZNodeAddOnCollection.Count > 0)
					{
						string bundleProdAddOnMessage;
						status = uxBundleProduct.ValidateAddons(bundledProduct, selectedQty, out bundleProdAddOnMessage);
						if (!status)
						{
							// Display Out of stock message
							lblstockmessage.Text = string.Format("{0} for the bundled product: {1}", bundleProdAddOnMessage, bundledProduct.Name);
							lblstockmessage.CssClass = "OutOfStockMsg";
							uxAddToCart.Visible = false;
							return false;
						}
					}
				}

                string addOnMessage = string.Empty;

                status = uxBundleProduct.ValidateAddons(out addOnMessage);

                if (!status)
                {
                    uxStatus.Visible = true;

                    // Error Message
                    uxStatus.Text = addOnMessage;
                    
                    // Hide AddToCart button
                    uxAddToCart.Visible = false;
                   
                    return status;
                }

                string attributeMessage = string.Empty;

                status = uxBundleProduct.ValidateAttributes(out attributeMessage);

                if (!status)
                {
                    uxStatus.Visible = true;

                    uxStatus.Text = attributeMessage;
                   
                    // Hide AddToCart button
                    uxAddToCart.Visible = false;
                   
                    return status;
                }
            }

            if (status)
            {
                status = this.CheckInventory(this.Product);

                if (this.Product.ZNodeBundleProductCollection.Count > 0 && status)
                {
                    bool applypromotion = false;
                    foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductBaseEntity _bundleProduct in this.Product.ZNodeBundleProductCollection)
                    {
                        ZNodeProduct _znodeBundleProduct = ZNodeProduct.Create(_bundleProduct.ProductID, applypromotion);

                        _znodeBundleProduct.SelectedSKUvalue = _bundleProduct.SelectedSKUvalue;

                        status = status && this.CheckInventory(_znodeBundleProduct);
                    }
                }
            }

            // Check for 'Call for Pricing'
            if (this.Product.CallForPricing)
            {
                lblstockmessage.Visible = false;
                uxAddToCart.Visible = false;
                uxCallForPricing.Visible = true;
                uxStatus.Visible = false;

                if (!string.IsNullOrEmpty(this.Product.CallMessage))
                {
                    uxCallForPricing.Text = this.Product.CallMessage;
                }
            }
            else
            {
                lblstockmessage.Visible = true;
            }

            // Bind selected sku product price if one exists
            uxProductPrice.Quantity = this.ShoppingCartQuantity;
            uxProductPrice.Product = this.Product;
            uxProductPrice.Bind();

            // Set product properties       
            ProductTitle.Text = string.Format(ProductTitle.Text, this.Product.Name);
            ProductID.Text = this.Product.ProductNum;
            ProductDescription.Text = Server.HtmlDecode(this.Product.Description);

            // Set Manufacturer Name
            if (this.Product.ManufacturerName != string.Empty)
            {
                BrandLabel.Visible = true;
                lblBrandName.Text = this.Product.ManufacturerName;
            }

            return status;
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Adds Item to Shopping Cart
        /// </summary>
        protected void AddtoCart()
        {
            // Reset status
            uxStatus.Text = string.Empty;

            if (this.UpdateProductStatus())
            {
                // Create shopping cart item
                ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                item.Product = new ZNodeProductBase(this.Product);
                item.Quantity = this.ShoppingCartQuantity;

                // Add product to cart
                ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                // If shopping cart is null, create in session
                if (shoppingCart == null)
                {
                    shoppingCart = new ZNodeShoppingCart();
                    shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                }

                // Add item to cart
                if (shoppingCart.AddToCart(item))
                {
                    //Update Display Order
                    ZNodeDisplayOrder displayOrder = new ZNodeDisplayOrder();
                    displayOrder.SetDisplayOrder(this.Product);

					Session["ProductObject"] = null;

                    ZNodeSavedCart.AddToSavedCart(item);
                    string link = "~/shoppingcart.aspx";
                    Response.Redirect(link);
                }
                else
                {
                    // Display Out of Stock message
                    uxStatus.Visible = true;
                    uxStatus.Text = this.Product.OutOfStockMsg;
                    lblstockmessage.Visible = false;
                    return;
                }
            }
            else
            {
                // Display Out of Stock message
                uxStatus.Visible = true;

                return;
            }
        }

        /// <summary>
        /// Triggered when addtoCart is fired on the product listing page using query string
        /// </summary>
        protected void Buy_Click()
        {
            // Reset status
            uxStatus.Text = string.Empty;

            // Check product attributes count
            if (this.Product.ZNodeAttributeTypeCollection.Count > 0)
            {
                return;
            }

            // Loop through the product addons
            foreach (ZNodeAddOn _addOn in this.Product.ZNodeAddOnCollection)
            {
                if (!_addOn.OptionalInd)
                {
                    return;
                }
            }

            // Create shopping cart item
            ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
            item.Product = new ZNodeProductBase(this.Product);
            item.Quantity = 1;

            // Add product to cart
            ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            // If shopping cart is null, create in session
            if (shoppingCart == null)
            {
                shoppingCart = new ZNodeShoppingCart();
                shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
            }

            // Add item to cart
            if (shoppingCart.AddToCart(item))
            {
                // Update SavedCart items
                ZNodeSavedCart.AddToSavedCart(item);

                string link = "~/shoppingcart.aspx";
                Response.Redirect(link);
            }
            else
            {
                // Display Out of Stock message
                uxStatus.Visible = true;
                uxStatus.Text = this.Product.OutOfStockMsg;
                return;
            }
        }

        /// <summary>
        /// Represents the AddToViewList Method
        /// </summary>
        protected void AddToViewedList()
        {
            ZNodeProductList productList = null;
            if (Session["ProductViewedList" + ZNodeCatalogManager.LocaleId.ToString()] != null)
            {
                productList = Session["ProductViewedList" + ZNodeCatalogManager.LocaleId.ToString()] as ZNodeProductList;
                ZNodeProductBase objectToAdd = new ZNodeProductBase(this.Product);
                bool isItemExists = false;
                int index = 0;

                foreach (ZNodeProductBase product in productList.ZNodeProductCollection)
                {
                    if (product.ProductID == objectToAdd.ProductID)
                    {
                        isItemExists = true;
                        objectToAdd.DisplayOrder = Convert.ToInt32(Session["DisplayOrder"]) + 1;
                        productList.ZNodeProductCollection[index] = objectToAdd; // Update product details                    
                        break;
                    }

                    index++;
                }

                if (!isItemExists)
                {
                    objectToAdd.DisplayOrder = Convert.ToInt32(Session["DisplayOrder"]) + 1;
                    productList.ZNodeProductCollection.Add(objectToAdd);
                }

                Session["DisplayOrder"] = objectToAdd.DisplayOrder.ToString();
            }
            else
            {
                productList = new ZNodeProductList();

                Session["DisplayOrder"] = "1";

                ZNodeProductBase addProduct = new ZNodeProductBase(this.Product);
                addProduct.DisplayOrder = Convert.ToInt32(Session["DisplayOrder"]);
                productList.ZNodeProductCollection.Add(addProduct);

                Session["ProductViewedList" + ZNodeCatalogManager.LocaleId.ToString()] = productList;
            }
        }

        /// <summary>
        /// Add to cart button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxAddToCart_Click(object sender, EventArgs e)
        {
            this.AddtoCart();
        }

        /// <summary>
        /// Quantity Drop down list index changed event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Quantity_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateProductStatus();
        }

        /// <summary>
        /// Buy from Vendor site.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxBuyDirectFromVendor_Click(object sender, EventArgs e)
        {
            decimal price = uxProductPrice.ProductPrice;
            ZNode.Libraries.ECommerce.Analytics.ZNodeOutboundTracking outboundTracking = new ZNode.Libraries.ECommerce.Analytics.ZNodeOutboundTracking();
            outboundTracking.LogTrackingEvent("Product Referral", HttpContext.Current.Request.Url.ToString(), this.Product.AffiliateUrl, this.Product.ProductID, price);
        }

        /// <summary>
        /// Send a Email Link Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EmailLink_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/EmailFriend.aspx?zpid=" + this._productId.ToString());
        }

        /// <summary>
        /// Add to Wish List link button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddToWishList_Click(object sender, EventArgs e)
        {
            ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();
            string redirectURL = "~/account.aspx";
            if (_userAccount != null)
            {
                WishListService service = new WishListService();
                WishListQuery query = new WishListQuery();
                query.AppendEquals(WishListColumn.ProductID, this._productId.ToString());
                query.AppendEquals(WishListColumn.AccountID, _userAccount.AccountID.ToString());
                TList<WishList> wishList = service.Find(query.GetParameters());

                if (wishList.Count == 0)
                {
                    WishList wishListEntity = new WishList();
                    wishListEntity.AccountID = _userAccount.AccountID;
                    wishListEntity.ProductID = this._productId;
                    wishListEntity.CreateDte = System.DateTime.Today;
                    service.Insert(wishListEntity);
                }
            }
            else
            {
                redirectURL += "?zpid=" + this._productId;
            }

            Response.Redirect(redirectURL);
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {            
            this.BindDefaultValues();

            this.BindProduct();

            // Bind reference user controls
            this.BindControls();

            uxStatus.Visible = false;

            if (uxQty.SelectedIndex != -1)
            {
                this.ShoppingCartQuantity = int.Parse(uxQty.SelectedValue);
            }

            if (!IsPostBack)
            {
                uxProductPrice.Quantity = this.ShoppingCartQuantity;
                uxProductPrice.Product = this.Product;
                uxProductPrice.Bind();
            }
            // Check if add to cart action was requested from a different page
            if (Request.Params["action"] != null)
            {
                if (Request.Params["action"].Equals("addtocart"))
                {
                    if (!Page.IsPostBack)
                    {
                        this.Buy_Click();
                    }
                }
            }  
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {

            if (Session["BreadCrumzcid"] != null)
            {
                this.zcid = Session["BreadCrumzcid"].ToString();
            }
            else
            {
                var service = new ProductCategoryService();
                TList<ProductCategory> pcategories = service.GetByProductID(Convert.ToInt32(Request.QueryString["zpid"]));
                if (pcategories.Count > 0)
                {
                    this.zcid = pcategories[0].CategoryID.ToString();
                }
            }

            // Retrieve category data from httpContext (set previously in the page_preinit event)
            var cservice = new CategoryService();
            Category category = cservice.GetByCategoryID(Convert.ToInt32(zcid));

            if (category != null)
            {
                CategoryTitle.Text = category.Name;
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Bind all the controls to the data
        /// </summary>
        private void BindControls()
        {
            // Review star rating        
            uxReviewRating.TotalReviews = this.Product.TotalReviews;
            uxReviewRating.ReviewRating = this.Product.ReviewRating;
            uxReviewRating.ViewProductLink = this.Product.ViewProductLink;

            Hyperlink.HRef = "~/CustomerReview.aspx?zpid=" + this.Product.ProductID;

            // Related products        
            uxProductRelated.Product = this.Product;
            uxProductRelated.Bind();

			if (this.Product.ZNodeCrossSellItemCollection.OfType<ZNodeCrossSellItem>().Any(x => x.RelationTypeId == (int)RelationType.FBT))
	        {
		        ProductFrequentlyBought.Product = this.Product;
		        ProductFrequentlyBought.Bind();
	        }
	        else
	        {
		        pnlFBT.Visible = false;
	        }

	        // Best Sellers
            uxBestSellers.BindProducts();

            // Set visible for best sellers and related products
            pnlBestSellers.Visible = uxBestSellers.HasItems;
            pnlProductRelated.Visible = uxProductRelated.HasItems;

            if (!uxProductRelated.HasItems && !uxBestSellers.HasItems)
            {
                ProductTabs.Visible = false;
            }
            else
            {
                ProductTabs.Visible = true;
            }

            // Registers the event for the payment (child) control
            this.uxProductAttributes.SelectedIndexChanged += new EventHandler(this.UxProductAttributes_SelectedIndexChanged);
            this.uxProductAddOns.SelectedIndexChanged += new EventHandler(this.UxProductAddOns_SelectedIndexChanged);
            this.uxBundleProduct.SelectedIndexChanged += new EventHandler(this.UxBundleProduct_SelectedIndexChanged);


            // Highlights         
            uxHighlights.Product = this.Product;
            uxHighlights.Bind();
            if (uxHighlights.Product != null)
            {
                uxHighlights.Visible = true;
            }
          
            
            // Price
            uxProductPrice.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;
            pnlQty.Visible = this.Profile.ShowPrice;
        }

        /// <summary>
        /// Bind a product
        /// </summary>
        private void BindProduct()
        {
            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this._productId = int.Parse(Request.Params["zpid"]);
            }
            else
            {
                throw new ApplicationException("Invalid Product Id");
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this.Product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            if (!Page.IsPostBack)
            {
                this.AddToViewedList();
                ZNodeDisplayOrder displayOrder = new ZNodeDisplayOrder();
                displayOrder.SetDisplayOrder(this.Product);
            }

            // Set Visible true for Affilate Url
            if (!string.IsNullOrEmpty(this.Product.AffiliateUrl))
            {
                uxBuyDirectFromVendor.Visible = true;
                uxBuyDirectFromVendor.Attributes.Add("onclick", "return affiliateurl_click('" + this.Product.AffiliateUrl + "');" + Page.ClientScript.GetPostBackEventReference(uxBuyDirectFromVendor, string.Empty).ToString());
            }
            else
            {
                uxBuyDirectFromVendor.Visible = false;
            }

            //Znode Version 7.2.2
            //Display the Vendor Name, when the product belongs to the vendor - Start 
            this.BindVendorName();
            //Display the Vendor Name, when the product belongs to the vendor - End
            this.BindProductData();
        }

        /// <summary>
        /// Represents the BindDefaultValues Method
        /// </summary>
        private void BindDefaultValues()
        {
            // This code must be placed here for SEO Url Redirect to work and fix postback problem with URL rewriting
            if (this.Page.Form != null)
            {
                this.Page.Form.Action = Request.RawUrl;
            }

            // Set button image
            uxAddToCart.Attributes.Add("onclick", "return detect()");

            FeaturedItemImage.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/sale.gif";
            NewItemImage.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/New.gif";           
        }

        /// <summary>
        /// Represents the BindProductData method
        /// </summary>
        private void BindProductData()
        {
            // set product properties       
            ProductTitle.Text = string.Format(ProductTitle.Text, this.Product.Name);
            ProductID.Text = this.Product.ProductNum;
            ProductDescription.Text = Server.HtmlDecode(this.Product.Description);

            // Set Manufacturer Name
            if (this.Product.ManufacturerName != string.Empty)
            {
                BrandLabel.Visible = true;
                lblBrandName.Text = this.Product.ManufacturerName;
            }

            if (!Page.IsPostBack)
            {
                this.BindStockData();
                BindQty();
            }
        }

        public void BindQty()
        {
            // Bind the MaxQuantity value to the Dropdown list
            // If Min quantity is not set in admin, set it to 1
            int minQty = this.Product.MinQty == 0 ? 1 : this.Product.MinQty;

            // If Max quantity is not set in admin , set it to 10
            int maxQty = this.Product.MaxQty == 0 ? 10 : this.Product.MaxQty;

            ArrayList quantityList = new ArrayList();

            for (int itemIndex = minQty; itemIndex <= maxQty; itemIndex++)
            {
                quantityList.Add(itemIndex);
            }

            uxQty.DataSource = quantityList;
            uxQty.DataBind();  
        }

        /// <summary>
        /// Bind all the controls to the data
        /// </summary>
        private void BindStockData()
        {
            uxProductSwatches.ProductId = this._productId;
            pnlSwatches.Visible = uxProductSwatches.HasImages;

            // Product highlights         
            uxHighlights.Product = this.Product;
            uxHighlights.Bind();

            uxProductPrice.Visible = this.Profile.ShowPrice && !this.Product.CallForPricing;
            pnlQty.Visible = this.Profile.ShowPrice;

            bool showCartButton = this.Profile.ShowAddToCart;

            // If out of stock and backorder disabled then hide the AddToCart button
            if (this.Product.ZNodeAttributeTypeCollection.Count > 0)
            {
                // If product has attributes,then display the AddToCart button
                showCartButton &= true;
            }
            else if (this.Product.AllowBackOrder && this.Product.QuantityOnHand <= 0)
            {
                // Allow back Order
                showCartButton &= true;
                
                // Set back order message
                lblstockmessage.Text = this.Product.BackOrderMsg;
            }
            else if (this.Product.AllowBackOrder && this.Product.QuantityOnHand > 0)
            {
                showCartButton &= true;
                
                // Set Item In-Stock message
                lblstockmessage.Text = this.Product.InStockMsg;
            }
            else if (this.Product.QuantityOnHand > 0 && this.Product.TrackInventoryInd == true && !this.Product.AllowBackOrder)
            { 
                // Track Inventory
                showCartButton &= true;
                
                // Set In stock message
                lblstockmessage.Text = this.Product.InStockMsg;
            }
            else if (this.Product.AllowBackOrder == false && this.Product.TrackInventoryInd == false && this.Product.QuantityOnHand > 0)
            {
                // Don't track Inventory Items can always be added to the cart,TrackInventory is disabled
                showCartButton &= true;

                // Set In stock message
                lblstockmessage.Text = this.Product.InStockMsg;
            }
            else if (this.Product.AllowBackOrder == false && this.Product.TrackInventoryInd == false && this.Product.QuantityOnHand <= 0)
            {
                // Don't track Inventory. Items can always be added to the cart,TrackInventory is disabled
                showCartButton &= true;
                
                // Set OutOf Stock message
                lblstockmessage.Text = string.Empty;
            }
            else
            {
                // Set OutOf Stock message
                lblstockmessage.Text = this.Product.OutOfStockMsg;
                lblstockmessage.CssClass = "OutOfStockMsg";
                showCartButton &= false;
            }

            // Set Visible true for NewItem.
            if (this.Product.NewProductInd)
            {
                NewItemImage.Visible = true;
            }

            if (this.Product.InventoryDisplay == 1)
            {
                uxBuyDirectFromVendor.Visible = true;
            }

            // Set Visible true for FeaturedItem.
            if (this.Product.FeaturedInd)
            {
                FeaturedItemImage.Visible = true;
            }

            // Add to cart button
            if (this.Product.CallForPricing)
            {
                // If call for pricing is enabled,then hide AddToCart Button
                uxCallForPricing.Text = ZNodeCatalogManager.MessageConfig.GetMessage("ProductCallForPricing");
                showCartButton &= false;
                FeaturedItemImage.Visible = false;
                lblstockmessage.Text = string.Empty;
                uxCallForPricing.Visible = !showCartButton;

                if (!string.IsNullOrEmpty(this.Product.CallMessage))
                {
                    uxCallForPricing.Text = this.Product.CallMessage;
                }
            }

            uxAddToCart.Visible = showCartButton;
        }
        
        /// <summary>
        /// Product Addons selected Index Changed Event raised by the Product Addon control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxProductAddOns_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateProductStatus();
        }

        /// <summary>
        /// Product Attributes selected Index Changed Event raised by the Product Attributes control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxProductAttributes_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            this.Product.SelectedSKU = uxProductAttributes.SelectedSKU;

            if (this.Product.SelectedSKU.SKUID > 0)
            {
				ScriptManager.RegisterClientScriptBlock(UpdPnlOrderingOptions, sender.GetType(), "AttributeImage", "document.getElementById('CatalogItemImage').src='" + ResolveUrl(this.Product.MediumImageFilePath) + "'; document.getElementById('CatalogItemImage').alt='" + this.Product.SelectedSKU.ImageAltTag + "'; document.getElementById('CatalogItemImage').title='" + this.Product.SelectedSKU.ImageAltTag + "';", true);
                BindQty();
                UpdPnlOrderingOptions.Update();
            }

            this.UpdateProductStatus();
        }

        /// <summary>
        /// Event is raised when BundleProduct Selected Index Changed event is called
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        private void UxBundleProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            UpdPnlOrderingOptions.Update();
            this.UpdateProductStatus();               
        }

        #region Znode Version 7.2.2
       

        /// <summary>
        /// Znode version 7.2.2
        /// Represents the BindVendorName method.
        /// Display the vendor name, in case the product belongs to vendor.
        /// </summary>
        private void BindVendorName()
        {
            string vendorName = this.GetVendorName();

            if (!string.IsNullOrEmpty(vendorName))
            {
                VendorName.Visible = true;
                lblVendorName.Text = vendorName;
            }
            else
            {
                VendorName.Visible = false;
            }
        }

        /// <summary>
        /// Znode version 7.2.2
        /// Represents the GetVendorName method.
        /// Method returns the Vendor name, in case the product has supplier id.
        /// </summary>
        /// <returns>Returns vendorName on the basis of SupplierID</returns>
        private string GetVendorName()
        {
            string vendorName = string.Empty;
            SupplierService supplierService = new SupplierService();

            if (Product.SupplierID > 0)
            {
                Supplier supplierInfo = supplierService.GetBySupplierID(Product.SupplierID);
                vendorName = string.Format("{0} {1} ({2})", supplierInfo.ContactFirstName, supplierInfo.ContactLastName, supplierInfo.ContactEmail);
            }
            return vendorName;
        }
        #endregion

        #endregion
    }
}