using System;
using System.Text;
using System.Web;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Product Price user control class.
    /// </summary>
    public partial class Controls_Default_Product_ProductTitle : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct _product;
        private int _Quantity = 1;
        private ZNodeProfile _Profile = new ZNodeProfile();
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Znodeproduct object
        /// </summary>
        public ZNodeProduct Product
        {
            get
            {
                return this._product;
            }

            set
            {
                this._product = value;
            }
        }

        /// <summary>
        /// Gets or sets the quantity value
        /// </summary>
        public int Quantity
        {
            get 
            { 
                return this._Quantity; 
            }

            set 
            { 
                this._Quantity = value; 
            }
        }

        /// <summary>
        /// Gets or sets the product price.
        /// </summary>
        public decimal ProductPrice
        {
            get
            {
                if (ViewState["ProductPrice"] != null)
                {
                    return Convert.ToDecimal(ViewState["ProductPrice"]);
                }

                return 0;
            }

            set
            {
                ViewState["ProductPrice"] = value;
            }
        }
        #endregion

        #region Bind Data
        
        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            if (Visible)
            {
                if (this._product.ZNodeAttributeTypeCollection.Count == 0)
                {
                    ZNodeSKU SKU = ZNodeSKU.CreateByProductDefault(this._product.ProductID);
                    this._product.SelectedSKU = SKU;
                }

                if (this._product.CallForPricing)
                {
                    StringBuilder stringBuilderPrice = new StringBuilder();
                    MessageConfigAdmin mconfig = new MessageConfigAdmin();
                    stringBuilderPrice.Append("<span class=CallForPriceMsg>");

                    if (this._product.CallMessage != null && !string.IsNullOrEmpty(this._product.CallMessage.ToString()))
                    {
                        stringBuilderPrice.Append(this._product.CallMessage.ToString());
                    }
                    else
                    {
                        stringBuilderPrice.Append(mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43));
                    }
                    

                    stringBuilderPrice.Append("</span>");

                    return;
                }

                ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                item.Product = new ZNodeProductBase(this._product);
                item.Quantity = this._Quantity;

                decimal price = item.ExtendedPrice;
                decimal unitPrice = this._product.RetailPrice + this._product.SelectedAddOnItems.TotalAddOnRetailCost;

                if ((this._product.WholesalePrice.HasValue && this._Profile.UseWholesalePricing) && this._product.SelectedSKU.NegotiatedPrice.HasValue)
                {
					unitPrice = this._product.WholesalePrice.GetValueOrDefault() + this._product.SelectedAddOnItems.TotalAddOnRetailCost;
                }
                for (int idx = 0; idx < this._product.ZNodeBundleProductCollection.Count; idx++)
                {
                    unitPrice += this._product.ZNodeBundleProductCollection[idx].AddOnPrice;                    
                }

                decimal retailprice = unitPrice * this._Quantity;

                this.ProductPrice = retailprice;

                if (retailprice != price )
                {
                    Price.Text = ZNodePricingFormatter.ConfigureProductPricing(retailprice, price);
                }
                else
                {
                    Price.Text = ZNodePricingFormatter.ConfigureProductPricing(price);
                }

                item.Product = null;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            if (!Page.IsPostBack)
            {
                this.Bind();
            }
        }
        #endregion
    }
}