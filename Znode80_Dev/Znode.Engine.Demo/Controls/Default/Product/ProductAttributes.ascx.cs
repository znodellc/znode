using System;
using System.Web;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;
using System.Collections.Generic;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the ProductAttributes user control class.
    /// </summary>
    public partial class Controls_Default_Product_ProductAttributes : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct _product;
        private ZNodeSKU _SelectedSKU = new ZNodeSKU();
        private string _DefaultAttributeType = string.Empty;
        private bool _IsValid = false;
        private int _BundleProductID = 0;
        #endregion

        // Public Event Handler
        public event System.EventHandler SelectedIndexChanged;

        #region Public Properties
        /// <summary>
        /// Gets or sets the selected Sku for this attribute combination
        /// </summary>
        public ZNodeSKU SelectedSKU
        {
            get 
            { 
                return this._SelectedSKU; 
            }

            set 
            { 
                this._SelectedSKU = value; 
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the selected items are valid
        /// </summary>
        public bool IsValid
        {
            get 
            { 
                return this._IsValid; 
            }

            set 
            { 
                this._IsValid = value; 
            }
        }

        public string DefaultAttributeType
        {
            get
            {
                return this._DefaultAttributeType;
            }
            
            set
            {
                this._DefaultAttributeType = value;
            }
        }

        /// <summary>
        /// Gets or sets the bundle product
        /// </summary>
        public int BundleProductID
        {
            get 
            {
                return this._BundleProductID; 
            }

            set 
            { 
                this._BundleProductID = value; 
            }
        }

        #endregion

        #region Bind Data

        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            if (this._product == null)
                return;

			ControlPlaceHolder.Controls.Clear();

            if (this._product.ZNodeAttributeTypeCollection.Count > 0)
            {
                int counter = 1;
                pnlOptions.Visible = true;
				
                ControlPlaceHolder.Controls.Add(new LiteralControl("<div>"));

				var selectSkuIds = new List<ZNodeAttribute>();

                foreach (ZNodeAttributeType attributeType in this._product.ZNodeAttributeTypeCollection)
                {					
                    var lstControl = new DropDownList
	                    {
		                    ID = "lstAttribute" + attributeType.AttributeTypeId.ToString(),
		                    AutoPostBack = true,
		                    CssClass = "AttributeDropDown"
	                    };

	                if (counter++ == 1)
                    {
                        this.DefaultAttributeType = lstControl.ID;
                    }

                    var li = new ListItem("Select " + attributeType.Name, "0") {Selected = true};

	                lstControl.SelectedIndexChanged += new EventHandler(this.LstControl_SelectedIndexChanged);
                    lstControl.Items.Add(li);

					var attributeSkus = selectSkuIds.SelectMany(x => x.SkuAttributes.Cast<ZNodeSkuAttribute>())
								.GroupBy(x => x.SKUID)
								.Where(x => x.Count() >= selectSkuIds.Count()).ToList(); 
	                
					foreach (ZNodeAttribute attribute in attributeType.ZNodeAttributeCollection.Cast<ZNodeAttribute>())
                    {
                        var li1 = new ListItem(attribute.Name, attribute.AttributeId.ToString());

						if ((attributeSkus.Any() || selectSkuIds.Any()) &&
							attributeSkus.All(x => attribute.SkuAttributes.Cast<ZNodeSkuAttribute>().All(z => z.SKUID != x.Key)))
							li1.Attributes.Add("style", "color:rgb(197, 196, 196)");                        

                        lstControl.Items.Add(li1);
                    }

	                if (attributeType.SelectedAttributeId > 0)
		                lstControl.SelectedValue = attributeType.SelectedAttributeId.ToString();                    

                    if (!attributeType.IsPrivate)
                    {
                        var lit1 = new Literal {Text = @"<table class=""Option"">"};

	                    ControlPlaceHolder.Controls.Add(lit1);

                        var ltrl = new Literal
	                        {
		                        Text = string.Format("<tr><td class='OptionLabel'>{0} :</td>", attributeType.Name)
	                        };

	                    ControlPlaceHolder.Controls.Add(ltrl);

                        var lt = new Literal {Text = @"<td class='OptionValue'>"};

	                    ControlPlaceHolder.Controls.Add(lt);
                        ControlPlaceHolder.Controls.Add(lstControl);

                        var literal = new Literal {Text = @"</td>"};
	                    ControlPlaceHolder.Controls.Add(literal);
                        ControlPlaceHolder.Controls.Add(new LiteralControl("<td>"));
                        var img = new Image
	                        {
		                        ID = string.Format("Image_Attribute{0}", attributeType.AttributeTypeId),
		                        ImageAlign = ImageAlign.AbsMiddle,
		                        ImageUrl = ResolveUrl(ZNodeCatalogManager.GetImagePathByLocale("Required.gif")),
								Visible = (lstControl.SelectedIndex <= 0 && IsPostBack)
	                        };

	                    ControlPlaceHolder.Controls.Add(img);
                        ControlPlaceHolder.Controls.Add(new LiteralControl("</td>"));
                        ControlPlaceHolder.Controls.Add(new LiteralControl("</tr></table>"));
                    }

					selectSkuIds.AddRange(attributeType.ZNodeAttributeCollection
								 .Cast<ZNodeAttribute>()
								 .Where(x => x.AttributeId == attributeType.SelectedAttributeId)
								 .Select(z => new ZNodeAttribute() { SkuAttributes = z.SkuAttributes }));
                }

                ControlPlaceHolder.Controls.Add(new LiteralControl("</div>"));
            }
            else
            {
                pnlOptions.Visible = false;                
            }
        }

        #endregion

        #region Helper Functions
        /// <summary>
        /// Validate selected attributes and return a user message
        /// </summary>
        /// <param name="message">The value of Message</param>
        /// <param name="attributes">The Value of attributes</param>
        /// <param name="selectedAttributesDescription">Selected attributes Description</param>
        /// <returns>Returns the user message to validate selected attributes </returns>
        public bool ValidateAttributes(out string message, out string attributes, out string selectedAttributesDescription)
        {
            var sbAttributes = new System.Text.StringBuilder();
            var description = new System.Text.StringBuilder();

            // Loop through types to locate the controls
            foreach (ZNodeAttributeType attributeType in this._product.ZNodeAttributeTypeCollection)
            {
                if (sbAttributes.Length > 0)
                {
                    sbAttributes.Append(",");
                }

                if (!attributeType.IsPrivate)
                {
                    var lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + attributeType.AttributeTypeId.ToString());

                    int selValue = int.Parse(lstControl.SelectedValue);

                    var img = (Image)ControlPlaceHolder.FindControl("Image_Attribute" + attributeType.AttributeTypeId.ToString());

                    if (img != null)
                    {
                        img.Visible = false;
                    }

                    if (selValue > 0)
                    {
                        attributeType.SelectedAttributeId = selValue;

                        sbAttributes.Append(selValue.ToString());

                        description.Append(attributeType.Name);
                        description.Append(" - ");
                        description.Append(lstControl.SelectedItem.Text);
                        description.Append("<br />");
                    }
                    else
                    {
                        if (img != null)
                        {
                            img.Visible = true;
                        }
	                    attributeType.SelectedAttributeId = 0;
                        message = " <div>Select " + attributeType.Name + "</div>";
                        attributes = sbAttributes.ToString();
                        selectedAttributesDescription = string.Empty;
						Bind();
                        return false;
                    }
                }
            }

            message = string.Empty;
            attributes = sbAttributes.ToString();
            selectedAttributesDescription = description.ToString();

			Bind();

            return true;
        }
        #endregion

        #region Protected Methods and  Events

        /// <summary>
        /// Event Handler for the dynamic control DropDownList
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            string Message = String.Empty;
            string ProductAttributes = String.Empty;
            string Description = String.Empty;

	        // Validate attributes first - This is to verify that all required attributes have a valid selection
            // If a valid selection is not found then an error message is displayed
            this._IsValid = this.ValidateAttributes(out Message, out ProductAttributes, out Description);

            if (this._IsValid)
            {
                // Get a sku based on attributes selected
                ZNodeSKU _SKU = ZNodeSKU.CreateByProductAndAttributes(this._product.ProductID, ProductAttributes);
                _SKU.AttributesDescription = Description;

                this._SelectedSKU = _SKU;
            }

            if (this.BundleProductID > 0)
            {
                var product = (ZNodeProduct)HttpContext.Current.Items["Product"];

                for (int idx = 0; idx < product.ZNodeBundleProductCollection.Count; idx++)
                {
                    if (product.ZNodeBundleProductCollection[idx].ProductID == this.BundleProductID)
                    {
                        // Set sku object
                        product.ZNodeBundleProductCollection[idx].SelectedSKUvalue = this._SelectedSKU;
                    }
                }
            }

            if (this.SelectedIndexChanged != null)
            {
                this.SelectedIndexChanged(sender, e);
            }
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._product = (ZNodeProduct)HttpContext.Current.Items["Product"];

			if (this.BundleProductID > 0)
			{
				// Binds the Bundle Product.
				this._product = ZNodeProduct.Create(this.BundleProductID);
			}
           
            this.Bind();
        }
        #endregion
    }
}