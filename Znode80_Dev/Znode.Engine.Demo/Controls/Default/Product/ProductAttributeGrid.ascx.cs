using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Catalog;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Product Attribute Grid user control class.
    /// </summary>
    public partial class Controls_Default_Product_ProductAttributeGrid : System.Web.UI.UserControl
    {
        #region Private Variables
        private static int _attributeId;
        private bool _ShowInventoryLevels = false;
        private bool _ShowfooterBar = true;
        private int _ProductId;
        private int _ProductTypeId;
        private string _AttributeName;
        private string _SelectorName = string.Empty;
        private string _Xaxis = string.Empty;
        private string _Yaxis = string.Empty;
        private string _Path = string.Empty;
        #endregion

        #region Public Properties

        /// <summary>
        /// Sets a value indicating whether to show the grid
        /// </summary>
        public bool ShowGrid
        {
            set
            {
                AttributeGrid.Visible = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the value true shows the footer section, false shows just a empty row.
        /// </summary>
        public bool ShowFooter
        {
            get
            {
                return this._ShowfooterBar;
            }

            set
            {
                this._ShowfooterBar = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the value true shows the number in inventory, false shows just a bullet.
        /// </summary>
        public bool ShowInventoryLevels
        {
            get 
            { 
                return this._ShowInventoryLevels; 
            }

            set 
            { 
                this._ShowInventoryLevels = value; 
            }
        }

        /// <summary>
        /// Gets or sets the AttributeId
        /// </summary>
        public int AttributeId
        {
            get
            {
                return _attributeId;
            }

            set
            {
                _attributeId = value;
            }
        }

        /// <summary>
        /// Gets or sets the ProductTypeID
        /// </summary>
        public int ProductTypeID
        {
            get
            {
                return this._ProductTypeId;
            }

            set
            {
                this._ProductTypeId = value;
            }
        }

        /// <summary>
        /// Gets or sets the AttributeName
        /// </summary>
        public string AttributeName
        {
            get
            {
                return this._AttributeName;
            }

            set
            {
                this._AttributeName = value;
            }
        }

        /// <summary>
        /// Gets or sets the SelectorName
        /// </summary>
        public string SelectorName
        {
            get
            {
                return this._SelectorName;
            }

            set
            {
                this._SelectorName = value;
            }
        }

        /// <summary>
        /// Gets or sets the Xaxis
        /// </summary>
        public string Xaxis
        {
            get
            {
                return this._Xaxis;
            }

            set
            {
                this._Xaxis = value;
            }
        }

        /// <summary>
        /// Gets or sets the Yaxis
        /// </summary>
        public string Yaxis
        {
            get
            {
                return this._Yaxis;
            }

            set
            {
                this._Yaxis = value;
            }
        }

        /// <summary>
        /// Gets or sets the selected color product image path
        /// </summary>
        public string Path
        {
            get
            {
                return this._Path;
            }

            set
            {
                this._Path = value;
            }
        }

        /// <summary>
        /// Gets or sets the productID
        /// </summary>
        public int ProductID
        {
            get
            {
                return this._ProductId;
            }

            set
            {
                this._ProductId = value;
            }
        }

        #endregion

        #region Bind
        /// <summary>
        /// Represents the Bind Method
        /// </summary>
        public void Bind()
        {
            // Remove the repeated grid
            ControlPlaceHolder.Controls.Clear();

            DataTable dt = new DataTable();
            dt = ZNodeProduct.GetByAttributeID(this._ProductId, _attributeId, this._ProductTypeId, this._SelectorName, this._Xaxis, this._Yaxis);

            if (dt.Rows.Count > 0)
            {
                // Create a Data Table to store our reformatted table.
                // Find the columns in our table.
                lbltext.Visible = true;
                lbltext.Text = "Attribute Grid";
                DataRow[] rows = dt.Select("YAXIS Like '*'", "YDisplayOrder");
                ArrayList yaxis = new ArrayList();
                ArrayList yaxisId = new ArrayList();
                int lastYaxisId = -1;
                foreach (DataRow row in rows)
                {
                    if (lastYaxisId != (int)row["YAXISID"])
                    {
                        yaxisId.Add((int)row["YAXISID"]);
                        yaxis.Add(row["YAXIS"].ToString());
                    }

                    lastYaxisId = (int)row["YAXISID"];
                }

                StringBuilder tableHtml = new StringBuilder(1000);
                tableHtml.Append("<div><!-- Grid -->\r\n<table cellpadding='0' cellspacing='0' class='Grid'>\r\n");

                // Start our first row.
                tableHtml.Append("\t<tr class='HeaderStyle' style='text-align: center;'>\r\n");

                // Create a column for column.
                tableHtml.Append("\t\t<td>&nbsp;</td>\r\n");

                // Create the header for the table.
                int yaxisIndex;
                for (yaxisIndex = 0; yaxisIndex < yaxis.Count; yaxisIndex++)
                {
                    tableHtml.Append("\t\t<td class='HeaderStyle'><div class='Header'>");
                    tableHtml.Append(yaxis[yaxisIndex]);
                    tableHtml.Append("</div></td>\r\n");
                }

                // Fill in our newly created table. 
                int lastXaxisId = -1;
                bool altRow = false;
                int colNum = 0;
                ArrayList XaixsID = new ArrayList();
                DataRow[] Xaxisrows = dt.Select("XAXIS Like '*'", "XDisplayOrder");
                foreach (DataRow row in Xaxisrows)
                {
                    {
                        // If there is a new Xaxis then we need a new row.
                        if (lastXaxisId != (int)row["XAXISID"])
                        {
                            colNum = 0;

                            // Padd out the rest of the cells with empty values.
                            for (int i = yaxisIndex; i < yaxis.Count; i++)
                            {
                                tableHtml.Append("\t\t<td>&nbsp;</td>\r\n");
                                colNum++;
                            }

                            // Finish the previous row and start a new row and put in the xaxis value.
                            if (altRow)
                            {
                                tableHtml.Append("\t</tr>\r\n\t<tr class='AlternatingRowStyle' style='text-align: center;'>\r\n\t\t<td class='LeftColStyle'>");
                            }
                            else
                            {
                                tableHtml.Append("\t</tr>\r\n\t<tr class='RowStyle' style='text-align: center;'>\r\n\t\t<td class='LeftColStyle'>");
                            }

                            altRow = !altRow;

                            tableHtml.Append(row["XAXIS"].ToString());
                            tableHtml.Append("</td>\r\n");

                            // Start indexing our cells from the first available yaxis.
                            yaxisIndex = 0;
                        }

                        if (this._Path.Length == 0)
                        {
                            // Get the PicturePath for this AttributeId and ProductId
                            string _SkuPicturePath = row["SkuPicturePath"].ToString();

                            if (_SkuPicturePath.Length > 0)
                            {
                                this.Path = _SkuPicturePath;
                            }
                        }

                        // Fill in missing yaxis with an empty cells.
                        while (yaxisIndex < yaxis.Count && row["YAXIS"].ToString() != yaxis[yaxisIndex++].ToString())
                        {
                            tableHtml.Append("\t\t<td>&nbsp;</td>\r\n");
                            colNum++;
                        }

                        // Add our quantity on hand to the current cell.
                        tableHtml.Append("\t\t<td>");
                        if (this._ShowInventoryLevels)
                        {
                            if (row["QuantityOnHand"].ToString().Equals("0"))
                            {
                                tableHtml.Append(row["QuantityOnHand"]);
                            }
                            else
                            {
                                tableHtml.Append("<strong>" + row["QuantityOnHand"] + "</strong>");
                            }
                        }
                        else
                        {
                            // Add a bullet showing it is in or out of stock stock.
                            if (row["QuantityOnHand"].ToString().Equals("0"))
                            {
                                tableHtml.Append("<span class='inStock'>&#45;</span>");
                            }
                            else
                            {
                                tableHtml.Append("<span class='inStock'>&#8226;</span>");
                            }
                        }

                        tableHtml.Append("</td>\r\n");
                        colNum++;

                        // Take note of our last row.                        
                        lastXaxisId = (int)row["XAXISID"];
                    }
                }

                // Fill in any remaining empty columns.
                for (int i = colNum; i < yaxis.Count; i++)
                {
                    tableHtml.Append("\t\t<td>&nbsp;</td>\r\n");
                }

                // Commented out until we start selling items for real. Uncomment to show the available/unavailable key.
                tableHtml.Append("\t</tr>\r\n");
                tableHtml.Append("\t<tr>\r\n");
                tableHtml.Append("\t\t<td class='LeftColStyle'>&nbsp;</td>\r\n");
                tableHtml.Append("\t\t<td colspan='");
                tableHtml.Append(yaxis.Count);
                tableHtml.Append("' align='right'>\r\n");

                // Show Footer.
                if (this._ShowfooterBar)
                {
                    tableHtml.Append("\t\t\t<div class='Legend'><span class='inStock'>&#8226;</span> available. <span class='outOfStock'>-</span> out of stock.</div>\r\n");
                }
                else
                {
                    tableHtml.Append("\t\t\t<div class='Legend'>&nbsp;</div>\r\n");
                }

                tableHtml.Append("\t\t</td>\r\n");

                // Close the last row, table, and div.
                tableHtml.Append("\t</tr>\r\n</table>\r\n</div>\r\n");

                // Dynamic create customize grid control.
                Literal ltr = new Literal();

                // This is a hack so that we don't show grids that don't have inventory.
                if (yaxis.Count > 2)
                {
                    ltr.Text = tableHtml.ToString() + "<br />";
                }
                else
                {
                    ltr.Text = "Grid Coming Soon" + "<br />";
                }

                ControlPlaceHolder.Controls.Add(ltr);
            }
        }

        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["zpid"] != null)
            {
                this._ProductId = int.Parse(Request.Params["zpid"]);
            }
        }
        #endregion
    }
}