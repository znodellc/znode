using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the SwatchImages user control class.
    /// </summary>
    public partial class Controls_Default_Product_SwatchImages : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private bool _IsProductPage = false;
        private int _ProductId;        
        private ZNodeProduct _product = new ZNodeProduct();
        private bool _HasImages = false;
        private int _MaxDisplayColumns = ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.MaxCatalogCategoryDisplayThumbnails;
        private bool _IncludeProductImage = true;
        private string _ImageSizePath = "thumbnail";
        private string _ControlType = string.Empty;
        private ZNodeImage znodeImage = new ZNodeImage();
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets a value indicating whether the value returns if there are related images in this control.
        /// </summary>
        public bool HasImages
        {
            get
            {
                return this._HasImages;
            }
        }

        /// <summary>
        /// Gets or sets the maximum number of columns used to display the images. Defaults to Max Catalog Display Columns in General Settings.
        /// </summary>
        public int MaxDisplayColumns
        {
            get 
            { 
                return this._MaxDisplayColumns; 
            }

            set 
            { 
                this._MaxDisplayColumns = value; 
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to include the main product picture with the images displayed.
        /// </summary>
        public bool IncludeProductImage
        {
            get 
            { 
                return this._IncludeProductImage; 
            }

            set 
            { 
                this._IncludeProductImage = value; 
            }
        }

        /// <summary>
        /// Gets or sets the Image Size path.The directory the images should be pulled from (thumbnail, small, medium or large). You do not need to specify the whole path.
        /// </summary>
        public string ImageSizePath
        {
            get 
            { 
                return this._ImageSizePath; 
            }

            set 
            { 
                this._ImageSizePath = value; 
            }
        }

        /// <summary>
        /// Gets or sets the ControlType for the AdditionalImages
        /// </summary>
        public string ControlType
        {
            get 
            { 
                return this._ControlType; 
            }

            set 
            { 
                this._ControlType = value; 
            }
        }

        /// <summary>
        /// Gets or sets the Product Id
        /// </summary>
        public int ProductId
        {
            get 
            { 
                return this._ProductId; 
            }

            set
            {
                this._ProductId = value;
                this.BindImage();
            }
        }

        /// <summary>
        /// Gets or sets the Product
        /// </summary>
        public ZNodeProductBase Product
        {
            get
            {
                return this._product;
            }

            set
            {
                this._product = new ZNodeProduct() { ProductID = value.ProductID, Name = value.Name, ImageFile = value.ImageFile, ImageAltTag = value.ImageAltTag };
                this.BindImage();
            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Get catalog product image Name
        /// </summary>
        /// <param name="title">The value of Title</param>
        /// <param name="imageFileName">Image File Name</param>
        /// <returns>Returns the Image name</returns>
        protected string GetImageName(string title, string imageFileName)
        {
            int idx = imageFileName.IndexOf('.');
            if (title.Trim().Length > 0)
            {
                return title;
            }
            else 
            {
                if (imageFileName.Length > 0 && idx > 0 && idx < imageFileName.Length)
                {
                    return imageFileName.Remove(imageFileName.IndexOf('.'));
                }
                else
                {
                    return imageFileName;
                }
            }
        }

        /// <summary>
        /// Represents the ShowImage method
        /// </summary>
        /// <param name="ShowOnCategoryPage">Show on Category Page</param>
        /// <returns>Returns the bool value whether to show the Image on Category Page</returns>
        protected bool ShowImage(object ShowOnCategoryPage)
        {
            return this._IsProductPage || bool.Parse(ShowOnCategoryPage.ToString());
        }

        /// <summary>
        /// Get product image alternative text
        /// </summary>
        /// <param name="altTagText">Alternate Tag text</param>
        /// <param name="productName">Product Name</param>
        /// <returns>Returns the Alternative Image Text</returns>
        protected string GetImageAltTagText(object altTagText, string productName)
        {
            if (altTagText == null)
            {
                return productName;
            }
            else if (altTagText.ToString().Length == 0)
            {
                return productName;
            }
            else
            {
                return altTagText.ToString();
            }
        }

        #endregion

        #region DataList Events
        /// <summary>
        /// Event that is raised when Item Data Bound is called for Swatches Datalist control
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DataListSwatches_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            string clickImg = string.Empty;
            try
            {
                if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
                {
                    ProductImage img = (ProductImage)e.Item.DataItem;
                    clickImg = img.AlternateThumbnailImageFile;
                    if (string.IsNullOrEmpty(clickImg))
                    {
                        clickImg = img.ImageFile;
                    }

                    if (this._IsProductPage)
                    {
                        clickImg = this.znodeImage.GetImageHttpPathMedium(clickImg);
                    }
                    else
                    {
                        clickImg = this.znodeImage.GetImageHttpPathSmall(clickImg);
                    }

                    HtmlImage imgSwatch = (HtmlImage)e.Item.FindControl("imgSwatch");

                    if (this._IsProductPage)
                    {
                        imgSwatch.Attributes.Add("onclick", "document.getElementById('CatalogItemImage').src='" + ResolveUrl(clickImg) + "';");
                    }
                    else
                    {
                        imgSwatch.Attributes.Add("onclick", "document.getElementById('Img" + this._ProductId + "').src='" + ResolveUrl(clickImg) + "';");
                    }
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }
        }

        protected string GetImagePath(string fileName, bool useSwatch)
        {
            string sourceFileName = string.Empty;
            if (!useSwatch)
            {
                sourceFileName = this.znodeImage.GetImageHttpPathSmallThumbnail(fileName);
            }
            else
            {
                sourceFileName = this.znodeImage.GetImageHttpPathSmallThumbnail(fileName.ToLower().Replace(".", "-swatch."));
            }

            return sourceFileName;
        }

        /// <summary>
        /// Event that is raised when Databind Method is called
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DataListAlternateImages_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            string clickImg = string.Empty;
            try
            {
                ProductImage img = (ProductImage)e.Item.DataItem;
                clickImg = img.AlternateThumbnailImageFile;
                if (string.IsNullOrEmpty(clickImg))
                {
                    clickImg = img.ImageFile;
                }

                if (this._IsProductPage)
                {
                    clickImg = this.znodeImage.GetImageHttpPathMedium(clickImg);
                }
                else
                {
                    clickImg = this.znodeImage.GetImageHttpPathSmall(clickImg);
                }

                HtmlImage imgSwatch = (HtmlImage)e.Item.FindControl("imgSwatch");

                if (this._IsProductPage)
                {
                    imgSwatch.Attributes.Add("onclick", "document.getElementById('CatalogItemImage').src='" + ResolveUrl(clickImg) + "';");
                }
                else
                {
                    imgSwatch.Attributes.Add("onclick", "document.getElementsByName('img" + this._ProductId + "')[0].src='" + ResolveUrl(clickImg) + "';");
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }
        }
        #endregion

        #region Bind Method
        /// <summary>
        /// Represents the BindImage method
        /// </summary>
        private void BindImage()
        {
            if (Request.Params["zpid"] != null)
            {
                this._ProductId = int.Parse(Request.Params["zpid"]);

                this._IsProductPage = true;

                // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
                this._product = (ZNodeProduct)HttpContext.Current.Items["Product"];
            }

            if (this._ProductId > 0)
            {
                ZNode.Libraries.DataAccess.Service.ProductImageService service = new ZNode.Libraries.DataAccess.Service.ProductImageService();

                // Set up our grid.
                DataListSwatches.RepeatColumns = this._MaxDisplayColumns;

                // Set up our grid.
                DataListAlternateImages.RepeatColumns = this._MaxDisplayColumns;

                if (this.ControlType == "AlternateImage")
                {
                    TList<ProductImage> alternateImages = service.GetByProductIDActiveIndProductImageTypeID(this._ProductId, true, 1);
                    alternateImages.Sort("DisplayOrder");

                    DataListAlternateImages.DataSource = alternateImages;
                    DataListAlternateImages.DataBind();
                    foreach (DataListItem item in DataListAlternateImages.Items)
                    {
                        item.EnableViewState = false;
                    }

                    this._HasImages = alternateImages.Count > 0;
                }
                else if (this.ControlType == "Swatches")
                {
                    TList<ProductImage> swatchesList = new TList<ProductImage>();

                    // Pick category page swatches.
                    if (!this._IsProductPage)
                    {
                        swatchesList = service.Find("ProductId = " + this._ProductId.ToString() + " AND ProductImageTypeID=" + Convert.ToInt32(ZNodeProductImageType.Swatch).ToString() + " AND ShowOnCategoryPage = true");
                    }
                    else
                    {
                        // Pick Product Page swatches.
                        swatchesList = service.GetByProductIDActiveIndProductImageTypeID(this._ProductId, true, 2);
                    }

                    swatchesList.Sort("DisplayOrder");
                    swatchesList.Filter = "ReviewstateId = 20";

                    if (swatchesList.Count > 0 && this._product != null)
                    {
                        ProductImage defaultProductImage = new ProductImage();
                        defaultProductImage.ActiveInd = true;
                        defaultProductImage.ImageAltTag = this._product.ImageAltTag;
                        defaultProductImage.ImageFile = this._product.ImageFile;
                        defaultProductImage.AlternateThumbnailImageFile = this._product.ImageFile;
                        defaultProductImage.Name = this._product.Name;
                        defaultProductImage.ProductID = this._product.ProductID;
                        defaultProductImage.ProductImageID = this._product.ProductID;
                        defaultProductImage.ShowOnCategoryPage = true;

                        swatchesList.Add(defaultProductImage);
                    }

                    DataListSwatches.DataSource = swatchesList;
                    DataListSwatches.DataBind();

                    foreach (DataListItem item in DataListSwatches.Items)
                    {
                        item.EnableViewState = false;
                    }

                    this._HasImages = swatchesList.Count > 0;
                }
            }
        }
        #endregion
    }
}