using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

/// <summary>
/// Displays the related products
/// </summary>
namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the ProductRelated user control class.
    /// </summary>
    public partial class Controls_Default_Product_ProductRelated : System.Web.UI.UserControl
    {
        #region Private Variables
        private string BuyImage = "~/themes/" + ZNodeCatalogManager.Theme + "/images/buynow.gif";
        private ZNodeProductBase _product;
        private ZNodeProfile _ProductProfile = new ZNodeProfile();   
        private bool _HasItems = false;
        private bool _ShowDescription = false;
        private bool _ShowName = false;
        private bool _ShowImage = false;
        private bool _ShowPrice = false;
        private bool _IsQuickWatch = false;
        private ZNodeImage znodeImage = new ZNodeImage();
        #endregion

        #region Public Properties

        public ZNodeProfile ProductProfile
        {
            get { return this._ProductProfile; }
            set { this._ProductProfile = value; }
        }
        /// <summary>
        /// Gets or sets the catalog item object. The control will bind to the data from this object
        /// </summary>
        public ZNodeProductBase Product
        {
            get
            {
                return this._product;
            }

            set
            {
                this._product = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether Product has related items or not
        /// </summary>
        public bool HasItems
        {
            get
            {
                return this._HasItems;
            }

            set
            {
                this._HasItems = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Name or not
        /// </summary>
        public bool ShowName
        {
            get 
            { 
                return this._ShowName; 
            }

            set 
            { 
                this._ShowName = value; 
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Price
        /// </summary>
        public bool ShowPrice
        {
            get
            {
                return this._ShowPrice;
            }

            set
            {
                this._ShowPrice = value;
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether to Show Image
        /// </summary>
        public bool ShowImage
        {
            get 
            { 
                return this._ShowImage; 
            }

            set 
            { 
                this._ShowImage = value; 
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Description
        /// </summary>
        public bool ShowDescription
        {
            get 
            { 
                return this._ShowDescription; 
            }

            set 
            { 
                this._ShowDescription = value; 
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether QuickWatch is enabled or not
        /// </summary>
        public bool IsQuickwatch
        {
            get
            {
                return this._IsQuickWatch;
            }

            set
            {
                this._IsQuickWatch = value;
            }
        }

        #endregion

        #region Bind Data

        /// <summary>
        /// Get ImagePath
        /// </summary>
        /// <param name="imageFile">Image File Name</param>
        /// <returns>Returns the Image Path</returns>
        public string GetImagePath(string imageFile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathCrossSell(imageFile);
        }

        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
	        var crossSellItems =
		        this.Product.ZNodeCrossSellItemCollection.Cast<ZNodeCrossSellItem>()
		            .Where(item => item.RelationTypeId == (int) RelationType.YMAL && item.IsActive);

            if (crossSellItems.Any())
            {
                pnlRelated.Visible = true;
                if (!Page.IsPostBack)
                {
                    this._HasItems = true;
                    DataListRelated.DataSource = crossSellItems;
                    DataListRelated.DataBind();

                    // Disable view state for data list item
                    foreach (DataListItem item in DataListRelated.Items)
                    {
                        item.EnableViewState = false;
                    }
                }
            }
            else
            {
                pnlRelated.Visible = false;
            }
        }
        #endregion

        #region Helper Functions
        /// <summary>
        /// Represents the GetProductUrl method
        /// </summary>
        /// <param name="productId">Product Id value</param>
        /// <returns>Returns the Product Url</returns>
        protected string GetProductUrl(string productId)
        {
            ZNodeUrl url = new ZNodeUrl();
            string PagePath = "~/product.aspx?zpid=" + productId;
            return PagePath;
        }

        /// <summary>
        ///  Represents the GetViewProductImageUrl method
        /// </summary>
        /// <param name="link">Represents the Link Value</param>
        /// <returns>Returns the Path of Product Page Url</returns>
        protected string GetViewProductPageUrl(string link)
        {
            if (this._IsQuickWatch)
            {
                return "javascript:self.parent.location ='" + ResolveUrl(link) + "';";
            }

            return link;
        }

        /// <summary>
        /// Returns properties value for PlugIn Control
        /// </summary>
        /// <param name="reviewCollection">Review Collection List</param>
        /// <param name="viewProductLink">View Product Link</param>
        /// <returns>Returns the ReviewCollection hashtable list</returns>
        protected Hashtable GetParameterList(object reviewCollection, object viewProductLink)
        {
            // Create Instance for hashtable
            Hashtable collection = new Hashtable();

            // Add property name and value to it
            collection.Add("ViewProductLink", viewProductLink);
            collection.Add("ReviewCollection", reviewCollection);

            return collection;
        }

        #endregion

        #region General Events
        /// <summary>
        /// Event is raised when Buy button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Buy_Click(object sender, ImageClickEventArgs e)
        {
            string link = "~/product.aspx?zpid=";

            // Getting ProductID from the image button
            ImageButton but_buy = sender as ImageButton;
            int zpid = int.Parse(but_buy.CommandArgument);

            Response.Redirect(link + zpid + "&action=addtocart");
        }
        #endregion
    }
}