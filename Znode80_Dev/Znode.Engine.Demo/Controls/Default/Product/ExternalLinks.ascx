﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Product_ExternalLinks" CodeBehind="ExternalLinks.ascx.cs" %>
<script>

    function fbs1_click(nam) {
        u = location.href;
        //t = document.title;      
        t = nam;

        window.open('http://www.facebook.com/sharer.php?u=' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;
        //window.open('https://www.facebook.com/sharer/sharer.php?s=100&p[url]=' + encodeURIComponent(u) + '&p[images][0]=' + encodeURIComponent(imagelocation) + '&p[title]=' + encodeURIComponent(nam), 'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;
    }
    function twitter1_click() {
        u = location.href;
        t = document.title;
        window.open('http://twitter.com/home?status=Currently reading ' + encodeURIComponent(u) + '&t=' + encodeURIComponent(t), 'sharer', 'toolbar=0,status=0,width=626,height=436'); return false;

    }

</script>
<div id="DetailPageLink">
    <div id="InternalLinks">
        <div style="margin-top:-1px !important;">
            <asp:LinkButton OnClick="WishListLink_Click" ID="lbAddToWishList" runat="server" meta:resourcekey="LinkButtonResource1"></asp:LinkButton>
            <asp:ImageButton ID="WishListImage" runat="server" EnableViewState="false"
                CssClass="GiftImage" ImageAlign="AbsMiddle" OnClick="WishListImage_Click"
                meta:resourcekey="WishListImageResource1"></asp:ImageButton>
        </div>
        <div style="margin-top:-1px !important;">
            <asp:HyperLink ID="EmailFriendLink" runat="server" meta:resourcekey="EmailFriendLinkResource"></asp:HyperLink>
            <asp:ImageButton ID="EmailImage" EnableViewState="false" runat="server" CssClass="FriendImage" ImageAlign="AbsMiddle" OnClick="EmailImage_Click"
                meta:resourcekey="EmailImageResource1"></asp:ImageButton>
        </div >
        <div class="NoPrint product-print-holder" style="margin-top:2px !important;">
            <a href="#" class="PrintLink">PRINT</a>
            <input type="image" class="PrintLink" src="images/icons/print_icon.png">
        </div>

    </div>

    <div id="SocialLinks">
        <!-- uses the AddThis.com plugin -->
        <!-- AddThis Button BEGIN -->
        <script src="js/Print/jQuery.print.js"></script>
        <script type="text/javascript">
            var addthis_config = {
                ui_language: "<%= this.AddThisLanguageTwoLetterCode %>"
            }
        </script>


        <div class="ShareIt">
            <asp:Localize ID="Localize5" runat="server" EnableViewState="false" meta:resourceKey="txtShareit"></asp:Localize>
        </div>
        <span class="addthis_toolbox addthis_default_style">
            <a class="addthis_button_facebook" title="Facebook"></a>
            <a class="addthis_button_twitter" title="Twitter"></a>
            <a class="addthis_button_pinterest_share" title="Pinterest"></a>
            <%--   <a class="addthis_button_google_plusone_share" title="Google+"></a>
            <a class="addthis_button_compact"></a>
            <a class="addthis_counter addthis_bubble_style"></a>--%>
        </span>
        <script type="text/javascript" src="<%= this.AddThisScriptURL %>"></script>
        <!-- AddThis Button END -->
    </div>
</div>


