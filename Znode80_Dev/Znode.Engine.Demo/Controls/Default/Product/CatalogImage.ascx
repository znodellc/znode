﻿<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Product_CatalogImage" Codebehind="CatalogImage.ascx.cs" %>

<script type="text/javascript" language="javascript">
	var catalogImageItems;
	var prm = Sys.WebForms.PageRequestManager.getInstance();
	prm.add_pageLoaded(PageLoadedEventHandler);

	//
	function PageLoadedEventHandler()
	{
		window.addEvents({
			'domready': function() {
				/* thumbnails example , div containers */
				catalogImageItems = new SlideItMoo({
					overallContainer: 'CatalogImage_outer',
					elementScrolled: 'CatalogImage_inner',
					thumbsContainer: 'CatalogImage_items',
					itemsVisible: 1,
					elemsSlide: 1,
					duration: 500,
					itemsSelector: '.CatalogImage_element',
					showControls: 1
				});
			}
		});
	}
</script>

  <!--thumbnails slideshow begin-->
<div id="CatalogImage_outer">   
	<div id="CatalogImage_inner">
		<div id="CatalogImage_items">	
			<span><asp:PlaceHolder ID="uxPlaceHolder" runat="server"></asp:PlaceHolder></span>
		</div>
	</div>			   
</div>