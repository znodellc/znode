using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Product Highlights ForgotPassword user control class.
    /// </summary>
    public partial class Controls_Default_Product_ProductHighlights : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct _product;
        private int _maxDisplayColumns = ZNodeConfigManager.SiteConfig.MaxCatalogDisplayColumns;
        private string _imageSizePath = ZNodeConfigManager.EnvironmentConfig.SmallImagePath;
        private ZNodeImage _ZNodeImage = new ZNodeImage();
      
        #endregion

        #region Public Properties

        public ZNodeImage ZNodeImage
        {
            get 
            {
                return this._ZNodeImage; 
            }

            set 
            {
                this._ZNodeImage = value; 
            }
        }

        /// <summary>
        /// Gets or sets the catalog item object. The control will bind to the data from this object
        /// </summary>
        public ZNodeProduct Product
        {
            get
            {
                return this._product;
            }

            set
            {
                this._product = value;
            }
        }

        /// <summary>
        /// Gets or sets the Maximum number of columns used to display the images. Defaults to Max Catalog Display Columns in General Settings.
        /// </summary>
        public int MaxDisplayColumns
        {
            get 
            { 
                return this._maxDisplayColumns; 
            }

            set 
            { 
                this._maxDisplayColumns = value; 
            }
        }

        /// <summary>
        /// Gets or sets the directory the images should be pulled from (thumbnail, small, medium or large). 
        /// You do not need to specify the whole path.
        /// </summary>
        public string ImageSizePath
        {
            get 
            { 
                return this._imageSizePath; 
            }

            set 
            { 
                this._imageSizePath = value; 
            }
        }
        #endregion

        #region Bind Data
        
        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            DataListHighlights.RepeatColumns = this._maxDisplayColumns;

            if (this._product.ProductID > 0)
            {
                this._product.ZNodeHighlightCollection.Sort("DisplayOrder");
                DataListHighlights.DataSource = this._product.ZNodeHighlightCollection.Cast<ZNodeHighlight>().Where(x => x.IsActive);
                DataListHighlights.DataBind();
            }
        }

        /// <summary>
        /// Gets or sets the image alternative tag text
        /// </summary>  
        /// <param name="ImageAltTag">Alternate Image Tag</param>
        /// <param name="Name">Name of the Image</param>
        /// <returns>Returns the Alternate Image tag text</returns>
        public string GetImageAltTag(string ImageAltTag, string Name)
        {
            // If image Alt tag value not exists then use highlight name
            if (string.IsNullOrEmpty(ImageAltTag))
            {
                return Name;
            }

            // Otherwise, return Alternative text
            return ImageAltTag;
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Set the control to open the hyperlink in a new Window 
        /// </summary>
        /// <param name="IsEnabled">IsEnabled  value either true or false</param>  
        /// <returns>Returns the string to open the Hyperlink in new Window</returns>
        protected string HyperlinkNewWinInd(bool IsEnabled)
        {
            if (IsEnabled)
            {
                return "_blank";
            }
            else
            {
                return "_self";
            }
        }

        /// <summary>
        /// Get catalog product image Name
        /// </summary>
        /// <param name="Title">The Title value</param>
        /// <returns>Returns the Image Name</returns>
        protected string GetImageName(string Title)
        {
            if (Title.Trim().Length > 0)
            {
                return Title;
            }
            else
            {
                return "&nbsp";
            }
        }

        /// <summary>
        /// Represents the GetNavigationUrl method
        /// </summary>
        /// <param name="EnableExternalHyperlinks">Enable External Hyperlinks</param>
        /// <param name="InternalLink">String that represents InternalLink</param>
        /// <param name="ExternalLink">String that represents ExternalLink</param>
        /// <returns>Returns the Navigation Url</returns>
        protected string GetNavigationUrl(bool EnableExternalHyperlinks, string InternalLink, string ExternalLink)
        {
            if (EnableExternalHyperlinks)
            {
                return ExternalLink;
            }

            return InternalLink;
        }

        #endregion
    }
}