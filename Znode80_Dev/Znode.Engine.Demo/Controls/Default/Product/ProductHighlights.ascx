<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Product_ProductHighlights" Codebehind="ProductHighlights.ascx.cs" %>

<div id="Highlight" class="Highlight">
    <asp:DataList ID="DataListHighlights" runat="server" RepeatDirection="Horizontal">
        <ItemTemplate>
            <div class="HighlightItem">
                 <div>
                <a href='<%# Eval("Hyperlink").ToString()%>' target="_blank">
                    <img runat="server"
                        src='<%# ZNodeImage.GetImageBySize(27, Eval("ImageFile").ToString()) %>' 
                        alt='<%# GetImageAltTag(DataBinder.Eval(Container.DataItem,"ImageAltTag").ToString(), DataBinder.Eval(Container.DataItem,"Name").ToString())%>'
                        visible='<%# Eval("ImageFile").ToString().Length > 0 && !(bool)Eval("DisplayPopup") %>' /></a>
                </div>                
                <div><%-- Image hyperLink --%>
                    <asp:HyperLink id="imageHyperLink" runat="server"  Target="_parent"
                        ImageUrl='<%# ZNodeImage.GetImageBySize(27, Eval("ImageFile").ToString()) %>'
                        NavigateUrl='<%# GetNavigationUrl((bool)Eval("HyperlinkNewWinInd"), "~/HighlightInfo.aspx" + "?zpid=" + (Product != null ? Product.ProductID : 0)  + "&highlightid=" + DataBinder.Eval(Container.DataItem,"HighlightID"),Eval("Hyperlink").ToString()) %>'
                        ToolTip = '<%# GetImageAltTag(DataBinder.Eval(Container.DataItem,"ImageAltTag").ToString(), DataBinder.Eval(Container.DataItem,"Name").ToString())%>'
                        Visible='<%# Eval("ImageFile").ToString().Length > 0 && (bool)Eval("DisplayPopup") %>' />
                </div>
                <div><%-- Name hyperLink --%>
                    <asp:HyperLink id="hyperLink" runat="server" Target='<%# HyperlinkNewWinInd((bool)Eval("HyperlinkNewWinInd"))  %>'
                        NavigateUrl='<%# GetNavigationUrl((bool)Eval("HyperlinkNewWinInd"), "~/HighlightInfo.aspx" + "?zpid=" + (Product != null ? Product.ProductID : 0) + "&highlightid=" + DataBinder.Eval(Container.DataItem,"HighlightID"),Eval("Hyperlink").ToString()) %>'
                        Visible='<%# (Eval("ImageFile").ToString().Length == 0) && !(bool)Eval("DisplayPopup") %>' Text='<%# Eval("Name") %>'></asp:HyperLink>
                </div>                
            </div>
        </ItemTemplate>
    </asp:DataList>
</div>

