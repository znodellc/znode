<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Product_ProductRelated" Codebehind="ProductRelated.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>

<asp:Panel ID="pnlRelated" runat="server" Visible="true">
	<script language="javascript" type="text/javascript">
	var prm = Sys.WebForms.PageRequestManager.getInstance();
	prm.add_pageLoaded(PageLoadedEventHandler);

	//
	function PageLoadedEventHandler() {
		var crossSellItems;
		window.addEvents({
			'domready': function() {
				/* thumbnails example , div containers */
				crossSellItems = new SlideItMoo({
					overallContainer: 'CrossSellItems_outer',
					elementScrolled: 'CrossSellItems_inner',
					thumbsContainer: 'CrossSellItems',
					itemsVisible: 2,
					elemsSlide: 1,
					duration: 300,
					itemsSelector: '.CrossSellItem',
					showControls: 1
				});
			}
		});
	}	
	</script>
	
	<%--<h5><uc1:CustomMessage ID ="RelatedProductTitle" MessageKey="ProductRelatedProductsTitle" runat="server"></uc1:CustomMessage></h5>--%>		
	<div class="CrossSellItem">
	<div id="CrossSellItems_outer">
		<div id="CrossSellItems_inner">
		    <div id="CrossSellItems">
				<asp:DataList ID="DataListRelated" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
					<ItemTemplate>
						<div class="CrossSellItem">
                            <div class="ItemBorder"></div>
							<div class="DetailLink">
								<a href='<%# GetViewProductPageUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>' visible='<%# ShowName %>' runat="server"><%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a>        
							</div>
							<div class="Image">
								<a id="A1" href='<%# GetViewProductPageUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>' runat="server">
									<img alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' Visible = '<%# ShowImage %>' src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>' border="0" runat='server' />
								</a>
							</div>
                            <div class="ItemBorder"></div>
                             <div class="NamePriceDetail">
                                <span class="Name"><asp:Label ID="hlName"  Runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name").ToString()%>'></asp:Label>
                                </span>
							</div>   
					    </div> 
                                  
					</ItemTemplate>					
				</asp:DataList>
			</div>
		</div>
	</div>
        </div>
</asp:Panel>
