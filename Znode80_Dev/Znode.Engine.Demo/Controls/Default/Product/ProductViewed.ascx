<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Product_ProductViewed" CodeBehind="ProductViewed.ascx.cs" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>

<asp:Panel ID="pnlRecentlyViewedProducts" runat="server" Visible="false">
    <script language="javascript" type="text/javascript">
        var recentlyViewedItems;
        window.addEvents({
            'domready': function () {
                /* thumbnails example , div containers */
                recentlyViewedItems = new SlideItMoo({
                    overallContainer: 'RecentlyViewed_outer',
                    elementScrolled: 'RecentlyViewed_inner',
                    thumbsContainer: 'RecentlyViewed_Items',
                    itemsVisible: 1,
                    elemsSlide: 1,
                    duration: 300,
                    itemsSelector: '.RecentlyViewedItem',
                    itemWidth: 130,
                    showControls: 1
                });
            }
        });
    </script>

    <div id="RecentlyViewedProduct">
        <div class="CustomTitle">
            <ZNode:CustomMessage ID="CustomMessage1" MessageKey="RecentlyViewedTitle" runat="server"></ZNode:CustomMessage>
        </div>
        <div id="RecentlyViewed_outer">
            <div id="RecentlyViewed_inner">
                <div id="RecentlyViewed_Items">
                    <asp:DataList ID="RecentlyViewedItems" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow" Height="105" Width="110">
                        <ItemTemplate>
                            <div class="RecentlyViewedItem">
                                <div class="ItemBorder"></div>
                                <div class="Image">
                                    <a id="A1" href='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()%>' runat="server">
                                        <img alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' border='0' src='<%# DataBinder.Eval(Container.DataItem, "CrossSellImagePath")%>' runat="server" visible='<%# ShowImage %>' />
                                    </a>
                                </div>
                                <div class="ItemBorder "></div>
                                <div class="NamePriceDetail">
                                    <span class="Name">
                                        <asp:Label ID="hlName" runat="server" Visible='<%# ShowName %>' Text='<%# DataBinder.Eval(Container.DataItem, "Name").ToString()%>'></asp:Label></span>
                                    <span class="Price">
                                        <asp:Label ID="Price" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>' Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && ProductProfile.ShowPrice && ShowPrice %>'></asp:Label><asp:Image EnableViewState="false" ID="FeaturedItemImage" runat="server" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("sale.gif") %>' Visible='<%# DataBinder.Eval(Container.DataItem, "FeaturedInd") %>' />
                                        
                                        <br/><asp:Label ID="uxCallForPricing" EnableViewState="false" runat="server" meta:resourcekey="uxCallForPricingResource1"
                                            Text='<%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing"), DataBinder.Eval(Container.DataItem, "CallMessage")) %>'></asp:Label>
                                    </span>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
