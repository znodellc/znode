<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Product_Product" CodeBehind="Product.ascx.cs" %>
<%@ Register Src="~/Controls/Default/navigation/NavigationBreadCrumbs.ascx" TagName="StoreBreadCrumbs" TagPrefix="ZNode" %>
<%--<%@ Register Src="~/Controls/Default/common/HomeQuickSearch.ascx" TagName="QuickSearch" TagPrefix="ZNode" %>--%>
<%@ Register Src="~/Controls/Default/Category/navigationcategories.ascx" TagName="NavigationCategories" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/ShoppingCart/NavigationCart.ascx" TagName="NavigationCart" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ProductViewed.ascx" TagName="RecentlyViewedProducts" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Specials/BestSellers.ascx" TagName="BestSellers" TagPrefix="ZNode" %>
<%@ Register Src="SwatchImages.ascx" TagName="Swatches" TagPrefix="ZNode" %>
<%@ Register Src="ProductHighlights.ascx" TagName="ProductHighlights" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/Common/spacer.ascx" TagName="Spacer" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>
<%@ Register Src="ProductPrice.ascx" TagName="ProductPrice" TagPrefix="ZNode" %>
<%@ Register Src="ProductRelated.ascx" TagName="ProductRelated" TagPrefix="ZNode" %>
<%@ Register Src="ProductAttributes.ascx" TagName="ProductAttributes" TagPrefix="ZNode" %>
<%@ Register Src="ProductTabs.ascx" TagName="ProductTabs" TagPrefix="ZNode" %>
<%@ Register Src="ProductAddOns.ascx" TagName="ProductAddOns" TagPrefix="ZNode" %>
<%@ Register Src="ProductAttributeGrid.ascx" TagName="ProductAttributeGrid" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/Bundleproducts.ascx" TagName="BundleProduct" TagPrefix="ZNode" %>
<%@ Register Src="CatalogImage.ascx" TagName="CatalogItemImage" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ExternalLinks.ascx" TagName="ExternalLinks" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CustomMessage/Banner.ascx" TagName="Banner"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/Product/ProductFrequentlyBought.ascx" TagPrefix="uc1" TagName="ProductFrequentlyBought" %>


<script>

    function affiliateurl_click(url) {

        window.open(url, 'sharer', 'location=1,resizable=1,directories=1,menubar=1,scrollbars=1,toolbar=1,status=1,width=800,height=600'); return true;
    }
</script>
<div id="LeftColumn">
    <%-- <ZNode:QuickSearch ID="uxQuickSearch" runat="server"></ZNode:QuickSearch>--%>
    <ZNode:NavigationCategories ID="CATEGORIES" runat="server"></ZNode:NavigationCategories>
    <ZNode:NavigationCart runat="server" ID="uxNavigationCart" />
    <ZNode:RecentlyViewedProducts ID="uxRecentlyViewedProducts" runat="server" Title="Recently Viewed Items"
        ShowName="true" ShowReviews="false" ShowImage="true" ShowDescription="false"
        ShowPrice="true" ShowAddToCart="false" />
</div>

<div id="MiddleColumn">
    <asp:UpdatePanel ID="UpdatePnlProductDetail" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="BreadCrumb">
                <ZNode:StoreBreadCrumbs ID="BREAD_CRUMBS" runat="server"></ZNode:StoreBreadCrumbs>
                <span class="PromoText">
                    <uc1:CustomMessage ID="HomeCustomMessage5" MessageKey="StoreSpecials" runat="server" />
                </span>
            </div>
            <div id="ProductDetail">
                <div class="PageTitle">
                    <asp:Label ID="CategoryTitle" EnableViewState="false" runat="server"></asp:Label>
                </div>
                <div>
                    <ZNode:Banner ID="Banner1" runat="server" BannerKey="ProductPage" DisplayTime="5000" TransitionTime="1000" />
                </div>
                <div class="Horizontalline">
                    &nbsp;
                </div>
                <div>
                    <!-- Left Content -->
                    <div class="LeftContent">
                        <div>
                            <h1 class="ProductTitle">
                                <asp:Literal ID="ProductTitle" Text="{0}" EnableViewState="false" runat="server" meta:resourcekey="ProductTitleResource1"></asp:Literal>
                            </h1>
                        </div>
                        <div class="StarRating">
                            <ZNode:ProductAverageRating ID="uxReviewRating" runat="server" />
                            | <a id="Hyperlink" runat="server">
                                <asp:Localize ID="Localize5" runat="server" meta:resourceKey="txtWriteaReview"></asp:Localize></a>
                        </div>
                        <div class="Label">
                            <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtITEM"></asp:Localize>:&nbsp;#<asp:Label
                                ID="ProductID" runat="server" EnableViewState="false" meta:resourcekey="ProductIDResource1"></asp:Label>
                        </div>
                        <div id="BrandLabel" runat="server" visible="false">
                            <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtBRAND"></asp:Localize>:&nbsp;
                            <asp:Label ID="lblBrandName" runat="server" EnableViewState="false" Text="Label"  
                                meta:resourcekey="ProductDescriptionResource1"></asp:Label>
                        </div>
                         <div id="VendorName" runat="server" visible="false">
                              <asp:Localize ID="ltrVendorName" runat="server" meta:resourceKey="txtVendorName"></asp:Localize>:&nbsp;
                             <asp:Label ID="lblVendorName" runat="server" EnableViewState="false"></asp:Label>
                        </div>
                        <div class="Description">
                            <asp:Label ID="ProductDescription" EnableViewState="false" runat="server" meta:resourcekey="ProductDescriptionResource1"></asp:Label>
                        </div>
                        
                        <div class="StarRight">
                            <ZNode:ProductHighlights ID="uxHighlights" MaxDisplayColumns="10" runat="server" Visible="False" />
                        </div>                                               
                        <asp:UpdatePanel ID="UpdPnlOrderingOptions" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div class="OrderingOptions">
                                    <ZNode:ProductAttributes ID="uxProductAttributes" runat="server" />
                                    <ZNode:ProductAddOns ID="uxProductAddOns" runat="server" ShowCheckBoxesVertically="true"
                                        ShowRadioButtonsVerically="true"></ZNode:ProductAddOns>
                                    <asp:Panel ID="pnlQty" runat="server" Visible="False" meta:resourcekey="pnlQtyResource1">
                                        <table class="Quantity">
                                            <tr><td class="QuantityOption">
                                                <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtQuantity"></asp:Localize> : </td>
                                                <td><asp:DropDownList OnSelectedIndexChanged="Quantity_SelectedIndexChanged"
                                                    ID="uxQty" Width="136px" runat="server" AutoPostBack="True" meta:resourcekey="uxQtyResource1">
                                                </asp:DropDownList></td></tr>
                                        </table>
                                    </asp:Panel>
                                    <div class="StockMsg">
                                        <asp:Label ID="lblstockmessage" runat="server" CssClass="InStockMsg" meta:resourcekey="lblstockmessageResource1"></asp:Label>
                                    </div>
                                    <div class="PriceContent">
                                        <ZNode:ProductPrice ID="uxProductPrice" runat="server" />
                                        <asp:Image ID="FeaturedItemImage" EnableViewState="false" runat="server" Visible="False" ImageAlign="AbsMiddle"
                                            meta:resourcekey="FeaturedItemImageResource1" />
                                    </div>
                                    <div class="CallForPrice">
                                        <asp:Label ID="uxCallForPricing" EnableViewState="false" runat="server" Text="Call For Pricing" Visible="False"
                                            meta:resourcekey="uxCallForPricingResource1"></asp:Label>
                                    </div>
                                    <div class="SalePrice">
                                        <asp:Literal EnableViewState="False" ID="AdditionalPrice" runat="server" meta:resourcekey="AdditionalPriceResource1"></asp:Literal>
                                    </div>
                                    <div class="StatusMsg">
                                        <asp:Label ID="uxStatus" EnableViewState="False" runat="server" CssClass="OutOfStockMsg"
                                            meta:resourcekey="uxStatusResource1"></asp:Label>
                                    </div>
                                    <div class="BuyDirectFromVendor">
                                        <asp:LinkButton ID="uxBuyDirectFromVendor" runat="server" Visible="false" CssClass="Button"
                                            OnClick="UxBuyDirectFromVendor_Click"
                                            meta:resourcekey="uxBuyDirectFromVendorResource1">Buy Direct From Vendor &rsaquo;&rsaquo;</asp:LinkButton>
                                    </div>
                                    <div class="Error" id="cookiediv">
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div id="BundleProduct">
                            <ZNode:BundleProduct ID="uxBundleProduct" runat="server" />
                        </div>
                        <asp:UpdatePanel ID="pnlAddToCart" runat="server" UpdateMode="Always">
                            <ContentTemplate>
                                <div class="AddToCartButton">
                                    <asp:LinkButton  CssClass="addtocartText" ID="uxAddToCart" EnableViewState="false" runat="server" OnClick="UxAddToCart_Click"
                                         meta:resourcekey="uxAddToCartResource1">Add to Cart &rsaquo;&rsaquo;</asp:LinkButton>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div>
                            <ZNode:Spacer ID="Spacer5" SpacerHeight="25" EnableViewState="false" SpacerWidth="10"
                                runat="server"></ZNode:Spacer>
                        </div>
                        <!--Ajax Tabs-->                         
                        <div class="Tabs">
                            <ZNode:ProductTabs ID="uxProductTabs" runat="server" />
                        </div>
						<asp:Panel ID="pnlFBT" runat="server">
						<div>
							 <ZNode:Spacer ID="Spacer6" SpacerHeight="25" EnableViewState="false" SpacerWidth="10"
                                runat="server"></ZNode:Spacer>
							 <uc1:ProductFrequentlyBought ID="ProductFrequentlyBought" runat="server" ShowName="False" ShowImage="true" ShowDescription="false" />				
						</div>
							</asp:Panel>
                    </div>
                    <!-- Right Content -->
                    <div class="RightContent">
                        <div class="Image">
                            <div>
                                <ZNode:CatalogItemImage ID="CatalogItemImage" runat="server" />
                                <div class="ProductPageNewItem">
                                    <asp:Image ID="NewItemImage" runat="server" Visible="False" meta:resourcekey="NewItemImageResource1" />
                                </div>
                            </div>
                        </div>
                        <asp:Panel ID="pnlSwatches" runat="server" meta:resourcekey="pnlSwatchesResource1">
                            <div class="ProductSwatches">
                                <span class="Text">
                                    <asp:Localize ID="Localize2" runat="server" meta:resourceKey="txtCOLORSOPTIONS"></asp:Localize></span>
                                <ZNode:Swatches ID="uxProductSwatches" IncludeProductImage="true" runat="server"
                                    ControlType="Swatches" />
                            </div>
                        </asp:Panel>
                        <div>
                            <ZNode:ExternalLinks ID="ExternalLinks1" runat="server" />
                        </div>
                        <div id="RelatedTabs">
                            <ajaxToolKit:TabContainer OnClientActiveTabChanged="ActiveTabChanged" runat="server"
                                ID="ProductTabs" CssClass="RelatedProductTabStyle" ScrollBars="Auto" meta:resourcekey="ProductTabsResource1">
                                <ajaxToolKit:TabPanel ID="pnlProductRelated" runat="server">
                                    <HeaderTemplate>
                                        <asp:Localize ID="lblProductRelated" runat="server" meta:resourceKey="lblProductRelated"></asp:Localize></span>
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <div class="CrossSell">
                                            <ZNode:ProductRelated ID="uxProductRelated" runat="server" ShowName="false" ShowImage="true"
                                                ShowDescription="false" />
                                        </div>
                                    </ContentTemplate>
                                </ajaxToolKit:TabPanel>
                                <ajaxToolKit:TabPanel ID="pnlBestSellers" runat="server">
                                    <HeaderTemplate>
                                        <asp:Localize ID="lblBestSellers" runat="server" meta:resourceKey="lblBestSellers"></asp:Localize>
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <ZNode:BestSellers ID="uxBestSellers" runat="server" ShowName="false" ShowImage="true"
                                            ShowDescription="false" />
                                    </ContentTemplate>
                                </ajaxToolKit:TabPanel>
                            </ajaxToolKit:TabContainer>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="uxBuyDirectFromVendor" />
        </Triggers>
    </asp:UpdatePanel>

    <script language="javascript" type="text/javascript">
        document.cookie = 'ZNode' + escape('nothing')
        function ActiveTabChanged(sender, e) {
            PageLoadedEventHandler();
        }

        function detect() {
            document.cookie = 'ZNodeCookie';
            if (document.cookie.indexOf("ZNodeCookie") < 0) {
                alert("You must enable cookies in your browser settings in order to add items to your cart.");
                return false;
            }
            else {
                return true;
            }
        }
    </script>
</div>
