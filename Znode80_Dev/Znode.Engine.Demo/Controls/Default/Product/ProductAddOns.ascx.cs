using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;

namespace Znode.Engine.Demo 
{
    /// <summary>
    /// Represents the Product Addons user control class.
    /// </summary>
    public partial class Controls_Default_Product_ProductAddOns : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct _product;
        private bool _ShowRadioButtonsVerically = false;
        private bool _ShowCheckBoxesVertically = false;
        private int _BundleProductID = 0;
        #endregion

        // Public Event Handler
        public event System.EventHandler SelectedIndexChanged;

        #region Public properties

        /// <summary>
        /// Gets or sets a value indicating whether to show the RadioButtons Vertically
        /// </summary>
        public bool ShowRadioButtonsVerically
        {
            get 
            { 
                return this._ShowRadioButtonsVerically; 
            }

            set 
            { 
               this._ShowRadioButtonsVerically = value; 
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show the checkboxes vertically or not
        /// </summary>
        public bool ShowCheckBoxesVertically
        {
            get 
            { 
                return this._ShowCheckBoxesVertically; 
            }

            set 
            { 
               this._ShowCheckBoxesVertically = value; 
            }
        }

        /// <summary>
        /// Gets or sets a BundleProductID
        /// </summary>
        public int BundleProductID
        {
            get 
            { 
                return this._BundleProductID; 
            }

            set 
            {
                this._BundleProductID = value; 
            }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            if (this._product.ZNodeAddOnCollection.Count > 0)
            {
                pnlAddOns.Visible = true;

                string addOnValues = string.Empty;

                foreach (ZNodeAddOn AddOn in this._product.ZNodeAddOnCollection)
                {
                    string DisplayType = AddOn.DisplayType;

                    if (DisplayType == "RadioButton")
                    {
                        this.BindRadioButtonList(AddOn);
                    }

                    if (DisplayType == "CheckBox")
                    {
                        this.BindCheckBoxList(AddOn);
                    }

                    if (DisplayType == "TextBox")
                    {
                        this.BindTextBoxControl(AddOn);
                    }

                    if (DisplayType == "DropDownList")
                    {
                        // By default we use Dropdownist for AddOns
                        this.BindDropDownList(AddOn);
                    }
                }
            }
            else
            {
                pnlAddOns.Visible = false;
                return;
            }
        }
       
        /// <summary>
        /// Returns current selected addons from the list
        /// </summary>
        /// <param name="status">The Status Value</param>
        /// <param name="Message">The Message Value</param>
        /// <returns>Returns the Selected Addons list</returns>
        public ZNodeAddOnList GetSelectedAddOns(out bool status, out string Message)
        {
            bool retValue = false;
            string statusMessage = string.Empty;

            System.Text.StringBuilder addOnValues = new System.Text.StringBuilder();

            foreach (ZNodeAddOn AddOn in this._product.ZNodeAddOnCollection)
            {
                // Bind RadioButton Control
                if (AddOn.DisplayType == "RadioButton")
                {
                    var rbtnlistControl = new RadioButtonList();
                    rbtnlistControl = (RadioButtonList)ControlsPlaceHolder.FindControl("lstAddOn" + AddOn.AddOnID.ToString());

                    if (addOnValues.Length > 0)
                    {
                        addOnValues.Append(",");
                    }

                    // Loop through the Add-on values for each Add-on
                    foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                    {
                        if (rbtnlistControl.SelectedValue != string.Empty)
                        {
                            int selvalue = int.Parse(rbtnlistControl.SelectedValue);

                            if (selvalue > 0 && AddOnValue.AddOnValueID.ToString() == rbtnlistControl.SelectedValue)
                            {
                                // Check for quantity on hand and back-order,track inventory settings
                                if (AddOnValue.QuantityOnHand <= 0 && AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                                {
                                    statusMessage = this.GetLocalResourceObject("OutOfStock").ToString().Replace("<name>", AddOn.Name);
                                    retValue = true;
                                }

                                // Add to Selected Addon list for this product
                                addOnValues.Append(AddOnValue.AddOnValueID.ToString());
                                break;
                            }
                        }
                    }
                }

                // BindCheckBox
                if (AddOn.DisplayType == "CheckBox")
                {
                    var chkBoxListControl = new CheckBoxList();
                    chkBoxListControl = (CheckBoxList)ControlsPlaceHolder.FindControl("lstAddOn" + AddOn.AddOnID.ToString());

                    if (addOnValues.Length > 0)
                    {
                        addOnValues.Append(",");
                    }

                    // Loop through the Add-on values for each Add-on
                    foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                    {
                        // Loop through the Add-on values for each Add-on
                        foreach (ListItem listCheckedItem in chkBoxListControl.Items)
                        {
                            if (listCheckedItem.Selected)
                            {
                                // Optional Addons are not selected,then leave those addons 
                                // If optinal Addons are selected, it should add with the Selected item 
                                // Check for Selected Addon value 
                                if (AddOnValue.AddOnValueID.ToString() == listCheckedItem.Value)
                                {
                                    // Check for quantity on hand and back-order,track inventory settings
                                    if (AddOnValue.QuantityOnHand <= 0 && AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                                    {
                                        statusMessage = this.GetLocalResourceObject("OutOfStock").ToString().Replace("<name>", AddOn.Name);
                                        retValue = true;
                                    }

                                    // Add to Selected Addon list for this product
                                    addOnValues.Append(AddOnValue.AddOnValueID.ToString());
                                    addOnValues.Append(",");
                                    break;
                                }
                            }
                        }
                    }
                }

                if (AddOn.DisplayType == "DropDownList")
                {
                    // Default DropDownList Control
                    var ddlistControl = new DropDownList();
                    ddlistControl = (DropDownList)ControlsPlaceHolder.FindControl("lstAddOn" + AddOn.AddOnID.ToString());

                    if (addOnValues.Length > 0)
                    {
                        addOnValues.Append(",");
                    }

                    // Loop through the Add-on values for each Add-on
                    foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                    {
                        int selvalue = int.Parse(ddlistControl.SelectedValue);

                        if (selvalue > 0 && AddOnValue.AddOnValueID.ToString() == ddlistControl.SelectedValue)
                        {
                            // Check for quantity on hand and back-order,track inventory settings
                            if (AddOnValue.QuantityOnHand <= 0 && AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                            {
                                statusMessage = this.GetLocalResourceObject("OutOfStock").ToString().Replace("<name>", AddOn.Name);
                                retValue = true;
                            }

                            // Add to Selected Addon list for this product
                            addOnValues.Append(AddOnValue.AddOnValueID.ToString());
                            break;
                        }
                    }
                }
            }

            ZNodeAddOnList _AddOnList = new ZNodeAddOnList();

            if (addOnValues.Length > 0)
            {
                // Get a sku based on attributes selected
                _AddOnList = ZNodeAddOnList.CreateByProductAndAddOns(this._product.ProductID, addOnValues.ToString());
            }

            status = retValue;
            Message = statusMessage;

            return _AddOnList;
        }

        /// <summary>
        /// Validate selected attributes and return a user message
        /// </summary>
        /// <param name="Message">The Value of Message</param>
        /// <param name="SelectedAddOnList">The value of Selected Addon List</param>
        /// <returns>Returns the bool value whether to validate Addons or not</returns>
        public bool ValidateAddOns(out string Message, int shoppingCartQty, out ZNodeAddOnList SelectedAddOnList)
        {
			var existingCartAddonQty = 0;
			var existingCart = ZNodeShoppingCart.CurrentShoppingCart();  
			if (existingCart != null)
			{
				var existingCartItem = new ZNodeShoppingCartItem {Product = new ZNodeProductBase(this._product)};
				existingCartAddonQty = existingCart.GetQuantityAddOnOrdered(existingCartItem);
			}
			
            System.Text.StringBuilder addOnValues = new System.Text.StringBuilder();
            bool status = false;
            SelectedAddOnList = new ZNodeAddOnList();
            StringBuilder stockMessage = new StringBuilder();
            foreach (ZNodeAddOn AddOn in this._product.ZNodeAddOnCollection)
            {
                status = false;

                // Bind RadioButton Control
                if (AddOn.DisplayType == "TextBox")
                {
                    // Loop through the Add-on values for each Add-on
                    foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                    {
                        status = false;

                        TextBox textBoxControl = new TextBox();
                        textBoxControl =
                            (TextBox)
                            ControlsPlaceHolder.FindControl("txtBoxAddOn" + AddOnValue.AddOnValueID.ToString());

                        if (textBoxControl != null &&
                            !string.IsNullOrEmpty(textBoxControl.Text.Trim()))
                        {
                            if (addOnValues.Length > 0)
                            {
                                addOnValues.Append(",");
                            }

                            // Check for quantity on hand and back-order,track inventory settings
                            if ((AddOnValue.QuantityOnHand <= 0 ||
                                 AddOnValue.QuantityOnHand < shoppingCartQty + existingCartAddonQty) &&
                                AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                            {
                                Message = this.GetLocalResourceObject("OutOfStock")
                                              .ToString()
                                              .Replace("<name>", AddOnValue.Name);
                                status = false;

                                return false;
                            }
                            else if (AddOn.AllowBackOrder && AddOnValue.QuantityOnHand < shoppingCartQty)
                            {
                                Message = AddOn.BackOrderMsg;
                                if (!string.IsNullOrEmpty(stockMessage.ToString()))
                                {
                                    stockMessage.Append(", " + Message);
                                }
                                else
                                {
                                    stockMessage.Append(Message);
                                }
                            }

                            status = true;

                            // Add to Selected Addon list for this product
                            addOnValues.Append(AddOnValue.AddOnValueID.ToString());
                        }

                        Image img =
                            (Image)
                            ControlsPlaceHolder.FindControl("Image_AddOnValue" + AddOnValue.AddOnValueID.ToString());

                        if (img != null)
                        {
                            img.Visible = false;
                        }

                        if (!status && !AddOn.OptionalInd)
                        {
                            if (img != null)
                            {
                                img.Visible = true;
                            }

                            Message = "Enter " + AddOnValue.Name;

                            status = false;

                            return status;
                        }
                    }
                }

                // Bind RadioButton Control
                if (AddOn.DisplayType == "RadioButton")
                {
                    var rbtnlistControl = new RadioButtonList();
                    rbtnlistControl =
                        (RadioButtonList) ControlsPlaceHolder.FindControl("lstAddOn" + AddOn.AddOnID.ToString());

                    if (addOnValues.Length > 0)
                    {
                        addOnValues.Append(",");
                    }

                    // Loop through the Add-on values for each Add-on
                    foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                    {
                        // Optional Addons are not selected,then leave those addons 
                        // If optinal Addons are selected, it should add with the Selected item 
                        // Check for Selected Addon value 
                        if (AddOnValue.AddOnValueID.ToString() == rbtnlistControl.SelectedValue)
                        {
                            if (existingCart != null)
                                existingCartAddonQty = existingCart.GetQuantityAddOnOrdered(AddOnValue.AddOnValueID);
                            // Check for quantity on hand and back-order,track inventory settings
                            if ((AddOnValue.QuantityOnHand <= 0 ||
                                 AddOnValue.QuantityOnHand < shoppingCartQty + existingCartAddonQty) &&
                                AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                            {
                                Message = this.GetLocalResourceObject("OutOfStock")
                                              .ToString()
                                              .Replace("<name>", AddOn.Name);

                                return false;
                            }
                            else if (AddOn.AllowBackOrder && AddOnValue.QuantityOnHand < shoppingCartQty)
                            {
                                Message = AddOn.BackOrderMsg;
                                if (!string.IsNullOrEmpty(stockMessage.ToString()))
                                {
                                    stockMessage.Append(", " + Message);
                                }
                                else
                                {
                                    stockMessage.Append(Message);
                                }
                            }

                            status = true;

                            // Add to Selected Addon list for this product
                            addOnValues.Append(AddOnValue.AddOnValueID.ToString());

                            break;
                        }
                    }
                }

                // BindCheckBox
                if (AddOn.DisplayType == "CheckBox")
                {
                    var chkBoxListControl = new CheckBoxList();
                    chkBoxListControl =
                        (CheckBoxList) ControlsPlaceHolder.FindControl("lstAddOn" + AddOn.AddOnID.ToString());

                    if (addOnValues.Length > 0)
                    {
                        addOnValues.Append(",");
                    }

                    foreach (ListItem listCheckedItem in chkBoxListControl.Items)
                    {
                        if (listCheckedItem.Selected)
                        {
                            // Loop through the Add-on values for each Add-on
                            foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                            {
                                // Optional Addons are not selected,then leave those addons 
                                // If optinal Addons are selected, it should add with the Selected item 
                                // Check for Selected Addon value 
                                if (AddOnValue.AddOnValueID.ToString() == listCheckedItem.Value)
                                {
                                    if (existingCart != null)
                                        existingCartAddonQty =
                                            existingCart.GetQuantityAddOnOrdered(AddOnValue.AddOnValueID);
                                    // Check for quantity on hand and back-order,track inventory settings
                                    if ((AddOnValue.QuantityOnHand <= 0 ||
                                         AddOnValue.QuantityOnHand < shoppingCartQty + existingCartAddonQty) &&
                                        AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                                    {
                                        Message = this.GetLocalResourceObject("OutOfStock")
                                                      .ToString()
                                                      .Replace("<name>", AddOn.Name);

                                        return false;
                                    }
                                    else if (AddOn.AllowBackOrder && AddOnValue.QuantityOnHand < shoppingCartQty)
                                    {
                                        Message = AddOn.BackOrderMsg;
                                        if (!string.IsNullOrEmpty(stockMessage.ToString()))
                                        {
                                            stockMessage.Append(", " + Message);
                                        }
                                        else
                                        {
                                            stockMessage.Append(Message);
                                        }
                                    }

                                    status = true;

                                    // Add to Selected Addon list for this product
                                    addOnValues.Append(AddOnValue.AddOnValueID.ToString());
                                    addOnValues.Append(",");
                                    break;
                                }
                            }
                        }
                    }
                }

                if (AddOn.DisplayType == "DropDownList")
                {
                    // Default DropDownList Control
                    var ddlistControl = new DropDownList();
                    ddlistControl =
                        (DropDownList) ControlsPlaceHolder.FindControl("lstAddOn" + AddOn.AddOnID.ToString());

                    if (addOnValues.Length > 0)
                    {
                        addOnValues.Append(",");
                    }

                    // Loop through the Add-on values for each Add-on
                    foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                    {
                        // Optional Addons are not selected,then leave those addons 
                        // If optinal Addons are selected, it should add with the Selected item

                        // Check for Selected Addon value
                        if (AddOnValue.AddOnValueID.ToString() == ddlistControl.SelectedValue)
                        {
                            if (existingCart != null)
                                existingCartAddonQty = existingCart.GetQuantityAddOnOrdered(AddOnValue.AddOnValueID);
                            // Check for quantity on hand and back-order,track inventory settings
                            if ((AddOnValue.QuantityOnHand <= 0 ||
                                 AddOnValue.QuantityOnHand < shoppingCartQty + existingCartAddonQty) &&
                                AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                            {
                                Message = this.GetLocalResourceObject("OutOfStock")
                                              .ToString()
                                              .Replace("<name>", AddOn.Name);

                                return false;
                            }
                            else if (AddOn.AllowBackOrder &&
                                     AddOnValue.QuantityOnHand < shoppingCartQty + existingCartAddonQty)
                            {
                                Message = AddOn.BackOrderMsg;
                                if (!string.IsNullOrEmpty(stockMessage.ToString()))
                                {
                                    stockMessage.Append(", " + Message);
                                }
                                else
                                {
                                    stockMessage.Append(Message);
                                }
                            }

                            status = true;

                            // Add to Selected Addon list for this product
                            addOnValues.Append(AddOnValue.AddOnValueID.ToString());

                            break;
                        }
                    }
                }

                if (string.Compare(AddOn.DisplayType, "TextBox") != 0)
                {
                    Image img = (Image) ControlsPlaceHolder.FindControl("Image_AddOn" + AddOn.AddOnID.ToString());

                    if (img != null)
                    {
                        img.Visible = false;
                    }

                    if (!status && !AddOn.OptionalInd)
                    {
                        if (img != null)
                        {
                            img.Visible = true;
                        }

                        Message = "Select " + AddOn.Name;

                        if (AddOn.DisplayType.Equals("TextBox", StringComparison.OrdinalIgnoreCase))
                        {
                            Message = "Enter " + AddOn.Name;
                        }

                        return false;
                    }
                }
            }

            if (addOnValues.Length > 0)
            {
                // get a add-on values based on Add-ons selected
                SelectedAddOnList = ZNodeAddOnList.CreateByProductAndAddOns(this._product.ProductID, addOnValues.ToString());
            }

            foreach (ZNodeAddOn addOn in SelectedAddOnList.ZNodeAddOnCollection)
            {
                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnValueEntity addOnvalue in addOn.AddOnValueCollection)
                {
                    TextBox textBoxControl = new TextBox();
                    textBoxControl = (TextBox)ControlsPlaceHolder.FindControl("txtBoxAddOn" + addOnvalue.AddOnValueID.ToString());

                    if (textBoxControl != null)
                    {
                        addOnvalue.CustomText = textBoxControl.Text.Trim();
                    }
                }
            }

            SelectedAddOnList.SelectedAddOnValueIds = addOnValues.ToString();

            if (string.IsNullOrEmpty(stockMessage.ToString()))
            {
                Message = string.Empty;
            }
            else
            {
                Message = stockMessage.ToString();
            }
            return true;
        }

        #endregion

        #region Events
        /// <summary>
        /// validate selected addons and returns the Inventory related messages
        /// </summary>
        /// <param name="AddOn">Addon instance</param>
        /// <param name="AddOnValue">AddonValue Instance</param>
        /// <returns>Returns the Status message to bind</returns>
        protected string BindStatusMsg(ZNodeAddOn AddOn, ZNodeAddOnValue AddOnValue)
        {
            int qty = 1;

            // Don't track Inventory - Items can always be added to the cart,TrackInventory is disabled
            if (!AddOn.AllowBackOrder && !AddOn.TrackInventoryInd)
            {
                if (AddOnValue.QuantityOnHand <= 0)
                {
                    return string.Empty;
                }
                else
                {
                    return AddOn.InStockMsg;
                }
            }
            else if (AddOnValue.QuantityOnHand < qty && AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
            {
                // If quantity available is less and track inventory is enabled
                return AddOn.OutOfStockMsg;
            }
            else if (AddOnValue.QuantityOnHand < qty && AddOn.AllowBackOrder == true && AddOn.TrackInventoryInd)
            {
                return AddOn.BackOrderMsg;
            }
            else if (AddOn.TrackInventoryInd && AddOnValue.QuantityOnHand >= qty)
            {
                return AddOn.InStockMsg;
            }

            return string.Empty;
        }

        /// <summary>
        /// Event Handler for the dynamic control DropDownList
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Controls_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.SelectedIndexChanged != null)
            {
                this.SelectedIndexChanged(sender, e);
            }
        }

        #endregion

        #region Bind Dynamic Controls

        /// <summary>
        /// Bind the Addon in DropDown Control
        /// </summary>
        /// <param name="AddOn">Addon Instance</param>
        protected void BindTextBoxControl(ZNodeAddOn AddOn)
        {
            // Don't display list box if there is no add-on values for AddOns
            if (AddOn.AddOnID > 0)
            {
                foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                {
                    ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='Option'>"));

                    TextBox txtBox = new TextBox();
                    txtBox.ID = "txtBoxAddOn" + AddOnValue.AddOnValueID.ToString();
                    txtBox.CssClass = "AddOnTextBox";
                    txtBox.Text = AddOnValue.Description;

                    txtBox.AutoPostBack = true;
                    txtBox.TextChanged += new EventHandler(this.Controls_SelectedIndexChanged);

                    // Add controls to the place holder
                    Literal lit1 = new Literal();
                    lit1.Text += "<table class=\"Option\">";

                    ControlsPlaceHolder.Controls.Add(lit1);

                    RegularExpressionValidator validator = new RegularExpressionValidator();
                    validator.CssClass = "AddOnValidator";
                    validator.ControlToValidate = txtBox.ID;
                    validator.ErrorMessage = "Invalid";
                    validator.Display = System.Web.UI.WebControls.ValidatorDisplay.Dynamic;
                    validator.ValidationExpression = @"^[a-zA-Z0-9_.@\s]+$";

                    ControlsPlaceHolder.Controls.Add(validator);
                    Literal ltrl = new Literal();
                    ltrl.Text = "<tr><td class='OptionLabel'>" + AddOnValue.Name + " :</td>";

                    ControlsPlaceHolder.Controls.Add(ltrl);

                    Literal lt = new Literal();
                    lt.Text = "<td class='OptionValue'>";

                    ControlsPlaceHolder.Controls.Add(lt);
                    ControlsPlaceHolder.Controls.Add(txtBox);

                    Literal literal = new Literal();
                    literal.Text = "</td>";

                    ControlsPlaceHolder.Controls.Add(literal);

                    if (!AddOn.OptionalInd)
                    {
                        ControlsPlaceHolder.Controls.Add(new LiteralControl("<td>"));
                        Image img = new Image();
                        img.ID = "Image_AddOnValue" + AddOnValue.AddOnValueID;
                        img.ImageAlign = ImageAlign.AbsMiddle;
                        img.ImageUrl = ResolveUrl(ZNodeCatalogManager.GetImagePathByLocale("Required.gif"));
                        img.Visible = false;

                        ControlsPlaceHolder.Controls.Add(img);
                        ControlsPlaceHolder.Controls.Add(new LiteralControl("</td>"));
                    }
                    ControlsPlaceHolder.Controls.Add(new LiteralControl("</tr></table>"));

                    ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
                }
            }
        }

        /// <summary>
        /// Bind the Addon in DropDown Control
        /// </summary>
        /// <param name="AddOn">Addon Instance</param>
        protected void BindDropDownList(ZNodeAddOn AddOn)
        {
            // Don't display list box if there is no add-on values for AddOns
            if (AddOn.ZNodeAddOnValueCollection.Count > 0)
            {
                ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='DropDownOption'>"));
                var ddlistControl = new DropDownList();
                ddlistControl.ID = "lstAddOn" + AddOn.AddOnID.ToString();
                ddlistControl.AutoPostBack = true;
                ddlistControl.CssClass = "AddOnDropDown";

                if (AddOn.OptionalInd)
                {
                    ListItem OptionalItem = new ListItem("Select " + AddOn.Title + " - Optional", "0");
                    ddlistControl.Items.Insert(0, OptionalItem);
                    ddlistControl.SelectedValue = "0";
                }
                else
                {
                    // Added the Label as title
                    ListItem li = new ListItem("Select " + AddOn.Title, "0");
                    ddlistControl.Items.Add(li);
                }

                foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                {
                    string AddOnValueName = AddOnValue.Name;

                    // Added Inventory Message with the Addon Value Name in the dropdownlist
                    AddOnValueName += " " + this.BindStatusMsg(AddOn, AddOnValue);

                    ListItem li1 = new ListItem(AddOnValueName, AddOnValue.AddOnValueID.ToString());
                    ddlistControl.Items.Add(li1);

                    // Set Add-on is Default.
                    if (AddOnValue.IsDefault && !AddOn.OptionalInd && !IsPostBack)
                    {
                        ddlistControl.SelectedValue = AddOnValue.AddOnValueID.ToString();
                    }
                }

                ddlistControl.SelectedIndexChanged += new EventHandler(this.Controls_SelectedIndexChanged);

                Literal lit1 = new Literal();
                lit1.Text += "<table class=\"Option\">";

                ControlsPlaceHolder.Controls.Add(lit1);

                Literal ltrl = new Literal();
                ltrl.Text = "<tr><td class='DropDownTitle'>" + AddOn.Title + " :</td>";

                ControlsPlaceHolder.Controls.Add(ltrl);

                Literal lt = new Literal();
                lt.Text = "<td class='DropDownValue'>";

                ControlsPlaceHolder.Controls.Add(lt);
                ControlsPlaceHolder.Controls.Add(ddlistControl); // Dropdown list control

                Literal literal = new Literal();
                literal.Text = "</td>";
                ControlsPlaceHolder.Controls.Add(literal);

                if (!AddOn.OptionalInd)
                {
                    ControlsPlaceHolder.Controls.Add(new LiteralControl("<td>"));
                    Image img = new Image();
                    img.ID = "Image_AddOn" + AddOn.AddOnID;
                    img.ImageAlign = ImageAlign.AbsMiddle;
                    img.ImageUrl = ResolveUrl(ZNodeCatalogManager.GetImagePathByLocale("Required.gif"));
                    img.Visible = false;

                    ControlsPlaceHolder.Controls.Add(img);
                    ControlsPlaceHolder.Controls.Add(new LiteralControl("</td>"));
                }
                ControlsPlaceHolder.Controls.Add(new LiteralControl("</tr></table>"));
                ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
            }
        }

        /// <summary>
        /// Bind the Addon in RadioButton Control
        /// </summary>
        /// <param name="AddOn">Addon Instance</param>
        protected void BindRadioButtonList(ZNodeAddOn AddOn)
        {
            // Don't display list box if there is no add-on values for AddOns
            if (AddOn.ZNodeAddOnValueCollection.Count > 0)
            {
                ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='RadioButtonOption'>"));

                var rbtnlistControl = new RadioButtonList();

                rbtnlistControl.ID = "lstAddOn" + AddOn.AddOnID.ToString();
                rbtnlistControl.AutoPostBack = true;

                rbtnlistControl.SelectedIndexChanged += new EventHandler(this.Controls_SelectedIndexChanged);

                // Set Vertical Or Horizontal view
                if (this.ShowRadioButtonsVerically)
                {
                    rbtnlistControl.RepeatDirection = RepeatDirection.Vertical;
                }
                else
                {
                    rbtnlistControl.RepeatDirection = RepeatDirection.Horizontal;
                }

                rbtnlistControl.TextAlign = TextAlign.Left;
                rbtnlistControl.CellPadding = 0;
                rbtnlistControl.CellSpacing = 0;

                if (AddOn.OptionalInd)
                {
                    // Add controls to the place holder
                    Literal litOptional = new Literal();
                    litOptional.Text = "<table><tr><td class='RadioButtonTitle'>Select " + AddOn.Title + " - Optional</td>";
                    ControlsPlaceHolder.Controls.Add(litOptional);
                }
                else
                {
                    // Add controls to the place holder
                    Literal litrl = new Literal();
                    litrl.Text = "<table><tr><td class='RadioButtonTitle'>Select " + AddOn.Title + "</td>";
                    ControlsPlaceHolder.Controls.Add(litrl);
                }

                foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                {
                    string AddOnValueName = AddOnValue.Name;

                    // Added Inventory Message with the Addon Value Name in the dropdownlist
                    AddOnValueName += " " + this.BindStatusMsg(AddOn, AddOnValue);

                    ListItem li1 = new ListItem(AddOnValueName, AddOnValue.AddOnValueID.ToString());
                    rbtnlistControl.Items.Add(li1);

                    // Set Add-on is Default.
                    if (AddOnValue.IsDefault && !AddOn.OptionalInd && !IsPostBack)
                    {
                        rbtnlistControl.SelectedValue = AddOnValue.AddOnValueID.ToString();
                    }
                }

                if (AddOn.OptionalInd)
                {
                    ListItem li = new ListItem("None", "0");
                    rbtnlistControl.Items.Add(li);
                }

                // Add controls to the place holder
                Literal lit1 = new Literal();
                lit1.Text = "<td class='RadioButtonValue'>";

                // Set Width here.               
                // rbtnlistControl.Width = 165;
                ControlsPlaceHolder.Controls.Add(lit1);
                ControlsPlaceHolder.Controls.Add(rbtnlistControl); // Dropdown list control

                Literal lit3 = new Literal();
                lit3.Text = "</td></tr></table>";

                ControlsPlaceHolder.Controls.Add(lit3);
                ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
            }
        }

        /// <summary>
        /// Bind the Addon in CheckBox Control
        /// </summary>
        /// <param name="AddOn">Addon Instance</param>
        protected void BindCheckBoxList(ZNodeAddOn AddOn)
        {
            // Don't display list box if there is no add-on values for AddOns
            if (AddOn.ZNodeAddOnValueCollection.Count > 0)
            {
                ControlsPlaceHolder.Controls.Add(new LiteralControl("<div class='CheckBoxOption'>"));

                var chkboxlistControl = new CheckBoxList();

                chkboxlistControl.ID = "lstAddOn" + AddOn.AddOnID.ToString();
                chkboxlistControl.AutoPostBack = true;

                chkboxlistControl.SelectedIndexChanged += new EventHandler(this.Controls_SelectedIndexChanged);

                // Set Vertical Or Horizontal view
                if (this.ShowCheckBoxesVertically)
                {
                    chkboxlistControl.RepeatDirection = RepeatDirection.Vertical;
                }
                else
                {
                    chkboxlistControl.RepeatDirection = RepeatDirection.Horizontal;
                }

                chkboxlistControl.TextAlign = TextAlign.Left;
                chkboxlistControl.CellPadding = 0;
                chkboxlistControl.CellSpacing = 0;

                if (AddOn.OptionalInd)
                {
                    // Add controls to the place holder
                    Literal litOptional = new Literal();
                    litOptional.Text = "<table><tr><td class='CheckBoxTitle'>Select " + AddOn.Title + " - Optional</td>";
                    ControlsPlaceHolder.Controls.Add(litOptional);
                }
                else
                {
                    // Add controls to the place holder
                    Literal litrl = new Literal();
                    litrl.Text = "<table><tr><td class='CheckBoxTitle'>Select " + AddOn.Title + "</td>";
                    ControlsPlaceHolder.Controls.Add(litrl);
                }

                foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                {
                    string AddOnValueName = AddOnValue.Name;

                    // Added Inventory Message with the Addon Value Name in the dropdownlist
                    AddOnValueName += " " + this.BindStatusMsg(AddOn, AddOnValue);

                    ListItem li1 = new ListItem(AddOnValueName, AddOnValue.AddOnValueID.ToString());
                    chkboxlistControl.Items.Add(li1);

                    // Set Add-on is Default.
                    if (AddOnValue.IsDefault && !AddOn.OptionalInd)
                    {
                        chkboxlistControl.SelectedValue = AddOnValue.AddOnValueID.ToString();
                    }
                }

                // Add controls to the place holder
                Literal lit1 = new Literal();
                lit1.Text = "<td class='CheckBoxValue'>";

                // Set Width here.chkboxlistControl.Width = 165;
                ControlsPlaceHolder.Controls.Add(lit1);
                ControlsPlaceHolder.Controls.Add(chkboxlistControl); // Dropdown list control

                Literal lit3 = new Literal();
                lit3.Text = "</td></tr></table>";

                ControlsPlaceHolder.Controls.Add(lit3);
                ControlsPlaceHolder.Controls.Add(new LiteralControl("</div>"));
            }
        }

        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            if (this._BundleProductID > 0)
            {               
                    this._product = ZNodeProduct.Create(this._BundleProductID);
               
            }

            this.Bind();

            if (!IsPostBack)
            {
                bool ret = false;
                string msg = string.Empty;
                if (this._BundleProductID > 0)
                {
                    ZNodeProduct product = (ZNodeProduct)HttpContext.Current.Items["Product"];

                    for (int idx = 0; idx < product.ZNodeBundleProductCollection.Count; idx++)
                    {
                        if (product.ZNodeBundleProductCollection[idx].ProductID == this.BundleProductID)
                        {
                            product.ZNodeBundleProductCollection[idx].SelectedAddOns = this.GetSelectedAddOns(out ret, out msg);
                        }
                    }
                }
                else
                {
                    this._product.SelectedAddOnItems = this.GetSelectedAddOns(out ret, out msg);
                }
            }
        }
        #endregion
    }
}