﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the CatalogImage user control class.
    /// </summary>
    public partial class Controls_Default_Product_CatalogImage : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct _Product;
        private int _ProductId = 0;
        #endregion

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this._ProductId = int.Parse(Request.Params["zpid"]);
            }
            else
            {
                throw new ApplicationException("Invalid Product Id");
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this._Product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            this.Bind();
        }

        /// <summary>
        ///  Represents the Bind Method
        /// </summary>
        protected void Bind()
        {
            if (this._Product != null)
            {
                string element = "<div class=\"CatalogImage_element\">{0}{1}</div>";

                uxPlaceHolder.Controls.Add(ParseControl(string.Format(element, "<img border='0'  EnableViewState='false' ID='CatalogItemImage' alt=\"" + this._Product.ImageAltTag + "\" src=\"" + ResolveUrl(this._Product.MediumImageFilePath) + "\" Title=\"" + this._Product.ImageAltTag + "\" />", "<link rel='image_src' href=\"" + ResolveUrl(this._Product.MediumImageFilePath) + "\" />")));

                ZNode.Libraries.DataAccess.Service.ProductImageService service = new ZNode.Libraries.DataAccess.Service.ProductImageService();

                // AlternateImages
                TList<ProductImage> productImages = service.GetByProductIDActiveIndProductImageTypeID(this._Product.ProductID, true, 1);

                productImages.Filter = "ReviewstateId = 20";

                productImages.Sort("DisplayOrder");

                ZNodeImage znodeImage = new ZNodeImage();

                foreach (ProductImage productImage in productImages)
                {
                    uxPlaceHolder.Controls.Add(ParseControl(string.Format(element, "<img border='0' runat='server'  EnableViewState='false' alt='" + this.GetImageAltTag(productImage.ImageAltTag, this._Product.Name) + "' src=\"" + znodeImage.GetImageHttpPathMedium(productImage.ImageFile) + "\" Title=\"" + this.GetImageAltTag(productImage.ImageAltTag, this._Product.Name) + "\" />", "<link rel='image_src' href=\"" + znodeImage.GetImageHttpPathMedium(productImage.ImageFile) + "\" />")));
                    
                }
            }
        }

        /// <summary>
        /// Get product image alternative text
        /// </summary>
        /// <param name="AltTagText">Alternate Image Tag Text</param>
        /// <param name="ProductName">Name of the product</param>
        /// <returns>Returns the Alternate Image Tag</returns>
        protected string GetImageAltTag(string AltTagText, string ProductName)
        {
            if (string.IsNullOrEmpty(AltTagText))
            {
                return ProductName;
            }
            else
            {
                return AltTagText;
            }
        }
    }
}