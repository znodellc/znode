<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_StoreLocator_StoreLocator"
    CodeBehind="StoreLocator.ascx.cs" %>
<asp:UpdatePanel runat="server" ID="updatePnlStoreLocator">
<ContentTemplate>
<div class="StoreLocator">
    <div class="PageTitle">
        <asp:Localize ID="txtStoreLocatorTitle" meta:resourceKey="txtStoreLocatorTitle" runat="server" />
    </div>
    <p>
        <asp:Localize ID="txtStoreLocatorHintText" meta:resourceKey="txtStoreLocatorHintText"
            runat="server" />
    </p>
    <div class="Form">

        <asp:Panel ID="pnlStoreLocator" runat="server" DefaultButton="ibSearch">
            <div class="FieldStyle LeftContent">
                <asp:Localize ID="lblZip" runat="server" meta:resourceKey="lblZip" />
            </div>
            <div class="FieldStyle">
                <asp:TextBox ID="txtZipcode" runat="server" Width="140px" MaxLength="6" ValidationGroup="dropdown" />
                <div>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator" runat="server" ValidationGroup="dropdown"
                        meta:resourcekey="RegularExpressionValidator" ControlToValidate="txtZipcode"
                        ValidationExpression="\d+" Display="Dynamic"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="Clear">
            </div>
            <div class="FieldStyle LeftContent">
                <asp:Localize ID="tdSearchMessage" runat="server" meta:resourcekey="tdSearch" />
            </div>
            <div class="FieldStyle">
                <asp:DropDownList ID="ddlRadiusinMiles" runat="server" Font-Size="Small" CausesValidation="True"
                    ValidationGroup="dropdown" Width="145px">
                    <asp:ListItem Value="0" meta:resourceKey="liOne" />
                    <asp:ListItem Value="5" meta:resourceKey="liTwo" />
                    <asp:ListItem Value="10" meta:resourceKey="liThree" />
                    <asp:ListItem Value="25" meta:resourceKey="liFour" />
                    <asp:ListItem Value="50" meta:resourceKey="liFive" />
                    <asp:ListItem Value="75" meta:resourceKey="liSix" />
                    <asp:ListItem Value="100" meta:resourceKey="liSeven" />
                </asp:DropDownList>
            </div>
            <!--Advanced Search -->
            <div class="Clear">
            </div>
            <div class="FieldStyle LeftContent">
                <asp:Localize ID="lblCity" runat="server" meta:resourceKey="lblCity" />
            </div>
            <div class="FieldStyle">
                <asp:TextBox ID="txtCity" Width="140px" runat="server" />
            </div>
            <div class="Clear">
            </div>
            <div class="FieldStyle LeftContent">
                <asp:Localize ID="lblState" runat="server" meta:resourceKey="lblState" />
            </div>
            <div class="FieldStyle">
                <asp:TextBox ID="txtState" runat="server" Width="140px" MaxLength="2" />
            </div>
            <div class="Clear">
            </div>
            <div class="FieldStyle LeftContent">
                <asp:Localize ID="lblAreaCode" runat="server" meta:resourceKey="lblAreaCode" />
            </div>
            <div class="FieldStyle">
                <asp:TextBox ID="txtAreaCode" runat="server" Width="140px" MaxLength="3" />
            </div>
            <div class="Clear">
            </div>
            <div class="FieldStyle LeftContent">
            </div>
            <div class="FieldStyle" style="text-align: left;">
	            <asp:Button  runat="server" ID="ibSearch" OnClick="Search_Click" ValidationGroup="dropdown" Font-Bold="True"
	                        CssClass="button" meta:resourceKey="btnSubmit"/>
				 &nbsp;
                <asp:LinkButton ID="ibClear" runat="server" CausesValidation="False" OnClick="Clear_Click"
                    CssClass="Button" meta:resourceKey="btnCancel" />
            </div>
            <br />
        </asp:Panel>
    </div>
    <div class="Row Error">
        <asp:Label ID="lblMsg" runat="server"></asp:Label>
    </div>
    <br />
    <asp:Panel ID="PnlStorelist" runat="server">
        <div class="StoreList">
            <div id="TopLink" runat="server">
                <asp:LinkButton ID="TopPrevLink" Text="<%$ Resources:CommonCaption, Prev%>" runat="server"
                    OnClick="PrevRecord"></asp:LinkButton>
                <asp:Localize ID="lblTopPaging" runat="server" Text="| <%$ Resources:CommonCaption, Page%>
                <%= ncurrentpage.ToString() %>
                <%$ Resources:CommonCaption, of%>
                <%= nRecCount.ToString() %>
                ( <%$ Resources:CommonCaption, Total%>
                <%=TotalRecords.ToString() %>
                <%$ Resources:CommonCaption, Item%> ) |" />
                <asp:LinkButton ID="TopNextLink" Text="<%$ Resources:CommonCaption, Next%>" OnClick="NextRecord"
                    runat="server"></asp:LinkButton>
            </div>
            <br />
            <asp:DataList ID="DataListStores" EnableViewState="false" runat="server" BackColor="#eff3f6"
                CellPadding="5" CellSpacing="10" RepeatColumns="1" Width="450px">
                <ItemTemplate>
                    <div class="TextContent">
                        <div class="Title">
                            <%# "<a target='_blank' href='" + DataBinder.Eval(Container.DataItem, "MapQuestURL").ToString() + "'>"%>
                            <%# DataBinder.Eval(Container.DataItem, "Name")%></a>
                        </div>
                        <div class="Address">
                            <%# DataBinder.Eval(Container.DataItem, "Address1")%><br>
                            <%# DataBinder.Eval(Container.DataItem, "Address2")%>
                        </div>
                        <div class="Address">
                            <%# DataBinder.Eval(Container.DataItem, "Address3")%>
                        </div>
                        <div class="Address">
                            <%# DataBinder.Eval(Container.DataItem, "City")%>,&nbsp;<%# DataBinder.Eval(Container.DataItem, "State")%>&nbsp;-&nbsp;<%# DataBinder.Eval(Container.DataItem, "Zip")%><br />
                            <b>Phone: </b>
                            <%# DataBinder.Eval(Container.DataItem, "Phone")%><br />
                            <b>Fax: </b>
                            <%# DataBinder.Eval(Container.DataItem, "Fax")%>
                        </div>
                        <div class="MapLink">
                            <%# "<a target='_blank' href='" + DataBinder.Eval(Container.DataItem, "MapQuestURL") +  "'>"%>
                            map and directions
                            <img src='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogHelper.GetCatalogImagePath("bullet") %>'
                                align="absbottom" runat="server" border="0" alt="" /></a>
                        </div>
                    </div>
                    <div class="ImageContent">
                        <img id="Img1" alt="IMG1" border='0' src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile"))%> '
                            visible='<%# IsImageExists(DataBinder.Eval(Container.DataItem, "ImageFile")) %>'
                            runat="server" />
                    </div>
                </ItemTemplate>
                <ItemStyle BackColor="White" VerticalAlign="Top" />
            </asp:DataList>
            <br />
            <div id="BottomLink" runat="server">
                <asp:LinkButton ID="BotPrevLink" runat="server" OnClick="PrevRecord" Text="<%$ Resources:CommonCaption, Prev%>"></asp:LinkButton>
                <asp:Localize ID="lblBottomPaging" runat="server" Text="| <%$ Resources:CommonCaption, Page%>
                <%= ncurrentpage.ToString() %>
                <%$ Resources:CommonCaption, of%>
                <%= nRecCount.ToString() %>
                (<%$ Resources:CommonCaption, Total%>
                <%=TotalRecords.ToString() %>
                <%$ Resources:CommonCaption, Item%> ) |" />
                <asp:LinkButton ID="BotNextLink" OnClick="NextRecord" runat="server" Text="<%$ Resources:CommonCaption, Next%>"></asp:LinkButton>
            </div>
        </div>
    </asp:Panel>
</div>
	</ContentTemplate>
	</asp:UpdatePanel>
