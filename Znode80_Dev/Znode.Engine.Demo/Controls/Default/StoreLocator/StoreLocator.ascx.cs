using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Store Locator user control class
    /// </summary>
    public partial class Controls_Default_StoreLocator_StoreLocator : System.Web.UI.UserControl
    {
        #region Private Variables
        private static int TotalRecords = 0;
        private static int RecCount = 0;
        private static int Currentpage = 0;
        private string _noRecordsText = string.Empty;
        private int CurrentPage = 0;
        private ZNodeImage znodeImage = new ZNodeImage();
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Search_Click(object sender, EventArgs e)
        {
            this.BindData();
        }

        /// <summary>
        /// Clear button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Clear_Click(object sender, EventArgs e)
        {
            txtZipcode.Text = string.Empty;
            ddlRadiusinMiles.SelectedIndex = 0;
            lblMsg.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtState.Text = string.Empty;
            txtAreaCode.Text = string.Empty;
            PnlStorelist.Visible = false;
        }
        #endregion

        #region Page Events
        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ZNodeResourceManager resourceManager = new ZNodeResourceManager();

            // Set SEO Properties
            ZNodeSEO seo = new ZNodeSEO();
            seo.SEOTitle = resourceManager.GetLocalResourceObject(txtStoreLocatorTitle.TemplateControl.AppRelativeVirtualPath, "txtStoreLocatorTitle.Text");
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            PnlStorelist.Visible = false;
        }

        #endregion

        #region Bind
        /// <summary>
        /// Bind Search stores
        /// </summary>
        private void BindData()
        {
            string Zipcode = string.Empty;
            int InRadius = 0;
            string StateAbbr = string.Empty;
            string City = string.Empty;
            string Areacode = string.Empty;
            bool IsValue = false;
                       
            Zipcode = txtZipcode.Text;
            if (ddlRadiusinMiles.SelectedIndex != 0)
            {
                InRadius = Convert.ToInt32(ddlRadiusinMiles.SelectedItem.Value);
            }

            City = txtCity.Text;
            StateAbbr = txtState.Text;
            Areacode = txtAreaCode.Text;

            // Set Validation for Miles calculation.
            if (ddlRadiusinMiles.SelectedItem.Value != "0")
            {
                if (txtZipcode.Text.Length != 0)
                {
                    IsValue = true;
                }
                else
                {
                    IsValue = false;
                }
            }
            else
            {
                IsValue = true;
            }

            if (IsValue)
            {
                ZNodeStoreList _store = new ZNodeStoreList();

                int count = _store.GetZipCodeCount();

                if (count > 0)
                {
                    ZNodeStoreList storesList = ZNodeStoreList.SearchByZipCodeAndRadius(Zipcode, InRadius, City, StateAbbr, Areacode, ZNodeConfigManager.SiteConfig.PortalID);

                    if (storesList.StoreCollection.Count == 0)
                    {
                        lblMsg.Text = this.GetLocalResourceObject("NotFound").ToString();
                        PnlStorelist.Visible = false;
                    }
                    else
                    {
                        lblMsg.Text = string.Empty;
                        
                        // Set Dataset object to Viewstate
                        ViewState["StoreList"] = storesList;
                        
                        // Set initial page value
                        ViewState["CurrentPage"] = 0;
                        this.BindDataListPagedDataSource();
                        PnlStorelist.Visible = true;
                    }
                }
                else
                {
                    lblMsg.Text = this.GetLocalResourceObject("ZipCodeNotUploaded").ToString();
                }
            }
            else
            {
                lblMsg.Text = this.GetLocalResourceObject("ZipCodeRequired").ToString();
                PnlStorelist.Visible = false;
            }
        }

        #endregion

        #region Bind Paging Methods

        private void BindDataListPagedDataSource()
        {
            // Declare dataset
            ZNodeStoreList StoreList = new ZNodeStoreList();

            // Retrieve dataset from Viewstate
            if (ViewState["StoreList"] != null)
            {
                StoreList = (ZNodeStoreList)ViewState["StoreList"];
            }

            // Creating an object for the 'PagedDataSource' for holding the data.
            PagedDataSource objPage = new PagedDataSource();

            // Assigning the datasource to the 'objPage' object.
            objPage.DataSource = StoreList.StoreCollection;

            // Enable paging
            objPage.AllowPaging = true;

            // Set Paging Size.
            objPage.PageSize = 10;

            // "CurrentPage" is public static variable to hold the current page index value declared in the global section.
            objPage.CurrentPageIndex = int.Parse(ViewState["CurrentPage"].ToString());

            // Checking for enabling/disabling next/prev buttons.
            // Next/prev buton will be disabled when is the last/first page of the pageobject.
            BotNextLink.Enabled = !objPage.IsLastPage;
            TopNextLink.Enabled = !objPage.IsLastPage;
            TopPrevLink.Enabled = !objPage.IsFirstPage;
            BotPrevLink.Enabled = !objPage.IsFirstPage;

            // Set current page index
            Currentpage = objPage.CurrentPageIndex + 1;
            RecCount = objPage.PageCount;
            TotalRecords = objPage.DataSourceCount;

            // Assigning Datasource to the DataList.
            DataListStores.DataSource = objPage;
            DataListStores.DataBind();

            this.DisplayCount();
        }

        /// <summary>
        /// Previous Link Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void PrevRecord(object sender, EventArgs e)
        {
            this.CurrentPage = int.Parse(ViewState["CurrentPage"].ToString());
            ViewState["CurrentPage"] = this.CurrentPage -= 1;

            this.BindDataListPagedDataSource();
            PnlStorelist.Visible = true;
            this.DisplayCount();
        }

        /// <summary>
        /// Next Link Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void NextRecord(object sender, EventArgs e)
        {
            this.CurrentPage = int.Parse(ViewState["CurrentPage"].ToString());
            ViewState["CurrentPage"] = this.CurrentPage += 1;

            this.BindDataListPagedDataSource();
            PnlStorelist.Visible = true;
            this.DisplayCount();
        }

        /// <summary>
        /// Get http image path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the image file path with name.</returns>
        protected string GetImagePath(object imageFileName)
        {
            if (imageFileName != null && imageFileName.ToString().Length > 0)
            {
                ZNodeImage znodeImage = new ZNodeImage();
                return znodeImage.GetImageHttpPathSmall(imageFileName.ToString());
            }
            else
                return string.Empty;
        }

        /// <summary>
        /// Checks the imagefile exists or not
        /// </summary>
        /// <param name="Imagefile">Image File</param>
        /// <returns>Returns true if Image Exists otherwise false</returns>
        protected bool IsImageExists(object Imagefile)
        {
            if (Imagefile == null)
            {
                return false;
            }

            return true;
        }

        private void DisplayCount()
        {
            lblTopPaging.Text = string.Format("{0} {1} {2} (Total {3} {4})", Currentpage, Resources.CommonCaption.of, RecCount, TotalRecords, Resources.CommonCaption.Item);
            lblBottomPaging.Text = lblTopPaging.Text;
        }       

        #endregion
    }
}