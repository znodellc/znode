﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Facets.ascx.cs" Inherits="Znode.Engine.Demo.Controls.Default.SearchEngine.Facets" %>

<div class="ProductTagging">
    <div class="Title" id="SelectedTitle" runat="server">
        YOU'VE SELECTED:
    </div>
    <asp:Repeater runat="server" ID="FacetSelectedRepeater">
        <HeaderTemplate>
        </HeaderTemplate>
        <ItemTemplate>
            <div>
                <span class="SelectedTagText"><%# Eval("Key").ToString().Replace("_"," ")%>:</span>
                <span class="SelectedTagValue"><%# Eval("Value.Key") %></span>

                <asp:HyperLink runat="server" NavigateUrl='<%# Eval("Value.Value") %>' ID="remove" CssClass="linkBtnTagText">[Remove]</asp:HyperLink>
            </div>
        </ItemTemplate>

    </asp:Repeater>
    <div class="Title" runat="server">
        <asp:Label ID="SearchTitle" runat="server" Text=""></asp:Label>
    </div>
    <div id="ClearSearchAll">
        <asp:Label runat="server" ID="SearchWithText" CssClass="SelectedTagValue"></asp:Label>
        <asp:LinkButton runat="server" ID="removeText" CssClass="linkBtnTagText" Visible="False" OnClick="removeText_Click">[Remove]</asp:LinkButton>
    </div>
    <asp:Repeater runat="server" ID="SearchWithinSearchRepeater" OnItemCommand="SearchWithinSearchRepeater_ItemCommand">
        <ItemTemplate>
            <asp:Label CssClass="SelectedTagValue" runat="server" ID="SearchWithText" Text='<%# Container.DataItem %>'></asp:Label>
            <asp:LinkButton CssClass="linkBtnTagText" runat="server" ID="SearchWithRemove">[Remove]</asp:LinkButton>
            <br />
        </ItemTemplate>
    </asp:Repeater>
    <div class="ClearAll">
        <asp:LinkButton runat="server" ID="ClearAllSelection" CssClass="linkBtnTagText" OnClick="ClearAllSelection_Click">Clear all selections</asp:LinkButton>
    </div>
    <asp:Repeater ID="FacetRepeater" runat="server" OnItemDataBound="FacetRepeater_ItemDataBound">
        <HeaderTemplate>
            <div class="Title">
                Refine By
            </div>
            <div class="menu">
                <ul id="FacetTreeMenu">
        </HeaderTemplate>
        <ItemTemplate>
            <li><span class="facetName"><b>
                <%# DataBinder.Eval(Container.DataItem,"AttributeName").ToString().Split('|')[0].Replace("_"," ") %></b></span>
                <ul>
                    <li>
                        <asp:Repeater ID="FacetLinks" runat="server" OnItemCommand="FacetLinks_ItemCommand">
                            <ItemTemplate>

                                <div class="facetValue">
                                    <asp:Button CssClass="SelectedTagValue facetsLink_label" runat="server" CommandName="UpdateFacet"
                                        ID="facetLink" CommandArgument='<%# DataBinder.Eval(Container.NamingContainer.NamingContainer, "DataItem.AttributeName") +":" + DataBinder.Eval(Container.DataItem, "AttributeValue") %>'
                                        Text='<%# DataBinder.Eval(Container.DataItem, "AttributeValue") +"(" + DataBinder.Eval(Container.DataItem, "FacetCount")+")" %>' />
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </li>
                </ul>
            </li>
        </ItemTemplate>
        <FooterTemplate>
            </ul> </div>
            <script type="text/javascript">                maketreefacets('FacetTreeMenu');</script>
        </FooterTemplate>
    </asp:Repeater>
</div>

