﻿<%@ Control Language="C#" EnableViewState="False" AutoEventWireup="true" CodeBehind="TopSearch.ascx.cs" Inherits="Znode.Engine.Demo.Controls.Default.SearchEngine.TopSearch" %>
<div id="SearchEngine" class="NoPrint">

    <asp:Panel ID="pnlTopSearch" runat="server" DefaultButton="btnSearch">
        <script>
            var searchURL = '<%= Page.ResolveUrl("~/GetSuggestions.asmx/TypeAhead") %>';
        </script> 
        <div class="RoundedBox">
            <div style="float: left;">
                 <select name="ddlCategory" runat="server" ClientIDMode="Static" ID="ddlCategory"  TabIndex="1" Height="20px" Width="130px" ></select>
               <%-- <asp:DropDownList ID="ddlCategory" runat="server" Height="20px" Width="130px" TabIndex="1" EnableViewState="False" AppendDataBoundItems="True" ClientIDMode="Static">
                </asp:DropDownList>--%>
            </div>
            <ajaxToolKit:TextBoxWatermarkExtender ID="watermark" TargetControlID="SearchText"
                WatermarkCssClass="WaterMark" WatermarkText="Search by SKU# or Keyword" runat="server">
            </ajaxToolKit:TextBoxWatermarkExtender>
           
            <asp:TextBox ID="SearchText" CssClass="TextBoxPosition" runat="server" ClientIDMode="Static"></asp:TextBox>
        </div>
        <asp:ImageButton ID="btnSearch" runat="server" OnClick="btnSearch_Click" CssClass="Button" CausesValidation="false" />
        <asp:HiddenField ID="hdneditMode" runat="server" Value="" ClientIDMode="Static" />
    </asp:Panel>
</div>
