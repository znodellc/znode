﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using ZNode.Libraries.Ecommerce.Entities;
using ZNode.Libraries.ECommerce.Tagging;

namespace Znode.Engine.Demo.Controls.Default.SearchEngine
{
  
    public partial class Facets : System.Web.UI.UserControl
    {

		#region Private Variales
		IEnumerable<ZNodeFacet> facets;

		const string querystringDefaultParams = "zcid,category,text,page,size,sort,srt";
		private string searchText = string.Empty;
		private string searchresultText = string.Empty;
		private List<string> splitText;

		#endregion

		#region Public Variables
		public delegate void UpdatePageDelegate(string facetquery);
		public event UpdatePageDelegate UpdatePageHandler;
		public IEnumerable<ZNodeFacet> Facetslist
        {
            get
            {
                return this.facets;
            }

            set
            {
                this.facets = value;
            }
        }


		/// <summary>
		/// Gets the SearchText 
		/// </summary>
		public string SearchText
		{
			get
			{
				searchText = string.Empty;

				if (Request.QueryString["text"] != null)
				{
					searchText = Request.QueryString["text"].ToString();
				}

				return searchText;
			}
		}

		/// <summary>
		/// Gets the SearchText 
		/// </summary>
		public string SearchResultText
		{
			get
			{
				searchresultText = string.Empty;

				if (Request.QueryString["SRT"] != null)
				{
					searchresultText = Request.QueryString["SRT"].ToString();
				}

				return searchresultText;
			}
		}

		#endregion

		#region Protected Methods and Events

		protected void FacetRepeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
			{
				ZNodeFacet facet = (ZNodeFacet)e.Item.DataItem;
				switch ((ZNodeFacetControlType)(facet.ControlTypeID))
				{
					case ZNodeFacetControlType.LABEL:
						Repeater links = e.Item.FindControl("FacetLinks") as Repeater;
						links.DataSource = facet.AttributeValues;
						links.DataBind();
						break;
				}
			}
		}

		/// <summary>
		/// Facet Value with the Query
		/// </summary>
		/// <param name="source"></param>
		/// <param name="e"></param>
		protected void FacetLinks_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			if (e.CommandName == "UpdateFacet")
			{
				string query = string.Empty;
				bool isKeyAdded = false;
				string[] qparams = querystringDefaultParams.Split(',');
				var currentQuery = e.CommandArgument.ToString().Split(':');
				var currentkey = currentQuery[0].Split('|')[0];

				foreach (string key in Request.QueryString.Keys)
				{
					if (!qparams.Contains(key.ToLower()))
						Request.QueryString[key].Split(',').ToList().ForEach(x =>
						{
							query += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(x));
						});

					if (key.Equals(currentkey, StringComparison.InvariantCultureIgnoreCase))
					{
						query += string.Format("&{0}={1}", currentkey, HttpUtility.UrlEncode(currentQuery[1]));
						isKeyAdded = true;
					}
				}

				if (isKeyAdded == false)
				{
					query += string.Format("&{0}={1}", currentkey, HttpUtility.UrlEncode(currentQuery[1]));
				}


				var resultString = string.Empty;
				if (!string.IsNullOrEmpty(Request.QueryString["SRT"]))
				{
					resultString += Request.QueryString["SRT"] + "||";
					query += string.Format("&{0}={1}", "SRT", HttpUtility.UrlEncode(resultString));
				}



				if (UpdatePageHandler != null)
					UpdatePageHandler(query);
			}
		}

		/// <summary>
		/// Binding the SRT Value to the repeater.
		/// </summary>
		/// <param name="source"></param>
		/// <param name="e"></param>
		protected void SearchWithinSearchRepeater_ItemCommand(object source, RepeaterCommandEventArgs e)
		{
			Label searchwithText = (Label)e.Item.FindControl("SearchWithText");
			string searchValue = searchwithText.Text + "||";
			string srtValue = string.Empty;
			srtValue = Request.QueryString["SRT"];
			RemoveLink(srtValue, searchValue, "srt");
		}

		/// <summary>
		/// Remove method for Search Text Value.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void removeText_Click(object sender, EventArgs e)
		{
			string searchValue = SearchWithText.Text;
			string srtValue = string.Empty;
			srtValue = Request.QueryString["text"];
			RemoveLink(srtValue, searchValue, "text");
		}
		#endregion

		#region Pageload
		protected void Page_Load(object sender, EventArgs e)
		{
			SearchTitle.Text = String.Empty;
			SelectedTitle.Visible = false;
            
			if (!IsPostBack)
				BindSelectedItems();

            if (!string.IsNullOrEmpty(SearchResultText))
			{
				SearchTitle.Text = "YOU'VE SEARCHED:";			
			}
			
        }
		#endregion

		#region Private Methods

		
		/// <summary>
		/// Remove Single Facet Value 
		/// </summary>
		/// <param name="facetName"></param>
		/// <returns></returns>
        private string GetRemoveUrlForFacet(string facetName)
        {
            string query = string.Empty;
            foreach (string key in Request.QueryString.Keys)
            {
                if (key != facetName)
                {
                    Request.QueryString[key].Split(',').ToList().ForEach(x => {
                        query += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(x));
                    });
                }
            }

            if (query.Length > 0)
                query = query.Substring(1);

            return Request.Url.GetLeftPart(UriPartial.Path) + "?" + query;
        }

		/// <summary>
		/// Bind Method for the Facet Value
		/// </summary>
		private void BindSelectedItems()
		{
			Dictionary<string, KeyValuePair<string, string>> selectedItems = new Dictionary<string, KeyValuePair<string, string>>();
			if (FacetRepeater.Visible)
			{

				foreach (RepeaterItem item in FacetRepeater.Items)
				{
					var repeater = item.FindControl("FacetLinks") as Repeater;
					foreach (RepeaterItem rptitem in repeater.Items)
					{
                        
                        //Znode Version 7.2.2
                        //Replace Facets link button to Submit Button - Start
						var button = rptitem.FindControl("facetLink") as Button;
                        //Replace Facets link button to Submit Button - End
						var currentItem = button.CommandArgument.ToString().Split(':');
						var key = currentItem[0].Split('|')[0];
						if (Request.QueryString[key] != null)
						{
							if (button.Font.Bold = Request.QueryString[key].Split(',').Contains(currentItem[1]))
							{
								button.Visible = false;
							}

							if (!selectedItems.Keys.Contains(key))
								selectedItems.Add(key, new KeyValuePair<string, string>(Request.QueryString[key], GetRemoveUrlForFacet(key)));
						}
					}

                    //Znode Version 7.2.2
                    //Replace Facets link button to Submit Button - Start
					item.Visible = repeater.Items.Count > 0 && repeater.Items.Cast<RepeaterItem>().Any(x => (x.FindControl("facetLink") as Button).Visible);
                    //Replace Facets link button to Submit Button - End
				}


				FacetRepeater.Visible = FacetRepeater.Items.Cast<RepeaterItem>().Any(x => x.Visible);
			}
			else
			{
				// show selected facets when no facets found from search engine result.      
				string[] qparams = querystringDefaultParams.Split(',');
				foreach (string key in Request.QueryString.Keys)
				{
					if (!qparams.Contains(key.ToLower()))
					{
						selectedItems.Add(key, new KeyValuePair<string, string>(Request.QueryString[key], GetRemoveUrlForFacet(key)));

					}
				}
			}

			FacetSelectedRepeater.DataSource = selectedItems;
			FacetSelectedRepeater.DataBind();
			FacetSelectedRepeater.Visible = selectedItems.Any();
			ClearAllSelection.Visible = selectedItems.Any();
			SelectedTitle.Visible = selectedItems.Any();

			SearchTextSplit();
			if (splitText.Count > 0)
			{
				SearchWithinSearchRepeater.DataSource = splitText;
				SearchWithinSearchRepeater.DataBind();
				SearchWithinSearchRepeater.Visible = splitText.Any();
                SearchTitle.Visible = splitText.Any();
			}

		}

		/// <summary>
		/// Method for Splitting the Pipe symbol from the SRT Value.
		/// </summary>
		private void SearchTextSplit()
		{
			splitText = new List<string>();
			string[] stringSeparators = new string[] { "||" };

			if (!string.IsNullOrEmpty(SearchResultText))
			{
				string[] splitName = SearchResultText.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

				for (int i = 0; i < splitName.Count(); i++)
				{
					splitText.Add(splitName[i].ToString());
				}
			}
		}


		#endregion

		#region Public Methods
		/// <summary>
		/// Clearing all the Facet Values
		/// </summary>
		/// <returns></returns>
        public void GetClearAllUrlForFacet()
        {
            string query = string.Empty;
            string[] facetName = querystringDefaultParams.Split(',');
            foreach (string key in Request.QueryString.Keys)
            {
                if (facetName.Contains(key.ToLower()))
                {
                    Request.QueryString[key].Split(',').ToList().ForEach(x =>
                    {
						if (key.ToLower() != "srt")
						{
							query += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(x));
						}
                    });
                }
            }
            if (query.Length > 0)
                query = query.Substring(1);

            Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path) + "?" + query);
        }

		public void BindFacets()
		{
			FacetRepeater.DataSource = facets;
			FacetRepeater.DataBind();
			FacetRepeater.Visible = facets.Any();
		}

		/// <summary>
		/// Remove Method for the Searched Text SRT Value.
		/// </summary>
		/// <param name="srtValue"></param>
		/// <param name="searchValue"></param>
		/// <param name="keyValue"></param>
		public void RemoveLink(string srtValue, string searchValue, string keyValue)
		{
			string query = string.Empty;
			srtValue = srtValue.Replace(searchValue, "");

			string[] searchName = querystringDefaultParams.Split(',');
			foreach (string key in Request.QueryString.Keys)
			{
				if (searchName.Contains(key.ToLower()))
				{
					Request.QueryString[key].Split(',').ToList().ForEach(x =>
					{
						if (key.ToLower() == keyValue)
						{
							query += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(srtValue));
						}
						else
						{
							query += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(x));
						}
					});
				}
			}
			string[] qparams = querystringDefaultParams.Split(',');
			foreach (string key in Request.QueryString.Keys)
			{
				string itemString = key.ToString();
				if (!querystringDefaultParams.Contains(itemString.ToLower()))
				{
					Request.QueryString[key].Split(',').ToList().ForEach(x =>
					{
						query += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(x));
					});
				}
			}
			if (query.Length > 0)
				query = query.Substring(1);
			Response.Redirect("SearchEngine.aspx?" + query);
		}
		#endregion	

		protected void ClearAllSelection_Click(object sender, EventArgs e)
		{
			GetClearAllUrlForFacet();
			ClearAllSelection.Visible = false;
		}
    }
}