﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SearchWithInSearch.ascx.cs" Inherits="Znode.Engine.Demo.Controls.Default.SearchEngine.SearchWithInSearch" %>

<asp:Panel runat="server" ID="SearchwithResult" DefaultButton="BtnSearchResult">
	<div class="Title">
		Search within these results
	</div>

	<div class="CategoryTreeView">
		<asp:TextBox ID="SearchResultText" runat="server" ClientIDMode="Static"></asp:TextBox>
		<asp:ImageButton ID="BtnSearchResult" runat="server" OnClick="BtnSearchResult_Click" CssClass="Button" CausesValidation="false" ClientIDMode="Static" />
	</div>
</asp:Panel>
