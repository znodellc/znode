﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Ecommerce.Entities;

namespace Znode.Engine.Demo.Controls.Default.SearchEngine
{
    public partial class SearchList : System.Web.UI.UserControl
    {
        #region private members
        private string searchText = string.Empty;
        private string category = string.Empty;
        private string refineBy = string.Empty;
        private int sort = 0;
        const string querystringparams = "category,text,page,size,sort,srt";
        #endregion

      

        protected void Page_Load(object sender, EventArgs e)
        {
			category = Request.QueryString["Category"];

            if (!Page.IsPostBack)
            {
                searchText = Request.QueryString["text"];
                var searchResultText = Request.QueryString["SRT"];
                sort = (!string.IsNullOrEmpty(Request.QueryString["sort"]))
                           ? Convert.ToInt32(Request.QueryString["sort"])
                           : 0;

                List<KeyValuePair<string, IEnumerable<string>>> RefinedFacets =
                    new List<KeyValuePair<string, IEnumerable<string>>>();

                foreach (var item in Request.QueryString.Keys)
                {
                    string itemString = item.ToString();
                    if (!querystringparams.Contains(itemString.ToLower()))
                    {
                        RefinedFacets.Add(new KeyValuePair<string, IEnumerable<string>>(itemString,
                                                                                        Request.QueryString.GetValues(
                                                                                            itemString)));
                    }
                }

                BindData(category, searchText, searchResultText, RefinedFacets, sort);

                if (searchText.Length > 0 && !Page.IsPostBack)
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity(
                        (int) ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.KeywordSearch,
                        searchText.ToLower());
            }

			FacetSearch.UpdatePageHandler += UpdatePage;
			
        }

        private void BindData(string category, string searchText,string searchResultText, List<KeyValuePair<string, IEnumerable<string>>> refineBy, int sort)
        {
            ZNodeSearchEngine search = new ZNodeSearchEngine();
            ZNodeSearchEngineResult result = search.Search(category, searchText,searchResultText, refineBy ,sort);
            if (result != null)
            {
                if (result.Facets != null)
                {
                    FacetSearch.Facetslist = result.Facets;
                    FacetSearch.BindFacets();
                }
                if (result.CategoryNavigation != null)
                {
                    Categories.CategoryNavigation = result.CategoryNavigation;
                    Categories.DataBind();
                }
                if (result.Ids != null)
                {
                    SearchProductList.ProductID =  result.Ids.Select(int.Parse).ToList();
                    SearchProductList.DataBind();
                }
            }
        }

        public void UpdatePage(string facetquery)
        {
            var baseUrl = this.GetNavigationUrl();

			var sortOption = Request.Params["sort"];
			
			string navigateUrl = null;

	        var searchQuery = Request.QueryString["text"];

			//If sorting, page can end up on Page2 which shows no results.
			if (sortOption != null)
			{
				//Set to page 1
				if (SearchProductList.CurrentPage > 1)
					navigateUrl = string.Format("{0}Category={1}&text={2}&page={3}&size={4}&sort={5}", baseUrl, HttpUtility.UrlEncode(category), HttpUtility.UrlEncode(searchQuery), 1, SearchProductList.PageSize, SearchProductList.SortOption);
			}

			if (string.IsNullOrEmpty(navigateUrl))
				navigateUrl = string.Format("{0}Category={1}&text={2}&page={3}&size={4}&sort={5}", baseUrl, HttpUtility.UrlEncode(category), HttpUtility.UrlEncode(searchQuery), SearchProductList.CurrentPage, SearchProductList.PageSize, SearchProductList.SortOption);

            Response.Redirect(navigateUrl + facetquery);
        }

        /// <summary>
        /// Gets the navigation url for the previous/next navigation link
        /// </summary>
        /// <returns>Returns the navigation base url.</returns>
        private string GetNavigationUrl()
        {
            string baseUrl = string.Empty;
            if (this.Request.QueryString.Count > 0)
            {
                StringBuilder url = new StringBuilder();
                url.Append(Request.Url.GetLeftPart(UriPartial.Authority));
                url.Append(Request.Url.AbsolutePath);
                url.Append("?");
                baseUrl = url.ToString();
            }
            else
            {
                baseUrl = Request.Url.ToString() + "?";
            }

            return baseUrl;
        }
       
    }
}