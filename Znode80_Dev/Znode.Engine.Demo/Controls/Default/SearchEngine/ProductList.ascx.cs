﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;
using System.Data;
using System.Text;
using ZNode.Libraries.Admin;
using Znode.Engine.Common;

namespace Znode.Engine.Demo.Controls.Default.SearchEngine
{
    public partial class ProductList : System.Web.UI.UserControl
    {

        #region Private Variables
        private ZNodeProfile _ProductListProfile = new ZNodeProfile();
        private ZNodeProductList _ProductList;
        private List<int> _ProductId;
        private int RecCount = 0;
        private int Currentpage = 0;
        private string category = string.Empty;
        private string searchText = string.Empty;
        private string sortOrder = string.Empty;
        private string sortDirection = string.Empty;
        private int _pageSize = 8;
        private string _selectedPageSizeText = "SelectedPageSize";
        #endregion
		
        public event System.EventHandler PreviousButtonClicked;

        public event System.EventHandler NextButtonClicked;

        #region Public Properties

        /// <summary>
        /// Gets or sets the product list profile
        /// </summary>
        public ZNodeProfile ProductListProfile
        {
            get { return this._ProductListProfile; }
            set { this._ProductListProfile = value; }
        }

        /// <summary>
        /// Gets the Category 
        /// </summary>
        public string Category
        {
            get
            {
                category = string.Empty;

                if (!string.IsNullOrEmpty(Request.QueryString["Category"]))
                {
                    category = Request.QueryString["Category"].ToString();
                }

                return category;
            }
        }


        /// <summary>
        /// Gets the SearchText 
        /// </summary>
        public string SearchText
        {
            get
            {
                searchText = string.Empty;

                if (!string.IsNullOrEmpty(Request.QueryString["text"]))
                {
                    searchText = Request.QueryString["text"].ToString();
                }

                return searchText;
            }
        }

        /// <summary>
        /// Gets the current page
        /// </summary>
        public int CurrentPage
        {
            get
            {
                int currentPage = 1;

                if (!string.IsNullOrEmpty(Request.QueryString["page"]))
                {
                    currentPage = Convert.ToInt32(Request.QueryString["page"]);
                }

                return currentPage;
            }
        }

        /// <summary>
        /// Gets or sets the Total Records
        /// </summary>
        public int TotalRecords
        {
            get
            {
                int totalRecords = 0;

                if (ViewState["TotalRecords"] != null)
                {
                    totalRecords = (int)ViewState["TotalRecords"];
                }

                return totalRecords;
            }

            set
            {
                ViewState["TotalRecords"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the Total Pages
        /// </summary>
        public int TotalPages
        {
            get
            {
                int totalPages = 0;

                if (ViewState["TotalPages"] != null)
                {
                    totalPages = (int)ViewState["TotalPages"];
                }

                return totalPages;
            }

            set
            {
                ViewState["TotalPages"] = value;
            }
        }

        /// <summary>
        /// Gets the search list page pize
        /// </summary>
        public int PageSize
        {
            get
            {
                //Znode Version 7.2.2
                //Change Description - Start 

                int pageSize = _pageSize;

                //Sets the Selected Page Size in session.
                if (Equals(Session[_selectedPageSizeText], null))
                {
                    Session.Add(_selectedPageSizeText, pageSize);
                }
                //Change Description - End 

                string eventTarget = "__EVENTTARGET";
               
                if (ddlTopPaging.SelectedIndex > 0 || ddlBottomPaging.SelectedIndex > 0)
                {
                    pageSize = (Request[eventTarget] != null && Request[eventTarget].Contains(ddlTopPaging.ID)) ? Convert.ToInt32(ddlTopPaging.SelectedValue) : Convert.ToInt32(ddlBottomPaging.SelectedValue);
                    Session[_selectedPageSizeText] = pageSize;
                }
                else if (!string.IsNullOrEmpty(Request.QueryString["size"]))
                {
                    pageSize = Convert.ToInt32(Request.QueryString["size"]);
                    Session[_selectedPageSizeText] = pageSize;
                }
                else
                    pageSize = _pageSize;

                //Znode Version 7.2.2
                //Change Description - Start 
                //Add SelectedPageSize key in Session and bind the selected drop down value to the session key. And Access page value from Session.
                //Bind Session Page Size to the controls.
                if (Session[_selectedPageSizeText] != null)
                {
                    pageSize = Convert.ToInt32(Session[_selectedPageSizeText]);
                    ddlTopPaging.ClearSelection();
                    ddlTopPaging.SelectedValue = pageSize.ToString();
                    ddlBottomPaging.ClearSelection();
                    ddlBottomPaging.SelectedValue = pageSize.ToString();
                }
                //Change Description - End 

                return pageSize;
            }
        }

        /// <summary>
        /// Gets the sort option
        /// </summary>
        public int SortOption
        {
            get
            {
                if (lstFilter.SelectedValue.Equals("2"))
                {
                    return 2;
                }

                // Apply Sorting Only Page gets Reload.
                if (!lstFilter.SelectedValue.Equals("0"))
                {
                    return 1;
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the  product list passed in from the search page
        /// </summary>
        public ZNodeProductList ProductSearchList
        {
            get
            {
                return this._ProductList;
            }

            set
            {
                this._ProductList = value;
            }
        }

        public List<int> ProductID
        {
            get
            {
                return this._ProductId;
            }

            set
            {
                this._ProductId = value;
            }
        }
        #endregion

        #region Private Properties
        /// <summary>
        /// Gets or sets the Last Index
        /// </summary>
        private int LastIndex
        {
            get
            {
                int lastIndex = 0;

                if (ViewState["LastIndex"] != null)
                {
                    lastIndex = Convert.ToInt32(ViewState["LastIndex"]);
                }

                return lastIndex;
            }

            set
            {
                ViewState["LastIndex"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the First Index
        /// </summary>
        private int FirstIndex
        {
            get
            {
                int firstIndex = 0;

                if (ViewState["FirstIndex"] != null)
                {
                    firstIndex = Convert.ToInt32(ViewState["FirstIndex"]);
                }

                return firstIndex;
            }

            set
            {
                ViewState["FirstIndex"] = value;
            }
        }
        #endregion

        #region Protected Methods and Events

        /// <summary>
        /// Set the page size to selected DropDownList value size. If "SHOW ALL" selected then display all products.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlTopPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.NavigationToUrl(1, Convert.ToInt32(ddlTopPaging.SelectedValue), lstFilter);
        }

        /// <summary>
        /// Set the page size to selected DropDownList value size. If "SHOW ALL" selected then display all products.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlBottomPaging_SelectedIndexChanged(object sender, EventArgs e)
        {
           this.NavigationToUrl(1, Convert.ToInt32(ddlBottomPaging.SelectedValue),LstFilterBottom);
        }

        /// <summary>
        /// Event is raised when LstFilter control Selected Index is Changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.NavigationToUrl(1, Convert.ToInt32(ddlBottomPaging.SelectedValue),lstFilter);
        }

      
        protected void LstFilterBottom_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.NavigationToUrl(1, Convert.ToInt32(ddlBottomPaging.SelectedValue),LstFilterBottom);
        }
        #endregion

		#region Page Load
		/// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(Request.QueryString["sort"]))
                {
                    lstFilter.SelectedValue = Request.QueryString["sort"].ToString();
                    LstFilterBottom.SelectedValue = Request.QueryString["sort"].ToString();
                }
                if (!string.IsNullOrEmpty(Request.QueryString["size"]))
                {
                    int size = Convert.ToInt32(Request.QueryString["size"].ToString());
                    ddlTopPaging.SelectedIndex = ddlTopPaging.Items.IndexOf(ddlTopPaging.Items.FindByValue(size.ToString()));
                    ddlBottomPaging.SelectedIndex = ddlTopPaging.SelectedIndex;
                }
            }

            this._ProductList = new ZNodeProductList();
            this._ProductList = ZNodePaging.GetProducts(ProductID, GetSortField(Request.QueryString["sort"]), GetSortDirection(Request.QueryString["sort"]), this.CurrentPage, this.PageSize, 1);

            int previousPageIndex = this.Currentpage;

            if (previousPageIndex < this.CurrentPage)
            {
                if (this.NextButtonClicked != null)
                {
                    this.NextButtonClicked(sender, e);
                }
            }
            else
            {
                if (this.PreviousButtonClicked != null)
                {
                    this.PreviousButtonClicked(sender, e);
                }
            }

			if (this._ProductList == null)
			{
				pnlProductList.Visible = false;
				ErrorMsg.Visible = true;
				SearchMessage.Text = GetSearchResultMessage(Category, SearchText, null);
			}
			else
			{
				pnlProductList.Visible = true;
				ErrorMsg.Visible = false;
				SearchMessage.Visible = true;
				SearchMessage.Text = GetSearchResultMessage(Category, SearchText, ProductID);
			}

            this.BindDataListPagedDataSource();

            string baseUrl = this.GetNavigationUrl();

            //// Do not add the & or ? in the following query string.
            hlTopPrevLink.NavigateUrl = hlBotPrevLink.NavigateUrl = string.Format("{0}Category={1}&text={2}&page={3}&size={4}&sort={5}", baseUrl, Category, SearchText, this.CurrentPage - 1, ddlTopPaging.SelectedValue, lstFilter.SelectedValue);
            hlTopNextLink.NavigateUrl = hlBotNextLink.NavigateUrl = string.Format("{0}Category={1}&text={2}&page={3}&size={4}&sort={5}", baseUrl, Category, SearchText, this.CurrentPage + 1, ddlTopPaging.SelectedValue, lstFilter.SelectedValue);

            this.Page.Title = GetLocalResourceObject("txtSearchResultTitle.Text").ToString();
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Navigate to the category page with query string the specified current page and page size.
        /// </summary>
        /// <param name="currentPage">Current page index.</param>
        /// <param name="pageSize">Category page size. If Show All selected then -1 will be used.</param>
        private void NavigationToUrl(int currentPage, int pageSize, DropDownList ddlSort)
        {
            string baseUrl = this.GetNavigationUrl();
            string faceturl = "";
            foreach (String key in Request.QueryString.AllKeys)
            {
                if (key.ToLower() == "category" || key.ToLower() == "text" || key.ToLower() == "page" || key.ToLower() == "size" || key.ToLower() == "sort")
                    continue;


                if (Request.QueryString[key].Contains(","))
                {
                    string[] facets = Request.QueryString[key].Split(',');
                    foreach (string facet in facets)
                    {
                        faceturl = faceturl + "&" + key + "=" + HttpUtility.UrlEncode(facet);
                    }
                }
                else
                {
                    faceturl = faceturl + "&" + key + "=" + HttpUtility.UrlEncode(Request.QueryString[key]);
                }
            }

            // Do not add the & or ? in the following query string.
            string navigateUrl = string.Format("{0}Category={1}&text={2}&page={3}&size={4}&sort={5}", baseUrl, category, searchText, currentPage, pageSize, ddlSort.SelectedValue);
            Response.Redirect(navigateUrl + faceturl);
        }

        /// <summary>
        /// Gets the navigation url for the previous/next navigation link
        /// </summary>
        /// <returns>Returns the navigation base url.</returns>
        private string GetNavigationUrl()
        {
            string baseUrl = string.Empty;
            if (this.Request.QueryString.Count > 0)
            {
                StringBuilder url = new StringBuilder();
                url.Append(Request.Url.GetLeftPart(UriPartial.Authority));
                url.Append(Request.Url.AbsolutePath);

                // Append the category Url with base url
                if (Request.QueryString["keyword"] != null)
                {
                    url.Append("?keyword=").Append(Request.QueryString["keyword"]).Append("&");
                }
                else
                {
                    url.Append("?");
                }

                baseUrl = url.ToString();
            }
            else
            {
                baseUrl = Request.Url.ToString() + "?";
            }

            return baseUrl;
        }

        /// <summary>
        /// Bind Datalist using Paged DataSource object
        /// </summary>
        private void BindDataListPagedDataSource()
        {

			// Retrieve collection object from Viewstate
			if (this._ProductList == null)
			{
				return;
			}

            // Assigning Datasource to the DataList.
            DataListProducts.DataSource = this._ProductList.ZNodeProductCollection;
            DataListProducts.DataBind();

            // Disable the view state for the data list item. 
            foreach (DataListItem item in DataListProducts.Items)
            {
                item.EnableViewState = false;
            }

            this.Currentpage = this.CurrentPage;
            this.RecCount = this.TotalPages = this._ProductList.TotalPageCount;
            this.TotalRecords = this._ProductList.TotalRecordCount;

            hlTopNextLink.Enabled = hlBotNextLink.Enabled = !(this.Currentpage == this.RecCount);
            hlTopPrevLink.Enabled = hlBotPrevLink.Enabled = !(this.Currentpage == 1);

            if (!this.IsPostBack)
            {
                this.DoPaging();
            }
        }

        private string GetSearchResultMessage(string category, string searchText, List<int> productID)
        {
            if (productID==null && !string.IsNullOrEmpty(SearchText))
            {
                return string.Format("We found 0 results for \"{0}\"", SearchText);
            }
			else if (ProductID != null)
			{
				if (ProductID.Count() > 0 && string.IsNullOrEmpty(category) && !string.IsNullOrEmpty(searchText))
				{
					return string.Format("We found {0} results for \"{1}\"", this.ProductID.Count(), searchText);
				}
				else if (ProductID.Count() > 0 && !string.IsNullOrEmpty(category) && !string.IsNullOrEmpty(searchText))
				{
					return string.Format("We found {0} results for \"{1}\" in {2} ", this.ProductID.Count(), searchText, category);
				}
				else if (ProductID.Count() > 0 && string.IsNullOrEmpty(category) || string.IsNullOrEmpty(searchText))
				{
					return string.Format("We found {0} results", this.ProductID.Count());
				}
			}
	        return null;
        }


       

        /// <summary>
        /// Binding Paging List
        /// </summary>
        private void DoPaging()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("PageIndex");
            dt.Columns.Add("PageText");

            this.FirstIndex = this.CurrentPage - 5;
            this.LastIndex = (this.CurrentPage > 5) ? this.CurrentPage + 5 : 10;
            if (this.LastIndex > this.TotalPages)
            {
                this.LastIndex = this.TotalPages;
                this.FirstIndex = this.LastIndex - 10;
            }

            if (this.FirstIndex < 0)
            {
                this.FirstIndex = 0;
            }

            ddlTopPaging.Items.Clear();
            ddlBottomPaging.Items.Clear();

            string pagingText = this.GetLocalResourceObject("PagingText").ToString();

            // Each items seperated with & symbol
            string[] pagingItems = pagingText.Split(new char[] { '&' });

            foreach (string pagingItem in pagingItems)
            {
                if (pagingItem.Contains("|") && pagingItem.Trim().Length > 0)
                {
                    // Key value pair seperated with | symbol
                    string[] itemText = pagingItem.Split(new char[] { '|' });
                    DataRow dr = null;
                    dr = dt.NewRow();
                    dr[0] = Convert.ToInt32(itemText[1].Trim());
                    dr[1] = itemText[0].Trim();
                    dt.Rows.Add(dr);
                }
            }

            this.ddlTopPaging.DataTextField = "PageText";
            this.ddlTopPaging.DataValueField = "PageIndex";
            this.ddlTopPaging.DataSource = dt;
            this.ddlTopPaging.DataBind();

            this.ddlBottomPaging.DataTextField = "PageText";
            this.ddlBottomPaging.DataValueField = "PageIndex";
            this.ddlBottomPaging.DataSource = dt;
            this.ddlBottomPaging.DataBind();

            if (!string.IsNullOrEmpty(Request.QueryString["size"]))
            {
                int size = Convert.ToInt32(Request.QueryString["size"].ToString());
                ddlTopPaging.SelectedIndex = ddlTopPaging.Items.IndexOf(ddlTopPaging.Items.FindByValue(size.ToString()));
                ddlBottomPaging.SelectedIndex = ddlTopPaging.SelectedIndex;
            }
        }

        /// <summary>
        /// Get Sort Field
        /// </summary>
        private string GetSortField(string sort)
        {
            string sortOrder = string.Empty;

            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort)
                {
                    case "2": return "FinalPrice";

                    case "3": return "FinalPrice";
                }
            }
            return null;
        }

        /// <summary>
        /// Get Sort Direction
        /// </summary>
        private string GetSortDirection(string sort)
        {
            if (!string.IsNullOrEmpty(sort))
            {
                switch (sort)
                {
                    case "2": return "ASC";

                    case "3": return "DESC";
                }
            }
            return null;
        }

        #endregion


        #region Public Methods
        /// <summary>
        /// Represents the CheckFor Call for pricing message
        /// </summary>
        /// <param name="fieldValue">passed the field value </param>
        /// <returns>Returns the Call For pricing message </returns>
        public string CheckForCallForPricing(object fieldValue, object callMessage)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();
            bool Status = bool.Parse(fieldValue.ToString());
            string message = mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);

            if (callMessage != null && !string.IsNullOrEmpty(callMessage.ToString()))
            {
                message = callMessage.ToString();
            }

            if (Status)
            {
                return message;
            }
            else if (!this._ProductListProfile.ShowPrice)
            {
                return message;
            }
            else
            {
                return string.Empty;
            }
        }

        #endregion


    }
}