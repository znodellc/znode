﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="LeftNavigation.ascx.cs"
    Inherits="Znode.Engine.Demo.Controls.Default.SearchEngine.LeftNavigation" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>

<div class="CategoryTreeView">
    <div class="Title">
        <uc1:CustomMessage ID="CustomMessage2" MessageKey="LeftNavigationShopByCategoryTitle" runat="server" />
    </div>
    <div>
        <asp:TreeView runat="server" ID="CategoryList" Font-Bold="true" ShowExpandCollapse="false">
        </asp:TreeView>
    </div>
</div>

