﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="SearchList.ascx.cs"
    Inherits="Znode.Engine.Demo.Controls.Default.SearchEngine.SearchList" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/SearchEngine/Facets.ascx" TagName="FacetSearch"TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/SearchEngine/LeftNavigation.ascx" TagName="LeftNavigation" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/SearchEngine/ProductList.ascx" TagName="ProductList" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/SearchEngine/SearchWithInSearch.ascx" TagName="SearchWithin" TagPrefix="ZNode" %>

<div id="LeftColumn">
    <div>
         <ZNode:SearchWithin id="SearchWithInSearch" IsCategoryPage="false" runat="server"></ZNode:SearchWithin> 

    </div>
    <div>
        <ZNode:LeftNavigation id="Categories" IsCategoryPage="false" runat="server"></ZNode:LeftNavigation> 
    </div>   
        <ZNode:FacetSearch ID="FacetSearch" runat="server" Title="Featured Products"></ZNode:FacetSearch>      
    <div id="NewsLetter">
    </div>
</div>
<div id="MiddleColumn">
    <div><ZNode:ProductList id="SearchProductList" runat="server"></ZNode:ProductList></div>
</div>
