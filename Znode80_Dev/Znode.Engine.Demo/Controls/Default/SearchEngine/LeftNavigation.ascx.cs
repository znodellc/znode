﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Ecommerce.Entities;


namespace Znode.Engine.Demo.Controls.Default.SearchEngine
{
    public partial class LeftNavigation : System.Web.UI.UserControl
    {
        public bool IsCategoryPage { get; set; }

        # region Private Variables
        private IEnumerable<ZNodeCategoryNavigation> _CategoryNavigation;
        private ZNodeCategory _category;
        private string searchText = string.Empty;
        private string category = string.Empty;
        private string SRT = string.Empty;
        private bool _showSubCategory = false;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["text"] != null)
            {
                searchText = Request.QueryString["text"].ToString();
            }
            category = Request.QueryString["Category"];

            SRT = Request.QueryString["srt"];

            if (!Page.IsPostBack)
            {
                if (CategoryNavigation != null)
                {
                    CategoryList.CollapseImageUrl = string.Empty;
                    CategoryList.ExpandImageUrl = string.Empty;
                    CategoryList.NoExpandImageUrl = string.Empty;
                    CategoryList.ExpandAll();

                    if (CategoryNavigation.Count() > 0)
                    {
                        // Retrieve category data from httpContext (set previously in the page_preinit event)
                        this._category = (ZNodeCategory)HttpContext.Current.Items["Category"];
                        var displayedCategories = CategoryNavigation.ToList();
                        if (!string.IsNullOrEmpty(category)) 
                        {
                            displayedCategories = FilterCategoryNavigation(category);
                        }
                        else if (this._category != null && this._category.Name != null)
                        {
                            displayedCategories = FilterCategoryNavigation(HttpUtility.HtmlDecode(this._category.Name));                          
                        }

                        PopulateTreeView(displayedCategories, null);                            
                    }
                    else if (!string.IsNullOrEmpty(Request.QueryString["category"]))
                    {
                        var rootItems = new List<ZNodeCategoryNavigation>() { new ZNodeCategoryNavigation() { Name = Request.QueryString["category"], NumberOfProducts = 0 } };
                        PopulateTreeView(rootItems, null);
                    }
                    else
                    {
                        CategoryList.Visible = false;
                    }                  
                }
            }
        }
        
        public IEnumerable<ZNodeCategoryNavigation> CategoryNavigation
        {
            get
            {
                return this._CategoryNavigation;
            }

            set
            {
                this._CategoryNavigation = value;
            }
        }

        public bool ShowSubCategory { get { return _showSubCategory; } set { _showSubCategory = value; } } 
           
        private List<ZNodeCategoryNavigation> FilterCategoryNavigation(string category)
        {
            var displayCategories = CategoryNavigation.Where(x => x.Name == category).ToList();

            if (!displayCategories.Any())
            {
                var rootCategories = CategoryNavigation.Where(x => x.CategoryHierarchy.Any(y => y.Name == category)).ToList();
                rootCategories.ForEach(x => x.CategoryHierarchy = x.CategoryHierarchy.Where(y => y.Name == category).ToList());
                displayCategories = rootCategories;
            }

            return displayCategories;
        }

        public string GetNavigationURL(string categoryName)
        {
            if (IsCategoryPage)
            {
                var url = new ZNodeNavigation().GetCategoryUrl(categoryName);

                if (!string.IsNullOrEmpty(url))
                {
                    return url;
                }
            }

            return "~/SearchEngine.aspx?category=" + HttpUtility.UrlEncode(categoryName) + "&text=" + HttpUtility.UrlEncode(searchText) + "&SRT=" + HttpUtility.UrlEncode(SRT);
        }

        private void PopulateTreeView(IEnumerable<ZNodeCategoryNavigation> displayedCategories, TreeNode parentNode)
        {
            foreach(var node in displayedCategories)
            {
                var treeNode = new TreeNode();
                treeNode.Text = string.Format("<span class=\"Rightarrow\">»</span><a class=\"UnderlineText\" href=\"{0}\"><b>{1} ({2})</b></a>", Page.ResolveClientUrl(GetNavigationURL(node.Name)), node.Name, node.NumberOfProducts);
                treeNode.Value = node.Name; 
                if (parentNode == null)
                {
                    CategoryList.Nodes.Add(treeNode);
                }
                else
                {
                    parentNode.ChildNodes.Add(treeNode);
                }

                if (node.CategoryHierarchy != null && node.CategoryHierarchy.Any())
                {
                    PopulateTreeView(node.CategoryHierarchy, treeNode);
                }
            }
        }
    }
}