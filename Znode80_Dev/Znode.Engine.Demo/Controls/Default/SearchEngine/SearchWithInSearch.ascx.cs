﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;

namespace Znode.Engine.Demo.Controls.Default.SearchEngine
{
	public partial class SearchWithInSearch : System.Web.UI.UserControl
	{
		#region Member Variables
		const string querystringDefaultParams = "zcid,category,text,srt,page,size,sort";
		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			BtnSearchResult.ImageUrl = "~/Themes/" + ZNodeCatalogManager.Theme + "/Images/Search.gif";

			if (Session["CurrentCategoryTheme"] != null)
				BtnSearchResult.ImageUrl = "~/Themes/" + Session["CurrentCategoryTheme"].ToString() + "/Images/Search.gif";
		}

		protected void BtnSearchResult_Click(object sender, ImageClickEventArgs e)
		{
			var category = Request.QueryString["Category"];
			var searchText = Request.QueryString["text"];
			var searchResultText = Request.QueryString["SRT"];
			int sort = (!string.IsNullOrEmpty(Request.QueryString["sort"])) ? Convert.ToInt32(Request.QueryString["sort"]) : 0;

			if (string.IsNullOrEmpty(category))
			{
				var categoryObj = (ZNodeCategory)HttpContext.Current.Items["Category"];
				if (categoryObj != null)
				{
					category = categoryObj.Name;
				}
			}

			string facetquery = string.Empty;
			string[] qparams = querystringDefaultParams.Split(',');
			foreach (string key in Request.QueryString.Keys)
			{
				string itemString = key.ToString();
				if (!qparams.Contains(itemString.ToLower()))
				{
					Request.QueryString[key].Split(',').ToList().ForEach(x =>
					{
						facetquery += string.Format("&{0}={1}", key, HttpUtility.UrlEncode(x));
					});
				}
			}

			Response.Redirect("SearchEngine.aspx?" + GetQueryString(category, searchText, searchResultText, facetquery));
		}

		private string GetQueryString(string category, string text, string searchResultText, string facetquery)
		{
			NameValueCollection queryString = HttpUtility.ParseQueryString(string.Empty);

			queryString["Category"] = category;
			queryString["text"] = text;
			var resultString = string.Empty;
			if (!string.IsNullOrEmpty(searchResultText))
			{
				resultString += searchResultText;
			}

			resultString += SearchResultText.Text + "||";

			queryString["SRT"] = resultString;
			queryString["page"] = (!string.IsNullOrEmpty(SearchResultText.Text)) ? "1" : Request.QueryString["page"];
			queryString["size"] = Request.QueryString["size"];
			queryString["sort"] = Request.QueryString["sort"];

			return queryString.ToString() + facetquery;

		}
	}
}