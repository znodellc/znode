﻿using System;
using System.Collections.Specialized;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.DataAccess.Custom;
using Category = ZNode.Libraries.DataAccess.Entities.Category;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo.Controls.Default.SearchEngine
{
    public partial class TopSearch : System.Web.UI.UserControl
    {

        #region Private Members

        private ZNodeCategory _category;
        private string zcid = string.Empty;

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            btnSearch.ImageUrl = "~/Themes/" + ZNodeCatalogManager.Theme + "/Images/Search.gif";

            if (Session["CurrentCategoryTheme"] != null)
                btnSearch.ImageUrl = "~/Themes/" + Session["CurrentCategoryTheme"].ToString() + "/Images/Search.gif";
            if (!Page.IsPostBack)
            {
                BindData();

                SetCategory(Request.QueryString["Category"]);

                //Populate the category and dropdown while category page load / product page load.
                PopulateCategory();
                if (ddlCategory.SelectedIndex == 0)
                {
                    hdneditMode.Value = "true";
                }
                else
                {
                    hdneditMode.Value = "false";
                }
            }

        }

        #endregion

        #region Events
        protected void btnSearch_Click(object sender, ImageClickEventArgs e)
        {
            var category = Request.Form[ddlCategory.UniqueID];
            var searchText = Request.Form[SearchText.UniqueID];
            
            Response.Redirect("SearchEngine.aspx?" + GetQueryString(category,searchText));
        }

        protected override void Render(HtmlTextWriter writer)
        {
           // GetAllCategories();
            base.Render(writer);
        }
        #endregion

        #region Private Methods

        private void SetCategory(string categoryString)
        {
            if (!string.IsNullOrEmpty(categoryString))
            {
                var category = ddlCategory.Items.FindByText(categoryString);
                if (category == null)
                {

                    ddlCategory.Items.Insert(0, new ListItem(categoryString, categoryString));
                }
                var index = ddlCategory.Items.IndexOf(new ListItem(categoryString, categoryString));

                ddlCategory.SelectedIndex = index;
               
                // SearchText.Text = Request.QueryString["text"];
            }

        }


        private string GetQueryString(string category, string text)
        {
            NameValueCollection queryString = HttpUtility.ParseQueryString(string.Empty);

            queryString["Category"] = category;
            queryString["text"] = text;

            return queryString.ToString();
        
        }

        private void GetAllCategories()
        {
            CategoryHelper categoryHelper = new CategoryHelper();
            var categoryList = categoryHelper.GetCategoryByCatalogID(ZNodeCatalogManager.CatalogConfig.CatalogID);

            if (ddlCategory.Items.Count > 0)
            {
                for (int index = 0; index < categoryList.Tables[0].Rows.Count; index++)
                {
                    var categoryField = Server.HtmlDecode(categoryList.Tables[0].Rows[index]["Name"].ToString());

                    if (ddlCategory.Items.FindByValue(categoryField) == null)
                    {
                        Page.ClientScript.RegisterForEventValidation(ddlCategory.UniqueID, categoryField);
                    }
                }
            }

        }

        private void BindData()
        {
            string cacheKey = string.Format("{0}_{1}_{2}_{3}", "SearchDropdownData", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId, ZNodeProfile.CurrentUserProfileId);
            DataSet categoryList = HttpContext.Current.Cache[cacheKey] as DataSet;
            if (categoryList == null)
            {
                CategoryHelper categoryHelper = new CategoryHelper();
                categoryList = categoryHelper.GetRootCategoryItems(ZNodeCatalogManager.CatalogConfig.PortalID, ZNodeCatalogManager.CatalogConfig.CatalogID);

                for (int index = 0; index < categoryList.Tables[0].Rows.Count; index++)
                {
                    categoryList.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(categoryList.Tables[0].Rows[index]["Name"].ToString());
                }

                ZNodeCacheDependencyManager.Insert(cacheKey, categoryList, "ZNodeCategoryNode", "ZNodeCategory", "ZNodePortalCatalog");
            }
            ddlCategory.DataSource = categoryList;
            ddlCategory.DataTextField = "Name";
           ddlCategory.DataValueField = "Name";
            ddlCategory.DataBind();
            ListItem li = new ListItem(Resources.CommonCaption.AllDepartmentsText, "");
            ddlCategory.Items.Insert(0, li);
        }

        private void PopulateCategory()
        {
            //Populate the category dropdown while category page load.
            if (Request.QueryString["zcid"] != null)
            {
                this._category = (ZNodeCategory)HttpContext.Current.Items["Category"];

                if (_category != null && !string.IsNullOrEmpty(_category.Name.ToString()))
                {
                    SetCategory(this._category.Name);
                    //ddlCategory.SelectedIndex = ddlCategory.Items.IndexOf(ddlCategory.Items.FindByText(HttpUtility.HtmlDecode(new ZNodeNavigation().GetParentCategoryTitle(this._category.Title))));
                }
            }
            else if (!string.IsNullOrEmpty(Request.QueryString["Category"]))
            {
                //ddlCategory.SelectedIndex = ddlCategory.Items.IndexOf(ddlCategory.Items.FindByValue(HttpUtility.HtmlDecode(new ZNodeNavigation().GetParentCategoryName(HttpUtility.HtmlEncode(Request.QueryString["Category"])))));
            }

            //Populate the category dropdown while product page load.
            if (Request.QueryString["zpid"] != null)
            {
                if (Session["BreadCrumzcid"] != null)
                {
                    this.zcid = Session["BreadCrumzcid"].ToString();
                }
                else
                {
                    ZNode.Libraries.DataAccess.Service.ProductCategoryService service = new ZNode.Libraries.DataAccess.Service.ProductCategoryService();
                    TList<ProductCategory> pcategories = service.GetByProductID(Convert.ToInt32(Request.QueryString["zpid"]));
                    if (pcategories.Count > 0)
                    {
                        this.zcid = pcategories[0].CategoryID.ToString();
                    }
                }

                if (!string.IsNullOrEmpty(this.zcid))
                {
                    ZNode.Libraries.DataAccess.Service.CategoryService cservice = new ZNode.Libraries.DataAccess.Service.CategoryService();
                    Category category = cservice.GetByCategoryID(Convert.ToInt32(zcid));
                    if (category != null)
                    {
                        ddlCategory.SelectedIndex = ddlCategory.Items.IndexOf(ddlCategory.Items.FindByText(HttpUtility.HtmlDecode(new ZNodeNavigation().GetParentCategoryName(category.Name))));

                    }
                }
            }
           
        }
        #endregion
    }
}