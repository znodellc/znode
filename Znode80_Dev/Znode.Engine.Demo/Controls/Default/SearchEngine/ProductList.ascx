﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductList.ascx.cs" Inherits="Znode.Engine.Demo.Controls.Default.SearchEngine.ProductList" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating"TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/common/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>
<div class="Title">
    <asp:Localize ID="txtSearchResultTitle" meta:resourceKey="txtSearchResultTitle" runat="server" />
</div>
 <div class="SearchError">
    <asp:Label ID="ErrorMsg" runat="server" CssClass="Error" meta:resourcekey="ErrorMsgResource1"></asp:Label>
 </div>
<div class="SearchFound">
    <asp:Label ID="SearchMessage" runat="server"></asp:Label>
</div>
<asp:Panel ID="pnlProductList" runat="server" Visible="True" meta:resourcekey="pnlProductListResource1">
    <div class="CategoryDetail">
        <div  style="margin-left: 10px;"> <%--class="TopPagingSection">--%>
            <div class="TopPagingSection" style="width:765px;">
            <div class="Sorting">
                <span class="Label">
                    <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:CommonCaption, Sort%>"></asp:Localize>
                </span>
                <asp:DropDownList ID="lstFilter" runat="server" OnSelectedIndexChanged="LstFilter_SelectedIndexChanged" AutoPostBack="True">
                    <asp:ListItem Value="1" meta:resourcekey="ListItemResource1">Relevance</asp:ListItem>
                    <asp:ListItem Value="2" meta:resourcekey="ListItemResource2">Price Low to High</asp:ListItem>
                    <asp:ListItem Value="3" meta:resourcekey="ListItemResource3">Price High to Low</asp:ListItem>
                    <asp:ListItem Value="4" meta:resourcekey="ListItemResource4">Product A-Z</asp:ListItem>
                    <asp:ListItem Value="5" meta:resourcekey="ListItemResource5">Product Z-A</asp:ListItem>
                    <asp:ListItem Value="6" meta:resourcekey="ListItemResource6">Highest Rated</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="TopPaging">
                <asp:HyperLink ID="hlTopPrevLink" CssClass="Button" Text="<<" runat="server"></asp:HyperLink>
                <span>
                    <asp:Localize ID="Localize12" runat="server" meta:resourceKey="txtPage">
                    </asp:Localize>
                    <asp:Localize ID="Localize13" runat="server" meta:resourceKey="txtOF">
                    </asp:Localize>
                </span>
                <asp:HyperLink ID="hlTopNextLink" CssClass="Button" Text=">>" runat="server"></asp:HyperLink>
                <span>|
                        <asp:DropDownList ID="ddlTopPaging" runat="server" AutoPostBack="True" CssClass="Pagingdropdown"
                            meta:resourcekey="ddlTopPagingResource1" OnSelectedIndexChanged="DdlTopPaging_SelectedIndexChanged">
                        </asp:DropDownList>
                </span><span>
                    <asp:Localize ID="Localize9" runat="server" meta:resourceKey="txtProducts">
                    </asp:Localize><span class="SlashSeparator">/</span>
                    <asp:Literal ID="Literal6" runat="server" Text="<%$ Resources:CommonCaption, Page%>" />
                </span>
            </div>
                </div>
            
        </div>
        <div class="HorizontalLine"></div>
            
        <div>
            <asp:Label ID="Label1" runat="server" meta:resourcekey="Label1Resource1"></asp:Label>
          
            <div id="ProductListItem">
                
                <div>
                    <div class="CategoryProductlist">
                        <asp:DataList ID="DataListProducts" runat="server" RepeatDirection="Vertical" meta:resourcekey="DataListProductsResource1">
                            <ItemStyle CssClass="ItemStyle" VerticalAlign="Top" />
                            <ItemTemplate>

                                <table class="UnderlineText">
                                    <tr >
                                        <td></td>
                                        <td>
                                            <div class="ProductLink">
                                                <asp:HyperLink ID="HyperLink2" EnableViewState="false" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>' NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink") %>'>  </asp:HyperLink>
                                                <asp:Image ID="NewItemImage" ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.gif") %>'
                                            runat="server" Visible='<%# DataBinder.Eval(Container.DataItem, "NewProductInd") %>' />
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="ProductImage">
                                            <div class="Image">
                                                 <a id="imgProduct" class="boxed"  href='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>'>
                                                <img id='Img1' alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>'
                                                    border="0" src='<%# ResolveUrl(DataBinder.Eval(Container.DataItem, "SmallImageFilePath").ToString()) %>'
                                                    style="vertical-align: bottom;"></a>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="Price">
                                                <asp:Label ID="Label2" EnableViewState="false" runat="server" meta:resourcekey="Label2Resource1"
                                                    Text='<%# DataBinder.Eval(Container.DataItem, "FormattedPrice") %>'
                                                    Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && ProductListProfile.ShowPrice %>'></asp:Label>
                                            </div>
                                            <div class="CallForPrice">
                                                <asp:Label ID="uxCallForPricing" runat="server" CssClass="Price" meta:resourcekey="uxCallForPricingResource1"
                                                    Text='<%# CheckForCallForPricing(DataBinder.Eval(Container.DataItem, "CallForPricing"), DataBinder.Eval(Container.DataItem, "CallMessage")) %>'></asp:Label>

                                            </div>
                                            <br />
                                            <div class="BoldTitle">
                                                ITEM : <%# DataBinder.Eval(Container.DataItem, "ProductNum").ToString() %>
                                            </div>
                                            <br />
                                            <div class="ShortDescription">
                                                <%# DataBinder.Eval(Container.DataItem, "ShortDescription").ToString() %>
                                            </div>
                                            <br />
                                            <div class="StarRating">
                                            Customer Reviews:<ZNode:ProductAverageRating ID="ProductAverageRating1"
                                                    TotalReviews='<%# DataBinder.Eval(Container.DataItem, "TotalReviews") %>'
                                                    ViewProductLink='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink")%>'
                                                    ReviewRating='<%# DataBinder.Eval(Container.DataItem, "ReviewRating")%>' runat="server" />
                                            </div>
                                            <br />
                                            <div class="NoUnderlineText">
                                             <div style="float: left;">
                                <asp:HyperLink ID="btnView" CssClass="Button View" EnableViewState="false" meta:resourcekey="btnbuyResource1"
                                    runat="server" Visible='<%# !(bool)DataBinder.Eval(Container.DataItem, "CallForPricing") && ProductListProfile.ShowAddToCart %>'
                                     NavigateUrl='<%# DataBinder.Eval(Container.DataItem, "ViewProductLink") %>'>View »</asp:HyperLink>
                                </div>
                                  </div>
                                  <br /><br />
                                        </td>
                                    </tr>
                                </table>

                            </ItemTemplate>
                        </asp:DataList>
                    </div>
                    <div class="space"> </div>
                </div>
                <div class="BottomPaging" style="margin-left: 10px;">
                    <div class="Sorting">
                        <span class="Label">
                            <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:CommonCaption, Sort%>"></asp:Localize>
                        </span>
                        <asp:DropDownList ID="LstFilterBottom" runat="server" OnSelectedIndexChanged="LstFilterBottom_SelectedIndexChanged"
                            AutoPostBack="True">
                            <asp:ListItem Value="1" meta:resourcekey="ListItemResource1">Relevance</asp:ListItem>
                            <asp:ListItem Value="2" meta:resourcekey="ListItemResource2">Price Low to High</asp:ListItem>
                            <asp:ListItem Value="3" meta:resourcekey="ListItemResource3">Price High to Low</asp:ListItem>
                            <asp:ListItem Value="4" meta:resourcekey="ListItemResource4">Product A-Z</asp:ListItem>
                            <asp:ListItem Value="5" meta:resourcekey="ListItemResource5">Product Z-A</asp:ListItem>
                            <asp:ListItem Value="6" meta:resourcekey="ListItemResource6">Highest Rated</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="Paging">
                        <asp:HyperLink ID="hlBotPrevLink" CssClass="Button" Text="<<" runat="server"></asp:HyperLink>
                        <span>
                            <asp:Localize ID="Localize3" runat="server" meta:resourceKey="txtPage"></asp:Localize>
                            <asp:Localize ID="Localize4" runat="server" meta:resourceKey="txtOF"></asp:Localize>
                        </span>
                        <asp:HyperLink ID="hlBotNextLink" Text=">>" CssClass="Button" runat="server"></asp:HyperLink>
                        <span>|<span class="ShowText">
                            <asp:DropDownList runat="server" ID="ddlBottomPaging" CssClass="Pagingdropdown" AutoPostBack="True"
                                OnSelectedIndexChanged="DdlBottomPaging_SelectedIndexChanged" meta:resourcekey="ddlBottomPagingResource1">
                            </asp:DropDownList>
                        </span></span><span>
                            <asp:Localize ID="Localize1" runat="server" meta:resourceKey="txtProducts"></asp:Localize><span
                                class="SlashSeparator">/</span>
                            <asp:Literal ID="Literal4" runat="server" Text="<%$ Resources:CommonCaption, Page%>" /></span>
                    </div>
                </div>

        </div>

        </div>
        
    </div>
</asp:Panel>