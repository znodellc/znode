<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Common_LoginStatus" Codebehind="LoginStatus.ascx.cs" %>
<span>
    <asp:LoginStatus ID="uxUserLoginStatus" runat="server" LoginText="" OnLoggingOut="UxUserLoginStatus_LoggingOut"
        OnLoggedOut="UxUserLoginStatus_LoggedOut" />
</span>