using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

/// <summary>
/// Displays the logo
/// </summary>
namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Logo user control class.
    /// </summary>
    public partial class Controls_Default_Common_Logo : System.Web.UI.UserControl
    {
        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            int logoWidth = 200;
            int logoHeight= 75;
            ZNodeImage znodeImage = new ZNodeImage();
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogoWidth"]))
            {
                logoWidth = Convert.ToInt32(ConfigurationManager.AppSettings["LogoWidth"]);
            }

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogoHeight"]))
            {
                logoHeight = Convert.ToInt32(ConfigurationManager.AppSettings["LogoHeight"]);
            }

            System.Drawing.Size dimension = new System.Drawing.Size();
            dimension.Width = logoWidth;
            dimension.Height= logoHeight;
			string orginalImageDirectory = string.Format("{0}images/catalog/original/turnkey/{1}/", ZNodeConfigManager.EnvironmentConfig.DataPath, ZNodeConfigManager.SiteConfig.PortalID);
			uxLogoImage.ImageUrl = Path.Combine(orginalImageDirectory, Path.GetFileName(ZNodeConfigManager.SiteConfig.LogoPath));
        }
        #endregion
    }
}