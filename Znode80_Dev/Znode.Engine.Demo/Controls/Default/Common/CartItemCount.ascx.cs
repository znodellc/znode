using System;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;

/// <summary>
/// Displays number of items in the shopping cart
/// </summary>
namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the CartItemCount user control class.
    /// </summary>
    public partial class Controls_Default_Common_CartItemCount : System.Web.UI.UserControl
    {
        #region Private Variables
        private string CartItemCount = string.Empty;
        #endregion

        #region Public Methods
        public void CalcCartItemCount()
        {
            ZNodeShoppingCart shoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);

            if (shoppingCart != null)
            {
                lblCartItemCount.Text = shoppingCart.ShoppingCartItems.Count.ToString();
            }
            else
            {
                lblCartItemCount.Text = "0";
            }
        }
       #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get shopping cart
            this.CalcCartItemCount();
        }
        #endregion
    }
}