using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Catalog;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the LoginStatus user control class.
    /// </summary>
    public partial class Controls_Default_Common_LoginStatus : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Raised when the user clicks the logout link.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxUserLoginStatus_LoggingOut(object sender, LoginCancelEventArgs e)
        {           
            // Clearing a per-user cache of objects here.            
            Session.Remove(ZNode.Libraries.Framework.Business.ZNodeSessionKeyType.UserAccount.ToString());
            Session["ProfileCache"] = null; // Reset profile cache    
            Session.Abandon();
            FormsAuthentication.SignOut();
        }

        /// <summary>
        /// Raised after the user clicks the logout link and the logout process is complete.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxUserLoginStatus_LoggedOut(object sender, EventArgs e)
        {
            ZNodeCacheDependencyManager.Remove("SpecialsNavigation" + ZNodeConfigManager.SiteConfig.PortalID + ZNodeCatalogManager.LocaleId + HttpContext.Current.Session.SessionID);
          
            ZNode.Libraries.ECommerce.Catalog.ZNodeProfile profile = new ZNode.Libraries.ECommerce.Catalog.ZNodeProfile();
            Response.Redirect(Request.Url.ToString());
        }
    }
}