<%@ Control Language="C#" EnableViewState="false" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Common_CartItemCount" CodeBehind="CartItemCount.ascx.cs" %>
		<span id="CartItemCount"><a id="A1" href="ShoppingCart.aspx" class="Button">
			<span class="Text">
				<asp:Localize ID="lblViewCart" runat="server" meta:resourceKey="lblViewCart">
				</asp:Localize>
				<span class="CartItemCountText">
					<asp:Label ID="lblCartItemCount" runat="server"></asp:Label>
					Item<span class="ItemsText">(s)</span>
				</span>
			</span>
		</a>
		</span>
