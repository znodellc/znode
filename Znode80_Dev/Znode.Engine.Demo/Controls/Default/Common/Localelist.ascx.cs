﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the LocaleList user control class.
    /// </summary>
    public partial class Controls_Default_Common_Localelist : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                this.Binddropdown();
            }
        }

        protected void Binddropdown()
        {
            PortalCatalogService portalCatalogService = new PortalCatalogService();
            TList<PortalCatalog> portalCatalogList = portalCatalogService.GetByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

            if (portalCatalogList.Count > 0)
            {
                portalCatalogService.DeepLoad(portalCatalogList, true, DeepLoadType.IncludeChildren, typeof(Locale));

                foreach (PortalCatalog pc in portalCatalogList)
                {
                    ListItem item = new ListItem(pc.LocaleIDSource.LocaleDescription, pc.LocaleIDSource.LocaleCode);

                    ddlLanguage.Items.Add(item);
                }

                if (Session["CurrentUICulture"] != null)
                {
                    ListItem title = ddlLanguage.Items.FindByValue(Session["CurrentUICulture"].ToString());

                    if (title != null)
                    {
                        title.Selected = true;
                    }
                }
            }
        }
    }
}