<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Specials_BestSellers" CodeBehind="BestSellers.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/Reviews/ProductAverageRating.ascx" TagName="ProductAverageRating" TagPrefix="ZNode" %>

<asp:Panel ID="pnlBestSellerList" runat="server"
    meta:resourcekey="pnlBestSellerListResource1">
    <script language="javascript" type="text/javascript">
        var bestSellers;
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(PageLoadedEventHandler);

        //
        function PageLoadedEventHandler() {
            window.addEvents({
                'domready': function () {
                    /* thumbnails example , div containers */
                    bestSellers = new SlideItMoo({
                        overallContainer: 'BestSellers_outer',
                        elementScrolled: 'BestSellers_inner',
                        thumbsContainer: 'BestSellers_Items',
                        itemsVisible: 2,
                        elemsSlide: 1,
                        duration: 300,
                        itemsSelector: '.BestSellerItem',
                        showControls: 1
                    });
                }
            });
        }
    </script>

    <div class="BestSeller">
        <div id="BestSellers_outer">
            <div id="BestSellers_inner">
                <div id="BestSellers_Items">
                    <asp:DataList ID="DataListBestSeller" runat="server"
                        RepeatDirection="Horizontal" RepeatLayout="Flow"
                        meta:resourcekey="DataListBestSellerResource1" EnableViewState="false">
                        <ItemStyle CssClass="ItemStyle" />
                        <ItemTemplate>
                            <div class="BestSellerItem">
                                <div class="ItemBorder"></div>
                                <div class="DetailLink">
                                    <asp:HyperLink ID="hlName" runat="server" CssClass="DetailLink"
                                        meta:resourcekey="hlNameResource1"
                                        NavigateUrl='<%# GetViewProductPageUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>'
                                        Text='<%# DataBinder.Eval(Container.DataItem, "Name").ToString() %>'
                                        Visible="<%# ShowName %>"></asp:HyperLink>
                                    <asp:Image ID="NewItemImage" runat="server"
                                        ImageUrl='<%# ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.GetImagePathByLocale("new.gif") %>'
                                        meta:resourcekey="NewItemImageResource1"
                                        Visible='<%# DataBinder.Eval(Container.DataItem, "NewProductInd") %>' />
                                </div>
                                <div class="Image">
                                    <div>
                                        <a id="A1" runat="server"
                                            href='<%# GetViewProductPageUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>'>
                                            <img id="Img1" runat="server"
                                                alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag") %>' border="0"
                                                src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString()) %>'></img>
                                        </a>
                                    </div>
                                </div>
                                <div class="ItemBorder"></div>
                                <div style="clear:both;"></div>
                                <div class="NamePriceDetail">
                                    <span class="Name">
                                        <asp:Label ID="Label1" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Name").ToString()%>'></asp:Label>
                                  </span>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
                </div>
            </div>
        </div>
    </div>
</asp:Panel>
