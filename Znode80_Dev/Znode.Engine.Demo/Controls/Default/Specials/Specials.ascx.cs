using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

/// <summary>
/// Displays home page specials
/// </summary>
namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Specials user control class
    /// </summary>
    public partial class Controls_Default_Specials_Specials : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProfile _SpecialProfile = new ZNodeProfile();        
        private ZNodeProductList _ProductList;
        private string _Title = string.Empty;
        private string BuyImage = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/ViewAdmin.gif";
        private ZNodeUserAccount _userAccount = null;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the profile.
        /// </summary>
        public ZNodeProfile SpecialProfile
        {
            get { return this._SpecialProfile; }
            set { this._SpecialProfile = value; }
        }

        /// <summary>
        /// Gets or sets the Product list
        /// </summary>
        public ZNodeProductList ProductList
        {
            get
            {
                return this._ProductList;
            }

            set
            {
                this._ProductList = value;
            }
        }

        /// <summary>
        /// Gets or sets the title for this control
        /// </summary>
        public string Title
        {
            get
            {
                return this._Title;
            }

            set
            {
                this._Title = value;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (HttpContext.Current.Session[ZNodeSessionKeyType.UserAccount.ToString()] != null)
                {
                    this._userAccount = ZNodeUserAccount.CurrentAccount();
                    if (this._userAccount.EnableCustomerPricing && ZNodeConfigManager.SiteConfig.EnableCustomerPricing.GetValueOrDefault(false))
                    {
                        return;
                    }
                }
                this._ProductList = ZNodeProductList.GetHomePageSpecials(ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
                this.BindProducts();
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind display based on a product list
        /// </summary>
        public void BindProducts()
        {
            if (this._ProductList.ZNodeProductCollection.Count == 0)
            {
                pnlProductList.Visible = false;
            }
            else
            {
                pnlProductList.Visible = true;
            }

            DataListProducts.DataSource = this._ProductList.ZNodeProductCollection;
            DataListProducts.DataBind();
        }
        #endregion

        #region Helper Functions
        /// <summary>
        /// Represents the Check for Call For Pricing method
        /// </summary>
        /// <param name="fieldValue">The field value</param>
        /// <returns>Returns the Call For Pricing message</returns>
        public string CheckForCallForPricing(object fieldValue, object callMessage)
        {
            MessageConfigAdmin mconfig = new MessageConfigAdmin();
            bool Status = bool.Parse(fieldValue.ToString());

            if (Status)
            {
                if (callMessage != null && !string.IsNullOrEmpty(callMessage.ToString()))
                {
                    return callMessage.ToString();
                }

                return mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
            }
            else if (!this.SpecialProfile.ShowPrice)
            {
                return mconfig.GetMessage(ZNodeMessageKey.ProductCallForPricing, Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0)), 43);
            }
            else
            {
                return string.Empty;
            }
        }      

        /// <summary>
        /// Represents the ShowPricing Method
        /// </summary>
        /// <param name="CallForPricing">Call For Pricing</param>
        /// <returns>Returns a bool value to show Call For Pricing</returns>
        private bool ShowPricing(bool CallForPricing)
        {
            if (CallForPricing)
            {
                return false;
            }
            else if (!this.SpecialProfile.ShowPrice)
            {
                return false;
            }

            return true;
        }
        #endregion
    }
}