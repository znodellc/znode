using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Navigation Specials user control class
    /// </summary>
    public partial class Controls_Default_Specials_NavigationSpecials : System.Web.UI.UserControl
    {
        #region Private Variables
        private bool _EnableNavigationViewState = true;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value indicating whether the treeview control persists its view
        /// state, to the requesting client.
        /// <remarks>true if the server control maintains its view state; 
        /// otherwise false. The default is true.</remarks>
        /// </summary>
        public bool EnableNavigationViewState
        {
            get
            {
                return this._EnableNavigationViewState;
            }

            set
            {
                this._EnableNavigationViewState = value;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack || !this._EnableNavigationViewState)
            {
                ctrlNavigation.EnableViewState = this._EnableNavigationViewState;

                this.BindData();
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the treeview
        /// </summary>
        private void BindData()
        {
            ZNodeNavigation navigation = new ZNodeNavigation();
            navigation.PopulateSpecialsTreeView(ctrlNavigation);

            if (ctrlNavigation.Nodes.Count == 0)
            {
                pnlProductList.Visible = false;
            }
            else
            {
                pnlProductList.Visible = true;
            }
        }
        #endregion
    }
}