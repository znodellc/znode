using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Demo
{
    public partial class Controls_Default_Content_Content : System.Web.UI.UserControl
    {
        private string _pageName = string.Empty;

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // get product id from querystring  
            if (Request.Params["page"] != null)
            {
                this._pageName = Request.Params["page"];
            }
            else if (Request.Params["zpgid"] != null)
            {
                int zpgid = Convert.ToInt32(Request.Params["zpgid"]);
                ContentPage contentPage = ZNodeContentManager.GetPageById(zpgid);
                this._pageName = contentPage.Name;
            }
            else
            {
                this._pageName = "home";
            }

            // Title
            if (HttpContext.Current.Items["PageTitle"] != null)
            {
                // HTML 
                lblHtml.Text = ZNodeContentManager.GetPageHTMLByName(this._pageName, ZNodeConfigManager.SiteConfig.PortalID);

                if (HttpContext.Current.Items["PageTitle"] != null)
                {
                    lblTitle.Text = HttpContext.Current.Items["PageTitle"].ToString();
                }
            }
        }
        #endregion
    }
}