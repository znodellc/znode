<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.Controls_Default_Content_Content" Codebehind="Content.ascx.cs" %>

<div class="ContentPage">
	<div class="PageTitle"><h1><asp:Label ID="lblTitle" runat="server" EnableViewState="false"></asp:Label></h1></div>
	<div class="SubTitle"><asp:Label ID="lblHtml" runat="server" EnableViewState="false"></asp:Label></div>
</div>