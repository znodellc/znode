﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the ShoppingCart Page class.
    /// </summary>
    public partial class ShoppingCart : CommonPageBase
    {
        protected override void Page_PreInit(object sender, EventArgs e)
        {
            // If request comes from mobile then load the theme from ZNodePortal.MobileTheme
            if (Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null)
            {
                this.MasterPageFile = "~/themes/" + ZNodeConfigManager.SiteConfig.MobileTheme + "/MasterPages/shoppingcart.master";
            }
            else
            {
                // Clear the current category session theme if some other category selected.
                Session["CurrentCategoryTheme"] = null;

                this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/shoppingcart.master";
            }
        }
    }
}
