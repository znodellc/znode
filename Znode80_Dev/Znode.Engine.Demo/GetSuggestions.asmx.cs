﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using ZNode.Libraries.ECommerce.Catalog;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Summary description for GetSuggestions
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
     [System.Web.Script.Services.ScriptService]
    public class GetSuggestions : System.Web.Services.WebService
    {

        [WebMethod(EnableSession =true)]
        [ScriptMethod(UseHttpGet = false,ResponseFormat = ResponseFormat.Json)]
        public string TypeAhead(string term, string category)
        {
            
            ZNodeSearchEngine engine = new ZNodeSearchEngine();

            var respone = engine.SuggestTermsFor(term, ValidateCategoryString(category));

              // Return JSON data
           JavaScriptSerializer js = new JavaScriptSerializer();
          string strJSON = js.Serialize(respone);
          
            return strJSON;
        }

        private string ValidateCategoryString(string category)
        {
            if (!string.IsNullOrEmpty(category))
            {
                if(category.Equals(Resources.CommonCaption.AllDepartmentsText,StringComparison.OrdinalIgnoreCase))
                    return string.Empty;


            }

            return category;
        }
    }
}
