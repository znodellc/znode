//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.17929
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources.main {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "11.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class master {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal master() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.main.master", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Maxwell&apos;s Fine Foods is an grocery store specaializing in high quality organic foods, consectetur adipiscing elit. Vivamus rhoncus suscipit leo, sed faucibus turpis rhoncus etc. Nunc at velit risus. Aluquam quam arcu, tempus vel commodo semper, nec dolor. Quisque sollicitudin arcu a orci mattis ut gravida lorem dapibus. Lorem ipsum dolor sit amet, consectetur adipiscinf elit. Quisque ac mi porttitor sem hndrerit feugiat. Morbiornare leo ut fermentum. Cras turpis eros, congue et auctoriaculis, egestas eu Ali [rest of string was truncated]&quot;;.
        /// </summary>
        internal static string footerCompanyDescription {
            get {
                return ResourceManager.GetString("footerCompanyDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Customer Service.
        /// </summary>
        internal static string footerCustomerService {
            get {
                return ResourceManager.GetString("footerCustomerService", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Home.
        /// </summary>
        internal static string footerHome {
            get {
                return ResourceManager.GetString("footerHome", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Account.
        /// </summary>
        internal static string footerMyAccount {
            get {
                return ResourceManager.GetString("footerMyAccount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Privacy Policy.
        /// </summary>
        internal static string footerPrivacyPolicy {
            get {
                return ResourceManager.GetString("footerPrivacyPolicy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Return Policy.
        /// </summary>
        internal static string footerReturnPolicy {
            get {
                return ResourceManager.GetString("footerReturnPolicy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to SECURED BY:.
        /// </summary>
        internal static string footerSecuredBy {
            get {
                return ResourceManager.GetString("footerSecuredBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Shipping.
        /// </summary>
        internal static string footerShipping {
            get {
                return ResourceManager.GetString("footerShipping", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Site Map.
        /// </summary>
        internal static string footerSiteMap {
            get {
                return ResourceManager.GetString("footerSiteMap", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Store Locator.
        /// </summary>
        internal static string footerStoreLocator {
            get {
                return ResourceManager.GetString("footerStoreLocator", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Terms &amp; Conditions.
        /// </summary>
        internal static string footerTermsandConditons {
            get {
                return ResourceManager.GetString("footerTermsandConditons", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to WE ACCEPT:.
        /// </summary>
        internal static string footerWeAccept {
            get {
                return ResourceManager.GetString("footerWeAccept", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to About Us.
        /// </summary>
        internal static string topLinkAboutUsResource {
            get {
                return ResourceManager.GetString("topLinkAboutUsResource", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Contact Us.
        /// </summary>
        internal static string topLinkContactUsResource {
            get {
                return ResourceManager.GetString("topLinkContactUsResource", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to FAQs.
        /// </summary>
        internal static string topLinkFaqsResource {
            get {
                return ResourceManager.GetString("topLinkFaqsResource", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to My Account.
        /// </summary>
        internal static string topLinkMyAccountResource {
            get {
                return ResourceManager.GetString("topLinkMyAccountResource", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Store Locator.
        /// </summary>
        internal static string topLinkStoreLocatorResource {
            get {
                return ResourceManager.GetString("topLinkStoreLocatorResource", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Live Chat.
        /// </summary>
        internal static string txtLiveChat {
            get {
                return ResourceManager.GetString("txtLiveChat", resourceCulture);
            }
        }
    }
}
