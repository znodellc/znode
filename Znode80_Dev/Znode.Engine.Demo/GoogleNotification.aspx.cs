﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Google Notification Page class.
    /// </summary>
    public partial class GoogleNotification : ZNodePageBase
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/GoogleNotification.master";
        }

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // HTML 
            HtmlGenericControl include = new HtmlGenericControl("link");
            include.Attributes.Add("type", "text/css");
            include.Attributes.Add("rel", "stylesheet");
            include.Attributes.Add("href", "themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/css/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.CSS + ".css");

            // Add a reference for StyleSheet to the head section
            this.Page.Header.Controls.Add(include);
        }
    }
}
