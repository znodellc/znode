﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the QuickWatch Page class.
    /// </summary>
    public partial class QuickWatch : CommonPageBase
    {
        protected override void Page_PreInit(object sender, EventArgs e)
        {
            string theme = ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme;
            
            // If category theme wise theme selected then apply the current category theme.
            if (Session["CurrentCategoryTheme"] != null)
            {
                theme = Session["CurrentCategoryTheme"].ToString();
            }

            this.MasterPageFile = string.Format("~/themes/{0}/MasterPages/QuickWatch.master", ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme);
        }
    }
}
