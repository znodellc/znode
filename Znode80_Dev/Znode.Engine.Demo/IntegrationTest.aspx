<%@ Page Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Demo.IntegrationTest" Codebehind="IntegrationTest.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Integration Test Harness</title>
</head>
<body>
    <form id="form1" runat="server">
    <h1>Znode Integration Test Harness</h1>
    <p>Use this page to quickly test integration points. You can add your own test cases here..</p>
    <div>
        <h4>Test Results</h4>
        <asp:Label ID="lblMsg" runat="server" Text=''></asp:Label><br />
    </div>      
    </form>
</body>
</html>
