<%@ Page Language="C#" AutoEventWireup="True" Title="Error"  MasterPageFile="Themes/Default/MasterPages/main.master" Inherits="Znode.Engine.Demo.ErrorPageBase" Codebehind="ErrorPageBase.cs" %>





<asp:content id="Content1" ContentPlaceHolderID="MainContent" runat="server">
  

    
        <div align="center" style=" font-family:Arial; text-align:left; padding:20px; margin:20px;" id="Container">
            <h1>An Error has Occurred</h1>
           
            <div>
                An error occurred because of which we were unable to complete your request. Please click on the browser's back button to go back to the
                website. We apologize for the inconvenience.
            </div> 
            <br /><br />
            <div style=" font-size:14px;">
                <div style=" padding-bottom:5px;"><b>Stack Trace</b></div>
              
            </div>
            <br /><br />
            <div>
                <asp:Label ID="lblException" runat="server"></asp:Label>
            </div> 
            <hr />   
            <div style=" font-size:12px;">Copyright 2010, Znode Inc., All Rights Reserved. <a href="http://www.znode.com">www.znode.com</a></div> 
        </div>
    
        </asp:content>


