﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the HighlightInfo Page class.
    /// </summary>
    public partial class HighlightInfo : CommonPageBase
    {
        protected override void Page_PreInit(object sender, EventArgs e)
        {
            // Clear the current category session theme if some other category selected.
            Session["CurrentCategoryTheme"] = null;

            this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/HighlightInfo.master";
        }
    }
}
