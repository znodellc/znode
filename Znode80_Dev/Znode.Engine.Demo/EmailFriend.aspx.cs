﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Email Friend Page class.
    /// </summary>
    public partial class EmailFriend : CommonPageBase
    {
        #region Private Variables
        private int _productId;
        private ZNodeProduct _product;
        #endregion

        /// <summary>
        /// Page Pre-Initialization Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected override void Page_PreInit(object sender, EventArgs e)
        {
            // Clear the current category session theme if some other category selected.
            Session["CurrentCategoryTheme"] = null;

            this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/emailfriend.master";

            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this._productId = int.Parse(Request.Params["zpid"]);
            }
            else
            {
                throw new ApplicationException("Invalid Product Id");
            }

            // Retrieve product data
            this._product = ZNodeProduct.Create(this._productId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

            // Add to http context so it can be shared with user controls
            HttpContext.Current.Items.Add("Product", this._product);
        }
    }
}
