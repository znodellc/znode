﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the PaymentThankyou Page class.
    /// </summary>
    public partial class PaymentThankyou : CommonPageBase
    {
        protected override void Page_PreInit(object sender, EventArgs e)
        {
            this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/PaymentThankyou.master";
        }
    }
}
