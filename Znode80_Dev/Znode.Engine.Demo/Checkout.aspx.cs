﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Checkout Page class.
    /// </summary>
    public partial class Checkout : CommonPageBase 
    {
        protected override void Page_PreInit(object sender, EventArgs e)
        {
             // If request comes from mobile then load the theme from ZNodePortal.MobileTheme
            if (Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null)
            {
                this.MasterPageFile = "~/themes/" + ZNodeConfigManager.SiteConfig.MobileTheme + "/MasterPages/checkout.master";
            }
            else
            {
                // Clear the current category session theme if some other category selected.
                Session["CurrentCategoryTheme"] = null;

                this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/checkout.master";
            }

			this.Master.Master.FindControl("SearchandLinks").Visible = false;
			this.Master.Master.FindControl("Menu").Visible = false;

            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {
                    string inURL = Request.Url.ToString();
                    string outURL = inURL.ToLower().Replace("http://", "https://");

                    Response.Redirect(outURL);
                }
            }

            Response.ClearHeaders();

            // HTTP 1.1
            Response.AppendHeader("Cache-Control", "no-cache");
            Response.AppendHeader("Cache-Control", "private");
            Response.AppendHeader("Cache-Control", "no-store"); 
            Response.AppendHeader("Cache-Control", "must-revalidate"); 
            Response.AppendHeader("Cache-Control", "max-stale=0");
            Response.AppendHeader("Cache-Control", "post-check=0");
            Response.AppendHeader("Cache-Control", "pre-check=0"); 
            Response.AppendHeader("Pragma", "no-cache"); 
            Response.AppendHeader("Keep-Alive", "timeout=3, max=993"); 
            Response.AppendHeader("Expires", "Mon, 26 Jul 1997 05:00:00 GMT"); 

            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
        }
    }
}
