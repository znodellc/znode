﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Ecommerce.Entities;
using ZNode.Libraries.Search.LuceneSearchProvider.Search;

namespace Znode.Engine.Demo
{
    public partial class UpdateLuceneReader : System.Web.UI.Page
    {
        
        
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!Page.IsPostBack)
            {
                ReloadLuceneIndex(Request.HttpMethod,Request.Params["Key"]);
            }

        }

      
        private void ReloadLuceneIndex(string method, string key)
        {
            if (method == "POST" && key == ConfigurationManager.AppSettings[LuceneSearchManager.LuceneIndexReaderKey])
            {
                ZNodeSearchEngine search = new ZNodeSearchEngine();
                search.UpdateReader();

            }
        
        }
    }
}