Please Note: 
>> The following applies only to Open Source applications which are included with Znode Multifront. 
>> Please refer to the Znode Multifront EULA for Znode Multifront's License agreement.		  

You can find a copy of the MIT license for JQuery, Mootools and SqueezeBox at this URL.
http://www.opensource.org/licenses/mit-license.php

A copy of the TinyMCE license can be found at this URL.
http://tinymce.moxiecode.com/js/tinymce/jscripts/tiny_mce/license.txt

