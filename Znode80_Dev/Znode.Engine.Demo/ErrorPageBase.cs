using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the ErrorPageBase Page class.
    /// </summary>
    public partial class ErrorPageBase : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
           
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.IsDebuggingEnabled)
            {
                var ex = Server.GetLastError();
                if (ex != null)
                {

                    lblException.Text = ex.StackTrace;
                }
            }
        }
    }
}