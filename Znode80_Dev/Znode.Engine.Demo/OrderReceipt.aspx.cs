﻿using System;
using System.Web;
using System.Web.UI.HtmlControls;
using Znode.Engine.Common;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the OrderReceipt Page class.
    /// </summary>
    public partial class OrderReceipt : CommonPageBase
    {
        protected override void Page_PreInit(object sender, EventArgs e)
        {
            if (HttpContext.Current.Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null)
            {
                this.MasterPageFile = "~/themes/" + ZNodeConfigManager.SiteConfig.MobileTheme + "/MasterPages/OrderReceipt.master";
            }
            else
            {
                // Clear the current category session theme if some other category selected.
                Session["CurrentCategoryTheme"] = null;

                this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/OrderReceipt.master";
            }
        }

        /// <summary>
        /// Set the Header Section
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected override void Page_Load(object sender, EventArgs e)
        {
            // HTML 
            HtmlGenericControl cssInclude = new HtmlGenericControl("link");
            cssInclude.Attributes.Add("type", "text/css");
            cssInclude.Attributes.Add("rel", "stylesheet");
            if (HttpContext.Current.Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null)
            {
                cssInclude.Attributes.Add("href", ZNodeCatalogManager.GetCssPathByLocale(ZNodeConfigManager.SiteConfig.MobileTheme, ZNodeCatalogManager.CSS));
            }
            else
            {
                cssInclude.Attributes.Add("href", ZNodeCatalogManager.GetCssPathByLocale(ZNodeCatalogManager.Theme, ZNodeCatalogManager.CSS));
            }

            // Add a reference for StyleSheet to the head section
            this.Page.Header.Controls.Add(cssInclude);
            cssInclude = new HtmlGenericControl("link");
            cssInclude.Attributes.Add("type", "text/css");
            cssInclude.Attributes.Add("rel", "stylesheet");
            if (HttpContext.Current.Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null)
            {
                cssInclude.Attributes.Add("href", ZNodeCatalogManager.GetReceiptCssPathByLocale(ZNodeConfigManager.SiteConfig.MobileTheme, "Receipt"));
            }
            else
            {
                cssInclude.Attributes.Add("href", ZNodeCatalogManager.GetReceiptCssPathByLocale(ZNodeCatalogManager.Theme, "Receipt"));
            }

            // Add a reference for StyleSheet to the head section
            this.Page.Header.Controls.Add(cssInclude);
        }
    }
}
