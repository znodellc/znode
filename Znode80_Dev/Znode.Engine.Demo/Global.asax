<%@ Application Language="C#" Inherits="System.Web.HttpApplication" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="StructureMap" %>
<%@ Import Namespace="Znode.Engine.Promotions" %>
<%@ Import Namespace="Znode.Engine.Shipping" %>
<%@ Import Namespace="Znode.Engine.Suppliers" %>
<%@ Import Namespace="Znode.Engine.Taxes" %>
<%@ Import Namespace="ZNode.Libraries.ECommerce.Analytics" %>
<%@ Import Namespace="ZNode.Libraries.ECommerce.Catalog" %>
<%@ Import Namespace="ZNode.Libraries.ECommerce.SEO" %>
<%@ Import Namespace="ZNode.Libraries.ECommerce.ShoppingCart" %>
<%@ Import Namespace="ZNode.Libraries.ECommerce.Utilities" %>
<%@ Import Namespace="ZNode.Libraries.Framework.Business" %>
<%@ Import Namespace="ZNode.Libraries.Search.Interfaces" %>
<%@ Import Namespace="ZNode.Libraries.Search.LuceneSearchProvider.Search" %>

<script runat="server">	
	void Application_Start(object sender, EventArgs e)
	{
		TryDatabaseConnection();
		TryDataFolderPermissions();
        TryCacheActivePromotions();
        TryCacheAvailablePromotionTypes();
		TryCacheAvailableShippingTypes();
        TryCacheAvailableSupplierTypes();
		TryCacheAvailableTaxTypes();
        TryMapSearchProvider();
		TryLogApplicationStart();
	}

	protected void Application_PreRequestHandlerExecute(Object sender, EventArgs e)
	{
		// This call must be placed in the PreRequest handler for order desk to work
		if (Request.Path.IndexOf("OrderDesk.aspx", StringComparison.OrdinalIgnoreCase) < 0 && HttpContext.Current.Session != null)
		{
			if (Request.Path.IndexOf("OrderDeskReceipt.aspx", StringComparison.OrdinalIgnoreCase) < 0)
			{
				ZNodeConfigManager.UnAliasSiteConfig();
			}
		}

		if (HttpContext.Current.Session != null)
		{
			// Set culture settings
			ZNodeCurrencyManager.SetPageCulture();

			// This call must be placed to remove session object used for category and search page
			if (Request.Path.IndexOf("Search.aspx", StringComparison.OrdinalIgnoreCase) < 0 &&
			    Request.Path.IndexOf("Category.aspx", StringComparison.OrdinalIgnoreCase) < 0 &&
			    Request.Path.IndexOf("default.aspx", StringComparison.OrdinalIgnoreCase) < 0 &&
			    Request.Path.IndexOf("quickwatch.aspx", StringComparison.OrdinalIgnoreCase) < 0)
			{
				HttpContext.Current.Session.Remove("ProductList");
			}
		}
	}

	protected void Application_BeginRequest(Object sender, EventArgs e)
	{
		// Set the store config for the URL that has been requested. This call must be placed in the BeginRequest for multi stores to work
		if (ZNodeConfigManager.CheckSiteConfigCache() == false)
		{
			if (ZNodeConfigManager.SetSiteConfig() == false)
			{
				// The URL was not found in our config, send out a 404 error
				HttpContext.Current.Response.StatusCode = 404;
				HttpContext.Current.Response.SuppressContent = true;
				HttpContext.Current.ApplicationInstance.CompleteRequest();
				return;
			}
		}

		// Check that DomainConfig exists
		if (ZNodeConfigManager.DomainConfig != null)
		{
			// Check the URL, if it's disabled then return 404 error page
			if (ZNodeConfigManager.DomainConfig.IsActive == false)
			{
				// The URL was not found in our config, send out a 404 error
				HttpContext.Current.Response.StatusCode = 404;
				HttpContext.Current.Response.SuppressContent = true;
				HttpContext.Current.ApplicationInstance.CompleteRequest();
				return;
			}
		}

		var newPath = String.Empty;
		
		// This section rewrites product/cateogy/content path
		try
		{
			ZNodeCatalogManager.SetLocaleCode();

			var currentUrl = Request.Path.ToLower();

			// Only rewrite paths to our ASPX pages, not any internal links
			if (currentUrl.Contains(".aspx"))
			{
				if (currentUrl.IndexOf(".aspx") != currentUrl.LastIndexOf(".aspx"))
				{
					// Rewrites the URL, if .aspx repeats more than once
					HttpContext.Current.RewritePath(Request.Path);
				}
				else if (ZNodeSEOUrl.LocaleID > 0)
				{
					var localeId = ZNodeSEOUrl.LocaleID;
					var replacePath = ZNodeSEOUrl.RewriteUrlPath(currentUrl, out localeId);

					if (localeId != ZNodeSEOUrl.LocaleID)
					{
						newPath = String.Format("~/default.aspx?nlid={0}&redir={1}&{2}", localeId, HttpUtility.UrlEncode(replacePath), Request.QueryString);
					}

					if (replacePath.Length > 0 && newPath.Length == 0)
					{
						// Mark this URL as being found
						HttpContext.Current.Items.Add("SeoUrlFound", "1");

						// Get query string
						var queryString = Request.Url.Query.Replace("?", "&");

						// Add it back to the URL and rewrite the request
						HttpContext.Current.RewritePath(replacePath + queryString);
					}
					else if (currentUrl.ToLower().Contains("/login.aspx"))
					{
						// Redirect depending on the returnUrl
						var returnUrl = Request.QueryString["returnurl"];
						
						if (returnUrl.ToLower().Contains("/siteadmin"))
						{
							Response.Redirect(string.Format("~/siteadmin/default.aspx?ReturnUrl={0}", HttpUtility.UrlEncode(returnUrl)));
						}
						
						if (returnUrl.ToLower().Contains("/franchiseadmin"))
						{
							Response.Redirect(string.Format("~/franchiseadmin/default.aspx?ReturnUrl={0}", HttpUtility.UrlEncode(returnUrl)));
						}
						
						if (returnUrl.ToLower().Contains("/malladmin"))
						{
							Response.Redirect(string.Format("~/MallAdmin/default.aspx?ReturnUrl={0}", HttpUtility.UrlEncode(returnUrl)));
						}
					}
				}
			}
		}
		catch (Exception ex)
		{
			ZNodeLogging.LogMessage("Could not re-write URL path for SEO friendly URLs. Additional information: " + ex.Message);
		}

		if (newPath.Length > 0)
		{
			Response.Redirect(newPath, true);
		}
	}

	void Application_End(object sender, EventArgs e)
	{
		ZNodeLogging.LogActivity(2001);
	}

	void Application_Error(object sender, EventArgs e)
	{
		var ex = Server.GetLastError();

		if (ex is HttpRequestValidationException)
		{
			Response.Clear();
			Response.StatusCode = 200;
			
			var outputCode = new StringBuilder();
			outputCode.Append("&lt;html&gt;&lt;head&gt;&lt;title&gt;HTML Not Allowed&lt;/title&gt; ");
			outputCode.Append(" &lt;script language='JavaScript'&gt; ");
			outputCode.Append(" function back() { history.go(-1); } ");
			outputCode.Append(@"&lt;/script&gt; &lt;/head&gt;&lt;body style='font-family: Arial, Sans-serif;'&gt; ");
			outputCode.Append("&lt;h1&gt;Oops!&lt;/h1&gt; ");
			outputCode.Append("&lt;p&gt;We are sorry but you have entered invalid characters.&lt;/p&gt; ");
			outputCode.Append("&lt;p&gt;Please correct your entry and try again.");
			outputCode.Append("&lt;p&gt;&lt;a href='javascript:back()'&gt;Go back&lt;/a&gt;&lt;/p&gt; ");
			outputCode.Append("&lt;/body&gt;&lt;/html&gt;");
			
			Response.Write(HttpUtility.HtmlDecode(outputCode.ToString()));
			Response.End();
		}
	}

	void Session_Start(object sender, EventArgs e)
	{
		// Set culture settings
		ZNodeCurrencyManager.SetPageCulture();

		// Cache currency settings
		TryCacheCurrencySettings();

		// Make sure the profile cache exists and is cleared
		HttpContext.Current.Session.Add("ProfileCache", null);

		// Initialize tracking information
		TryInitializeTracking();

		// Ignore saved cart when admins are logged in and using the site
		var currentUrl = Request.Path.ToLower();
		
		if (!(currentUrl.Contains("/siteadmin/") || currentUrl.Contains("/malladmin/") || currentUrl.Contains("/franchiseadmin/")))
		{
			// Restore persistent cart, if any
			var savedCart = new ZNodeSavedCart();
			savedCart.InitializeShoppingCart();
		}
	}

	void Session_End(object sender, EventArgs e)
	{
		if (HttpContext.Current != null)
		{
			HttpContext.Current.Session.Remove("ProfileCache");
			HttpContext.Current.Session.Remove("ZnodeMultifrontRefer");
			HttpContext.Current.Session.Remove("ProductList");
		}
	}

	private void TryDatabaseConnection()
	{
		try
		{
			var cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["ZNodeECommerceDB"].ConnectionString);
			cnn.Open();
			cnn.Close();
			cnn.Dispose();
		}
		catch (Exception ex)
		{
			ZNodeLogging.LogMessage("Could not connect to the database. Additional information: " + ex.Message);
			throw new ApplicationException("Could not connect to the database. Additional information: " + ex.Message);
		}
	}

	private void TryDataFolderPermissions()
	{
		try
		{
			var file = new FileInfo(Server.MapPath("~/Data/Default/") + "test.txt");
			var filestream = file.Create();
			filestream.Close();
			file.Delete();
		}
		catch (Exception ex)
		{
			ZNodeLogging.LogMessage("Could not read/write to the Data folder; check permissions on this folder. Additional information: " + ex.Message);
			throw new ApplicationException("Could not read/write to the Data folder; check permissions on this folder. Additional information: " + ex.Message);
		}
	}
	
	private void TryCacheActivePromotions()
	{
		try
		{
			ZnodePromotionManager.CacheActivePromotions();
		}
		catch (Exception ex)
		{
			ZNodeLogging.LogMessage("Could not cache promotions. Additional information: " + ex.Message);
			throw new ApplicationException("Could not cache promotions. Additional information: " + ex.Message);
		}
	}

	private void TryCacheAvailablePromotionTypes()
	{
		try
		{
			ZnodePromotionManager.CacheAvailablePromotionTypes();
		}
		catch (Exception ex)
		{
			ZNodeLogging.LogMessage("Could not cache promotion types. Additional information: " + ex.Message);
			throw new ApplicationException("Could not cache promotion types. Additional information: " + ex.Message);
		}
	}
	
	private void TryCacheAvailableShippingTypes()
	{
		try
		{
			ZnodeShippingManager.CacheAvailableShippingTypes();
		}
		catch (Exception ex)
		{
			ZNodeLogging.LogMessage("Could not cache shipping types. Additional information: " + ex.Message);
			throw new ApplicationException("Could not cache shipping types. Additional information: " + ex.Message);
		}
	}

	private void TryCacheAvailableSupplierTypes()
	{
		try
		{
			ZnodeSupplierManager.CacheAvailableSupplierTypes();
		}
		catch (Exception ex)
		{
			ZNodeLogging.LogMessage("Could not cache supplier types. Additional information: " + ex.Message);
			throw new ApplicationException("Could not cache supplier types. Additional information: " + ex.Message);
		}
	}

	private void TryCacheAvailableTaxTypes()
	{
		try
		{
			ZnodeTaxManager.CacheAvailableTaxTypes();
		}
		catch (Exception ex)
		{
			ZNodeLogging.LogMessage("Could not cache tax types. Additional information: " + ex.Message);
			throw new ApplicationException("Could not cache tax types. Additional information: " + ex.Message);
		}
	}
	
	private void TryMapSearchProvider()
	{
		try
		{
			if (ObjectFactory.Container == null)
			{
				ObjectFactory.Initialize(f => f.For<IZnodeSearchProvider>().Use(x => new LuceneSearchProvider()));	
			}
			else
			{
				ObjectFactory.Configure(f => f.For<IZnodeSearchProvider>().Use(x => new LuceneSearchProvider()));
			}
		}
		catch (Exception ex)
		{
			ZNodeLogging.LogMessage("Could not map search provider. Additional information: " + ex.Message);
			throw new ApplicationException("Could not map search provider. Additional information: " + ex.Message);
		}
	}
	
	private void TryCacheCurrencySettings()
	{
		try
		{
			ZNodeCurrencyManager.CacheCurrencySetting();
		}
		catch (Exception ex)
		{
			ZNodeLogging.LogMessage("Could not cache currency settings. Additional information: " + ex.Message);
			throw new ApplicationException("Could not cache currency settings. Additional information: " + ex.Message);
		}
	}
	
	private void TryLogApplicationStart()
	{
		try
		{
			ZNodeLogging.LogActivity(2000);
			ZNodeLogging.LogMessage("Application Start");
		}
		catch (Exception ex)
		{
			ZNodeLogging.LogMessage("Could not log the application start. Additional information: " + ex.Message);
			throw new ApplicationException("Could not log the application start. Additional information: " + ex.Message);
		}
	}

	private void TryInitializeTracking()
	{
		try
		{
			var tracker = new ZNodeTracking();
			tracker.LogTrackingEvent("Entering Site");
		}
		catch (Exception ex)
		{
			ZNodeLogging.LogMessage("Affiliate tracking could not be initialized. Additional information: " + ex.Message);
			throw new ApplicationException("Affiliate tracking could not be initialized. Additional information: " + ex.Message);
		}
	}
</script>
