﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the AddToCart Page class.
    /// </summary>
    public partial class AddToCart : CommonPageBase
    {
        #region Protected Member Variables
        private string sku = string.Empty;
        private string productAddOns = string.Empty;
        private int productID = 0;
        private int _quantity = 1;
        private ZNodeProduct _product = null;
        private string shoppingCartDescription = string.Empty;
        private string continueShopping = string.Empty;
        #endregion

        #region General Events

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected override void Page_Load(object sender, EventArgs e)
        {
            // Get product sku from querystring 
            if (Request.Params["zpid"] != null)
            {
                this.productID = int.Parse(Request.Params["zpid"]);
            }

            // Get product sku from querystring 
            if (Request.Params["sku"] != null)
            {
                this.sku = Request.Params["sku"];
            }

            // Get product num # from querystring 
            if (Request.QueryString["Product_AddOns"] != null)
            {
                this.productAddOns = Request.QueryString["Product_AddOns"];
            }

            // Get the setting for Continue Shopping button.
            // If request comes from mobile then hide the Continue Shopping button from Shooping Cart.
            if (Request.QueryString["cs"] != null)
            {
                this.continueShopping = Request.QueryString["cs"];
                Session["cs"] = this.continueShopping;
            }

            // Get quantity from querystring 
            if (Request.Params["quantity"] != null)
            {
                if (!int.TryParse(Request.Params["quantity"], out this._quantity))
                {
                    this._quantity = 1;
                }
            }

            if (this.productID == 0)
            {
                Response.Redirect("~/product.aspx?zpid=" + this.productID);
                return;
            }

            if (!Page.IsPostBack)
            {
                this._product = ZNodeProduct.Create(this.productID, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

                ZNodeSKU productSKU = new ZNodeSKU();

                if (this._product.ZNodeAttributeTypeCollection.Count > 0)
                {
                    productSKU = ZNodeSKU.CreateBySKU(this.sku);

                    // Set product SKU
                    this._product.SelectedSKU = productSKU;

                    // Set Attributes Description to Description property
                    productSKU.AttributesDescription += "<br />";
                }

                if (this._product.ZNodeAddOnCollection.Count > 0)
                {
                    ZNodeAddOnList selectedAddOn = new ZNodeAddOnList();

                    // Get a sku based on Add-ons selected
                    selectedAddOn = ZNodeAddOnList.CreateByProductAndAddOns(this._product.ProductID, this.productAddOns);
                    selectedAddOn.SelectedAddOnValueIds = this.productAddOns;

                    // Set Selected Add-on 
                    this._product.SelectedAddOnItems = selectedAddOn;
                }

                if (this._product.CallForPricing)
                {
                    Response.Redirect(this._product.ViewProductLink);
                }

                // Create shopping cart item
                ZNodeShoppingCartItem item = new ZNodeShoppingCartItem();
                item.Product = new ZNodeProductBase(this._product);
                item.Quantity = this._quantity;

                // Add product to cart
                ZNodeShoppingCart shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

                // If shopping cart is null, create in session
                if (shoppingCart == null)
                {
                    shoppingCart = new ZNodeShoppingCart();
                    shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                }
               
                // Add item to cart
                if (shoppingCart.AddToCart(item))
                {
                    string link = "~/shoppingcart.aspx";
                    Response.Redirect(link);
                }
                else
                {
                   Response.Redirect(this._product.ViewProductLink);
                }
            }
        }
        #endregion
    }
}
