﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Default Page class.
    /// </summary>
    public partial class Default : CommonPageBase
    {
        #region Member Variables
        private bool isApplyStyle = true;
        private ContentPage contentPage;
        #endregion

        #region Public Methods
        /// <summary>
        /// Set UI culture value in cookie.
        /// </summary>
        /// <param name="localeID">localeID selected</param>
        public void SetUICulture(int localeID)
        {
            ZNode.Libraries.DataAccess.Service.LocaleService localeService = new ZNode.Libraries.DataAccess.Service.LocaleService();
            ZNode.Libraries.DataAccess.Entities.Locale local = localeService.GetByLocaleID(localeID);
            string cultureValue = local.LocaleCode;

            // Remove the existing Cookie
            HttpContext.Current.Response.Cookies.Remove("CultureLanguage");

            // Set Culture in Cookie
            HttpCookie cookie = new HttpCookie("CultureLanguage");
            cookie.Value = cultureValue;
            cookie.Expires = DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]));
            HttpContext.Current.Response.Cookies.Add(cookie);

            if (HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session["NewCulture"] = cultureValue;

                // Set Culture in Session
                HttpContext.Current.Session["CurrentUICulture"] = cultureValue;

                // Set NULL to last viewed category when locale change.
                HttpContext.Current.Session["BreadCrumzcid"] = null;

                // Reset the Session and Cache Value
                System.Web.HttpContext.Current.Session.Remove("CatalogConfig");

                // Set Culture in Thread
                System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo(cultureValue);
            }

            // Set locale id in Cookie
            HttpCookie cookieLocaleId = new HttpCookie("LocaleID");
            cookieLocaleId.Value = localeID.ToString();
            cookieLocaleId.Expires = DateTime.Now.AddDays(Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["CookieExpiresValue"]));
            HttpContext.Current.Response.Cookies.Add(cookieLocaleId);

            if (HttpContext.Current.Request.QueryString["redir"] != null)
            {
                HttpContext.Current.Response.Redirect(HttpContext.Current.Request.QueryString["redir"].ToString());
            }
            else
            {
                HttpContext.Current.Response.Redirect("~/default.aspx", true);
            }
        }
        #endregion

        protected override void Page_PreInit(object sender, EventArgs e)
        {
            if (ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig == null)
            {
                throw new ApplicationException("Could not retrieve site settings from the database. Please check your database connection by browsing to Diagnostics.aspx");
            }

            // Set new locale.
            if (Request.QueryString["nlid"] != null)
            {
                this.SetUICulture(Convert.ToInt32(Request.QueryString["nlid"]));
            }

            // Clear the current category session theme if some other category selected.
            Session["CurrentCategoryTheme"] = null;

            // Default home.master page file path
            string masterPageFilePath = "~/themes/" + ZNodeCatalogManager.Theme + "/MasterPages/home.master";

            // Get Home page data
            this.contentPage = ZNodeContentManager.GetPageByName("home", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

            if (this.contentPage != null)
            {
                // Set Template
                if (this.contentPage.MasterPageIDSource != null && this.contentPage.ThemeIDSource != null)
                {
                    string masterFilePath = ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + this.contentPage.ThemeIDSource.Name + "/MasterPages/" + this.contentPage.MasterPageIDSource.Name + ".master";

                    if (System.IO.File.Exists(Server.MapPath(masterFilePath)))
                    {
                        // If template master page exists, then override default master page
                        masterPageFilePath = masterFilePath;
                    }
                }

                // Set CSS
                if (this.contentPage.CSSID != null && this.contentPage.ThemeID != null)
                {
                    this.isApplyStyle = false;
                }

                // Set master page
                this.MasterPageFile = masterPageFilePath;

                // Add to context for control access
                HttpContext.Current.Items.Add("PageTitle", this.contentPage.Title);

                // SEO stuff
                ZNodeSEO seo = new ZNodeSEO();
                seo.SEODescription = this.contentPage.SEOMetaDescription;
                seo.SEOKeywords = this.contentPage.SEOMetaKeywords;
                seo.SEOTitle = this.contentPage.SEOTitle;
            }

            // Get the user account from session
            ZNodeUserAccount _userAccount = (ZNodeUserAccount)ZNodeUserAccount.CurrentAccount();
        }

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected override void Page_Load(object sender, EventArgs e)
        {
            if (this.isApplyStyle)
            {
                // HTML 
                HtmlGenericControl include = new HtmlGenericControl("link");
                include.Attributes.Add("type", "text/css");
                include.Attributes.Add("rel", "stylesheet");
                include.Attributes.Add("href", ZNodeCatalogManager.GetCssPathByLocale(ZNodeCatalogManager.Theme, ZNodeCatalogManager.CSS));

                // Add a reference for StyleSheet to the head section
                if (this.Page.Header != null)
                {
                    this.Page.Header.Controls.Add(include);
                }
            }
            else
            {
                // HTML 
                HtmlGenericControl include = new HtmlGenericControl("link");
                include.Attributes.Add("type", "text/css");
                include.Attributes.Add("rel", "stylesheet");
                include.Attributes.Add("href", ZNodeCatalogManager.GetCssPathByLocale(this.contentPage.ThemeIDSource.Name, this.contentPage.CSSIDSource.Name));

                // Add a reference for StyleSheet to the head section
                if (this.Page.Header != null)
                {
                    this.Page.Header.Controls.Add(include);
                }
            }
        }
    }
}
