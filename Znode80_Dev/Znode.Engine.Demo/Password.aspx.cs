﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Password Page class.
    /// </summary>
    public partial class Password : CommonPageBase
    {
        protected override void Page_PreInit(object sender, EventArgs e)
        {
            // Clear the current category session theme if some other category selected.
            Session["CurrentCategoryTheme"] = null;

            this.MasterPageFile = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/password.master";

            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {
                    string inURL = Request.Url.ToString();
                    string outURL = inURL.ToLower().Replace("http://", "https://");

                    Response.Redirect(outURL);
                }
            }
        }
    }
}
