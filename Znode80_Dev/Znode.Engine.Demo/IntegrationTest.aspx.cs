using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Shipping.FedEx;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.Framework.Business;

/// <summary>
/// This page is a test harness to perform integration testing on your multifront Feel free
/// to add your own test methods here.  
/// You should disable this page in the production environment by updating the "EnableIntegrationTest"
/// configuration settings in the WEB.CONFIG file
/// </summary>
namespace Znode.Engine.Demo
{
    /// <summary>
    /// Represents the Integration Test Page class.
    /// </summary>
    public partial class IntegrationTest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // Check Integration test harness is enabled on the config
            if (System.Configuration.ConfigurationManager.AppSettings["EnableIntegrationTest"].ToString() == "0")
            {
                throw new ApplicationException("This page has been disabled by the site administrator");
            }
            else
            {
                // Call methods that you need to test in this section...
                lblMsg.Text = "Starting Tests..." + "<br>";
            }
        }
       
        #region Sample Method to test FedEx service
        /// <summary>
        /// Test the fedex service
        /// </summary>
        /// <returns>Returns the FedEx Rate</returns>
        protected decimal GetFedexRate()
        {
            var fdx = new FedExAgent();
            fdx.ClientProductId = "KHHG";
            fdx.ClientProductVersion = "3203";
            fdx.CspAccessKey = "vuB2Ui3CCpHkCs53";
            fdx.CspPassword = "TAt234utxOFiJuJM2Yw0oL7sD";
            fdx.CurrencyCode = "USD";
            fdx.DimensionUnit = "IN";
            fdx.DropOffType = "BUSINESS_SERVICE_CENTER";
            fdx.FedExAccessKey = "14J85cqDbYBbt9Yd";
            fdx.FedExAccountNumber = "510087100";
            fdx.FedExMeterNumber = "1195903";
            fdx.FedExSecurityCode = "1xgqmADPRr6Q4UfWH5SUWTx6Y";
            fdx.FedExServiceType = "PRIORITY_OVERNIGHT";
            fdx.PackageHeight = "15";
            fdx.PackageLength = "15";
            fdx.PackageWidth = "15";
            fdx.PackageTypeCode = "YOUR_PACKAGING";
            fdx.PackageWeight = 30;
            fdx.ShipperCountryCode = "US";
            fdx.ShipperStateCode = "OH";
            fdx.ShipperZipCode = "43016";
            fdx.ShipToAddressIsResidential = true;
            fdx.ShipToCountryCode = "US";
            fdx.ShipToStateCode = "OH";
            fdx.ShipToZipCode = "43229";
            fdx.UseDiscountRate = false;
            fdx.WeightUnit = "LB";
            decimal rate = fdx.GetShippingRate();
            return rate;
        }
        #endregion

        #region Sample Authorize.net method

        private void CCTransaction()
        {
            // Set gateway info
            GatewayInfo gi = new GatewayInfo();
            gi.TestMode = false;
            gi.Gateway = GatewayType.AUTHORIZE;

            // Set credit card
            CreditCard cc = new CreditCard();
            cc.Amount = 1.0M;
            cc.AuthCode = string.Empty;
            cc.CardNumber = string.Empty;
            cc.CardSecurityCode = string.Empty;
            cc.CreditCardExp = string.Empty;
            cc.OrderID = 5678;
            cc.TransactionID = string.Empty;

            // Address
            Address ba = new Address();
            ba.FirstName = "John";
            ba.LastName = "Doe";
            ba.PostalCode = "43016";
            ba.StateCode = "OH";
            ba.CountryCode = "US";

            GatewayAuthorize auth = new GatewayAuthorize();

            GatewayResponse resp = auth.SubmitPayment(gi, ba, ba, cc);
            Response.Write("Response Code: " + resp.ResponseCode + " Response text: " + resp.ResponseText + " Transaction ID: " + resp.TransactionId);
        }

        private void VoidTransaction()
        {
            // Set gateway info
            GatewayInfo gi = new GatewayInfo();
            gi.GatewayLoginID = string.Empty;
            gi.TransactionKey = string.Empty;
            gi.TestMode = false;
            gi.Gateway = GatewayType.AUTHORIZE;

            // Set credit card
            CreditCard cc = new CreditCard();
            cc.Amount = 1.0M;
            cc.AuthCode = string.Empty;
            cc.CardNumber = string.Empty;
            cc.CardSecurityCode = string.Empty;
            cc.CreditCardExp = string.Empty;
            cc.OrderID = 5678;
            cc.TransactionID = string.Empty;

            GatewayAuthorize auth = new GatewayAuthorize();

            GatewayResponse resp = auth.RefundPayment(gi, cc);
            Response.Write(resp.ResponseCode + " " + resp.ResponseText + " " + resp.TransactionId);
        }

        #endregion
    }
}