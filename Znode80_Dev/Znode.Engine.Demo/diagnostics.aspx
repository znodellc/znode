<%@ Page Language="C#"  AutoEventWireup="True" Inherits="Znode.Engine.Demo.DiagnosticsPageBase" Codebehind="DiagnosticsPageBase.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title>Znode Diagnostics</title>
</head>
<style type="text/css">
    .Error {font-weight: bold; color: #ff0000; margin-bottom: 5px; margin-top: 5px; }
    .Success {font-weight: bold; color: #0000ff; margin-bottom: 5px; margin-top: 5px; }
</style>
<body leftmargin="15px" rightmargin="15px" topmargin="15px">
    <form id="form1" runat="server">
        <div align="left" style=" font-family:Arial; text-align:left;" id="Container">
            <h1>Znode Diagnostics <%= GetProductVersion()%></h1>
            <div>
                Here are the results of diagnostic tests that were run on your installation
            </div>  
            <hr />     
             <asp:Label ID="BaseVersionLabel" runat="server"></asp:Label>
             <asp:DataList ID="VersionDetails" runat="server" Visible="true">
                  <HeaderTemplate>
                  ZNode Patches:
                  </HeaderTemplate>
                  <ItemTemplate>
                   <%# DataBinder.Eval(Container.DataItem, "Data1") %> Install Date:<%# DataBinder.Eval(Container.DataItem, "CreateDte") %>
                </ItemTemplate> 
            </asp:DataList>
             <p><b>Database Status</b> - <asp:Label ID="lblDatabaseStatus" runat="server" ></asp:Label></p>
             <p><b>Folder Permission</b> - <asp:Label ID="lblPublicPermissions" runat="server" ></asp:Label></p>             
             <p><b>License Status</b> - <asp:Label ID="lblLicenseStatus" runat="server"></asp:Label></p>
             <p><b>SMTP Status</b> - <asp:Label ID="lblSMTPAccountStatus" runat="server"></asp:Label></p>
             <hr />
             <asp:Panel ID="PnlExceptionSummary" runat="server" Visible= "false">
             <h4>Exception Log</h4>
             <div><asp:Literal EnableViewState="false" ID="lblMsg" runat="server" ></asp:Literal></div>
             </asp:Panel>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="CaseNumber" ErrorMessage="Case Number must be specified" CssClass="Error"></asp:RequiredFieldValidator>
            <asp:Label ID="lblCase" runat="server" Text="ZNode Support Case Number:" Visible ="false" /><asp:TextBox ID="CaseNumber" runat="server" MaxLength="6" size="6" Visible="false"></asp:TextBox>
            <asp:Button ID="btnEmailDiagnostics" runat="server" Text="Email Diagnostics to ZNode Support" OnClick="Button_Click"  Visible="false"/>
             <hr />
        </div>
    </form>
</body>
</html>


  
