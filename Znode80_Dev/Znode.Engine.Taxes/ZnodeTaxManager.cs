using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;
using StructureMap;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using Address = ZNode.Libraries.DataAccess.Entities.Address;

namespace Znode.Engine.Taxes
{
	/// <summary>
	/// Helps manage taxes.
	/// </summary>
	public class ZnodeTaxManager : ZNodeBusinessBase
	{
		private ZNodeShoppingCart _shoppingCart;
		private ZNodeGenericCollection<IZnodeTaxType> _taxes;

		/// <summary>
		/// Throws a NotImplementedException because this class requires a shopping cart to work.
		/// </summary>
		public ZnodeTaxManager()
		{
			throw new NotImplementedException();
		}

		public ZnodeTaxManager(ZNodeShoppingCart shoppingCart)
		{
			_shoppingCart = shoppingCart;
			_taxes = new ZNodeGenericCollection<IZnodeTaxType>();

			if (TaxExemptProfile)
			{
				return;
			}

			var taxRules = TaxRules;

			if (taxRules.Count > 0)
			{
				// Apply sorting based on precedence 
				taxRules.Sort = "Precedence";

				// Loop through tax rules and apply to cart based on precedence
				foreach (DataRowView rule in taxRules)
				{
					var taxBag = BuildTaxBag(rule);
					AddTaxTypes(rule, taxBag);
				}
			}
		}

		public DataView TaxRules
		{
			get
			{
				string countryCode = null;
				//Added for SiteAdmin CreateOrder Without Customer Selected Mingle #17163
				if (_shoppingCart.Payment.ShippingAddress == null)
					_shoppingCart.Payment.ShippingAddress = new Address();

				if (_shoppingCart != null && _shoppingCart.Payment != null)
				{
					countryCode = _shoppingCart.Payment.ShippingAddress.CountryCode ?? String.Empty;
				}
				
				//var countryCode = _shoppingCart != null && _shoppingCart.Payment != null ? _shoppingCart.Payment.ShippingAddress.CountryCode : String.Empty;

				var taxHelper = new TaxHelper();
				var dt = taxHelper.GetActiveTaxRulesByPortalId(0, countryCode);

				var dv = new DataView(dt);

				if (ZNodeConfigManager.SiteConfig.InclusiveTax)
				{
					dv.RowFilter = "InclusiveInd = false";
				}

				return dv;
			}
		}

		public bool TaxExemptProfile
		{
			get
			{
				if (HttpContext.Current.Session["ProfileCache"] != null)
				{
					var customerProfile = (Profile)HttpContext.Current.Session["ProfileCache"];
					if (customerProfile != null)
					{
						return customerProfile.TaxExempt;
					}
				}

				return false;
			}
		}

		/// <summary>
		/// Calculates the sales tax for the shopping cart and its items.
		/// </summary>
		/// <param name="shoppingCart">The current shopping cart.</param>
		public void Calculate(ZNodeShoppingCart shoppingCart)
		{
			// Reset values            
			shoppingCart.OrderLevelTaxes = 0;
			shoppingCart.GST = 0;
			shoppingCart.HST = 0;
			shoppingCart.PST = 0;
			shoppingCart.VAT = 0;
			shoppingCart.SalesTax = 0;
			shoppingCart.TaxRate = 0;

			// Go through each item in the cart
			foreach (ZNodeShoppingCartItem cartItem in shoppingCart.ShoppingCartItems)
			{
				cartItem.IsTaxCalculated = false;
				cartItem.Product.GST = 0;
				cartItem.Product.HST = 0;
				cartItem.Product.PST = 0;
				cartItem.Product.VAT = 0;
				cartItem.Product.SalesTax = 0;

				// Check for addons
				foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
				{
					foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
					{
						addOnValue.IsTaxCalculated = false;
						addOnValue.GST = 0;
						addOnValue.HST = 0;
						addOnValue.PST = 0;
						addOnValue.VAT = 0;
						addOnValue.SalesTax = 0;
					}
				}
			}

			foreach (ZnodeTaxType tax in _taxes)
			{
				tax.Calculate();
			}
		}

		/// <summary>
		/// Process anything that must be done before the order is submitted.
		/// </summary>
		/// <param name="shoppingCart">The current shopping cart.</param>
		/// <returns>True if everything is good for submitting the order; otherwise, false.</returns>
		public bool PreSubmitOrderProcess(ZNodeShoppingCart shoppingCart)
		{
			var allPreConditionsOk = true;

			foreach (ZnodeTaxType tax in _taxes)
			{
				// Make sure all pre-conditions are good before letting the customer check out
				allPreConditionsOk &= tax.PreSubmitOrderProcess();
			}

			return allPreConditionsOk;
		}

		/// <summary>
		/// Process anything that must be done after the order is submitted.
		/// </summary>
		/// <param name="shoppingCart">The current shopping cart.</param>
		public void PostSubmitOrderProcess(ZNodeShoppingCart shoppingCart)
		{
			foreach (ZnodeTaxType tax in _taxes)
			{
				tax.PostSubmitOrderProcess();
			}
		}

		/// <summary>
		/// Caches all available tax types in the application cache.
		/// </summary>
		public static void CacheAvailableTaxTypes()
		{
			if (HttpRuntime.Cache["TaxTypesCache"] == null)
			{
				if (ObjectFactory.Container == null)
				{
					ObjectFactory.Initialize(scanner => scanner.Scan(x =>
					{
						x.AssembliesFromApplicationBaseDirectory();
						x.AddAllTypesOf<IZnodeTaxType>();
					}));
				}
				else
				{
					ObjectFactory.Configure(scanner => scanner.Scan(x =>
					{
						x.AssembliesFromApplicationBaseDirectory();
						x.AddAllTypesOf<IZnodeTaxType>();
					}));
				}

				// Only cache tax types that have a ClassName and Name; this helps avoid showing base classes in some of the dropdown lists
				var taxTypes = ObjectFactory.GetAllInstances<IZnodeTaxType>().Where(x => !String.IsNullOrEmpty(x.ClassName) && !String.IsNullOrEmpty(x.Name));
				HttpRuntime.Cache["TaxTypesCache"] = taxTypes.ToList();
			}
		}

		/// <summary>
		/// Gets all available tax types from the application cache.
		/// </summary>
		/// <returns>A list of the available tax types.</returns>
		public static List<IZnodeTaxType> GetAvailableTaxTypes()
		{
			var list = HttpRuntime.Cache["TaxTypesCache"] as List<IZnodeTaxType>;
			if (list != null)
			{
				list.Sort((taxA, taxB) => String.CompareOrdinal(taxA.Name, taxB.Name));
			}
			else
			{
				list = new List<IZnodeTaxType>();
			}

			return list;
		}

		private ZnodeTaxBag BuildTaxBag(DataRowView rule)
		{
			var taxBag = new ZnodeTaxBag();
			taxBag.DestinationCountryCode = String.IsNullOrEmpty(rule["DestinationCountryCode"].ToString()) ? null : rule["DestinationCountryCode"].ToString();
			taxBag.DestinationStateCode = String.IsNullOrEmpty(rule["DestinationStateCode"].ToString()) ? null : rule["DestinationStateCode"].ToString();
			taxBag.CountyFIPS = String.IsNullOrEmpty(rule["CountyFIPS"].ToString()) ? null : rule["CountyFIPS"].ToString();
			taxBag.SalesTax = decimal.Parse(rule["SalesTax"].ToString());
			taxBag.ShippingTaxInd = bool.Parse(rule["TaxShipping"].ToString());
			taxBag.VAT = decimal.Parse(rule["VAT"].ToString());
			taxBag.TaxClassId = int.Parse(rule["TaxClassID"].ToString());
			taxBag.GST = decimal.Parse(rule["GST"].ToString());
			taxBag.PST = decimal.Parse(rule["PST"].ToString());
			taxBag.HST = decimal.Parse(rule["HST"].ToString());
			taxBag.Custom1 = Convert.ToString(rule["Custom1"]);
			taxBag.InclusiveInd = bool.Parse(rule["InclusiveInd"].ToString());

			return taxBag;
		}

		private void AddTaxTypes(DataRowView rule, ZnodeTaxBag taxBag)
		{
			var availableTaxTypes = GetAvailableTaxTypes();
			foreach (var t in availableTaxTypes.Where(t => t.ClassName == rule["ClassName"].ToString()))
			{
				try
				{
					var taxTypeAssembly = Assembly.GetAssembly(t.GetType());
					var taxType = (IZnodeTaxType)taxTypeAssembly.CreateInstance(t.ToString());

					if (taxType != null)
					{
						taxType.Bind(_shoppingCart, taxBag);
						_taxes.Add(taxType);
					}
				}
				catch (Exception ex)
				{
					ZNodeLogging.LogMessage("Error while instantiating tax type: " + t);
					ZNodeLogging.LogMessage(ex.ToString());
				}
			}
		}
	}
}

