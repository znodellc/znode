﻿using System.Collections.Generic;
using System.Linq;
using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Taxes
{
	public static class ZnodeTaxTypeExtensions
	{
		/// <summary>
		/// Extension method that converts an IZnodeTaxType list to an IZnodeProviderType list.
		/// </summary>
		/// <param name="list">The list of IZnodeTaxType items.</param>
		/// <returns>A list of IZnodeProviderType items.</returns>
		public static List<IZnodeProviderType> ToProviderTypeList(this List<IZnodeTaxType> list)
		{
			return list.Cast<IZnodeProviderType>().ToList();
		}
	}
}