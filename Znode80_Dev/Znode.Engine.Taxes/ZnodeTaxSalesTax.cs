using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Taxes
{
	public class ZnodeTaxSalesTax : ZnodeTaxType
    {
		public ZnodeTaxSalesTax()
		{
			Name = "Sales Tax";
			Description = "Applies sales tax to the shopping cart.";

			Controls.Add(ZnodeTaxRuleControl.SalesTax);
			Controls.Add(ZnodeTaxRuleControl.VAT);
			Controls.Add(ZnodeTaxRuleControl.GST);
			Controls.Add(ZnodeTaxRuleControl.PST);
			Controls.Add(ZnodeTaxRuleControl.HST);
			Controls.Add(ZnodeTaxRuleControl.Precedence);
			Controls.Add(ZnodeTaxRuleControl.Inclusive);
		}

        /// <summary>
        /// Calculates the sales tax and updates the shopping cart.
        /// </summary>
        public override void Calculate()
        {
            if (IsValid())
            {
				// Go through each item in the cart
                foreach (ZNodeShoppingCartItem cartItem in ShoppingCart.ShoppingCartItems)
                {
					decimal gst;
					decimal hst;
					decimal pst;
					decimal vat;
					decimal salesTax;

                    var basePrice = cartItem.PromotionalPrice;                   
                    var extendedPrice = (cartItem.Product.DiscountAmount > basePrice ? 0 : basePrice - cartItem.Product.DiscountAmount) * cartItem.Quantity;

                    if (cartItem.ExtendedPriceDiscount > extendedPrice)
                    {
                        extendedPrice = 0;
                    }
                    else
                    {
                        extendedPrice -= cartItem.ExtendedPriceDiscount;
                    }

                    if (TaxBag.ShippingTaxInd)
                    {
                        extendedPrice += cartItem.ShippingCost;
                    }

					if (cartItem.Product.TaxClassID == TaxBag.TaxClassId && cartItem.IsTaxCalculated == false)
                    {
						gst = extendedPrice * (TaxBag.GST / 100);
						hst = extendedPrice * (TaxBag.HST / 100);
						pst = extendedPrice * (TaxBag.PST / 100);
						vat = extendedPrice * (TaxBag.VAT / 100);
						salesTax = extendedPrice * (TaxBag.SalesTax / 100);

                        cartItem.Product.GST += gst;
                        cartItem.Product.HST += hst;
                        cartItem.Product.PST += pst;
                        cartItem.Product.VAT += vat;
                        cartItem.Product.SalesTax += salesTax;                        

                        ShoppingCart.GST += gst;
						ShoppingCart.HST += hst;
						ShoppingCart.PST += pst;
                        ShoppingCart.VAT += vat;
                        ShoppingCart.SalesTax += salesTax;

                        cartItem.IsTaxCalculated = true;
                    }

                    // Go through the addons
                    foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
                    {
                        foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                        {
							if (addOnValue.TaxClassID == TaxBag.TaxClassId && addOnValue.IsTaxCalculated == false)
                            {
                                extendedPrice = (addOnValue.DiscountAmount > addOnValue.FinalPrice ? 0 : addOnValue.FinalPrice - addOnValue.DiscountAmount) * cartItem.Quantity;

								if (TaxBag.ShippingTaxInd)
                                {
                                    extendedPrice += addOnValue.ShippingCost;
                                }

								gst = extendedPrice * (TaxBag.GST / 100);
								hst = extendedPrice * (TaxBag.HST / 100);
								pst = extendedPrice * (TaxBag.PST / 100);
								vat = extendedPrice * (TaxBag.VAT / 100);
								salesTax = extendedPrice * (TaxBag.SalesTax / 100);

                                addOnValue.GST += gst;
                                addOnValue.HST += hst;
                                addOnValue.PST += pst;
                                addOnValue.VAT += vat;
                                addOnValue.SalesTax += salesTax;
                                
                                ShoppingCart.GST += gst;
								ShoppingCart.HST += hst;
								ShoppingCart.PST += pst;
                                ShoppingCart.VAT += vat;
                                ShoppingCart.SalesTax += salesTax;

                                addOnValue.IsTaxCalculated = true;
                            }
                        }
                    }
                }               
            }            
        }

		/// <summary>
		/// Process anything that must be done before the order is submitted.
		/// </summary>
		/// <returns>True if everything is good for submitting the order; otherwise, false.</returns>
		public override bool PreSubmitOrderProcess()
		{
			var destinationAddress = ShoppingCart.Payment.BillingAddress;
			var destinationCountry = destinationAddress.CountryCode;
			var destinationState = destinationAddress.StateCode;

			if (string.IsNullOrEmpty(destinationCountry) || string.IsNullOrEmpty(destinationState))
            {
				// Set tax rate and error message
				ShoppingCart.TaxRate = 0;
				ShoppingCart.AddErrorMessage = "Tax Error: Invalid destination country or state code.";
				return false;
			}

			// Otherwise
			return true;
		}

		/// <summary>
		/// Process anything that must be done after the order is submitted.
		/// </summary>
		public override void PostSubmitOrderProcess()
		{
			// Nothing to see here, move along, move along
		}
    }
}
