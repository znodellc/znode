﻿using System;
using System.Collections.ObjectModel;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Taxes
{
	/// <summary>
	/// This is the base class for all tax types.
	/// </summary>
	public class ZnodeTaxType : IZnodeTaxType
	{
		private string _className;
		private Collection<ZnodeTaxRuleControl> _controls;

		public string ClassName
		{
			get
			{
				if (String.IsNullOrEmpty(_className))
				{
					_className = GetType().Name;
				}

				return _className;
			}

			set { _className = value; }
		}

		public string Name { get; set; }
		public string Description { get; set; }
		public int Precedence { get; set; }
		public ZNodeShoppingCart ShoppingCart { get; set; }
		public ZnodeTaxBag TaxBag { get; set; }

		public Collection<ZnodeTaxRuleControl> Controls
		{
			get { return _controls ?? (_controls = new Collection<ZnodeTaxRuleControl>()); }
		}

		/// <summary>
		/// Binds the shopping cart and tax data to the tax rule.
		/// </summary>
		/// <param name="shoppingCart">The current shopping cart.</param>
		/// <param name="taxBag">The tax properties.</param>
		public virtual void Bind(ZNodeShoppingCart shoppingCart, ZnodeTaxBag taxBag)
		{
			ShoppingCart = shoppingCart;
			TaxBag = taxBag;
		}

		/// <summary>
		/// Calculates the tax and updates the shopping cart.
		/// </summary>
		public virtual void Calculate()
		{
		}

		/// <summary>
		/// Process anything that must be done before the order is submitted.
		/// </summary>
		/// <returns>True if everything is good for submitting the order; otherwise, false.</returns>
		public virtual bool PreSubmitOrderProcess()
		{
			// Most taxes don't need any special verification
			return true;
		}

		/// <summary>
		/// Process anything that must be done after the order is submitted.
		/// </summary>
		public virtual void PostSubmitOrderProcess()
		{
			// Most taxes don't need any further processing after the order is submitted
		}

		/// <summary>
		/// Indicates whether or not the tax rule is valid and should be applied to the shopping cart.
		/// </summary>
		/// <returns>True if tax rule is valid; otherwise, false.</returns>
		public virtual bool IsValid()
		{
			var isValid = false;
			var destinationAddress = ShoppingCart.Payment.ShippingAddress;

			// Check if this applies to all countries
			if (TaxBag.DestinationCountryCode == null || destinationAddress.CountryCode == TaxBag.DestinationCountryCode)
			{
				// Check if this applies to all states
				if (TaxBag.DestinationStateCode == null)
				{
					isValid = true;
				}
				else if (TaxBag.DestinationStateCode == destinationAddress.StateCode)
				{
					// Check if this applies to specific state
					if (TaxBag.CountyFIPS == null)
					{
						isValid = true;
					}
					else
					{
						var filters = new ZipCodeQuery();
						filters.AppendEquals(ZipCodeColumn.CountyFIPS, TaxBag.CountyFIPS);
						filters.AppendEquals(ZipCodeColumn.CityName, destinationAddress.City);
						filters.AppendEquals(ZipCodeColumn.StateAbbr, destinationAddress.StateCode);
						filters.AppendEquals(ZipCodeColumn.ZIP, destinationAddress.PostalCode);

						var zipService = new ZipCodeService();
						var zipCodeList = zipService.Find(filters.GetParameters());

						if (zipCodeList.Count != 0)
						{
							isValid = true;
						}
					}
				}
			}

			return isValid;
		}
	}
}
