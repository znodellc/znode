using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

#region Enum RequestType
/// <summary>
/// Represents request type
/// </summary>
public enum RequestType
{
    /// <summary>
    /// Represents a ItemDownload.
    /// </summary>
    ItemDownload = 1,

    /// <summary>
    /// Represents a OrderDownload.
    /// </summary>
    OrderDownload = 2,

    /// <summary>
    /// Represents a AdjustItemInventory.
    /// </summary>
    AdjustItemInventory = 3,

    /// <summary>
    /// Represents a AccountDownload.
    /// </summary>
    AccountDownload = 4,
}
#endregion

/// <summary>
/// Represents the QuickBooks class.
/// </summary>
public class QuickBooks : Znode.Engine.Common.CommonPageBase
{
    #region Protected member Variables
    private static ArrayList requestList = new ArrayList();
    private static ArrayList ItemRequestList = new ArrayList();
    private static ArrayList ItemInventoryDownloadRequestList = new ArrayList();
    private static ArrayList accountDownloadRequestList = new ArrayList();
    #endregion

    #region Constructor
    /// <summary>
    /// Initializes a new instance of the QuickBooks class.
    /// </summary>
    public QuickBooks()
    {
    }
    #endregion

    #region Protected Properties
    /// <summary>
    /// Gets or sets the message configuration
    /// </summary>
    protected ZNodeMessageConfig MessageConfig
    {
        get
        {
            if (HttpContext.Current.Session["QuickbooksConfig"] != null)
            {
                return (ZNodeMessageConfig)HttpContext.Current.Session["QuickbooksConfig"];
            }
            else
            {
                string messageConfigPath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "QuickbooksConfig.xml");

                ZNodeMessageConfig messageConfig = new ZNodeMessageConfig(messageConfigPath);

                // Add to session
                HttpContext.Current.Session.Add("QuickbooksConfig", messageConfig);

                return messageConfig;
            }
        }
        
        set
        {
            HttpContext.Current.Session["QuickbooksConfig"] = value;
        }
    }

    #endregion

    #region Public  properties
    /// <summary>
    /// Removes all the request xml items from the list
    /// </summary>
    /// <param name="requestType"> Request Type instance</param>
    public void ClearRequestList(RequestType requestType)
    {
        switch (requestType)
        {
            case RequestType.OrderDownload:
                requestList.Clear();
                break;
            case RequestType.ItemDownload:
                ItemRequestList.Clear();
                break;
            case RequestType.AdjustItemInventory:
                ItemInventoryDownloadRequestList.Clear();
                break;
            case RequestType.AccountDownload:
                accountDownloadRequestList.Clear();
                break;
            default:
                requestList.Clear();
                ItemRequestList.Clear();
                ItemInventoryDownloadRequestList.Clear();
                accountDownloadRequestList.Clear();
                break;
        }
    }
    #endregion

    #region  Public - Request Methods

    /// <summary>
    /// This method parse the QB response xml.
    /// </summary>
    /// <param name="message">The value of message</param>
    /// <param name="count">The total count</param>
    /// <param name="reqType">Request Type instance</param>
    public void ReceiveResponse(string message, out int count, RequestType reqType)
    {
        int value = 0;

        if (Regex.IsMatch(message, "<SalesReceiptAddRs\\s*[^><]*>"))
        {
            this.UpdateQuickBooksStatus(message, "SalesReceiptAddRs");
        }
        else if (Regex.IsMatch(message, "<SalesOrderAddRs\\s*[^><]*>"))
        {
            this.UpdateQuickBooksStatus(message, "SalesOrderAddRs");
        }
        else if (Regex.IsMatch(message, "<CustomerAddRs\\s*[^><]*>"))
        {
            this.UpdateQuickBooksStatus(message, "CustomerAddRs");
        }
        else if (Regex.IsMatch(message, "<CustomerModRs\\s*[^><]*>"))
        {
            this.UpdateQuickBooksStatus(message, "CustomerModRs");
        }
        else if (Regex.IsMatch(message, "<CustomerQueryRs\\s*[^><]*>"))
        {
            value = this.DownloadCustomerInfo(message, "CustomerQueryRs", reqType);
        }
        else if (Regex.IsMatch(message, "<ItemInventoryAddRs\\s*[^><]*>"))
        {
            this.UpdateQuickBooksStatus(message, "ItemInventoryAddRs");
        }
        else if (Regex.IsMatch(message, "<ItemInventoryModRs\\s*[^><]*>"))
        {
            this.UpdateQuickBooksStatus(message, "ItemInventoryModRs");
        }
        else if (Regex.IsMatch(message, "<ItemInventoryQueryRs\\s*[^><]*>"))
        {
            value = this.DownloadProduct(message, "ItemInventoryQueryRs");
        }

        count = value;
    }

    /// <summary>
    /// Build modify request xml for this item using ListId and Edit Sequence
    /// </summary>
    /// <param name="message">Represents the message</param>
    /// <param name="rootTagName">The Root tag Name</param>
    /// <returns>Returns a integer</returns>
    public int DownloadProduct(string message, string rootTagName)
    {
        ZNodeMessageConfig messageConfig = this.MessageConfig;

        string editSequence = string.Empty;
        string listID = string.Empty;
        string statusCode = string.Empty;

        string requestID = this.ParseRequestID(message, rootTagName, out statusCode);
        if (requestID.Length == 0)
        {
            requestID = "0";
        }

        Match m = Regex.Match(message, "(?<=<EditSequence[^>]*>).*?(?=</EditSequence>)", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
        editSequence = m.Value;

        m = Regex.Match(message, "(?<=<ListID[^>]*>).*?(?=</ListID>)", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
        listID = m.Value;

        if (requestID.Contains("Product"))
        {
            requestID = requestID.Replace("Product", string.Empty);

            ProductService service = new ProductService();
            Product _product = service.GetByProductID(int.Parse(requestID));

            if (_product == null)
            {
                return 0;
            }

            if (statusCode == "0")
            {
                // Build modify request xml
                this.BuildProductModifyRequestXML(ItemRequestList, _product, listID, editSequence, messageConfig);

                SKUInventory skuInventory = this.GetDefaultSKU(_product.ProductID);

                if (skuInventory != null)
                {
                    this.BuildAdjustItemInventoryRequestXML(ItemRequestList, skuInventory.SKU, skuInventory.QuantityOnHand.GetValueOrDefault(0), messageConfig);
                }
                else
                {
                    this.BuildAdjustItemInventoryRequestXML(ItemRequestList, string.Empty, 0, messageConfig);
                }
            }
        }
        else if (requestID.Contains("Sku"))
        {
            requestID = requestID.Replace("Sku", string.Empty);
            SKUService service = new SKUService();
            SKU sku = service.DeepLoadBySKUID(
                              int.Parse(requestID),
                              true,
                              DeepLoadType.IncludeChildren,
                              typeof(Product));

            if (statusCode == "0")
            {
                // Build modify request xml
                this.BuildItemModifyRequestXML(ItemRequestList, sku, listID, editSequence, messageConfig);

                // Build item inventory adjustment request xml
                SKUInventoryService sis = new SKUInventoryService();
                SKUInventory skuInventory = sis.GetBySKU(sku.SKU);

                this.BuildAdjustItemInventoryRequestXML(ItemRequestList, sku.SKU, skuInventory.QuantityOnHand.Value, messageConfig);
            }
        }
        else if (requestID.Contains("AddOnValue"))
        {
            requestID = requestID.Replace("AddOnValue", string.Empty);
            AddOnValueService service = new AddOnValueService();
            AddOnValue _addOnValue = service.GetByAddOnValueID(int.Parse(requestID));

            if (statusCode == "0")
            {
                // Build modify request xml
                this.BuildItemModifyRequestXML(ItemRequestList, _addOnValue, listID, editSequence, messageConfig);

                // Build item inventory adjustment request xml
                this.BuildAdjustItemInventoryRequestXML(ItemRequestList, _addOnValue.SKU, ProductAdmin.GetQuantity(_addOnValue), messageConfig);
            }
        }

        return 1;
    }

    /// <summary>
    /// Returns the request xml list based on the type of the request
    /// </summary>
    /// <param name="requestType">Request Type instance</param>
    /// <returns>Returns the requested xml list</returns>
    public ArrayList GetRequestXmlList(RequestType requestType)
    {
        ZNodeMessageConfig messageConfig = this.MessageConfig;

        // Order download 
        if (requestType == RequestType.OrderDownload)
        {
            if (requestList.Count == 0)
            {
                requestList = new ArrayList();

                OrderService service = new OrderService();
                OrderQuery query = new OrderQuery();
                query.AppendIsNull(OrderColumn.WebServiceDownloadDate);
                TList<Order> orderList = service.Find(query.GetParameters());

                if (orderList != null)
                {
                    service.DeepLoad(orderList, true, DeepLoadType.IncludeChildren, typeof(Account), typeof(TList<OrderLineItem>), typeof(Shipping));
                    this.BuildOrderRequestXML(orderList, requestList, messageConfig);
                }
            }

            return requestList;
        }
        else if (requestType == RequestType.ItemDownload) 
        {
            // Item list download request type
            if (ItemRequestList.Count == 0)
            {
                ItemRequestList = new ArrayList();

                ZNode.Libraries.DataAccess.Custom.ProductHelper helperAccess = new ZNode.Libraries.DataAccess.Custom.ProductHelper();
                DataSet ItemList = helperAccess.GetItemInventoryList(ZNodeConfigManager.SiteConfig.PortalID);

                foreach (DataRow dr in ItemList.Tables[0].Rows)
                {
                    if (dr["WebServiceDownloadDte"].ToString().Length > 0)
                    {
                        string returnXML = this.BuildItemQueryRequest("Product" + dr["ProductID"].ToString(), "Contains", dr["SKU"].ToString());

                        // Add to request list
                        ItemRequestList.Add(returnXML);
                    }
                    else
                    {
                        this.BuildProductAddRequestXML(ItemRequestList, dr, messageConfig);
                    }
                }

                foreach (DataRow dr in ItemList.Tables[1].Rows)
                {
                    if (dr["WebServiceDownloadDte"].ToString().Length > 0)
                    {
                        string returnXML = this.BuildItemQueryRequest("Sku" + dr["SKUID"].ToString(), "Contains", dr["SKU"].ToString());

                        // Add to request list
                        ItemRequestList.Add(returnXML);
                    }
                    else
                    {
                        this.BuildSKUItemAddRequestXML(ItemRequestList, dr, messageConfig);
                    }
                }

                // Loop through each addon value Item in the dataset
                foreach (DataRow dr in ItemList.Tables[2].Rows)
                {
                    if (dr["WebServiceDownloadDte"].ToString().Length > 0)
                    {
                        string returnXML = this.BuildItemQueryRequest("AddOnValue" + dr["AddOnValueID"].ToString(), "Contains", dr["SKU"].ToString());

                        // Add to request list
                        ItemRequestList.Add(returnXML);
                    }
                    else
                    {
                        this.BuildAddOnValueItemAddRequestXML(ItemRequestList, dr, messageConfig);
                    }
                }
            }

            return ItemRequestList;
        }
        else if (requestType == RequestType.AdjustItemInventory)
        {
            return ItemInventoryDownloadRequestList;
        }
        else if (requestType == RequestType.AccountDownload)
        {
            if (accountDownloadRequestList.Count == 0)
            {
                accountDownloadRequestList = new ArrayList();

                AccountService accountService = new AccountService();
                int totalCount = 0;
                int rowsCount = accountService.GetTotalItems("WebServiceDownloadDte is null OR UpdateDte > WebServiceDownloadDte", out totalCount);

                if (rowsCount > 0)
                {
                    TList<Account> accountList = accountService.GetPaged("WebServiceDownloadDte is null OR UpdateDte > WebServiceDownloadDte", "AccountID Asc", 0, rowsCount, out totalCount);

                    foreach (Account accountEntity in accountList)
                    {
                        if (!accountEntity.WebServiceDownloadDte.HasValue)
                        {
                            this.BuildCustomerAddRequestXML(accountDownloadRequestList, accountEntity, messageConfig);
                        }
                        else if (accountEntity.UpdateDte.GetValueOrDefault(System.DateTime.Today) > accountEntity.WebServiceDownloadDte.Value)
                        {
                            // Build Request Query
                            this.BuildCustomerQueryRequestXML(accountDownloadRequestList, accountEntity);
                        }
                    }
                }
            }

            return accountDownloadRequestList;
        }

        return new ArrayList();
    }

    #endregion

    #region Build (Product)ItemInventoy RequestXML Methods

    #region Product
    /// <summary>
    /// Builds the product add query as a inventory Item
    /// </summary>
    /// <param name="req">ArrayList instance</param>
    /// <param name="productRow">Product Row</param>
    /// <param name="messageConfig">Message Config instance</param>
    protected void BuildProductAddRequestXML(ArrayList req, DataRow productRow, ZNodeMessageConfig messageConfig)
    {
        XmlDocument requestXMLDoc = new XmlDocument();

        // ItemInventoryadd request Query        
        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", null, null));
        requestXMLDoc.AppendChild(requestXMLDoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

        XmlElement queryBuilderXML = requestXMLDoc.CreateElement("QBXML");
        requestXMLDoc.AppendChild(queryBuilderXML);

        XmlElement queryBuilderXMLMsgsRq = requestXMLDoc.CreateElement("QBXMLMsgsRq");
        queryBuilderXMLMsgsRq.SetAttribute("onError", "stopOnError");
        queryBuilderXML.AppendChild(queryBuilderXMLMsgsRq);

        XmlElement ItemInventoryAddRq = requestXMLDoc.CreateElement("ItemInventoryAddRq");
        ItemInventoryAddRq.SetAttribute("requestID", "Product" + productRow["ProductID"].ToString());
        queryBuilderXMLMsgsRq.AppendChild(ItemInventoryAddRq);

        XmlElement ItemInventoryAdd = requestXMLDoc.CreateElement("ItemInventoryAdd");
        ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "Name", productRow["SKU"].ToString()));
        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "IsActive", Convert.ToInt32(productRow["ActiveInd"]).ToString()));
        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "ManufacturerPartNumber", productRow["ProductNum"].ToString()));
        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "SalesDesc", productRow["Name"].ToString()));
        decimal retailPrice = 0;
        if (productRow["RetailPrice"].ToString().Length > 0)
        {
            retailPrice = decimal.Parse(productRow["RetailPrice"].ToString());
        }

        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "SalesPrice", retailPrice.ToString("N2")));
        XmlElement IncomeAccountRef = requestXMLDoc.CreateElement("IncomeAccountRef");
        IncomeAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", messageConfig.GetMessage("SalesIncomeAccount")));
        ItemInventoryAdd.AppendChild(IncomeAccountRef);

        XmlElement COGSAccountRef = requestXMLDoc.CreateElement("COGSAccountRef");
        COGSAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", messageConfig.GetMessage("CostofGoodsSoldAccount")));
        ItemInventoryAdd.AppendChild(COGSAccountRef);

        XmlElement AssetAccountRef = requestXMLDoc.CreateElement("AssetAccountRef");
        AssetAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", messageConfig.GetMessage("InventoryAssetAccount")));
        ItemInventoryAdd.AppendChild(AssetAccountRef);
        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "QuantityOnHand", productRow["QuantityOnHand"].ToString()));
        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "TotalValue", retailPrice.ToString("N2")));

        // Add to request list
        req.Add(requestXMLDoc.OuterXml);
    }

    /// <summary>
    /// Search product Inventory Item in the QB list
    /// </summary>
    /// <param name="req">Array List instance</param>
    /// <param name="product">Product Instance</param>
    protected void BuildProductSKUQueryRequestXML(ArrayList req, Product product)
    {
        int skuCount = product.SKUCollection.Count;
        string returnOuterXML = string.Empty;

        if (skuCount == 1)
        {
            returnOuterXML = this.BuildItemQueryRequest("Product" + product.ProductID.ToString(), "Contains", product.SKUCollection[0].SKU);

            // Add to request list
            req.Add(returnOuterXML);
        }
        else
        {
            // Add
            foreach (SKU sku in product.SKUCollection)
            {
                returnOuterXML = this.BuildItemQueryRequest("Sku" + sku.SKUID.ToString(), "Contains", sku.SKU);

                // Add to request list
                req.Add(returnOuterXML);
            }
        }
    }

    /// <summary>
    /// Modify Product Inventory Item using ListID and Edit sequence objects
    /// </summary>
    /// <param name="req">ArrayList instnace</param>
    /// <param name="product">Product Instance</param>
    /// <param name="ListID">The value of List ID</param>
    /// <param name="EditSequence">Edit Sequence</param>
    /// <param name="messageConfig">Message Config instance</param>
    protected void BuildProductModifyRequestXML(ArrayList req, Product product, string ListID, string EditSequence, ZNodeMessageConfig messageConfig)
    {
        XmlDocument requestXMLDoc = new XmlDocument();

        // Sales Receipt Query
        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", null, null));
        requestXMLDoc.AppendChild(requestXMLDoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

        XmlElement queryBuilderXML = requestXMLDoc.CreateElement("QBXML");
        requestXMLDoc.AppendChild(queryBuilderXML);

        XmlElement queryBuilderXMLMsgsRq = requestXMLDoc.CreateElement("QBXMLMsgsRq");
        queryBuilderXMLMsgsRq.SetAttribute("onError", "stopOnError");
        queryBuilderXML.AppendChild(queryBuilderXMLMsgsRq);

        XmlElement ItemInventoryModRq = requestXMLDoc.CreateElement("ItemInventoryModRq");
        ItemInventoryModRq.SetAttribute("requestID", product.ProductID.ToString());
        queryBuilderXMLMsgsRq.AppendChild(ItemInventoryModRq);

        XmlElement ItemInventoryMod = requestXMLDoc.CreateElement("ItemInventoryMod");
        ItemInventoryModRq.AppendChild(ItemInventoryMod);

        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "ListID", ListID));
        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "EditSequence", EditSequence));

        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "IsActive", Convert.ToInt32(product.ActiveInd).ToString()));
        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "ManufacturerPartNumber", product.ProductNum));
        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "SalesDesc", product.Name));
        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "SalesPrice", product.RetailPrice.GetValueOrDefault(0).ToString("N2")));

        XmlElement IncomeAccountRef = requestXMLDoc.CreateElement("IncomeAccountRef");
        IncomeAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", messageConfig.GetMessage("SalesIncomeAccount")));
        ItemInventoryMod.AppendChild(IncomeAccountRef);

        XmlElement COGSAccountRef = requestXMLDoc.CreateElement("COGSAccountRef");
        COGSAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", messageConfig.GetMessage("CostofGoodsSoldAccount")));
        ItemInventoryMod.AppendChild(COGSAccountRef);

        XmlElement AssetAccountRef = requestXMLDoc.CreateElement("AssetAccountRef");
        AssetAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", messageConfig.GetMessage("InventoryAssetAccount")));
        ItemInventoryMod.AppendChild(AssetAccountRef);

        // Add to request list
        req.Add(requestXMLDoc.OuterXml);
    }
    #endregion

    #region SKU

    /// <summary>
    /// Builds the product add query as a inventory Item
    /// </summary>
    /// <param name="request">ArrayList instance</param>
    /// <param name="skuDataRow">Sku Data Row</param>
    /// <param name="configSettings">MessageConfig settings</param>
    protected void BuildSKUItemAddRequestXML(ArrayList request, DataRow skuDataRow, ZNodeMessageConfig configSettings)
    {
        XmlDocument requestXMLDoc = new XmlDocument();

        // ItemInventoryadd request Query        
        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", null, null));
        requestXMLDoc.AppendChild(requestXMLDoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

        XmlElement queryBuilderXML = requestXMLDoc.CreateElement("QBXML");
        requestXMLDoc.AppendChild(queryBuilderXML);

        XmlElement queryBuilderXMLMsgsRq = requestXMLDoc.CreateElement("QBXMLMsgsRq");
        queryBuilderXMLMsgsRq.SetAttribute("onError", "stopOnError");
        queryBuilderXML.AppendChild(queryBuilderXMLMsgsRq);

        XmlElement ItemInventoryAddRq = requestXMLDoc.CreateElement("ItemInventoryAddRq");
        ItemInventoryAddRq.SetAttribute("requestID", "Sku" + skuDataRow["SKUID"].ToString());
        queryBuilderXMLMsgsRq.AppendChild(ItemInventoryAddRq);

        XmlElement ItemInventoryAdd = requestXMLDoc.CreateElement("ItemInventoryAdd");
        ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "Name", skuDataRow["SKU"].ToString()));
        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "IsActive", Convert.ToInt32(skuDataRow["ActiveInd"]).ToString()));
        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "ManufacturerPartNumber", skuDataRow["ProductNum"].ToString()));
        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "SalesDesc", skuDataRow["ProductName"].ToString()));

        decimal retailPriceOverride = 0;
        if (skuDataRow["RetailPriceOverride"].ToString().Length > 0)
        {
            retailPriceOverride = decimal.Parse(skuDataRow["RetailPriceOverride"].ToString());
        }

        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "SalesPrice", retailPriceOverride.ToString("N2")));
        XmlElement IncomeAccountRef = requestXMLDoc.CreateElement("IncomeAccountRef");
        IncomeAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", configSettings.GetMessage("SalesIncomeAccount")));
        ItemInventoryAdd.AppendChild(IncomeAccountRef);

        XmlElement COGSAccountRef = requestXMLDoc.CreateElement("COGSAccountRef");
        COGSAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", configSettings.GetMessage("CostofGoodsSoldAccount")));
        ItemInventoryAdd.AppendChild(COGSAccountRef);

        XmlElement AssetAccountRef = requestXMLDoc.CreateElement("AssetAccountRef");
        AssetAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", configSettings.GetMessage("InventoryAssetAccount")));
        ItemInventoryAdd.AppendChild(AssetAccountRef);
        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "QuantityOnHand", skuDataRow["QuantityOnHand"].ToString()));
        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "TotalValue", retailPriceOverride.ToString("N2")));

        // Add to request list
        request.Add(requestXMLDoc.OuterXml);
    }

    /// <summary>
    /// Modify Product Inventory Item using ListID and Edit sequence objects
    /// </summary>
    /// <param name="req">ArrayList instance</param>
    /// <param name="sku">SKU instance</param>
    /// <param name="listID">The value of List ID</param>
    /// <param name="editSequence">Edit Sequence</param>
    /// <param name="messageConfig">Message Config instance</param>
    protected void BuildItemModifyRequestXML(ArrayList req, SKU sku, string listID, string editSequence, ZNodeMessageConfig messageConfig)
    {
        XmlDocument requestXMLDoc = new XmlDocument();

        // Sales Receipt Query
        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", null, null));
        requestXMLDoc.AppendChild(requestXMLDoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

        XmlElement queryBuilderXML = requestXMLDoc.CreateElement("QBXML");
        requestXMLDoc.AppendChild(queryBuilderXML);

        XmlElement queryBuilderXMLMsgsRq = requestXMLDoc.CreateElement("QBXMLMsgsRq");
        queryBuilderXMLMsgsRq.SetAttribute("onError", "stopOnError");
        queryBuilderXML.AppendChild(queryBuilderXMLMsgsRq);

        XmlElement ItemInventoryModRq = requestXMLDoc.CreateElement("ItemInventoryModRq");
        ItemInventoryModRq.SetAttribute("requestID", "SKU" + sku.SKUID.ToString());
        queryBuilderXMLMsgsRq.AppendChild(ItemInventoryModRq);

        XmlElement ItemInventoryMod = requestXMLDoc.CreateElement("ItemInventoryMod");
        ItemInventoryModRq.AppendChild(ItemInventoryMod);

        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "ListID", listID));
        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "EditSequence", editSequence));

        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "IsActive", Convert.ToInt32(sku.ActiveInd).ToString()));
        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "ManufacturerPartNumber", sku.ProductIDSource.ProductNum));
        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "SalesDesc", sku.ProductIDSource.Name));
        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "SalesPrice", sku.RetailPriceOverride.GetValueOrDefault(0).ToString("N2")));

        XmlElement IncomeAccountRef = requestXMLDoc.CreateElement("IncomeAccountRef");
        IncomeAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", messageConfig.GetMessage("SalesIncomeAccount")));
        ItemInventoryMod.AppendChild(IncomeAccountRef);

        XmlElement COGSAccountRef = requestXMLDoc.CreateElement("COGSAccountRef");
        COGSAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", messageConfig.GetMessage("CostofGoodsSoldAccount")));
        ItemInventoryMod.AppendChild(COGSAccountRef);

        XmlElement AssetAccountRef = requestXMLDoc.CreateElement("AssetAccountRef");
        AssetAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", messageConfig.GetMessage("InventoryAssetAccount")));
        ItemInventoryMod.AppendChild(AssetAccountRef);

        // Add to request list
        req.Add(requestXMLDoc.OuterXml);
    }
    #endregion

    #region AddOnValue

    /// <summary>
    /// Builds the product add query as a inventory Item
    /// </summary>
    /// <param name="request">Array List instance</param>
    /// <param name="addOnValueRow">Add on Value Data Row</param>
    /// <param name="configSettings">Message Config instance</param>
    protected void BuildAddOnValueItemAddRequestXML(ArrayList request, DataRow addOnValueRow, ZNodeMessageConfig configSettings)
    {
        XmlDocument requestXMLDoc = new XmlDocument();

        // ItemInventoryadd request Query        
        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", null, null));
        requestXMLDoc.AppendChild(requestXMLDoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

        XmlElement queryBuilderXML = requestXMLDoc.CreateElement("QBXML");
        requestXMLDoc.AppendChild(queryBuilderXML);

        XmlElement queryBuilderXMLMsgsRq = requestXMLDoc.CreateElement("QBXMLMsgsRq");
        queryBuilderXMLMsgsRq.SetAttribute("onError", "stopOnError");
        queryBuilderXML.AppendChild(queryBuilderXMLMsgsRq);

        XmlElement ItemInventoryAddRq = requestXMLDoc.CreateElement("ItemInventoryAddRq");
        ItemInventoryAddRq.SetAttribute("requestID", "AddOnValue" + addOnValueRow["AddOnValueID"].ToString());
        queryBuilderXMLMsgsRq.AppendChild(ItemInventoryAddRq);

        XmlElement ItemInventoryAdd = requestXMLDoc.CreateElement("ItemInventoryAdd");
        ItemInventoryAddRq.AppendChild(ItemInventoryAdd);

        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "Name", addOnValueRow["SKU"].ToString()));
        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "IsActive", "1"));
        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "ManufacturerPartNumber", string.Empty));
        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "SalesDesc", addOnValueRow["Name"].ToString()));

        decimal retailPrice = 0;
        if (addOnValueRow["RetailPrice"].ToString().Length > 0)
        {
            retailPrice = decimal.Parse(addOnValueRow["RetailPrice"].ToString());
        }

        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "SalesPrice", retailPrice.ToString("N2")));
        XmlElement IncomeAccountRef = requestXMLDoc.CreateElement("IncomeAccountRef");
        IncomeAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", configSettings.GetMessage("SalesIncomeAccount")));
        ItemInventoryAdd.AppendChild(IncomeAccountRef);

        XmlElement COGSAccountRef = requestXMLDoc.CreateElement("COGSAccountRef");
        COGSAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", configSettings.GetMessage("CostofGoodsSoldAccount")));
        ItemInventoryAdd.AppendChild(COGSAccountRef);

        XmlElement AssetAccountRef = requestXMLDoc.CreateElement("AssetAccountRef");
        AssetAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", configSettings.GetMessage("InventoryAssetAccount")));
        ItemInventoryAdd.AppendChild(AssetAccountRef);
        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "QuantityOnHand", addOnValueRow["QuantityOnHand"].ToString()));
        ItemInventoryAdd.AppendChild(this.MakeElement(requestXMLDoc, "TotalValue", retailPrice.ToString("N2")));

        // Add to request list
        request.Add(requestXMLDoc.OuterXml);
    }

    /// <summary>
    /// Modify Product Inventory Item using ListID and Edit sequence objects
    /// </summary>
    /// <param name="req">ArrayList instance</param>
    /// <param name="addOnValue">Add on Value instance</param>
    /// <param name="listID">Value of List ID</param>
    /// <param name="editSequence">Edit Sequence</param>
    /// <param name="messageConfig">Message Config instance</param>
    protected void BuildItemModifyRequestXML(ArrayList req, AddOnValue addOnValue, string listID, string editSequence, ZNodeMessageConfig messageConfig)
    {
        XmlDocument requestXMLDoc = new XmlDocument();

        // Sales Receipt Query
        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", null, null));
        requestXMLDoc.AppendChild(requestXMLDoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

        XmlElement queryBuilderXML = requestXMLDoc.CreateElement("QBXML");
        requestXMLDoc.AppendChild(queryBuilderXML);

        XmlElement queryBuilderXMLMsgsRq = requestXMLDoc.CreateElement("QBXMLMsgsRq");
        queryBuilderXMLMsgsRq.SetAttribute("onError", "stopOnError");
        queryBuilderXML.AppendChild(queryBuilderXMLMsgsRq);

        XmlElement ItemInventoryModRq = requestXMLDoc.CreateElement("ItemInventoryModRq");
        ItemInventoryModRq.SetAttribute("requestID", "AddOnValue" + addOnValue.AddOnValueID.ToString());
        queryBuilderXMLMsgsRq.AppendChild(ItemInventoryModRq);

        XmlElement ItemInventoryMod = requestXMLDoc.CreateElement("ItemInventoryMod");
        ItemInventoryModRq.AppendChild(ItemInventoryMod);

        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "ListID", listID));
        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "EditSequence", editSequence));

        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "IsActive", "1"));
        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "ManufacturerPartNumber", string.Empty));
        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "SalesDesc", addOnValue.Name));
        ItemInventoryMod.AppendChild(this.MakeElement(requestXMLDoc, "SalesPrice", addOnValue.RetailPrice.ToString("N2")));

        XmlElement IncomeAccountRef = requestXMLDoc.CreateElement("IncomeAccountRef");
        IncomeAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", messageConfig.GetMessage("SalesIncomeAccount")));
        ItemInventoryMod.AppendChild(IncomeAccountRef);

        XmlElement COGSAccountRef = requestXMLDoc.CreateElement("COGSAccountRef");
        COGSAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", messageConfig.GetMessage("CostofGoodsSoldAccount")));
        ItemInventoryMod.AppendChild(COGSAccountRef);

        XmlElement AssetAccountRef = requestXMLDoc.CreateElement("AssetAccountRef");
        AssetAccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", messageConfig.GetMessage("InventoryAssetAccount")));
        ItemInventoryMod.AppendChild(AssetAccountRef);

        // Add to request list
        req.Add(requestXMLDoc.OuterXml);
    }
    #endregion

    #endregion

    #region Download Customers request methods
    /// <summary>
    /// Build Customer add request xml
    /// </summary>
    /// <param name="req">ArrayList object</param>
    /// <param name="account">Account instance</param>
    /// <param name="messageConfig">Message Config instance</param>
    protected void BuildCustomerAddRequestXML(ArrayList req, Account account, ZNodeMessageConfig messageConfig)
    {
        XmlDocument requestXMLDoc = new XmlDocument();

        // Customer add Request Query        
        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", null, null));
        requestXMLDoc.AppendChild(requestXMLDoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

        XmlElement queryBuilderXML = requestXMLDoc.CreateElement("QBXML");
        requestXMLDoc.AppendChild(queryBuilderXML);

        XmlElement queryBuilderXMLMsgsRq = requestXMLDoc.CreateElement("QBXMLMsgsRq");
        queryBuilderXMLMsgsRq.SetAttribute("onError", "stopOnError");
        queryBuilderXML.AppendChild(queryBuilderXMLMsgsRq);

        XmlElement CustomerAddRq = requestXMLDoc.CreateElement("CustomerAddRq");
        CustomerAddRq.SetAttribute("requestID", account.AccountID.ToString());
        queryBuilderXMLMsgsRq.AppendChild(CustomerAddRq);

        XmlElement CustomerAdd = requestXMLDoc.CreateElement("CustomerAdd");
        CustomerAddRq.AppendChild(CustomerAdd);
        CustomerAdd.AppendChild(this.MakeElement(requestXMLDoc, "Name", account.AccountID.ToString()));

        AccountAdmin accountAdmin = new AccountAdmin();
        Address address = accountAdmin.GetDefaultBillingAddress(account.AccountID);
        if (address != null)
        {
            // Optional - Customer information
            CustomerAdd.AppendChild(this.MakeElement(requestXMLDoc, "CompanyName", account.CompanyName));
            CustomerAdd.AppendChild(this.MakeElement(requestXMLDoc, "FirstName", address.FirstName));
            CustomerAdd.AppendChild(this.MakeElement(requestXMLDoc, "MiddleName", address.MiddleName));
            CustomerAdd.AppendChild(this.MakeElement(requestXMLDoc, "LastName", address.LastName));

            // Billing address - Optional
            XmlElement BillAddress = requestXMLDoc.CreateElement("BillAddress");
            CustomerAdd.AppendChild(BillAddress);

            BillAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr1", address.FirstName + " " + address.LastName));
            BillAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr2", address.Street));
            BillAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr3", address.Street1));
            BillAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr4", string.Empty));
            BillAddress.AppendChild(this.MakeElement(requestXMLDoc, "City", address.City));
            BillAddress.AppendChild(this.MakeElement(requestXMLDoc, "State", address.StateCode));
            BillAddress.AppendChild(this.MakeElement(requestXMLDoc, "PostalCode", address.PostalCode));
        }

        address = accountAdmin.GetDefaultShippingAddress(account.AccountID);
        if (address != null)
        {
            // Shipping address  - Optional
            XmlElement ShipAddress = requestXMLDoc.CreateElement("ShipAddress");
            CustomerAdd.AppendChild(ShipAddress);

            ShipAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr1", address.FirstName + " " + address.LastName));
            ShipAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr2", address.Street));
            ShipAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr3", address.Street1));
            ShipAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr4", string.Empty));
            ShipAddress.AppendChild(this.MakeElement(requestXMLDoc, "City", address.City));
            ShipAddress.AppendChild(this.MakeElement(requestXMLDoc, "State", address.StateCode));
            ShipAddress.AppendChild(this.MakeElement(requestXMLDoc, "PostalCode", address.PostalCode));

            CustomerAdd.AppendChild(this.MakeElement(requestXMLDoc, "Phone", address.PhoneNumber));
            CustomerAdd.AppendChild(this.MakeElement(requestXMLDoc, "Fax", string.Empty));
            CustomerAdd.AppendChild(this.MakeElement(requestXMLDoc, "Email", account.Email));
            CustomerAdd.AppendChild(this.MakeElement(requestXMLDoc, "Contact", address.FirstName));
        }

        // Customer type (Retail Customer or Dealer) - Optional        
        string customerType = messageConfig.GetMessage("ProfileID" + account.ProfileID.GetValueOrDefault(0).ToString());
        if (customerType.Length > 0)
        {
            XmlElement CustomerTypeRef = requestXMLDoc.CreateElement("CustomerTypeRef");
            CustomerAdd.AppendChild(CustomerTypeRef);
            CustomerTypeRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", customerType));
        }

        // Add customer add request to web service request Queue
        req.Add(requestXMLDoc.OuterXml);
    }

    /// <summary>
    /// Method to build customer mod request xml
    /// </summary>
    /// <param name="req">ArrayList instance</param>
    /// <param name="account">Account instance</param>
    protected void BuildCustomerQueryRequestXML(ArrayList req, Account account)
    {
        XmlDocument requestXMLDoc = new XmlDocument();

        // Customer Query to find customer based on the Name match criterion
        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", null, null));
        requestXMLDoc.AppendChild(requestXMLDoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

        XmlElement queryBuilderXML = requestXMLDoc.CreateElement("QBXML");
        requestXMLDoc.AppendChild(queryBuilderXML);

        XmlElement queryBuilderXMLMsgsRq = requestXMLDoc.CreateElement("QBXMLMsgsRq");
        queryBuilderXMLMsgsRq.SetAttribute("onError", "stopOnError");
        queryBuilderXML.AppendChild(queryBuilderXMLMsgsRq);

        XmlElement CustomerQueryRq = requestXMLDoc.CreateElement("CustomerQueryRq");
        CustomerQueryRq.SetAttribute("requestID", account.AccountID.ToString());
        queryBuilderXMLMsgsRq.AppendChild(CustomerQueryRq);

        XmlElement NameFilter = requestXMLDoc.CreateElement("NameFilter");
        NameFilter.AppendChild(this.MakeElement(requestXMLDoc, "MatchCriterion", "Contains"));
        NameFilter.AppendChild(this.MakeElement(requestXMLDoc, "Name", account.AccountID.ToString()));
        CustomerQueryRq.AppendChild(NameFilter);

        req.Add(requestXMLDoc.OuterXml);
    }

    /// <summary>
    /// Method to find customer in the QB list
    /// </summary>
    /// <param name="req">ArrayList instance</param>
    /// <param name="account">Account Instance</param>
    /// <param name="listID">The value of List Id</param>
    /// <param name="editSequence">Edit Sequence</param>
    /// <param name="messageConfig">Message Config</param>
    protected void BuildCustomerModifyRequestXML(ArrayList req, Account account, string listID, string editSequence, ZNodeMessageConfig messageConfig)
    {
        XmlDocument requestXMLDoc = new XmlDocument();

        // Customer Mod(edit) Request Query        
        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", null, null));
        requestXMLDoc.AppendChild(requestXMLDoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

        XmlElement queryBuilderXML = requestXMLDoc.CreateElement("QBXML");
        requestXMLDoc.AppendChild(queryBuilderXML);

        XmlElement queryBuilderXMLMsgsRq = requestXMLDoc.CreateElement("QBXMLMsgsRq");
        queryBuilderXMLMsgsRq.SetAttribute("onError", "stopOnError");
        queryBuilderXML.AppendChild(queryBuilderXMLMsgsRq);

        XmlElement CustomerModRq = requestXMLDoc.CreateElement("CustomerModRq");
        CustomerModRq.SetAttribute("requestID", account.AccountID.ToString());
        queryBuilderXMLMsgsRq.AppendChild(CustomerModRq);

        XmlElement CustomerMod = requestXMLDoc.CreateElement("CustomerMod");
        CustomerModRq.AppendChild(CustomerMod);

        CustomerMod.AppendChild(this.MakeElement(requestXMLDoc, "ListID", listID));
        CustomerMod.AppendChild(this.MakeElement(requestXMLDoc, "EditSequence", editSequence));

        AccountAdmin accountAdmin = new AccountAdmin();
        Address address = accountAdmin.GetDefaultBillingAddress(account.AccountID);
        if (address != null)
        {
            // Set company & customer name
            // Optional
            CustomerMod.AppendChild(this.MakeElement(requestXMLDoc, "CompanyName", account.CompanyName));
            CustomerMod.AppendChild(this.MakeElement(requestXMLDoc, "FirstName", address.FirstName));
            CustomerMod.AppendChild(this.MakeElement(requestXMLDoc, "LastName", address.LastName));

            // Billing address
            XmlElement BillAddress = requestXMLDoc.CreateElement("BillAddress");
            CustomerMod.AppendChild(BillAddress);

            BillAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr1", address.FirstName + " " + address.LastName));
            BillAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr2", address.Street));
            BillAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr3", address.Street1));
            BillAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr4", string.Empty));
            BillAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr5", string.Empty));
            BillAddress.AppendChild(this.MakeElement(requestXMLDoc, "City", address.City));
            BillAddress.AppendChild(this.MakeElement(requestXMLDoc, "State", address.StateCode));
            BillAddress.AppendChild(this.MakeElement(requestXMLDoc, "PostalCode", address.PostalCode));
        }

        address = accountAdmin.GetDefaultShippingAddress(account.AccountID);
        if (address != null)
        {
            // Shipping address
            XmlElement ShipAddress = requestXMLDoc.CreateElement("ShipAddress");
            CustomerMod.AppendChild(ShipAddress);

            ShipAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr1", address.FirstName + " " + address.LastName));
            ShipAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr2", address.Street));
            ShipAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr3", address.Street1));
            ShipAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr4", string.Empty));
            ShipAddress.AppendChild(this.MakeElement(requestXMLDoc, "Addr5", string.Empty));
            ShipAddress.AppendChild(this.MakeElement(requestXMLDoc, "City", address.City));
            ShipAddress.AppendChild(this.MakeElement(requestXMLDoc, "State", address.StateCode));
            ShipAddress.AppendChild(this.MakeElement(requestXMLDoc, "PostalCode", address.PostalCode));

            CustomerMod.AppendChild(this.MakeElement(requestXMLDoc, "Phone", address.PhoneNumber));
            CustomerMod.AppendChild(this.MakeElement(requestXMLDoc, "Fax", string.Empty));
            CustomerMod.AppendChild(this.MakeElement(requestXMLDoc, "Email", account.Email));
            CustomerMod.AppendChild(this.MakeElement(requestXMLDoc, "Contact", address.FirstName));
        }

        // Customer type (Retail Customer or Dealer) - Optional
        string customerType = messageConfig.GetMessage("ProfileID" + account.ProfileID.GetValueOrDefault(0).ToString());
        if (customerType.Length > 0)
        {
            XmlElement CustomerTypeRef = requestXMLDoc.CreateElement("CustomerTypeRef");
            CustomerMod.AppendChild(CustomerTypeRef);
            CustomerTypeRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", customerType));
        }

        // Add customer add request to web service request Queue
        req.Add(requestXMLDoc.OuterXml);
    }
    #endregion

    #region Adjust Item Inventory level request XML

    /// <summary>
    /// Build request xml for the item to adjust quantity level
    /// </summary>
    /// <param name="request">Array List instance</param>
    /// <param name="sku">The value of sku</param>
    /// <param name="quantityOnHand">Quantity on Hand</param>
    /// <param name="messageConfig">Message Config instance</param>
    protected void BuildAdjustItemInventoryRequestXML(ArrayList request, string sku, int quantityOnHand, ZNodeMessageConfig messageConfig)
    {
        XmlDocument requestXMLDoc = new XmlDocument();

        // Sales Receipt Query
        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", null, null));
        requestXMLDoc.AppendChild(requestXMLDoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

        XmlElement queryBuilderXML = requestXMLDoc.CreateElement("QBXML");
        requestXMLDoc.AppendChild(queryBuilderXML);

        XmlElement queryBuilderXMLMsgsRq = requestXMLDoc.CreateElement("QBXMLMsgsRq");
        queryBuilderXMLMsgsRq.SetAttribute("onError", "stopOnError");
        queryBuilderXML.AppendChild(queryBuilderXMLMsgsRq);

        XmlElement InventoryAdjustmentAddRq = requestXMLDoc.CreateElement("InventoryAdjustmentAddRq");
        InventoryAdjustmentAddRq.SetAttribute("requestID", "InventoryAdjustent");
        queryBuilderXMLMsgsRq.AppendChild(InventoryAdjustmentAddRq);

        XmlElement InventoryAdjustmentAdd = requestXMLDoc.CreateElement("InventoryAdjustmentAdd");
        InventoryAdjustmentAddRq.AppendChild(InventoryAdjustmentAdd);

        XmlElement AccountRef = requestXMLDoc.CreateElement("AccountRef");
        InventoryAdjustmentAdd.AppendChild(AccountRef);
        AccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", messageConfig.GetMessage("SalesIncomeAccount")));

        InventoryAdjustmentAdd.AppendChild(this.BuildInventoryAdjustmentLineAdd(requestXMLDoc, InventoryAdjustmentAdd, sku, quantityOnHand));

        request.Add(requestXMLDoc.OuterXml);
    }

    /// <summary>
    /// Build bulk update request for all the items to adjust quantity level
    /// </summary>
    /// <param name="request">Array List instance</param>
    /// <param name="productList">Product List TList value</param>
    /// <param name="addOnValueList">Add On Value TList value</param>
    /// <param name="messageConfig">Message Config instance</param>
    protected void BuildAdjustItemInventoryRequestXML(ArrayList request, TList<Product> productList, TList<AddOnValue> addOnValueList, ZNodeMessageConfig messageConfig)
    {
        XmlDocument requestXMLDoc = new XmlDocument();

        // Sales Receipt Query
        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", null, null));
        requestXMLDoc.AppendChild(requestXMLDoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

        XmlElement queryBuuilderXML = requestXMLDoc.CreateElement("QBXML");
        requestXMLDoc.AppendChild(queryBuuilderXML);

        XmlElement queryBuilderXMLMsgsRq = requestXMLDoc.CreateElement("QBXMLMsgsRq");
        queryBuilderXMLMsgsRq.SetAttribute("onError", "stopOnError");
        queryBuuilderXML.AppendChild(queryBuilderXMLMsgsRq);

        XmlElement InventoryAdjustmentAddRq = requestXMLDoc.CreateElement("InventoryAdjustmentAddRq");
        InventoryAdjustmentAddRq.SetAttribute("requestID", "InventoryAdjustent");
        queryBuilderXMLMsgsRq.AppendChild(InventoryAdjustmentAddRq);

        XmlElement InventoryAdjustmentAdd = requestXMLDoc.CreateElement("InventoryAdjustmentAdd");
        InventoryAdjustmentAddRq.AppendChild(InventoryAdjustmentAdd);

        XmlElement AccountRef = requestXMLDoc.CreateElement("AccountRef");
        InventoryAdjustmentAdd.AppendChild(AccountRef);
        AccountRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", messageConfig.GetMessage("SalesIncomeAccount")));

        foreach (Product product in productList)
        {
            if (product.SKUCollection.Count == 1)
            {
                SKUInventoryService pis = new SKUInventoryService();
                SKUInventory productInventory = pis.GetBySKU(product.SKUCollection[0].SKU);

                if (productInventory != null)
                {
                    InventoryAdjustmentAdd.AppendChild(this.BuildInventoryAdjustmentLineAdd(requestXMLDoc, InventoryAdjustmentAdd, productInventory.SKU, productInventory.QuantityOnHand.GetValueOrDefault(0)));
                }
                else
                {
                    InventoryAdjustmentAdd.AppendChild(this.BuildInventoryAdjustmentLineAdd(requestXMLDoc, InventoryAdjustmentAdd, product.SKUCollection[0].SKU, 0));
                }
            }
            else
            {
                foreach (SKU sku in product.SKUCollection)
                {
                    SKUInventoryService sis = new SKUInventoryService();
                    SKUInventory skuInventory = sis.GetBySKU(sku.SKU);

                    InventoryAdjustmentAdd.AppendChild(this.BuildInventoryAdjustmentLineAdd(requestXMLDoc, InventoryAdjustmentAdd, sku.SKU, skuInventory.QuantityOnHand.GetValueOrDefault(0)));
                }
            }
        }

        foreach (AddOnValue addOnValue in addOnValueList)
        {
            InventoryAdjustmentAdd.AppendChild(this.BuildInventoryAdjustmentLineAdd(requestXMLDoc, InventoryAdjustmentAdd, addOnValue.SKU, ProductAdmin.GetQuantity(addOnValue)));
        }

        request.Add(requestXMLDoc.OuterXml);
    }
#endregion

    #region Download Orders request methods
    /// <summary>
    /// Build xml request to send it to quick books through web connector
    /// </summary>
    /// <param name="orderList">Order List</param>
    /// <param name="req">ArrayList object</param>
    /// <param name="messageConfig">Message Config instance</param>
    private void BuildOrderRequestXML(TList<Order> orderList, ArrayList req, ZNodeMessageConfig messageConfig)
    {
        string strRequestXML = string.Empty;
        XmlDocument requestXMLDoc = null;
        ProductService productService = new ProductService();

        foreach (Order order in orderList)
        {
            Account accountEntity = order.AccountIDSource;
            requestXMLDoc = new XmlDocument();
            string customerName = accountEntity.AccountID.ToString();

            if (!accountEntity.WebServiceDownloadDte.HasValue)
            {
                this.BuildCustomerAddRequestXML(req, accountEntity, messageConfig);
            }
            else if (accountEntity.UpdateDte.GetValueOrDefault(System.DateTime.Today) > accountEntity.WebServiceDownloadDte.Value)
            {
                this.BuildCustomerQueryRequestXML(req, accountEntity);
            }

            if (messageConfig.GetMessage("OrdersDownloadType") == "SalesOrder")
            {
                this.BuildSalesOrderRequestXML(requestXMLDoc, customerName, order, messageConfig);
            }
            else
            {
                // Sales Receipt Add Query        
                requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", null, null));
                requestXMLDoc.AppendChild(requestXMLDoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

                XmlElement queryBuilderXML = requestXMLDoc.CreateElement("QBXML");
                requestXMLDoc.AppendChild(queryBuilderXML);

                XmlElement queryBuilderXMLMsgsRq = requestXMLDoc.CreateElement("QBXMLMsgsRq");
                queryBuilderXMLMsgsRq.SetAttribute("onError", "stopOnError");
                queryBuilderXML.AppendChild(queryBuilderXMLMsgsRq);

                XmlElement SalesReceiptAddRq = requestXMLDoc.CreateElement("SalesReceiptAddRq");
                SalesReceiptAddRq.SetAttribute("requestID", order.OrderID.ToString());
                queryBuilderXMLMsgsRq.AppendChild(SalesReceiptAddRq);

                XmlElement SalesReceiptAdd = requestXMLDoc.CreateElement("SalesReceiptAdd");
                SalesReceiptAddRq.AppendChild(SalesReceiptAdd);

                // Associate customer with this order using User AccountID
                XmlElement CustomerRef = requestXMLDoc.CreateElement("CustomerRef");
                CustomerRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", customerName));
                SalesReceiptAdd.AppendChild(CustomerRef);

                string msg = messageConfig.GetMessage("SalesReceiptMessage");
                if (msg != null)
                {
                    // Custom message reference
                    XmlElement CustomerMsgRef = requestXMLDoc.CreateElement("CustomerMsgRef");
                    CustomerMsgRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", msg));
                    SalesReceiptAdd.AppendChild(CustomerMsgRef);
                }

                // Loop throught each order line item
                foreach (OrderLineItem lineItem in order.OrderLineItemCollection)
                {
                    string ItemRef = lineItem.SKU;
                    string description = lineItem.Name;
                    if (lineItem.ParentOrderLineItemID.HasValue)
                    {
                        description = lineItem.Name + " - " + lineItem.Description;
                    }

                    SalesReceiptAdd.AppendChild(this.BuildSaleReceiptLineItem(requestXMLDoc, ItemRef, description, lineItem.Quantity.Value.ToString(), lineItem.Price.Value.ToString("N2")));
                }

                string couponCode = "Discount";
                if (order.CouponCode.Length > 0) 
                { 
                    couponCode = " - " + order.CouponCode; 
                }

                // Set Tax
                SalesReceiptAdd.AppendChild(this.BuildSaleReceiptLineItem(requestXMLDoc, messageConfig.GetMessage("SalesTaxItem"), "Tax", " ", order.TaxCost.Value.ToString("N2")));
                
                // Set Discount
                SalesReceiptAdd.AppendChild(this.BuildSaleReceiptLineItem(requestXMLDoc, messageConfig.GetMessage("DiscountItemName"), couponCode, string.Empty, order.DiscountAmount.Value.ToString("N2")));
                
                // Set shipping Info
                string shippingDescription = order.ShippingIDSource.Description.Replace("�", string.Empty);
                SalesReceiptAdd.AppendChild(this.BuildSaleReceiptLineItem(requestXMLDoc, messageConfig.GetMessage("ShippingItemName"), shippingDescription, string.Empty, order.ShippingCost.Value.ToString("N2")));
            }

            // Request xml
            strRequestXML = requestXMLDoc.OuterXml;

            // Add it to request list
            req.Add(strRequestXML); 

            strRequestXML = string.Empty;
        }
    }

    /// <summary>
    /// Returns xml element for salereceiptLine item
    /// </summary>
    /// <param name="requestXMLDoc">Requested XML Doc</param>
    /// <param name="customerName">Customer Name</param>
    /// <param name="entity">Order Instance</param>
    /// <param name="messageConfig">Message Config instance</param>
    private void BuildSalesOrderRequestXML(XmlDocument requestXMLDoc, string customerName, Order entity, ZNodeMessageConfig messageConfig)
    {
        // Sales Receipt Add Query        
        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", null, null));
        requestXMLDoc.AppendChild(requestXMLDoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

        XmlElement queryBuilderXML = requestXMLDoc.CreateElement("QBXML");
        requestXMLDoc.AppendChild(queryBuilderXML);

        XmlElement queryBuilderXMLMsgsRq = requestXMLDoc.CreateElement("QBXMLMsgsRq");
        queryBuilderXMLMsgsRq.SetAttribute("onError", "stopOnError");
        queryBuilderXML.AppendChild(queryBuilderXMLMsgsRq);

        XmlElement SalesOrderAddRq = requestXMLDoc.CreateElement("SalesOrderAddRq");
        SalesOrderAddRq.SetAttribute("requestID", entity.OrderID.ToString());
        queryBuilderXMLMsgsRq.AppendChild(SalesOrderAddRq);

        XmlElement SalesOrderAdd = requestXMLDoc.CreateElement("SalesOrderAdd");
        SalesOrderAddRq.AppendChild(SalesOrderAdd);

        // Associate customer with this order using User AccountID
        XmlElement CustomerRef = requestXMLDoc.CreateElement("CustomerRef");
        CustomerRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", customerName));
        SalesOrderAdd.AppendChild(CustomerRef);

        string msg = messageConfig.GetMessage("SalesReceiptMessage");
        if (msg != null)
        {
            // Custom message reference
            XmlElement CustomerMsgRef = requestXMLDoc.CreateElement("CustomerMsgRef");
            CustomerMsgRef.AppendChild(this.MakeElement(requestXMLDoc, "FullName", msg));
            SalesOrderAdd.AppendChild(CustomerMsgRef);
        }

        // Loop throught each order line item
        foreach (OrderLineItem lineItem in entity.OrderLineItemCollection)
        {
            string ItemRef = lineItem.SKU;
            string description = lineItem.Name;
            if (lineItem.ParentOrderLineItemID.HasValue)
            {
                description = lineItem.Name + " - " + lineItem.Description;
            }

            SalesOrderAdd.AppendChild(this.BuildSaleOrderLineItem(requestXMLDoc, ItemRef, description, lineItem.Quantity.Value.ToString(), lineItem.Price.Value.ToString("N2")));
        }

        string couponCode = "Discount";
        if (entity.CouponCode.Length > 0) 
        { 
            couponCode = " - " + entity.CouponCode; 
        }

        // Set Tax
        SalesOrderAdd.AppendChild(this.BuildSaleOrderLineItem(requestXMLDoc, messageConfig.GetMessage("SalesTaxItem"), "Tax", string.Empty, entity.TaxCost.Value.ToString("N2")));
        
        // Set Discount
        SalesOrderAdd.AppendChild(this.BuildSaleOrderLineItem(requestXMLDoc, messageConfig.GetMessage("DiscountItemName"), couponCode, string.Empty, entity.DiscountAmount.Value.ToString("N2")));
        
        // Set shipping Info
        string shippingDescription = entity.ShippingIDSource.Description.Replace("�", string.Empty);
        SalesOrderAdd.AppendChild(this.BuildSaleOrderLineItem(requestXMLDoc, messageConfig.GetMessage("ShippingItemName"), shippingDescription, string.Empty, entity.ShippingCost.Value.ToString("N2")));
    }

    /// <summary>
    /// Returns xml element for salereceiptLine item
    /// </summary>
    /// <param name="requestXMLDoc">Requested XML Doc</param>
    /// <param name="ItemRef">Item Reference</param>
    /// <param name="Description">The value of Description</param>
    /// <param name="Quantity">The value of Quantity</param>
    /// <param name="rate">The Rate Value</param>
    /// <returns>Returns the Xml Element</returns>
    private XmlElement BuildSaleReceiptLineItem(XmlDocument requestXMLDoc, string ItemRef, string Description, string Quantity, string rate)
    {
        XmlElement SalesReceiptLineAdd = requestXMLDoc.CreateElement("SalesReceiptLineAdd");

        XmlElement ItemRefXML = requestXMLDoc.CreateElement("ItemRef");
        ItemRefXML.AppendChild(this.MakeElement(requestXMLDoc, "FullName", ItemRef));
        SalesReceiptLineAdd.AppendChild(ItemRefXML);

        SalesReceiptLineAdd.AppendChild(this.MakeElement(requestXMLDoc, "Desc", Description.Replace("<br />", string.Empty)));
        SalesReceiptLineAdd.AppendChild(this.MakeElement(requestXMLDoc, "Quantity", Quantity.ToString()));
        SalesReceiptLineAdd.AppendChild(this.MakeElement(requestXMLDoc, "Amount", rate));

        return SalesReceiptLineAdd;
    }

    /// <summary>
    /// Returns xml element for salereceiptLine item
    /// </summary>
    /// <param name="requestXMLDoc">Requested XML Document</param>
    /// <param name="ItemRef">Item Reference</param>
    /// <param name="Description">The value of Description</param>
    /// <param name="Quantity">The value of Quantity</param>
    /// <param name="rate">The Rate value</param>
    /// <returns>Returns the Xml Element</returns>
    private XmlElement BuildSaleOrderLineItem(XmlDocument requestXMLDoc, string ItemRef, string Description, string Quantity, string rate)
    {
        XmlElement SalesOrderLineAdd = requestXMLDoc.CreateElement("SalesOrderLineAdd");

        XmlElement ItemRefXML = requestXMLDoc.CreateElement("ItemRef");
        ItemRefXML.AppendChild(this.MakeElement(requestXMLDoc, "FullName", ItemRef));
        SalesOrderLineAdd.AppendChild(ItemRefXML);

        SalesOrderLineAdd.AppendChild(this.MakeElement(requestXMLDoc, "Desc", Description.Replace("<br />", string.Empty)));
        SalesOrderLineAdd.AppendChild(this.MakeElement(requestXMLDoc, "Quantity", Quantity.ToString()));
        SalesOrderLineAdd.AppendChild(this.MakeElement(requestXMLDoc, "Rate", rate));

        return SalesOrderLineAdd;
    }
    #endregion

    #region Helper Methods

    /// <summary>
    /// Update generic flag "DownloadDate" in the order table with time created in QB
    /// </summary>
    /// <param name="message">The value of Message</param>
    /// <param name="rootTagName">The value of rootTagName</param>
    private void UpdateQuickBooksStatus(string message, string rootTagName)
    {
        // Find matching time created tag in the response message
        Match m = Regex.Match(message, "(?<=<TimeModified[^>]*>).*?(?=</TimeModified>)", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
        string TimeModified = m.Value;
        string statusCode;

        string entityID = this.ParseRequestID(message, rootTagName, out statusCode);

        if (entityID.Length == 0)
        {
            entityID = "0";
        }

        if (statusCode == "0" && (rootTagName == "SalesReceiptAddRs" || rootTagName == "SalesOrderAddRs"))
        {
            OrderService service = new OrderService();
            Order _order = service.GetByOrderID(int.Parse(entityID));

            if (_order != null)
            {
                _order.WebServiceDownloadDate = DateTime.Parse(TimeModified);
                service.Update(_order);
            }
        }
        else if (statusCode == "0" && (rootTagName == "CustomerAddRs" || rootTagName == "CustomerModRs"))
        {
            AccountService accountService = new AccountService();
            Account entity = accountService.GetByAccountID(int.Parse(entityID));

            if (entity != null)
            {
                entity.WebServiceDownloadDte = DateTime.Parse(TimeModified);
                accountService.Update(entity);
            }
        }
        else if (statusCode == "0" && (rootTagName == "ItemInventoryAddRs" || rootTagName == "ItemInventoryModRs"))
        {
            if (entityID.Contains("Product"))
            {
                entityID = entityID.Replace("Product", string.Empty);

                ProductService service = new ProductService();
                Product _product = service.GetByProductID(int.Parse(entityID));

                if (statusCode == "0" && _product != null)
                {
                    _product.WebServiceDownloadDte = DateTime.Parse(TimeModified);
                    service.Update(_product);
                }
            }
            else if (entityID.Contains("Sku"))
            {
                entityID = entityID.Replace("Sku", string.Empty);
                SKUService service = new SKUService();
                SKU _sku = service.GetBySKUID(int.Parse(entityID));

                if (statusCode == "0" && _sku != null)
                {
                    _sku.WebServiceDownloadDte = DateTime.Parse(TimeModified);
                    service.Update(_sku);
                }
            }
            else if (entityID.Contains("AddOnValue"))
            {
                entityID = entityID.Replace("AddOnValue", string.Empty);
                AddOnValueService service = new AddOnValueService();
                AddOnValue _addOnValue = service.GetByAddOnValueID(int.Parse(entityID));

                if (statusCode == "0" && _addOnValue != null)
                {
                    _addOnValue.WebServiceDownloadDte = DateTime.Parse(TimeModified);
                    service.Update(_addOnValue);
                }
            }
        }
    }
    
    /// <summary>
    /// Build adjustment line item
    /// </summary>
    /// <param name="requestXMLDoc">Requested XML Doc</param>
    /// <param name="rootElement">Root Element</param>
    /// <param name="itemRef">Item Reference</param>
    /// <param name="newQuantity">New Quantity</param>
    /// <returns> Returns the XML Element</returns>
    private XmlElement BuildInventoryAdjustmentLineAdd(XmlDocument requestXMLDoc, XmlElement rootElement, string itemRef, int newQuantity)
    {
        XmlElement InventoryAdjustmentLineAdd = requestXMLDoc.CreateElement("InventoryAdjustmentLineAdd");
        rootElement.AppendChild(InventoryAdjustmentLineAdd);

        XmlElement ItemReference = requestXMLDoc.CreateElement("ItemRef");
        ItemReference.AppendChild(this.MakeElement(requestXMLDoc, "FullName", itemRef));
        InventoryAdjustmentLineAdd.AppendChild(ItemReference);

        XmlElement QuantityAdjustment = requestXMLDoc.CreateElement("QuantityAdjustment");
        QuantityAdjustment.AppendChild(this.MakeElement(requestXMLDoc, "NewQuantity", newQuantity.ToString()));
        InventoryAdjustmentLineAdd.AppendChild(QuantityAdjustment);

        return InventoryAdjustmentLineAdd;
    }
   
    /// <summary>
    /// Creates and Returns an Element with the specified tagName with the value 
    /// </summary>
    /// <param name="doc">Xml document</param>
    /// <param name="tagName">Value of Tag Name</param>
    /// <param name="tagValue">Value of TagValue</param>
    /// <returns>Returns the Xml Element</returns>
    private XmlElement MakeElement(XmlDocument doc, string tagName, string tagValue)
    {
        XmlElement elem = doc.CreateElement(tagName);
        elem.InnerText = tagValue;
        return elem;
    }

    /// <summary>
    /// Returns the RequestID by parsing the response xml using Xml reader
    /// </summary>
    /// <param name="message">Message Value</param>
    /// <param name="responseTagName">Response Tag Name</param>
    /// <param name="statusCode">Status Code</param>
    /// <returns>Returns the RequestID </returns>
    private string ParseRequestID(string message, string responseTagName, out string statusCode)
    {
        System.IO.TextReader txtReader = new System.IO.StringReader(message);
        XmlTextReader xmlreader = new XmlTextReader(txtReader);
        string requestID = "0";
        string _statusCode = "-1";

        // Read from XML File          
        while (xmlreader.Read())
        {
            switch (xmlreader.NodeType)
            {
                case XmlNodeType.Element:
                    if (xmlreader.HasAttributes && xmlreader.Name == responseTagName)
                    {
                        for (int i = 0; i < xmlreader.AttributeCount; i++)
                        {
                            xmlreader.MoveToAttribute(i);

                            if (xmlreader.Name == "statusCode")
                            {
                                _statusCode = xmlreader.Value;
                            }
                            else if (xmlreader.Name == "requestID")
                            {
                                requestID = xmlreader.Value;
                            }
                        }
                    }

                    break;

                default:
                    {
                        break;
                    }
            }
        }

        statusCode = _statusCode;
        return requestID;
    }

    /// <summary>
    /// Returns the profile Name for this profileID
    /// </summary>
    /// <param name="profileID">Profile ID</param>
    /// <returns>Returns the profile Name</returns>
    private string ProfileName(int profileID)
    {
        ProfileService profileService = new ProfileService();
        Profile profile = profileService.GetByProfileID(profileID);

        if (profile != null)
        {
            return profile.Name;
        }

        return string.Empty;
    }

    /// <summary>
    /// Get the Default SKU
    /// </summary>
    /// <param name="productID">Product ID</param>
    /// <returns>Returns the SKU Inventory instance</returns>
    private SKUInventory GetDefaultSKU(int productID)
    {
        SKUService skuService = new SKUService();
        TList<SKU> skuList = skuService.GetByProductID(productID);

        if (skuList != null && skuList.Count == 1)
        {
            SKUInventoryService skuInventoryService = new SKUInventoryService();

            return skuInventoryService.GetBySKU(skuList[0].SKU);
        }

        return null;
    }
    #endregion

    #region Receive & Parse Response XML
    /// <summary>
    /// Receive QB Customer Query and retireve ListId and EidtSequence tag value to update customer Info
    /// </summary>
    /// <param name="message">The value of message</param>
    /// <param name="rootTagName">The value of root Tag Name</param>
    /// <param name="reqType">The value of Request Type</param>
    /// <returns>Returns an Integer value</returns>
    private int DownloadCustomerInfo(string message, string rootTagName, RequestType reqType)
    {
        ZNodeMessageConfig messageConfig = this.MessageConfig;
        string editSequence = string.Empty;
        string listID = string.Empty;
        string statusCode = string.Empty;

        Match m = Regex.Match(message, "(?<=<EditSequence[^>]*>).*?(?=</EditSequence>)", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
        editSequence = m.Value;

        m = Regex.Match(message, "(?<=<ListID[^>]*>).*?(?=</ListID>)", RegexOptions.IgnoreCase | RegexOptions.IgnorePatternWhitespace);
        listID = m.Value;

        string accountID = this.ParseRequestID(message, rootTagName, out statusCode);
        if (accountID.Length == 0)
        {
            accountID = "0";
        }

        if (statusCode == "0")
        {
            AccountService service = new AccountService();
            Account account = service.GetByAccountID(int.Parse(accountID));

            if (account == null)
            {
                return 0;
            }

            if (reqType == RequestType.AccountDownload)
            {
                // Build update query
                this.BuildCustomerModifyRequestXML(accountDownloadRequestList, account, listID, editSequence, messageConfig);
            }
            else
            {
                // Build update query
                this.BuildCustomerModifyRequestXML(requestList, account, listID, editSequence, messageConfig);
            }

            return 1;
        }

        return 0;
    }

    #endregion

    #region ItemQuery request XML
    /// <summary>
    /// Search product Inventory Item in the QB list
    /// </summary>
    /// <param name="requestID">Request ID</param>
    /// <param name="matchCriterion">Match Criterion</param>
    /// <param name="valueToSearch">The value to Search</param>
    /// <returns>Returns the Inventory Item list</returns>
    private string BuildItemQueryRequest(string requestID, string matchCriterion, string valueToSearch)
    {
        XmlDocument requestXMLDoc = new XmlDocument();

        // Sales Receipt Query        
        requestXMLDoc.AppendChild(requestXMLDoc.CreateXmlDeclaration("1.0", null, null));
        requestXMLDoc.AppendChild(requestXMLDoc.CreateProcessingInstruction("qbxml", "version=\"7.0\""));

        XmlElement queryBuilderXML = requestXMLDoc.CreateElement("QBXML");
        requestXMLDoc.AppendChild(queryBuilderXML);

        XmlElement queryBuilderXMLMsgsRq = requestXMLDoc.CreateElement("QBXMLMsgsRq");
        queryBuilderXMLMsgsRq.SetAttribute("onError", "stopOnError");
        queryBuilderXML.AppendChild(queryBuilderXMLMsgsRq);

        XmlElement ItemInventoryQueryRq = requestXMLDoc.CreateElement("ItemInventoryQueryRq");
        ItemInventoryQueryRq.SetAttribute("requestID", requestID);
        queryBuilderXMLMsgsRq.AppendChild(ItemInventoryQueryRq);

        XmlElement NameFilter = requestXMLDoc.CreateElement("NameFilter");
        NameFilter.AppendChild(this.MakeElement(requestXMLDoc, "MatchCriterion", matchCriterion));
        NameFilter.AppendChild(this.MakeElement(requestXMLDoc, "Name", valueToSearch));
        ItemInventoryQueryRq.AppendChild(NameFilter);

        // Add to request list
        return requestXMLDoc.OuterXml;
    }
    #endregion
}