﻿using Microsoft.Practices.EnterpriseLibrary.Caching;
using System;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
/// <summary>
/// Summary description for CommonPageBase
/// </summary>
namespace Znode.Engine.Common
{
    /// <summary>
    /// Represents a Common Page Base class
    /// </summary>
    public class CommonPageBase : ZNodePageBase
    {
        #region Member Variables

        private const string PostBackEventTarget = "__EVENTTARGET";
        private int _categoryId;
        private int _productId;
        private int _lastcategoryId;
        private bool IsApplyStyle = true;
        private ContentPage _ContentPage;

        public ContentPage ContentPage
        {
            get { return _ContentPage; }
            set { _ContentPage = value; }
        }

        private ZNodeCategory _Category;

        public ZNodeCategory Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        private ZNodeProduct _Product;

        /// <summary>
        /// Gets or sets the ZNodeProduct
        /// </summary>
        public ZNodeProduct Product
        {
            get { return _Product; }
            set { _Product = value; }
        }

        private string LanguageDropDownName = "$ddlLanguage";

        #endregion

        #region Public Methods
        /// <summary>
        /// Get the UI Culture
        /// </summary>
        /// <returns>Returns the current UI culture</returns>
        public static string GetCurrentUICulture()
        {
            string CurrentUICulture = string.Empty;

            if (HttpContext.Current.Session["NewCulture"] != null)
            {
                CurrentUICulture = HttpContext.Current.Session["NewCulture"].ToString();
                HttpContext.Current.Session.Remove("NewCulture");
            }
            else
            {
                // Set Culture From Cookie
                if (HttpContext.Current.Request.Cookies["CultureLanguage"] != null)
                {
                    CurrentUICulture = HttpContext.Current.Request.Cookies["CultureLanguage"].Value;
                }
            }

            Locale validLocale = ZNodeCatalogManager.GetLocaleByLocaleCode(CurrentUICulture);

            if (validLocale != null)
            {
                // Set the localeId
                ZNode.Libraries.ECommerce.SEO.ZNodeSEOUrl.LocaleID = validLocale.LocaleID;

                CurrentUICulture = validLocale.LocaleCode;
            }

            return CurrentUICulture;
        }

        /// <summary>
        /// Set UI culture value in cookie.
        /// </summary>
        /// <param name="cultureValue">Culture Value</param>
        public void SetUICulture(string cultureValue)
        {
            // Remove the existing Cookie
            Response.Cookies.Remove("CultureLanguage");

            // Set Culture in Cookie
            HttpCookie Cookie = new HttpCookie("CultureLanguage");
            Cookie.Value = cultureValue;
            Cookie.Expires = DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]));
            Response.Cookies.Add(Cookie);

            HttpContext.Current.Session["NewCulture"] = cultureValue;

            // Set Culture in Session
            Session["CurrentUICulture"] = cultureValue;

            // Set NULL to last viewed category when locale change.
            Session["BreadCrumzcid"] = null;

            // Reset the Session and Cache Value
            System.Web.HttpContext.Current.Session.Remove("CatalogConfig");

            // Set Culture in Thread
            Thread.CurrentThread.CurrentUICulture = new CultureInfo(GetCurrentUICulture());

            // Set locale id in Cookie
            HttpCookie cookieLocaleId = new HttpCookie("LocaleID");
            cookieLocaleId.Value = ZNodeCatalogManager.LocaleId.ToString();
            cookieLocaleId.Expires = DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]));
            Response.Cookies.Add(cookieLocaleId);

            // Redirect to homepage.
            Response.Redirect("~/default.aspx");
        }
        #endregion

        #region Protected Methods and Events1
        /// <summary>
        /// Get Category Instance method
        /// </summary>
        /// <param name="categoryId">Represents the Category ID</param>
        protected void GetCategoryInstance(int categoryId)
        {
            // Default master page file path
            string masterPageFilePath = "~/themes/" + ZNodeCatalogManager.Theme + "/MasterPages/Category/category.master";

            // Check whether the category is enabled in category node. If not then redirect to home page
            CategoryNodeService categoryNodeService = new CategoryNodeService();
            string filterExpression = string.Format("CatalogID = {0} AND CategoryID = {1}", ZNodeCatalogManager.CatalogConfig.CatalogID, categoryId);
            TList<CategoryNode> categoryNodeList = categoryNodeService.Find(filterExpression);
            if (categoryNodeList == null || categoryNodeList.Count == 0)
            {
                Response.Redirect("~/default.aspx");
            }

            // Retrieve category data
            this.Category = ZNodeCategory.Create(categoryId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);

            if (this.Category != null && this.Category.VisibleInd)
            {
                // Set Template
                if (this.Category.MasterPage != null && this.Category.MasterPage.Length > 0 && this.Category.Theme != null && this.Category.Theme.Length > 0)
                {
                    string masterFilePath = ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + this.Category.Theme + "/MasterPages/" + this.Category.MasterPage + ".master";

                    if (System.IO.File.Exists(Server.MapPath(masterFilePath)))
                    {
                        // If template master page exists, then override default master page
                        masterPageFilePath = masterFilePath;
                    }
                }

                // Set CSS
                if (this.Category.Css != null && this.Category.Css.Length > 0 && this.Category.Theme != null && this.Category.Theme.Length > 0)
                {
                    this.IsApplyStyle = false;
                }
            }
            else
            {
                Response.Redirect("~/default.aspx");
            }

            // If department wise theme selected then save the theme to use in quick search and newsletter sign-up page.
            if (!string.IsNullOrEmpty(this.Category.Theme))
            {
                Session["CurrentCategoryTheme"] = this.Category.Theme;
            }

            // Set master page
            this.MasterPageFile = masterPageFilePath;

            // Add to httpContext
            HttpContext.Current.Items.Add("Category", this.Category);

            // Set SEO Properties 
            ZNodeSEO seo = new ZNodeSEO();

            // Set Default SEO values
            ZNodeMetaTags tags = new ZNodeMetaTags();
            seo.SEOTitle = tags.CategoryTitle(this.Category);
            seo.SEODescription = tags.CategoryDescription(this.Category);
            seo.SEOKeywords = tags.CategoryKeywords(this.Category);
        }

        /// <summary>
        /// Get Product Instance method
        /// </summary>
        /// <param name="productID">The value of Product ID</param>
        protected void GetProductInstance(int productID)
        {
            // Default master page file path
            string masterPageFilePath = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/Product/Product.master";

            if (Session["ProductObject"] != null)
            {
                // Retrieve product data
                this.Product = (ZNodeProduct)Session["ProductObject"];
                if (this.Product.ProductID != this._productId)
                {
                    this.Product = null;
                }
            }

            if (this.Product == null)
            {
                // Retrieve product data
                this.Product = ZNodeProduct.Create(this._productId, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.LocaleId);
            }

            if (Session["BreadCrumbCategoryId"] != null)
            {
                // Get categoryid from session
                this._lastcategoryId = int.Parse(Session["BreadCrumbCategoryId"].ToString());
            }

            if (this.Product != null && this.Product.IsActive)
            {
                foreach (ZnodeProductCategory productCategory in this.Product.ZNodeProductCategoryCollection)
                {
                    if (this._lastcategoryId == productCategory.CategoryID)
                    {
                        // Set Template
                        if (productCategory.MasterPage.Length > 0 && productCategory.MasterPage != null)
                        {
                            string masterFilePath = ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + productCategory.Theme + "/MasterPages/" + productCategory.MasterPage + ".master";

                            if (System.IO.File.Exists(Server.MapPath(masterFilePath)))
                            {
                                // If template master page exists, then override default master page
                                masterPageFilePath = masterFilePath;
                            }
                        }

                        // Set Theme
                        if (productCategory.CSS != null && productCategory.CSS.Length > 0 && productCategory.Theme != null && productCategory.Theme.Length > 0)
                        {
                            this.IsApplyStyle = false;
                            Session["CurrentCategoryTheme"] = productCategory.Theme;
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("~/default.aspx");
            }

            // Set master page
            this.MasterPageFile = masterPageFilePath;

            // Add to http context so it can be shared with user controls
            HttpContext.Current.Items.Add("Product", this.Product);

            Session["ProductObject"] = this.Product;

            // Set SEO Properties
            ZNodeSEO seo = new ZNodeSEO();

            // Set SEO default values to the product 
            ZNodeMetaTags tags = new ZNodeMetaTags();
            seo.SEOTitle = tags.ProductTitle(this.Product);
            seo.SEODescription = tags.ProductDescription(this.Product);
            seo.SEOKeywords = tags.ProductKeywords(this.Product);
        }

        /// <summary>
        /// Get Content page Instance method
        /// </summary>
        /// <param name="pageName">Represents a Page name</param>
        protected void GetContentPageInstance(string pageName)
        {
            string masterPageFilePath = string.Empty;

            if (pageName == "home")
            {
                // Default home.master page file path
                masterPageFilePath = "~/themes/" + ZNodeCatalogManager.Theme + "/MasterPages/home.master";

                // Get Home page data
                this.ContentPage = ZNodeContentManager.GetPageByName("home", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.CatalogConfig.LocaleID);
            }
            else
            {
                // Default master page file path
                masterPageFilePath = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/content.master";

                // Get content page data
                this.ContentPage = ZNodeContentManager.GetPageByName(pageName, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.CatalogConfig.LocaleID);
            }

            if (this.ContentPage != null && this.ContentPage.ActiveInd)
            {
                // Set Template
                if (this.ContentPage.MasterPageIDSource != null && this.ContentPage.ThemeIDSource != null)
                {
                    string masterFilePath = ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + this.ContentPage.ThemeIDSource.Name + "/MasterPages/" + this.ContentPage.MasterPageIDSource.Name + ".master";

                    // Only do the template override operation if the master page file exists                
                    if (System.IO.File.Exists(Server.MapPath(masterFilePath)))
                    {
                        // If template master page exists, then override default master page
                        masterPageFilePath = masterFilePath;
                    }
                }

                // Set Css
                if (this.ContentPage.CSSID != null && this.ContentPage.ThemeID != null)
                {
                    this.IsApplyStyle = false;
                }

                // Add to context for control access
                HttpContext.Current.Items.Add("PageTitle", this.ContentPage.Title);

                // SEO stuff
                ZNodeSEO seo = new ZNodeSEO();

                // Set Default SEO Values
                ZNodeMetaTags tags = new ZNodeMetaTags();
                seo.SEOTitle = tags.ContentTitle(this.ContentPage);
                seo.SEODescription = tags.ContentDescription(this.ContentPage);
                seo.SEOKeywords = tags.ContentKeywords(this.ContentPage);
                seo.SEOAdditionalMeta = tags.ContentAdditionalMeta(this.ContentPage);
            }

            // Set master page
            this.MasterPageFile = masterPageFilePath;
        }

        /// <summary>
        /// Get Content Page Instance
        /// </summary>
        /// <param name="contentPageId">Content page Id to load.</param>
        protected void GetContentPageInstance(int contentPageId)
        {
            string masterPageFilePath = string.Empty;

            // Default master page file path
            masterPageFilePath = "~/themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/MasterPages/content.master";

            // Get content page data
            this.ContentPage = ZNodeContentManager.GetPageById(contentPageId);

            if (this.ContentPage != null && this.ContentPage.ActiveInd)
            {
                // Set Template
                if (this.ContentPage.MasterPageIDSource != null && this.ContentPage.ThemeIDSource != null)
                {
                    string masterFilePath = ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Themes/" + this.ContentPage.ThemeIDSource.Name + "/MasterPages/" + this.ContentPage.MasterPageIDSource.Name + ".master";

                    // Only do the template override operation if the master page file exists                
                    if (System.IO.File.Exists(Server.MapPath(masterFilePath)))
                    {
                        // If template master page exists, then override default master page
                        masterPageFilePath = masterFilePath;
                    }
                }

                // Set Css
                if (this.ContentPage.CSSID != null && this.ContentPage.ThemeID != null)
                {
                    this.IsApplyStyle = false;
                }

                // Add to context for control access
                HttpContext.Current.Items.Add("PageTitle", this.ContentPage.Title);

                // SEO stuff
                ZNodeSEO seo = new ZNodeSEO();

                // Set Default SEO Values
                ZNodeMetaTags tags = new ZNodeMetaTags();
                seo.SEOTitle = tags.ContentTitle(this.ContentPage);
                seo.SEODescription = tags.ContentDescription(this.ContentPage);
                seo.SEOKeywords = tags.ContentKeywords(this.ContentPage);
                seo.SEOAdditionalMeta = tags.ContentAdditionalMeta(this.ContentPage);
            }
            else
            {
                this.GetContentPageInstance("home");
            }

            // Set master page
            this.MasterPageFile = masterPageFilePath;
        }

        #endregion

        #region InitializeCulture Events
        /// <summary>
        /// Initializes a UI Culture
        /// </summary>
        protected override void InitializeCulture()
        {
            string cultureValue = string.Empty;

            if (Request[PostBackEventTarget] != null && Request[PostBackEventTarget].Contains(this.LanguageDropDownName))
            {
                string controlID = Request[PostBackEventTarget];

                if (controlID.Contains(this.LanguageDropDownName))
                {
                    cultureValue = Request.Form[Request[PostBackEventTarget]].ToString();

                    // Set Culture
                    if (cultureValue != "-1")
                    {
                        this.SetUICulture(cultureValue);
                    }
                    else
                    {
                        this.SetUICulture("en");
                    }
                }
            }
            else
            {
                // Set Culture From Session
                if (HttpContext.Current.Session["CurrentUICulture"] != null)
                {
                    cultureValue = HttpContext.Current.Session["CurrentUICulture"].ToString();
                }
                else
                {
                    cultureValue = GetCurrentUICulture();
                }

                // Set Culture in Thread
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(cultureValue);
            }

            // Reset the session value
            HttpContext.Current.Session["CurrentUICulture"] = cultureValue;

            // Set locale id in Cookie
            HttpCookie cookieLocaleId = new HttpCookie("LocaleID");
            cookieLocaleId.Value = ZNodeCatalogManager.LocaleId.ToString();
            cookieLocaleId.Expires = DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]));
            Response.Cookies.Add(cookieLocaleId);

            base.InitializeCulture();
        }

        #endregion

        #region Render

        /// <summary>
        /// This method overrides the Render() method for the page and moves the ViewState
        /// from its default location at the top of the page to the bottom of the page. 
        /// </summary>
        /// <param name="writer">Html text Write instance</param>
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            base.Render(hw);
            string html = sw.ToString();

            hw.Close();
            //sw.Close();

            int start = html.IndexOf(@"<input type=""hidden"" name=""__VIEWSTATE""");

            if (start > -1)
            {
                int end = html.IndexOf("/>", start) + 2;

                string viewstate = html.Substring(start, end - start);
                html = html.Remove(start, end - start);

                int formend = html.IndexOf("</form>");
                html = html.Insert(formend, viewstate);
            }

            writer.Write(html);
        }

        #endregion

        #region Pre_Init
        /// <summary>
        /// Page Pre-Initialization Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected virtual void Page_PreInit(object sender, EventArgs e)
        {
            // Clear the current category session theme if some other category selected.
            Session["CurrentCategoryTheme"] = null;

            // Get page id from querystring  
            string _pageName = string.Empty;

            // Get Category id from querystring  
            if (Request.Params["zcid"] != null)
            {
                this._categoryId = int.Parse(Request.Params["zcid"]);

                this.GetCategoryInstance(this._categoryId);
            }
            else if (Request.Params["zpid"] != null)
            {
                // Get product id from querystring 
                this._productId = int.Parse(Request.Params["zpid"]);

                this.GetProductInstance(this._productId);
            }
            else if (Request.Params["page"] != null)
            {
                _pageName = Request.Params["page"];

                this.GetContentPageInstance(_pageName);
            }
            else if (Request.Params["zpgid"] != null)
            {
                int _pageId = Convert.ToInt32(Request.Params["zpgid"]);

                this.GetContentPageInstance(_pageId);
            }
            else
            {
                _pageName = "home";

                this.GetContentPageInstance(_pageName);
            }
        }

        /// <summary>
        /// Set the Header Section
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected virtual void Page_Load(object sender, EventArgs e)
        {
            BindCanonicalTags();

            if (this.IsApplyStyle)
            {
                // HTML 
                HtmlGenericControl Include = new HtmlGenericControl("link");
                Include.Attributes.Add("type", "text/css");
                Include.Attributes.Add("rel", "stylesheet");

                // If request comes from mobile then load the theme from ZNodePortal.MobileTheme
                if (Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null)
                {
                    Include.Attributes.Add("href", ZNodeCatalogManager.GetCssPathByLocale(ZNodeConfigManager.SiteConfig.MobileTheme, ZNodeCatalogManager.CSS));
                }
                else
                {
                    Include.Attributes.Add("href", ZNodeCatalogManager.GetCssPathByLocale(ZNodeCatalogManager.Theme, ZNodeCatalogManager.CSS));
                }

                // Add a reference for StyleSheet to the head section
                this.Page.Header.Controls.Add(Include);
            }
            else if (Request.Params["zcid"] != null)
            {
                // Get Category id from querystring  
                // HTML 
                HtmlGenericControl Include = new HtmlGenericControl("link");
                Include.Attributes.Add("type", "text/css");
                Include.Attributes.Add("rel", "stylesheet");
                Include.Attributes.Add("href", ZNodeCatalogManager.GetCssPathByLocale(this.Category.Theme, this.Category.Css));

                // Add a reference for StyleSheet to the head section
                this.Page.Header.Controls.Add(Include);
            }
            else if (Request.Params["zpid"] != null && Session["BreadCrumbCategoryId"] != null)
            {
                // get categoryid from session
                this._lastcategoryId = int.Parse(Session["BreadCrumbCategoryId"].ToString());

                foreach (ZnodeProductCategory _productCategory in this.Product.ZNodeProductCategoryCollection)
                {
                    if (this._lastcategoryId == _productCategory.CategoryID)
                    {
                        // HTML 
                        HtmlGenericControl Include = new HtmlGenericControl("link");
                        Include.Attributes.Add("type", "text/css");
                        Include.Attributes.Add("rel", "stylesheet");
                        Include.Attributes.Add("href", ZNodeCatalogManager.GetCssPathByLocale(_productCategory.Theme, _productCategory.CSS));

                        // Add a reference for StyleSheet to the head section
                        this.Page.Header.Controls.Add(Include);
                    }
                }
            }
            else
            {
                // HTML 
                HtmlGenericControl Include = new HtmlGenericControl("link");
                Include.Attributes.Add("type", "text/css");
                Include.Attributes.Add("rel", "stylesheet");
                Include.Attributes.Add("href", ZNodeCatalogManager.GetCssPathByLocale(this.ContentPage.ThemeIDSource.Name, this.ContentPage.CSSIDSource.Name));

                // Add a reference for StyleSheet to the head section
                this.Page.Header.Controls.Add(Include);
            }

            // Apply coupon from querystring
            if (Request.Params["coupon"] != null)
            {
                ZNodeShoppingCart _shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
                string couponcode = Request.Params["coupon"].ToString();

                // Check the coupon
                ZNode.Libraries.DataAccess.Service.PromotionService promoService = new PromotionService();

                PromotionQuery promoQuery = new PromotionQuery();
                promoQuery.Append(PromotionColumn.CouponCode, couponcode);
                promoQuery.Append(PromotionColumn.EnableCouponUrl, "true");

                TList<Promotion> promotionList = promoService.Find(promoQuery);

                if (promotionList.Count > 0)
                {
                    if (_shoppingCart == null)
                    {
                        _shoppingCart = new ZNodeShoppingCart();
                        _shoppingCart.AddToSession(ZNodeSessionKeyType.ShoppingCart);
                    }

                    _shoppingCart.AddCouponCode(couponcode);
                }
            }
        }

        #endregion

        #region Znode Version 7.2.2 - Canonical Tags

        /// <summary>
        /// Znode version 7.2.2 
        /// This function will solve your internal duplicate content issues and strengthen your websites presence in organic search.       
        /// <summary>
        /// <return></return> 
        private void BindCanonicalTags()
        {
            string domainPath = Request.Url.GetLeftPart(UriPartial.Authority);

            //Add Cannonical Tags on Category Page
            if (Request.Params["zcid"] != null && this.Category != null)
            {
                if (Request.Params["zcid"] != null && Request.Params["Brand"] != null && Request.QueryString.Count == 2)
                {
                    if (System.Configuration.ConfigurationManager.AppSettings["EnableCategoryBrandURL"].Equals("1"))
                    {
                        int brandId = 0;
                        int.TryParse(Request.Params["brand"].ToString(), out brandId);
                        ZNode.Libraries.DataAccess.Service.ManufacturerService mfgService = new ZNode.Libraries.DataAccess.Service.ManufacturerService();
                        ZNode.Libraries.DataAccess.Entities.Manufacturer mfg = mfgService.GetByManufacturerID(brandId);
                        string brandName = string.Empty;
                        brandName = (mfg != null) ? mfg.Name : Request.Params["brand"].ToString();
                        AddCanonicalTagOnCategoryBrandPage(this.Category, brandName, domainPath);
                    }
                }
                else
                {
                    AddCanonicalTagOnCategoryPage(this.Category, domainPath);
                }
            }

            //Add Cannonical Tags on Product Page
            else if (Request.Params["zpid"] != null && this.Product != null)
            {
                //Add Cannonical Tags on SKU Page
                if (Request.Params["sku"] != null)
                {
                    AddCanonicalTagOnSKUPage(domainPath);
                }
                else
                {
                    AddCanonicalTagOnProductPage(this.Product, domainPath);
                }
            }

            //Add Cannonical Tags on Brand Page
            else if (Request.Params["mid"] != null)
            {
                AddCanonicalTagOnBrandPage(Request.Params["mid"].ToString(), domainPath);
            }
            else if (Request.Params["page"] != null)
            {
                AddCanonicalTagOnContentPage(Request.Params["page"], domainPath);
            }
            else if (Request.Params["zpgid"] != null)
            {
                AddCanonicalTagOnContentPageByPageId(Convert.ToInt32(Request.Params["zpgid"]), domainPath);
            }
            else
            {
                string currentURL = Request.Path.ToLower();
                string includeContentCanonical = ConfigurationManager.AppSettings["IncludeContentCanonical"];
                string includeHomeCanonical = ConfigurationManager.AppSettings["IncludeHomeCanonical"];

                //Add canonical tag for pages which is neither static nor created as static content pages through admin
                if ((currentURL.ToLower().Contains("/AllCategories.aspx") || currentURL.ToLower().Contains("/quickordernotepad.aspx") || currentURL.ToLower().Contains("/contact.aspx")) && includeContentCanonical.Equals("1"))
                {
                    HtmlGenericControl IncludeConanical = new HtmlGenericControl("link");
                    IncludeConanical.Attributes.Add("rel", "canonical");
                    IncludeConanical.Attributes.Add("href", domainPath.ToLower() + currentURL.ToLower());

                    // Add a reference for canonical to the head section
                    if (this.Page.Header != null) this.Page.Header.Controls.Add(IncludeConanical);
                }
            }
        }

        /// <summary>
        /// Znode version 7.2.2 
        /// This function is used to add canonical tags on categary pages under head tag.       
        /// <summary>
        /// <param="category">Object of ZNodeCategory</param>
        /// <param name="domainPath">String Domain Name</param>
        private void AddCanonicalTagOnCategoryPage(ZNodeCategory category, string domainPath)
        {
            string includeCategoryCanonical = ConfigurationManager.AppSettings["IncludeCategoryCanonical"];
            if (!string.IsNullOrEmpty(includeCategoryCanonical))
            {
                if (includeCategoryCanonical.Equals("1"))
                {
                    string categoryURL = string.IsNullOrEmpty(category.SEOURL) ? string.Empty : category.SEOURL.ToLower().ToString();
                    if (!string.IsNullOrEmpty(categoryURL))
                    {
                        HtmlGenericControl IncludeConanical = new HtmlGenericControl("link");
                        IncludeConanical.Attributes.Add("rel", "canonical");
                        IncludeConanical.Attributes.Add("href", domainPath.ToLower() + "/" + categoryURL.ToLower());

                        // Add a reference for canonical to the head section
                        if (this.Page.Header != null) this.Page.Header.Controls.Add(IncludeConanical);
                    }
                }
            }

        }

        /// <summary>
        /// Znode version 7.2.2 
        /// This function is used to add canonical tags on product pages under head tag.       
        /// <summary>
        /// <paparam name="product">Object of ZnodeProduct</paparam>
        /// <param name="domainPath">string Domain Path</param>
        private void AddCanonicalTagOnProductPage(ZNodeProduct product, string domainPath)
        {

            string includeProductCanonical = ConfigurationManager.AppSettings["IncludeProductCanonical"];
            if (!string.IsNullOrEmpty(includeProductCanonical))
            {
                if (includeProductCanonical.Equals("1"))
                {
                    string productURL = string.IsNullOrEmpty(product.SEOURL) ? string.Empty : product.SEOURL.ToLower().ToString();
                    if (!string.IsNullOrEmpty(productURL))
                    {
                        HtmlGenericControl IncludeConanical = new HtmlGenericControl("link");
                        IncludeConanical.Attributes.Add("rel", "canonical");
                        IncludeConanical.Attributes.Add("href", domainPath.ToLower() + "/" + productURL.ToLower());

                        // Add a reference for canonical to the head section
                        if (this.Page.Header != null) this.Page.Header.Controls.Add(IncludeConanical);
                    }
                }
            }
        }

        /// <summary>
        /// Znode version 7.2.2 
        /// This function is used to add canonical tags on brand pages under head tag.       
        /// <summary>
        /// <param name="manufacturerID">string Manufacture ID</param>
        /// <param name="domainPath">string Domain Path</param>
        private void AddCanonicalTagOnBrandPage(string manufacturerID, string domainPath)
        {
            string includeBrandCanonical = ConfigurationManager.AppSettings["IncludeBrandCanonical"];
            if (!string.IsNullOrEmpty(includeBrandCanonical))
            {
                if (includeBrandCanonical.Equals("1"))
                {
                    int mid = 0;
                    Int32.TryParse(manufacturerID, out mid);
                    ManufacturerService manufacturerService = new ManufacturerService();
                    Manufacturer brand = manufacturerService.GetByManufacturerID(mid);

                    if (brand != null)
                    {
                        string brandURL = string.IsNullOrEmpty(brand.Custom3) ? string.Empty : brand.Custom3.ToLower().ToString();

                        if (!string.IsNullOrEmpty(brandURL))
                        {
                            HtmlGenericControl IncludeConanical = new HtmlGenericControl("link");
                            IncludeConanical.Attributes.Add("rel", "canonical");
                            IncludeConanical.Attributes.Add("href", domainPath.ToLower() + "/" + brandURL.ToLower());

                            // Add a reference for canonical to the head section
                            if (this.Page.Header != null) this.Page.Header.Controls.Add(IncludeConanical);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Znode version 7.2.2 
        /// This function is used to add canonical tags on SKU pages under head tag.       
        /// <summary>
        /// <param name="domainPath">string Domain Path</param>
        private void AddCanonicalTagOnSKUPage(string domainPath)
        {
            string includeSKUCanonical = ConfigurationManager.AppSettings["IncludeSKUCanonical"];
            if (!string.IsNullOrEmpty(includeSKUCanonical))
            {
                if (includeSKUCanonical.Equals("1"))
                {
                    string skuURL = string.Empty;
                    if (HttpContext.Current.Session["SetCanonicalForSKU"] != null)
                    {
                        // Retrieve profile
                        skuURL = HttpContext.Current.Session["SetCanonicalForSKU"] as string;
                        HttpContext.Current.Session.Remove("SetCanonicalForSKU");
                    }

                    if (!string.IsNullOrEmpty(skuURL))
                    {
                        HtmlGenericControl IncludeConanical = new HtmlGenericControl("link");
                        IncludeConanical.Attributes.Add("rel", "canonical");
                        IncludeConanical.Attributes.Add("href", domainPath.ToLower() + "/" + skuURL.ToLower());

                        // Add a reference for canonical to the head section
                        if (this.Page.Header != null) this.Page.Header.Controls.Add(IncludeConanical);
                    }
                }
            }
        }

        /// <summary>
        /// Znode version 7.2.2 
        /// This function is used to add canonical tags on category brand pages under head tag.       
        /// <summary>
        /// <paparam name="category">Object of ZnodeCategary</paparam>
        /// <param name="brand">string Brand</param>
        /// <param name="domainPath">string Domain Path</param>
        private void AddCanonicalTagOnCategoryBrandPage(ZNodeCategory category, string brand, string domainPath)
        {
            string includeCategoryBrandCanonical = ConfigurationManager.AppSettings["IncludeCategoryBrandCanonical"];
            if (!string.IsNullOrEmpty(includeCategoryBrandCanonical))
            {

                if (includeCategoryBrandCanonical.Equals("1"))
                {
                    string categoryURL = string.IsNullOrEmpty(category.SEOURL) ? string.Empty : category.SEOURL.ToLower().ToString();
                    if (!string.IsNullOrEmpty(categoryURL))
                    {
                        HtmlGenericControl IncludeConanical = new HtmlGenericControl("link");
                        IncludeConanical.Attributes.Add("rel", "canonical");
                        if (!string.IsNullOrEmpty(brand))
                        {
                            IncludeConanical.Attributes.Add("href", domainPath + "/" + categoryURL.ToLower() + "/" + HttpUtility.UrlEncode(brand.ToLower()));
                        }
                        else
                        {
                            IncludeConanical.Attributes.Add("href", domainPath + "/" + categoryURL.ToLower());
                        }

                        // Add a reference for canonical to the head section
                        if (this.Page.Header != null) this.Page.Header.Controls.Add(IncludeConanical);
                    }
                }
            }
        }

        /// <summary>
        /// Znode version 7.2.2 
        /// This function is used to add canonical tags on content pages under head tag.       
        /// <summary>
        /// <paparam name="pageName">string Page Name</paparam>
        /// <param name="domainPath">string Domain Path</param>
        private void AddCanonicalTagOnContentPage(string pageName, string domainPath)
        {
            string includeContentCanonical = string.Empty;
            if (pageName == "home")
            {
                // Get Home page data
                this.ContentPage = ZNodeContentManager.GetPageByName("home", ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.CatalogConfig.LocaleID);

                includeContentCanonical = ConfigurationManager.AppSettings["IncludeHomeCanonical"];
            }
            else
            {
                // Get content page data
                this.ContentPage = ZNodeContentManager.GetPageByName(pageName, ZNodeConfigManager.SiteConfig.PortalID, ZNodeCatalogManager.CatalogConfig.LocaleID);

                includeContentCanonical = ConfigurationManager.AppSettings["IncludeContentCanonical"];
            }

            if (this.ContentPage != null && this.ContentPage.ActiveInd && !string.IsNullOrEmpty(includeContentCanonical))
            {
                if (includeContentCanonical.Equals("1"))
                {
                    string contentURL = string.IsNullOrEmpty(this.ContentPage.SEOURL) ? string.Empty : this.ContentPage.SEOURL.ToLower().ToString();
                    if (!string.IsNullOrEmpty(contentURL))
                    {
                        HtmlGenericControl IncludeConanical = new HtmlGenericControl("link");
                        IncludeConanical.Attributes.Add("rel", "canonical");
                        IncludeConanical.Attributes.Add("href", domainPath.ToLower() + "/" + contentURL.ToLower());

                        // Add a reference for canonical to the head section
                        if (this.Page.Header != null) this.Page.Header.Controls.Add(IncludeConanical);
                    }
                }
            }
        }

        /// <summary>
        /// Znode version 7.2.2 
        /// This function is used to add canonical tags on content page by pageId under head tag.       
        /// <summary>
        /// <paparam name="pageId">int PageID</paparam>
        /// <param name="domainPath">string DomainPath</param>
        private void AddCanonicalTagOnContentPageByPageId(int pageId, string domainPath)
        {
            string includeContentCanonical = ConfigurationManager.AppSettings["IncludeContentCanonical"];

            // Get content page data
            this.ContentPage = ZNodeContentManager.GetPageById(pageId);

            if (this.ContentPage != null && this.ContentPage.ActiveInd && !string.IsNullOrEmpty(includeContentCanonical))
            {
                if (includeContentCanonical.Equals("1"))
                {
                    string contentURL = string.IsNullOrEmpty(this.ContentPage.SEOURL) ? string.Empty : this.ContentPage.SEOURL.ToLower().ToString();
                    if (!string.IsNullOrEmpty(contentURL))
                    {
                        HtmlGenericControl IncludeConanical = new HtmlGenericControl("link");
                        IncludeConanical.Attributes.Add("rel", "canonical");
                        IncludeConanical.Attributes.Add("href", domainPath.ToLower() + "/" + contentURL.ToLower());

                        // Add a reference for canonical to the head section
                        if (this.Page.Header != null) this.Page.Header.Controls.Add(IncludeConanical);
                    }
                }
            }
        }

        #endregion
    }
}