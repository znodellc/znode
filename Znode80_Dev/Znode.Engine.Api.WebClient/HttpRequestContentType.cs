﻿namespace Znode.Engine.Api.WebClient
{
    public static class HttpRequestContentType
    {
        public static readonly string Json = "application/json";
        public static readonly string Xml = "application/xml";
    }
}
