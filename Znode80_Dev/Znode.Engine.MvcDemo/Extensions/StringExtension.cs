﻿using System;
using System.Linq;

namespace Znode.Engine.MvcDemo.Extensions
{
	public static class StringExtension
	{
		// Extension method; the first parameter takes the "this" keyword and specifies the type for which the method is defined
		public static string FirstCharToUpper(this string input)
		{
			if (String.IsNullOrEmpty(input))
				throw new ArgumentException("Input string is null");

			return input.First().ToString().ToUpper() + String.Join("", input.Skip(1));
		}
	}
}