﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Web;
using WebGrease.Css.Extensions;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public class SearchAgent : BaseAgent, ISearchAgent
    {
        private readonly ISearchClient _searchClient;
        private readonly IProductsClient _productClient;
        private readonly ISkuAgent _skuAgent;
        private readonly ICategoryAgent _categoryAgent;

        private readonly ExpandCollection expands = new ExpandCollection()
            {
                ExpandKeys.Images,
                ExpandKeys.Skus,
                ExpandKeys.Promotions,
                ExpandKeys.Reviews,
                ExpandKeys.Facets,
                ExpandKeys.Categories,
                ExpandKeys.Attributes,
                ExpandKeys.AddOns
            };

        public SearchAgent()
        {
            _searchClient = GetClient<SearchClient>();
            _productClient = GetClient<ProductsClient>();
            _skuAgent = new SkuAgent();
            _categoryAgent = new CategoryAgent();
        }

        /// <summary>
        /// Gets the suggestions for the search
        /// </summary>
        /// <param name="searchTerm">Search Term</param>
        /// <param name="category">Category Name</param>
        /// <returns>Returns a list of suggestion as suggested search model</returns>
        public List<SuggestedSearchModel> GetSuggestions(string searchTerm, string category)
        {
            
return _searchClient.GetTypeAheadResponse(SearchMap.ToModel(searchTerm, category)).SuggestedSearchResults.ToList();
        }

        /// <summary>
        /// Search based on the search term
        /// </summary>
        /// <param name="requestModel">Search Request Model</param>
        /// <returns>Returns the list of Keyword search view model</returns>
        public KeywordSearchViewModel Search(SearchRequestModel requestModel)
        {
			var searchResult = SearchProducts(requestModel.SearchTerm, requestModel.Category, requestModel.RefineBy, GetInnerSearchList(requestModel.InnerSearchKeywords), expands, GetSortCollection(requestModel.Sort));

			if (searchResult != null)
			{
				var pageNumber = requestModel.PageNumber.GetValueOrDefault(1);
				var pageSize = requestModel.PageSize.GetValueOrDefault(MvcDemoConstants.DefaultPageSize);

				// check current page number exceeds total products, then set page = 1;
				if (pageNumber > Math.Ceiling(searchResult.Products.Count / (float)pageSize))
				{
					pageNumber = 1;
				}

				requestModel.PageNumber = pageNumber;

				var productList = GetProducts(searchResult, pageNumber, pageSize, requestModel.Sort, expands);

				if (productList != null && productList.Count > 0)
				{
					var searchFacets = new Collection<SearchFacetModel>();
					var facetsRefinedBy = requestModel.RefineBy.SelectMany(x => x.Value.Select(y => new KeyValuePair<string, string>(x.Key, y)));

					if (searchResult.Facets != null)
					{
						searchResult.Facets.ToList().ForEach
							(x => x.AttributeValues = new Collection<SearchFacetValueModel>(x.AttributeValues.Where
																								(y =>
																									!facetsRefinedBy.Any(
																										z =>
																										z.Key ==
																										x.AttributeName
																										.Split('|')[0] &&
																										z.Value ==
																										y.AttributeValue))
																								.ToList()));

						searchFacets = new Collection<SearchFacetModel>(searchResult.Facets.Where(x => x.AttributeValues.Any()).ToList());
					}

					// To retrieve  Selected Facets 
					var selectedFacets = new List<KeyValuePair<string, KeyValuePair<string, string>>>();

					requestModel.RefineBy.ForEach(
						x =>
							selectedFacets.Add(new KeyValuePair<string, KeyValuePair<string, string>>(
													x.Key, new KeyValuePair<string, string>(
																string.Join(",", x.Value), SearchMap.UpdateUrlQueryString(x.Key, string.Empty, false, true)))));

					var facetList = new Collection<FacetViewModel>(selectedFacets.Select(x => new FacetViewModel()
					{
						AttributeName = x.Key,
						AttributeValues = new Collection<FacetValueViewModel>(x.Value.ToString().Split(',').Take(x.Value.ToString().Split(',').Length - 1).Select(y => new FacetValueViewModel()
						{
							AttributeValue = y.Replace("[", ""),
							RefineByUrl = SearchMap.UpdateUrlQueryString(x.Key, y.Replace("[", ""), false, true).Replace("]", "")
						}).ToList())
					}).ToList());

					SaveInSession(MvcDemoConstants.CategoryName, requestModel.Category);

                    CategoryViewModel categoryModel = null;

                    if (!string.IsNullOrEmpty(requestModel.Category))
                    {
                        categoryModel = _categoryAgent.GetCategoryByName(requestModel.Category);
                    }
                    
					return new KeywordSearchViewModel()
					{
						Products = new Collection<ProductViewModel>(productList),
						TotalProducts = searchResult.Products.Count,
						Facets = searchFacets != null ? new Collection<FacetViewModel>(searchFacets.Select(SearchMap.ToFacetViewModel).ToList()) : null,
						Categories = searchResult.Categories != null ? new Collection<SearchCategoryViewModel>(searchResult.Categories.Select(SearchMap.ToCategoryViewModel).ToList()) : null,
						SelectedFacets = selectedFacets != null ? new Collection<FacetViewModel>(facetList) : null,
						TotalPages = (int)Math.Ceiling(searchResult.Products.Count() / (decimal)MvcDemoConstants.DefaultPageSize),
						SearchTerm = requestModel.SearchTerm,
						Category = WebUtility.UrlDecode(requestModel.Category),
                        SeoDescription = categoryModel != null ? categoryModel.SeoDescription : string.Empty,
                        SeoTitle = categoryModel != null ? categoryModel.SeoTitle : string.Empty,
                        SeoKeywords = categoryModel != null ? categoryModel.SeoKeywords : string.Empty,
                        CategoryBanner = categoryModel != null ? categoryModel.CategoryBanner : string.Empty

					};
				}
			}

			return new KeywordSearchViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorProductSearchResult };
        }

        /// <summary>
        /// Search the products
        /// </summary>
        /// <param name="searchTerm">Search Term</param>
        /// <param name="category">Category Name</param>
        /// <param name="refineBy">Refine By Key and values</param>
        /// <param name="innerSearchList">Inner Search List</param>
        /// <param name="expands">Expands</param>
        /// <param name="sortCollection">Sort Collection</param>
        /// <returns>Returns the list of Keyword search model</returns>
        private KeywordSearchModel SearchProducts(string searchTerm, string category, Collection<KeyValuePair<string, IEnumerable<string>>> refineBy, List<string> innerSearchList, ExpandCollection expands, SortCollection sortCollection)
        {
            return _searchClient.Search(SearchMap.ToKeywordSearchModel(searchTerm, category, refineBy, innerSearchList), expands, sortCollection);
        }

        /// <summary>
        /// Get the products based on the search result.
        /// </summary>
        /// <param name="searchResult">Search Result</param>
        /// <param name="pageNumber">Page Number</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="sortBy">Sort By</param>
        /// <param name="expands">Expands</param>
        /// <param name="status">Status</param>
        /// <returns>Returns the list of products as Product View Model</returns>
        private List<ProductViewModel> GetProducts(KeywordSearchModel searchResult, int pageNumber, int pageSize, string sortBy, ExpandCollection expands)
        {
            var productList = new Collection<ProductModel>();
            string selectedSku = string.Empty;
            string imagePath = string.Empty;
            //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
            string imageMediumPath = string.Empty;

            var currentProductIds = GetCurrentPageProductIDs(searchResult, pageNumber, pageSize, sortBy);

            // API Call to get Product Information
            if (searchResult.Products.Count == 1)
            {
                productList.Add(_productClient.GetProduct(Convert.ToInt32(searchResult.Products.First().Id), expands));
            }
            else
            {
                productList = GetProductsByIds(currentProductIds);
            }

            var currentProductList = currentProductIds.Select(x => ProductViewModelMap.ToViewModel(productList.FirstOrDefault(y => y.ProductId == int.Parse(x)))).ToList();

            currentProductList.ForEach(x =>
                {
                    x.Price = x.ProductAttributes.Attributes.Any() ? _skuAgent.GetSkusPrice(x.ProductId,
                                               x.ProductAttributes.Attributes.FirstOrDefault().ProductAttributes.FirstOrDefault().AttributeId,
                                               x.Price, out selectedSku, out imagePath, out imageMediumPath) : x.Price;
                                               x.Sku = selectedSku;
                                               //Znode Version 7.2.2
                                               //Populate Swatch Images - Start 
                                               x.SwatchImages = GetSwatchImages(x);
                                               //Populate Swatch Images - End 
                });

            // Price to be sorted from the DB
            if (!string.IsNullOrEmpty(sortBy) && sortBy.StartsWith("price|"))
            {
                // Price to be sorted from the DB as ASC or DESC
                currentProductList = sortBy.EndsWith("|desc") ? currentProductList.OrderBy(x => x.IsCallForPricing).ThenByDescending(x => x.Price).ToList() : currentProductList.OrderBy(x => x.IsCallForPricing).ThenBy(x => x.Price).ToList();

                currentProductList = currentProductList.Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
            }
           
            return currentProductList;
        }

        /// <summary>
        /// Get the product by IDs
        /// </summary>
        /// <param name="ids">Product IDs</param>
        /// <returns>Returns the list of product as product model</returns>
        public Collection<ProductModel> GetProductsByIds(List<string> ids)
        {
            var productIds = ids.Distinct().ToList();

            if (productIds.Count() == 1)
            {
                var singleProduct = _productClient.GetProduct(int.Parse(productIds.First()), expands);
                return new Collection<ProductModel>() { singleProduct };
            }

            IEnumerable<ProductModel> products = new Collection<ProductModel>();

            var page = 1;
            const int pagesize = 10;
            do
            {
                var currentProductIds = productIds.Skip((page - 1) * pagesize).Take(pagesize).ToList();

                if (currentProductIds.Count() == 1)
                {
					var singleProduct = _productClient.GetProduct(int.Parse(currentProductIds.First()), expands);
                    products = products.Union(new Collection<ProductModel>() { singleProduct });
                }
                else
                {
                    var idString = String.Join(",", currentProductIds);
					var list = _productClient.GetProductsByProductIds(idString, expands, new SortCollection());
					products = products.Union(list.Products);
                }

                page++;
            } while (page <= Math.Ceiling(productIds.Count() / (decimal)pagesize));

            return new Collection<ProductModel>(productIds.Select(x => products.FirstOrDefault(y => y.ProductId.ToString() == x)).ToList());
        }

        /// <summary>
        /// Get the current page product ids
        /// </summary>
        /// <param name="searchResult">Search Result</param>
        /// <param name="pageNumber">Page Number</param>
        /// <param name="pageSize">Page Size</param>
        /// <param name="sortBy">Sort By</param>
        /// <returns>Returns the list of string of product ids </returns>
        private List<string> GetCurrentPageProductIDs(KeywordSearchModel searchResult, int pageNumber, int pageSize, string sortBy)
        {
            // Do Paging
            if (pageNumber == 0 && pageSize == 0)
            {
                pageNumber = MvcDemoConstants.DefaultPageNumber;
                pageSize = MvcDemoConstants.DefaultPageSize;
            }

            // Pass -1 for Show ALL option
            if (pageSize == -1 || pageSize > searchResult.Products.Count)
            {
                pageSize = searchResult.Products.Count;
                pageNumber = MvcDemoConstants.DefaultPageNumber;
            }

            var currentProductIds = searchResult.Products.Skip((pageNumber - 1) * pageSize).Take(pageSize).Select(x => x.Id);

            // Price to be sorted from the DB
            if (!string.IsNullOrEmpty(sortBy) && sortBy.StartsWith("price|"))
            {
                currentProductIds = searchResult.Products.Select(x => x.Id);
            }

            return currentProductIds.ToList();
        }

        /// <summary>
        /// get the inner search list.
        /// </summary>
        /// <param name="innerSearchTerm">Inner Search Term</param>
        /// <returns>Returns the list of string for the inner search list</returns>
        private List<string> GetInnerSearchList(string innerSearchTerm)
        {
            var innerSearchList = new List<string>();

            if (!string.IsNullOrEmpty(innerSearchTerm))
            {
                var stringSeparators = new[] { "||" };
                innerSearchList = innerSearchTerm.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries).ToList();
            }

            return innerSearchList;
        }

        /// <summary>
        /// Get the Sort Collection
        /// </summary>
        /// <param name="sortBy">Sort By</param>
        /// <returns>Returns the sort collection object</returns>
        private SortCollection GetSortCollection(string sortBy)
        {
            var sortCollection = new SortCollection();

            if (!string.IsNullOrEmpty(sortBy))
            {
                sortCollection.Add(sortBy.Split('|')[0], sortBy.Split('|')[1]);
            }

            return sortCollection;
        }

        #region Znode Version 7.2.2

        //Populate Swatch Images - Start 
       
        /// <summary>
        /// Get All Swatch Images
        /// Filter All Product images to get the swatch iamges based on ProductImageTypeId.
        /// </summary>
        /// <param name="ProductViewModel">ProductViewModel</param>
        /// <returns>Returns swatch images collection object</returns>
        private List<ImageViewModel> GetSwatchImages(ProductViewModel model)
        {
            List<ImageViewModel> swatchModel = new List<ImageViewModel>();
            swatchModel = (model.Images != null && model.Images.Any()) ? model.Images.Where(y => y.ProductImageTypeId == 2 && y.ShowOnCategoryPage).OrderBy(y => y.DisplayOrder).ToList() : null;
            if (swatchModel != null && swatchModel.Any())
            {
                swatchModel.Add(new ImageViewModel { ImageAltTag = model.ImageAltTag, ImageMediumPath = model.ImageMediumPath, ImageSmallThumbnailPath = model.ImageSmallThumbnailPath });
            }
            return swatchModel;
        }
        #endregion
    }
}