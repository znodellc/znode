﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public interface ICheckoutAgent
    {
        void SaveShippingAddess(AddressViewModel model);
        BillingAddressViewModel SaveBillingAddess(AddressViewModel model, bool validModel, bool useShippingAddress = false);
        AddressViewModel GetShippingAddess();
        BillingAddressViewModel GetBillingAddress();
        AddressViewModel GetAddressByAddressID(AddressViewModel address);
        ReviewOrderViewModel GetCheckoutReview(int shippingOptions);
        PaymentsViewModel GetPaymentModel();
        PaymentsViewModel GetPaymentModel(PaymentOptionViewModel model);
        OrderViewModel SubmitOrder(PaymentOptionViewModel paymentInformation, string purchaseOrderNumber);
        CartViewModel ApplyGiftCard(string giftCard);
        OrderViewModel SubmitPayPalOrder(string token, string payerId, int paymentOptionId);
        List<AddressViewModel> GetAllAddesses();

        //Znode Version 7.2.2 Multi shipping
        List<OrderShipmentDataModel> SortMultipleShippingAddress(IEnumerable<OrderShipmentDataModel> multipleAddress);
        ReviewOrderListViewModel CreateMultipleShoppingCart(List<OrderShipmentDataModel> sortedAddress, IEnumerable<OrderShipmentDataModel> multipleAddress);
        void RecreateActualCart();
        ReviewOrderListViewModel GetMultiCartOrderReview();

        void CalculateMultiCartShipping(int rowIndex, int shippingOption);
        List<CartItemViewModel> GetMultipleShipingViewModel();

        void CalculateMultiCartShipping(int shippingOption);
    }
}