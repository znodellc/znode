﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;
using Znode.Engine.Api.Client;
using System.IO;

namespace Znode.Engine.MvcDemo.Agents
{
    public class CartAgent : BaseAgent, ICartAgent
    {
        private readonly IShoppingCartsClient _shoppingCartsClient;
        private readonly IPromotionsClient _promotionsClient;
        readonly ISkuAgent _skuAgent;
        private readonly IProductAgent _productAgent;
        /// <summary>
        /// Cart Agent Constructor
        /// </summary>
        public CartAgent()
        {
            _shoppingCartsClient = GetClient<ShoppingCartsClient>();
            _skuAgent = new SkuAgent();
            _productAgent = new ProductAgent();
            _promotionsClient = new PromotionsClient();
        }

        /// <summary>
        /// Check Inventory for Wishlist product
        /// </summary>
        /// <param name="productId">Proudct Id</param>
        /// <param name="quantity">Quantity</param>
        /// <returns>Returns the productview model with inventory message</returns>
        public ProductViewModel WishlistCheckInventory(int productId, int quantity)
        {
            if (!string.IsNullOrEmpty(productId.ToString()))
                // Get the product
                return _productAgent.GetProduct(productId);
            return null;
        }

        /// <summary>
        /// Create the shopping cart object and add cart items in the session and cookie.
        /// </summary>
        /// <param name="cartItem">Cart Item</param>
        /// <returns>Returns the shopping cart items as cart view model</returns>
        public CartViewModel Create(CartItemViewModel cartItem)
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey) ??
                       GetCartFromCookie() ?? new ShoppingCartModel();

          UpdateAccountInfo(cart);

            cart.ShoppingCartItems.Add(CartItemViewModelMap.ToModel(cartItem));            

            var shoppingCartModel = _shoppingCartsClient.CreateCart(cart);

            if (shoppingCartModel != null)
            {
                shoppingCartModel.Coupon = cart.Coupon;
                shoppingCartModel.GiftCardNumber = cart.GiftCardNumber;
                shoppingCartModel.GiftCardValid = cart.GiftCardValid;
                shoppingCartModel.GiftCardMessage = cart.GiftCardMessage;
                shoppingCartModel.GiftCardApplied = cart.GiftCardApplied;
                shoppingCartModel.GiftCardAmount = cart.GiftCardAmount;
                shoppingCartModel.GiftCardBalance = cart.GiftCardBalance;

                SaveInSession(MvcDemoConstants.CartModelSessionKey, shoppingCartModel);

                // if persistent cart disabled, we need not call below method, need to check with portal record.
                if ((shoppingCartModel.Account == null || shoppingCartModel.Account.AccountId == 0) && PortalAgent.CurrentPortal.PersistentCartEnabled)
                {                 
                    SaveInCookie(MvcDemoConstants.CartCookieKey, shoppingCartModel.CookieId.ToString());
                }

                return CartViewModelMap.ToViewModel(shoppingCartModel);
            }

            return new CartViewModel() {HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorAddProductToCart};
        }

        /// <summary>
        /// Get Cart method to check Session or Cookie to get the existing shopping cart.
        /// </summary>
        /// <returns>Returns the cart items as cart view Model</returns>
        public CartViewModel GetCart()
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey) ??
                       GetCartFromCookie();

            if (cart == null)
                return new CartViewModel()
                    {
                        HasError = true
                    };

            var cartModel = Calculate(cart);
          
            if (GetFromSession<bool>(MvcDemoConstants.CartMerged))
            {
                cartModel.ErrorMessage = Resources.ZnodeResource.SavedCartText;
                cartModel.HasError = true;
                SaveInSession(MvcDemoConstants.CartMerged, false);                
            }

            return cartModel;
        }

        /// <summary>
        /// Get Cart method to check Session or Cookie to get the existing shopping cart.
        /// </summary>
        /// <returns>Returns the cart items as cart view Model</returns>
        public int GetCartCount()
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey) ??
                       GetCartFromCookie();

            if (cart != null) return cart.ShoppingCartItems.Count;

            return 0;
        }


        private void UpdateAccountInfo(ShoppingCartModel cart)
        {
            if (cart == null)
            {
                return;
            }
            var account = GetFromSession<AccountViewModel>(MvcDemoConstants.AccountKey);

            if (account != null)
            {
                cart.Account = AccountViewModelMap.ToAccountModel(account);

            }
        }


        /// <summary>
        /// Get Cart method to check Session or Cookie to get the existing shopping cart.
        /// </summary>
        /// <returns>Returns the calculated cart items as cart view model</returns>
        public CartViewModel Calculate(ShoppingCartModel shoppingCartModel)
        {
            IProductAgent productAgent = new ProductAgent();
            var caluculatedModel = shoppingCartModel;

            UpdateAccountInfo(shoppingCartModel);
            if (!shoppingCartModel.ShoppingCartItems.Any(item => item.InsufficientQuantity))
                caluculatedModel = _shoppingCartsClient.Calculate(shoppingCartModel);

            if (caluculatedModel != null && caluculatedModel.ShoppingCartItems.Count > 0)
            {
                var ids = string.Join(",",caluculatedModel.ShoppingCartItems.Select(x => x.ProductId.ToString()));
                var productsViewModel = productAgent.GetProductByIds(ids);
                var cartViewModel = CartViewModelMap.ToViewModel(caluculatedModel);

                productsViewModel.ToList().ForEach(productModel =>
                {
                    var items = cartViewModel.Items.Where(itemModel => itemModel.ProductId == productModel.ProductId);

                    items.ToList().ForEach(itemModel => itemModel.Product = productModel);
                });


                //Znode Version 7.2.2 
                //Update SKU Image Path with Product Main Image in Case no image found -Start
                foreach (var data in cartViewModel.Items)
                {
                    if (Path.GetFileName(data.ImagePath).Equals(MvcDemoConstants.NoImageName))
                    {
                        data.ImagePath = data.Product.ImageLargePath;
                    }
                }
                //Update SKU Image Path with Product Main Image in Case no image found -Start

                // Set Account Id or Cookie Id.
                if (shoppingCartModel.Account == null || shoppingCartModel.Account.AccountId == 0)
                    caluculatedModel.CookieId = shoppingCartModel.CookieId;
                else
                    caluculatedModel.Account = shoppingCartModel.Account;

                if (caluculatedModel.Coupon != null && !caluculatedModel.CouponApplied)
                {
                    cartViewModel.HasError = true;
                }
                // Model into Session 
                SaveInSession(MvcDemoConstants.CartModelSessionKey, caluculatedModel);
                return cartViewModel;            
            }
          

            return new CartViewModel() { HasError = true };
        }

        /// <summary>
        /// Removes the items from the shopping cart.
        /// </summary>
        /// <param name="guid">GUID of the cart item</param>
        public bool RemoveItem(string guid)
        {
            // Get cart from Session.
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            if (cart == null || !cart.ShoppingCartItems.Any()) return false;

            // Check if item exists.
            var item = cart.ShoppingCartItems.FirstOrDefault(x => x.ExternalId == guid);
            if (item == null) return false;

            UpdateAccountInfo(cart);

            // Remove item and update the cart in Session and API.
            cart.ShoppingCartItems.Remove(item);

            var shoppingCartModel = _shoppingCartsClient.CreateCart(cart);

            if (shoppingCartModel != null)
            {
                shoppingCartModel.Coupon = cart.Coupon;
                shoppingCartModel.GiftCardNumber = cart.GiftCardNumber;
                shoppingCartModel.GiftCardValid = cart.GiftCardValid;
                shoppingCartModel.GiftCardMessage = cart.GiftCardMessage;
                shoppingCartModel.GiftCardApplied = cart.GiftCardApplied;
                shoppingCartModel.GiftCardAmount = cart.GiftCardAmount;
                shoppingCartModel.GiftCardBalance = cart.GiftCardBalance;
            }

            SaveInSession(MvcDemoConstants.CartModelSessionKey, shoppingCartModel);
	        return true;
        }

        /// <summary>
        /// Updates the Quantity updated in the shopping cart page.
        /// </summary>
        /// <param name="guid">GUID of the cart item</param>
        /// <param name="qty">Quantity Selected</param>
        public bool UpdateItem(string guid, int qty)
        {
            // Get cart from Session.
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            if (cart == null || !cart.ShoppingCartItems.Any()) return false;

            // Check if item exists.
            var item = cart.ShoppingCartItems.FirstOrDefault(x => x.ExternalId == guid);
            if (item == null) return false;
			
			//Check Inventory
            item.InsufficientQuantity = false;
            var skumodel = _skuAgent.GetBySku(item.ProductId, item.Sku);
            var product = _productAgent.GetProduct(item.ProductId);
            if (skumodel.Inventory.QuantityOnHand < qty &&
                (!product.AllowBackOrder.GetValueOrDefault() && product.TrackInventory.GetValueOrDefault()))
            {
                item.InsufficientQuantity = true;
                SaveInSession(MvcDemoConstants.CartModelSessionKey,cart);
                return false;
            }
            // update item and update the cart in Session and API.
            item.Quantity = qty;

            UpdateAccountInfo(cart);

            var shoppingCartModel = _shoppingCartsClient.CreateCart(cart);
            
            if (shoppingCartModel != null)
            {
                shoppingCartModel.Coupon = cart.Coupon;
                shoppingCartModel.GiftCardNumber = cart.GiftCardNumber;
                shoppingCartModel.GiftCardValid = cart.GiftCardValid;
                shoppingCartModel.GiftCardMessage = cart.GiftCardMessage;
                shoppingCartModel.GiftCardApplied = cart.GiftCardApplied;
                shoppingCartModel.GiftCardAmount = cart.GiftCardAmount;
                shoppingCartModel.GiftCardBalance = cart.GiftCardBalance;
            }

            SaveInSession(MvcDemoConstants.CartModelSessionKey, shoppingCartModel);
	        return true;
        }

        /// <summary>
        /// Merge the cart after login
        /// </summary>
        /// <returns></returns>
        public bool Merge()
        {
            bool returnStatus = false;

            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);

            var accountId = GetFromSession<AccountViewModel>(MvcDemoConstants.AccountKey).AccountId;
            var model = _shoppingCartsClient.GetCartByAccount(accountId);
                          
            if (model != null)
            {
                returnStatus = model.ShoppingCartItems.Any() && cart != null && cart.ShoppingCartItems.Any();
                if (cart != null)
                    cart.ShoppingCartItems.ToList().ForEach(model.ShoppingCartItems.Add);                
            }
            else
            {
                if (cart == null) return false;
                model = cart;                
            }

            if (PortalAgent.CurrentPortal.PersistentCartEnabled)
                model.Account = new AccountModel() { AccountId = accountId };

	        try
	        {
				model = _shoppingCartsClient.CreateCart(model);
                if (cart != null  && !string.IsNullOrEmpty(cart.Coupon))
                {
                    model.Coupon = cart.Coupon;
                    model.CouponApplied = cart.CouponApplied;
                    model.CouponValid = cart.CouponValid;
                    model.CouponMessage = cart.CouponMessage;
                }

	        }
	        catch (ZnodeException)
	        {
		        return false;
	        }

            if (PortalAgent.CurrentPortal.PersistentCartEnabled)
                model.Account = new AccountModel() { AccountId = accountId };

            if (returnStatus) SaveInSession(MvcDemoConstants.CartMerged, true);            
            SaveInSession(MvcDemoConstants.CartModelSessionKey, model);          

            RemoveCookie(MvcDemoConstants.CartCookieKey);

            return returnStatus;
        }

        /// <summary>
        /// Finds the couponCode is valid or invalid
        /// </summary>
        /// <param name="couponCode">CouponCode of the cart</param>
        /// <returns>Returns valid coupon or not</returns>
        public bool IsCouponValid(string couponCode)
        {
            var promotions = _promotionsClient.GetPromotions(null, null, null);

            if (promotions == null || !promotions.Promotions.Any())
            {
                return false;
            }

            var now = DateTime.Now;

            return promotions.Promotions.Any(model => model.CouponCode.Equals(couponCode) && model.StartDate <= now && model.EndDate >= now);
        }

        /// <summary>
        /// Adds the Coupon Code to the Cart
        /// </summary>
        /// <param name="couponCode">couponCode of the Cart</param>
        public CartViewModel ApplyCoupon(string couponCode)
        {
            var promoCode = string.Empty;

            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey) ??
                       GetCartFromCookie();

            if (cart == null)
                return new CartViewModel()
                {
                    HasError = true
                };

            if (cart.CouponApplied)
            {
                promoCode = cart.Coupon;
            }

            cart.Coupon = couponCode;
            var cartModel = Calculate(cart);

            if (!cartModel.CouponApplied)
            {
                var couponError = cartModel.CouponMessage;
                cart.Coupon = promoCode;
                cartModel = Calculate(cart);
                cartModel.HasError = true;
                cartModel.Coupon = couponCode;
                cartModel.CouponMessage = couponError;
            }

            return cartModel;
        }

        /// <summary>
        /// Set the Coupon Applied success and error message
        /// </summary>
        /// <param name="model">Cart View Model</param>
        /// <param name="success">Coupon applied or not</param>
        public void SetMessages(CartViewModel model)
        {
            SetStatusMessage(model);
        }

        /// <summary>
        /// Get the cart items based on the cookie mapping id
        /// </summary>
        /// <returns>Returns the shopping cart items Shopping Cart Model</returns>
        private ShoppingCartModel GetCartFromCookie()
        {
            if (!PortalAgent.CurrentPortal.PersistentCartEnabled)
                return null;

            var cookieValue = GetFromCookie(MvcDemoConstants.CartCookieKey);
            if (!string.IsNullOrEmpty(cookieValue))
            {
                int cookieId;
                if (int.TryParse(cookieValue, out cookieId))
                {
                    try
                    {
                        var shoppingCartModel = _shoppingCartsClient.GetCartByCookie(cookieId);

                        if (shoppingCartModel != null)
                        {
                            return shoppingCartModel;
                        }
                    }
                    catch (Exception)
                    {

                    }

                    return null;
                }
            }

            return null;
        }
        #region Znode version 7.2.2
        /// <summary>
        /// Znode version 7.2.2 
        /// This function will Removes all item of shopping cart from Session & Cookie.
        /// </summary>
        /// <returns>Returns true or false.</returns>
        public bool RemoveAllCartItems()
        {
            RemoveCookie(MvcDemoConstants.CartCookieKey);
            var cart = new ShoppingCartModel();
            UpdateAccountInfo(cart);
            var shoppingCartModel = _shoppingCartsClient.CreateCart(cart);
            SaveInSession(MvcDemoConstants.CartModelSessionKey, new ShoppingCartModel());
            return true;
        }
        #endregion
    }
}