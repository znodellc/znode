﻿using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public interface ICartAgent
    {
        CartViewModel Create(CartItemViewModel cartItem);                
        CartViewModel GetCart();
        int GetCartCount();
        CartViewModel Calculate(ShoppingCartModel shoppingCartModel);
        bool RemoveItem(string guid);
        bool UpdateItem(string guid, int qty);
        bool Merge();
        bool IsCouponValid(string couponCode);
        CartViewModel ApplyCoupon(string couponCode);
        void SetMessages(CartViewModel model);
        ProductViewModel WishlistCheckInventory(int productId, int quantity);
        /// <summary>
        /// Znode version 7.2.2 
        /// This function will Removes all item from shopping cart.
        /// </summary>
        /// <returns>Returns true or false.</returns>
        bool RemoveAllCartItems();
    }
}