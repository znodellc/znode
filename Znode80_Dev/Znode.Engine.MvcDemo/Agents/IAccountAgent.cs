﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public interface IAccountAgent
    {
        RegisterViewModel GetRegisterViewModel();
        RegisterViewModel SignUp(RegisterViewModel model);
        LoginViewModel Login(LoginViewModel model);
        AccountViewModel GetAccountViewModel();
        AccountViewModel GetDashboard();
        Collection<StateModel> GetStates();
        void UpdateAccountViewModel(object model);
        int GetWishlistByProductId(int productId);
        WishListViewModel GetWishList();
        WishListViewModel CreateWishList(int productId);
        bool DeleteWishList(int wishlistId);
        int CheckWishListExists(int productId, int accountId);
        OrdersViewModel GetOrderReceiptDetails(int orderId);
        AddressViewModel GetAddress(int addressId);
        void SetAddressLink(AccountViewModel model);
        AddressViewModel UpdateAddress(AddressViewModel address);
        AddressViewModel UpdateAddress(int addressId, bool isDefaultBillingAddress);
        AddressViewModel DeleteAddress(int addressId);
        ReviewViewModel GetReviews();
        AccountViewModel UpdateProfile(AccountViewModel model);
        ChangePasswordViewModel ChangePassword(ChangePasswordViewModel model);
        AccountViewModel ForgotPassword(AccountViewModel model);
        void SetMessages(AccountViewModel model);
        void Logout();
        GiftCardViewModel GetGiftCardBalance(string giftcard);

        #region Znode Version 7.2.2

        List<CartItemViewModel> GetReorderItems(int orderId);
        CartItemViewModel GetReorderSingleItems(int orderLineItemId);

        //TODO
        List<CartItemViewModel> GetReorderItemsList(int orderId, int orderLineItemId, bool isOrder);

        /// <summary>      
        /// This function will check the Reset Password Link current status.
        /// </summary>
        /// <param name="model">ChangePasswordViewModel</param>
        /// <returns>Returns Status of Reset Password Link.</returns>
        ResetPasswordStatusTypes CheckResetPasswordLinkStatus(ChangePasswordViewModel model);

        /// <summary>
        /// This function is used to get all countries list
        /// </summary>
        /// <returns>returns collection of countries</returns>
        Collection<CountryModel> GetCountries();

        /// <summary>
        /// This function is used to get all state list by countryCode
        /// </summary>
        /// <param name="countryCode">string countryCode</param>
        /// <returns>list of state</returns>
        IEnumerable<StateModel> GetStateByCountryCode(string countryCode);


        #endregion


    }
}
