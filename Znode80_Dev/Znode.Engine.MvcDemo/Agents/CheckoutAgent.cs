﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Web;
using WebGrease.Css.Extensions;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.Exceptions;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public class CheckoutAgent : BaseAgent, ICheckoutAgent
    {
        private readonly IShippingOptionAgent _shippingOptionAgent;
        private readonly ICartAgent _cartAgent;
        private readonly IAccountAgent _accountAgent;
        private readonly IPaymentOptionsClient _paymentOptionsClient;
        private readonly IOrdersClient _orderClient;
        private readonly IAccountsClient _accountClient;
        private readonly IAddressesClient _addressClient;

        /// <summary>
        /// Checkout Agent Constructor
        /// </summary>
        public CheckoutAgent()
        {
            _shippingOptionAgent = GetClient<ShippingOptionAgent>();
            _cartAgent = new CartAgent();
            _accountAgent = new AccountAgent();
            _paymentOptionsClient = GetClient<PaymentOptionsClient>();
            _orderClient = GetClient<OrdersClient>();
            _accountClient = GetClient<AccountsClient>();
            _addressClient = GetClient<AddressesClient>();
        }

        /// <summary>
        /// Save the Shipping Address in the session and assign it to the cart object
        /// </summary>
        /// <param name="model">Address View Model</param>
        public void SaveShippingAddess(AddressViewModel model)
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            cart.ShippingAddress = AddressViewModelMap.ToModel(model);
            SaveInSession(MvcDemoConstants.CartModelSessionKey, cart);
            SaveInSession(MvcDemoConstants.ShippingAddressKey, model);
        }

        /// <summary>
        /// Save the Billing Address in the session and assign it to the cart object
        /// </summary>
        /// <param name="model">Address View Model</param>
        /// <param name="useShippingAddress">Flag to say Use Same Shipping Address</param>
        public BillingAddressViewModel SaveBillingAddess(AddressViewModel model, bool validModel, bool useShippingAddress = false)
        {
            if (!useShippingAddress || string.IsNullOrEmpty(model.Email))
            {
                if (!validModel || string.IsNullOrEmpty(model.Email))
                {
                    return new BillingAddressViewModel()
                    {
                        BillingAddress = model,
                        ShippingAddress = GetShippingAddess(),
                        UseSameAsShippingAddress = useShippingAddress,
                        HasError = true
                    };
                }
            }

            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            cart.Payment = new PaymentModel();
            if (useShippingAddress)
            {
                var billAddress = GetShippingAddess();
                billAddress.Email = model.Email;
                billAddress.UseSameAsShippingAddress = true;
                billAddress.IsDefaultBilling = true;
                model = billAddress;
            }

            cart.Payment.BillingAddress = AddressViewModelMap.ToModel(model);
            SaveInSession(MvcDemoConstants.CartModelSessionKey, cart);
            SaveInSession(MvcDemoConstants.BillingAddressKey, model);

            return new BillingAddressViewModel() { HasError = false };
        }

        /// <summary>
        /// Get the shipping address from session
        /// </summary>
        /// <returns>Returns the shipping address as address view model</returns>
        public AddressViewModel GetShippingAddess()
        {
            var model = GetFromSession<AddressViewModel>(MvcDemoConstants.ShippingAddressKey);
            if (model == null)
            {
                var accountModel = _accountAgent.GetAccountViewModel();
                if (accountModel != null)
                    model = accountModel.Addresses.FirstOrDefault(x => x.IsDefaultShipping);
            }

            return model;
        }



        /// <summary>
        /// Get the billing address from session
        /// </summary>
        /// <returns>Returns the billing address as address view model</returns>
        public AddressViewModel GetBillingAddessFromSession()
        {
            var model = GetFromSession<AddressViewModel>(MvcDemoConstants.BillingAddressKey);
            if (model == null)
            {
                var accountModel = _accountAgent.GetAccountViewModel();
                if (accountModel != null)
                {
                    if (accountModel.Addresses.Any())
                    {
                        model = accountModel.Addresses.FirstOrDefault(x => x.IsDefaultBilling);
                    }
                    else
                    {
                        model = new AddressViewModel();
                    }
                    model.Email = accountModel.EmailAddress;
                }
            }

            return model;
        }

        /// <summary>
        /// Get the billing and shipping address from the session.
        /// </summary>
        /// <returns>Returns the address as Billing address view model</returns>
        public BillingAddressViewModel GetBillingAddress()
        {
            var viewModel = new BillingAddressViewModel()
            {
                BillingAddress = GetBillingAddessFromSession() ?? new AddressViewModel(),
                ShippingAddress = GetShippingAddess(),
            };

            return viewModel;
        }


        public AddressViewModel GetAddressByAddressID(AddressViewModel address)
        {
            var accountViewModel = GetFromSession<AccountViewModel>(MvcDemoConstants.AccountKey);

            if (accountViewModel != null && accountViewModel.Addresses.Any())
            {
                var addressViewModel = accountViewModel.Addresses.FirstOrDefault(x => x.AddressId == address.AddressId);

                if (addressViewModel != null)
                {
                    addressViewModel.Email = accountViewModel.EmailAddress;
                }

                return addressViewModel; ;
            }

            return new AddressViewModel();
        }

        /// <summary>
        ///  Sets the Selected Shipping Options and show the confirmation page details for the cart.
        /// </summary>
        /// <param name="shippingOptions">Shipping Options Selected</param>
        /// <returns>Return the cart items, address as review order view model</returns>
        public ReviewOrderViewModel GetCheckoutReview(int shippingOptions)
        {
            var selectedShippingOption = _shippingOptionAgent.GetShippingOption(shippingOptions);
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            cart.Shipping = new ShippingModel
            {
                ShippingOptionId = selectedShippingOption.ShippingId,
                ShippingName = selectedShippingOption.ShippingDescription
            };

            cart.ShippingAddress = AddressViewModelMap.ToModel(GetShippingAddess());
            cart.ShoppingCartItems.ForEach(item => item.InsufficientQuantity = false);
            SaveInSession(MvcDemoConstants.CartModelSessionKey, cart);
            var cartViewModel = _cartAgent.Calculate(cart);

            return new ReviewOrderViewModel()
            {
                ShippingAddress = GetShippingAddess(),
                ShoppingCart = cartViewModel,
                ShippingOption = selectedShippingOption,
            };
        }

        /// <summary>
        /// Gets the payment options to be displayed and the cart items
        /// </summary>        
        /// <returns>returns the payment types as payments view model</returns>
        public PaymentsViewModel GetPaymentModel()
        {
            // Get All profile payment options.
            var allList = _paymentOptionsClient.GetPaymentOptions(
                new ExpandCollection() { ExpandKeys.PaymentType, ExpandKeys.PaymentGateway },
                new FilterCollection()
                    {
                        new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, "null"),
                        new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, "true")
                    },
                new SortCollection());


            var accountModel = _accountAgent.GetAccountViewModel();

            // Get Account ProfileID, or Get Default Anonymous ProfileId from Portal
            int? profileId = null;
            profileId = accountModel != null ? accountModel.ProfileId : PortalAgent.CurrentPortal.DefaultAnonymousProfileId;

            // Get Profile based options and merge with All Profile options.
            if (profileId.HasValue)
            {
                var profileList = _paymentOptionsClient.GetPaymentOptions(
                    new ExpandCollection() { ExpandKeys.PaymentType, ExpandKeys.PaymentGateway },
                    new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, profileId.ToString()),
                            new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, "true")
                        },
                    new SortCollection());
                if (profileList.PaymentOptions != null && profileList.PaymentOptions.Any())
                {
                    allList.PaymentOptions =
                        new Collection<PaymentOptionModel>(
                            allList.PaymentOptions.Union(profileList.PaymentOptions).OrderBy(x => x.DisplayOrder).ToList());
                }
            }

            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            var billingAddress = cart.Payment.BillingAddress;
            if (billingAddress.AddressId.Equals(0))
            {
                billingAddress = AddressViewModelMap.ToModel(GetBillingAddessFromSession());
            }

            var billAddress = AddressViewModelMap.ToModel(billingAddress);
            var shipAddress = GetShippingAddess();

            cart.ShippingAddress = AddressViewModelMap.ToModel(shipAddress);

            // set the actionname for change address.
            // Znode 7.2.2 
            // start
            billAddress.SourceLink = MvcDemoConstants.RouteGuestCheckoutShipping;
            // end
            shipAddress.SourceLink = MvcDemoConstants.RouteGuestCheckoutShipping;

            return PaymentsViewModelMap.ToPaymentsViewModel(billAddress, shipAddress, cart, _cartAgent.Calculate(cart),
                                                           allList.PaymentOptions);
        }

        public PaymentsViewModel GetPaymentModel(PaymentOptionViewModel model)
        {
            var paymentModel = GetPaymentModel();
            var paymentType =
                paymentModel.PaymentOptions.FirstOrDefault(x => x.PaymentTypeId == (int)EnumPaymentType.CreditCard);
            if (paymentType != null) paymentType.CreditCardModel = model.CreditCardModel;
            return paymentModel;
        }

        /// <summary>
        /// Process the order
        /// </summary>
        /// <param name="paymentInformation">Payment Details</param>
        /// <param name="purchaseOrderNumber">Purchase Order Number</param>
        /// <param name="paymentInformationViewModel">PaymentInformationViewModel</param>
        /// <returns>Returns if order is placed or not</returns>
        public OrderViewModel SubmitOrder(PaymentOptionViewModel paymentInformationViewModel, string purchaseOrderNumber)
        {
            //Znode Version 7.2.2 
            //Check order is multiple shipping cart
            var _multiCart = GetMultiCartOrderReview();
            if (_multiCart != null)
            {
                if (_multiCart.ReviewOrderList.Count > 0)
                {
                    return SubmitMutiCartOrder(paymentInformationViewModel, purchaseOrderNumber);
                }
            }
            //Znode Version 7.2.2 

            var shoppingCart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            var loginAccount = _accountAgent.GetAccountViewModel() ?? new AccountViewModel();
            var paymentInformationModel = PaymentsViewModelMap.ToViewModel(paymentInformationViewModel);
            var account = AccountViewModelMap.ToAccountModel(loginAccount);

            if (account.AccountId == 0)
            {
                try
                {
                    account = _accountClient.CreateAccount(account);
                }
                catch (ZnodeException ex)
                {
                    return new OrderViewModel() { HasError = true, ErrorMessage = ex.ErrorMessage };
                }
            }

            var shippingAddress = GetShippingAddess();

            if (shippingAddress == null)
            {
                return null;
            }

            UpdateAddress(account, AddressViewModelMap.ToModel(shippingAddress));

            if (GetBillingAddessFromSession() != null)
            {
                if (!GetBillingAddessFromSession().UseSameAsShippingAddress)
                {
                    UpdateAddress(account, AddressViewModelMap.ToModel(GetBillingAddessFromSession()));
                }
                account.Email = GetBillingAddessFromSession().Email;
            }

            if (paymentInformationModel.PaymentOption.PaymentTypeId == (int)EnumPaymentType.PaypalExpressCheckout)
            {
                string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;

                shoppingCart.ReturnUrl = baseUrl + MvcDemoConstants.RouteCheckOutPaypal + "?PaymentOptionId=" +
                                         paymentInformationModel.PaymentOption.PaymentOptionId;
                shoppingCart.CancelUrl = baseUrl + MvcDemoConstants.RouteShoppingCart;
            }

            shoppingCart.Payment = paymentInformationModel;
            shoppingCart.Payment.BillingAddress =
                AddressViewModelMap.ToModel(GetBillingAddessFromSession() ?? shippingAddress);
            shoppingCart.ShippingAddress = AddressViewModelMap.ToModel(shippingAddress);

            var checkout = CartViewModelMap.ToShoppingCartModel(account, shoppingCart, purchaseOrderNumber);

            OrderModel order = null;

            try
            {
                order = _orderClient.CreateOrder(checkout);

                if (order.IsOffsitePayment)
                {
                    HttpContext.Current.Response.Redirect(order.ReceiptHtml);
                    return new OrderViewModel() { HasError = true };
                }
            }
            catch (ZnodeException exception)
            {
                return new OrderViewModel() { HasError = true, ErrorMessage = exception.ErrorMessage };
            }

            // reset the cart
            RemoveCookie(MvcDemoConstants.CartCookieKey);
            SaveInSession(MvcDemoConstants.CartModelSessionKey, new ShoppingCartModel());
            // Reset the address.
            RemoveInSession(MvcDemoConstants.BillingAddressKey);
            RemoveInSession(MvcDemoConstants.ShippingAddressKey);

            _accountAgent.UpdateAccountViewModel(AccountViewModelMap.ToOrderViewModel(order));

            return new OrderViewModel { EmailId = account.Email, OrderId = order.OrderId };

        }

        /// <summary>
        /// Update the address
        /// </summary>
        /// <param name="account">Account Model/param>
        /// <param name="address">Address Model</param>
        private void UpdateAddress(AccountModel account, AddressModel address)
        {
            var existingAddress = account.Addresses.FirstOrDefault(x => x.AddressId == address.AddressId);

            if (existingAddress != null)
                account.Addresses.Remove(existingAddress);

            account.Addresses.Add(address);

            if (address.AddressId == 0)
            {
                address.AccountId = account.AccountId;
                address = _addressClient.CreateAddress(address);
            }
            else
                _addressClient.UpdateAddress(address.AddressId, address);

            _accountAgent.UpdateAccountViewModel(AddressViewModelMap.ToModel(address));
        }

        /// <summary>
        /// Submit the paypal after success from Paypal
        /// </summary>
        /// <param name="token"></param>
        /// <param name="payerId"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        public OrderViewModel SubmitPayPalOrder(string token, string payerId, int paymentOptionId)
        {
            var shoppingCart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);

            shoppingCart.Token = token;
            shoppingCart.Payerid = payerId;

            var payments = GetPaymentModel();

            var paymentOptionViewModel = payments.PaymentOptions.FirstOrDefault(x => x.PaymentOptionId == paymentOptionId);

            SaveInSession(MvcDemoConstants.CartModelSessionKey, shoppingCart);

            return SubmitOrder(paymentOptionViewModel, string.Empty);
        }

        /// <summary>
        /// Apply GiftCard method
        /// </summary>
        /// <param name="giftCard">Giftcard number</param>
        /// <returns>Returns the updated CartViewModel</returns>
        public CartViewModel ApplyGiftCard(string giftCard)
        {
            var shoppingCart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            shoppingCart.GiftCardNumber = giftCard;

            var cartModel = _cartAgent.Calculate(shoppingCart);

            if (cartModel.GiftCardApplied)
            {
                cartModel.SuccessMessage = Resources.ZnodeResource.ValidGiftCard;
            }
            else
            {
                cartModel.HasError = true;
                cartModel.ErrorMessage = Resources.ZnodeResource.ErrorGiftCard;
            }
            return cartModel;
        }


        #region Znode Version 7.2.2

        /// <summary>
        /// Get the All address from session
        /// </summary>
        /// <returns>Returns the address list as address view model</returns>
        public List<AddressViewModel> GetAllAddesses()
        {
            List<AddressViewModel> addressList = new List<AddressViewModel>();
            var model = GetFromSession<AddressViewModel>(MvcDemoConstants.ShippingAddressKey);

            if (model != null)
                addressList.Add(model);

            var accountModel = _accountAgent.GetAccountViewModel();
            if (accountModel != null)
            {
                foreach (var adr in accountModel.Addresses)
                {
                    if (!addressList.Exists(element => element.AddressId.Equals(adr.AddressId)))
                    {
                        addressList.Add(adr);
                    }

                }
            }

            return addressList;
        }

        /// <summary>
        /// Get cart item list for multiple shipping
        /// </summary>
        /// <returns>returns lost of CartItemViewModel</returns>
        public List<CartItemViewModel> GetMultipleShipingViewModel()
        {
            var cartViewModel = _cartAgent.GetCart();
            var shoppingCart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            List<OrderShipmentModel> _MultipleShipToAddress = new List<OrderShipmentModel>();
            shoppingCart.ShoppingCartItems.ForEach(cartitems =>
            {
                _MultipleShipToAddress = cartitems.MultipleShipToAddress;
            });
            var shippingAddress = GetAllAddesses();

            List<CartItemViewModel> cartitemList = new List<CartItemViewModel>();

            cartViewModel.Items.ToList().ForEach(item =>
            {
                int count = item.Quantity;
                for (int i = 0; i < count; i++)
                {
                    CartItemViewModel objCartItem = new CartItemViewModel();
                    item.Quantity = 1;
                    objCartItem = item.Clone() as CartItemViewModel;
                    objCartItem.Addresses = shippingAddress;
                    cartitemList.Add(objCartItem);
                }
            });

            return cartitemList;
        }

        /// <summary>
        /// Get group by multiple shipping address collection.
        /// </summary>
        /// <param name="multipleAddress">collection of OrderShipmentDataModel</param>
        /// <returns>returns list of OrderShipmentDataModel</returns>
        public List<OrderShipmentDataModel> SortMultipleShippingAddress(IEnumerable<OrderShipmentDataModel> multipleAddress)
        {
            //Filter address
            List<OrderShipmentDataModel> multiAddressList = new List<OrderShipmentDataModel>();
            multipleAddress.ForEach(addressItem =>
            {
                if (!multiAddressList.Exists(element => element.addressid.Equals(addressItem.addressid)))
                {
                    multiAddressList.Add(addressItem);
                }
            });

            return multiAddressList;
        }

        /// <summary>
        /// create multi shipping cart.
        /// </summary>
        /// <param name="sortedAddress">selected shipping addresses</param>
        /// <param name="multipleAddress">collection of shipping addresses</param>
        /// <returns>returns ReviewOrderListViewModel</returns>
        public ReviewOrderListViewModel CreateMultipleShoppingCart(List<OrderShipmentDataModel> sortedAddress, IEnumerable<OrderShipmentDataModel> multipleAddress)
        {

            if (sortedAddress.Count.Equals(1))
            {
                var shippingaddress = GetAddressByAddressID(new AddressViewModel { AddressId = int.Parse(sortedAddress[0].addressid) });
                SaveShippingAddess(shippingaddress);
                return null;
            }
            else
            {
                var cartViewModel = _cartAgent.GetCart();
                AddressViewModel ShippingAddress = new AddressViewModel();
                ReviewOrderListViewModel reviewOrder = new ReviewOrderListViewModel();
                int rowIndex = 0;

                sortedAddress.ForEach(adr =>
                {
                    //Empty crat
                    _cartAgent.RemoveAllCartItems();

                    multipleAddress.ForEach(product =>
                    {
                        if (adr.addressid.Equals(product.addressid))
                        {
                            int productid = int.Parse(product.productid);

                            //Create New cart
                            CartItemViewModel _Item = new CartItemViewModel();
                            _Item = cartViewModel.Items.FirstOrDefault(x => x.ProductId == Convert.ToInt32(productid));
                            _Item.Quantity = 1;
                            ShippingAddress.AddressId = Convert.ToInt32(product.addressid);
                            _Item.Addresses.Add(GetAddressByAddressID(ShippingAddress));

                            var cartmodel = _cartAgent.Create(_Item);

                        }
                    });

                    //Create Review arder list for multiple shipping
                    ReviewOrderViewModel _reviewOrderModel = new ReviewOrderViewModel();
                    _reviewOrderModel = GetCheckoutReview(9);
                    _reviewOrderModel.ShippingAddress = GetAddressByAddressID(ShippingAddress);
                    _reviewOrderModel.ShippingOptionList = _shippingOptionAgent.GetShippingList();
                    _reviewOrderModel.OrderViewModelIndex = rowIndex;
                    reviewOrder.ReviewOrderList.Add(_reviewOrderModel);
                    rowIndex++;
                });

                SaveInSession(MvcDemoConstants.MultiCartModelSessionKey, reviewOrder);

                RecreateActualCart();

                return reviewOrder;
            }
        }

        /// <summary>
        /// Recreate actual cart.
        /// </summary>
        public void RecreateActualCart()
        {
            _cartAgent.RemoveAllCartItems();

            var orderList = GetFromSession<ReviewOrderListViewModel>(MvcDemoConstants.MultiCartModelSessionKey);

            orderList.ReviewOrderList.ForEach(ReviewOrder =>
            {
                ReviewOrder.ShoppingCart.Items.ForEach(item =>
                {
                    _cartAgent.Create(item);
                });
            });

        }

        /// <summary>
        /// get multi shipping cart from session
        /// </summary>
        /// <returns></returns>
        public ReviewOrderListViewModel GetMultiCartOrderReview()
        {
            return GetFromSession<ReviewOrderListViewModel>(MvcDemoConstants.MultiCartModelSessionKey);
        }

        /// <summary>
        /// get the calculated multi shipping cart
        /// </summary>
        /// <param name="rowIndex">index of collection</param>
        /// <param name="shippingOption">shipping option id</param>
        public void CalculateMultiCartShipping(int rowIndex, int shippingOption)
        {
            var reviewModel = GetMultiCartOrderReview();
            var _model = reviewModel.ReviewOrderList.FirstOrDefault(x => x.OrderViewModelIndex.Equals(rowIndex));

            reviewModel.ReviewOrderList.Remove(_model);
            //Empty crat
            _cartAgent.RemoveAllCartItems();

            //Create New cart
            _model.ShoppingCart.Items.ForEach(itm =>
            {
                itm.Addresses.Add(GetAddressByAddressID(_model.ShippingAddress));
                var cartmodel = _cartAgent.Create(itm);
            });

            //Create Review arder list for multiple shipping
            _model = GetCheckoutReview(shippingOption);
            _model.ShippingAddress = GetAddressByAddressID(_model.ShippingAddress);
            _model.ShippingOptionList = _shippingOptionAgent.GetShippingList();
            _model.OrderViewModelIndex = rowIndex;
            reviewModel.ReviewOrderList.Insert(rowIndex, _model);

            SaveInSession(MvcDemoConstants.MultiCartModelSessionKey, reviewModel);

            RecreateActualCart();

        }

        /// <summary>
        /// get calculated multi cart shipping 
        /// </summary>
        /// <param name="shippingOption">shipping option id</param>
        public void CalculateMultiCartShipping(int shippingOption)
        {
            var reviewModel = GetMultiCartOrderReview();
            var DefaultShipAddresmodel = GetFromSession<AddressViewModel>(MvcDemoConstants.ShippingAddressKey);
            ReviewOrderListViewModel _modelList = new ReviewOrderListViewModel();
            reviewModel.ReviewOrderList.ForEach(_model =>
            {
                //Empty crat
                _cartAgent.RemoveAllCartItems();

                //Create New cart
                _model.ShoppingCart.Items.ForEach(itm =>
                {
                    itm.Addresses.Add(GetAddressByAddressID(_model.ShippingAddress));
                    var cartmodel = _cartAgent.Create(itm);
                });

                //Create Review arder list for multiple shipping
                SaveInSession(MvcDemoConstants.ShippingAddressKey, GetAddressByAddressID(_model.ShippingAddress));

                _model = GetCheckoutReview(shippingOption);
                _model.ShippingOptionList = _shippingOptionAgent.GetShippingList();
                _modelList.ReviewOrderList.Add(_model);
            });

            SaveInSession(MvcDemoConstants.ShippingAddressKey, DefaultShipAddresmodel);
            SaveInSession(MvcDemoConstants.MultiCartModelSessionKey, _modelList);

            RecreateActualCart();

        }

        /// <summary>
        /// Process the multi shipping order
        /// </summary>
        /// <param name="paymentInformation">Payment Details</param>
        /// <param name="purchaseOrderNumber">Purchase Order Number</param>
        /// <param name="paymentInformationViewModel">PaymentInformationViewModel</param>
        /// <returns>Returns if order is placed or not</returns>
        public OrderViewModel SubmitMutiCartOrder(PaymentOptionViewModel paymentInformationViewModel, string purchaseOrderNumber)
        {
            //Multiple shipping cart
            var model = GetMultiCartOrderReview();

            ShoppingCartModel shoppingCart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            shoppingCart.MultipleShipToEnabled = true;

            shoppingCart.OrderShipment = (List<OrderShipmentDataModel>)HttpContext.Current.Session["MultipleOrderShipment"];

            model.ReviewOrderList.ForEach(cart =>
            {
                shoppingCart.Total += (decimal)cart.ShoppingCart.Total;
                shoppingCart.ShippingCost += (decimal)cart.ShoppingCart.ShippingCost;
                shoppingCart.Discount += (decimal)cart.ShoppingCart.Discount;
                shoppingCart.GiftCardAmount += (decimal)cart.ShoppingCart.GiftCardAmount;
                shoppingCart.SubTotal += (decimal)cart.ShoppingCart.SubTotal;
                shoppingCart.TaxCost += (decimal)cart.ShoppingCart.TaxCost;
                shoppingCart.TaxRate += (decimal)cart.ShoppingCart.TaxRate;

                shoppingCart.OrderShipment.ForEach(_ordershipment =>
                {
                    if (int.Parse(_ordershipment.addressid).Equals(cart.ShippingAddress.AddressId))
                    {
                        _ordershipment.ShippingId = cart.ShippingOption.ShippingId;
                        _ordershipment.ShippingCode = cart.ShippingOption.ShippingCode;
                        _ordershipment.ShippingDescription = cart.ShippingOption.ShippingDescription;
                    }
                });

            });


            SaveInSession(MvcDemoConstants.CartModelSessionKey, shoppingCart);
            var loginAccount = _accountAgent.GetAccountViewModel() ?? new AccountViewModel();
            var paymentInformationModel = PaymentsViewModelMap.ToViewModel(paymentInformationViewModel);
            var account = AccountViewModelMap.ToAccountModel(loginAccount);

            if (account.AccountId == 0)
            {
                try
                {
                    account = _accountClient.CreateAccount(account);
                }
                catch (ZnodeException ex)
                {
                    return new OrderViewModel() { HasError = true, ErrorMessage = ex.ErrorMessage };
                }
            }

            var shippingAddress = GetShippingAddess();

            if (shippingAddress == null)
            {
                return null;
            }

            UpdateAddress(account, AddressViewModelMap.ToModel(shippingAddress));

            if (GetBillingAddessFromSession() != null)
            {
                if (!GetBillingAddessFromSession().UseSameAsShippingAddress)
                {
                    UpdateAddress(account, AddressViewModelMap.ToModel(GetBillingAddessFromSession()));
                }
                account.Email = GetBillingAddessFromSession().Email;
            }

            if (paymentInformationModel.PaymentOption.PaymentTypeId == (int)EnumPaymentType.PaypalExpressCheckout)
            {
                string baseUrl = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + HttpContext.Current.Request.ApplicationPath;

                shoppingCart.ReturnUrl = baseUrl + MvcDemoConstants.RouteCheckOutPaypal + "?PaymentOptionId=" +
                                         paymentInformationModel.PaymentOption.PaymentOptionId;
                shoppingCart.CancelUrl = baseUrl + MvcDemoConstants.RouteShoppingCart;
            }

            shoppingCart.Payment = paymentInformationModel;
            shoppingCart.Payment.BillingAddress =
                AddressViewModelMap.ToModel(GetBillingAddessFromSession() ?? shippingAddress);

            var checkout = CartViewModelMap.ToShoppingCartModel(account, shoppingCart, purchaseOrderNumber);

            OrderModel order = null;

            try
            {
                checkout.ShippingAddress = null;

                order = _orderClient.CreateOrder(checkout);

                if (order.IsOffsitePayment)
                {
                    HttpContext.Current.Response.Redirect(order.ReceiptHtml);
                    return new OrderViewModel() { HasError = true };
                }

            }
            catch (ZnodeException exception)
            {
                return new OrderViewModel() { HasError = true, ErrorMessage = exception.ErrorMessage };
            }

            // reset the cart
            RemoveCookie(MvcDemoConstants.CartCookieKey);
            SaveInSession(MvcDemoConstants.CartModelSessionKey, new ShoppingCartModel());

            // Reset the address.
            RemoveInSession(MvcDemoConstants.BillingAddressKey);
            RemoveInSession(MvcDemoConstants.ShippingAddressKey);
            RemoveInSession(MvcDemoConstants.MultiCartModelSessionKey);

            _accountAgent.UpdateAccountViewModel(AccountViewModelMap.ToOrderViewModel(order));

            return new OrderViewModel { EmailId = account.Email, OrderId = order.OrderId };

        }


        #endregion
    }
}