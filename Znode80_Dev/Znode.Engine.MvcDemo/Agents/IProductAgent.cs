﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public interface IProductAgent
    {
        ProductViewModel GetProduct(int productId);
        ProductAttributesViewModel UpdateProductAttributes(ProductViewModel product, int? attributeId, string selectedAttributeIds);
        IEnumerable<ProductViewModel> GetProductByIds(string ids);
        decimal GetSkuPrice(int attributeId, int productId);
        decimal GetExtendedPrice(int productId, int quantity, string sku, decimal prodPrice);
        void CheckInventory(ProductViewModel model, string attributeId, int? quantity);
        int GetOrderedItemQuantity(string sku);
        //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
        ProductViewModel GetAttributes(int? id, int? attributeId, string selectedIds, int? quantity, out decimal prodPrice, out string selectedSku, out string imagePath, out string imageMediumPath);
        Collection<ProductViewModel> GetHomeSpecials();
        BundleDetailsViewModel GetBundleProductsDetails(CartItemViewModel cartItemViewModel);
        List<BundleDisplayViewModel> GetBundles(string bundleProductIds);
        WishListViewModel CreateWishList(int productId);
        IEnumerable<AddOnViewModel> GetAddOnDetails(ProductViewModel product, string selectedAddOnIds, int? quantity, decimal productPrice, out decimal finalProductPrice);
        void SetMessages(ProductViewModel model);
        ReviewItemViewModel CreateReview(ReviewItemViewModel model);
        ReviewViewModel GetAllReviews(int productId);

        BundleDetailsViewModel GetUpdatedBundles(BundleDetailsViewModel bundleCartItem);
        Task<IEnumerable<ProductViewModel>> GetProductByIdsAsyn(string ids, int accountId);
        Task<Collection<ProductViewModel>> GetHomeSpecialsAsync();
        string GetProductUrl(int id, UrlHelper urlHelper);
    }
}