﻿using System.Collections.Generic;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public interface IShippingOptionAgent
    {
        ShippingOptionListViewModel GetShippingList();
        ShippingOptionViewModel GetShippingOption(int shippingOptionId);
    }
}