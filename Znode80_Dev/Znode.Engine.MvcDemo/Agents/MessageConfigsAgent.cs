﻿using System;
using System.Threading.Tasks;
using Znode.Engine.Api.Client;

namespace Znode.Engine.MvcDemo.Agents
{
	public class MessageConfigsAgent : BaseAgent, IMessageConfigsAgent
	{
		private readonly IMessageConfigsClient _messageConfigsClient;

		public MessageConfigsAgent()
		{
			_messageConfigsClient = new MessageConfigsClient();
		}

		public string GetByKey(string key)
		{
		    try
		    {
                var model = _messageConfigsClient.GetMessageConfigByKey(key, null);
                if (!String.IsNullOrEmpty(model.Value))
                {
                    return model.Value;
                }
		    }
		    catch (Exception)
		    {
		        
		        
		    }
			

			return string.Empty;
		}

        public async Task<string> GetByKeyAsync(string key)
        {
            try
            {
                var model = await _messageConfigsClient.GetMessageConfigByKeyAsync(key, null);
                if (!String.IsNullOrEmpty(model.Value))
                {
                    return model.Value;
                }
            }
            catch (Exception)
            {


            }


            return string.Empty;
        }
	}
}