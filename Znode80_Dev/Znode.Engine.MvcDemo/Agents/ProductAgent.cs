﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Web.Mvc;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public class ProductAgent : BaseAgent, IProductAgent
    {
        readonly IProductsClient _client;
        readonly ISkuAgent _skuAgent;
        readonly IShoppingCartsClient _shoppingCartsClient;
        readonly IReviewsClient _reviewsClient;
        readonly ICategoryAgent _categoryAgent;
        readonly IAccountAgent _accountAgent;
        readonly ISearchAgent _searchAgent;
        readonly ISuppliersClient _supplierClient;

        public ProductAgent()
        {
            _client = GetClient<ProductsClient>();
            _shoppingCartsClient = GetClient<ShoppingCartsClient>();
            _reviewsClient = GetClient<ReviewsClient>();
            _skuAgent = new SkuAgent();
            _categoryAgent = new CategoryAgent();
            _accountAgent = new AccountAgent();
            _searchAgent = new SearchAgent();
            _supplierClient = GetClient<SuppliersClient>();
        }

        /// <summary>
        /// Get the product based on the product id
        /// </summary>
        /// <param name="productId">Product ID</param>
        /// <returns>Returns the product details as product view model</returns>
        public ProductViewModel GetProduct(int productId)
        {
            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Skus);
            Expands.Add(ExpandKeys.Attributes);
            Expands.Add(ExpandKeys.Promotions);
            Expands.Add(ExpandKeys.Categories);
            Expands.Add(ExpandKeys.AddOns);
            Expands.Add(ExpandKeys.Reviews);
            Expands.Add(ExpandKeys.Images);
            Expands.Add(ExpandKeys.BundleItems);
            Expands.Add(ExpandKeys.FrequentlyBoughtTogether);
            Expands.Add(ExpandKeys.YouMayAlsoLike);

            var product = _client.GetProduct(productId, Expands);

            if (product == null)
            {
                return null;
            }
            // If the product is not active redirect to home page
            if (!product.IsActive)
            {
                return null;
            }

            var categories = _categoryAgent.GetCategories();

            // Check if it is this store product.
            var isStoreProduct = product.Categories.Any(x => categories.Categories.Any(y => x.CategoryId == y.CategoryId));

            if (!isStoreProduct)
                return null;

            var productViewModel = ProductViewModelMap.ToViewModel(product);

            // Check the bundles and populate it.
            if (!string.IsNullOrEmpty(productViewModel.BundleItemsIds))
            {
                var bundleViewModel = GetBundles(productViewModel.BundleItemsIds);

                if (bundleViewModel == null)
                {
                    return null;
                }
            }

            // Populate the product attributes
            if (productViewModel.ProductAttributes.Attributes.Any())
            {
                int attributeId = productViewModel.ProductAttributes.Attributes.FirstOrDefault().ProductAttributes.FirstOrDefault().AttributeId;
                string selectedSku = string.Empty;
                string imagePath = string.Empty;
                //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
                string imageMediumPath = string.Empty;

                productViewModel.Price = _skuAgent.GetSkusPrice(productId, attributeId, productViewModel.Price, out selectedSku, out imagePath, out imageMediumPath);
                productViewModel.Sku = selectedSku;

                productViewModel.ProductAttributes = UpdateProductAttributes(productViewModel, attributeId, string.Empty);

                if (Path.GetFileName(imagePath) != MvcDemoConstants.NoImageName)
                {
                   productViewModel.ImageLargePath = imagePath;
                }

            }

            // Check the inventory
            if (!productViewModel.IsCallForPricing)
            {
                IEnumerable<int> attributeId = new List<int>();

                if (productViewModel.ProductAttributes.Attributes.Any())
                {
                    if (productViewModel.ProductAttributes.Attributes.First().ProductAttributes.Any())
                        attributeId = productViewModel.ProductAttributes.Attributes.Select(x => x.ProductAttributes.First(y => y.Available).AttributeId);
                }

                CheckInventory(productViewModel, string.Join(",", attributeId), productViewModel.SelectedQuantity);
            }
            else
            {
                productViewModel.InventoryMessage = string.Format(Resources.ZnodeResource.TextErrorFormat, Resources.ZnodeResource.TextCallForPricing);
                productViewModel.ShowAddToCart = false;
            }

            // Populate the product add-on
            if (productViewModel.AddOns != null && productViewModel.AddOns.Any())
            {
                SetAddOnPrice(productViewModel);

                decimal finalProductPrice;
                productViewModel.AddOns = GetAddOnDetails(productViewModel, string.Join(",", productViewModel.AddOns.Where(x => x.SelectedAddOnValue != null).SelectMany(x => x.SelectedAddOnValue)), 1, productViewModel.ProductPrice, out finalProductPrice);

                productViewModel.OriginalPrice = finalProductPrice;
            }

            // Populate the product Alternate Image
            if (productViewModel.Images != null && productViewModel.Images.Any())
            {
                //Znode Version 7.2.2
                //Populate Swatch Images - Start 
                //Filter All Product images to get the swatch iamges based on ProductImageTypeId.
                ProductViewModel swatchImageModel = productViewModel;
                productViewModel.SwatchImages = swatchImageModel.Images.Where(x => x.ProductImageTypeId == 2 && x.IsActive).OrderBy(x => x.DisplayOrder).ToList();
                if (productViewModel.SwatchImages != null && productViewModel.SwatchImages.Any())
                {
                    productViewModel.SwatchImages.Add(new ImageViewModel { ImageAltTag = productViewModel.ImageAltTag, ImageSmallThumbnailPath = productViewModel.ImageSmallThumbnailPath,ImageMediumPath=productViewModel.ImageMediumPath, ImageLargePath = productViewModel.ImageLargePath });
                }
                //Populate Swatch Images - End
                productViewModel.Images = productViewModel.Images.Where(x => x.ProductImageTypeId == 1 && x.IsActive).OrderBy(x => x.DisplayOrder).ToList();
            }

            // For Displaying the category name in the product page
            var categoryName = GetFromSession<string>(Helpers.MvcDemoConstants.CategoryName);

            productViewModel.CategoryName = !string.IsNullOrEmpty(categoryName) ? categoryName : product.Categories.FirstOrDefault().Name;

            var accountViewModel = _accountAgent.GetAccountViewModel();
            productViewModel.WishListID = _accountAgent.CheckWishListExists(productId, accountViewModel != null ? accountViewModel.AccountId : 0);
            productViewModel.ShowWishlist = productViewModel.WishListID > 0;


            productViewModel.Title =
                 (!string.IsNullOrEmpty(product.SeoTitle)) ? product.SeoTitle : product.Name;

            //Znode Version 7.2.2
            //Show Vendor Name in Product Details, in case product belongs to Vendor - Start
            if (product.SupplierId > 0)
            {
                //Gets Supplier Details based on Id.
                var supplierDetails = _supplierClient.GetSupplier((int)product.SupplierId);
                productViewModel.VendorName = string.Format("{0} {1} ({2})", supplierDetails.ContactFirstName, supplierDetails.ContactLastName, supplierDetails.ContactEmail);
            }
            //Show Vendor Name in Product Details, in case product belongs to Vendor - End
            return productViewModel;
        }

        /// <summary>
        /// Get the Bundled product details 
        /// </summary>
        /// <param name="ids">bundled product ids</param>
        /// <returns>Collection of products</returns>
        public BundleDetailsViewModel GetBundleProductsDetails(CartItemViewModel cartItemViewModel)
        {
            var bundleViewModel = new BundleDetailsViewModel();
            bundleViewModel.SelectedAttributesIds = cartItemViewModel.AttributeIds;
            bundleViewModel.SelectedProductId = cartItemViewModel.ProductId;
            bundleViewModel.SelectedQuantity = cartItemViewModel.Quantity;
            bundleViewModel.SelectedAddOnValueIds = cartItemViewModel.AddOnValueIds;
            bundleViewModel.SelectedSku = cartItemViewModel.Sku;
            bundleViewModel.ProductName = cartItemViewModel.ProductName;

            var items = cartItemViewModel.BundleItemsIds.Split(',');

            var productsModel = _searchAgent.GetProductsByIds(items.ToList());

            if (productsModel != null && productsModel.Count > 0)
            {
                bundleViewModel.Bundles = productsModel.Where(x => x.IsActive).Select(ProductViewModelMap.ToViewModel).ToList();

                foreach (var bundle in bundleViewModel.Bundles)
                {
                    bundle.ShowAddToCart = true;
                    IEnumerable<int> attributeIds = new List<int>();

                    // Initial Load of the dropdown
                    if (bundle.ProductAttributes.Attributes.Any())
                    {
                        int attributeId =
                            bundle.ProductAttributes.Attributes.FirstOrDefault()
                                  .ProductAttributes.FirstOrDefault()
                                  .AttributeId;


                        string selectedSku = string.Empty;

                        bundle.Sku = selectedSku;
                        bundle.ProductAttributes = UpdateProductAttributes(bundle, attributeId, string.Empty);

                        attributeIds = bundle.ProductAttributes.Attributes.Select(x => x.ProductAttributes.First(y => y.Available).AttributeId);
                    }

                    CheckInventory(bundle, string.Join(",", attributeIds), bundle.SelectedQuantity);

                    if (bundle.AddOns != null && bundle.AddOns.Any())
                    {
                        var selectedAddOnValue = string.Join(",", bundle.AddOns.Where(x => x.SelectedAddOnValue != null).SelectMany(x => x.SelectedAddOnValue).ToArray());
                        //selectedAddOnValue = selectedAddOnValue + "," + String.Join(",", bundle.AddOns.Where(x => x.CheckBoxSelectedAddOnValue != null).SelectMany(y => y.CheckBoxSelectedAddOnValue).ToArray());
                        CheckAddOnInventory(bundle, selectedAddOnValue, bundle.SelectedQuantity);
                    }

                    bundle.ShowQuantity = false;
                }
            }

            return bundleViewModel;
        }

        public BundleDetailsViewModel GetUpdatedBundles(BundleDetailsViewModel bundleCartItem)
        {
            var cartItem = new CartItemViewModel()
            {
                AddOnValueIds = bundleCartItem.SelectedAddOnValueIds,
                AttributeIds = bundleCartItem.SelectedAttributesIds,
                ProductId = bundleCartItem.SelectedProductId,
                Quantity = bundleCartItem.SelectedQuantity,
                Sku = bundleCartItem.SelectedSku,
                BundleItemsIds = string.Join(",", bundleCartItem.Bundles.Select(x => x.ProductId)),
                ProductName = bundleCartItem.ProductName,
            };

            var productsViewModel = GetBundleProductsDetails(cartItem);

            foreach (var currentBundleItem in productsViewModel.Bundles)
            {
                var currentBundleCartItem =
                           bundleCartItem.Bundles.FirstOrDefault(x => x.ProductId == currentBundleItem.ProductId);
                if (currentBundleItem != null && currentBundleCartItem != null)
                {

                    if (currentBundleItem.ProductAttributes != null &&
                        currentBundleItem.ProductAttributes.Attributes.Any())
                    {
                        decimal prodPrice;
                        string selectedSku;
                        string imagePath;
                        //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
                        string imageMediumPath =string.Empty;

                        var attributeId = currentBundleCartItem.ProductAttributes.Attributes.FirstOrDefault().SelectedAttributeId;

                        if (currentBundleItem.ProductId == bundleCartItem.SelectedProductId)
                        {
                            attributeId = Convert.ToInt32(bundleCartItem.SelectedBundleAttributeId);
                        }

                        // Update the selected attributes
                        var selectedAttributeIds = string.Join(",",
                                                               currentBundleCartItem.ProductAttributes.Attributes.Select
                                                                   (
                                                                       y => y.SelectedAttributeId));

                        var model = GetAttributes(currentBundleItem.ProductId, attributeId, selectedAttributeIds,
                                                  cartItem.Quantity, out prodPrice, out selectedSku, out imagePath, out imageMediumPath);

                        currentBundleItem.ProductAttributes = model.ProductAttributes;
                        currentBundleItem.InventoryMessage = model.InventoryMessage;
                        currentBundleItem.ShowAddToCart = model.ShowAddToCart;
                        currentBundleItem.Sku = selectedSku;
                    }

                    // Update the selected add-Ons
                    if (currentBundleItem.AddOns != null && currentBundleItem.AddOns.Any())
                    {
                        currentBundleItem.AddOns.ToList().ForEach(x =>
                        {
                            x.SelectedAddOnValue = currentBundleCartItem.AddOns.FirstOrDefault(z => z.AddOnId == x.AddOnId).SelectedAddOnValue;
                            //x.CheckBoxSelectedAddOnValue = currentBundleCartItem.AddOns.FirstOrDefault(z => z.AddOnId == x.AddOnId).CheckBoxSelectedAddOnValue;
                        });

                        var selectedAddOnValue = string.Join(",", currentBundleCartItem.AddOns.Where(x => x.SelectedAddOnValue != null).SelectMany(x => x.SelectedAddOnValue).ToArray());
                        //selectedAddOnValue = selectedAddOnValue + "," + String.Join(",", currentBundleCartItem.AddOns.Where(x => x.CheckBoxSelectedAddOnValue != null).SelectMany(y => y.CheckBoxSelectedAddOnValue).ToArray());

                        CheckAddOnInventory(currentBundleItem, selectedAddOnValue, currentBundleItem.SelectedQuantity);
                    }
                }
            }

            return productsViewModel;
        }

        /// <summary>
        /// Update product attributes availablity based on selected attributes.
        /// </summary>
        /// <param name="product"></param>
        /// <param name="attributeId"></param>
        /// <param name="selectedAttributeIds"></param>
        /// <returns></returns>
        public ProductAttributesViewModel UpdateProductAttributes(ProductViewModel product, int? attributeId, string selectedAttributeIds)
        {
            // to keep already selected attribute item
            var attributeIds = selectedAttributeIds.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries)
                                    .Select(x => int.Parse(x));

            // select all the dropdowns ids that are selected, 
            var selectedAttributes = attributeIds.TakeWhile(x => x != attributeId).Union(new List<int> { attributeId.GetValueOrDefault(0) });

            // Get distinct SKU Ids for selected attribute dropdown values.
            var selectedAttributeSkuIds = product.ProductAttributes.Attributes.Select(attributeType =>
                {
                    var attributeViewModel =
                        attributeType.ProductAttributes.FirstOrDefault(
                            attribute => selectedAttributes.Any(x => x == attribute.AttributeId));

                    if (attributeViewModel != null)
                        return new { attributeType.AttributeTypeId, attributeViewModel.SkuIds };

                    // return empty collction if no attribute found.
                    return null;

                }).Where(x => x != null).Distinct();

            // Update "Available" and "Selected" flag only based on SKU Id combination that is based on selected dropdown options. 
            // e.g. 2nd dropdown should populate 1st dropdown selection, 3rd dropdown based on 2nd dropdown selection.
            // if 1st dropdown selected, then it will populate all other dropdowns based on first dropdown only.
            // if 2nd dropdown selected, 
            //      then 1st dropdown will populate as it is, 
            //      then 2nd dropdown will populate based on 1st dropdown selection only. 
            //      all other dropdowns based on combination of 1st and 2nd dropdown dropdown only.

            // Loop all attribute types and its attributes to set Available and Selected flag value.
            var toBeFiltered = false;
            foreach (var attributeType in product.ProductAttributes.Attributes)
            {
                attributeType.ToBeFiltered = toBeFiltered;
                foreach (var attribute in attributeType.ProductAttributes)
                {
                    // select all dropdown values upto currently selected one.
                    var skuIds = selectedAttributeSkuIds.TakeWhile(x => x.AttributeTypeId != attribute.AttributeTypeId);

                    // set first one if not.
                    if (!skuIds.Any())
                        skuIds = skuIds.Union(new[] { selectedAttributeSkuIds.FirstOrDefault() });

                    // Set available flag only it matches all the selected dropdown values
                    attribute.Available = skuIds.All(x => x.SkuIds.Any(v => attribute.SkuIds.Any(skuid => v == skuid)));

                    // Set the selected flag to repopulate to all dropdowns.
                    attribute.Selected = attributeIds.Any(attId => attId == attribute.AttributeId);
                }

                toBeFiltered = true;

                // Set which is currently selected dropdown.
                attributeType.IsSelectedOption = attributeType.ProductAttributes.Any(x => x.AttributeId == attributeId);
            }

            return product.ProductAttributes;
        }

        /// <summary>
        /// Get the product details by Ids
        /// </summary>
        /// <param name="ids">Product IDs</param>
        /// <returns>Returns the list of products as product view model</returns>
        public IEnumerable<ProductViewModel> GetProductByIds(string ids)
        {
            if (string.IsNullOrEmpty(ids))
            {
                return null;
            }
            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Skus);
            Expands.Add(ExpandKeys.Attributes);
            Expands.Add(ExpandKeys.Promotions);
            IEnumerable<ProductViewModel> products = new Collection<ProductViewModel>();
            var idString = (ids.Split(',').Length == 1) ? ids + "," : ids;
            var list = _client.GetProductsByProductIds(idString, Expands, new SortCollection());
            products = products.Union(ProductViewModelMap.ToViewModels(list.Products));
            return products;
        }

        /// <summary>
        /// Get the product details by Ids
        /// </summary>
        /// <param name="ids">Product IDs</param>
        /// <param name="accountId">Account ID</param>
        /// <returns>Returns the list of products as product view model</returns>
        public async Task<IEnumerable<ProductViewModel>> GetProductByIdsAsyn(string ids, int accountId)
        {
            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Skus);
            Expands.Add(ExpandKeys.Attributes);
            Expands.Add(ExpandKeys.Promotions);

            IEnumerable<ProductViewModel> products = new Collection<ProductViewModel>();
            var idString = (ids.Split(',').Length == 1) ? ids + "," : ids;
            var list = await _client.GetProductsByProductIdsAsync(idString, accountId, Expands, new SortCollection());
            var currentList = ProductViewModelMap.ToViewModels(new Collection<ProductModel>(list.Products.Where(x => x.IsActive).ToList()));
            if (currentList != null)
            {
                products = products.Union(currentList);
            }
            return products;
        }

        /// <summary>
        /// Get the SKU price based on the product and attribute
        /// </summary>
        /// <param name="productId">Product ID</param>
        /// <param name="attributeId">Attribute ID</param>
        /// <returns>Returns the SKU Price</returns>
        public decimal GetSkuPrice(int productId, int attributeId)
        {
            Expands = new ExpandCollection();
            Expands.Add(ExpandKeys.Skus);
            Expands.Add(ExpandKeys.Attributes);
            Expands.Add(ExpandKeys.Promotions);

            var model = _client.GetProduct(productId, Expands);

            if (model != null)
            {

                if (model.ProductId == productId)
                {
                    var attribute = model.Skus.Where(x => x.Attributes.Equals(attributeId)).First();

                    foreach (var sku in model.Skus)
                    {
                        var attributeSelected = sku.Attributes.Where(x => x.AttributeId.Equals(attributeId)).FirstOrDefault();
                        if (attributeSelected != null)
                        {
                            if (sku.ProductPrice.HasValue)
                            {
                                return sku.ProductPrice.Value;
                            }
                            return model.RetailPrice.Value;
                        }
                    }


                }
            }

            return 0;
        }
        /// <summary>
        /// Returns Home Page specials for a store
        /// </summary>
        /// <returns>Collection of ProductViewModel</returns>
        public Collection<ProductViewModel> GetHomeSpecials()
        {
            Expands = new ExpandCollection { ExpandKeys.Promotions,ExpandKeys.Skus,ExpandKeys.Reviews };


            var model = _client.GetProductsByHomeSpecials(Expands, null, new SortCollection(), 0, MvcDemoConstants.DefaultHomeSpecialsSize);
           

            if (model != null && model.Products != null && model.Products.Count > 0)
            {

                return new Collection<ProductViewModel>(ProductViewModelMap.ToViewModels(new Collection<ProductModel>(model.Products.Where(x => x.IsActive).ToList())));

            }

            return null;
        }

        /// <summary>
        /// Returns Home Page specials for a store
        /// </summary>
        /// <returns>Collection of ProductViewModel</returns>
        public async Task<Collection<ProductViewModel>> GetHomeSpecialsAsync()
        {
            Expands = new ExpandCollection { ExpandKeys.Promotions };


            var model = await _client.GetProductsByHomeSpecialsAsync(Expands, null, new SortCollection(), 0, MvcDemoConstants.DefaultHomeSpecialsSize);

            if (model != null && model.Products != null && model.Products.Count > 0)
            {

                return new Collection<ProductViewModel>(ProductViewModelMap.ToViewModels(new Collection<ProductModel>(model.Products.Where(x => x.IsActive).ToList())));

            }

            return null;
        }

        /// <summary>
        /// Returns Bundles for list of prdocuts
        /// </summary>
        /// <returns>Collection of ProductViewModel</returns>
        public List<BundleDisplayViewModel> GetBundles(string bundleProductIds)
        {
            if (bundleProductIds != null)
            {
                var items = bundleProductIds.Split(',');
                var model = _searchAgent.GetProductsByIds(items.ToList());

                if (model != null && model.Count > 0)
                {
                    var activeBundleProducts = model.Where(x => x.IsActive);

                    if (model.Count == activeBundleProducts.Count())
                    {
                        return BundleViewModelMap.GetBundleDisplayViewModel(model);
                    }
                }
            }

            return null;
        }

        /// <summary>
        /// Get the extented price
        /// </summary>
        /// <param name="productId">Product ID</param>
        /// <param name="quantity">Quantity</param>
        /// <param name="sku">SKU</param>
        /// <param name="prodPrice"></param>
        /// <returns>Returns the extended price</returns>
        public decimal GetExtendedPrice(int productId, int quantity, string sku, decimal prodPrice)
        {
            var cartModel = new ShoppingCartModel();
            cartModel.ShoppingCartItems.Add(CartItemViewModelMap.ToShoppingCartModel(productId, quantity, sku));

            var shoppingCartModel = _shoppingCartsClient.Calculate(cartModel);

            // Check if item exists after API call.
            if (shoppingCartModel != null && shoppingCartModel.ShoppingCartItems.Any())
            {
                var item = shoppingCartModel.ShoppingCartItems.FirstOrDefault(x => x.ProductId == productId);
                if (item == null) return 0;
                return item.ExtendedPrice;
            }
            return quantity * prodPrice;
        }

        /// <summary>
        /// Check Inventory for the selected product sku and displays the error message based on Quantity on hand.
        /// </summary>
        /// <param name="model">Product Model</param>
        /// <param name="attributeId">Selected attribute id</param>
        public void CheckInventory(ProductViewModel model, string attributeId, int? quantity)
        {
            var selectedSku = _skuAgent.GetSkuInventory(model.ProductId, attributeId);

            if (selectedSku == null)
            {
                model.InventoryMessage = Resources.ZnodeResource.ErrorSelectAttribute;
                model.ShowAddToCart = false;
                return;
            }

            if (selectedSku != null)
            {
                var selectedQuantity = quantity.HasValue && quantity.Value > 0 ? quantity.Value : model.MinQuantity;

                var cartQuantity = GetOrderedItemQuantity(selectedSku.Sku);

                var combinedQuantity = selectedQuantity + cartQuantity;

                if (combinedQuantity > model.MaxQuantity)
                {
                    model.InventoryMessage = string.Format(Resources.ZnodeResource.TextErrorFormat,
                        Resources.ZnodeResource.TextMaxQuantityReached);
                    model.ShowAddToCart = false;
                    return;
                }

                if (selectedSku.Inventory.QuantityOnHand < combinedQuantity && (!model.AllowBackOrder.GetValueOrDefault()) && model.TrackInventory.GetValueOrDefault())
                {
                    model.InventoryMessage = !string.IsNullOrEmpty(model.OutOfStockMessage) ? model.OutOfStockMessage : string.Format(Resources.ZnodeResource.TextErrorFormat, Resources.ZnodeResource.TextOutofStock);
                    model.ShowAddToCart = false;
                    return;
                }

                if (selectedSku.Inventory.QuantityOnHand < combinedQuantity && model.AllowBackOrder.GetValueOrDefault())
                {
                    model.InventoryMessage = !string.IsNullOrEmpty(model.BackOrderMessage) ? model.BackOrderMessage : Resources.ZnodeResource.TextBackOrderMessage;
                    model.ShowAddToCart = true;
                    return;
                }

            }
            model.InventoryMessage = !string.IsNullOrEmpty(model.InStockMessage) ? model.InStockMessage : Resources.ZnodeResource.TextInstock;
            model.ShowAddToCart = true;
        }

        /// <summary>
        /// Check Inventory for the selected addon and displays the error message based on Quantity on hand.
        /// </summary>
        /// <param name="model">Product Model</param>
        /// <param name="selectedAddOnId">Selected addon id</param>
        /// <param name="quantity"></param>
        public void CheckAddOnInventory(ProductViewModel model, string selectedAddOnId, int? quantity)
        {
            string[] selectedAddOn = selectedAddOnId.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);


            foreach (var addonvalueId in selectedAddOn)
            {
                AddOnViewModel addOn = null;
                if (!string.IsNullOrEmpty(addonvalueId))
                {
                    addOn =
                        model.AddOns.FirstOrDefault(
                            x => x.AddOnValues.Any(y => y.AddOnValueId == int.Parse(addonvalueId)));
                }

                if (addOn != null)
                {
                    var addOnValue = addOn.AddOnValues.FirstOrDefault(y => y.AddOnValueId == int.Parse(addonvalueId));

                    if (addOnValue != null)
                    {
                        var selectedQuantity = quantity.HasValue && quantity.Value > 0 ? quantity.Value : model.MinQuantity;

                        var cartQuantity = GetOrderedItemQuantity(addOnValue.SKU);

                        var combinedQuantity = selectedQuantity + cartQuantity;

                        if (combinedQuantity > model.MaxQuantity)
                        {
                            model.InventoryMessage = string.Format(Resources.ZnodeResource.TextErrorFormat,
                                                                   Resources.ZnodeResource.TextMaxQuantityReached);
                            model.ShowAddToCart = false;
                            return;
                        }

                        if (addOnValue.QuantityOnHand < combinedQuantity && (!addOn.AllowBackOrder) && addOn.TrackInventory)
                        {
                            model.InventoryMessage = !string.IsNullOrEmpty(addOn.OutOfStockMessage) ? addOn.OutOfStockMessage : string.Format(Resources.ZnodeResource.TextAddOnOutofStock, addOn.Name);

                            model.ShowAddToCart = false;
                            return;
                        }

                        if (addOnValue.QuantityOnHand < combinedQuantity && addOn.AllowBackOrder)
                        {
                            model.InventoryMessage = !string.IsNullOrEmpty(addOn.BackOrderMessage) ? addOn.BackOrderMessage : Resources.ZnodeResource.TextBackOrderMessage;
                            model.ShowAddToCart = true;
                            return;
                        }
                    }
                }
            }

            if (string.IsNullOrEmpty(model.InventoryMessage) && model.ShowAddToCart)
            {
                model.InventoryMessage = !string.IsNullOrEmpty(model.AddOns.FirstOrDefault().InStockMessage) ? model.AddOns.FirstOrDefault().InStockMessage : Resources.ZnodeResource.TextInstock;
                model.ShowAddToCart = true;
            }
        }

        /// <summary>
        /// Get ordered items quantity by the given sku.
        /// </summary>
        /// <param name="sku"></param>
        /// <returns></returns>
        public int GetOrderedItemQuantity(string sku)
        {
            var cart = GetFromSession<ShoppingCartModel>(MvcDemoConstants.CartModelSessionKey);
            var cartQuantity = default(int);

            if (cart != null && cart.ShoppingCartItems.Any(model => model.Sku.Equals(sku)))
            {
                var shoppingCartItemModel = cart.ShoppingCartItems.FirstOrDefault(model => model.Sku.Equals(sku));
                if (shoppingCartItemModel != null)
                    cartQuantity = shoppingCartItemModel.Quantity;
            }
            return cartQuantity;
        }

        /// <summary>
        /// Gets SelectedSku, Product Price based on selected attributeId
        /// </summary>
        /// <param name="id">Product Id</param>
        /// <param name="attributeId">Attribute Id</param>
        /// <param name="selectedIds">Selected Attribute Ids</param>
        /// <param name="quantity">Quantity</param>
        /// <param name="prodPrice">Product Price</param>
        /// <param name="selectedSku">Selected Sku</param>
        /// <param name="imageMediumPath">Selected Sku imageMediumPath</param>
        /// <returns></returns>
        public ProductViewModel GetAttributes(int? id, int? attributeId, string selectedIds, int? quantity, out decimal prodPrice, out string selectedSku, out string imagePath, out string imageMediumPath)
        {
            var product = GetProduct(id.Value);

            // update product attributes based on selection.
            product.ProductAttributes = UpdateProductAttributes(product, attributeId, selectedIds);

            var updatedSelectedIds = product.ProductAttributes.Attributes.Select(x =>
                {
                    var attributeViewModel =
                        x.ProductAttributes.FirstOrDefault(y => (y.Available && y.Selected));

                    if (attributeViewModel == null)
                        attributeViewModel = x.ProductAttributes.FirstOrDefault(y => (y.Available));

                    return attributeViewModel != null ? attributeViewModel.AttributeId : 0;
                }).Where(x => x > 0);

            if (!(product.IsCallForPricing))
            {
                CheckInventory(product, string.Join(",", updatedSelectedIds), quantity.GetValueOrDefault(1));
            }
            //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
            prodPrice = _skuAgent.GetSkusPrice(id.Value, attributeId.GetValueOrDefault(0), product.Price, out  selectedSku, out imagePath, out imageMediumPath);

            prodPrice = GetExtendedPrice(id.Value, quantity.GetValueOrDefault(1), selectedSku, prodPrice);

            //Set Product Main Image Path in case no image found.
            if (Path.GetFileName(imagePath).Equals(MvcDemoConstants.NoImageName))
            {
                imagePath = product.ImageLargePath;
                imageMediumPath = product.ImageMediumPath;
            }
            return product;
        }

        /// <summary>
        /// Get the add on details
        /// </summary>
        /// <param name="product"></param>
        /// <param name="selectedAddOnIds"></param>
        /// <param name="quantity"></param>
        /// <param name="productPrice"></param>
        /// <param name="finalProductPrice"></param>
        /// <returns></returns>
        public IEnumerable<AddOnViewModel> GetAddOnDetails(ProductViewModel product, string selectedAddOnIds, int? quantity, decimal productPrice, out decimal finalProductPrice)
        {
            // To DO Check the inventory for the addon
            CheckAddOnInventory(product, selectedAddOnIds, quantity.GetValueOrDefault(1));

            finalProductPrice = productPrice;
            string[] selectedAddOn = selectedAddOnIds.Split(',');

            foreach (var addonvalueId in selectedAddOn)
            {
                if (!string.IsNullOrEmpty(addonvalueId))
                {
                    var addOnValuesViewModel =
                        product.AddOns.SelectMany(
                            y => y.AddOnValues.Where(x => x.AddOnValueId == int.Parse(addonvalueId))).FirstOrDefault();
                    if (addOnValuesViewModel == null) continue;
                    var price = addOnValuesViewModel.AddOnFinalPrice;
                    finalProductPrice += price * quantity.GetValueOrDefault(1);
                }
            }

            return product.AddOns;
        }

        /// <summary>
        /// Gets ProductViewModel based on wishlist added Productid
        /// </summary>
        /// <param name="productId">ProductId added to wishlist </param>
        /// <returns>Returns ProductViewModel with wishlist success message</returns>
        public WishListViewModel CreateWishList(int productId)
        {
            var wishListViewModel = _accountAgent.CreateWishList(productId);
            return wishListViewModel;
        }

        /// <summary>
        /// Set the wishlist success and error message
        /// </summary>
        /// <param name="model">Product View Model</param>
        /// <param name="success">wishlist added or not</param>
        public void SetMessages(ProductViewModel model)
        {
            SetStatusMessage(model);
        }

        /// <summary>
        /// Creates a new review for the product
        /// </summary>
        /// <param name="model">ReviewItemViewModel</param>
        /// <returns>Returns the updated ProductViewModel</returns>
        public ReviewItemViewModel CreateReview(ReviewItemViewModel model)
        {
            var accountViewModel = _accountAgent.GetAccountViewModel();

            model.AccountId = accountViewModel != null ? (int?)accountViewModel.AccountId : null;

            _reviewsClient.CreateReview(AccountViewModelMap.ToReviewViewModel(model));


            model.SuccessMessage = Resources.ZnodeResource.SuccessWriteReview;
            return model;


            model.ErrorMessage = Resources.ZnodeResource.ErrorWriteReview;
            model.HasError = true;
            return model;
        }

        /// <summary>
        /// Get all the reviews based on ProductId
        /// </summary>
        /// <param name="productId">ProductId</param>
        /// <returns>Returns a ReviewViewModel along with the product information</returns>
        public ReviewViewModel GetAllReviews(int productId)
        {

            var list = _reviewsClient.GetReviews(new ExpandCollection() { ExpandKeys.Reviews },
                                                        new FilterCollection()
                                                            {
                                                                new FilterTuple(FilterKeys.ProductId, FilterOperators.Equals,
                                                                                productId.ToString())
                                                            },
                                                        new SortCollection());


            var reviewModel = list.Reviews.Where(x => x.Status == MvcDemoConstants.ApprovedReviewStatus).OrderByDescending(x => x.CreateDate);

            var reviewModelList = AccountViewModelMap.ToReviewViewModel(new Collection<ReviewModel>(reviewModel.ToList()));

            reviewModelList.Items.ToList()
                              .ForEach(
                                  x =>
                                  x.Product =
                                  ProductViewModelMap.ToViewModel(
                                      _client.GetProduct(x.ProductId.GetValueOrDefault())));


            return reviewModelList;
        }

        /// <summary>
        /// Sets the addon price for the addons
        /// </summary>
        /// <param name="productViewModel"></param>
        private void SetAddOnPrice(ProductViewModel productViewModel)
        {
            productViewModel.AddOns.ToList().ForEach(x => x.AddOnValues.ToList().ForEach(y => y.AddOnFinalPrice = GetAddOnPrice(y)));
        }

        /// <summary>
        /// Gets the addon prices for an addonvalue
        /// </summary>
        /// <param name="addOnValuesViewModel"></param>
        /// <returns></returns>
        private decimal GetAddOnPrice(AddOnValuesViewModel addOnValuesViewModel)
        {
            decimal finalPrice = 0;
            finalPrice = addOnValuesViewModel.RetailPrice;

            if (addOnValuesViewModel.SalePrice.HasValue)
            {
                finalPrice = addOnValuesViewModel.SalePrice.GetValueOrDefault(0);
            }

            if (System.Web.HttpContext.Current.User.Identity.IsAuthenticated && addOnValuesViewModel.WholeSalePrice.HasValue)
            {
                var accountViewModel = _accountAgent.GetAccountViewModel();

                if (accountViewModel != null && accountViewModel.UseWholeSalePricing)
                {
                    finalPrice = addOnValuesViewModel.WholeSalePrice.GetValueOrDefault(0);
                }
            }

            return finalPrice;
        }

        public string GetProductUrl(int id, UrlHelper urlHelper)
        {
            if (id > 0)
            {
                var productViewModel = GetProduct(id);

                if (!string.IsNullOrEmpty(productViewModel.SeoPageName))
                {
                    return urlHelper.RouteUrl("seoproduct-details", new { seo = productViewModel.SeoPageName.ToLower(), id });
                }

                return urlHelper.RouteUrl("product-details", new { id });
            }

            return string.Empty;
        }

    }
}