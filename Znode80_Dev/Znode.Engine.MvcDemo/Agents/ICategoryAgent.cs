﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public interface ICategoryAgent
    {
        CategoryListViewModel GetCategories();
        CategoryViewModel GetCategory(int categoryId);
        CategoryViewModel GetCategoryByName(string categoryName);
    }
}