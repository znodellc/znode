﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcDemo.Agents
{
    public class UrlRedirectAgent : BaseAgent, IUrlRedirectAgent
    {
        private readonly IUrlRedirectClient _client;

        public UrlRedirectAgent()
        {
            _client = GetClient<UrlRedirectClient>();
        }

        public UrlRedirectListModel GetActive301Redirects()
        {
            Filters = new FilterCollection {{FilterKeys.IsActive, FilterOperators.Equals, "true"}};

            return _client.Get301Redirects(Filters, new SortCollection(), null, null);
        }

        private static UrlRedirectListModel GetActive301RedirectsFromCache()
        {
            const string cacheKey = "301Redirects";
            if (HttpContext.Current.Cache[cacheKey] == null)
            {
                var agent = new UrlRedirectAgent();
                var model = agent.GetActive301Redirects();

                if (model != null)
                {
                    HttpContext.Current.Cache.Insert(cacheKey, model, null, DateTime.Now.AddMinutes(20),
                                                     Cache.NoSlidingExpiration);
                }
            }

            return HttpContext.Current.Cache[cacheKey] as UrlRedirectListModel;
        }

        public static UrlRedirectModel Has301Redirect(string url)
        {
            var redirects = GetActive301RedirectsFromCache();

            if (redirects == null || redirects.UrlRedirects == null) return null;

            var item = redirects.UrlRedirects.FirstOrDefault(x => x.OldUrl.ToLower() == url.ToLower());

            return item;
        }
    }
}