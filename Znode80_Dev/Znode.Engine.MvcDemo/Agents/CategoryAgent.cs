﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using Znode.Engine.Api.Client;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Agents
{
    public class CategoryAgent : BaseAgent, ICategoryAgent
    {
        private readonly ICategoriesClient _categoriesClient;
        private readonly IAccountAgent _accountAgent;

        private readonly ExpandCollection _expands = new ExpandCollection()
            {
                ExpandKeys.Images,
                ExpandKeys.Subcategories,
                ExpandKeys.CategoryNodes,
            };

        /// <summary>
        /// Category Agent Constructor
        /// </summary>
        public CategoryAgent()
        {
            _categoriesClient = GetClient<CategoriesClient>();
            _accountAgent = new AccountAgent();
        }

        /// <summary>
        /// Get the categories based on the catalog.
        /// </summary>
        /// <returns>Returns the categories as category list view model</returns>
        public CategoryListViewModel GetCategories()
        {
            var accountModel = _accountAgent.GetAccountViewModel();
            int? profileId = accountModel != null ? accountModel.ProfileId : PortalAgent.CurrentPortal.DefaultAnonymousProfileId;
           
            var list = _categoriesClient.GetCategoriesByCatalog(PortalAgent.CurrentPortal.CatalogId, new ExpandCollection() { ExpandKeys.CategoryNodes, ExpandKeys.CategoryProfiles }, new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.IsVisible, FilterOperators.Equals,"true"),

                        },  null);


			if (list.Categories != null)
            {
                var tcategories =
					list.Categories.Where(
                        p =>
                        p.CategoryProfiles.Any(y => y.ProfileID == profileId && y.EffectiveDate > System.DateTime.Now))
                              .ToList();


                return CategoryListViewModelMap.ToViewModel(list.Categories.Except(tcategories), PortalAgent.CurrentPortal.CatalogId);
            }
            return new CategoryListViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorNoCategoryListFound };
        }

        /// <summary>
        /// Get the category based on the category id.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns>Returns the category as category view model</returns>
        public CategoryViewModel GetCategory(int categoryId)
        {
            var categoryResult = _categoriesClient.GetCategory(categoryId, _expands);
          
            if (categoryResult != null)
            {
                return CategoryViewModelMap.ToCategoryViewModel(categoryResult);
            }

            return new CategoryViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorNoCategoryFound };
        }

        /// <summary>
        /// Get the category based on the category name.
        /// </summary>
        /// <param name="categoryId"></param>
        /// <returns>Returns the category as category view model</returns>
        public CategoryViewModel GetCategoryByName(string categoryName)
        {
            var categoryList = _categoriesClient.GetCategories(null, new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.Name, FilterOperators.Equals,categoryName),

                        }, null);

            if (categoryList != null && categoryList.Categories != null)
            {
                var categoryDetails = CategoryViewModelMap.ToCategoryViewModel(categoryList.Categories.First());
                if(categoryDetails != null)
                {
                    categoryDetails.Title =
                          !string.IsNullOrEmpty(categoryDetails.SeoTitle)
                                 ? categoryDetails.SeoTitle
                                 : categoryName;
                    return categoryDetails;
                }
            }

            return new CategoryViewModel() { HasError = true, ErrorMessage = Resources.ZnodeResource.ErrorNoCategoryFound };
        }
    }
}