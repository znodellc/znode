﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Web;
using Znode.Engine.Api.Client.Expands;
using Znode.Engine.Api.Client.Filters;
using Znode.Engine.Api.Client.Sorts;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;
using Znode.Engine.Api.Client;
namespace Znode.Engine.MvcDemo.Agents
{
    public class ShippingOptionAgent : BaseAgent, IShippingOptionAgent
    {
        private readonly IAccountAgent _accountAgent;
        private readonly IShippingOptionsClient _shippingOptionsClient;

        public ShippingOptionAgent()
        {
            _shippingOptionsClient = GetClient<ShippingOptionsClient>();
            _accountAgent = new AccountAgent();
        }

        /// <summary>
        /// Get the shipping List for current logged in account profile
        /// </summary>        
        /// <returns>Returns the shipping list based on the profile id</returns>
        public ShippingOptionListViewModel GetShippingList()
        {
            // Get All profile shipping options.
            var allList = _shippingOptionsClient.GetShippingOptions(null, new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, "null"),
                            new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, "true" )
                        }, null);

            var accountModel = _accountAgent.GetAccountViewModel();

            if (allList == null)
                allList = new ShippingOptionListModel();

			if (allList.ShippingOptions == null)
                allList.ShippingOptions = new Collection<ShippingOptionModel>();
				
            // Get Account ProfileID, or Get Default Anonymous ProfileId from Portal
            int? profileId = null;
            if (accountModel != null)
            {
                profileId = accountModel.ProfileId;
            }
            else
            {
                profileId = PortalAgent.CurrentPortal.DefaultAnonymousProfileId;
            }

            // Get Profile based options and merge with All Profile options.
            if (profileId.HasValue)
            {
                var profileList = _shippingOptionsClient.GetShippingOptions(null,
                    new FilterCollection()
                        {
                            new FilterTuple(FilterKeys.ProfileId, FilterOperators.Equals, profileId.ToString()),
                            new FilterTuple(FilterKeys.IsActive, FilterOperators.Equals, "true")
                        },
                    new SortCollection());

                if (profileList.ShippingOptions != null && profileList.ShippingOptions.Any())
                {
                    allList.ShippingOptions = new Collection<ShippingOptionModel>(allList.ShippingOptions.Union(profileList.ShippingOptions).ToList());
                }
            }

            return ShippingOptionMap.ToViewModel(allList.ShippingOptions.OrderBy(x => x.DisplayOrder).ToList());
        }

        /// <summary>
        /// Get the shipping options based on the shipping option id
        /// </summary>
        /// <param name="shippingOptionId">Shipping Option ID</param>
        /// <returns>Returns the Shipping Option as Shipping Option View model</returns>
        public ShippingOptionViewModel GetShippingOption(int shippingOptionId)
        {
            var shippingOption = _shippingOptionsClient.GetShippingOption(shippingOptionId,null);

            return ShippingOptionMap.ToViewModel(shippingOption);
        }

      
    }
}