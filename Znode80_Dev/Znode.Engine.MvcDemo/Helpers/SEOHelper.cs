﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcDemo.Helpers
{
    public static class SEOHelper
    {
        public static string GetCategoryUrl(this UrlHelper url, string page, string category)
        {
            return string.IsNullOrEmpty(page) 
                ? url.RouteUrl("category-details", new { category }) 
                : url.RouteUrl("seocategory-details", new { controller = "Category", action ="Index", seo = page.ToLower(), category });
        }

        public static string GetProductUrl(this UrlHelper url, string page, string id)
        {
            return string.IsNullOrEmpty(page)
                ? url.RouteUrl("product-details", new { id })
                : url.RouteUrl("seoproduct-details", new { controller = "Product", action = "Details", seo = page.ToLower(), id });
        }

    }
}