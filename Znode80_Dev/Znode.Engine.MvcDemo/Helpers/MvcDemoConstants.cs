﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.MvcDemo.Agents;

namespace Znode.Engine.MvcDemo.Helpers
{
    public static class MvcDemoConstants
    {

        public static string CartModelSessionKey = "ShoppingCartModel";
        public static string CartViewModelSessionKey = "ShoppingCartModel";
        public static string StatesSessionKey = "States";
        public static string CartCookieKey = "CookieMappingID";
        public static string CartMerged = "CookieMerged";
        public static string PaypalSessionKey = "PaypalModel";

        public static int DefaultPageSize = 12;
        public static int DefaultPageNumber = 1;
        public static int DefaultHomeSpecialsSize = 10;

        public static string ShippingAddressKey = "ShippingAddress";
        public static string BillingAddressKey = "BillingAddress";

        public static string RouteBillingAddress = "/checkout/billingaddress";
        public static string RouteGuestCheckoutShipping = "/checkout/shipping";

        public static string RouteShoppingCart = "Cart";
        public static string RouteCheckOutPaypal = "checkout/Paypal";

        public static string AccountKey = "UserAccount";

        public static int DashboardOrderCount = 3;

        public static string CategoryName = "CategoryName";

        public static string AddressLink = "/account/editAddress/{0}";

        public static int ReviewStarCount = 5;

        public static int ReviewCommentsCount = 300;

        public static string DefaultReviewStatus = "N";

        public static string ApprovedReviewStatus = "A";

        public static long ReviewTimeSixty = 60;

        public static long ReviewTimeOneTwenty = 120;

        public static long ReviewTimeTwoSevenHundred = 2700;

        public static long ReviewTimeFiveFourHundred = 5400;

        public static long ReviewTimeEightySixFourHundred = 86400;

        public static long ReviewTimeOneSeventyTwoEightHundred = 172800;

        public static long ReviewTimeTwentyFiveNinetyTwo = 2592000;

        public static long ReviewTimeThreeoneonezeroFour = 31104000;

        public static int DaysInMonth = 30;

        public static int DaysInYear = 365;

        public static int ProductReviewCount = 4;

        public static string AddonDisplayTypeDropDown = "DropDownList";

        public static string AddonDisplayTypeRadioButton = "RadioButton";

        public static string AddonDisplayTypeCheckBox = "CheckBox";

        public static string NoImageName = "noimage.gif";

        public static string LoginPage = "/account/login";

        public static string PaypalPaymentType = "paypal";

        //Znode Version 7.2.2
        public static int MaxRecentViewItemToDisplay = PortalAgent.CurrentPortal.MaxRecentViewItemToDisplay;
        public static string CountriesSessionKey = "Countries";
        public static string SortAscending = "ASC";
        public static string NoSwatchImageName = "noimage-swatch.gif";
        public static string MultiCartModelSessionKey = "MultiShoppingCartModel";
        public static string CheckoutShippingBilling = "/checkout/shipping";
        public static string RouteMultipleShipto = "/checkout/Multipleshipto";
    }
}