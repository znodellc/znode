﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace Znode.Engine.MvcDemo.Helpers
{
    public static class HelperMethods
    {

        public static string GetImagePath(string image)
        {

            if (!string.IsNullOrEmpty(image))
            {
                return image.Replace("~", ConfigurationManager.AppSettings["ZnodeApiRootUri"]);
            }
            // Provide a static image 
            return string.Empty;
        }

       
    }
}