﻿
namespace Znode.Engine.MvcDemo.Helpers
{
    #region Reset Password Status Types
    public enum ResetPasswordStatusTypes
    {
        Continue = 2001,
        LinkExpired = 2002,
        TokenMismatch = 2003,
        NoRecord = 2004,
    }
    #endregion
}