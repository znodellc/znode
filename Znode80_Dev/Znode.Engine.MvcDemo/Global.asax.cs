﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using System.Text.RegularExpressions;

namespace Znode.Engine.MvcDemo
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            MvcConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
            var currentUrl = string.Format("~{0}", Request.Path.ToLower());

            var redirectUrl = UrlRedirectAgent.Has301Redirect(currentUrl);
            if (redirectUrl != null)
                Context.Response.RedirectPermanent(redirectUrl.NewUrl);
        }

        private void Session_Start(object sender, EventArgs e)
        {
            string id = Session.SessionID;

            if (PortalAgent.CurrentPortal.PersistentCartEnabled)
            {
                var _cartAgent = new CartAgent();

                var cartViewModel = _cartAgent.GetCart();

                if (cartViewModel != null && cartViewModel.Items.Any())
                {
                    Session[MvcDemoConstants.CartMerged] = true;
                    Response.Redirect("~/Cart");
                }
            }
        }

        public override string GetVaryByCustomString(HttpContext context, string custom)
        {
            var customstring = string.Empty;
            if (custom.ToLower().Contains("portal"))
            {
                customstring = context.Request.Url.Authority;
            }

            if (custom.ToLower().Contains("account") && HttpContext.Current.User.Identity.IsAuthenticated)
            {
                customstring += context.User.Identity.Name;
            }

            if (!string.IsNullOrEmpty(customstring))
            {
                return customstring;
            }

            return base.GetVaryByCustomString(context, custom);
        }
    }
}
