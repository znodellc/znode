﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class CategoryListViewModelMap
    {

        public static CategoryListViewModel ToViewModel(IEnumerable<CategoryModel> models, int catalogId)
        {
            var viewModel = new CategoryListViewModel()
                {
                    Categories = models.Where(p => p.CategoryNodes.Any(y => y.ParentCategoryNodeId == null && y.IsActive && y.CatalogId == catalogId)).ToList().Select(x =>
                            new CategoryHeaderViewModel()
                                {
                                    CategoryId = x.CategoryId, 
                                    CategoryName = x.Name,
                                    SEOPageName = x.SeoUrl
                                }).ToList()
                };

            return viewModel;
        }
    }
}