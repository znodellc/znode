﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Resources;
using Znode.Engine.MvcDemo.ViewModels;
using System.Web.Mvc;
namespace Znode.Engine.MvcDemo.Maps
{
    public static class RegisterViewModelMap
    {

        public static RegisterViewModel ToViewModel()
        {
            return new RegisterViewModel()
                {
                    SecurityQuestionList = GetSecurityQuestionlist(),
                };
        }


        public static IEnumerable<SelectListItem> GetSecurityQuestionlist()
        {
            return new[] {   
                new SelectListItem { Text = ZnodeResource.SecurityQuestion1, Selected = true},
                new SelectListItem { Text =ZnodeResource.SecurityQuestion2},
                new SelectListItem { Text = ZnodeResource.SecurityQuestion3},
                new SelectListItem { Text = ZnodeResource.SecurityQuestion4},
                new SelectListItem { Text =ZnodeResource.SecurityQuestion5},
                new SelectListItem { Text = ZnodeResource.SecurityQuestion6},
                new SelectListItem { Text = ZnodeResource.SecurityQuestion7 }
                };
        } 

        public static RegisterViewModel ToModel(AccountViewModel model)
        {
            return new RegisterViewModel()
                {
                    EmailAddress = model.EmailAddress,
                    UserName = model.UserName
                };
        }
    }
}