﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class ProductViewModelMap
    {
        #region Constant Variables
        private const string _OriginalPriceTextinLowercase = "originalprice";
        #endregion

        /// <summary>
        /// Convert ProductModel to ProductViewModel
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static ProductViewModel ToViewModel(ProductModel model)
        {
            var viewModel = new ProductViewModel()
                {
                    BundleItemsIds = model.BundledProductIds,
                    Description = model.Description,
                    IsCallForPricing = model.CallForPricing,
                    ImageAltTag = model.ImageAltTag,
                    ImageFile = model.ImageFile,
                    ImageLargePath = HelperMethods.GetImagePath(model.ImageLargePath),
                    ImageMediumPath = HelperMethods.GetImagePath((model.Skus != null && model.Skus.Any() && Path.GetFileName(model.Skus.First().ImageMediumPath) != MvcDemoConstants.NoImageName) ? HelperMethods.GetImagePath(model.Skus.First().ImageMediumPath) : HelperMethods.GetImagePath(model.ImageMediumPath)),
                    MaxQuantity = model.MaxSelectableQuantity.GetValueOrDefault(10),
                    MinQuantity = model.MinSelectableQuantity.GetValueOrDefault(1),
                    Name = model.Name,
                    OutOfStockMessage = model.OutOfStockMessage,
                    Price = GetProductPrice(model, "originalprice"),
                    ProductPrice = GetProductPrice(model, "price"),
                    OriginalPrice = GetProductPrice(model, "originalprice"),
                    ProductId = model.ProductId,
                    ProductNumber = model.ProductNumber,
                    SeoDescription = model.SeoDescription,
                    SeoKeywords = model.SeoKeywords,
                    SeoTitle = model.SeoTitle,
                    SeoPageName = model.SeoUrl,
                    ShortDescription = model.ShortDescription,
                    IsActive = model.IsActive,
                    AllowBackOrder = model.AllowBackOrder,
                    TrackInventory = model.TrackInventory,
                    BackOrderMessage = model.BackOrderMessage,
                    InStockMessage = model.InStockMessage,
                    Images = ToImageViewModel(model.Images),
                    //Znode Version 7.2.2
                    //Add ImageSmallThumbnail Path to Set Swatch Images
                    ImageSmallThumbnailPath = HelperMethods.GetImagePath((model.Skus != null && model.Skus.Any() && (Path.GetFileName(model.Skus.First().ImageSmallThumbnailPath) != MvcDemoConstants.NoImageName && Path.GetFileName(model.Skus.First().ImageSmallThumbnailPath) != MvcDemoConstants.NoSwatchImageName)) ? HelperMethods.GetImagePath(model.Skus.First().ImageSmallThumbnailPath) : HelperMethods.GetImagePath(model.ImageSmallThumbnailPath)),
                    // Added product specification data
                    ProductSpecification = model.Specifications
                };

            if (model.Skus.Count > 0)
            {
                viewModel.Sku = model.Skus.FirstOrDefault().Sku;
            }

            viewModel.ProductAttributes =   ProductAttributeViewModelMap.ToViewModel(model.Attributes, model);

            if (model.AddOns != null && model.AddOns.Count > 0)
            {
                viewModel.AddOns = model.AddOns.Select(AddOnViewModelMap.ToViewModel).Where(x => x != null).ToList();
            }

            if (model.Reviews.Any())
            {
                var reviewModel = model.Reviews.Where(x => x.Status == MvcDemoConstants.ApprovedReviewStatus).OrderByDescending(x => x.CreateDate);
                viewModel.Rating = reviewModel.Any() ? Convert.ToInt16(reviewModel.Average(x => x.Rating)) : 0;
                viewModel.ProductReviews = AccountViewModelMap.ToReviewViewModel(new Collection<ReviewModel>(reviewModel.ToList()));
            }
            else
            {
                viewModel.Rating = 0;
                viewModel.ProductReviews = null;
            }

            if (model.FrequentlyBoughtTogether.Any())
            {
                viewModel.FBTProductsIds = string.Join(",",model.FrequentlyBoughtTogether.Select(fbt => fbt.RelatedProductId.ToString()));

            }
            if (model.YouMayAlsoLike.Any())
            {
                viewModel.YMALProductsIds = string.Join(",",model.YouMayAlsoLike.Select(ymal => ymal.RelatedProductId.ToString()));
            }
           
               
            return viewModel;
        }

        /// <summary>
        /// Convert multiple Product models to list of product view models
        /// </summary>
        /// <param name="models"></param>
        /// <returns></returns>
        public static List<ProductViewModel> ToViewModels(Collection<ProductModel> models)
        {
            if (models == null)
            {
                return null;
            }
            //Znode 7.2.2
            //Showing Revies and Ratings on Home page-Starts
            var list = new List<ProductViewModel>();
       
            foreach (ProductModel model in models)
            {
                list.Add(ToViewModel(model));
            }
            //Showing Revies and Ratings on Home page-Ends
            return list;
        }

        /// <summary>
        /// Convert list cross sell items model to list of cross sell items view model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static Collection<CrossSellViewModel> ToCrossSellViewModel(Collection<CrossSellModel> model)
        {
            var crossSellList =
                new Collection<CrossSellViewModel>(model.Select(x => new CrossSellViewModel()
                {
                    DisplayOrder = x.DisplayOrder,
                    ProductCrossSellTypeId = x.ProductCrossSellTypeId,
                    ProductId = x.ProductId,
                    RelatedProductId = x.RelatedProductId,
                    RelationTypeId = x.RelationTypeId,
                    ProductRelation = (int) x.ProductRelation,
                }).ToList());

            return crossSellList;
        }

        /// <summary>
        /// Convert list of image model to list of image view model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private static List<ImageViewModel> ToImageViewModel(Collection<ImageModel> model)
        {
            var imageViewModel = model.Select(x => new ImageViewModel()
                {
                    AlternateThumbnailImageFile = x.AlternateThumbnailImageFile,
                    DisplayOrder = x.DisplayOrder,
                    ImageAltTag = x.ImageAltTag,
                    ImageFile = x.ImageFile,
                    ImageLargePath = HelperMethods.GetImagePath(x.ImageLargePath),
                    ImageMediumPath = HelperMethods.GetImagePath(x.ImageMediumPath),
                    ImageSmallPath = HelperMethods.GetImagePath(x.ImageSmallPath),
                    ImageSmallThumbnailPath = HelperMethods.GetImagePath(x.ImageSmallThumbnailPath),
                    ImageThumbnailPath = HelperMethods.GetImagePath(x.ImageThumbnailPath),
                    IsActive = x.IsActive,
                    Name = x.Name,
                    ProductId = x.ProductId,
                    ProductImageId = x.ProductImageId,
                    ProductImageTypeId = x.ProductImageTypeId,
                    ReviewStateId = x.ReviewStateId,
                    ShowOnCategoryPage = x.ShowOnCategoryPage
                }).ToList();

            return imageViewModel;
        }

        /// <summary>
        /// Get the product price based on product model and price type
        /// </summary>
        /// <param name="model">ProductModel model</param>
        /// <param name="priceType">string price type</param>
        /// <returns>decimal product price</returns>
        private static decimal GetProductPrice(ProductModel model, string priceType)
        {
            decimal finalPrice = 0.00m;

            if (priceType.ToLower().Equals(_OriginalPriceTextinLowercase))
            {
                finalPrice = model.RetailPrice.HasValue ? model.RetailPrice.Value : 0;
                return finalPrice;
            }
            
            if (model.PromotionPrice.HasValue )
            {
                finalPrice = model.PromotionPrice.Value;
                return finalPrice;
            }

            if (model.SalePrice.HasValue )
            {
                finalPrice = model.SalePrice.Value;
                return finalPrice;
            }

            finalPrice = model.RetailPrice.HasValue ? model.RetailPrice.Value : 0;
            return finalPrice;
        }
    }
}