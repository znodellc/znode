﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class BundleViewModelMap
    {
        public static List<BundleDisplayViewModel> GetBundleDisplayViewModel(IEnumerable<ProductModel> models)
        {
            if (models != null)
            {
               return models.Select(product => new BundleDisplayViewModel()
                    {
                        Name = product.Name,
                        ImageAltTag = product.ImageAltTag,
                        ImageFile = product.ImageFile,
                        ImageLargePath = HelperMethods.GetImagePath(product.ImageLargePath),
                        ImageMediumPath = HelperMethods.GetImagePath(product.ImageMediumPath),
                        ImageSmallPath = HelperMethods.GetImagePath(product.ImageSmallPath),

                    }).ToList();

            }

            return null;
        }
    }
}