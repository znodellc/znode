﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class PaymentsViewModelMap  
    {
        public static PaymentModel ToViewModel(PaymentOptionViewModel paymentOptionViewModel)
        {
            var model = new PaymentModel()
            {
                PaymentOption = ToPaymentOptionsModel(paymentOptionViewModel),
                CreditCard = ToCreditCartModel(paymentOptionViewModel),
                PaymentName = paymentOptionViewModel.PaymentName
            };

            return model;
        }

        public static PaymentOptionModel ToPaymentOptionsModel(PaymentOptionViewModel paymentOptionModel)
        {
            var model = new PaymentOptionModel()
                {
                    PaymentType = new PaymentTypeModel() {PaymentTypeId = paymentOptionModel.PaymentTypeId},
                    PaymentOptionId = paymentOptionModel.PaymentOptionId,
                    PaymentTypeId = paymentOptionModel.PaymentTypeId
                };

            return model;
        }

        public static CreditCardModel ToCreditCartModel(PaymentOptionViewModel paymentOptionViewModel)
        {
            var model = new CreditCardModel()
                {
                    CardNumber = paymentOptionViewModel.CreditCardModel.CardNumber,
                    CardExpiration =
                        paymentOptionViewModel.CreditCardModel.CardExpirationMonth + "/" +
                        paymentOptionViewModel.CreditCardModel.CardExpirationYear,
                    CardSecurityCode = paymentOptionViewModel.CreditCardModel.CardSecurityCode
                };

            return model;
        }

        public static PaymentsViewModel ToPaymentsViewModel(AddressViewModel billAddress, AddressViewModel shipAddress,
                                                           ShoppingCartModel shoppingCart, CartViewModel cart,
                                                           Collection<PaymentOptionModel> allPaymentOptions)
        {
            return new PaymentsViewModel()
                {
                    BillingAddress = billAddress,
                    ShippingAddress = shipAddress,
                    Cart = new ReviewOrderViewModel()
                        {
                            ShoppingCart = cart,
                            ShippingOption =
                                new ShippingOptionViewModel()
                                    {
                                        ShippingDescription = shoppingCart.Shipping.ShippingName,
                                        ShippingId = shoppingCart.Shipping.ShippingOptionId
                                    },
                        },

                    PaymentOptions =
                        new Collection<PaymentOptionViewModel>(
                            allPaymentOptions.Where(x => x != null).Select(
                                x =>
                                new PaymentOptionViewModel()
                                    {
                                        PaymentName = x.PaymentType.Name,
                                        PaymentTypeId = x.PaymentTypeId,
                                        PaymentOptionId = x.PaymentOptionId
                                    }).ToList()),
                };

        }
    }
}