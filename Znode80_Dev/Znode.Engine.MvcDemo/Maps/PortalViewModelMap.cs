﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class PortalViewModelMap
    {
        public static PortalViewModel ToModel(PortalModel model)
        {
            return new PortalViewModel()
                {
                    PortalId = model.PortalId,
                    Name = model.StoreName,
                    PersistentCartEnabled = model.PersistentCartEnabled.GetValueOrDefault(false),
                    CatalogId = model.Catalogs.First().CatalogId,
                    DefaultAnonymousProfileId = model.DefaultAnonymousProfileId.GetValueOrDefault(0),
                    MaxRecentViewItemToDisplay = model.MaxCatalogCategoryDisplayThumbnails
                };
        }
    }
}