﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class AddOnViewModelMap
    {

        public static AddOnViewModel ToViewModel(AddOnModel model)
        {
            AddOnViewModel viewModel = null;

            if (model == null)
            {
                return null;
            }

            if (model.AddOnValues.Any())
            {
                viewModel = new AddOnViewModel
                    {
                        AddOnId = model.AddOnId,
                        Description = model.Description,
                        DisplayType = model.DisplayType,
                        IsRequired = model.IsOptional,
                        OptionalIndicator = model.IsOptional,
                        Title = model.Title,
                        SelectedAddOnValue = model.IsOptional ? null : GetDefaultSelectedAddOnValues(model.AddOnValues),
                        AllowBackOrder = model.AllowBackOrder,
                        TrackInventory = model.TrackInventory,
                        InStockMessage = model.InStockMessage,
                        OutOfStockMessage = model.OutOfStockMessage,
                        BackOrderMessage = model.BackOrderMessage,
                        PromptMessage = model.PromptMessage,
                        AddOnValues = model.AddOnValues.Select(ToAddonValuesViewModel).OrderBy(x => x.DisplayOrder).ToList(),
                        Name = model.Name
                    };
            }

            return viewModel;
        }

        public static AddOnValuesViewModel ToAddonValuesViewModel(AddOnValueModel model)
        {
            return new AddOnValuesViewModel()
                    {
                        AddOnValueId = model.AddOnValueId,
                        Name = model.Name,
                        DefaultIndicator = model.IsDefault,
                        CustomText = model.CustomText,
                        RetailPrice = model.RetailPrice,
                        SalePrice = model.SalePrice,
                        WholeSalePrice = model.WholesalePrice,
                        SKU = model.SKU,
                        QuantityOnHand = model.QuantityOnHand,
                        DisplayOrder = model.DisplayOrder
                    };
        }

        private static int[] GetDefaultSelectedAddOnValues(Collection<AddOnValueModel> addOnValues)
        {
            var defaultAddOnValue = 0;

            var addOnValuesViewModel = addOnValues.FirstOrDefault(x => x.IsDefault);

            if (addOnValuesViewModel != null)
            {
                defaultAddOnValue = addOnValuesViewModel.AddOnValueId;
            }
            else
            {
                var addOnValueModel = addOnValues.OrderBy(x => x.DisplayOrder).FirstOrDefault();

                if (addOnValueModel != null)
                {
                    defaultAddOnValue = addOnValueModel.AddOnValueId;
                }
            }

            return new int[] { defaultAddOnValue };
        }
    }
}