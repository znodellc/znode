﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class CartItemViewModelMap
    {

        public static CartItemViewModel ToViewModel(ShoppingCartItemModel model)
        {
            var viewModel = new CartItemViewModel()
                {
                    Description = model.CartDescription,
                    ExternalId = model.ExternalId,
                    AttributeIds = model.AttributeIds,
                    ProductId = model.ProductId,
                    Quantity = model.Quantity,
                    ShippingCost = model.ShippingCost,
                    Sku = model.Sku,
                    UnitPrice = model.UnitPrice,
                    ExtendedPrice = model.ExtendedPrice,
                    InsufficientQuantity = model.InsufficientQuantity,
                    AddOnValueIds = model.AddOnValueIds != null ? string.Join(",", model.AddOnValueIds) : string.Empty,
                    //ZNode Version 7.2.2 - Map SKU Image Path
                    ImagePath = HelperMethods.GetImagePath(model.ImagePath),
                };

            if (model.BundleItems != null && model.BundleItems.Any())
            {
                viewModel.SelectedBundles =
                    new Collection<SelectedBundleViewModel>(model.BundleItems.Select(ToBundleItemViewModel).ToList());
            }

            return viewModel;
        }

        public static ShoppingCartItemModel ToModel(CartItemViewModel viewModel)
        {
            var itemModel = new ShoppingCartItemModel()
            {
                
                ExternalId = viewModel.ExternalId,
                AttributeIds = viewModel.AttributeIds,
                ProductId = viewModel.ProductId,
                Quantity = viewModel.Quantity,
                ShippingCost = viewModel.ShippingCost,
                Sku = viewModel.Sku,
                UnitPrice = viewModel.UnitPrice,
                ExtendedPrice = viewModel.ExtendedPrice,
                AddOnValueIds = (!string.IsNullOrEmpty(viewModel.AddOnValueIds) ? viewModel.AddOnValueIds.Split( new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToArray() : null),
               
            };

            if (viewModel.SelectedBundles != null && viewModel.SelectedBundles.Any())
            {
                itemModel.BundleItems =
                    new Collection<BundleItemModel>(viewModel.SelectedBundles.Select(ToBundleItemModel).ToList());
            }
            return itemModel;
        }

        public static BundleItemModel ToBundleItemModel(SelectedBundleViewModel model)
        {
            var itemModel = new BundleItemModel()
            {
                AddOnValueIds = model.AddOnValueIds,
                ProductId = model.ProductId,
                Sku = model.Sku,
                ExternalId = model.ExternalId
            };
            return itemModel;
        }

        public static SelectedBundleViewModel ToBundleItemViewModel(BundleItemModel model)
        {
            var itemModel = new SelectedBundleViewModel()
            {
                AddOnValueIds = model.AddOnValueIds,
                ProductId = model.ProductId,
                Sku = model.Sku,
                ExternalId = model.ExternalId
            };
            return itemModel;
        }

        public static ShoppingCartItemModel ToShoppingCartModel(int productId, int quantity, string sku)
        {
            var itemModel = new ShoppingCartItemModel()
                {
                    ProductId = productId,
                    Sku = sku,
                    Quantity = quantity,
                };
            return itemModel;
        }

        public static CartItemViewModel ToViewModel(BundleDetailsViewModel bundleDetailsView)
        {
            var itemViewModel = new CartItemViewModel()
                {
                    AttributeIds = bundleDetailsView.SelectedAttributesIds,
                    ProductId = bundleDetailsView.SelectedProductId,
                    Quantity = bundleDetailsView.SelectedQuantity,
                    Sku = bundleDetailsView.SelectedSku,
                    AddOnValueIds = bundleDetailsView.SelectedAddOnValueIds,

                };

            if (bundleDetailsView.Bundles != null && bundleDetailsView.Bundles.Any())
            {
                itemViewModel.SelectedBundles = new Collection<SelectedBundleViewModel>();
                foreach (var bundle in bundleDetailsView.Bundles)
                {
                    var bundleAttributes =
                        bundle.ProductAttributes.Attributes.Select(x => x.SelectedAttributeId).Where(x => x != null);
                    itemViewModel.SelectedBundles.Add(new SelectedBundleViewModel()
                        {
                            SelectedAttributes = bundleAttributes.Any() ? string.Join(",", bundleAttributes.ToArray()) : "",
                            ProductId = bundle.ProductId,
                            AddOnValueIds = bundle.AddOns != null ? bundle.AddOns.Where(y => y.SelectedAddOnValue != null).SelectMany(x => x.SelectedAddOnValue).ToArray() : null,
                            Sku = bundle.Sku
                        });
                }
            }
            return itemViewModel;
        }
    }
}