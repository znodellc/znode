﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public class CategoryViewModelMap
    {
        public static CategoryViewModel ToCategoryViewModel(CategoryModel searchCategoryModel)
        {
            var category = new CategoryViewModel()
            {
               CategoryId =  searchCategoryModel.CategoryId,
               Name =  searchCategoryModel.Name,
               Title = searchCategoryModel.Title,
               ShortDescription = searchCategoryModel.ShortDescription,
               SeoTitle =  searchCategoryModel.SeoTitle,
               SeoUrl = searchCategoryModel.SeoUrl,
               SeoKeywords = searchCategoryModel.SeoKeywords,
               SeoDescription = searchCategoryModel.SeoDescription,
               Subcategories = PopulateCategory(searchCategoryModel.Subcategories),
                //Znode version 7.2.2 To map Category Banner
               CategoryBanner=searchCategoryModel.CategoryBanner,
            };

            return category;
        }

        private static Collection<CategoryViewModel> PopulateCategory(Collection<CategoryModel> categoryModel)
        {
            if (categoryModel != null && categoryModel.Any())
            {
                var categoryViewList = categoryModel.Select(x => new CategoryViewModel()
                {
                    Name = x.Name,
                    Title =  x.Title,
                    CategoryId =  x.CategoryId,
                    SeoTitle = x.SeoTitle,
                    SeoUrl = x.SeoUrl,
                    Subcategories = PopulateCategory(x.Subcategories)
                }).ToList();

                if (categoryViewList.Any())
                    return new Collection<CategoryViewModel>(categoryViewList);
            }

            return new Collection<CategoryViewModel>();
        }
    }
}