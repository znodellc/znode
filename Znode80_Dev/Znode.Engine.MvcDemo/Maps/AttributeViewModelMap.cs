﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class AttributeViewModelMap
    {
        public static AttributeViewModel ToEntity(AttributeModel model)
        {
            if (model != null)
            {

                return  new AttributeViewModel()
                    {
                        AttributeId = model.AttributeId,
                        DisplayOrder = model.DisplayOrder,
                        AttributeTypeId   = model.AttributeTypeId,
                        Name = model.Name

                    };

            }

            return null;
        }
    }
}