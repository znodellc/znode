﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Maps
{
    public static class AddressViewModelMap
    {

        public static  AddressModel ToModel(AddressViewModel viewModel)
        {
            var model = new AddressModel()
                {
                    AccountId = viewModel.AccountId,
                    AddressId = viewModel.AddressId,
                    City = viewModel.City,
                    CompanyName = viewModel.CompanyName,
                    FirstName = viewModel.FirstName,
                    IsDefaultBilling = viewModel.IsDefaultBilling,
                    IsDefaultShipping = viewModel.IsDefaultShipping,
                    LastName = viewModel.LastName,
                    Name = viewModel.Name,
                    MiddleName = viewModel.MiddleName,
                    PhoneNumber = viewModel.PhoneNumber,
                    PostalCode = viewModel.PostalCode,
                    StateCode = viewModel.StateCode,
                    StreetAddress1 = viewModel.StreetAddress1,
                    StreetAddress2 = viewModel.StreetAddress2,
                    CountryCode = viewModel.CountryCode,                    
                    
                };
            return model;
        }

        public static AddressViewModel ToModel(AddressModel viewModel)
        {
            var model = new AddressViewModel()
            {
                AccountId = viewModel.AccountId,
                AddressId = viewModel.AddressId,
                City = viewModel.City,
                CompanyName = viewModel.CompanyName,
                FirstName = viewModel.FirstName,
                IsDefaultBilling = viewModel.IsDefaultBilling,
                IsDefaultShipping = viewModel.IsDefaultShipping,
                LastName = viewModel.LastName,
                Name = viewModel.Name,
                MiddleName = viewModel.MiddleName,
                PhoneNumber = viewModel.PhoneNumber,
                PostalCode = viewModel.PostalCode,
                StateCode = viewModel.StateCode,
                StreetAddress1 = viewModel.StreetAddress1,
                StreetAddress2 = viewModel.StreetAddress2,
                CountryCode = viewModel.CountryCode,                
            };
            return model;
        }

        public static AddressViewModel ToModel(OrderModel viewModel)
        {
            var model = new AddressViewModel()
            {
                City = viewModel.BillingCity,
                CompanyName = viewModel.BillingCompanyName,
                FirstName = viewModel.BillingFirstName,
                LastName = viewModel.BillingLastName,
                PhoneNumber = viewModel.BillingPhoneNumber,
                PostalCode = viewModel.BillingPostalCode,
                StateCode = viewModel.BillingStateCode,
                StreetAddress1 = viewModel.BillingStreetAddress1,
                StreetAddress2 = viewModel.BillingStreetAddress2,
                CountryCode = viewModel.BillingCountryCode
            };
            return model;
        }

        public static AddressViewModel ToModel(OrderShipmentModel viewModel)
        {
            var model = new AddressViewModel()
            {
                City = viewModel.ShipToCity,
                CompanyName = viewModel.ShipToCompanyName,
                FirstName = viewModel.ShipToFirstName,
                LastName = viewModel.ShipToLastName,
                PhoneNumber = viewModel.ShipToPhoneNumber,
                PostalCode = viewModel.ShipToPostalCode,
                StateCode = viewModel.ShipToStateCode,
                StreetAddress1 = viewModel.ShipToStreetAddress1,
                StreetAddress2 = viewModel.ShipToStreetAddress2,
                CountryCode = viewModel.ShipToCountryCode
            };
            return model;
        }
    }
}