﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class CartViewModel : BaseViewModel
    {
        public string Coupon { get; set; }
        public string CouponMessage { get; set; }
        public bool CouponApplied { get; set; }
        public bool CouponValid { get; set; }
        public bool GiftCardApplied { get; set; }
        public decimal GiftCardAmount { get; set; }
        public string GiftCardMessage { get; set; }
        public string GiftCardNumber { get; set; }
        public bool GiftCardValid { get; set; }
        public decimal GiftCardBalance { get; set; }
        public int CookieId { get; set; }
        public decimal? SubTotal { get; set; }
        public decimal TaxCost { get; set; }
        public decimal TaxRate { get; set; }
        public decimal Discount { get; set; }
        public decimal? Total { get; set; }
        public decimal? Vat { get; set; }
        public int Count  {get; set;}
        public Collection<CartItemViewModel> Items { get; set; }
        public Collection<AddressViewModel> Addresses { get; set; }
        public decimal ShippingCost { get; set; }

        public CartViewModel()
        {
            Items= new Collection<CartItemViewModel>();
            Addresses = new Collection<AddressViewModel>();
        }
    }
}