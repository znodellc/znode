﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class CategoryListViewModel : BaseViewModel
    {
        public CategoryListViewModel()
        {
            Categories = new List<CategoryHeaderViewModel>();
        }

        public List<CategoryHeaderViewModel> Categories { set; get; }
    }
}