﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class BundleDetailsViewModel : BaseViewModel
    {

        public string BundleItemsIds { get; set; }
        public int SelectedProductId { get; set; }
        public int SelectedQuantity { get; set; }
        public string SelectedSku { get; set; }
        public string SelectedAddOnValueIds { get; set; }
        public int[] SelectedAttributesIds { get; set; }
        public IEnumerable<ProductViewModel> Bundles { get; set; }

        public string SelectedBundleAttributeId { get; set; }
        public string SelectedBundleProductId { get; set; }

        public string ProductName { get; set; }

        public BundleDetailsViewModel()
        {
            Bundles = new Collection<ProductViewModel>();
        
        }
    }
}