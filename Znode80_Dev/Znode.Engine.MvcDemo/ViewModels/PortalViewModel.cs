﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PortalViewModel : BaseViewModel
    {
        public int PortalId { get; set; }
        public string Name { get; set; }
        public bool PersistentCartEnabled { get; set; }
        public int CatalogId { get; set; }
        public int DefaultAnonymousProfileId { get; set; }
        public int MaxRecentViewItemToDisplay { get; set; }
    }
}