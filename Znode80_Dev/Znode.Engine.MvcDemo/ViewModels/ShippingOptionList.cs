﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class ShippingOptionListViewModel : BaseViewModel
    {

        public ShippingOptionListViewModel()
        {
            ShippingOptions = new List<ShippingOptionViewModel>();
        }

        public List<ShippingOptionViewModel> ShippingOptions { get; set; }

        public AddressViewModel ShippingAddress { get; set; }

    }
}