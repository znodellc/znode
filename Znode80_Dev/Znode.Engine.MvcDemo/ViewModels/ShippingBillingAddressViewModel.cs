﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class ShippingBillingAddressViewModel : BaseViewModel
    {
        public AddressViewModel ShippingAddressModel { get; set; }
        public AddressViewModel BillingAddressModel { get; set; }
        [Required]
        public string EmailAddress { get; set; }
        public int ShippingAddressId { get; set; }
        public int BillingAddressId { get; set; }

        public ShippingBillingAddressViewModel()
        {
            ShippingAddressModel = new AddressViewModel();
            BillingAddressModel = new AddressViewModel();
        }
    }
}