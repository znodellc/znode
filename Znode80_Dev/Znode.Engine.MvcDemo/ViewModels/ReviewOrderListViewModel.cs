﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class ReviewOrderListViewModel : BaseViewModel
    {
        public ReviewOrderListViewModel()
        {
            ReviewOrderList = new List<ReviewOrderViewModel>();
        }

        public List<ReviewOrderViewModel> ReviewOrderList { set; get; }
       
    }
}