﻿
namespace Znode.Engine.MvcDemo.ViewModels
{
    public class OrderViewModel : BaseViewModel
    {
        /// <summary>
        /// OrderViewModel Constructor
        /// </summary>
        public OrderViewModel()
        {

        }

        public int OrderId { get; set; }
        public string EmailId { get; set; }
    }
}
