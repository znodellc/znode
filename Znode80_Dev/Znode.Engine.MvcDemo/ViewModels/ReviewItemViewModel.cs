﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class ReviewItemViewModel : BaseViewModel
    {
        public int? AccountId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredComments")]
        public string Comments { get; set; }

        public string CreateDate { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredReviewName")]
        public string CreateUser { get; set; }

        public ProductViewModel Product { get; set; }

        public int? ProductId { get; set; }

        public int Rating { get; set; }

        public int ReviewId { get; set; }

        public string Status { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredReviewHeadline")]
        public string Subject { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredReviewLocation")]
        public string UserLocation { get; set; }

        public string Duration { get; set; }
    }
}