﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class CreditCardViewModel
    {
        [DataType(DataType.CreditCard)]
        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredCardNumber")]
        [RegularExpression("(^[3|4|5|6]([0-9]{15}$|[0-9]{12}$|[0-9]{13}$|[0-9]{14}$))", ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "ErrorCardNumber")]
        public string CardNumber { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredCardHolderName")]
        public string CardHolderName { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredCardyear")]
        public string CardExpirationYear { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredCardMonth")]
        public string CardExpirationMonth { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "RequiredCardSecurity")]
        [StringLength(3, MinimumLength = 3, ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "CardSecurityNumberRange")]
        [RegularExpression("^[0-9]*$", ErrorMessageResourceType = typeof(Resources.ZnodeResource), ErrorMessageResourceName = "CardSecurityNumberRange")]
        public string CardSecurityCode { get; set; }

        public IEnumerable<SelectListItem> Month
        {
            get
            {                
                for (int month = 1; month <= 12; month++)
                {
                    yield return new SelectListItem() {Text = month.ToString("00"), Value = month.ToString("00")};
                }
            }
        }

        public IEnumerable<SelectListItem> Year
        {
            get
            {
                for (int year = DateTime.Now.Year; year <= DateTime.Now.Year + 10; year++)
                {
                    yield return new SelectListItem() { Text = year.ToString("0000"), Value = year.ToString("0000") };
                }
            }
        } 
    }
}