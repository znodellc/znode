﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class CategoryViewModel : BaseViewModel
    {
        public int CategoryId { get; set; }
        public string Title { get; set; }
        public string Name { get; set; }
        public string SeoKeywords { get; set; }
        public string SeoTitle { get; set; }
        public string SeoUrl { get; set; }
        public string SeoDescription { get; set; }
        public string ShortDescription { get; set; }       
        public Collection<CategoryViewModel> Subcategories { get; set; }
        //Znode version 7.2.2 To get or set the value of Category Banner
        public string CategoryBanner { get; set; }
    }
}