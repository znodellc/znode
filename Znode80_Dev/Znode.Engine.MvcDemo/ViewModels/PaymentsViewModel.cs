﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Web;
using Znode.Engine.Api.Models;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public class PaymentsViewModel : BaseViewModel
    {
        public decimal PaymanetTotal { get; set; }

        public AddressViewModel BillingAddress { get; set; }

        public AddressViewModel ShippingAddress { get; set; }

        public ReviewOrderViewModel Cart { get; set; }

        public string PaymentTypeName { get; set; }        

        public string PurchaseOrderNumber { get; set; }

        public Collection<PaymentOptionViewModel> PaymentOptions { get; set; }
        //Znode Version 7.2.2 
        public Collection<AddressViewModel> MultipleShippingAddress { get; set; }
        public bool IsMultipleShipping { get; set; }

        public PaymentsViewModel()
        {
            BillingAddress = new AddressViewModel();
            ShippingAddress = new AddressViewModel();
            MultipleShippingAddress = new Collection<AddressViewModel>();
        }        
    }

    public class PaymentOptionViewModel
    {
        public string PaymentName { get; set; }
        public int PaymentTypeId { get; set; }
        public int PaymentOptionId { get; set; }

        public CreditCardViewModel CreditCardModel { get; set; }

        public PaymentOptionViewModel()
        {
            CreditCardModel = new CreditCardViewModel();
        }
    }

    /// <summary>
    /// Represents the PaymentType enumeration
    /// </summary>
    public enum EnumPaymentType
    {        
        [Description("Credit Card")]
        CreditCard = 1,

        [Description("Purchase Order")]
        PurchaseOrder = 2,

        [Description("Paypal Express Checkout")]
        PaypalExpressCheckout = 3,

        [Description("Google Checkout")]
        GoogleCheckout = 4,

        [Description("Cash on Delivery")]
        COD = 5,
    }
}