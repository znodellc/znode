﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Znode.Engine.MvcDemo.ViewModels
{
    public abstract class BaseViewModel
    {

        public string ErrorMessage { get; set; }

        public bool HasError { get; set; }

        public string SuccessMessage { get; set; }

        public string Title { get; set; }

        public object Clone()
        {
            var clonedObject = this.MemberwiseClone();
            return clonedObject;
        }
    }
}