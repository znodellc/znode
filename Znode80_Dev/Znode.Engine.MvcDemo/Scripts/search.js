/*
 | Controller: Search
 | Views: Single view
 |
 | 2014; Znode, Inc.
*/

App.Search = (function ($, Modernizr, App) {

    // INIT	
    var init = function () {
        _debug("search.init()");
        $.getScript("/Scripts/lib/purl.js"); // Pull in URL helper      
        bind.hideControl();
        bind.searchSort();      
        bind.changeSearchProductViewDisplay();       
        bind.setProductViewDisplay();        
        bind.SetSwatchImage();        
        bind.recentProductList(function (res) {            
            if (res === true) { loadSlider(); }
        });
    }

    // BINDINGS
    var bind = {
        // Redirects to new search results page when the sorting dropdown is changed
        // Uses purl library
        searchSort: function () {
            _debug("bind.searchSort()");

            $("#layout-search .search-sorting select").on("change", function () {
                $("#layout-search .search-results").html('<div class="search-results-wait">...</div>');

                var url = purl(),
					query = url.param();

                query.sort = $(this).val();
                query.pagenumber = 1;

                window.location.href = url.attr("path") + "?" + $.param(query);
            });
        },
        // get recent  product grids
        recentProductList: function (fun_res) {            
            var productId = null;
            var htmlContainer = "#category-recent-product .recent-view-items";
            App.Api.getRecentProducts(productId, function (response) {                
                if (htmlContainer) {
                    $(htmlContainer).html(response.data.html);
                }
                if (response.data.html == undefined || response.data.html.length < 1) {
                    $("#category-recent-product").hide();
                }
                else {
                    $("#category-recent-product").show();
                }
                fun_res(true);
            });
        },
        changeSearchProductViewDisplay: function () {
            $(".productview").on("click", function () {
                var previousClass = $("#view-option").attr('class').split(' ')[1];
                var newClass = $(this).attr('title').toLowerCase().replace(" ", "-");

                $(".productview").each(function () {
                    if ($(this).attr("class").indexOf('-active') >= 0) {
                        var baseClass = $(this).attr('class').replace('-active', '');
                        $(this).removeClass($(this).attr('class'));
                        $(this).addClass(baseClass);
                    }
                });

                var activeclass = $(this).attr('class') + '-active';
                $(this).removeClass($(this).attr('class'));
                $(this).addClass(activeclass);

                if (previousClass != undefined && previousClass.length > 0) {
                    $("#view-option").removeClass(previousClass).addClass(newClass)
                } else {
                    $("#view-option").addClass(newClass)
                }
                localStorage["currentDisplayType"] = newClass;
            });
        },
        setProductViewDisplay: function () {
            var displayType = localStorage["currentDisplayType"];
            var previousClass = $("#view-option").attr('class').split(' ')[1];

            $(".productview").each(function () {
                if ($(this).attr("class").indexOf('-active') >= 0) {
                    var baseClass = $(this).attr('class').replace('-active', '');
                    $(this).removeClass($(this).attr('class'));
                    $(this).addClass(baseClass);
                }
            });

            $(".productview").each(function () {
                if (!displayType) {
                    if ($(this).attr("class").indexOf("grid-view") >= 0) {
                        var firstClass = $(this).attr('class');
                        $(this).removeClass(firstClass);
                        $(this).addClass(firstClass + "-active");
                    }
                }
                else {
                    if ($(this).attr("class").indexOf(displayType) >= 0) {
                        var activeclass = $(this).attr('class') + '-active';
                        $(this).removeClass($(this).attr('class'));
                        $(this).addClass(activeclass);
                    }
                }
            });
            if (!displayType) {
                $("#view-option").removeClass(previousClass).addClass("grid-view");
            }
            else {
                $("#view-option").removeClass(previousClass).addClass(displayType);
            }
            $(".view-opt").show();
        },       
        SetSwatchImage: function () {
            _debug("bind.SetSwatchImage()");
            $(".SwatchImage").on("click", function () {
                var productImageId = $(this).attr("data-imgid");
                if (productImageId != null && productImageId > 0) {
                    $("#" + productImageId).attr('src', $(this).attr("data-mediumsrc"));
                    $("#" + productImageId).attr('data-src', $(this).attr("data-mediumsrc"));
                }
            });
        },
        hideControl: function () {
            $("#category-recent-product").hide();
            $(".view-opt").hide();
        }
    }

    return {
        init: init
    };

}(jQuery, Modernizr, App));
