/*
 | Controller: Product
 | Views: 
 |		/product/details/       -> viewProductDetail
 |		/Product/writereview    -> viewWriteReviewForm
 |		/Product/bundlesettings -> viewAddToCart
 |
 | 2014; Znode, Inc.
*/
App.Product = (function ($, Modernizr, App) {

    // Module variables. Use for local tracking
    var data = {
        productId: 0,
        lastSelectedAttributeId: 0,
        
        //Show reviews on product listing pages.-Starts
        productContent: "product-content"
        //Show reviews on product listing pages.-Ends
    }


    // WRITE REVIEW
    var viewWriteReviewForm = {
        init: function () {
            _debug("writeReviewForm.init");
            bind.writeReviewStars();
        }
    };


    // PRODUCT DETAIL
    var viewProductDetail = {
        init: function () {
            _debug("viewProductDetail.init()");

            // Pull in Lightbox library for image zooming
            $.getScript("/Scripts/lib/lightbox-2.6.min.js");

            $('#demo-1 .simpleLens-thumbnails-container img').simpleGallery({ loading_image: '/Images/lightbox/loading.gif' });
            $('#demo-1 .simpleLens-big-image').simpleLens({ loading_image: '/Images/lightbox/loading.gif' });

            // Only load social widget when non-touch; mobile devices have this built-in already
            if (!Modernizr.touch) {
                $.getScript("/Scripts/lib/addthis_widget.js", function () {
                    _debug("AddThis widget loaded, showing icons");
                    $("#product-socialsharing").removeClass("hide");
                });
            }

            // Bindings
            bind.productAttributes();
            bind.productAddOns();
            bind.productQuantity();
            bind.addToWishlist();
            bind.readReviews();
            bind.ChangeMainImage();
            bind.jumpToReviews();

            // Lazy load related product grids
            viewProductDetail.getRelatedProducts();

            // Disable addTocart button on renavigation when quantity reach to max quantity
            viewProductDetail.quantityVerifieronRenavigation();

            // get recent  product grids
            viewProductDetail.getRecentProductList(function (res) {
                if (res === true) { loadSlider(); }                
            });
        },

        getRelatedProducts: function () {
            _debug("getRelatedProducts()");

            App.global.lazyLoadContent("#layout-product", function (el, params, renderAction) {
                App.Api.getRelatedProducts(params.ids, params.accountid, function (response) {
                    renderAction(response);
                    App.global.lazyLoadImages("#" + el.id); // Lazy load images in the incoming markup
                });

            });
        },

        // get recent  product grids
        getRecentProductList: function (fun_res) {
            var productId = local.setProductId();
            App.Api.getRecentProducts(productId, function (response) {
                var title = $("#hdnRecentViewTitle").val();
                $("#layout-product .recent-view-items").html("<h4>" + title + "</h4>" + response.data.html);
                fun_res(true);
            });
        },

        // Disable addTocart button on renavigation when quantity reach to max quantity
        quantityVerifieronRenavigation: function () {
            $("#Quantity").change();
        },

        // Updates total price element
        refreshPrice: function (amount) {
            $("#layout-product .dynamic-product-price").html(amount);
        },
        refreshMainProductImage: function (data) {
            if (data.imagePath != null && data.imagePath != "") {
                $("#product-image").attr('src', data.imageMediumPath);
                $("#product-image").attr('data-src', data.imageMediumPath);
                $("#product-lens-image").attr('data-lens-image', data.imagePath);


            }
        },
        inventoryStatus: function (response) {
            if (response.success) {
                // In stock
                $("#dynamic-inventory").hide();
                $("#button-addtocart").removeAttr("disabled");
            } else {
                // Out of stock
                $("#button-addtocart").attr("disabled", "disabled");
                $("#dynamic-inventory").show().html(response.message);
            }
        },

        updateProductValues: function (response, quantity) {
            var selectedAddOnIds = local.getAddOnIds();

            // Set form values for submit
            $("#dynamic-productid").val(data.productId);
            $("#dynamic-sku").val(response.data.sku);
            $("#Quantity").val(quantity);
            $("#dynamic-addonvalueid").val(selectedAddOnIds);
            $("#dynamic-productName").val(data.ProductName);
        }

    }


    // ADD TO CART
    // Bundle option page
    var viewAddToCart = {
        init: function () {
            _debug("viewAddToCart.init()");

            bind.bundleAttributes();
        }

    }

    // LOCAL ACTIONS, various views
    var local = {
        setProductId: function () {
            if (!data.productId) {
                data.productId = $("#layout .product-meta").data("id");
            }

            return (data.productId);
        },

        addToWishList: function (el) {
            var productId = local.setProductId();
            var clicked = $(el);

            App.Api.addProductToWishList(productId, function (response) {

                $(".wishlist-status").remove(); // Clears any existing message
                $("#SuccessMessage").empty();

                response.data.message = response.message; // Adds response message to data object for template

                // Build status message template
                var template = $("#tmpl-wishliststatus").html();
                var html = Mustache.render(template, response.data);

                if (response.success) {
                    // Added to list
                    clicked.after(html);
                } else {
                    if (response.data.redirect) {
                        // Login needed
                        App.global.setAlertMessage(response.message, "info");
                        document.location = response.data.redirect;
                    } else {
                        // Catchall error
                        clicked.after(html);
                    }
                }
            });

        },

        updateProductVariations: function (htmlContainer, quantity, callback) {
            var selectedIds = local.getAttributeIds();
            var selectedAddOnIds = local.getAddOnIds();

            if (data.lastSelectedAttributeId <= 0) {
                data.lastSelectedAttributeId = selectedIds;
            }

            var params = "attributeId=" + data.lastSelectedAttributeId + "&selectedIds=" + selectedIds + "&quantity=" + quantity + "&selectedAddOnIds=" + selectedAddOnIds;

            App.Api.getProductAttributes(local.setProductId(), params, function (response) {
                if (htmlContainer) {
                    $(htmlContainer).html(response.data.html);
                }

                if (callback) {
                    callback(response);
                }
            });
        },

        updateBundleVariations: function (htmlContainer, bundleProductId, callback) {
            var selectedIds = local.getAttributeIds("#bundle-item-" + bundleProductId);
            var selectedAddOnIds = local.getAddOnIds("#bundle-item-" + bundleProductId);

            if (data.lastSelectedAttributeId <= 0) {
                data.lastSelectedAttributeId = selectedIds;
            }

            var params = "attributeId=" + data.lastSelectedAttributeId + "&selectedIds=" + selectedIds + "&quantity=1&selectedAddOnIds=" + selectedAddOnIds;

            App.Api.getProductAttributes(bundleProductId, params, function (response) {
                if (htmlContainer) {
                    $(htmlContainer).html(response.data.html);
                }

                if (callback) {
                    callback(response);
                }
            });
        },

        getAttributeIds: function (parentSelector) {
            var selectedIds = [];

            if (typeof parentSelector == "undefined") { parentSelector = ""; }

            $(parentSelector + " select.attribute").each(function () {
                selectedIds.push($(this).val());
            });

            return (selectedIds.join());
        },

        getAddOnIds: function (parentSelector) {
            var selectedIds = [];

            if (typeof parentSelector == "undefined") { parentSelector = ""; }

            $(parentSelector + " select.AddOn").each(function () {
                selectedIds.push($(this).val());
            });

            $(parentSelector + " input.AddOn:checked").each(function () {
                selectedIds.push($(this).val());
                /*
				if ($("input[id=" + input.attr("id") + "]:checked + label").css("background-position")) {
                    selectedIds.push(input.val());
                }
				*/
            });

            return (selectedIds.join());
        }
    };




    // BINDINGS
    // Keep all bindings in this object and call as needed from view init()	
    var bind = {
        // Change to attributes on bundle options view; view does not include quantity option
        bundleAttributes: function () {
            _debug("bind.bundleAttributes()");
            $("#layout-bundlesettings .bundle-item-details").on("change", ".attribute, .AddOn", function (ev) {

                /*
				| !! - Need to find ajax-based solution rather than full POST on each change
				*/
                var form = $(this).closest("form");
                $("#dynamic-bundle-attributeId").val($(this).val());
                $("#dynamic-bundle-ProductId").val($(this).closest(".bundle-item").data("id"));
                form.attr('action', '/Product/UpdateBundles');
                form.submit();
            });

            $("#layout-bundlesettings .bundle-item-details").on("change", ".AddOn", function (ev) {
                $("#dynamic-addons").val($(this).val());
            });
        },

        // When attribute dropdown is changed, update region with new HTML
        productAttributes: function () {
            _debug("bind.productAttributes()");

            $("#dynamic-product-variations").on("change", "select.attribute", function (ev) {
                data.lastSelectedAttributeId = $(this).val();
                var quantity = 1;
                local.updateProductVariations("#dynamic-product-variations", quantity, function (response) {

                    viewProductDetail.updateProductValues(response, quantity);

                    // Update display
                    viewProductDetail.refreshPrice(response.data.price);
                    viewProductDetail.inventoryStatus(response);

                    viewProductDetail.refreshMainProductImage(response.data);
                });
            });
        },

        // When add-on input control is changed, update region with new HTML
        productAddOns: function () {
            _debug("bind.productAddOns()");

            $("#dynamic-product-addons").on("change", ".AddOn", function (ev) {
                var quantity = $("#Quantity").val();

                local.updateProductVariations(false, quantity, function (response) {
                    viewProductDetail.updateProductValues(response, quantity);
                    viewProductDetail.refreshPrice(response.data.price);
                    viewProductDetail.inventoryStatus(response);
                });
            });
        },


        // When quantity is changed, update region with new HTML
        productQuantity: function () {
            _debug("bind.productQuantity()");

            $("#dynamic-product-variations").on("change", "select.quantity", function (ev) {
                local.updateProductVariations(false, $(this).val(), function (response) {
                    viewProductDetail.refreshPrice(response.data.price);
                    viewProductDetail.inventoryStatus(response);
                });
            });
        },


        // Click for Add to Wishlist button
        addToWishlist: function () {
            _debug("bind.addToWishlist()");

            $("#layout-product .button-wishlist").on("click", function (e) {
                local.addToWishList(this);
            });
        },

        // Click for "read reviews" link at top of product; jump and toggle
        readReviews: function () {
            _debug("bind.readReviews()");

            $("#jumpto-readreviews").on("click", function (ev) {
                ev.preventDefault();
                App.global.toggle($("#tab-reviews"));
                scrollToTop(300, "#product-content");
            });
        },

        jumpToReviews: function () {
            var url = document.URL.toString();
            var name = '';
            if (!(url.indexOf('#') === -1)) {
                var urlParts = url.split("#");
                name = urlParts[1];
            }
            if (name == data.productContent.toString()) {
                App.global.toggle($("#tab-reviews"));
                scrollToTop(300, "#product-content");
            }
        },

        // Click for write review form stars
        writeReviewStars: function () {
            _debug("bind.writeReviewStars()");

            $("#layout-writereview .setrating label").on("click", function () {
                $("#layout-writereview .setrating label").removeClass("full").addClass("empty"); // Reset all to empty

                var stars = $(this).data("stars");

                for (a = 1; a <= stars; a += 1) {
                    $(".star" + a).removeClass("empty").addClass("full");
                }
            });
        },
        
        //Change the Main image in Product Details on click of Swatch(Color) Images. - Start 
        ChangeMainImage: function () {
            _debug("bind.ChangeMainImage()");
            $(".SwatchImage").on("click", function () {
                $("#product-image").attr('src', $(this).attr("data-mediumsrc"));
                $("#product-image").attr('data-src', $(this).attr("data-mediumsrc"));
                $("#product-lens-image").attr('data-lens-image', $(this).attr("data-largesrc"));
            });
        },
        //Change the Main image in Product Details on click of Swatch(Color) Images. - End 
    };



    return {
        Writereview: viewWriteReviewForm,
        Details: viewProductDetail,
        Wishlist: viewProductDetail,
        AddToCart: viewAddToCart,
        UpdateBundles: viewAddToCart
    };
}(jQuery, Modernizr, App));

// END module