/*
 | Controller: Checkout
 | Views:
 | 		/checkout/billingaddress -> viewBillingAddress
 |		/checkout/payment        -> viewPayments
 |
 | 2014; Znode, Inc.
*/


App.Checkout = (function ($, Modernizr, App) {

    // PAYMENT VIEW
    var viewPayments = {
        init: function () {
            _debug("checkout.payments.init()");
            bind.payment();
            bind.giftCard();
        },

        // Toggle payment option tabs
        togglePaymentMethod: function (el) {
            var clicked = $(el),
			    content = clicked.data("toggle");
            _debug("payment method: " + content);
            $("#payment-methods a").removeClass("active");

            clicked.addClass("active");

            $("#layout-checkout-payment .payment-method").hide();
            $("#" + content).show();
        }
    }


    // BILLING ADDRESS VIEW
    var viewBillingAddress = {
        init: function () {
            _debug("checkout.billingaddress.init()");
            bind.billingAddress();
            bind.BillingSameAsShippingHandler();
            bind.setStateByCountry();
            bind.setDefaultState();
            bind.callAddressChange(0);
            bind.OnStateChange();
           
        }
    }

    //NEW ADDRESS 
    var viewAddnewaddress = {
        init: function () {
            _debug("checkout.addnewaddress.init()");           
            bind.setStateByCountryById();
            bind.setDefaultStateById();
            bind.OnStateChangeForAddNewAddress();
        }
    }

    // BINDINGS
    var bind = {
        // Payment option toggling
        payment: function () {
            _debug("checkout.bind.payment()");

            if ($('#defaultpayment').length > 0) {
                _debug("default payment:" + $('#defaultpayment').val());
                viewPayments.togglePaymentMethod('a[data-toggle=' + $('#defaultpayment').val() + ']');
            }

            // Need Gluten because same links are used differently per screen size
            Gluten.rules([
				{
				    selector: "#payment-methods",
				    live: "a",
				    event: "click.paymentmethod",
				    sizes: "small",
				    callback: function (e) {
				        e.preventDefault();

				        if ($(this).hasClass("active")) {
				            $("#payment-methods a").removeClass("active").show();
				            $("#layout-checkout-payment .payment-method").hide();
				        } else {
				            viewPayments.togglePaymentMethod(this);

				            $("#payment-methods a").hide();
				            $("#payment-methods a.active").show();
				        }

				        _debug("payment method: small");
				    }
				},
				{
				    selector: "#payment-methods",
				    live: "a",
				    event: "click.paymentmethod",
				    sizes: "medium,large",
				    callback: function (e) {
				        e.preventDefault();
				        $("#payment-methods a").removeClass("active").show();

				        viewPayments.togglePaymentMethod(this);

				        _debug("payment method: ml");
				    }
				}
            ]);

            // COD confirm to continue
            $("#pick-cod").on("change", function (e) {
                $("#payment-cod .button").toggleDisabled();
            });

        },

        giftCard: function () {
            _debug("bind.giftcard()");

            // Close gift card after application
            $("#giftcard-action").on("click", ".close-giftcard", function (e) {
                e.preventDefault();
                $("#layout-checkout-payment .payment-method").hide();


                // If not on a phone, display first payment option in menu
                if (Gluten.size != "small") {
                    var defaultPayment = "#" + $("#payment-methods a").first().data("toggle");
                    $(defaultPayment).show();
                } else {
                    $("#payment-methods a").removeClass("active").show();
                }

                scrollToTop(300, "#payment-container");
            });



            // Apply gift card, handles invalid and valid codes
            $("#payment-giftcard").on("click", ".giftcard-apply", function () {
                // Gets nearest gift card number
                // Need this because there are two gift card inputs after the first submit
                var form = $(this).closest(".form");
                var giftCardNumber = form.find("input").val();

                // Apply card and get back new total details
                App.Api.useGiftCard(giftCardNumber, function (response) {
                    // Is it a valid number?
                    if (response.success) {
                        $("#giftcard-initial").hide();

                        // If the gift card covers order total then offer button, else link to close results
                        if (response.data.paid) {
                            var template = $("#tmpl-giftcard-finish").html();
                        } else {
                            var template = $("#tmpl-giftcard-continue").html();
                        }


                        $("#dynamic-total").html(response.data.total);
                        var html = Mustache.render(template, response.data);

                        $("#giftcard-action").html(html);
                    } else {
                        form.find(".form-error").html('<div class="field-validation-error">' + response.message + '</div>');
                    }
                });
            });


        },

        // Toggle for billing address same-as
        billingAddress: function () {
            if ($("#UseSameAsShippingAddress").attr("checked") == "checked") {
                $("#billingaddressform .edit-address-form").toggle();
            }

            $("#UseSameAsShippingAddress").on("click", function () {
                $("#billingaddressform .edit-address-form").toggle();
            });
        },

        BillingSameAsShippingHandler: function () {

            if ($("#billingSameAsShippingAddress").is(':checked')) {
                $("#billingaddressform").hide();
            }

            $("#billingSameAsShippingAddress").on("click", function () {

                if ($("#billingSameAsShippingAddress").is(':checked')) {
                    $("#billingaddressform").hide();
                }
                else {
                    $("#billingaddressform").show();
                }
            });

        },

        // Get State by country
        setStateByCountry: function () {
            $(".address_country").on("change", function (e) {                        
                var countryId = $(this).attr('id');
                if (countryId.indexOf("Shipping") > -1) {
                    bind.ResetCityAndZipCodeShipping();
                }
                else {
                    bind.ResetCityAndZipCodeBilling();
                }
                var stateId = countryId.replace("CountryCode", "StateCode");
                var countryCode = $(this).val();               
                    App.Api.getStateByCountryCode(countryCode, function (response) {
                        $('#' + stateId).empty();
                        for (var i = 0; i < response.length; i++) {
                            $("#" + stateId).append("<option value='" + response[i].Code + "'>" + response[i].Name + "</option>");
                        }
                        if (response.length < 1) {
                            $("#" + stateId).append("<option value=''>Please Select</option>");
                        }
                    });              
            });
        },
        setDefaultState: function () {
            this.callAddressChange(0);
            this.callAddressChange(1);
        },
        callAddressChange: function (addressCnt) {
            var state = $($(".hdn_StateCode")[addressCnt]).val();
            if (state != undefined && state != null && state.length < 1) {
                $($(".address_country")[addressCnt]).change();
            }
        },
        // Get State by country
        setStateByCountryById: function () {
            $("#address_country").on("change", function (e) {
                var stateCode = $(this).val();
                bind.ResetCityAndZipCode();
                App.Api.getStateByCountryCode(stateCode, function (response) {
                    $('#address_state').empty();
                    for (var i = 0; i < response.length; i++) {
                        $("#address_state").append("<option value='" + response[i].Code + "'>" + response[i].Name + "</option>");
                    }
                    if (response.length < 1) {
                        $("#address_state").append("<option value=''>Please Select</option>");
                    }
                });
            });
        },

        setDefaultStateById: function () {
            var state = $("#hdn_StateCode").val();
            if (state.length < 1) {
                $("#address_country").change();
            }
        },
        OnStateChange: function () {
            $(".address_state").on("change", function (e) {
                var stateId = $(this).attr('id');
                if (stateId.indexOf("Shipping") > -1) {
                    bind.ResetCityAndZipCodeShipping();
                }
                else {
                    bind.ResetCityAndZipCodeBilling();
                }
            });
        },
        OnStateChangeForAddNewAddress: function () {
            $("#address_state").on("change", function (e) {
                bind.ResetCityAndZipCode();
            });
        },

        ResetCityAndZipCodeBilling: function () {
            $('[name="BillingAddressModel.City"]').val("");
            $('[name="BillingAddressModel.PostalCode"]').val("");                
        },
        ResetCityAndZipCodeShipping: function () {
            $('[name="ShippingAddressModel.City"]').val("");
            $('[name="ShippingAddressModel.PostalCode"]').val("");
        },

        ResetCityAndZipCode: function () {
        $("#address_city").val("");
        $("#address_zipcode").val("");
    }

    }

    return {
        Payment: viewPayments,
        Billingaddress: viewBillingAddress,
        AddNewAddress: viewAddnewaddress,
        Creditcard: viewPayments,
        Cod: viewPayments,
        Paypal: viewPayments,
        Shipping: viewBillingAddress

    };

}(jQuery, Modernizr, App));

// END module