/*
 | Ajax API Module
 | All ajax calls should be in this module
 |
 | 2014; Znode, Inc.
*/
App.Api = (function ($, Modernizr, App) {
    var ajax = {
        settings: {
            errorAsAlert: false // True will show error as alert dialog
        },

        // Common ajax request; includes cache buster for IE
        request: function (url, method, parameters, successCallback, responseType) {
            if (!method) { method = "GET"; }
            if (!responseType) { responseType = "json"; }

            // Requests must have callback method
            if (typeof successCallback != "function") {
                ajax.errorOut("Callback is not defined. No request made.");
            } else {
                $.ajax({
                    type: method,
                    url: url,
                    data: ajax.cachestamp(parameters),
                    dataType: responseType,
                    success: function (response) {
                        _debug(response); // Always send response to console after request
                        successCallback(response);
                    },
                    error: function () {
                        ajax.errorOut("API Endpoint not available: " + url);
                    }
                });
            }
        },

        // Timestamp for cache busting
        cachestamp: function (data) {
            var d = new Date();

            if (typeof data == "string") {
                data += "&_=" + d.getTime();
            } else if (typeof data == "object") {
                data["_"] = d.getTime();
            } else {
                data = { "_": d.getTime() };
            }


            return (data);
        },

        // Error output
        errorOut: function (message) {
            console.log(message);

            if (this.settings.errorAsAlert) {
                alert(message);
            }
        }
    }


    // App API endpoints
    // Add methods here as needed for additional API calls
    // All endpoint methods are public
    var endpoints = {
        // Get grid of related products (FBT/YML); Returns raw HTML, not JSON
        getRelatedProducts: function (ids, accountId, callbackMethod) {
            ajax.request("/ProductAsync/GetProductsByIds", "get", { "ids": ids, "accountId": accountId }, callbackMethod, "html");
        },


        // Get grid of related products (FBT/YML); Returns raw HTML, not JSON
        countinueCheckout: function (address, callbackMethod) {
            ajax.request("/Checkout/ContinueCheckout", "post", { "multipleAddress": address }, callbackMethod, "json");
        },


        // get recent product
        getRecentProducts: function (productId, callbackMethod) {
            ajax.request("/Product/GetRecentProducts", "get", { "productId": productId }, callbackMethod);
        },

        // Get number of items in cart; Returns raw integer value, not JSON
        getCartCount: function (callbackMethod) {
            ajax.request("/Cart/CartCount", "get", false, callbackMethod);
        },

        // Apply coupon code to cart order
        useCouponCode: function (couponCode, callbackMethod) {
            ajax.request("/Cart/ApplyCoupon", "get", { "Coupon": couponCode }, callbackMethod);
        },

        // Returns balance of gift card
        getGiftCardBalance: function (gcNumber, callbackMethod) {
            ajax.request("/Home/GiftCard", "get", { "giftcard": gcNumber }, callbackMethod);
        },

        // Apply gift card to order during checkout
        useGiftCard: function (gcNumber, callbackMethod) {
            ajax.request("/Checkout/ApplyGiftCard", "get", { "number": gcNumber }, callbackMethod);
        },

        // Add single item to wish list
        addProductToWishList: function (productId, callbackMethod) {
            ajax.request("/Product/WishList", "get", { "id": productId }, callbackMethod);
        },

        // Remove item from account wish list
        removeProductFromWishList: function (wishlistId, callbackMethod) {
            ajax.request("/Account/Wishlist", "post", { "wishid": wishlistId }, callbackMethod);
        },

        // Update product attributes
        getProductAttributes: function (productId, data, callbackMethod) {
            ajax.request("/Product/GetAttributes/" + productId, "get", data, callbackMethod);
        },

        // Get security question of specified user
        getUserSecurityQuestion: function (username, email, callbackMethod) {
            ajax.request("/Account/ForgotPassword/", "post", { "UserName": username, "EmailAddress": email }, callbackMethod);
        },

        // Get suggestions for search term (typeahead)
        getSearchSuggestions: function (searchTerm, callbackMethod) {
            ajax.request("/Search/GetSuggestions", "get", { "searchTerm": searchTerm }, callbackMethod);
        },

        // get list of State bt CountryCode .
        getStateByCountryCode: function (countryCode, callbackMethod) {
            ajax.request("/Account/GetStateByCountryCode", "get", { "countryCode": countryCode }, callbackMethod);
        },

    }



    return (endpoints);

}(jQuery, Modernizr, App));

// END module