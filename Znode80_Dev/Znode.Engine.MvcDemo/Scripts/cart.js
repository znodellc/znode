/*
 | Controller: Cart
 | Views: This is a single view
 |
 | 2014; Znode, Inc.
*/
App.Cart = (function ($, Modernizr, App) {


    // INIT	
    var init = function () {
        _debug("cart.init()");

        bind.removeCartItem();
        bind.updateQuantity();
        bind.updateShipTo();
        bind.promoCode();

        //Removes all item from cart - Start 
        bind.removeAllCartItem();
        //Removes all item from cart - End 

        bind.submitMultipleShipping();
        bind.calculateShipping();
        bind.viewMultiAddressCart();
    }


    // BINDINGS
    var bind = {
        // Removes item from cart
        removeCartItem: function () {
            _debug("bind.removeCartItem()");
            $("#layout-cart .cart-item-remove").on("click", function (e) {
                debugger;
                e.preventDefault();
                $(this).closest("form").submit();
            });
        },

        // Updates quantity of item 
        updateQuantity: function () {
            _debug("bind.updateQuantity()");
            $("#layout-cart .cart-item-total select.shipTo").on("change", function (ev) {
                $(this).closest("form").submit();
            });
        },

        // Updates quantity of item 
        updateShipTo: function () {
            $("#layout-cart .cart-item-quantity select.cartQuantity").on("change", function (ev) {
                $(this).closest("form").submit();
            });
        },

        // Updates quantity of item 
        submitMultipleShipping: function () {
            $("#layout-cart #submitMultipleShipping").on("click", function (ev) {
                debugger;
                var DataList = new Array;
                $("#layout-cart select.shipTo option:selected").each(function (e) {
                    DataList.push({
                        addressid: $(this).val(),
                        productid: $(this).parent().next().next().val(),
                        quantity: 1,
                        slno: $(this).parent().next().next().next().val(),
                        itemguid: $(this).parent().next().val()
                    });
                });

                var jArray = JSON.stringify(DataList);

                $.ajax({
                    url: "/Checkout/ContinueCheckout/",
                    type: "get",
                    dataType: "json",
                    data: { "multipleAddress": jArray },
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        window.location.href = "/Checkout/ShippingMethods?returnUrl='multipleshipto'";
                    },
                    error: function (msg) {
                        window.location.href = "/Checkout/ShippingMethods?returnUrl='multipleshipto'";
                    }
                });

            });
        },

        // Updates quantity of item 
        calculateShipping: function () {
            //shippingOptionList
            $(".multipleShippingOption").on("change", function (ev) {
                $(this).closest("form").submit();
            });
        },

        viewMultiAddressCart: function () {
            //shippingOptionList
            $(".view-multi-address-cart").on("click", function (ev) {
                $(this).closest("form").submit();
            });
        },


        promoCode: function () {
            _debug("bind.promoCode()");
            $("#cart-promocode-container").on("submit", function (e) {
                e.preventDefault();

                var template = $("#tmpl-promocodes").html();

                App.Api.useCouponCode($("#promocode").val(), function (response) {
                    response.data.message = response.message;

                    var html = Mustache.render(template, response.data);

                    if (response.success) {
                        $(".cart-promocode-status .success").css("text-decoration", "line-through");
                        $(".cart-promocode-status").prepend(html);
                        $("#cart-promocode-container input").val("");
                        $("#dynamic-discount-amount").html(response.data.discountamount);
                        $("#dynamic-order-total").html(response.data.ordertotal);
                    } else {
                        $(".cart-promocode-status").prepend(html);
                    }
                });
            });
        },

        //Removes all item from shopping cart - Start 
        removeAllCartItem: function () {
            _debug("bind.removeAllCartItem()");
            $("#layout-cart .cart-item-remove-all").on("click", function (e) {
                e.preventDefault();
                $(this).closest("form").submit();
            });
        }
        //Removes all item from shopping cart - End
    }

    return {
        init: init
    };

}(jQuery, Modernizr, App));

// END module