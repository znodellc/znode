// Global debug
var enableDebug = true;

function _debug(s) {
	if (enableDebug) {
		if (typeof console === "undefined") {
			//console = { log: function(s){ alert(s); } }
		} else {
			console.log(s);
		}
	}
}


// In-window scrolling, uses jQuery
function scrollToTop(speed, selector) {
	if (!speed) { speed = 300; }
	
	if (!selector) { selector = "body"; }
	
	$("html, body").animate({scrollTop: $(selector).offset().top}, speed);
}


// jQuery plugin for disabled attribute on element
(function ($) {
    $.fn.toggleDisabled = function () {
        return this.each(function () {
            var $this = $(this);
            if ($this.attr('disabled')) {
                $this.removeAttr('disabled');
            } else {
                $this.attr('disabled', 'disabled');
            }
        });
    };
})(jQuery);