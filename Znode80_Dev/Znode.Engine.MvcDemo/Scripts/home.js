/*
 | Controller: Home
 | Views:
 | 		/home/giftcard -> viewGiftCardBalance
 |
 | 2014; Znode, Inc.
*/
App.Home = (function($, Modernizr, App) {

// INIT VIEWS	
	var viewGiftCardBalance = {
		init: function() {
			_debug("viewGiftCardBalance.init()");

			bind.giftCardBalance();
		}
	}
	
	
	
// BINDINGS
	var bind = {
		// Get balance of provided gift card number
		giftCardBalance: function() {
			_debug("bind.giftCardBalance()");
			
			$("#giftcard-balance-form").on("submit", function(e) {
				e.preventDefault();
				
				var form    = $(this);
				var gcInput = $("#giftcard_number");

				// Call API for response
				App.Api.getGiftCardBalance(gcInput.val(), function(response) {
					if (response.success) {
						// Generate markup from inline template (within view)
						var template = $("#tmpl-giftcardbalance").html();
						var html     = Mustache.render(template, response.data);
						
						$("#giftcard-balance-result").html(html);
						gcInput.val("");
					} else {
						form.find(".form-error").html('<div class="field-validation-error">'+response.message+'</div>');
					}
				
				});
			});
		}
	}
	
	
    return {
        Giftcard: viewGiftCardBalance
    };

}(jQuery, Modernizr, App));

// END module