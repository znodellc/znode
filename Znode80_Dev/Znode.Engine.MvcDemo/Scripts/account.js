﻿/*
 | Controller: Account
 | Views:
 | 		/account/forgotpassword -> viewForgotPassword
 |		/account/wishlist       -> viewWishlist
 |
 | 2014; Znode, Inc.
*/

App.Account = (function ($, Modernizr, App) {

    // Module variables. Use for local tracking
    var data = {
        addressId: 0
    }


    // FORGOT PASSWORD VIEW
    var viewForgotPassword = {
        init: function () {
            _debug("Account.ForgotPassword.init()");
            this.bind.buttonContinue();
        },

        // Passes user credentials and returns security question if valid
        // Final action is handled by POST submit
        toggleSecurityQuestion: function () {
            App.Api.getUserSecurityQuestion($("#forgot_username").val(), $("#forgot_email").val(), function (response) {
                if (response.success) {
                    App.global.clearAlertMessage();
                    $("#display-securityquestion").html(response.data.question);
                    $("#dynamic-userid").val(response.data.userid);

                    $("#reset-securityquestion").show();
                    $("#toggle-securityquestion").hide();
                } else {
                    App.global.setAlertMessage(response.message, "error");
                    App.global.displayAlertMessage();
                    scrollToTop();
                }

            });
        },

        bind: {

            // Continue button to submit username/mail for security question
            buttonContinue: function () {
                _debug("bind.buttonContinue()");

                $("#toggle-securityquestion button").on("click", function () {
                    viewForgotPassword.toggleSecurityQuestion();
                });
            }
        }
    }


    //Edit Address
    var viewEditaddress = {
        init: function () {           
            _debug("account.editaddress.init()");
            this.bind.setStateByCountry();
            this.bind.setDefaultState();
            this.bind.OnStateChange();    
        },
        bind: {
            // Get State by country
            setStateByCountry: function () {
                $("#address_country").on("change", function (e) {                
                    viewEditaddress.ResetCityAndZipCode();
                    var stateCode = $(this).val();
                    App.Api.getStateByCountryCode(stateCode, function (response) {
                        $('#address_state').empty();
                        for (var i = 0; i < response.length; i++) {
                            $("#address_state").append("<option value='" + response[i].Code + "'>" + response[i].Name + "</option>");
                        }
                        if (response.length < 1) {
                            $("#address_state").append("<option value=''>Please Select</option>");
                        }
                    });
                });
            },

            setDefaultState: function () {             
                var state = $("#hdn_StateCode").val();
                if (state.length < 1) {
                    $("#address_country").change();
                }
            },

            OnStateChange: function () {
                $("#address_state").on("change", function (e) {
                    viewEditaddress.ResetCityAndZipCode();
                });
            },

        },

        ResetCityAndZipCode: function () {
            $("#address_city").val("");
            $("#address_zipcode").val("");
        }


    }


    // WISH LIST VIEW
    // Only fires when the view is loaded
    var viewWishlist = {
        init: function () {
            _debug("Account.Wishlist.init()");
            this.bind.removeIcon();
        },

        // Removes item from account wish list
        removeWishlistItem: function (el) {
            var clicked = $(el);
            var wishlistId = clicked.data("id");
            var wishListCount = parseInt($("#wishlistcount").text());

            App.Api.removeProductFromWishList(wishlistId, function (response) {
                if (response.success) {
                    clicked.closest(".wishlist-item").remove();

                    $("#wishlistcount").html(response.data.total);
                } else {
                    App.global.setAlertMessage(response.message, "error");
                    App.global.displayAlertMessage();
                    scrollToTop();
                }
            });
        },

        bind: {

            removeIcon: function () {
                _debug("bind.removeIcon()");

                $("#layout-account-wishlist .wishlist-item-remove a").on("click", function (ev) {
                    ev.preventDefault();
                    viewWishlist.removeWishlistItem(this);
                });
            }
        }
    }


    return {
        Forgotpassword: viewForgotPassword,
        ForgotPassword: viewForgotPassword,
        Wishlist: viewWishlist,
        EditAddress: viewEditaddress,
        Editaddress: viewEditaddress
    };

}(jQuery, Modernizr, App));