﻿using System.Web.Mvc;
using DevTrends.MvcDonutCaching;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;

namespace Znode.Engine.MvcDemo.Controllers
{    
    public class CartController : BaseController
    {
        private readonly ICartAgent _cartAgent;

        public CartController()
        {
            _cartAgent = new CartAgent();
        }

        public ActionResult Index()
        {
            var cartViewModel = _cartAgent.GetCart();
            _cartAgent.SetMessages(cartViewModel);
            return View("Cart", cartViewModel);
        }
       
        
        public ActionResult CartCount()
        {
            var count = _cartAgent.GetCartCount();
            return Content(count.ToString());
        }

        [HttpPost]
        public ActionResult RemoveItem(string guid)
        {
            bool removeItem = _cartAgent.RemoveItem(guid);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult UpdateQuantity(string guid, int quantity)
        {
            _cartAgent.UpdateItem(guid, quantity);
            return RedirectToAction("Index");
        }

        public ActionResult ApplyCoupon(string coupon)
        {
            var cartViewModel = _cartAgent.ApplyCoupon(coupon);
         
            var errorMsg = string.IsNullOrEmpty(coupon) ? Resources.ZnodeResource.RequiredCouponCode : Resources.ZnodeResource.ErrorCouponCode;
            
            return Json(new
            {
                success = !cartViewModel.HasError,
                message = !cartViewModel.HasError ? cartViewModel.CouponMessage : errorMsg,
                data = new
                {
                    style = !cartViewModel.HasError ? "success" : "error",
                    promocode = cartViewModel.Coupon,
                    discountamount = cartViewModel.Discount.ToString(),
                    ordertotal = cartViewModel.Total.GetValueOrDefault().ToString()
                }
            }, JsonRequestBehavior.AllowGet);
          
        }
        #region Znode Version 7.2.2
        //Removes all item from shopping cart - Start 
        [HttpPost]
        public ActionResult RemoveAllCartItems()
        {
            bool removeItem = _cartAgent.RemoveAllCartItems();
            return RedirectToAction("Index");
        }
        //Removes all item from shopping cart - End 
        #endregion
    }
}
