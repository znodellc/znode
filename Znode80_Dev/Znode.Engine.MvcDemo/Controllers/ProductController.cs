﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Web.Mvc;
using DevTrends.MvcDonutCaching;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;
using System.Linq;
using System.Web;
namespace Znode.Engine.MvcDemo.Controllers
{
    public class ProductController : BaseController
    {
        private readonly IProductAgent _productAgent;
        private readonly IAccountAgent _accountAgent;
        private readonly ISkuAgent _skuAgent;
        private readonly ICartAgent _cartAgent;

        #region Znode Version 7.2.2 Private Variables
        private string _recentlyViewProductList = "RecentlyViewProductList";
        #endregion

        public ProductController()
        {
            _productAgent = new ProductAgent();
            _skuAgent = new SkuAgent();
            _cartAgent = new CartAgent();
            _accountAgent = new AccountAgent();
        }

        [DonutOutputCache(CacheProfile = "CatalogCacheProfile")]
        [HttpGet]
        public ActionResult Details(int? id, string seo)
        {
            if (id.HasValue)
            {
                var product = _productAgent.GetProduct(id.Value);
                if (product == null)
                    throw new HttpException(404, "Couldn't find the product ");

                if (!string.IsNullOrEmpty(seo))
                {
                    if (product.SeoPageName.ToLower() != seo.ToLower())
                    {
                        throw new HttpException(404, "SEO does not match");
                    }
                }

                _productAgent.SetMessages(product);

                var accountViewModel = _accountAgent.GetAccountViewModel();

                ViewBag.AccountId = accountViewModel != null ? accountViewModel.AccountId : 0;



                ViewBag.Title = product.Title;

                return View(product);
            }

            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public JsonResult GetAttributes(int? id, int? attributeId, string selectedIds, int? quantity, string selectedAddOnIds)
        {
            string selectedSku = string.Empty;
            string imagePath = string.Empty;
            //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
            string imageMediumPath = string.Empty;

            decimal productPrice;
            decimal finalProductPrice;
            string selectedAddOnValue = string.Empty;
            //ZNode Version 7.2.2 - Add imageMediumPath Out parameter
            var product = _productAgent.GetAttributes(id.Value, attributeId.GetValueOrDefault(0), selectedIds, quantity.GetValueOrDefault(1), out productPrice, out selectedSku, out imagePath, out imageMediumPath);

            if (product.AddOns != null && !string.IsNullOrEmpty(selectedAddOnIds))
            {
                product.AddOns = _productAgent.GetAddOnDetails(product, selectedAddOnIds, quantity.GetValueOrDefault(1),
                                                               productPrice, out finalProductPrice);
            }
            else
            {
                finalProductPrice = productPrice;
            }

            return Json(new
            {
                success = product.ShowAddToCart,
                message = product.InventoryMessage,
                data = new
                {
                    style = product.ShowAddToCart ? "success" : "error",
                    price = finalProductPrice.ToString("c"),
                    sku = selectedSku,
                    html = RenderHelper.PartialView(this, "_attributes", product),
                    //Znode Version 7.2.2
                    //Change Main Image on Product Details page OnChange of Color Attributes - Start
                    imagePath = imagePath,
                    imageMediumPath = imageMediumPath,
                    //Change Main Image on Product Details page OnChange of Color Attributes - End

                }
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddToCart(CartItemViewModel cartItem)
        {
            if (!string.IsNullOrEmpty(cartItem.BundleItemsIds))
            {
                var productsViewModel = _productAgent.GetBundleProductsDetails(cartItem);
                return View("BundleSettings", productsViewModel);
            }

            var product = _cartAgent.WishlistCheckInventory(cartItem.ProductId, cartItem.Quantity);

            if (!string.IsNullOrEmpty(product.InventoryMessage) && !product.ShowAddToCart)
            {
                Session.Add("ErrorMessage", product.InventoryMessage);

                return RedirectToAction("Index", "Cart");
            }

            var viewModel = _cartAgent.Create(cartItem);
            return RedirectToAction("Index", "Cart");
        }

        [HttpPost]
        public ActionResult UpdateBundles(BundleDetailsViewModel bundleCartItem)
        {
            var productsViewModel = _productAgent.GetUpdatedBundles(bundleCartItem);
            ModelState.Clear();
            return View("BundleSettings", productsViewModel);
        }

        [HttpPost]
        public ActionResult ContinueToCart(BundleDetailsViewModel bundleCartItem)
        {
            var item = CartItemViewModelMap.ToViewModel(bundleCartItem);

            var viewModel = _cartAgent.Create(item);

            return RedirectToAction("Index", "Cart");
        }

        public ActionResult WishList(int? id)
        {
            if (!User.Identity.IsAuthenticated)
            {
                return Json(new
                {
                    success = false,
                    message = Resources.ZnodeResource.ErrorLoginWishlist,
                    data = new { style = "info", redirect = "/Account/Login?returnurl=/Product/Wishlist/" + id }
                }, JsonRequestBehavior.AllowGet);
            }

            var wishListViewModel = _productAgent.CreateWishList(id.GetValueOrDefault(0));

            if (Request.IsAjaxRequest())
            {
                return Json(new
                {
                    success = !wishListViewModel.HasError,
                    message = !wishListViewModel.HasError ? Resources.ZnodeResource.SuccessProductAddWishlist : Resources.ZnodeResource.ErrorProductAddWishlist,
                    data = new { style = !wishListViewModel.HasError ? "success" : "error", link = "/Account/Wishlist" }
                }, JsonRequestBehavior.AllowGet);
            }

            if (wishListViewModel.HasError)
            {
                Session.Add("ErrorMessage", Resources.ZnodeResource.ErrorProductAddWishlist);
            }
            else
            {
                Session.Add("SuccessMessage", Resources.ZnodeResource.SuccessProductAddWishlist);
            }

            return Redirect(_productAgent.GetProductUrl(id.GetValueOrDefault(0), Url));
        }



        public ActionResult Allreviews(int id)
        {
            var reviewModelList = _productAgent.GetAllReviews(id);
            return View("AllReviews", reviewModelList);
        }

        [HttpGet]
        public ActionResult GetHomePageSpecials()
        {

            var viewModel = _productAgent.GetHomeSpecials();
            if (viewModel == null || viewModel.Count < 1)
            {
                return new EmptyResult();
            }
            return PartialView("_ProductGrid", viewModel);
        }

        [HttpGet]
        public ActionResult GetProductsByIds(string ids)
        {

            var viewModel = new Collection<ProductViewModel>(_productAgent.GetProductByIds(ids).ToList());
            if (!viewModel.Any())
            {
                return new EmptyResult();
            }
            return PartialView("_ProductGrid", viewModel);
        }


        public ActionResult GetBundleDisplay(string ids)
        {
            if (string.IsNullOrEmpty(ids))
            {
                return new EmptyResult();
            }
            var viewModel = _productAgent.GetBundles(ids);
            if (viewModel == null || viewModel.Count < 1)
            {
                return new EmptyResult();
            }
            return PartialView("_Bundles", viewModel);
        }

        public ActionResult Writereview(int id, string name)
        {
            var model = new ReviewItemViewModel();
            model.Product = new ProductViewModel();
            model.ProductId = id;
            model.Product.Name = name;
            model.Rating = 1;
            return View("WriteReview", model);
        }

        [HttpPost]
        public ActionResult Writereview(ReviewItemViewModel reviewItem, int id, string name)
        {
            reviewItem.Product = new ProductViewModel { Name = name };
            reviewItem.ProductId = id;

            if (ModelState.IsValid)
            {
                reviewItem.ProductId = id;
                var model = _productAgent.CreateReview(reviewItem);

                if (!model.HasError)
                {
                    ModelState.Clear();
                    var reviewItemViewModel = new ReviewItemViewModel { Product = new ProductViewModel(), ProductId = id };
                    reviewItemViewModel.Product.Name = name;
                    reviewItemViewModel.Rating = 1;
                    reviewItemViewModel.SuccessMessage = model.SuccessMessage;

                    return View("WriteReview", reviewItemViewModel);
                }
            }

            return View("WriteReview", reviewItem);
        }

        public ActionResult wfbundlesettings()
        {
            return View("BundleSettings");

        }

        public IEnumerable<SelectedBundleViewModel> GetBundles(FormCollection formCollection)
        {
            var bundles = new Collection<SelectedBundleViewModel>();

            if (formCollection.Count > 0)
            {
                if (formCollection.GetValues("Attribute") != null)
                {
                    var values = formCollection.GetValues("Attribute");
                }

            }

            return bundles;
        }

        #region Znode Version 7.2.2

        /// <summary>
        /// To get recently view product asynchronous
        /// </summary>
        /// <param name="productId">nullable int productId</param>
        /// <returns>returns JsonResult</returns>
        [HttpGet]
        public JsonResult GetRecentProducts(int? productId)
        {
            Collection<ProductViewModel> _ProductList = GetRecentlyViewItems(productId);
            return Json(new
            {
                success = true,
                message = productId,
                data = new
                {
                    style = "success",
                    html = _ProductList == null ? "" : RenderHelper.PartialView(this, "_RelatedItems", _ProductList),
                }
            }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// To get max number of recently view item of current session
        /// Max number of items set from admin
        /// </summary>
        /// <param name="productId">nullable int productId</param>
        /// <returns>collection of Recently View Items</returns>
        private Collection<ProductViewModel> GetRecentlyViewItems(int? productId)
        {
            Collection<ProductViewModel> _ProductList = new Collection<ProductViewModel>();
            if (!productId.Equals(null))
            {

                _ProductList = GetRecentProductFromSession();
                if (!IsProductExistInList(_ProductList, productId.Value))
                {
                    _ProductList = AddProductInList(_ProductList, productId);
                    _ProductList = SetMaxRecentProductInSession(_ProductList);
                }
            }
            else
            {
                _ProductList = GetRecentProductFromSession();
            }
            //To reverse the product list for showing latest item at first position
            if (!Equals(_ProductList, null))
            {
                _ProductList = new Collection<ProductViewModel>
                    (
                      _ProductList.Reverse().ToList()
                    );
            }
            return _ProductList;
        }

        /// <summary>
        /// To get all recently view item from session object
        /// </summary>
        /// <returns>collection of Recently View Items</returns>
        private Collection<ProductViewModel> GetRecentProductFromSession()
        {
            Collection<ProductViewModel> _ProductList = null;
            try
            {
                if (!Equals(Session[_recentlyViewProductList], null))
                {
                    _ProductList = new Collection<ProductViewModel>();
                    _ProductList = Session[_recentlyViewProductList] as Collection<ProductViewModel>;
                }
            }
            catch
            {
            }
            return _ProductList;
        }

        /// <summary>
        /// To set recently view item in session object
        /// </summary>
        /// <param name="productList">collection of Recently View Items</param>
        /// <returns>collection of Recently View Items as per maximum items</returns>
        private Collection<ProductViewModel> SetMaxRecentProductInSession(Collection<ProductViewModel> productList)
        {
            try
            {
                int maxItemToDisplay = MvcDemoConstants.MaxRecentViewItemToDisplay;
                if (productList.Count > maxItemToDisplay)
                {
                    int difference = productList.Count - maxItemToDisplay;
                    for (int count = 0; count < difference; count++)
                    {
                        productList.RemoveAt(0);
                    }
                }

                if (productList.Count > 0)
                {
                    Session[_recentlyViewProductList] = productList;
                }
            }
            catch
            {
            }
            return productList;
        }

        /// <summary>
        /// To check current product is exist in recently viewed product List by ProductId
        /// </summary>
        /// <param name="productList">collection of Recently View Items</param>
        /// <param name="productId">int productId</param>
        /// <returns>bool true/false</returns>
        private bool IsProductExistInList(Collection<ProductViewModel> productList, int productId)
        {
            if (!Equals(productList, null))
            {
                var items = productList.FirstOrDefault(p => p.ProductId.Equals(productId));
                if (Equals(items, null))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// To add current product in recently viewed product List by ProductId
        /// </summary>
        /// <param name="productList">collection of Recently View Items</param>
        /// <param name="productId">int productId</param>
        /// <returns>collection of Recently View Items</returns>
        private Collection<ProductViewModel> AddProductInList(Collection<ProductViewModel> productList, int? productId)
        {
            if (!productId.Equals(null))
            {
                ProductViewModel product = _productAgent.GetProduct(productId.Value);
                if (Equals(productList, null))
                {
                    productList = new Collection<ProductViewModel>();
                }
                productList.Add(product);
            }
            return productList;
        }

        #endregion
    }
}
