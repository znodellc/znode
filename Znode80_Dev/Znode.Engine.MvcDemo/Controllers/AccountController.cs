﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;


namespace Znode.Engine.MvcDemo.Controllers
{

    public class AccountController : BaseController
    {
        private readonly IAccountAgent _accountAgent;
        private readonly ICartAgent _cartAgent;
        private readonly IAuthenticationAgent _authenticationAgent;
        LoginViewModel model;

        public AccountController()
        {
            _accountAgent = new AccountAgent();
            _cartAgent = new CartAgent();
            _authenticationAgent = new AuthenticationAgent();

        }

        public ActionResult Redirect()
        {

            // your code

            return RedirectPermanent("~/Account/Login");
        }

        public PartialViewResult LoginStatus()
        {
            return PartialView("_LoginPartial");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Signup()
        {
            var viewModel = _accountAgent.GetRegisterViewModel();
            return View("Register", viewModel);
        }

        [HttpPost]
        public ActionResult Signup(RegisterViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                model = _accountAgent.SignUp(model);

                if (!model.HasError)
                {
                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        _authenticationAgent.SetAuthCookie(model.UserName, true);
                        return RedirectToAction("Dashboard");
                    }

                    _authenticationAgent.RedirectFromLoginPage(model.UserName, true);
                    return new EmptyResult();
                }
            }

            return View("Register", model);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login(string returnUrl)
        {
            if (User.Identity.IsAuthenticated && _accountAgent.GetAccountViewModel() != null)
            {
                return RedirectToAction("Dashboard");
            }

            if (!string.IsNullOrEmpty(returnUrl) && returnUrl.Contains("checkout"))
            {
                return View("CheckoutLogin");
            }

            //Get user name from cookies
            GetLoginRememberMeCookie();

            return View("Login", model);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(LoginViewModel model, string returnUrl)
        {
            if (ModelState.IsValid)
            {
                var loginviewModel = _accountAgent.Login(model);

                if (!loginviewModel.HasError)
                {
                    _authenticationAgent.SetAuthCookie(model.Username, model.RememberMe);

                    //Remember me
                    if (checked(model.RememberMe) == true)
                    {
                        SaveLoginRememberMeCookie(model.Username);
                    }

                    var isMerged = _cartAgent.Merge();

                    if (isMerged)
                    {
                        return RedirectToAction("Index", "Cart");
                    }

                    if (string.IsNullOrEmpty(returnUrl))
                    {
                        return RedirectToAction("Dashboard");
                    }

                    _authenticationAgent.RedirectFromLoginPage(model.Username, true);
                    return new EmptyResult();
                }

                if (loginviewModel != null && loginviewModel.IsResetPassword)
                {
                    if(!string.IsNullOrEmpty(loginviewModel.Username) && !string.IsNullOrEmpty(loginviewModel.PasswordResetToken))
                    {
                       return RedirectToAction("ResetPassword", new { passwordToken = loginviewModel.PasswordResetToken, userName = loginviewModel.Username });
                    }
                    return View("ResetPassword");
                }

                return View("Login", loginviewModel);
            }

            return View("Login", model);
        }

        [Authorize]
        public ActionResult Dashboard()
        {
            var model = _accountAgent.GetDashboard();

            if (model == null)
            {
                return RedirectToAction("Login");
            }

            return View("Dashboard", model);
        }

        [Authorize]
        public ActionResult Wishlist(int? wishid)
        {
            var wishlistModel = new WishListViewModel();

            if (wishid.HasValue)
            {
                var deleteWishList = _accountAgent.DeleteWishList(wishid.GetValueOrDefault(0));

                return Json(new
                {
                    success = deleteWishList,
                    message = deleteWishList ? Resources.ZnodeResource.SuccessProductDeleteWishlist : Resources.ZnodeResource.ErrorProductDeleteWishlist,
                    data = new { style = deleteWishList ? "success" : "error", total = _accountAgent.GetAccountViewModel().WishlistCount }
                }, JsonRequestBehavior.AllowGet);
            }

            wishlistModel = _accountAgent.GetWishList();

            return View("Wishlist", wishlistModel);
        }

        [Authorize]
        [HttpGet]
        public ActionResult OrderHistory()
        {
            var model = _accountAgent.GetAccountViewModel();

            return View("OrderHistory", model);
        }

        [Authorize]
        public ActionResult OrderReceipt(int id)
        {
            var model = _accountAgent.GetOrderReceiptDetails(id);
            return View("OrderReceipt", model);
        }


        #region Znode Version 7.2.2 Reorder

        /// <summary>
        /// Znode Version 7.2.2
        /// Reorder complite order.
        /// </summary>
        /// <param name="id">orderId</param>
        /// <returns>Redirect to Cart View</returns>
        [Authorize]
        public ActionResult ReorderProducts(int id)
        {
            List<CartItemViewModel> cartItemList = new List<CartItemViewModel>();

            cartItemList = _accountAgent.GetReorderItems(id);

            //Add items into cart 
            foreach (var crt in cartItemList)
            {
                var viewModel = _cartAgent.Create(crt);
            }

            return RedirectToAction("Index", "Cart");
        }

        /// <summary>
        /// Znode Version 7.2.2
        /// reorder single item of order.
        /// </summary>
        /// <param name="id">orderLineItemId</param>
        /// <returns>Redirect to Cart View</returns>
        [Authorize]
        public ActionResult ReorderSingleProduct(int id)
        {
            CartItemViewModel cartItem = new CartItemViewModel();

            //Get order item for reorder
            cartItem = _accountAgent.GetReorderSingleItems(id);

            //Add item into cart
            var viewModel = _cartAgent.Create(cartItem);
            return RedirectToAction("Index", "Cart");
        }


        //TODO
        //Znode Version 7.2.2
        [Authorize]
        public ActionResult ReorderProductsList(int id, int orderLineItemId, bool isOrder)
        {
            List<CartItemViewModel> cartItemList = new List<CartItemViewModel>();
            cartItemList = _accountAgent.GetReorderItemsList(id, orderLineItemId, isOrder);
            foreach (var crt in cartItemList)
            {
                var viewModel = _cartAgent.Create(crt);
            }
            return RedirectToAction("Index", "Cart");
        }



        #endregion

        [Authorize]
        [HttpGet]
        public ActionResult AddressBook()
        {
            var model = _accountAgent.GetAccountViewModel();

            _accountAgent.SetMessages(model);

            _accountAgent.SetAddressLink(model);

            ViewBag.ShowHeader = true;

            if (model.Addresses != null)
                return View("AddressBook", model);

            return View("AddressBook", new AccountViewModel());
        }

        [Authorize]
        [HttpPost]
        public ActionResult AddressBook(int id, bool isDefaultBillingAddress)
        {
            var addressModel = _accountAgent.UpdateAddress(id, isDefaultBillingAddress);

            var model = _accountAgent.GetAccountViewModel();

            if (addressModel.HasError)
            {
                model.HasError = addressModel.HasError;
                model.ErrorMessage = addressModel.ErrorMessage;
            }
            else
            {
                model.SuccessMessage = addressModel.SuccessMessage;
            }

            return View("AddressBook", model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult EditAddress(int? id)
        {
            var model = new AddressViewModel();

            if (id.HasValue)
                model = _accountAgent.GetAddress(id.GetValueOrDefault(0));



            if (model == null)
                return RedirectToAction("AddressBook");

            model.States = _accountAgent.GetStates();
            model.Countries = _accountAgent.GetCountries();
            return View("EditAddress", model);
        }

        [HttpPost]
        public ActionResult EditAddress(AddressViewModel model)
        {
            if (ModelState.IsValid)
            {
                model = _accountAgent.UpdateAddress(model);
            }
            else
            {
                model.States = _accountAgent.GetStates();
                model.Countries = _accountAgent.GetCountries();
                return View("EditAddress", model);
            }

            if (model.HasError)
            {
                model.States = _accountAgent.GetStates();
                model.Countries = _accountAgent.GetCountries();
                return View("EditAddress", model);
            }

            return RedirectToAction("AddressBook");
        }


        [Authorize]
        [HttpPost]
        public ActionResult DeleteAddress(int id)
        {
            var addressViewModel = _accountAgent.DeleteAddress(id);

            if (!addressViewModel.HasError)
            {
                var model = _accountAgent.GetAccountViewModel();
                Session.Add("SuccessMessage", addressViewModel.SuccessMessage);
                return RedirectToAction("AddressBook");
            }

            return RedirectToAction("EditAddress", new { id = id });
        }

        [Authorize]
        [HttpGet]
        public ActionResult Reviews()
        {
            var model = _accountAgent.GetReviews();
            return View("ReviewHistory", model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult EditProfile()
        {
            var model = _accountAgent.GetAccountViewModel();
            return View("EditProfile", model);
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditProfile(AccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                model = _accountAgent.UpdateProfile(model);
            }

            return View("EditProfile", model);
        }

        [Authorize]
        [HttpGet]
        public ActionResult Changepassword()
        {
            return View("ChangePassword", new ChangePasswordViewModel());
        }

        [Authorize]
        [HttpPost]
        public ActionResult Changepassword(ChangePasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.UserName = User.Identity.Name;
                model = _accountAgent.ChangePassword(model);
            }

            return View("ChangePassword", model);
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ForgotPassword()
        {
            if (TempData["ResetPasswordLinkErrorMessage"] != null)
            {
                return View("ForgotPassword", new AccountViewModel { SuccessMessage = TempData["ResetPasswordLinkErrorMessage"].ToString(), HasError = false });
            }
            else
            {
                return View("ForgotPassword", new AccountViewModel());
            }
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult ForgotPassword(AccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                model = _accountAgent.ForgotPassword(model);

               
                return View("ForgotPassword", model);
            }

         

            return View("ForgotPassword", model);
        }

        [Authorize]
        public ActionResult Logout()
        {
            _accountAgent.Logout();
            return RedirectToAction("Dashboard");
        }

        #region Znode 7.2.2
        //Reset user password using link, passed in email. Below method reads the password token & user name.

        [AllowAnonymous]
        [HttpGet]
        public ActionResult ResetPassword(string passwordToken, string userName)
        {

            passwordToken = WebUtility.UrlDecode(passwordToken);
            userName = WebUtility.UrlDecode(userName);
            var resetPassword = new ChangePasswordViewModel();
            resetPassword.UserName = userName;
            resetPassword.PasswordToken = passwordToken;

            //Set ResetPasword flag, use to hide Old Password field in View.
            resetPassword.IsResetPassword = true;
            ResetPasswordStatusTypes enumstatus;

            enumstatus = _accountAgent.CheckResetPasswordLinkStatus(resetPassword);
            switch (enumstatus)
            {
                case ResetPasswordStatusTypes.Continue:
                    {
                        return View("ResetPassword", resetPassword);
                    }
                case ResetPasswordStatusTypes.LinkExpired:
                    {
                        TempData["ResetPasswordLinkErrorMessage"] = Resources.ZnodeResource.ResetPasswordLinkError;
                        return RedirectToAction("ForgotPassword");
                    }
                case ResetPasswordStatusTypes.TokenMismatch:
                    {
                        TempData["ResetPasswordLinkErrorMessage"] = Resources.ZnodeResource.ResetPasswordLinkError;
                        return RedirectToAction("ForgotPassword");
                    }
                case ResetPasswordStatusTypes.NoRecord:
                    {
                        TempData["ResetPasswordLinkErrorMessage"] = Resources.ZnodeResource.ResetPasswordLinkError;
                        return RedirectToAction("ForgotPassword");
                    }
                default:
                    {
                        TempData["ResetPasswordLinkErrorMessage"] = Resources.ZnodeResource.ResetPasswordLinkError;
                        return RedirectToAction("ForgotPassword");
                    }
            }
        }

        [HttpPost]
        public ActionResult ResetPassword(ChangePasswordViewModel model)
        {
            ModelState.Remove("OldPassword");
            model.IsResetPassword = true;
            if (ModelState.IsValid)
            {
                var changepasswordmodel = _accountAgent.ChangePassword(model);

                if (!model.HasError)
                {
                    var loginModel = new LoginViewModel()
                        {
                            Username = model.UserName,
                            Password = model.NewPassword,
                        };
                    var accountViewModel = _accountAgent.Login(loginModel);

                    if (!accountViewModel.HasError)
                    {
                        Session.Add("SuccessMessage", changepasswordmodel.SuccessMessage);
                        _authenticationAgent.SetAuthCookie(loginModel.Username, true);
                        return RedirectToAction("Dashboard");
                    }
                }
            }

            return View("ResetPassword", model);

        }

        /// <summary>
        /// Remember me function for logged in user on remember me selection
        /// This function is used to save user name in cookies.
        /// </summary>
        /// <param name="userId">string userId</param>
        private void SaveLoginRememberMeCookie(string userId)
        {
            //Check if the browser support cookies 
            if ((HttpContext.Request.Browser.Cookies))
            {
                HttpCookie cookieLoginRememberMe = new HttpCookie("loginCookie");
                cookieLoginRememberMe.Values["loginCookie"] = userId;
                cookieLoginRememberMe.Expires = DateTime.Now.AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["CookieExpiresValue"]));
                HttpContext.Response.Cookies.Add(cookieLoginRememberMe);
                cookieLoginRememberMe.HttpOnly = true;
            }
        }
        
        /// <summary>
        /// Remember me function for logged in user on remember me selection
        /// This method is used to get user name value from cookies.
        /// </summary>
        private void GetLoginRememberMeCookie()
        {
            if ((HttpContext.Request.Browser.Cookies))
            {
                if (HttpContext.Request.Cookies["loginCookie"] != null)
                {
                    HttpCookie cookieRememberMe = HttpContext.Request.Cookies["loginCookie"];
                    if (cookieRememberMe != null)
                    {
                        var loginName = HttpUtility.HtmlEncode(cookieRememberMe.Values["loginCookie"]);
                        model = new LoginViewModel();
                        model.Username = loginName;
                        model.RememberMe = true;
                    }
                }
            }
        }

        /// <summary>
        /// This function is used to get all state list by countryCode
        /// </summary>
        /// <param name="countryCode">string countryCode</param>
        /// <returns>returns state list</returns>
        [HttpGet]
        public JsonResult GetStateByCountryCode(string countryCode)
        {
            var result = _accountAgent.GetStateByCountryCode(countryCode);
            return Json(result, JsonRequestBehavior.AllowGet);

        }
        #endregion
    }
}
