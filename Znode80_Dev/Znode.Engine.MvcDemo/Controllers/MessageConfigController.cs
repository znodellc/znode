﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Znode.Engine.MvcDemo.Agents;

namespace Znode.Engine.MvcDemo.Controllers
{
    public class MessageConfigController : BaseController
    {
        private readonly IMessageConfigsAgent _agent;
        public MessageConfigController()
        {
            _agent = new MessageConfigsAgent();
        }

        [HttpGet]
        public ActionResult GetMessage(string key)
        {
           var content = _agent.GetByKey(key);

           return Content(content);
        }

    }
}
