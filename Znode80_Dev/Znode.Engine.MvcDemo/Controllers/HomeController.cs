﻿using System.Web.Mvc;
using Znode.Engine.MvcDemo.Agents;

namespace Znode.Engine.MvcDemo.Controllers
{
	public class HomeController : BaseController
	{
        
	    private readonly IAccountAgent _accountAgent;

	    public HomeController()
	    {
	        _accountAgent = new AccountAgent();
        }

	    public ActionResult Index()
		{
			ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
			return View();
		}

		public ActionResult About()
		{
			ViewBag.Message = "Your app description page.";
			return View();
		}

		public ActionResult Contact()
		{
			ViewBag.Message = "Your contact page.";
			return View();
		}

        public ActionResult GiftCard(string giftcard)
        {
            if (ModelState.IsValid && Request.IsAjaxRequest())
            {
                if (string.IsNullOrEmpty(giftcard))
                {
                    return Json(new
                    {
                        success = false,
                        message = Resources.ZnodeResource.RequiredGiftCard,
                        data = new
                        {
                            style = "error",

                        }
                    }, JsonRequestBehavior.AllowGet);
                }
               
                var giftcardModel = _accountAgent.GetGiftCardBalance(giftcard);

                return Json(new
                    {
                        success = !giftcardModel.HasError,
                        message =
                                !giftcardModel.HasError ? giftcardModel.SuccessMessage : giftcardModel.ErrorMessage,
                        data = new
                            {
                                style = !giftcardModel.HasError ? "success" : "error",
                                amount = giftcardModel.Amount.GetValueOrDefault().ToString("c"),
                            }
                    }, JsonRequestBehavior.AllowGet);
            }

            return View("GiftCardBalance");
        }

        public ActionResult ContentPage()
        {
            return View("ContentPage");
        }
       
	}
}
