﻿using System.Net;
using System.Web.Mvc;
using DevTrends.MvcDonutCaching;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;
using System.Web;
namespace Znode.Engine.MvcDemo.Controllers
{
    public class CategoryController : BaseController
    {
        private readonly ISearchAgent _searchAgent;
        private readonly ICategoryAgent _categoryAgent;

        public CategoryController()
        {
            _searchAgent = new SearchAgent();
            _categoryAgent = new CategoryAgent();
        }

        [DonutOutputCache(CacheProfile = "PortalCacheProfile")]
        [ChildActionOnly]
        public ActionResult TopLevelList()
        {
            var categories = _categoryAgent.GetCategories();

            return PartialView("_HeaderNav", categories);
        }

        [DonutOutputCache(CacheProfile = "PortalCacheProfile")]
        [ChildActionOnly]
        public ActionResult GetCategories()
        {
            var categories = _categoryAgent.GetCategories();

            if (categories == null && categories.Categories.Count == 0)
            {
                return new EmptyResult();
            }

            return PartialView("../Home/Categories", categories);
        }

        [DonutOutputCache(CacheProfile = "CatalogCacheProfile")]
        [HttpGet]
        public ActionResult Index(SearchRequestModel searchRequestModel, string category, string seo)
        {
            searchRequestModel.RefineBy = SearchHelper.GetRefineBy();
            searchRequestModel.Sort = SearchHelper.GetSortBy(searchRequestModel.Sort);
            searchRequestModel.Category = !string.IsNullOrEmpty(category) ? category : WebUtility.UrlDecode(searchRequestModel.Category);

            var result = _searchAgent.Search(searchRequestModel);

            if (!(result.Categories != null && result.Categories.Count > 0))
            {
                result.HasError = true;
                result.ErrorMessage = Resources.ZnodeResource.ErrorNoCategoryListFound;

                return View("Landing", result);
            }

            result.OldPageNumber = searchRequestModel.PageNumber ?? MvcDemoConstants.DefaultPageNumber;

            var categoryDetails = _categoryAgent.GetCategoryByName(searchRequestModel.Category);
            string title = string.Empty;

            if (!string.IsNullOrEmpty(seo))
            {
                if (categoryDetails.SeoUrl.ToLower() != seo.ToLower())
                {
                    throw new HttpException(404, "SEO does not match");
                }
            }

            //Znode version 7.2.2  Start-  To show category banner            
            var categoryBanner = _categoryAgent.GetCategoryByName(searchRequestModel.Category).CategoryBanner;
            if (!string.IsNullOrEmpty(categoryBanner))
            {
                result.CategoryBanner = categoryBanner;
            }
            //Znode version 7.2.2  End-To show category banner

            ViewBag.Title = categoryDetails.Title;
            return View("Landing", result);
        }

        // This is a copy of Index, used for alternate category landing page view 
        [HttpGet]
        public ActionResult AltLanding(SearchRequestModel searchRequestModel, string category, string seo)
        {
            searchRequestModel.RefineBy = SearchHelper.GetRefineBy();
            searchRequestModel.Sort = SearchHelper.GetSortBy(searchRequestModel.Sort);
            searchRequestModel.Category = !string.IsNullOrEmpty(category) ? category : WebUtility.UrlDecode(searchRequestModel.Category);

            var result = _searchAgent.Search(searchRequestModel);

            if (!(result.Categories != null && result.Categories.Count > 0))
            {
                result.HasError = true;
                result.ErrorMessage = Resources.ZnodeResource.ErrorNoCategoryListFound;
            }

            result.OldPageNumber = searchRequestModel.PageNumber ?? MvcDemoConstants.DefaultPageNumber;

            var categoryDetails = _categoryAgent.GetCategoryByName(searchRequestModel.Category);
            string title = string.Empty;

            if (!string.IsNullOrEmpty(seo))
            {
                if (categoryDetails.SeoUrl.ToLower() != seo.ToLower())
                {
                    return new HttpNotFoundResult("SEO name does not match");
                }
            }

            ViewBag.Title = categoryDetails.Title;

            return View("LandingAlt", result);
        }

        [HttpGet]
        public JsonResult GetCategory(int categoryId)
        {
            var result = _categoryAgent.GetCategory(categoryId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}
