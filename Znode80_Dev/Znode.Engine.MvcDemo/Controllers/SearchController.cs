﻿using System.Web.Mvc;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.Maps;
using Znode.Engine.MvcDemo.ViewModels;

namespace Znode.Engine.MvcDemo.Controllers
{
    [OutputCache(CacheProfile = "CatalogCacheProfile")]
    public class SearchController : BaseController
    {
        private readonly ISearchAgent _searchAgent;
      
        public SearchController()
        {
            _searchAgent = new SearchAgent();
        }

        [HttpGet]
        public JsonResult GetSuggestions(string searchTerm,string category)
        {

           var result = _searchAgent.GetSuggestions(searchTerm, category);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        ///  // searchTerm = Keyword
        // category = Cateogry name Selected
        // sortBy = Should be passed in format as sort=2 in the querystring. If nothing is passed it will sory as sort=1
        // pageSize = Exact page size or -1 for Show All option or if nothing is passed, it is considered as 10 as default page size.
        // pageNumber = valid page number in integer
        // refine By = this should be generated as below. This is an example. This should be a valid facets associated. 
        // The Key and value will come dynamically from the Querystring. To test it uncomment the below and comment the var refineBy = GetRefineBy().
        // var refineBy = new Collection<KeyValuePair<string, IEnumerable<string>>>
        //{
        //    new KeyValuePair<string, IEnumerable<string>>("Price_Range", new string[] {"1 - 10"}),
        //    new KeyValuePair<string, IEnumerable<string>>("Color", new string[] {"Black"})
        //};
        /// </summary>
        /// <param name="searchRequestModel"></param>
        /// <returns></returns>
        public ActionResult Index(SearchRequestModel searchRequestModel)
        {
            searchRequestModel.RefineBy = SearchHelper.GetRefineBy();
            searchRequestModel.Sort = SearchHelper.GetSortBy(searchRequestModel.Sort);

		    var result = _searchAgent.Search(searchRequestModel);

            result.OldPageNumber = searchRequestModel.PageNumber ?? MvcDemoConstants.DefaultPageNumber;

            int previousPage = (result.OldPageNumber - 1) == 0 ? 1 : (result.OldPageNumber - 1);
            int nextPage = (result.OldPageNumber >= result.TotalPages) ? result.OldPageNumber : result.OldPageNumber + 1;

            result.NextPageUrl = SearchMap.UpdateUrlQueryString("pagenumber", nextPage.ToString());
            result.PreviousPageurl = SearchMap.UpdateUrlQueryString("pagenumber", previousPage.ToString());
            
            return View("BrowseSearch", result);
        }
    }
}
