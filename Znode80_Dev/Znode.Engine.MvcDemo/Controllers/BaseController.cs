﻿using System.Web.Mvc;
using System.Web.Routing;
using Znode.Engine.MvcDemo.Agents;

namespace Znode.Engine.MvcDemo.Controllers
{
    public class BaseController : Controller
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            ViewBag.Title = PortalAgent.CurrentPortal.Name;

            this.ControllerContext = new ControllerContext(requestContext, this);
        }
    }
}
