﻿using System.Linq;
using System.Web.Mvc;
using System.Web.Security;
using Znode.Engine.Api.Models;
using Znode.Engine.MvcDemo.Agents;
using Znode.Engine.MvcDemo.Helpers;
using Znode.Engine.MvcDemo.ViewModels;
using System.Collections.ObjectModel;
using Resources;
using System.Collections.Generic;
using Newtonsoft.Json;
namespace Znode.Engine.MvcDemo.Controllers
{
    public class CheckoutController : BaseController
    {
        private readonly IShippingOptionAgent _shippingOptionAgent;
        private readonly ICheckoutAgent _checkoutAgent;
        private readonly IAccountAgent _accountAgent;
        private readonly ICartAgent _cartAgent;
        public CheckoutController()
        {
            _shippingOptionAgent = new ShippingOptionAgent();
            _checkoutAgent = new CheckoutAgent();
            _accountAgent = new AccountAgent();
            _cartAgent = new CartAgent();
        }

        public ActionResult Index()
        {
            if (User.Identity.IsAuthenticated)
            {
                var model = _accountAgent.GetAccountViewModel();

                if (model == null)
                {
                    FormsAuthentication.SignOut();
                    return RedirectToAction("Index", "Cart");
                }
                return RedirectToAction("Shipping");
            }

            return RedirectToAction("Login", "Account", new { returnUrl = "~/checkout" });
        }




        #region Znode Version 7.2.2

        //Znode Version 7.2.2
        // Shipping also contains billing process.
        [HttpGet]
        public ActionResult Shipping(bool isAddressChange = false)
        {
            ShippingBillingAddressViewModel viewModel = new ShippingBillingAddressViewModel();
            Collection<CountryModel> countries = _accountAgent.GetCountries();
            Collection<StateModel> states = _accountAgent.GetStates();
            if (User.Identity.IsAuthenticated)
            {
                AccountViewModel accountViewModel = _accountAgent.GetAccountViewModel();

                if (Equals(accountViewModel, null))
                {
                    FormsAuthentication.SignOut();
                    return RedirectToAction("Index", "Cart");
                }

                if (!Equals(accountViewModel.BillingAddress, null))
                {
                    accountViewModel.BillingAddress.SourceLink = MvcDemoConstants.CheckoutShippingBilling;
                }
                if (!Equals(accountViewModel.ShippingAddress, null))
                {
                    accountViewModel.ShippingAddress.SourceLink = MvcDemoConstants.CheckoutShippingBilling;
                }

                if (!Equals(accountViewModel.BillingAddress, null) && !Equals(accountViewModel.ShippingAddress, null) && !isAddressChange)
                {
                    return View("CheckoutAddressbook", accountViewModel);
                }
                else
                {
                    SetAddressesInViewModel(viewModel);
                    viewModel.ShippingAddressModel.AccountId = accountViewModel.AccountId;
                    viewModel.BillingAddressModel.AccountId = accountViewModel.AccountId;
                    FillStatesAndCountries(viewModel);
                    return View("CheckoutGuestShipping", viewModel);
                }
            }
            else
            {
                SetAddressesInViewModel(viewModel);
            }

            FillStatesAndCountries(viewModel);

            return View("CheckoutGuestShipping", viewModel);
        }


        #region Muliple shipping

        //Znode Version 7.2.2
        public ActionResult Multipleshipto()
        {
            var cartViewModel = _cartAgent.GetCart();
            var addressModel = _checkoutAgent.GetAllAddesses();

            foreach (var adr in addressModel)
            {
                cartViewModel.Addresses.Add(adr);
            }
            return View("MultipleAddressCart", cartViewModel);
        }

        //Znode Version 7.2.2
        [HttpPost]
        public ActionResult ViewMultipleshipto()
        {
            var cartViewModel = _cartAgent.GetCart();

            var shippingAddress = _checkoutAgent.GetAllAddesses();

            foreach (var adr in shippingAddress)
            {
                cartViewModel.Addresses.Add(adr);
            }

            return View("MultipleAddressCart", cartViewModel);

        }

        [HttpPost]
        public ActionResult RemoveItem(string guid, int quantity)
        {
            quantity = quantity - 1;
            _cartAgent.UpdateItem(guid, quantity);
            return RedirectToAction("Multipleshipto");
        }

        [HttpPost]
        public ActionResult UpdateQuantity(string guid, int quantity)
        {
            _cartAgent.UpdateItem(guid, quantity);
            return RedirectToAction("Multipleshipto");
        }


        public string ContinueCheckout(string multipleAddress)
        {
            IEnumerable<OrderShipmentDataModel> data = JsonConvert.DeserializeObject<IEnumerable<OrderShipmentDataModel>>(multipleAddress);

            Session["MultipleOrderShipment"] = data;

            //Sort Multiple Shipping Address
            List<OrderShipmentDataModel> sortedAddress = _checkoutAgent.SortMultipleShippingAddress(data);

            //Create multicart
            _checkoutAgent.CreateMultipleShoppingCart(sortedAddress, data);

            return "True";
        }

        public ActionResult MultiAddressCartReview()
        {
            var model = _checkoutAgent.GetMultiCartOrderReview();

            return View("MultipleCheckoutReview", model);
        }

        [HttpPost]
        public ActionResult CalculateShipping(int cartIndex, string shipping)
        {
            _checkoutAgent.CalculateMultiCartShipping(cartIndex, int.Parse(shipping));
            var model = _checkoutAgent.GetMultiCartOrderReview();

            return View("MultipleCheckoutReview", model);

        }

        #endregion


        [HttpPost]
        public ActionResult Shipping(ShippingBillingAddressViewModel models)
        {
            if (models.BillingAddressModel.UseSameAsShippingAddress)
            {
                ModelState.RemoveFor<ShippingBillingAddressViewModel>(ob => ob.BillingAddressModel);
            }

            if (User.Identity.IsAuthenticated)
            {
                if (AnyExistingAddresses(models))
                {
                    models.ShippingAddressModel.AddressId = models.ShippingAddressId;
                    models.BillingAddressModel.AddressId = models.BillingAddressId;
                    models.BillingAddressModel.Email = models.EmailAddress;

                    AddressViewModel shippingModel = _checkoutAgent.GetAddressByAddressID(models.ShippingAddressModel);
                    _checkoutAgent.SaveShippingAddess(shippingModel);

                    SaveBillingAddress(models.BillingAddressModel, models.BillingAddressModel.UseSameAsShippingAddress);
                    return RedirectToAction("ShippingMethods");
                }
                else
                {
                    if (string.IsNullOrEmpty(models.ShippingAddressModel.Name))
                    {
                        models.ShippingAddressModel.Name = string.Format("{0} {1}", models.ShippingAddressModel.FirstName, models.ShippingAddressModel.LastName);
                    }
                    if (string.IsNullOrEmpty(models.BillingAddressModel.Name))
                    {
                        models.BillingAddressModel.Name = string.Format("{0} {1}", models.BillingAddressModel.FirstName, models.BillingAddressModel.LastName);
                    }

                    if (ModelState.IsValid)
                    {
                        AccountViewModel accountViewModel = _accountAgent.GetAccountViewModel();

                        if (accountViewModel == null)
                        {
                            FormsAuthentication.SignOut();
                            return RedirectToAction("Index", "Cart");
                        }
                        SaveShippingBillingAddresses(models);
                        return RedirectToAction("ShippingMethods");
                    }
                    else
                    {
                        FillStatesAndCountries(models);
                        return View("CheckoutGuestShipping", models);
                    }
                }
            }
            else
            {
                ModelState.Remove("ShippingAddressModel.Name");
                ModelState.Remove("BillingAddressModel.Name");

                if (ModelState.IsValid)
                {
                    models.ShippingAddressModel.Name = "Default Address";
                    models.BillingAddressModel.Name = "Default Address";
                    _checkoutAgent.SaveShippingAddess(models.ShippingAddressModel);
                    models.BillingAddressModel.Email = models.EmailAddress;
                    SaveBillingAddress(models.BillingAddressModel, models.BillingAddressModel.UseSameAsShippingAddress);
                    return RedirectToAction("ShippingMethods");
                }

                FillStatesAndCountries(models);
                return View("CheckoutGuestShipping", models);
            }
        }



        [HttpPost]
        public ActionResult AddMultipleAddress(string guid, int shipto, int product)
        {
            ReviewOrderViewModel model = new ReviewOrderViewModel();

            if (shipto > 0)
            {
                AddressViewModel ShippingAddress = new AddressViewModel();
                ShippingAddress.AddressId = shipto;

                ShippingAddress = _checkoutAgent.GetAddressByAddressID(ShippingAddress);

                _checkoutAgent.SaveShippingAddess(ShippingAddress);
            }

            model = _checkoutAgent.GetCheckoutReview(9);

            model.ShippingAddress.SourceLink = MvcDemoConstants.RouteGuestCheckoutShipping;
            return View("CheckoutReview", model);

        }


        [HttpPost]
        public ActionResult ContinueCheckout()
        {
            ReviewOrderViewModel model = new ReviewOrderViewModel();

            AddressViewModel ShippingAddress = new AddressViewModel();
            ShippingAddress.AddressId = 10;
            model.ShippingAddress = ShippingAddress;
            for (int i = 0; i < 2; i++)
            {
                model = _checkoutAgent.GetCheckoutReview(9);
            }
            model.ShippingAddress.SourceLink = MvcDemoConstants.RouteGuestCheckoutShipping;
            return View("CheckoutReview", model);
        }

        #endregion

        [HttpGet]
        public ActionResult ShippingMethods()
        {
            var model = _shippingOptionAgent.GetShippingList();

            if (!model.ShippingOptions.Any())
            {
                return RedirectToAction("Review", new { shippingOptions = 0 });
            }

            model.ShippingAddress = _checkoutAgent.GetShippingAddess();
            model.ShippingAddress.SourceLink = MvcDemoConstants.RouteGuestCheckoutShipping;

            return View("CheckoutShippingMethod", model);
        }

        [HttpGet]
        public ActionResult Review(int shippingOptions,string isMultipleShip)
        {
            var Multicartmodel = _checkoutAgent.GetMultiCartOrderReview();
            if (Multicartmodel != null && isMultipleShip.Equals("true"))
            {
                _checkoutAgent.CalculateMultiCartShipping(shippingOptions);
                return RedirectToAction("MultiAddressCartReview");
            }
            else
            {
                var model = _checkoutAgent.GetCheckoutReview(shippingOptions);
                model.ShippingAddress.SourceLink = MvcDemoConstants.RouteGuestCheckoutShipping;
                return View("CheckoutReview", model);
            }
        }

        //Znode Version 7.2.2
        public void SaveBillingAddress(AddressViewModel billAddress, bool? useSameAsShippingAddress)
        {
            if (User.Identity.IsAuthenticated && billAddress.AddressId > 0)
            {
                billAddress = _checkoutAgent.GetAddressByAddressID(billAddress);
                _checkoutAgent.SaveBillingAddess(billAddress, true, useSameAsShippingAddress.GetValueOrDefault(false));
            }
            else
            {
                _checkoutAgent.SaveBillingAddess(billAddress, ModelState.IsValid, useSameAsShippingAddress.GetValueOrDefault(false));
                if (string.IsNullOrEmpty(billAddress.Email))
                {
                    ModelState.AddModelError("Email", Resources.ZnodeResource.RequiredEmailID);
                }
            }
        }


        [HttpGet]
        public ActionResult AddNewAddress(string returnUrl)
        {
            var model = new AddressViewModel();

            model.States = _accountAgent.GetStates();
            model.Countries = _accountAgent.GetCountries();
            if (!string.IsNullOrEmpty(returnUrl))
            {
                ViewBag.IsShipping = returnUrl.ToLower().Contains(MvcDemoConstants.RouteGuestCheckoutShipping);
            }

            return View("AddNewAddress", model);
        }

        [HttpPost]
        public ActionResult AddNewAddress(AddressViewModel model, string returnUrl)
        {
            if (!string.IsNullOrEmpty(returnUrl))
            {
                ViewBag.IsShipping = returnUrl.ToLower().Contains(MvcDemoConstants.RouteGuestCheckoutShipping);
            }

            model.States = _accountAgent.GetStates();
            model.Countries = _accountAgent.GetCountries();
            if (ModelState.IsValid)
            {
                model = _accountAgent.UpdateAddress(model);
            }
            else
            {
                return View("AddNewAddress", model);
            }

            if (!model.HasError && !string.IsNullOrEmpty(returnUrl))
            {
                Session.Add("NewAddressID", model.AddressId);
                return Redirect(returnUrl);
            }


            return View("AddNewAddress", model);
        }

        [HttpGet]
        public ActionResult Payment()
        {
            var paymentModel = _checkoutAgent.GetPaymentModel();

            //Znode Version 7.2.2 
            //Multiple Shipping Address - start
            var model = _checkoutAgent.GetMultiCartOrderReview();

            if (model != null)
            {
                decimal cartTotal = 0;
                decimal shippingCost = 0;
                decimal discount = 0;
                decimal giftcardamount = 0;
                decimal subtotal = 0;
                decimal taxcost = 0;
                decimal taxrate = 0;

                if (model.ReviewOrderList.Count() > 0)
                {
                    paymentModel.IsMultipleShipping = true;

                    model.ReviewOrderList.ForEach(reviewOrder =>
                    {
                        paymentModel.MultipleShippingAddress.Add(reviewOrder.ShippingAddress);
                        cartTotal += (decimal)reviewOrder.ShoppingCart.Total;
                        shippingCost += (decimal)reviewOrder.ShoppingCart.ShippingCost;
                        discount += (decimal)reviewOrder.ShoppingCart.Discount;
                        giftcardamount += (decimal)reviewOrder.ShoppingCart.GiftCardAmount;
                        subtotal += (decimal)reviewOrder.ShoppingCart.SubTotal;
                        taxcost += (decimal)reviewOrder.ShoppingCart.TaxCost;
                        taxrate += (decimal)reviewOrder.ShoppingCart.TaxRate;
                    });

                    paymentModel.PaymanetTotal = cartTotal;
                    paymentModel.Cart.ShoppingCart.Total = cartTotal;
                    paymentModel.Cart.ShoppingCart.ShippingCost = shippingCost;
                    paymentModel.Cart.ShoppingCart.Discount = discount;
                    paymentModel.Cart.ShoppingCart.GiftCardAmount = giftcardamount;
                    paymentModel.Cart.ShoppingCart.SubTotal = subtotal;
                    paymentModel.Cart.ShoppingCart.TaxCost = taxcost;
                    paymentModel.Cart.ShoppingCart.TaxRate = taxrate;

                }

                if (model.ReviewOrderList.Count().Equals(1))
                {
                    paymentModel.ShippingAddress = model.ReviewOrderList[0].ShippingAddress;
                    paymentModel.IsMultipleShipping = false;
                }

            }
            //Multiple Shipping Address - End

            if (TempData["PaypalError"] != null)
            {
                ModelState.AddModelError("", TempData["PaypalError"].ToString());
            }
            if (TempData["PaymentType"] != null)
            {
                ViewBag.DefaultPayment = TempData["PaymentType"];
            }
            return View("CheckoutPayment", paymentModel);
        }

        [HttpPost]
        public ActionResult CreditCard(PaymentOptionViewModel model)
        {
            if (ModelState.IsValid)
            {
                var orderViewModel = _checkoutAgent.SubmitOrder(model, string.Empty);

                if (orderViewModel == null)
                {
                    return RedirectToAction("Index", "Checkout");
                }

                if (!orderViewModel.HasError)
                {
                    return View("CheckoutReceipt", orderViewModel);
                }

                ModelState.AddModelError("", Resources.ZnodeResource.ErrorUnablePlaceOrder);
            }

            return View("CheckoutPayment", _checkoutAgent.GetPaymentModel(model));
        }

        [HttpPost]
        public ActionResult Cod(PaymentOptionViewModel model)
        {

            var orderViewModel = _checkoutAgent.SubmitOrder(model, string.Empty);

            if (orderViewModel == null)
            {
                return RedirectToAction("Index", "Checkout");
            }

            if (!orderViewModel.HasError)
            {
                return View("CheckoutReceipt", orderViewModel);
            }
            ModelState.AddModelError("", Resources.ZnodeResource.ErrorUnablePlaceOrder);

            return View("CheckoutPayment", _checkoutAgent.GetPaymentModel(model));
        }

        [HttpGet]
        public ActionResult Paypal(string token, string payerId, int paymentOptionId)
        {
            if (string.IsNullOrEmpty(token) || string.IsNullOrEmpty(payerId))
            {
                return RedirectToAction("Payment");
            }

            var orderViewModel = _checkoutAgent.SubmitPayPalOrder(token, payerId, paymentOptionId);

            if (!orderViewModel.HasError)
            {
                return View("CheckoutReceipt", orderViewModel);
            }

            ModelState.AddModelError("", Resources.ZnodeResource.ErrorUnablePlaceOrder);

            return RedirectToAction("Payment");
        }

        [HttpPost]
        public ActionResult Paypal(PaymentOptionViewModel model)
        {
            var orderViewModel = _checkoutAgent.SubmitOrder(model, string.Empty);

            if (orderViewModel == null)
            {
                return RedirectToAction("Index", "Checkout");
            }
            TempData["PaypalError"] = orderViewModel.ErrorMessage;
            TempData["PaymentType"] = MvcDemoConstants.PaypalPaymentType;

            return RedirectToAction("Payment");
        }

        public ActionResult ApplyGiftCard(string number)
        {
            var cartViewModel = _checkoutAgent.ApplyGiftCard(number);

            return Json(new
            {
                success = !cartViewModel.HasError,
                message = !cartViewModel.HasError ? cartViewModel.SuccessMessage : cartViewModel.ErrorMessage,
                data = new
                {
                    style = !cartViewModel.HasError ? "success" : "error",
                    applied = cartViewModel.GiftCardAmount.ToString("c"),
                    balance = cartViewModel.GiftCardBalance.ToString("c"),
                    total = cartViewModel.Total.GetValueOrDefault().ToString("c"),
                    paid = cartViewModel.Total == 0
                }
            }, JsonRequestBehavior.AllowGet);

        }

        //Znode Version 7.2.2
        #region Private methods

        private void SetAddressesInViewModel(ShippingBillingAddressViewModel viewModel)
        {
            var addresses = _checkoutAgent.GetBillingAddress();
            viewModel.ShippingAddressModel = addresses.ShippingAddress ?? new AddressViewModel();
            viewModel.BillingAddressModel = addresses.BillingAddress ?? new AddressViewModel();
            viewModel.EmailAddress = viewModel.BillingAddressModel.Email;
            viewModel.ShippingAddressModel.IsDefaultShipping = true;
            viewModel.BillingAddressModel.IsDefaultBilling = true;
        }

        private void SaveShippingBillingAddresses(ShippingBillingAddressViewModel models)
        {
            _checkoutAgent.SaveShippingAddess(models.ShippingAddressModel);
            models.BillingAddressModel.Email = models.EmailAddress;
            SaveBillingAddress(models.BillingAddressModel, models.BillingAddressModel.UseSameAsShippingAddress);
        }

        private bool AnyExistingAddresses(ShippingBillingAddressViewModel models)
        {
            return models.ShippingAddressId > 0 && models.BillingAddressId > 0;
        }

        private void FillStatesAndCountries(ShippingBillingAddressViewModel models)
        {
            models.ShippingAddressModel.States = _accountAgent.GetStates();
            models.BillingAddressModel.States = models.ShippingAddressModel.States;
            models.BillingAddressModel.Countries = _accountAgent.GetCountries();
            models.ShippingAddressModel.Countries = models.BillingAddressModel.Countries;
        }

        #endregion
    }
}
