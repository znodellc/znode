﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Znode.Engine.MvcDemo
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute("category-details", "category/{category}", new { controller = "category", action = "Index" }, new { httpMethod = new HttpMethodConstraint("GET"), category = @"[\w- ]+$" });

            routes.MapRoute("seocategory-details", "category/{seo}/{category}", new { controller = "category", action = "Index" }, new { httpMethod = new HttpMethodConstraint("GET"), seo = @"[\w-]+$", category = @"[\w- ]+$" });

            routes.MapRoute("AccountLoginRedirect", "login.aspx", new { controller = "account", action = "redirect" }, new { httpMethod = new HttpMethodConstraint("GET") });
               
            routes.MapRoute("product-details", "product/{id}", new { controller = "product", action = "Details" }, new { httpMethod = new HttpMethodConstraint("GET"), id = @"[0-9]+$" });
            routes.MapRoute("product-reviews-", "product/Allreviews/{id}", new { controller = "product", action = "Allreviews" }, new { httpMethod = new HttpMethodConstraint("GET"), id = @"[0-9]+$" });
            routes.MapRoute("product-writereviews-", "product/Writereview/{id}", new { controller = "product", action = "Writereview" }, new { httpMethod = new HttpMethodConstraint("GET"), id = @"[0-9]+$" });
            routes.MapRoute("product-writereviews1-", "product/Writereview/{id}", new { controller = "product", action = "Writereview" }, new { httpMethod = new HttpMethodConstraint("POST"), id = @"[0-9]+$" });

            routes.MapRoute("product-attribute-details", "product/GetAttributes/{id}", new { controller = "product", action = "GetAttributes" }, new { httpMethod = new HttpMethodConstraint("GET"), id = @"[0-9]+$" });

            routes.MapRoute("product-wishlist-details", "product/WishList/{id}", new { controller = "product", action = "WishList" }, new { httpMethod = new HttpMethodConstraint("GET"), id = @"[0-9]+$" });
            routes.MapRoute("product-wishlist-details1", "product/WishList/{id}", new { controller = "product", action = "WishList" }, new { httpMethod = new HttpMethodConstraint("POST"), id = @"[0-9]+$" });

            routes.MapRoute("seoproduct-details", "product/{seo}/{id}", new { controller = "product", action = "Details" }, new { httpMethod = new HttpMethodConstraint("GET"), seo = @"[\w-]+$", id = @"[0-9]+$" });

			routes.MapRoute(
				name: "Default",
				url: "{controller}/{action}/{id}",
				defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}