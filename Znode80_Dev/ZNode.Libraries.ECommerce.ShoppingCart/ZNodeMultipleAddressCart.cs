﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Znode.Engine.Promotions;
using Znode.Engine.Shipping;
using Znode.Engine.Taxes;

namespace ZNode.Libraries.ECommerce.ShoppingCart
{
	[Serializable()]
	public class ZNodeMultipleAddressCart: ZNodeShoppingCart
	{
        public Guid AddressCartID { get; set; }

		public int AddressID { get; set; }

		public int OrderShipmentID { get; set; }

		public ZNodeMultipleAddressCart() : base()
		{
            AddressCartID = System.Guid.NewGuid();
		}

		/// <summary>
		/// Calculates final pricing, shipping and taxes in the cart.
		/// </summary>
		public override void Calculate()
		{
			// Clear previous messages
			this._ErrorMessage = new StringBuilder();

			// ShippingRules
			var shipping = new ZnodeShippingManager(this);
			shipping.Calculate();

			//// Promotions
			//var promotionRules = new ZnodeCartPromotionManager(this);
			//promotionRules.Calculate();

			//// TaxRules
			var taxRules = new ZnodeTaxManager(this);
			taxRules.Calculate(this);
		}
	}
}
