using System;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Shipping
{
	/// <summary>
	/// Calculates the size of a package from a list of products. 
	/// </summary>    
	public class ZnodeShippingPackage : ZNodeBusinessBase
	{
		public decimal Width { get; set; }
		public decimal Height { get; set; }
		public decimal Length { get; set; }
		public decimal Weight { get; set; }
		public decimal Value { get; set; }
		public bool UseGrossValue { get; set; }
		public bool RoundWeightToNextIncrement { get; set; }
		public ZNodeShoppingCartItem ShoppingCartItem { get; set; }
		public ZNodeGenericCollection<ZNodeShoppingCartItem> ShoppingCartItems { get; set; }
		public TList<AddOnValue> AddOns { get; set; }
		public ZNodeAddOnValueEntity AddOnValue { get; set; }
		public TList<Product> Products { get; set; }

		public ZnodeShippingPackage(ZNodeGenericCollection<ZNodeShoppingCartItem> shoppingCartItems) : this(shoppingCartItems, false, false)
		{
		}

		public ZnodeShippingPackage(ZNodeGenericCollection<ZNodeShoppingCartItem> shoppingCartItems, bool roundWeightToNextIncrement, bool useGrossValue)
		{
			ShoppingCartItem = new ZNodeShoppingCartItem();
			ShoppingCartItems = shoppingCartItems;
			AddOns = new TList<AddOnValue>();
			AddOnValue = new ZNodeAddOnValueEntity();
			Products = new TList<Product>();
			RoundWeightToNextIncrement = roundWeightToNextIncrement;
			UseGrossValue = useGrossValue;
			EstimateShipmentPackageForShoppingCartItems();
		}

		public ZnodeShippingPackage(TList<Product> products, TList<AddOnValue> addOns)
		{
			ShoppingCartItem = new ZNodeShoppingCartItem();
			ShoppingCartItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();
			AddOns = addOns;
			AddOnValue = new ZNodeAddOnValueEntity();
			Products = products;
			RoundWeightToNextIncrement = true;
			UseGrossValue = false;
			EstimateShipmentPackageForProductsAndAddOns();
		}

		private void EstimatePackageDimensions(decimal weight, decimal height, decimal width, decimal length, decimal price, ref int quantity, ref decimal totalVolume, ref decimal totalWeight, ref decimal totalValue, decimal itemWeight, bool getsFreeShipping)
		{
			if (!getsFreeShipping)
			{
				// Get dimensions
				totalVolume += (height * width * length) * quantity;

				// Check to round the weight up to the next pound
				if (RoundWeightToNextIncrement)
				{
					itemWeight += Math.Round(weight + 0.51m, 0);
				}
				else
				{
					itemWeight += weight;
				}

				if (UseGrossValue)
				{
					totalValue += price * quantity;
					itemWeight *= quantity;
				}
				else
				{
					totalValue += price;
				}

				totalWeight += itemWeight;
			}
		}

		/// <summary>
		/// Estimates the total package size and weight when given list of products and addons.
		/// </summary>
		private void EstimateShipmentPackageForProductsAndAddOns()
		{
			// We are going to get a very approximate package size by taking the total volume of all items in
			// the cart and then getting the cube root of that volume. This will give us a minimum package size.
			// NOTE: This will underestimate the total package volume since packages will rarely be cubes!

			decimal totalVolume = 0;
			decimal totalWeight = 0;
			decimal totalValue = 0;
           
			foreach (var p in Products)
			{
				// Quantity is set to 1 because we're sending in a product row for every single item in the order
				var quantity = 1;

				if (p.Weight.HasValue && p.Height.HasValue && p.Width.HasValue && p.Length.HasValue && p.RetailPrice.HasValue)
				{
					decimal itemWeight = 0;
					EstimatePackageDimensions(p.Weight.Value, p.Height.Value, p.Width.Value, p.Length.Value, p.RetailPrice.Value, ref quantity, ref totalVolume, ref totalWeight, ref totalValue, itemWeight, false);
				}
			}

			foreach (var a in AddOns)
			{
				if (!a.Length.HasValue) a.Length = 0M;
				if (!a.Width.HasValue) a.Width = 0M;
				if (!a.Height.HasValue) a.Height = 0M;

				// Quantity is set to 1 because we're sending in a product row for every single item in the order
				var quantity = 1;

				decimal itemWeight = 0;
				EstimatePackageDimensions(a.Weight, a.Height.Value, a.Width.Value, a.Length.Value, a.RetailPrice, ref quantity, ref totalVolume, ref totalWeight, ref totalValue, itemWeight, false);
			}

			// Approximate dimensions by taking the qube root of the total volume
			var dimension = (decimal)Math.Pow(Convert.ToDouble(totalVolume), 1.0 / 3.0);
			dimension = Math.Round(dimension);
			Height = dimension;
			Length = dimension;
			Width = dimension;
			Weight = totalWeight;
			Value = totalValue; // Total product and addon value
		}

		/// <summary>
		/// Estimates the total package size and weight for items in the shopping cart.
		/// </summary>
		private void EstimateShipmentPackageForShoppingCartItems()
		{
			// We are going to get a very approximate package size by taking the total volume of all items in
			// the cart and then getting the cube root of that volume. This will give us a minimum package size.
			// NOTE: This will underestimate the total package volume since packages will rarely be cubes!

			decimal totalVolume = 0;
			decimal totalWeight = 0;
			decimal totalValue = 0;

			foreach (ZNodeShoppingCartItem cartItem in ShoppingCartItems)
			{
				var quantity = cartItem.Quantity;
				decimal itemWeight = 0;
				EstimatePackageDimensions(cartItem.Product.Weight, cartItem.Product.Height, cartItem.Product.Width, cartItem.Product.Length, cartItem.TieredPricing, ref quantity, ref totalVolume, ref totalWeight, ref totalValue, itemWeight, cartItem.Product.FreeShippingInd);

				// Get each addon dimensions
				foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
				{
					foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
					{
						EstimatePackageDimensions(addOnValue.Weight, addOnValue.Height, addOnValue.Width, addOnValue.Length, addOnValue.FinalPrice, ref quantity, ref totalVolume, ref totalWeight, ref totalValue, itemWeight, addOnValue.FreeShippingInd);
					}
				}
			}

			// Approximate dimensions by taking the qube root of the total volume
			var dimension = (decimal)Math.Pow(Convert.ToDouble(totalVolume), 1.0 / 3.0);
			dimension = Math.Round(dimension);
			Height = dimension;
			Length = dimension;
			Width = dimension;
			Weight = totalWeight;
			Value = totalValue; // Total product and addon value
		}
	}
}
