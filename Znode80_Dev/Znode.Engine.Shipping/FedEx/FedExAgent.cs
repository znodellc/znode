using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Services.Protocols;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Shipping.FedEx
{
	public class FedExAgent : ZNodeBusinessBase
	{
		private string _errorDescription;

		public string ClientProductId { get; set; }
		public string ClientProductVersion { get; set; }
		public string CspAccessKey { get; set; }
		public string CspPassword { get; set; }
		public string CurrencyCode { get; set; }
		public string DimensionUnit { get; set; }
		public string DropOffType { get; set; }
		public string ErrorCode { get; set; }
		public string FedExAccessKey { get; set; }
		public string FedExAccountNumber { get; set; }
		public string FedExGatewayUrl { get; set; }
		public string FedExMeterNumber { get; set; }
		public string FedExSecurityCode { get; set; }
		public string FedExServiceType { get; set; }
		public byte[] LabelImage { get; set; }
		public string OriginLocationId { get; set; }
		public string PackageHeight { get; set; }
		public string PackageLength { get; set; }
		public string PackageTypeCode { get; set; }
		public decimal PackageWeight { get; set; }
		public string PackageWidth { get; set; }
		public decimal ShipmentCharge { get; set; }
		public string ShipperAddress1 { get; set; }
		public string ShipperAddress2 { get; set; }
		public bool ShipperAddressIsResidential { get; set; }
		public string ShipperCity { get; set; }
		public string ShipperCompany { get; set; }
		public string ShipperCountryCode { get; set; }
		public string ShipperPhone { get; set; }
		public string ShipperStateCode { get; set; }
		public string ShipperZipCode { get; set; }
		public string ShipToAddress1 { get; set; }
		public string ShipToAddress2 { get; set; }
		public bool ShipToAddressIsResidential { get; set; }
		public string ShipToCity { get; set; }
		public string ShipToCompany { get; set; }
		public string ShipToCountryCode { get; set; }
		public string ShipToFirstName { get; set; }
		public string ShipToLastName { get; set; }
		public string ShipToPhone { get; set; }
		public string ShipToStateCode { get; set; }
		public string ShipToZipCode { get; set; }
		public string TrackingNumber { get; set; }
		public decimal TotalCustomsValue { get; set; }
		public decimal TotalInsuredValue { get; set; }
		public bool UseDiscountRate { get; set; }
		public string VendorProductPlatform { get; set; }
		public string WeightUnit { get; set; }

		public string ErrorDescription
		{
			get
			{
				// FedEx returns "Service type is missing or invalid". Replace this with a user friendly message.
				if (ErrorCode.Equals("540"))
				{
					_errorDescription = "FedEx does not support the selected shipping option to this zip code. Please select another shipping option.";
				}

				return _errorDescription;
			}

			set
			{
				_errorDescription = value;
			}
		}

		public FedExAgent()
		{
			// Get config and set other defaults
			FedExGatewayUrl = ConfigurationManager.AppSettings["FedExGatewayURL"];
			CurrencyCode = "USD";
			DimensionUnit = "IN";
			DropOffType = "REGULAR_PICKUP";
			ErrorCode = "0";
			PackageHeight = "1";
			PackageLength = "1";
			PackageTypeCode = "YOUR_PACKAGING";
			PackageWeight = 0;
			PackageWidth = "1";
			ShipperAddressIsResidential = false;
			ShipToAddressIsResidential = false;
			TotalCustomsValue = 0;
			TotalInsuredValue = 0;
			UseDiscountRate = false;
			WeightUnit = "LB";
		}

		public decimal GetShippingRate()
		{
			decimal shippingRate = 0;

			try
			{
				// Instantiate the FedEx web service
				var service = new FedExService.RateService { Url = FedExGatewayUrl };

				// Create the rate request and get the reply from FedEx
				var request = CreateRateRequest();
				var reply = service.getRates(request);

				// For most FedEx calls we can consider Note, Warning, and above succcess
				if (IsRateResponseValid(reply.HighestSeverity))
				{
					ErrorDescription = reply.Notifications[0].Message;
					ErrorCode = "0";

					foreach (var rateDetail in reply.RateReplyDetails)
					{
						foreach (var shipmentDetail in rateDetail.RatedShipmentDetails)
						{
							if (!UseDiscountRate && shipmentDetail.EffectiveNetDiscount != null)
							{
								shippingRate += shipmentDetail.ShipmentRateDetail.TotalNetCharge.Amount - shipmentDetail.EffectiveNetDiscount.Amount;
								break;
							}
							else
							{
								shippingRate += shipmentDetail.ShipmentRateDetail.TotalNetCharge.Amount;
								break;
							}
						}
					}
				}
				else
				{
					ErrorCode = reply.Notifications[0].Code;
					ErrorDescription = reply.Notifications[0].Message;
				}
			}
			catch (SoapException ex)
			{
				ErrorCode = "-1";
				ErrorDescription = ex.Detail.InnerText;
			}
			catch (Exception ex)
			{
				ErrorCode = "-1";
				ErrorDescription = ex.Message;
			}

			return shippingRate;
		}

		private FedExService.RateRequest CreateRateRequest()
		{
			// Build the RateRequest
			var request = new FedExService.RateRequest();
			request.WebAuthenticationDetail = new FedExService.WebAuthenticationDetail();
			request.WebAuthenticationDetail.UserCredential = new FedExService.WebAuthenticationCredential();
			request.WebAuthenticationDetail.UserCredential.Key = FedExAccessKey;
			request.WebAuthenticationDetail.UserCredential.Password = FedExSecurityCode;

			request.ClientDetail = new FedExService.ClientDetail();
			request.ClientDetail.AccountNumber = FedExAccountNumber;
			request.ClientDetail.MeterNumber = FedExMeterNumber;

			request.TransactionDetail = new FedExService.TransactionDetail();

			// This is a reference field for the customer, any value can be used and will be provided in the response
			request.TransactionDetail.CustomerTransactionId = "Rate Request";

			// WSDL version information, value is automatically set from WSDL
			request.Version = new FedExService.VersionId();
			request.ReturnTransitAndCommit = true;
			request.ReturnTransitAndCommitSpecified = true;
			request.CarrierCodes = new FedExService.CarrierCodeType[1];
			request.CarrierCodes[0] = FedExService.CarrierCodeType.FDXE;

			SetShipmentDetails(request);
			SetOrigin(request);
			SetDestination(request);
			SetPayment(request);
			SetIndividualPackageLineItems(request);

			return request;
		}

		private void SetShipmentDetails(FedExService.RateRequest request)
		{
			request.RequestedShipment = new FedExService.RequestedShipment();

			// Drop off types are BUSINESS_SERVICE_CENTER, DROP_BOX, REGULAR_PICKUP, REQUEST_COURIER, STATION
			request.RequestedShipment.DropoffType = (FedExService.DropoffType)Enum.Parse(typeof(FedExService.DropoffType), DropOffType);

			// Service types are STANDARD_OVERNIGHT, PRIORITY_OVERNIGHT, FEDEX_GROUND, etc.
			request.RequestedShipment.ServiceType = (FedExService.ServiceType)Enum.Parse(typeof(FedExService.ServiceType), FedExServiceType);
			request.RequestedShipment.ServiceTypeSpecified = true;

			// Packaging type FEDEX_BOK, FEDEX_PAK, FEDEX_TUBE, YOUR_PACKAGING, etc.
			request.RequestedShipment.PackagingType = (FedExService.PackagingType)Enum.Parse(typeof(FedExService.PackagingType), PackageTypeCode);
			request.RequestedShipment.PackagingTypeSpecified = true;

			// If TotalInsured value is greater than 0, then set related properties
			if (TotalInsuredValue > 0)
			{
				request.RequestedShipment.TotalInsuredValue = new FedExService.Money();
				request.RequestedShipment.TotalInsuredValue.Amount = TotalInsuredValue;
				request.RequestedShipment.TotalInsuredValue.Currency = CurrencyCode;
			}

			// Shipping date and time
			request.RequestedShipment.ShipTimestamp = DateTime.Now;
			request.RequestedShipment.ShipTimestampSpecified = true;

			if (UseDiscountRate)
			{
				// If enabled, set "Account" as the element value, it will send discount rates in the reply
				request.RequestedShipment.RateRequestTypes = new FedExService.RateRequestType[1] { FedExService.RateRequestType.ACCOUNT };
			}
			else
			{
				request.RequestedShipment.RateRequestTypes = new FedExService.RateRequestType[1] { FedExService.RateRequestType.LIST };
			}

			request.RequestedShipment.PackageDetail = FedExService.RequestedPackageDetailType.INDIVIDUAL_PACKAGES;
			request.RequestedShipment.PackageDetailSpecified = true;
		}

		private void SetOrigin(FedExService.RateRequest request)
		{
			request.RequestedShipment.Shipper = new FedExService.Party();
			request.RequestedShipment.Shipper.Address = new FedExService.Address();

			// Resdential or Standard
			request.RequestedShipment.Shipper.Address.Residential = ShipperAddressIsResidential;
			request.RequestedShipment.Shipper.Address.StreetLines = new string[2] { ShipperAddress1, ShipperAddress2 };
			request.RequestedShipment.Shipper.Address.City = ShipperCity;
			request.RequestedShipment.Shipper.Address.StateOrProvinceCode = ShipperStateCode;
			request.RequestedShipment.Shipper.Address.PostalCode = ShipperZipCode;
			request.RequestedShipment.Shipper.Address.CountryCode = ShipperCountryCode;
		}

		private void SetDestination(FedExService.RateRequest request)
		{
			request.RequestedShipment.Recipient = new FedExService.Party();
			request.RequestedShipment.Recipient.Address = new FedExService.Address();
			request.RequestedShipment.Recipient.Address.StreetLines = new string[2] { ShipToAddress1, ShipToAddress2 };
			request.RequestedShipment.Recipient.Address.City = ShipToCity;
			request.RequestedShipment.Recipient.Address.StateOrProvinceCode = ShipToStateCode;
			request.RequestedShipment.Recipient.Address.PostalCode = ShipToZipCode;
			request.RequestedShipment.Recipient.Address.CountryCode = ShipToCountryCode;
			request.RequestedShipment.Recipient.Address.Residential = ShipToAddressIsResidential;
		}

		private void SetPayment(FedExService.RateRequest request)
		{
			request.RequestedShipment.ShippingChargesPayment = new FedExService.Payment();

			// Payment options are RECIPIENT, SENDER, THIRD_PARTY
			request.RequestedShipment.ShippingChargesPayment.PaymentType = FedExService.PaymentType.SENDER;
			request.RequestedShipment.ShippingChargesPayment.PaymentTypeSpecified = true;
			request.RequestedShipment.ShippingChargesPayment.Payor = new FedExService.Payor();
			request.RequestedShipment.ShippingChargesPayment.Payor.AccountNumber = FedExAccountNumber;
		}

		private void SetIndividualPackageLineItems(FedExService.RateRequest request)
		{
			// Passing individual pieces rate request            
			request.RequestedShipment.PackageCount = "1";

			request.RequestedShipment.RequestedPackageLineItems = new FedExService.RequestedPackageLineItem[1];
			request.RequestedShipment.RequestedPackageLineItems[0] = new FedExService.RequestedPackageLineItem();

			// Set the package sequence number
			request.RequestedShipment.RequestedPackageLineItems[0].SequenceNumber = "1";

			// Set the package weight
			request.RequestedShipment.RequestedPackageLineItems[0].Weight = new FedExService.Weight();
			request.RequestedShipment.RequestedPackageLineItems[0].Weight.Value = PackageWeight;
			request.RequestedShipment.RequestedPackageLineItems[0].Weight.Units = (FedExService.WeightUnits)Enum.Parse(typeof(FedExService.WeightUnits), WeightUnit);

			if (PackageTypeCode == FedExService.PackagingType.YOUR_PACKAGING.ToString())
			{
				// Set the package dimensions
				request.RequestedShipment.RequestedPackageLineItems[0].Dimensions = new FedExService.Dimensions();
				request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Length = PackageLength;
				request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Width = PackageWidth;
				request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Height = PackageHeight;
				request.RequestedShipment.RequestedPackageLineItems[0].Dimensions.Units = (FedExService.LinearUnits)Enum.Parse(typeof(FedExService.LinearUnits), DimensionUnit);
			}
		}

		private bool IsRateResponseValid(FedExService.NotificationSeverityType highestSeverity)
		{
			// SUCCESS, NOTE, or WARNING equals a valid response
			if (highestSeverity == FedExService.NotificationSeverityType.SUCCESS ||
				highestSeverity == FedExService.NotificationSeverityType.NOTE ||
				highestSeverity == FedExService.NotificationSeverityType.WARNING)
			{
				return true;
			}

			// Otherwise
			return false;
        }


        



    }
}
