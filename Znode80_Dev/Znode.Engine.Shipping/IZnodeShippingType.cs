﻿using System.Collections.ObjectModel;
using ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Shipping
{
	/// <summary>
	/// This is the root interface for all shipping types.
	/// </summary>
	public interface IZnodeShippingType : IZnodeProviderType
	{
		Collection<ZnodeShippingControl> Controls { get; }

		void Bind(ZNodeShoppingCart shoppingCart, ZnodeShippingBag shippingBag);
		void Calculate();
		bool PreSubmitOrderProcess();
		void PostSubmitOrderProcess();
	}
}
