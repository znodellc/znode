namespace Znode.Engine.Shipping
{
	public class ZnodeShippingCustom : ZnodeShippingType
	{
		public ZnodeShippingCustom()
		{
			Name = "Custom";
			Description = "Calculates custom shipping rates.";

			Controls.Add(ZnodeShippingControl.Profile);
			Controls.Add(ZnodeShippingControl.DisplayName);
			Controls.Add(ZnodeShippingControl.InternalCode);
			Controls.Add(ZnodeShippingControl.HandlingCharge);
			Controls.Add(ZnodeShippingControl.Countries);
		}

		/// <summary>
		/// Calculates custom shipping rates.
		/// </summary>
		public override void Calculate()
		{
			var flatRateShipping = new ZnodeShippingCustomFlatRate();
			flatRateShipping.Calculate(ShoppingCart, ShippingBag);

			var quantityBasedShipping = new ZnodeShippingCustomQuantity();
			quantityBasedShipping.Calculate(ShoppingCart, ShippingBag);

			var weightBasedShipping = new ZnodeShippingCustomWeight();
			weightBasedShipping.Calculate(ShoppingCart, ShippingBag);

			var fixedRateShipping = new ZnodeShippingCustomFixedRate();
			fixedRateShipping.Calculate(ShippingBag);

			// Apply handling charge
			if (ShippingBag.PackageItems.Count == 0 && ShippingBag.ShipSeparatelyItems.Count == 0 && ShippingBag.ApplyPackageItemHandlingCharge)
			{
				ShoppingCart.Shipping.ShippingHandlingCharge = ShippingBag.HandlingCharge;
			}
			else if (ShippingBag.PackageItems.Count > 0 && ShippingBag.ApplyPackageItemHandlingCharge)
			{
				ShoppingCart.Shipping.ShippingHandlingCharge = ShippingBag.HandlingCharge;
			}
		}
	}
}
