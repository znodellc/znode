using System;
using System.Configuration;
using System.Net;
using System.Text;
using System.Xml;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Shipping.Ups
{
	public class UpsAgent : ZNodeBusinessBase
	{
		public string ErrorCode { get; set; }
		public string ErrorDescription { get; set; }
		public decimal PackageHeight { get; set; }
		public decimal PackageLength { get; set; }
		public string PackageTypeCode { get; set; }
		public decimal PackageWeight { get; set; }
		public decimal PackageWidth { get; set; }
		public string PickupType { get; set; }
		public string ShipperCountryCode { get; set; }
		public string ShipperZipCode { get; set; }		
		public string ShipToAddressType { get; set; }
		public string ShipToCountryCode { get; set; }
		public string ShipToZipCode { get; set; }
		public string UpsGatewayUrl { get; set; }
		public string UpsKey { get; set; }
		public string UpsPassword { get; set; }
		public string UpsServiceCode { get; set; }
		public string UpsUserId { get; set; }
		public string WeightUnit { get; set; }

		public UpsAgent()
		{
			// Get gateway URL and set other defaults
			UpsGatewayUrl = ConfigurationManager.AppSettings["UPSGatewayURL"];
			PickupType = "One Time Pickup";
			ShipToAddressType = "Residential";
			WeightUnit = "LBS";
		}

		public decimal GetShippingRate()
		{
			// The 0 is the commercial address type
			var addressType = ShipToAddressType == "Residential" ? "1" : "0";

			// Build the payload for sending to UPS
			var payload = new StringBuilder();
			payload.Append("<?xml version='1.0'?>");
			payload.Append("<AccessRequest xml:lang='en-US'>");
			payload.Append("	<AccessLicenseNumber>" + UpsKey + "</AccessLicenseNumber>");
			payload.Append("	<UserId>" + UpsUserId + "</UserId>");
			payload.Append("	<Password>" + UpsPassword + "</Password>");
			payload.Append("</AccessRequest>");
			payload.Append("<?xml version='1.0'?>");
			payload.Append("<RatingServiceSelectionRequest xml:lang='en-US'>");
			payload.Append("	<Request>");
			payload.Append("		<TransactionReference>");
			payload.Append("			<CustomerContext>Rating and Service</CustomerContext>");
			payload.Append("			<XpciVersion>1.0001</XpciVersion>");
			payload.Append("		</TransactionReference>");
			payload.Append("		<RequestAction>Rate</RequestAction>");
			payload.Append("		<RequestOption>Rate</RequestOption>");
			payload.Append("	</Request>");
			payload.Append("	<PickupType>");
			payload.Append("		<Code>" + PickupType + "</Code>");
			payload.Append("	</PickupType>");
			payload.Append("	<Shipment>");
			payload.Append("		<Shipper>");
			payload.Append("			<Address>");
			payload.Append("				<PostalCode>" + ShipperZipCode + "</PostalCode>");
			payload.Append("				<CountryCode>" + ShipperCountryCode + "</CountryCode>");
			payload.Append("			</Address>");
			payload.Append("		</Shipper>");
			payload.Append("		<ShipTo>");
			payload.Append("			<Address>");
			payload.Append("				<PostalCode>" + ShipToZipCode + "</PostalCode>");
			payload.Append("				<CountryCode>" + ShipToCountryCode + "</CountryCode>");
			payload.Append("				<ResidentialAddress>" + addressType + "</ResidentialAddress>");
			payload.Append("			</Address>");
			payload.Append("		</ShipTo>");
			payload.Append("		<Service>");
			payload.Append("			<Code>" + UpsServiceCode + "</Code>");
			payload.Append("		</Service>");
			payload.Append("		<Package>");
			payload.Append("			<PackagingType>");
			payload.Append("				<Code>" + PackageTypeCode + "</Code>");
			payload.Append("			</PackagingType>");
			payload.Append("			<Dimensions>");
			payload.Append("				<UnitOfMeasurement>");
			payload.Append("					<Code>IN</Code>");
			payload.Append("				</UnitOfMeasurement>");
			payload.Append("				<Length>" + PackageLength + "</Length>");
			payload.Append("				<Width>" + PackageWidth + "</Width>");
			payload.Append("				<Height>" + PackageHeight + "</Height>");
			payload.Append("			</Dimensions>");
			payload.Append("			<PackageWeight>");
			payload.Append("				<UnitOfMeasurement>");
			payload.Append("					<Code>" + WeightUnit + "</Code>");
			payload.Append("				</UnitOfMeasurement>");
			payload.Append("				<Weight>" + PackageWeight + "</Weight>");
			payload.Append("			</PackageWeight>");
			payload.Append("		</Package>");
			payload.Append("	</Shipment>");
			payload.Append("</RatingServiceSelectionRequest>");

			try
			{
				// Instantiate the request
				var request = new WebClient();
				request.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

				// Convert the payload into byte array and send the request to UPS
				var requestBytes = Encoding.ASCII.GetBytes(payload.ToString());
				var responseBytes = request.UploadData(UpsGatewayUrl, "POST", requestBytes);

				// Decode the response bytes into an XML string and create an XML document from it
				var xmlResponse = Encoding.ASCII.GetString(responseBytes);
				var xmlDocument = new XmlDocument();
				xmlDocument.LoadXml(xmlResponse);

				decimal shippingRate = 0;

				var responseCodesNodes = xmlDocument.SelectNodes("RatingServiceSelectionResponse/Response/ResponseStatusCode");
				if (responseCodesNodes != null && responseCodesNodes.Count > 0 && responseCodesNodes[0].InnerText.Equals("1"))
				{
					// Get response charges
					var chargesNodes = xmlDocument.SelectNodes("RatingServiceSelectionResponse/RatedShipment/TotalCharges/MonetaryValue");
					if (chargesNodes != null && chargesNodes.Count > 0)
					{
						ErrorCode = "0";
						shippingRate = Decimal.Parse(chargesNodes[0].InnerText);
					}
				}
				else
				{
					// Get response error code and description
					ErrorCode = xmlDocument.SelectNodes("RatingServiceSelectionResponse/Response/Error/ErrorCode")[0].InnerText;
					ErrorDescription = xmlDocument.SelectNodes("RatingServiceSelectionResponse/Response/Error/ErrorDescription")[0].InnerText;
				}

				return shippingRate;
			}
			catch (Exception)
			{
				ErrorCode = "Connection failed.";
				ErrorDescription = "Error while trying to connect with host server. Please try again.";
				return 0;
			}
		}
	}
}
