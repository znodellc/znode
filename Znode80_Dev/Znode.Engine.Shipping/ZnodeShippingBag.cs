using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Shipping
{
	/// <summary>
	/// Property bag of settings used by the shipping types. 
	/// </summary>
    public class ZnodeShippingBag
    {
		private ZNodeGenericCollection<ZNodeShoppingCartItem> _shipSeparatelyItems;
        private ZNodeGenericCollection<ZNodeShoppingCartItem> _packageItems;

		public decimal HandlingCharge { get; set; }
		public string ShippingCode { get; set; }
		public bool ApplyPackageItemHandlingCharge { get; set; }
		public ZNodeShoppingCart ShoppingCart { get; set; }
		public TList<ShippingRule> ShippingRules { get; set; }

		public ZNodeGenericCollection<ZNodeShoppingCartItem> ShipSeparatelyItems
        {
            get
            {
                if (_shipSeparatelyItems == null)
                {
                    _shipSeparatelyItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();

                    foreach (ZNodeShoppingCartItem item in ShoppingCart.ShoppingCartItems)
                    {
                        if (item.Product.ShipSeparately && !item.Product.FreeShippingInd)
                        {
                            _shipSeparatelyItems.Add(item);
                        }
                    }
                }

                return _shipSeparatelyItems;
            }
        }

        public ZNodeGenericCollection<ZNodeShoppingCartItem> PackageItems
        {
            get
            {
                if (_packageItems == null)
                {
                    _packageItems = new ZNodeGenericCollection<ZNodeShoppingCartItem>();

                    foreach (ZNodeShoppingCartItem item in ShoppingCart.ShoppingCartItems)
                    {
                        if (!item.Product.ShipSeparately && !item.Product.FreeShippingInd)
                        {
                            _packageItems.Add(item);
                            ApplyPackageItemHandlingCharge = true;                           
                        }

                        foreach (ZNodeAddOnEntity addOn in item.Product.SelectedAddOns.AddOnCollection)
                        {
                            foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
                            {
                                if (!addOnValue.FreeShippingInd)
                                {
                                    ApplyPackageItemHandlingCharge = true;
                                }
                            }
                        } 
                    }
                }

                return _packageItems;
            }
        }
    }
}
