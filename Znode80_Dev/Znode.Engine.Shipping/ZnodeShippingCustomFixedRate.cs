﻿using System.Collections.Generic;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Shipping
{
	public class ZnodeShippingCustomFixedRate : ZNodeBusinessBase
	{
		/// <summary>
		/// Calculates the custom fixed shipping rate.
		/// </summary>
		/// <param name="shippingBag">The shipping data for the custom shipping type.</param>
		public void Calculate(ZnodeShippingBag shippingBag)
		{
			// Filter for fixed rate per item (ShippingRuleTypeID = 3 in the ZNodeShippingRuleType table)
			var shippingRules = shippingBag.ShippingRules.FindAll(ShippingRuleColumn.ShippingRuleTypeID,
																  (int)ZnodeShippingRuleType.FixedRatePerItem);

			// Determine fixed shipping rate for each item
			foreach (ZNodeShoppingCartItem cartItem in shippingBag.ShoppingCart.ShoppingCartItems)
			{
				var shippingRuleTypeId = cartItem.Product.ShippingRuleTypeID.GetValueOrDefault(-1);
				decimal itemShippingCost = 0;

				if (shippingRuleTypeId == (int)ZnodeShippingRuleType.FixedRatePerItem &&
					!cartItem.Product.FreeShippingInd)
				{
					foreach (var rule in shippingRules)
					{
						itemShippingCost += rule.BaseCost;
					}

					itemShippingCost += cartItem.Product.ShippingRate;

					cartItem.Product.ShippingCost = itemShippingCost;
				}
				// Reset if rule is applied
				var isRuleApplied = true;

				var applyHandlingCharge = false;

				// Get flat rate for addons
				foreach (ZNodeAddOnEntity addOn in cartItem.Product.SelectedAddOns.AddOnCollection)
				{
					foreach (ZNodeAddOnValueEntity addOnValue in addOn.AddOnValueCollection)
					{
						var getsFreeShipping = addOnValue.FreeShippingInd;
						shippingRuleTypeId = addOnValue.ShippingRuleTypeID.GetValueOrDefault(-1);

						if (shippingRuleTypeId == (int)ZnodeShippingRuleType.FixedRatePerItem && !getsFreeShipping)
						{
							isRuleApplied &= ApplyRule(shippingRules, out itemShippingCost);

							applyHandlingCharge |= isRuleApplied;

							if (isRuleApplied)
							{
								addOnValue.ShippingCost = itemShippingCost;
							}
						}
					}
				}

				if (applyHandlingCharge && cartItem.Product.ShipSeparately)
				{
					shippingBag.ShoppingCart.OrderLevelShipping += shippingBag.HandlingCharge;
				}
			}
		}

		private bool ApplyRule(IEnumerable<ShippingRule> shippingRules, out decimal itemShippingCost)
		{
			var isRuleApplied = false;
			itemShippingCost = 0;

			foreach (var rule in shippingRules)
			{
				itemShippingCost += rule.BaseCost;
				isRuleApplied = true;
			}

			return isRuleApplied;
		}
	}
}
