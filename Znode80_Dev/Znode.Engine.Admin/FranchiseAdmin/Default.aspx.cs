using System;
using System.Web;
using System.Web.Security;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.FranchiseAdmin
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Default class.
    /// </summary>
    public partial class Admin_Default : System.Web.UI.Page
    {

        #region Variables

        //Znode Version 7.2.2
        //Franchise Section, Error message for account gets locked - Start 
        //Gets the membership provider details for the key "ZNodeMembershipProvider".

        #region Private Variables
        private MembershipProvider _membershipProvider = Membership.Providers["ZNodeMembershipProvider"];
        #endregion

        #region Public Variables
        public MembershipProvider MembershipProvider
        {
            get { return _membershipProvider; }
            set { _membershipProvider = value; }
        }
        #endregion

        //Franchise Section, Error message for account gets locked - End
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Session.Abandon();

                FormsAuthentication.SignOut();
            }

            // Instantiated just to trigger licensing > Do not remove!
            ZNodeHelper hlp = new ZNodeHelper();

            // Set input focus on the page load
            UserName.Focus();

            // Check if SSL is required
            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {
                    string inURL = Request.Url.ToString();
                    string outURL = inURL.ToLower().Replace("http://", "https://");

                    Response.Redirect(outURL);
                }
            }

            // Forgot password link
            string link = "~/FranchiseAdmin/ForgotPassword.aspx";
            forgotPasswordLink.HRef = link.ToLower().Replace("https://", "http://");

            link = Request.Url.GetLeftPart(UriPartial.Authority) + Response.ApplyAppPathModifier("Signup.aspx");
            SignupLink.HRef = link.ToLower().Replace("https://", "http://");            
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            ZNodeUserAccount userAccount = new ZNodeUserAccount();
            bool isLoginSuccess = userAccount.Login(ZNodeConfigManager.SiteConfig.PortalID, UserName.Text.Trim(), Password.Text.Trim());

            if (isLoginSuccess)
            {
                if (Roles.IsUserInRole(UserName.Text.Trim(), "FRANCHISE") || Roles.IsUserInRole(UserName.Text.Trim(), "ADMIN"))
                {
                    string returnValue = string.Empty;

                    bool isLastPasswordChanged = ZNodeUserAccount.CheckLastPasswordChangeDate((Guid)userAccount.UserID, out returnValue);

                    if (!isLastPasswordChanged)
                    {
                        ZNode.Libraries.DataAccess.Service.AccountService acctService = new ZNode.Libraries.DataAccess.Service.AccountService();
                        ZNode.Libraries.DataAccess.Entities.Account account = acctService.GetByAccountID(userAccount.AccountID);

                        // Set Error Code to session Object
                        Session.Add("ErrorCode", returnValue);

                        // Get account and set to session
                        Session.Add("AccountObject", account);

                        Response.Redirect("~/FranchiseAdmin/ResetPassword.aspx");
                    }

                    // Set CurrentUserProfile ProfileID
                    userAccount.ProfileID = ZNodeProfile.CurrentUserProfileId;

                    // Get account and set to session
                    Session.Add(ZNodeSessionKeyType.UserAccount.ToString(), userAccount);

                    FormsAuthentication.SetAuthCookie(UserName.Text.Trim(), false);

                    // If user is an admin, then redirect to the admin dashboard
                    if (Roles.IsUserInRole(UserName.Text.Trim(), "ADMIN") && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
                    {
                        Response.Redirect("~/secure/default.aspx", true);
                    }
                    else if (Roles.IsUserInRole(UserName.Text.Trim(), "FRANCHISE") && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
                    {
                        Response.Redirect("~/FranchiseAdmin/secure/default.aspx", true);
                    }

                    FormsAuthentication.RedirectFromLoginPage(UserName.Text.Trim(), false);
                }
                else
                {
                    FailureText.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInvalidUsername").ToString(); 
                }                
            }
            else
            {
                //Znode Version 7.2.2
                //Franchise Section, Error message for account gets locked - Start 
                //Set the Error messages in case of Account gets locked. And Also display warning messages before the final last two attempts.


                //Gets the user details based on username.
                MembershipUser membershipUser = Membership.GetUser(UserName.Text.Trim());

                if (membershipUser != null)
                {
                    //Check whether user accoung is locked or not.
                    if (membershipUser.IsLockedOut)
                    {
                        FailureText.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFranchiseAccountLockedOut").ToString();
                    }
                    else
                    {
                        //Gets current failed password atttemt count for setting the message.
                        AccountHelper accountHelper = new AccountHelper();
                        int inValidAttemtCount = accountHelper.GetUserFailedPasswordAttemptCount((Guid)membershipUser.ProviderUserKey);
                        if (inValidAttemtCount > 0)
                        {
                            //Gets Maximum failed password atttemt count from web.config
                            int maxInvalidPasswordAttemptCount = _membershipProvider.MaxInvalidPasswordAttempts;

                            //Set warning error at (MaxFailureAttemptCount - 2) & (MaxFailureAttemptCount - 1) attempt.
                            FailureText.Text = ((maxInvalidPasswordAttemptCount - inValidAttemtCount) == 2)
                                ? this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountLockedOutAfterTwoAttempt").ToString()
                                : ((maxInvalidPasswordAttemptCount - inValidAttemtCount) == 1)
                                    ? this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountLockedOutAfterOneAttempt").ToString()
                                    : this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorLoginFailure").ToString();

                        }
                        else
                        {
                            FailureText.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorLoginFailure").ToString();
                        }
                    }
                }// Franchise Section, Error message for account gets locked. - End
                else
                {
                    FailureText.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorLoginFailure").ToString();
                }
            }
        }
    }
}