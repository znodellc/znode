<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.Default" Title="Manage Suppliers" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">

    <div class="LeftMargin">
        
        <h1><asp:Localize runat="server" ID="OrderManagement" Text='<%$ Resources:ZnodeAdminResource, TitleOrderManagement %>'></asp:Localize></h1>
        <hr />
        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/approve orders.png" /></div>
            <div class="Shortcut"><a id="A1" href="~/FranchiseAdmin/Secure/Orders/OrderManagement/ViewOrders/Default.aspx" runat="server"><asp:Localize runat="server" ID="ViewOrders" Text='<%$ Resources:ZnodeAdminResource, LinkTextViewOrders %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="ViewOrdersText" Text='<%$ Resources:ZnodeAdminResource, TextViewOrders %>'></asp:Localize></p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/create-an-order.png" /></div>
            <div class="Shortcut"><a id="A3" href="~/FranchiseAdmin/Secure/Orders/OrderManagement/CreateOrder/Default.aspx" runat="server"><asp:Localize runat="server" ID="CreateOrder" Text='<%$ Resources:ZnodeAdminResource, LinkTextCreateOrder %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="CreateOrderText" Text='<%$ Resources:ZnodeAdminResource, TextCreateOrder %>'></asp:Localize></p>
            </div>
        </div>
        
        <h1><asp:Localize runat="server" ID="CustomerManagement" Text='<%$ Resources:ZnodeAdminResource, TitleCustomerManagement %>'></asp:Localize></h1>
        <hr />
        
           <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/CustomerAcctManager.png" /></div>
            <div class="Shortcut"><a id="A6" href="~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/Default.aspx" runat="server"><asp:Localize runat="server" ID="Customers" Text='<%$ Resources:ZnodeAdminResource, LinKTextCusotmers %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="CustomersText" Text='<%$ Resources:ZnodeAdminResource, TextCustomers %>'></asp:Localize></p>
            </div>
        </div>


          <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/ServiceRequests.png" /></div>
            <div class="Shortcut"><a id="A4" href="~/FranchiseAdmin/Secure/Orders/CustomerManagement/ServiceRequests/Default.aspx" runat="server"><asp:Localize runat="server" ID="ServiceRequests" Text='<%$ Resources:ZnodeAdminResource, LinkTextServiceRequests %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p>
                    <p><asp:Localize runat="server" ID="ServiceRequestsText" Text='<%$ Resources:ZnodeAdminResource, TextServiceRequests %>'></asp:Localize></p>
                </p>
            </div>
        </div> 

    </div>
</asp:Content>

