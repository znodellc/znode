using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Xsl;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.ViewOrders
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_orders_refund class.
    /// </summary>
    public partial class Refund : System.Web.UI.Page
    {
        #region Private Member Variables
        private int orderId = 0;
        private string _ReceiptTemplate = string.Empty;        
        private string receiptText = string.Empty;
        private Order order;
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the receipt template.
        /// </summary>
        public string ReceiptTemplate
        {
            get { return this._ReceiptTemplate; }
            set { this._ReceiptTemplate = value; }
        }

        /// <summary>
        /// Gets the feedback Url
        /// </summary>
        public string FeedBackUrl
        {
            get
            {
                return Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath + "/CustomerFeedback.aspx";
            }
        }
        #endregion

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.Params["itemid"] != null) || (Request.Params["itemid"].Length != 0))
            {
                this.orderId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
                lblOrderID.Text = this.orderId.ToString();
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }

        /// <summary>
        /// Refund button clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Refund_Click(object sender, EventArgs e)
        {
            // Retrieve order
            ZNode.Libraries.Admin.OrderAdmin orderAdmin = new ZNode.Libraries.Admin.OrderAdmin();
            this.order = orderAdmin.GetOrderByOrderID(this.orderId);

            // Get payment settings
            int paymentSettingId = (int)this.order.PaymentSettingID;
            PaymentSettingService paymentSettingService = new PaymentSettingService();
            PaymentSetting paymentSetting = paymentSettingService.GetByPaymentSettingID(paymentSettingId);

            // Set gateway info
            GatewayInfo gatewayInfo = new GatewayInfo();
            gatewayInfo.GatewayLoginID = this.encryption.DecryptData(paymentSetting.GatewayUsername);
            gatewayInfo.GatewayPassword = this.encryption.DecryptData(paymentSetting.GatewayPassword);
            gatewayInfo.TransactionKey = this.encryption.DecryptData(paymentSetting.TransactionKey);
            gatewayInfo.Vendor = paymentSetting.Vendor;
            gatewayInfo.Partner = paymentSetting.Partner;
            gatewayInfo.TestMode = paymentSetting.TestMode;
            gatewayInfo.Gateway = (GatewayType)paymentSetting.GatewayTypeID;

            string creditCardExp = Convert.ToString(this.order.CardExp);
            if (creditCardExp == null)
            {
                creditCardExp = string.Empty;
            }

            // Set credit card
            CreditCard creditCard = new CreditCard();
            creditCard.Amount = Decimal.Parse(txtAmount.Text);
            creditCard.CardNumber = txtCardNumber.Text.Trim();
            creditCard.CreditCardExp = creditCardExp;
            creditCard.OrderID = this.order.OrderID;
            creditCard.TransactionID = this.order.CardTransactionID;

            GatewayResponse response = new GatewayResponse();

            if ((GatewayType)paymentSetting.GatewayTypeID == GatewayType.AUTHORIZE)
            {
                GatewayAuthorize authorize = new GatewayAuthorize();
                response = authorize.RefundPayment(gatewayInfo, creditCard);
            }
            else if ((GatewayType)paymentSetting.GatewayTypeID == GatewayType.VERISIGN)
            {
                GatewayPayFlowPro payFlowPro = new GatewayPayFlowPro();
                response = payFlowPro.RefundPayment(gatewayInfo, creditCard);
            }
            else if ((GatewayType)paymentSetting.GatewayTypeID == GatewayType.PAYMENTECH)
            {
                GatewayOrbital orbital = new GatewayOrbital();
                response = orbital.ReversePayment(gatewayInfo, creditCard);
            }
            else if ((GatewayType)paymentSetting.GatewayTypeID == GatewayType.CYBERSOURCE)
            {
                GatewayCyberSource cybersource = new GatewayCyberSource();
                creditCard.TransactionID = lblTransactionID.Text;
                creditCard.CardDataToken = this.order.CardAuthCode;
                creditCard.Amount = Decimal.Parse(txtAmount.Text.Trim());
                gatewayInfo.TransactionType = Enum.GetName(typeof(ZNode.Libraries.ECommerce.Entities.ZNodePaymentStatus), this.order.PaymentStatusID.Value); 
                response = cybersource.RefundPayment(gatewayInfo, creditCard);
            }
            else
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorRefundNoGatewaySupport").ToString();
                return;
            }

            if (response.IsSuccess)
            {
                // Update order status
                // Returned status
                this.order.OrderStateID = 30;

                // Refund status
                this.order.PaymentStatusID = 3;

                OrderService orderService = new OrderService();
                orderService.Update(this.order);

                this.ReceiptTemplate = this.GetGlobalResourceObject("ZnodeAdminResource", "TextSuccessTransactionRefund").ToString();
                pnlEdit.Visible = false;
                pnlConfirm.Visible = true;
            }
            else
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCouldNotCompleteRequest").ToString());
                sb.Append(response.ResponseText);
                lblError.Text = sb.ToString();
            }
        }

        /// <summary>
        /// Void button clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Void_Click(object sender, EventArgs e)
        {
            // Retrieve order
            ZNode.Libraries.Admin.OrderAdmin orderAdmin = new ZNode.Libraries.Admin.OrderAdmin();
            this.order = orderAdmin.GetOrderByOrderID(this.orderId);

            // Get payment settings
            int paymentSettingId = (int)this.order.PaymentSettingID;
            PaymentSettingService paymentSettingService = new PaymentSettingService();
            PaymentSetting paymentSetting = paymentSettingService.GetByPaymentSettingID(paymentSettingId);

            // Set gateway info
            GatewayInfo gatewayInfo = new GatewayInfo();
            gatewayInfo.GatewayLoginID = this.encryption.DecryptData(paymentSetting.GatewayUsername);
            gatewayInfo.GatewayPassword = this.encryption.DecryptData(paymentSetting.GatewayPassword);
            gatewayInfo.TransactionKey = this.encryption.DecryptData(paymentSetting.TransactionKey);
            gatewayInfo.Vendor = paymentSetting.Vendor;
            gatewayInfo.Partner = paymentSetting.Partner;
            gatewayInfo.TestMode = paymentSetting.TestMode;
            gatewayInfo.Gateway = (GatewayType)paymentSetting.GatewayTypeID;

            string creditCardExp = Convert.ToString(this.order.CardExp);
            if (creditCardExp == null)
            {
                creditCardExp = string.Empty;
            }

            // Set credit card
            CreditCard creditCard = new CreditCard();
            creditCard.Amount = Decimal.Parse(txtAmount.Text);
            creditCard.CardNumber = txtCardNumber.Text.Trim();
            creditCard.CreditCardExp = creditCardExp;
            creditCard.OrderID = this.order.OrderID;
            creditCard.TransactionID = this.order.CardTransactionID;
            creditCard.ProcTxnId = this.order.CardTransactionID;
            GatewayResponse response = new GatewayResponse();

            if ((GatewayType)paymentSetting.GatewayTypeID == GatewayType.AUTHORIZE)
            {
                GatewayAuthorize authorize = new GatewayAuthorize();
                response = authorize.VoidPayment(gatewayInfo, creditCard);
            }
            else if ((GatewayType)paymentSetting.GatewayTypeID == GatewayType.VERISIGN)
            {
                GatewayPayFlowPro payFlowPro = new GatewayPayFlowPro();
                response = payFlowPro.VoidPayment(gatewayInfo, creditCard);
            }
            else if ((GatewayType)paymentSetting.GatewayTypeID == GatewayType.PAYMENTECH)
            {
                GatewayOrbital orbital = new GatewayOrbital();
                response = orbital.ReversePayment(gatewayInfo, creditCard);
            }
            else
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorVoidNoGatewaySupport").ToString();
                return;
            }

            if (response.IsSuccess)
            {
                // Update order status
                // Cancelled status
                this.order.OrderStateID = 40;

                // Refund status
                this.order.PaymentStatusID = 4;

                OrderService orderService = new OrderService();
                orderService.Update(this.order);

                pnlEdit.Visible = false;
                pnlConfirm.Visible = true;

                pnlEdit.Visible = false;
                pnlConfirm.Visible = true;
            }
            else
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCouldNotCompleteRequest").ToString());
                sb.Append(response.ResponseText);
                lblError.Text = sb.ToString();
            }
        }

        /// <summary>
        /// Cancel button clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("View.aspx?itemid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.orderId.ToString())) + "&Pg=" + HttpUtility.UrlEncode(this.encryption.EncryptData("List")));
        }

        #region Bind Methods
        /// <summary>
        /// Bind fields
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.Admin.OrderAdmin orderAdmin = new ZNode.Libraries.Admin.OrderAdmin();
            this.order = orderAdmin.DeepLoadByOrderID(this.orderId);

            lblCustomerName.Text = this.order.BillingFirstName + " " + this.order.BillingLastName;
            lblTotal.Text = this.order.Total.Value.ToString("c");
            lblTransactionID.Text = this.order.CardTransactionID;

            txtAmount.Text = ((decimal)this.order.Total.Value).ToString("N");
           
            if (order.PaymentSettingIDSource != null && order.PaymentSettingIDSource.GatewayTypeID != null)
            {  
                txtCardNumber.Width = 170;
                lblCardNumber.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnTitleEnterCreditCard").ToString();
                this.BindYearList();

                string currentMonth = System.DateTime.Now.Month.ToString();
                if (currentMonth.Length == 1)
                {
                    currentMonth = "0" + currentMonth;
                }

                // Pre-select item in the list
                lstMonth.SelectedValue = currentMonth;
                pnlCreditCardInfo.Visible = true;
            }

            btnRefund.Visible = this.EnableRefund(this.order.PaymentSettingIDSource.GatewayTypeID.GetValueOrDefault());
            Void.Visible = this.EnableVoid(this.order.PaymentSettingIDSource.GatewayTypeID.GetValueOrDefault());
        }

        /// <summary>
        /// Binds the expiration year list based on current year
        /// </summary>
        private void BindYearList()
        {
            int currentYear = System.DateTime.Now.Year;
            int counter = 25;

            do
            {
                string itemText = currentYear.ToString();
                lstYear.Items.Add(new ListItem(itemText));
                currentYear = currentYear + 1;
                counter = counter - 1;
            } 
            while (counter > 0);
        }
        #endregion

        #region Helper Functions

        /// <summary>
        /// Get the enable refund buton status.
        /// </summary>
        /// <param name="gatewayTypeId">Gateway type Id</param>
        /// <returns>Returns the refund button status</returns>
        private bool EnableRefund(int gatewayTypeId)
        {
            GatewayHelper helper = new GatewayHelper();
            return helper.GetCapability((GatewayType)gatewayTypeId, GatewayTransactionType.REFUND);
        }

        /// <summary>
        /// Get the Void button status
        /// </summary>
        /// <param name="gatewayTypeId">Gateway type Id</param>
        /// <returns>Returns the void button status.</returns>
        private bool EnableVoid(int gatewayTypeId)
        {
            GatewayHelper helper = new GatewayHelper();
            return helper.GetCapability((GatewayType)gatewayTypeId, GatewayTransactionType.VOID);
        }

        #endregion
    }
}