using System;
using System.Web;
using System.Web.UI;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.ViewOrders
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_orders_capture class.
    /// </summary>
    public partial class Capture : System.Web.UI.Page
    {
        #region Private Member Variables
        private int orderId = 0;
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.Params["itemid"] != null) || (Request.Params["itemid"].Length != 0))
            {
                this.orderId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
                lblOrderID.Text = this.orderId.ToString();
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }

        /// <summary>
        /// Refund button clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Capture_Click(object sender, EventArgs e)
        {
            // Retrieve order
            ZNode.Libraries.Admin.OrderAdmin orderAdmin = new ZNode.Libraries.Admin.OrderAdmin();
            Order order = orderAdmin.GetOrderByOrderID(this.orderId);
            
            // Get payment settings
            int paymentSettingId = (int)order.PaymentSettingID;
            PaymentSettingService paymentSettingService = new PaymentSettingService();
            PaymentSetting paymentSetting = paymentSettingService.GetByPaymentSettingID(paymentSettingId);

            // Set gateway info
            GatewayInfo gatewayInfo = new GatewayInfo();
            gatewayInfo.GatewayLoginID = this.encryption.DecryptData(paymentSetting.GatewayUsername);
            gatewayInfo.GatewayPassword = this.encryption.DecryptData(paymentSetting.GatewayPassword);
            gatewayInfo.TransactionKey = this.encryption.DecryptData(paymentSetting.TransactionKey);
            gatewayInfo.Vendor = paymentSetting.Vendor;
            gatewayInfo.Partner = paymentSetting.Partner;
            gatewayInfo.TestMode = paymentSetting.TestMode;
            gatewayInfo.Gateway = (GatewayType)paymentSetting.GatewayTypeID;

            string creditCardExp = Convert.ToString(order.CardExp);
            if (creditCardExp == null)
            {
                creditCardExp = string.Empty;
            }

            // Set credit card
            CreditCard creditCard = new CreditCard();
            creditCard.CreditCardExp = creditCardExp;
            creditCard.OrderID = order.OrderID;
            creditCard.TransactionID = order.CardTransactionID;

            GatewayResponse response = new GatewayResponse();

            if ((GatewayType)paymentSetting.GatewayTypeID == GatewayType.AUTHORIZE)
            {
                GatewayAuthorize authorize = new GatewayAuthorize();
                response = authorize.CapturePayment(gatewayInfo, creditCard);
            }
            else if ((GatewayType)paymentSetting.GatewayTypeID == GatewayType.VERISIGN)
            {
                GatewayPayFlowPro payFlowPro = new GatewayPayFlowPro();
                response = payFlowPro.CapturePayment(gatewayInfo, creditCard);
            }
            else if ((GatewayType)paymentSetting.GatewayTypeID == GatewayType.PAYMENTECH)
            {
                GatewayOrbital orbital = new GatewayOrbital();
                response = orbital.CapturePayment(gatewayInfo, creditCard);
            }
            else if ((GatewayType)paymentSetting.GatewayTypeID == GatewayType.CYBERSOURCE)
            {
                GatewayCyberSource cyberSource = new GatewayCyberSource();
                creditCard.TransactionID = lblTransactionID.Text;
                creditCard.CardDataToken = order.CardAuthCode;
                creditCard.Amount = Decimal.Parse(order.Total.GetValueOrDefault(0).ToString());
                response = cyberSource.Capture(gatewayInfo, creditCard);
            }
            else
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoCreditcardCapture").ToString();
            }

            if (response.IsSuccess)
            {
                // Update order status (Refund)
                order.PaymentStatusID = 1; 

                OrderService orderService = new OrderService();
                orderService.Update(order);

                pnlEdit.Visible = false;
                pnlConfirm.Visible = true;
            }
            else
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCouldNotCompleteRequest").ToString());
                sb.Append(response.ResponseText);
                lblError.Text = sb.ToString();
            }
        }

        /// <summary>
        /// Cancel button clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("View.aspx?itemid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(this.orderId.ToString())) + "&Pg=" + HttpUtility.UrlEncode(this.encryption.EncryptData("List")));
        }

        /// <summary>
        /// Bind fields
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.Admin.OrderAdmin orderAdmin = new ZNode.Libraries.Admin.OrderAdmin();
            ZNode.Libraries.DataAccess.Entities.Order order = orderAdmin.GetOrderByOrderID(this.orderId);

            lblCustomerName.Text = order.BillingFirstName + " " + order.BillingLastName;
            lblTotal.Text = order.Total.Value.ToString("c");
            lblTransactionID.Text = order.CardTransactionID;
        }
    }
}