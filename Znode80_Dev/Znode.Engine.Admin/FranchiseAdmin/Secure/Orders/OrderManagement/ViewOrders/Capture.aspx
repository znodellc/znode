<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.ViewOrders.Capture" Title="Untitled Page" CodeBehind="Capture.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <h1>
            <asp:Localize ID="CreditCardPayment" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleCaptureCreditCardPayment %>'></asp:Localize></h1>
        <asp:Panel ID="pnlEdit" runat="server" Visible="true">
            <p>
                <asp:Localize ID="TextCapturePayment" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCapturePayment %>'></asp:Localize>
            </p>
            <p>
                <asp:Localize ID="TextCaptureGateway" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCaptureGateway %>'></asp:Localize>
            </p>
            <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
            <div style="margin-bottom: 20px; margin-top: 20px;">
                <div>
                    <b>
                        <asp:Localize ID="OrderID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOrderID %>'></asp:Localize>
                    </b>
                    <asp:Label ID="lblOrderID" runat="server" />
                </div>
                <div>
                    <b>
                        <asp:Localize ID="TransactionID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTransactionID %>'></asp:Localize></b>
                    <asp:Label ID="lblTransactionID" runat="server" />
                </div>
                <div>
                    <b>
                        <asp:Localize ID="CustomerName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCustomerName %>'></asp:Localize></b>
                    <asp:Label ID="lblCustomerName" runat="server" />
                </div>
                <div>
                    <b>
                        <asp:Localize ID="OrderTotal" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOrderTotal %>'></asp:Localize>
                    </b>
                    <asp:Label ID="lblTotal" runat="server" />
                </div>
            </div>
            <div>
                <zn:Button runat="server" ID="btnCapture" ButtonType="SubmitButton" OnClick="Capture_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCapture %>' />
                <zn:Button ID="btnCancelBottom" runat="server" ButtonType="CancelButton" OnClick="Cancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlConfirm" runat="server" Visible="false">
            <div>
                <asp:Localize ID="TextSuccessTransaction" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextSuccessTransaction %>'></asp:Localize>
            </div>
            <div style="margin-top: 20px;">
                <a id="A1" href="~/FranchiseAdmin/Secure/Orders/OrderManagement/ViewOrders/Default.aspx" runat="server">
                    <asp:Localize ID="Back" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkBackToOrder %>'></asp:Localize></a>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
