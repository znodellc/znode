using System;
using System.Collections;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.ViewOrders
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_orders_view class.
    /// </summary>
    public partial class View : System.Web.UI.Page
    {
        #region Private variables
        private int orderId = 0;
        private bool advancedShipping = false;
        private string changeStatusPage = "OrderStatus.aspx?itemid=";
        private string refundPage = "Refund.aspx?itemid=";
        private string listPage = "Default.aspx";
        private string orderTrackingNumber = string.Empty;
        private string filepath = ZNodeConfigManager.EnvironmentConfig.DataPath + "/ShippingLabels//FedEx//";
        private ZNodeEncryption encryption = new ZNodeEncryption();
        private string statusText = string.Empty;
        #endregion

        #region Page Load
        protected void Page_PreInit(object sender, EventArgs e)
        {
            // Change the master file, if the user is Order Approver.
            if (HttpContext.Current.User.IsInRole("ORDER APPROVER"))
            {
                if (this.encryption.DecryptData(Request.Params["Pg"].ToString()).ToString() == "List")
                {
                    this.listPage = "Default.aspx";
                }
                else
                {
                    this.MasterPageFile = "~/FranchiseAdmin/Themes/Standard/order.master";
                    this.listPage = "~/FranchiseAdmin/Secure/Orders/OrderManagement/ApproveOrders/Default.aspx";
                }
            }
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.Params["itemid"] != null) || (Request.Params["itemid"].Length != 0))
            {
                this.orderId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
                lblOrderHeader.Text = this.orderId.ToString();
            }

            // Change the ChangeStatus button status, if the user is Order Approver.
            ChangeStatus.Enabled = true;
            if (HttpContext.Current.User.IsInRole("ORDER APPROVER") || HttpContext.Current.User.IsInRole("FRANCHISE"))
            {
                if (this.encryption.DecryptData(Request.Params["Pg"].ToString()).ToString() != "List")
                {
                    ChangeStatus.Enabled = false;
                }
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
                this.BindGrid();
            }

            // Build the javascript block
            StringBuilder script = new StringBuilder();
            script.Append("<script language=JavaScript>");
            script.Append("    function Back() {");
            script.Append("        javascript:history.go(-1);");
            script.Append("    }");
            script.Append("<" + "/script>");

            if (!ClientScript.IsStartupScriptRegistered("GoBack"))
            {
                ClientScript.RegisterStartupScript(GetType(), "GoBack", script.ToString());
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Change Status Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChangeStatus_Click(object sender, EventArgs e)
        {
            this.changeStatusPage = this.changeStatusPage + HttpUtility.UrlEncode(this.encryption.EncryptData(this.orderId.ToString()));
            Response.Redirect(this.changeStatusPage);
        }

        protected void Refund_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.refundPage + HttpUtility.UrlEncode(this.encryption.EncryptData(this.orderId.ToString())));
        }

        protected void List_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listPage);
        }

        /// <summary>
        /// Label for Order shipment event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Label_Click(object sender, EventArgs e)
        {
            this.orderTrackingNumber = lblTrackingNumber.Text;
            string fileName = this.orderTrackingNumber + ".pdf";
            if (this.orderTrackingNumber == string.Empty)
            {
                // No tracking number
            }
            else
            {
                this.DownloadLabel(fileName);
            }
        }
        #endregion

        #region Grid Events

        /// <summary>
        /// Order Items Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        /// <summary>
        /// Grid Button Command for Estimating Package Size, Creating Shipments, Cancelling Shipments, and Printing Labels
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ShippingErrors.Text = string.Empty;
            ZNode.Libraries.Admin.ShippingAdmin shippingAdmin = new ShippingAdmin();
            OrderAdmin orderAdmin = new OrderAdmin();
            TList<OrderLineItem> orderLineItemList = orderAdmin.GetOrderLineItemByOrderID(this.orderId);
            TList<OrderLineItem> singleShipItemList = new TList<OrderLineItem>();

            foreach (OrderLineItem orderLineItem in orderLineItemList)
            {
                if (orderLineItem.OrderLineItemID.ToString() == e.CommandArgument.ToString())
                {
                    singleShipItemList.Add(orderLineItem);
                }
            }

            if (shippingAdmin.ErrorCode != "0")
            {
                ShippingErrors.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCodeMessage").ToString(), shippingAdmin.ErrorCode, shippingAdmin.ErrorDescription);
            }
        }

        /// <summary>
        /// Sets grid button Text
        /// </summary>
        /// <param name="trackingNumber">Orde tracking number.</param>
        /// <param name="orderLineItemId">Order line item Id</param>
        /// <returns>Returns the button text</returns>
        protected string SetButtonText(object trackingNumber, object orderLineItemId)
        {
            if (orderLineItemId != null)
            {
                // If the value for this row matches the just estimated line item we set the command to create
                if (orderLineItemId.ToString() == EstimatedLineItemID.Value)
                {
                    return "Create Shipment";
                }

                if (trackingNumber != null)
                {
                    if (trackingNumber.ToString().Length > 0)
                    {
                        return "Cancel Shipment";
                    }
                }

                return "Estimate Dimensions";
            }

            return string.Empty;
        }

        /// <summary>
        /// Sets the Command for the Ship seperately grid
        /// </summary>
        /// <param name="trackingNumber">Orde tracking number.</param>
        /// <param name="orderLineItemId">Order line item Id</param>
        /// <returns>Returns the button text</returns>
        protected string ShippingCommand(object trackingNumber, object orderLineItemId)
        {
            if (orderLineItemId != null)
            {
                // If the value for this row matches the just estimated line item we set the command to create
                if (orderLineItemId.ToString() == EstimatedLineItemID.Value)
                {
                    return "CreateShipment";
                }

                if (trackingNumber != null)
                {
                    if (trackingNumber.ToString().Length > 0)
                    {
                        return "CancelShipment";
                    }
                }

                return "EstimateDimensions";
            }

            return string.Empty;
        }

        /// <summary>
        /// Checks to see if a label exists before displaying the label button
        /// </summary>
        /// <param name="trackingNumber">Order tracking number</param>
        /// <returns>Returns true if label exists else false.</returns>
        protected bool ShowLabelButton(object trackingNumber)
        {
            if (trackingNumber != null)
            {
                if (trackingNumber.ToString().Length > 0)
                {
                    if (File.Exists(Server.MapPath(this.filepath) + trackingNumber.ToString() + ".pdf") && this.advancedShipping)
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        #endregion

        #region Helper Funtions
        /// <summary>
        /// Format the price.
        /// </summary>
        /// <param name="price">Price value to format</param>
        /// <returns>Returns the formatted price value.</returns>
        private string Formatprice(object price)
        {
            if (price != null)
            {
                return string.Format("{0:c}", price);
            }

            return string.Empty;
        }

        /// <summary>
        /// Returns shipping option name for this shipping Id
        /// </summary>
        /// <param name="shippingId">The value of shippingId</param>
        /// <returns>Returns the Shipping option Name</returns>
        public string GetShippingOptionName(int shippingId)
        {
            string Name = string.Empty;
            ZNode.Libraries.Admin.ShippingAdmin shippingAdmin = new ZNode.Libraries.Admin.ShippingAdmin();
            ZNode.Libraries.DataAccess.Entities.Shipping entity = shippingAdmin.GetShippingOptionById(shippingId);
            if (entity != null)
            {
                Name = entity.Description;
            }
            return Name;
        }

        /// <summary>
        /// Returns payment type name for this payment type id
        /// </summary>
        /// <param name="PaymentTypeId">Payment type Id</param>
        /// <returns>Returns the payment type name</returns>
        private string GetPaymentTypeName(int PaymentTypeId)
        {
            string paymentTypeName = string.Empty;

            ZNode.Libraries.Admin.StoreSettingsAdmin settingsAdmin = new ZNode.Libraries.Admin.StoreSettingsAdmin();
            PaymentType paymentType = settingsAdmin.GetPaymentTypeById(PaymentTypeId);

            if (paymentType != null)
            {
                paymentTypeName = paymentType.Name;
            }

            return paymentTypeName;
        }

        /// <summary>
        /// Writes a label PDF to the browser.
        /// </summary>
        /// <param name="fileName">Downloaded fielname</param>
        private void DownloadLabel(string fileName)
        {
            if (File.Exists(Server.MapPath(this.filepath) + fileName))
            {
                FileStream stream = new FileStream(Server.MapPath(this.filepath) + fileName, FileMode.Open);
                long fileSize = stream.Length;
                byte[] Buffer = new byte[(int)fileSize];
                stream.Read(Buffer, 0, (int)stream.Length);
                stream.Close();
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment; fileName=" + fileName);
                Response.BinaryWrite(Buffer);
                Response.End();
            }
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind the order data.
        /// </summary>
        private void BindData()
        {
            OrderAdmin orderAdmin = new OrderAdmin();
            Order order = orderAdmin.DeepLoadByOrderID(this.orderId);

            TList<OrderLineItem> orderLineItem = new TList<OrderLineItem>();
            var orderShipmentService = new OrderShipmentService();
            var orderShipment = new OrderShipment();
            var orderLineItemService = new OrderLineItemService();

            ArrayList arrayList = new ArrayList();
            StringBuilder builder = new StringBuilder();

            orderLineItem = orderLineItemService.GetByOrderID(this.orderId);
            foreach (var orderItem in orderLineItem)
            {
                if (orderItem.OrderShipmentID.HasValue)
                {
                    orderShipment = orderShipmentService.GetByOrderShipmentID(int.Parse(orderItem.OrderShipmentID.ToString()));
	                if (orderShipment.ShippingID.HasValue)
	                {
		                if (!arrayList.Contains(orderShipment.ShippingID))
		                {
			                arrayList.Add(orderShipment.ShippingID);
		                }
	                }
                }
            }


            if (order != null)
            {
                if (!UserStoreAccess.CheckStoreAccess(order.PortalId.GetValueOrDefault(0)))
                {
                    Response.Redirect("Default.aspx", true);
                }

                this.advancedShipping = false;

                // FedEx shipping is the only advanced shipping currently supported.
                if (order.ShippingID.HasValue && order.ShippingIDSource.ShippingTypeID == 3)
                {
                    if (order.ShippingIDSource.ShippingCode.ToLower().Contains("international"))
                    {
                        this.advancedShipping = false;
                    }
                }

                StringBuilder address = new StringBuilder();
                address.Append(order.BillingFirstName + " ");
                address.Append(order.BillingLastName + "<br>");
                address.Append(order.BillingCompanyName + "<br>");
                address.Append(order.BillingStreet + " ");
                address.Append(order.BillingStreet1 + "<br>");
                address.Append(order.BillingCity + ", ");
                address.Append(order.BillingStateCode + " ");
                address.Append(order.BillingPostalCode + "<br>");
                address.Append(order.BillingCountry + "<br>");
                address.Append("Tel: " + order.BillingPhoneNumber + "<br>");
                address.Append("Email: " + order.BillingEmailId);

                lblBillingAddress.Text = address.ToString();

                // Clear the string builde data.
                address.Remove(0, address.Length);

				// Commenting this code as we need to rewrite the code for #16485
				//address.Append(order.ShipFirstName.ToString() + " ");
				//address.Append(order.ShipLastName.ToString() + "<br>");
				//address.Append(order.ShipCompanyName.ToString() + "<br>");
				//address.Append(order.ShipStreet.ToString() + " ");
				//address.Append(order.ShipStreet1.ToString() + "<br>");
				//address.Append(order.ShipCity.ToString() + ", ");
				//address.Append(order.ShipStateCode.ToString() + " ");
				//address.Append(order.ShipPostalCode.ToString() + "<br>");
				//address.Append(order.ShipCountry.ToString() + "<br>");
				//address.Append("Tel: " + order.ShipPhoneNumber.ToString() + "<br>");
				//address.Append("Email: " + order.ShipEmailID.ToString());

                lblShippingAddress.Text = address.ToString();

                lblOrderDate.Text = order.OrderDate.Value.ToString("MM/dd/yyyy hh:mm tt");
                if (order.OrderStateIDSource != null)
                {
                    lblOrderStatus.Text = order.OrderStateIDSource.Description;
                    if (lblOrderStatus.Text == "Pending Approval")
                    {
                        lblOrderStatus.ForeColor = System.Drawing.Color.Red;
                    }
                    statusText = lblOrderStatus.Text;
                    lblOrderStatus.Text = statusText.ToUpper();
                    statusText = string.Empty;
                }

                lblShipAmount.Text = this.Formatprice(order.ShippingCost);
                lblOrderAmount.Text = this.Formatprice(order.Total);
                lblTaxAmount.Text = this.Formatprice(order.TaxCost);
                if (order.DiscountAmount.HasValue)
                {
                    lblDiscountAmt.Text = order.DiscountAmount.Value.ToString("c");
                }

                lblCouponCode.Text = order.CouponCode;

                if (order.PaymentTypeId.HasValue)
                {
                    string paymentTypeName = this.GetPaymentTypeName(order.PaymentTypeId.Value);

                    // If purchase order payment
                    if (order.PaymentTypeId.Value == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.PURCHASE_ORDER)
                    {
                        lblPurchaseOrder.Text = order.PurchaseOrderNumber;
                    }

                    lblPaymentType.Text = paymentTypeName;
                }

                lblTransactionId.Text = order.CardTransactionID;
                if (orderLineItem != null)
                {
                    foreach (int item in arrayList)
                    {
                        if (!string.IsNullOrEmpty(builder.ToString()))
                        {
                            builder.Append(", " + this.GetShippingOptionName(item));
                        }
                        else
                        {
                            builder.Append(this.GetShippingOptionName(item));
                        }
                        lblShippingMethod.Text = builder.ToString();
                    }
                }

                lblTrackingNumber.Text = order.TrackingNumber;
                if (order.PaymentStatusIDSource != null)
                {
                 
					lblPaymentStatus.Text = order.PaymentStatusIDSource.Description;

					if (order.PaymentStatusIDSource.PaymentStatusName == "CC_PENDING" || order.PaymentStatusIDSource.PaymentStatusName == "PO_PENDING" || order.PaymentStatusIDSource.PaymentStatusName == "COD_PENDING")
	                {
						lblPaymentStatus.ForeColor = System.Drawing.Color.Red;
	                }

					statusText = lblPaymentStatus.Text;
                    lblPaymentStatus.Text = statusText.ToUpper();
                    statusText = string.Empty;
                }

                // Bind custom additional instructions to label field
                lblAdditionalInstructions.Text = order.AdditionalInstructions;

                if (order.PaymentTypeId != (int)ZNode.Libraries.ECommerce.Entities.PaymentType.CREDIT_CARD)
                {
                    Refund.Visible = false;
                }

                // Display gift card amount if any.
                GiftCardAdmin giftCardAdmin = new GiftCardAdmin();
                TList<GiftCardHistory> giftCardHistoryList = giftCardAdmin.GetGiftCardHistoryByAccountID(Convert.ToInt32(order.AccountID), order.OrderID);
                decimal giftCardAmount = 0;
                if (giftCardHistoryList.Count > 0)
                {
                    giftCardAmount = giftCardHistoryList[0].TransactionAmount;
                }

                lblGiftCardAmount.Text = this.Formatprice(giftCardAmount);

                this.orderTrackingNumber = order.TrackingNumber;
                if (this.orderTrackingNumber == null)
                {
                    this.orderTrackingNumber = string.Empty;
                }

                if (!this.advancedShipping)
                {
                    ShippingErrorPanel.Visible = false;
                }
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoOrderRequest").ToString());
            }
        }

		/// <summary>
		/// Check Null Method
		/// </summary>
		/// <param name="value">The value of sValue</param>
		/// <returns>Returns the string </returns>
		private string CheckNull(string value)
		{
			if (!string.IsNullOrEmpty(value))
			{
				return value;
			}
			else
			{
				return string.Empty;
			}
		}
        /// <summary>
        /// Bind grid with Order line items
        /// </summary>
        private void BindGrid()
        {
          
			ZNode.Libraries.Admin.OrderAdmin _OrderLineItemAdmin = new ZNode.Libraries.Admin.OrderAdmin();
			ShippingAdmin _ShippingAdmin = new ShippingAdmin();
			TList<OrderLineItem> fullorderlist = _OrderLineItemAdmin.GetOrderLineItemByOrderID(this.orderId);
			TList<OrderLineItem> shiptogetherlist = new TList<OrderLineItem>();
			TList<OrderLineItem> shipseperatelist = new TList<OrderLineItem>();
			shiptogetherlist = fullorderlist.FindAll(delegate(OrderLineItem item) { return item.ShipSeparately != true; });
			shipseperatelist = fullorderlist.FindAll(delegate(OrderLineItem item) { return item.ShipSeparately == true; });
			if (shiptogetherlist.Any())
			{
				bool isMultipleAddress = shiptogetherlist.Select(x => x.OrderShipmentID).Distinct().Count() > 1;
				StringBuilder Build = new StringBuilder();
				if (shiptogetherlist.Count > 1 && isMultipleAddress)
				{
                    lblShippingAddress.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextShipMultipleAddress").ToString();
				}
				else
				{
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToFirstName) + " ");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToLastName) + "<br>");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToCompanyName) + "<br>");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToStreet) + " ");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToStreet1) + "<br>");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToCity) + ", ");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToStateCode) + " ");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToPostalCode) + "<br>");
					Build.Append(this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToCountry) + "<br>");
					Build.Append("Tel: " + this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToPhoneNumber) + "<br>");
					Build.Append("Email: " + this.CheckNull(shiptogetherlist[0].OrderShipmentIDSource.ShipToEmailID));
					lblShippingAddress.Text = Build.ToString();
				}
			}
			uxGrid2.DataSource = shipseperatelist;
			uxGrid.DataSource = shiptogetherlist;
			uxGrid.DataBind();
			uxGrid2.DataBind();
        }
        #endregion
    }
}