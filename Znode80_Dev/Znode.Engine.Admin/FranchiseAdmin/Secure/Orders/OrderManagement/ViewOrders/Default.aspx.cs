using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.ViewOrders
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_orders_list class.
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Member Variables
        private static bool isSearchEnabled = false;
        private string viewOrderLink = "~/FranchiseAdmin/Secure/Orders/OrderManagement/ViewOrders/View.aspx?itemid=";
        private string changeStatusLink = "~/FranchiseAdmin/Secure/Orders/OrderManagement/ViewOrders/OrderStatus.aspx?itemid=";
        private string refundOrderLink = "~/FranchiseAdmin/Secure/Orders/OrderManagement/ViewOrders/Refund.aspx?itemid=";
        private string captureLink = "~/FranchiseAdmin/Secure/Orders/OrderManagement/ViewOrders/Capture.aspx?itemid=";
        private string portalIds = string.Empty;
        private DataSet ordersDataSet;
        #endregion

        #region Public Properties

        // Gets or sets the SortField property is tracked in ViewState
        public string SortField
        {
            get
            {
                object sortField = ViewState["SortField"];
                if (sortField == null)
                {
                    return string.Empty;
                }

                return (string)sortField;
            }

            set
            {
                if (value == this.SortField)
                {
                    // Same as current sort file, toggle sort direction
                    this.SortAscending = !this.SortAscending;
                }

                ViewState["SortField"] = value;
            }
        }

        // Gets or sets a value indicating whether to ascending or not.
        public bool SortAscending
        {
            get
            {
                object sortAscending = ViewState["SortAscending"];
                if (sortAscending == null)
                {
                    return true;
                }

                return (bool)sortAscending;
            }

            set
            {
                ViewState["SortAscending"] = value;
            }
        }
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get Portals
            this.portalIds = UserStoreAccess.GetAvailablePortals;

            if (!Page.IsPostBack)
            {
                isSearchEnabled = false;
                this.BindPortal();
                this.BindOrderState();
                this.BindSearchData();
            }
        }

        #endregion

        #region General Events

        /// <summary>
        /// Search Button Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            isSearchEnabled = false;
            txtStartDate.Text = string.Empty;
            txtEndDate.Text = string.Empty;
            txtorderid.Text = string.Empty;
            txtfirstname.Text = string.Empty;
            txtlastname.Text = string.Empty;
            txtcompanyname.Text = string.Empty;
            txtaccountnumber.Text = string.Empty;
            ddlPortal.SelectedIndex = 0;

            this.BindOrderState();
            this.BindSearchData();
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Method to sort the grid in Ascending and Descending Order
        /// </summary>   
        protected void SortGrid()
        {
            string firstName = null;
            string lastName = null;
            string companyName = null;
            string accountNumber = null;
            DateTime? startDate = null;
            DateTime? endDate = null;
            int? orderStateId = null;
            int? portalId = null;
            int? orderId = null;

            if (txtStartDate.Text.Length > 0)
            {
                startDate = DateTime.Parse(txtStartDate.Text.Trim());
            }

            if (txtEndDate.Text.Length > 0)
            {
                endDate = DateTime.Parse(txtEndDate.Text.Trim());
            }

            if (this.portalIds.Length > 0)
            {
                OrderAdmin orderAdmin = new OrderAdmin();
                DataSet ds;

                if (Roles.IsUserInRole("ADMIN"))
                {
                    ds = orderAdmin.FindOrders(orderId, firstName, lastName, companyName, accountNumber, startDate, endDate, orderStateId, portalId, this.portalIds);
                }
                else
                {
                    ds = orderAdmin.FindOrders(orderId, firstName, lastName, companyName, accountNumber, startDate, endDate, orderStateId, portalId, this.portalIds);
                }

                uxGrid.DataSource = ds;

                DataSet dataSet = uxGrid.DataSource as DataSet;
                DataView dataView = new DataView(dataSet.Tables[0]);

                // Apply sort filter and direction
                dataView.Sort = this.SortField;

                // If sortDirection is not Ascending
                if (!this.SortAscending)
                {
                    dataView.Sort += " DESC";
                }

                uxGrid.DataSource = null;
                uxGrid.DataSource = dataView;
                uxGrid.DataBind();
            }
            else
            {
                lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoPermission").ToString();
            }
        }

        /// <summary>
        /// Grid Sorting Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            uxGrid.PageIndex = 0;
            this.SortField = e.SortExpression;
            this.SortGrid();
        }

        /// <summary>
        /// Grid Paging Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        /// <summary>
        ///  Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ZNodeEncryption encryption = new ZNodeEncryption();
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "ViewOrder" || e.CommandName == "ViewID")
                {
                    this.viewOrderLink = this.viewOrderLink + HttpUtility.UrlEncode(encryption.EncryptData(Id)) + "&Pg=" + HttpUtility.UrlEncode(encryption.EncryptData("List"));
                    Response.Redirect(this.viewOrderLink);
                }

                if (e.CommandName == "RefundOrder")
                {
                    Response.Redirect(this.refundOrderLink + HttpUtility.UrlEncode(encryption.EncryptData(Id)) + "&Pg=" + HttpUtility.UrlEncode(encryption.EncryptData("List")));
                }
                else if (e.CommandName == "Status")
                {
                    this.changeStatusLink = this.changeStatusLink + HttpUtility.UrlEncode(encryption.EncryptData(Id)) + "&Pg=" + HttpUtility.UrlEncode(encryption.EncryptData("List"));
                    Response.Redirect(this.changeStatusLink);
                }
                else if (e.CommandName == "Capture")
                {
                    Response.Redirect(this.captureLink + HttpUtility.UrlEncode(encryption.EncryptData(Id)) + "&Pg=" + HttpUtility.UrlEncode(encryption.EncryptData("List")));
                }
            }
        }

        #endregion

        #region Helper methods
        /// <summary>
        /// Includes Javascript file and css file into this page
        /// </summary>
        protected void RegisterClientScript()
        {
            // Include the Client Side Script from the resource file
            // The Resource File is named �Calender.js�
            // Located inside the Calendar directory
            HtmlGenericControl Include = new HtmlGenericControl("script");
            Include.Attributes.Add("type", "text/javascript");
            Include.Attributes.Add("src", "Calendar/Calendar.js");

            // The Resource File is named �Calender.css�
            // Located inside the Calendar directory
            HtmlGenericControl Include1 = new HtmlGenericControl("link");
            Include1.Attributes.Add("type", "text/css");
            Include1.Attributes.Add("rel", "stylesheet");
            Include1.Attributes.Add("href", "Calendar/Calendar.css");

            // Add a script reference for Javascript to the head section
            this.Page.Header.Controls.Add(Include);
            this.Page.Header.Controls.Add(Include1);
        }

        /// <summary>
        /// Get the customer full name
        /// </summary>
        /// <param name="firstName">Customer first name</param>
        /// <param name="lastName">Customer last name</param>
        /// <returns>Returns the customer full name.</returns>
        protected string GetCustomerName(object firstName, object lastName)
        {
            return firstName.ToString() + " " + lastName.ToString();
        }

        /// <summary>
        /// Enable or disable refund button
        /// </summary>
        /// <param name="prymentTypeId">Payment Type Id</param>
        /// <param name="paymentStatusId">Payment status Id</param>
        /// <returns>Returns true if refund enabled else false.</returns>
        protected bool EnableRefund(object prymentTypeId, object paymentStatusId)
        {
            bool isEnabled = false;
            if (prymentTypeId != null)
            {
                if (prymentTypeId.ToString().Length > 0)
                {
                    if (int.Parse(prymentTypeId.ToString()) == 1 && (int.Parse(paymentStatusId.ToString()) != 4))
                    {
                        isEnabled = true;
                    }
                    else
                    {
                        isEnabled = false;
                    }
                }
            }

            return isEnabled;
        }

        /// <summary>
        /// Enable or Disable Capture Button
        /// </summary>
        /// <param name="paymentTypeId">Payment Type Id</param>
        /// <param name="paymentStatusId">Payment status Id.</param>
        /// <returns>Returns true if Capture enable </returns>
        protected bool EnableCapture(object paymentTypeId, object paymentStatusId)
        {
            if (paymentStatusId != null)
            {
                if (paymentStatusId.ToString().Length > 0)
                {
                    if (int.Parse(paymentStatusId.ToString()) == 0 && Convert.ToString(paymentTypeId) == "1")
                    {
                        // Show Capture button only for credit card payment type and status is authorized.
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }

            return false;
        }

        /// <summary>
        /// Get Store Name by PortalId
        /// </summary>
        /// <param name="portalId">Portal Id to get the portal object</param>
        /// <returns>Returns the store name</returns>
        protected string GetStoreName(string portalId)
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            return portalAdmin.GetStoreNameByPortalID(portalId);
        }

        /// <summary>
        /// Get the order line items
        /// </summary>
        /// <returns>Returns the order line item dataset based on the input fields.</returns>
        private DataSet GetOrderLineItems()
        {
            OrderAdmin orderAdmin = new OrderAdmin();

            string stdate = String.Empty;
            string endDate = String.Empty;

            // Check for Search is enabled or not
            if (isSearchEnabled)
            {
                stdate = txtStartDate.Text.Trim();
                endDate = txtEndDate.Text.Trim();
            }

            DataSet orderLineItemDataSet = orderAdmin.GetOrderLineItems(txtorderid.Text.Trim(), txtfirstname.Text.Trim(), txtlastname.Text.Trim(), txtcompanyname.Text.Trim(), txtaccountnumber.Text.Trim(), stdate, endDate, int.Parse(ListOrderStatus.SelectedValue.ToString()), UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0));
            return orderLineItemDataSet;
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind Portals
        /// </summary>        
        private void BindPortal()
        {
            PortalAdmin portalAdmin = new PortalAdmin();

            // Portal Drop Down List
            ddlPortal.DataSource = UserStoreAccess.CheckStoreAccess(portalAdmin.GetAllPortals());
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();
        }

        /// <summary>
        /// Bind order search data.
        /// </summary>
        private void BindSearchData()
        {
            if (this.portalIds.Length > 0)
            {
                string firstName = null;
                string lastName = null;
                string companyName = null;
                string accountNumber = null;
                DateTime? startDate = null;
                DateTime? endDate = null;
                int? orderStateId = null;
                int? portalId = null;
                int? orderId = null;

                if (txtorderid.Text.Length > 0)
                {
                    orderId = Int16.Parse(txtorderid.Text.Trim());
                }

                if (txtfirstname.Text.Length > 0)
                {
                    firstName = txtfirstname.Text.Trim();
                }

                if (txtlastname.Text.Length > 0)
                {
                    lastName = txtlastname.Text.Trim();
                }

                if (txtcompanyname.Text.Length > 0)
                {
                    companyName = txtcompanyname.Text.Trim();
                }

                if (txtaccountnumber.Text.Length > 0)
                {
                    accountNumber = txtaccountnumber.Text.Trim();
                }

                if (txtlastname.Text.Length > 0)
                {
                    lastName = txtlastname.Text.Trim();
                }

                if (txtStartDate.Text.Length > 0)
                {
                    startDate = DateTime.Parse(txtStartDate.Text.Trim());
                }

                if (txtEndDate.Text.Length > 0)
                {
                    endDate = DateTime.Parse(txtEndDate.Text.Trim());
                }

                if (ListOrderStatus.SelectedValue != "0")
                {
                    orderStateId = Convert.ToInt32(ListOrderStatus.SelectedValue);
                }

                if (ddlPortal.SelectedValue != "0")
                {
                    portalId = Convert.ToInt32(ddlPortal.SelectedValue);
                }

                OrderAdmin orderAdmin = new OrderAdmin();
                this.ordersDataSet = orderAdmin.FindOrders(orderId, firstName, lastName, companyName, accountNumber, startDate, endDate, orderStateId, portalId, this.portalIds);

                DataView dv = new DataView(this.ordersDataSet.Tables[0]);
                dv.Sort = "OrderID Desc";
                uxGrid.DataSource = dv;
                uxGrid.DataBind();
            }
            else
            {
                lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoPermission").ToString();
            }
        }

        /// <summary>
        /// Bind order data.
        /// </summary>
        private void BindOrderState()
        {
            ZNode.Libraries.Admin.OrderAdmin orderAdmin = new ZNode.Libraries.Admin.OrderAdmin();

            // Add New Item
            ListItem Li = new ListItem();
            Li.Text = "All";
            Li.Value = "0";

            // Load Order State Item 
            ListOrderStatus.DataSource = orderAdmin.GetAllOrderStates();
            ListOrderStatus.DataTextField = "OrderStateName";
            ListOrderStatus.DataValueField = "OrderStateId";
            ListOrderStatus.DataBind();
            ListOrderStatus.Items.Insert(0, Li);
            ListOrderStatus.Items[0].Selected = true;

            var lastItemIndex = ListOrderStatus.Items.Count - 1;

            // Find and remove the PENDING APPROVAL item
            var listItemPending = ListOrderStatus.Items[lastItemIndex];
            ListOrderStatus.Items.RemoveAt(lastItemIndex);

            ListOrderStatus.Items.Insert(1, listItemPending);
        }
        #endregion

        protected void uxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            foreach(GridViewRow gvC in uxGrid.Rows)
            {
                Label OrderStatus = (Label)gvC.FindControl("OrderStatus");
                if(OrderStatus.Text == "PENDING APPROVAL")
                {
                    OrderStatus.Text = "PENDING";
                    OrderStatus.ForeColor = System.Drawing.Color.Red;   
                }
            }
        }
    }
}