<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master"
	AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.ViewOrders.View"
	ValidateRequest="false" CodeBehind="View.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
	<div align="center">
		<div>
			<div class="LeftFloat" style="width: 30%; text-align: left">
				<h1><asp:Localize ID="TitleorderID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOrderID %>'></asp:Localize>
                    <asp:Label ID="lblOrderHeader" runat="server" Text="Label" /></h1>
			</div>
			<div style="float: right">
                <zn:Button runat="server" ID="List" Width="150px" ButtonType="EditButton" OnClick="List_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonBackToOrder %>' />
                <zn:Button runat="server" ID="Refund" Width="150px" ButtonType="EditButton" OnClick="Refund_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonVoidOrRefund %>' />
			</div>
			<div class="ClearBoth">
			</div>
			<div align="left">
				<div>
					<div class="LeftFloat" style="width: 620px">
						<!-- Order Info -->
						<h4 class="SubTitle"><asp:Localize ID="OrderInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderInformation %>'></asp:Localize></h4>
						<div class="ViewForm100">
							<div class="FieldStyle">
								<asp:Localize ID="OrderStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderStatus %>'></asp:Localize>
							</div>
							<div class="ValueStyle">
								<div class="LeftFloat">
									<asp:Label ID="lblOrderStatus" Font-Bold="true" runat="server" />
								</div>
								<div class="RightFloatButton">
                                    <zn:Button runat="server" ID="ChangeStatus" Width="180px" ButtonType="EditButton" OnClick="ChangeStatus_Click" Text='<%$ Resources:ZnodeAdminResource, TitleUpdateOrderStatus %>' />
								</div>
							</div>
							<div class="FieldStyleA">
								<asp:Localize ID="PaymentStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePaymentStatus %>'></asp:Localize>
							</div>
							<div class="ValueStyleA">
								<asp:Label ID="lblPaymentStatus" Font-Bold="true" runat="server" />&nbsp;
							</div>
							<div class="FieldStyle">
								 <asp:Localize ID="OrderDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderDate %>'></asp:Localize>
							</div>
							<div class="ValueStyle">
								<asp:Label ID="lblOrderDate" runat="server" />
							</div>
							<div class="FieldStyleA">
								<asp:Localize ID="OrderAmount" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderAmount %>'></asp:Localize>
							</div>
							<div class="ValueStyleA">
								<asp:Label ID="lblOrderAmount" runat="server" />&nbsp;
							</div>
							<div class="FieldStyle">
								<asp:Localize ID="ShippingAmount" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleShippingAmount %>'></asp:Localize>
							</div>
							<div class="ValueStyle">
								<asp:Label ID="lblShipAmount" runat="server" />
							</div>
							<div class="FieldStyleA">
								<asp:Localize ID="TaxAmount" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTaxAmount %>'></asp:Localize>
							</div>
							<div class="ValueStyleA">
								<asp:Label ID="lblTaxAmount" runat="server" />&nbsp;
							</div>
							<div class="FieldStyle">
								<asp:Localize ID="DiscountAmount" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDiscountAmount %>'></asp:Localize>
							</div>
							<div class="ValueStyle">
								<asp:Label ID="lblDiscountAmt" runat="server" />
							</div>
							<div class="FieldStyleA">
								<asp:Localize ID="GiftCardAmount" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnGiftCardAmount %>'></asp:Localize>
							</div>
							<div class="ValueStyleA">
								<asp:Label ID="lblGiftCardAmount" runat="server" />
							</div>
							<div class="FieldStyle">
								<asp:Localize ID="PaymentMethod" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePaymentMethod %>'></asp:Localize>
							</div>
							<div class="ValueStyle">
								<asp:Label ID="lblPaymentType" runat="server" />&nbsp;
							</div>
							<div class="FieldStyleA">
								<asp:Localize ID="TransactionID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTransactionID %>'></asp:Localize>
							</div>
							<div class="ValueStyleA">
								<asp:Label ID="lblTransactionId" runat="server" />&nbsp;
							</div>
							<div class="FieldStyle">
								<asp:Localize ID="PurchaseOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePurchaseOrder %>'></asp:Localize>
							</div>
							<div class="ValueStyle">
								<asp:Label ID="lblPurchaseOrder" runat="server" />
							</div>
							<div class="FieldStyleA">
								<asp:Localize ID="ShippingMethod" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleShippingMethod %>'></asp:Localize>
							</div>
							<div class="ValueStyleA">
								<asp:Label ID="lblShippingMethod" runat="server" />&nbsp;
							</div>
							<div class="FieldStyle">
								<asp:Localize ID="TrackingNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource,  ColumnTitleTrackingNumber %>'></asp:Localize>
							</div>
							<div class="ValueStyle">
								<asp:Label ID="lblTrackingNumber" runat="server" />
							</div>
							<div class="FieldStyleA">
								<asp:Localize ID="PromotionCodes" runat="server" Text='<%$ Resources:ZnodeAdminResource,  ColumnTitlePromotionCodes %>'></asp:Localize>
							</div>
							<div class="ValueStyleA">
								<asp:Label ID="lblCouponCode" runat="server" />&nbsp;
							</div>
						</div>
					</div>
					<div class="LeftFloat" style="width: 100%;">
						<!-- Address -->
						<h4 class="SubTitle"><asp:Localize ID="CustomerInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource,  SubTitleCustomerInformation %>'></asp:Localize></h4>
						<div>
							
							<div style="float: left; width: 400px; word-wrap: break-word;">
								<div class="FieldStyle" style="text-align: left">
									<b><asp:Localize ID="ShippingAddress" runat="server" Text='<%$ Resources:ZnodeAdminResource,  ColumnShippingAddress %>'></asp:Localize></b>
								</div>
								<div class="ValueStyle">
									<asp:Label ID="lblShippingAddress" runat="server" Text="Label"></asp:Label>
								</div>
							</div>
							<div class="LeftFloat" style="text-align: left; width: 400px; word-wrap: break-word;">
								<div class="FieldStyle" style="text-align: left">
									<b><asp:Localize ID="BillingAddress" runat="server" Text='<%$ Resources:ZnodeAdminResource,  ColumnBillingAddress %>'></asp:Localize>s</b>
								</div>
								<div class="ValueStyle">
									<asp:Label ID="lblBillingAddress" runat="server" Text="Label"></asp:Label>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="ClearBoth">
				</div>
				<br />
				<asp:Panel ID="ShippingErrorPanel" runat="server">
					<h4>
						<asp:Label ID="ErrorHeader" Text="Shipping Errors" runat="server"></asp:Label></h4>
					<div align="justify">
						<asp:Label ID="ShippingErrors" runat="server" CssClass="Error"></asp:Label>
					</div>
				</asp:Panel>
				<h4 class="GridTitle"><asp:Localize ID="OrderItem" runat="server" Text='<%$ Resources:ZnodeAdminResource,  GridTitleOrderItem %>'></asp:Localize></h4>
				<asp:GridView ID="uxGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
					runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
					CssClass="Grid" Width="100%" GridLines="None" EmptyDataText='<%$ Resources:ZnodeAdminResource,  GridOrderlineEmptyData %>'
					AllowPaging="True" OnPageIndexChanging="UxGrid_PageIndexChanging" OnRowCommand="UxGrid_RowCommand"
					PageSize="10">
					<Columns>
						<asp:BoundField DataField="OrderLineItemID" HeaderText='<%$ Resources:ZnodeAdminResource,  ColumnTitleLineItemID %>' HeaderStyle-HorizontalAlign="Left" />
						<asp:BoundField DataField="Name" HtmlEncode="false"  HeaderText='<%$ Resources:ZnodeAdminResource,  ColumnTitleProductName %>'  HeaderStyle-HorizontalAlign="Left" />
						<asp:BoundField DataField="ProductNum" HeaderText='<%$ Resources:ZnodeAdminResource,  ColumnProductCode %>' HeaderStyle-HorizontalAlign="Left" />
						<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,  ColumnTitleDescription %>' HeaderStyle-HorizontalAlign="Left">
							<ItemTemplate>
								<%# DataBinder.Eval(Container.DataItem, "Description") %>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="SKU" HeaderText='<%$ Resources:ZnodeAdminResource,  ColumnTitleProductsSKU %>' HeaderStyle-HorizontalAlign="Left" />
						<asp:BoundField DataField="Quantity" HeaderText='<%$ Resources:ZnodeAdminResource,  ColumnTitleQuantity %>' HeaderStyle-HorizontalAlign="Left" />
						<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,  ColumnTitlePrice %>'  HeaderStyle-HorizontalAlign="Left">
							<ItemTemplate>
								<%# DataBinder.Eval(Container.DataItem, "price","{0:c}").ToString()%>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="ShipDate" HeaderText='<%$ Resources:ZnodeAdminResource,  ColumnTitleShipDate %>' DataFormatString="{0:d}"
							HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />
								<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,  ColumnTitleShipAddress %>' HeaderStyle-HorizontalAlign="Left">
							<ItemTemplate>
								<div class="remark">
									<asp:LinkButton ID="lnkShipName" runat="server"><%# DataBinder.Eval(Container.DataItem, "OrderShipmentIDSource.ShipName")%></asp:LinkButton>
									<div id="divPopup" class="remarkdetail">
										<%# DataBinder.Eval(Container.DataItem, "OrderShipmentIDSource.ShipName")%><br />
										<%# DataBinder.Eval(Container.DataItem, "OrderShipmentIDSource.ShipToFirstName")%> and
										<%# DataBinder.Eval(Container.DataItem, "OrderShipmentIDSource.ShipToLastName")%><br />
										<%# DataBinder.Eval(Container.DataItem, "OrderShipmentIDSource.ShipToStreet")%><br />
										<%# DataBinder.Eval(Container.DataItem, "OrderShipmentIDSource.ShipToCity")%>,
										<%# DataBinder.Eval(Container.DataItem, "OrderShipmentIDSource.ShipToStateCode")%>
										<%# DataBinder.Eval(Container.DataItem, "OrderShipmentIDSource.ShipToPostalCode")%><br />
										<%# DataBinder.Eval(Container.DataItem, "OrderShipmentIDSource.ShipToCountry")%><br />
										Tel:<%# DataBinder.Eval(Container.DataItem, "OrderShipmentIDSource.ShipToPhoneNumber")%></div>
								</div>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="TrackingNumber" HeaderText='<%$ Resources:ZnodeAdminResource,  ColumnTitleTrackingNumber %>' HeaderStyle-HorizontalAlign="Left" />
						<asp:BoundField DataField="TransactionNumber" HeaderText='<%$ Resources:ZnodeAdminResource,  ColumnTitleTransactionNumber %>' HeaderStyle-HorizontalAlign="Left" />
					</Columns>
					<EmptyDataTemplate>
						<asp:Localize ID="BillingAddress" runat="server" Text='<%$ Resources:ZnodeAdminResource,  GridOrderlineEmptyData %>'></asp:Localize>
					</EmptyDataTemplate>
					<RowStyle CssClass="RowStyle" />
					<HeaderStyle CssClass="HeaderStyle" />
					<AlternatingRowStyle CssClass="AlternatingRowStyle" />
					<FooterStyle CssClass="FooterStyle" />
					<PagerStyle CssClass="PagerStyle" />
				</asp:GridView>
				
				<div style="display: none">
					<h4 class="GridTitle"><asp:Localize ID="OrderShipSeperately" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleOrderShipSeperately %>'></asp:Localize></h4>
					<asp:GridView ID="uxGrid2" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
						runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
						CssClass="Grid" Width="100%" GridLines="None" EmptyDataText="No orderline items found"
						AllowPaging="True" OnPageIndexChanging="UxGrid_PageIndexChanging" OnRowCommand="UxGrid_RowCommand"
						PageSize="10">
						<Columns>
							<asp:BoundField DataField="OrderLineItemID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleLineItemID %>' HeaderStyle-HorizontalAlign="Left" />
							<asp:BoundField DataField="Name" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left" />
							<asp:BoundField DataField="ProductNum" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnProductCode %>' HeaderStyle-HorizontalAlign="Left" />
							<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDescription %>' HeaderStyle-HorizontalAlign="Left">
								<ItemTemplate>
									<%# DataBinder.Eval(Container.DataItem, "Description") %>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="SKU" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSKU %>' HeaderStyle-HorizontalAlign="Left" />
							<asp:BoundField DataField="Quantity" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleQuantity %>' HeaderStyle-HorizontalAlign="Left" />
							<asp:TemplateField HeaderText="Price" HeaderStyle-HorizontalAlign="Left">
								<ItemTemplate>
									<%# DataBinder.Eval(Container.DataItem, "price","{0:c}").ToString()%>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="ShipDate" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleShipDate %>' DataFormatString="{0:d}"
								HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />
							<asp:TemplateField HeaderText="Shipping Cost" HeaderStyle-HorizontalAlign="Left">
								<ItemTemplate>
									<%#DataBinder.Eval(Container.DataItem, "ShippingCost", "{0:c}").ToString()%>
								</ItemTemplate>
							</asp:TemplateField>
							<asp:BoundField DataField="TrackingNumber" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleTrackingNumber %>' HeaderStyle-HorizontalAlign="Left" />
							<asp:TemplateField Visible="false">
								<ItemTemplate>
									<asp:Button runat="server" CausesValidation="true" ID="CancelShipping" Text='<%# SetButtonText(Eval("TrackingNumber"),Eval("OrderLineItemID")) %>'
										CssClass="Button" CommandArgument='<%# Eval("OrderLineItemID") %>' CommandName='<%# ShippingCommand(Eval("TrackingNumber"),Eval("OrderLineItemID")) %>'
										Visible="false" />
								</ItemTemplate>
							</asp:TemplateField>
							<asp:TemplateField Visible="false">
								<ItemTemplate>
									<asp:Button runat="server" CausesValidation="false" ID="Label" Text='<%$ Resources:ZnodeAdminResource, TextPrintLabel %>'
										CssClass="Button" CommandArgument='<%# Eval("TrackingNumber") %>' CommandName="Label"
										Visible='<%# ShowLabelButton(Eval("TrackingNumber")) %>' />
								</ItemTemplate>
							</asp:TemplateField>
						</Columns>
						<EmptyDataTemplate>
							 <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, RecordNotFoundItems %>'></asp:Localize>
						</EmptyDataTemplate>
						<RowStyle CssClass="RowStyle" />
						<HeaderStyle CssClass="HeaderStyle" />
						<AlternatingRowStyle CssClass="AlternatingRowStyle" />
						<FooterStyle CssClass="FooterStyle" />
						<PagerStyle CssClass="PagerStyle" />
					</asp:GridView>
					<asp:HiddenField runat="server" ID="EstimatedLineItemID" />
				</div>
				<h4 class="SubTitle"><asp:Localize ID="AdditionalInstructions" runat="server" Text='<%$ Resources:ZnodeAdminResource,  SubTitleAdditionalInstructions %>'></asp:Localize></h4>
				<div align="justify">
					<asp:Label ID="lblAdditionalInstructions" runat="server"></asp:Label>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
