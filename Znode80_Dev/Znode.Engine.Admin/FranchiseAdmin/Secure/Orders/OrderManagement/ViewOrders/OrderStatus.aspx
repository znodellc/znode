<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.ViewOrders.OrderStatus" CodeBehind="OrderStatus.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <h1>
            <asp:Localize ID="TitleUpdateOrderStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleUpdateOrderStatus %>'></asp:Localize>
        </h1>
        <div style="margin-bottom: 20px;">
            <p>
                <asp:Localize ID="TextUpdateOrderStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextUpdateOrderStatus %>'></asp:Localize>
            </p>
        </div>
        <div style="margin-bottom: 20px;">
            <div class="BottomPixel">
                <b>
                    <asp:Localize ID="OrderID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOrderID %>'></asp:Localize></b>
                <asp:Label ID="lblOrderID" runat="server" />
            </div>
            <div class="BottomPixel">
                <b>
                    <asp:Localize ID="CustomerName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCustomerName %>'></asp:Localize></b>
                <asp:Label ID="lblCustomerName" runat="server" />
            </div>
            <div class="BottomPixel">
                <b>
                    <asp:Localize ID="OrderTotal" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOrderTotal %>'></asp:Localize>
                </b>
                <asp:Label ID="lblTotal" runat="server" />
            </div>
            <div>
                <b>
                    <asp:Localize ID="PaymentStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPaymentStatus %>'></asp:Localize></b>
                <asp:Label ID="lblPaymentStatus" runat="server" />
            </div>
        </div>
        <div class="FieldStyle">
            <b>
                <asp:Localize ID="OrderStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOrderStatus %>'></asp:Localize>&nbsp;&nbsp;&nbsp;&nbsp;</b>
            <asp:DropDownList ID="ListOrderStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="OrderStatus_SelectedIndexChanged" />
        </div>
        <br />
        <p>
            <small>
                <asp:Localize ID="OrderStatusNote" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextOrderStatusNote %>'></asp:Localize>
            </small>
        </p>
        <br />
        <asp:Panel ID="pnlTrack" runat="server">
            <div class="FieldStyle">
                <asp:Label ID="TrackTitle" runat="server"></asp:Label>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="TrackingNumber" runat="server"></asp:TextBox>
            </div>
            <div>
                <asp:Label ID="trackmessage" runat="server"></asp:Label><br />
                <br />
            </div>
        </asp:Panel>
        <div>
            <zn:Button runat="server" ID="UpdateOrderStatus" ButtonType="EditButton" Width="100px" OnClick="UpdateOrderStatus_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonUpdate %>' />
            <zn:Button ID="btnCancelOrder" runat="server" ButtonType="CancelButton" OnClick="CancelStatus_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
        </div>
        <uc1:Spacer ID="LongSpace" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
</asp:Content>
