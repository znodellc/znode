using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.ViewOrders
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_orders_orderStatus class.
    /// </summary>
    public partial class OrderStatus : System.Web.UI.Page
    {
        #region Private Member Variables
        private int orderId = 0;
        private string listPage = "Default.aspx";
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        #region General Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.Params["itemid"] != null) || (Request.Params["itemid"].Length != 0))
            {
                this.orderId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
                lblOrderID.Text = this.orderId.ToString();
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }

        /// <summary>
        /// Update Order Status button click event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UpdateOrderStatus_Click(object sender, EventArgs e)
        {
            OrderAdmin orderAdmin = new OrderAdmin();
            Order order = orderAdmin.GetOrderByOrderID(this.orderId);
            if (order != null)
            {
                order.OrderStateID = int.Parse(ListOrderStatus.SelectedValue);
                order.OrderID = this.orderId;
                order.TrackingNumber = TrackingNumber.Text.Trim();
                if (order.OrderStateID == 20)
                {
                    order.ShipDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                }

                if (order.OrderStateID == 30)
                {
                    order.ReturnDate = DateTime.Now.Date + DateTime.Now.TimeOfDay;
                }

                // Delete referral commission entry of order if order status is "RETURNED" (30) or "CANCELLED" (40)
                bool isUpdated = orderAdmin.Update(order);
                bool isReferralCommissionDeleted = true;           

                if (order.OrderStateID == 30 || order.OrderStateID == 40)
                {
                    ReferralCommissionAdmin referralCommissionAdmin = new ReferralCommissionAdmin();
                    if (referralCommissionAdmin.GetReferralCommissionByOrderID(this.orderId))
                    {
                        referralCommissionAdmin.GetReferralCommissionByOrderID(this.orderId);
                        isReferralCommissionDeleted = referralCommissionAdmin.DeleteByOrderId(this.orderId);
                    }                   
                }

                if (isUpdated && isReferralCommissionDeleted)
                {
                    //If Selected index changed is selected as Shipped then Send the mail
                    if (order.OrderStateID == 20)
                    {
                        Order _OrderList = orderAdmin.DeepLoadByOrderID(this.orderId);
                        this.SendEmailReceipt(_OrderList);
                    }
                    Response.Redirect(this.listPage);
                }
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CancelStatus_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listPage);
        }

        /// <summary>
        /// Dropdown list selected index change
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void OrderStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ListOrderStatus.SelectedValue.Equals("20"))
            {
                this.BindData();
                ListOrderStatus.SelectedValue = "20";
             
            }
            
        }

        /// <summary>
        /// Email Status Button Click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        //protected void EmailStatus_Click(object sender, EventArgs e)
        //{
        //    OrderAdmin orderAdmin = new OrderAdmin();
        //    Order order = orderAdmin.DeepLoadByOrderID(this.orderId);
        //    this.SendEmailReceipt(order);
        //}

        /// <summary>
        /// Send the order status email to customer.
        /// </summary>
        /// <param name="order">Order object</param>
        private void SendEmailReceipt(Order order)
        {
            string recepientEmail = string.Empty;

            try
            {
                recepientEmail = order.BillingEmailId;
                string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
                ZNodeConfigManager.AliasSiteConfig(Convert.ToInt32(UserStoreAccess.GetTurnkeyStorePortalID));
                if (string.IsNullOrEmpty(senderEmail))
                {
                    trackmessage.CssClass = "Error";
                    trackmessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextTrackMessageSender").ToString();
                    return;
                }

                if (string.IsNullOrEmpty(recepientEmail))
                {
                    trackmessage.CssClass = "Error";
                    trackmessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextTrackMessageRecipient").ToString();
                    return;
                }

                string subject = this.GetGlobalResourceObject("ZnodeAdminResource", "TextTrackSubject").ToString();

                // Get message text.
                StreamReader rw = new StreamReader(Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "TrackingNumber.htm"));
                string messageText = rw.ReadToEnd();
                Regex firstName = new Regex("#BillingFirstName#", RegexOptions.IgnoreCase);
                messageText = firstName.Replace(messageText, order.BillingFirstName);

                Regex lastName = new Regex("#BillingLastName#", RegexOptions.IgnoreCase);
                messageText = lastName.Replace(messageText, order.BillingLastName);

                Regex custom1 = new Regex("#Custom1#", RegexOptions.IgnoreCase);
                messageText = custom1.Replace(messageText, TrackingNumber.Text);

                Regex trackingMessage = new Regex("#TrackingMessage#", RegexOptions.IgnoreCase);
                if (order.ShippingIDSource.ShippingTypeID == 3)
                {
                    messageText = trackingMessage.Replace(messageText, this.GetGlobalResourceObject("ZnodeAdminResource", "TextYourFedExTrackingNo").ToString()); 
                }
                else if (order.ShippingIDSource.ShippingTypeID == 2)
                {
                    messageText = trackingMessage.Replace(messageText, this.GetGlobalResourceObject("ZnodeAdminResource", "TextYourUPSTrackingNo").ToString());
                }
                else
                {
                    messageText = trackingMessage.Replace(messageText, string.Empty);
                }

                Regex message = new Regex("#Message#", RegexOptions.IgnoreCase);
                if (order.ShippingIDSource.ShippingTypeID == 3)
                {
                    messageText = message.Replace(messageText, this.GetGlobalResourceObject("ZnodeAdminResource", "TextTrackMessageShippedTrackNo").ToString());
                }
                else if (order.ShippingIDSource.ShippingTypeID == 2)
                {
                    messageText = message.Replace(messageText, this.GetGlobalResourceObject("ZnodeAdminResource", "TextTrackMessageShippedTrackNo").ToString());
                }
                else
                {
                    messageText = message.Replace(messageText, this.GetGlobalResourceObject("ZnodeAdminResource", "TextTrackMessageShipped").ToString());
                }

                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(recepientEmail, senderEmail, senderEmail, subject, messageText, true);

                trackmessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextTrackingNoEmail").ToString() + "<a href=\"mailto:" + recepientEmail.ToString() + "\">" + recepientEmail + "</a>";
                
               // EmailStatus.Enabled = false;
            }
            catch (Exception ex)
            {
                trackmessage.CssClass = "Error";
                trackmessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextProblemSendingEmail").ToString() + "<a href=\"mailto:" + recepientEmail.ToString() + "\">" + recepientEmail + "</a>";

                // Log exception
                ExceptionPolicy.HandleException(ex, "ZNODE_GLOBAL_EXCEPTION_POLICY");                
            }
        }

        #endregion

        #region Bind Data

        private void BindData()
        {
            ZNode.Libraries.Admin.OrderAdmin orderAdmin = new ZNode.Libraries.Admin.OrderAdmin();
            TList<OrderLineItem> orderLineItem = new TList<OrderLineItem>();
            var orderLineItemService = new OrderLineItemService();
            var orderShipment = new OrderShipment();
            var orderShipmentService = new OrderShipmentService();

            // Load Order State Item 
            ListOrderStatus.DataSource = orderAdmin.GetAllOrderStates();
            ListOrderStatus.DataTextField = "OrderStateName";
            ListOrderStatus.DataValueField = "OrderStateId";
            ListOrderStatus.DataBind();

            // Remove Pending Approval Status from the dropdown
            ListItem li = ListOrderStatus.Items.FindByValue(((int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.PENDING_APPROVAL).ToString());
            if (li != null)
            {
                ListOrderStatus.Items.Remove(li);
            }

            ZNode.Libraries.DataAccess.Entities.Order order = orderAdmin.DeepLoadByOrderID(this.orderId);  //GetOrderByOrderID(this.orderId);

            orderLineItem = orderLineItemService.GetByOrderID(this.orderId);
            var orderItem = orderLineItem.FirstOrDefault().OrderShipmentID;
            orderShipment = orderShipmentService.GetByOrderShipmentID(int.Parse(orderItem.ToString()));

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                string[] stores = profiles.StoreAccess.Split(',');
                Array storesArray = (Array)stores;
                int found = Array.IndexOf(storesArray, order.PortalId.ToString());
                if (found == -1)
                {
                    Response.Redirect("Default.aspx", true);
                }
            }

            int shippingId = Convert.ToInt32(orderShipment.ShippingID);

            ZNode.Libraries.Admin.ShippingAdmin shippingAdmin = new ZNode.Libraries.Admin.ShippingAdmin();

            int ShipId = shippingAdmin.GetShippingTypeId(shippingId);

            // Set the Tracking number title based on the ShippingTypeId
            if (ShipId == 1)
            {
                pnlTrack.Visible = true;
                TrackTitle.Visible = false;
                TrackingNumber.Visible = false;
            }
            else if (ShipId == 2)
            {
                pnlTrack.Visible = true;
                TrackTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextFedExTrackingNo").ToString();
            }
            else if (ShipId == 3)
            {
                pnlTrack.Visible = true;
                TrackTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextUPSTrackingNo").ToString();
            }
            else
            {
                pnlTrack.Visible = true;
                TrackTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextUSPSTrackingNo").ToString();
            }

            if (order != null)
            {
                ListItem item = ListOrderStatus.Items.FindByValue(order.OrderStateID.ToString());
                if (item != null)
                {
                    item.Selected = true;
                }

                TrackingNumber.Text = order.TrackingNumber;
                if (order.PaymentStatusIDSource != null)
                {
                    lblPaymentStatus.Text = order.PaymentStatusIDSource.Description;
                }
            }

            lblCustomerName.Text = order.BillingFirstName + " " + order.BillingLastName;
            lblTotal.Text = order.Total.Value.ToString("c");
        }

        #endregion
    }
}