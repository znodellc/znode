﻿using System;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.CreateOrder
{
    /// <summary>
    /// Represents the FranchiseAdmin - OrderDesk Receipt class
    /// </summary>
    public partial class Receipt : System.Web.UI.Page
    {
        #region Events
        /// <summary>
        /// Fires when New Customer link is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LinkNewOrder_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Orders/OrderManagement/CreateOrder/Default.aspx");
        }

        #endregion
    }
}