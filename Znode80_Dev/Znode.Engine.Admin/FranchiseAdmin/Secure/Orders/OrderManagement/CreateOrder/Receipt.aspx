﻿<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.CreateOrder.Receipt" Codebehind="Receipt.aspx.cs" %>
<%@ Register Src="Confirm.ascx" TagName="OrderReceipt" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div align="right">
        <zn:LinkButton ID="LinkButton1" runat="server" CausesValidation="false" Text='<%$ Resources:ZnodeAdminResource, LinkButtonCreateNewOrder %>'
            CommandArgument="CUSTOMER" OnCommand="LinkNewOrder_Click"
            ButtonType="Button" ButtonPriority="Primary" />
    </div>
    <!-- Order Receipt -->
    <div>
        <ZNode:OrderReceipt ID="uxConfirm" runat="server" />
    </div>
    <div>
        <ZNode:Spacer SpacerHeight="20" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>
    <div>
        <zn:LinkButton ID="LinkButton2" runat="server" CausesValidation="false" Text='<%$ Resources:ZnodeAdminResource, LinkButtonCreateNewOrder %>'
            CommandArgument="CUSTOMER" OnCommand="LinkNewOrder_Click"
            ButtonType="Button" ButtonPriority="Primary" />
    </div>
    <div>
        <ZNode:Spacer SpacerHeight="40" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>
</asp:Content>
