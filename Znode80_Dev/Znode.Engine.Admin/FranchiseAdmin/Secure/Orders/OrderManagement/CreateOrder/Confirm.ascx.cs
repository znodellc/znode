using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Xsl;
using Znode.Engine.Common;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Suppliers;

/// <summary>
/// Checkout confirmation
/// </summary>
namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.CreateOrder
{
    /// <summary>
    /// Represents the FranchiseAdmin - Confirm user control class
    /// </summary>
    public partial class Confirm : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeOrderFulfillment _Order = new ZNodeOrderFulfillment();
        private string SiteName = string.Empty;
        private string ReceiptText = string.Empty;
        private string CustomerServiceEmail = string.Empty;
        private string CustomerServicePhoneNumber = string.Empty;
        private string _ReceiptTemplate = string.Empty;

        
        private ZNodeShoppingCart _shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
        private int CatalogId = 0;
        private string FeedBackUrl = string.Empty;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the Order
        /// </summary>
        public ZNodeOrderFulfillment Order
        {
            get
            {
                return this._Order;
            }

            set
            {
                this._Order = value;
            }
        }

        /// <summary>
        /// Gets or sets the order receipt template text.
        /// </summary>
        public string ReceiptTemplate
        {
            get { return _ReceiptTemplate; }
            set { _ReceiptTemplate = value; }
        }
        #endregion

        #region Private Properties

        /// <summary>
        /// Gets the PortalCatalog
        /// </summary>
        private PortalCatalog PortalCatalog
        {
            get
            {
                try
                {
                    PortalCatalogService pa = new PortalCatalogService();

                    PortalCatalogQuery portalCatalogQuery = new PortalCatalogQuery();
                    portalCatalogQuery.Append(PortalCatalogColumn.PortalID, UserStoreAccess.GetTurnkeyStorePortalID.ToString());
                    portalCatalogQuery.Append(PortalCatalogColumn.CatalogID, this.CatalogId.ToString());

                    TList<PortalCatalog> tpc = pa.Find(portalCatalogQuery.GetParameters());

                    PortalCatalog pc = null;

                    if (tpc != null && tpc.Count > 0)
                    {
                        pc = tpc[0];

                        pa.DeepLoad(pc, true, DeepLoadType.IncludeChildren, typeof(Locale), typeof(Theme), typeof(CSS));
                    }

                    return pc;
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                }

                return null;
            }
        }

        /// <summary>
        /// Gets the Receipt CSS path by locale using Theme and CSS as Parameters
        /// </summary>
        private string ReceiptCssPath
        {
            get
            {
                PortalCatalog pc = PortalCatalog;

                if (pc != null)
                {
					string cssFilePath = "~\\Themes\\Default\\" + pc.LocaleIDSource.LocaleCode + "\\receipt\\Receipt.css";

					if (pc.ThemeID == null)
					{
						pc.ThemeID = 1; // "Default";
					}
					else
					{
						cssFilePath = "~\\Themes\\" + pc.ThemeIDSource.Name + "\\" + pc.LocaleIDSource.LocaleCode + "\\receipt\\Receipt.css";
					}

                    bool isCssFileExists = System.IO.File.Exists(HttpContext.Current.Server.MapPath(cssFilePath));

                    if (isCssFileExists)
                    {
                        return "~/Themes/" + pc.ThemeIDSource.Name + "/" + pc.LocaleIDSource.LocaleCode + "/receipt/Receipt.css";
                    }
                    else
                    {
                        return "~/Themes/" + pc.ThemeIDSource.Name + "/receipt/Receipt.css";
                    }
                }

                return "~/Themes/Default/receipt/Receipt.css";
            }
        }
        #endregion

        #region Page Events

        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            this._Order = Session["OrderDetail"] as ZNodeOrderFulfillment;

            if (this._Order != null && this._Order.OrderID > 0)
            {
                // Override the default page analytics so we can include the Receipt Page specific info.
                ZNodeAnalytics analytics = new ZNodeAnalytics();
                analytics.AnalyticsData.IsOrderReceiptPage = true;
                analytics.AnalyticsData.Order = this._Order;
                analytics.Bind();

                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ZNode_JavascriptSiteWideAnalytics", analytics.AnalyticsData.SiteWideBottomJavascript);

                this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ZNode_JavascriptOrderReceipt", analytics.AnalyticsData.OrderReceiptJavascript);
            }
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                int.TryParse(Request.Params["itemid"], out this.CatalogId);
            }

            // HTML 
            HtmlGenericControl cssInclude = new HtmlGenericControl("link");
            cssInclude = new HtmlGenericControl("link");
            cssInclude.Attributes.Add("type", "text/css");
            cssInclude.Attributes.Add("rel", "stylesheet");

            cssInclude.Attributes.Add("href", ResolveUrl(this.ReceiptCssPath));

            // Add a reference for StyleSheet to the head section
            this.Page.Header.Controls.Add(cssInclude);

            if (this._Order != null && this._Order.OrderID > 0)
            {
                this.ReceiptText = ZNodeCatalogManager.MessageConfig.GetMessage("OrderReceiptConfirmationIntroText");
                this.SiteName = ZNodeConfigManager.SiteConfig.CompanyName;
                this.CustomerServiceEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
                this.CustomerServicePhoneNumber = ZNodeConfigManager.SiteConfig.CustomerServicePhoneNumber;
                ZnodeReceipt receipt = new ZnodeReceipt(this._Order, this.FeedBackUrl);
                this.ReceiptTemplate = receipt.GetHtmlReceiptForUI();

                Session.Remove("OrderDetail");

                this._Order = null;

                // Remove the session user account object.
                HttpContext.Current.Session.Remove("AliasUserAccount");

                // Empty cart & remove selected user
                Session.Remove(ZNodeSessionKeyType.ShoppingCart.ToString());
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }
        #endregion
    }
}