using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using Znode.Engine.Common;
using Znode.Engine.Suppliers;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.CreateOrder
{
    /// <summary>
    /// Represents the FranchiseAdmin - Admin_Secure_sales_OrderDesk_Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Private Variables
        private ZNodeShoppingCart shoppingCart;
        private ZNodeCheckout checkout = null;
        private string PortalIds = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Account Object from/to Cache
        /// </summary>
        public ZNodeUserAccount UserAccount
        {
            get
            {
                if (ViewState["AccountObject"] != null)
                {
                    // The account info with 'AccountObject' is retrieved from the cache using Get Method and
                    // It is converted to a Entity Account object
                    return (ZNodeUserAccount)ViewState["AccountObject"];
                }

                return new ZNodeUserAccount();
            }

            set
            {
                // Customer account object is placed in the session and assigned a key, AccountObject.            
                ViewState["AccountObject"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the billing address
        /// </summary>
        public int BillingAddressID
        {
            get
            {
                if (ViewState["BillingAddressID"] != null)
                {
                    return (int)ViewState["BillingAddressID"];
                }

                return 0;
            }

            set
            {
                ViewState["BillingAddressID"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the shipping address
        /// </summary>
        public int ShippingAddressID
        {
            get
            {
                if (ViewState["ShippingAddressID"] != null)
                {
                    return (int)ViewState["ShippingAddressID"];
                }

                return 0;
            }

            set
            {
                ViewState["ShippingAddressID"] = value;
            }
        }

        /// <summary>
        /// Gets the Customer Billing address
        /// </summary>
        public Address BillingAddress
        {
            get
            {
                if (ViewState["BillingAddress"] != null)
                {
                    return (Address)ViewState["BillingAddress"];
                }

                return new Address();
            }

            set
            {
                ViewState["BillingAddress"] = value;
            }
        }

        /// <summary>
        ///  Gets the Customer shipping address
        /// </summary>
        public Address ShippingAddress
        {
            get
            {
                if (ViewState["ShippingAddress"] != null)
                {
                    return (Address)ViewState["ShippingAddress"];
                }

                return new Address();
            }

            set
            {
                ViewState["ShippingAddress"] = value;
            }
        }

        #endregion

        #region Protected Properties

        /// <summary>
        /// Gets the Login Name
        /// </summary>
        protected string LoginName
        {
            get
            {
                string _loginName = string.Empty;
                ZNodeUserAccount _account = this.UserAccount;

                if (_account.UserID.HasValue)
                {
                    MembershipUser user = Membership.GetUser(_account.UserID.Value);

                    if (user != null)
                    {
                        _loginName = user.UserName;
                    }
                }

                return _loginName;
            }
        }

        #endregion

        #region Private Properties
        /// <summary>
        /// Gets the Profile Name
        /// </summary>
        private string ProfileName
        {
            get
            {
                string _profileName = string.Empty;

                _profileName = ZNodeProfile.ProfileName;

                return _profileName;
            }
        }

        #endregion

        #region Page Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Registers the event for the customer search (child) control
            this.uxCustomerSearch.SelectedIndexChanged += new EventHandler(this.FoundUsers_SelectedIndexChanged);

            // Registers the event for the create user (child) control
            this.uxCreateUser.ButtonClick += new EventHandler(this.BtnCancel_Click);

            // Registers the event for the create user (child) control
            this.uxCustomerSearch.SearchButtonClick += new EventHandler(this.BtnSearchClose_Click);

            // Registers the event for the quick Order (child) control - Addto cart button
            this.uxQuickOrder.SubmitButtonClicked += new EventHandler(this.AddToCart_Click);

            // Registers the event for the quick Order (child) control - Addto cart button
            this.uxQuickOrder.CancelButtonClicked += new EventHandler(this.AddToCart_Click);

            // Registers the event for the quick Order (child) control - Add another product button
            this.uxQuickOrder.AddProductButtonClicked += new EventHandler(this.AddAnotherProduct_Click);

            // Registers the event for the payment (child) control - Shipping Type selected change list
            this.uxPayment.ShippingSelectedIndexChanged += new EventHandler(this.Shipping_SelectedIndexChanged);

            // Registers the event for the quick Order (child) control - Addto cart button
            this.uxCartItems.CloseButtonClicked += new EventHandler(this.AddToCart_Click);

            // Registers the event for the payment (child) control - Shipping Type selected change list
            this.uxCartItems.AddressSelectedIndexChanged += new EventHandler(this.Address_SelectedIndexChanged);

            // Registers the event for the payment (child) control - Shipping Type selected change list
            this.uxCartItems.ContinueButtonClicked += new EventHandler(this.Continue_ButtonClicked);

            this.uxShoppingCart.ChangeShipToChanged += new EventHandler(this.ChangeShipTo_Clicked);

			this.uxShoppingCart.ShippingSelectedIndexChanged += new EventHandler(this.Shipping_SelectedIndexChanged);

			this.uxCartItems.MultiShipRemoveItemClicked += (o, args) =>
			{
				this.Bind();
				UpdatePanelOrderDesk.Update();
				mdlCartItemsPopup.Show();
			};

            // Bind Portals
            this.BindPortal();

            if (!Page.IsPostBack)
            {
                // Remove the Session
                Session.Remove("currentStoreID");

                this.shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);

                if (this.shoppingCart != null)
                {
                    //this.shoppingCart.EmptyCart();
					RefreshOrderPage();
                }

                // Bind fields
                this.Bind();
            }
        }

        /// <summary>
        /// Page Pre Render Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            this.shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);

            if (this.shoppingCart != null)
            {
                this.shoppingCart.Payment = uxPayment.ZnodePayment;

                if (this.UserAccount.AccountID > 0)
                {
                    uxPayment.UserAccount = this.UserAccount;
                    this.shoppingCart.Payment.BillingAddress = this.BillingAddress;
                    this.shoppingCart.Payment.ShippingAddress = this.ShippingAddress;
                }

                if (this.shoppingCart.ShoppingCartItems.Count > 0 && this.UserAccount.AccountID > 0)
                {
                    pnlPaymentShipping.Visible = true;

                    // Calculate tax if Customer selected
                    this.CalculateTaxes(this.shoppingCart);
                }
				if (this.shoppingCart.PortalCarts.Select(x => x.Total).FirstOrDefault() == 0)
				{
					uxPayment.ShowPaymentSection = false;
				}
				else
				{
					uxPayment.ShowPaymentSection = true;
				}
                if (this.shoppingCart.ShoppingCartItems.Count > 0)
                {
                    // Calculate shipping  
                    uxShoppingCart.Visible = true;

                    uxShoppingCart.ShoppingCartObject = this.shoppingCart;

                    // Bind Shopping Cart items
                    uxShoppingCart.Bind();
                }
                else
                {
                    uxShoppingCart.Visible = false;
                    pnlPaymentShipping.Visible = false;
                }
				 
                if (shoppingCart.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().Sum(x => x.Quantity) > 1)
                {
					if (ZNodeShoppingCart.CurrentShoppingCart().IsMultipleShipToAddress || !this.UserAccount.UserID.HasValue)
                    {
                        linkShipMultipleAddress.Visible = false;
                    }
                    else
                    {
                        linkShipMultipleAddress.Visible = true;
                    }
                }
                else
                {
                    linkShipMultipleAddress.Visible = false;
                }


            }
            else
            {
                uxShoppingCart.Visible = false;
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// Shipping Type list selected index changed event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChangeShipTo_Clicked(object sender, EventArgs e)
        {
            string addressId = ((System.Web.UI.WebControls.LinkButton)sender).CommandArgument;
            if (this.UserAccount.AccountID > 0)
            {
                uxCreateUser.UserAccount = this.UserAccount;
                uxCreateUser.BindAddressList();
                uxCreateUser.BillingAddressID = this.BillingAddressID;
				uxCreateUser.ShippingAddressID = this.ShippingAddressID;
                uxCreateUser.AddressId = Convert.ToInt32(addressId);
                uxCreateUser.Bind();
                UpdatePanelOrderDesk.Update();
                mdlCreateUserPopup.Show();
            }
        }
        /// <summary>
        /// Fires when Cancel Button Click triggered by child control (Create User)
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
			this.UserAccount = uxCreateUser.UserAccount;

			if (this.UserAccount.AccountID > 0)
			{
				if (uxCreateUser.IsNewCustomer == true)
				{
					this.BillingAddressID = uxCreateUser.BillingAddress.AddressID;
					this.ShippingAddressID = uxCreateUser.ShippingAddress.AddressID;
				}
				else
				{
					this.BillingAddressID = uxCreateUser.BillingAddressID;
					this.ShippingAddressID = uxCreateUser.ShippingAddressID;
				}
				this.BillingAddress = uxCreateUser.BillingAddress;
				this.ShippingAddress = uxCreateUser.ShippingAddress;
				this.Bind();
			}

			UpdatePanelOrderDesk.Update();
			mdlCreateUserPopup.Hide();
        }

        /// <summary>
        /// Shipping Type list selected index changed event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Address_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Bind();
            UpdatePanelOrderDesk.Update();
            mdlCartItemsPopup.Show();
        }
        /// <summary>
        /// Search Close button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearchClose_Click(object sender, EventArgs e)
        {
            this.Bind();

            mdlPopup.Hide();
            UpdatePanelOrderDesk.Update();
        }

        /// <summary>
        /// Cancel Order Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancelOrder_Click(object sender, EventArgs e)
        {
            RefreshOrderPage();

            Response.Redirect("~/FranchiseAdmin/Secure/Orders/Default.aspx");
        }

        /// <summary>
        /// Add To cart Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddToCart_Click(object sender, EventArgs e)
        {
            this.Bind();
            UpdatePanelOrderDesk.Update();
        }

        /// <summary>
        /// Search Customer Link Button click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LinkSearchCustomer_Click(object sender, EventArgs e)
        {
            uxCustomerSearch.ClearUI();
            mdlPopup.Show();
        }

        /// <summary>
        /// Shipping Type list selected index changed event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Continue_ButtonClicked(object sender, EventArgs e)
        {
            this.Bind();
            UpdatePanelOrderDesk.Update();
        }

        /// <summary>
        /// Create new User link button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LinkNewCustomer_Click(object sender, EventArgs e)
        {
			uxPayment.ClearUI();
			uxCreateUser.IsNewCustomer = true;
			uxCreateUser.UserAccount = this.UserAccount;
			// Clear previous values bounded with the text box fields
			uxCreateUser.Bind();
			uxCreateUser.BindCountry();
			mdlCreateUserPopup.Show();


        }

        /// <summary>
        /// Quick Order Popup Link button
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LinkbtnQuickOrder_Click(object sender, EventArgs e)
        {
            uxQuickOrder.Product = new ZNodeProduct();
            uxQuickOrder.Bind();
            UpdatePanelQuickOrder.Update();
            mdlQuickOrderPopup.Show();
        }

        /// <summary>
        /// Quick Order Popup Link button
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddAnotherProduct_Click(object sender, EventArgs e)
        {
            uxQuickOrder.Product = new ZNodeProduct();
            uxQuickOrder.Bind();
            UpdatePanelQuickOrder.Update();
            mdlQuickOrderPopup.Show();
        }

        /// <summary>
        /// Portal Dropdown Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlPortal_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Catalog Dropdown Selected Index Changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Found Users Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void FoundUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            uxPayment.ClearUI();
            this.UserAccount = uxCustomerSearch.UserAccount;
            this.BillingAddressID = uxCustomerSearch.BillingAddressID;
            this.ShippingAddressID = uxCustomerSearch.ShippingAddressID;
            this.BillingAddress = uxCustomerSearch.BillingAddress;
            this.ShippingAddress = uxCustomerSearch.ShippingAddress;
            this.Bind();
            mdlPopup.Hide();
            UpdatePanelOrderDesk.Update();
        }

        /// <summary>
        /// Shipping Type list selected index changed event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Shipping_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Bind();
            UpdatePanelOrderDesk.Update();
        }

        /// <summary>
        /// Edit Customer
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Ui_Edit_Click(object sender, EventArgs e)
        {
			int addressId = this.BillingAddressID;
            if (this.UserAccount.AccountID > 0)
            {
				uxCreateUser.IsNewCustomer = false;
				uxCreateUser.IsShipping = false;
                uxCreateUser.UserAccount = this.UserAccount;
                uxCreateUser.BillingAddressID = this.BillingAddressID;
                uxCreateUser.ShippingAddressID = this.ShippingAddressID;
                uxCreateUser.AddressId = addressId;
                uxCreateUser.Bind();
                UpdatePanelOrderDesk.Update();
                mdlCreateUserPopup.Show();
            }
        }

        /// <summary>
        /// Submit Button Click Event 
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Submit_Click(object sender, EventArgs e)
        {
            // Get ShoppingCart object from current session
            this.shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
            ZNodeOrderFulfillment order = new ZNodeOrderFulfillment();
            ZNodeUserAccount _userAccount = new ZNodeUserAccount();

            // Validate the customer shipping address if address validation enabled.
            if (ZNodeConfigManager.SiteConfig.EnableAddressValidation != null ||
                ZNodeConfigManager.SiteConfig.EnableAddressValidation == true)
            {
                AccountAdmin _accountAdmin = new AccountAdmin();
                bool isAddressValid = _accountAdmin.IsAddressValid(this.ShippingAddress);

                // Do not allow the customer to go to next page if valid shipping address required is enabled. 
                if (!isAddressValid && ZNodeConfigManager.SiteConfig.RequireValidatedAddress != null &&
                    ZNodeConfigManager.SiteConfig.RequireValidatedAddress == true)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCustomerShippingAddress").ToString();
                    return;
                }
            }

            if (this.shoppingCart != null)
            {
                // Check no.of items in the shopping cart object
                if (this.shoppingCart.ShoppingCartItems.Count == 0)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoItemsShoppingCart").ToString(); 
                    return;
                }

                if (!uxPayment.IsPaymentOptionExists)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoPaymentProfile").ToString();
                    return;
                }

                try
                {
                    if (this.UserAccount.AccountID == 0)
                    {
                        btnSubmit.Enabled = true;
                        lblError.Text = "Customer information is incomplete.";
                        return;
                    }

                    // User Settings
                    _userAccount = this.UserAccount;

                    _userAccount.ProfileID = ZNodeProfile.CurrentUserProfileId;
					 

					// Set address
					_userAccount.BillingAddress = this.BillingAddress;
					_userAccount.ShippingAddress = this.ShippingAddress;

					if ((this.BillingAddress.Street.Length == 0) || (this.BillingAddress.City.Length == 0) || (this.BillingAddress.PostalCode.Length == 0) || (this.BillingAddress.StateCode.Length == 0) || (this.BillingAddress.FirstName == null) || (this.BillingAddress.LastName == null))
					{
                        lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCustomerIncomplete").ToString();
						return;
					}

					bool isValidAddress = false;
					isValidAddress = shoppingCart.IsMultipleShipToAddress ?
										_userAccount.CheckValidAddress(_userAccount.BillingAddress) &&
										shoppingCart.PortalCarts.All(x => x.AddressCarts.All(
												y => _userAccount.CheckValidAddress(_userAccount.Addresses.FirstOrDefault(z => z.AddressID == y.AddressID)))) : true;

					if (!isValidAddress)
					{
                        lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidShippingAddress").ToString();
						return;
					}
					 

                    // Get data from user controls
                    this.GetControlData(_userAccount);

                    // Set the current store portalID 
                    // To make a note in Order table.
                    this.checkout.PortalID = ZNodeConfigManager.SiteConfig.PortalID;
                    this.checkout.UserAccount = _userAccount;

                    if (this.checkout.ShoppingCart.PreSubmitOrderProcess())
                    {
                        order = (ZNodeOrderFulfillment)this.checkout.SubmitOrder();
                    }
                    else
                    {
                        lblError.Text = this.checkout.ShoppingCart.ErrorMessage;
                        return;
                    }
                }
                catch (ZNode.Libraries.ECommerce.Entities.ZNodePaymentException ex)
                {
                    // Display payment error message
                    lblError.Text = ex.Message;
                    return;
                }
                catch
                {
                    // Display error page
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSubmitOrder").ToString();
                    return;
                }

                if (this.checkout.IsSuccess)
                {
                    // Receipt 
                    Session["OrderDetail"] = order;

                    // Update product inventory and coupon
                    this.OnSubmitOrder(order, this.checkout.ShoppingCart);

                    // Log Activity
                    string AssociateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogCreateOrder").ToString(), order.OrderID, order.BillingAddress.FirstName);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, "OrderDesk");

                    this.ResetProfileCache();

                    Response.Redirect("Receipt.aspx?itemid=" + uxQuickOrder.CatalogId);
                }
                else
                {
                    // Display error page
                    lblError.Text = this.checkout.PaymentResponseText;
                    return;
                }
            }
            else
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoItemsShoppingCart").ToString();
                return;
            }
        }

        /// <summary>
        /// This method get data from user controls(payment and shippping info)
        /// </summary>
        /// <param name="userAccount">The User Account instance</param>
        protected void GetControlData(ZNodeUserAccount userAccount)
        {
            // Get objects from session
            this.checkout = new ZNodeCheckout();

            // Set payment data in checkout object
            this.checkout.ShoppingCart.Payment = uxPayment.ZnodePayment;
            this.checkout.ShoppingCart.Payment.BillingAddress = this.BillingAddress;
            this.checkout.ShoppingCart.Payment.ShippingAddress = this.ShippingAddress;
            this.checkout.UserAccount = new ZNodeUserAccount();
            this.checkout.UserAccount.BillingAddress = this.BillingAddress;
            this.checkout.UserAccount.ShippingAddress = this.ShippingAddress;
            this.checkout.PaymentSettingID = uxPayment.PaymentSettingID;
            this.checkout.UpdateUserInfo = false;
            this.checkout.PurchaseOrderNumber = uxPayment.PurchaseOrderNumber;
            this.checkout.AdditionalInstructions = uxPayment.AdditionalInstructions;

        }

        /// <summary>
        /// For Multiple Shipping Address
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void linkShipMultipleAddress_Click(object sender, EventArgs e)
        {
            this.shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
            if (this.UserAccount.AccountID > 0)
            {
                uxCartItems.UserAccount = this.UserAccount;
                uxCartItems.BindAddressNames(this.UserAccount);
                UpdatePanelOrderDesk.Update();
                mdlCartItemsPopup.Show();
            }
        }

        /// <summary>
        /// Method for Change Shipping Address
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void linkChangeShipAddress_Click(object sender, EventArgs e)
        {
            int addressId = this.ShippingAddressID;
            if (this.UserAccount.AccountID > 0)
            {
                uxCreateUser.UserAccount = this.UserAccount;
				uxCreateUser.IsNewCustomer = false;
				uxCreateUser.IsShipping = true;
                uxCreateUser.BindAddressList();
                uxCreateUser.BillingAddressID = this.BillingAddressID;
                uxCreateUser.ShippingAddressID = this.ShippingAddressID;
                uxCreateUser.AddressId = addressId;
                uxCreateUser.Bind();
                UpdatePanelOrderDesk.Update();
                mdlCreateUserPopup.Show();
            }
        }



        #endregion

        #region Private Methods



        /// <summary>
        /// Bind Portals
        /// </summary>
        private void BindPortal()
        {
            pnlSelectCustomer.Visible = true;
            pnlCart.Visible = true;

            int portalId = UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault();

            if (ZNodeConfigManager.SiteConfig.PortalID != portalId)
            {
                ZNode.Libraries.Framework.Business.ZNodeConfigManager.AliasSiteConfig(portalId);
            }

            CatalogHelper catalogHelper = new CatalogHelper();
            DataSet ds = catalogHelper.GetCatalogsByPortalId(portalId);

            if (ds != null)
            {
                uxQuickOrder.CatalogId = Convert.ToInt32(ds.Tables[0].Rows[0]["CatalogID"]);
            }
            else
            {
                lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCreateOrderPermission").ToString();
            }
        }

        /// <summary>
        /// Bind Customer section and Shopping cart 
        /// </summary>
        private void Bind()
        {
            if (this.UserAccount.AccountID > 0)
            {
                ui_lblProfile.Text = this.ProfileName;
                ui_lblUserID.Text = this.LoginName;
                ui_BillingAddress.Text = string.Empty;
                ui_ShippingAddress.Text = string.Empty;

                try
                {
					uxShoppingCart.UserAccount = this.UserAccount;
                    uxPayment.UserAccount = this.UserAccount;
                    uxPayment.BindPaymentTypeData();
                    ui_pnlAcctInfo.Visible = true;
                    ui_BillingAddress.Text = this.BillingAddress.ToString();
                    ui_ShippingAddress.Text = this.ShippingAddress.ToString();

                    if (ZNodeShoppingCart.CurrentShoppingCart().IsMultipleShipToAddress)
                    {
                        lnkViewShip.Visible = true;
                        ui_ShippingAddress.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextShipMultipleAddress").ToString();
                    }
                    else
                    {
						lnkViewShip.Visible = false;
                        ui_ShippingAddress.Text = this.ShippingAddress.ToString();

                        if (uxCartItems.GetBillingAddress() != null)
                        {
                            ui_BillingAddress.Text = uxCartItems.GetBillingAddress().ToString();
                        }
                    }
                    // Hide the edit button if the seleted Customer is Admin user
                    ZNodeUserAccount _account = this.UserAccount;
                    MembershipUser _user = Membership.GetUser(_account.UserID.Value);

                    string roleList = string.Empty;

                    // Get roles for this User account
                    string[] roles = Roles.GetRolesForUser(_user.UserName);

                    foreach (string Role in roles)
                    {
                        roleList += Role + "<br>";
                    }

                    string rolename = roleList;

                    // Hide the Delete button if a NonAdmin user has entered this page
                    if (!Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "ADMIN"))
                    {
                        if (Roles.IsUserInRole(_user.UserName, "ADMIN"))
                        {
                            // ui_Edit.Visible = false;
                        }
                        else if (Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "CUSTOMER SERVICE REP"))
                        {
                            if (rolename == Convert.ToString("USER<br>") || rolename == Convert.ToString(string.Empty))
                            {
                                // ui_Edit.Visible = true;
                            }
                            else
                            {
                                //  ui_Edit.Visible = false;
                            }
                        }
                    }
                }
                catch
                {
                }
            }
            else
            {
                ui_pnlAcctInfo.Visible = false;
                ui_lblProfile.Text = string.Empty;
                ui_lblUserID.Text = string.Empty;
                ui_BillingAddress.Text = string.Empty;
                ui_ShippingAddress.Text = string.Empty;
            }
        }

        /// <summary>
        /// Calculate taxes
        /// </summary>
        /// <param name="shoppingCart">The Shopping cart object</param>
        private void CalculateTaxes(ZNodeShoppingCart shoppingCart)
        {
            if (this.UserAccount.AccountID > 0)
            {
                shoppingCart.Payment.BillingAddress = this.BillingAddress;
	            shoppingCart.Payment.ShippingAddress = this.ShippingAddress;
                shoppingCart.Calculate();
            }
        }

        /// <summary>
        /// Submit Order Method
        /// </summary>
        /// <param name="order">The Instance of Order</param>
        /// <param name="shoppingCart">The Instance of Shopping Cart</param>
        private void OnSubmitOrder(ZNodeOrderFulfillment order, ZNodeShoppingCart shoppingCart)
        {
            // Set Digital Asset
            int Counter = 0;
            DigitalAssetService digitalAssetService = new DigitalAssetService();

            // Loop through the Order Line Items
            foreach (OrderLineItem orderLineItem in order.OrderLineItems)
            {
                var shoppingCartItem = this.shoppingCart.PortalCarts.FirstOrDefault().AddressCarts
                                .SelectMany(x => x.ShoppingCartItems.Cast<ZNodeShoppingCartItem>()).ElementAt(Counter++);

                // Set quantity ordered
                int qty = shoppingCartItem.Quantity;

                // Set product id
                int productId = shoppingCartItem.Product.ProductID;

                ZNodeGenericCollection<ZNodeDigitalAsset> AssignedDigitalAssets = new ZNodeGenericCollection<ZNodeDigitalAsset>();

                // Get Digital assets for productid and quantity
                AssignedDigitalAssets = ZNodeDigitalAssetList.CreateByProductIdAndQuantity(productId, qty).DigitalAssetCollection;

                // Loop through the digital asset retrieved for this product
                foreach (ZNodeDigitalAsset digitalAsset in AssignedDigitalAssets)
                {
                    DigitalAsset entity = digitalAssetService.GetByDigitalAssetID(digitalAsset.DigitalAssetID);

                    // Set OrderLineitemId property
                    entity.OrderLineItemID = orderLineItem.OrderLineItemID;

                    // Update digital asset to the database
                    digitalAssetService.Update(entity);
                }

                // Set retrieved digital asset collection to shopping product object
                // If product has digital assets, it will display it on the receipt page along with the product name
                shoppingCartItem.Product.ZNodeDigitalAssetCollection = AssignedDigitalAssets;
            }

            // Invoke any supplier web services
            var supplierWebService = new ZnodeSupplierWebServiceManager(order, shoppingCart);
            supplierWebService.InvokeWebService();

            // Send email receipts to the suppliers
            var supplierEmail = new ZnodeSupplierEmailManager(order, shoppingCart);
            supplierEmail.SendEmailReceipt();

            shoppingCart.PostSubmitOrderProcess();
        }

        /// <summary>
        /// Reset Profile Cache
        /// </summary>
        private void ResetProfileCache()
        {
            // Reset current cached profile cache object with logged in user profile.
            if (System.Web.HttpContext.Current.Session["ProfileCache"] != null)
            {
                ZNode.Libraries.DataAccess.Entities.Profile _profile = (ZNode.Libraries.DataAccess.Entities.Profile)System.Web.HttpContext.Current.Session["ProfileCache"];

                ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();

                _userAccount.ProfileID = ZNodeProfile.CurrentUserProfileId;

                if (_profile.ProfileID != _userAccount.ProfileID)
                {
                    ZNode.Libraries.DataAccess.Service.ProfileService profileService = new ZNode.Libraries.DataAccess.Service.ProfileService();
                    ZNode.Libraries.DataAccess.Entities.Profile profile = profileService.GetByProfileID(_userAccount.ProfileID);

                    System.Web.HttpContext.Current.Session["ProfileCache"] = profile;
                }
            }
        }

		/// <summary>
		/// Method for Clearing the Cart Items and Customer Details in the Order page.
		/// </summary>
		private void RefreshOrderPage()
		{
			this.shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

			if (this.shoppingCart != null)
			{
				this.shoppingCart.EmptyCart();
				this.shoppingCart = null;
				Session.Remove(ZNodeSessionKeyType.ShoppingCart.ToString());
			}
			if (this.UserAccount != null)
			{
				this.UserAccount = null;
				Session.Remove(ZNodeSessionKeyType.UserAccount.ToString());
			}

			this.ResetProfileCache();
			uxPayment.ShowPaymentSection = false;
			ui_pnlAcctInfo.Visible = false;
		}

        #endregion


    }
}