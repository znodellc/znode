<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.CreateOrder.QuickOrder" Codebehind="QuickOrder.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>

<div>
    <div class="LeftFloat" style="width: 30%; text-align: left">
        <h1>
            <asp:Localize ID="SearchProducts" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchProducts%>'></asp:Localize></h1>
    </div>
    <div style="float: right">
        <asp:LinkButton ID="btnClose" CausesValidation="false" runat="server" OnClick="BtnClose_Click"><asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ButtonClose %>'></asp:Localize>
</asp:LinkButton>
    </div>
    <div class="ClearBoth">
    </div>
    <asp:HiddenField ID="PageIndex" Value="1" runat="server" />
    <asp:HiddenField ID="TotalPages" Value="0" runat="server" />
    <!-- Product Search -->
    <asp:Panel ID="pnlSearchParams" runat="server" DefaultButton="btnSearch">
        <div class="SearchForm">
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle" style="width: 200px"> <asp:Localize ID="ColumnTitlePartialProductName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePartialProductName%>'></asp:Localize></span><br />
                    <span class="ValueStyle" style="width: 200px">
                        <asp:TextBox ID="txtProductName" runat="server"></asp:TextBox></span>
                </div>
                <div class="ItemStyle">
                    <span class="FieldStyle" style="width: 150px"><asp:Localize ID="ColumnTitlePartialItem" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePartialItem%>'></asp:Localize></span><br />
                    <span class="ValueStyle" style="width: 150px">
                        <asp:TextBox ID="txtProductNum" runat="server" Width="80px"></asp:TextBox></span>
                </div>
                <div class="ItemStyle">
                    <span class="FieldStyle" style="width: 150px"><asp:Localize ID="ColumnTitlePartialSKU" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePartialSKU%>'></asp:Localize></span><br />
                    <span class="ValueStyle" style="width: 150px">
                        <asp:TextBox ID="txtProductSku" runat="server" Width="80px"></asp:TextBox></span>
                </div>
                <div class="ItemStyle">
                    <span class="FieldStyle" style="width: 150px"><asp:Localize ID="ColumnTitlePartialBrand" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePartialBrand%>'></asp:Localize></span><br />
                    <span class="ValueStyle" style="width: 150px">
                        <asp:TextBox ID="txtBrand" runat="server" Width="100px"></asp:TextBox></span>
                </div>
                <div class="ItemStyle">
                    <span class="FieldStyle" style="width: 100px"><asp:Localize ID="ColumnTitlePartialCategory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePartialCategory%>'></asp:Localize></span><br />
                    <span class="ValueStyle" style="width: 100px">
                        <asp:TextBox ID="txtCategory" runat="server" Width="100px"></asp:TextBox></span>
                </div>
            </div>
            <div class="RowStyle">
                <div>
                    <ZNode:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="1" runat="server"></ZNode:Spacer>
                </div>
            </div>
            <div class="RowStyle">
                <div class="ButtonStyle">
                     <zn:Button runat="server" ButtonType="SubmitButton" OnClick="Search_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch%>' ID="btnSearch" />

                </div>
            </div>
            <div class="RowStyle">
                <div>
                    <ZNode:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="1" runat="server"></ZNode:Spacer>
                </div>
            </div>
            <div class="RowStyle">
                <div>
                    <asp:Label CssClass="Error" ID="lblSearhError" runat="server" EnableViewState="false" />
                </div>
            </div>
        </div>
    </asp:Panel>
    <!-- Product List -->
    <asp:Panel ID="pnlProductList" runat="server" Visible="false">
        <div>
            <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="1" runat="server"></ZNode:Spacer>
        </div>
        <div class="HintText">
           <asp:Localize ID="SubTextSelectProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextSelectProduct%>'></asp:Localize></div>
        <asp:GridView ID="uxGrid" PageSize="10" CellSpacing="0" CellPadding="6" CssClass="SearchGrid"
            runat="server" AutoGenerateColumns="False" GridLines="None" OnSelectedIndexChanged="UxGrid_SelectedIndexChanged">
            <Columns>
                <asp:CommandField CausesValidation="false" ShowSelectButton="true" ValidationGroup="grpCartItems"
                    ButtonType="Link" SelectText='<%$ Resources:ZnodeAdminResource, ColumnTitleSelect%>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID%>' Visible="false" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="ui_CustomerIdSelect" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"ProductId") %>'></asp:Label></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName%>' HtmlEncode="false" DataField="Name" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProductsNumber%>' DataField="ProductNum" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnShortDescription%>' HtmlEncode="false" DataField="ShortDescription" HeaderStyle-HorizontalAlign="Left" />
               <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProductPrice%>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblPrice" runat="server" Text='<%# GetPrice(DataBinder.Eval(Container.DataItem,"RetailPrice"),DataBinder.Eval(Container.DataItem,"SalePrice"),DataBinder.Eval(Container.DataItem,"NegotiatedPrice")) %>'></asp:Label></a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerStyle CssClass="PagerStyle" />
        </asp:GridView>
        <div class="PagingField">
            <asp:LinkButton ID="FirstPageLink" Text="&laquo; First" OnClick="MoveToFirstPage"
                runat="server"></asp:LinkButton>
            <asp:LinkButton ID="PreviousPageLink" Text="&laquo; Prev" runat="server" OnClick="PrevRecord"></asp:LinkButton>
            &nbsp;| Page
            <%= PageIndex.Value + " of " + TotalPages.Value %>
            |&nbsp;
            <asp:LinkButton ID="NextPageLink" Text='<%$ Resources:ZnodeAdminResource, ButtonNext%>' OnClick="NextRecord" runat="server"></asp:LinkButton>
            <asp:LinkButton ID="LastPageLink" Text='<%$ Resources:ZnodeAdminResource, ButtonLast%>' OnClick="MoveToLastPage" runat="server"></asp:LinkButton>
        </div>
    </asp:Panel>
    <!-- Product Detail section -->
    <asp:Panel ID="pnlProductDetails" runat="server" Visible="false">
        <div id="Table1" runat="server">
            <div class="Grid">
                <div class="HeaderStyle">
                    <span style="width: 200px; display:inline-block;"> <asp:Localize ID="ColumnQuantity" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleQty%>'></asp:Localize></span>
                    <span style="width: 200px; display:inline-block;"><asp:Localize ID="ColumnOptions" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOptions%>'></asp:Localize> </span>
                    <span style="width: 200px; display:inline-block;"><asp:Localize ID="TitleUnitPrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleUnitPrice%>'></asp:Localize></span>
                    <span style="width: 200px; display:inline-block;"><asp:Localize ID="TitleTotalPrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTotalPrice%>'></asp:Localize> </span>
                </div>
                <div class="RowStyle" style="border-bottom:solid 1px #5d7b9d;" valign="top">
                    <span style="width: 200px; display: inline-block; margin: 0px; padding: 5px 0 10px 5px;">
                        <asp:DropDownList ID="Qty" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Qty_SelectedIndexChanged" CausesValidation="true" ValidationGroup="grpCartItems">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ValidationGroup="grpCartItems" ID="req1" ControlToValidate="Qty"
                            CssClass="Error" ErrorMessage="*required" runat="server" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ValidationGroup="grpCartItems" ValidationExpression="^\d+$"
                            ID="val1" CssClass="Error" ControlToValidate="Qty" ErrorMessage='<%$ Resources:ZnodeAdminResource, ErrorInvalid%>' runat="server"
                            Display="Dynamic" SetFocusOnError="true"></asp:RegularExpressionValidator>
                        <asp:RangeValidator ValidationGroup="grpCartItems" ID="QuantityRangeValidator" CssClass="Error"
                            MinimumValue="0" ControlToValidate="Qty" ErrorMessage='<%$ Resources:ZnodeAdminResource, ErrorOutofStock%>' MaximumValue="1"
                            Display="Dynamic" SetFocusOnError="true" runat="server" Type="Integer"></asp:RangeValidator>
                    </span><span style="width: 200px; margin: 0px; display: inline-block; padding: 5px 0 0 5px;">
                        <asp:PlaceHolder ID='ControlPlaceHolder' runat="server"></asp:PlaceHolder>
                    </span><span style="width: 200px; margin: 0px; display: inline-block; padding: 5px 0 10px 5px;">
                        <asp:Label ID="lblUnitPrice" runat="server" Text='&nbsp;'></asp:Label>
                    </span><span style="width: 200px; margin: 0px; display: inline-block; padding: 5px 0 10px 5px;">
                        <asp:Label ID="lblTotalPrice" runat="server" Text='&nbsp;'></asp:Label>
                    </span>
                </div>                
            </div>
            <div align="right">
                    <asp:Label ID="uxStatus" CssClass="Error" EnableViewState="false" runat="server"
                        Text=''></asp:Label></div>
                <div>
                    <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="5" runat="server"></ZNode:Spacer>
                </div>
            <div>
                <div id="tablerow" class="OuterBorder" style="border: solid 5px #eff3f6; display: table;"
                    runat="server" visible="false">
                    <div class="LeftFloat" style="width: 25%; padding: 5px; border-right: solid 5px #eff3f6;
                        display: inline-block;">
                        <asp:Image ID="CatalogItemImage" AlternateText="NA" runat="server" />
                    </div>
                    <div class="LeftFloat" style="width: 70%; padding: 5px;">
                        <asp:Label ID="ProductDescription" Text='' runat="server"></asp:Label>
                        <span>
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <div align="right">
                        <div>
                            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="SubmitButton" ValidationGroup="grpCartItems" />
                            <zn:Button runat="server" ButtonType="EditButton" OnClick="BtnSubmitAnother_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddAnotherProduct%>' ID="btnAddProduct" ValidationGroup="grpCartItems" Width="175px" />
                            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="CancelButton" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</div>
