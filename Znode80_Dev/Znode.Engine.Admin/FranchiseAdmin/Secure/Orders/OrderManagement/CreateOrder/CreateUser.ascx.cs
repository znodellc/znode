using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.ShoppingCart;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.CreateOrder
{
    /// <summary>
    /// Represents the FranchiseAdmin - Admin_Secure_Enterprise_OrderDesk_CreateUser user control class
    /// </summary>
    public partial class CreateUser : System.Web.UI.UserControl
    {
        #region Private Variables
        private Address _BillingAddress = new Address();
        private Address _ShippingAddress = new Address();
        private static int _addressId;
        private int currentPortalID = 0;
        #endregion

        #region Public Events
        public event System.EventHandler ButtonClick;
        #endregion

        #region Public Properties
		/// <summary>
		/// Gets or sets the New Customer 
		/// </summary>
		public bool IsNewCustomer
		{
			get
			{
				if (ViewState["IsNewCustomer"] != null)
				{
					return (bool)ViewState["IsNewCustomer"];
				}

				return false;
			}

			set { ViewState["IsNewCustomer"] = value; }

		}

		/// <summary>
		/// Gets or sets the IsShipping change clicked or not.
		/// </summary>
		public bool IsShipping
		{
			get
			{
				if (ViewState["IsShipping"] != null)
				{
					return (bool)ViewState["IsShipping"];
				}

				return false;
			}

			set { ViewState["IsShipping"] = value; }

		}

        /// <summary>
        /// Gets or sets the User Account
        /// </summary>
        public ZNodeUserAccount UserAccount
        {
            get
            {
                if (ViewState["AccountObject"] != null)
                {
                    // The account info with 'AccountObject' is retrieved from the cache and
                    // It is converted to a Entity Account object
					return (ZNodeUserAccount)ViewState["AccountObject"];
                }

				return new ZNodeUserAccount();
            }
            
            set
            {
                // Customer account object is placed in the cache and assigned a key, AccountObject.            
                ViewState["AccountObject"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the billing address
        /// </summary>
        public int BillingAddressID
        {
            get
            {
                if (ddlBillingAddressName.SelectedItem != null)
                {
                    return Convert.ToInt32(ddlBillingAddressName.SelectedItem.Value);
                }

                return 0;
            }

            set
            {
                ddlBillingAddressName.SelectedIndex = ddlBillingAddressName.Items.IndexOf(ddlBillingAddressName.Items.FindByValue(value.ToString()));
            }
        }

        /// <summary>
        /// Gets or sets the shipping address
        /// </summary>
        public int ShippingAddressID
        {
            get
            {
                int addressId = 0;
                if (ddlShippingAddressName.SelectedItem != null && !chkSameAsBilling.Checked)
                {
                    addressId = Convert.ToInt32(ddlShippingAddressName.SelectedItem.Value);
                }
                else if (ddlBillingAddressName.SelectedItem != null)
                {
                    // If shipping address is same as billing address selected.
                    addressId = Convert.ToInt32(ddlBillingAddressName.SelectedItem.Value);
                }

                return addressId;
            }

            set
            {
                ddlShippingAddressName.SelectedIndex = ddlShippingAddressName.Items.IndexOf(ddlShippingAddressName.Items.FindByValue(value.ToString()));
				chkSameAsBilling.Checked = value == BillingAddressID;
            }
        }

        /// <summary>
        /// Gets or sets the Email
        /// </summary>
        public string Email
        {
            get 
            { 
                return txtEmailAddress.Text; 
            }
            
            set 
            { 
                txtEmailAddress.Text = value; 
            }
        }

        public int AddressId
        {
            get
            {
                return _addressId;
            }

            set
            {
                _addressId = value;
            }
        }
        
        /// <summary>
        /// Gets or sets the accountobject with Billing addresses
        /// </summary>
        public Address BillingAddress
        {
            get
            {
                // Get fields
				if (this._BillingAddress.AddressID > 0 || this.BillingAddressID > 0)
				{
					this._BillingAddress.Name = txtBillingAddressName.Text;
				}
                this._BillingAddress.FirstName = Server.HtmlEncode(txtBillingFirstName.Text);
                this._BillingAddress.LastName = Server.HtmlEncode(txtBillingLastName.Text);
                this._BillingAddress.CompanyName = Server.HtmlEncode(txtBillingCompanyName.Text);
                this._BillingAddress.Street = Server.HtmlEncode(txtBillingStreet1.Text);
                this._BillingAddress.Street1 = Server.HtmlEncode(txtBillingStreet2.Text);
                this._BillingAddress.City = Server.HtmlEncode(txtBillingCity.Text);
                this._BillingAddress.StateCode = Server.HtmlEncode(txtBillingState.Text.ToUpper());
                this._BillingAddress.PostalCode = txtBillingPostalCode.Text;
                this._BillingAddress.CountryCode = lstBillingCountryCode.SelectedValue;
                this._BillingAddress.PhoneNumber = txtBillingPhoneNumber.Text;
                return this._BillingAddress;
            }
            
            set
            {
                this._BillingAddress = value;

                // Set field values
				if (this._ShippingAddress.AddressID > 0 || this.BillingAddressID > 0)
	            {
		            txtBillingAddressName.Text = Server.HtmlDecode(this._BillingAddress.Name);
	           }
	            txtBillingFirstName.Text = Server.HtmlDecode(this._BillingAddress.FirstName);
                txtBillingLastName.Text = Server.HtmlDecode(this._BillingAddress.LastName);
                txtBillingCompanyName.Text = Server.HtmlDecode(this._BillingAddress.CompanyName);
                txtBillingStreet1.Text = Server.HtmlDecode(this._BillingAddress.Street);
                txtBillingStreet2.Text = Server.HtmlDecode(this._BillingAddress.Street1);
                txtBillingCity.Text = Server.HtmlDecode(this._BillingAddress.City);
                txtBillingState.Text = this._BillingAddress.StateCode;
                txtBillingPostalCode.Text = this._BillingAddress.PostalCode;

                ListItem blistItem = lstBillingCountryCode.Items.FindByValue(this._BillingAddress.CountryCode);
                if (blistItem != null)
                {
                    lstBillingCountryCode.SelectedIndex = lstBillingCountryCode.Items.IndexOf(blistItem);
                }
                
                txtBillingPhoneNumber.Text = this._BillingAddress.PhoneNumber;
            }
        }

        /// <summary>
        /// Gets or sets the  accountobject with shipping addresses
        /// </summary>
        public Address ShippingAddress
        {
            get
            {
                if (chkSameAsBilling.Checked)
                {
					if (this._ShippingAddress.AddressID > 0 || this.ShippingAddressID > 0)
					{
						this._ShippingAddress.Name = Server.HtmlEncode(txtShippingAddressName.Text);
					}
                    this._ShippingAddress.FirstName = Server.HtmlEncode(txtBillingFirstName.Text);
                    this._ShippingAddress.LastName = Server.HtmlEncode(txtBillingLastName.Text);
                    this._ShippingAddress.CompanyName = Server.HtmlEncode(txtBillingCompanyName.Text);
                    this._ShippingAddress.Street = Server.HtmlEncode(txtBillingStreet1.Text);
                    this._ShippingAddress.Street1 = Server.HtmlEncode(txtBillingStreet2.Text);
                    this._ShippingAddress.City = Server.HtmlEncode(txtBillingCity.Text);
                    this._ShippingAddress.StateCode = txtBillingState.Text.ToUpper();
                    this._ShippingAddress.PostalCode = txtBillingPostalCode.Text;
                    this._ShippingAddress.CountryCode = lstBillingCountryCode.SelectedValue;
                    this._ShippingAddress.PhoneNumber = txtBillingPhoneNumber.Text;
                }
                else
                {
					if (this._ShippingAddress.AddressID > 0  || this.ShippingAddressID > 0)
					{
						this._ShippingAddress.Name = Server.HtmlEncode(txtShippingAddressName.Text);
					}
                    this._ShippingAddress.FirstName = Server.HtmlEncode(txtShippingFirstName.Text);
                    this._ShippingAddress.LastName = Server.HtmlEncode(txtShippingLastName.Text);
                    this._ShippingAddress.CompanyName = Server.HtmlEncode(txtShippingCompanyName.Text);
                    this._ShippingAddress.Street = Server.HtmlEncode(txtShippingStreet1.Text);
                    this._ShippingAddress.Street1 = Server.HtmlEncode(txtShippingStreet2.Text);
                    this._ShippingAddress.City = Server.HtmlEncode(txtShippingCity.Text);
                    this._ShippingAddress.PostalCode = txtShippingPostalCode.Text;
                    this._ShippingAddress.CountryCode = lstShippingCountryCode.SelectedValue;
                    this._ShippingAddress.StateCode = txtShippingState.Text.ToUpper();
                    this._ShippingAddress.PhoneNumber = txtShippingPhoneNumber.Text;
                }

                return this._ShippingAddress;
            }
            
            set
            {
                this._ShippingAddress = value;

                // Set field values
	            if (this._ShippingAddress.AddressID > 0 || this.ShippingAddressID > 0)
	            {
		            txtShippingAddressName.Text = Server.HtmlDecode(this._ShippingAddress.Name);
	            }
	            txtShippingFirstName.Text = Server.HtmlDecode(this._ShippingAddress.FirstName);
                txtShippingLastName.Text = Server.HtmlDecode(this._ShippingAddress.LastName);
                txtShippingCompanyName.Text = Server.HtmlDecode(this._ShippingAddress.CompanyName);
                txtShippingStreet1.Text = Server.HtmlDecode(this._ShippingAddress.Street);
                txtShippingStreet2.Text = Server.HtmlDecode(this._ShippingAddress.Street1);
                txtShippingCity.Text = Server.HtmlDecode(this._ShippingAddress.City);
                txtShippingState.Text = this._ShippingAddress.StateCode;
                txtShippingPostalCode.Text = this._ShippingAddress.PostalCode;

                ListItem slistItem = lstShippingCountryCode.Items.FindByValue(this._ShippingAddress.CountryCode);
                if (slistItem != null)
                {
                    lstShippingCountryCode.SelectedIndex = lstShippingCountryCode.Items.IndexOf(slistItem);
                }
                
                txtShippingPhoneNumber.Text = this._ShippingAddress.PhoneNumber;
            }
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Binds country drop-down list
        /// </summary>
        public void BindCountry()
        {
            // PortalCountry
            ZNode.Libraries.DataAccess.Custom.PortalCountryHelper portalCountryHelper = new ZNode.Libraries.DataAccess.Custom.PortalCountryHelper();
            DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(this.currentPortalID);

            DataView BillingView = new DataView(countryDs.Tables[0]);
            BillingView.RowFilter = "BillingActive = 1";

            DataView ShippingView = new DataView(countryDs.Tables[0]);
            ShippingView.RowFilter = "ShippingActive = 1";

            lstBillingCountryCode.DataSource = BillingView;
            lstBillingCountryCode.DataTextField = "Name";
            lstBillingCountryCode.DataValueField = "Code";
            lstBillingCountryCode.DataBind();
            ListItem blistItem = lstBillingCountryCode.Items.FindByValue("US");
            if (blistItem != null)
            {
                lstBillingCountryCode.SelectedIndex = lstBillingCountryCode.Items.IndexOf(blistItem);
            }

            lstShippingCountryCode.DataSource = ShippingView;
            lstShippingCountryCode.DataTextField = "Name";
            lstShippingCountryCode.DataValueField = "Code";
            lstShippingCountryCode.DataBind();
            ListItem slistItem = lstShippingCountryCode.Items.FindByValue("US");
            if (slistItem != null)
            {
                lstShippingCountryCode.SelectedIndex = lstShippingCountryCode.Items.IndexOf(slistItem);
            }

            pnlCustomerDetail.Update();
        }

		public void BindAddressList()
		{
			AddressService addressService = new AddressService();
			TList<Address> addressList = addressService.GetByAccountID(this.UserAccount.AccountID);

			ddlShippingAddressName.DataTextField = "Name";
			ddlShippingAddressName.DataValueField = "AddressID";
			ddlShippingAddressName.DataSource = addressList;
			ddlShippingAddressName.DataBind();

			ddlBillingAddressName.DataTextField = "Name";
			ddlBillingAddressName.DataValueField = "AddressID";
			ddlBillingAddressName.DataSource = addressList;
			ddlBillingAddressName.DataBind();
		}


        public void Bind()
        {
            if (IsNewCustomer == false)
            {
                if (this.UserAccount.AccountID > 0)
                {

                    // Load shipping address names.
                    this.BindAddressNames(false);

                    // Load billing address names
                    this.BindAddressNames(true);


                    AccountAdmin accountAdmin = new AccountAdmin();
                    Account account = accountAdmin.GetByAccountID(this.UserAccount.AccountID);
                    if (account != null)
                    {
                        this.Email = account.Email;
                        this.UserAccount.EmailID = this.Email;
                    }
					if (this.BillingAddressID == this.ShippingAddressID)
					{
						if (IsShipping)
						{
							chkSameAsBilling.Checked = false;
							chkSameAsBilling.Visible = false;
							pnlBillingAddress.Visible = false;
							pnlShipping.Visible = true;
						}
						else
						{
							chkSameAsBilling.Checked = true;
							chkSameAsBilling.Visible = true;
							pnlBillingAddress.Visible = true;
							pnlShipping.Visible = false;
						}
					}
					else if (this.BillingAddressID == this.AddressId)
					{
						pnlBillingAddress.Visible = true;
						pnlShipping.Visible = false;
						chkSameAsBilling.Visible = true;
					}
					else if (this.ShippingAddressID == this.AddressId)
					{
						pnlBillingAddress.Visible = false;
						pnlShipping.Visible = true;
						chkSameAsBilling.Visible = false;
					}
					else
					{
						pnlShipping.Visible = true;
						pnlBillingAddress.Visible = true;
						chkSameAsBilling.Visible = true;
						chkSameAsBilling.Checked = false;
					}

                    btnUpdate.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonUpdate").ToString();
                }
            }
            else
            {
				pnlShipping.Visible = true;
				pnlBillingAddress.Visible = true;
				chkSameAsBilling.Checked = false;
                // For anonymous user hide the address name
                divBillingAddressName.Visible = false;
                divShippingAddressName.Visible = false;
                this.Email = string.Empty;
                this._BillingAddress = new Address();
                this._ShippingAddress = new Address();
                btnUpdate.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonCreateAnAccount").ToString();
            }

            // Get user's address
            this.BillingAddress = this._BillingAddress;
            this.ShippingAddress = this._ShippingAddress;
        }

        /// <summary>
        /// Load the selected address
        /// </summary>
        /// <param name="addressId">Address Id</param>
        /// <param name="isBillingAddress">IsBilling address (true/false)</param>
        public void LoadAddress(int addressId, bool isBillingAddress)
        {
            AddressService addressService = new AddressService();
            Address address = addressService.GetByAddressID(addressId);
            if (address != null)
            {
                if (isBillingAddress)
                {
                    this._BillingAddress = address;
					txtBillingAddressName.Text = address.Name;
                    txtBillingFirstName.Text = address.FirstName;
                    txtBillingLastName.Text = address.LastName;
                    txtBillingCompanyName.Text = address.CompanyName;
                    txtBillingStreet1.Text = address.Street;
                    txtBillingStreet2.Text = address.Street1;
                    txtBillingCity.Text = address.City;
                    txtBillingState.Text = address.StateCode;
                    txtBillingPostalCode.Text = address.PostalCode;

                    if (address.CountryCode != null && address.CountryCode.Length > 0)
                    {
                        lstBillingCountryCode.SelectedValue = address.CountryCode;
                    }
                    
                    txtBillingPhoneNumber.Text = address.PhoneNumber;

                    this.BillingAddress = address;
                    this.BillingAddressID = address.AddressID;
                }
                else
                {
                    this._ShippingAddress = address;
					txtShippingAddressName.Text = address.Name;
                    txtShippingFirstName.Text = address.FirstName;
                    txtShippingLastName.Text = address.LastName;
                    txtShippingCompanyName.Text = address.CompanyName;
                    txtShippingStreet1.Text = address.Street;
                    txtShippingStreet2.Text = address.Street1;
                    txtShippingCity.Text = address.City;
                    txtShippingState.Text = address.StateCode;
                    txtShippingPostalCode.Text = address.PostalCode;
                    if (address.CountryCode != null && address.CountryCode.Length > 0)
                    {
                        lstShippingCountryCode.SelectedValue = address.CountryCode;
                    }

                    txtShippingPhoneNumber.Text = address.PhoneNumber;

                    this.ShippingAddress = address;
                    this.ShippingAddressID = address.AddressID;
                }
            }
        }
        #endregion

        #region Events

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Set session value for current store portal        
            this.currentPortalID = ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.PortalID;

            if (!Page.IsPostBack)
            {
                this.BindCountry();
            }
        }

        /// <summary>
        /// Address Name Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlAddressName_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = sender as DropDownList;
            int addressId = Convert.ToInt32(ddl.SelectedValue);
            if (addressId > 0)
            {
                if (ddl.ID.ToLower().Contains("billing"))
                {
                    this.LoadAddress(addressId, true);
                }
                else
                {
                    this.LoadAddress(addressId, false);
                }
            }
        }

		/// <summary>
		///  Validate whether shipping option is available to country.
		/// </summary>
		public bool ValidateCountry()
		{
			bool flag = false;
			ZNode.Libraries.DataAccess.Custom.PortalCountryHelper portalCountryHelper =
				new ZNode.Libraries.DataAccess.Custom.PortalCountryHelper();
			DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

			DataTable BillingView = countryDs.Tables[0];
			DataRow[] drBillingView = BillingView.Select("CountryCode='" + this.lstBillingCountryCode.Text + "'");
			foreach (DataRow drView in drBillingView)
			{
				if (drView["ShippingActive"].ToString() != "True")
				{
					flag = false;
				}
				else
				{
					flag = true;
				}
			}
			return flag;
		}
        /// <summary>
        /// Event is raised when the "Update" Button is clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnUpdate_Click(object sender, EventArgs e)
        {
			if (IsNewCustomer)
			{
				this.UserAccount = new ZNodeUserAccount();
			}
            if ((lstBillingCountryCode.SelectedValue == "US") && (!string.IsNullOrEmpty(txtBillingState.Text.Trim())))
            {
                if (txtBillingState.Text.Trim().Length > 2 || txtBillingState.Text.Trim().Length < 2)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorValidStateCode").ToString();
                    return;
                }
            }
            
            // Validate shipping state code
            if ((lstShippingCountryCode.SelectedValue == "US") && (!string.IsNullOrEmpty(txtShippingState.Text.Trim())))
            {
                if (txtShippingState.Text.Trim().Length > 2 || txtShippingState.Text.Trim().Length < 2)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorValidStateCode").ToString();
                    return;
                }
            }


			if (chkSameAsBilling.Checked)
			{
				if (!ValidateCountry())
				{
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCountry").ToString();
					return;
				}
			}
            AccountService accountService = new AccountService();
            ZNodeUserAccount _account = this.UserAccount;
            ZNodeUserAccount _userAccount = new ZNodeUserAccount();

            if (_account.AccountID == 0)
            {
                ProfileAdmin prfAdmin = new ProfileAdmin();
                Profile profile = prfAdmin.GetDefaultProfileByPortalID(this.currentPortalID);
                int profileId = 0;

                if (profile != null)
                {
                    profileId = profile.ProfileID;
                }
                else
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCreateFailed").ToString();
                    return;
                }
                
                _userAccount.EmailID = txtEmailAddress.Text;
                _userAccount.BillingAddress = this.BillingAddress;
                _userAccount.ShippingAddress = this.ShippingAddress;
                _userAccount.AddUserAccount();

                // Get inserted Address
                AddressService addressService = new AddressService();
                if (chkSameAsBilling.Checked)
                {
                    // If both address are same then create single address entity                    
                    this.BillingAddress.Name = "Default Address";
                    this.BillingAddress.IsDefaultBilling = true;
                    this.BillingAddress.IsDefaultShipping = true;
                    this.BillingAddress.AccountID = _userAccount.AccountID;
                    addressService.Insert(this.BillingAddress);

                    // Both address are same
                    this.BillingAddress = _userAccount.BillingAddress;
                    this.ShippingAddress = _userAccount.BillingAddress;

                    _userAccount.SetBillingAddress(this.BillingAddress);
                    _userAccount.SetShippingAddress(this.BillingAddress);
                }
                else
                {
                    // Insert billing address
                    this.BillingAddress.Name = "Default Billing Address";
                    this.BillingAddress.IsDefaultBilling = true;
                    this.BillingAddress.AccountID = _userAccount.AccountID;
                    bool isSuccess = addressService.Insert(this.BillingAddress);
                    _userAccount.SetBillingAddress(this.BillingAddress);

                    // Insert shipping address
                    this.ShippingAddress.Name = "Default Shipping Address";
                    this.ShippingAddress.IsDefaultShipping = true;
                    this.ShippingAddress.AccountID = _userAccount.AccountID;
                    isSuccess = addressService.Insert(this.ShippingAddress);
                    _userAccount.SetShippingAddress(this.ShippingAddress);

                    this.BillingAddress = _userAccount.BillingAddress;
                    this.ShippingAddress = _userAccount.ShippingAddress;
                }

                AccountProfile accountProfile = new AccountProfile();
                accountProfile.AccountID = _userAccount.AccountID;
                accountProfile.ProfileID = profileId;

                AccountProfileService serv = new AccountProfileService();
                serv.Insert(accountProfile);

				this.UserAccount = _userAccount;

                // Set the user account object in session to be used for gift card processing.
                HttpContext.Current.Session["AliasUserAccount"] = this.UserAccount;

                ZNode.Libraries.Admin.ProfileAdmin profileAdmin = new ZNode.Libraries.Admin.ProfileAdmin();

				HttpContext.Current.Session["ProfileCache"] = profileAdmin.GetByProfileID(_userAccount.ProfileID);
            }
            else
            {
                Address billingAddress = new Address();
                Address shippingAddress = new Address();

				AddressService addressService = new AddressService();
				if (this.BillingAddressID > 0)
				{
					billingAddress = this.UserAccount.Addresses.Find(x => x.AddressID == this.BillingAddressID);
				}

                // Billing Address
                billingAddress.Name = this.BillingAddress.Name;
                billingAddress.FirstName = this.BillingAddress.FirstName;
                billingAddress.LastName = this.BillingAddress.LastName;
                billingAddress.CompanyName = this.BillingAddress.CompanyName;
                billingAddress.Street = this.BillingAddress.Street;
                billingAddress.Street1 = this.BillingAddress.Street1;
                billingAddress.City = this.BillingAddress.City;
                billingAddress.PostalCode = this.BillingAddress.PostalCode;
                billingAddress.StateCode = this.BillingAddress.StateCode;
                billingAddress.CountryCode = this.BillingAddress.CountryCode;
                billingAddress.PhoneNumber = this.BillingAddress.PhoneNumber;
               
                // Update the account address
                addressService.Update(billingAddress);

                // Shipping Address
				if (this.ShippingAddressID > 0)
				{
					shippingAddress = this.UserAccount.Addresses.Find(x => x.AddressID == this.ShippingAddressID);
				}
                
                // Shipping Address
                shippingAddress.Name = this.ShippingAddress.Name;
                shippingAddress.FirstName = this.ShippingAddress.FirstName;
                shippingAddress.LastName = this.ShippingAddress.LastName;
                shippingAddress.CompanyName = this.ShippingAddress.CompanyName;
                shippingAddress.Street = this.ShippingAddress.Street;
                shippingAddress.Street1 = this.ShippingAddress.Street1;
                shippingAddress.City = this.ShippingAddress.City;
                shippingAddress.PostalCode = this.ShippingAddress.PostalCode;
                shippingAddress.StateCode = this.ShippingAddress.StateCode;
                shippingAddress.CountryCode = this.ShippingAddress.CountryCode;
                shippingAddress.PhoneNumber = this.ShippingAddress.PhoneNumber;

                addressService.Update(shippingAddress);

                // Set account object to cache object
                this.UserAccount = _account;

				this.UserAccount.SetBillingAddress(
				  UserAccount.Addresses.Find(x => x.AddressID == UserAccount.BillingAddress.AddressID));

				this.UserAccount.SetShippingAddress(
					UserAccount.Addresses.Find(x => x.AddressID == UserAccount.ShippingAddress.AddressID));

				var cart = ZNodeShoppingCart.CurrentShoppingCart();

				if (cart != null)
				{
					cart.PortalCarts.ForEach(x =>
					{
						if (x.Payment != null)
						{
							x.Payment.BillingAddress = this.UserAccount.BillingAddress;
							x.Payment.ShippingAddress = this.UserAccount.ShippingAddress;
							//Product Adddress to ship Property
						}
						x.AddressCarts.ForEach(y =>
						{
							if (x.Payment != null)
							{
								y.Payment.ShippingAddress = this.UserAccount.ShippingAddress;
								y.Payment.BillingAddress = this.UserAccount.BillingAddress;

								y.ShoppingCartItems.Cast<ZNodeShoppingCartItem>()
								 .ToList()
								 .ForEach(
									 z =>
									 z.Product.AddressToShip =
									 this.UserAccount.Addresses.FirstOrDefault(v => v.AddressID == y.AddressID));
							}
						});
					});
				}

                _account.EmailID = txtEmailAddress.Text;

                if (_account.AccountID > 0)
                {
                    _account.UpdateUserAccount();
                }

                if (_account.UserID.HasValue)
                {
                    MembershipUser user = Membership.GetUser(_account.UserID.Value);

                    if (user != null)
                    {
                        user.Email = _account.EmailID;
                        Membership.UpdateUser(user);
                    }
                }

				// Set the user account object in session to be used for gift card processing.
				HttpContext.Current.Session["AliasUserAccount"] = this.UserAccount;
            }

            // Triggers parent control event
            if (this.ButtonClick != null)
            {
                this.ButtonClick(sender, e);
            }
        }

        /// <summary>
        /// Event is raised when the "Cancel" Button is clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            this.IsNewCustomer = false;
			this.Bind();

			if (this.ButtonClick != null)
            {
                // Triggers parent control event
                this.ButtonClick(sender, e);
            }
        }

        /// <summary>
        /// CheckBox checked change event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChkSameAsBilling_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSameAsBilling.Checked)
            {
                this.ShippingAddress = this.BillingAddress;
                pnlShipping.Visible = false;
            }
            else
            {
                ZNodeUserAccount account = this.UserAccount;
                AddressService addressService = new AddressService();
                Address address = null;
                TList<Address> addressList = addressService.GetByAccountID(account.AccountID);

                if (addressList.Count == 1)
                {
                    // Create new shipping address from existing billing address.
                    address = (Address)addressList[0].Clone();
                    address.AddressID = 0;
                    address.Name = "Default Shipping Address";
                    address.IsDefaultBilling = false;
                    address.IsDefaultShipping = true;
                    addressService.Insert(address);

                    // Clear previous default shipping address.
                    foreach (Address currentItem in addressList)
                    {
                        currentItem.IsDefaultShipping = false;
                    }

                    addressService.Update(addressList);

                    this.BindAddressNames(false);
                }
                else
                {
                    // Load user's default shipping address from account.
                    AccountAdmin accountAdmin = new AccountAdmin();
                    address = accountAdmin.GetDefaultShippingAddress(this.UserAccount.AccountID);
                }

                if (address != null)
                {
                    this.LoadAddress(address.AddressID, false);
                }

                pnlShipping.Visible = true;
            }

            // Update panel
            pnlCustomerDetail.Update();
        }
        #endregion

        #region Private Methods
        
        /// <summary>
        /// Bind address names.
        /// </summary>
        /// <param name="isBillingAddress">Bool value whether isBillingAddress</param>
        private void BindAddressNames(bool isBillingAddress)
        {
            AddressService addressService = new AddressService();
            TList<Address> addressList = addressService.GetByAccountID(this.UserAccount.AccountID);
            int defaultAddressId = 0;
            if (isBillingAddress)
            {
                // If account has no billing address then hide the billing address name
                divBillingAddressName.Visible = addressList.Count == 0 ? false : true;
                foreach (Address currentItem in addressList)
                {
                    if (currentItem.IsDefaultBilling)
                    {
                        currentItem.Name = currentItem.Name + " (default)";
                        if (this.BillingAddressID == 0)
                        {
                            defaultAddressId = currentItem.AddressID;
                        }
                        else
                        {
                            defaultAddressId = this.BillingAddressID;
                        }
                    }
                }

                ddlBillingAddressName.DataTextField = "Name";
                ddlBillingAddressName.DataValueField = "AddressID";
                ddlBillingAddressName.DataSource = addressList;
                ddlBillingAddressName.DataBind();

                this.LoadAddress(defaultAddressId, true);

                if (this.AddressId == 0)
                {
                    ddlBillingAddressName.SelectedIndex =
                        ddlBillingAddressName.Items.IndexOf(ddlBillingAddressName.Items.FindByValue(defaultAddressId.ToString()));
                    this.BillingAddressID = defaultAddressId;
                }
            }
            else
            {
                // If account has no shipping address then hide the shipping address name
                divShippingAddressName.Visible = addressList.Count == 0 ? false : true;
                foreach (Address currentItem in addressList)
                {
                    if (currentItem.IsDefaultShipping)
                    {
                        currentItem.Name = currentItem.Name + " (default)";
                        if (this.ShippingAddressID == 0)
                        {
                            defaultAddressId = currentItem.AddressID;
                        }
                        else
                        {
                            defaultAddressId = this.ShippingAddressID;
                        }
                    }
                }

                ddlShippingAddressName.DataTextField = "Name";
                ddlShippingAddressName.DataValueField = "AddressID";
                ddlShippingAddressName.DataSource = addressList;
                ddlShippingAddressName.DataBind();

                this.LoadAddress(defaultAddressId, false);

                if (this.AddressId == 0)
                {
                    ddlShippingAddressName.SelectedIndex =
                        ddlShippingAddressName.Items.IndexOf(ddlShippingAddressName.Items.FindByValue(defaultAddressId.ToString()));
                    this.ShippingAddressID = defaultAddressId;
                }
            }
        }
        #endregion
    }
}