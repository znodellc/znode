﻿using System;
using System.Collections;
using System.Linq;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.CreateOrder
{
    public partial class MultipleAddressCart : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeShoppingCart _ShoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);        
        #endregion

        #region Public Events
        public event System.EventHandler CartItem_RemoveLinkClicked;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the User Account
        /// </summary>
        public ZNodeUserAccount UserAccount
        {
            get
            {
                if (ViewState["AccountObject"] != null)
                {
                    // The account info with 'AccountObject' is retrieved from the cache and
                    // It is converted to a Entity Account object
					return (ZNodeUserAccount)ViewState["AccountObject"];
                }

				return new ZNodeUserAccount();
            }

            set
            {
                // Customer account object is placed in the cache and assigned a key, AccountObject.            
                ViewState["AccountObject"] = value;
            }
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
	        lblError.Text = string.Empty;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns boolean value to enable Validator to check against the Quantity textbox
        /// It checks for AllowBackOrder and TrackInventory settings for this product,and returns the value
        /// </summary>
        /// <param name="value">The value to enable Stock Validator or not</param>
        /// <returns>Returns a bool value to enable Stock validator or not</returns>
        public bool EnableStockValidator(string value)
        {
            // Get Shopping Cart Item
            ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(value);

            bool enablevalidator = true;
            bool enableaddonvalidator = false;
            if (item != null)
            {
                // Allow Back Order
                if (item.Product.AllowBackOrder && item.Product.TrackInventoryInd)
                {
                    enablevalidator = false;
                }
                else if (item.Product.AllowBackOrder == false && item.Product.TrackInventoryInd == false)
                {
                    // Don't track inventory
                    enablevalidator = false;
                }

                if (item.Product.SelectedAddOns.AddOnCollection.Count > 0)
                {
                    enableaddonvalidator = true;
                    // Loop through the Selected addons for each Shopping cartItem
                    foreach (
                        ZNode.Libraries.ECommerce.Entities.ZNodeAddOnEntity AddOn in
                            item.Product.SelectedAddOns.AddOnCollection)
                    {
                        // Loop through the Add-on values for each Add-on
                        foreach (
                            ZNode.Libraries.ECommerce.Entities.ZNodeAddOnValueEntity AddOnValue in
                                AddOn.AddOnValueCollection)
                        {
                            // Check for quantity on hand and back-order,track inventory settings
                            if (AddOn.AllowBackOrder && AddOn.TrackInventoryInd)
                            {
                                enableaddonvalidator = false;
                            }
                            else if (AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd == false)
                            {
                                // Don't track inventory
                                enableaddonvalidator = false;
                            }
                        }
                    }
                }
            }

            return enablevalidator || enableaddonvalidator;
        }

        /// <summary>
        /// Returns Quantity on Hand (inventory stock value)
        /// </summary>
        /// <param name="value">The Item value </param>
        /// <returns>Returns the Quantity on Hand</returns>
        public int CheckInventory(string value)
        {
            // Get Shopping Cart Item
            ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(value);

            if (item != null)
            {
                int? quantityAvailable = null;

                if (item.Product.ZNodeBundleProductCollection.Count == 0)
                {
                    quantityAvailable = item.Product.QuantityOnHand;
                }

                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductBaseEntity _bundleProduct in item.Product.ZNodeBundleProductCollection)
                {
                    if (quantityAvailable == null)
                    {
                        quantityAvailable = _bundleProduct.QuantityOnHand;
                    }
                    else if (quantityAvailable > _bundleProduct.QuantityOnHand)
                    {
                        quantityAvailable = _bundleProduct.QuantityOnHand;
                    }
                }

                if (item.Product.SelectedAddOns.SelectedAddOnValueIds.Length > 0 && item.Product.SelectedAddOns.AddOnCollection
                    .Cast<ZNodeAddOn>()
                    .Any(y => y.TrackInventoryInd))
                {

                    // Loop through the Selected addons for each Shopping cartItem
                    foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnEntity AddOn in item.Product.SelectedAddOns.AddOnCollection)
                    {
                        // Loop through the Add-on values for each Add-on
                        foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                        {
                            if (quantityAvailable > AddOnValue.QuantityOnHand)
                                quantityAvailable = AddOnValue.QuantityOnHand;

                            // Check for quantity on hand and back-order,track inventory settings
                            if (AddOnValue.QuantityOnHand <= 0 && AddOn.AllowBackOrder == false && AddOn.TrackInventoryInd)
                            {
                                quantityAvailable = 0;
                            }
                        }
                    }
                }

                // Get Current Qunatity by Subtracting QunatityOrdered from Quantity On Hand(Inventory)
                int CurrentQuantity = quantityAvailable.GetValueOrDefault(0) - this._ShoppingCart.GetQuantityOrdered(item);

                if (CurrentQuantity <= 0)
                {
                    return 0;
                }
                else
                {
                    return CurrentQuantity;
                }
            }
            else
            {
                return 1;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public string GetValue(string value, string FieldName)
        {
            // Get Shopping Cart Item
            ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(value);
            if (item != null)
            {
                switch (FieldName)
                {
                    case "ViewProductLink":
                        return item.Product.ViewProductLink;
                    case "ImageAltTag":
                        return item.Product.ImageAltTag;
                    case "ThumbnailImageFilePath":
                        return item.Product.ThumbnailImageFilePath;
                    case "Name":
                        return item.Product.Name;
                    case "ShoppingCartDescription":
                        return item.Product.ShoppingCartDescription;
                    case "ProductNum":
                        return item.Product.ProductNum;
                }
            }
            return string.Empty;
        }

        /// <summary>
        /// Represents the UpdateQuantity method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
		public bool UpdateOrderShipment()
		{
            lblError.Text = string.Empty;
            foreach (RepeaterItem repeateritem in rptCartItems.Items)
            {
                GridView gvCart = repeateritem.FindControl("Cart") as GridView;

                foreach (GridViewRow row in gvCart.Rows)
                {
                    HiddenField hdnSlNo = row.Cells[0].FindControl("SLNO") as HiddenField;
                    HiddenField hdnGUID = row.Cells[0].FindControl("GUID") as HiddenField;
                    DropDownList ddlQty = row.Cells[0].FindControl("uxQty") as DropDownList;
                    DropDownList ddlAddress = row.Cells[3].FindControl("ddlShippingAddress") as DropDownList;
                    string GUID = hdnGUID.Value;
                    int SelectedQty = Convert.ToInt32(ddlQty.Text);

                    // Get Shopping cart item using GUID value
                    ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(GUID);
                    int Qty = (item.Quantity - 1) + SelectedQty;

                    if (Qty == 0)
                    {
                        this._ShoppingCart.RemoveFromCart(GUID);
                    }
                    else
                    {
                        if (!CheckInventory(item, Qty))
                        {
                            lblError.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateItemText").ToString(), item.Product.Name);
                            return false;
                        }
                    }

                    ZNode.Libraries.ECommerce.Entities.ZNodeOrderShipment ordershipment = new ZNode.Libraries.ECommerce.Entities.ZNodeOrderShipment();
                    ordershipment.SlNo = hdnSlNo.Value;
                    ordershipment.Quantity = SelectedQty;
                    ordershipment.ItemGUID = GUID;
                    ordershipment.AddressID = Convert.ToInt32(ddlAddress.SelectedValue);

					//Validate for maximum quantity reached
					if (!_ShoppingCart.UpdateShipment(ordershipment))
					{
						lblError.ForeColor = System.Drawing.Color.Red;
                        lblError.Text = item.Product.Name + string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateShipment").ToString(), item.Product.Name);
						return false;
					}
					lblError.ForeColor = System.Drawing.Color.Green;
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SuccessUpdateQuantity").ToString();

                }

            }
            return true;
		}

		
        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        public void Bind()
        {
            if (this._ShoppingCart != null)
            {
                rptCartItems.DataSource = this._ShoppingCart.ShoppingCartItems;
                rptCartItems.DataBind();
            }
        }

        #endregion

        #region General Events

        /// <summary>
        /// Event is raised when update button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param> 
		protected void btnUpdate_Click(object sender, EventArgs e)
		{
			UpdateOrderShipment();

			if (this.CartItem_RemoveLinkClicked != null)
			{
				CartItem_RemoveLinkClicked(sender, e);
			}
		}
        /// <summary>
        /// Grid command event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cart_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (this._ShoppingCart != null)
            {
                // The remove link was clicked
                if (e.CommandName.Equals("remove"))
                {
                    lblError.Text = string.Empty;
                    string SlNo = e.CommandArgument.ToString();

                    bool status = this._ShoppingCart.RemoveShipmentItem(SlNo);

                    if (!status)
                        lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductMinimumQuantity").ToString();

                    if (this.CartItem_RemoveLinkClicked != null)
                    {
                        this.CartItem_RemoveLinkClicked(sender, e);
                    }
                }
            }
        }

        /// <summary>
        /// Cart items row databound event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cart_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this._ShoppingCart != null)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Retrieve the dropdownlist control from the first column
                    DropDownList qty = e.Row.Cells[0].FindControl("uxQty") as DropDownList;
                    DropDownList ShippingAddress = e.Row.Cells[3].FindControl("ddlShippingAddress") as DropDownList;
                    BindAddressNames(ShippingAddress);
                }
            }
        }

        /// <summary>
        /// Repeater control Item data bound event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptCartItems_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var cartitem = e.Item.DataItem as ZNodeShoppingCartItem;

                var gvShipment = (GridView)e.Item.FindControl("Cart");
                gvShipment.DataSource = cartitem.OrderShipments;
                gvShipment.DataBind();

                gvShipment.Rows.Cast<GridViewRow>().ToList().ForEach(x => LoadGridViewData(x));

            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Bind address names.
        /// </summary>
        /// <param name="isBillingAddress">isBillingAddress value either true or false</param>
        public void BindAddressNames(DropDownList ddlShippingAddress)
        {
            AddressService addressService = new AddressService();
            TList<Address> addressList = null;
            if (UserAccount.AccountID > 0)
            {
                addressList = addressService.GetByAccountID(UserAccount.AccountID);
                addressList.FindAll(x => x.IsDefaultShipping == true).ForEach(y => y.Name = y.Name + " (default)");
            }
            else
            {
                addressList = DataRepository.AddressProvider.GetByAccountID(UserAccount.AccountID);
                addressList.FindAll(x => x.IsDefaultShipping == true).ForEach(y => y.Name = y.Name + " (default)");
            }
            if (addressList.Count != 0)
            {
                int defaultAddressId = addressList.Find(x => x.IsDefaultShipping == true).AddressID;

                ddlShippingAddress.DataTextField = "Name";
                ddlShippingAddress.DataValueField = "AddressID";
                ddlShippingAddress.DataSource = addressList;
                ddlShippingAddress.DataBind();
                ddlShippingAddress.SelectedIndex = ddlShippingAddress.Items.IndexOf(ddlShippingAddress.Items.FindByValue(defaultAddressId.ToString()));
            }
        }

        /// <summary>
        /// Check inventory for the given shopping cart item with selected quantity
        /// </summary>
        /// <param name="item"></param>
        /// <param name="selectedQuantity"></param>
        /// <returns></returns>
        private bool CheckInventory(ZNodeShoppingCartItem item, int selectedQuantity)
        {
            if (item.Product.QuantityOnHand < selectedQuantity && !item.Product.AllowBackOrder && item.Product.TrackInventoryInd)
            {
                return false;
            }
            foreach (ZNode.Libraries.ECommerce.Entities.ZNodeProductBaseEntity _bundleProduct in item.Product.ZNodeBundleProductCollection)
            {
                if (_bundleProduct.QuantityOnHand < selectedQuantity && !_bundleProduct.AllowBackOrder && _bundleProduct.TrackInventoryInd)
                {
                    return false;
                }
            }
            if (item.Product.SelectedAddOns.SelectedAddOnValueIds.Length > 0 && item.Product.SelectedAddOns.AddOnCollection.Cast<ZNodeAddOn>().Any(y => y.TrackInventoryInd))
            {

                // Loop through the Selected addons for each Shopping cartItem
                foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnEntity AddOn in item.Product.SelectedAddOns.AddOnCollection)
                {
                    // Loop through the Add-on values for each Add-on
                    foreach (ZNode.Libraries.ECommerce.Entities.ZNodeAddOnValueEntity AddOnValue in AddOn.AddOnValueCollection)
                    {
                        // Check for quantity on hand and back-order,track inventory settings
                        if (AddOnValue.QuantityOnHand < selectedQuantity && !AddOn.AllowBackOrder && AddOn.TrackInventoryInd)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Load Cart items gridview control data
        /// </summary>
        /// <param name="row"></param>
        private void LoadGridViewData(GridViewRow row)
        {
            HiddenField hdnGUID = row.FindControl("GUID") as HiddenField;
            string GUID = hdnGUID.Value;

            HiddenField hdnSlNo = row.FindControl("SLNO") as HiddenField;
            string SlNo = hdnSlNo.Value;

            // Get Shopping cart item using GUID value
            ZNodeShoppingCartItem item = this._ShoppingCart.GetItem(GUID);
            DropDownList ddlQty = row.FindControl("uxQty") as DropDownList;
            DropDownList ddlAddress = row.FindControl("ddlShippingAddress") as DropDownList;

            var ordershipment = _ShoppingCart.GetOrderShipment(SlNo);
            if (ordershipment != null && ordershipment.AddressID > 0)
            {
                ddlAddress.SelectedValue = ordershipment.AddressID.ToString();
            }

            int minQty = 1;
            // If Max quantity is not set in admin , set it to 10
            int maxQty = item.Product.MaxQty == 0 ? 10 : item.Product.MaxQty;


            ArrayList quantityList = new ArrayList();

            for (int itemIndex = minQty; itemIndex <= maxQty; itemIndex++)
            {
                quantityList.Add(itemIndex);
            }

            // Bind Quantity drop down list
            ddlQty.DataSource = quantityList;
            ddlQty.DataBind();

            ddlQty.SelectedValue = minQty.ToString();

            if (ordershipment != null && ordershipment.Quantity > 0)
            {
                ddlQty.SelectedValue = ordershipment.Quantity.ToString();
            }
        }
        #endregion

		
    }
}