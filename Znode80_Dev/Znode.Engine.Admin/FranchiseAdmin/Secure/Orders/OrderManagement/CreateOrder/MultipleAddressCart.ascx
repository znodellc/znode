﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MultipleAddressCart.ascx.cs" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.CreateOrder.MultipleAddressCart" %>

<%@ Register Src="~/FranchiseAdmin/Controls/Default/SalesDepartmentPhone.ascx" TagName="SalesDepartmentPhoneNo"
    TagPrefix="ZNode" %>
<%@ Register Src="~/FranchiseAdmin/Controls/Default/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>

<div>
    <asp:UpdatePanel ID="UpdatePnlAddress" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
    <asp:Repeater ID="rptCartItems" runat="server" OnItemDataBound="rptCartItems_ItemDataBound" >
        <HeaderTemplate>
            <div class="MultiShipHeader">
                    <div class="QTY">
                        <asp:Localize ID="ColumnTitleQuantity" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleQuantity%>'></asp:Localize> 
                    </div>
                    <div class="PdtImage">
                        <asp:Localize ID="ColumnTitleProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProduct%>'></asp:Localize>  
                    </div>
                    <div class="Product">
                       &nbsp;
                    </div>
                    <div class="ShipTo"><asp:Localize ID="ColumnShipTo" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShipTo%>'></asp:Localize></div>
            </div>

        </HeaderTemplate>
        <ItemTemplate>
            <asp:GridView ID="Cart" class="TableContainer" runat="server" AutoGenerateColumns="False"
                EmptyDataText="Shopping Cart Empty" GridLines="None" CellPadding="4" OnRowCommand="Cart_RowCommand"
                OnRowDataBound="Cart_RowDataBound" CssClass="Grid" meta:resourcekey="uxCartResource1" ShowHeader="false">
                <Columns>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleQuantity%>' HeaderStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource1" ItemStyle-Width="120px" >
                        <ItemTemplate>
                            <asp:DropDownList ID="uxQty" runat="server" meta:resourcekey="uxQtyResource1">
                            </asp:DropDownList>
                            <asp:RangeValidator ValidationGroup="groupCart" ID="RangeValidator1" MinimumValue="0" ErrorMessage='<%$ Resources:ZnodeAdminResource, ErrorOutofStock%>'
                                ControlToValidate="uxQty" MaximumValue='<%# CheckInventory(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString()) %>'
                                Enabled='<%# EnableStockValidator(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString()) %>'
                                SetFocusOnError="True" runat="server" Type="Integer" Display="Dynamic" meta:resourcekey="RangeValidator1Resource1"></asp:RangeValidator>
                            <asp:HiddenField ID="GUID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "ItemGUID").ToString() %>' />
                            <asp:HiddenField ID="SLNO" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "SlNo").ToString() %>' />
                            
                            <div class="Mylink">
                                <asp:LinkButton ID="ibRemoveLineItem" runat="server" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "SlNo").ToString() %>'
                                    CommandName="remove" Text='<%$ Resources:ZnodeAdminResource, ButtonRemove%>' CssClass="mylink" ToolTip='<%$ Resources:ZnodeAdminResource, TextRemovethisItem%>' CausesValidation="false"  />
                            </div>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left"/>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource2" ItemStyle-Width="100px">
                        <ItemTemplate>
                                <img id="Img1" enableviewstate="false" alt='<%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"ImageAltTag")%>'
                                    border='0' src='<%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"ThumbnailImageFilePath") %>'
                                    runat="server" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle CssClass="QTY" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProduct%>' HeaderStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource3" ItemStyle-Width="320px">
                        <ItemTemplate>
							<div class="ProductName">
                                        <asp:Label ID="lblProductName" runat="server" Text='<%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"Name")%>'></asp:Label>
                                    </div>
                            <div class="Description" enableviewstate="false">
                               <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextItem %>'></asp:Localize>
                        <%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"ProductNum")%><br>
                                <%# GetValue(DataBinder.Eval(Container.DataItem, "ItemGUID").ToString(),"ShoppingCartDescription")%>
                            </div>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleShipTo%>'  HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:DropDownList ID="ddlShippingAddress" runat="server"></asp:DropDownList>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    </asp:TemplateField>

                </Columns>
                <FooterStyle CssClass="Footer" />
                <RowStyle CssClass="Row" />
                <HeaderStyle CssClass="Header" />
                <AlternatingRowStyle CssClass="AlternatingRow" />
            </asp:GridView>
        </ItemTemplate>
    </asp:Repeater>
    <uc1:Spacer ID="Spacer1" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
    <div class="Error">
        <asp:Label ID="lblError" runat="server"></asp:Label>
    </div>
    <div class="ImageButtons UpdatedQuantity">
        <asp:Localize ID="ColumnTextChangedQuantities" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextChangedQuantities%>'></asp:Localize>
        <zn:Button runat="server" ButtonType="SubmitButton" OnClick="btnUpdate_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonUpdate%>' ID="btnUpdate" CausesValidation="false" />
    </div>
              </ContentTemplate>
    </asp:UpdatePanel>
</div>
