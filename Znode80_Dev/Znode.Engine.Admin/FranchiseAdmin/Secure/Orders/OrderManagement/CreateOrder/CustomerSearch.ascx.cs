using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.CreateOrder
{
    /// <summary>
    /// Represents the FranchiseAdmin - Customersearch user control class
    /// </summary>
    public partial class CustomerSearch : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeAddress _billingAddress = new ZNodeAddress();
        private ZNodeAddress _shippingAddress = new ZNodeAddress();
        private string _profileName = string.Empty;
        private string _loginName = string.Empty;
        private string PortalIds = string.Empty;
        private int OrderId = 0;
        private int CurrentPortalID = 0;
        #endregion

        #region Public Events
        
        public event System.EventHandler SelectedIndexChanged;
        
        public event System.EventHandler SearchButtonClick;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the Customer Billing address
        /// </summary>
        public Address BillingAddress
        {
            get
            {
                if (ViewState["BillingAddress"] != null)
                {
                    return (Address)ViewState["BillingAddress"];
                }

                return new Address();
            }

            set
            {
                ViewState["BillingAddress"] = value;
            }
        }

        /// <summary>
        ///  Gets the Customer shipping address
        /// </summary>
        public Address ShippingAddress
        {
            get
            {
                if (ViewState["ShippingAddress"] != null)
                {
                    return (Address)ViewState["ShippingAddress"];
                }

                return new Address();
            }

            set
            {
                ViewState["ShippingAddress"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the Account Object from/to page viewstate
        /// </summary>
        public ZNodeUserAccount UserAccount
        {
            get
            {
                if (ViewState["AccountObject"] != null)
                {
                    // The account info with 'AccountObject' is retrieved from the viewstate and
                    // It is converted to a Entity Account object
					return (ZNodeUserAccount)ViewState["AccountObject"];
                }

				return new ZNodeUserAccount();
            }
            
            set
            {
                // Customer account object is placed in the viewstate and assigned a key, AccountObject.            
                ViewState["AccountObject"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the billing address
        /// </summary>
        public int BillingAddressID
        {
            get
            {
                if (ViewState["BillingAddressID"] != null)
                {
                    return (int)ViewState["BillingAddressID"];
                }

                return 0;
            }
            
            set
            {
                ViewState["BillingAddressID"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the shipping address
        /// </summary>
        public int ShippingAddressID
        {
            get
            {
                if (ViewState["ShippingAddressID"] != null)
                {
                    return (int)ViewState["ShippingAddressID"];
                }

                return 0;
            }
            
            set
            {
                ViewState["ShippingAddressID"] = value;
            }
        }

        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Search Data Method
        /// </summary>
        public void BindSearchData()
        {
            ui_FoundUsers.DataBind();

            if (txtOrderID.Text.Trim().Length > 0)
            {
                this.OrderId = int.Parse(txtOrderID.Text.Trim());
            }

            CustomerAdmin customerAdmin = new CustomerAdmin();
            DataSet ds = customerAdmin.CustomerSearch(Server.HtmlEncode(FirstName.Text.Trim()), Server.HtmlEncode(LastName.Text.Trim()), Server.HtmlEncode(CompanyName.Text.Trim()), ZipCode.Text.Trim(), txtUserID.Text.Trim(), this.OrderId, this.CurrentPortalID, this.PortalIds);

            if (ds.Tables[0].Rows.Count > 0)
            {
                // Bind account list grid
                ui_FoundUsers.DataSource = ds;
                ui_FoundUsers.DataBind();
            }
            else
            {
                lblSearhError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotFoundNoUser").ToString();
            }
        }

        /// <summary>
        /// Clear UI Method
        /// </summary>
        public void ClearUI()
        {
            ui_FoundUsers.DataBind();

            FirstName.Text = string.Empty;
            LastName.Text = string.Empty;
            ZipCode.Text = string.Empty;
            CompanyName.Text = string.Empty;
            txtOrderID.Text = string.Empty;
            txtUserID.Text = string.Empty;
            UpdatePnlSearch.Update();
            updatePnlFoundUsers.Update();
        }

        #endregion

        #region Page Load Event

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get Portals 
            this.PortalIds = UserStoreAccess.GetAvailablePortals;

            // Set session value for current store portal
            this.CurrentPortalID = ZNodeConfigManager.SiteConfig.PortalID;
        }
        #endregion

        #region General Events
        /// <summary>
        /// Event is raised when the "Search" Button is clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Ui_Search_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
            updatePnlFoundUsers.Update();
        }

        /// <summary>
        /// Occurs when one of the pager buttons is clicked,
        /// but before the "FoundUsers" GridView control handles the paging operation.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Ui_FoundUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Retrieve Account Id
            Label userLogin = ui_FoundUsers.Rows[ui_FoundUsers.SelectedIndex].FindControl("ui_CustomerIdSelect") as Label;

            AccountService accountService = new AccountService();
            Account _account = accountService.GetByAccountID(int.Parse(userLogin.Text));

            if (_account != null)
            {
				this.UserAccount = new ZNodeUserAccount(_account);

				if (_account.ParentAccountID.HasValue)
				{
					this.UserAccount.ParentAccountID = _account.ParentAccountID.Value;
				}

				this.UserAccount.UpdateUser = _account.UpdateUser;
				this.UserAccount.UserID = _account.UserID;
				this.UserAccount.CreateUser = _account.CreateUser;
				this.UserAccount.EmailID = _account.Email;

                // Set Page session object
                // Get inserted Address Id
                AccountAdmin accountAdmin = new AccountAdmin();
				this.UserAccount.SetAccountAddress();
                if (this.UserAccount.BillingAddress != null)
                {
					this.BillingAddressID = this.UserAccount.BillingAddress.AddressID;
                    this.BillingAddress = this.UserAccount.BillingAddress;
                }

				if (this.UserAccount.ShippingAddress != null)
                {
					this.ShippingAddressID = this.UserAccount.ShippingAddress.AddressID;
                    this.ShippingAddress = this.UserAccount.ShippingAddress;
                }

                // Set the user account object in session to be used for gift card processing.
                HttpContext.Current.Session["AliasUserAccount"] = this.UserAccount;

                ZNode.Libraries.DataAccess.Custom.AccountHelper accountHelper = new ZNode.Libraries.DataAccess.Custom.AccountHelper();
                int _ProfileID = accountHelper.GetCustomerProfile(_account.AccountID, this.CurrentPortalID);

                if (_ProfileID != 0)
                {
                    ZNode.Libraries.Admin.ProfileAdmin profileAdmin = new ZNode.Libraries.Admin.ProfileAdmin();

                    HttpContext.Current.Session["ProfileCache"] = profileAdmin.GetByProfileID(_ProfileID);
                }
            }

            if (this.SelectedIndexChanged != null)
            {
                // Triggers parent control event
                this.SelectedIndexChanged(sender, e);
            }
        }

        /// <summary>
        /// Occurs when one of the pager buttons is clicked, 
        /// but before the GridView control handles the paging operation.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Ui_FoundUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ui_FoundUsers.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }
         
        /// <summary>
        /// Event is raised when the "Close" Button is clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (this.SearchButtonClick != null)
            {
                this.SearchButtonClick(sender, e);
            }
        } 

      
        /// <summary>
        /// Returns login name for a Provider access key(UserID)
        /// </summary>
        /// <param name="userId">The value of userId</param>
        /// <returns>Returns the Login name</returns>
        protected string GetLoginName(object userId)
        {
            if (userId != null)
            {
                if (!string.IsNullOrEmpty(userId.ToString()))
                {
                    MembershipUser user = Membership.GetUser(userId);

                    if (user != null)
                    {
                        return user.UserName;
                    }
                }
            }

            return string.Empty;
        }
        
        #endregion
    }
}