﻿using System;
using System.Web.UI;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.CreateOrder
{
    public partial class CartList : System.Web.UI.UserControl
    {

        #region Public Events
        public System.EventHandler ContinueButtonClicked;
		public System.EventHandler AddressSelectedIndexChanged;
        public System.EventHandler _CloseButtonClicked;
        #endregion

        /// <summary>
        /// Gets or sets the cancel button click event
        /// </summary>
        /// 
        public System.EventHandler CloseButtonClicked
        {
            get { return this._CloseButtonClicked; }
            set { this._CloseButtonClicked = value; }
        }

		public System.EventHandler MultiShipRemoveItemClicked { get; set; }

        #region Member Variables
        private ZNodeShoppingCart shoppingCart;
        private ZNodeUserAccount userAccount;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the User Account
        /// </summary>
        public ZNodeUserAccount UserAccount
        {
            get
            {
                if (ViewState["AccountObject"] != null)
                {
                    // The account info with 'AccountObject' is retrieved from the cache and
                    // It is converted to a Entity Account object
					return (ZNodeUserAccount)ViewState["AccountObject"];
                }

				return new ZNodeUserAccount();
            }

            set
            {
                // Customer account object is placed in the cache and assigned a key, AccountObject.            
                ViewState["AccountObject"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the billing address
        /// </summary>
        public int BillingAddressID
        {
            get
            {
                if (ViewState["BillingAddressID"] != null)
                {
                    return (int)ViewState["BillingAddressID"];
                }

                return 0;
            }

            set
            {
                ViewState["BillingAddressID"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the shipping address
        /// </summary>
        public int ShippingAddressID
        {
            get
            {
                if (ViewState["ShippingAddressID"] != null)
                {
                    return (int)ViewState["ShippingAddressID"];
                }

                return 0;
            }

            set
            {
                ViewState["ShippingAddressID"] = value;
            }
        }

        /// <summary>
        ///  Gets the Customer shipping address
        /// </summary>
        public Address ShippingAddress
        {
            get
            {
                ZNodeUserAccount _account = this.UserAccount;
                Address _shippingAddress = new Address();

                if (_account.AccountID > 0)
                {
                    Address shippingAddress = null;
                    if (this.ShippingAddressID == 0)
                    {
                        // If first time user created then shipping address Id will be 0, but address will created on database.                        
						shippingAddress = this.UserAccount.ShippingAddress;
                        this.ShippingAddressID = shippingAddress.AddressID;
                    }
                    
                    if (shippingAddress == null)
                    {
                        shippingAddress = new Address();
                    }

                    _shippingAddress.FirstName = shippingAddress.FirstName;
                    _shippingAddress.LastName = shippingAddress.LastName;
                    _shippingAddress.CompanyName = shippingAddress.CompanyName;
                    _shippingAddress.PhoneNumber = shippingAddress.PhoneNumber;
                    _shippingAddress.Street = shippingAddress.Street;
                    _shippingAddress.Street1 = shippingAddress.Street1;
                    _shippingAddress.City = shippingAddress.City;
                    _shippingAddress.PostalCode = shippingAddress.PostalCode;
                    _shippingAddress.StateCode = shippingAddress.StateCode;
                    _shippingAddress.CountryCode = shippingAddress.CountryCode;
                }

                return _shippingAddress;
            }
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Registers the event for the new address (child) control
            this.uxNewAddress.ButtonSubmitClick += new EventHandler(this.BtnSubmit_Click);

            // Registers the event for the new address (child) control
            this.uxNewAddress.ButtonCancelClick += new EventHandler(this.BtnCancel_Click);

			// Registers the event for the new address (child) control
			this.uxCart.CartItem_RemoveLinkClicked += new EventHandler(this.CartItem_RemoveLinkClicked);

            this.shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();

            if (!Page.IsPostBack)
            {
                PaymentSettingService _pmtServ = new PaymentSettingService();
                ZNode.Libraries.DataAccess.Entities.TList<ZNode.Libraries.DataAccess.Entities.PaymentSetting> _pmtSetting = _pmtServ.GetAll();

                int profileID = 0;

                // Check user Session
                if (this.userAccount != null)
                {
                    profileID = this.userAccount.ProfileID;
                }
                else
                {
                    // Get Default Registered profileId
                    profileID = ZNodeProfile.DefaultRegisteredProfileId;
                }
            }
        }

        /// <summary>
        /// On Pre Render Method
        /// </summary>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected override void OnPreRender(EventArgs e)
        {
            if (this.userAccount == null)
            {
                this.userAccount = ZNodeUserAccount.CurrentAccount();
            }

            if (this.shoppingCart != null)
            {
                this.Bind();
            }
            else
            {
                pnlShoppingCart.Visible = false;
            }
        }
        #endregion

        #region General Methods

        /// <summary>
        /// Fired when child Control Remove link triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
		 
		protected void btnContinue_Click(object sender, EventArgs e)
		{
			uxCart.UpdateOrderShipment();

			if (ContinueButtonClicked != null)
			{
				this.ContinueButtonClicked(sender, e);
			} 
		} 

        /// <summary>
        /// Fired when child Control Remove link triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CartItem_RemoveLinkClicked(object sender, EventArgs e)
        {
			int cartCount = 0;

			ZNodeShoppingCart ShoppingCart = (ZNodeShoppingCart)ZNodeCheckout.CreateFromSession(ZNodeSessionKeyType.ShoppingCart);

			if (ShoppingCart != null)
			{
				cartCount = ShoppingCart.ShoppingCartItems.Count;
				if (cartCount > 0)
				{
					if (this.MultiShipRemoveItemClicked != null)
					{
						this.MultiShipRemoveItemClicked(sender, e);
					}
				}
				else
				{
					if (this.CloseButtonClicked != null)
					{
						this.CloseButtonClicked(sender, e);
					}

				}

			}
        }

        protected void addNewAddress_Click(object sender, EventArgs e)
        {
            uxNewAddress.UserAccount = this.UserAccount;
            uxCart.UserAccount = this.UserAccount;
            mdlNewAddressPopup.Show();
        }

        /// <summary>
        /// Fires when Cancel Button Click triggered by child control (Create User)
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
			System.Web.UI.WebControls.Label label = uxNewAddress.FindControl("lblErrorMsg") as System.Web.UI.WebControls.Label;
			if (string.IsNullOrEmpty(label.Text))
			{
				mdlNewAddressPopup.Hide();
			}
			else
			{
				mdlNewAddressPopup.Show();
			}
			
			this.Bind();
			UpdatePnlNewAddress.Update();
			if (this.AddressSelectedIndexChanged != null)
			{
				this.AddressSelectedIndexChanged(sender, e);
			}

        }

        /// <summary>
        /// Fires when Cancel Button Click triggered by child control (Create User)
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            mdlNewAddressPopup.Hide();
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            if (this.CloseButtonClicked != null)
            {
                this.CloseButtonClicked(sender, e);
            }
        }
        #endregion

        #region Public Methods
        public void BindAddressNames(ZNodeUserAccount accountInfo)
        {
            uxCart.UserAccount = this.UserAccount;
        }

        /// <summary>
        /// Method to get the Billing Address of newly added address
        /// </summary>
        /// <returns></returns>
        public Address GetBillingAddress()
        {
            AddressService addressService = new AddressService();
            Address billingAddress = null;
            this.BillingAddressID = uxNewAddress.BillingAddressID;
            billingAddress = addressService.GetByAddressID(this.BillingAddressID);
            return billingAddress;
        }

        #endregion

        #region Protected Methods
        /// <summary>
        /// Bind All Controls
        /// </summary>
        protected void Bind()
        {
            // Show/hide cart
            if (this.shoppingCart.Count > 0)
            {
                pnlShoppingCart.Visible = true;
                uxMsg.Text = string.Empty;
            }
			
            // Bind grid
            uxCart.Bind();
        }
        #endregion

		
    }
}