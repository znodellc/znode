<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.OrderManagement.CreateOrder.CustomerSearch" Codebehind="CustomerSearch.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<div>
    <h4 class="SubTitle">
        <asp:Localize ID="SubTitleSearchCustomers" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchCustomers%>'></asp:Localize></h4>
    <asp:UpdatePanel ID="UpdatePnlSearch" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel CssClass="CustomerSearch" ID="ui_pnlSearchParams" runat="server" DefaultButton="ui_Search">
                 
                <div class="SearchForm">
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize ID="ColumnFirstName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFirstName%>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="FirstName" runat="server" ValidationGroup="groupSearch"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize ID="ColumnLastName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleLastName%>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="LastName" runat="server" ValidationGroup="groupSearch"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize ID="ColumnTitleCompany" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCompanyName%>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="CompanyName" runat="server" ValidationGroup="groupSearch"></asp:TextBox></span>
                        </div>
                    </div>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize ID="ColumnTitleZip" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleZip%>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="ZipCode" runat="server" ValidationGroup="groupSearch"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize ID="ColumnTitleUserID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleUserID%>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtUserID" runat="server" ValidationGroup="groupSearch"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize ID="ColumnTitleOrderID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderID%>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtOrderID" runat="server" ValidationGroup="groupSearch"></asp:TextBox>
                                <asp:RegularExpressionValidator ValidationGroup="groupSearch" ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularValidOrderId%>'
                                    ID="val1" ControlToValidate="txtOrderID" ErrorMessage='<%$ Resources:ZnodeAdminResource, ErrorInvalid%>' runat="server"
                                    Display="Dynamic" CssClass="Error" SetFocusOnError="true"></asp:RegularExpressionValidator></span>
                        </div>
                    </div>
                    <div class="ClearBoth">
                        <asp:Label CssClass="Error" ID="lblSearhError" runat="server" EnableViewState="false" /><br />
                         <zn:Button runat="server" ButtonType="SubmitButton" OnClick="Ui_Search_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch%>' ID="ui_Search" />
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <asp:Panel ID="pnlSelectUsers" runat="server" Width="100%">
        <asp:UpdatePanel ID="updatePnlFoundUsers" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div>
                    <h4 class="GridTitle">
                        <asp:Localize ID="GridTitleSelectCustomer" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleSelectCustomer%>'></asp:Localize></h4> 
                    <asp:GridView ID="ui_FoundUsers" CellPadding="0" CssClass="Grid" runat="server" AutoGenerateColumns="False"
                        OnSelectedIndexChanged="Ui_FoundUsers_SelectedIndexChanged" AllowPaging="True"
                        OnPageIndexChanging="Ui_FoundUsers_PageIndexChanging" Width="100%" GridLines="None">
                        <Columns>
                            <asp:CommandField ShowSelectButton="true" ValidationGroup="groupSearch" ButtonType="Link"
                                SelectText='<%$ Resources:ZnodeAdminResource, LinkTextSelect%>' HeaderStyle-HorizontalAlign="Left" />
                            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleLogin%>' HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="ui_CustomerIdSelect" runat="server" Text='<%# DataBinder.Eval(Container.DataItem,"AccountID") %>'></asp:Label></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleLastName%>' DataField="BillingLastName" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleFirstName%>' DataField="BillingFirstName" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCompany%>' DataField="BillingCompanyName" HeaderStyle-HorizontalAlign="Left" />
                            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleAddress%>' HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <%# DataBinder.Eval(Container.DataItem, "BillingStreet") + " " + DataBinder.Eval(Container.DataItem, "BillingStreet1")%>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCity%>' DataField="BillingCity" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSt%>' DataField="BillingStateCode" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleZip%>' DataField="BillingPostalCode" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePhone%>' DataField="BillingPhoneNumber" HeaderStyle-HorizontalAlign="Left" />
                            <asp:BoundField DataField="UserName" HeaderStyle-HorizontalAlign="Left" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleUserID%>' />
                        </Columns>
                        <EditRowStyle CssClass="EditRowStyle" />
                        <FooterStyle CssClass="FooterStyle" />
                        <RowStyle CssClass="RowStyle" />
                        <SelectedRowStyle CssClass="SelectedRowStyle" />
                        <PagerStyle CssClass="PagerStyle" />
                        <HeaderStyle CssClass="HeaderStyle" />
                        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    </asp:GridView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            &nbsp;</div>
        <div align="right"> 
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="btnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancel" />
        </div>
    </asp:Panel>
</div>
