using System;
using System.Web.Security;
using System.Web.UI;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders
{
    /// <summary>
    /// Represents the Order Manager
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            Session["SelectedMenuItem"] = Request.Url.PathAndQuery;

            if (Roles.IsUserInRole(Page.User.Identity.Name, "ORDER APPROVER"))
            {
                Response.Redirect("~/FranchiseAdmin/Secure/Orders/OrderManagement/ViewOrders/Default.aspx");
            }
        }
    }
}