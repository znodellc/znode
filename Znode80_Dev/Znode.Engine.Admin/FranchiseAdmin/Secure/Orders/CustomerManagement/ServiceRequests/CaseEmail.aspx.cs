using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Common;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.ServiceRequests
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_cases_case_email class.
    /// </summary>
    public partial class CaseEmail : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private string redirectLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/ServiceRequests/Default.aspx";
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
                if (this.itemId > 0)
                {
                    this.BindValues();
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            string fileName = string.Empty;
            int portalID = 0;
            if (Session["PortalID"] != null)
            {
                portalID = int.Parse(Session["PortalID"].ToString());
            }

            MailMessage emailContent = new MailMessage(ZNodeConfigManager.SiteConfig.AdminEmail, lblEmailid.Text);
            emailContent.Subject = txtEmailSubj.Text;
            emailContent.Body = ctrlHtmlText.Html.ToString();
            emailContent.IsBodyHtml = true;

            // Get the file name 
            fileName = Path.GetFileName(FileBrowse.PostedFile.FileName);

            if (!fileName.Equals(string.Empty))
            {
                // Email Attachment
                Attachment attach = new Attachment(FileBrowse.PostedFile.InputStream, fileName);

                // Attach the created email attachment 
                emailContent.Attachments.Add(attach);
            }

            string smtpServer = string.Empty;
            string smtpUsername = string.Empty;
            string smtpPassword = string.Empty;

            // To get the SMTP settings based on PortalID
            ZNodeConfigManager.AliasSiteConfig(portalID);

            if (ZNodeConfigManager.SiteConfig.SMTPServer != null)
            {
                smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            }

            if (ZNodeConfigManager.SiteConfig.SMTPUserName != null)
            {
                smtpUsername = this.encrypt.DecryptData(ZNodeConfigManager.SiteConfig.SMTPUserName);
            }

            if (ZNodeConfigManager.SiteConfig.SMTPPassword != null)
            {
                smtpPassword = this.encrypt.DecryptData(ZNodeConfigManager.SiteConfig.SMTPPassword);
            }

            try
            {
                // Create mail client and send email
                System.Net.Mail.SmtpClient emailClient = new System.Net.Mail.SmtpClient();
                emailClient.Host = smtpServer;
                emailClient.Credentials = new NetworkCredential(smtpUsername, smtpPassword);

                // Send MailContent
                emailClient.Send(emailContent);

                emailContent.Attachments.Dispose();

                // Delete the attachements if any
                if (fileName != null && fileName != string.Empty)
                {
                    File.Delete(Server.MapPath(fileName));
                }

                NoteAdmin noteAdmin = new NoteAdmin();
                Note note = new Note();

                note.CaseID = this.itemId;
                if (txtAccountId.Value.Length > 0)
                {
                    note.AccountID = int.Parse(txtAccountId.Value);
                }

                note.CreateDte = System.DateTime.Now;
                note.CreateUser = HttpContext.Current.User.Identity.Name;
                note.NoteTitle = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEmailReplySubjectTo").ToString() + lblCaseTitle.Text.Trim() + ")";
                note.NoteBody = ctrlHtmlText.Html;

                bool isSuccess = noteAdmin.Insert(note);

                // Log Activity
                AccountAdmin accountadmin = new AccountAdmin();
                Account account = new Account();
                string userName = string.Empty;

                if (txtAccountId.Value.Length > 0)
                {
                    account = accountadmin.GetByAccountID(int.Parse(txtAccountId.Value));
                    MembershipUser _user = Membership.GetUser(account.UserID);
                    userName = _user.UserName;
                }

                string associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEmailReply").ToString(), userName);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, userName);

                Response.Redirect(this.redirectLink);
            }
            catch (Exception ex)
            {
                lblErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorEmailReply").ToString();

                // Log exception
                ExceptionPolicy.HandleException(ex, this.GetGlobalResourceObject("ZnodeAdminResource", "TextZnodeGlobalExceptionPolicy").ToString());
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.redirectLink);
        }

        #endregion

        #region Bind Methods
        private void BindData()
        {
            CaseAdmin caseAdmin = new CaseAdmin();
            CaseRequest caseRequest = caseAdmin.GetByCaseID(this.itemId);

            if (caseRequest != null)
            {
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = profiles.StoreAccess.Split(',');
                    Array storesArray = (Array)stores;
                    int found = Array.IndexOf(storesArray, caseRequest.PortalID.ToString());
                    if (found == -1)
                    {
                        Response.Redirect("list.aspx", true);
                    }
                }

                txtEmailSubj.Text = Server.HtmlDecode(caseRequest.Title);
                lblEmailid.Text = caseRequest.EmailID;
                Session.Add("PortalID", caseRequest.PortalID);
            }
        }

        /// <summary>
        /// Bind case request.
        /// </summary>
        private void BindValues()
        {
            CaseAdmin caseAdmin = new CaseAdmin();
            CaseRequest caseRequest = caseAdmin.GetByCaseID(this.itemId);

            if (caseRequest != null)
            {
                // Set General Case Information     
                if (caseRequest.AccountID.HasValue)
                {
                    txtAccountId.Value = caseRequest.AccountID.Value.ToString();
                }

                lblCaseTitle.Text = caseRequest.Title;
                lblCaseStatus.Text = this.GetCaseStatusByCaseID(caseRequest.CaseStatusID);
                lblCasePriority.Text = this.GetCasePriorityByCaseID(caseRequest.CasePriorityID);
                txtCaseDescription.Text = Server.HtmlDecode(caseRequest.Description).Replace("<br>", "\r\n");

                // Set Customer Information
                lblCustomerName.Text = caseRequest.FirstName + " " + caseRequest.LastName;
                lblCompanyName.Text = caseRequest.CompanyName;
                lblEmailTo.Text = caseRequest.EmailID;
                lblPhoneNumber.Text = caseRequest.PhoneNumber;
            }
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Get accoun type by account Id
        /// </summary>
        /// <param name="accountTypeId">AccountTypeId to get the account type object.</param>
        /// <returns>Returns the accoun type name.</returns>
        private string GetAccountTypeByAccountID(object accountTypeId)
        {
            if (accountTypeId == null)
            {
                return string.Empty;
            }
            else
            {
                CaseAdmin caseAdmin = new CaseAdmin();
                AccountType accountType = caseAdmin.GetByAccountTypeID(int.Parse(accountTypeId.ToString()));

                if (accountType != null)
                {
                    return accountType.AccountTypeNme;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Get case status by case Id
        /// </summary>
        /// <param name="caseStatusId">CaseStatusId to get the case status object</param>
        /// <returns>Returns the case status name.</returns>
        private string GetCaseStatusByCaseID(object caseStatusId)
        {
            if (caseStatusId == null)
            {
                return string.Empty;
            }
            else
            {
                CaseAdmin caseAdmin = new CaseAdmin();
                CaseStatus caseStatus = caseAdmin.GetByCaseStatusID(int.Parse(caseStatusId.ToString()));
                if (caseStatus == null)
                {
                    return string.Empty;
                }
                else
                {
                    return caseStatus.CaseStatusNme;
                }
            }
        }

        /// <summary>
        /// Get case priority by case Id
        /// </summary>
        /// <param name="caseId">CaseId to get the case priority object</param>
        /// <returns>Returns the case priority name.</returns>
        private string GetCasePriorityByCaseID(object caseId)
        {
            if (caseId == null)
            {
                return string.Empty;
            }
            else
            {
                CaseAdmin caseAdmin = new CaseAdmin();
                CasePriority casePriority = caseAdmin.GetByCasePriorityID(int.Parse(caseId.ToString()));
                if (casePriority == null)
                {
                    return string.Empty;
                }
                else
                {
                    return casePriority.CasePriorityNme;
                }
            }
        }
        #endregion
    }
}