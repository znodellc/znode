<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.ServiceRequests.NoteAdd" ValidateRequest="false" Title="Untitled Page"
    CodeBehind="NoteAdd.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/FranchiseAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <div class="LeftFloat" style="width: 80%; text-align: left">
            <h1>
                <asp:Label ID="lblHeading" runat="server" Text="Label"></asp:Label></h1>
        </div>
        <div style="float: right">
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="True" ID="btnSubmitTop" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" ID="btnCancelTop" />
        </div>
        <div class="ClearBoth">
        </div>
        <div class="FormView">
            <div class="FieldStyle">
                <asp:Localize ID="TitleColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTitle%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID='txtNoteTitle' runat='server' MaxLength="50" Columns="20"></asp:TextBox>
                <asp:Label runat="server" ID="lblNoteTitle" CssClass="Error" Visible="False"></asp:Label>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="NoteColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnNote %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
                <asp:Label runat="server" ID="lblHtmlTextError" CssClass="Error" Visible="False"></asp:Label>
            </div>
            <div class="ClearBoth">
                <asp:Label ID="lblError" CssClass="Error" ForeColor="DarkRed" runat="server" Text="Label"
                    Visible="False"></asp:Label>
            </div>
        </div>
        <div>
            <zn:Button runat="server" ID="btnCancel" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnSubmit" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
        </div>
    </div>
</asp:Content>
