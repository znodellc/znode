using System;
using System.Web;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.ServiceRequests
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_Note_Add class.
    /// </summary>
    public partial class NoteAdd : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId;
        private string cancelLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/ServiceRequests/View.aspx";
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region General Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
                CaseAdmin caseAdmin = new CaseAdmin();
                CaseRequest caseRequest = caseAdmin.GetByCaseID(this.itemId);

                if (caseRequest != null)
                {
                    lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddNoteToServiceRequest").ToString() + caseRequest.Title;
                    ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                    if (profiles.StoreAccess != "AllStores")
                    {
                        string[] stores = profiles.StoreAccess.Split(',');
                        Array storesArray = (Array)stores;
                        int found = Array.IndexOf(storesArray, caseRequest.PortalID.ToString());
                        if (found == -1)
                        {
                            Response.Redirect("list.aspx", true);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            NoteAdmin noteAdmin = new NoteAdmin();
            Note note = new Note();

            note.CaseID = this.itemId;
            note.AccountID = null;
            note.CreateDte = System.DateTime.Now;
            note.CreateUser = HttpContext.Current.User.Identity.Name;
            note.NoteTitle = Server.HtmlEncode(txtNoteTitle.Text.Trim());
            note.NoteBody = ctrlHtmlText.Html;

            //Validating for empty text
            bool isValid = IsValidate();
            if (isValid)
                return;

            bool isSuccess = noteAdmin.Insert(note);

            if (isSuccess)
            {
                CaseAdmin caseAdmin = new CaseAdmin();
                CaseRequest caseRequest = caseAdmin.GetByCaseID(this.itemId);

                string associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddNoteToServiceRequest").ToString(), txtNoteTitle.Text.Trim(), caseRequest.Title);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, caseRequest.Title);

                // Redirect to main page
                Response.Redirect(this.cancelLink + "?itemid=" + this.encrypt.EncryptData(this.itemId.ToString()));
            }
            else
            {
                // Display error message
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoteAdd").ToString();
                lblError.Visible = true;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.cancelLink + "?itemid=" + this.encrypt.EncryptData(this.itemId.ToString()));
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Validating the Controls
        /// </summary>
        /// <returns></returns>
        private bool IsValidate()
        {
            if (string.IsNullOrEmpty(ctrlHtmlText.Html.Trim()) && string.IsNullOrEmpty(txtNoteTitle.Text.Trim()))
            {
                lblHtmlTextError.Visible = true;
                lblHtmlTextError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "RequiredNote").ToString();
                lblNoteTitle.Visible = true;
                lblNoteTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "RequiredNoteTitle").ToString();
                return true;
            }
            else if (!string.IsNullOrEmpty(txtNoteTitle.Text.Trim()) && string.IsNullOrEmpty(ctrlHtmlText.Html.Trim()))
            {
                lblHtmlTextError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "RequiredNote").ToString();
                lblHtmlTextError.Visible = true;
                lblNoteTitle.Visible = false;
                return true;
            }
            else if (string.IsNullOrEmpty(txtNoteTitle.Text.Trim()) && !string.IsNullOrEmpty(ctrlHtmlText.Html.Trim()))
            {
                lblNoteTitle.Visible = true;
                lblNoteTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "RequiredNoteTitle").ToString();
                lblHtmlTextError.Visible = false;

                return true;
            }
            return false;
        }
        #endregion
    }
}