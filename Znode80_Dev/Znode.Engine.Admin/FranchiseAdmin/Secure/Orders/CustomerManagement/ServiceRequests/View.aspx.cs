using System;
using System.Text;
using System.Web;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.ServiceRequests
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_cases_view class.
    /// </summary>
    public partial class View : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private string listLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/ServiceRequests/Default.aspx";
        private string editLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/ServiceRequests/Add.aspx?itemid=";
        private string emailLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/ServiceRequests/CaseEmail.aspx?itemid=";
        private string addNoteLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/ServiceRequests/NoteAdd.aspx?itemid=";
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region General Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }

            if (!Page.IsPostBack)
            {
                if (this.itemId > 0)
                {
                    this.BindNotes();
                    this.BindValues();
                }
            }
        }

        /// <summary>
        /// Add New Note Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddNewNote_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addNoteLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Case List Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CaseList_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listLink);
        }

        /// <summary>
        /// Edit Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CaseEdit_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.editLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Reply to Customer Button  Click Event 
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ReplyToCase_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.emailLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        #endregion        

        #region Helper Methods

        /// <summary>
        /// Format note description
        /// </summary>
        /// <param name="field1">Field1 to format.</param>
        /// <param name="field2">Field2 to format.</param>
        /// <param name="field3">Field3 to format.</param>
        /// <returns>Returns the formatted node description</returns>
        protected string FormatCustomerNote(object field1, object field2, object field3)
        {
            StringBuilder customerNote = new StringBuilder();
            customerNote.Append(field1);
            customerNote.Append(" " + this.GetGlobalResourceObject("ZnodeAdminResource", "Hyphen").ToString() + " ");
            customerNote.Append(field2);
            customerNote.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnOn").ToString());
            customerNote.Append(field3);

            return customerNote.ToString();
        }

        /// <summary>
        /// Gets account type name by account Id
        /// </summary>
        /// <param name="accountId">Account ID to get account type.</param>
        /// <returns>Returns the account type name.</returns>
        private string GetAccountTypeByAccountID(object accountId)
        {
            if (accountId == null)
            {
                return string.Empty;
            }
            else
            {
                CaseAdmin caseAdmin = new CaseAdmin();
                AccountType accountType = caseAdmin.GetByAccountTypeID(int.Parse(accountId.ToString()));
                if (accountType != null)
                {
                    return accountType.AccountTypeNme;
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// Get case status by case Id
        /// </summary>
        /// <param name="caseId">Case Id to get case status.</param>
        /// <returns>Returns the case status.</returns>
        private string GetCaseStatusByCaseID(object caseId)
        {
            if (caseId == null)
            {
                return string.Empty;
            }
            else
            {
                CaseAdmin caseAdmin = new CaseAdmin();
                CaseStatus caseStatus = caseAdmin.GetByCaseStatusID(int.Parse(caseId.ToString()));
                if (caseStatus == null)
                {
                    return string.Empty;
                }
                else
                {
                    return caseStatus.CaseStatusNme;
                }
            }
        }

        /// <summary>
        /// Get case priority by case Id
        /// </summary>
        /// <param name="priorityId">Priority Id to get the case priority</param>
        /// <returns>Returns the case priority name.</returns>
        private string GetCasePriorityByCaseID(object priorityId)
        {
            if (priorityId == null)
            {
                return string.Empty;
            }
            else
            {
                CaseAdmin caseAdmin = new CaseAdmin();
                CasePriority casePriority = caseAdmin.GetByCasePriorityID(int.Parse(priorityId.ToString()));
                if (casePriority == null)
                {
                    return string.Empty;
                }
                else
                {
                    return casePriority.CasePriorityNme;
                }
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// //Bind Repeater
        /// </summary>
        private void BindNotes()
        {
            NoteAdmin noteAdmin = new NoteAdmin();
            CustomerNotes.DataSource = noteAdmin.GetByCaseID(this.itemId);
            CustomerNotes.DataBind();
        }

        /// <summary>
        /// Bind case request data.
        /// </summary>
        private void BindValues()
        {
            CaseAdmin caseAdmin = new CaseAdmin();
            CaseRequest caseRequest = caseAdmin.GetByCaseID(this.itemId);
            UserStoreAccess.CheckStoreAccess(caseRequest.PortalID, true);

            if (caseRequest != null)
            {
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = profiles.StoreAccess.Split(',');
                    Array storesArray = (Array)stores;
                    int found = Array.IndexOf(storesArray, caseRequest.PortalID.ToString());
                    if (found == -1)
                    {
                        Response.Redirect("Default.aspx", true);
                    }
                }

                // Set General Case Information
                lblCreatedDate.Text = caseRequest.CreateDte.ToShortDateString();
                lblTitle.Text = caseRequest.Title;
                lblCaseTitle.Text = caseRequest.Title;
                lblCaseStatus.Text = this.GetCaseStatusByCaseID(caseRequest.CaseStatusID);
                if (lblCaseStatus.Text == "Pending")
                {
                    lblCaseStatus.ForeColor = System.Drawing.Color.Red;
                }
                lblCasePriority.Text = this.GetCasePriorityByCaseID(caseRequest.CasePriorityID);
                lblCaseDescription.Text = Server.HtmlDecode(caseRequest.Description);

                // Set Customer Information 
                lblFirstName.Text = caseRequest.FirstName;
                lblLastName.Text = caseRequest.LastName; 
                lblCompanyName.Text = caseRequest.CompanyName;
                lblEmailID.Text = caseRequest.EmailID;
                lblPhoneNumber.Text = caseRequest.PhoneNumber;
            }
        }
        #endregion
    }
}