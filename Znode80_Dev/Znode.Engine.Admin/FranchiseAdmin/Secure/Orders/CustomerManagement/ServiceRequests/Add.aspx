<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.ServiceRequests.Add" ValidateRequest="false" CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/FranchiseAdmin/Controls/Default/StoreName.ascx" TagName="StoreName" TagPrefix="ZNode" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmitTop" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelTop" />
        </div>
        <div class="ClearBoth">
            <uc1:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="FormView">
            <h4 class="ServiceRequestSubTitle">
                <asp:Localize ID="ContactInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleContact %>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="FirstName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFirstName %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                    ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName"
                    Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredFirstName %>' SetFocusOnError="True" CssClass="Error"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="LastName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLastName %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                    ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLastName" Display="Dynamic"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredLastName %>' SetFocusOnError="True" CssClass="Error"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="CompanyName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCompanyName %>'></asp:Localize>
                <asp:Localize ID="ColumnOptional" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOptional %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCompanyName" runat="server"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="EmailID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEmailID %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtEmailID" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtEmailID"
                    Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEmailID %>' SetFocusOnError="True" CssClass="Error"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator
                    ID="regemailID" runat="server" ControlToValidate="txtEmailID" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularEmailAddress %>'
                    Display="Dynamic" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                    CssClass="Error" SetFocusOnError="True"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="PhoneNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPhoneNumber %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtPhoneNo" runat="server" MaxLength="20"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionPhoneNo" runat="server" ControlToValidate="txtPhoneNo" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidPhoneNumber %>' Display="Dynamic"
                    ValidationExpression="^([0-9\(\)\/\+ \-]*)$" CssClass="Error" SetFocusOnError="True"></asp:RegularExpressionValidator>
            </div>
            <h4 class="ServiceRequestSubTitle">
                <asp:Localize ID="ServiceRequest" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleServiceRequest %>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="CreateDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCreateDate %>'></asp:Localize></div>
            <div class="ValueStyle">
                <b>
                    <asp:Label ID="lblCaseDate" runat="server" /></b>
            </div>

            <div class="FieldStyle">
                <asp:Localize ID="Origin" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOrigin %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <b>
                    <asp:Label ID="lblCaseOrigin" runat="server" /></b>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="Status" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstCaseStatus" runat="server">
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="Priority" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPriority %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstCasePriority" runat="server">
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="TitleColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitle %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCaseTitle" runat="server" MaxLength="50" Columns="20"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtCaseTitle"
                    Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterCaseTitle %>' SetFocusOnError="True" CssClass="Error"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="Message" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnMessage %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCaseDescription" runat="server" TextMode="MultiLine" Height="190px"
                    MaxLength="10000" Width="600px"></asp:TextBox><br />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtCaseDescription"
                    Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredCaseDescription %>' SetFocusOnError="True"
                    CssClass="Error"></asp:RequiredFieldValidator>
            </div>
        </div>
        <div class="ClearBoth">
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmit" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancel" />
        </div>
        <asp:HiddenField ID="HiddenOldStatus" runat="server" />
    </div>
</asp:Content>
