<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.ServiceRequests.Default" ValidateRequest="false" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1><asp:Localize ID="ServiceRequests" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextServiceRequests %>'></asp:Localize></h1>
            <p style="width: 700px;">
                <asp:Localize ID="ServiceRequestsText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextServiceRequests %>'></asp:Localize>
            </p>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAdd" runat="server" CausesValidation="False"
                ButtonType="Button" OnClick="BtnAdd_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCreateServiceRequest %>'
                ButtonPriority="Primary" />
        </div>
        <div class="ClearBoth">
        </div>
        <uc1:Spacer ID="Spacer8" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
        <div align="left">
            <h4 class="ServiceRequestSubTitle"><asp:Localize ID="SearchServiceRequests" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchServiceRequests %>'></asp:Localize>
            </h4>
            <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
                <div class="SearchForm">
                    <div class="RowStyle">
                        <div class="ItemStyle">
                        </div>
                    </div>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize ID="CaseID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCaseID %>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtcaseid" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize ID="TitleColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitle %>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txttitle" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize ID="CaseStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCaseStatus %>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:DropDownList runat="server" ID="ListCaseStatus" />
                            </span>
                        </div>
                    </div>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize ID="FirstName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFirstName %>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtfirstname" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize ID="LastName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLastName %>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtlastname" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize ID="CompanyName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCompanyName %>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtcompanyname" runat="server"></asp:TextBox></span>
                        </div>
                    </div>
                    <div class="ClearBoth"> 
                         <zn:Button runat="server" ID="btnClear" OnClick="BtnClearSearch_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource,  ButtonClear %>' CausesValidation="False"/>
                         <zn:Button runat="server" ID="btnSearch" OnClick="BtnSearch_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                  </div>
                </div>
            </asp:Panel>
            <br />
            <h4 class="GridTitle"><asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleServiceRequest %>'></asp:Localize>
            </h4>
            <div>
                <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" Width="100%" AllowSorting="True"
                    CellPadding="4" DataKeyNames="accountid" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundServiceRequest %>'
                    OnRowCommand="UxGrid_RowCommand" OnPageIndexChanging="UxGrid_PageIndexChanging"
                    PageSize="50" AllowPaging="True" GridLines="None" AutoGenerateColumns="False"
                    CaptionAlign="Left" OnSorting="UxGrid_Sorting" OnRowDataBound="uxGrid_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <a href='View.aspx?itemid=<%# GetEncryptId(DataBinder.Eval(Container.DataItem, "CaseID"))%>'>
                                    <%# Eval("CaseID")%></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitle %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <a href='View.aspx?itemid=<%# GetEncryptId(DataBinder.Eval(Container.DataItem, "CaseID"))%>'>
                                    <%# Eval("Title")%></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="StoreName" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnStoreName %>'  HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnCaseStatus %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblCaseStatus" Text='<%# Eval("CaseStatusNme") %>'
                                    runat="server" Font-Bold="true" Font-Size="Smaller"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="CasePriorityNme" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePriority %>' HeaderStyle-HorizontalAlign="Left" />

                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCreatedDate %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# (DataBinder.Eval(Container.DataItem, "CreateDte", "{0:d}"))%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleActions %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <div class="LeftFloat" style="width: 17%; text-align: left">
                                    <asp:LinkButton ID="butView" CssClass="LinkButton" runat="server" CommandArgument='<%# Eval("caseid") %>'
                                        CommandName="View" Text='<%$ Resources:ZnodeAdminResource, LinkManage %>' />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
