using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.ServiceRequests
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_cases_Default class.
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Private Member Variables
        private string cancelLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/ServiceRequests/Default.aspx";
        private int itemId = 0;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region General Events

        /// <summary>
        /// page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }

            if (!Page.IsPostBack)
            {
                // Bind data to the fields on the page
                this.BindData();
                if (this.itemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditServiceRequest").ToString();
                    txtCaseDescription.ReadOnly = true;
                    txtCaseTitle.ReadOnly = true;
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddServiceRequest").ToString();
                    lblCaseOrigin.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnAdmin").ToString();
                    lblCaseDate.Text = System.DateTime.Now.ToShortDateString();
                }
            }
        }

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            CaseAdmin caseAdmin = new CaseAdmin();
            CaseRequest caseRequest = new CaseRequest();

            // If edit mode then retrieve data first
            if (this.itemId > 0)
            {
                caseRequest = caseAdmin.GetByCaseID(this.itemId);
            }

            // Set Null Values
            caseRequest.OwnerAccountID = null;
            caseRequest.CaseOrigin = lblCaseOrigin.Text;
            caseRequest.CreateUser = System.Web.HttpContext.Current.User.Identity.Name;
            caseRequest.CreateDte = System.DateTime.Now;
            caseRequest.PortalID = UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0);

            // Set Values 
            caseRequest.Title = Server.HtmlEncode(txtCaseTitle.Text.Trim());
            caseRequest.Description = Server.HtmlEncode(txtCaseDescription.Text.Trim());
            caseRequest.EmailID = txtEmailID.Text.Trim();
            caseRequest.FirstName = Server.HtmlEncode(txtFirstName.Text.Trim());
            caseRequest.LastName = Server.HtmlEncode(txtLastName.Text.Trim());
            caseRequest.PhoneNumber = txtPhoneNo.Text.Trim();
            caseRequest.CompanyName = Server.HtmlEncode(txtCompanyName.Text.Trim());

            caseRequest.AccountID = null;

            if (lstCasePriority.SelectedIndex != -1)
            {
                caseRequest.CasePriorityID = int.Parse(lstCasePriority.SelectedValue);
            }

            if (lstCaseStatus.SelectedValue != null)
            {
                caseRequest.CaseStatusID = int.Parse(lstCaseStatus.SelectedValue);
            }

            bool isSuccess = false;
            string associateName = string.Empty;
            if (this.itemId > 0)
            {
                isSuccess = caseAdmin.Update(caseRequest);
                associateName =  this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogEditServiceRequest").ToString() + txtCaseTitle.Text.Trim();
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, txtCaseTitle.Text.Trim());
            }
            else
            {
                isSuccess = caseAdmin.Add(caseRequest);
                associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddServiceRequest").ToString() + txtCaseTitle.Text.Trim();
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, txtCaseTitle.Text.Trim());
            }

            if (HiddenOldStatus.Value != lstCaseStatus.SelectedItem.Text)
            {
                NoteAdmin noteAdmin = new NoteAdmin();
                Note note = new Note();

                note.CaseID = caseRequest.CaseID;
                note.AccountID = null;
                note.CreateDte = System.DateTime.Now;
                note.CreateUser = HttpContext.Current.User.Identity.Name;
                note.NoteTitle = this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnStatusChanged").ToString();
                note.NoteBody = this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnStatusChanged").ToString() + HiddenOldStatus.Value.ToUpper() + this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnTo").ToString() + lstCaseStatus.SelectedItem.Text.ToUpper() +
                                        this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnBy").ToString() + ZNodeUserAccount.CurrentAccount().BillingAddress.FirstName + " " + ZNodeUserAccount.CurrentAccount().BillingAddress.LastName;

                isSuccess = noteAdmin.Insert(note);
            }

            if (isSuccess)
            {
                // Redirect to list page
                Response.Redirect("~/FranchiseAdmin/Secure/Orders/CustomerManagement/ServiceRequests/Default.aspx");
            }
            else
            {
                // Display error message
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoteAdd").ToString();
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.cancelLink);
        }
        #endregion

        #region Bind Data

        /// <summary>
        /// Bind the case priority and case status.
        /// </summary>
        private void BindData()
        {
            // Bind Account Type Dropdown List
            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);

            CaseAdmin caseAdmin = new CaseAdmin();

            // Bind Case status DropdownList
            lstCaseStatus.DataSource = caseAdmin.GetAllCaseStatus();
            lstCaseStatus.DataTextField = "CaseStatusNme";
            lstCaseStatus.DataValueField = "CaseStatusID";
            lstCaseStatus.DataBind();

            // Bind case priority DropdownList
            lstCasePriority.DataSource = caseAdmin.GetAllCasePriority();
            lstCasePriority.DataTextField = "CasePriorityNme";
            lstCasePriority.DataValueField = "CasePriorityID";
            lstCasePriority.DataBind();
        }

        /// <summary>
        /// Bind Edit Data
        /// </summary>
        private void BindEditData()
        {
            CaseAdmin caseAdmin = new CaseAdmin();
            CaseRequest caseRequest = caseAdmin.GetByCaseID(this.itemId);
            UserStoreAccess.CheckStoreAccess(caseRequest.PortalID, true);

            if (caseRequest != null)
            {
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = profiles.StoreAccess.Split(',');
                    Array storesArray = (Array)stores;
                    int found = Array.IndexOf(storesArray, caseRequest.PortalID.ToString());
                    if (found == -1)
                    {
                        Response.Redirect("list.aspx", true);
                    }
                }

                if (lstCasePriority.Items.Count > 0)
                {
                    lstCaseStatus.SelectedValue = caseRequest.CaseStatusID.ToString();
                    HiddenOldStatus.Value = lstCaseStatus.SelectedItem.Text;
                }

                if (lstCaseStatus.Items.Count > 0)
                {
                    lstCasePriority.SelectedValue = caseRequest.CasePriorityID.ToString();
                }

                // Set Values                 
                lblCaseOrigin.Text = caseRequest.CaseOrigin;
                txtCaseTitle.Text = Server.HtmlDecode(caseRequest.Title);
                txtCaseDescription.Text = Server.HtmlDecode(caseRequest.Description.Replace("<br>", "\r\n"));
                txtFirstName.Text = Server.HtmlDecode(caseRequest.FirstName);
                txtLastName.Text = Server.HtmlDecode(caseRequest.LastName);
                txtCompanyName.Text = Server.HtmlDecode(caseRequest.CompanyName);
                txtEmailID.Text = caseRequest.EmailID;
                txtPhoneNo.Text = caseRequest.PhoneNumber;
                lblCaseDate.Text = caseRequest.CreateDte.ToShortDateString();
            }
        }
        #endregion
    }
}