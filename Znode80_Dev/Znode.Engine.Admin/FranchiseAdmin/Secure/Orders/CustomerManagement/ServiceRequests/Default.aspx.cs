using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.ServiceRequests
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_cases_list class.
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Private Member Variables
        private string addLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/ServiceRequests/Add.aspx";
        private string notesLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/ServiceRequests/NoteAdd.aspx?itemid=";
        private string viewLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/ServiceRequests/View.aspx?itemid=";
        private string editLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/ServiceRequests/Add.aspx?itemid=";
        private string emailLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/ServiceRequests/CaseEmail.aspx?itemid=";
        private string portalIds = string.Empty;
        private DataSet caseDataSet = null;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Protected properties
        private string GridViewSortDirection
        {
            get { return ViewState["SortDirection"] as string ?? "ASC"; }

            set { ViewState["SortDirection"] = value; }
        }

        #endregion

        #region General Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get Portals 
            this.portalIds = UserStoreAccess.GetAvailablePortals;

            if (!Page.IsPostBack)
            {
                this.BindList();
                this.BindSearchData();
            }
        }

        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            txtcaseid.Text = string.Empty;
            txtfirstname.Text = string.Empty;
            txtlastname.Text = string.Empty;
            txtcompanyname.Text = string.Empty;
            txttitle.Text = string.Empty;
            this.BindList();
            this.BindSearchData();
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addLink);
        }

        protected void uxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            foreach (GridViewRow gvC in uxGrid.Rows)
            {
                Label lblCaseStatus = (Label)gvC.FindControl("lblCaseStatus");
                if (lblCaseStatus.Text == this.GetGlobalResourceObject("ZnodeAdminResource", "SubTextPending").ToString())
                {
                    lblCaseStatus.ForeColor = System.Drawing.Color.Red;
                }
                lblCaseStatus.Text = lblCaseStatus.Text.ToUpper();
            }
        }
        #endregion

        #region Grid Events

        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument Get the values from the appropriate 
                // cell in the GridView control.
                string Id = e.CommandArgument.ToString();
                if (e.CommandName == "Edit")
                {
                    this.editLink = this.editLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(Id));
                    Response.Redirect(this.editLink);
                }
                else if (e.CommandName == "AddNote")
                {
                    Response.Redirect(this.notesLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(Id)));
                }
                else if (e.CommandName == "Reply")
                {
                    Response.Redirect(this.emailLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(Id)));
                }
                else if (e.CommandName == "View")
                {
                    Response.Redirect(this.viewLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(Id)));
                }
            }
        }

        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        /// <summary>
        /// Occurs when the hyperlink to sort a column is clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            CaseAdmin caseAdmin = new CaseAdmin();
            if (Roles.IsUserInRole("ADMIN"))
            {
                this.caseDataSet = caseAdmin.SearchCase(int.Parse(ListCaseStatus.SelectedValue), txtcaseid.Text.Trim(), txtfirstname.Text.Trim(), txtlastname.Text.Trim(), txtcompanyname.Text.Trim(), txttitle.Text.Trim(), ZNodeConfigManager.SiteConfig.PortalID, this.portalIds);
            }
            else
            {
                this.caseDataSet = caseAdmin.SearchCase(int.Parse(ListCaseStatus.SelectedValue), txtcaseid.Text.Trim(), txtfirstname.Text.Trim(), txtlastname.Text.Trim(), txtcompanyname.Text.Trim(), txttitle.Text.Trim(), ZNodeConfigManager.SiteConfig.PortalID, this.portalIds);
            }

            uxGrid.DataSource = this.SortDataTable(this.caseDataSet, e.SortExpression, true);
            uxGrid.DataBind();

            if (this.GetSortDirection() == "DESC")
            {
                e.SortDirection = SortDirection.Descending;
            }
            else
            {
                e.SortDirection = SortDirection.Ascending;
            }
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Get the encrypted case Id
        /// </summary>
        /// <param name="caseId">Case Id to encrypt.</param>
        /// <returns>Returns the encrypted Id</returns>
        protected string GetEncryptId(object caseId)
        {
            return HttpUtility.UrlEncode(this.encrypt.EncryptData(caseId.ToString()));
        }

        #endregion

        #region Bind Data

        /// <summary>
        /// Bind Searched Data
        /// </summary>
        private void BindSearchData()
        {
            CaseAdmin caseAdmin = new CaseAdmin();
            this.caseDataSet = caseAdmin.SearchCase(int.Parse(ListCaseStatus.SelectedValue), txtcaseid.Text.Trim(), Server.HtmlEncode(txtfirstname.Text.Trim()), Server.HtmlEncode(txtlastname.Text.Trim()), Server.HtmlEncode(txtcompanyname.Text.Trim()), Server.HtmlEncode(txttitle.Text.Trim()), UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0), this.portalIds);

            DataView dv = new DataView(this.caseDataSet.Tables[0]);
            dv.Sort = "CaseID Desc";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Sort the dataset.
        /// </summary>
        /// <param name="dataSet">DataSet to sort.</param>
        /// <param name="gridViewSortExpression">Gridview expression.</param>
        /// <param name="isPageIndexChanging">Indicates whether the page index changed or not.</param>
        /// <returns>Returns the sorted dataview.</returns>
        private DataView SortDataTable(DataSet dataSet, string gridViewSortExpression, bool isPageIndexChanging)
        {
            if (dataSet != null)
            {
                DataView dataView = new DataView(dataSet.Tables[0]);

                if (gridViewSortExpression.Length > 0)
                {
                    if (isPageIndexChanging)
                    {
                        dataView.Sort = string.Format("{0} {1}", gridViewSortExpression, this.GridViewSortDirection);
                    }
                    else
                    {
                        dataView.Sort = string.Format("{0} {1}", gridViewSortExpression, this.GetSortDirection());
                    }
                }

                return dataView;
            }
            else
            {
                return new DataView();
            }
        }

        /// <summary>
        /// Get the sort direction 
        /// </summary>
        /// <returns>Returns the sort direction.</returns>
        private string GetSortDirection()
        {
            switch (this.GridViewSortDirection)
            {
                case "ASC":
                    this.GridViewSortDirection = "DESC";
                    break;

                case "DESC":
                    this.GridViewSortDirection = "ASC";
                    break;
            }

            return this.GridViewSortDirection;
        }

        private void BindList()
        {
            CaseAdmin _AdminAccess = new CaseAdmin();
            ListCaseStatus.DataSource = _AdminAccess.GetAllCaseStatus();
            ListCaseStatus.DataTextField = "CaseStatusNme";
            ListCaseStatus.DataValueField = "CaseStatusID";
            ListCaseStatus.DataBind();
            ListItem newItem = new ListItem();
            newItem.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString();
            newItem.Value = "-1";
            ListCaseStatus.Items.Insert(0, newItem);
            ListCaseStatus.SelectedIndex = 1;
        }

        #endregion 
    }
}