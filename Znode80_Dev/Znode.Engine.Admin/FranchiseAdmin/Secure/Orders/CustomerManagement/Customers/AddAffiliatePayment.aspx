<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers.AddAffiliatePayment" CodeBehind="AddAffiliatePayment.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" Text='<%$ Resources:ZnodeAdminResource, TitleAddPayment%>' runat="server"></asp:Label></h1>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmitTop" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelTop" />
        </div>
        <div class="ClearBoth">
            <ZNode:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="3" runat="server" />
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="AmountOwed" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleAmountOwed%>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:Label ID="lblAmountOwed" runat="server" Text="" />
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="PaymentDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePaymentDescription%>'></asp:Localize><br />
            <small>
                <asp:Localize ID="PaymentDescriptionHint" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextPaymentDescription%>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtDescription" runat="server" Columns="30" Rows="5" TextMode="MultiLine" />
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="PaymentDate" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitlePaymentDate%>"></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtDate" Text='' runat="server" />
            &nbsp;<asp:Image ID="imgbtnStartDt" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/FranchiseAdmin/Themes/images/SmallCalendar.gif" />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredPaymentDate%>'
                ControlToValidate="txtDate" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtDate"
                CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularPaymentDate%>'
                ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"></asp:RegularExpressionValidator>
            <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                runat="server" TargetControlID="txtDate">
            </ajaxToolKit:CalendarExtender>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleAmount%>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAmount" runat="server" Width="130" Columns="30" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter a amount."
                ControlToValidate="txtAmount" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtAmount"
                Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredAffiliateAmount%>'
                CssClass="Error" Display="Dynamic" />
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtAmount"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeAffiliateAmount%>' MaximumValue="99999999" Type="Currency"
                MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <asp:Label ID="ErrorMessage" runat="server" CssClass="Error"></asp:Label>
        </div>
        <ZNode:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server" />
        <div>
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit%>" ID="ImageButton1" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel%>" CausesValidation="False" ID="ImageButton2" />
        </div>
    </div>
</asp:Content>
