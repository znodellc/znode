<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers.AddAffiliateSettings"
    Title="Affiliate Info" CodeBehind="AddAffiliateSettings.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>
        <div style="text-align: right; padding-right: 10px;">
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmitTop" CausesValidation="true" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelTop" />

        </div>

        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="2" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <h4 class="SubTitle">
                    <asp:Localize ID="TrackingInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTrackingInformation%>'></asp:Localize></h4>
                <div class="HintStyle">
                    <asp:Localize ID="TrackingInformationHint" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextTrackingInformation%>'></asp:Localize>
                    <br />
                    <br />
                </div>
                <div align="left" class="ClearBoth">
                    <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label>
                </div>
                <div class="FieldStyle">
                     <asp:Label runat="server" ID="lblAffiliateLink">
                    <asp:Localize ID="TrackingUrl" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTrackingLink%>'></asp:Localize>
                   </asp:Label><br />
                    <small>
                        <asp:Localize ID="TrackingUrlHint" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextTrackingUrl%>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <asp:Label ID="lblAffiliateLinkError" runat="server" CssClass="Error"></asp:Label>
                    <asp:HyperLink ID="hlAffiliateLink" runat="server" Target="_blank"></asp:HyperLink>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="CommissionType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCommissionType%>'></asp:Localize><br />
                    <small>
                        <asp:Localize ID="CommissionTypeHint" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextCommissionType%>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="lstReferral" runat="server" OnSelectedIndexChanged="DiscountType_SelectedIndexChanged"
                        AutoPostBack="True">
                    </asp:DropDownList>
                    <asp:TextBox ID="Discount" runat="server" MaxLength="7" Columns="25"></asp:TextBox><br />
                    <asp:RegularExpressionValidator ValidationExpression="^\d*\.?\d*$" ID="discrevAmountValidator" runat="server"
                        ControlToValidate="Discount" ValidationGroup="EditContact" CssClass="Error"
                        Enabled="true" Display="Dynamic" SetFocusOnError="true" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularAffiliateDiscount%>'>
                    </asp:RegularExpressionValidator><br />
                    <asp:RangeValidator ID="discAmountValidator" runat="server" ControlToValidate="Discount"
                        ValidationGroup="EditContact" CssClass="Error" Enabled="false" Display="Dynamic"
                        MaximumValue="9999999" MinimumValue="0" CultureInvariantValues="true" Type="Currency"></asp:RangeValidator>
                    <asp:RangeValidator ID="discPercentageValidator" Enabled="false" runat="server" ControlToValidate="Discount"
                        ValidationGroup="EditContact" CssClass="Error" Display="Dynamic" MaximumValue="100"
                        CultureInvariantValues="true" MinimumValue="0" SetFocusOnError="True" Type="Double"></asp:RangeValidator>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="TaxId" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTaxId%>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtTaxId" runat="server" Width="179px"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="PartnerApprovalStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPartnerApprovalStatus%>'></asp:Localize><br />
                    <small>
                        <asp:Localize ID="PartnerApprovalStatusHint" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextPartnerApprovalStatus%>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="lstReferralStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="LstReferralStatus_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div class="FieldStyle" runat="server" id="amountOwed">
                    <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleAmountOwed%>' meta:resourcekey="AmountOwed"></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:Label ID="lblAmountOwed" runat="server" Text="" />
                </div>
                <uc1:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmitBottom" CausesValidation="true" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelBottom" />

        </div>
    </div>
</asp:Content>
