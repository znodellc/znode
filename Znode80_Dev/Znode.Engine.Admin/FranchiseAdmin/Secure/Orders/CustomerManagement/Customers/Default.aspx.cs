using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_customers_list class.
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Private Member Variables
        private static bool checkSearch = false;
        private string viewLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=";
        private string disablePageLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/Disable.aspx?itemid=";
        private string unlockPageLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/Unlock.aspx?itemid=";
        private string deletePageLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/Delete.aspx?itemid=";
        private string portalIds = string.Empty;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get Portals
            this.portalIds = UserStoreAccess.GetAvailablePortals;

            if (!Page.IsPostBack)
            {
                if (Roles.IsUserInRole("ADMIN"))
                {
                    ddlPortal.Enabled = true;
                }
                else
                {
                    ddlPortal.Visible = false;
                }

                this.BindPortal();
                this.BindTrackingStatus();
                this.BindSearchCustomer();

                if (Session["ContactList"] != null)
                {
                    Session.Remove("ContactList");
                }

                checkSearch = false;
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchCustomer();
        }

        /// <summary>
        /// Clear Search Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            // Clear Search and Redirect to same page
            string link = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/Default.aspx";
            Response.Redirect(link);
        }

        /// <summary>
        /// Add Contact Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddContact_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/Edit.aspx");
        }

        #endregion

        #region Grid Events

        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Get the Account id  stored in the CommandArgument
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Edit")
                {
                    this.viewLink = this.viewLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(Id));
                    Response.Redirect(this.viewLink);
                }

                if (e.CommandName == "Delete")
                {
                    this.deletePageLink = this.deletePageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(Id));
                    Response.Redirect(this.deletePageLink);
                }
                else if (e.CommandName == "View")
                {
                    this.viewLink = this.viewLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(Id));
                    Response.Redirect(this.viewLink);
                }
                else if (e.CommandName == "DISABLE")
                {
                    this.disablePageLink = this.disablePageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(Id));
                    Response.Redirect(this.disablePageLink);
                }
                else if (e.CommandName == "ENABLE")
                {
                    this.unlockPageLink = this.unlockPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(Id));
                    Response.Redirect(this.unlockPageLink);
                }
                else if (e.CommandName == "Unlock")
                {
                    this.unlockPageLink = this.unlockPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(Id));
                    Response.Redirect(this.unlockPageLink);
                }
            }
        }

        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;

            if (checkSearch)
            {
                if (Session["ContactList"] != null)
                {
                    DataSet contactListDataSet = Session["ContactList"] as DataSet;

                    uxGrid.DataSource = contactListDataSet;
                    uxGrid.DataBind();

                    // Release resources
                    contactListDataSet.Dispose();
                }
            }
            else
            {
                this.BindSearchCustomer();
            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Concate firstname and lastname 
        /// </summary>
        /// <param name="firstName">Customer first name</param>
        /// <param name="lastName">Customer last name.</param>
        /// <returns>Returns the customer full name</returns>
        protected string ConcatName(object firstName, object lastName)
        {
            return string.Concat(firstName, " ", lastName);
        }

        /// <summary>
        /// Get the delete button visible property
        /// </summary>
        /// <param name="userId">Current User Id</param>
        /// <returns>Returns true if user has permission to view the delete button else false.</returns>
        protected bool HideDeleteButton(object userId)
        {
            if (userId != null)
            {
                if (userId.ToString().Length > 0)
                {
                    MembershipUser user = Membership.GetUser(userId);
                    if (user == null)
                    {
                        return true;
                    }

                    if (user.UserName.ToLower().Equals(HttpContext.Current.User.Identity.Name))
                    {
                        return false;
                    }

                    string roleList = string.Empty;

                    // Get roles for this User account
                    string[] roles = Roles.GetRolesForUser(user.UserName);

                    foreach (string Role in roles)
                    {
                        roleList += Role + ",";
                    }

                    string roleName = roleList;

                    if (Roles.IsUserInRole("CUSTOMER SERVICE REP"))
                    {
                        if (roleName == Convert.ToString("USER,") || roleName == string.Empty)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else if (roleList.Contains("ADMIN"))
                    {
                        if (user.UserName.Equals(HttpContext.Current.User.Identity.Name))
                        {
                            return false;
                        }
                        else if (Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "ADMIN"))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Hides the disable or enable button
        /// </summary>
        /// <param name="userId">Current User Id</param>
        /// <param name="isEnableButton">Indicates whether to enable or disable Enable button</param>
        /// <returns>Returns true if user has permission to view the lock/unlock button else false.</returns>
        protected bool HideLockUnlockButton(object userId, bool isEnableButton)
        {
            bool isHidden = false;

            if (userId != null)
            {
                if (userId.ToString().Length > 0)
                {
                    bool isAdmin = Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "ADMIN");

                    if (isAdmin
                        || Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "CUSTOMER SERVICE REP"))
                    {
                        MembershipUser user = Membership.GetUser(userId);

                        // Hide the button if the account is not found.
                        if (user != null)
                        {
                            // Hide the button if I am editing my own account so I can't lock myself out.
                            if (!user.UserName.ToLower().Equals(HttpContext.Current.User.Identity.Name))
                            {
                                if (isEnableButton)
                                {
                                    // We are looking at the enable button, Enable it if the user is locked out.
                                    isHidden = user.IsLockedOut || !user.IsApproved;
                                }
                                else
                                {
                                    // The Unlock button must always be the oposite of the Enable button.
                                    isHidden = !user.IsLockedOut || !user.IsApproved;
                                }
                            }

                            string roleList = string.Empty;

                            // Get roles for this User account
                            string[] roles = Roles.GetRolesForUser(user.UserName);

                            foreach (string role in roles)
                            {
                                roleList += role + "<br>";
                            }

                            string roleName = roleList;

                            if (Roles.IsUserInRole("CUSTOMER SERVICE REP"))
                            {
                                if (roleName == Convert.ToString("USER<br>") || roleName == string.Empty)
                                {
                                    isHidden = true;
                                }
                                else
                                {
                                    isHidden = false;
                                }
                            }

                            // Don't let non-Admins disable Admin Accounts.
                            if (!isAdmin && Roles.IsUserInRole(user.UserName, "ADMIN"))
                            {
                                isHidden = false;
                            }
                        }
                    }
                }
            }

            return isHidden;
        }

        /// <summary>
        /// Hide the Edit button for admin accounts if the user is not an Admin
        /// </summary>
        /// <param name="userId">Current user Id</param>
        /// <returns>Returns true if user has permission to view the edit button else false.</returns>
        protected bool HideEditButton(object userId)
        {
            if (userId != null)
            {
                if (userId.ToString().Length > 0)
                {
                    MembershipUser user = Membership.GetUser(userId);

                    if (user == null)
                    {
                        return true;
                    }

                    string roleList = string.Empty;

                    // Get roles for this User account
                    string[] roles = Roles.GetRolesForUser(user.UserName);

                    foreach (string role in roles)
                    {
                        roleList += role + "<br>";
                    }

                    string roleName = roleList;

                    if (!Roles.IsUserInRole("ADMIN"))
                    {
                        if (Roles.IsUserInRole("CUSTOMER SERVICE REP"))
                        {
                            if (roleName == Convert.ToString("USER<br>") || roleName == string.Empty)
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else if (roleName == Convert.ToString("ADMIN<br>"))
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Gets the Name of the Role for this user Id
        /// </summary>
        /// <param name="userId">UserId Id to get the role name</param>
        /// <returns>Returns the users role name.</returns>
        protected string GetRoleName(object userId)
        {
            if (userId != null)
            {
                string roleList = string.Empty;

                if (userId.ToString().Length > 0)
                {
                    MembershipUser user = Membership.GetUser(userId);

                    if (user == null)
                    {
                        return string.Empty;
                    }

                    // Get roles for this User account
                    string[] roles = Roles.GetRolesForUser(user.UserName);

                    foreach (string role in roles)
                    {
                        roleList += role + "<br>";
                    }
                }

                return roleList;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the Name of the Profile for this ProfileID
        /// </summary>
        /// <param name="profileId">Profile Id to get the profile object.</param>
        /// <returns>Returns the profile name.</returns>
        protected string GetProfileName(object profileId)
        {
            ProfileAdmin profileAdmin = new ProfileAdmin();
            if (profileId == null)
            {
                return "All Profile";
            }
            else
            {
                Profile profile = profileAdmin.GetByProfileID(int.Parse(profileId.ToString()));
                if (profile != null)
                {
                    return profile.Name;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Encrypt the account Id
        /// </summary>
        /// <param name="accountId">Account Id to encrypt.</param>
        /// <returns>Returns the encrypted account Id</returns>
        protected string GetEncryptId(object accountId)
        {
            return HttpUtility.UrlEncode(this.encrypt.EncryptData(accountId.ToString()));
        }

        /// <summary>
        /// Bind Portals
        /// </summary>        
        private void BindPortal()
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portalList = portalAdmin.GetAllPortals();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess.Length > 0 && string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                portalList.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
            }
            else
            {
                Portal po = new Portal();
                po.StoreName = this.GetGlobalResourceObject("ZnodeAdminResource","DropDownTextAllStores").ToString();
                portalList.Insert(0, po);
            }

            // Portal Drop Down List
            ddlPortal.DataSource = portalList;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();
        }

        /// <summary>
        /// Return DataSet for a given Input
        /// </summary>        
        private void BindSearchCustomer()
        {
            // Create Instance for customer helper class
            CustomerHelper customerHelper = new CustomerHelper();
            DataSet customerListDataSet = customerHelper.SearchCustomer(
                txtFName.Text.Trim(),
                txtLName.Text.Trim(),
                txtComapnyNM.Text.Trim(),
                txtLoginName.Text.Trim(),
                txtExternalaccountno.Text.Trim(),
                txtContactID.Text.Trim(),
                txtStartDate.Text.Trim(),
                txtEndDate.Text.Trim(),
                txtPhoneNumber.Text.Trim(),
                txtEmailID.Text.Trim(),
                UserStoreAccess.GetTurnkeyStoreProfileID.ToString(),
                lstReferralStatus.SelectedValue,
                ddlPortal.SelectedValue);

            Session["ContactList"] = customerListDataSet;
            if (customerListDataSet.Tables[0].Rows.Count > 0)
            {
                checkSearch = true;
            }

            uxGrid.DataSource = customerListDataSet.Tables[0];
            uxGrid.DataBind();
            uxGrid.Columns[1].Visible = ZNodeConfigManager.SiteConfig.EnableCustomerPricing.GetValueOrDefault();
        }
        #endregion

        #region Bind Grid Data
        /// <summary>
        /// Binds Tracking Status drop-down list
        /// </summary>
        private void BindTrackingStatus()
        {
            Array names = Enum.GetNames(typeof(ZNodeApprovalStatus.ApprovalStatus));
            Array values = Enum.GetValues(typeof(ZNodeApprovalStatus.ApprovalStatus));

            // Clear list items
            lstReferralStatus.Items.Clear();

            // Add default value to the item
            lstReferralStatus.Items.Add(new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource","DropdownTextAll").ToString().ToUpper(), string.Empty));

            for (int index = 0; index < names.Length; index++)
            {
                ListItem listitem = new ListItem(ZNodeApprovalStatus.GetEnumValue(values.GetValue(index)), names.GetValue(index).ToString());
                lstReferralStatus.Items.Add(listitem);
            }
        }

        /// <summary>
        /// Bind account list.
        /// </summary>
        private void BindList()
        {
            DataSet ds = new DataSet();
            AccountAdmin accountAdmin = new AccountAdmin();
            PortalAdmin portalAdmin = new PortalAdmin();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                ds = accountAdmin.GetAccountByPortal(profiles.StoreAccess);
            }
            else
            {
                ds = accountAdmin.GetAllCustomers();
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                uxGrid.DataSource = ds.Tables[0];
                uxGrid.DataBind();
            }
        }

        #endregion
        public string GetActiveInd(object userId)
        {
            if (userId != null)
            {
                string roleList = string.Empty;

                if (userId.ToString().Length > 0)
                {
                    MembershipUser _user = Membership.GetUser(userId);

                    if (_user == null)
                    {
                        return string.Empty;
                    }
                    if (!_user.IsApproved || _user.IsLockedOut)
                        return "ENABLE";
                    else
                        return "DISABLE";
                }

                return string.Empty;
            }
            else
            {
                return string.Empty;
            }
        }
    }
}