<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers.Default" Title="Untitled Page" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div align="center">
        <div>
            <div class="LeftFloat" style="width: 70%; text-align: left">
                <h1>
                    <asp:Localize ID="TitleCustomers" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleCustomers %>'></asp:Localize></h1>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="LeftFloat">
                <p>
                    <asp:Localize ID="TextCustomer" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCustomer %>'></asp:Localize>
                </p>
            </div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="AddContact" runat="server" ButtonType="Button" OnClick="AddContact_Click" CausesValidation="false" Text='<%$ Resources:ZnodeAdminResource, ButtonAddCustomer %>' ButtonPriority="Primary" />
            </div>
            <div class="ClearBoth">
            </div>
            <div align="left">
                <h4 class="SubTitle">
                    <asp:Localize ID="SearchCustomersSubTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchCustomers %>'></asp:Localize></h4>
                <asp:Panel ID="SearchCustomers" DefaultButton="btnSearch" runat="Server">
                    <div class="SearchForm">
                        <div class="RowStyle">
                            <div class="ItemStyle">
                                <span class="FieldStyle" style="display: none">
                                    <asp:Label ID="lbStoreName" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleStoreName%>" runat="server"></asp:Label></span><br />
                                <span class="ValueStyle">
                                    <asp:DropDownList ID="ddlPortal" runat="server"></asp:DropDownList>
                                </span>
                            </div>
                        </div>
                        <div class="RowStyle">
                            <div class="ItemStyle">
                                <span class="FieldStyle"><asp:Localize ID="FirstName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFirstName%>'></asp:Localize></span><br />
                                <span class="ValueStyle">
                                    <asp:TextBox ID="txtFName" runat="server"></asp:TextBox></span>
                            </div>
                            <div class="ItemStyle">
                                <span class="FieldStyle"><asp:Localize ID="LastName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleLastName%>'></asp:Localize></span><br />
                                <span class="ValueStyle">
                                    <asp:TextBox ID="txtLName" runat="server"></asp:TextBox></span>
                            </div>
                            <div class="ItemStyle">
                                <span class="FieldStyle"> <asp:Localize ID="CompanyName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCompanyName%>'></asp:Localize></span><br />
                                <span class="ValueStyle">
                                    <asp:TextBox ID="txtComapnyNM" runat="server"></asp:TextBox></span>
                            </div>
                        </div>
                        <div class="RowStyle">
                            <div class="ItemStyle">
                                <span class="FieldStyle"><asp:Localize ID="LoginName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleLoginName%>'></asp:Localize></span><br />
                                <span class="ValueStyle">
                                    <asp:TextBox ID="txtLoginName" runat="server"></asp:TextBox></span>
                            </div>
                            <div class="ItemStyle">
                                <span class="FieldStyle"><asp:Localize ID="AccountNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCusotmerNumber%>'></asp:Localize></span><br />
                                <span class="ValueStyle">
                                    <asp:TextBox ID="txtExternalaccountno" runat="server"></asp:TextBox></span>
                            </div>
                            <div class="ItemStyle">
                                <span class="FieldStyle"><asp:Localize ID="ContactID" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleContactID %>'></asp:Localize></span><br />
                                <span class="ValueStyle">
                                    <asp:TextBox ID="txtContactID" runat="server"></asp:TextBox></span>
                            </div>
                        </div>
                        <div class="RowStyle">
                            <div class="ItemStyle">
                                <span class="FieldStyle"><asp:Localize ID="PhoneNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePhoneNumber%>'></asp:Localize></span><br />
                                <span class="ValueStyle">
                                    <asp:TextBox ID="txtPhoneNumber" runat="server"></asp:TextBox></span>
                            </div>
                            <div class="ItemStyle">
                                <span class="FieldStyle"><asp:Localize ID="EmailId" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEmailID%>'></asp:Localize></span><br />
                                <span class="ValueStyle">
                                    <asp:TextBox ID="txtEmailID" runat="server"></asp:TextBox></span>
                            </div>

                        </div>
                        <div class="RowStyle">
                            <div class="ItemStyle">
                                <span class="FieldStyle"><asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStartDate%>'></asp:Localize><small><asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleSubTextStartDate%>"></asp:Localize></small></span><br />
                                <span class="ValueStyle">
                                    <asp:TextBox ID="txtStartDate" Text="" runat="server"></asp:TextBox>
                                    <asp:Image ID="imgbtnStartDt" runat="server" Style="cursor: pointer; vertical-align: top"
                                        ImageUrl="~/FranchiseAdmin/Themes/images/SmallCalendar.gif"></asp:Image>
                                    <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                                        runat="server" TargetControlID="txtStartDate">
                                    </ajaxToolKit:CalendarExtender>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
                                        CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularStartDate%>'
                                        ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"></asp:RegularExpressionValidator>
                                </span>
                            </div>
                            <div class="ItemStyle">
                                <span class="FieldStyle"><asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEndDate%>'></asp:Localize><small><asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleSubTextStartDate%>"></asp:Localize></small></span><br />
                                <span class="ValueStyle">
                                    <asp:TextBox ID="txtEndDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="ImgbtnEndDt"
                                        runat="server" ImageAlign="AbsMiddle" ImageUrl="~/FranchiseAdmin/Themes/images/SmallCalendar.gif" />
                                    <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt"
                                        runat="server" TargetControlID="txtEndDate">
                                    </ajaxToolKit:CalendarExtender>

                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEndDate"
                                        CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularStartDate%>'
                                        ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"></asp:RegularExpressionValidator>
                                    <br />
                                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtStartDate"
                                        ControlToValidate="txtEndDate" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareBeginDate%>'
                                        Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator><br/>
                                </span>
                            </div>
                            <div class="ItemStyle">
                                <span class="FieldStyle"><asp:Localize ID="PartnerApprovalStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPartnerApprovalStatus %>'></asp:Localize></span><br />
                                <span class="ValueStyle">
                                    <asp:DropDownList ID="lstReferralStatus" runat="server"></asp:DropDownList>
                                </span>
                            </div>
                        </div>
                        <div class="ClearBoth">
                           <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnClearSearch_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonClear%>" CausesValidation="False" ID="btnClearSearch" />
                           <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSearch%>" ID="btnSearch" />
                        </div>
                        <asp:Label ID="lblError" runat="server" Text="" CssClass="Error"></asp:Label>
                    </div>
                </asp:Panel>

                <h4 class="GridTitle"><asp:Localize ID="CustomerGridTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleCustomer %>'></asp:Localize></h4>
            </div>
        </div>
        <div>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" Width="100%" ForeColor="#333333"
                CellPadding="4" DataKeyNames="accountid" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundCustomer%>'
                OnRowCommand="UxGrid_RowCommand" OnPageIndexChanging="UxGrid_PageIndexChanging"
                PageSize="50" AllowPaging="True" GridLines="None" AutoGenerateColumns="False"
                CaptionAlign="Left">
                <Columns>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCustomerID%>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:HyperLink runat="server" ID="editaccount" NavigateUrl='<%# "~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + GetEncryptId(DataBinder.Eval(Container.DataItem,"AccountId")) %>' Visible='<%# HideEditButton(DataBinder.Eval(Container.DataItem,"UserId")) %>' Text='<%# Eval("AccountId") %>'></asp:HyperLink>
                            <asp:Label runat="server" ID="editaccountid" Visible='<%# !HideEditButton(DataBinder.Eval(Container.DataItem,"UserId")) %>' Text='<%# Eval("AccountId") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCustomerBasedPricing%>' HeaderStyle-HorizontalAlign="Left" Visible="False" HeaderStyle-Width="90" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "EnableCustomerPricing").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleFullName%>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblFullName" runat="Server" Text='<%# ConcatName(Eval("BillingFirstName"),Eval("BillingLastName")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="BillingPhoneNumber" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePhoneNumber%>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleEmailID%>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <a href='<%# "mailto:" + Eval("BillingEmailID") %>'>
                                <%# DataBinder.Eval(Container.DataItem,"BillingEmailID") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleActions%>' HeaderStyle-HorizontalAlign="Left">
                        <ItemStyle Wrap="false" />
                        <ItemTemplate>
                            <asp:LinkButton ID="ViewCustomer" Text='<%$ Resources:ZnodeAdminResource, LinkManage%>' CommandName="View" CssClass="LinkButton"
                                CommandArgument='<%# DataBinder.Eval(Container.DataItem,"AccountId") %>' Visible='<%# HideEditButton(DataBinder.Eval(Container.DataItem,"UserId")) %>'
                                runat="server" />&nbsp;&nbsp;
                                <asp:LinkButton ID="DeleteCustomer" Text='<%$ Resources:ZnodeAdminResource, LinkDelete%>' CommandName="Delete" CssClass="LinkButton"
                                    CommandArgument='<%# DataBinder.Eval(Container.DataItem,"AccountId") %>' Visible='<%# HideDeleteButton(DataBinder.Eval(Container.DataItem,"UserId")) %>'
                                    runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
