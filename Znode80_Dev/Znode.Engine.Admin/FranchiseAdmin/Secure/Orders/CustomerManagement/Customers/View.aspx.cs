using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_customers_view class.
    /// </summary>
    public partial class View : System.Web.UI.Page
    {
        #region Private Member Variables
        private string _viewOrderLink = "~/FranchiseAdmin/Secure/Orders/OrderManagement/ViewOrders/View.aspx?itemid=";
        private int _accountId;
        private string _mode = string.Empty;
        private readonly ZNodeEncryption _encryption = new ZNodeEncryption();
        private TList<Domain> _domainList;
        #endregion

        #region Protected Bind Methods
        /// <summary>
        /// Bind the account data
        /// </summary>
        protected void BindData()
        {
            var accountAdmin = new AccountAdmin();
            var customerAdmin = new CustomerAdmin();

            var account = accountAdmin.GetByAccountID(this._accountId);

            if (account != null)
            {
                var defaultBillingAddress = accountAdmin.GetDefaultBillingAddress(this._accountId) ?? new Address();

                // General Information
                lblName.Text = defaultBillingAddress.FirstName + " " + defaultBillingAddress.LastName;
                lblPhone.Text = defaultBillingAddress.PhoneNumber;
                lblAccountID.Text = account.AccountID.ToString();
                lblExternalAccNumber.Text = account.ExternalAccountNo;
                lblDescription.Text = account.Description;
                lblLoginName.Text = customerAdmin.GetByUserID(int.Parse(this._accountId.ToString()));
                lblCompanyName.Text = account.CompanyName;
                CustomerBasedPriceEnable.Src = Helper.GetCheckMark(account.EnableCustomerPricing.GetValueOrDefault());

                lblCustomerDetails.Text = account.AccountID + " - " + defaultBillingAddress.FirstName + " " + defaultBillingAddress.LastName;
                lblWebSite.Text = account.Website;
                lblSource.Text = account.Source;
                lblCreatedDate.Text = account.CreateDte.ToShortDateString();
                lblCreatedUser.Text = account.CreateUser;

                // Get Referral Type Name for a Account
                lblReferralType.Text = account.ReferralCommissionTypeID != null ? this.GetCommissionTypeById(account.ReferralCommissionTypeID.ToString()) : string.Empty;

                if (account.ReferralStatus == "A")
                {
                    // Get the domain details
                    var domainAdmin = new DomainAdmin();
                    this._domainList = domainAdmin.GetDomainByPortalID(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0));
                    if (this._domainList.Count > 0)
                    {
                        var affiliateLink = "http://" + this._domainList[0].DomainName + "/Default.aspx?affiliate_id=" + account.AccountID;
                        hlAffiliateLink.Text = affiliateLink;
                        hlAffiliateLink.NavigateUrl = affiliateLink;
                    }
                    else
                    {
                        dvTrackingLink.Visible = false;
                        dvTrackingLinkError.Visible = true;
                        lblErrorMsg.Visible = true;
                        lblErrorMsg.Text = string.Empty;
                        lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAffiliateLink").ToString();
                    }
                }
                else
                {
                    hlAffiliateLink.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "HyperLinkNA").ToString();
                }

                if (account.ReferralCommission != null)
                {
                    if (account.ReferralCommissionTypeID == 1)
                    {
                        lblReferralCommission.Text = account.ReferralCommission.Value.ToString("N");
                    }
                    else
                    {
                        lblReferralCommission.Text = account.ReferralCommission.Value.ToString("c");
                    }
                }
                else
                {
                    lblReferralCommission.Text = string.Empty;
                }

                lblTaxId.Text = account.TaxID;
                lblReferralStatus.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTextInactive").ToString();
                if (account.ReferralStatus != null)
                {
                    // Getting the Status Description 
                    var values = Enum.GetValues(typeof(ZNodeApprovalStatus.ApprovalStatus));
                    var names = Enum.GetNames(typeof(ZNodeApprovalStatus.ApprovalStatus));
                    for (int i = 0; i < names.Length; i++)
                    {
                        if (names.GetValue(i).ToString() != account.ReferralStatus) continue;
                        lblReferralStatus.Text = ZNodeApprovalStatus.GetEnumValue(values.GetValue(i));
                        break;
                    }

                    this.BindPayments(accountAdmin);
                }
                else
                {
                    pnlAffiliatePayment.Visible = false;
                    lblReferralStatus.Text = string.Empty;
                }

                if (account.UpdateDte != null)
                {
                    lblUpdatedDate.Text = account.UpdateDte.Value.ToShortDateString();
                }

                // Email Opt-In
                if (account.EmailOptIn)
                {
                    EmailOptin.Src = Helper.GetCheckMark(true);
                }
                else
                {
                    EmailOptin.Src = Helper.GetCheckMark(false);
                }

                lblEmail.Text = account.Email;
                lblUpdatedUser.Text = account.UpdateUser;
                lblCustom1.Text = account.Custom1;
                lblCustom2.Text = account.Custom2;
                lblCustom3.Text = account.Custom3;

                // To get Amount owed 
                var accountHelper = new AccountHelper();
                var commissionDataSet = accountHelper.GetCommisionAmount(ZNodeConfigManager.SiteConfig.PortalID, account.AccountID.ToString());

                lblAmountOwed.Text = "$" + commissionDataSet.Tables[0].Rows[0]["CommissionOwed"].ToString();

                // Orders Grid
                this.BindGrid();

                this.BindReferralCommission();

                // Retrieves the Role for User using Userid
                if (account.UserID != null)
                {
                    var userGuid = new Guid();
                    userGuid = account.UserID.Value;
                    var user = Membership.GetUser(userGuid);
                    string roleList = string.Empty;

                    // Get Profile Store Access using userName.
                    var newProfile = new Znode.Engine.Common.ProfileCommon();

                    newProfile = newProfile.GetProfile(user.UserName);

                    // Get roles for this User account
                    var roles = Roles.GetRolesForUser(user.UserName);

                    roleList = roles.Aggregate(roleList, (current, role) => current + (role + "<br>"));

                    var roleName = roleList;

                    // If the user is Admin role, then show permissions tab
                    if (Roles.IsUserInRole("ADMIN"))
                    {
                        tabpnlOrders.Visible = true;
                    }

                    // Hide the Edit button if a NonAdmin user has entered this page
                    if (!Roles.IsUserInRole("ADMIN"))
                    {
                        if (Roles.IsUserInRole(user.UserName, "ADMIN"))
                        {
                            EditCustomer.Visible = false;
                        }
                        else if (Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "CUSTOMER SERVICE REP"))
                        {
                            if (roleName == Convert.ToString("USER<br>") || roleName == string.Empty)
                            {
                                EditCustomer.Visible = true;
                            }
                            else
                            {
                                EditCustomer.Visible = false;
                            }
                        }
                    }
                }

                // Bind address list
                this.BindAddresses();
            }
        }

        /// <summary>
        /// Get the commission type by Id
        /// </summary>
        /// <param name="commissionTypeId">Commission Type ID</param>
        /// <returns>Returns the commission type name.</returns>
        protected string GetCommissionTypeById(object commissionTypeId)
        {
            string commissionType = string.Empty;
            if (commissionTypeId.ToString() != string.Empty)
            {
                var referralCommissionAdmin = new ReferralCommissionAdmin();
                var referralType = referralCommissionAdmin.GetByReferralID(Convert.ToInt32(commissionTypeId));
                commissionType = referralType.Name;
            }

            return commissionType;
        }
        #endregion

        #region General Events
        /// <summary>
        /// download button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            var csv = new DataDownloadAdmin();
            if (Session["CustomerPricingList"] == null)
                BindSearchCustomerPricingProduct();

            var ds = (DataSet)Session["CustomerPricingList"];
            // Set Formatted Data from DataSet           
            var strData = csv.Export(ds, true, ",");

            var data = Encoding.ASCII.GetBytes(strData);

            Response.Clear();

            // Set as Excel as the primary format
            Response.AddHeader("Content-Type", "application/Excel");

            Response.AddHeader("Content-Disposition", "attachment;filename=CustomerPricing.csv");
            Response.ContentType = "application/vnd.xls";
            Response.BinaryWrite(data);

            Response.End();
        }
        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchCustomerPricingProduct();
        }

        /// <summary>
        /// Clear button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtproductname.Text = string.Empty;
            txtProductCode.Text = string.Empty;
            txtSKU.Text = string.Empty;
            txtCategory.Text = string.Empty;
            txtCategory.Value = "0";
            txtManufacturer.Text = string.Empty;
            txtManufacturer.Value = "0";

            this.BindSearchCustomerPricingProduct();
        }


        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this._accountId = Request.Params["itemid"] != null ? Convert.ToInt32(this._encryption.DecryptData(Request.Params["itemid"].ToString())) : 0;

            if (Request.Params["Mode"] != null)
            {
                this._mode = Request.Params["Mode"].ToString();
            }

            if (IsPostBack) return;
            this.BindData();
            this.BindNotes();
            this.ResetTabs();
            this.BindSearchCustomerPricingProduct();
        }

        /// <summary>
        /// CustomerList Button  Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CustomerList_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        /// <summary>
        /// Edit Customer Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CustomerEdit_Click(object sender, EventArgs e)
        {
            Response.Redirect("Edit.aspx?mode=0&itemid=" + HttpUtility.UrlEncode(this._encryption.EncryptData(this._accountId.ToString())));
        }

        /// <summary>
        /// Edit Tracking Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void TrackingEdit_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddAffiliateSettings.aspx?mode=1&itemid=" + HttpUtility.UrlEncode(this._encryption.EncryptData(this._accountId.ToString())));
        }

        /// <summary>
        /// Edit Permission Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void PermissionEdit_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddPermission.aspx?itemid=" + HttpUtility.UrlEncode(this._encryption.EncryptData(this._accountId.ToString())));
        }

        /// <summary>
        /// Add Note Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddNewNote_Click(object sender, EventArgs e)
        {
            Response.Redirect("NoteAdd.aspx?itemid=" + HttpUtility.UrlEncode(this._encryption.EncryptData(this._accountId.ToString())));
        }

        /// <summary>
        /// Add Payment button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddAffiliatePayment_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddAffiliatePayment.aspx?accId=" + HttpUtility.UrlEncode(this._encryption.EncryptData(this._accountId.ToString())));
        }

        protected void AddProfile_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddProfile.aspx?itemid=" + HttpUtility.UrlEncode(this._encryption.EncryptData(this._accountId.ToString())));
        }

        protected void BtnAddNewAddress_Click(object sender, EventArgs e)
        {
            Response.Redirect("EditAddress.aspx?accountid=" + HttpUtility.UrlEncode(this._encryption.EncryptData(this._accountId.ToString())));
        }

        #endregion

        #region Gridview Events

        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridPortalAccounts_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Seventh column.
                var deleteButton = (Button)e.Row.Cells[2].FindControl("btnDelete");

                // Set the Button's CommandArgument property with the row's index.
                deleteButton.CommandArgument = e.Row.RowIndex.ToString();

                // Add Client Side confirmation
                deleteButton.OnClientClick = "return confirm('" + this.GetGlobalResourceObject("ZnodeAdminResource", "ConfirmDelete").ToString() + "');";
            }
        }

        #endregion

        #region Related to Profile Grid Events
        /// <summary>
        /// Referral commission Items Page Index Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxReferralCommission_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxReferralCommission.PageIndex = e.NewPageIndex;
            this.BindReferralCommission();
        }


        protected void uxReferralCommission_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var encryption = new ZNodeEncryption();
            if (e.CommandName != "Page")
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                string id = e.CommandArgument.ToString();

                if (e.CommandName == "ViewID")
                {
                    this._viewOrderLink = this._viewOrderLink + HttpUtility.UrlEncode(encryption.EncryptData(id)) +
                                          "&Pg=" + HttpUtility.UrlEncode(encryption.EncryptData("List"));
                    Response.Redirect(this._viewOrderLink);
                }
            }

        }


        protected void GvAddress_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAddress.PageIndex = e.NewPageIndex;

            this.BindAddresses();
        }

        protected void GvAddress_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var addressId = Convert.ToInt32(e.CommandArgument);
            if (e.CommandName != "Page")
            {
                if (e.CommandName == "Edit")
                {
                    string editAddressUrl =
                        string.Format("EditAddress.aspx?mode=0&itemid=" +
                                      HttpUtility.UrlEncode(this._encryption.EncryptData(addressId.ToString())) +
                                      "&accountid=" +
                                      HttpUtility.UrlEncode(this._encryption.EncryptData(this._accountId.ToString())));
                    Response.Redirect(editAddressUrl);
                }

                if (e.CommandName == "Remove")
                {
                    this.DeleteAddress(addressId);
                }
            }

        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Customer Pricing Grid page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void uxCustomerPricing_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxCustomerPricing.PageIndex = e.NewPageIndex;
            this.BindSearchCustomerPricingProduct();
        }
        /// <summary>
        /// User account grid page index changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        /// <summary>
        /// Payment list grid page index changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxPaymentList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxPaymentList.PageIndex = e.NewPageIndex;
            this.BindPayments();
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Delete the selected address.
        /// </summary>
        /// <param name="addressId">Address Id</param>
        protected void DeleteAddress(int addressId)
        {
            AddressService addressService = new AddressService();
            AccountAdmin accountAdmin = new AccountAdmin();
            TList<Address> addressList = addressService.GetByAccountID(this._accountId);
            int addressCount = addressList.Count;

            // If account has more than one address then validate for default address.
            if (addressCount > 0)
            {
                // Validate default shipping address.
                addressList.ApplyFilter(delegate(Address a) { return a.IsDefaultBilling == true && a.AddressID == addressId; });
                if (addressList.Count == 1)
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDefaultBilling").ToString();
                    return;
                }

                addressList.RemoveFilter();

                // Validate default shipping address.
                addressList.ApplyFilter(delegate(Address a) { return a.IsDefaultShipping == true && a.AddressID == addressId; });
                if (addressList.Count == 1)
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDefaultShipping").ToString();
                    return;
                }
            }

            addressService.Delete(addressId);

            this.BindAddresses();
            this.ClearBillingDetails();
        }

        /// <summary>
        /// Bind the user account list.
        /// </summary>
        /// <param name="addressId">Address id to get the address object</param>
        /// <returns>Returns the address text.</returns>
        protected string GetAddress(string addressId)
        {
            string addressText = string.Empty;

            AddressService addressService = new AddressService();
            Address address = addressService.GetByAddressID(Convert.ToInt32(addressId));
            if (address != null)
            {
                addressText = address.ToString();
            }

            return addressText;
        }

        /// <summary>
        /// Format the Price with two decimal
        /// </summary>
        /// <param name="price">The price value to format.</param>
        /// <returns>Returns the formatted price</returns>
        protected string FormatPrice(object price)
        {
            if (price == DBNull.Value)
            {
                return string.Empty;
            }
            else
            {
                if (price.ToString().Length == 0)
                {
                    return string.Empty;
                }
                else
                {
                    return "$" + price.ToString().Substring(0, price.ToString().Length - 2);
                }
            }
        }

        /// <summary>
        /// Display the Order State name for a Order state
        /// </summary>
        /// <param name="orderStateId">OrderStateId to get the order state object</param>
        /// <returns>Returns the order state name</returns>
        protected string DisplayOrderStatus(object orderStateId)
        {
            ZNode.Libraries.Admin.OrderAdmin orderStateAdmin = new OrderAdmin();
            OrderState orderState = orderStateAdmin.GetByOrderStateID(int.Parse(orderStateId.ToString()));
            return orderState.OrderStateName.ToString();
        }

        /// <summary>
        /// Format the customer note.
        /// </summary>
        /// <param name="field1">Custom field1 to format.</param>
        /// <param name="field2">Custom field2 to format.</param>
        /// <param name="field3">Custom field3 to format.</param>
        /// <returns>Returns the formatted custom field.</returns>
        protected string FormatCustomerNote(object field1, object field2, object field3)
        {
            return "<b>" + field1.ToString() + "</b>  [created by " + field2.ToString() + " on " + Convert.ToDateTime(field3).ToShortDateString() + "]";
        }

        /// <summary>
        /// Get the store name for the portal Id
        /// </summary>
        /// <param name="portalId">Portal Id to get the portal object</param>
        /// <returns>Returns the store name.</returns>
        protected string GetStoreName(object portalId)
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            Portal portal = portalAdmin.GetByPortalId(int.Parse(portalId.ToString()));

            if (portal != null)
            {
                return portal.StoreName;
            }

            return string.Empty;
        }

        /// <summary>
        /// Get the company name for the portal Id
        /// </summary>
        /// <param name="portalId">Portal Id to get the portal object</param>
        /// <returns>Returns the company name.</returns>
        protected string GetStoreTitle(object portalId)
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            Portal portal = portalAdmin.GetByPortalId(int.Parse(portalId.ToString()));

            if (portal != null)
            {
                return portal.CompanyName;
            }

            return string.Empty;
        }

        /// <summary>
        /// Reset the user tab.
        /// </summary>
        private void ResetTabs()
        {
            tabCustomerDetails.ActiveTabIndex = 0;

            if (this._mode.Equals("affiliate"))
            {
                tabCustomerDetails.ActiveTabIndex = 2;
            }
            else if (this._mode.Equals("notes"))
            {
                tabCustomerDetails.ActiveTabIndex = 1;
            }
            else
            {
                tabCustomerDetails.ActiveTabIndex = 0;
            }
        }

        #endregion

        #region Bind Methods


        /// <summary>
        /// Bind Customer pricing Product list
        /// </summary>
        private void BindSearchCustomerPricingProduct()
        {

            // Create Instance for Customer HElper class
            var helperAccess = new ProductHelper();
            var ds = helperAccess.SearchCustomerPricingProduct(
                Server.HtmlEncode(txtproductname.Text.Trim()),
                txtProductCode.Text.Trim(),
                txtSKU.Text.Trim(),
                txtManufacturer.Value,
                txtCategory.Value,
                UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0),
                this._accountId);

            Session["CustomerPricingList"] = ds;
            uxCustomerPricing.DataSource = ds.Tables[0];
            uxCustomerPricing.DataBind();

            btnDownload.Visible = ds.Tables[0].Rows.Count > 0;

        }


        /// <summary>
        /// Bind Order Grid
        /// </summary>
        private void BindGrid()
        {
            var orderAdmin = new OrderAdmin();
            var orderList = orderAdmin.GetByAccountID(this._accountId);

            uxGrid.DataSource = orderList;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind the account referral commision grid 
        /// </summary>    
        private void BindReferralCommission()
        {
            var referralCommissionAdmin = new ReferralCommissionAdmin();
            var referralCommissionList = new TList<ReferralCommission>();

            // Gets the referral commission details
            if (this._accountId != 0)
            {
                referralCommissionList = referralCommissionAdmin.GetReferralCommissionByAccountID(this._accountId);
            }

            uxReferralCommission.DataSource = referralCommissionList;
            uxReferralCommission.DataBind();
        }

        /// <summary>
        /// Bind payments Grid
        /// </summary>
        private void BindPayments()
        {
            var accountAdmin = new AccountAdmin();
            this.BindPayments(accountAdmin);
        }

        /// <summary>
        /// Bind the payment grid
        /// </summary>
        /// <param name="accountAdmin">Account admin object</param>
        private void BindPayments(AccountAdmin accountAdmin)
        {
            uxPaymentList.DataSource = accountAdmin.GetPaymentsByAccountId(this._accountId);
            uxPaymentList.DataBind();

            pnlAffiliatePayment.Visible = true;
        }

        /// <summary>
        /// //Bind Repeater
        /// </summary>
        private void BindNotes()
        {
            var noteAdmin = new NoteAdmin();
            var noteList = noteAdmin.GetByAccountID(this._accountId);
            noteList.Sort("NoteID Desc");
            CustomerNotes.DataSource = noteList;
            CustomerNotes.DataBind();
        }

        /// <summary>
        /// Bind the user account list.
        /// </summary>
        private void BindAddresses()
        {
            var addressService = new AddressService();
            var addressList = addressService.GetByAccountID(this._accountId);

            addressList.Sort("Name ASC");
            gvAddress.DataSource = addressList;
            gvAddress.DataBind();
        }
        /// <summary>
        /// clearing the billingdetails
        /// </summary>
        private void ClearBillingDetails()
        {
            lblName.Text = string.Empty;
            lblPhone.Text = string.Empty;
        }

        #endregion


    }
}