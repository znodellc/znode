<%@ Page Language="C#" MaintainScrollPositionOnPostback="true" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master"
    AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers.Edit"
    CodeBehind="Edit.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView" align="center">
        <div>
            <div class="LeftFloat" style="width: auto; text-align: left">
                <h1>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
            </div>
            <div style="float: right">
                  <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit%>" ID="btnSubmitTop" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel%>" CausesValidation="False" ID="btnCancelTop" />

            </div>
            <div class="ClearBoth">
            </div>
            <div align="left">
                <div class="ClearBoth" style="float: left">
                    <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label></div>
                <div>
                <br />
                <div class="FieldStyle1">
				<b style="text-decoration: underline"><asp:Localize ID="TextNote" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextNote%>"></asp:Localize></b> <b><asp:Label ID="lblInstructions" runat="server"></asp:Label></b></div>
                </div>

                <!-- Update Panel for profiles drop down list to avoid the postbacks -->
                <asp:UpdatePanel ID="updPnlRealtedGrid" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
	                 <h4 class="SubTitle PageTop"><asp:Localize ID="PhoneNumber" runat="server" Text="<%$ Resources:ZnodeAdminResource, SubTitleGeneral%>"></asp:Localize></h4>
                        <div class="FieldStyle">
                           <asp:Localize ID="ExternalID" runat="server" Text="<%$ Resources:ZnodeAdminResource, SubTitleExternalID%>"></asp:Localize><br />
                            <small><asp:Localize ID="ExternalIDHint" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnExternalID%>'></asp:Localize></small></div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtExternalAccNumber" runat="server" Width="150px" MaxLength="50"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="AccountNumRequired" runat="server"
                                ControlToValidate="txtExternalAccNumber" CssClass="Error" Display="Dynamic"
                                ErrorMessage="Account Number is required."
                                ToolTip='<%$ Resources:ZnodeAdminResource, RequiredAccountNumber %>' ValidationGroup="EditContact" Enabled="False"></asp:RequiredFieldValidator>
                        </div>
                        
                         <div class="FieldStyle">
                           <asp:Localize ID="CustomerBasedPricingEnabled" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnCustomerBasedPricingEnabled%>"></asp:Localize>
                            <asp:Label ID="lblcustomerpriceastric" runat="server" ForeColor="Red" Text="*"
                                Visible="False"></asp:Label>
                            <br />
                            <small><asp:Localize ID="CustomerBasedPricingEnabledHint" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextCustomerBasedPricingEnabled%>'></asp:Localize></small>
                        </div>
                        <div class="ValueStyle">
                            <asp:CheckBox ID="chkCustomerPricing" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnEnableCustomerBasedPricing%>" />
                        </div>

                        <h4 class="SubTitle">
                            <asp:Localize ID="LoginInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleLoginInformation%>'></asp:Localize></h4>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnUserID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnUserID%>'></asp:Localize>  <span class="Asterix">*</span></div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="UserID" runat="server" Columns="40" Width="150px" />
                            <asp:RequiredFieldValidator ID="UseridRequired" runat="server"
                            ControlToValidate="UserID" ToolTip='<%$ Resources:ZnodeAdminResource, RequiredAccountUserID%>' ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredAccountUserID%>'
                            ValidationGroup="EditContact" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="UserID"
                                    Display="Dynamic" ErrorMessage="Enter only alpha numeric characters" SetFocusOnError="True"                                    
                                    ValidationExpression="^([a-zA-Z0-9]+)$" ValidationGroup="EditContact"
                                    CssClass="Error"></asp:RegularExpressionValidator></div>
                       <%-- <div class="FieldStyle">
                            Password</div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="Password" ValidationGroup="PasswordField" runat="server" TextMode="Password" />
                            <asp:RequiredFieldValidator ID="PasswordFieldValidator" runat="server"
                                ControlToValidate="Password" CssClass="Error" Display="Dynamic"
                                ErrorMessage="Password is required."
                                ToolTip="Password is required." ValidationGroup="EditContact"></asp:RequiredFieldValidator>
                            <div>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="Password"
                                    Display="Dynamic" ErrorMessage="New Password length should be minimum 8" SetFocusOnError="True"
                                    ToolTip="New Password length should be minimum 8 and should contain at least 1 number."
                                    ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,})" ValidationGroup="EditContact"
                                    CssClass="Error">Password must be 8 or more characters and should contain only alphanumeric values with at least 1 numeric character.</asp:RegularExpressionValidator></div>
                            <asp:Label ID="lblPwdErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label></div>
                        <div class="FieldStyle" >
                            Security Question</div>
                        <div class="ValueStyle" >
                            <asp:DropDownList ID="ddlSecretQuestions" runat="server">
                                <asp:ListItem Enabled="true" Selected="True" Text="What is the name of your favorite pet?"></asp:ListItem>
                                <asp:ListItem Enabled="true" Text="In what city were you born?"></asp:ListItem>
                                <asp:ListItem Enabled="true" Text="What high school did you attend?"></asp:ListItem>
                                <asp:ListItem Enabled="true" Text="What is your favorite movie?"></asp:ListItem>
                                <asp:ListItem Enabled="true" Text="What is your mother's maiden name?"></asp:ListItem>
                                <asp:ListItem Enabled="true" Text="What was the make of your first car?"></asp:ListItem>
                                <asp:ListItem Enabled="true" Text="What is your favorite color?"></asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="FieldStyle" >
                            Security Answer</div>
                        <div class="ValueStyle" >
                            <asp:TextBox ID="Answer" runat="server" Text=""></asp:TextBox>
							 <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" ControlToValidate="Answer"
                                    ErrorMessage="Security Answer is required." ToolTip="Security Answer is required." ValidationGroup="EditContact"
                                    CssClass="Error" Display="Dynamic">
                                </asp:RequiredFieldValidator>
                            <asp:Label ID="lblAnswerErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label>
                        </div>
                        <h4 class="SubTitle">
                            Contact Information</h4>--%>
                        <div class="FieldStyle">
                           <asp:Localize ID="ColumnEmailAddress" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnEmailAddress%>"></asp:Localize>   <span class="Asterix">*</span></div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtEmail" runat="server" Width="131px"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="EmailFieldValidator" runat="server"
                                ControlToValidate="txtEmail" CssClass="Error" Display="Dynamic"
                                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEmailAddress%>'
                                ToolTip="Email is required." ValidationGroup="EditContact"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" ControlToValidate="txtEmail"
                                ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularEmailAddress%>' Display="Dynamic" ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"
                                ValidationGroup="EditContact" CssClass="Error"></asp:RegularExpressionValidator>
                        </div>
                        <div class="FieldStyle">
                        </div>
                        <div class="ValueStyle">
                            <asp:CheckBox ID="chkOptIn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEmailOptn%>'/>
                        </div>
						
						<asp:Panel ID="pnlResetPassword" runat="server">
						<div class="FieldStyle"><asp:Localize ID="ColumnResetPassword" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnResetPassword%>"></asp:Localize></div>
						<div class="ValueStyle">
                            <zn:Button runat="server" ID="btnResetPassword" OnClick="BtnResetPassword_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>'  OnClientClick="return ResetPasswordMailConfirmation();" CausesValidation="true"/>
						</div>
					</asp:Panel>
                        <h4 class="SubTitle">
                            <asp:Localize ID="SubTitleCustomInformation" runat="server" Text="<%$ Resources:ZnodeAdminResource, SubTitleCustomInformation%>"></asp:Localize></h4>

                         <div>
                            <div class="FieldStyle">
                               <asp:Localize ID="CompanyName" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnCompanyName%>"></asp:Localize> </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtCompanyName" runat="server" /></div>
                        </div>
                        <div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnWebsite" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnWebsite%>"></asp:Localize></div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtWebSite" runat="server" /></div>
                        </div>
                        <div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnSource" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnSource%>"></asp:Localize></div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtSource" runat="server" /></div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnCustom1" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnCustom1%>"></asp:Localize></div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtCustom1" runat="server" TextMode="MultiLine" Width="400" Height="100"
                                MaxLength="2"></asp:TextBox></div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnCustom2" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnCustom2%>"></asp:Localize></div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtCustom2" runat="server" TextMode="MultiLine" Width="400" Height="100"
                                MaxLength="2"></asp:TextBox></div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnCustom3" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnCustom3%>"></asp:Localize></div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtCustom3" runat="server" TextMode="MultiLine" Width="400" Height="100"
                                MaxLength="2"></asp:TextBox></div>
                        <uc1:Spacer ID="Spacer4" SpacerHeight="1" SpacerWidth="3" runat="server">
                        </uc1:Spacer>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnDescription" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnDescription%>"></asp:Localize></div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Width="400"
                                Height="200" MaxLength="2" /></div>
                        <uc1:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="3" runat="server">
                        </uc1:Spacer>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <uc1:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server">
                </uc1:Spacer>
                <uc1:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="3" runat="server">
                </uc1:Spacer>
                <div class="ClearBoth">
                    <br />
             <zn:Button runat="server" ID="btnSubmitBottom" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true"/>
            <zn:Button runat="server" ID="btnCancelBottom" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
                </div>
            </div>
        </div>
    </div>

    <script language="javascript">
        function ResetPasswordMailConfirmation() {
            return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ValidResetPasswordButton")) %>');
    }
</script>

</asp:Content>

