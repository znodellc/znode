using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Admin.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers
{
	/// <summary>
	/// Represents the Franchise Admin Admin_FranchiseAdmin_Secure_sales_customers_Add/Edit class.
	/// </summary>
	public partial class Edit : System.Web.UI.Page
	{
		#region Private Variables
		private int _accountId;
		private int _mode;
		private readonly ZNodeEncryption _encrypt = new ZNodeEncryption();
		private MembershipProvider _adminMembershipProvider = Membership.Providers["ZNodeAdminMembershipProvider"];
		#endregion

		#region Public Variables
		/// <summary>
		/// AdminProvider used to create a user without password nor questions nor answers.
		/// </summary>
		public MembershipProvider AdminMembershipProvider
		{
			get { return _adminMembershipProvider; }
			set { _adminMembershipProvider = value; }
		}
		#endregion

		#region Private Methods

		/// <summary>
		/// Bind Account Details
		/// </summary>
		private void BindData()
		{
			ZNode.Libraries.Admin.AccountAdmin accountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
			ZNode.Libraries.DataAccess.Entities.Account account = accountAdmin.GetByAccountID(this._accountId);
			if (account != null)
			{
				// General Information
				if (string.IsNullOrEmpty(account.ExternalAccountNo))
				{
					txtExternalAccNumber.Text = string.Empty;
				}
				else
				{
					txtExternalAccNumber.Text = account.ExternalAccountNo;
				}

				txtCompanyName.Text = Server.HtmlDecode(account.CompanyName);
				txtWebSite.Text = Server.HtmlDecode(account.Website);
				txtSource.Text = Server.HtmlDecode(account.Source);
				chkCustomerPricing.Checked = account.EnableCustomerPricing.GetValueOrDefault();
				// Description
				txtDescription.Text = Server.HtmlDecode(account.Description);

				// Login Info
				if (account.UserID.HasValue)
				{
					UserID.Enabled = false;

					MembershipUser user = Membership.GetUser(account.UserID.Value);
					UserID.Text = user.UserName;
					//ddlSecretQuestions.Text = user.PasswordQuestion;
				}

				txtEmail.Text = account.Email;
				chkOptIn.Checked = account.EmailOptIn;

				// Custom properties
				txtCustom1.Text = Server.HtmlDecode(account.Custom1);
				txtCustom2.Text = Server.HtmlDecode(account.Custom2);
				txtCustom3.Text = Server.HtmlDecode(account.Custom3);
			}
		}

		/// <summary>
		/// Sends a new password in an email to an existing account by id
		/// </summary>
		/// <param name="acctId">Takes an integer for the accountId parameter.</param>
		private bool SendPasswordResetEmail(int acctId)
		{
			//Getting the Franchise Store to display the current domain url
			var portalProfile = new PortalProfileService().GetByProfileID(UserStoreAccess.GetTurnkeyStoreProfileID)[0];
			var storePortalId = portalProfile.PortalID;
			var portal = new PortalAdmin().GetByPortalId(storePortalId);
			var storeName = portal.StoreName;
			var franchiseStoreDomain = new DomainService().GetByPortalID(storePortalId).FirstOrDefault();
            
            if (franchiseStoreDomain == null)
            {
                lblErrorMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPasswordResetNotification").ToString(), storeName);
                ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                return false;
            }

			var franchiseStoreDomainName = franchiseStoreDomain.DomainName;
			var accountAdmin = new AccountAdmin();

			var account = accountAdmin.GetByAccountID(acctId);

			if (account == null || !account.UserID.HasValue) return false;

			var userToEmail = AdminMembershipProvider.GetUser(account.UserID.Value, false);

			if (userToEmail == null)
			{
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorMembershipFind").ToString();
				ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
				return false;
			}

			if (String.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SMTPServer))
			{
                lblErrorMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCreateSMTP").ToString(), storeName);
				ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
				return false;
			}
			
			var currentCulture = ZNodeCatalogManager.CultureInfo;
			var senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            var subject = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextNewUserCreation").ToString(), storeName);
			var toAddress = account.Email;


			var defaultTemplatePath =
				Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "AdminResetPassword_en.htm"));

			var templatePath = currentCulture != string.Empty
								   ? Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "AdminResetPassword_" +
													currentCulture + ".htm")
								   : Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath,
																 "AdminResetPassword_en.htm"));
			// Check for language specific file existence.
			if (!File.Exists(templatePath))
			{
				// Check the default template file existence.
				if (!File.Exists(defaultTemplatePath))
				{
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCreateTemplateFile").ToString();
					ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
					return false;
				}
				templatePath = defaultTemplatePath;
			}
			try
			{
				string body;
				using (var streamReader = new StreamReader(templatePath))
				{
					body = streamReader.ReadToEnd();
					
				}

				var userName = userToEmail.UserName;
				var regularExpName = new Regex("#UserName#", RegexOptions.IgnoreCase);
				body = regularExpName.Replace(body, userName);

				var userPassword = userToEmail.GetPassword();
				var regularExpPassword = new Regex("#Password#", RegexOptions.IgnoreCase);
				body = regularExpPassword.Replace(body, userPassword);
				
				var franchiseCustLoginUrl = new StringBuilder(@"http://").Append(franchiseStoreDomainName + @"/login.aspx").ToString();
				
                var regularUrl = new Regex("#Url#", RegexOptions.IgnoreCase);
				body = regularUrl.Replace(body, franchiseCustLoginUrl);

				ZNodeEmail.SendEmail(toAddress, senderEmail, String.Empty, subject, body, true);
			}
			catch (Exception ex)
			{
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorResetPasswordEmail").ToString();
				ZNodeLoggingBase.LogMessage(ex.Message);
				return false;
			}
			return true;
		}

		/// <summary>
		/// Sends a new password in an email to the newly created account by id
		/// </summary>
		/// <param name="acctId">Takes an integer for the AccountID parameter.</param>
		private bool SendNewUserEmail(int acctId)
		{
			//Getting the Franchise Store to display the current domain url
			var portalProfile = new PortalProfileService().GetByProfileID(UserStoreAccess.GetTurnkeyStoreProfileID)[0];
			var storePortalId = portalProfile.PortalID;
			var franchiseStoreDomain = new DomainService().GetByPortalID(storePortalId).FirstOrDefault();

			var portal = new PortalAdmin().GetByPortalId(storePortalId);
			var accountAdmin = new AccountAdmin();
			var storeName = portal.StoreName;
			
			var account = accountAdmin.GetByAccountID(acctId);

			if (account == null || !account.UserID.HasValue) return false;

			var user = AdminMembershipProvider.GetUser(account.UserID.Value, false);

			if (user == null)
			{
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCreateEmail").ToString();
					
				ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
				return false;
			}

			if (String.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SMTPServer))
			{
                lblErrorMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCreateSMTP").ToString(), storeName);
				ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
				return false;
			}

            if (franchiseStoreDomain == null)
            {
                lblErrorMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCreateURL").ToString(), storeName);
                ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                return false;
            }

            var franchiseStoreDomainName = franchiseStoreDomain.DomainName;

			var senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
			var currentCulture = ZNodeCatalogManager.CultureInfo;

            var subject = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextNewUserCreation").ToString(), storeName);
			var toAddress = account.Email;

			var defaultTemplatePath =
				Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "NewUserAccount_en.htm"));

			var templatePath = currentCulture != string.Empty
								   ? Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "NewUserAccount_" +
													currentCulture + ".htm")
								   : Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath,
																 "NewUserAccount_en.htm"));

			// Check for language specific file existence.
			if (!File.Exists(templatePath))
			{
				// Check the default template file existence.
				if (!File.Exists(defaultTemplatePath))
				{
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCreateTemplateFile").ToString();
					return false;
				}
				templatePath = defaultTemplatePath;
			}

			try
			{
				string body;
				using (var streamReader = new StreamReader(templatePath))
				{
					body = streamReader.ReadToEnd();
				}

				var userName = user.UserName;
				var regularExpName = new Regex("#UserName#", RegexOptions.IgnoreCase);
				body = regularExpName.Replace(body, userName);

				var userPassword = user.GetPassword();
				var regularExpPassword = new Regex("#Password#", RegexOptions.IgnoreCase);
				body = regularExpPassword.Replace(body, userPassword);

				var franchiseCustLoginUrl = new StringBuilder(@"http://").Append(franchiseStoreDomainName + @"/login.aspx").ToString();

				var regularUrl = new Regex("#Url#", RegexOptions.IgnoreCase);
				body = regularUrl.Replace(body, franchiseCustLoginUrl);

				ZNodeEmail.SendEmail(toAddress, senderEmail, String.Empty, subject, body, true);
			}
			catch (Exception ex)
			{
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogPasswordMail").ToString();
				ZNodeLoggingBase.LogMessage(ex.Message);
				return false;
			}

			return true;
		}

		#endregion

		#region Protected Methods

		#region Page Load

		/// <summary>
		/// Page Load Event
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			if (HttpContext.Current.Request.Params["itemid"] == null)
			{
				this._accountId = 0;
			}
			else
			{
				this._accountId = Convert.ToInt32(this._encrypt.DecryptData(Request.Params["itemid"].ToString()));
			}

			if (HttpContext.Current.Request.Params["mode"] == null)
			{
				this._mode = 0;
			}
			else
			{
				this._mode = int.Parse(Request.Params["mode"].ToString());
			}

			if (!Page.IsPostBack)
			{
				this.BindData();

				if (this._accountId > 0)
				{
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditCustomerInformation").ToString();
                    lblInstructions.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextEditCustomerInstruction").ToString();
					pnlResetPassword.Visible = true;

				}
				else
				{
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddCustomerInformation").ToString();
                    lblInstructions.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextAddCustomerInstuction").ToString();
					pnlResetPassword.Visible = false;
				}
			}
		}
		#endregion

		/// <summary>
		/// Submit Button Click Event
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			var logger = new ZNodeLogging();
			var accountAdmin = new AccountAdmin();
			var account = accountAdmin.GetByAccountID(_accountId);
			var transactionManager = ConnectionScope.CreateTransaction();

			//TODO: Update Account
			if (_accountId != 0 && _accountId > 0)
			{
				var userAccount = new ZNodeUserAccount(account);

				logger.LogActivityTimerStart();

				var accountUpdated = UpdateZNodeUserAccount(userAccount);

				if (!accountUpdated)
				{
					if (transactionManager.IsOpen)
						transactionManager.Rollback();

					if (string.IsNullOrEmpty(lblErrorMsg.Text))
                        lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountUpdate").ToString();

					logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateFailed, UserID.Text.Trim());
					return;
				}

				var existingMembershipUser = AdminMembershipProvider.GetUser(userAccount.UserID, false);

				if (existingMembershipUser == null)
				{
					if (transactionManager.IsOpen)
						transactionManager.Rollback();

					if (string.IsNullOrEmpty(lblErrorMsg.Text))
                        lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountMembership").ToString();

					logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateFailed, UserID.Text.Trim());
					return;
				}

				//Save
				userAccount.UpdateUserAccount();

                var associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogUpdateAccount").ToString(), UserID.Text.Trim());

				//Customer Update MembershipUser EmailAddress
				existingMembershipUser.Email = txtEmail.Text.Trim();

				//Update db with MembershipUser Data
				Membership.UpdateUser(existingMembershipUser);

				//End Timer
				logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateSuccess, UserID.Text.Trim());

				ZNodeLoggingBase.LogActivity((int)ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(),
																					null, null, null, null, associateName, "Account");
				transactionManager.Commit();

				Response.Redirect("~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=" + _mode + "&itemid=" +
								  HttpUtility.UrlEncode(_encrypt.EncryptData(account.AccountID.ToString(CultureInfo.InvariantCulture))));
			}

			//TODO: Add Account
			else if (_accountId == 0)
			{
				var userAccount = new Account();

				logger.LogActivityTimerStart();

				var accountAdded = AddZNodeUserAccount(userAccount);

				if (!accountAdded)
				{
					if (transactionManager.IsOpen)
						transactionManager.Rollback();

					if (string.IsNullOrEmpty(lblErrorMsg.Text))
                        lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUserAdd").ToString();
					logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateFailed, UserID.Text.Trim());
					return;
				}

				var emailSent = SendNewUserEmail(userAccount.AccountID);

				if (!emailSent)
				{
					//If email fails, still save the user & provide error message
					if (transactionManager.IsOpen)
						transactionManager.Commit();

					if (string.IsNullOrEmpty(lblErrorMsg.Text))
                        lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCredential").ToString();

					logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateFailed, UserID.Text.Trim());
					return;
				}

				logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateSuccess, UserID.Text.Trim());
				transactionManager.Commit();

				Response.Redirect("~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=" + _mode + "&itemid=" +
								  HttpUtility.UrlEncode(_encrypt.EncryptData(userAccount.AccountID.ToString(CultureInfo.InvariantCulture))));
			}
			else
			{
				if (transactionManager.IsOpen)
					transactionManager.Rollback();

                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInvalidAccount").ToString();
				logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateFailed, UserID.Text.Trim());
			}
		}

		/// <summary>
		/// Updates the current ZNodeUserAccount given by param
		/// </summary>
		/// <param name="userAccount"></param>
		/// <returns>Returns true if update is successful.</returns>
		protected bool UpdateZNodeUserAccount(ZNodeUserAccount userAccount)
		{
			if (userAccount.UserID == null)
				return false;

			userAccount.EmailID = txtEmail.Text;
			userAccount.EmailOptIn = chkOptIn.Checked;
			userAccount.EnableCustomerPricing = chkCustomerPricing.Checked;
			userAccount.ExternalAccountNo = string.IsNullOrEmpty(txtExternalAccNumber.Text.Trim())
														? null
														: Server.HtmlEncode(txtExternalAccNumber.Text.Trim());
			userAccount.Description = Server.HtmlEncode(txtDescription.Text);
			userAccount.CompanyName = Server.HtmlEncode(txtCompanyName.Text);
			userAccount.Website = Server.HtmlEncode(txtWebSite.Text.Trim());
			userAccount.Source = Server.HtmlEncode(txtSource.Text.Trim());
			userAccount.AccountProfileCode = userAccount.AccountProfileCode ??
													 DBNull.Value.ToString(CultureInfo.InvariantCulture);
			userAccount.CreateUser = userAccount.CreateUser ?? HttpContext.Current.User.Identity.Name;
			userAccount.CreateDte = userAccount.CreateDte;
			userAccount.UpdateUser = HttpContext.Current.User.Identity.Name;
			userAccount.UpdateDte = DateTime.Now;
			userAccount.UserID = userAccount.UserID;
			userAccount.AccountID = _accountId;
			userAccount.Custom1 = Server.HtmlEncode(txtCustom1.Text.Trim());
			userAccount.Custom2 = Server.HtmlEncode(txtCustom2.Text.Trim());
			userAccount.Custom3 = Server.HtmlEncode(txtCustom3.Text.Trim());
			return true;
		}

		/// <summary>
		/// Adds the current Account given by param
		/// </summary>
		/// <param name="userAccount"></param>
		/// <returns>Returns true if add is successful.</returns>
		protected bool AddZNodeUserAccount(Account userAccount)
		{
			MembershipCreateStatus createStatus;
			var znodeUserAccount = new ZNodeUserAccount();
			var accountAdmin = new AccountAdmin();
			var accountService = new AccountService();
			var accountProfileService = new AccountProfileService();
			var addressService = new AddressService();
            var profileId = UserStoreAccess.GetTurnkeyStoreProfileID;
			var newlyCreatedPassword = Membership.GeneratePassword(8, 1);

			userAccount.CreateDte = DateTime.Now;
			userAccount.CreateUser = HttpContext.Current.User.Identity.Name;
			userAccount.ActiveInd = true;
			userAccount.UpdateDte = DateTime.Now;

			if (UserID.Text.Trim().Length <= 0)
			{
				lblErrorMsg.Text =  this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUserID").ToString();
				return false;
			}

			if (profileId == 0)
			{
				lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountProfile").ToString();
				return false;
			}

			//TODO: Database can have emptystring for ExternalAccountNo, so adding length check, then proceeding
			if (txtExternalAccNumber.Text.Length > 0)
			{
                TList<Account> account = accountService.Find(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextExternalAccountNo").ToString(), txtExternalAccNumber.Text.Trim()));
				if (account.Count > 0)
				{
					if (account[0].UserID != null)
					{
                        lblErrorMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorExternalNumberAssociate").ToString(), txtExternalAccNumber.Text, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);
						return false;
					}
				}
			}

			// Check if loginName already exists
			if (!znodeUserAccount.IsLoginNameAvailable(ZNodeConfigManager.SiteConfig.PortalID, UserID.Text.Trim()))
			{
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorLoginExist").ToString();
				return false;
			}

			var newMembershipUser = AdminMembershipProvider.CreateUser(Server.HtmlEncode(UserID.Text.Trim()),
																	   newlyCreatedPassword.Trim(), txtEmail.Text.Trim(),
																	   null, null, true, Guid.NewGuid(),
																	   out createStatus);

			if (newMembershipUser == null ||
				(createStatus != MembershipCreateStatus.Success || newMembershipUser.ProviderUserKey == null))
			{
				lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorMembershipCreate").ToString();
				return false;
			}

			userAccount.ActiveInd = true;
			userAccount.ProfileID = profileId;
			userAccount.Email = txtEmail.Text;
			userAccount.EmailOptIn = chkOptIn.Checked;
			userAccount.EnableCustomerPricing = chkCustomerPricing.Checked;
			userAccount.ExternalAccountNo = string.IsNullOrEmpty(txtExternalAccNumber.Text.Trim())
														? null
														: Server.HtmlEncode(txtExternalAccNumber.Text.Trim());
			userAccount.Description = Server.HtmlEncode(txtDescription.Text);
			userAccount.CompanyName = Server.HtmlEncode(txtCompanyName.Text);
			userAccount.Website = Server.HtmlEncode(txtWebSite.Text.Trim());
			userAccount.Source = Server.HtmlEncode(txtSource.Text.Trim());
			userAccount.AccountProfileCode = userAccount.AccountProfileCode ??
													 DBNull.Value.ToString(CultureInfo.InvariantCulture);
			userAccount.CreateUser = HttpContext.Current.User.Identity.Name;
			userAccount.CreateDte = userAccount.CreateDte;
			userAccount.UpdateUser = HttpContext.Current.User.Identity.Name;
			userAccount.UpdateDte = DateTime.Now;
			userAccount.UserID = new Guid(newMembershipUser.ProviderUserKey.ToString());
			userAccount.Custom1 = Server.HtmlEncode(txtCustom1.Text.Trim());
			userAccount.Custom2 = Server.HtmlEncode(txtCustom2.Text.Trim());
			userAccount.Custom3 = Server.HtmlEncode(txtCustom3.Text.Trim());
			userAccount.AccountID = _accountId;

			accountService.Insert(userAccount);

			var customerAddress = accountAdmin.GetDefaultBillingAddress(userAccount.AccountID) ??
						  new Address
						  {
							  IsDefaultBilling = true,
							  IsDefaultShipping = true,
							  AccountID = userAccount.AccountID,
						  };

			//Setting Default Values
			customerAddress.FirstName = String.Empty;
			customerAddress.LastName = String.Empty;
			customerAddress.PhoneNumber = String.Empty;
			customerAddress.Street = String.Empty;
			customerAddress.Street1 = String.Empty;
			customerAddress.City = String.Empty;
			customerAddress.CompanyName = String.Empty;
			customerAddress.StateCode = String.Empty;
			customerAddress.PostalCode = String.Empty;
			customerAddress.CountryCode = "US";
			customerAddress.Name = "Default Address";

			addressService.Insert(customerAddress);

			var customerAccountProfile = new AccountProfile { AccountID = userAccount.AccountID, ProfileID = profileId };

			accountProfileService.Insert(customerAccountProfile);

			//Roles.AddUserToRole(UserID.Text.Trim(), "CUSTOMER SERVICE REP");

			var customerProfile = ProfileCommon.Create(UserID.Text, true);

			customerProfile.StoreAccess = "AllStores";

			customerProfile.Save();

			accountService.Update(userAccount);

			return true;
		}

		/// <summary>
		/// Reset Password Button Click Event that gets current user Account by accountId 
		/// resets password using ZNodeAdminMembershipProvider & calls SendPasswordResetEmail
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contans event data.</param>
		protected void BtnResetPassword_Click(object sender, EventArgs e)
		{
			var transactionManager = ConnectionScope.CreateTransaction();
			var helperAccess = new AccountHelper();
			var accountAdmin = new AccountAdmin();
			var newlyCreatedPassword = Membership.GeneratePassword(8, 1);

			if (_accountId <= 0)
			{
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorResetPassword").ToString();
				ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
				return;
			}

			var userAccount = accountAdmin.GetByAccountID(_accountId);

			if (userAccount == null || !userAccount.UserID.HasValue)
			{
				if (transactionManager.IsOpen)
					transactionManager.Rollback();

				lblErrorMsg.Text =
                     this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSelectUserAccount").ToString();
				ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
				return;
			}

			var currentUser = AdminMembershipProvider.GetUser(userAccount.UserID.Value, false);

			if (currentUser == null)
			{
				if (transactionManager.IsOpen)
					transactionManager.Rollback();

				lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorMembershipRetrieve").ToString();
				ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
				return;
			}

			var currentPassword = currentUser.GetPassword();
			
			var passwordChanged = currentUser.ChangePassword(currentPassword, newlyCreatedPassword);

			if (!passwordChanged)
			{
				if (transactionManager.IsOpen)
					transactionManager.Rollback();

				lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorChangePassword").ToString();
				ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
				return;
			}

			accountAdmin.UpdateLastPasswordChangedDate(userAccount.UserID.ToString());

			var userUpdated = accountAdmin.Update(userAccount);

			if (!userUpdated)
			{
				if (transactionManager.IsOpen)
					transactionManager.Rollback();

				lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPasswordUpdate").ToString();
				ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
				return;
			}

			if (currentUser.ProviderUserKey == null)
			{
				if (transactionManager.IsOpen)
					transactionManager.Rollback();

                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountRetrieve").ToString();
				ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
				return;
			}

			try
			{
				//Remove password log
				helperAccess.DeletePasswordLogByUserId(((Guid)currentUser.ProviderUserKey).ToString());
				//Add password log
				ZNodeUserAccount.LogPassword((Guid)currentUser.ProviderUserKey, newlyCreatedPassword);
			}
			catch (Exception ex)
			{
				if (transactionManager.IsOpen)
					transactionManager.Rollback();

				ZNodeLoggingBase.LogMessage(ex.Message);
                lblErrorMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPasswordLog").ToString(), ex.Message);
				return;
			}

			var emailSent = SendPasswordResetEmail(userAccount.AccountID);

			if (!emailSent)
			{
				if (transactionManager.IsOpen)
					transactionManager.Rollback();
				if (string.IsNullOrEmpty(lblErrorMsg.Text))
					lblErrorMsg.Text =  this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPasswordEmail").ToString();
				ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
				return;
			}

			transactionManager.Commit();

			Response.Redirect("~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=" + _mode +
							  "&itemid=" + HttpUtility.UrlEncode(_encrypt.EncryptData(userAccount.AccountID.ToString(CultureInfo.InvariantCulture))));

		}

		/// <summary>
		///  Cancel Button Click Event provides a redirect
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			if (_accountId > 0)
				Response.Redirect("~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=" + _mode + "&itemid=" +
								  HttpUtility.UrlEncode(_encrypt.EncryptData(_accountId.ToString(CultureInfo.InvariantCulture))));
			else
				Response.Redirect("~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/Default.aspx");
		}
		#endregion
	}
}