using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_customers_AddAffiliateSettings class.
    /// </summary>
    public partial class AddAffiliateSettings : System.Web.UI.Page
    {
        #region Private Member Variables
        private int accountId;
        private decimal discountAmount;
        private int mode;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.Params["itemid"] == null)
            {
                this.accountId = 0;
            }
            else
            {
                this.accountId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }

            if (HttpContext.Current.Request.Params["mode"] == null)
            {
                this.mode = 0;
            }
            else
            {
                this.mode = int.Parse(Request.Params["mode"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindTrackingStatus();
                this.BindReferral();
                this.BindData();

                discAmountValidator.ErrorMessage = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDiscountAmount").ToString(), 0.ToString("N") + "- 9999999");
                discPercentageValidator.ErrorMessage = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPercentageAmount").ToString(), 0.ToString("N") + "- 100");

                if (this.accountId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditAffiliateSettings").ToString();
                    amountOwed.Visible = true;
                    lblAmountOwed.Visible = true;
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddAffiliateSettings").ToString();
                    amountOwed.Visible = false;
                    lblAmountOwed.Visible = false;
                }
            }
        }
        #endregion       

        #region General Events

        /// <summary>
        /// Discount Type select index change event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DiscountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ToggleDiscountValidator();
        }

        protected void LstReferralStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstReferralStatus.SelectedValue == "A" && this.accountId > 0)
            {
                string affiliateLink = string.Empty;

                // Get the domain details
                DomainAdmin domainAdmin = new DomainAdmin();
                TList<Domain> domainList = domainAdmin.GetDomainByPortalID(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0));
                if (domainList.Count > 0)
                {
                    affiliateLink = "http://" + domainList[0].DomainName + "/Default.aspx?affiliate_id=" + this.accountId;
                    hlAffiliateLink.Text = affiliateLink;
                    hlAffiliateLink.NavigateUrl = affiliateLink;
                }
                else
                {
                    lblAffiliateLinkError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAffiliateLink").ToString();
                }
            }
            else
            {
                hlAffiliateLink.Text = "NA";
                hlAffiliateLink.NavigateUrl = string.Empty;
            }
        }

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            Page.Validate();

            if (!Page.IsValid)
            {
                return;
            }

            ZNode.Libraries.Admin.AccountAdmin accountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Account account = new Account();

            decimal discountAmt = 0;
            if (Discount.Text != string.Empty && decimal.TryParse(Discount.Text, out discountAmt))
            {
                account.ReferralCommission = discountAmt;
                this.discountAmount = discountAmt;
            }
            else if (!string.IsNullOrEmpty(Discount.Text))
            {
                return;
            }
            
            this.ToggleDiscountValidator();

            if (this.accountId > 0)
            {
                account = accountAdmin.GetByAccountID(this.accountId);
            }

            if (this.accountId == 0)
            {
                account.CreateDte = System.DateTime.Now;
            }

            // Referral Detail        
            if (lstReferral.SelectedIndex != -1)
            {
                account.ReferralCommissionTypeID = Convert.ToInt32(lstReferral.SelectedValue);
            }

            account.TaxID = Server.HtmlEncode(txtTaxId.Text);
            if (Discount.Text != string.Empty)
            {
                account.ReferralCommission = Convert.ToDecimal(Discount.Text);
            }
            else
            {
                account.ReferralCommission = null;
            }

            if (lstReferralStatus.SelectedValue == "I")
            {
                account.ReferralStatus = "I";
            }
            else if (lstReferralStatus.SelectedValue == "A")
            {
                account.ReferralStatus = "A";
            }
            else if (lstReferralStatus.SelectedValue == "N")
            {
                account.ReferralStatus = "N";
            }
            else
            {
                account.ReferralStatus = string.Empty;
            }

            bool isUpdated = false;
            if (this.accountId > 0)
            {
                // Set update date
                account.UpdateDte = System.DateTime.Now;
                isUpdated = accountAdmin.Update(account);
            }
            
            if (isUpdated)
            {
                account = accountAdmin.GetByAccountID(this.accountId);
                string userName = string.Format("AccoountID: {0}", this.accountId.ToString());
                if (account.UserID.HasValue)
                {
                    MembershipUser _user = Membership.GetUser(account.UserID);
                    userName = _user.UserName;
                }

                string associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditAffiliateSettings").ToString(), userName + " Customer ");
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, userName);

                Response.Redirect("~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.accountId.ToString())) + "&Mode=affiliate");
            }
            else
            {
                lblErrorMsg.Text = string.Empty;
            }
        }

        /// <summary>
        ///  Cancel Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.accountId.ToString())) + "&Mode=affiliate");
        }

        #endregion

        #region Bind Data

        /// <summary>
        /// Bind Account Details
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.Admin.AccountAdmin accountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Account account = accountAdmin.GetByAccountID(this.accountId);

            if (account != null)
            {
                // Referral Details
                if (account.ReferralCommissionTypeID != null)
                {
                    lstReferral.SelectedValue = account.ReferralCommissionTypeID.ToString();
                }

                if (account.ReferralCommission != null)
                {
                    Discount.Text = account.ReferralCommission.Value.ToString("N");
                    this.discountAmount = Convert.ToDecimal(account.ReferralCommission);
                }

                if (account.ReferralStatus != null)
                {
                    lstReferralStatus.SelectedValue = account.ReferralStatus;
                }
                else
                {
                    lstReferralStatus.SelectedValue = account.ReferralStatus;
                }

                if (account.ReferralStatus == "A")
                {
                    string affiliateLink = string.Empty;

                    // Get the domain details
                    DomainAdmin domainAdmin = new DomainAdmin();
                    TList<Domain> domainList = domainAdmin.GetDomainByPortalID(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0));

                    if (domainList.Count > 0)
                    {
                        affiliateLink = "http://" + domainList[0].DomainName + "/Default.aspx?affiliate_id=" + account.AccountID;
                        hlAffiliateLink.Text = affiliateLink;
                        hlAffiliateLink.NavigateUrl = affiliateLink;
                    }
                    else
                    {
                        lblAffiliateLinkError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAffiliateLink").ToString();
                    }
                }
                else
                {
                    hlAffiliateLink.Text = "NA";
                }

                AccountHelper accountHelper = new AccountHelper();
                DataSet commissionDataSet = accountHelper.GetCommisionAmount(ZNodeConfigManager.SiteConfig.PortalID, this.accountId.ToString());

                lblAmountOwed.Text = "$" + commissionDataSet.Tables[0].Rows[0]["CommissionOwed"].ToString();

                this.ToggleDiscountValidator();

                txtTaxId.Text = Server.HtmlDecode(account.TaxID);
            }
        }

        /// <summary>
        /// Binds Referral type drop-down list
        /// </summary>
        private void BindReferral()
        {
            ZNode.Libraries.Admin.ReferralCommissionAdmin referralType = new ReferralCommissionAdmin();
            lstReferral.DataSource = referralType.GetAll();
            lstReferral.DataTextField = "Name";
            lstReferral.DataValueField = "ReferralCommissiontypeID";
            lstReferral.DataBind();
        }

        /// <summary>
        /// Binds Tracking Status drop-down list
        /// </summary>
        private void BindTrackingStatus()
        {
            Array names = Enum.GetNames(typeof(ZNodeApprovalStatus.ApprovalStatus));
            Array values = Enum.GetValues(typeof(ZNodeApprovalStatus.ApprovalStatus));

            // Clear list items
            lstReferralStatus.Items.Clear();
     
            // Add default value to the item
            lstReferralStatus.Items.Add(new ListItem("Inactive", string.Empty));

            for (int index = 0; index < names.Length; index++)
            {
                ListItem listitem = new ListItem(ZNodeApprovalStatus.GetEnumValue(values.GetValue(index)), names.GetValue(index).ToString());
                lstReferralStatus.Items.Add(listitem);
            }
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Enable/Disable Percentage/Amount validator on Discount field
        /// </summary>
        private void ToggleDiscountValidator()
        {
            int Id = int.Parse(lstReferral.SelectedValue);

            discAmountValidator.Enabled = false;
            discPercentageValidator.Enabled = false;

            if (Id == 1)
            {
                // Percentage Discount 
                if ((this.discountAmount <= 1) || (this.discountAmount >= 100))
                {
                    discPercentageValidator.Enabled = true;
                    discPercentageValidator.SetFocusOnError = true;
                    discAmountValidator.Enabled = false;
                }
            }
            else if (Id == 2) 
            {
                // Amount Discount
                if ((this.discountAmount <= 1) || (this.discountAmount >= 9999999))
                {
                    discAmountValidator.Enabled = true;
                    discAmountValidator.SetFocusOnError = true;
                    discPercentageValidator.Enabled = false;
                }
            }
        }
        #endregion
    }
}