﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers
{
    /// <summary>
    /// Represents the Franchise Admin FranchiseAdmin_Secure_sales_customers_EditAddress class.
    /// </summary>
    public partial class EditAddress : System.Web.UI.Page
    {
        #region Private Member Variables
        private int addressId;
        private int accountId;
        private int mode;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.Request.Params["itemid"] == null)
            {
                this.addressId = 0;
            }
            else
            {
                this.addressId = int.Parse(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }

            if (HttpContext.Current.Request.Params["accountid"] == null)
            {
                this.accountId = 0;
            }
            else
            {
                this.accountId = int.Parse(this.encrypt.DecryptData(Request.Params["accountid"].ToString()));
            }

            if (HttpContext.Current.Request.Params["mode"] == null)
            {
                this.mode = 0;
            }
            else
            {
                this.mode = int.Parse(Request.Params["mode"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindCountry();

                if (this.addressId > 0)
                {
                    this.BindData();

                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditAddress").ToString();
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddAddress").ToString();
                }
            }
        }
        #endregion        

        #region Event Methods
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            this.RediretPage();
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            this.SubmitPage();
        }
        #endregion

        #region Helper Methods


		/// <summary>
		/// Validate whether shipping option is available to country.
		/// </summary>
		public bool ValidateCountry()
		{
			bool flag = false;
			ZNode.Libraries.DataAccess.Custom.PortalCountryHelper portalCountryHelper =
				new ZNode.Libraries.DataAccess.Custom.PortalCountryHelper();
			DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

			DataTable BillingView = countryDs.Tables[0];
			DataRow[] drBillingView = BillingView.Select("CountryCode='" + this.ddlCountryCode.Text + "'");
			foreach (DataRow drView in drBillingView)
			{
				if (drView["ShippingActive"].ToString() != "True")
				{
					flag = false;
				}
				else
				{
					flag = true;
				}
			}
			return flag;
		}

        /// <summary>
        /// Submit the page data.
        /// </summary>
        private void SubmitPage()
        {
            AddressService addressService = new AddressService();
            Address address = new Address();
            if (this.addressId > 0)
            {
                address = addressService.GetByAddressID(this.addressId);
            }

            if (!this.HasDefaultAddress(this.accountId, true))
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorBilling").ToString();
                return;
            }

            if (!this.HasDefaultAddress(this.accountId, false))
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorShipping").ToString();
                return;
            }

			if (chkUseAsDefaultShippingAddress.Checked)
			{
				if (!ValidateCountry())
				{
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCountry").ToString();
					return;
				}
			}


            address.AccountID = this.accountId;
            address.Name = txtName.Text;
            address.FirstName = txtFirstName.Text;
            address.LastName = txtLastName.Text;
            address.CompanyName = txtCompanyName.Text;
            address.Street = txtStreet1.Text;
            address.Street1 = txtStreet2.Text;
            address.City = txtCity.Text;
            address.StateCode = txtState.Text.ToUpper();
            address.PostalCode = txtPostalCode.Text;
            address.CountryCode = ddlCountryCode.SelectedValue;
            address.PhoneNumber = txtPhoneNumber.Text;

            TList<Address> addressList = addressService.GetByAccountID(this.accountId);
            if (chkUseAsDefaultBillingAddress.Checked)
            {
                // Clear all previous default billing address flag.
                foreach (Address currentItem in addressList)
                {
                    currentItem.IsDefaultBilling = false;
                }

                addressService.Update(addressList);
            }

            if (chkUseAsDefaultShippingAddress.Checked)
            {
                // Clear all previous default billing address flag.
                foreach (Address currentItem in addressList)
                {
                    currentItem.IsDefaultShipping = false;
                }

                addressService.Update(addressList);
            }

            address.IsDefaultBilling = chkUseAsDefaultBillingAddress.Checked;
            address.IsDefaultShipping = chkUseAsDefaultShippingAddress.Checked;

            bool isSuccess = false;
            if (this.addressId > 0)
            {
                isSuccess = addressService.Update(address);
            }
            else
            {
                isSuccess = addressService.Insert(address);
            }

            if (isSuccess)
            {
                this.RediretPage();
            }
        }

        /// <summary>
        /// Check whether the account has default billing or shipping address.
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <param name="isBillingAddress">True to check billing address, false to check shipping address.</param>
        /// <returns>Returns true if account has default address or false.</returns>
        private bool HasDefaultAddress(int accountId, bool isBillingAddress)
        {
            bool hasDefault = true;

            AccountAdmin accountAdmin = new AccountAdmin();
            Address address = null;
            if (isBillingAddress)
            {
                address = accountAdmin.GetDefaultBillingAddress(accountId);
                hasDefault = this.ValidateDefaultAddress(address, chkUseAsDefaultBillingAddress);
            }
            else
            {
                address = accountAdmin.GetDefaultShippingAddress(accountId);
                hasDefault = this.ValidateDefaultAddress(address, chkUseAsDefaultShippingAddress);
            }

            return hasDefault;
        }

        /// <summary>
        /// Validate account had default address.
        /// </summary>
        /// <param name="address">Address object</param>
        /// <param name="cb">Selected checkbox control</param>
        /// <returns>Returns true if account has default address.</returns>
        private bool ValidateDefaultAddress(Address address, CheckBox cb)
        {
            if (address == null && !cb.Checked)
            {
                return false;
            }
            else
            {
                if (!cb.Checked && address != null && address.AddressID == this.addressId)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Redirect to the account view page.
        /// </summary>
        private void RediretPage()
        {
            if (this.accountId > 0)
            {
                Response.Redirect("~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=" + this.mode + "&itemid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.accountId.ToString())));
            }
            else
            {
                Response.Redirect("~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/Default.aspx");
            }
        }
        #endregion

        #region Bind Data

        /// <summary>
        /// Bind Account Details
        /// </summary>
        private void BindData()
        {
            AddressService addressService = new AddressService();
            ZNode.Libraries.DataAccess.Entities.Address address = addressService.GetByAddressID(this.addressId);

            if (address != null)
            {
                // Set field values
                txtName.Text = address.Name;
                txtFirstName.Text = address.FirstName;
                txtLastName.Text = address.LastName;
                txtCompanyName.Text = address.CompanyName;
                txtStreet1.Text = address.Street;
                txtStreet2.Text = address.Street1;
                txtCity.Text = address.City;
                txtState.Text = address.StateCode;
                txtPostalCode.Text = address.PostalCode;
                ddlCountryCode.SelectedIndex = ddlCountryCode.Items.IndexOf(ddlCountryCode.Items.FindByValue(address.CountryCode));
                txtPhoneNumber.Text = address.PhoneNumber;
                chkUseAsDefaultBillingAddress.Checked = address.IsDefaultBilling;
                chkUseAsDefaultShippingAddress.Checked = address.IsDefaultShipping;
            }
        }

        /// <summary>
        /// Binds country drop-down list
        /// </summary>
        private void BindCountry()
        {
            // PortalCountry
            PortalCountryHelper portalCountryHelper = new PortalCountryHelper();
            DataSet countryDataSet = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

            DataView View = new DataView(countryDataSet.Tables[0]);
            View.RowFilter = "ActiveInd = 1";

            // Drop Down List
            ddlCountryCode.DataSource = View;
            ddlCountryCode.DataTextField = "Name";
            ddlCountryCode.DataValueField = "Code";
            ddlCountryCode.DataBind();
            ddlCountryCode.SelectedIndex = ddlCountryCode.Items.IndexOf(ddlCountryCode.Items.FindByValue("US"));
        }

        #endregion
    }
}
