<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers.Unlock" Codebehind="Unlock.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h5>
        Please Confirm</h5>
    <p>
        Please confirm that you want to <strong>Enable</strong> this customer.</p>
    <asp:Label ID="lblMsg" runat="server" Width="450px" CssClass="Error"></asp:Label><br />
    <br />
    <div>
        <zn:Button Width="175" ID="btnUnlock" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonEnableCustomer %>' runat="server" OnClick="BtnUnlockUSer_Click" ButtonType="EditButton"/>
        <zn:Button runat="server" ID="btnCancelBottom" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
    </div>
    <br />
    <br />
    <br />
</asp:Content>
