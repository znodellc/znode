using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_customers_Unlock class.
    /// </summary>
    public partial class Unlock : System.Web.UI.Page
    {
        #region Private Member Variables
        private string listPageLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/Default.aspx";
        private int itemId = 0;
        private AccountAdmin accountAdmin = new AccountAdmin();
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
            }

            if (!Page.IsPostBack)
            {
                Account account = this.accountAdmin.GetByAccountID(this.itemId);

                MembershipUser user = Membership.GetUser(account.UserID.Value);

                string roleList = string.Empty;

                // Get roles for this User account
                string[] roles = Roles.GetRolesForUser(user.UserName);

                foreach (string role in roles)
                {
                    roleList += role + "<br>";
                }

                string roleName = roleList;

                // Hide the Delete button if a NonAdmin user has entered this page
                if (!Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "ADMIN"))
                {
                    if (Roles.IsUserInRole(user.UserName, "ADMIN"))
                    {
                        btnUnlock.Visible = false;
                    }
                    else if (Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "CUSTOMER SERVICE REP"))
                    {
                        if (roleName == Convert.ToString("USER<br>") || roleName == string.Empty)
                        {
                            btnUnlock.Visible = true;
                        }
                        else
                        {
                            btnUnlock.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Unlock User button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnUnlockUSer_Click(object sender, EventArgs e)
        {
            Account account = this.accountAdmin.GetByAccountID(this.itemId);

            bool isUnlocked = false;
            lblMsg.Text = "User account not found. Please go back to the previous page and reselect the account.";

            if (account != null)
            {
                if (account.UserID.HasValue)
                {
                    // Get user by Unique GUID - User Id
                    MembershipUser user = Membership.GetUser(account.UserID.Value);
                    if (user != null)
                    {
                        // If the user is locked out then unlock them so they will be able to log in.
                        isUnlocked = user.UnlockUser();

                        // Update user
                        user.IsApproved = true;
                        Membership.UpdateUser(user);
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.AccountUnLock, user.UserName);
                    }
                }
            }

            if (isUnlocked)
            {
                Response.Redirect(this.listPageLink);
            }
        }

        /// <summary>
        /// Cancel Button Click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listPageLink);
        }
        #endregion
    }
}