using System;
using System.Globalization;
using System.Web;
using System.Web.Security;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_customers_Note_Add class.
    /// </summary>
    public partial class NoteAdd : System.Web.UI.Page
    {
        #region Private Member Variables
        private int _accountId;
	    private const string CancelLink = "View.aspx?itemid=";
	    private readonly ZNodeEncryption _encryption = new ZNodeEncryption();
        #endregion

        #region General Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this._accountId = Convert.ToInt32(this._encryption.DecryptData(Request.Params["itemid"]));
            }
        }

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            // Validate the controls
            var isValid = Validates();

	        if (!isValid) return;

	        var noteAdmin = new NoteAdmin();
	        var note = new Note
		        {
			        CaseID = null,
			        AccountID = _accountId,
			        CreateDte = DateTime.Now,
			        CreateUser = HttpContext.Current.User.Identity.Name,
			        NoteTitle = Server.HtmlEncode(txtNoteTitle.Text.Trim()),
			        NoteBody = ctrlHtmlText.Html
		        };

	        var isAdded = noteAdmin.Insert(note);

	        if (!isAdded)
	        {
		        // Display error message
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAddNote").ToString();
		        lblError.Visible = true;
		        return;
	        }
	        
			var accountAdmin = new AccountAdmin();
	        var account = accountAdmin.GetByAccountID(_accountId);
	        
			var userName = string.Empty;
	        
			if (account.UserID != null)
			{
				var user = Membership.GetUser(account.UserID);
				if (user != null) userName = user.UserName;
			}

            var associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddNote").ToString(), txtNoteTitle.Text.Trim());

	        ZNodeLogging.LogActivity((int) ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, userName);

	        // Redirect to Notes Tab of Main View
	        Response.Redirect(CancelLink +
	                          HttpUtility.UrlEncode(
		                          _encryption.EncryptData(_accountId.ToString(CultureInfo.InvariantCulture))) + "&Mode=notes");
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(CancelLink + HttpUtility.UrlEncode(this._encryption.EncryptData(this._accountId.ToString())) + "&Mode=notes");
        }
        #endregion


        #region Private Methods
        /// <summary>
        /// Validate the controls
        /// </summary>
        /// <returns></returns>
        private bool Validates()
        {
            bool isValid = true;
            if (string.IsNullOrEmpty(txtNoteTitle.Text))
            {
                //Display error message for Note Title
                lblErrorTitle.Visible = true;
                lblErrorTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoteTitle").ToString();
                isValid = false;
            }
            else
            {
                lblErrorTitle.Visible = false;
            }

            if (string.IsNullOrEmpty(ctrlHtmlText.Html))
            {
                // Display error message for body note
                lblError.Visible = true;
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoteBody").ToString();
                isValid = false;
            }
            else
            {
                lblError.Visible = false;
            }

            return isValid;
        }
        #endregion
    }
}