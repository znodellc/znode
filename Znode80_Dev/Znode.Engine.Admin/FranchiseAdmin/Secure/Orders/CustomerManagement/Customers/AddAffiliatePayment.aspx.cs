using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_customers_addAffiliatePayment class.
    /// </summary>
    public partial class AddAffiliatePayment : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private int accountId = 0;
        private string viewPageLink = "View.aspx?itemid=";
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Page Events        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }
            else
            {
                this.itemId = 0;
            }

            if (Request.Params["accId"] != null)
            {
                this.accountId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["accId"].ToString()));
            }            

            if (this.itemId > 0)
            {
                lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditPayment").ToString();
            }

            AccountHelper accountHelper = new AccountHelper();
            DataSet commissionDataSet = accountHelper.GetCommisionAmount(ZNodeConfigManager.SiteConfig.PortalID, this.accountId.ToString());

            lblAmountOwed.Text = "$" + commissionDataSet.Tables[0].Rows[0]["CommissionOwed"].ToString();

            
        }

        #endregion

        #region General Events
        /// <summary>
        /// Submit buttn click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.Parse(txtDate.Text);

            if (dt.CompareTo(System.DateTime.Today) > 0)
            {
                ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidReceivedDate").ToString();
                return;
            }

            AccountAdmin accountAdmin = new AccountAdmin();
            AccountPayment accountPayment = new AccountPayment();

            if (this.itemId > 0)
            {
                accountPayment = accountAdmin.GetByAccountPaymentId(this.itemId);
            }

            accountPayment.AccountID = this.accountId;
            accountPayment.Amount = decimal.Parse(txtAmount.Text.Trim());
            accountPayment.Description = Server.HtmlEncode(txtDescription.Text.Trim());
            accountPayment.ReceivedDate = dt;

            bool isAdded = accountAdmin.AddAffiliatePayment(accountPayment);
            
            string associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddAffiliatePayment").ToString(), txtDescription.Text.Trim());
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, txtDescription.Text.Trim());
            
            if (isAdded)
            {
                Response.Redirect(this.viewPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.accountId.ToString())) + "&Mode=affiliate");
            }
            else
            {
                ErrorMessage.Text = string.Empty;
            }
        }

        /// <summary>
        /// Cancel button event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.viewPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.accountId.ToString())) + "&Mode=affiliate");
        }
        #endregion
    }
}