using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_sales_customers_Delete class.
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Private Member Variables
        private string listPageLink = "~/FranchiseAdmin/Secure/Orders/CustomerManagement/Customers/Default.aspx";
        private int itemId = 0;
        private AccountAdmin accountAdmin = new AccountAdmin();
        private OrderAdmin orderAdmin = new OrderAdmin();
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }

            if (!Page.IsPostBack)
            {
                Account account = this.accountAdmin.GetByAccountID(this.itemId);

                if (account.UserID != null)
                {
                    MembershipUser user = Membership.GetUser(account.UserID.Value);

                    string roleList = string.Empty;

                    // Get roles for this User account
                    string[] roles = Roles.GetRolesForUser(user.UserName);

                    foreach (string Role in roles)
                    {
                        roleList += Role + "<br>";
                    }

                    string roleName = roleList;

                    // Hide the Delete button if a NonAdmin user has entered this page
                    if (!Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "ADMIN"))
                    {
                        if (Roles.IsUserInRole(user.UserName, "ADMIN"))
                        {
                            btnDelete.Visible = false;
                        }
                        else if (Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "CUSTOMER SERVICE REP"))
                        {
                            if (roleName == Convert.ToString("USER<br>") || roleName == string.Empty)
                            {
                                btnDelete.Visible = true;
                            }
                            else
                            {
                                btnDelete.Visible = false;
                            }
                        }
                    }
                }
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// Delete button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
/// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            CaseAdmin caseAdmin = new CaseAdmin();
            NoteAdmin noteAdmin = new NoteAdmin();
            AccountProfileAdmin accountProfileAdmin = new AccountProfileAdmin();

            Account account = this.accountAdmin.GetByAccountID(this.itemId);

            Guid usreGuid = new Guid();
            TransactionManager transactionManager = ConnectionScope.CreateTransaction();
            try
            {
                if (account != null)
                {
                    if (account.UserID.HasValue)
                    {
                        // Get UserId for this account
                        usreGuid = account.UserID.Value;
                    }

                    int orderCount = this.orderAdmin.GetTotalOrderItemsByAccountId(this.itemId);
                    if (orderCount <= 0)
                    {
                        // Delete the Profiles for this AccountId
                        TList<AccountProfile> accountProfileList = accountProfileAdmin.DeepLoadProfilesByAccountID(this.itemId);
                        foreach (AccountProfile accountProfile in accountProfileList)
                        {
                            accountProfileAdmin.Delete(accountProfile.AccountProfileID);
                        }

                        // Delete the note for this AccountId.
                        noteAdmin.DeleteByAccountId(this.itemId);

                        // Delete the case for this AccountId.
                        caseAdmin.DeleteByAccountId(this.itemId);

                        // Delete accounts all address 
                        bool isSuccess = false;
                        TList<Address> addressList = this.accountAdmin.GetAllAddressByAccountID(account.AccountID);
                        foreach (Address address in addressList)
                        {
                            isSuccess = this.accountAdmin.DeleteAddress(address.AddressID);

                            // Address delete failed.
                            if (!isSuccess)
                            {
                                if (transactionManager.IsOpen)
                                {
                                    transactionManager.Rollback();
                                }
                            }
                        }

                        isSuccess = this.accountAdmin.Delete(account);
                        if (isSuccess)
                        {
                            // Get user by Unique GUID - User Id
                            MembershipUser user = Membership.GetUser(usreGuid);
                            if (user != null)
                            {
                                string associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteAccount").ToString(), user.UserName);
                                string accountName = user.UserName;

                                // Delete online account
                                isSuccess = Membership.DeleteUser(user.UserName);

                                if (!isSuccess)
                                {
                                    if (transactionManager.IsOpen)
                                    {
                                        transactionManager.Rollback();
                                    }

                                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteAccount").ToString();
                                }
                                else
                                {
                                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, accountName);
                                }
                            }

                            if (transactionManager.IsOpen)
                            {
                                transactionManager.Commit();
                            }

                            // Redirect to list page
                            Response.Redirect(this.listPageLink);
                        }
                        else
                        {
                            // Delete account failed.
                            if (transactionManager.IsOpen)
                            {
                                transactionManager.Rollback();
                            }

                            lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteAccount").ToString();
                        }
                    }
                    else
                    {
                        lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteAccount").ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                if (transactionManager.IsOpen)
                {
                    transactionManager.Rollback();
                }

                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);

                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteAccount").ToString();
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listPageLink);
        }
        #endregion
    }
}