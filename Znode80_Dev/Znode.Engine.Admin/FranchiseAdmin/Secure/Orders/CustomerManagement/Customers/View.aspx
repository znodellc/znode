<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master"
    AutoEventWireup="True" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers.View"
    ValidateRequest="false" CodeBehind="View.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/FranchiseAdmin/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ManufacturerAutoComplete" Src="~/FranchiseAdmin/Controls/Default/ManufacturerAutoComplete.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    
    <script language="javascript" type="text/javascript">
        function DeleteConfirmation() {
            return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ConfirmDelete")) %>');
        }
    </script>

    <div>
        <div>
            <div class="LeftFloat" style="width: 30%; text-align: left">
                <h1 style="width: 700px;">
                    <asp:Localize runat="server" ID="TitleCustomer" Text='<%$ Resources:ZnodeAdminResource, TitleCustomer %>'></asp:Localize>
                    <asp:Label ID="lblCustomerDetails" runat="server" Text="Label"></asp:Label>
                </h1>
            </div>
            <div style="float: right">
                <zn:Button runat="server" ID="CustomerList"  ButtonType="EditButton" OnClick="CustomerList_Click" 
                    Width="100px" Text='<%$ Resources:ZnodeAdminResource, LinkBack %>' />
            </div>
            <div class="ClearBoth">
            </div>
            <div align="left">
                <ajaxToolKit:TabContainer ID="tabCustomerDetails" runat="server">
                    <ajaxToolKit:TabPanel ID="pnlGeneral" runat="server">
                        <HeaderTemplate>
                            <asp:Localize runat="server" ID="TabTitleGeneral" Text='<%$ Resources:ZnodeAdminResource, TabTitleGeneral %>'></asp:Localize>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div>
                                <ZNode:Spacer ID="Spacer7" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div>
                                <zn:Button runat="server" ID="EditCustomer"  ButtonType="EditButton" OnClick="CustomerEdit_Click" 
                                    Width="100px" Text='<%$ Resources:ZnodeAdminResource, ButtonEditAccount %>' />
                            </div>
                            <br />
                            <h4 class="SubTitle">
                                <asp:Localize runat="server" ID="SubTitleContact" Text='<%$ Resources:ZnodeAdminResource, SubTitleContact %>'></asp:Localize>
                            </h4>
                            <div class="ViewForm200">
                                <div class="FieldStyleA">
                                    <asp:Localize ID="ColumnTitleName" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleNameBilling %>' runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize ID="ColumnTitleCompany" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleCompany %>' runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblCompanyName" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="ColumnTitleBillingPhone" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleBillingPhone %>' runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblPhone" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize ID="ColumnTitleEmail" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleEmail %>' runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA" nowrap="nowrap">
                                    <asp:Localize ID="EmailOpt" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleEmailOptIn %>' runat="server"></asp:Localize>
                                </div>
                                <div valign="top" class="ValueStyleA">
                                    <img id="EmailOptin" runat="server" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                                <ZNode:Spacer ID="Spacer10" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                                <h4 class="SubTitle">
                                    <asp:Localize ID="SubTitleAccount" Text='<%$ Resources:ZnodeAdminResource,SubTitleAccountInfo %>' runat="server"></asp:Localize>
                                </h4>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="ColumnAccountID" Text='<%$ Resources:ZnodeAdminResource,ColumnAccountID %>' runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblAccountID" runat="server" Text="Label"></asp:Label>
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize ID="ExternalID" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleExternalID %>' runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblExternalAccNumber" runat="server" Text="Label" Width="98px"></asp:Label>
                                </div>
                                <asp:Panel runat="server" ID="pnlCustomerPricing">
                                    <div class="FieldStyleA" nowrap="nowrap">
                                        <asp:Localize ID="ColumnTitleCustomerBasedPriceEnable" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCustomerBasedPriceEnable %>' runat="server"></asp:Localize>
                                    </div>
                                    <div valign="top" class="ValueStyleA">
                                        <img id="CustomerBasedPriceEnable" runat="server" />
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>

                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnTitleLoginName" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleLoginName %>' runat="server"></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblLoginName" runat="server" Text="Label"></asp:Label>&nbsp;
                                    </div>
                                </asp:Panel>

                                <ZNode:Spacer ID="Spacer11" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                                <h4 class="SubTitle">
                                    <asp:Localize runat="server" ID="Addresses" Text='<%$ Resources:ZnodeAdminResource, SubTitleAddresses %>'></asp:Localize>
                                </h4>
                                <div>
                                    <div>
                                        <zn:Button runat="server" ID="btnAddNewAddress"  ButtonType="EditButton" OnClick="BtnAddNewAddress_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddNewAddress %>' />
                                    </div>
                                    <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>

                                    <asp:GridView ID="gvAddress" ShowFooter="True" CaptionAlign="Left" runat="server"
                                        CellPadding="4" AutoGenerateColumns="False" CssClass="Grid" Width="100%" GridLines="None"
                                        AllowPaging="True" PageSize="5" EmptyDataText='<%$ Resources:ZnodeAdminResource,GridEmptyAddressText %>' OnPageIndexChanging="GvAddress_PageIndexChanging"
                                        OnRowCommand="GvAddress_RowCommand">
                                        <Columns>
                                            <asp:BoundField DataField="Name" HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleName %>'>
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleDefaultShipping %>'>
                                                <ItemTemplate>
                                                    <img id="Img11" runat="server" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsDefaultShipping").ToString()))%>' />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleDefaultBilling %>'>
                                                <ItemTemplate>
                                                    <img alt="" id="Img12" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsDefaultBilling").ToString()))%>'
                                                        runat="server" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleAddress %>'>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAddress" runat="server" Text='<%# GetAddress(DataBinder.Eval(Container.DataItem, "AddressID").ToString())%>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Edit" runat="server" CommandArgument='<%# Eval("AddressID") %>'
                                                        CommandName="Edit" CssClass="actionlink" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' />
                                                    <asp:LinkButton ID="Delete" runat="server" CommandArgument='<%# Eval("AddressID") %>'
                                                        CommandName="Remove" CssClass="actionlink" Text='<%$ Resources:ZnodeAdminResource, LinkRemove %>' OnClientClick="return DeleteConfirmation();" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <RowStyle CssClass="RowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                        <FooterStyle CssClass="FooterStyle" />
                                    </asp:GridView>
                                </div>
                                <div>
                                    <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
                                </div>
                                <div class="ClearBoth"></div>
                                <h4 class="SubTitle">
                                    <asp:Localize ID="SubTitleAdditional" Text='<%$ Resources:ZnodeAdminResource,SubTitleAdditionalInfo %>' runat="server"></asp:Localize>
                                </h4>
                                <div>
                                    <div class="FieldStyleA">
                                        <asp:Localize ID="ColumnTitleWebsite" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleWebsite %>' runat="server"></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Label ID="lblWebSite" runat="server" Text="Label"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnTitleDescription" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleDescription %>' runat="server"></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblDescription" runat="server" Text="Label"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyleA" style="display: none">
                                        <asp:Localize ID="ContactStatus" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleContactStatus %>' runat="server"></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA" style="display: none">
                                        <asp:Label ID="lblContactTemperature" runat="server"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyleA">
                                        <asp:Localize ID="ColumnTitleSource" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleSource %>' runat="server"></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Label ID="lblSource" runat="server" Text="Label"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnTitleCreateDate" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleCreateDate %>' runat="server"></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblCreatedDate" runat="server" Text="Label"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyleA">
                                        <asp:Localize ID="ColumnTitleCreateUser" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleCreateUser %>' runat="server"></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Label ID="lblCreatedUser" runat="server" Text="Label"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnTitleUpdateDate" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleUpdateDate %>' runat="server"></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblUpdatedDate" runat="server" Text="Label"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyleA">
                                        <asp:Localize ID="ColumnTitleUpdateUser" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleUpdateUser %>' runat="server"></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Label ID="lblUpdatedUser" runat="server" Text="Label"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnTitleCustom1" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleCustom1 %>' runat="server"></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblCustom1" runat="server" Text="Label"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyleA">
                                        <asp:Localize ID="ColumnTitleCustom2" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleCustom2 %>' runat="server"></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Label ID="lblCustom2" runat="server" Text="Label"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnTitleCustom3" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleCustom3 %>' runat="server"></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblCustom3" runat="server" Text="Label"></asp:Label>&nbsp;
                                    </div>
                                </div>
                            </div>
                            <div class="ClearBoth">
                            </div>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="tabpnlNotes" runat="server">
                        <HeaderTemplate>
                            <asp:Localize ID="TabTitleNotes" Text='<%$ Resources:ZnodeAdminResource,TabTitleNotes %>' runat="server"></asp:Localize>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div>
                                <ZNode:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div class="ButtonStyle">
                                <zn:LinkButton ID="AddNewNote" runat="server"
                                    ButtonType="Button" OnClick="AddNewNote_Click" Text='<%$ Resources:ZnodeAdminResource,ButtonAddNote%>'
                                    ButtonPriority="Primary" />
                            </div>
                            <div>
                                <uc1:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                            </div>

                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleNoteList" Text='<%$ Resources:ZnodeAdminResource,SubTitleNoteList%>' runat="server"></asp:Localize>
                            </h4>
                            <asp:Repeater ID="CustomerNotes" runat="server">
                                <ItemTemplate>
                                    <asp:Label ID="Label1" CssClass="ValueStyle" runat="Server" Text='<%# FormatCustomerNote(Eval("NoteTitle"),Eval("CreateUser"),Eval("CreateDte")) %>' />
                                    <ZNode:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                                    <asp:Label ID="Label2" CssClass="ValueStyle" runat="Server" Text='<%# Eval("NoteBody") %>' />
                                    <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                                </ItemTemplate>
                            </asp:Repeater>
                            <div>
                                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="tabpnlAffiliate" runat="server">
                        <HeaderTemplate>
                            <asp:Localize ID="TabTitleAffiliate" Text='<%$ Resources:ZnodeAdminResource,TabTitleAffiliate%>' runat="server"></asp:Localize>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div>
                                <ZNode:Spacer ID="Spacer12" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div>
                                <zn:Button runat="server" Width="150px" ButtonType="EditButton" OnClick="TrackingEdit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonEditTracking%>" ID="TrackingEdit" />
                            </div>
                            <br />
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleTracking" Text='<%$ Resources:ZnodeAdminResource,SubTitleTracking%>' runat="server"></asp:Localize>
                            </h4>
                            <div class="ViewForm200">
                                <div class="FieldStyle">
                                    <asp:Localize ID="ColumnTitleTracking" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleTracking%>' runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyle" runat="server" id="dvTrackingLink">
                                    <asp:HyperLink ID="hlAffiliateLink" runat="server" Target="_blank"></asp:HyperLink>
                                </div>
                                <div class="ValueStyle" runat="server" id="dvTrackingLinkError" visible="false">
                                    <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error"></asp:Label>
                                </div>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="ColumnTitleCommissiong" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleCommissionType%>' runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblReferralType" runat="server" Text=""></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize ID="ColumnTitleCommission" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleCommission%>' runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblReferralCommission" runat="server" Text="" Width="98px"></asp:Label>
                                </div>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="ColumnTitleTaxID" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleTaxID%>' runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblTaxId" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize ID="ColumnTitlePartner" Text='<%$ Resources:ZnodeAdminResource,ColumnTitlePartner%>' runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblReferralStatus" runat="server" Text=""></asp:Label>
                                </div>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="ColumnTitleAmount" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleAmountOwed%>' runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblAmountOwed" runat="server"></asp:Label>&nbsp;
                                </div>
                            </div>
                            <br />
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleReferral" Text='<%$ Resources:ZnodeAdminResource,SubTitleReferral%>' runat="server"></asp:Localize>
                            </h4>
                            <asp:GridView ID="uxReferralCommission" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CaptionAlign="Left" CellPadding="4" CssClass="Grid" PageSize="10" OnPageIndexChanging="UxReferralCommission_PageIndexChanging"
                                EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyCustomerCommissionText %>' GridLines="None" OnRowCommand="uxReferralCommission_RowCommand"
                                Width="100%">
                                <Columns>
                                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderID%>'>
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" CausesValidation="False" ID="ViewID" Text='<%# Eval("orderid") %>'
                                                CommandArgument='<%# Eval("orderid") %>' CommandName="ViewID" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ReferralCommission" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCommission %>' DataFormatString="{0:0.00}"
                                        HeaderStyle-HorizontalAlign="Right" />
                                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleCommissionType%>' HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="ReferralCommissionTypeID" Text='<%# GetCommissionTypeById(Eval("ReferralCommissionTypeID")) %>'
                                                runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ReferralAccountID" Visible="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleReferralCustomer %>'
                                        HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="TransactionID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleTransactionID%>' HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Description" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDescription%>' HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="CommissionAmount" ItemStyle-HorizontalAlign="Right" HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleCommissionAmount%>'
                                        DataFormatString="{0:c}" />
                                </Columns>
                                <RowStyle CssClass="RowStyle" />
                                <EditRowStyle CssClass="EditRowStyle" />
                                <HeaderStyle CssClass="HeaderStyle" />
                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            </asp:GridView>
                            <div>
                                <ZNode:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <asp:Panel ID="pnlAffiliatePayment" runat="server">
                                <div class="ButtonStyle">
                                    <zn:LinkButton ID="btnAddPayment" runat="server"
                                        ButtonType="Button" OnClick="AddAffiliatePayment_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddPayment %>'
                                        ButtonPriority="Primary" />
                                </div>
                                <div>
                                    <ZNode:Spacer ID="Spacer9" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                                </div>
                                <h4 class="GridTitle">
                                    <asp:Localize ID="GridTitleAffiliate" Text='<%$ Resources:ZnodeAdminResource,GridTitleAffiliate%>' runat="server"></asp:Localize></h4>
                                </h4>
                                <asp:GridView ID="uxPaymentList" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                    CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyNoPayments %>'
                                    ForeColor="#333333" GridLines="None" Width="100%" OnPageIndexChanging="UxPaymentList_PageIndexChanging">
                                    <Columns>
                                        <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePaymentDescription %>' DataField="Description" HeaderStyle-HorizontalAlign="Left" />
                                        <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePaymentDate %>' DataField="ReceivedDate" DataFormatString="{0:d}"
                                            HeaderStyle-HorizontalAlign="Left" />
                                        <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleAmount %>' DataField="Amount" DataFormatString="{0:c}" HeaderStyle-HorizontalAlign="Left" />
                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <EditRowStyle CssClass="EditRowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" />
                                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                </asp:GridView>
                            </asp:Panel>

                            <div class="ClearBoth">
                            </div>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="tabpnlOrders" runat="server" Visible="false">
                        <HeaderTemplate>
                            <asp:Localize runat="server" ID="TabTitleOrders" Text='<%$ Resources:ZnodeAdminResource, TabTitleOrders %>'></asp:Localize>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleOrderList" Text="<%$ Resources:ZnodeAdminResource,SubTitleOrderList%>" runat="server"></asp:Localize>
                            </h4>
                            <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyNoOrders %>'
                                GridLines="None" Width="100%" OnPageIndexChanging="UxGrid_PageIndexChanging">
                                <Columns>
                                    <asp:HyperLinkField DataNavigateUrlFields="orderid" DataNavigateUrlFormatString="~/FranchiseAdmin/Secure/Orders/OrderManagement/ViewOrders/Default.aspx?itemid={0}"
                                        DataTextField="orderid" HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleOrderID%>' SortExpression="orderid" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Orderdate" DataFormatString="{0:d}" HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleOrderDate%>'
                                        HtmlEncode="False" SortExpression="OrderDate" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleOrderAmount%>' SortExpression="total" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="TotalAmount" Text='<%# FormatPrice(Eval("total")) %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderStatus%>' SortExpression="OrderStateID" HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <asp:Label ID="OrderStatus" Text='<%# DisplayOrderStatus(Eval("OrderStateID")) %>'
                                                runat="server"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <RowStyle CssClass="RowStyle" />

                                <EditRowStyle CssClass="EditRowStyle" />
                                <HeaderStyle CssClass="HeaderStyle" />
                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            </asp:GridView>
                            <div>
                                <ZNode:Spacer ID="Spacer15" SpacerHeight="1" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="tabPricing" runat="server">
                        <HeaderTemplate>
                            <asp:Localize runat="server" ID="CustomerBasedPricing" Text='<%$ Resources:ZnodeAdminResource, TabTitleCustomerBasedPricing %>'></asp:Localize>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleSearchProduct" Text="<%$ Resources:ZnodeAdminResource,SubTitleSearchProducts%>" runat="server"></asp:Localize>
                            </h4>
                            <asp:Panel ID="SearchPanel" DefaultButton="btnSearch" runat="server">
                                <div class="SearchForm">

                                    <div class="RowStyle">
                                        <div class="ItemStyle">
                                            <span class="FieldStyle">
                                                <asp:Localize ID="ColumnTitleSkuorPart" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleSkuorPart%>" runat="server"></asp:Localize>
                                            </span><br />
                                            <span class="ValueStyle">
                                                <asp:TextBox ID="txtSKU" runat="server"></asp:TextBox></span>
                                        </div>
                                        <div class="ItemStyle">
                                            <span class="FieldStyle">
                                                <asp:Localize ID="ColumnTitleExternal" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleExternal%>" runat="server"></asp:Localize>
                                            </span><br />
                                            <span class="ValueStyle">
                                                <asp:TextBox ID="txtProductCode" runat="server"></asp:TextBox></span>
                                        </div>
                                        <div class="ItemStyle">
                                            <span class="FieldStyle">
                                                <asp:Localize ID="ColumnTitleProductName" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleProductName%>" runat="server"></asp:Localize>
                                            </span><br />
                                            <span class="ValueStyle">
                                                <asp:TextBox ID="txtproductname" runat="server"></asp:TextBox></span>
                                        </div>
                                    </div>
                                    <div class="RowStyle">
                                        <div class="ItemStyle">
                                            <span class="FieldStyle">
                                                <asp:Localize ID="ColumnTitleProductCategory" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleProductCategory%>" runat="server"></asp:Localize>
                                            </span><br />
                                            <span class="ValueStyle">
                                                <ZNode:CategoryAutoComplete ID="txtCategory" runat="server" />
                                            </span>
                                        </div>
                                        <div class="ItemStyle">
                                            <span class="FieldStyle">
                                                <asp:Localize ID="ColumnTitleProductBrand" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleProductBrand%>" runat="server"></asp:Localize>
                                            </span><br />
                                            <span class="ValueStyle">
                                                <ZNode:ManufacturerAutoComplete runat="server" ID="txtManufacturer" />
                                            </span>
                                        </div>

                                    </div>
                                    <div class="ClearBoth">
                                        <br />
                                    </div>
                                    <div>
                                        <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                                        <zn:Button runat="server" ID="btnSearch" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                                        <div style="float: right">
                                            <zn:Button runat="server" ID="btnDownload" Width="150px" ButtonType="EditButton" OnClick="btnDownload_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDownloadToExcel %>' />
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                            <br />
                            <h4 class="GridTitle">
                                <asp:Localize ID="GridTitleCustomerBased" Text="<%$ Resources:ZnodeAdminResource,GridTitleCustomerBased%>" runat="server"></asp:Localize>
                            </h4>
                            <asp:Label ID="Label3" runat="server" CssClass="Error"></asp:Label>
                            <asp:GridView ID="uxCustomerPricing" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
                                GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="uxCustomerPricing_PageIndexChanging"
                                EmptyDataText='<%$ Resources:ZnodeAdminResource,GridEmptyCustomerBasedText%>'
                                Width="100%" CellPadding="4">
                                <Columns>
                                    <asp:BoundField DataField="ExternalID" HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleExtProductCode%>' HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleSkuorPart%>' DataField="SKU" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleProductName%>' DataField="Name" HeaderStyle-HorizontalAlign="Left" />

                                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleBasePrice%>' HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <%# FormatPrice(DataBinder.Eval(Container.DataItem,"BasePrice")) %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleCustomerNegotiatedPrice%>' HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <%# FormatPrice(DataBinder.Eval(Container.DataItem,"NegotiatedPrice")) %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleDiscount%>' HeaderStyle-HorizontalAlign="Left">
                                        <ItemTemplate>
                                            <%# FormatPrice(DataBinder.Eval(Container.DataItem,"Discount")) %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <FooterStyle CssClass="FooterStyle" />
                                <RowStyle CssClass="RowStyle" />
                                <PagerStyle CssClass="PagerStyle" />
                                <HeaderStyle CssClass="HeaderStyle" />
                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                            </asp:GridView>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                </ajaxToolKit:TabContainer>
            </div>
        </div>
    </div>
</asp:Content>
