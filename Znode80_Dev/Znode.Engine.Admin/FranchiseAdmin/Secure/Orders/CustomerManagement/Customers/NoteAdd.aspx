<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers.NoteAdd" ValidateRequest="false"
    Title="Untitled Page"  Codebehind="NoteAdd.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/FranchiseAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Localize runat="server" ID="AddNote" Text='<%$ Resources:ZnodeAdminResource, TitleAddNote %>'></asp:Localize>
            </h1>
        </div>
        <div style="text-align: right; padding-right: 10px;">
            <zn:Button runat="server" ID="btnSubmitTop" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancelTop" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
        </div>
        <div align="left" class="ClearBoth">
        </div>
        <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        <div class="FieldStyle">
            <asp:Localize ID="NoteTitle" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnNoteTitle%>"></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID='txtNoteTitle' runat='server' MaxLength="50" Columns="50"></asp:TextBox>
            <asp:Label ID="lblErrorTitle" runat="server" CssClass="Error" Text="Label" Visible="False"></asp:Label>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="NoteBody" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnNoteBody%>"></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
        </div>
        <div class="FieldStyle"></div>
        <div class="ValueStyle">
            <asp:Label ID="lblError" CssClass="Error" runat="server" Text="Label"
                Visible="False"></asp:Label>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <zn:Button runat="server" ID="btnSubmitBottom" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancelBottom" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
        </div>
    </div>
</asp:Content>
