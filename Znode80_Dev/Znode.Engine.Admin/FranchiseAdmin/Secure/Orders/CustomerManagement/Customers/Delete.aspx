<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Orders.CustomerManagement.Customers.Delete" Codebehind="Delete.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h5>
        <asp:Localize ID="PleaseConfirm" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextPleaseConfirm%>"></asp:Localize></h5>
    <p>
      <asp:Localize ID="DeleteAccountConfirm" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextDeleteAccount%>"></asp:Localize></p>
    <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label><br />
    <br />
    <div>
        <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnDelete_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonDelete%>" CausesValidation="True" ID="btnDelete" />
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel%>" CausesValidation="False" ID="btnCancel" />
    </div>
    <br />
</asp:Content>
