using System;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.FranchiseAdmin.Secure
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_URLList class.
    /// </summary>
    public partial class URLList : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.BindGrid();
        }

        #region Grid Events
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }
        #endregion

        #region Bind Method

        private void BindGrid()
        {
            DomainAdmin domainAdmin = new DomainAdmin();
            TList<Domain> domainList = domainAdmin.GetAllDomain();
            domainList.Sort("DomainName");
            uxGrid.DataSource = domainList;
            uxGrid.DataBind();
        }
        #endregion
    }
}