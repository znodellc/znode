using System;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Inventory.Product.Referencetypes.AddOnTypes
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_addons_delete class.
    /// </summary>
    public partial class ProductAddOnsDelete : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId;
        private string _ProductAddOnName = string.Empty;        
        private string addonName = string.Empty;
        #endregion

        /// <summary>
        /// Gets or sets the product addon name.
        /// </summary>
        public string ProductAddOnName
        {
            get { return _ProductAddOnName; }
            set { _ProductAddOnName = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring     
            ZNodeEncryption encrypt = new ZNodeEncryption();
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }
            else
            {
                this.itemId = 0;
            }

            this.BindData();
            DeleteConfirmText.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextConfirmDeleteAddOn").ToString(), this.ProductAddOnName);
        }       

        #region Events
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Inventory/Referencetypes/AddOnTypes/default.aspx");
        }

        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            bool isDeleted = false;
            try
            {
                ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
                AddOn addOn = new AddOn();
                addOn.AddOnID = this.itemId;
                this.addonName = addOn.Name;

                isDeleted = productAddOnAdmin.DeleteAddOn(addOn);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }

            if (isDeleted)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteAddOnValues").ToString() + this.addonName, this.addonName);
                Response.Redirect("~/FranchiseAdmin/Secure/Inventory/Referencetypes/AddOnTypes/default.aspx");
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteAddOnValues").ToString();
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the screen
        /// </summary>
        protected void BindData()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            AddOn addOnEntity = productAddOnAdmin.GetByAddOnId(this.itemId);
            UserStoreAccess.CheckStoreAccess(addOnEntity.PortalID.GetValueOrDefault(0), true);
            if (addOnEntity != null)
            {
                this.ProductAddOnName = addOnEntity.Name;
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotFoundProductAddOn").ToString());
            }
        }
        #endregion
    }
}