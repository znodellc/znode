using System;
using System.Web;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.FranchiseAdmin.Secure.Inventory.Product.Referencetypes.AddOnTypes
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_addons_add_Addonvalues class.
    /// </summary>
    public partial class AddOnValues : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId;
        private int addOnValueId = 0;
        private string viewLink = "~/FranchiseAdmin/Secure/Inventory/Referencetypes/AddOnTypes/view.aspx?itemid=";
        private string cancelLink = "~/FranchiseAdmin/Secure/Inventory/Referencetypes/AddOnTypes/view.aspx?itemid=";
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event - fires while page is loading
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring                 
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }
            else
            {
                this.itemId = 0;
            }

            if (Request.Params["AddOnValueId"] != null)
            {
                this.addOnValueId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["AddOnValueId"].ToString()));
            }

            if (!Page.IsPostBack)
            {
                this.BindTaxClasses();
                this.BindShippingTypes();

                // If edit func then bind the data fields
                if (this.addOnValueId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditAddOnValue").ToString();
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddAddOnValue").ToString();
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>                
        protected void BtnAddOnValueSubmit_Click(object sender, EventArgs e)
        {
            bool isUpdated = this.UpdateAddOnValue();

            if (isUpdated)
            {
                Response.Redirect(this.viewLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
            }
            else
            {
                lblAddonValueMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAddOnValue").ToString();
            }
        }

        /// <summary>
        /// Cancel Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.cancelLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Shipping Type SelecteIndex event.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ShippingTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.EnableValidators();
        }

        /// <summary>
        /// Recurring billing selected event.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChkRecurringBillingInd_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRecurringBillingInd.Checked)
            {
                pnlRecurringBilling.Visible = true;
            }
            else
            {
                pnlRecurringBilling.Visible = false;
            }
        }

        /// <summary>
        /// Recurring billing period selected.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlBillingPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Update the input fields to AddOnValue object/database.
        /// </summary>
        /// <returns>Returns true if Addon value updated else false.</returns>
        private bool UpdateAddOnValue()
        {
            try
            {
                ProductAddOnAdmin AddOnValueAdmin = new ProductAddOnAdmin();
                AddOnValue addOnValue = new AddOnValue();

                if (this.addOnValueId > 0)
                {
                    addOnValue = AddOnValueAdmin.GetByAddOnValueID(this.addOnValueId);
                }

                // Set Properties
                // General Settings
                addOnValue.AddOnID = this.itemId;
                addOnValue.Name = Server.HtmlEncode(txtAddOnValueName.Text.Trim());
                addOnValue.Description = Server.HtmlEncode(txtDescription.Text.Trim());
                addOnValue.RetailPrice = decimal.Parse(txtAddOnValueRetailPrice.Text.Trim());
                addOnValue.SalePrice = null;
                addOnValue.WholesalePrice = null;

                if (txtSalePrice.Text.Trim().Length > 0)
                {
                    addOnValue.SalePrice = decimal.Parse(txtSalePrice.Text.Trim());
                }

                if (txtWholeSalePrice.Text.Trim().Length > 0)
                {
                    addOnValue.WholesalePrice = decimal.Parse(txtWholeSalePrice.Text.Trim());
                }

                // Display Settings
                addOnValue.DisplayOrder = int.Parse(txtAddonValueDispOrder.Text.Trim());
                addOnValue.DefaultInd = chkIsDefault.Checked;

                // Inventory Settings
                addOnValue.SKU = txtAddOnValueSKU.Text.Trim();

                int quantity = 0;
                int reorder = 0;
                int.TryParse(txtAddOnValueQuantity.Text.Trim(), out quantity);
                int.TryParse(txtReOrder.Text.Trim(), out reorder);
                ProductAdmin.UpdateQuantity(addOnValue, quantity, reorder);

                if (txtAddOnValueWeight.Text.Trim().Length > 0)
                {
                    addOnValue.Weight = decimal.Parse(txtAddOnValueWeight.Text.Trim());
                }

                // Add-On Package Dimensions
                if (Height.Text.Trim().Length > 0)
                {
                    addOnValue.Height = decimal.Parse(Height.Text.Trim());
                }
                else
                {
                    addOnValue.Height = null;
                }

                if (Width.Text.Trim().Length > 0)
                {
                    addOnValue.Width = decimal.Parse(Width.Text.Trim());
                }
                else
                {
                    addOnValue.Width = null;
                }

                if (Length.Text.Trim().Length > 0)
                {
                    addOnValue.Length = decimal.Parse(Length.Text.Trim());
                }
                else
                {
                    addOnValue.Length = null;
                }

                // Set shipping type for this add-on item
                addOnValue.ShippingRuleTypeID = int.Parse(ShippingTypeList.SelectedValue);
                addOnValue.FreeShippingInd = chkFreeShippingInd.Checked;

                if (chkRecurringBillingInd.Checked)
                {
                    // Recurring Billing
                    addOnValue.RecurringBillingInd = chkRecurringBillingInd.Checked;
                    addOnValue.RecurringBillingInitialAmount = Convert.ToDecimal(txtRecurringBillingInitialAmount.Text);
                    addOnValue.RecurringBillingPeriod = ddlBillingPeriods.SelectedItem.Value;
                    addOnValue.RecurringBillingFrequency = "1";
                    addOnValue.RecurringBillingTotalCycles = 0;
                }
                else
                {
                    addOnValue.RecurringBillingInd = false;
                    addOnValue.RecurringBillingInstallmentInd = false;
                }

                // Always it will be null
                addOnValue.SupplierID = null;

                // Tax Class
                if (ddlTaxClass.SelectedIndex != -1)
                {
                    addOnValue.TaxClassID = int.Parse(ddlTaxClass.SelectedValue);
                }

                bool isSuccess = false;
                if (this.addOnValueId > 0)
                {
                    // Set update date
                    addOnValue.UpdateDte = System.DateTime.Now;

                    // Update option values
                    isSuccess = AddOnValueAdmin.UpdateAddOnValue(addOnValue);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogEditAddOnValue").ToString() + txtAddOnValueName.Text, txtAddOnValueName.Text);
                }
                else
                {
                    // Add new option values
                    isSuccess = AddOnValueAdmin.AddNewAddOnValue(addOnValue);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogAddAddOnValue").ToString() + txtAddOnValueName.Text, txtAddOnValueName.Text);
                }

                return isSuccess;
            }
            catch (Exception e)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(e.ToString());
            }

            return false;
        }

        #endregion

        #region Helper Method

        /// <summary>
        /// Enable or disable weight control validator.
        /// </summary>
        private void EnableValidators()
        {
            if (ShippingTypeList.SelectedValue == "2")
            {
                weightBasedRangeValidator.Enabled = true;
                RequiredForWeightBasedoption.Enabled = true;
            }
            else
            {
                weightBasedRangeValidator.Enabled = false;
                RequiredForWeightBasedoption.Enabled = false;
            }
        }


        #endregion

        #region Bind Methods

        /// <summary>
        /// Bind Shipping option list
        /// </summary>
        private void BindShippingTypes()
        {
            // Bind ShippingRuleTypes
            ShippingAdmin shippingAdmin = new ShippingAdmin();
            ShippingTypeList.DataSource = shippingAdmin.GetShippingRuleTypes();
            ShippingTypeList.DataTextField = "Description";
            ShippingTypeList.DataValueField = "ShippingRuleTypeId";
            ShippingTypeList.DataBind();
        }

        /// <summary>
        /// Bind tax classes list
        /// </summary>
        private void BindTaxClasses()
        {
            // Bind Tax Class
            TaxRuleAdmin TaxRuleAdmin = new TaxRuleAdmin();
            TList<TaxClass> taxClass = UserStoreAccess.CheckStoreAccess(TaxRuleAdmin.GetAllTaxClass());
			taxClass.Sort("DisplayOrder Asc");
            taxClass.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.TaxClass tax) { return tax.ActiveInd == true; });
            ddlTaxClass.DataSource = taxClass;
            ddlTaxClass.DataTextField = "name";
            ddlTaxClass.DataValueField = "TaxClassID";
            ddlTaxClass.DataBind();
        }

        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        private void BindEditData()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            AddOnValue addOnValueEntity = productAddOnAdmin.DeepLoadByAddOnValueID(this.addOnValueId);

            if (addOnValueEntity != null)
            {
                // General Settings
                lblTitle.Text += addOnValueEntity.Name;
                txtAddOnValueName.Text = Server.HtmlDecode(addOnValueEntity.Name);
                txtDescription.Text = Server.HtmlEncode(addOnValueEntity.Description);
                txtAddOnValueRetailPrice.Text = addOnValueEntity.RetailPrice.ToString("N");
                if (addOnValueEntity.SalePrice.HasValue)
                {
                    txtSalePrice.Text = addOnValueEntity.SalePrice.Value.ToString("N");
                }

                if (addOnValueEntity.WholesalePrice.HasValue)
                {
                    txtWholeSalePrice.Text = addOnValueEntity.WholesalePrice.Value.ToString("N");
                }

                if (ddlTaxClass.SelectedIndex != -1)
                {
                    ddlTaxClass.SelectedValue = addOnValueEntity.TaxClassID.GetValueOrDefault(0).ToString();
                }

                // Display Settings
                txtAddonValueDispOrder.Text = addOnValueEntity.DisplayOrder.ToString();
                chkIsDefault.Checked = addOnValueEntity.DefaultInd;

                // Inventory Settings
                txtAddOnValueSKU.Text = addOnValueEntity.SKU;

                // Load the inventory record and assign to text boxes.
                SKUInventory skuInventory = ProductAdmin.GetAddOnInventory(addOnValueEntity);

                if (skuInventory != null)
                {
                    txtAddOnValueQuantity.Text = skuInventory.QuantityOnHand.ToString();

                    if (skuInventory.ReOrderLevel.HasValue)
                    {
                        txtReOrder.Text = skuInventory.ReOrderLevel.ToString();
                    }
                }

                txtAddOnValueWeight.Text = addOnValueEntity.Weight.ToString();
                if (addOnValueEntity.Height.HasValue)
                {
                    Height.Text = addOnValueEntity.Height.Value.ToString("N2");
                }

                if (addOnValueEntity.Width.HasValue)
                {
                    Width.Text = addOnValueEntity.Width.Value.ToString("N2");
                }

                if (addOnValueEntity.Length.HasValue)
                {
                    Length.Text = addOnValueEntity.Length.Value.ToString("N2");
                }

                // Shipping Settings
                if (addOnValueEntity.ShippingRuleTypeID.HasValue)
                {
                    ShippingTypeList.SelectedValue = addOnValueEntity.ShippingRuleTypeID.Value.ToString();
                }

                this.EnableValidators();

                if (addOnValueEntity.FreeShippingInd.HasValue)
                {
                    chkFreeShippingInd.Checked = addOnValueEntity.FreeShippingInd.Value;
                }

                // Recurring Billing
                pnlRecurringBilling.Visible = addOnValueEntity.RecurringBillingInd;
                chkRecurringBillingInd.Checked = addOnValueEntity.RecurringBillingInd;
                ddlBillingPeriods.SelectedValue = addOnValueEntity.RecurringBillingPeriod;
                if (addOnValueEntity.RecurringBillingInitialAmount.HasValue)
                    txtRecurringBillingInitialAmount.Text = addOnValueEntity.RecurringBillingInitialAmount.Value.ToString("N");

                if (addOnValueEntity.AddOnIDSource != null &&
                    string.Compare(addOnValueEntity.AddOnIDSource.DisplayType, "TextBox", true) == 0)
                {
                    pnlAddOnTextBox.Visible = true;
                }
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource","RecordNotFoundAddOnValue").ToString());
            }
        }

        #endregion
    }
}