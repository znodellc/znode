using System;
using System.Web;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Inventory.Product.Referencetypes.AddOnTypes
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_addons_add class.
    /// </summary>
    public partial class ProductAddonsAdd : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId;
        private int productId = 0;
        private bool isToRedirect = false;
        private string listLink = "~/FranchiseAdmin/Secure/Inventory/Referencetypes/AddOnTypes/default.aspx";
        private string viewLink = "~/FranchiseAdmin/Secure/Inventory/Referencetypes/AddOnTypes/view.aspx?itemid=";
        private string cancelLink = "~/FranchiseAdmin/Secure/Inventory/Referencetypes/AddOnTypes/view.aspx?itemid=";
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring     
            ZNodeEncryption encrypt = new ZNodeEncryption();
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }
            else
            {
                this.itemId = 0;
            }

            // Get productid from querystring        
            if (Request.Params["zpid"] != null)
            {
                this.productId = int.Parse(Request.Params["zpid"]);
            }

            // Reset the edit fields
            if (Request.Params["mode"] != null)
            {
                this.isToRedirect = bool.Parse(Request.Params["mode"]);
            }

            // Redirect directly to product details page
            if (this.isToRedirect)
            {
                // Reset the URL with Product details page
                this.listLink = "~/FranchiseAdmin/Secure/catalog/product/view.aspx?itemid=" + this.productId + "&mode=addons";
                this.viewLink = this.listLink;
                this.cancelLink = this.listLink;
            }

            if (Page.IsPostBack == false)
            {
                // If edit func then bind the data fields
                if (this.itemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditAddOn").ToString();
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddAddOn").ToString();
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            AddOn addOnEntity = new AddOn();
            ZNodeEncryption encrypt = new ZNodeEncryption();

            if (this.itemId > 0)
            {
                addOnEntity = productAddOnAdmin.GetByAddOnId(this.itemId);
            }

            // Set properties - General settings
            addOnEntity.Name = Server.HtmlEncode(txtName.Text.Trim());
            addOnEntity.Title = Server.HtmlEncode(txtAddOnTitle.Text.Trim());
            addOnEntity.DisplayOrder = int.Parse(txtDisplayOrder.Text.Trim());
            addOnEntity.OptionalInd = chkOptionalInd.Checked;
            addOnEntity.Description = ctrlHtmlText.Html;
            addOnEntity.DisplayType = ddlDisplayType.SelectedItem.Value;

            // Set properties - Inventory settings
            // Out of Stock Options
            if (InvSettingRadiobtnList.SelectedValue.Equals("1"))
            {
                // Only Sell if Inventory Available - Set values
                addOnEntity.TrackInventoryInd = true;
                addOnEntity.AllowBackOrder = false;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("2"))
            {
                // Allow Back Order - Set values
                addOnEntity.TrackInventoryInd = true;
                addOnEntity.AllowBackOrder = true;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("3"))
            {
                // Don't Track Inventory - Set property values
                addOnEntity.TrackInventoryInd = false;
                addOnEntity.AllowBackOrder = false;
            }

            // Inventory Setting - Stock Messages
            if (txtOutofStock.Text.Trim().Length == 0)
            {
                addOnEntity.OutOfStockMsg = this.GetGlobalResourceObject("ZnodeAdminResource", "ValueOutOfStock").ToString();
            }
            else
            {
                addOnEntity.OutOfStockMsg = Server.HtmlEncode(txtOutofStock.Text.Trim());
            }

            addOnEntity.InStockMsg = Server.HtmlEncode(txtInStockMsg.Text.Trim());
            addOnEntity.BackOrderMsg = Server.HtmlEncode(txtBackOrderMsg.Text.Trim());

            // LocaleID for English
            addOnEntity.LocaleId = 43;
            addOnEntity.PortalID = UserStoreAccess.GetTurnkeyStorePortalID;

            bool isSuccess = false;
            if (this.itemId > 0)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditAddOns").ToString() + txtName.Text.Trim(), txtName.Text.Trim());
                isSuccess = productAddOnAdmin.UpdateNewProductAddOn(addOnEntity);
            }
            else
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditAddOns").ToString() + txtName.Text.Trim(), txtName.Text.Trim());
                isSuccess = productAddOnAdmin.CreateNewProductAddOn(addOnEntity, out this.itemId);
            }

            if (isSuccess)
            {
                if (this.isToRedirect)
                {
                    Response.Redirect(this.viewLink);
                }

                Response.Redirect(this.viewLink + HttpUtility.UrlEncode(encrypt.EncryptData(this.itemId.ToString())));
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAddOn").ToString();
                lblMsg.CssClass = "Error";
                return;
            }
        }

        /// <summary>
        /// Cancel Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            if (this.itemId > 0)
            {
                Response.Redirect(this.cancelLink + HttpUtility.UrlEncode(encrypt.EncryptData(this.itemId.ToString())));
            }
            else
            {
                Response.Redirect(this.listLink);
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        private void BindEditData()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            AddOn addOnEntity = productAddOnAdmin.GetByAddOnId(this.itemId);
            UserStoreAccess.CheckStoreAccess(addOnEntity.PortalID.GetValueOrDefault(0), true);
            if (addOnEntity != null)
            {
                lblTitle.Text += addOnEntity.Name;
                txtName.Text = Server.HtmlDecode(addOnEntity.Name);
                txtAddOnTitle.Text = Server.HtmlDecode(addOnEntity.Title);
                txtDisplayOrder.Text = addOnEntity.DisplayOrder.ToString();
                chkOptionalInd.Checked = addOnEntity.OptionalInd;
                ctrlHtmlText.Html = addOnEntity.Description;
                ddlDisplayType.SelectedValue = addOnEntity.DisplayType;

                // Inventory Setting - Out of Stock Options
                if (addOnEntity.TrackInventoryInd && addOnEntity.AllowBackOrder == false)
                {
                    InvSettingRadiobtnList.Items[0].Selected = true;
                }
                else if (addOnEntity.TrackInventoryInd && addOnEntity.AllowBackOrder)
                {
                    InvSettingRadiobtnList.Items[1].Selected = true;
                }
                else if ((addOnEntity.TrackInventoryInd == false) && (addOnEntity.AllowBackOrder == false))
                {
                    InvSettingRadiobtnList.Items[2].Selected = true;
                }

                // Inventory Setting - Stock Messages
                txtInStockMsg.Text = Server.HtmlDecode(addOnEntity.InStockMsg);
                txtOutofStock.Text = Server.HtmlDecode(addOnEntity.OutOfStockMsg);
                txtBackOrderMsg.Text = Server.HtmlDecode(addOnEntity.BackOrderMsg);
            }
            else
            {
                throw new ApplicationException(GetGlobalResourceObject("ZnodeAdminResource", "RecordNotFoundAddOnRequested").ToString());
            }
        }
        #endregion
    }
}