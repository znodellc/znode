using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.FranchiseAdmin.Secure.Inventory.Product.Referencetypes.AddOnTypes
{
    /// <summary>
    /// Represents the Franchise Admin CLASS class.
    /// </summary>
    public partial class ProductAddonsView : System.Web.UI.Page
    {
        #region Private Variables
        private int _ItemId;
        private string addOnValuepageLink = "~/FranchiseAdmin/Secure/Inventory/Referencetypes/AddOnTypes/AddOnValues.aspx?itemid=";
        private string editLink = "~/FranchiseAdmin/Secure/Inventory/Referencetypes/AddOnTypes/add.aspx?itemid=";
        private string listLink = "~/FranchiseAdmin/Secure/Inventory/Referencetypes/AddOnTypes/default.aspx";
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        /// <summary>
        /// Gets or sets the item Id.
        /// </summary>
        public int ItemId
        {
            get { return this._ItemId; }
            set { this._ItemId = value; }
        }

        #region Helper Methods
        /// <summary>
        /// Get the disabled icon relative path.
        /// </summary>
        /// <returns>Returns the disabled icon relative path.</returns>
        protected string SetImage()
        {
            return ZNode.Libraries.Admin.Helper.GetCheckMark(false);
        }

        /// <summary>
        /// Get addon quantity.
        /// </summary>
        /// <param name="addOnValueId">AddOnValueId to get the Addon value.</param>
        /// <returns>Returns the addon quantity.</returns>
        protected string GetQuantity(int addOnValueId)
        {
            ZNode.Libraries.DataAccess.Service.AddOnValueService avs = new ZNode.Libraries.DataAccess.Service.AddOnValueService();
            return ProductAdmin.GetQuantity(avs.GetByAddOnValueID(addOnValueId)).ToString();
        }

        /// <summary>
        /// Get the reorder level
        /// </summary>
        /// <param name="addOnValueId">AddOnValueId to get the Addon inventory.</param>
        /// <returns>Returns the product reorder value.</returns>
        protected string GetReOrderLevel(int addOnValueId)
        {
            ZNode.Libraries.DataAccess.Service.AddOnValueService addOnValueService = new ZNode.Libraries.DataAccess.Service.AddOnValueService();
            return ProductAdmin.GetAddOnInventory(addOnValueService.GetByAddOnValueID(addOnValueId)).ReOrderLevel.ToString();
        }

        /// <summary>
        /// Encrypt the addon Id
        /// </summary>
        /// <param name="addOnId">Addon Id to encrypt.</param>
        /// <returns>Returns the encrypted Addon Id</returns>
        protected string GetEncryptID(object addOnId)
        {
            return HttpUtility.UrlEncode(this.encrypt.EncryptData(addOnId.ToString()));
        }

        #endregion

        #region Page Load Event
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring                
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Create New AddOnValue Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>        
        protected void BtnAddNewAddOnValues_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addOnValuepageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.ItemId.ToString())));
        }

        /// <summary>
        /// Back button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listLink);
        }

        /// <summary>
        /// Edit Add-On Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditAddOn_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.editLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.ItemId.ToString())));
        }

        /// <summary>
        /// BAck to Add-On Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BacktoAddOn_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listLink);
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Seventh column.
                LinkButton deleteButton = (LinkButton)e.Row.Cells[7].FindControl("btnDelete");

                // Set the Button's CommandArgument property with the row's index.
                deleteButton.CommandArgument = e.Row.RowIndex.ToString();

                // Add Client Side confirmation
                deleteButton.OnClientClick = "return confirm('" + this.GetGlobalResourceObject("ZnodeAdminResource", "ConfirmDeleteAddOn").ToString() + "');";
            }
        }

        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        /// <summary>
        /// Even triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // cell in the GridView control.
                GridViewRow selectedRow = uxGrid.Rows[index];

                TableCell tableCell = selectedRow.Cells[0];
                string Id = tableCell.Text;
                if (e.CommandName == "Edit")
                {
                    this.editLink = this.addOnValuepageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.ItemId.ToString())) + "&AddOnValueId=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(Id));
                    Response.Redirect(this.editLink);
                }

                if (e.CommandName == "Delete")
                {
                    ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();

                    // Get the Addon name from itemId
                    AddOn addon = new AddOn();
                    addon = productAddOnAdmin.GetByAddOnId(this.ItemId);
                    string addonName = addon.Name;

                    // Get AddOnValue name from Id
                    AddOnValue addOnValue = new AddOnValue();
                    addOnValue = productAddOnAdmin.GetByAddOnValueID(int.Parse(Id));

                    string associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteAddOnValues").ToString() + addOnValue.Name;
                    bool isDeleted = productAddOnAdmin.DeleteAddOnValue(int.Parse(Id));
                    if (isDeleted)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, addonName);
                        this.BindGrid();
                    }
                }
            }
        }
        #endregion

        #region Bind Methods
        /// <summary>
        ///  Bind data to grid
        /// </summary>
        private void BindGrid()
        {
            ProductAddOnAdmin addOnValueAdmin = new ProductAddOnAdmin();
            TList<AddOnValue> valueList = addOnValueAdmin.GetAddOnValuesByAddOnId(ItemId);
            if (valueList != null)
            {
                valueList.Sort("DisplayOrder");
            }

            uxGrid.DataSource = valueList;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        private void BindData()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            AddOn addOnEntity = productAddOnAdmin.GetByAddOnId(this.ItemId);
            UserStoreAccess.CheckStoreAccess(addOnEntity.PortalID.GetValueOrDefault(0), true);

            if (addOnEntity != null)
            {
                lblTitle.Text = addOnEntity.Name;
                lblName.Text = addOnEntity.Name;
                lblAddOnTitle.Text = addOnEntity.Title;
                lblDisplayOrder.Text = addOnEntity.DisplayOrder.ToString();
                lblDisplayType.Text = addOnEntity.DisplayType.ToString();

                // Display Settings
                chkOptionalInd.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(addOnEntity.OptionalInd.ToString()));

                // Inventory Setting - Out of Stock Options
                if (addOnEntity.TrackInventoryInd && addOnEntity.AllowBackOrder == false)
                {
                    chkCartInventoryEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                    chkIsBackOrderEnabled.Src = this.SetImage();
                    chkIstrackInvEnabled.Src = this.SetImage();
                }
                else if (addOnEntity.TrackInventoryInd && addOnEntity.AllowBackOrder)
                {
                    chkCartInventoryEnabled.Src = this.SetImage();
                    chkIsBackOrderEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                    chkIstrackInvEnabled.Src = this.SetImage();
                }
                else if ((addOnEntity.TrackInventoryInd == false) && (addOnEntity.AllowBackOrder == false))
                {
                    chkCartInventoryEnabled.Src = this.SetImage();
                    chkIsBackOrderEnabled.Src = this.SetImage();
                    chkIstrackInvEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                }

                this.BindGrid();

                // Inventory Setting - Stock Messages
                lblInStockMsg.Text = addOnEntity.InStockMsg;
                lblOutofStock.Text = addOnEntity.OutOfStockMsg;
                lblBackOrderMsg.Text = addOnEntity.BackOrderMsg;
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotFoundAddOnRequested").ToString());
            }
        }
        #endregion
    }
}