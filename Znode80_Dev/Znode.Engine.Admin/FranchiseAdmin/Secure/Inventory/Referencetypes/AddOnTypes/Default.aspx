<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.FranchiseAdmin.Secure.Inventory.Product.Referencetypes.AddOnTypes.ProductAddonsList" Title="Untitled Page" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1 class="LeftFloat">
                <asp:Localize ID="AddOnTypes" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleAddOnTypes %>'></asp:Localize></h1>
            <div class="tooltip AddOnType">
                <a href="javascript:void(0);" class="learn-more"></a>
                <div class="content">
                    <h6>
                        <asp:Localize ID="TitleHelp" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleHelp%>'></asp:Localize></h6>
                    <p>
                        <asp:Localize ID="TextAddon" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextAddon%>'></asp:Localize>
                    </p>
                </div>
            </div>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAddCategory" runat="server" CausesValidation="False"
                ButtonType="Button" OnClick="BtnAddCategory_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCreateNewAddOn %>'
                ButtonPriority="Primary" />
        </div>
        <div>
            <uc1:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="ClearBoth">
            <p>
                <asp:Localize ID="AddOnTypesText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextAddOnTypes %>'></asp:Localize>
            </p>
        </div>
        <br />
        <h4 class="SubTitle">
            <asp:Localize ID="SearchAddOnsSubTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleAddOns %>'></asp:Localize></h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleName%>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAddonName" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="TitleColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTitle %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAddOnTitle" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="SKU" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSkuorPart %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtsku" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <zn:Button runat="server" ID="btnClear" OnClick="BtnClear_Click" CausesValidation="False" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' />
                    <zn:Button runat="server" ID="btnSearch" OnClick="BtnSearch_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle">
            <asp:Localize ID="GridTitleAddOnList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleAddOnList %>'></asp:Localize></h4>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" EnableSortingAndPagingCallbacks="False"
            PageSize="25" AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundAddOnTypes %>'>
            <Columns>
                <asp:BoundField DataField="AddOnId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="Title" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleTitle %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='view.aspx?itemid=<%# GetAddOnID(DataBinder.Eval(Container.DataItem, "AddOnId"))%>'>
                            <%# Eval("Name") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsOptional %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "OptionalInd").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:ButtonField CommandName="Manage" Text='<%$ Resources:ZnodeAdminResource, LinkManage %>' ButtonType="Link">
                    <ControlStyle CssClass="actionlink" />
                </asp:ButtonField>
                <asp:ButtonField CommandName="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' ButtonType="Link">
                    <ControlStyle CssClass="actionlink" />
                </asp:ButtonField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>
