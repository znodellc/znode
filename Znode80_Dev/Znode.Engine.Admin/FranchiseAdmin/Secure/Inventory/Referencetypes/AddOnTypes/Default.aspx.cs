using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Inventory.Product.Referencetypes.AddOnTypes
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_addons_list class.
    /// </summary>
    public partial class ProductAddonsList : System.Web.UI.Page
    {
        #region Private Variables
        private static bool isSearchEnabled = false;
        private string addLink = "~/FranchiseAdmin/Secure/Inventory/Referencetypes/AddOnTypes/add.aspx";
        private string viewLink = "~/FranchiseAdmin/Secure/Inventory/Referencetypes/AddOnTypes/view.aspx";
        private string deleteLink = "~/FranchiseAdmin/Secure/Inventory/Referencetypes/AddOnTypes/delete.aspx";
        #endregion

        #region Helper methods
        /// <summary>
        /// Get the encrypted AddOn Id 
        /// </summary>
        /// <param name="addOnId">AddOnId to encrypt.</param>
        /// <returns>Returns the encrypted AddOn Id.</returns>
        protected string GetAddOnID(object addOnId)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            return HttpUtility.UrlEncode(encrypt.EncryptData(addOnId.ToString()));
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindGridData();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Add New Product AddOn Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddCategory_Click(object sender, System.EventArgs e)
        {
            Response.Redirect(this.addLink);
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.SearchAddOns();
            isSearchEnabled = true;
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // Check if search is enabled
            if (isSearchEnabled)
            {
                uxGrid.PageIndex = e.NewPageIndex;

                // Re-bind the grid.
                this.SearchAddOns();
            }
            else
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindGridData();
            }
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // cell in the GridView control.
                GridViewRow selectedRow = uxGrid.Rows[index];

                TableCell tableCell = selectedRow.Cells[0];
                string Id = tableCell.Text;
                ZNodeEncryption encrypt = new ZNodeEncryption();

                if (e.CommandName == "Manage")
                {
                    this.viewLink = this.viewLink + "?itemid=" + HttpUtility.UrlEncode(encrypt.EncryptData(Id));
                    Response.Redirect(this.viewLink);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.deleteLink + "?itemid=" + HttpUtility.UrlEncode(encrypt.EncryptData(Id)));
                }
            }
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(productAddOnAdmin.GetAllAddOns());
            uxGrid.DataBind();
        }

        /// <summary>
        /// Search AddOn
        /// </summary>
        private void SearchAddOns()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            int localeId = 0;
            uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(productAddOnAdmin.SearchAddOns(Server.HtmlEncode(txtAddonName.Text.Trim()), Server.HtmlEncode(txtAddOnTitle.Text.Trim()), txtsku.Text.Trim(), localeId, 0, UserStoreAccess.GetTurnkeyStoreCatalogs[0].PortalID.GetValueOrDefault(0)).Tables[0]);
            uxGrid.DataBind();
        }
        #endregion
    }
}