<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Inventory.Default" Title="Inventory - Default" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="LeftMargin">
        <h1>
            <asp:Localize runat="server" ID="Inventory" Text='<%$ Resources:ZnodeAdminResource, TitleInventory %>'></asp:Localize>
        </h1>
        <hr />
        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/NewProducts.png" />
            </div>
            <div class="Shortcut">
                <a id="A3" href="~/FranchiseAdmin/Secure/Inventory/Products/Default.aspx" runat="server">
                    <asp:Localize runat="server" ID="TextProducts" Text='<%$ Resources:ZnodeAdminResource, LinkTextProducts %>'></asp:Localize>
                </a>
            </div>
            <div class="LeftAlign">
                <p>
                    <asp:Localize runat="server" ID="TextProduct" Text='<%$ Resources:ZnodeAdminResource, TextProducts %>'></asp:Localize>
                </p>
            </div>
        </div>
        <h1>
            <asp:Localize runat="server" ID="ReferenceTypes" Text='<%$ Resources:ZnodeAdminResource, TitleReferenceTypes %>'></asp:Localize>
        </h1>
        <hr />
        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/add-ons.png" />
            </div>
            <div class="Shortcut">
                <a id="A4" href="~/FranchiseAdmin/Secure/Inventory/ReferenceTypes/AddOnTypes/Default.aspx" runat="server">
                    <asp:Localize runat="server" ID="AddOnTypes" Text='<%$ Resources:ZnodeAdminResource, LinkTextAddOnTypes %>'></asp:Localize>
                </a>
            </div>
            <div class="LeftAlign">
                <p>
                    <asp:Localize runat="server" ID="TextAddOnTypes" Text='<%$ Resources:ZnodeAdminResource, TextAddOnTypes %>'></asp:Localize>
                </p>
            </div>
        </div>
    </div>
</asp:Content>

