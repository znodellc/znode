using System;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_products_delete class.
    /// </summary>
    public partial class ProductsDelete : System.Web.UI.Page
    {
        #region Private  Member Variables
        private int itemId;
        private string _ProductName = string.Empty;        
        private string cancelLink = "default.aspx";
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        /// <summary>
        /// Gets or sets the product name.
        /// </summary>
        public string ProductName
        {
            get { return _ProductName; }
            set { _ProductName = value; }
        }

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring   
            if (Request.Params["Itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
                this.BindData();
            }
            else
            {
                this.itemId = 0;
            }
        }
        #endregion       

        #region General Events

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.cancelLink);
        }

        /// <summary>
        /// Occurs when delete button clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = productAdmin.GetByProductId(this.itemId);
            this.ProductName = product.Name;
            bool isDeleted = productAdmin.DeleteByProductID(this.itemId);

            if (isDeleted)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Delete Product - " + this.ProductName, this.ProductName);

                Response.Redirect(this.cancelLink);
            }
            else
            {
                lblErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductCannotDelete").ToString();
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind the data for a Product id
        /// </summary>
        private void BindData()
        {
            ProductAdmin productAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product ProductList = productAdmin.GetByProductId(this.itemId);
            UserStoreAccess.CheckStoreAccess(ProductList.PortalID.GetValueOrDefault(0), true);
            if (ProductList != null)
            {
                this.ProductName = ProductList.Name;
            }
        }
        #endregion
    }
}