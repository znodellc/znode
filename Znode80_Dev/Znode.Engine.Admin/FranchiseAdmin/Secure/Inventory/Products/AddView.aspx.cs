using System;
using System.Web;
using System.Web.UI;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace  Znode.Engine.FranchiseAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_add_view class.
    /// </summary>
    public partial class AddView : System.Web.UI.Page
    {
        #region Private member variables
        private int itemId = 0;
        private int productImageId = 0;
        private int productTypeId = 0;
        private string associateName = string.Empty;
        private string editLink = "~/FranchiseAdmin/Secure/Inventory/Products/view.aspx?itemid=";
        private string cancelLink = "~/FranchiseAdmin/Secure/Inventory/Products/view.aspx?itemid=";
        private ProductViewAdmin imageAdmin = new ProductViewAdmin();
        private ProductImage productImage = new ProductImage();
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        private ProductAdmin productAdmin = new ProductAdmin();
        private ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();
        #endregion

        #region page load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
                if (!UserStoreAccess.CheckUserRoleInProduct(ProfileCommon.Create(HttpContext.Current.User.Identity.Name, true), this.itemId))
                {
                    Response.Redirect("default.aspx");
                }
            }

            if (Request.Params["productimageid"] != null)
            {
                this.productImageId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["productimageid"].ToString()));
            }

            if (Request.Params["typeid"] != null)
            {
                this.productTypeId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["typeid"].ToString()));
            }

            if (!Page.IsPostBack)
            {
                product = productAdmin.GetByProductId(this.itemId);
                if ((this.itemId > 0) && (this.productImageId > 0))
                {
                    lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditAlternateImage").ToString()+this.product.Name;
                    this.BindDatas();
                    this.BindImageType();

                    tblShowImage.Visible = true;
                    txtimagename.Visible = true;
                }
                else
                {
                    // Show the image upload control, when adding new product.
                    tblShowImage.Visible = true;
                    tblProductDescription.Visible = true;
                    pnlShowOption.Visible = false;
                    ProductAlternateImage.Visible = false;

                    lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddAlternateImage").ToString() + this.product.Name;
                    this.BindImageType();
                }
            }
        }
        #endregion              

        #region General events
        /// <summary>
        /// Occurs when submit button clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();

            System.IO.FileInfo fileInfo = null;
            bool isSwatchSelected = false;
            bool isNewProductImageSelected = false;

            if (this.productImageId > 0)
            {
                this.productImage = this.imageAdmin.GetByProductImageID(this.productImageId);
            }

            if (this.productImage.ProductImageTypeID.GetValueOrDefault(0) != Convert.ToInt32(ImageType.SelectedValue))
            {
                // If swatch is selected
                isSwatchSelected = true && Convert.ToInt32(ImageType.SelectedValue) == 2;
            }

            this.productImage.Name = Server.HtmlEncode(txttitle.Text);
            this.productImage.ActiveInd = VisibleInd.Checked;
            this.productImage.ShowOnCategoryPage = VisibleCategoryInd.Checked;
            this.productImage.ProductID = this.itemId;
            this.productImage.ProductImageTypeID = Convert.ToInt32(ImageType.SelectedValue);
            this.productImage.DisplayOrder = int.Parse(DisplayOrder.Text.Trim());
            this.productImage.ImageAltTag = txtImageAltTag.Text.Trim();
            this.productImage.AlternateThumbnailImageFile = txtAlternateThumbnail.Text.Trim();
            this.productImage.ReviewStateID = Convert.ToInt32(ZNodeProductReviewState.Approved);
            UploadProductImage.AppendToFileName = "-" + this.itemId.ToString();
            
            // Validate image
            if ((this.productImageId == 0) || (RadioProductNewImage.Checked == true))
            {
                if (!UploadProductImage.IsFileNameValid())
                {
                    return;
                }

                // Check for Product View Image
                fileInfo = UploadProductImage.FileInformation;
                if (fileInfo != null)
                {
                    this.productImage.ImageFile = "Turnkey/" + UserStoreAccess.GetTurnkeyStorePortalID.Value.ToString() + "/" + fileInfo.Name;
                    if (ImageType.SelectedItem.Value == "2")
                    {
                        this.productImage.AlternateThumbnailImageFile = "Turnkey/" + UserStoreAccess.GetTurnkeyStorePortalID.Value.ToString() + "/" + fileInfo.Name;
                    }
                }
                
            }
            else
            {
                this.productImage.ImageFile = this.productImage.ImageFile;
            }

                       // Upload File if this is a new product or the New Image option was selected for an existing product
            if (RadioProductNewImage.Checked || this.productImageId == 0)
            {
                if (fileInfo != null)
                {
                    UploadProductImage.SaveImage("fadmin", UserStoreAccess.GetTurnkeyStorePortalID.Value.ToString());

                    // Release all resources used
                    UploadProductImage.Dispose();
                }
            }

            if (isSwatchSelected && this.productImageId > 0 && !isNewProductImageSelected)
            {
                // Create fileInfo for Original product View Image
                fileInfo = UploadProductImage.FileInformation;
                if (fileInfo != null)
                {
                    this.productImage.AlternateThumbnailImageFile = fileInfo.Name;
                }
            }

            bool isSuccess = false;
            product = productAdmin.GetByProductId(this.itemId);          

            if (this.productImageId > 0)
            {
                // Update the Imageview
                this.associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditAlternateImage").ToString() + this.productImage.ImageFile + " - " + product.Name;
                isSuccess = this.imageAdmin.Update(this.productImage);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, product.Name);
            }
            else
            {
                this.associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddAlternateImage").ToString() + this.productImage.ImageFile + " - " + product.Name;
                isSuccess = this.imageAdmin.Insert(this.productImage);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, product.Name);
            }

            if (isSuccess)
            {
                product.ReviewStateID = productAdmin.UpdateReviewStateID(UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId, "Alternate / Swatch Image Changed");

                productAdmin.Update(product);

                Response.Redirect(this.editLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())) + "&mode=" + HttpUtility.UrlEncode(this.encrypt.EncryptData("views")));
            }
            else
            {
                // Display error message
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoteAdd").ToString();
            }
        }

        /// <summary>
        /// Occurs when cancel button clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.cancelLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())) + "&mode=" + HttpUtility.UrlEncode(this.encrypt.EncryptData("views")));
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = true;
        }

        /// <summary>
        /// Dropdown list Seclected Index changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ImageType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetVisibleData();
        }

        #endregion

        #region Bind event
        /// <summary>
        /// Bind Datas
        /// </summary>
        private void BindDatas()
        {
            this.productImage = this.imageAdmin.GetByProductImageID(this.productImageId);
            ZNodeImage znodeImage = new ZNodeImage();
            if (this.productImage != null)
            {
                ProductAdmin productAdmin = new ProductAdmin();
                ZNode.Libraries.DataAccess.Entities.Product product = productAdmin.GetByProductId(this.productImage.ProductID);

                if (product != null)
                {
                    UserStoreAccess.CheckStoreAccess(product.PortalID.GetValueOrDefault(0), true);
                }

                txtimagename.Text = Server.HtmlDecode(this.productImage.ImageFile);
                txttitle.Text = Server.HtmlDecode(this.productImage.Name);
                VisibleInd.Checked = this.productImage.ActiveInd;
                VisibleCategoryInd.Checked = this.productImage.ShowOnCategoryPage;
                ProductAlternateImage.ImageUrl = znodeImage.GetImageHttpPathSmall(this.productImage.ImageFile);
                ImageType.SelectedValue = this.productImage.ProductImageTypeID.ToString();
                DisplayOrder.Text = this.productImage.DisplayOrder.GetValueOrDefault().ToString();
                txtImageAltTag.Text = this.productImage.ImageAltTag;
                txtAlternateThumbnail.Text = this.productImage.AlternateThumbnailImageFile;

                if (txtAlternateThumbnail.Text != string.Empty)
                {
                    ProductAlternateImage.ImageUrl = znodeImage.GetImageHttpPathSmall(this.productImage.AlternateThumbnailImageFile);
                }
            }
        }

        /// <summary>
        /// Binds ImageType Type drop-down list
        /// </summary>
        private void BindImageType()
        {
            ZNode.Libraries.Admin.ProductViewAdmin imageadmin = new ProductViewAdmin();
            ImageType.DataSource = this.imageAdmin.GetImageType();
            ImageType.DataTextField = "Name";
            ImageType.DataValueField = "ProductImageTypeID";
            ImageType.DataBind();

            this.SetVisibleData();
        }

        #endregion       

        #region Private Methods
        /// <summary>
        /// Set the controls visibility.
        /// </summary>
        private void SetVisibleData()
        {
            AlternateThumbnail.Visible = false;
            SwatchHint.Visible = false;
            ProductHint.Visible = false;
            
            if (ImageType.SelectedItem.Value == "1")
            {
                AlternateThumbnail.Visible = false;
                lblImage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnProductImage").ToString();
                lblImageName.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextImageName").ToString(); 
                ProductHint.Visible = true;
            }
            else if (ImageType.SelectedItem.Value == "2")
            {
                AlternateThumbnail.Visible = true;
                lblImage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnSwatchImage").ToString();
                lblImageName.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextSwatchImageFileName").ToString(); 
                SwatchHint.Visible = true;
            }
        } 
        #endregion
    }
}