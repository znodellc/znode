using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Data;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.FranchiseAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_products_view class.
    /// </summary>
    public partial class ProductsView : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId;
        private string mode = string.Empty;
        private bool isAdminProduct = false;
        private string editPageLink = "add.aspx?itemid=";
        private string editSEOLink = "EditSeo.aspx?itemid=";
        private string editAdvancedPageLink = "EditAdvancedSettings.aspx?itemid=";
        private string addTieredPricingPageLink = "AddTieredPricing.aspx?itemid=";
        private string addDigitalAssetPageLink = "~/FranchiseAdmin/Secure/Inventory/Products/AddDigitalAsset.aspx?itemid=";
        private string addProductAddOnLink = "~/FranchiseAdmin/Secure/Inventory/Products/AddAddons.aspx?";
        private string listLink = "~/FranchiseAdmin/Secure/Inventory/Products/default.aspx";
        private string previewLink = "/product.aspx?zpid=";
        private string addSKULink = "~/FranchiseAdmin/Secure/Inventory/Products/AddSku.aspx?itemid=";
        private string addViewlink = "~/FranchiseAdmin/Secure/Inventory/Products/AddView.aspx?itemid=";
        private string addHighlightPageLink = "~/FranchiseAdmin/Secure/Inventory/Products/AddHighlights.aspx?itemid=";
        private string editHighlightPageLink = "~/FranchiseAdmin/Secure/catalog/product_Highlights/add.aspx?itemid=";
        private string associateCategoryPageLink = "AddProductCategory.aspx?itemid=";
        private string AddBundleProductsLink = "addbundleproduct.aspx?itemid=";
        private string viewProductReviewPageLink = "~/FranchiseAdmin/Secure/Inventory/Products/ViewProductReview.aspx?itemid=";
        private bool hasAttributes = false;
        private ZNodeEncryption encrypt = new ZNodeEncryption();

        /// <summary>
        /// Gets or sets a value of Product ProductTypeID.
        /// </summary>
        private int ProductTypeId
        {
            get
            {
                int value;
                int.TryParse(ViewState["ProductTypeID"].ToString(), out value);
                return value;
            }

            set
            {
                ViewState["ProductTypeID"] = value;
            }
        }

        #endregion

        #region Protected Helper Methods
        /// <summary>
        /// Get ImagePath path
        /// </summary>
        /// <param name="Imagefile">Image file name.</param>
        /// <returns>Returns image file name with path.</returns>
        protected string GetImagePath(string Imagefile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(Imagefile);
        }

        /// <summary>
        /// Get the product name by product Id 
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <returns>Return the name of the product</returns>
		protected string GetProductName(object productId)
		{
			ProductAdmin productAdmin = new ProductAdmin();
			return productAdmin.GetByProductId(Convert.ToInt32(productId)).Name;
		}

        /// <summary>
        /// Returns a Format Weight string
        /// </summary>
        /// <param name="weight">Product weight to format.</param>
        /// <returns>Returns the formatted product weight.</returns>
        protected string FormatProductWeight(object weight)
        {
            if (weight == null)
            {
                return string.Empty;
            }
            else
            {
                if (Convert.ToDecimal(weight.ToString()) == 0)
                {
                    return string.Empty;
                }
                else
                {
                    return weight.ToString() + " " + ZNodeConfigManager.SiteConfig.WeightUnit;
                }
            }
        }

        /// <summary>
        /// Format the Price of a Product
        /// </summary>
        /// <param name="price">Price value to convert.</param>
        /// <returns>Returns the converted price value.</returns>
        protected string FormatPrice(object price)
        {
            if (price == null)
            {
                return string.Empty;
            }
            else
            {
                if (Convert.ToDecimal(price) == 0)
                {
                    return string.Empty;
                }
                else
                {
                    return string.Format("{0:c}", price);
                }
            }
        }

        /// <summary>
        /// Validate for Null Values and return a Boolean Value
        /// </summary>
        /// <param name="fieldvalue">fieldvalue ( 0 or 1) </param>
        /// <returns>Returns true if value is 1 else false.</returns>
        protected bool DisplayVisible(object fieldvalue)
        {
            if (fieldvalue == DBNull.Value)
            {
                return false;
            }
            else
            {
                if (Convert.ToInt32(fieldvalue) == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// Gets the name of the Addon for this AddonId
        /// </summary>
        /// <param name="addOnId">AddOnId to get the AddOn object.</param>
        /// <returns>Returns the Addon name.</returns>
        protected string GetAddOnName(object addOnId)
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            AddOn addOn = productAddOnAdmin.GetByAddOnId(int.Parse(addOnId.ToString()));
            if (addOn != null)
            {
                return addOn.Name;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the title of the Addon for this AddonId
        /// </summary>
        /// <param name="addOnId">AddOnId to get the addon object.</param>
        /// <returns>Returns the Addon title.</returns>
        protected string GetAddOnTitle(object addOnId)
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            AddOn addOn = productAddOnAdmin.GetByAddOnId(int.Parse(addOnId.ToString()));

            if (addOn != null)
            {
                return addOn.Title;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the Name of the Profile for this ProfileID
        /// </summary>
        /// <param name="profileId">Profile Id to get the profile object.</param>
        /// <returns>Returns the profile name.</returns>
        protected string GetProfileName(object profileId)
        {
            ProfileAdmin profileAdmin = new ProfileAdmin();
            if (profileId == null)
            {
                return this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAllProfiles").ToString();
            }
            else
            {
                Profile profile = profileAdmin.GetByProfileID(int.Parse(profileId.ToString()));
                if (profile != null)
                {
                    return profile.Name;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the name of the shipping rule type name for this shippingRuleTypeId
        /// </summary>
        /// <param name="shippingRuleTypeId">ShippingRuleTypeId to get the ShippingRuleType object</param>
        /// <returns>Returns the shipping rule type name.</returns>
        protected string GetShippingRuleTypeName(int shippingRuleTypeId)
        {
            ShippingRuleTypeAdmin shippingRuleTypeAdmin = new ShippingRuleTypeAdmin();
            ShippingRuleType shippingRuleType = shippingRuleTypeAdmin.GetByShippingRuleTypeID(shippingRuleTypeId);

            if (shippingRuleType != null)
            {
                return shippingRuleType.Name;
            }

            return string.Empty;
        }

        /// <summary>
        /// Return cross-mark image path
        /// </summary>
        /// <returns>Returns disabled icon file name with path.</returns>
        protected string SetCheckMarkImage()
        {
            return ZNode.Libraries.Admin.Helper.GetCheckMark(false);
        }

        /// <summary>
        /// Get whether the digital Id is assigned or not.
        /// </summary>
        /// <param name="orderLineItemId">Order line item Id</param>
        /// <returns>Returns true if digital Id assigned.</returns>
        protected bool IsDigitalAssetAssigned(object orderLineItemId)
        {
            if (orderLineItemId == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Get SKU quantity.
        /// </summary>
        /// <param name="skuId">Product SKU id to get quantity.</param>
        /// <returns>Returns SKU quantity.</returns>
        protected string GetSkuQuantity(int skuId)
        {
            ZNode.Libraries.DataAccess.Service.SKUService ss = new ZNode.Libraries.DataAccess.Service.SKUService();
            return SKUAdmin.GetQuantity(ss.GetBySKUID(skuId)).ToString();
        }

        /// <summary>
        /// Get the SKU reorder level.
        /// </summary>
        /// <param name="skuId">Product SKU Id</param>
        /// <returns>Returns the SKU reorder level.</returns>
        protected string GetSkuReorderLevel(int skuId)
        {
            ZNode.Libraries.DataAccess.Service.SKUService skuService = new ZNode.Libraries.DataAccess.Service.SKUService();
            return SKUAdmin.GetInventory(skuService.GetBySKUID(skuId)).ReOrderLevel.ToString();
        }

        /// <summary>
        /// Get the encrypted product Id.
        /// </summary>
        /// <param name="productId">Product Id to encrypt.</param>
        /// <returns>Returns the encrypted product Id</returns>
        protected string GetEncryptID(object productId)
        {
            return HttpUtility.UrlEncode(this.encrypt.EncryptData(productId.ToString()));
        }
        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get mode value from querystring        
            if (Request.Params["mode"] != null)
            {
                this.mode = HttpUtility.UrlDecode(this.encrypt.DecryptData(Request.Params["mode"]));
            }

            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
                ZNode.Libraries.Admin.ProductAdmin prodAdmin = new ProductAdmin();
                ZNode.Libraries.DataAccess.Entities.Product product = prodAdmin.GetByProductId(this.itemId);
                if ((product.PortalID == null || product.Franchisable == true) && product.PortalID != UserStoreAccess.GetTurnkeyStorePortalID)
                {
                    EditProduct.Visible = false;
                    Button2.Visible = false;
                    Button1.Visible = false;
                    btnAddNewAddOn.Visible = false;
                    AddTieredPricing.Visible = false;
                    btnAddNewHighlight.Visible = false;
                    btnAddDigitalAsset.Visible = false;
                    this.isAdminProduct = true;
                }
                else
                {
                    EditProduct.Visible = true;
                    Button2.Visible = true;
                    Button1.Visible = true;
                    btnAddNewAddOn.Visible = true;
                    AddTieredPricing.Visible = true;
                    btnAddNewHighlight.Visible = true;
                    btnAddDigitalAsset.Visible = true;
                    this.isAdminProduct = false;
                }
            }
            else
            {
                this.itemId = 0;
            }

            if (!Page.IsPostBack)
            {
                if (this.itemId > 0)
                {
                    this.BindViewData();
                    this.previewLink = ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/product.aspx?zpid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString()));
                }
                else
                {
                    throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductNotFound").ToString());
                }

                this.ResetTab();

                divReviewHistory.Visible = true;
            }

            // Add Client Side Script
            StringBuilder script = new StringBuilder();
            script.Append("<script language=JavaScript>");
            script.Append("    function  PreviewProduct() {");
            script.Append("  window.open('" + this.previewLink + "');");
            script.Append("    }");
            script.Append("<" + "/script>");

            if (!ClientScript.IsStartupScriptRegistered("Preview"))
            {
                ClientScript.RegisterStartupScript(GetType(), "Preview", script.ToString());
            }

            // Bind the Attribute List
            this.BindSkuAttributes();

            if (!Page.IsPostBack)
            {
                this.BindSKU();
            }
        }
        #endregion

        #region Grid Events

        #region Events for ProductAddon Grid

        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridProductAddOns_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Seventh column.
                LinkButton deleteButton = (LinkButton)e.Row.Cells[2].FindControl("btnDelete");

                // Set the Button's CommandArgument property with the row's index.
                deleteButton.CommandArgument = e.Row.RowIndex.ToString();

                // Add Client Side confirmation
                deleteButton.OnClientClick = this.GetGlobalResourceObject("ZnodeAdminResource", "ConfirmDelete").ToString();
                if (this.isAdminProduct == true)
                {
                    deleteButton.Visible = false;
                }
                else
                {
                    deleteButton.Visible = true;
                }
            }
        }

        /// <summary>
        /// Product Add On Row command event - occurs when delete button is fired.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridProductAddOns_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // cell in the GridView control.
                GridViewRow selectedRow = uxGridProductAddOns.Rows[index];

                TableCell Idcell = selectedRow.Cells[0];
                string Id = Idcell.Text;

                if (e.CommandName == "Remove")
                {
                    ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();
                    ProductAdmin prodAdmin = new ProductAdmin();
                    ZNode.Libraries.DataAccess.Entities.Product product = prodAdmin.GetByProductId(this.itemId);

                    ProductAddOnService productaddonService = new ProductAddOnService();
                    ProductAddOn addon = new ProductAddOn();
                    addon = productaddonService.GetByProductAddOnID(int.Parse(Id));
                    productaddonService.DeepLoad(addon);
                    string addonName = addon.AddOnIDSource.Name;

                    string associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddOn").ToString(), addonName, product.Name);
                    if (AdminAccess.DeleteProductAddOn(int.Parse(Id)))
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, product.Name);
                    }

                    this.BindProductAddons();
                }
            }
        }

        /// <summary>
        /// Product AddOn grid Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridProductAddOns_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGridProductAddOns.PageIndex = e.NewPageIndex;
            this.BindProductAddons();
        }

        #endregion

        #region Events for Bundle Product Grid
        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxBundleProductGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                ProductService _prodService = new ProductService();
                ProductAdmin _prodAdmin = new ProductAdmin();
                ZNode.Libraries.DataAccess.Entities.Product _prodParentEntity = _prodAdmin.GetByProductId(this.itemId);
                bool Check = false;

                if (e.CommandName == "RemoveItem")
                {
                    ParentChildProductAdmin _parentChildProdAdmin = new ParentChildProductAdmin();

                    Check = _parentChildProdAdmin.Delete(int.Parse(e.CommandArgument.ToString()));

                    if (Check)
                    {
                        this.BindBundleProducts();
                    }
                }
            }
        }

        /// <summary>
        /// Bundle Product Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxBundleProductGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxBundleProductGrid.PageIndex = e.NewPageIndex;
            this.BindBundleProducts();
        }
        #endregion


        #region InventoryDisplay Grid related Methods
        /// <summary>
        /// Event triggered when a command button is clicked on the grid (InventoryDisplay Grid)
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridInventoryDisplay_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandArgument.ToString() == "page")
            {
            }
            else
            {
                string id = e.CommandArgument.ToString();
                if (e.CommandName == "Edit")
                {
                    // Redirect Edit SKUAttrbute Page
                    Response.Redirect(this.addSKULink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())) + "&skuid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(id)) + "&typeid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.ProductTypeId.ToString())));
                }
                else if (e.CommandName == "Delete")
                {
                    // Delete SKU and SKU Attribute
                    SKUAdmin _AdminAccess = new SKUAdmin();
                    int skuId = int.Parse(e.CommandArgument.ToString());
                    TList<SKU> skuList = _AdminAccess.GetByProductID(this.itemId);

                    // If product has only one SKU then do not delete.
                    if (skuList.Count == 1)
                    {
                        lblSkuErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCouldnotdeletelastsku").ToString(); 
                        return;
                    }

                    bool isDeleted = _AdminAccess.Delete(skuId);
                    if (isDeleted)
                    {
                        _AdminAccess.DeleteBySKUId(skuId);
                    }
                }
            }
        }

        protected void UxGridInventoryDisplay_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton lnkRemove = (LinkButton)e.Row.FindControl("RemoveSKU");
                LinkButton lnkEdit = (LinkButton)e.Row.FindControl("EditSKU");
                if (this.isAdminProduct == true)
                {
                    lnkRemove.Visible = false;
                    lnkEdit.Visible = false;
                }
                else
                {
                    lnkRemove.Visible = true && this.hasAttributes;
                    lnkEdit.Visible = true;
                }
            }
        }

        /// <summary>
        /// Event triggered when the Grid Row is Deleted
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridInventoryDisplay_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Event triggered when the grid(Inventory) page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridInventoryDisplay_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGridInventoryDisplay.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        #endregion

        #region Related to Product Views Grid Events

        /// <summary>
        /// Related Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>        
        protected void GridThumb_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridThumb.PageIndex = e.NewPageIndex;
            this.BindImageDatas();
        }

        /// <summary>
        /// Occurs when thumbnail image grid row deleted.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GridThumb_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindImageDatas();
        }

        /// <summary>
        /// Alternate Images RowDataBound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GridThumb_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton editButton = (LinkButton)e.Row.Cells[7].FindControl("EditProductView");
                LinkButton deleteButton = (LinkButton)e.Row.Cells[7].FindControl("Delete");

                if (this.isAdminProduct == true)
                {
                    editButton.Visible = false;
                    deleteButton.Visible = false;
                }
                else
                {
                    editButton.Visible = true;
                    deleteButton.Visible = true;
                }
            }
        }

        /// <summary>
        /// Occurs when row command
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GridThumb_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                string id = e.CommandArgument.ToString();
                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.addViewlink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())) + "&productimageid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(id)) + "&typeid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.ProductTypeId.ToString())));
                }

                if (e.CommandName == "RemoveItem")
                {
                    ZNode.Libraries.Admin.ProductViewAdmin prodadmin = new ProductViewAdmin();
                    bool isDeleted = prodadmin.Delete(int.Parse(e.CommandArgument.ToString()));
                    if (isDeleted)
                    {
                        this.BindImageDatas();
                    }
                }
            }
        }
        #endregion

        #region Related to Product - Tiered Pricing
        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridTieredPricing_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Seventh column.
                LinkButton deleteButton = (LinkButton)e.Row.Cells[5].FindControl("btnRemoveTieredPricing");

                // Add Client Side confirmation
                deleteButton.OnClientClick = this.GetGlobalResourceObject("ZnodeAdminResource", "ConfirmDelete").ToString();
                LinkButton editButton = (LinkButton)e.Row.Cells[5].FindControl("EditTieredPricing");

                if (this.isAdminProduct == true)
                {
                    editButton.Visible = false;
                    deleteButton.Visible = false;
                }
                else
                {
                    editButton.Visible = true;
                    deleteButton.Visible = true;
                }
            }
        }

        /// <summary>
        /// Related Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridTieredPricing_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridThumb.PageIndex = e.NewPageIndex;
            this.BindTieredPricing();
        }

        /// <summary>
        /// Tiered pricing grid row deleted.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridTieredPricing_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        }

        /// <summary>
        /// Tiered pricing row command clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridTieredPricing_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                string id = e.CommandArgument.ToString();

                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.addTieredPricingPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())) + "&tierid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(id.ToString())));
                }

                if (e.CommandName == "Delete")
                {
                    ZNode.Libraries.Admin.ProductAdmin productAdmin = new ProductAdmin();
                    bool isDeleted = productAdmin.DeleteProductTierById(int.Parse(e.CommandArgument.ToString()));
                    if (isDeleted)
                    {
                        ZNode.Libraries.DataAccess.Entities.Product product = productAdmin.GetByProductId(this.itemId);
                        string associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteTieredPricing").ToString() + product.Name;
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, product.Name);

                        this.BindTieredPricing();
                    }
                }
            }
        }
        #endregion

        #region Related to product Highlights
        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridHighlights_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Seventh column.
                LinkButton deleteButton = (LinkButton)e.Row.Cells[1].FindControl("DeleteHighlight");

                // Add Client Side confirmation
                deleteButton.OnClientClick = this.GetGlobalResourceObject("ZnodeAdminResource", "ConfirmDelete").ToString();
                if (this.isAdminProduct == true)
                {
                    deleteButton.Visible = false;
                }
                else
                {
                    deleteButton.Visible = true;
                }
            }
        }

        /// <summary>
        /// product highlight Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridHighlights_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGridHighlights.PageIndex = e.NewPageIndex;
            this.BindProductHighlights();
        }

        protected void UxGridHighlights_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        }

        /// <summary>
        /// Product highlights row command event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridHighlights_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Delete")
                {
                    ZNode.Libraries.Admin.ProductAdmin productAdmin = new ProductAdmin();

                    ProductHighlightService productHighlightService = new ProductHighlightService();
                    ProductHighlight productHighlight = new ProductHighlight();
                    productHighlight = productHighlightService.GetByProductHighlightID(int.Parse(e.CommandArgument.ToString()));
                    productHighlightService.DeepLoad(productHighlight);
                    string highlightname = productHighlight.HighlightIDSource.Name;

                    bool isDeleted = productAdmin.DeleteProductHighlight(int.Parse(e.CommandArgument.ToString()));

                    ZNode.Libraries.DataAccess.Entities.Product product = productAdmin.GetByProductId(this.itemId);
                    string associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteHighlights").ToString() + highlightname + " - " + product.Name;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, product.Name);

                    this.BindProductHighlights();
                }
                else if (e.CommandName == "Edit")
                {
                    string id = e.CommandArgument.ToString();
                    Response.Redirect(this.editHighlightPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(id)) + "&productid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
                }
            }
        }
        #endregion

        #region Related to Digital Asset
        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridDigitalAsset_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DigitalAsset da = (DigitalAsset)e.Row.DataItem;

                int? orderLineItemId = da.OrderLineItemID;
                ////Retrieve the Button control from the Seventh column.
                LinkButton DeleteButton = (LinkButton)e.Row.Cells[3].FindControl("btnRemoveDigitalAsset");

                DeleteButton.OnClientClick = this.GetGlobalResourceObject("ZnodeAdminResource", "ConfirmDelete").ToString();

                if (this.isAdminProduct == true || IsDigitalAssetAssigned(orderLineItemId))
                {
                    DeleteButton.Visible = false;
                }
                else
                {
                    DeleteButton.Visible = true;
                }
            }
        }

        /// <summary>
        /// Digital asset grid Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridDigitalAsset_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGridDigitalAsset.PageIndex = e.NewPageIndex;
            this.BindDigitalAssets();
        }

        /// <summary>
        /// Digital asset row deleted.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridDigitalAsset_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
        }

        /// <summary>
        /// Digital Asset row command clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridDigitalAsset_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Delete")
                {
                    ZNode.Libraries.Admin.ProductAdmin productAdmin = new ProductAdmin();

                    DigitalAssetService digitalService = new DigitalAssetService();
                    DigitalAsset digitalAsset = new DigitalAsset();
                    digitalAsset = digitalService.GetByDigitalAssetID(int.Parse(e.CommandArgument.ToString()));
                    digitalService.DeepLoad(digitalAsset);
                    string digitalassetname = digitalAsset.DigitalAsset.ToString();

                    bool isDeleted = productAdmin.DeleteDigitalAsset(int.Parse(e.CommandArgument.ToString()));

                    ZNode.Libraries.DataAccess.Entities.Product product = productAdmin.GetByProductId(this.itemId);
                    string associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteDigitalAsset").ToString() + digitalassetname + " - " + product.Name;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, product.Name);

                    if (isDeleted)
                    {
                        this.BindDigitalAssets();
                    }
                }
            }
        }
        #endregion

        #region Events for Departments Tab

        protected void GvProductDepartment_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvProductDepartment.PageIndex = e.NewPageIndex;
            this.BindProductDepartment();
        }

        protected void GvProductDepartment_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "RemoveItem")
                {
                    ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
                    bool isDeleted = productCategoryAdmin.Delete(int.Parse(e.CommandArgument.ToString()));
                    if (isDeleted)
                    {
                        this.BindProductDepartment();
                    }
                }
            }
        }

        protected void BtnAssociateDepartment_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.associateCategoryPageLink + Server.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }
        #endregion

        #region Events for ReviewHistory
        /// <summary>
        /// Grid View History Row command event.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GvReviewHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "View")
            {
                Response.Redirect(this.viewProductReviewPageLink + this.GetEncryptId(this.itemId) + "&historyid=" + this.GetEncryptId(e.CommandArgument.ToString()));
            }
        }

        /// <summary>
        /// Grid View History Page Index changing event.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GvReviewHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvReviewHistory.PageIndex = e.NewPageIndex;
            this.BindReviewHistory();
        }
        #endregion

        #endregion

        #region Events

        #region Events for ProductAddon

        /// <summary>
        /// Add AddOn Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddNewAddOn_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addProductAddOnLink + "&itemid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        #endregion

        /// <summary>
        /// Add New Highlight button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddHighlight_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addHighlightPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Add New bundle button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddBundleProducts_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddBundleProductsLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }


        /// <summary>
        /// Add Digital asset button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddDigitalAsset_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addDigitalAssetPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Add Tiered pricing button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddTieredPricing_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addTieredPricingPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Add New product sku button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddSKU_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addSKULink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())) + "&typeid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.ProductTypeId.ToString())));
        }

        /// <summary>
        /// Redirecting to Product Edit Page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditProduct_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.editPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Redirecting to product advance setting page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditAdvancedSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.editAdvancedPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Redirecting to product SEO setting page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditSEOSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.editSEOLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Redirecting to Product List Page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ProductList_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listLink);
        }

        /// <summary>
        /// Redirecting to product views page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddProductView_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addViewlink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Back Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listLink);
        }

        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Clear button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            DataSet ds = productAdmin.GetProductDetails(this.itemId);

            // Check for Number of Rows
            if (ds.Tables[0].Rows.Count != 0)
            {
                // Check For Product Type
                this.ProductTypeId = int.Parse(ds.Tables[0].Rows[0]["ProductTypeId"].ToString());
            }

            // For Attribute value.
            string attributes = String.Empty;

            // GetAttribute for this ProductType
            DataSet attributeTypeDataSet = productAdmin.GetAttributeTypeByProductTypeID(this.ProductTypeId);

            foreach (DataRow MyDataRow in attributeTypeDataSet.Tables[0].Rows)
            {
                System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());
                lstControl.SelectedIndex = 0;
            }

            this.BindSKU();
        }

        #endregion

        #region Helper Functions
        /// <summary>
        /// This will automatically pre-select the tab according the query string value(mode)
        /// </summary>
        private void ResetTab()
        {
            if (this.mode.Equals("advanced"))
            {
                // Set Advanced Settings as active tab 
                tabProductDetails.ActiveTab = pnlAdvancedSettings;
            }
            else if (this.mode.Equals("departments"))
            {
                // Activate the Departments tab.
                tabProductDetails.ActiveTab = pnlDepartments;
            }
            else if (this.mode.Equals("inventory"))
            {
                // For Manage Inventory
                tabProductDetails.ActiveTab = pnlManageinventory;
            }
            else if (this.mode.Equals("bundle"))
            {
                // For Bundle Products
                tabProductDetails.ActiveTab = pnlBundleProduct;
            }
            else if (this.mode.Equals("tags"))
            {
                // For Tags Products
                tabProductDetails.ActiveTab = pnlTag;
            }
            else if (this.mode.Equals("views"))
            {
                // For Related Images
                tabProductDetails.ActiveTab = pnlAlternateImages;
            }
            else if (this.mode.Equals("addons"))
            {
                // For Product Options
                tabProductDetails.ActiveTab = pnlProductOptions;
            }
            else if (this.mode.Equals("tieredPricing"))
            {
                // For Product Tiered pricing tab
                tabProductDetails.ActiveTab = pnlTieredPricing;
            }
            else if (this.mode.Equals("highlight"))
            {
                // For Product Highlights tab
                tabProductDetails.ActiveTab = pnlHighlights;
            }
            else if (this.mode.Equals("digitalAsset"))
            {
                // For product digital assets tab
                tabProductDetails.ActiveTab = pnlDigitalAsset;
            }
            else
            {
                // Product Info
                tabProductDetails.ActiveTabIndex = 0;
            }
        }

        /// <summary>
        /// Get the details product is active 
        /// </summary>
        /// <param name="productId">Product Id</param>
        /// <returns>Returns Boolean value</returns>
        protected bool GetProductIsActive(object productId)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            return productAdmin.GetByProductId(Convert.ToInt32(productId)).ActiveInd;
        }

        /// <summary>
        /// Format the reason. If empty then return N/A
        /// </summary>
        /// <param name="reason">Reason object string</param>
        /// <returns>Returns the formatted reason.</returns>
        protected string FormatReason(object reason)
        {
            if (reason != null)
            {
                return reason.ToString() == string.Empty ? "N/A" : reason.ToString();
            }
            else
            {
                return "N/A";
            }
        }
        #endregion

        #region Bind Methods

        /// <summary>
        /// Bind Attributes List.
        /// </summary>
        private void BindSkuAttributes()
        {
            ProductAdmin adminAccess = new ProductAdmin();
            DataSet attributeTypeDataSet = adminAccess.GetAttributeTypeByProductTypeID(this.ProductTypeId);

            // Repeats until Number of AttributeType for this Product
            foreach (DataRow dr in attributeTypeDataSet.Tables[0].Rows)
            {
                // Bind Attributes
                DataSet attributeDataSet = adminAccess.GetAttributesByAttributeTypeIdandProductID(int.Parse(dr["attributetypeid"].ToString()), this.itemId);

                System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                lstControl.ID = "lstAttribute" + dr["AttributeTypeId"].ToString();

                ListItem li = new ListItem(dr["Name"].ToString(), "0");
                //li.Selected = true;

                lstControl.DataSource = attributeDataSet;
                lstControl.DataTextField = "Name";
                lstControl.DataValueField = "AttributeId";
                lstControl.DataBind();
                lstControl.Items.Insert(0, li);
                lstControl.SelectedValue = attributeDataSet.Tables[0].Rows[0]["AttributeId"].ToString();

                // Add Dynamic Attribute DropDownlist in the Placeholder
                ControlPlaceHolder.Controls.Add(lstControl);

                Literal lit1 = new Literal();
                lit1.Text = "&nbsp;&nbsp;";
                ControlPlaceHolder.Controls.Add(lit1);
            }

            this.hasAttributes = attributeTypeDataSet.Tables[0].Rows.Count > 0;
            pnlSKUAttributes.Visible = attributeTypeDataSet.Tables[0].Rows.Count > 0;
        }

        /// <summary>
        /// Bind Inventory Grid
        /// </summary>
        private void BindSKU()
        {
            SKUAdmin skuAdmin = new SKUAdmin();
            TList<SKU> skuList = skuAdmin.GetByProductID(this.itemId);

            skuList.Sort("SKU");

            uxGridInventoryDisplay.DataSource = skuList;
            uxGridInventoryDisplay.DataBind();
        }

        /// <summary>
        /// Binds the Search data
        /// </summary>
        private void BindSearchData()
        {
            ProductAdmin adminAccess = new ProductAdmin();
            DataSet ds = adminAccess.GetProductDetails(this.itemId);

            // Check for Number of Rows
            if (ds.Tables[0].Rows.Count != 0)
            {
                // Check For Product Type
                this.ProductTypeId = int.Parse(ds.Tables[0].Rows[0]["ProductTypeId"].ToString());
            }

            // For Attribute value.
            string attributes = String.Empty;

            // GetAttribute for this ProductType
            DataSet attributeTypeDataSet = adminAccess.GetAttributeTypeByProductTypeID(this.ProductTypeId);

            foreach (DataRow MyDataRow in attributeTypeDataSet.Tables[0].Rows)
            {
                System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                if (lstControl != null)
                {
                    int selValue = int.Parse(lstControl.SelectedValue);

                    if (selValue > 0)
                    {
                        attributes += selValue.ToString() + ",";
                    }
                }
            }

            // If attributes length is more than zero.
            if (attributes.Length > 0)
            {
                // Split the string
                string attribute = attributes.Substring(0, attributes.Length - 1);

                if (attribute.Length > 0)
                {
                    SKUAdmin skuAdmin = new SKUAdmin();
                    DataSet attributeDataSet = skuAdmin.GetBySKUAttributes(this.itemId, attribute);

                    DataView sku = new DataView(attributeDataSet.Tables[0]);
                    sku.Sort = "SKU";
                    uxGridInventoryDisplay.DataSource = sku;
                    uxGridInventoryDisplay.DataBind();
                }
            }
            else
            {
                this.BindSKU();
            }
        }

        /// <summary>
        /// Binding Product tiered pricing for this product
        /// </summary>
        private void BindTieredPricing()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ProductAdmin();
            TList<ProductTier> productTierList = ProdAdmin.GetTieredPricingByProductId(this.itemId);

            uxGridTieredPricing.DataSource = productTierList;
            uxGridTieredPricing.DataBind();
        }

        /// <summary>
        /// Binding Product Values into label Boxes
        /// </summary>
        private void BindViewData()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin prodAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = prodAdmin.GetByProductId(this.itemId);
            UserStoreAccess.CheckStoreAccess(product.PortalID.GetValueOrDefault(0), true, product.Franchisable);
            DataSet ds = prodAdmin.GetProductDetails(this.itemId);
            int count = 0;

            // Check for Number of Rows
            if (ds.Tables[0].Rows.Count != 0)
            {
                // Checking the user profile for roles and permission and then loading the data based on roles
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);

                // Check For Product Type
                this.ProductTypeId = int.Parse(ds.Tables[0].Rows[0]["ProductTypeId"].ToString());

                lblProdType.Text = ds.Tables[0].Rows[0]["producttype name"].ToString();

                count = prodAdmin.GetAttributeCount(int.Parse(ds.Tables[0].Rows[0]["ProductTypeId"].ToString()));
				var bundleProductList = DataRepository.ParentChildProductProvider.GetByChildProductID(this.itemId);

				// If the attribute count is not zero, then its not default product type 
				// So we will hide the bundle tab, if the product doesn't have default product type
				if (count != 0 || bundleProductList.Count > 0)
				{
					pnlBundleProduct.Enabled = false;
				}
            }

            if (product != null)
            {
                //TODO: General Information
                lblProdName.Text = product.Name;
                lblProdNum.Text = product.ProductNum.ToString();

                //Set Product BrandName
                if (product.ManufacturerID.HasValue && product.ManufacturerID.Value > 0)
                {
                    var manufacturer = new ManufacturerAdmin().GetByManufactureId(Convert.ToInt32(product.ManufacturerID));
                    lblBrandName.Text = manufacturer.Name;
                }
                else
                {
                    lblBrandName.Text = String.Empty;
                }

                if (product.MinQty.HasValue)
                {
                    lblMinQuantity.Text = product.MinQty.Value.ToString();
                }

                if (product.MaxQty.HasValue)
                {
                    lblMaxQuantity.Text = product.MaxQty.Value.ToString();
                }

                ZNodeImage znodeImage = new ZNodeImage();
                ItemImage.ImageUrl = znodeImage.GetImageHttpPathMedium(product.ImageFile);

                // Product Description and Features
                lblShortDescription.Text = Server.HtmlDecode(product.ShortDescription);
                lblProductDescription.Text = Server.HtmlDecode(product.Description);
                lblProductFeatures.Text = Server.HtmlDecode(product.FeaturesDesc);
                lblproductspecification.Text = Server.HtmlDecode(product.Specifications);
                lbladditionalinfo.Text = Server.HtmlDecode(product.AdditionalInformation);
                lblDownloadLink.Text = product.DownloadLink;

                // Product Attributes
                if (product.RetailPrice.HasValue)
                {
                    lblRetailPrice.Text = product.RetailPrice.Value.ToString("c");
                }

                if (product.WholesalePrice.HasValue)
                {
                    lblWholeSalePrice.Text = product.WholesalePrice.Value.ToString("c");
                }

                lblSalePrice.Text = this.FormatPrice(product.SalePrice);
                lblWeight.Text = this.FormatProductWeight(product.Weight);
                if (product.Height.HasValue)
                {
                    lblHeight.Text = product.Height.Value.ToString("N2") + " " + ZNodeConfigManager.SiteConfig.DimensionUnit;
                }

                if (product.Width.HasValue)
                {
                    lblWidth.Text = product.Width.Value.ToString("N2") + " " + ZNodeConfigManager.SiteConfig.DimensionUnit;
                }

                if (product.Length.HasValue)
                {
                    lblLength.Text = product.Length.Value.ToString("N2") + " " + ZNodeConfigManager.SiteConfig.DimensionUnit;
                }

                // Display Settings
                chkProductEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(product.ActiveInd.ToString()));
                lblProdDisplayOrder.Text = product.DisplayOrder.ToString();
                chkIsSpecialProduct.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.HomepageSpecial));
                chkIsNewItem.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.NewProductInd));
                chkProductPricing.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.CallForPricing));
                chkproductInventory.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.InventoryDisplay));
                chkFranchisable.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.Franchisable));

                // Display Tax Class Name
                if (product.TaxClassID.HasValue)
                {
                    TaxRuleAdmin taxRuleAdmin = new TaxRuleAdmin();
                    TaxClass taxClass = taxRuleAdmin.GetByTaxClassID(product.TaxClassID.Value);

                    if (taxClass != null)
                    {
                        lblTaxClass.Text = taxClass.Name;
                    }
                }

                // Recurring Billing
                imgRecurringBillingInd.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.RecurringBillingInd));
                lblBillingPeriod.Text = product.RecurringBillingPeriod;
                if (product.RecurringBillingInitialAmount.HasValue)
                    lblRecurringBillingInitialAmount.Text = product.RecurringBillingInitialAmount.Value.ToString("c");


                shipSeparatelyInd.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(product.ShipSeparately);

                // SEO settings
                lblSEODescription.Text = product.SEODescription;
                lblSEOKeywords.Text = product.SEOKeywords;
                lblSEOTitle.Text = product.SEOTitle;
                lblSEOURL.Text = product.SEOURL;

                // Inventory Setting - Out of Stock Options
                if (product.TrackInventoryInd.HasValue && product.AllowBackOrder.HasValue)
                {
                    if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value == false)
                    {
                        chkCartInventoryEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                        chkIsBackOrderEnabled.Src = this.SetCheckMarkImage();
                        chkIstrackInvEnabled.Src = this.SetCheckMarkImage();
                    }
                    else if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value)
                    {
                        chkCartInventoryEnabled.Src = this.SetCheckMarkImage();
                        chkIsBackOrderEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                        chkIstrackInvEnabled.Src = this.SetCheckMarkImage();
                    }
                    else if ((product.TrackInventoryInd.Value == false) && (product.AllowBackOrder.Value == false))
                    {
                        chkCartInventoryEnabled.Src = this.SetCheckMarkImage();
                        chkIsBackOrderEnabled.Src = this.SetCheckMarkImage();
                        chkIstrackInvEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                    }
                }
                else
                {
                    chkCartInventoryEnabled.Src = this.SetCheckMarkImage();
                    chkIsBackOrderEnabled.Src = this.SetCheckMarkImage();
                    chkIstrackInvEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                }

                // Inventory Setting - Stock Messages
                lblInStockMsg.Text = product.InStockMsg;
                lblOutofStock.Text = product.OutOfStockMsg;
                lblBackOrderMsg.Text = product.BackOrderMsg;

                if (product.DropShipInd.HasValue)
                {
                    IsDropShipEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(product.DropShipInd.Value);
                }
                else
                {
                    IsDropShipEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(false);
                }

                if (product.FeaturedInd)
                {
                    FeaturedProduct.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                }
                else
                {
                    FeaturedProduct.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(false);
                }

                // Bind ShippingRule type
                if (product.ShippingRuleTypeID.HasValue)
                {
                    lblShippingRuleTypeName.Text = this.GetShippingRuleTypeName(product.ShippingRuleTypeID.Value);

                    if (product.ShippingRate.HasValue && product.ShippingRuleTypeID.Value == (int)Znode.Engine.Shipping.ZnodeShippingRuleType.FixedRatePerItem)
                    {
                        lblShippingRate.Text = product.ShippingRate.Value.ToString("c");
                    }
                }

                if (product.FreeShippingInd.HasValue)
                {
                    freeShippingInd.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(product.FreeShippingInd.Value);
                }
                else
                {
                    freeShippingInd.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(false);
                }

                // Bind Grid - Product Related Items To Be removed
                //this.BindRelatedItems();

                this.BindImageDatas();

                // Bind Grid - Bundle Products
                this.BindBundleProducts();

                // Bind Grid - Product Addons
                this.BindProductAddons();

                // Tiered Pricing
                this.BindTieredPricing();

                // Bind product highlights
                this.BindProductHighlights();

                // Digital Asset
                this.BindDigitalAssets();

                this.BindProductDepartment();

                // Bind the review history.
                this.BindReviewHistory();

            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductNotFound").ToString());
            }
        }

        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindProductHighlights()
        {
            ProductViewAdmin productViewAdmin = new ProductViewAdmin();
            uxGridHighlights.DataSource = productViewAdmin.GetAllHighlightImage(this.itemId);
            uxGridHighlights.DataBind();
        }

        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindProductAddons()
        {
            ProductAddOnAdmin ProdAddonAdminAccess = new ProductAddOnAdmin();

            // Bind Associated Addons for this product
            uxGridProductAddOns.DataSource = ProdAddonAdminAccess.GetByProductId(this.itemId);
            uxGridProductAddOns.DataBind();
        }

        /// <summary>
        /// Bind Bundle Products
        /// </summary>
        private void BindBundleProducts()
        {
            ParentChildProductAdmin parentChildProduct = new ParentChildProductAdmin();
            TList<ParentChildProduct> parentChildProductList = parentChildProduct.GetParentChildProductByProductId(this.itemId);
            uxBundleProductGrid.DataSource = parentChildProductList;
            uxBundleProductGrid.DataBind();
        }

        /// <summary>
        /// Bind the product image.
        /// </summary>
        private void BindImage()
        {
            ZNode.Libraries.Admin.StoreSettingsAdmin imageadmin = new StoreSettingsAdmin();
            GridThumb.DataSource = imageadmin.Getall();
            GridThumb.DataBind();
        }

        /// <summary>
        /// Bind Related ImageViews 
        /// </summary>
        private void BindImageDatas()
        {
            ProductViewAdmin imageadmin = new ProductViewAdmin();
            GridThumb.DataSource = imageadmin.GetAllAlternateImage(this.itemId);
            GridThumb.DataBind();
        }

        /// <summary>
        /// Bind Digital assets for this product
        /// </summary>
        private void BindDigitalAssets()
        {
            ZNode.Libraries.Admin.ProductAdmin productAdmin = new ProductAdmin();
            uxGridDigitalAsset.DataSource = productAdmin.GetDigitAssetByProductId(this.itemId);
            uxGridDigitalAsset.DataBind();
        }

        /// <summary>
        /// Bind the product department. 
        /// </summary>
        private void BindProductDepartment()
        {
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            DataSet ds = productCategoryAdmin.GetByProductID(this.itemId, UserStoreAccess.GetTurnkeyStorePortalID);
            gvProductDepartment.DataSource = ds.Tables[0];
            gvProductDepartment.DataBind();
        }

        /// <summary>
        /// To get the encrypted productId in the grid
        /// </summary>
        /// <param name="productId">Product Id to encrypt.</param>
        /// <returns>Returns the formatted product Id</returns>
        private string GetEncryptId(object productId)
        {
            return HttpUtility.UrlEncode(this.encrypt.EncryptData(productId.ToString()));
        }

        /// <summary>
        /// Bind the review history
        /// </summary>
        private void BindReviewHistory()
        {
            // Add entry to Product Review history table.
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            TList<ProductReviewHistory> productReviewHistoryList = productReviewHistoryAdmin.GetByProductID(this.itemId);
            productReviewHistoryList.Sort("ProductReviewHistoryID DESC");

            gvReviewHistory.DataSource = productReviewHistoryList;
            gvReviewHistory.DataBind();
        }

        #endregion
    }
}