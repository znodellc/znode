<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master"
    AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.FranchiseAdmin.Secure.Inventory.Products.AddView"
    CodeBehind="AddView.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/FranchiseAdmin/Controls/Default/ImageUploader.ascx" TagName="UploadImage"
    TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblHeading" runat="server" />
            </h1>
        </div>
        <div style="text-align: right;display:none;"> 
              <zn:Button runat="server" ID="BtnSubmitTop"  ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" />
              <zn:Button runat="server"  ID="BtnCancelTop" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
        </div>
        <div class="ClearBoth">
           
        </div>
        <div>
            <ZNode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <asp:Label ID="lblError" runat="server"></asp:Label>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="FormView">
            <div class="FieldStyle">
              <asp:Localize ID="ColumnTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitle %>'></asp:Localize><br />
                <small> <asp:Localize ID="HintSubTextTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintSubTextTitle %>'></asp:Localize></small></div>
            <div class="ValueStyle">
                <asp:TextBox ID="txttitle" runat="server" Columns="30" MaxLength="30"></asp:TextBox></div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnTitleImageType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleImageType %>'></asp:Localize><br />
                </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ImageType" runat="server">
                </asp:DropDownList>
            </div>
            <div class="ClearBoth" style="width: 100%">
                <span style="font-size: 10.5pt; font-weight: bold; color: #404040; font-family: Calibri, arial;">
                    <asp:Label ID="lblImage" runat="server"> <asp:Localize ID="ColumnProductImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductImage %>'></asp:Localize></asp:Label></span><span class="Asterix">*</span><br />
                <div id="ProductHint" runat="server" visible="false" style="width: 100%">
                    <small>  <asp:Localize ID="HintSubtextUploadSuitableImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintSubtextUploadSuitableImage %>'></asp:Localize></small></div>
                <div id="SwatchHint" runat="server" visible="false" style="width: 100%">
                    <small><asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintSubtextUploadSuitableSwatchImage %>'></asp:Localize></small></div>
            </div>
            <div class="ClearBoth">
            </div>
            <div id="tblShowImage" runat="server" visible="false" style="margin: 10px;">
                <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;">
                    <asp:Image ID="ProductAlternateImage" runat="server" />
                </div>
                <div class="LeftFloat" style="padding-left: 10px; margin-left: -1px; border-left: solid 1px #cccccc;">
                    <asp:Panel runat="server" ID="pnlShowOption">
                        <div>
                             <asp:Localize ID="TextOption" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextOption %>'></asp:Localize></div>
                        <div>
                            <asp:RadioButton ID="RadioProductCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonCurrentImage %>' runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductCurrentImage_CheckedChanged"
                                Checked="True" /><br />
                            <asp:RadioButton ID="RadioProductNewImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNewImage %>' runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductNewImage_CheckedChanged" />
                        </div>
                    </asp:Panel>
                    <br />
                    <div id="tblProductDescription" runat="server" visible="false">
                        <div class="FieldStyle">
                             <asp:Localize ID="ColumnSelectanImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectanImage %>'></asp:Localize></div>
                        <div class="ValueStyle">
                            <ZNode:UploadImage ID="UploadProductImage" runat="server"></ZNode:UploadImage>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="ColumnSKUImageALTText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSKUImageALTText %>'></asp:Localize><br />
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtImageAltTag" runat="server"></asp:TextBox></div>
                    <div>
                        <asp:Label ID="lblProductImageError" runat="server" CssClass="Error" ForeColor="Red"
                            Text="" Visible="False"></asp:Label>
                    </div>
                    <div id="AlternateThumbnail" runat="server" visible="false">
                        <asp:TextBox ID="txtAlternateThumbnail" Visible="false" runat="server" Columns="30"
                            MaxLength="30"></asp:TextBox>
                    </div>
                    <asp:Panel ID="ImagefileName" runat="server" Visible="false">
                        <div class="FieldStyle">
                            <asp:Label ID="lblImageName" runat="server"></asp:Label></div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtimagename" runat="server"></asp:TextBox></div>
                    </asp:Panel>
                </div>
            </div>
            <div class="ClearBoth">
                <br />
            </div>         
            <div class="ClearBoth">
                <br />
            </div>
            <div class="FieldStyle">
              <asp:Localize ID="ColumnTitleProductPage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductPage %>'></asp:Localize></div>
            <div class="ValueStyle">
                <asp:CheckBox ID='VisibleInd' runat='server' Checked="true" Text='<%$ Resources:ZnodeAdminResource, CheckBoxThumbNailProductpage %>'>
                </asp:CheckBox></div>
            <div class="FieldStyle"> 
                <asp:Localize ID="ColumnTitleCategoryPage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCategoryPage %>'></asp:Localize> 
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID='VisibleCategoryInd' runat='server' Text='<%$ Resources:ZnodeAdminResource, CheckBoxThumbNailCategeryPage %>' /></div>
             <div class="FieldStyle">
                    <asp:Localize ID="ColumnTitleDisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>'></asp:Localize><span class="Asterix">*</span><br />
                    <small>
                        <asp:Localize ID="HindSubTextDisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, HindSubTextDisplayOrder %>'></asp:Localize></small>
                </div>
            <div class="ValueStyle">
                <asp:TextBox ID="DisplayOrder" runat="server" MaxLength="9">500</asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="DisplayOrder"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductDisplayOrder %>'></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="DisplayOrder"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>'  ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
            </div>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
               <zn:Button runat="server" ID="btnSubmitBottom" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" />
                <zn:Button runat="server" ID="btnCancelBottom" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />

        </div>
    </div>
</asp:Content>
