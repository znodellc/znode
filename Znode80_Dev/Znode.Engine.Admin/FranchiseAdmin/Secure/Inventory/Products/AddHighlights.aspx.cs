using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.FranchiseAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_add_Highlights class.
    /// </summary>
    public partial class AddHighlights : System.Web.UI.Page
    {
        #region Private Variables
        private string viewPageLink;
        private int itemId = 0;
        private string portalIds = string.Empty;
        private string associateName = string.Empty;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get Portals
            this.viewPageLink = "~/FranchiseAdmin/Secure/Inventory/Products/view.aspx?mode=" + HttpUtility.UrlEncode(this.encrypt.EncryptData("highlight"));

            this.portalIds = UserStoreAccess.GetAvailablePortals;

            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
                if (!UserStoreAccess.CheckUserRoleInProduct(ProfileCommon.Create(HttpContext.Current.User.Identity.Name, true), this.itemId))
                {
                    Response.Redirect("default.aspx");
                }
            }
            else
            {
                this.itemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.Bind();
                this.BindProductName();
                this.BindHighLightType();
            }
        }
        #endregion

        #region Events

        /// <summary>
        /// Occurs when Add Selected Addon button clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddSelectedAddons_Click(object sender, EventArgs e)
        {
            ProductAdmin AdminAccess = new ProductAdmin();
            StringBuilder sb = new StringBuilder();
            StringBuilder highlightName = new StringBuilder();

            // Loop through the grid values
            foreach (GridViewRow row in uxGrid.Rows)
            {
                CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProductHighlight") as CheckBox;

                // Get AddOnId
                int HighlighID = int.Parse(row.Cells[1].Text);
                int DisplayOrder = int.Parse(row.Cells[4].Text);
                string name = row.Cells[2].Text;

                if (check.Checked)
                {
                    ProductHighlight entity = new ProductHighlight();

                    // Set Properties
                    entity.ProductID = this.itemId;
                    entity.HighlightID = HighlighID;
                    entity.DisplayOrder = DisplayOrder;
                    highlightName.Append(name + ",");

                    if (!AdminAccess.IsHighlightExists(this.itemId, HighlighID))
                    {
                        AdminAccess.AddProductHighlight(entity);
                        check.Checked = false;
                    }
                    else
                    {
                        sb.Append(name + ",");
                        lblErrorMessage.Visible = true;
                    }
                }
            }

            if (sb.ToString().Length > 0)
            {
                sb.Remove(sb.ToString().Length - 1, 1);

                // Display Error message
                lblErrorMessage.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductHighlightExist").ToString(), sb.ToString());
            }
            else
            {
                ProductAdmin prodAdmin = new ProductAdmin();
                ZNode.Libraries.DataAccess.Entities.Product entity = prodAdmin.GetByProductId(this.itemId);
                if (highlightName.Length > 0)
                {
                    entity.ReviewStateID = prodAdmin.UpdateReviewStateID(UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId, "Highlights Changed");
                    prodAdmin.Update(entity);

                    highlightName.Remove(highlightName.Length - 1, 1);

                    this.associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAssociateProductHighlight").ToString(), highlightName, entity.Name);

                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, entity.Name);
                    Response.Redirect(this.viewPageLink + "&itemid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
                }
                else
                {
                    lblErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductHighlistSelect").ToString();
                    lblErrorMessage.Visible = true;
                }
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.viewPageLink + "&itemid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(this.BindSearchData(), true);
            uxGrid.DataBind();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            ddlHighlightType.ClearSelection();
            txtName.Text = string.Empty;
            this.BindHighLightType();
            this.Bind();
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Occurs when grid page index selected.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.Bind();
        }

        /// <summary>
        /// Get the highlight name.
        /// </summary>
        /// <param name="highlight">Highlight object</param>
        /// <returns>Returns the highlight object name.</returns>
        protected string GetHighlightTypeName(object highlight)
        {
            if (highlight is Highlight)
            {
                return (highlight as Highlight).HighlightTypeIDSource.Name;
            }

            return null;
        }
        #endregion

        #region Bind Methods

        /// <summary>
        /// Bind highlight Grid - all Addons
        /// </summary>
        private void Bind()
        {
            // List of product Highlights
            TList<Highlight> highlightList = this.BindSearchData();
            if (highlightList.Count > 0)
            {
                uxGrid.DataSource = highlightList;
                uxGrid.DataBind();
            }
            else
            {
                btnAddSelectedAddons.Enabled = false;
                btnBottomCancel.Enabled = false;
            }
        }

        /// <summary>
        /// Bind product name.
        /// </summary>
        private void BindProductName()
        {
            ProductAdmin ProductAdminAccess = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product entity = ProductAdminAccess.GetByProductId(this.itemId);
            if (entity != null)
            {
                lblTitle.Text = lblTitle.Text + ": " + entity.Name;
            }
        }

        /// <summary>
        /// Binds Highlight Type drop-down list
        /// </summary>
        private void BindHighLightType()
        {
            ZNode.Libraries.Admin.HighlightAdmin highlight = new HighlightAdmin();
            ddlHighlightType.DataSource = highlight.GetAllHighLightType();
            ddlHighlightType.DataTextField = "Name";
            ddlHighlightType.DataValueField = "HighlightTypeId";
            ddlHighlightType.DataBind();
            ListItem item2 = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString().ToUpper(), string.Empty);
            ddlHighlightType.Items.Insert(0, item2);
            ddlHighlightType.SelectedIndex = 0;
        }

        /// <summary>
        /// Binds Search Data
        /// </summary>
        /// <returns>Returns list of Highlights object.</returns>
        private TList<Highlight> BindSearchData()
        {
            HighlightService highlightService = new HighlightService();
            HighlightQuery highlightQuery = new HighlightQuery();

            if (txtName.Text.Trim().Length > 0)
            {
                highlightQuery.Append("Name", "%" + Server.HtmlEncode(txtName.Text) + "%");
            }

            if (ddlHighlightType.SelectedValue.Trim().Length > 0)
            {
                highlightQuery.Append("HighlightTypeID", ddlHighlightType.SelectedValue);
            }

            TList<Highlight> highlightList = highlightService.Find(highlightQuery.GetParameters());

            if (highlightList != null)
            {
                highlightService.DeepLoad(highlightList, true, DeepLoadType.IncludeChildren, typeof(HighlightType));

                highlightList.Sort("DisplayOrder");
            }
            if (highlightList != null && highlightList.Count > 0)
            {
                btnAddSelectedAddons.Visible = true;
                btnBottomCancel.Visible = true;
            }
            else
            {
                btnAddSelectedAddons.Visible = false;
                btnBottomCancel.Visible = false;
            }
            return highlightList;
        }

        #endregion
    }
}