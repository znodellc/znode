<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Inventory.Products.ProductsDelete" CodeBehind="Delete.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h5>
        <asp:Localize ID="PleaseConfirm" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm %>'></asp:Localize></h5>
    <p>
        <asp:Localize ID="DeleteProductConfirmation" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextDeleteProductConfirmation %>'></asp:Localize><b>
            <%=ProductName%></b>
        <asp:Localize ID="ProductCannotUndone" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextDeleteProductCannotUndone %>'></asp:Localize>
    </p>
    <asp:Label CssClass="Error" ID="lblErrorMessage" runat="server" Text=''></asp:Label>
    <div>
        <zn:Button ID="btnDelete" runat="server" ButtonType="CancelButton" CausesValidation="True" OnClick="BtnDelete_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonDelete %>" />
        <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" CausesValidation="false" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel %>" />
    </div>
</asp:Content>
