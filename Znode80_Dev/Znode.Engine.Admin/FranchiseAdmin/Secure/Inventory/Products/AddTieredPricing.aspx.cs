using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_add_TieredPricing class.
    /// </summary>
    public partial class AddTieredPricing : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int itemId = 0;
        private int productTierId = 0;
        private string associateName = string.Empty;
        private string viewLink = "~/FranchiseAdmin/Secure/Inventory/Products/view.aspx?itemid=";
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Protected Properties
        /// <summary>
        /// Gets the product name.
        /// </summary>
        protected string ProductName
        {
            get
            {
                ProductAdmin productAdmin = new ProductAdmin();
                ZNode.Libraries.DataAccess.Entities.Product product = productAdmin.GetByProductId(this.itemId);

                if (product != null)
                {
                    return product.Name;
                }

                return string.Empty;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Retrieve Product Id from Query string
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
                if (!UserStoreAccess.CheckUserRoleInProduct(ProfileCommon.Create(HttpContext.Current.User.Identity.Name, true), this.itemId))
                {
                    Response.Redirect("default.aspx");
                }
            }

            if (Request.Params["tierid"] != null)
            {
                this.productTierId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["tierid"].ToString()));
            }

            if (!IsPostBack)
            {
                if (this.productTierId > 0)
                {
                    // Edit Mode
                    lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditTierPricing").ToString() + this.ProductName;

                    this.BindData();
                }
                else
                {
                    lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddTierPricing").ToString() + this.ProductName;
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAdmin productTierAdmin = new ProductAdmin();
            ProductTier productTier = new ProductTier();

            if (this.productTierId > 0)
            {
                productTier = productTierAdmin.GetByProductTierId(this.productTierId);
            }

            productTier.ProfileID = UserStoreAccess.GetTurnkeyStoreProfileID;

            productTier.TierStart = int.Parse(txtTierStart.Text.Trim());
            productTier.TierEnd = int.Parse(txtTierEnd.Text.Trim());
            productTier.Price = decimal.Parse(txtPrice.Text.Trim());

            // Set ProductId field
            productTier.ProductID = this.itemId;

            if (this.IsTieredPricingExists(productTier))
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorTiredPricingAlreadyExist").ToString();
                return;
            }

            bool isSuccess = false;

            // If Edit mode, then update fields
            if (this.productTierId > 0)
            {
                this.associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditTieredPricing").ToString() +this.ProductName;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, ProductName);

                isSuccess = productTierAdmin.UpdateProductTier(productTier);
            }
            else
            {
                this.associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddedTieredPricing").ToString() + this.ProductName;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, ProductName);

                isSuccess = productTierAdmin.AddProductTier(productTier);
            }

            if (isSuccess)
            {
                if (this.itemId > 0)
                {
                    ZNode.Libraries.DataAccess.Entities.Product product = productTierAdmin.GetByProductId(this.itemId);
                    product.ReviewStateID = productTierAdmin.UpdateReviewStateID(UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId, "Tiered Pricing");
                    productTierAdmin.Update(product);
                }

                Response.Redirect(this.viewLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())) + "&mode=" + HttpUtility.UrlEncode(this.encrypt.EncryptData("tieredPricing")));
            }
            else
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditTieredPricing").ToString();
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.viewLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())) + "&mode=" + HttpUtility.UrlEncode(this.encrypt.EncryptData("tieredPricing")));
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind edit data fields
        /// </summary>
        private void BindData()
        {
            ProductAdmin productTierAdmin = new ProductAdmin();
            ProductTier productTier = productTierAdmin.GetByProductTierId(this.productTierId);

            if (productTier != null)
            {
                ProductAdmin productAdmin = new ProductAdmin();
                ZNode.Libraries.DataAccess.Entities.Product product = productAdmin.GetByProductId(productTier.ProductID);

                if (product != null)
                {
                    UserStoreAccess.CheckStoreAccess(product.PortalID.GetValueOrDefault(0), true);
                }

                txtPrice.Text = productTier.Price.ToString("N");
                txtTierStart.Text = productTier.TierStart.ToString();
                txtTierEnd.Text = productTier.TierEnd.ToString();
            }
        }
        #endregion

        #region Helper Method
        /// <summary>
        /// To check whether the tiered pricing already exists for this product
        /// </summary>
        /// <param name="productTier">ProductTier object to check tiered pricing exist.</param>
        /// <returns>Returns true if tiered pricing exist else false.</returns>
        private bool IsTieredPricingExists(ProductTier productTier)
        {
            TList<ProductTier> list;
            ProductTierService productTierService = new ProductTierService();
            ProductTierQuery query = new ProductTierQuery();
            query.Append(ProductTierColumn.ProductID, productTier.ProductID.ToString());
            if (this.productTierId > 0)
            {
                query.AppendNotEquals(ProductTierColumn.ProductTierID, productTier.ProductTierID.ToString());
            }

            TList<ProductTier> Mainlist = productTierService.Find(query.GetParameters());
            if (productTier.ProfileID.HasValue)
            {
                list = Mainlist.FindAll(delegate(ProductTier p) { return p.ProfileID == null || p.ProfileID == productTier.ProfileID; });
            }
            else
            {
                list = Mainlist;
            }

            if (list.Count > 0)
            {
                foreach (ProductTier listitem in list)
                {
                    if ((productTier.TierStart >= listitem.TierStart) && (productTier.TierStart <= listitem.TierEnd))
                    {
                        return true;
                    }

                    if ((productTier.TierEnd >= listitem.TierStart) && (productTier.TierEnd <= listitem.TierEnd))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
        #endregion
    }
}