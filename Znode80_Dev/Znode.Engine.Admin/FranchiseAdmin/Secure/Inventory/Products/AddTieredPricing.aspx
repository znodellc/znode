<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.FranchiseAdmin.Secure.Inventory.Products.AddTieredPricing" Codebehind="AddTieredPricing.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblHeading" runat="server" /></h1>
        </div>
        <div style="text-align: right;display:none;"> 
           <zn:Button runat="server"  ID="btnSubmitTop" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" />
           <zn:Button runat="server" ID="btnCancelTop"  ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="false" />
             
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
        <asp:Label ID="lblError" CssClass="Error" runat="server"></asp:Label>
        <div class="FormView"> 
            <div class="FieldStyle"> 
                <asp:Localize ID="ColumnTitleTierStart" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTierStart %>'></asp:Localize> 
                <span class="Asterix">*</span><br />
                <small><asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextMinQuantityItems %>'></asp:Localize> </small></div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtTierStart" runat="server" Width="80px"></asp:TextBox>
          
                <asp:RequiredFieldValidator ID="RequiredFieldValidator19" runat="server" ControlToValidate="txtTierStart"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredMinimumQuantity %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator10" runat="server" ControlToValidate="txtTierStart"
                    CssClass="Error" Display="dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeProductQuantity %>' MaximumValue="10000"
                    MinimumValue="1" SetFocusOnError="true" Type="Integer"></asp:RangeValidator>
                </div>
            <div class="FieldStyle">
                
                <asp:Localize ID="ColumntitleTierEnd" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumntitleTierEnd %>'></asp:Localize> 
                <span class="Asterix">*</span><br />
                <small><asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextMaxQuantityItems %>'></asp:Localize> </small></div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtTierEnd" runat="server" Width="80px"></asp:TextBox>
            
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTierEnd"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredMaximumQuantity %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtTierEnd"
                    CssClass="Error" Display="dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>' MaximumValue="10000"
                    MinimumValue="1" SetFocusOnError="true" Type="Integer"></asp:RangeValidator>
                <asp:CompareValidator ID="cmpTierLimitsValidator" runat="server" ControlToValidate="txtTierEnd"
                    ControlToCompare="txtTierStart" Display="Dynamic" Type="Integer" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareMaxQuantity %>'
                    CssClass="Error" Operator="GreaterThan"></asp:CompareValidator>
            </div>
            <div class="FieldStyle">
                Price<span class="Asterix">*</span></div>
            <div class="ValueStyle">
                <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox
                    ID="txtPrice" runat="server" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDiscountPrice %>'
                    ControlToValidate="txtPrice" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtPrice"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeDiscountedPrice %>'
                    MaximumValue="99999999" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div>  
                  <zn:Button runat="server"  ID="btnSubmitBottom" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" />
                 <zn:Button runat="server" ID="btnCancelBottom"  ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="false" />
                 
            </div>
        </div>
    </div>
</asp:Content>
