﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductTags.ascx.cs" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Inventory.Products.ProductTags" %>
<%@ Register TagPrefix="znode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:UpdatePanel runat="server" ID="upProductTags">
	<ContentTemplate>
		<asp:Panel ID="pnlTagsList" runat="server">
			<div class="FormView">
				<h4 class="GridTitle">
                    <asp:Localize runat="server" ID="AssociatedTags" Text='<%$ Resources:ZnodeAdminResource, SubTitleAssociatedTags %>'></asp:Localize>
				</h4>
				<div>
					<div class="TabDescription" style="width: 80%;">
						<p>
							<asp:Localize runat="server" ID="SubTextAssociatedTags" Text='<%$ Resources:ZnodeAdminResource, SubTextAssociatedTags %>'></asp:Localize>
						</p>
					</div>
					<asp:Panel ID="pnlAddTag" runat="server">
						<div>
						</div>
						<div class="FieldStyle">
							<asp:Localize runat="server" ID="TagNames" Text='<%$ Resources:ZnodeAdminResource, ColumnTagNames %>'></asp:Localize>
						</div>
						<div class="ValueStyle">
							<asp:TextBox ID="txtTagNames" runat="server" Columns="50" TextMode="MultiLine"
								Rows="12"></asp:TextBox>
							<div>
								<znode:Spacer ID="Spacer" SpacerHeight="15" SpacerWidth="3" runat="server"></znode:Spacer>
							</div>
						</div>
						<div class="ClearBoth"></div>
						<div>
						    <zn:Button runat="server" ID="btnSubmitBottom"  ButtonType="SubmitButton" OnClick="Update_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true" />
                            <zn:Button runat="server" ID="btnCancelBottom" ButtonType="CancelButton" OnClick="Cancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
						</div>
					</asp:Panel>
				</div>
				<div class="FieldStyle">
					<asp:Label ID="lblMsg" CssClass="Error" runat="server"></asp:Label>
				</div>
				<br />
				<znode:Spacer ID="Spacer66" runat="server" SpacerHeight="10" />
			</div>
		</asp:Panel>
	</ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="uxTagUpdateProgress" runat="server" AssociatedUpdatePanelID="upProductTags" DisplayAfter="10">
	<ProgressTemplate>
		<div id="ajaxProgressBg"></div>
		<div id="ajaxProgress"></div>
	</ProgressTemplate>
</asp:UpdateProgress>
