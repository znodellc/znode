<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Inventory.Products.AddSku" ValidateRequest="false" Codebehind="AddSku.aspx.cs" %>

<%@ Register TagPrefix="znode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ProductTags" Src="ProductTags.ascx" %>
<%@ Register Src="~/FranchiseAdmin/Controls/Default/ImageUploader.ascx" TagName="UploadImage" TagPrefix="ZNode" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">

<asp:HiddenField ID="existSKUvalue" runat="server" />
    <div class="FormView">
        <div class="LeftFloat" style="text-align: left">
            <h1>
                <asp:Label ID="lblHeading" runat="server" />
            </h1>
        </div>
        <div style="text-align: right;display:none;">
            <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../Themes/Images/buttons/button_submit_highlight.gif';"
            onmouseout="this.src='../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/FranchiseAdmin/Themes/images/buttons/button_submit.gif"
            runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
        <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../Themes/Images/buttons/button_cancel_highlight.gif';"
            onmouseout="this.src='../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/FranchiseAdmin/Themes/images/buttons/button_cancel.gif"
            runat="server" AlternateText="Submit" OnClick="BtnCancel_Click" />
        </div>
        <div class="ClearBoth" align="left">           
        </div>       
        <div>
            <asp:Label ID="lblError" CssClass="Error" runat="server"></asp:Label>
        </div>
        <div class="FieldStyle">
            &nbsp;</div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnSKUorPart" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSKUorPart%>'></asp:Localize><span class="Asterix">*</span></div>
        <div class="ValueStyle">
            <asp:TextBox ID="SKU" runat="server" Columns="30" MaxLength="100"></asp:TextBox>
             <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="SKU"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSKU%>' SetFocusOnError="True" CssClass="Error"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnQuantityOnHand" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnQuantityOnHand%>'></asp:Localize><span class="Asterix">*</span><br />
        <small><asp:Localize ID="HintTextskuInventoryLevel" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextskuInventoryLevel%>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="Quantity" runat="server" Columns="5" MaxLength="4">1</asp:TextBox>&nbsp;
             <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" ControlToValidate="Quantity"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSkuQuantity%>' CssClass="Error"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="Quantity"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeSKuQuantity%>' MaximumValue="9999"
                MinimumValue="0" Type="Integer" CssClass="Error"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
             <asp:Localize ID="ColumnReOrderLevel" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnReOrderLevel%>'></asp:Localize></div>
        <div class="ValueStyle">
            <asp:TextBox ID="ReOrder" runat="server" Columns="5" MaxLength="3"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="ReOrder"
            Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSKUReOrderLevel%>'
            SetFocusOnError="true" ValidationExpression="(^N/A$)|(^[-]?(\d+)(\.\d{0,3})?$)|(^[-]?(\d{1,3},(\d{3},)*\d{3}(\.\d{1,3})?|\d{1,3}(\.\d{1,3})?)$)"
            CssClass="Error"></asp:RegularExpressionValidator>
        </div>
        
        <asp:Panel ID="pnlProductAttributes" runat="server">
            <div class="FieldStyle">
                <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAdditionalWeight%>'></asp:Localize></div>
            
            <div class="ValueStyle">
                <asp:TextBox ID="WeightAdditional" runat="server" Columns="10" MaxLength="7"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.WeightUnit %>
                <asp:CompareValidator ID="CompareValidator4" SetFocusOnError="true" runat="server"
                ControlToValidate="WeightAdditional" Type="Double" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeAdditionalWeight%>'
                CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnRetailPrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRetailPrice%>'></asp:Localize> <br />
            <small><asp:Localize ID="ColumnTextSKURetailPrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextSKURetailPrice%>'></asp:Localize></small></div>
            <div class="ValueStyle" style="margin-left:-10px;">
                <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox ID="RetailPrice" runat="server" Columns="10" MaxLength="7"></asp:TextBox>
                  <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="RetailPrice"
                Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeSKURetailPrice%>'
                CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnSalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSalePrice%>'></asp:Localize> <br />
            <small> <asp:Localize ID="ColumnTextSKUSalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextSKUSalePrice%>'></asp:Localize></small></div>
           
            <div class="ValueStyle" style="margin-left:-10px;">
                <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox ID="SalePrice" runat="server" Columns="10" MaxLength="7"></asp:TextBox>
                 <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="SalePrice"
                Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareSKUSalePrice%>'
                CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnWholeSalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWholeSalePrice%>'></asp:Localize> <br />
            <small><asp:Localize ID="ColumnTextSKUWholeSalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextSKUWholeSalePrice%>'></asp:Localize></small>
            </div>
           
            <div class="ValueStyle" style="margin-left:-10px;">
                <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox ID="WholeSalePrice" runat="server" Columns="10" MaxLength="7"></asp:TextBox>
                 <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="WholeSalePrice"
                Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareSKUWholeSalePrice%>'
                CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnEnableInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableInventory%>'></asp:Localize></div>
            <div class="ValueStyle">
                <asp:CheckBox ID='VisibleInd' runat='server' Text='<%$ Resources:ZnodeAdminResource, ColumnTextEnableInventory%>'
                    Checked="true"></asp:CheckBox></div>
            <div class="FieldStyle" id="DivAttributes" runat="Server">
                <asp:Localize ID="ColumnProductAttributes" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductAttributes%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:PlaceHolder ID="ControlPlaceHolder" runat="server"></asp:PlaceHolder>
            </div>
            <asp:Panel runat="server" ID="pnlSupplier" Visible="false">
                <div class="FieldStyle">
                    <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSKUSupplier%>'></asp:Localize>
                    <br />
                    <small><asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextSKUSupplier%>'></asp:Localize></small></div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlSupplier" runat="server" />
                </div>
            </asp:Panel>
            <div class="ClearBoth">
            </div>          
            <h4 class="SubTitle">
               <asp:Localize ID="SubTitleSKUImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSKUImage%>'></asp:Localize></h4>
            <small><asp:Localize ID="SubTextSKUImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextSKUImage%>'></asp:Localize></small>
            <div>
                <znode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></znode:Spacer>
            </div>
             <div id="tblShowImage" visible="false" runat="server">
                    <div>
                        <asp:Image ID="SKUImage" runat="server" />
                    </div>
                    <div>
                        <znode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></znode:Spacer>
                    </div>
                    <div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnSelectanOption" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectanOption%>'></asp:Localize></div>
                        <div class="ValueStyle">
                            <asp:RadioButton ID="RadioSKUCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioTextKeepCurrentImage%>' runat="server"
                                GroupName="SKU Image" AutoPostBack="True" OnCheckedChanged="RadioSKUCurrentImage_CheckedChanged"
                                Checked="True" />
                            <asp:RadioButton ID="RadioSKUNewImage"  Text='<%$ Resources:ZnodeAdminResource, RadioTextUploadNewImage%>' runat="server" GroupName="SKU Image"
                                AutoPostBack="True" OnCheckedChanged="RadioSKUNewImage_CheckedChanged" />
                            <asp:RadioButton ID="RadioSKUNoImage" Text='<%$ Resources:ZnodeAdminResource, RadioTextNoImage%>' runat="server" GroupName="SKU Image"
                                AutoPostBack="True" OnCheckedChanged="RadioSKUNoImage_CheckedChanged" />
                        </div>
                    </div>
                
            </div>
            <div class="ClearBoth"></div>
            <div id="tblSKUDescription" runat="server" visible="false">
                <div>
                    <div>
                        <div>
                            <asp:Label ID="lblSKUImageError" runat="server" CssClass="Error" ForeColor="Red"
                                Text="" Visible="False"></asp:Label>
                        </div>
                        <div class="FieldStyle">                        
                            <asp:Localize ID="ColumnSelectImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectImage%>'></asp:Localize>&nbsp;                            
                        </div>
                        <div class="ValueStyle">
                            <ZNode:UploadImage ID="UploadSKUImage" runat="server"></ZNode:UploadImage>
                        </div>
                    </div>
                </div>
            </div>
           
            <div class="FieldStyle">
               <asp:Localize ID="Localize4" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSKUImageALTText%>'></asp:Localize><br />
           </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtImageAltTag" runat="server"></asp:TextBox></div>
            <asp:Panel ID="pnlRecurringBilling" runat="server" Visible="false">
                <h4>
                    <asp:Localize ID="SubTitleRecurringBilling" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleRecurringBilling%>'></asp:Localize></h4>
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnBillingPeriod" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBillingPeriod%>'></asp:Localize><span class="Asterix">*</span>
                <small><asp:Localize ID="ColumnTextRecurringBilling" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextRecurringBilling%>'></asp:Localize></small></div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlBillingPeriods" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DdlBillingPeriods_SelectedIndexChanged">
                                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, ColumnBillingPeriodDay%>' Value="DAY"></asp:ListItem>
                                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, ColumnBillingPeriodWeek%>' Value="WEEK"></asp:ListItem>
                                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, ColumnBillingPeriodMonth%>' Value="MONTH"></asp:ListItem>
                                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, ColumnBillingPeriodYear%>' Value="YEAR"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="Localize5" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBillingFrequency%>'></asp:Localize><span class="Asterix">*</span>
                <small><asp:Localize ID="Localize6" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextBillingFrequency%>'></asp:Localize></small></div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlBillingFrequency" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="FieldStyle">
                   <asp:Localize ID="ColumnBillingCycles" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBillingCycles%>'></asp:Localize><span class="Asterix">*</span>
                <small><asp:Localize ID="ColumnTextBillingCycles" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextBillingCycles%>'></asp:Localize></small></div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSubscrptionCycles" runat="server">1</asp:TextBox></div>
            </asp:Panel>
        </asp:Panel>
        
        <div class="ClearBoth"></div>
        <div>
           <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmit" CausesValidation="true" />
           <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancel" />
        </div>
    </div>
</asp:Content>
