using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace  Znode.Engine.FranchiseAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_add_sku class.
    /// </summary>
    public partial class AddSku : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private int skuId = 0;
        private int productTypeId = 0;
        private string viewLink = "~/FranchiseAdmin/Secure/Inventory/Products/view.aspx?itemid=";
        private string associateName = string.Empty;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Private Property
        private string ProductName
        {
            get
            {
                ProductAdmin AdminAccess = new ProductAdmin();
                ZNode.Libraries.DataAccess.Entities.Product entity = AdminAccess.GetByProductId(this.itemId);
                UserStoreAccess.CheckStoreAccess(entity.PortalID.GetValueOrDefault(0), true);
                if (entity != null)
                {
                    return entity.Name;
                }

                return string.Empty;
            }
        }

        #endregion
        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }

            if (Request.Params["skuid"] != null)
            {
                this.skuId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["skuid"].ToString()));
            }

            if (Request.Params["typeid"] != null)
            {
                this.productTypeId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["typeid"].ToString()));
            }
           
            this.Bind();

            if (!Page.IsPostBack)
            {
                this.BindBillingFrequency();

                // Bind Supplier 
                this.BindSuppliersTypes();

                this.ShowSubscriptionPanel();

                if ((this.itemId > 0) && (this.skuId > 0))
                {
                    lblHeading.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditProductSKU").ToString(), this.ProductName);
                    // Bind Sku Details
                    this.BindData();

                    // Bind Attributes
                    this.BindAttributes();
                    tblShowImage.Visible = true;
                }
                else
                {
                    tblSKUDescription.Visible = true;
                    lblHeading.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddProductSKU").ToString(), this.ProductName);
                }
            }
        }
        #endregion

        #region General Events

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            bool isSuccess = this.UpdateSKU();
            if (isSuccess)
            {
                ProductAdmin prodAdmin = new ProductAdmin();
                ZNode.Libraries.DataAccess.Entities.Product entity = prodAdmin.GetByProductId(this.itemId);

                associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditProductSKU").ToString(), entity.Name);
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, entity.Name);

                // Redirect to View Page
                Response.Redirect(this.viewLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())) + "&mode=" + HttpUtility.UrlEncode(this.encrypt.EncryptData("inventory")));
           
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            // Redirect to View Page
            Response.Redirect(this.viewLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())) + "&mode=" + HttpUtility.UrlEncode(this.encrypt.EncryptData("inventory")));
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioSKUCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblSKUDescription.Visible = false;
            SKUImage.Visible = true;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioSKUNoImage_CheckedChanged(object sender, EventArgs e)
        {
            tblSKUDescription.Visible = false;
            SKUImage.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioSKUNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblSKUDescription.Visible = true;
            SKUImage.Visible = true;
        }

        /// <summary>
        /// Occurs when billing period dropdown list item selected.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlBillingPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindBillingFrequency();
        }

        /// <summary>
        /// Update the product SKU.
        /// </summary>
        /// <returns>Returns true if updated else false.</returns>
        private bool UpdateSKU()
        {
            System.IO.FileInfo fileInfo = null;
            SKUAdmin skuAdmin = new SKUAdmin();
            SKU skuEntity = new SKU();
            SKUAttribute skuAttribute = new SKUAttribute();
            ProductAdmin prodAdminAccess = new ProductAdmin();

            if (this.skuId > 0)
            {
                skuEntity = skuAdmin.GetBySKUID(this.skuId);
            }

            // Check SKU exists, If already exists return false
            bool isSkuExist = skuAdmin.IsSkuExist(Server.HtmlEncode(SKU.Text.Trim()), this.itemId);

            if (isSkuExist)
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductSKUExist").ToString(); 
                return false;
            }

            // Set Values to SKU
            skuEntity.SKU = Server.HtmlEncode(SKU.Text.Trim());

            // Update inventory record.
            int reorder = 0;
            int.TryParse(ReOrder.Text.Trim(), out reorder);
            SKUAdmin.UpdateQuantity(skuEntity, int.Parse(Quantity.Text.Trim()), reorder);

            if (WeightAdditional.Text.Trim().Length > 0)
            {
                skuEntity.WeightAdditional = Convert.ToDecimal(WeightAdditional.Text);
            }

            if (RetailPrice.Text.Trim().Length > 0)
            {
                skuEntity.RetailPriceOverride = Convert.ToDecimal(RetailPrice.Text.Trim());
            }
            else
            {
                skuEntity.RetailPriceOverride = null;
            }

            // Set Sale price override property
            if (SalePrice.Text.Trim().Length > 0)
            {
                skuEntity.SalePriceOverride = Convert.ToDecimal(SalePrice.Text.Trim());
            }
            else
            {
                skuEntity.SalePriceOverride = null;
            }

            // Set wholesale price override property
            if (WholeSalePrice.Text.Trim().Length > 0)
            {
                skuEntity.WholesalePriceOverride = decimal.Parse(WholeSalePrice.Text.Trim());
            }
            else
            {
                skuEntity.WholesalePriceOverride = null;
            }

            skuEntity.ProductID = this.itemId;
            skuEntity.ActiveInd = VisibleInd.Checked;
            skuEntity.ImageAltTag = txtImageAltTag.Text.Trim();

            // Image Validation
            if (RadioSKUNoImage.Checked == false)
            {
                // Validate image    
                if ((this.skuId == 0) || (RadioSKUNewImage.Checked == true))
                {
                    if (UploadSKUImage.PostedFile != null && !string.IsNullOrEmpty(UploadSKUImage.PostedFile.FileName))
                    {
                        if (!UploadSKUImage.IsFileNameValid())
                        {
                            return false;
                        }

                        if (UploadSKUImage.PostedFile.FileName != string.Empty)
                        {
                            // Check for Store Image
                            fileInfo = UploadSKUImage.FileInformation;

                            if (fileInfo != null)
                            {
								skuEntity.SKUPicturePath = "Turnkey/" + UserStoreAccess.GetTurnkeyStorePortalID.Value.ToString() + "/" + fileInfo.Name;
                            }
                        }
                    }
                }
                else
                {
					skuEntity.SKUPicturePath = skuEntity.SKUPicturePath;  
                }
            }
            else
            {
                skuEntity.SKUPicturePath = null;
            }

            // Upload File if this is a new sku or the New Image option was selected for an existing sku
            if (RadioSKUNewImage.Checked || this.skuId == 0)
            {
                if (fileInfo != null)
                {
					UploadSKUImage.SaveImage("fadmin", UserStoreAccess.GetTurnkeyStorePortalID.Value.ToString());
                }
            }

            // Supplier
            if (ddlSupplier.SelectedIndex != -1)
            {
                if (ddlSupplier.SelectedItem.Text.Equals("None") || !pnlSupplier.Visible)
                {
                    skuEntity.SupplierID = null;
                }
                else
                {
                    skuEntity.SupplierID = Convert.ToInt32(ddlSupplier.SelectedValue);
                }
            }

            // Recurring Billing
            skuEntity.RecurringBillingPeriod = ddlBillingPeriods.SelectedItem.Value;
            skuEntity.RecurringBillingFrequency = ddlBillingFrequency.SelectedItem.Text;
            skuEntity.RecurringBillingTotalCycles = int.Parse(txtSubscrptionCycles.Text.Trim());

            // For Attribute value.
            string attributes = String.Empty;

            DataSet attributeTypeDataSet = prodAdminAccess.GetAttributeTypeByProductTypeID(this.productTypeId);

            foreach (DataRow MyDataRow in attributeTypeDataSet.Tables[0].Rows)
            {
                System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                int selectedValue = int.Parse(lstControl.SelectedValue);
                if (selectedValue > 0)
                {
                    attributes += selectedValue.ToString() + ",";
                }
            }

            // Split the string
            string attribute;
            bool isExist = false;
            if (attributes.Length > 0)
            {
                attribute = attributes.Substring(0, attributes.Length - 1);

                // To check SKU combination is already exists.
                isExist = skuAdmin.CheckSKUAttributes(this.itemId, this.skuId, attribute);
            }

            if (isExist)
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductSKUAttributeExist").ToString();
                return false;
            }

            bool isSuccess = false;

            int? reviewStateID = prodAdminAccess.UpdateReviewStateID(skuEntity, UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId);

            // Check For Edit SKu
            if (this.skuId > 0)
            {
                // Set update date for web service download
                skuEntity.UpdateDte = System.DateTime.Now;

                // Update Product SKU
                isSuccess = skuAdmin.Update(skuEntity);

                // Delete SKUAttributes
                skuAdmin.DeleteBySKUId(this.skuId);
            }
            else
            {
                // Add New Product SKU and SKUAttribute
                isSuccess = skuAdmin.Add(skuEntity, out this.skuId);
            }

            foreach (DataRow MyDataRow in attributeTypeDataSet.Tables[0].Rows)
            {
                System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                int selValue = int.Parse(lstControl.SelectedValue);

                if (selValue > 0)
                {
                    skuAttribute.AttributeId = selValue;

                    skuAttribute.SKUID = this.skuId;

                    skuAdmin.AddSKUAttribute(skuAttribute);
                }
            }
            return isSuccess;
        }

        #endregion

        #region Bind Methods
        /// <summary>
        /// Set description panel visibility.
        /// </summary>
        private void ShowSubscriptionPanel()
        {
            ProductAdmin prodAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product entity = prodAdmin.GetByProductId(this.itemId);

            if (entity != null)
            {
                pnlRecurringBilling.Visible = entity.RecurringBillingInd;
            }
        }

        /// <summary>
        /// Bind control display based on properties set
        /// </summary>
        private void Bind()
        {
            ProductAdmin productAdmin = new ProductAdmin();
            DataSet attributeTypeDataSet = productAdmin.GetAttributeTypeByProductTypeID(this.productTypeId);

            // Repeats until Number of AttributeType for this Product
            foreach (DataRow dr in attributeTypeDataSet.Tables[0].Rows)
            {
                // Bind Attributes
                DataSet attributeDataSet = productAdmin.GetByAttributeTypeID(int.Parse(dr["attributetypeid"].ToString()));

                System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                lstControl.ID = "lstAttribute" + dr["AttributeTypeId"].ToString();

                ListItem li = new ListItem(dr["Name"].ToString(), "0");
                li.Selected = true;

                lstControl.DataSource = attributeDataSet;
                lstControl.DataTextField = "Name";
                lstControl.DataValueField = "AttributeId";
                lstControl.DataBind();
                lstControl.Items.Insert(0, li);

                if (!Convert.ToBoolean(dr["IsPrivate"]))
                {
                    // Add Dynamic Attribute DropDownlist in the Placeholder
                    ControlPlaceHolder.Controls.Add(lstControl);

                    // Required Field validator to check SKU Attribute
                    RequiredFieldValidator FieldValidator = new RequiredFieldValidator();
                    FieldValidator.ID = "Validator" + dr["AttributeTypeId"].ToString();
                    FieldValidator.ControlToValidate = "lstAttribute" + dr["AttributeTypeId"].ToString();
                    FieldValidator.ErrorMessage = "Select " + dr["Name"].ToString();
                    FieldValidator.Display = ValidatorDisplay.Dynamic;
                    FieldValidator.CssClass = "Error";
                    FieldValidator.InitialValue = "0";

                    ControlPlaceHolder.Controls.Add(FieldValidator);
                    Literal lit1 = new Literal();
                    lit1.Text = "&nbsp;&nbsp;";
                    ControlPlaceHolder.Controls.Add(lit1);
                }
            }

            // Hide the Product Attribute
            if (attributeTypeDataSet.Tables[0].Rows.Count == 0)
            {
                DivAttributes.Visible = false;
                pnlProductAttributes.Visible = false;
            }
        }

        /// <summary>
        /// Bind Edit Attributes
        /// </summary>
        private void BindAttributes()
        {
            SKUAdmin skuAdmin = new SKUAdmin();
            ProductAdmin productAdmin = new ProductAdmin();
            SKUQuery skuQuery = new SKUQuery();
            DataSet SkuDataSet = skuAdmin.GetBySKUId(this.skuId);
            DataSet attributeTypeDataSet = productAdmin.GetAttributeTypeByProductTypeID(this.productTypeId);

            foreach (DataRow dr in attributeTypeDataSet.Tables[0].Rows)
            {
                foreach (DataRow Dr in SkuDataSet.Tables[0].Rows)
                {
                    System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + dr["AttributeTypeId"].ToString());

                    if (lstControl != null)
                    {
                        lstControl.SelectedValue = Dr["Attributeid"].ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Binds Edit SKU Datas
        /// </summary>
        private void BindData()
        {
            // Create Instance for SKU Admin and SKU entity
            SKUAdmin skuAdmin = new SKUAdmin();
            SKU skuEntity = skuAdmin.DeepLoadBySKUID(this.skuId);

            if (skuEntity != null)
            {
                UserStoreAccess.CheckStoreAccess(skuEntity.ProductIDSource.PortalID.GetValueOrDefault(0), true);

                SKU.Text = Server.HtmlDecode(skuEntity.SKU);

                existSKUvalue.Value = skuEntity.SKU;

                // Load the inventory record and assign to text boxes.
                SKUInventory si = SKUAdmin.GetInventory(skuEntity);
                if (si != null)
                {
                    Quantity.Text = si.QuantityOnHand.ToString();
                    ReOrder.Text = si.ReOrderLevel.ToString();
                }

                WeightAdditional.Text = skuEntity.WeightAdditional.ToString();
                if (skuEntity.RetailPriceOverride.HasValue)
                {
                    RetailPrice.Text = skuEntity.RetailPriceOverride.Value.ToString("N");
                }

                if (skuEntity.SalePriceOverride.HasValue)
                {
                    SalePrice.Text = skuEntity.SalePriceOverride.Value.ToString("N");
                }

                if (skuEntity.WholesalePriceOverride.HasValue)
                {
                    WholeSalePrice.Text = skuEntity.WholesalePriceOverride.Value.ToString("N");
                }

                VisibleInd.Checked = skuEntity.ActiveInd;
                if (ddlSupplier.SelectedIndex != -1)
                {
                    ddlSupplier.SelectedValue = skuEntity.SupplierID.ToString();
                }

                if (skuEntity.SKUPicturePath != null)
                {
                    ZNodeImage znodeImage = new ZNodeImage();
                    SKUImage.ImageUrl = znodeImage.GetImageHttpPathMedium(skuEntity.SKUPicturePath);
                }
                else
                {
                    RadioSKUNoImage.Checked = true;
                    RadioSKUCurrentImage.Visible = false;
                }

                txtImageAltTag.Text = skuEntity.ImageAltTag;

                pnlRecurringBilling.Visible = skuEntity.ProductIDSource.RecurringBillingInd;

                if (skuEntity.ProductIDSource.RecurringBillingInd)
                {
                    ddlBillingPeriods.SelectedValue = skuEntity.RecurringBillingPeriod;
                    ddlBillingFrequency.SelectedValue = skuEntity.RecurringBillingFrequency;
                    txtSubscrptionCycles.Text = skuEntity.RecurringBillingTotalCycles.GetValueOrDefault(0).ToString();
                }
            }
        }

        /// <summary>
        /// Bind supplier option list
        /// </summary>
        private void BindSuppliersTypes()
        {
            // Bind Supplier
            ZNode.Libraries.DataAccess.Service.SupplierService supplierService = new ZNode.Libraries.DataAccess.Service.SupplierService();
            TList<ZNode.Libraries.DataAccess.Entities.Supplier> list = supplierService.GetAll();
            list.Sort("DisplayOrder Asc");
            list.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.Supplier supplier)
            {
                return supplier.ActiveInd == true;
            });

            DataSet ds = list.ToDataSet(false);
            DataView dv = new DataView(ds.Tables[0]);
            ddlSupplier.DataSource = dv;
            ddlSupplier.DataTextField = "Name";
            ddlSupplier.DataValueField = "SupplierId";
            ddlSupplier.DataBind();
            ListItem li1 = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextNotApplicable").ToString(), "0");
            ddlSupplier.Items.Insert(0, li1);
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Bind billing frequency.
        /// </summary>
        private void BindBillingFrequency()
        {
            ddlBillingFrequency.Items.Clear();

            int upperBoundValue = 30;

            if (ddlBillingPeriods.SelectedValue == "WEEK")
            {
                upperBoundValue = 4;
            }
            else if (ddlBillingPeriods.SelectedValue == "MONTH")
            {
                upperBoundValue = 12;
            }
            else if (ddlBillingPeriods.SelectedValue == "YEAR")
            {
                upperBoundValue = 1;
            }

            for (int i = 1; i <= upperBoundValue; i++)
            {
                ddlBillingFrequency.Items.Add(i.ToString());
            }
        }
        #endregion
    }
}