using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_add_addons class.
    /// </summary>
    public partial class AddAddons : System.Web.UI.Page
    {
        #region Private Variables
        private static bool IsSearchEnabled = false;
        private string detailsLink;
        private int itemId = 0;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Helper Methods
        /// <summary>
        /// Gets the name of the Addon for this AddonId
        /// </summary>
        /// <param name="addOnId">Addon Id to get.</param>
        /// <returns>Returns the Addon name.</returns>
        public string GetAddOnName(object addOnId)
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            AddOn _addOn = productAddOnAdmin.GetByAddOnId(int.Parse(addOnId.ToString()));

            if (_addOn != null)
            {
                return _addOn.Name;
            }

            return string.Empty;
        }
        #endregion

        #region page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.detailsLink = "~/FranchiseAdmin/Secure/Inventory/Products/view.aspx?mode=" + HttpUtility.UrlEncode(this.encrypt.EncryptData("addons")) + "&itemid=";

            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
                if (!UserStoreAccess.CheckUserRoleInProduct(ProfileCommon.Create(HttpContext.Current.User.Identity.Name, true), this.itemId))
                {
                    Response.Redirect("default.aspx");
                }
            }
            else
            {
                this.itemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindAddons();
                this.BindProductName();
            }
        }
        #endregion                

        #region Related Addon Grid Events
        /// <summary>
        /// AddOn Grid items page event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxAddOnGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxAddOnGrid.PageIndex = e.NewPageIndex;

            if (IsSearchEnabled)
            {
                this.SearchAddons();
            }
            else
            {
                this.BindAddons();
            }
        }

        /// <summary>
        /// ProductAdd-Ons Grid Row command event - occurs when Manage button is fired.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxAddOnGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // cell in the GridView control.
                GridViewRow selectedRow = uxAddOnGrid.Rows[index];
            }
        }       
        
        /// <summary>
        /// Occurs when Add Addon button clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddSelectedAddons_Click(object sender, EventArgs e)
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            StringBuilder sb = new StringBuilder();
            StringBuilder addonName = new StringBuilder();

            // Loop through the grid values
            if (uxAddOnGrid.Rows.Count != 0)
            {
                foreach (GridViewRow row in uxAddOnGrid.Rows)
                {
                    CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProductAddon") as CheckBox;

                    // Get AddOnId
                    int AddOnID = int.Parse(row.Cells[1].Text);

                    if (check.Checked)
                    {
                        ProductAddOn ProdAddOnEntity = new ProductAddOn();

                        // Set Properties
                        ProdAddOnEntity.ProductID = this.itemId;
                        ProdAddOnEntity.AddOnID = AddOnID;

                        if (productAddOnAdmin.IsAddOnExists(ProdAddOnEntity))
                        {
                            productAddOnAdmin.AddNewProductAddOn(ProdAddOnEntity);
                            addonName.Append(this.GetAddOnName(AddOnID) + ",");
                            check.Checked = false;
                        }
                        else
                        {
                            sb.Append(this.GetAddOnName(AddOnID) + ",");
                            lblAddOnErrorMessage.Visible = true;
                        }
                    }
                }
            }
            else
            {
                return;
            }

            if (sb.ToString().Length > 0)
            {
                sb.Remove(sb.ToString().Length - 1, 1);

                // Display Error message
                lblAddOnErrorMessage.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductAddon").ToString(), sb.ToString());
            }
            else
            {
                ProductAdmin prodAdmin = new ProductAdmin();
                ZNode.Libraries.DataAccess.Entities.Product product = prodAdmin.GetByProductId(this.itemId);
                if (addonName.Length > 0)
                {
                    product.ReviewStateID = prodAdmin.UpdateReviewStateID(UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId, "AddOns Changed");

                    prodAdmin.Update(product);

                    addonName.Remove(addonName.Length - 1, 1);
                    string Associatename = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogProductAddon").ToString(), addonName, product.Name);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, Associatename, product.Name);
                    Response.Redirect(this.detailsLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
                }
                else
                {
                    lblAddOnErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorValidProductAddon").ToString();
                    lblAddOnErrorMessage.Visible = true;
                }
            }
        }

        /// <summary>
        /// Addon Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddOnSearch_Click(object sender, EventArgs e)
        {
            this.SearchAddons();
            IsSearchEnabled = true;
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.detailsLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Clear Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddOnClear_Click(object sender, EventArgs e)
        {
            this.BindAddons(); 

            // Reset Text fields & Bool variable
            txtAddonName.Text = string.Empty;
            txtAddOnTitle.Text = string.Empty;
            txtAddOnsku.Text = string.Empty;
            IsSearchEnabled = false;
        }
        #endregion        

        #region Bind Events
        /// <summary>
        /// Bind AddOn grid with filtered data
        /// </summary>
        private void SearchAddons()
        {
            ProductAddOnAdmin productAddOnAdmin = new ProductAddOnAdmin();
            int localeId = 0;
            uxAddOnGrid.DataSource = UserStoreAccess.CheckStoreAccess(productAddOnAdmin.SearchAddOns(Server.HtmlEncode(txtAddonName.Text.Trim()), Server.HtmlEncode(txtAddOnTitle.Text.Trim()), txtAddOnsku.Text.Trim(), localeId, 0, UserStoreAccess.GetTurnkeyStoreCatalogs[0].PortalID.GetValueOrDefault(0)).Tables[0]);
            uxAddOnGrid.DataBind();
            DisplayButtons();
        }

        private void BindProductName()
        {
            ProductAdmin ProductAdminAccess = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product entity = ProductAdminAccess.GetByProductId(this.itemId);
            if (entity != null)
            {
                lblTitle.Text = lblTitle.Text + entity.Name;
            }
        }

        /// <summary>
        /// Bind Addon Grid - all Addons
        /// </summary>
        private void BindAddons()
        {
            ProductAddOnAdmin ProdAddonAdminAccess = new ProductAddOnAdmin();

            // List of Addons
            uxAddOnGrid.DataSource = UserStoreAccess.CheckStoreAccess(ProdAddonAdminAccess.GetAllAddOns());
            uxAddOnGrid.DataBind();
            DisplayButtons();
        }

        /// <summary>
        /// To Enable / Disable Buttons Add and Cancel Buttons
        /// </summary>
        private void DisplayButtons()
        {
            if (uxAddOnGrid.Rows.Count != 0)
            {
                btnAddSelectedAddons.Visible = true;
                btnBottomCancel.Visible = true;
                return;
            }
            btnAddSelectedAddons.Visible = false;
            btnBottomCancel.Visible = false;
        }
        #endregion        
    }
}