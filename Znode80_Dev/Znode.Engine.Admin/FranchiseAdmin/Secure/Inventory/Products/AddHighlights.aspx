<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.FranchiseAdmin.Secure.Inventory.Products.AddHighlights" CodeBehind="AddHighlights.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <h1>
            <asp:Label ID="lblTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleAssociateHighlights%>'></asp:Label></h1>
        <div>
            <asp:Label CssClass="Error" ID="lblErrorMessage" runat="server" EnableViewState="false"
                Visible="false"></asp:Label>
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="SubTitleHighlightSearch" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleHighlightSearch%>'></asp:Localize></h4>
        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnName%>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtName" runat="server" ValidationGroup="grpSearch"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnType%>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlHighlightType" runat="server">
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnClearSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear%>' ID="btnClearSearch" CausesValidation="False" />
                    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch%>' ID="btnSearch" />
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle">
            <asp:Localize ID="GridTitleProductHighlight" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleProductHighlight%>'></asp:Localize></h4>
        <br />
        <!-- Update Panel for grid paging that are used to avoid the postbacks -->
        <asp:UpdatePanel ID="updPnlHightlight" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundNoProductHighlight%>'
                    PagerSettings-Visible="true" AllowSorting="True" PageSize="15" EnableSortingAndPagingCallbacks="False"
                    Width="100%" CaptionAlign="Left" GridLines="None" CellPadding="4"
                    AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="UxGrid_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSelect%>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkProductHighlight" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="HighlightId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID%>' HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName%>' HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleType%>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%# Eval("HighlightTypeIDSource.Name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder%>' DataField="DisplayOrder" HeaderStyle-HorizontalAlign="Left" />
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <PagerStyle CssClass="PagerStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            <uc1:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div>
            <zn:Button runat="server" Width="210px" ButtonType="EditButton" OnClick="BtnAddSelectedAddons_Click" CausesValidation="true" Text='<%$ Resources:ZnodeAdminResource, ButtonAssociateHighlights%>' ID="btnAddSelectedAddons" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnBottomCancel" />
        </div>
    </div>
</asp:Content>
