﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;


namespace  Znode.Engine.FranchiseAdmin.Secure.Inventory.Products
{
    public partial class AddDigitalAsset : System.Web.UI.Page
    {
        #region Protected Variables
        private int itemId;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Private Property
        /// <summary>
        /// Gets the product name.
        /// </summary>        
        private string ProductName
        {
            get
            {
                ProductAdmin AdminAccess = new ProductAdmin();
                ZNode.Libraries.DataAccess.Entities.Product entity = AdminAccess.GetByProductId(this.itemId);
                UserStoreAccess.CheckStoreAccess(entity.PortalID.GetValueOrDefault(0), true);
                if (entity != null)
                {
                    return entity.Name;
                }

                return string.Empty;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }
            else
            {
                this.itemId = 0;
            }

            if (Page.IsPostBack == false)
            {
                // If edit func then bind the data fields
                if (this.itemId > 0)
                {
                    lblTitle.Text += this.ProductName;
                }
                else
                {
                    throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotFoundProductRequested").ToString());
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Event fierd when submit button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAdmin AdminAccess = new ProductAdmin();

            DigitalAsset entity = new DigitalAsset();
            entity.DigitalAsset = Server.HtmlEncode(txtDigitalAsset.Text.Trim());
            entity.ProductID = this.itemId;
            entity.OrderLineItemID = null;

            bool isSuccess = AdminAccess.AddDigitalAsset(entity);

            if (isSuccess)
            {
                ProductAdmin productAdmin = new ProductAdmin();
                ZNode.Libraries.DataAccess.Entities.Product product = productAdmin.GetByProductId(this.itemId);
                product.ReviewStateID = productAdmin.UpdateReviewStateID(UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId, "Digital Asset Added");
                productAdmin.Update(product);
               
                string Associatename = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "AcitivityLogAddDigitalAsset").ToString(), txtDigitalAsset.Text, product.Name);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, Associatename, product.Name);

                Response.Redirect("~/FranchiseAdmin/Secure/Inventory/Products/View.aspx?mode=" + HttpUtility.UrlEncode(this.encrypt.EncryptData("digitalAsset")) + "&itemid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAddDigitalAsset").ToString();
                return;
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Inventory/Products/View.aspx?mode=" + HttpUtility.UrlEncode(this.encrypt.EncryptData("digitalAsset")) + "&itemid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }
        #endregion        
    }
}