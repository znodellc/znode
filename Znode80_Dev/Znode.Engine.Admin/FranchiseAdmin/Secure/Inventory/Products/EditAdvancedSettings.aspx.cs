using System;
using System.Web;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.FranchiseAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_edit_advancedsettings class.
    /// </summary>
    public partial class AdvancedSettings : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private string managePageLink;
        private string cancelPageLink;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.managePageLink = "~/FranchiseAdmin/Secure/Inventory/Products/view.aspx?mode=" + HttpUtility.UrlEncode(this.encrypt.EncryptData("advanced")) + "&itemid=";
            this.cancelPageLink = "~/FranchiseAdmin/Secure/Inventory/Products/view.aspx?mode=" + HttpUtility.UrlEncode(this.encrypt.EncryptData("advanced")) + "&itemid=";

            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }

            if (!Page.IsPostBack)
            {


                if (this.itemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleProductEditSettings").ToString();

                    // Bind Details
                    this.Bind();
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click Event - Fires when Submit button is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            string mappedSEOUrl = string.Empty;

            // If edit mode then get all the values first
            if (this.itemId > 0)
            {
                product = productAdmin.GetByProductId(this.itemId);

                if (product.SEOURL != null)
                {
                    mappedSEOUrl = product.SEOURL;
                }
            }

            // Display Settings
            product.ActiveInd = CheckEnabled.Checked;
            product.HomepageSpecial = CheckHomeSpecial.Checked;
            product.InventoryDisplay = Convert.ToByte(false);
            product.CallForPricing = CheckCallPricing.Checked;
            product.NewProductInd = CheckNewItem.Checked;
            product.FeaturedInd = ChkFeaturedProduct.Checked;
            product.Franchisable = CheckFranchisable.Checked;

            // Set properties
            product.SEOTitle = Server.HtmlEncode(txtSEOTitle.Text.Trim());
            product.SEOKeywords = Server.HtmlEncode(txtSEOMetaKeywords.Text.Trim());
            product.SEODescription = Server.HtmlEncode(txtSEOMetaDescription.Text.Trim());
            product.SEOURL = null;

            if (txtSEOUrl.Text.Trim().Length > 0)
            {
                product.SEOURL = txtSEOUrl.Text.Trim().Replace(" ", "-");
            }

            if (string.Compare(product.SEOURL, mappedSEOUrl, true) != 0)
            {
                if (urlRedirectAdmin.SeoUrlExists(product.SEOURL, product.ProductID))
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSEOFriendlyURLAlreadyExist").ToString();
                    return;
                }
            }

            if (!CheckFranchisable.Checked)
            {
                bool isRemoved = productCategoryAdmin.RemoveProductCategory(product.ProductID);
                if (isRemoved)
                {
                    string associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeletedfranchisable").ToString() + product.Name;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, product.Name);
                }
            }

            // Inventory Setting - Out of Stock Options
            if (InvSettingRadiobtnList.SelectedValue.Equals("1"))
            {
                // Only Sell if Inventory Available - Set values
                product.TrackInventoryInd = true;
                product.AllowBackOrder = false;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("2"))
            {
                // Allow Back Order - Set values
                product.TrackInventoryInd = true;
                product.AllowBackOrder = true;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("3"))
            {
                // Don't Track Inventory - Set property values
                product.TrackInventoryInd = false;
                product.AllowBackOrder = false;
            }

            // Inventory Setting - Stock Messages
            if (txtOutofStock.Text.Trim().Length == 0)
            {
                product.OutOfStockMsg = this.GetGlobalResourceObject("ZnodeAdminResource", "ValueOutOfStock").ToString(); 
            }
            else
            {
                product.OutOfStockMsg = Server.HtmlEncode(txtOutofStock.Text.Trim());
            }

            product.InStockMsg = Server.HtmlEncode(txtInStockMsg.Text.Trim());
            product.BackOrderMsg = Server.HtmlEncode(txtBackOrderMsg.Text.Trim());
            product.DropShipInd = chkDropShip.Checked;

            // Recurring Billing settings
            if (chkRecurringBillingInd.Checked)
            {
                product.RecurringBillingInitialAmount = Convert.ToDecimal(txtRecurringBillingInitialAmount.Text);
                product.RecurringBillingInd = chkRecurringBillingInd.Checked;
                product.RecurringBillingPeriod = ddlBillingPeriods.SelectedItem.Value;
                product.RecurringBillingFrequency = "1";
                product.RecurringBillingTotalCycles = 0;
            }
            else
            {
                product.RecurringBillingInd = false;
                product.RecurringBillingInstallmentInd = false;
            }

            bool isUpdated = false;
            try
            {
                if (this.itemId > 0)
                {
                    product.ReviewStateID = productAdmin.UpdateReviewStateID(product, UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId);

                    isUpdated = productAdmin.Update(product);
                }

                if (isUpdated)
                {
                    urlRedirectAdmin.UpdateUrlRedirect(mappedSEOUrl);

                    string associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditAdvanceSettings").ToString() + product.Name;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, product.Name);

                    Response.Redirect(this.managePageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
                }
                else
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnabletoUpdateAdvanceSettings").ToString();
                }
            }
            catch (Exception)
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnabletoUpdateAdvanceSettings").ToString();
                return;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.cancelPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Occurs whenn recurring billing checkbox selected.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChkRecurringBillingInd_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRecurringBillingInd.Checked)
            {
                pnlRecurringBilling.Visible = true;
            }
            else
            {
                pnlRecurringBilling.Visible = false;
            }
        }

        /// <summary>
        /// Occurs when billing period dropdownlist item selected.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlBillingPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        #endregion

        #region Helper Methods

        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind the products advanced settings.
        /// </summary>
        private void Bind()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin productAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = productAdmin.GetByProductId(this.itemId);
            UserStoreAccess.CheckStoreAccess(product.PortalID.GetValueOrDefault(0), true);

            if (product != null)
            {
                // Display Settings                                             
                CheckEnabled.Checked = product.ActiveInd;
                CheckHomeSpecial.Checked = product.HomepageSpecial;
                CheckCallPricing.Checked = product.CallForPricing;
                ChkFeaturedProduct.Checked = product.FeaturedInd;

                if (product.NewProductInd.HasValue)
                {
                    CheckNewItem.Checked = product.NewProductInd.Value;
                }

                // Inventory Setting - Out of Stock Options
                if (product.AllowBackOrder.HasValue && product.TrackInventoryInd.HasValue)
                {
                    if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value == false)
                    {
                        InvSettingRadiobtnList.Items[0].Selected = true;
                    }
                    else if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value)
                    {
                        InvSettingRadiobtnList.Items[1].Selected = true;
                    }
                    else if ((product.TrackInventoryInd.Value == false) && (product.AllowBackOrder.Value == false))
                    {
                        InvSettingRadiobtnList.Items[2].Selected = true;
                    }
                }
				else
				{
					InvSettingRadiobtnList.Items[2].Selected = true;
				}

                // Inventory Setting - Stock Messages
                txtInStockMsg.Text = Server.HtmlDecode(product.InStockMsg);
                txtOutofStock.Text = Server.HtmlDecode(product.OutOfStockMsg);
                txtBackOrderMsg.Text = Server.HtmlDecode(product.BackOrderMsg);

                if (product.DropShipInd.HasValue)
                {
                    chkDropShip.Checked = product.DropShipInd.Value;
                }

                lblTitle.Text += "\"" + product.Name + "\"";

                CheckFranchisable.Checked = product.Franchisable;

                // Recurring Billing
                chkRecurringBillingInd.Checked = product.RecurringBillingInd;
                pnlRecurringBilling.Visible = product.RecurringBillingInd;
                ddlBillingPeriods.SelectedValue = product.RecurringBillingPeriod;
                if (product.RecurringBillingInitialAmount.HasValue)
                    txtRecurringBillingInitialAmount.Text = product.RecurringBillingInitialAmount.Value.ToString("N");

                // SEO Settings
                txtSEOTitle.Text = Server.HtmlDecode(product.SEOTitle);
                txtSEOMetaKeywords.Text = Server.HtmlDecode(product.SEOKeywords);
                txtSEOMetaDescription.Text = Server.HtmlDecode(product.SEODescription);
                txtSEOUrl.Text = product.SEOURL;
            }
        }
        #endregion
    }
}