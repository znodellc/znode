using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_products_list class.
    /// </summary>
    public partial class ProductsList : System.Web.UI.Page
    {
        #region Private Variables
        private string addLink = "add.aspx";
        private string previewLink = string.Empty;
        private string deleteLink = "delete.aspx?itemid=";
        private string detailsLink = "~/FranchiseAdmin/Secure/Inventory/Products/view.aspx?itemid=";        
        private string portalIds = string.Empty;
        #endregion

        #region Helper Functions

        /// <summary>
        /// Format the Price of a product and return in string format
        /// </summary>
        /// <param name="price">Product price value.</param>
        /// <returns>Returns the products formatted price.</returns>
        public string DisplayPrice(object price)
        {
            if (price == DBNull.Value)
            {
                return "$0.00";
            }
            else
            {
                if (Convert.ToInt32(price) == 0)
                {
                    return "$0.00";
                }
                else
                {
                    return String.Format("${0:#.00}", price);
                }
            }
        }

        /// <summary>
        /// Get ImagePath
        /// </summary>
        /// <param name="Imagefile">Image file name.</param>
        /// <returns>Returns the image file name with path.</returns>
        public string GetImagePath(string Imagefile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(Imagefile);
        }

        #endregion

        #region FormatPrice Helper method
        /// <summary>
        /// Returns formatted price to the grid
        /// </summary>
        /// <param name="productPrice">Product price to format.</param>
        /// <returns>Returns the formatted product price.</returns>
        public string FormatPrice(object productPrice)
        {
            if (productPrice != null)
            {
                if (productPrice.ToString().Length > 0)
                {
                    return decimal.Parse(productPrice.ToString()).ToString("c");
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// To get the encrypted productId in the grid
        /// </summary>
        /// <param name="objectId">ObjectId to encrypt.</param>
        /// <returns>Returns the encrypted product Id.</returns>
        public string GetEncryptID(object objectId)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            return HttpUtility.UrlEncode(encrypt.EncryptData(objectId.ToString()));
        }

        /// <summary>
        /// Button visibility
        /// </summary>
        /// <param name="productId">Product Id to set the visibility of the product.</param>
        /// <returns>Returns false if franchise product else true.</returns>
        public bool IsFranchiseProduct(object productId)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();
            product = productAdmin.GetByProductId(Convert.ToInt32(productId));
            if ((product.PortalID == null || product.Franchisable == true) && product.PortalID != UserStoreAccess.GetTurnkeyStorePortalID)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// Get the product view page link with encrypted product Id
        /// </summary>
        /// <param name="productId">Product Id to disable the name link</param>
        /// <returns>Returns the product view page link.</returns>
        public string GetViewPageLink(object productId)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();
            product = productAdmin.GetByProductId(Convert.ToInt32(productId));

            return "view.aspx?itemid=" + this.GetEncryptID(productId);
        }

        /// <summary>
        /// Get the hyperlink text for product name.
        /// </summary>
        /// <param name="productId">Product Id to generate heperlink.</param>
        /// <returns>Returns the product name hyperlink.</returns>
        public string GetProductNameLink(object productId)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();
            product = productAdmin.GetByProductId(Convert.ToInt32(productId));
            if (product.PortalID == null)
            {
                return product.Name.ToString();
            }
            else
            {
                return "<a href='" + this.GetProductNameLink(productId) + "'>" + product.Name.ToString() + "</a>";
            }
        }

        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            this.previewLink = "http://" + UserStoreAccess.DomainName + "/product.aspx";
            this.portalIds = UserStoreAccess.GetAvailablePortals;

            if (!Page.IsPostBack)
            {
                this.BindDropDownData();
                this.BindSearchProduct();
                ddlproductseller.Items.Add(HttpContext.Current.User.Identity.Name);
            }

            txtCategory.ContextKey = ddlCatalog.SelectedValue;
        }
        #endregion

        #region Grid Events

        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchProduct();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                string Id = e.CommandArgument.ToString();
                ZNodeEncryption encrypt = new ZNodeEncryption();

                if (e.CommandName == "Manage")
                {
                    Response.Redirect(this.detailsLink + HttpUtility.UrlEncode(encrypt.EncryptData(Id)));
                } 
                else if (e.CommandName == "Copy")
                {
                    int copyProductId = 0;
                    ProductAdmin admin = new ProductAdmin();
                    copyProductId = admin.CopyProduct(Convert.ToInt32(e.CommandArgument));
                    Response.Redirect(this.detailsLink + HttpUtility.UrlEncode(encrypt.EncryptData(copyProductId.ToString())));
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.deleteLink + HttpUtility.UrlEncode(encrypt.EncryptData(Id)));
                }
            }
        }

        #endregion

        #region General Events
        /// <summary>
        /// AddNew Product button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddProduct_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addLink);
        }

        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchProduct();
        }

        /// <summary>
        /// Clear button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtproductname.Text = string.Empty;
            txtproductnumber.Text = string.Empty;
            txtsku.Text = string.Empty;
            txtCategory.Text = string.Empty;
            txtCategory.Value = "0";
            txtProductStatus.Value = "0";
            ddlproductseller.SelectedIndex = 0;
            this.BindDropDownData();
            this.BindSearchProduct();
        }

        /// <summary>
        /// Gets the review state icon name
        /// </summary>
        /// <param name="reviewStateId">Product review state Id</param>
        /// <returns>Returns the review state icon path</returns>
        protected string GetStausMark(string reviewStateId)
        {
            string imageSrc = "~/FranchiseAdmin/Themes/Images/223-point_pendingapprove.gif";

            if (reviewStateId != string.Empty)
            {
                int stateId = Convert.ToInt32(reviewStateId);

                if (stateId == Convert.ToInt32(ZNodeProductReviewState.Approved))
                {
                    imageSrc = "~/FranchiseAdmin/Themes/Images/222-point_approve.gif";
                }

                if (stateId == Convert.ToInt32(ZNodeProductReviewState.Declined))
                {
                    imageSrc = "~/FranchiseAdmin/Themes/Images/221-point_decline.gif";
                }

                if (stateId == Convert.ToInt32(ZNodeProductReviewState.ToEdit))
                {
                    imageSrc = "~/FranchiseAdmin/Themes/Images/220-point_toedit.gif";
                }
            }

            return imageSrc;
        }

        #endregion

        #region Bind Grid
        /// <summary>
        /// Bind data to the drop down list
        /// </summary>
        private void BindDropDownData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Catalog> catalogList = catalogAdmin.GetAllCatalogs();
            DataSet ds = catalogList.ToDataSet(false);
            DataView dataView = UserStoreAccess.CheckStoreAccess(ds.Tables[0]).DefaultView;
            ddlCatalog.DataSource = dataView;
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();
            ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), "0");
            ddlCatalog.Items.Insert(0, li);

            foreach (ListItem item in ddlCatalog.Items)
            {
                item.Text = Server.HtmlDecode(item.Text);
            }
        }

        #endregion

        #region Search Product
        /// <summary>
        /// Search a Product by name,productnumber,sku,manufacturer,category,producttype
        /// </summary>
        private void BindSearchProduct()
        {
            int portalId;
            if (ddlproductseller.SelectedValue == "0")
            {
                portalId = 0;
            }
            else
            {
                portalId = UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0);
            }

            ProductAdmin prodadmin = new ProductAdmin();
            DataSet ds = prodadmin.SearchFranchiseProduct(Server.HtmlEncode(txtproductname.Text.Trim()), txtproductnumber.Text.Trim(), txtsku.Text.Trim(), string.Empty, string.Empty, txtCategory.Value, "0", "0", portalId);
            DataView dv = new DataView(UserStoreAccess.CheckProductAccess(ds.Tables[0].DefaultView.ToTable(), true));

            dv.Sort = "DisplayOrder";
            if (!string.IsNullOrEmpty(txtProductStatus.Value))
            {
                dv.RowFilter = "ReviewStateID = " + txtProductStatus.Value;
            }
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }
        #endregion
    }
}