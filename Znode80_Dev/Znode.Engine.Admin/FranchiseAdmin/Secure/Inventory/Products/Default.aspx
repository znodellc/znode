<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Inventory.Products.ProductsList" ValidateRequest="false" Codebehind="Default.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/FranchiseAdmin/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ManufacturerAutoComplete" Src="~/FranchiseAdmin/Controls/Default/ManufacturerAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ProductTypeAutoComplete" Src="~/FranchiseAdmin/Controls/Default/ProductTypeAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ProductReviewStateAutoComplete" Src="~/FranchiseAdmin/Controls/Default/ProductReviewStateAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ReviewStateLegend" Src="~/FranchiseAdmin/Controls/Default/ReviewStateLegend.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">    
    <div>
        <div>
            <div class="LeftFloat" style="width: 50%">
                <h1>
                  <asp:Localize ID="LinkTextVendorProducts" runat ="server" Text='<%$ Resources:ZnodeAdminResource, TitleProducts %>'></asp:Localize>
                </h1>
            </div>
            <div class="ButtonStyle">
            <zn:LinkButton ID="btnAddProduct" runat="server" CausesValidation="False"
            ButtonType="Button" OnClick="BtnAddProduct_Click" Text='<%$ Resources:ZnodeAdminResource, LinkButtonAddNewProduct %>'
            ButtonPriority="Primary"/>
            </div>
        </div>
        <div class="ClearBoth" align="left">
            <p>
                <asp:Localize ID="VendorProducts" runat ="server" Text='<%$ Resources:ZnodeAdminResource, TextProducts %>'></asp:Localize></p>
            <br />
        </div>
        <h4 class="SubTitle">
           <asp:Localize ID="SearchVendorProducts" runat ="server" Text='<%$ Resources:ZnodeAdminResource, TitleSearchProducts %>'></asp:Localize></h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="ProductName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductname" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="ProductNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductsNumber %>' ></asp:Localize> </span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductnumber" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="eProductsSKU" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductsSKU %>' ></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtsku" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle" style="display:none">
                        <span class="FieldStyle"><asp:Localize ID="Catalog" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCatalog %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                             <asp:DropDownList ID="ddlCatalog" runat="server" AutoPostBack="true"></asp:DropDownList></span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="ProductCategory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductCategory %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <ZNode:CategoryAutoComplete ID="txtCategory" runat="server" />
                        </span>
                    </div>
                     <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductSeller %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                             <asp:DropDownList ID="ddlproductseller" runat="server">
                             <asp:ListItem Value="0" Text='<%$ Resources:ZnodeAdminResource, DropdownTextAll %>'> </asp:ListItem>
                             </asp:DropDownList></span>
                    </div>
                      <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="ColumnTitleProductStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductStatus %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <ZNode:ProductReviewStateAutoComplete ID="txtProductStatus" runat="server" />
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <br />
                </div>
                <div> 
                     <zn:Button ID="btnClear" runat="server" ButtonType="CancelButton" CausesValidation="false" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' />
                     <zn:Button ID="btnSearch" runat="server" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                 </div>
            </div>
        </asp:Panel>
        <br />
        <div class="GridTitle"></div>
        <div class="ItemStyle">
            <div style="float:left;padding-top:3px;"><h4 class="Title">
              <asp:Localize ID="ProductList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleProductList %>'></asp:Localize></h4></div>
             <div style="float: right;">
                <ZNode:ReviewStateLegend ID="ReviewStateLegend1" runat="server" />
            </div> 
        </div> 
        <div style="clear:both;"></div>    
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
            GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
            OnRowCommand="UxGrid_RowCommand" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyText %>' 
            Width="100%" CellPadding="4">
            <Columns>
                <asp:BoundField DataField="productid" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, GridColumnTitleImage %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='<%# GetViewPageLink(DataBinder.Eval(Container.DataItem, "productid"))%>'
                            id="LinkView">
                            <img alt=" " src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                                runat="server" style="border: none" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                      <a href='<%# GetViewPageLink(DataBinder.Eval(Container.DataItem, "productid"))%>'><%# Eval("name")%></a>
                    </ItemTemplate>
                </asp:TemplateField>
              
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnRetailPrice %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# FormatPrice(DataBinder.Eval(Container.DataItem,"RetailPrice")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSalePrice %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# FormatPrice(DataBinder.Eval(Container.DataItem,"SalePrice")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleWholesalePrice %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# FormatPrice(DataBinder.Eval(Container.DataItem,"WholesalePrice")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleInStock %>' DataField="Quantityonhand" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder %>'  DataField="displayorder" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <img alt="" id="Img1" src='<%# GetStausMark(DataBinder.Eval(Container.DataItem, "ReviewStateId").ToString())%>'
                            runat="server" />
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Center" />
                </asp:TemplateField>
              
                <asp:TemplateField ItemStyle-Wrap="false" ItemStyle-Width="150px">
                    <ItemTemplate>
                        <asp:LinkButton ID="Button1" CommandName="Manage" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"productid")%>'
                            Text='<%$ Resources:ZnodeAdminResource,LinkManage %>' runat="server" CssClass="actionlink" Visible='<%# IsFranchiseProduct(DataBinder.Eval(Container.DataItem,"productid"))%>' />
                        <asp:LinkButton ID="LinkButton1" CommandName="Copy" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"productid")%>'
                            Text='<%$ Resources:ZnodeAdminResource,LinkCopy %>' runat="server" CssClass="actionlink" Visible='<%# IsFranchiseProduct(DataBinder.Eval(Container.DataItem,"productid"))%>' />                                 
                        &nbsp;<asp:LinkButton ID="Button5" CommandName="Delete" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"productid")%>'
                            Text='<%$ Resources:ZnodeAdminResource,LinkDelete %>'  runat="server" CssClass="actionlink" Visible='<%# IsFranchiseProduct(DataBinder.Eval(Container.DataItem,"productid"))%>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
    </div>
</asp:Content>
