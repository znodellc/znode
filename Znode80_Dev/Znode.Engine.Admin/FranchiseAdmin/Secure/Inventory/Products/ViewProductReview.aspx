﻿<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="true" Inherits="Znode.Engine.FranchiseAdmin.Secure.Inventory.Products.ViewProductReview"
    Title="Untitled Page" Codebehind="ViewProductReview.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="clear: both">
        </div>
        <div class="FormView">
            <div>
                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
            </div>
            <div class="FieldStyle">
                  <asp:Localize ID="ProductName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductName %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblProductName" runat="server"></asp:Label>
            </div>
            <div class="FieldStyle">
               <asp:Localize ID="Vendor" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleVendor %>'></asp:Localize>
            </div>
            <br />
            <div class="ValueStyle">
                <asp:Label ID="lblVendor" runat="server"></asp:Label>
            </div>
            <div class="FieldStyle">
               <asp:Localize ID="Date" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDate %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblDateModified" runat="server"></asp:Label>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="EditedBy" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEditedBy %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblUserName" runat="server"></asp:Label>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="Status" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStatus %>'></asp:Localize>
            </div>            
            <div class="ValueStyle">
                <asp:Label ID="lblStatus" runat="server"></asp:Label>
            </div>
            <br />
            <div class="FieldStyle">
                <asp:Localize ID="RejectionReason" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleRejectionReason %>'></asp:Localize>
            </div>            
            <div class="ValueStyle">
                <asp:Label ID="lblReason" runat="server"></asp:Label>
            </div>
            <br />
            <div class="FieldStyle">
             <asp:Localize ID="RejectionDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRejectionDescription %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblDescription" runat="server"></asp:Label>
            </div>
            <div class="ClearBoth">
            </div>            
            <div>
                 <zn:Button runat="server" Width="100px" ID="btnCancel" OnClick="BtnCancel_Click" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonBackToSearch %>'/>
            </div>
        </div>
    </div>
</asp:Content>
