﻿using System;
using System.Linq;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.FranchiseAdmin.Secure.Inventory.Products
{
	/// <summary>
	/// Represents the Znode.Engine.Admin.FranchiseAdmin.Secure.Inventory.Products - ProductTags user control class
	/// </summary>
	public partial class ProductTags : System.Web.UI.UserControl
	{
		#region Protected Variables
		private int ItemId;
		private ZNodeEncryption encrypt = new ZNodeEncryption();
		#endregion

		#region Page Load Events
		protected void Page_Load(object sender, EventArgs e)
		{
			// Get ItemId from querystring        
			if (Request.Params["itemid"] != null)
			{
				this.ItemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"]));
			}
			else
			{
				this.ItemId = 0;
			}
			if (!Page.IsPostBack)
			{
				this.BindTags();
			}
		}
		#endregion

		#region General Events
		/// <summary>
		/// Event is raised when update button is clicked
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void Update_Click(object sender, EventArgs e)
		{
			TagsService tagService = new TagsService();
			Tags tagEntity = new Tags();
			bool check = false;
			tagEntity.Tags = Server.HtmlEncode(txtTagNames.Text.Trim());
			tagEntity.ProductID = this.ItemId;
			TList<ZNode.Libraries.DataAccess.Entities.Tags> tagList = tagService.GetByProductID(this.ItemId);
			if (tagList.Count > 0)
			{
				if (string.IsNullOrEmpty(txtTagNames.Text))
				{
					//For Deleting the Tags 
					tagEntity.TagID = tagList.FirstOrDefault().TagID;
					check = tagService.Delete(tagEntity);
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorTagsremovedsuccessfully").ToString();
				}
				else
				{
					//For Updating the Tags 
					tagEntity.TagID = tagList.FirstOrDefault().TagID;
					check = tagService.Update(tagEntity);
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorTagUpddatedSuccessfully").ToString();
				}
			}
			else
			{
				if (!string.IsNullOrEmpty(txtTagNames.Text))
				{	//For Adding The Tags
					check = tagService.Insert(tagEntity);
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorTagAddedSuccessfully").ToString();
				}
				else
				{
					//For no Tags Entered
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNotagshasEntered").ToString();
				}
			}
			if (check)
			{
				ProductAdmin prodAdmin = new ProductAdmin();
				Product product = prodAdmin.GetByProductId(this.ItemId);
			    string associateTagName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddTag").ToString(), txtTagNames.Text, product.Name);//"Added Tag " + txtTagNames.Text + " - " + product.Name;
				ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateTagName, product.Name);
			}
		}

		/// <summary>
		/// Event is raised when cancel button is clicked
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void Cancel_Click(object sender, EventArgs e)
		{
			lblMsg.Visible = false;
		}
		#endregion

		#region Private methods
		/// <summary>
		/// Bind Tags for this product
		/// </summary>
		private void BindTags()
		{
			TagsService getTag = new TagsService();
			TList<ZNode.Libraries.DataAccess.Entities.Tags> tagList = getTag.GetByProductID(this.ItemId);
			if (tagList.Count > 0)
			{
				string delimeter = ",";
				txtTagNames.Text = Server.HtmlDecode(tagList.Select(i => i.Tags).Aggregate((i, j) => i + delimeter + j));
			}
		}
		#endregion

	}
}