﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" CodeBehind="AddDigitalAsset.aspx.cs" Inherits="Znode.Engine.FranchiseAdmin.Secure.Inventory.Products.AddDigitalAsset" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/FranchiseAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" Text='<%$ Resources:ZnodeAdminResource, TitleAddDigitalAsset%>' runat="server"></asp:Label></h1>
        </div>
        <div class="ClearBoth">
            <div class="HintStyle" style="display:none;">
                <asp:Localize ID="TextAddDigitalAsset" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextAddDigitalAsset%>'></asp:Localize></div>
        </div>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div>
            <asp:Label ID="lblMsg" CssClass="Error" runat="server"></asp:Label></div>
        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="FieldStyle">
           <asp:Localize ID="ColumnDigitalAsset" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDigitalAsset%>'></asp:Localize><span class="Asterix">*</span><br />
            <div>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDigitalAsset"
                    Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDigitalAsset%>' CssClass="Error"
                    SetFocusOnError="True"></asp:RequiredFieldValidator></div>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtDigitalAsset" runat="server" Columns="50" TextMode="MultiLine"
                Rows="12"></asp:TextBox>
            <div>
                <uc1:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
        </div>
        <div class="ClearBoth"></div>
        <div>
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" ID="btnSubmitBottom" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelBottom" />
        </div>
    </div>
</asp:Content>
