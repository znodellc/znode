using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.FranchiseAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_products_add class.
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Private Member Variables
        private string cancelLink = "view.aspx?itemid=";
        private string listLink = "default.aspx";
        private string ProductImageName = string.Empty;
        private int itemId;
        private int skuId = 0;
        private int productId = 0;
        private StringBuilder selectedNodes = null;
        private StringBuilder deleteNodes = null;
        private int productTypeId = 0;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        private bool isSKUUpdated;
        #endregion

        #region Page load
        protected void Page_Load(object sender, EventArgs e)
        {
            this.selectedNodes = new StringBuilder();
            this.deleteNodes = new StringBuilder();

            // Get product id value from query string
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));

                this.productId = this.itemId;
            }
            else
            {
                this.itemId = 0;
            }

            if (!Page.IsPostBack)
            {
                // Dynamic error message for weight field compare validator
                WeightFieldCompareValidator.ErrorMessage = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorEnterValidProduct").ToString();

                // Bind shipping options
                this.BindShippingTypes();

                // If edit func then bind the data fields
                if (this.itemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditProduct").ToString();
                    this.BindData();
                    this.BindEditData();
                    tblShowImage.Visible = true;
                    pnlShowImage.Visible = true;
                    tblProductDescription.Visible = false;
                    pnlUploadSection.Attributes["style"] += "border-left: solid 1px #cccccc;padding-left: 10px; ";
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddProduct").ToString();
                    this.BindData();
                    tblShowImage.Visible = true;
                    pnlShowImage.Visible = false;
                    pnlImage.Visible = false;
                    tblProductDescription.Visible = true;
                    DivAttributes.Visible = false;
                }
            }

            // Bind SKU Attributes Dynamically
            if (!string.IsNullOrEmpty(ProductTypeList.Value))
            {
                this.Bind(int.Parse(ProductTypeList.Value));

                if (this.itemId > 0)
                {
                    // Bind Edit SKU Attributes
                    this.BindAttributes(int.Parse(ProductTypeList.Value));
                }
            }
            if (chkFreeShippingInd.Checked == true)
            {
                this.dvShipSeparate.Style.Add("display", "none");
            }
            else
            {
                this.dvShipSeparate.Style.Add("display", "block");
            }
        }

        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            SKUAdmin skuAdminAccess = new SKUAdmin();
            ProductAdmin productAdmin = new ProductAdmin();
            SKUAttribute skuAttribute = new SKUAttribute();
            SKU sku = new SKU();
            ZNode.Libraries.DataAccess.Entities.ProductCategory productCategory = new ZNode.Libraries.DataAccess.Entities.ProductCategory();
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();
            System.IO.FileInfo fileInfo = null;
            System.IO.FileInfo skufileInfo = null;

            if (string.IsNullOrEmpty(ProductTypeList.Value))
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextInformSiteAdmin").ToString();
                return;
            }

            bool isSuccess = false;

            // Passing Values 
            product.ProductID = this.itemId;

            // if edit mode then get all the values first
            if (this.itemId > 0)
            {
                product = productAdmin.GetByProductId(this.itemId);

                if (ViewState["productSkuId"] != null)
                {
                    sku.SKUID = int.Parse(ViewState["productSkuId"].ToString());
                }

                bool isSkuExist = skuAdminAccess.IsSkuExist(txtProductSKU.Text.Trim(), this.itemId);

                if (isSkuExist)
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSKUorPart").ToString();
                    return;
                }
            }
            else
            {
                bool isSkuExist = skuAdminAccess.IsSkuExist(txtProductSKU.Text.Trim(), this.itemId);

                if (isSkuExist)
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSKUorPart").ToString();
                    return;
                }
            }

            // General Info
            product.Name = Server.HtmlEncode(txtProductName.Text);
            product.ImageFile = txtimagename.Text;
            product.ProductNum = Server.HtmlEncode(txtProductNum.Text);

            product.PortalID = UserStoreAccess.GetTurnkeyStorePortalID;

            product.ProductTypeID = int.Parse(ProductTypeList.Value);

            // MANUFACTURER
            product.ManufacturerID = null;

            // Always it is NULL            
            product.SupplierID = null;
            product.DownloadLink = Server.HtmlEncode(txtDownloadLink.Text.Trim());
            product.ShortDescription = Server.HtmlEncode(txtshortdescription.Text);
            product.Description = ctrlHtmlText.Html;
            product.RetailPrice = Convert.ToDecimal(txtproductRetailPrice.Text);

            product.FeaturesDesc = ctrlHtmlPrdFeatures.Html.Trim();
            product.Specifications = ctrlHtmlPrdSpec.Html.Trim();
            product.AdditionalInformation = CtrlHtmlProdInfo.Html.Trim();

            if (txtproductSalePrice.Text.Trim().Length > 0)
            {
                product.SalePrice = Convert.ToDecimal(txtproductSalePrice.Text.Trim());
            }
            else
            {
                product.SalePrice = null;
            }

            if (txtProductWholeSalePrice.Text.Trim().Length > 0)
            {
                product.WholesalePrice = Convert.ToDecimal(txtProductWholeSalePrice.Text.Trim());
            }
            else
            {
                product.WholesalePrice = null;
            }

            // Quantity Available
            int quantity;
            int reorder;
            int.TryParse(txtProductQuantity.Text, out quantity);
            int.TryParse(txtReOrder.Text, out reorder);


            if (!string.IsNullOrEmpty(txtMinQuantity.Text.Trim()))
            {
                product.MinQty = Convert.ToInt32(txtMinQuantity.Text);
            }
            else
            {
                product.MinQty = 1;
            }

            if (!string.IsNullOrEmpty(txtMaxQuantity.Text.Trim()))
            {
                product.MaxQty = Convert.ToInt32(txtMaxQuantity.Text);
            }
            else
            {
                product.MaxQty = 10;
            }

            // Display Settings
            product.DisplayOrder = int.Parse(txtDisplayOrder.Text.Trim());

            // Tax Settings        
            if (ddlTaxClass.SelectedIndex != -1)
            {
                product.TaxClassID = int.Parse(ddlTaxClass.SelectedValue);
            }

            // Shipping Option setting
            product.ShippingRuleTypeID = Convert.ToInt32(ShippingTypeList.SelectedValue);
            product.FreeShippingInd = chkFreeShippingInd.Checked;
            product.ShipSeparately = chkShipSeparately.Checked;

            if (txtProductWeight.Text.Trim().Length > 0)
            {
                product.Weight = Convert.ToDecimal(txtProductWeight.Text.Trim());
            }
            else
            {
                product.Weight = null;
            }

            // Product Height - Which will be used to determine the shipping cost
            if (txtProductHeight.Text.Trim().Length > 0)
            {
                product.Height = decimal.Parse(txtProductHeight.Text.Trim());
            }
            else
            {
                product.Height = null;
            }

            if (txtProductWidth.Text.Trim().Length > 0)
            {
                product.Width = decimal.Parse(txtProductWidth.Text.Trim());
            }
            else
            {
                product.Width = null;
            }

            if (txtProductLength.Text.Trim().Length > 0)
            {
                product.Length = decimal.Parse(txtProductLength.Text.Trim());
            }
            else
            {
                product.Length = null;
            }

            if (product.ShippingRuleTypeID != (int)Znode.Engine.Shipping.ZnodeShippingRuleType.FixedRatePerItem)
            {
                product.ShippingRate = null;
            }
            else
            {
                // Shipping Rate
                decimal shippingRate = 0;

                decimal.TryParse(txtShippingRate.Text, out shippingRate);

                product.ShippingRate = shippingRate;
            }

            // Stock
            sku.ProductID = this.itemId;
            sku.SKU = txtProductSKU.Text.Trim();
            sku.ActiveInd = true;

            if (txtReOrder.Text.Trim().Length > 0)
            {
                // Update Quantity SKU
                isSKUUpdated = SKUAdmin.UpdateQuantity(sku, Convert.ToInt32(txtProductQuantity.Text), Convert.ToInt32(txtReOrder.Text.Trim()));
            }
            else
            {
                isSKUUpdated = SKUAdmin.UpdateQuantity(sku, Convert.ToInt32(txtProductQuantity.Text));
            }


            product.ImageAltTag = txtImageAltTag.Text.Trim();

            // Database & Image Updates

            DataSet MyAttributeTypeDataSet = productAdmin.GetAttributeTypeByProductTypeID(int.Parse(product.ProductTypeID.ToString()));

            // Create transaction
            TransactionManager tranManager = ConnectionScope.CreateTransaction();

            try
            {

                // Product update.
                if (this.itemId > 0)
                {
                    // Upload File if this is a new product or the New Image option was selected for an existing product
                    if (RadioProductNewImage.Checked)
                    {
                        if (uxProductImage.PostedFile != null && !string.IsNullOrEmpty(uxProductImage.PostedFile.FileName))
                        {
                            uxProductImage.AppendToFileName = "-" + this.productId.ToString();
                            isSuccess = uxProductImage.SaveImage("fadmin", UserStoreAccess.GetTurnkeyStorePortalID.Value.ToString());
                            fileInfo = uxProductImage.FileInformation;
                            skufileInfo = uxProductImage.FileInformation;
                            if (!isSuccess)
                            {
                                if (uxProductImage.FileInformation.Name != string.Empty)
                                {
                                    product.ImageFile = "Turnkey/" + UserStoreAccess.GetTurnkeyStorePortalID.Value + "/" + uxProductImage.FileInformation.Name;
                                    sku.SKUPicturePath = "Turnkey/" + UserStoreAccess.GetTurnkeyStorePortalID.Value + "/" + skufileInfo.Name;
                                }

                                return;
                            }
                            else
                            {
                                product.ImageFile = "Turnkey/" + UserStoreAccess.GetTurnkeyStorePortalID.Value + "/" + uxProductImage.FileInformation.Name;
                                sku.SKUPicturePath = "Turnkey/" + UserStoreAccess.GetTurnkeyStorePortalID.Value + "/" + skufileInfo.Name;
                            }
                        }
                    }

                    // Update product Sku and Product values
                    // If ProductType has SKU's
                    if (MyAttributeTypeDataSet.Tables[0].Rows.Count > 0)
                    {
                        // For this product already SKU if on exists
                        if (sku.SKUID > 0)
                        {
                            sku.UpdateDte = System.DateTime.Now;

                            // Check whether Duplicate attributes is created
                            string Attributes = String.Empty;

                            DataSet MyAttributeTypeDataSet1 = productAdmin.GetAttributeTypeByProductTypeID(int.Parse(product.ProductTypeID.ToString()));

                            foreach (DataRow MyDataRow in MyAttributeTypeDataSet1.Tables[0].Rows)
                            {
                                System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                                if (lstControl != null)
                                {
                                    int selValue = int.Parse(lstControl.SelectedValue);

                                    Attributes += selValue.ToString() + ",";
                                }
                            }

                            // Split the string
                            string Attribute = Attributes.Substring(0, Attributes.Length - 1);

                            // To check SKU combination is already exists.
                            bool RetValue = skuAdminAccess.CheckSKUAttributes(this.itemId, sku.SKUID, Attribute);

                            if (!RetValue)
                            {

                                product.ReviewStateID = productAdmin.UpdateReviewStateID(product, UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId);

                                // Set update date
                                product.UpdateDte = System.DateTime.Now;

                                // Then Update the database with new property values
                                isSuccess = productAdmin.Update(product, sku);
                            }
                            else
                            {
                                // Throw error if duplicate attribute
                                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAttributeExist").ToString();
                                return;
                            }
                        }
                        else
                        {
                            product.ReviewStateID = productAdmin.UpdateReviewStateID(product, UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId);

                            // Set update date
                            product.UpdateDte = System.DateTime.Now;

                            isSuccess = productAdmin.Update(product);

                            // If Product doesn't have any SKUs yet,then create new SKU in the database
                            skuAdminAccess.Add(sku);
                        }
                    }
                    else
                    {
                        product.ReviewStateID = productAdmin.UpdateReviewStateID(product, UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId);

                        if (isSKUUpdated)
                        {
                            product.ReviewStateID = productAdmin.UpdateReviewStateID(UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId, "Quantity");
                        }

                        // Set update date
                        product.UpdateDte = System.DateTime.Now;

                        isSuccess = productAdmin.Update(product);

                        // If User selectes Default product type for this product,
                        // then Remove all the existing SKUs for this product
                        skuAdminAccess.DeleteByProductId(this.itemId);

                        // Add default product sku
                        skuAdminAccess.Add(sku);
                    }

                    if (!isSuccess)
                    {
                        throw new ApplicationException();
                    }

                    // Delete existing SKUAttributes
                    skuAdminAccess.DeleteBySKUId(sku.SKUID);

                    // Add SKU Attributes                
                    foreach (DataRow MyDataRow in MyAttributeTypeDataSet.Tables[0].Rows)
                    {
                        System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                        int selValue = int.Parse(lstControl.SelectedValue);

                        if (selValue > 0)
                        {
                            skuAttribute.AttributeId = selValue;
                        }

                        skuAttribute.SKUID = sku.SKUID;

                        skuAdminAccess.AddSKUAttribute(skuAttribute);
                    }
                }
                else
                {
                    // Product Add
                    product.ActiveInd = true;

                    product.ReviewStateID = productAdmin.UpdateReviewStateID(product, UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId);

                    // if ProductType has SKUs, then insert sku with Product
                    isSuccess = productAdmin.Add(product, sku, out this.productId, out this.skuId);

                    if (!isSuccess)
                    {
                        throw new ApplicationException();
                    }

                    // Add SKU Attributes
                    foreach (DataRow MyDataRow in MyAttributeTypeDataSet.Tables[0].Rows)
                    {
                        System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                        int selValue = int.Parse(lstControl.SelectedValue);

                        if (selValue > 0)
                        {
                            skuAttribute.AttributeId = selValue;
                        }

                        skuAttribute.SKUID = this.skuId;

                        skuAdminAccess.AddSKUAttribute(skuAttribute);
                    }

                    if (uxProductImage.PostedFile != null)
                    {
                        // Validate image
                        if ((this.itemId == 0 || RadioProductNewImage.Checked == true) && uxProductImage.PostedFile.FileName.Length > 0)
                        {
                            uxProductImage.AppendToFileName = "-" + this.productId.ToString();
                        }
                    }

                    // Upload File if this is a new product or the New Image option was selected for an existing product
                    if (RadioProductNewImage.Checked || this.itemId == 0)
                    {
                        if (uxProductImage.PostedFile != null && !string.IsNullOrEmpty(uxProductImage.PostedFile.FileName))
                        {
                            isSuccess = uxProductImage.SaveImage("fadmin", UserStoreAccess.GetTurnkeyStorePortalID.Value.ToString());
                            fileInfo = uxProductImage.FileInformation;
                            skufileInfo = uxProductImage.FileInformation;
                            if (!isSuccess)
                            {
                                if (uxProductImage.FileInformation.Name != string.Empty)
                                {
                                    product.ImageFile = "Turnkey/" + UserStoreAccess.GetTurnkeyStorePortalID.Value + "/" + uxProductImage.FileInformation.Name;
                                    sku.SKUPicturePath = "Turnkey/" + UserStoreAccess.GetTurnkeyStorePortalID.Value + "/" + skufileInfo.Name;
                                }
                            }
                            else
                            {
                                product.ImageFile = "Turnkey/" + UserStoreAccess.GetTurnkeyStorePortalID.Value + "/" + uxProductImage.FileInformation.Name;
                                sku.SKUPicturePath = "Turnkey/" + UserStoreAccess.GetTurnkeyStorePortalID.Value + "/" + skufileInfo.Name;
                            }
                        }
                    }

                    productAdmin.Update(product);
                }

                // Commit transaction
                tranManager.Commit();
            }
            catch
            {
                // Error occurred so rollback transaction
                if (tranManager.IsOpen)
                {
                    tranManager.Rollback();
                }

                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductUpdate").ToString();
                return;
            }

            //Send Mail function
            this.SendMailByFranchiseAdmin(productId.ToString(), product.Name.ToString());

            // Redirect to next page
            if (this.itemId > 0)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit of Product - " + txtProductName.Text, txtProductName.Text);

                string ViewLink = "~/FranchiseAdmin/Secure/Inventory/Products/view.aspx?itemid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString()));
                Response.Redirect(ViewLink);
            }
            else
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Creation of Product - " + txtProductName.Text, txtProductName.Text);

                string NextLink = "~/FranchiseAdmin/Secure/Inventory/Products/view.aspx?itemid=" + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.productId.ToString()));
                Response.Redirect(NextLink);
            }
        }

        /// <summary> 
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.itemId > 0)
            {
                Response.Redirect(this.cancelLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
            }
            else
            {
                Response.Redirect(this.listLink);
            }
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = true;
        }

        /// <summary>
        /// Shipping rule type selected index change event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ShippingTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.EnableValidators();
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Enable/Disable validators.
        /// </summary>
        private void EnableValidators()
        {
            pnlShippingRate.Visible = false;

            if (ShippingTypeList.SelectedValue.Equals("3"))
            {
                pnlShippingRate.Visible = true;
            }

            weightBasedRangeValidator.Enabled = false;
            RequiredForWeightBasedoption.Enabled = false;

            if (ShippingTypeList.SelectedValue.Equals("2"))
            {
                weightBasedRangeValidator.Enabled = true;
                RequiredForWeightBasedoption.Enabled = true;
            }
        }
        #endregion

        #region Bind Edit Data
        /// <summary>
        /// Bind value for Particular Product
        /// </summary>
        private void BindEditData()
        {
            ProductAdmin productAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();
            ZNode.Libraries.DataAccess.Entities.ProductCategory productCategory = new ZNode.Libraries.DataAccess.Entities.ProductCategory();
            SKUAdmin skuAdmin = new SKUAdmin();

            if (this.itemId > 0)
            {
                product = productAdmin.GetByProductId(this.itemId);
                UserStoreAccess.CheckStoreAccess(product.PortalID.GetValueOrDefault(0), true);
                ProductService productService = new ProductService();
                productService.DeepLoad(product);
                productCategory = productAdmin.GetProductCategoryByProductID(this.itemId);

                // General Information - Section1
                lblTitle.Text += product.Name;
                txtProductName.Text = Server.HtmlDecode(product.Name);
                txtProductNum.Text = Server.HtmlDecode(product.ProductNum);

                if (product.ProductTypeID > 0)
                {
                    this.productTypeId = product.ProductTypeID;
                    ProductTypeList.IsEnabled = this.IsEnabled();
                    ProductTypeList.Value = product.ProductTypeID.ToString();
                    ProductTypeList.DataBind();
                }

                // Product Description and Image - Section2
                txtshortdescription.Text = Server.HtmlDecode(product.ShortDescription);
                ctrlHtmlText.Html = product.Description;

                ZNodeImage znodeImage = new ZNodeImage();
                Image1.ImageUrl = znodeImage.GetImageHttpPathMedium(product.ImageFile);

                // Displaying the Image file name in a textbox           
                txtimagename.Text = product.ImageFile;
                txtImageAltTag.Text = product.ImageAltTag;

                // Product properties
                if (product.RetailPrice.HasValue)
                {
                    txtproductRetailPrice.Text = product.RetailPrice.Value.ToString("N");
                }

                if (product.SalePrice.HasValue)
                {
                    txtproductSalePrice.Text = product.SalePrice.Value.ToString("N");
                }

                if (product.WholesalePrice.HasValue)
                {
                    txtProductWholeSalePrice.Text = product.WholesalePrice.Value.ToString("N");
                }

                txtDownloadLink.Text = Server.HtmlDecode(product.DownloadLink);

                // Tax Classes
                if (product.TaxClassID.HasValue)
                {
                    ddlTaxClass.SelectedValue = product.TaxClassID.Value.ToString();
                }

                txtShippingRate.Text = product.ShippingRate.GetValueOrDefault().ToString("N");

                // Display Settings            
                txtDisplayOrder.Text = product.DisplayOrder.ToString();

                // Inventory
                TList<SKU> skuList = skuAdmin.GetByProductID(this.itemId);

                if (skuList != null && skuList.Count > 0)
                {
                    ViewState["productSkuId"] = skuList[0].SKUID.ToString();
                    txtProductSKU.Text = skuList[0].SKU;

                    // Set the existing value in hidden
                    existSKUvalue.Value = skuList[0].SKU;

                    SKUInventory skuInventory = skuAdmin.GetSkuInventoryBySKU(skuList[0].SKU);

                    if (skuInventory != null)
                    {
                        txtProductQuantity.Text = skuInventory.QuantityOnHand.GetValueOrDefault(0).ToString();
                        txtReOrder.Text = skuInventory.ReOrderLevel.GetValueOrDefault(0).ToString();
                    }

                    this.BindAttributes(product.ProductTypeID);
                }

                ProductReviewHistoryAdmin reviewAdmin = new ProductReviewHistoryAdmin();
                string changedFields = reviewAdmin.GetChangedFields(this.itemId);

                // Remove unwanted Commas
                while (!string.IsNullOrEmpty(changedFields) && changedFields.Substring(0, 1) == ",")
                {
                    changedFields = changedFields.Substring(1);
                }

                if (product.MinQty.HasValue)
                {
                    txtMinQuantity.Text = product.MinQty.Value.ToString();
                }

                if (product.MaxQty.HasValue)
                {
                    txtMaxQuantity.Text = product.MaxQty.Value.ToString();
                }

                // Shipping settings
                ShippingTypeList.SelectedValue = product.ShippingRuleTypeID.ToString();

                // This method will enable or disable validators based on the shippingtype
                this.EnableValidators();
                chkShipSeparately.Checked = product.ShipSeparately;
                if (product.FreeShippingInd.HasValue)
                {
                    chkFreeShippingInd.Checked = product.FreeShippingInd.Value;
                    if (chkFreeShippingInd.Checked)
                    {
                        chkShipSeparately.Enabled = false;
                        chkShipSeparately.Checked = false;
                    }
                }



                if (product.Weight.HasValue)
                {
                    txtProductWeight.Text = product.Weight.Value.ToString("N2");
                }

                if (product.Height.HasValue)
                {
                    txtProductHeight.Text = product.Height.Value.ToString("N2");
                }

                if (product.Width.HasValue)
                {
                    txtProductWidth.Text = product.Width.Value.ToString("N2");
                }

                if (product.Length.HasValue)
                {
                    txtProductLength.Text = product.Length.Value.ToString("N2");
                }

                ctrlHtmlPrdFeatures.Html = product.FeaturesDesc;
                ctrlHtmlPrdSpec.Html = product.Specifications;
                CtrlHtmlProdInfo.Html = product.AdditionalInformation;

                // Release the Resources
                skuList.Dispose();
            }
        }

        /// <summary>
        /// Binds the Sku Attributes for this Product
        /// </summary>
        /// <param name="typeId">Product type Id</param>
        private void BindAttributes(int typeId)
        {
            SKUAdmin adminSKU = new SKUAdmin();
            ProductAdmin adminAccess = new ProductAdmin();

            if (ViewState["productSkuId"] != null)
            {
                // Get SKUID from the ViewState
                DataSet SkuDataSet = adminSKU.GetBySKUId(int.Parse(ViewState["productSkuId"].ToString()));
                DataSet MyDataSet = adminAccess.GetAttributeTypeByProductTypeID(typeId);

                foreach (DataRow dr in MyDataSet.Tables[0].Rows)
                {
                    foreach (DataRow Dr in SkuDataSet.Tables[0].Rows)
                    {
                        System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + dr["AttributeTypeId"].ToString());

                        if (lstControl != null)
                        {
                            lstControl.SelectedValue = Dr["Attributeid"].ToString();
                        }
                    }
                }
            }
        }
        #endregion

        /// <summary>
        /// Returns the boolean value
        /// </summary>
        /// <returns>Returns true or false</returns>
        private bool IsEnabled()
        {
            if (this.itemId > 0)
            {
                ParentChildProductAdmin parentChildProduct = new ParentChildProductAdmin();
                TList<ParentChildProduct> _parentChildProductList = parentChildProduct.GetParentChildProductByProductId(this.itemId);

                if (_parentChildProductList.Count > 0)
                {
                    return false;
                }
            }

            return true;
        }

        #region Bind Data
        /// <summary>
        /// Bind Shipping option list
        /// </summary>
        private void BindShippingTypes()
        {
            // Bind ShippingRuleTypes
            ShippingAdmin shippingAdmin = new ShippingAdmin();
            ShippingTypeList.DataSource = shippingAdmin.GetShippingRuleTypes();
            ShippingTypeList.DataTextField = "description";
            ShippingTypeList.DataValueField = "shippingruletypeid";
            ShippingTypeList.DataBind();
        }

        /// <summary>
        /// Bind Tax Class
        /// </summary>
        private void BindData()
        {
            TaxRuleAdmin TaxRuleAdmin = new TaxRuleAdmin();
            TList<TaxClass> taxClass = UserStoreAccess.CheckStoreAccess(TaxRuleAdmin.GetAllTaxClass());
            taxClass.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.TaxClass tax) { return tax.ActiveInd == true; });
            PropertyDescriptor property = TypeDescriptor.GetProperties(typeof(TaxClass))["DisplayOrder"];
            taxClass.ApplySort(property, ListSortDirection.Ascending);
            ddlTaxClass.DataSource = taxClass;
            ddlTaxClass.DataTextField = "name";
            ddlTaxClass.DataValueField = "TaxClassID";


            ddlTaxClass.DataBind();
        }

        /// <summary>
        /// Bind control display based on properties set-Dynamically adds DropDownList for Each AttributeType
        /// </summary>
        /// <param name="typeId">Product Type Id.</param>
        private void Bind(int typeId)
        {
            this.productTypeId = typeId;
            StringBuilder attributeNames = new StringBuilder();
            // Bind Attributes
            ProductAdmin adminAccess = new ProductAdmin();

            DataSet MyDataSet = adminAccess.GetAttributeTypeByProductTypeID(this.productTypeId);

            // Repeat until Number of Attributetypes for this Product
            foreach (DataRow dr in MyDataSet.Tables[0].Rows)
            {
                // Get all the Attribute for this Attribute
                DataSet attributeDataSet = new DataSet();
                attributeDataSet = adminAccess.GetByAttributeTypeID(int.Parse(dr["attributetypeid"].ToString()));

                DataView dv = new DataView(attributeDataSet.Tables[0]);

                // Concat Attributes Names
                if (attributeNames.Length > 0)
                {
                    attributeNames.Append(" and ");
                }
                attributeNames.Append(dr["Name"].ToString());

                // Create Instance for the DropDownlist
                System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                lstControl.ID = "lstAttribute" + dr["AttributeTypeId"].ToString();

                ListItem li = new ListItem(dr["Name"].ToString(), "0");
                li.Selected = true;
                dv.Sort = "DisplayOrder ASC";
                lstControl.DataSource = dv;
                lstControl.DataTextField = "Name";
                lstControl.DataValueField = "AttributeId";
                lstControl.DataBind();
                lstControl.Items.Insert(0, li);

                if (!Convert.ToBoolean(dr["IsPrivate"]))
                {
                    // Add the Control to Place Holder
                    ControlPlaceHolder.Controls.Add(lstControl);

                    RequiredFieldValidator FieldValidator = new RequiredFieldValidator();
                    FieldValidator.ID = "Validator" + dr["AttributeTypeId"].ToString();
                    FieldValidator.ControlToValidate = "lstAttribute" + dr["AttributeTypeId"].ToString();
                    FieldValidator.ErrorMessage = "Select " + dr["Name"].ToString();
                    FieldValidator.Display = ValidatorDisplay.Dynamic;
                    FieldValidator.CssClass = "Error";
                    FieldValidator.InitialValue = "0";

                    ControlPlaceHolder.Controls.Add(FieldValidator);
                    Literal lit1 = new Literal();
                    lit1.Text = "&nbsp;&nbsp;";
                    ControlPlaceHolder.Controls.Add(lit1);
                }
            }

            if (MyDataSet.Tables[0].Rows.Count == 0)
            {
                DivAttributes.Visible = false;
                pnlquantity.Visible = true;
                lblProductTypeHint.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "HintTextSimpleProductType").ToString();
            }
            else
            {
                DivAttributes.Visible = true;
                pnlquantity.Visible = false;
                lblProductTypeHint.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "HintTextComplexProductType ").ToString(), attributeNames);
            }
        }

        #endregion

        #region Znode version 7.2.2 Send Mail

        /// <summary>
        /// Znode version 7.2.2
        /// This function will send notification mail to site admin.
        /// For approval of new added product by Franchise Admin.
        /// </summary>
        /// <param name="productId">string productId to send productId in email</param>
        /// <param name="productName">string productName to send productName in email</param>
        protected void SendMailByFranchiseAdmin(string productId, string productName)
        {
            //Get franchise Name
            string franchiseName = GetFranchiseName();

            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;

            //Get franchise Email
            string senderEmail = GetFranchiseEmail(); //ZNodeConfigManager.SiteConfig.CustomerServiceEmail;

            //Subject is set from global resources of admin.
            string subject = productName + " " + HttpContext.GetGlobalResourceObject("CommonCaption", "SubjectProductAddedByVendor");
            string receiverEmail = ZNodeConfigManager.SiteConfig.AdminEmail;
            string currentCulture = string.Empty;
            string templatePath = string.Empty;
            string defaultTemplatePath = string.Empty;
            string shortDesc = txtshortdescription.Text;

            // Template selection
            currentCulture = ZNodeCatalogManager.CultureInfo;
            defaultTemplatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ProductsAddedByFranchisee.html"));

            templatePath = (!string.IsNullOrEmpty(currentCulture))
                ? Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ProductsAddedByFranchisee_" + currentCulture + ".html")
                : Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ProductsAddedByFranchisee.html"));

            // Check the default template file existence.
            if (!File.Exists(defaultTemplatePath))
            {
                return;
            }
            else
            {
                // If language specific template not available then load default template.
                templatePath = defaultTemplatePath;
            }

            StreamReader rw = new StreamReader(templatePath);
            string messageText = rw.ReadToEnd();

            Regex rx1 = new Regex("#PRODUCTID#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(messageText, productId);

            Regex rx2 = new Regex("#PRODUCTNAME#", RegexOptions.IgnoreCase);
            messageText = rx2.Replace(messageText, productName);

            Regex rx3 = new Regex("#SHORTDESCRIPTION#", RegexOptions.IgnoreCase);
            messageText = rx3.Replace(messageText, shortDesc);

            Regex rx4 = new Regex("#FRANCHISEENAME#", RegexOptions.IgnoreCase);
            messageText = rx4.Replace(messageText, franchiseName);

            Regex rx5 = new Regex("#FRANCHISEEMAIL#", RegexOptions.IgnoreCase);
            messageText = rx5.Replace(messageText, senderEmail);

            Regex RegularExpressionLogo = new Regex("#LOGO#", RegexOptions.IgnoreCase);
            messageText = RegularExpressionLogo.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);

            try
            {
                // Send mail
                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(receiverEmail, senderEmail, string.Empty, subject, messageText, true);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralMessage, receiverEmail, Request.UserHostAddress.ToString(), null, ex.Message, null);
            }
        }

        /// <summary>
        /// Znode version 7.2.2 
        /// This function will return franchise name for sending mail to Site Admin.        
        /// </summary>
        /// <returns>franchiserName for mail notification</returns>
        private string GetFranchiseName()
        {
            string franchiseName = string.Empty;
            AccountService accountService = new AccountService();
            int accountID = 0;

            if (int.TryParse((ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount) as ZNodeUserAccount).AccountID.ToString(), out accountID))
            {
                if (accountID > 0)
                {
                    AddressService addressService = new AddressService();
                    TList<Address> addressList = addressService.GetByAccountID(accountID);
                    if (!addressList.Equals(null) && addressList.Count > 0)
                    {
                        franchiseName = addressList[0].FirstName + " " + addressList[0].LastName;
                    }
                }
            }

            return franchiseName;
        }

        /// <summary>
        /// Znode version 7.2.2 
        /// This function will return franchise Email id for sending mail to Site Admin.
        /// </summary>
        /// <returns>franchiseEmail for mail notification</returns>
        private string GetFranchiseEmail()
        {
            string franchiseEmail = string.Empty;
            int accountID = 0;

            if (int.TryParse((ZNodeUserAccount.CreateFromSession(ZNodeSessionKeyType.UserAccount) as ZNodeUserAccount).AccountID.ToString(), out accountID))
            {
                if (accountID > 0)
                {
                    AccountService accountService = new AccountService();
                    Account account = accountService.GetByAccountID(accountID);
                    franchiseEmail = account.Email;
                }
            }

            return franchiseEmail;
        }
        #endregion
    }
}