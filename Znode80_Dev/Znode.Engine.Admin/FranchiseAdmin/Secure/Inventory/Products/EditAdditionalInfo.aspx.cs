using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_edit_additionalInfo class.
    /// </summary>
    public partial class AdditionalInfo : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private string managePageLink;
        private string cancelPageLink;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Page Load Event
        protected void Page_Load(object sender, EventArgs e)
        {
            this.managePageLink = "~/FranchiseAdmin/Secure/Inventory/Products/view.aspx?mode=" + HttpUtility.UrlEncode(this.encrypt.EncryptData("additional")) + "&itemid=";
            this.cancelPageLink = "~/FranchiseAdmin/Secure/Inventory/Products/view.aspx?mode=" + HttpUtility.UrlEncode(this.encrypt.EncryptData("additional")) + "&itemid=";
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }

            if (!Page.IsPostBack)
            {
                if (this.itemId > 0)
                {
                    lblTitle.Text = "Edit Additional Info for ";

                    // Bind Sku Details
                    this.Bind();
                }
            }
        }
        #endregion       

        #region General Events
        /// <summary>
        /// Submit Button Click Event - Fires when Submit button is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();

            // If edit mode then get all the values first
            if (this.itemId > 0)
            {
                product = productAdmin.GetByProductId(this.itemId);
            }

            product.FeaturesDesc = ctrlHtmlPrdFeatures.Html.Trim();
            product.Specifications = ctrlHtmlPrdSpec.Html.Trim();
            product.AdditionalInformation = CtrlHtmlProdInfo.Html.Trim();

            bool isSuccess = false;

            try
            {
                // Update the product
                if (this.itemId > 0) 
                {
                    product.ReviewStateID = productAdmin.UpdateReviewStateID(product, UserStoreAccess.GetTurnkeyStorePortalID.Value, this.itemId);

                    isSuccess = productAdmin.Update(product);
                }

                if (isSuccess)
                {
                    string associateName = "Edit Additional Info - " + product.Name;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, product.Name);

                    Response.Redirect(this.managePageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
                }
                else
                {
                    lblError.Text = "Unable to update product additional information settings. Please try again.";
                }
            }
            catch (Exception)
            {
                lblError.Text = "Unable to update product additional information settings. Please try again.";
                return;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.cancelPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(this.itemId.ToString())));
        }
        #endregion

        #region Bind Methods
        private void Bind()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin productAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = productAdmin.GetByProductId(this.itemId);
            UserStoreAccess.CheckStoreAccess(product.PortalID.GetValueOrDefault(0), true);
            if (product != null)
            {
                ctrlHtmlPrdFeatures.Html = product.FeaturesDesc;
                ctrlHtmlPrdSpec.Html = product.Specifications;
                CtrlHtmlProdInfo.Html = product.AdditionalInformation;
                lblTitle.Text += "\"" + product.Name + "\"";
            }
        }
        #endregion
    }
}