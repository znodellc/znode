﻿using System;
using System.Web;
using System.Web.UI;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Mall Admin Admin_Secure_Product_ViewProductReview class.
    /// </summary>
    public partial class ViewProductReview : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private int reviewHistoryId = 0;
        private string managePageLink = "~/FranchiseAdmin/Secure/Inventory/Products/view.aspx?ItemId=";
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["itemid"].ToString()));
            }

            if (Request.Params["historyid"] != null)
            {
                this.reviewHistoryId = Convert.ToInt32(this.encryption.DecryptData(Request.Params["historyid"].ToString()));
            }

            if (!Page.IsPostBack)
            {
                if (this.reviewHistoryId > 0)
                {
                    // Bind review history Details
                    this.Bind();
                }
            }
        }

        #region Bind Methods
        private void Bind()
        {
            // Add entry to Product Review history table.
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            ProductReviewHistory productReviewHistory = productReviewHistoryAdmin.GetByReviewHistoryID(this.reviewHistoryId);
            
            if (productReviewHistory != null)
            {
                ProductAdmin productAdmin = new ProductAdmin();
                if (this.itemId > 0)
                {
                    ZNode.Libraries.DataAccess.Entities.Product product = productAdmin.GetByProductId(this.itemId);
                    int? vendorId = 0;
                    if (product != null)
                    {
                        lblTitle.Text += this.GetGlobalResourceObject("ZnodeAdminResource", "TitleRejectHistoryDetails").ToString() + '"' + product.Name + '"';
                        lblProductName.Text = product.Name;
                        vendorId = product.AccountID;

                        if (vendorId != null)
                        {
                            AccountAdmin accountAdmin = new AccountAdmin();
                            Address address = accountAdmin.GetDefaultBillingAddress(Convert.ToInt32(vendorId));
                            if (address != null)
                            {
                                lblVendor.Text = address.FirstName + " " + address.LastName;
                            }
                        }
                    }
                }

                lblDateModified.Text = productReviewHistory.LogDate.ToString();
                lblUserName.Text = productReviewHistory.Username;
                lblReason.Text = productReviewHistory.Reason.Trim().Length == 0 ? "N/A" : productReviewHistory.Reason.Trim();
                lblStatus.Text = productReviewHistory.Status;
                
                if (productReviewHistory.Description.Trim().Length > 0)
                {
                    lblDescription.Text = HttpUtility.HtmlDecode(productReviewHistory.Description);
                }
                else
                {
                    lblDescription.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnNotApplicable").ToString();
                }
            }
        }

        #endregion

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.managePageLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())));
        }
    }
}