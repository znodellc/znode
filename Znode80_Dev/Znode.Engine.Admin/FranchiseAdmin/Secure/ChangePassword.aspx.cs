using System;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.ECommerce.UserAccount;

namespace  Znode.Engine.FranchiseAdmin.Secure
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_ChangePassword class.
    /// </summary>
    public partial class Changepassword : System.Web.UI.Page
    {
        #region Private Member Variables
        private string redirectUrl = "~/FranchiseAdmin/Secure/default.aspx";
        #endregion 

        #region General Events        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["Mode"] != null)
            {
                this.redirectUrl = "~/FranchiseAdmin/Secure/";
            }

            if (!Page.IsPostBack)
            {
                // Retrieve the information from the database
                MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            }
        }

        /// <summary>
        /// Continue button clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ContinuePushButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.redirectUrl);
        }

        /// <summary>
        /// Cancel button clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CancelPushButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.redirectUrl);
        }

        /// <summary>
        /// Change Password Error.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChangePassword1_ChangePasswordError(object sender, EventArgs e)
        {
            (AdminChangePassword.ChangePasswordTemplateContainer.FindControl("PasswordFailureText") as Literal).Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextPasswordFailure").ToString();
        }

        /// <summary>
        /// Change password Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AdminChangePassword_ChangingPassword(object sender, LoginCancelEventArgs e)
        {
            e.Cancel = true;
            this.ChangePassword();
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Custom method to updates the password
        /// </summary>
        protected void ChangePassword()
        {
            var user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
			var passwordQuestion = (AdminChangePassword.ChangePasswordTemplateContainer.FindControl("ddlSecretQuestions") as DropDownList).SelectedItem.Text;
			var passwordAnswer = (AdminChangePassword.ChangePasswordTemplateContainer.FindControl("Answer") as TextBox).Text.Trim();

            if (user != null)
            {
                // Verify if the new password specified by the user is in the list of the last 4 passwords used.
                bool isNewPasswordVerified = ZNodeUserAccount.VerifyNewPassword((Guid)user.ProviderUserKey, AdminChangePassword.NewPassword);

                if (!isNewPasswordVerified)
                {
                    (AdminChangePassword.ChangePasswordTemplateContainer.FindControl("PasswordFailureText") as Literal).Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextPreviousPasswordFailure").ToString();
                    return;
                }

                // Updates the password for this user
                if (user.ChangePassword(AdminChangePassword.CurrentPassword, AdminChangePassword.NewPassword))
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordChangeSuccess, HttpContext.Current.User.Identity.Name, Request.UserHostAddress.ToString(), null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogIncorrectPassword").ToString(), null);

                    // Log password for further debugging
                    ZNodeUserAccount.LogPassword((Guid)user.ProviderUserKey, AdminChangePassword.NewPassword);

                    // Log Activity
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogChangePassword") + HttpContext.Current.User.Identity.Name, HttpContext.Current.User.Identity.Name);

					if (passwordAnswer.Length > 0)
					{
						// Updates the password question and answer for this User
						user.ChangePasswordQuestionAndAnswer(AdminChangePassword.NewPassword, passwordQuestion, passwordAnswer);
					}

                    // Redirect to account page
                    Response.Redirect("~/FranchiseAdmin/Secure/default.aspx");
                }
                else
                {
                    // Display Error message
                    (AdminChangePassword.ChangePasswordTemplateContainer.FindControl("PasswordFailureText") as Literal).Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextPasswordFailure").ToString();
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordChangeFailed, HttpContext.Current.User.Identity.Name, Request.UserHostAddress.ToString(), null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogIncorrectPassword").ToString(), null);
                }
            }
        }
        #endregion
    }
}