<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Denied" Title="Access Denied" CodeBehind="Denied.aspx.cs" %>

<%@ Register Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h1>
        <asp:Localize ID="TitleAccessDenied" Text='<%$ Resources:ZnodeAdminResource,TitleAccessDenied%>' runat="server"></asp:Localize>
    </h1>
    <asp:Localize ID="TextAccessDenied" Text='<%$ Resources:ZnodeAdminResource,TextAccessDenied%>' runat="server"></asp:Localize>
    <br />
    <br />
    <a href="javascript: history.go(-1)">
        <asp:Localize ID="LinkBack" Text='<%$ Resources:ZnodeAdminResource,LinkBack%>' runat="server"></asp:Localize>
    </a>
    <br />
    <uc1:spacer ID="Spacer8" SpacerHeight="300" SpacerWidth="10" runat="server"></uc1:spacer>
</asp:Content>

