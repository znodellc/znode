<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True"
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Marketing.Other.StoreLocator.Delete" CodeBehind="Delete.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h5>
        <asp:Label ID="deletestore" runat="server">
            <asp:Localize runat="server" ID="TitleDeleteStore" Text='<%$ Resources:ZnodeAdminResource, TitleDeleteStore %>'></asp:Localize>
        </asp:Label>
    </h5>
    <p>
        <asp:Localize runat="server" ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, DeleteConfirmationStore %>'></asp:Localize>"<b><%=StoreName%></b>".
    </p>
    <p>
        <asp:Label ID="lblErrorMsg" runat="server" Visible="False" CssClass="Error"></asp:Label>
    </p>
    <zn:Button runat="server" ID="btnDelete" ButtonType="CancelButton" OnClick="BtnDelete_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>' CausesValidation="true" />
    <zn:Button runat="server" ID="btnCancel" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
    <br />
    <br />
    <br />
</asp:Content>
