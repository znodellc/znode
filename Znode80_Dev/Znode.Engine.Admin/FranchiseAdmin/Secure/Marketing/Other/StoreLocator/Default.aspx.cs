using System;
using System.Web;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Marketing.Other.StoreLocator
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_settings_StoreLocator_List class.
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Private Variables
        private static bool isSearchEnabled = false;
        private string addPageLink = "~/FranchiseAdmin/Secure/Marketing/Other/StoreLocator/Add.aspx";
        private string deletePageLink = "~/FranchiseAdmin/Secure/Marketing/Other/StoreLocator/Delete.aspx";
        
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindGridData();
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // Check if search is Enabled
            if (isSearchEnabled)
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindSearchStore();
            }
            else
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindGridData();
            }
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                ZNodeEncryption encryption = new ZNodeEncryption();
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Manage")
                {
                    this.addPageLink = this.addPageLink + "?ItemID=" + HttpUtility.UrlEncode(encryption.EncryptData(Id));
                    Response.Redirect(this.addPageLink);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.deletePageLink + "?ItemID=" + HttpUtility.UrlEncode(encryption.EncryptData(Id)));
                }
            }
        }

        #endregion      

        #region General Events

        /// <summary>
        /// Add New Store Detail
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddStore_Click(object sender, System.EventArgs e)
        {
            Response.Redirect(this.addPageLink);
        }

        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchStore();
            isSearchEnabled = true;
        }

        /// <summary>
        /// Clear button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtcity.Text = string.Empty;
            txtstate.Text = string.Empty;
            txtstorename.Text = string.Empty;
            txtzipcode.Text = string.Empty;

            this.BindGridData();
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Encrypt the store Id
        /// </summary>
        /// <param name="storeId">Store Id to encrypt.</param>
        /// <returns>Returns the encrypted store Id</returns>
        protected string GetEncryptId(object storeId)
        {
            ZNodeEncryption encryption = new ZNodeEncryption();
            return HttpUtility.UrlEncode(encryption.EncryptData(storeId.ToString()));
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Search a Store by Store Name, ZipCode , State and City
        /// </summary>
        private void BindSearchStore()
        {
            StoreLocatorAdmin storeLocatorAdmin = new StoreLocatorAdmin();
            uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(storeLocatorAdmin.SearchStore(Server.HtmlEncode(txtstorename.Text.Trim()), txtzipcode.Text.Trim(), txtcity.Text.Trim(), txtstate.Text.Trim()).Tables[0]);
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            StoreLocatorAdmin store = new StoreLocatorAdmin();
            uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(store.GetAllStore());
            uxGrid.DataBind();
        }

        #endregion
    }
}