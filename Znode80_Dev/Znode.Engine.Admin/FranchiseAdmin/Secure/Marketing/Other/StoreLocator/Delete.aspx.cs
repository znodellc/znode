using System;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Marketing.Other.StoreLocator
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_settings_StoreLocator_Delete class.
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId;
        private string redirectPageLink = "~/FranchiseAdmin/Secure/Marketing/Other/StoreLocator/default.aspx";
        private string _StoreName = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the store name.
        /// </summary>
        public string StoreName
        {
            get { return this._StoreName; }
            set { this._StoreName = value; }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemID from querystring        
            if (Request.Params["ItemID"] != null)
            {
                ZNodeEncryption encrypt = new ZNodeEncryption();
                this.itemId = int.Parse(encrypt.DecryptData(Request.Params["ItemID"]));
            }
            else
            {
                this.itemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }
        #endregion

        #region General Events

        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            ZNode.Libraries.Admin.StoreLocatorAdmin storeLocatorAdmin = new StoreLocatorAdmin();

            bool isDeleted = false;

            Store store = storeLocatorAdmin.GetByStoreID(this.itemId);
            string associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteStoreLocation").ToString(), store.Name);

            isDeleted = storeLocatorAdmin.DeleteStore(this.itemId);

            if (isDeleted)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, store.Name);

                Response.Redirect(this.redirectPageLink);
            }
            else
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteAction").ToString();
                lblErrorMsg.Visible = true;
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.redirectPageLink);
        }
        #endregion

        #region Bind Data

        private void BindData()
        {
            ZNode.Libraries.Admin.StoreLocatorAdmin storeLocatorAdmin = new StoreLocatorAdmin();
            ZNode.Libraries.DataAccess.Entities.Store store = storeLocatorAdmin.GetByStoreID(this.itemId);

            if (store != null)
            {
                UserStoreAccess.CheckStoreAccess(store.PortalID, true);
                this.StoreName = store.Name;
            }
        }

        #endregion
    }
}