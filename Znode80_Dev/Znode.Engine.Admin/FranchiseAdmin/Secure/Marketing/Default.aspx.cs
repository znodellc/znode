using System;

namespace  Znode.Engine.FranchiseAdmin.Secure.Marketing
{
    /// <summary>
    /// Represents the SiteAdmin - Znode.Engine.Admin.Secure.Marketing.Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Events

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            Session["SelectedMenuItem"] = Request.Url.PathAndQuery;
        }
        #endregion

    }
}