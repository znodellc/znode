﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" Title="Manage You May Also Like" CodeBehind="ManageYmal.aspx.cs" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Marketing.Search.Personalization.ManageYmal" %>

<%@ Register Src="~/FranchiseAdmin/Controls/Default/Search/YmalManage.ascx" TagPrefix="uc1" TagName="YmalManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
	<uc1:YmalManage runat="server" ID="YmalManage" />
</asp:Content>