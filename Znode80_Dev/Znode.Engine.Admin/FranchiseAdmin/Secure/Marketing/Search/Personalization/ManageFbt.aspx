﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" Title="Manage Frequently Bought Together" CodeBehind="ManageFbt.aspx.cs" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Marketing.Search.Personalization.ManageFbt" %>

<%@ Register Src="~/FranchiseAdmin/Controls/Default/Search/FbtManage.ascx" TagPrefix="uc1" TagName="FbtManage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
	<uc1:FbtManage runat="server" ID="FbtManage" />
</asp:Content>