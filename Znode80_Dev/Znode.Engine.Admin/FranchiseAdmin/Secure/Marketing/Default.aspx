<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True"
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Marketing.Default" Title="Marketing - Default"
    CodeBehind="Default.aspx.cs" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="LeftMargin">

        <h1> <asp:Localize runat="server" ID="Promotions" Text='<%$ Resources:ZnodeAdminResource, TabTitlePromotions %>'></asp:Localize></h1>
        <hr />

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/promotions.png" />
            </div>
            <div class="Shortcut"><a id="A2" href="~/FranchiseAdmin/Secure/Marketing/Promotions/PromotionsAndCoupons/Default.aspx" runat="server"><asp:Localize runat="server" ID="PromotionandCoupons" Text='<%$ Resources:ZnodeAdminResource, TitlePromotionandCoupons %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="TextPromotionandCoupons" Text='<%$ Resources:ZnodeAdminResource, TextPromotionandCoupons %>'></asp:Localize></p>
            </div>
        </div>


        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/customer-reviews.png" />
            </div>
            <div class="Shortcut"><a id="A1" href="~/FranchiseAdmin/Secure/Marketing/Promotions/CustomerReviews/Default.aspx" runat="server"><asp:Localize runat="server" ID="CustomerReviews" Text='<%$ Resources:ZnodeAdminResource, LinkTextCustomerReviews %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="TextCustomerReviews" Text='<%$ Resources:ZnodeAdminResource, TextCustomerReviews %>'></asp:Localize></p>
            </div>
        </div>
        
        <h1><asp:Localize runat="server" ID="TitleSearchPersonalization" Text='<%$ Resources:ZnodeAdminResource, TitleSearchPersonalization %>'></asp:Localize></h1>        
        <hr />

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/personalization.png" />
            </div>
            <div class="Shortcut"><a id="A22" href="~/FranchiseAdmin/Secure/Marketing/Search/Personalization/Default.aspx" runat="server"><asp:Localize runat="server" ID="Personalization" Text='<%$ Resources:ZnodeAdminResource, LinkTextPersonalization %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="TextPersonalization" Text='<%$ Resources:ZnodeAdminResource, TextPersonalization %>'></asp:Localize></p>
            </div>
        </div>

        <h1><asp:Localize runat="server" ID="Other" Text='<%$ Resources:ZnodeAdminResource, LinkTextOther %>'></asp:Localize></h1>        
        <hr />

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/store-locator.png" />
            </div>
            <div class="Shortcut"><a id="A16" href="~/FranchiseAdmin/Secure/Marketing/Other/StoreLocator/Default.aspx" runat="server"><asp:Localize runat="server" ID="StoreLocator" Text='<%$ Resources:ZnodeAdminResource, LinkTextStoreLocator %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="TextStoreLocator" Text='<%$ Resources:ZnodeAdminResource, TextStoreLocator %>'></asp:Localize></p>
            </div>
        </div>


    </div>
</asp:Content>
