<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master"  ValidateRequest="false" AutoEventWireup="True" 
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Marketing.Promotions.CustomerReviews.ReviewStatus" Codebehind="reviewStatus.aspx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">    
    <div class="Form">
          <h1> <asp:Localize ID="ReviewTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleReviewTitle %>'></asp:Localize> &nbsp;<asp:Label ID="lblReviewHeader" runat="server" /></h1>     
          <div class="HintStyle"><asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextReviewStatus %>'></asp:Localize></div><br />
          <div class="FieldStyle">
            <asp:DropDownList ID="ListReviewStatus" runat="server">            
                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropdownTextActive %>' Value="A"></asp:ListItem>
                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropdownTextInactive %>'  Value="I"></asp:ListItem>
                <asp:ListItem Selected="True" Text='<%$ Resources:ZnodeAdminResource, DropDownTextNew %>' Value="N"></asp:ListItem>
            </asp:DropDownList>
          </div>
          <div><ZNode:spacer id="Spacer1" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:spacer></div>
          <div class="ValueField">
           
              <zn:Button runat="server" ID="btnSubmit" OnClick="UpdateReviewStatus_Click" ValidationGroup="grpAddOn" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>'/>
            <zn:Button runat="server" ID="btnCancel" OnClick="CancelStatus_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>'/>

          </div>
         <div><ZNode:spacer id="LongSpace" SpacerHeight="200" SpacerWidth="3" runat="server"></ZNode:spacer></div>
    </div>
</asp:Content>

