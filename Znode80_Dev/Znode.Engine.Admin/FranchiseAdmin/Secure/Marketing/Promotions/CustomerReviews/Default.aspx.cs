using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Marketing.Promotions.CustomerReviews
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_reviews_Default class.
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Private Member Variables
        private string changeStatusPageLink = "~/FranchiseAdmin/Secure/Marketing/Promotions/CustomerReviews/reviewStatus.aspx?ItemId=";
        private string editReviewPageLink = "~/FranchiseAdmin/Secure/Marketing/Promotions/CustomerReviews/EditReview.aspx?ItemId=";
        private string deleteReviewPageLink = "~/FranchiseAdmin/Secure/Marketing/Promotions/CustomerReviews/delete.aspx?ItemId=";
        private string listReviewPageLink = "~/FranchiseAdmin/Secure/Marketing/Promotions/CustomerReviews/default.aspx";
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the server control is loaded into the Page object. 
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // This is to indicate whether the page is being loaded in response to a client postback, 
            // or if it is being loaded and accessed for the first time.
            if (!Page.IsPostBack)
            {
                this.SearchReviews();
            }
        }

        /// <summary>
        /// The Click event is raised when the "Search" Button control is clicked.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.SearchReviews();
        }

        /// <summary>
        /// The Click event is raised when the "Clear" Button control is clicked.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listReviewPageLink);
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Occurs when a button is clicked in a GridView control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string reviewId = e.CommandArgument.ToString();

            // Multiple buttons are used in a GridView control, use the
            // CommandName property to determine which button was clicked
            if (e.CommandName == "Edit")
            {
                // Redirect to change review status page
                Response.Redirect(this.editReviewPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(reviewId)));
            }
            else if (e.CommandName == "Delete")
            {
                // Redirect to change review delete confirmation page
                Response.Redirect(this.deleteReviewPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(reviewId)));
            }
            else if (e.CommandName == "Status")
            {
                // Redirect to change review status page
                Response.Redirect(this.changeStatusPageLink + HttpUtility.UrlEncode(this.encrypt.EncryptData(reviewId)));
            }
        }

        /// <summary>
        /// Occurs when one of the pager buttons is clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.SearchReviews();
        }

        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind filtered collection list to grid control
        /// </summary>
        private void SearchReviews()
        {
            ReviewAdmin reviewAdmin = new ReviewAdmin();
            string status = ListReviewStatus.SelectedIndex == 0 ? string.Empty : ListReviewStatus.SelectedValue;
            DataSet ds = reviewAdmin.SearchReview(Server.HtmlEncode(ReviewTitle.Text.Trim()), Server.HtmlEncode(Name.Text.Trim()), Server.HtmlEncode(ddlProductNames.Text.Trim()), status);

            uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(ds.Tables[0]).DefaultView;
            uxGrid.DataBind();
        }
        #endregion
    }
}