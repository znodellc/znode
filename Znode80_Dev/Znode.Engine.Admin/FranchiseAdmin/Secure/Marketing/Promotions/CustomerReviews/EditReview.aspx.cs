using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Marketing.Promotions.CustomerReviews
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_reviews_edit_review class.
    /// </summary>
    public partial class EditReview : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId;
        private string listPageLink = "~/FranchiseAdmin/Secure/Marketing/Promotions/CustomerReviews/default.aspx";
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Page Load Event
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }
            else
            {
                this.itemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindEditData();
            }
        }

        #endregion       

        #region General Events
        /// <summary>
        /// Update button click event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnUpdate_Click(object sender, EventArgs e)
        {
            ReviewAdmin reviewAdmin = new ReviewAdmin();
            Review review = reviewAdmin.GetByReviewID(this.itemId);

            if (review != null)
            {
                review.Subject = Server.HtmlEncode(Headline.Text.Trim());
                review.Pros = Server.HtmlEncode(Pros.Text.Trim());
                review.Cons = Server.HtmlEncode(Cons.Text.Trim());
                review.Comments = Server.HtmlEncode(Comments.Text.Trim());

                if (ListReviewStatus.SelectedValue == "I")
                {
                    review.Status = "I";
                }
                else if (ListReviewStatus.SelectedValue == "A")
                {
                    review.Status = "A";
                }
                else
                {
                    review.Status = "N";
                }

                bool isUpdated = reviewAdmin.Update(review);

                if (isUpdated)
                {
                    string AssociateName = this.GetGlobalResourceObject("ZnodeAdminResource", "TextEditCustomerReview").ToString() +review.Subject;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, review.Subject);
                    Response.Redirect(this.listPageLink);
                }                
            }
        }

        /// <summary>
        /// Cancel button click event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listPageLink);
        }
        #endregion

        #region Bind Method
        /// <summary>
        /// Bind fields with data
        /// </summary>
        private void BindEditData()
        {
            ReviewAdmin reviewAdmin = new ReviewAdmin();
            Review review = reviewAdmin.DeepLoadByReviewID(this.itemId);
            if (review.ProductIDSource != null)
            {
                int id = review.ProductIDSource.PortalID.GetValueOrDefault(0);
                UserStoreAccess.CheckStoreAccess(id, true);
            }

            if (review != null && review.ProductIDSource != null)
            {
                lblReviewHeader.Text = review.ProductIDSource.Name;

                Headline.Text = Server.HtmlDecode(review.Subject);
                Pros.Text = Server.HtmlDecode(review.Pros);
                Cons.Text = Server.HtmlDecode(review.Cons);
                Comments.Text = Server.HtmlDecode(review.Comments);
                ListReviewStatus.SelectedValue = review.Status;
            }
            else
            {
                Response.Redirect("default.aspx");
            }
        }
        #endregion
    }
}