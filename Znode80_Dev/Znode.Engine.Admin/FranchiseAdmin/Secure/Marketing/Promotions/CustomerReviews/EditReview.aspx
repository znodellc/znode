<%@ Page Language="C#"  MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master"
    AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.FranchiseAdmin.Secure.Marketing.Promotions.CustomerReviews.EditReview" Codebehind="EditReview.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Localize runat="server" ID="ReviewTitle" Text='<%$ Resources:ZnodeAdminResource, TextEditReview %>'></asp:Localize>
                <asp:Label ID="lblReviewHeader" runat="server"></asp:Label></h1>
        </div>
        <div style="text-align: right">
                        <zn:Button ID="btnSubmitTop" runat="server" ButtonType="SubmitButton" OnClick="BtnUpdate_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button ID="btnCancelTop" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
        </div>
        
        <div class="FormView">
            <div><ZNode:Spacer ID="Spacer1" SpacerHeight="25" SpacerWidth="3" runat="server"></ZNode:Spacer></div>
            <div class="FieldStyle">
              <asp:Localize runat="server" ID="ReviewStatus" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleReviewStatus %>'></asp:Localize></div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ListReviewStatus" runat="server">
                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropdownTextActive %>'  Value="A"></asp:ListItem>
                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropdownTextInactive %>'  Value="I"></asp:ListItem>
                    <asp:ListItem Selected="True" Text='<%$ Resources:ZnodeAdminResource, DropDownTextNew %>' Value="N"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                 <asp:Localize runat="server" ID="TextHeadLine" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleHeadline %>'></asp:Localize><span class="Asterix">*</span></div>
            <div class="ValueStyle">
                <asp:TextBox ID='Headline' runat='server' MaxLength="100" Columns="50"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Headline"
                        Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredHeadLine %>'  CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator></div>
            </div>
            <div id='divPros' runat="server" visible="false">
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="TextPros" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePros %>'></asp:Localize></div>
                <div class="ValueStyle">
                    <asp:TextBox ID='Pros' runat='server' MaxLength="100" Columns="50"></asp:TextBox></div>
            </div>
            <div id='divCons' runat="server" visible="false">
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="TextCons" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCons %>'></asp:Localize></div>
                <div class="ValueStyle">
                    <asp:TextBox ID='Cons' runat='server' MaxLength="100" Columns="50"></asp:TextBox></div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="TextComments" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleComments %>'></asp:Localize><span class="Asterix">*</span></div>
            <div class="ValueStyle">
                <asp:TextBox ID='Comments' runat='server' TextMode="MultiLine" Rows="20" Columns="50"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Comments"
                        Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ColumnTitleComments %>' CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator></div>
            </div>            
            <div class="ClearBoth">
                <zn:Button ID="btnSubmit" runat="server" ButtonType="SubmitButton" OnClick="BtnUpdate_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
                <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
            </div>
            <div>
                <ZNode:Spacer ID="LongSpace" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
        </div>
    </div>
</asp:Content>
