<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" ValidateRequest="false" 
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Marketing.Promotions.CustomerReviews.Default" Codebehind="Default.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="ProductAutoComplete" Src="~/FranchiseAdmin/Controls/Default/ProductAutoComplete.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
               <asp:Localize runat="server" ID="CustomerReviews" Text='<%$ Resources:ZnodeAdminResource, LinkTextCustomerReviews %>'></asp:Localize></h1>
            <p>
                <asp:Localize runat="server" ID="TextCustomerReviews" Text='<%$ Resources:ZnodeAdminResource, TextCustomerReviews %>'></asp:Localize>
            </p>
        </div>
        <div class="ClearBoth">
            <h4 class="SubTitle">
               <asp:Localize runat="server" ID="SearchProductReviews" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchProductReviews %>'></asp:Localize>
            </h4>
            <asp:Panel ID="pnlReviewSearch" DefaultButton="btnSearch" runat="server">
                <div class="SearchForm">
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle"> <asp:Localize runat="server" ID="TextReviewTitle" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleReviewTitle %>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="ReviewTitle" runat="server" ValidationGroup="grpSearch"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize runat="server" ID="NickName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleNickName %>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="Name" runat="server" ValidationGroup="grpSearch"></asp:TextBox></span>
                        </div>
                    </div>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize runat="server" ID="ProductName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <ZNode:ProductAutoComplete ID="ddlProductNames" runat="server" />
                            </span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize runat="server" ID="Status" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus %>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:DropDownList ID="ListReviewStatus" runat="server">
                                    <asp:ListItem Selected="True" Text='<%$ Resources:ZnodeAdminResource, DropdownTextAll %>' Value="0"></asp:ListItem>
                                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropdownTextActive %>' Value="A"></asp:ListItem>
                                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropdownTextInactive %>' Value="I"></asp:ListItem>
                                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextNew %>' Value="N"></asp:ListItem>
                                </asp:DropDownList>
                            </span>
                        </div>
                    </div>
                    <div class="ClearBoth">
                       
                         <zn:Button ID="btnClear" runat="server" ButtonType="CancelButton" OnClick="BtnClearSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                        <zn:Button ID="btnSearch" runat="server" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch%>' />
                    </div>
                </div>
            </asp:Panel>
        </div>
        <br />
        <h4 class="GridTitle">
            <asp:Localize runat="server" ID="ProductReviewsList" Text='<%$ Resources:ZnodeAdminResource, GridTitleProductReviewsList %>'></asp:Localize>
        </h4>
        <div>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
                CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="15"
                AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundReviews %>'>
                <Columns>
                    <asp:BoundField DataField="ReviewId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleComments %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <span class="FieldStyle">Title:</span><span class="ValueStyle"><%# DataBinder.Eval(Container.DataItem,"Subject")%></span><div>
                                <uc1:spacer id="Spacer1" spacerheight="5" spacerwidth="3" runat="server"></uc1:spacer>
                            </div>                                                      
                            <div style="overflow:hidden"><%# DataBinder.Eval(Container.DataItem, "Comments").ToString()%></div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Date" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "CreateDate","{0:MM/dd/yyyy}")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="CreateUser" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleUser %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnStatus %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "Status").ToString()%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ACTIONS" HeaderStyle-HorizontalAlign="Left">
                        <ItemStyle Wrap="true" Width="200px" />
                        <ItemTemplate>
                                <asp:LinkButton ID="btnEditReview" CommandName="Edit" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ReviewId")%>'
                                    Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' runat="server" CssClass="LinkButton" />&nbsp;&nbsp;
                                <asp:LinkButton ID="btnChangeStatus" CommandName="Status" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ReviewId")%>'
                                   Text='<%$ Resources:ZnodeAdminResource, LinkChangeStatus %>' runat="server" CssClass="LinkButton" />&nbsp;&nbsp;
                                <asp:LinkButton ID="btnDelete" CommandName="Delete" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ReviewId")%>'
                                    Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' runat="server" CssClass="LinkButton"  />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
        </div>
    </div>
</asp:Content>
