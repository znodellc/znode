using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Marketing.Promotions.CustomerReviews
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_reviews_Delete class.
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        private string _ProductReviewTitle = string.Empty;
        #endregion

        #region Public Property
        /// <summary>
        /// Gets or sets the product review title.
        /// </summary>
        public string ProductReviewTitle
        {
            get { return this._ProductReviewTitle; }
            set { this._ProductReviewTitle = value; }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }
            else
            {
                this.itemId = 0;
            }

            this.Bind();
        }

        #region Events
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Marketing/Promotions/CustomerReviews/default.aspx");
        }

        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            ReviewAdmin reviewAdmin = new ReviewAdmin();
            Review review = new Review();
            review = reviewAdmin.GetByReviewID(this.itemId);

            string associateName = "Delete Customer Review - " + review.Subject;
            string subjectName = review.Subject;
            bool isDeleted = reviewAdmin.Delete(this.itemId);

            if (isDeleted)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, subjectName);
                Response.Redirect("~/FranchiseAdmin/Secure/Marketing/Promotions/CustomerReviews/default.aspx");
            }
            else
            {
                // Please ensure that this Add-On does not contain Add-On Values or products. If it does, then delete the Add-On values and products first.";
                lblMsg.Text = "An error occurred and the product review could not be deleted.";
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind the entity data.
        /// </summary>
        private void Bind()
        {
            ReviewAdmin reviewAdmin = new ReviewAdmin();
            Review review = reviewAdmin.GetByReviewID(this.itemId);

            if (review.ProductIDSource != null)
            {
                int id = review.ProductIDSource.PortalID.GetValueOrDefault(0);
                UserStoreAccess.CheckStoreAccess(id, true);
            }

            if (review != null)
            {
                this.ProductReviewTitle = review.Subject;
            }
            else
            {
                throw new ApplicationException("Review Requested could not be found.");
            }
        }
        #endregion
    }
}