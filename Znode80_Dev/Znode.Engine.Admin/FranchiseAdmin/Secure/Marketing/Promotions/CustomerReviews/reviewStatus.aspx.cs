using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Marketing.Promotions.CustomerReviews
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_reviews_ReviewStatus class.
    /// </summary>
    public partial class ReviewStatus : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId;
        private string listPageLink = "~/FranchiseAdmin/Secure/Marketing/Promotions/CustomerReviews/default.aspx";
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Page Load Event
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
            }
            else
            {
                this.itemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }

        #endregion       

        #region General Events
        /// <summary>
        /// Update button click event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UpdateReviewStatus_Click(object sender, EventArgs e)
        {
            ReviewAdmin reviewAdmin = new ReviewAdmin();
            Review review = reviewAdmin.GetByReviewID(this.itemId);

            if (review != null)
            {
                if (ListReviewStatus.SelectedValue == "I")
                {
                    review.Status = "I";
                }
                else if (ListReviewStatus.SelectedValue == "A")
                {
                    review.Status = "A";
                }
                else
                {
                    review.Status = "N";
                }
            }

            reviewAdmin.Update(review);

            string AssociateName = this.GetGlobalResourceObject("ZnodeAdminResource", "TextEditCustomerReviewStatus").ToString() + review.Subject;

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, review.Subject);

            Response.Redirect(this.listPageLink);
        }

        protected void CancelStatus_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listPageLink);
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind review data
        /// </summary>
        private void BindData()
        {
            ReviewAdmin reviewAdmin = new ReviewAdmin();
            Review review = reviewAdmin.GetByReviewID(this.itemId);

            if (review != null)
            {
                lblReviewHeader.Text = review.Subject;
                ListReviewStatus.SelectedValue = review.Status;
            }
        }
        #endregion
    }
}