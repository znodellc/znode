<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Marketing.Promotions.PromotionsAndCoupons.Delete" Codebehind="delete.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="PromotionDelete" Src="~/Controls/Default/Providers/Promotions/PromotionDelete.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
	<zn:PromotionDelete ID="ctlPromotionDelete" runat="server" RedirectUrl="~/FranchiseAdmin/Secure/Marketing/Promotions/PromotionsAndCoupons/Default.aspx" />
</asp:Content>
