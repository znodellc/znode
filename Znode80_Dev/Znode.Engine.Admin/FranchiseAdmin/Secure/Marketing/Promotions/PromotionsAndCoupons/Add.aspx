<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Marketing.Promotions.PromotionsAndCoupons.Add" ValidateRequest="false" Codebehind="Add.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="PromotionAdd" Src="~/Controls/Default/Providers/Promotions/PromotionAdd.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:PromotionAdd ID="ctlPromotionAdd" runat="server" />
</asp:Content>
