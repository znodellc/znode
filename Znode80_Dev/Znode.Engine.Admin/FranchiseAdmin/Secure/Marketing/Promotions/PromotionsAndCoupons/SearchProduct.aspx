<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/popup.master" ValidateRequest="false" AutoEventWireup="True" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Marketing.Promotions.PromotionsAndCoupons.SearchProduct" CodeBehind="SearchProduct.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="ProductSearch" Src="~/Controls/Default/Providers/Promotions/ProductSearch.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:ProductSearch ID="ctlProductSearch" runat="server" />
</asp:Content>
