<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Marketing.Promotions.PromotionsAndCoupons.Default" ValidateRequest="false" Codebehind="Default.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="PromotionList" Src="~/Controls/Default/Providers/Promotions/PromotionList.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
	<zn:PromotionList ID="ctlPromotionList" runat="server"
		ListPageUrl="~/FranchiseAdmin/Secure/Marketing/Promotions/PromotionsAndCoupons/Default.aspx"
		AddPageUrl="~/FranchiseAdmin/Secure/Marketing/Promotions/PromotionsAndCoupons/Add.aspx"
		DeletePageUrl="~/FranchiseAdmin/Secure/Marketing/Promotions/PromotionsAndCoupons/Delete.aspx"
	/>
</asp:Content>
