﻿using Microsoft.Reporting.WebForms;
using System;
using System.Data;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.FranchiseAdmin.Secure.Reports
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_Reports_TaxReport class.
    /// </summary>
    public partial class TaxReport : System.Web.UI.UserControl
    {
        #region Private Member Variables       
        private DataSet reportDataSet = null;
        private string _ReportTitle;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the control postback state.
        /// </summary>
        public string IsPostback
        {
            get { return hdnIsPostBack.Value; }
            set { hdnIsPostBack.Value = value; }
        }

       /// <summary>
       /// Gets or sets the report mode.
       /// </summary>
        public ZnodeReport Mode
        {
            get
            {
                string mode = hdnMode.Value;
                ZnodeReport report = ZnodeReport.ServiceRequest;
                if (Enum.IsDefined(typeof(ZnodeReport), mode))
                {
                    report = (ZnodeReport)Enum.Parse(typeof(ZnodeReport), hdnMode.Value, true);
                }

                return report;
            }

            set 
            { 
                hdnMode.Value = value.ToString(); 
            }
        }
        
        /// <summary>
        /// Gets or sets the report title.
        /// </summary>
        public string ReportTitle
        {
            get 
            { 
                return this._ReportTitle; 
            }

            set
            {
                this._ReportTitle = value;
                lblTitle.Text = value;
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Headers["User-Agent"].Contains("WebKit") || Request.Headers["User-Agent"].Contains("Firefox"))
            {
                objReportViewer.InteractivityPostBackMode = InteractivityPostBackMode.AlwaysSynchronous;
            }

            if (!IsPostBack)
            {
                this.BindStores();
            }
        }                

        #region Evets Methods

        protected void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("ZNodeSalesTaxDataSet_SalesTax", this.reportDataSet.Tables[0]));
        }

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Reports/default.aspx");
        }

        protected void DdlPortal_SelectedIndexChanged(object sender, EventArgs e) 
        {
            objReportViewer.Visible = false;            
        }

        protected void DdlReportGroupBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            objReportViewer.Visible = false;           
        }

        protected void BtnOrderFilter_Click(object sender, EventArgs e)
        {
            this.ShowReport();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            hdnIsPostBack.Value = "false";
            ddlPortal.SelectedIndex = -1;
            this.objReportViewer.Visible = false;
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Bind store names
        /// </summary>
        private void BindStores()
        {
            // Load StoresList
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portalList = UserStoreAccess.CheckStoreAccess(portalAdmin.GetAllPortals());

            // Portal Drop Down List
            ddlPortal.DataSource = portalList;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();
            ddlPortal.SelectedIndex = 0;

            this.objReportViewer.Visible = false;
        }

        /// <summary>
        /// Display the activity log report based on parameter.
        /// </summary>
        private void ShowReport()
        {
            ReportAdmin reportAdmin = new ReportAdmin();
            string portalId = ddlPortal.SelectedValue;
            ZnodeReport report = this.GetZnodeReportName();

            this.reportDataSet = reportAdmin.ReportList(report, DateTime.Today, DateTime.Today, string.Empty, portalId, string.Empty);

            if (this.reportDataSet.Tables[0].Rows.Count == 0)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordsFound").ToString();
                objReportViewer.Visible = false;
                return;
            }
            else
            {
                this.objReportViewer.Visible = true;
                objReportViewer.LocalReport.DataSources.Clear();
                this.objReportViewer.PageCountMode = PageCountMode.Actual; 
                this.objReportViewer.LocalReport.ReportPath = "FranchiseAdmin/Secure/Reports/TaxReport.rdlc";
                ReportParameter param1 = new ReportParameter("CurrentLanguage", System.Globalization.CultureInfo.CurrentCulture.Name);
                this.objReportViewer.LocalReport.SetParameters(new ReportParameter[] { param1 });
                this.objReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(this.LocalReport_SubreportProcessing);
                this.objReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ZNodeSalesTaxDataSet_SalesTax", this.reportDataSet.Tables[0]));
                this.objReportViewer.LocalReport.Refresh();
            }
        }

        /// <summary>
        /// Get the ZNodeReport enumeration.
        /// </summary>
        /// <returns>Returns the ZNodeReport enum object</returns>
        private ZnodeReport GetZnodeReportName()
        {
            switch (ddlReportGroupBy.SelectedIndex)
            {
                case 1:
                    return ZnodeReport.SalesTaxByMonth;
                case 2:
                    return ZnodeReport.SalesTaxByQuarter;
                default:
                    return ZnodeReport.SalesTax;
            }
        }
        #endregion
    }
}

