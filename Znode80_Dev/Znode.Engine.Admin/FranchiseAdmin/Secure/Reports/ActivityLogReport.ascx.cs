﻿using Microsoft.Reporting.WebForms;
using System;
using System.Data;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.FranchiseAdmin.Secure.Reports
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_Reports_CActivityLogReport class.
    /// </summary>
    public partial class ActivityLogReport : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private string _ReportTitle; 
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the the postback value.
        /// </summary>
        public string IsPostback
        {
            get { return hdnIsPostBack.Value; }
            set { hdnIsPostBack.Value = value; }
        }

        /// <summary>
        /// Gets or sets the mode value.
        /// </summary>
        public int Mode
        {
            get { return Convert.ToInt32(hdnMode.Value); }
            set { hdnMode.Value = value.ToString(); }
        }
        
        /// <summary>
        /// Gets or sets the report title.
        /// </summary>
        public string ReportTitle
        {
            get 
            { 
                return this._ReportTitle; 
            }

            set
            {
                this._ReportTitle = value;
                lblTitle.Text = value;
            }
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Headers["User-Agent"].Contains("WebKit") || Request.Headers["User-Agent"].Contains("Firefox"))
            {
                objReportViewer.InteractivityPostBackMode = InteractivityPostBackMode.AlwaysSynchronous;
            }

            if (!IsPostBack)
            {
                this.BindStores();
            }
        }       

        #region Evets Methods
        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Reports/default.aspx");
        }

        protected void BtnOrderFilter_Click(object sender, EventArgs e)
        {
            this.ShowReport();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            hdnIsPostBack.Value = "false";
            ddlLogCategory.SelectedIndex = -1;
            ddlPortal.SelectedIndex = -1;
            txtStartDate.Text = string.Empty;
            this.objReportViewer.Visible = false;
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Bind store names
        /// </summary>
        private void BindStores()
        {
            // Load StoresList
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portalList = UserStoreAccess.CheckStoreAccess(portalAdmin.GetAllPortals());

            // Portal Drop Down List
            ddlPortal.DataSource = portalList;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();
            ddlPortal.SelectedIndex = 0;

            this.objReportViewer.Visible = false;
        }

        /// <summary>
        /// Display the activity log report based on parameter.
        /// </summary>
        private void ShowReport()
        {
            objReportViewer.Visible = true;
            ReportAdmin reportAdmin = new ReportAdmin();
            DateTime fromDate = DateTime.Parse(txtStartDate.Text);
            string category = ddlLogCategory.SelectedIndex == 0 ? string.Empty : ddlLogCategory.SelectedItem.Text;
            string portalId = ddlPortal.SelectedValue;
            DataSet ds = reportAdmin.ReportList(ZnodeReport.ActivityLog, fromDate, DateTime.Today, string.Empty, category, portalId);
            if (ds.Tables[0].Rows.Count == 0)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordsFound").ToString();
                objReportViewer.Visible = false;
                return;
            }
            else
            {
                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.ActivityLogReport, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogReport").ToString(), "ActivityLog");

                objReportViewer.LocalReport.DataSources.Clear();
                this.objReportViewer.PageCountMode = PageCountMode.Actual; 
                ReportParameter param1 = new ReportParameter("CurrentLanguage", System.Globalization.CultureInfo.CurrentCulture.Name);
                objReportViewer.LocalReport.SetParameters(new ReportParameter[] { param1 });
                objReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ZnodeActivityLog_ZnodeActivityLog", ds.Tables[0]));
            }
        }
        #endregion
    }
}