﻿using Microsoft.Reporting.WebForms;
using System;
using System.Data;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.FranchiseAdmin.Secure.Reports
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_Reports_CSupplierReport class.
    /// </summary>
    public partial class SupplierReport : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private string _ReportTitle;
        private DataTable subReportDataSource = new DataTable();
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the control postback state.
        /// </summary>
        public string IsPostback
        {
            get { return hdnIsPostBack.Value; }
            set { hdnIsPostBack.Value = value; }
        }

        /// <summary>
        /// Gets or sets the report title.
        /// </summary>
        public string ReportTitle
        {
            get
            {
                return this._ReportTitle;
            }

            set
            {
                this._ReportTitle = value;
                lblTitle.Text = value;
            }
        }
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Headers["User-Agent"].Contains("WebKit") || Request.Headers["User-Agent"].Contains("Firefox"))
            {
                objReportViewer.InteractivityPostBackMode = InteractivityPostBackMode.AlwaysSynchronous;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Convert.ToBoolean(hdnIsPostBack.Value))
            {
                hdnIsPostBack.Value = "true";

                // Load StoresList
                PortalAdmin portalAdmin = new PortalAdmin();
                TList<ZNode.Libraries.DataAccess.Entities.Portal> portalList = UserStoreAccess.CheckStoreAccess(portalAdmin.GetAllPortals());

                // Portal Drop Down List
                ddlPortal.DataSource = portalList;
                ddlPortal.DataTextField = "StoreName";
                ddlPortal.DataValueField = "PortalID";
                ddlPortal.DataBind();
                ddlPortal.SelectedIndex = 0;

                this.objReportViewer.Visible = false;
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Report Viewer - Sub Report processing event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("ZNodeSupplier_OrderLineItem", this.subReportDataSource));
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            hdnIsPostBack.Value = "false";
            txtEndDate.Text = string.Empty;
            txtStartDate.Text = string.Empty;
            ddlPortal.SelectedIndex = -1;
            ddlSupplier.Value = "0";
            this.objReportViewer.Visible = false;
        }

        /// <summary>
        /// Get Order details click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnOrderFilter_Click(object sender, EventArgs e)
        {
            this.ShowReport();
        }

        #endregion

        #region Helper Methods
        private void ShowReport()
        {
            objReportViewer.Visible = true;
            lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleSupplierList").ToString();

            // Get Filetered Orders in DataSet
            ReportAdmin reportAdmin = new ReportAdmin();

            string supplierId = ddlSupplier.Value;
            string portalId = ddlPortal.SelectedValue;
            DataSet reportDataSet = null;
            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;

            if (!string.IsNullOrEmpty(txtStartDate.Text))
            {
                startDate = DateTime.Parse(txtStartDate.Text.Trim());
            }

            if (!string.IsNullOrEmpty(txtEndDate.Text))
            {
                endDate = DateTime.Parse(txtEndDate.Text.Trim());
            }

            reportDataSet = reportAdmin.ReportList(ZnodeReport.SupplierList, startDate, endDate, supplierId, portalId, string.Empty);

            if (reportDataSet.Tables[0].Rows.Count == 0)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordsFound").ToString();
                objReportViewer.Visible = false;
                return;
            }
            else
            {
                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.ActivityLogReport, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogSupplierReport").ToString(), "Supplier");

                this.subReportDataSource = reportDataSet.Tables[1];
                objReportViewer.Visible = true;
                objReportViewer.LocalReport.DataSources.Clear();
                this.objReportViewer.PageCountMode = PageCountMode.Actual; 
                this.objReportViewer.LocalReport.ReportPath = "FranchiseAdmin/Secure/Reports/SupplierReport.rdlc";
                ReportParameter param1 = new ReportParameter("CurrentLanguage", System.Globalization.CultureInfo.CurrentCulture.Name);
                objReportViewer.LocalReport.SetParameters(new ReportParameter[] { param1 });
                objReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ZNodeSupplier_Order", reportDataSet.Tables[0]));
                this.objReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(this.LocalReport_SubreportProcessing);
                objReportViewer.LocalReport.Refresh();
            }
        }
        #endregion
    }
}