﻿<%@ Page Title="" Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master"
    AutoEventWireup="True" EnableEventValidation="false" Inherits="Znode.Engine.FranchiseAdmin.Secure.Reports.Report" Codebehind="Report.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="OrderReport" Src="~/FranchiseAdmin/Secure/Reports/OrderReport.ascx" %>
<%@ Register TagPrefix="uc1" TagName="InventoryReport" Src="~/FranchiseAdmin/Secure/Reports/InventoryReport.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ActivityLogReport" Src="~/FranchiseAdmin/Secure/Reports/ActivityLogReport.ascx" %>
<%@ Register TagPrefix="uc1" TagName="TaxReport" Src="~/FranchiseAdmin/Secure/Reports/TaxReport.ascx" %>
<%@ Register TagPrefix="uc1" TagName="SupplierReport" Src="~/FranchiseAdmin/Secure/Reports/SupplierReport.ascx" %>
<%@ Register TagPrefix="uc1" TagName="VendorRevenue" Src="~/FranchiseAdmin/Secure/Reports/VendorRevenueReport.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ProductsSoldOnVendorSitesReport" Src="~/FranchiseAdmin/Secure/Reports/ProductsSoldOnVendorSitesReport.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h1>
        <asp:Localize runat="server" ID="LinkTextReports" Text='<%$ Resources:ZnodeAdminResource, LinkReports %>'></asp:Localize>
    </h1>
  
   
    <div style="display: block;">
        <div class="ReportLeft">
            <asp:SiteMapDataSource ID="SiteMapDataSource2" runat="server" ShowStartingNode="False"
                SiteMapProvider="ZNodeFranchiseAdminSiteMap" StartingNodeUrl="~/FranchiseAdmin/Secure/Reports/report.aspx" />
            <asp:DataList runat="server" ID="ctrlSubMenu" RepeatDirection="Horizontal" RepeatLayout="Flow"
                CssClass="ReportMenuStyle" RepeatColumns="1" DataSourceID="SiteMapDataSource2"
                ItemStyle-Wrap="true">
                <ItemStyle Width="200px" />
                <ItemTemplate>
                    <span class='<%# GetSubmenuCss(Eval("Title")) %>'>
                        <asp:LinkButton runat="server" ID="SubMenu" onmouseover="this.innerText += ' &raquo;';"
                            onmouseout="this.innerText = this.innerText.replace(' &raquo;','');" Text='<%# GetSubmenuName(Eval("Title")) %>'
                            CommandArgument='<%# Eval("Title") %>' OnClick="SubMenu_OnClick"></asp:LinkButton>
                    </span>
                </ItemTemplate>
            </asp:DataList></div>
        <div class="ReportRight">
            <div class="Reports">
                <uc1:OrderReport runat="server" ID="uxOrderReport" Mode="12" />
                <uc1:InventoryReport runat="server" ID="uxInventoryReport" Mode="13" Visible="false" />
                <uc1:ActivityLogReport runat="server" ID="uxActivityLogReport" Mode="22" Visible="false" />
                <uc1:TaxReport runat="server" ID="uxTaxReport" Mode="25" Visible="false" />
                <uc1:SupplierReport runat="server" ID="uxSupplierReport" Mode="27" Visible="false" />
                <uc1:VendorRevenue runat="server" ID="uxVendorRevenueReport" Mode="266" Visible="false" />
                <uc1:ProductsSoldOnVendorSitesReport runat="server" ID="uxProductsSoldOnVendorSitesReport" Mode="268" Visible="false" />
            </div>
        </div>
        <div style="clear: both">
        </div>
        <div class="ReportLeft">
            &nbsp;</div>
        <div class="ReportRight">
            <div class="FormView">
                <div class="FieldStyle" style="width: 100%; padding-top: 25px;">
                    <small style="width: 100%;">
                        <asp:Localize runat="server" ID="TextReport" Text='<%$ Resources:ZnodeAdminResource, HintTextReport %>'></asp:Localize>
                    </small></div>
            </div>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnSelectedReport" Value="Orders" />
</asp:Content>
