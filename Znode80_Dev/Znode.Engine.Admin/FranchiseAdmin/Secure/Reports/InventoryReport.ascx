﻿<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Reports.InventoryReport" Codebehind="InventoryReport.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>

<div class="ReportLayout">
    <h4 class="SubTitle">
        <asp:Label ID="lblTitle" runat="server"></asp:Label>
    </h4>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <div>
        <div class="SearchForm">
            <div class="RowStyle">
                <asp:PlaceHolder ID="pnlStorelist" runat="server" Visible="false">
                    <div class="ItemStyle" style="margin-right: 35px;display:none;">
                        <span class="FieldStyle"> <asp:Localize runat="server" ID="TitleSelectStore" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectStore %>'></asp:Localize></span><br /><span class="ValueStyle">
                            <asp:DropDownList ID="ddlPortal" runat="server">
                            </asp:DropDownList>
                        </span>
                    </div>
                </asp:PlaceHolder>
                <div class="ItemStyle" style="margin-right: 35px; width:150px;">
                    <asp:PlaceHolder ID="pnlCustom" runat="server" Visible="false">
                     <span class="FieldStyle"><asp:Localize runat="server" ID="TitleGetReport" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleGetReport %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ListOrderStatus" runat="server">
                                <asp:ListItem Value="0" Text='<%$ Resources:ZnodeAdminResource, DropDownTextSelect %>'></asp:ListItem>
                                <asp:ListItem Value="1" Text='<%$ Resources:ZnodeAdminResource, DropDownTextDay %>'></asp:ListItem>
                                <asp:ListItem Value="2" Text='<%$ Resources:ZnodeAdminResource, DropDownTextWeek %>'></asp:ListItem>
                                <asp:ListItem Value="3" Text='<%$ Resources:ZnodeAdminResource, DropDownTextMonth %>'></asp:ListItem>
                                <asp:ListItem Value="4" Text='<%$ Resources:ZnodeAdminResource, DropDownTextQuarter %>'></asp:ListItem>
                                <asp:ListItem Value="5" Text='<%$ Resources:ZnodeAdminResource, DropDownTextYear %>'></asp:ListItem>
                            </asp:DropDownList>
                        </span></asp:PlaceHolder>&nbsp;
                </div>
                <div class="ItemStyle" style="width:200px; text-align: right; padding-top: 15px;">
                    <asp:PlaceHolder ID="pnlSearch" runat="server" Visible="true">
                        <div class="Buttons">
                             <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                            <zn:Button runat="server" ID="btnOrderFilter" ButtonType="SubmitButton" OnClick="BtnOrderFilter_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="True"/>
                        </div>
                    </asp:PlaceHolder>
                </div>
            </div>
        </div>
    </div>
    <div align="left" class="ClearBoth">
        <br />
    </div>
    <div class="ClearBoth">
        <uc1:Spacer ID="Spacer3" spacerheight="15" spacerwidth="3" runat="server"></uc1:Spacer>
    </div>
    <div>
        <asp:Label ID="lblErrorMsg" CssClass="Error" runat="server" EnableViewState="false"></asp:Label>
    </div>
    <div>
    <rsweb:ReportViewer ShowBackButton="False" ID="objReportViewer" runat="server" Width="100%"  AsyncRendering="false"  Font-Names="Verdana"
        Font-Size="8pt" ShowExportControls="true" ShowPageNavigationControls="true" ShowCredentialPrompts="false"
        ShowDocumentMapButton="false" ShowFindControls="false" ShowPrintButton="true"
        ShowRefreshButton="false" ShowZoomControl="false">
        <LocalReport DisplayName="Report">
        </LocalReport>
    </rsweb:ReportViewer>
    </div>
    <div>
        <uc1:Spacer ID="Spacer1" spacerheight="10" spacerwidth="3" runat="server"></uc1:Spacer>
    </div>
    <asp:HiddenField runat="server" ID="hdnIsPostBack" Value="false" />
    <asp:HiddenField runat="server" ID="hdnInventorySelector" Value="false" />
    <asp:HiddenField runat="server" ID="hdnMode" Value="13" />
</div>
