﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Reporting.WebForms;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Common;

namespace  Znode.Engine.FranchiseAdmin.Secure.Reports
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_Reports_InventoryReport class.
    /// </summary>
    public partial class InventoryReport : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private DataTable subReportDataSource = new DataTable();
        private bool _StoreSelectorVisible = false;
        private bool _IntervalSelector = false;
        private string _ReportTitle;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the page post back settings.
        /// </summary>
        public string IsPostback
        {
            get { return hdnIsPostBack.Value; }
            set { hdnIsPostBack.Value = value; }
        }
        
        /// <summary>
        /// Gets or sets a value indicating whether to show or hide the store selector.
        /// </summary>
        public bool StoreSelectorVisible
        {
            get
            {
                return this._StoreSelectorVisible;
            }

            set
            {
                this._StoreSelectorVisible = value;
            }
        }
        
        /// <summary>
        /// Gets or sets a value indicating whether to show or hide the interval selector.
        /// </summary>
        public bool IntervalSelector
        {
            get
            {
                if (hdnInventorySelector.Value.Trim().Length > 0)
                {
                    return Convert.ToBoolean(hdnInventorySelector.Value);
                }
                else
                {
                    return this._IntervalSelector;
                }
            }

            set
            {
                this._IntervalSelector = value;
                hdnInventorySelector.Value = value.ToString();
                pnlCustom.Visible = this._IntervalSelector;
                pnlSearch.Visible = true;
                if (!pnlCustom.Visible && !pnlStorelist.Visible)
                {
                    pnlSearch.Visible = false;
                }
            }
        }

        /// <summary>
        /// Gets or sets the report mode.
        /// </summary>
        public ZnodeReport Mode
        {
            get
            {
                string mode = hdnMode.Value;
                ZnodeReport report = ZnodeReport.ServiceRequest;
                if (Enum.IsDefined(typeof(ZnodeReport), mode))
                {
                    report = (ZnodeReport)Enum.Parse(typeof(ZnodeReport), hdnMode.Value, true);
                }

                return report;
            }

            set 
            { 
                hdnMode.Value = value.ToString(); 
            }
        }
        
        /// <summary>
        /// Gets or sets the report title.
        /// </summary>
        public string ReportTitle
        {
            get 
            {
                return this._ReportTitle; 
            }

            set
            {
                this._ReportTitle = value;
                lblTitle.Text = value;
            }
        }
        #endregion

        #region Page Load Event

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Headers["User-Agent"].Contains("WebKit") || Request.Headers["User-Agent"].Contains("Firefox"))
            {
                objReportViewer.InteractivityPostBackMode = InteractivityPostBackMode.AlwaysSynchronous;
            }
        }
        #endregion

        #region Page Pre Render
        /// <summary>
        /// Page PreRender Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_PreRender(object sender, EventArgs e)
        {
            pnlSearch.Visible = true;

            if (!Convert.ToBoolean(this.IsPostback))
            {
                // Set interval selector visiblity based on parameter.
                pnlCustom.Visible = this.IntervalSelector;

                hdnIsPostBack.Value = "true";

                pnlStorelist.Visible = false;

                this.objReportViewer.Visible = false;

                this.BindStores();

                if (!pnlCustom.Visible && !pnlStorelist.Visible)
                {
                    pnlSearch.Visible = false;
                    this.GenerateReport();
                }
            }
        } 
        
        #endregion

        #region Events
        /// <summary>
        /// Report Viewer - Sub Report processing event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            if (this.Mode == ZnodeReport.ServiceRequest)
            {
                e.DataSources.Add(new ReportDataSource("ZNodeCaseRequestDataSet_CaseRequestDetails", this.subReportDataSource));
            }

            if (this.Mode == ZnodeReport.Picklist)
            {
                e.DataSources.Add(new ReportDataSource("ZNodePickListDataSet_PickListItem", this.subReportDataSource));
            }
        }

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Reports/default.aspx");
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            ddlPortal.SelectedIndex = -1;
            ListOrderStatus.SelectedIndex = -1;
            this.objReportViewer.Visible = false;
        }

        /// <summary>
        /// Order filte click event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnOrderFilter_Click(object sender, EventArgs e)
        {
            this.GenerateReport();
        }        

        #endregion

        #region Helper Methods

        /// <summary>
        /// Bind store names.
        /// </summary>
        private void BindStores()
        {
            // Load Stores List
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portalList = UserStoreAccess.CheckStoreAccess(portalAdmin.GetAllPortals());

            // Portal Drop Down List
            ddlPortal.Items.Clear();
            ddlPortal.DataSource = portalList;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();
            ddlPortal.SelectedIndex = 0;

            string[] period = (string[])Enum.GetNames(typeof(ZnodeReportInterval));
            ListOrderStatus.DataSource = period;
        }

        /// <summary>
        /// Generate the report.
        /// </summary>
        private void GenerateReport()
        {
            if (ListOrderStatus.SelectedItem.Value == "0")
            {
                if (!(this.Mode == ZnodeReport.Picklist) && !(this.Mode == ZnodeReport.ReOrder) 
                    && !(this.Mode == ZnodeReport.ServiceRequest) && !(this.Mode == ZnodeReport.EmailOptInCustomer))
                {
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSelectAnyCriteria").ToString();
                    objReportViewer.Visible = false;
                    return;
                }
            }
            pnlCustom.Visible = this.IntervalSelector;
            ZnodeReport selectedReport = this.GetZnodeReportName();
            string period = ListOrderStatus.SelectedItem.Text;
            string portalId = ddlPortal.SelectedValue;
            string reportPath = string.Empty;
            string reportSourceName = string.Empty;

            DataSet reportDataSet = null;
            ReportAdmin reportAdmin = new ReportAdmin();

            switch (this.Mode)
            {
                case ZnodeReport.FrequentCustomer:
                    reportPath = "FranchiseAdmin/Secure/Reports/FrequentCustomer.rdlc";
                    reportSourceName = "ZNodeFrequentCustomerDataSet_FrequentCustomer";
                    break;
                case ZnodeReport.TopSpendingCustomer:
                    reportPath = "FranchiseAdmin/Secure/Reports/VolumeCustomer.rdlc";
                    reportSourceName = "ZNodeTopSpendingCustomerDataSet_TopSpendingCustomer";
                    break;
                case ZnodeReport.TopEarningProduct:
                    reportPath = "FranchiseAdmin/Secure/Reports/PopularProduct.rdlc";
                    reportSourceName = "ZnodeTopEarningProductDataSet_TopEarningProduct";
                    break;
                case ZnodeReport.BestSeller:
                    reportPath = "FranchiseAdmin/Secure/Reports/BestSellers.rdlc";
                    reportSourceName = "ZNodeBestSellerDataSet_BestSeller";
                    break;
                case ZnodeReport.CouponUsage:
                    reportPath = "FranchiseAdmin/Secure/Reports/CouponUsage.rdlc";
                    reportSourceName = "ZNodeCoupon_ZNodeCoupon";
                    break;
                case ZnodeReport.AffiliateOrder:
                    reportPath = "FranchiseAdmin/Secure/Reports/Affiliate.rdlc";
                    reportSourceName = "ZNodeAffiliate_ZNodeAffiliate";
                    break;
                case ZnodeReport.ServiceRequest:
                    reportPath = "FranchiseAdmin/Secure/Reports/Feedback.rdlc";
                    reportSourceName = "ZNodeCaseRequestDataSet_CaseRequest";
                    break;
                case ZnodeReport.ReOrder:
                    reportPath = "FranchiseAdmin/Secure/Reports/ReOrder.rdlc";
                    reportSourceName = "ZNodeReorderDataSet_Reorder";
                    break;
                case ZnodeReport.EmailOptInCustomer:
                    reportPath = "FranchiseAdmin/Secure/Reports/EmailOptIn.rdlc";
                    reportSourceName = "ZNodeEmailOptInCustomerDataSet_EmailOptInCustomer";
                    break;
                case ZnodeReport.Picklist:
                    reportPath = "FranchiseAdmin/Secure/Reports/Inventory.rdlc";
                    reportSourceName = "ZNodePickListDataSet_PickList";
                    break;
            }

            reportDataSet = reportAdmin.ReportList(selectedReport, DateTime.Today, DateTime.Today, string.Empty, portalId, string.Empty);

            // Description decoded for ServiceRequest Report
            if (this.Mode == ZnodeReport.ServiceRequest)
            {
                if (reportDataSet.Tables[1].Rows.Count > 0)
                {
                    for (int index = 0; index < reportDataSet.Tables[1].Rows.Count; index++)
                    {
                        reportDataSet.Tables[1].Rows[index]["Description"] = Server.HtmlDecode(reportDataSet.Tables[1].Rows[index]["Description"].ToString());
                    }
                }
            }

            if (reportDataSet.Tables.Count == 0 || reportDataSet.Tables[0].Rows.Count == 0)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordsFound").ToString();
                objReportViewer.Visible = false;
                return;
            }
            else
            {
                this.BindReport(reportPath, reportSourceName, reportDataSet);
            }

            string reportName = reportPath.ToString();
            reportName = reportName.Remove(0, 21);
            reportName = reportName.Remove(reportName.Length - 5, 5);

            // Log Activity
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.ActivityLogReport, UserStoreAccess.GetCurrentUserName(), null, null, null, null, reportName + " - Report", reportName);
        }

        /// <summary>
        /// Bind the report
        /// </summary>
        /// <param name="reportPath">Report path (*.rdlc)</param>
        /// <param name="reportSourceName">Report source name.</param>
        /// <param name="ds">Dataset bind to the report.</param>
        private void BindReport(string reportPath, string reportSourceName, DataSet ds)
        {
            this.IsPostback = "true";
            this.objReportViewer.Visible = true;

            this.objReportViewer.LocalReport.DataSources.Clear();
            this.objReportViewer.PageCountMode = PageCountMode.Actual; 
            this.objReportViewer.LocalReport.ReportPath = reportPath;
            ReportParameter param1 = new ReportParameter("CurrentLanguage", System.Globalization.CultureInfo.CurrentCulture.Name);
            objReportViewer.LocalReport.SetParameters(new ReportParameter[] { param1 });
            if (this.Mode == ZnodeReport.Picklist || this.Mode == ZnodeReport.ServiceRequest)
            {
                this.subReportDataSource = ds.Tables[1];
                this.objReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(this.LocalReport_SubreportProcessing);
            }

            this.objReportViewer.LocalReport.DataSources.Add(new ReportDataSource(reportSourceName, ds.Tables[0]));
            this.objReportViewer.LocalReport.Refresh();
        }

        /// <summary>
        /// Get the selected ZnodeReport enumeration object
        /// </summary>
        /// <returns>Returns the selected ZnodeReport</returns>
        private ZnodeReport GetZnodeReportName()
        {
            // Selected report
            ZnodeReport report = ZnodeReport.None;

            // Selected interval.
            string selectedinterval = ListOrderStatus.SelectedItem.Text;
            ZnodeReportInterval interval = ZnodeReportInterval.None;

            // Convert selected string to ZnodeReportinterval enumeration
            if (Enum.IsDefined(typeof(ZnodeReportInterval), selectedinterval))
            {
                interval = (ZnodeReportInterval)Enum.Parse(typeof(ZnodeReportInterval), selectedinterval, true);
            }

            switch (this.Mode)
            {
                case ZnodeReport.FrequentCustomer:
                    if (interval == ZnodeReportInterval.Day)
                    {
                        report = ZnodeReport.FrequentCustomerByDay;
                    }
                    else if (interval == ZnodeReportInterval.Week)
                    {
                        report = ZnodeReport.FrequentCustomerByWeek;
                    }
                    else if (interval == ZnodeReportInterval.Month)
                    {
                        report = ZnodeReport.FrequentCustomerByMonth;
                    }
                    else if (interval == ZnodeReportInterval.Quarter)
                    {
                        report = ZnodeReport.FrequentCustomerByQuarter;
                    }
                    else if (interval == ZnodeReportInterval.Year)
                    {
                        report = ZnodeReport.FrequentCustomerByYear;
                    }
                    else
                    {
                        report = ZnodeReport.FrequentCustomer;
                    }

                    break;

                case ZnodeReport.TopSpendingCustomer:
                    if (interval == ZnodeReportInterval.Day)
                    {
                        report = ZnodeReport.TopSpendingCustomerByDay;
                    }
                    else if (interval == ZnodeReportInterval.Week)
                    {
                        report = ZnodeReport.TopSpendingCustomerByWeek;
                    }
                    else if (interval == ZnodeReportInterval.Month)
                    {
                        report = ZnodeReport.TopSpendingCustomerByMonth;
                    }
                    else if (interval == ZnodeReportInterval.Quarter)
                    {
                        report = ZnodeReport.TopSpendingCustomerByQuarter;
                    }
                    else if (interval == ZnodeReportInterval.Year)
                    {
                        report = ZnodeReport.TopSpendingCustomerByYear;
                    }
                    else
                    {
                        report = ZnodeReport.TopSpendingCustomer;
                    }

                    break;

                case ZnodeReport.TopEarningProduct:
                    if (interval == ZnodeReportInterval.Day)
                    {
                        report = ZnodeReport.TopEarningProductByDay;
                    }
                    else if (interval == ZnodeReportInterval.Week)
                    {
                        report = ZnodeReport.TopEarningProductByWeek;
                    }
                    else if (interval == ZnodeReportInterval.Month)
                    {
                        report = ZnodeReport.TopEarningProductByMonth;
                    }
                    else if (interval == ZnodeReportInterval.Quarter)
                    {
                        report = ZnodeReport.TopEarningProductByQuarter;
                    }
                    else if (interval == ZnodeReportInterval.Year)
                    {
                        report = ZnodeReport.TopEarningProductByYear;
                    }
                    else
                    {
                        report = ZnodeReport.TopEarningProduct;
                    }

                    break;

                case ZnodeReport.BestSeller:
                    if (interval == ZnodeReportInterval.Day)
                    {
                        report = ZnodeReport.BestSellerByDay;
                    }
                    else if (interval == ZnodeReportInterval.Week)
                    {
                        report = ZnodeReport.BestSellerByWeek;
                    }
                    else if (interval == ZnodeReportInterval.Month)
                    {
                        report = ZnodeReport.BestSellerByMonth;
                    }
                    else if (interval == ZnodeReportInterval.Quarter)
                    {
                        report = ZnodeReport.BestSellerByQuarter;
                    }
                    else if (interval == ZnodeReportInterval.Year)
                    {
                        report = ZnodeReport.BestSellerByYear;
                    }
                    else
                    {
                        report = ZnodeReport.BestSeller;
                    }

                    break;

                case ZnodeReport.CouponUsage:

                    if (interval == ZnodeReportInterval.Day)
                    {
                        report = ZnodeReport.CouponUsageByDay;
                    }
                    else if (interval == ZnodeReportInterval.Week)
                    {
                        report = ZnodeReport.CouponUsageByWeek;
                    }
                    else if (interval == ZnodeReportInterval.Month)
                    {
                        report = ZnodeReport.CouponUsageByMonth;
                    }
                    else if (interval == ZnodeReportInterval.Quarter)
                    {
                        report = ZnodeReport.CouponUsageByQuarter;
                    }
                    else if (interval == ZnodeReportInterval.Year)
                    {
                        report = ZnodeReport.CouponUsageByYear;
                    }
                    else
                    {
                        report = ZnodeReport.CouponUsage;
                    }

                    break;

                case ZnodeReport.AffiliateOrder:

                    if (interval == ZnodeReportInterval.Day)
                    {
                        report = ZnodeReport.AffiliateOrderByDay;
                    }
                    else if (interval == ZnodeReportInterval.Week)
                    {
                        report = ZnodeReport.AffiliateOrderByWeek;
                    }
                    else if (interval == ZnodeReportInterval.Month)
                    {
                        report = ZnodeReport.AffiliateOrderByMonth;
                    }
                    else if (interval == ZnodeReportInterval.Quarter)
                    {
                        report = ZnodeReport.AffiliateOrderByQuarter;
                    }
                    else if (interval == ZnodeReportInterval.Year)
                    {
                        report = ZnodeReport.AffiliateOrderByYear;
                    }
                    else
                    {
                        report = ZnodeReport.AffiliateOrder;
                    }

                    break;

                case ZnodeReport.EmailOptInCustomer:
                    report = ZnodeReport.EmailOptInCustomer;
                    break;

                case ZnodeReport.Picklist:
                    report = ZnodeReport.Picklist;
                    break;

                case ZnodeReport.ServiceRequest:
                    report = ZnodeReport.ServiceRequest;
                    break;

                case ZnodeReport.ReOrder:
                    report = ZnodeReport.ReOrder;
                    break;
            }

            // Return the selected report name
            return report;
        }
        #endregion
    }
}