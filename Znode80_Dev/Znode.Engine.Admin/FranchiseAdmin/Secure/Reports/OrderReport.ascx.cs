﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.Reporting.WebForms;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Common;

namespace  Znode.Engine.FranchiseAdmin.Secure.Reports
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_Reports_OrderReport class.
    /// </summary>
    public partial class OrderReport : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private string _ReportTitle;
        private bool _OrderStatusSelector = false;
        private DataTable subReportDataSource = new DataTable();
        #endregion

        #region Public Properties        
        /// <summary>
        /// Gets or sets the control postback settings
        /// </summary>
        public string IsPostback
        {
            get { return hdnIsPostBack.Value; }
            set { hdnIsPostBack.Value = value; }
        }

        /// <summary>
        /// Gets or sets the report mode.
        /// </summary>
        public ZnodeReport Mode
        {
            get
            {
                string mode = hdnMode.Value;
                ZnodeReport report = ZnodeReport.ServiceRequest;
                if (Enum.IsDefined(typeof(ZnodeReport), mode))
                {
                    report = (ZnodeReport)Enum.Parse(typeof(ZnodeReport), hdnMode.Value, true);
                }

                return report;
            }

            set 
            { 
                hdnMode.Value = value.ToString(); 
            }
        }       

        /// <summary>
        /// Gets or sets the report title
        /// </summary>
        public string ReportTitle
        {
            get 
            {
                return this._ReportTitle; 
            }

            set
            {
                this._ReportTitle = value;
                lblTitle.Text = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show or hide the order state selector control.
        /// </summary>
        public bool OrderStatusSelector
        {
            get 
            {
                return this._OrderStatusSelector; 
            }

            set
            {
                this._OrderStatusSelector = value;
                pnlOrderStatus.Visible = this._OrderStatusSelector;
            }
        }
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Headers["User-Agent"].Contains("WebKit") || Request.Headers["User-Agent"].Contains("Firefox"))
            {
                objReportViewer.InteractivityPostBackMode = InteractivityPostBackMode.AlwaysSynchronous;
            }
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            if (!Convert.ToBoolean(hdnIsPostBack.Value))
            {
                this.objReportViewer.Visible = false;
                this.IsPostback = "true";
                this.BindOrderState();

                this.BindStores();
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Report Viewer - Sub Report processing event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LocalReport_SubreportProcessing(object sender, SubreportProcessingEventArgs e)
        {
            e.DataSources.Add(new ReportDataSource("ZNodeOrderLineItems_ZNodeOrderLineItem", this.subReportDataSource));
        }

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Reports/default.aspx");
        }

        protected void BtnOrderFilter_Click(object sender, EventArgs e)
        {
            this.ShowReport();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            if (this.Mode == ZnodeReport.Orders)
            {
                pnlprofile.Visible = false;
            }

            if (this.Mode == ZnodeReport.Accounts)
            {
                pnlprofile.Visible = false;
            }

            txtEndDate.Text = string.Empty;
            txtStartDate.Text = string.Empty;
            ddlOrderStatus.SelectedIndex = 0;
            ddlPortal.SelectedIndex = 0;
            this.objReportViewer.Visible = false;
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Bind store list.
        /// </summary>
        private void BindStores()
        {
            // Load StoresList
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portalList = UserStoreAccess.CheckStoreAccess(portalAdmin.GetAllPortals());

            ddlPortal.Items.Clear();

            // Portal Drop Down List
            ddlPortal.DataSource = portalList;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalId";
            ddlPortal.DataBind();
            ddlPortal.SelectedIndex = 0;
        }

        /// <summary>
        /// Bind order state.
        /// </summary>
        private void BindOrderState()
        {
            // Load Order State Item 
            OrderAdmin orderAdmin = new OrderAdmin();
            ddlOrderStatus.DataSource = orderAdmin.GetAllOrderStates();
            ddlOrderStatus.Items.Clear();
            ddlOrderStatus.DataTextField = "OrderStateName";
            ddlOrderStatus.DataValueField = "OrderStateId";
            ddlOrderStatus.DataBind();
            ListItem item1 = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAll").ToString().ToUpper(), "0");
            ddlOrderStatus.Items.Insert(0, item1);
            ddlOrderStatus.SelectedIndex = 0;
        }

        /// <summary>
        /// Show the report
        /// </summary>
        private void ShowReport()
        {
            // Get Filetered Orders in DataSet
            ReportAdmin reportAdmin = new ReportAdmin();

            string orderStatus = ddlOrderStatus.SelectedValue;
            string portalId = ddlPortal.SelectedValue;

            DateTime startDate = DateTime.MinValue;
            DateTime endDate = DateTime.MinValue;
            if (!string.IsNullOrEmpty(txtStartDate.Text))
            {
                startDate = DateTime.Parse(txtStartDate.Text.Trim());
            }

            if (!string.IsNullOrEmpty(txtEndDate.Text))
            {
                endDate = DateTime.Parse(txtEndDate.Text.Trim());
            }

            DataSet reportDataSet = null;
            objReportViewer.LocalReport.DataSources.Clear();
            this.objReportViewer.PageCountMode = PageCountMode.Actual; 
            if (this.Mode == ZnodeReport.Orders)
            {
                // Get order header DataSet
                reportDataSet = reportAdmin.ReportList(ZnodeReport.Orders, startDate, endDate, string.Empty, portalId, orderStatus);

                // Get order line item DataSet
                this.subReportDataSource = reportDataSet.Tables[1];
                this.objReportViewer.LocalReport.ReportPath = "FranchiseAdmin/Secure/Reports/Orders.rdlc";
                objReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ZNodeOrderDataSet_ZNodeOrder", reportDataSet.Tables[0]));
                this.objReportViewer.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(this.LocalReport_SubreportProcessing);
            }
            else if (this.Mode == ZnodeReport.Accounts)
            {
                // Custom1 parameter used here to send store name
                reportDataSet = reportAdmin.ReportList(ZnodeReport.Accounts, startDate, endDate, string.Empty, portalId, string.Empty);
                this.objReportViewer.LocalReport.ReportPath = "FranchiseAdmin/Secure/Reports/Accounts.rdlc";
                objReportViewer.LocalReport.DataSources.Add(new ReportDataSource("ZNodeAccountDataSet_ZNodeAccount", reportDataSet.Tables[0]));
            }

            if (reportDataSet.Tables[0].Rows.Count == 0)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordsFound").ToString();
                objReportViewer.Visible = false;
                return;
            }
            else
            {
                this.objReportViewer.Visible = true;
                ReportParameter param1 = new ReportParameter("CurrentLanguage", System.Globalization.CultureInfo.CurrentCulture.Name);
                objReportViewer.LocalReport.SetParameters(new ReportParameter[] { param1 });
                objReportViewer.LocalReport.Refresh();
            }
        }
        #endregion
    }
}