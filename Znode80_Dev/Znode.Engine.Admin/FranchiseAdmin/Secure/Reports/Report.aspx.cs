﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Reports
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_Reports_Report class.
    /// </summary>
    public partial class Report : System.Web.UI.Page
    {
        private const string ReportOrder = "Orders";
        private const string ReportAccounts = "Accounts";
        private const string ReportBestSellers = "Best Sellers";
        private const string ReportServiceRequest = "Service Requests";
        private const string ReportEmailOptn = "Email Opt-In Customers";
        private const string ReportInventory = "Inventory Re-Order";
        private const string ReportTopSpending = "Top Spending Customers";
        private const string ReportMostFrequent = "Most Frequent Customers";
        private const string ReportTopEarning = "Top Earning Products";
        private const string ReportOrderPickList = "Order Pick List";
        private const string ReportCouponUsage = "Coupon Usage";
        private const string ReportAffiliateOrders = "Affiliate Orders";
        private const string ReportActivityLog = "Activity Log";
        private const string ReportSalesTax = "Sales Tax";
        private const string ReportSupplierList = "Supplier List";
        private const string ReportVendorRevenue = "Franchise Orders";
        private const string ReportVendorProductRevenue = "Sale By Products";
        private const string ReportProductsSoldOnVendorSites = "Products Sold on Vendors Sites";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Mode"] != null)
            {
                this.BindReports(Server.HtmlDecode(Request.QueryString["Mode"].ToString()));
				Session["SelectedMenuItem"] = Request.QueryString["Mode"];
            }

            if (!IsPostBack)
            {
                hdnSelectedReport.Value = "Orders";
				if (Session["SelectedMenuItem"] != null)
				{
					hdnSelectedReport.Value = Session["SelectedMenuItem"].ToString();
					Session["SelectedMenuItem"] = null;
				}
                uxOrderReport.ReportTitle = hdnSelectedReport.Value;
            }          
        }

        protected void Page_PreRender(object sender, EventArgs e)
        {
            ctrlSubMenu.DataBind();

            GC.Collect();
        }

        /// <summary>
        /// Gets submenu CSS
        /// </summary>
        /// <param name="url">Url to get the submenu</param>
        /// <returns>Returns the submenu css</returns>
        protected string GetSubmenuCss(object url)
        {
            if (hdnSelectedReport.Value == url.ToString())
            {
                return "ReportSelectedStyle";
            }
            else
            {
                return "ReportMenuItemStyle";
            }
        }

        /// <summary>
        /// Get the submenu name.
        /// </summary>
        /// <param name="url">Url to format.</param>
        /// <returns>Returns the submenu.</returns>
        protected string GetSubmenuName(object url)
        {
            if (hdnSelectedReport.Value == url.ToString())
            {
                return url.ToString() + " &raquo";
            }
            else
            {
                return url.ToString();
            }
        }

        protected void SubMenu_OnClick(object sender, EventArgs e)
        {
            this.ClearReportSession();

            LinkButton obj = sender as LinkButton;

            this.BindReports(obj.CommandArgument);
        }

        /// <summary>
        /// Bind the reports.
        /// </summary>
        /// <param name="reportName">Report name to bind.</param>
        private void BindReports(string reportName)
        {
            uxSupplierReport.Visible = false;
            uxTaxReport.Visible = false;
            uxActivityLogReport.Visible = false;
            uxInventoryReport.Visible = false;
            uxOrderReport.Visible = false;
            uxVendorRevenueReport.Visible = false;
            uxProductsSoldOnVendorSitesReport.Visible = false;
            switch (reportName)
            {
                case ReportOrder:
                    uxOrderReport.Visible = true;
                    uxOrderReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.Orders;
                    uxOrderReport.IsPostback = "false";
                    uxOrderReport.OrderStatusSelector = true;
                    uxOrderReport.DataBind();
                    hdnSelectedReport.Value = "Orders";
                    uxOrderReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportAccounts:
                    uxOrderReport.Visible = true;
                    uxOrderReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.Accounts;
                    uxOrderReport.IsPostback = "false";
                    uxOrderReport.OrderStatusSelector = false;
                    uxOrderReport.DataBind();
                    hdnSelectedReport.Value = "Accounts";
                    uxOrderReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportBestSellers:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.BestSeller;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    hdnSelectedReport.Value = "Best Sellers";
                    uxInventoryReport.IntervalSelector = true;
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportServiceRequest:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.ServiceRequest;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = false;
                    hdnSelectedReport.Value = "Service Requests";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportEmailOptn:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.EmailOptInCustomer;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = false;
                    hdnSelectedReport.Value = "Email Opt-In Customers";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportInventory:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.ReOrder;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = false;
                    hdnSelectedReport.Value = "Inventory Re-Order";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportTopSpending:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.TopSpendingCustomer;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = true;
                    hdnSelectedReport.Value = "Top Spending Customers";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportMostFrequent:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.FrequentCustomer;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = true;
                    hdnSelectedReport.Value = "Most Frequent Customers";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportTopEarning:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.TopEarningProduct;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = true;
                    hdnSelectedReport.Value = "Top Earning Products";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportOrderPickList:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.Picklist;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = false;
                    hdnSelectedReport.Value = "Order Pick List";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportCouponUsage:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.CouponUsage;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    hdnSelectedReport.Value = "Coupon Usage";
                    uxInventoryReport.IntervalSelector = true;
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportAffiliateOrders:
                    uxInventoryReport.Visible = true;
                    uxInventoryReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.AffiliateOrder;
                    uxInventoryReport.IsPostback = "false";
                    uxInventoryReport.DataBind();
                    uxInventoryReport.IntervalSelector = true;
                    hdnSelectedReport.Value = "Affiliate Orders";
                    uxInventoryReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportActivityLog:
                    uxActivityLogReport.Visible = true;
                    uxActivityLogReport.Mode = 22;
                    uxActivityLogReport.IsPostback = "false";
                    uxActivityLogReport.DataBind();
                    hdnSelectedReport.Value = "Activity Log";
                    uxActivityLogReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportSalesTax:
                    uxTaxReport.Visible = true;
                    uxTaxReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.SalesTax;
                    uxTaxReport.IsPostback = "false";
                    uxTaxReport.DataBind();
                    hdnSelectedReport.Value = "Sales Tax";
                    uxTaxReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportSupplierList:
                    uxSupplierReport.Visible = true;
                    uxSupplierReport.IsPostback = "false";
                    uxSupplierReport.DataBind();
                    hdnSelectedReport.Value = "Supplier List";
                    uxSupplierReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportVendorRevenue:
                    uxVendorRevenueReport.Visible = true;
                    uxVendorRevenueReport.IsPostback = "false";
                    uxVendorRevenueReport.DataBind();
                    uxVendorRevenueReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.VendorRevenue;
                    hdnSelectedReport.Value = "Franchise Orders";
                    uxVendorRevenueReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportVendorProductRevenue:
                    uxVendorRevenueReport.Visible = true;
                    uxVendorRevenueReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.VendorProductRevenue;
                    uxVendorRevenueReport.IsPostback = "false";
                    uxVendorRevenueReport.DataBind();
                    hdnSelectedReport.Value = "Sale By Products";
                    uxVendorRevenueReport.ReportTitle = hdnSelectedReport.Value;
                    break;
                case ReportProductsSoldOnVendorSites:
                    uxProductsSoldOnVendorSitesReport.Visible = true;
                    uxProductsSoldOnVendorSitesReport.Mode = ZNode.Libraries.DataAccess.Custom.ZnodeReport.ProductSoldOnVendorSites;
                    uxProductsSoldOnVendorSitesReport.IsPostback = "false";
                    uxProductsSoldOnVendorSitesReport.DataBind();
                    hdnSelectedReport.Value = "Products Sold on Vendors Sites";
                    uxProductsSoldOnVendorSitesReport.ReportTitle = hdnSelectedReport.Value;
                    break;
            }
        }

        /// <summary>
        /// Clear the report data from session
        /// </summary>
        private void ClearReportSession()
        {
            if (Session.Count > 0 && Session != null)
            {
                for (int reportSessionIndex = 0; reportSessionIndex < Session.Count; reportSessionIndex++)
                {
                    if (Session[reportSessionIndex] != null && Session[reportSessionIndex].GetType().ToString().Contains("Microsoft.Reporting.WebForms.ReportHierarchy"))
                    {
                        Session.RemoveAt(reportSessionIndex);
                    }
                }
            }

            GC.Collect();
        }
    }
}