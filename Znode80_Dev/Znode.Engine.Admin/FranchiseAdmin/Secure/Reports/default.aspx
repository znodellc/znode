<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Reports.Default" Codebehind="default.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h1>Reports</h1>
    <p>Use the links on this page to view various reports. You can also write your own custom reports using the templates provided in your web project.</p>
    
	<div class="LandingPage">
		<hr />
		<div style="margin-right:10px;">
			<div class="Shortcut"><a id="A3" href="~/FranchiseAdmin/Secure/reports/ReportList.aspx?filter=12" runat="server">Orders</a></div>
			<div class="Shortcut"><a id="A12" href="~/FranchiseAdmin/Secure/reports/ReportList.aspx?filter=21" runat="server">Accounts</a></div> 
			<div class="Shortcut"><a id="A21" href="~/FranchiseAdmin/Secure/reports/InventoryReports.aspx?filter=20" runat="server">Best Sellers</a></div>  
			<div class="Shortcut"><a id="A20" href="~/FranchiseAdmin/Secure/reports/InventoryReports.aspx?filter=19" runat="server">Service Requests</a></div>  
			<div class="Shortcut"><a id="A15" href="~/FranchiseAdmin/Secure/reports/InventoryReports.aspx?filter=14" runat="server">Email Opt-In Customers</a></div>
			<div class="Shortcut"><a id="A19" href="~/FranchiseAdmin/Secure/reports/InventoryReports.aspx?filter=18" runat="server">Inventory Re-Order</a></div> 
			<div class="Shortcut"><a id="A16" href="~/FranchiseAdmin/Secure/reports/InventoryReports.aspx?filter=15" runat="server">Most Frequent Customers</a></div>
			<div class="Shortcut"><a id="A17" href="~/FranchiseAdmin/Secure/reports/InventoryReports.aspx?filter=16" runat="server">Top Spending Customers</a></div> 
		</div>
	    
		<div>
			<div class="Shortcut"><a id="A18" href="~/FranchiseAdmin/Secure/reports/InventoryReports.aspx?filter=17" runat="server">Top Earning Products</a></div> 
			<div class="Shortcut"><a id="A14" href="~/FranchiseAdmin/Secure/reports/InventoryReports.aspx?filter=13" runat="server">Order Pick List</a></div>   
			<div class="Shortcut"><a id="A1" href="~/FranchiseAdmin/Secure/reports/ActivityLogReport.aspx?filter=22" runat="server">Activity Log</a></div> 
			<div class="Shortcut"><a id="A2" href="~/FranchiseAdmin/Secure/reports/InventoryReports.aspx?filter=24" runat="server">Coupon Usage</a></div> 
			<div class="Shortcut"><a id="A4" href="~/FranchiseAdmin/Secure/reports/TaxReport.aspx?filter=25" runat="server">Sales Tax</a></div> 
			<div class="Shortcut"><a id="A5" href="~/FranchiseAdmin/Secure/reports/InventoryReports.aspx?filter=26" runat="server">Franchise Orders</a></div> 
			<div class="Shortcut"><a id="A6" href="~/FranchiseAdmin/Secure/reports/SupplierReport.aspx?filter=27" runat="server">Supplier List</a></div> 
		</div>            
    </div>
</asp:Content>
