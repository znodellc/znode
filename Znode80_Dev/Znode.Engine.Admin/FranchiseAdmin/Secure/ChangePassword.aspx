<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Changepassword" Title="Change Password" CodeBehind="ChangePassword.aspx.cs" %>

<%@ Register Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form Search">
        <div align="center">
            <div class="LeftFloat" style="width: 90%; text-align: left">
                <h1>
                    <asp:Localize ID="TitleChangePassword" Text='<%$ Resources:ZnodeAdminResource,TitleChangePassword%>' runat="server"></asp:Localize>
                </h1>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="LeftFloat">
                <p>
                    <asp:Localize ID="TextChangePassword" Text='<%$ Resources:ZnodeAdminResource,TextChangePassword%>' runat="server"></asp:Localize>
                </p>
            </div>
        </div>
        <div class="ClearBoth"></div>
        <div>
            <uc1:spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:spacer>
        </div>
        <asp:ChangePassword ID="AdminChangePassword" runat="server" OnCancelButtonClick="CancelPushButton_Click"
            ContinueDestinationPageUrl="~/account.aspx" DisplayUserName="False" ChangePasswordTitleText=""
            OnChangePasswordError="ChangePassword1_ChangePasswordError" OnChangingPassword="AdminChangePassword_ChangingPassword">
            <SuccessTemplate>
                <div class="Row">
                    <div align="left">
                        <asp:Localize ID="TextConfirmation" Text='<%$ Resources:ZnodeAdminResource,TextConfirmation%>' runat="server"></asp:Localize>
                    </div>
                    <div class="Success">
                        <asp:Localize ID="TextPasswordChanged" Text='<%$ Resources:ZnodeAdminResource,TextPasswordChanged%>' runat="server"></asp:Localize>
                    </div>
                    <div align="right" colspan="2">
                        <zn:Button  ID="ContinuePushButton" OnClick="ContinuePushButton_Click" runat="server" Width="100px"
                            CausesValidation="False" CommandName="Continue" CssClass="Button" Text='<%$ Resources:ZnodeAdminResource,ButtonContinue%>' />
                    </div>
                </div>
            </SuccessTemplate>
            <ChangePasswordTemplate>
                <div>
                    <div align="left" style="color: red" class="Error">
                        <asp:Literal ID="PasswordFailureText" runat="server" EnableViewState="False"></asp:Literal>
                    </div>
                </div>
                <div class="CPassword">
                    <span align="right" class="FieldStyle">
                        <asp:Label ID="CurrentPasswordLabel" runat="server" AssociatedControlID="CurrentPassword" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleCurrentPassword%>'></asp:Label></span>
                    <span class="ValueStyle">
                        <asp:TextBox ID="CurrentPassword" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="CurrentPasswordRequired" runat="server" ControlToValidate="CurrentPassword"
                            ErrorMessage='<%$ Resources:ZnodeAdminResource,RequiredPassword%>' ToolTip='<%$ Resources:ZnodeAdminResource,RequiredPassword%>' ValidationGroup="ChangePassword1"
                            CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                    </span>
                </div>
                <div class="CPassword">
                    <span align="right" class="FieldStyle">
                        <asp:Label ID="NewPasswordLabel" runat="server" AssociatedControlID="NewPassword" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleNewPassword%>'></asp:Label></span>
                    <span class="ValueStyle">
                        <asp:TextBox ID="NewPassword" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="NewPasswordRequired" runat="server" ControlToValidate="NewPassword"
                            ErrorMessage='<%$ Resources:ZnodeAdminResource,RequiredNewPassword%>' ToolTip='<%$ Resources:ZnodeAdminResource,RequiredNewPassword%>'
                            ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="NewPassword"
                            Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource,RegularNewPassword%>' SetFocusOnError="True"
                            ToolTip='<%$ Resources:ZnodeAdminResource,RegularNewPassword%>' ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularValidExpression %>'
                            ValidationGroup="ChangePassword1" CssClass="Error"></asp:RegularExpressionValidator>
                    </span>
                </div>
                <div class="CPassword">
                    <span align="right" class="FieldStyle">
                        <asp:Label ID="ConfirmNewPasswordLabel" runat="server" AssociatedControlID="ConfirmNewPassword" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleConfirmNewPassword%>'></asp:Label></span>
                    <span>
                        <asp:TextBox ID="ConfirmNewPassword" runat="server" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="ConfirmNewPasswordRequired" runat="server" ControlToValidate="ConfirmNewPassword"
                            ErrorMessage='<%$ Resources:ZnodeAdminResource,RequiredConfirmNewPassword%>' ToolTip='<%$ Resources:ZnodeAdminResource,RequiredConfirmNewPassword%>'
                            ValidationGroup="ChangePassword1" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                        <asp:CompareValidator ID="NewPasswordCompare" runat="server" ControlToCompare="NewPassword"
                            ControlToValidate="ConfirmNewPassword" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource,CompareNewPassword%>'
                            ValidationGroup="ChangePassword1"></asp:CompareValidator>
                    </span>
                </div>
                <div class="CPassword">
                    <span align="right" class="FieldStyle">
                        <asp:Label ID="lblSecretQuestion" runat="server" AssociatedControlID="ddlSecretQuestions" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleSecretQuestion%>'></asp:Label>
                    </span>
                    <span class="ValueStyle">
                        <asp:DropDownList ID="ddlSecretQuestions" runat="server">
                            <asp:ListItem Enabled="true" Selected="True" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFavoritePet %>' />
                            <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextCityBorn %>' />
                            <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextHighSchool %>' />
                            <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFavoriteMovie %>' />
                            <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextMotherMaiden %>' />
                            <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFirstCar %>' />
                            <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFavoriteColor %>' />
                        </asp:DropDownList>
                    </span>
                </div>

                <div class="CPassword">
                    <span align="right" class="FieldStyle">
                        <asp:Label ID="AnswerLabel" runat="server" AssociatedControlID="Answer">Security Answer</asp:Label>
                    </span>
                    <span class="ValueStyle">
                        <asp:TextBox ID="Answer" Width="140px" runat="server"></asp:TextBox></span>
                </div>

                <div class="CPassword">
                    <div class="Buttons">
                        <zn:Button runat="server" ID="ChangePasswordPushButton" ButtonType="SubmitButton" CommandName="ChangePassword" ValidationGroup="ChangePassword1" Text='<%$ Resources:ZnodeAdminResource,ButtonSubmit%>' CausesValidation="True" />
                        <zn:Button runat="server" ID="CancelPushButton" ButtonType="CancelButton" CommandName="Cancel"  Text='<%$ Resources:ZnodeAdminResource,ButtonCancel%>' CausesValidation="False" />
                    </div>
                </div>
            </ChangePasswordTemplate>
        </asp:ChangePassword>
    </div>
</asp:Content>

