<%@ Page Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.URLList" Codebehind="URLList.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<title></title>
    <link id="Link1" href="~/FranchiseAdmin/Themes/Standard/style.css" type="text/css" rel="stylesheet" runat="server" />
</head>
<body class="Body" style="background-color:Transparent;">
    <form id="form1" runat="server">
    <div class="Dashboard" style="padding-top:10px;padding-left:5px;">
        <asp:GridView ID="uxGrid" ShowHeader="false" ShowFooter="false"  runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" CaptionAlign="Left" Width="100%" PageSize="7" AllowPaging="true" AllowSorting="True" OnPageIndexChanging="UxGrid_PageIndexChanging" EmptyDataText='<%$ Resources:ZnodeAdminResource,GridEmptyRecordText %>'>
        <Columns>
           <asp:TemplateField HeaderStyle-Height="0px"> 
            <ItemTemplate>
                <span class="URLText"><a href='<%# "http://" + DataBinder.Eval(Container.DataItem, "DomainName").ToString()%>' style="text-decoration:none;" title='' runat="server" target="_blank"><img id="Img1" src="~/FranchiseAdmin/Themes/images/magnify.gif" style="border-style:none;" runat="server" alt='' class="MagnifyIcon" /><%# Eval("DomainName") %></a></span>     
            </ItemTemplate>
           </asp:TemplateField>
        </Columns>
        </asp:GridView>
        
    </div>
</form>
</body>
</html>
