using System;
using System.Diagnostics;
using System.Web;
using System.Web.Security;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.UserAccount;

namespace  Znode.Engine.FranchiseAdmin.Secure
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_Default class.
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        private DashboardAdmin _DashAdmin = new DashboardAdmin();
        private string daysToExpire = "0";
        private string _DaysToExpire = "0";
        private string _MultifrontUrl = string.Empty;
        
        #region Protected properties
        /// <summary>
        /// Gets or sets the Dashboard admin
        /// </summary>
        public DashboardAdmin DashAdmin
        {
            get { return this._DashAdmin; }
            set { this._DashAdmin = value; }
        }

        /// <summary>
        /// Gets or sets the days to expire
        /// </summary>
        public string DaysToExpire
        {
            get { return this._DaysToExpire; }
            set { this._DaysToExpire = value; }
        }

        /// <summary>
        /// Gets or sets the multi front URL
        /// </summary>
        public string MultifrontUrl
        {
            get { return this._MultifrontUrl; }
            set { this._MultifrontUrl = value; }
        }
        #endregion
      
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["SelectedMenuItem"] = null;
            this.BindData();
        }

        #region Helper Methods

        /// <summary>
        /// Concate Firstname, Lastname and UserRole
        /// </summary>   
        /// <returns>Returns the welcome string with username.</returns>
        protected string ConcatName()
        {
            ZNodeUserAccount userAccount = ZNodeUserAccount.CurrentAccount();
            MembershipUser user = Membership.GetUser(userAccount.UserID);
            string[] roles = Roles.GetRolesForUser(user.UserName);
            string roleList = string.Empty;

            foreach (string Role in roles)
            {
                roleList += " " + Role + ",";
            }

            roleList = " " + roleList.TrimEnd(',');

            string rolename = roleList;

            user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            TimeSpan span = System.DateTime.Now.Subtract(user.LastPasswordChangedDate);
            this.daysToExpire = (60 - span.Days).ToString();

            return this.GetGlobalResourceObject("ZnodeAdminResource", "TitleWelcome") + "<span> <b>" + userAccount.FirstName + " " + userAccount.LastName + "</b>.</span>" + this.GetGlobalResourceObject("ZnodeAdminResource", "TextLogged") + "<span> <b>" + rolename + "</b>.</span>";
        }

        /// <summary>
        /// Get the product Version
        /// </summary>
        /// <returns>Returns the product version number.</returns>
        protected string GetProductVersion()
        {
            try
            {
                string path = ZNode.Libraries.Framework.Business.ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Bin/ZNode.Libraries.Framework.Business.dll";

                // Create Instance for FileVersionInfo object
                FileVersionInfo info = FileVersionInfo.GetVersionInfo(Server.MapPath(path));
                if (info != null)
                {
                    return info.FileVersion;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }
        #endregion

        #region Bind data to the Dashboard
        /// <summary>
        /// Bind data to the dashboard
        /// </summary>
        private void BindData()
        {
            ZNodeUserAccount userAcct = ZNodeUserAccount.CurrentAccount();

            MembershipUser _user = Membership.GetUser(userAcct.UserID);

            if (_user != null)
            {
                string roleList = string.Empty;

                // Get roles for this User account
                string[] roles = Roles.GetRolesForUser(_user.UserName);

                foreach (string Role in roles)
                {
                    roleList += Role + ",";
                }

                try
                {
                    this.DashAdmin.GetDashboardItems(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(1));
                    inventoryText.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextLowInventory").ToString(), DashAdmin.TotalLowInventoryItems.ToString());
                    loginText.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextEmailOptIn").ToString(), DashAdmin.EmailOptInCustomers.ToString());
                    YTDSales.Text = DashAdmin.YTDRevenue.ToString("C2");
                    YTDOrders.Text = DashAdmin.TotalOrders.ToString();
                    YTDAccountsCreated.Text = DashAdmin.TotalAccounts.ToString();
                }
                catch
                {
                    // non critical - ignore        
                }
            }

            // Set the IFrame to https if we are on a secure connection.
            string prefix = "http://";

            if (Request.IsSecureConnection)
            {
                prefix = "https://";
            }

            // get multifront path
            this.MultifrontUrl = prefix + HttpContext.Current.Request.ServerVariables.Get("HTTP_HOST") + HttpContext.Current.Request.ApplicationPath;
        }
        #endregion
    }
}