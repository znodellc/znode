using System;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Catalog class.
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            Session["SelectedMenuItem"] = Request.Url.PathAndQuery;
        }
    }
}