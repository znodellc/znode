using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Content.Pages
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_design_Page_Revert class.
    /// </summary>
    public partial class Revert : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId;
        private string listLink = "~/FranchiseAdmin/Secure/Setup/Content/Pages/Default.aspx";
        private string _PageName = string.Empty;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the content page name.
        /// </summary>
        public string PageName
        {
            get { return this._PageName; }
            set { this._PageName = value; }
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(this.encrypt.DecryptData(Request.Params["itemid"]));
            }
            else
            {
                this.itemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindGridData();
            }
        }
        #endregion        

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate cell in the GridView control.
                GridViewRow selectedRow = uxGrid.Rows[index];

                TableCell Idcell = selectedRow.Cells[0];
                string Id = Idcell.Text;

                if (e.CommandName == "Revert")
                {
                    ContentPageAdmin pageAdmin = new ContentPageAdmin();
                    string oldcontent = string.Empty;
                    if (ViewState["oldcontent"] != null)
                    {
                        oldcontent = ViewState["oldcontent"].ToString();
                    }

                    bool isReverted = pageAdmin.RevertToRevision(int.Parse(Id), HttpContext.Current.User.Identity.Name, oldcontent);

                    if (isReverted)
                    {
                        this.BindGridData();
                        lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSuccessRevertedPage").ToString();//"Successfully reverted page version.";
                    }
                    else
                    {
                        lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUanabletoRevert").ToString();//"Unable to revert to the requested version.";
                    }                    
                }
            }
        }
        #endregion

        #region Other Events        
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.listLink);
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            ContentPageAdmin pageAdmin = new ContentPageAdmin();
            TList<ContentPageRevision> revisionList = pageAdmin.GetPageRevisions(this.itemId);
            revisionList.Sort("UpdateDate Desc");

            ContentPage page = pageAdmin.GetPageByID(this.itemId);
            ViewState["oldcontent"] = pageAdmin.GetPageHTMLByName(page.Name, page.PortalID, page.LocaleId.ToString());

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                string[] stores = profiles.StoreAccess.Split(',');
                Array Stores = (Array)stores;
                int found = Array.IndexOf(Stores, page.PortalID.ToString());
                if (found == -1)
                {
                    Response.Redirect("Default.aspx", true);
                }
            }

            this.PageName = page.Name;

            uxGrid.DataSource = revisionList;
            uxGrid.DataBind();
        }
        #endregion
    }
}