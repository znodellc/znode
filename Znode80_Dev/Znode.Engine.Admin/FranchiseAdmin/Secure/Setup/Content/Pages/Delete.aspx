<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Content.Pages.Delete" Title="Untitled Page" CodeBehind="Delete.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h5>
        <asp:Localize ID="TextPleaseConfirm" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm %>'></asp:Localize>
    </h5>
    <p>
        <asp:Localize ID="TextPageNameDeleteConfirm" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPageNameDeleteConfirm %>'></asp:Localize><b>"<% = PageName%>"</b>.
         <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPageNameCannotBeUndone %>'></asp:Localize>
    </p>
    <div>
        <asp:Label ID="lblMsg" runat="server" Width="450px" CssClass="Error"></asp:Label><br />
        <br />
    </div>
    <div>
        <zn:Button ID="btnDelete" runat="server" ButtonType="CancelButton" CausesValidation="True" OnClick="BtnDelete_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonDelete %>" />
        <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" CausesValidation="false" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
    </div>
    <br />
    <br />
    <br />
</asp:Content>
