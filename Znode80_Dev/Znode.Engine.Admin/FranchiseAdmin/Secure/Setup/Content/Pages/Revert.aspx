<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True"
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Content.Pages.Revert" Title="Untitled Page" CodeBehind="Revert.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 50%">
            <h1>
                <asp:Localize ID="TitleRevisionsforPage" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleRevisionsforPage %>'></asp:Localize>
                <%=PageName %>
            </h1>
        </div>
        <div class="LeftFloat" style="width: 49%" align="right">
            <zn:Button runat="server" Width="100px" ID="btnBack" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonGoBack %>' OnClick="BtnBack_Click" ButtonType="EditButton"/>
        </div>
    </div>
    <div class="ClearBoth" align="left">
    </div>
    <p>
        <asp:Localize ID="TextRevusionPage" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextRevertBack %>'></asp:Localize>
    </p>
    <div>
        <asp:Label ID="lblMsg" runat="server" Font-Bold="True" ForeColor="#00C000"></asp:Label>
    </div>
    <div>
        <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
    </div>
    <h4 class="GridTitle">
        <asp:Localize ID="SubTitleRevisions" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleRevisions %>'></asp:Localize>
    </h4>
    <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
        CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
        CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
        OnRowDeleting="UxGrid_RowDeleting" AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyNoContentPageText %>'>
        <Columns>
            <asp:BoundField DataField="RevisionID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="UpdateUser" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleUpdatedBy %>' HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="UpdateDate" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleUpdatedOn %>' HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="Description" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDescription %>' HeaderStyle-HorizontalAlign="Left"
                ItemStyle-Width="20%" />
            <asp:ButtonField CommandName="Revert" Text='<%$ Resources:ZnodeAdminResource, ButtonRevertVersion %>' ButtonType="Link">
                <ControlStyle CssClass="Button" />
            </asp:ButtonField>
        </Columns>
        <FooterStyle CssClass="FooterStyle" />
        <RowStyle CssClass="RowStyle" />
        <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
        <HeaderStyle CssClass="HeaderStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
    </asp:GridView>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
    </div>
</asp:Content>
