using System;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.FranchiseAdmin.Secure.Setup.Content.Pages
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_design_page_Default class.
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Private Member Variables
        private string listLink = "~/FranchiseAdmin/Secure/Setup/Content/Pages/Default.aspx";
        private string addLink = "~/FranchiseAdmin/Secure/Setup/Content/Pages/Add.aspx";
        private string revertLink = "~/FranchiseAdmin/Secure/Setup/Content/Pages/Revert.aspx";
        private string editLink = "~/FranchiseAdmin/Secure/Setup/Content/Pages/Add.aspx";
        private string deleteLink = "~/FranchiseAdmin/Secure/Setup/Content/Pages/Delete.aspx";
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindContentPage();
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindContentPage();
        }

        /// <summary>
        /// Event triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindContentPage();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                string Id = Convert.ToString(e.CommandArgument);

                if (e.CommandName == "Edit")
                {
                    ZNodeEncryption encrypt = new ZNodeEncryption();
                    this.editLink = this.editLink + "?itemid=" + HttpUtility.UrlEncode(encrypt.EncryptData(Id));
                    Response.Redirect(this.editLink);
                }
                else if (e.CommandName == "Publish")
                {
                    ContentPageAdmin pageAdmin = new ContentPageAdmin();
                    ContentPage contentPage = pageAdmin.GetPageByID(int.Parse(Id));
                    pageAdmin.PublishPage(contentPage);

                    Response.Redirect(this.listLink);
                }
                else if (e.CommandName == "Revert")
                {
                    ZNodeEncryption encrypt = new ZNodeEncryption();
                    this.revertLink = this.revertLink + "?itemid=" + HttpUtility.UrlEncode(encrypt.EncryptData(Id));
                    Response.Redirect(this.revertLink);
                }
                else if (e.CommandName == "Delete")
                {
                    ZNodeEncryption encrypt = new ZNodeEncryption();
                    Response.Redirect(this.deleteLink + "?itemid=" + HttpUtility.UrlEncode(encrypt.EncryptData(Id)));
                }
            }
        }
        #endregion

        #region Other Events
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindContentPage();
        }

        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addLink);
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Search the page based on the search criteria Portal Id, Locale Id and page name.
        /// </summary>
        private void BindContentPage()
        {
            ContentPageService contentPageService = new ContentPageService();
            StringBuilder expression = new StringBuilder();
            string pageName = txtPageName.Text.Replace("'", string.Empty).Trim();

            ContentPageQuery query = new ContentPageQuery();
            if (!string.IsNullOrEmpty(pageName))
            {
                query.Append(ContentPageColumn.Name, "*" + pageName + "*");
            }

            TList<ContentPage> contentPageList = null;
            contentPageList = contentPageService.Find(query.GetParameters());
            contentPageService.DeepLoad(contentPageList, true, DeepLoadType.IncludeChildren, typeof(ZNode.Libraries.DataAccess.Entities.MasterPage));

            contentPageList.ForEach(x => x.MasterPageIDSource = GetMasterPage(x.MasterPageIDSource));
            uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(contentPageList);
            uxGrid.DataBind();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="masterPage"></param>
        /// <returns></returns>
        private ZNode.Libraries.DataAccess.Entities.MasterPage GetMasterPage(ZNode.Libraries.DataAccess.Entities.MasterPage masterPage)
        {
            if (masterPage != null)
            {
                masterPage.Name = masterPage.Name.ToUpper().Replace("CONTENT/", "");
            }
            return masterPage;
        }
        /// <summary>
        /// Get the page Url
        /// </summary>
        /// <param name="pageName">Content page name.</param>
        /// <param name="seoURL">Content page SEO Url</param>
        /// <returns>Returns the page url.</returns>
        private string GetPageURL(string pageName, object seoURL)
        {
            // If page name is Home, then it must be open with default page.
            if (pageName.Equals("Home"))
            {
                return "~/";
            }

            string seoUrl = string.Empty;

            if (seoURL != null)
            {
                if (seoURL.ToString().Length > 0)
                {
                    seoUrl = seoURL.ToString();
                }
            }

            // Otherwise the content pages should be open with content.aspx page more Specific to Content pages
            return ZNodeSEOUrl.MakeURL(pageName, SEOUrlType.ContentPage, seoUrl);
        }
        #endregion
    }
}