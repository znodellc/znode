using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Content.Pages
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_design_Page_Add class.
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId;
        private string cancelLink = "~/FranchiseAdmin/Secure/Setup/Content/Pages/Default.aspx";
        private string nextLink = "~/FranchiseAdmin/Secure/Setup/Content/Pages/Default.aspx";
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                ZNodeEncryption encrypt = new ZNodeEncryption();
                this.itemId = int.Parse(encrypt.DecryptData(Request.Params["itemid"]));
            }
            else
            {
                this.itemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindThemeList();

                // If edit func then bind the data fields
                if (this.itemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditContentPage").ToString();
                    txtName.Enabled = false;
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddContentPage").ToString();
                }

                // Bind data to the fields on the page
                this.BindData();
            }
        }
        #endregion       

        #region General Events

        /// <summary>
        /// Theme selected index changed event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlThemeslist_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlTemplateList.Visible = true;
            pnlCssList.Visible = true;
            ddlPageTemplateList.Items.Clear();
            ddlCSSList.Items.Clear();
            this.BindMasterPageTemplates();
            this.BindCssList();

            if (ddlThemeslist.SelectedValue == "0")
            {
                pnlTemplateList.Visible = false;
                pnlCssList.Visible = false;
                ddlPageTemplateList.SelectedValue = "0";
                ddlCSSList.SelectedValue = "0";
            }
        }

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ContentPageAdmin pageAdmin = new ContentPageAdmin();
            ContentPage contentPage = new ContentPage();
            string mappedSEOUrl = string.Empty;

            bool allowDelete = true;

            // If edit mode then retrieve data first
            if (this.itemId > 0)
            {
                contentPage = pageAdmin.GetPageByID(this.itemId);

                // Override this setting
                allowDelete = contentPage.AllowDelete;
                if (contentPage.SEOURL != null)
                {
                    mappedSEOUrl = contentPage.SEOURL;
                }
            }
            else
            {
                // Add mode - check if this page name already exists
                if (!pageAdmin.IsNameAvailable(txtName.Text.Trim(), UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0), ZNodeCatalogManager.CatalogConfig.LocaleID))
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPagenameAlreadyExist").ToString();
                    return;
                }
            }

            // Set values
            contentPage.ActiveInd = true;
            contentPage.Name = txtName.Text.Trim();
            contentPage.LocaleId = ZNodeCatalogManager.CatalogConfig.LocaleID;
            contentPage.SEOMetaDescription = txtSEOMetaDescription.Text;
            contentPage.SEOMetaKeywords = txtSEOMetaKeywords.Text;
            contentPage.MetaTagAdditional = txtMetaTagAdditional.Text;
            contentPage.SEOTitle = txtSEOTitle.Text;
            contentPage.SEOURL = null;
            if (txtSEOUrl.Text.Trim().Length > 0 && !contentPage.Name.ToLower().Equals("home"))
            {
                contentPage.SEOURL = txtSEOUrl.Text.Trim().Replace(" ", "-");
            }

            contentPage.Title = txtTitle.Text;
            contentPage.AllowDelete = allowDelete;
            if (contentPage.Name.ToLower().Equals("home"))
            {
                contentPage.AllowDelete = false;
            }

            if (ddlThemeslist.SelectedValue != "0")
            {
                contentPage.ThemeID = int.Parse(ddlThemeslist.SelectedValue);
            }
            else
            {
                contentPage.ThemeID = null;
            }

			if (ddlPageTemplateList.SelectedValue != "0" && ddlPageTemplateList.Visible)
            {
                contentPage.MasterPageID = int.Parse(ddlPageTemplateList.SelectedValue);
            }
            else
            {
                contentPage.MasterPageID = null;
            }

			if (ddlCSSList.SelectedValue != "0" && ddlCSSList.Visible)
            {
                contentPage.CSSID = int.Parse(ddlCSSList.SelectedValue);
            }
            else
            {
                contentPage.CSSID = null;
            }

            bool isSuccess = false;

            if (this.itemId > 0)
            {
                contentPage.PortalID = contentPage.PortalID;

                // Update code here
                string oldcontent = string.Empty;
                if (ViewState["Oldcontent"] != null)
                {
                    oldcontent = ViewState["Oldcontent"].ToString();
                }

                isSuccess = pageAdmin.UpdatePage(contentPage, ctrlHtmlText.Html, oldcontent, contentPage.PortalID, ZNodeCatalogManager.CatalogConfig.LocaleID.ToString(), HttpContext.Current.User.Identity.Name, mappedSEOUrl, chkAddURLRedirect.Checked);

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, lblTitle.Text, txtName.Text.Trim());
            }
            else
            {
                contentPage.PortalID = UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0);

                // Add code here
                isSuccess = pageAdmin.AddPage(contentPage, ctrlHtmlText.Html, UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0), ZNodeCatalogManager.CatalogConfig.LocaleID.ToString(), HttpContext.Current.User.Identity.Name, mappedSEOUrl, chkAddURLRedirect.Checked);

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, lblTitle.Text, txtName.Text.Trim());
            }

            if (isSuccess)
            {
                // Redirect to main page
                Response.Redirect(this.nextLink);
            }
            else
            {
                if (contentPage.SEOURL != null)
                {
                    // Display error message
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSEOUrlSettings").ToString();
                }
                else
                {
                    // Display error message
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdatePageFailed").ToString();
                }
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.cancelLink);
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields 
        /// </summary>
        private void BindData()
        {
            ContentPageAdmin pageAdmin = new ContentPageAdmin();
            ContentPage contentPage = new ContentPage();

            if (this.itemId > 0)
            {
                contentPage = pageAdmin.GetPageByID(this.itemId);

                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = profiles.StoreAccess.Split(',');
                    Array Stores = (Array)stores;
                    int found = Array.IndexOf(Stores, contentPage.PortalID.ToString());
                    if (found == -1)
                    {
                        Response.Redirect("Default.aspx", true);
                    }
                }

                // Set fields          
                txtName.Text = contentPage.Name.Trim();
                txtSEOMetaDescription.Text = contentPage.SEOMetaDescription;
                txtMetaTagAdditional.Text = contentPage.MetaTagAdditional;
                txtSEOMetaKeywords.Text = contentPage.SEOMetaKeywords;
                txtSEOTitle.Text = contentPage.SEOTitle;
                txtSEOUrl.Text = contentPage.SEOURL;
                txtTitle.Text = contentPage.Title;

                if (contentPage.ThemeID != null)
                {
                    ddlThemeslist.SelectedValue = contentPage.ThemeID.ToString();
                }

                if (ddlThemeslist.SelectedValue != "0")
                {
                    pnlTemplateList.Visible = true;
                    pnlCssList.Visible = true;
                    this.BindMasterPageTemplates();
                    this.BindCssList();

                    if (contentPage.MasterPageID != null)
                    {
						ddlPageTemplateList.SelectedValue = contentPage.MasterPageID.ToString();
                    }

                    if (contentPage.CSSID != null)
                    {
						ddlCSSList.SelectedValue = contentPage.CSSID.ToString();
                    }
                }

                // Get content
                ctrlHtmlText.Html = pageAdmin.GetPageHTMLByName(contentPage.Name, contentPage.PortalID, contentPage.LocaleId.ToString());
                ViewState["Oldcontent"] = ctrlHtmlText.Html;

                if (contentPage.Name.Contains("Home"))
                {
                    pnlSEOURL.Visible = false;
                }
            }
        }

        /// <summary>
        /// Binds the themes to the DropDownList
        /// </summary>
        private void BindThemeList()
        {
            ddlThemeslist.DataSource = DataRepository.ThemeProvider.GetAll();
            ddlThemeslist.DataTextField = "Name";
            ddlThemeslist.DataValueField = "ThemeID";
            ddlThemeslist.DataBind();
            ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownSiteTheme").ToString(), "0");
            ddlThemeslist.Items.Insert(0, li);
            BindCssList();
        }

        /// <summary>
        /// Bind the CSS to the DropDownList
        /// </summary>
        private void BindCssList()
        {
            if (ddlThemeslist.SelectedIndex > 0)
            {
                ddlCSSList.DataSource = DataRepository.CSSProvider.GetByThemeID(int.Parse(ddlThemeslist.SelectedValue));
                ddlCSSList.DataTextField = "Name";
                ddlCSSList.DataValueField = "CSSID";
                ddlCSSList.DataBind();
                ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownSameAsStore").ToString(), "0");
                ddlCSSList.Items.Insert(0, li);
            }
        }

		/// <summary>
		/// Bind the MasterPage templates
		/// </summary>
        private void BindMasterPageTemplates()
        {
            if (ddlThemeslist.SelectedIndex > 0)
            {
                PortalAdmin portalAdmin = new PortalAdmin();

                ddlPageTemplateList.DataSource = portalAdmin.GetMasterPage(int.Parse(ddlThemeslist.SelectedValue), "Content");
                ddlPageTemplateList.DataTextField = "Name";
                ddlPageTemplateList.DataValueField = "MasterPageID";
                ddlPageTemplateList.DataBind();

                // Master template
                ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownSameAsTemplate").ToString(), "0");
                ddlPageTemplateList.Items.Insert(0, li);
            }
        }
        #endregion
    }
}