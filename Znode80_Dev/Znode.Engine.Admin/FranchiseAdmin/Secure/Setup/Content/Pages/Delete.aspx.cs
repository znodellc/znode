using System;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Content.Pages
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_design_Page_Delete class.
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId;
        private string _PageName = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the content page name.
        /// </summary>
        public string PageName
        {
            get { return this._PageName; }
            set { this._PageName = value; }
        } 
        #endregion

        #region Page load
        /// <summary>
        /// Page Load event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                ZNodeEncryption encrypt = new ZNodeEncryption();
                this.itemId = int.Parse(encrypt.DecryptData(Request.Params["itemid"]));
            }
            else
            {
                this.itemId = 0;
            }

            this.BindData();
        }
        #endregion        

        #region Events
        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Setup/Content/Pages/Default.aspx");
        }

        /// <summary>
        /// Delete button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            ContentPageAdmin pageAdmin = new ContentPageAdmin();
            ContentPage contentPage = pageAdmin.GetPageByID(this.itemId);

            string associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeletepage").ToString(), contentPage.Name);
            this.PageName = contentPage.Name;

            bool isDeleted = pageAdmin.DeletePage(contentPage);

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, this.PageName);

            if (!isDeleted)
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteactionCouldNotbeCompleted").ToString();
            }
            else
            {
                Response.Redirect("~/FranchiseAdmin/Secure/Setup/Content/Pages/Default.aspx");
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the screen
        /// </summary>
        private void BindData()
        {
            ContentPageAdmin pageAdmin = new ContentPageAdmin();
            ContentPage contentPage = pageAdmin.GetPageByID(this.itemId);

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                string[] stores = profiles.StoreAccess.Split(',');
                Array Stores = (Array)stores;
                int found = Array.IndexOf(Stores, contentPage.PortalID.ToString());
                if (found == -1)
                {
                    Response.Redirect("Default.aspx", true);
                }
            }

            this.PageName = contentPage.Name;

            if (!contentPage.AllowDelete)
            {
                btnDelete.Enabled = false;
                lblMsg.Text = "This page is a reserved page and cannot be deleted.";
            }
        }
        #endregion
    }
}