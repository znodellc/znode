<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" ValidateRequest="false"
    AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Content.Pages.Add"
    Title="Untitled Page" CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/FranchiseAdmin/Controls/Default/StoreName.ascx" TagName="StoreName" TagPrefix="ZNode" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/FranchiseAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div style="width: 870px; display: table-cell;">
        <div class="LeftFloat" style="width: 50%">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>
        <div class="LeftFloat" align="right" style="width: 50%">
            <zn:Button ID="btnSubmitTop" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
            <zn:Button ID="btnCancelTop" runat="server" ButtonType="CancelButton" CausesValidation="false" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
        </div>
    </div>
    <div class="ClearBoth">
    </div>
    <div class="FormView DesignPageAdd">
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="SubTitleGeneral" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralSettings %>'></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitlePageName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleContentPageName %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtName"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterNameForpage %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularPageNameContaints %>' CssClass="Error"
                ControlToValidate="txtName" ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidPageTitle %>'></asp:RegularExpressionValidator>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtName" runat="server" MaxLength="50" Columns="50"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleContentPageNameTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleContentPageNameTitle %>'></asp:Localize>
        </div>
        <div>
            <asp:TextBox ID="txtTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTitle"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredPageTitle %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div>
            <uc1:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleSelectTheme" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectTheme %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlThemeslist" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlThemeslist_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <asp:Panel ID="pnlTemplateList" runat="server" Visible="false">
            <div class="FieldStyle">
                <asp:Localize ID="ColumnTitleMasterPageTemplate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleMasterPageTemplate %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlPageTemplateList" runat="server">
                </asp:DropDownList>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlCssList" runat="server" Visible="false">
            <div class="FieldStyle">
                <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCSS %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlCSSList" runat="server">
                </asp:DropDownList>
            </div>
        </asp:Panel>
        <h4 class="SubTitle">
            <asp:Localize ID="SubTitleSeoSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSeoSettings %>'></asp:Localize>
        </h4>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleSEOTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOTitle %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSEOTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleSEOKeywords" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOKeywords %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSEOMetaKeywords" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleSEODescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEODescription %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSEOMetaDescription" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleMetaInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleMetaInformation %>'></asp:Localize><br />
            <small>
                <asp:Localize ID="HintTextMetaInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextMetaInformation %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMetaTagAdditional" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
        </div>
        <asp:Panel ID="pnlSEOURL" runat="server">
            <div class="FieldStyle">
                <asp:Localize ID="ColumnTitleSEOFriendlyPageName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOFriendlyPageName %>'></asp:Localize><br />
                <small>
                    <asp:Localize ID="HintTextSEOfriendlyPageName" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextSEOfriendlyPageName %>'></asp:Localize>
                </small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOUrl" runat="server" MaxLength="100" Columns="50"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtSEOUrl"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidSEOURL %>'
                    SetFocusOnError="True" ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidSEOUrl %>'></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle"></div>
            <div class="ValueStyleText">
                <asp:CheckBox ID="chkAddURLRedirect" runat="server" Text='<%$ Resources:ZnodeAdminResource, CheckBoxRedirectUrl %>' />
            </div>
            <div>
                <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
        </asp:Panel>
        <h4 class="SubTitle">
            <asp:Localize ID="SubTitlePageContent" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitlePageContent %>'></asp:Localize></h4>
        <div class="ValueStyle" style="width: 99%;">
            <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
        </div>
        <div class="ClearBoth" align="left">
        </div>
        <div>
            <zn:Button ID="btnSubmit" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
            <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" CausesValidation="false" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
        </div>
    </div>
</asp:Content>
