<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" 
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Content.Messages.Default" Title="Untitled Page" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <h1>
            <asp:Localize ID="TitleManageMessages" Text='<%$ Resources:ZnodeAdminResource, TitleManageMessages%>' runat="server"></asp:Localize>
        </h1>
        <div>
            <p>
                <asp:Localize ID="TextMessages" Text='<%$ Resources:ZnodeAdminResource, TextManageMessages%>' runat="server"></asp:Localize>
            </p>
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="SubTitleSearch" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchMessage%>' runat="server"></asp:Localize></h4>
        </h4>
       
        <div class="SearchForm">
            <div class="RowStyle">                                     
                    <div class="ItemStyle">
                       <span class="FieldStyle">
                           <asp:Localize ID="ColumnMessage" Text='<%$ Resources:ZnodeAdminResource, ColumnMessage%>' runat="server"></asp:Localize></span><br />
                       <span class="ValueStyle"><asp:TextBox ID="txtMessageKey" runat="server"></asp:TextBox><br/>
                        <asp:RegularExpressionValidator ID="MessageValidator" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidSearch%>'
                       ControlToValidate="txtMessageKey" Display="Dynamic" ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularValidSearchMessage%>' CssClass="Error"></asp:RegularExpressionValidator>
                       </span>
                    </div>
            </div>
        </div>
        
        <div class="ClearBoth" align="left"></div>
        
        <div>
            <zn:Button runat="server" ID="btnSearch" OnClick="BtnSearch_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' CausesValidation="true"/>
            <zn:Button runat="server" ID="btnClear" OnClick="BtnClear_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
        </div>
  
        <div><asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label></div><br />
        
        <h4 class="GridTitle">
            <asp:Localize ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, GridTitleCustomMessage%>' runat="server"></asp:Localize></h4>
        </h4>
        <asp:GridView ID="uxGrid" runat="server" PageSize="50" CssClass="Grid" AllowPaging="True"
            AutoGenerateColumns="False" CellPadding="4" OnRowCommand="UxGrid_RowCommand"
            GridLines="None" CaptionAlign="Left" Width="100%" AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyTextMessage%>'
            OnPageIndexChanging="UxGrid_PageIndexChanging">
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            <Columns>
                <asp:BoundField DataField="Description" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnMessage%>' HeaderStyle-HorizontalAlign="Left" />                             
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="Button" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkEditMessage %>' CommandName="Edit"
                            CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Key") %>' CssClass="actionlink" />
                    </ItemTemplate>
                </asp:TemplateField>                
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
