using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Content.Messages
{
    /// <summary>
    /// Represents the Franchise Admin CLASS class.
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Private Variables        
        private string editPageLink = "Editmessage.aspx?itemid=";
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {                
                this.BindSearchData();
            }
        }
        #endregion       

        #region Grid Events
        /// <summary>
        /// Triggers when grid row command clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                int idx = row.DataItemIndex;

                string key = e.CommandArgument.ToString();

                ZNodeEncryption encryption = new ZNodeEncryption();
                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.editPageLink + HttpUtility.UrlEncode(encryption.EncryptData(key)));
                }
            }
        }

        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }
        #endregion

        #region Events
        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Clear button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {        
            txtMessageKey.Text = string.Empty;
            this.BindSearchData();
        }

        #endregion        

        #region Bind methods
        /// <summary>
        /// Return the Searchdata
        /// </summary>        
        private void BindSearchData()
        {
            MessageConfigAdmin messageConfigAdmin = new MessageConfigAdmin();
            TList<MessageConfig> messageConfigList = messageConfigAdmin.GetMessagesConfigByPortalIdLocaleId(UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0), 43);

            DataSet ds = messageConfigList.ToDataSet(false);
            DataView dv = new DataView(ds.Tables[0]);

            dv.Sort = "Description ASC";
            string localeId = "43";
            string portalId = UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0).ToString();

            // Filter the Search data.
            if (portalId == "0" && localeId == "0")
            {
                dv.RowFilter = "Description like '%" + txtMessageKey.Text + "%'";
            }
            else if (portalId != "0" && localeId == "0")
            {
                dv.RowFilter = "Description like '%" + txtMessageKey.Text + "%' and portalId <> '0' and portalId = '" + portalId + "'";
            }
            else if (portalId == "0" && localeId != "0")
            {
                dv.RowFilter = "Description like '%" + txtMessageKey.Text + "%' and localeId <> '0' and localeId = '" + localeId + "'";
            }
            else
            {
                dv.RowFilter = "Description like '%" + txtMessageKey.Text + "%' and portalId <> '0' and portalId = '" + portalId + "' and localeId <> '0' and localeId = '" + localeId + "'";
            }

            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        #endregion
    }
}