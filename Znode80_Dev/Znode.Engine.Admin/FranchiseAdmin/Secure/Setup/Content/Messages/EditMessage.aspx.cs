using System;
using System.Web;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Content.Messages
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_settings_messages_EditMessage class.
    /// </summary>
    public partial class EditMessage : System.Web.UI.Page
    {
        #region Private Variables
        private string itemId = string.Empty;
        #endregion

        #region page load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.QueryString["itemid"] != null)
            {
                ZNodeEncryption encryption = new ZNodeEncryption();
                this.itemId = HttpUtility.HtmlDecode(encryption.DecryptData(Request.Params["itemid"]));
            }

            if (!Page.IsPostBack)
            {
                this.Bind();
            }
        }
        #endregion       

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            MessageConfigAdmin messageConfigAdmin = new MessageConfigAdmin();
            MessageConfig messageConfig = new MessageConfig();

            bool isUpdated = false;
            messageConfig.MessageID = Convert.ToInt32(hdnMessageID.Value);
            messageConfig.Key = this.itemId;
            messageConfig.Description = hdnDescription.Value;
            messageConfig.Value = ctrlHtmlText.Html;
            messageConfig.PortalID = UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0);
            messageConfig.LocaleID = 43;
            messageConfig.MessageTypeID = (int)ZNodeMessageType.Message;

            try
            {
                if (this.itemId != null)
                {
                    isUpdated = messageConfigAdmin.Update(messageConfig);
                }

                if (!isUpdated)
                {
                    lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorMessageConfig").ToString();
                    return;
                }
                else
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit Messages ", this.itemId);
                }
            }
            catch
            {
                // Display error message
                lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoteAdd").ToString();
            }

            Response.Redirect("~/FranchiseAdmin/Secure/Setup/Content/Messages/Default.aspx");
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Setup/Content/Messages/Default.aspx");
        }
        #endregion

        #region Bind MessageConfig
        private void Bind()
        {
            MessageConfigAdmin messageConfigAdmin = new MessageConfigAdmin();
            MessageConfig messageConfig = new MessageConfig();

            if (this.itemId != null)
            {
                messageConfig = messageConfigAdmin.GetByKeyPortalIDLocaleID(this.itemId, UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0), 43);
                UserStoreAccess.CheckStoreAccess(messageConfig.PortalID, true);

                ctrlHtmlText.Html = messageConfig.Value;
                string TitleMsg = messageConfig.Description;
                lbltitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditMessage").ToString() + TitleMsg.Replace("-", string.Empty);
                hdnMessageID.Value = messageConfig.MessageID.ToString();
                hdnDescription.Value = messageConfig.Description.ToString();
            }
        }
        #endregion
    }
}