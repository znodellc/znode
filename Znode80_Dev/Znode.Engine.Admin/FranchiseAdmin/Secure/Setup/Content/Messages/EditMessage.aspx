<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Content.Messages.EditMessage" ValidateRequest="false" CodeBehind="EditMessage.aspx.cs" %>

<%@ Register Src="~/FranchiseAdmin/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <asp:XmlDataSource ID="XmlDataSource1" runat="server"></asp:XmlDataSource>
        <br />
        <h1>
            <asp:Label ID="lbltitle" runat="server"></asp:Label></h1>
        <br />

        <div class="ClearBoth" align="left"></div>
        <div class="ValueStyle">
            <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
        </div>
        <br />
        <div>
            <zn:Button runat="server" ID="SubmitButton" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ValidationGroup="Messages" CausesValidation="True" />
            <zn:Button runat="server" ID="CancelButton" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
        </div>
        <br />
        <div>
            <asp:Label ID="lblmsg" runat="server" Font-Bold="True" ForeColor="LimeGreen"></asp:Label>
        </div>
        <asp:HiddenField ID="hdnMessageID" runat="server" Visible="false" />
        <asp:HiddenField ID="hdnDescription" runat="server" Visible="false" />
    </div>
</asp:Content>
