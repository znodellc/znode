﻿<%@ Page Language="C#" Title="Manage Taxes - Delete" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="true" CodeBehind="DeleteRule.aspx.cs" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Setup.Checkout.Taxes.DeleteRule" %>
<%@ Register TagPrefix="zn" TagName="TaxRuleDelete" Src="~/Controls/Default/Providers/Taxes/TaxRuleDelete.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
	<zn:TaxRuleDelete ID="ctlTaxRuleDelete" runat="server" RedirectUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Taxes/View.aspx" />
</asp:Content>
