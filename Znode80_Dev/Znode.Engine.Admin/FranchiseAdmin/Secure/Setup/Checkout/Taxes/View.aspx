<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Setup.Checkout.Taxes.View" ValidateRequest="false" Title="Untitled Page" CodeBehind="View.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="TaxClassDetails" Src="~/Controls/Default/Providers/Taxes/TaxClassDetails.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:TaxClassDetails ID="ctlTaxClassDetails" runat="server"
		DefaultPageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Taxes/Default.aspx"
		EditPageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Taxes/Add.aspx"
		AddRulePageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Taxes/AddRule.aspx"
		EditRulePageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Taxes/AddRule.aspx"
		DeleteRulePageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Taxes/DeleteRule.aspx"
	/>
</asp:Content>
