<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Setup.Checkout.Taxes.AddRule" ValidateRequest="false" Title="Untitled Page" CodeBehind="AddRule.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="TaxRuleAdd" Src="~/Controls/Default/Providers/Taxes/TaxRuleAdd.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:TaxRuleAdd ID="ctlTaxRuleAdd" runat="server" ViewPageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Taxes/View.aspx" />
</asp:Content>
