<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Setup.Checkout.Taxes.Add" ValidateRequest="false" Title="Untitled Page" Codebehind="Add.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="TaxClassAdd" Src="~/Controls/Default/Providers/Taxes/TaxClassAdd.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:TaxClassAdd ID="ctlTaxClassAdd" runat="server"
		DefaultPageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Taxes/Default.aspx"
		ViewPageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Taxes/View.aspx"
		AddRulePageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Taxes/AddRule.aspx"
	/>
</asp:Content>
