<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True"  Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Setup.Checkout.Taxes.Default" Title="Manage Taxes - List" Codebehind="Default.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="TaxClassList" Src="~/Controls/Default/Providers/Taxes/TaxClassList.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:TaxClassList ID="ctlTaxClassList" runat="server"
		ViewPageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Taxes/View.aspx"
		EditPageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Taxes/Add.aspx"
		DeletePageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Taxes/Delete.aspx"
	/>
</asp:Content>
