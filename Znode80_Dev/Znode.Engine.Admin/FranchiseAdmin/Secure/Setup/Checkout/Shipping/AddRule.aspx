<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Setup.Checkout.Shipping.AddRule" Title="Untitled Page" ValidateRequest="false" CodeBehind="AddRule.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="ShippingRuleAdd" Src="~/Controls/Default/Providers/Shipping/ShippingRuleAdd.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:ShippingRuleAdd ID="ctlShippingRuleAdd" runat="server" ViewPageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Shipping/View.aspx" />
</asp:Content>
