<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Setup.Checkout.Shipping.DeleteRule" Title="Untitled Page" CodeBehind="DeleteRule.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="ShippingRuleDelete" Src="~/Controls/Default/Providers/Shipping/ShippingRuleDelete.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:ShippingRuleDelete ID="ctlShippingRuleDelete" runat="server" RedirectUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Shipping/View.aspx" />
</asp:Content>
