<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Setup.Checkout.Shipping.View" Title="Untitled Page" CodeBehind="View.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="ShippingOptionDetails" Src="~/Controls/Default/Providers/Shipping/ShippingOptionDetails.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:ShippingOptionDetails ID="ctlShippingOptionDetails" runat="server"
		ListPageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Shipping/Default.aspx"
		EditPageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Shipping/Add.aspx"
		AddRulePageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Shipping/AddRule.aspx"
		EditRulePageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Shipping/AddRule.aspx"
		DeleteRulePageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Shipping/DeleteRule.aspx"
	/>
</asp:Content>
