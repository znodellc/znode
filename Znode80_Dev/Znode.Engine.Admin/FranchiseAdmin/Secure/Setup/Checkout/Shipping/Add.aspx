<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Setup.Checkout.Shipping.ShippingAdd" Title="Untitled Page" CodeBehind="Add.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="ShippingOptionAdd" Src="~/Controls/Default/Providers/Shipping/ShippingOptionAdd.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:ShippingOptionAdd ID="ctlShippingOptionAdd" runat="server"
		ListPageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Shipping/Default.aspx"
		ViewPageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Shipping/View.aspx"
		AddRulePageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Shipping/AddRule.aspx"
	/>
</asp:Content>
