<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Setup.Checkout.Shipping.ShippingDefault" Title="Untitled Page" CodeBehind="Default.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="ShippingOptionList" Src="~/Controls/Default/Providers/Shipping/ShippingOptionList.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:ShippingOptionList ID="ctlShippingOptionList" runat="server"
		ViewPageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Shipping/View.aspx"
		AddPageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Shipping/Add.aspx"
		EditPageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Shipping/Add.aspx"
		DeletePageUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Shipping/Delete.aspx"
	/>
</asp:Content>
