<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.FranchiseAdmin.Secure.Setup.Checkout.Shipping.Delete" Title="Untitled Page" CodeBehind="Delete.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="ShippingOptionDelete" Src="~/Controls/Default/Providers/Shipping/ShippingOptionDelete.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:ShippingOptionDelete ID="ctlShippingOptionDelete" runat="server" RedirectUrl="~/FranchiseAdmin/Secure/Setup/Checkout/Shipping/Default.aspx" />
</asp:Content>
