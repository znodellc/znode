using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Checkout.Payments
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_settings_payment_Default class.
    /// </summary>
    public partial class PaymentDefault : System.Web.UI.Page
    {
        #region Private Member Variables
        private string addPageLink = "~/FranchiseAdmin/Secure/Setup/Checkout/Payments/Add.aspx";
        private string editPageLink = "~/FranchiseAdmin/Secure/Setup/Checkout/Payments/Add.aspx";
        private string deletePageLink = "~/FranchiseAdmin/Secure/Setup/Checkout/Payments/Delete.aspx";
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindGridData();
            }
        }
        #endregion       

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                string Id = e.CommandArgument.ToString();
                ZNodeEncryption encryption = new ZNodeEncryption();
                if (e.CommandName == "Edit")
                {
                    this.editPageLink = this.editPageLink + "?itemid=" + HttpUtility.UrlEncode(encryption.EncryptData(Id));
                    Response.Redirect(this.editPageLink);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.deletePageLink + "?itemid=" + HttpUtility.UrlEncode(encryption.EncryptData(Id)));
                }
            }
        }

        #endregion

        #region Other Events
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addPageLink);
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Encrypt the message Id
        /// </summary>
        /// <param name="messageId">Message Id to encrypt.</param>
        /// <returns>Returns the encrypted message Id</returns>
        protected string GetEncryptID(object messageId)
        {
            ZNodeEncryption encryption = new ZNodeEncryption();
            return HttpUtility.UrlEncode(encryption.EncryptData(messageId.ToString()));
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();
            uxGrid.DataSource = settingsAdmin.GetAllPaymentSettings(UserStoreAccess.GetTurnkeyStoreProfileID);
            uxGrid.DataBind();

            uxGrid.Columns[2].Visible = !Roles.IsUserInRole("VENDOR");
        }
        #endregion
    }
}