<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" 
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Checkout.Payments.PaymentAdd" ValidateRequest="false" Title="Untitled Page" Codebehind="Add.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView Size350">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right"> 
             <zn:Button ID="btnSubmitTop" runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
             <zn:Button ID="btnCancelTop" runat="server" ButtonType="CancelButton"  CausesValidation="false" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
       </div>
        <div class="ClearBoth">
            <asp:Label ID="lblMsg" CssClass="Error" runat="server"></asp:Label></div>
        <div>
            <div>
                <uc1:Spacer ID="Spacer8" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <h4 class="SubTitle">
                <asp:Localize ID="SubTitleGeneralSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralSettings %>' ></asp:Localize></h4>
            <asp:Panel runat="server" ID="ProfilePanel">
            <div class="FieldStyle">
                <asp:Localize ID="ColumnTitleSelectProfiles" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextSelectProfile %>'></asp:Localize> <span class="Asterix">*</span></div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstProfile" runat="server">
                </asp:DropDownList>
            </div>
            </asp:Panel>    
            <div class="FieldStyle">
                <asp:Localize ID="ColumnTitleSelectPayMentType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectPayMentType %>'></asp:Localize><span class="Asterix">*</span></div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstPaymentType" runat="server" OnSelectedIndexChanged="LstPaymentType_SelectedIndexChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
            </div>
            <div class="GoogleCheckout" runat="server" id="googleNote" visible="false">
                <p class="ClearBoth">
                           <asp:Localize ID="TextGoogleCheckout" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextGoogleCheckout %>'></asp:Localize></p>
            </div>
            <div class="FieldStyle">
              <asp:Localize ID="ColumnTitleDisplaySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplaySetting %>'></asp:Localize></div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkActiveInd" runat="server" Checked="true" Text='<%$ Resources:ZnodeAdminResource, CheckboxEnablePaymentOption %>' /></div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnDisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder %>'></asp:Localize><span class="Asterix">*</span><br />
                <small> <asp:Localize ID="ColumnTextAddOnDisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextAddOnDisplayOrder %>'></asp:Localize></small></div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" runat="server" Display="Dynamic"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrderwithStar %>' ControlToValidate="txtDisplayOrder"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtDisplayOrder"
                    Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>' MaximumValue="999999999"
                    MinimumValue="1" Type="Integer"></asp:RangeValidator>
            </div>
            <asp:Panel ID="pnlCreditCard" runat="server" Visible="false">
                <h4 class="SubTitle">
                   <asp:Localize ID="SubTitleMerchantGateWaySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleMerchantGateWaySettings %>'></asp:Localize></h4>
                <asp:Panel ID="pnlGatewayList" runat="server" Visible="true">
                    <div class="FieldStyle">
                        <asp:Localize ID="ColumnTitleSelectGateWay" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectGateWay %>'></asp:Localize><span class="Asterix">*</span></div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="lstGateway" runat="server" OnSelectedIndexChanged="LstGateway_SelectedIndexChanged"
                            AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlLogin" runat="server" Visible="true">
                    <div class="FieldStyle">
                        <asp:Label ID="lblGatewayUserName" runat="server" Text="Merchant Login" /><span
                            class="Asterix">*</span></div>
                   
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtGatewayUserName" runat="server" MaxLength="50" Columns="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtGatewayUserName"
                            Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterMerchantLogin %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>                        
                </asp:Panel>
                <asp:Panel ID="pnlPassword" runat="server" Visible="true">
                    <div class="FieldStyle">
                        <asp:Label ID="lblGatewaypassword" runat="server" Text="Merchant account password" /><span class="Asterix">*</span></div>                    
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtGatewayPassword" runat="server" MaxLength="50" Columns="50" TextMode="Password"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtGatewayPassword"
                            Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredMerchantPassword %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </asp:Panel>
                <asp:Panel ID="pnlAuthorizeNet" runat="server" Visible="false">
                    <div class="FieldStyle">
                        <asp:Label ID="lblTransactionKey" runat="server"><asp:Localize ID="ColumnTitleTransactionKey" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTransactionKey %>'></asp:Localize></asp:Label></div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtTransactionKey" runat="server" MaxLength="70" Columns="50"></asp:TextBox></div>
                </asp:Panel>
                <asp:Panel ID="pnlVerisignGateway" runat="server" Visible="false">
                    <div class="FieldStyle">
                        <asp:Label ID="lblPartner" runat="server"><asp:Localize ID="ColumnTitlesPartner" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlesPartner %>'></asp:Localize></asp:Label><span class="Asterix">*</span><br />
                        <small><asp:Localize ID="HintTextPartner" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextPartner %>'></asp:Localize></small></div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtPartner" runat="server" MaxLength="70" Columns="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPartner"
                            Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequirPartnerName %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                    <div class="FieldStyle">
                        <asp:Label ID="lblVendor" runat="server"><asp:Localize ID="ColumnTitleVendor" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleVendor %>'></asp:Localize></asp:Label><span class="Asterix">*</span><br />
                        <small><asp:Localize ID="HintTextVendor" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextVendor %>'></asp:Localize></small></div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtVendor" runat="server" MaxLength="70" Columns="50"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtVendor"
                            Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredVendorName %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
                   </div>
                </asp:Panel>
                <asp:Panel ID="pnlTestMode" runat="server">
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnTitleGatewayTESTmode" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleGatewayTESTmode %>'></asp:Localize></div>
                <div class="ValueStyle">
                    <asp:CheckBox ID="chkTestMode" runat="server" Checked="false" Text='<%$ Resources:ZnodeAdminResource, TextEnableTestMode %>' /></div>
                </asp:Panel>
                <asp:Panel ID="pnlGatewayOptions" runat="server" Visible="false">
                    <div class="FieldStyle">
                        <asp:Localize ID="ColumnTitleCreditCardAuthorization" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCreditCardAuthorization %>'></asp:Localize></div>
                    <div class="ValueStyle">
                        <asp:CheckBox ID="chkPreAuthorize" runat="server" Checked="false" Text='<%$ Resources:ZnodeAdminResource, CheckBoxPreAuthorize %>' /></div>
                </asp:Panel>
                <asp:Panel ID="pnlCreditCardOptions" runat="server">
                    <div class="FieldStyle">
                     <asp:Localize ID="Localize14" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleAcceptedCards %>'></asp:Localize></div>
                    
                    <div class="ValueStyle">
                        <asp:CheckBox ID="chkEnableVisa" runat="server" Checked="true" Text='<%$ Resources:ZnodeAdminResource, CheckBoxVisa %>' />&nbsp;&nbsp;<asp:CheckBox
                            ID="chkEnableMasterCard" runat="server" Checked="true" Text='<%$ Resources:ZnodeAdminResource, CheckBoxMasterCard %>' />&nbsp;&nbsp;<asp:CheckBox
                                ID="chkEnableAmex" runat="server" Checked="true" Text='<%$ Resources:ZnodeAdminResource, CheckBoxAmericanExpress %>' />&nbsp;&nbsp;<asp:CheckBox
                                    ID="chkEnableDiscover" runat="server" Checked="true" Text='<%$ Resources:ZnodeAdminResource, CheckBoxDiscover %>' />
                    </div>


                </asp:Panel>
                <asp:Panel ID="pnl2COGateway" runat="server" Visible="false">
                    <div class="FieldStyle">
                        <asp:Label ID="Label1" runat="server" Text= '<%$ Resources:ZnodeAdminResource, TextAdditionalFee %>' /></div>                   
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtAdditionalFee" runat="server" MaxLength="11" Columns="50"></asp:TextBox>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtAdditionalFee"
                                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>' MaximumValue="999999999"
                                MinimumValue="0" Type="Double"></asp:RangeValidator>
                    </div>  
                </asp:Panel>
            </asp:Panel>
            <div class="ClearBoth">
            </div>
            <div> 
                 <zn:Button ID="btnSubmitBottom" runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
                 <zn:Button ID="btnCancelBottom" runat="server" ButtonType="CancelButton"  CausesValidation="false" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
           </div>
        </div>
    </div>
</asp:Content>
