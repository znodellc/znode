using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.FranchiseAdmin.Secure.Setup.Checkout.Payments
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_settings_payment_Add class.
    /// </summary>
    public partial class PaymentAdd : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId;
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                ZNodeEncryption encryption = new ZNodeEncryption();
                this.itemId = int.Parse(encryption.DecryptData(Request.Params["itemid"]));
            }
            else
            {
                this.itemId = 0;
            }

            if (Page.IsPostBack == false)
            {
                // If edit func then bind the data fields
                if (this.itemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditPaymentOption").ToString(); 
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddPaymentOption").ToString();  
                }

                // Bind data to the fields on the page
                this.BindData();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();
            PaymentSetting paymentSetting = new PaymentSetting();
            bool isSettingExists = false;

            // If edit mode then retrieve data first
            if (this.itemId > 0)
            {
                paymentSetting = settingsAdmin.GetPaymentSettingByID(this.itemId);

                if (paymentSetting.ProfileID.HasValue)
                {
                    if ((paymentSetting.ProfileID != int.Parse(lstProfile.SelectedValue)) || (paymentSetting.PaymentTypeID != int.Parse(lstPaymentType.SelectedValue)))
                    {
                        // Vheck if this setting already exists for this profile
                        isSettingExists = this.IsPaymentSettingExists();

                        if (isSettingExists)
                        {
                            lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPaymentOption").ToString();  
                            return;
                        }
                    }
                }
                else
                {
                    if (lstProfile.SelectedValue.ToString() != "-1")
                    {
                        // Check if this setting already exists for this profile
                        isSettingExists = this.IsPaymentSettingExists();
                        if (isSettingExists)
                        {
                            lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPaymentOption").ToString();  
                            return;
                        }
                    }
                }
            }

            // Set values based on user input
            paymentSetting.ActiveInd = chkActiveInd.Checked;
            paymentSetting.PaymentTypeID = int.Parse(lstPaymentType.SelectedValue);
            if (lstProfile.SelectedValue == "-1")
            {
                // If all profiles is selected
                paymentSetting.ProfileID = null;
            }
            else
            {
                paymentSetting.ProfileID = int.Parse(lstProfile.SelectedValue);
            }

            paymentSetting.DisplayOrder = Convert.ToInt32(txtDisplayOrder.Text);
            ZNodeEncryption encryption = new ZNodeEncryption();
            var paymentType = (ZNode.Libraries.ECommerce.Entities.PaymentType) Enum.Parse(typeof(ZNode.Libraries.ECommerce.Entities.PaymentType), paymentSetting.PaymentTypeID.ToString());
            switch (paymentType)
            {
                case ZNode.Libraries.ECommerce.Entities.PaymentType.CREDIT_CARD:
                     paymentSetting.GatewayTypeID = int.Parse(lstGateway.SelectedValue);

                    paymentSetting.EnableAmex = chkEnableAmex.Checked;
                    paymentSetting.EnableDiscover = chkEnableDiscover.Checked;
                    paymentSetting.EnableMasterCard = chkEnableMasterCard.Checked;
                    paymentSetting.EnableVisa = chkEnableVisa.Checked;
                    paymentSetting.TestMode = chkTestMode.Checked;
                    paymentSetting.PreAuthorize = chkPreAuthorize.Checked;
                    paymentSetting.GatewayPassword = string.Empty;
                    paymentSetting.TransactionKey = string.Empty;
                    paymentSetting.GatewayUsername = encryption.EncryptData(txtGatewayUserName.Text);
                    
                    var gatewayType = (GatewayType)Enum.Parse(typeof(GatewayType), lstGateway.SelectedValue);
                    switch (gatewayType)
                    {
                        case GatewayType.AUTHORIZE:
                            paymentSetting.TransactionKey = encryption.EncryptData(txtTransactionKey.Text);
                            break;
                        case GatewayType.VERISIGN :
                            // If Verisign PayFlow pro gateway is selected
                            paymentSetting.GatewayPassword = encryption.EncryptData(txtGatewayPassword.Text);
                            paymentSetting.Partner = txtPartner.Text.Trim();
                            paymentSetting.Vendor = txtVendor.Text.Trim();
                            break;
                        case GatewayType.PAYPAL:
                            // If Paypal direct payment gateway is selected
                            paymentSetting.GatewayPassword = encryption.EncryptData(txtGatewayPassword.Text);
                            paymentSetting.TransactionKey = txtTransactionKey.Text;
                            break;
                        case GatewayType.WORLDPAY:
                            // If World Pay gateway is selected
                            paymentSetting.GatewayPassword = encryption.EncryptData(txtGatewayPassword.Text);
                            // Installation Id
                            paymentSetting.TransactionKey = encryption.EncryptData(txtTransactionKey.Text);
                            break;
                        default:
                            paymentSetting.TransactionKey = string.Empty;
                            paymentSetting.GatewayPassword = encryption.EncryptData(txtGatewayPassword.Text);
                            break;
                    }
                    break;
                case ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL :
                    // If Paypal
                    paymentSetting.GatewayTypeID = null;
                    paymentSetting.GatewayUsername = encryption.EncryptData(txtGatewayUserName.Text);
                    paymentSetting.TransactionKey = txtTransactionKey.Text;
                    paymentSetting.GatewayPassword = encryption.EncryptData(txtGatewayPassword.Text);
                    paymentSetting.TestMode = chkTestMode.Checked;
                    paymentSetting.PreAuthorize = chkPreAuthorize.Checked;
                    break;
                case ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT :
                    // If Google Checkout
                    // Set null value to Google
                    paymentSetting.GatewayTypeID = null;
                    paymentSetting.GatewayUsername = encryption.EncryptData(txtGatewayUserName.Text);
                    paymentSetting.GatewayPassword = encryption.EncryptData(txtGatewayPassword.Text);
                    paymentSetting.TestMode = chkTestMode.Checked;
                    break;
                case ZNode.Libraries.ECommerce.Entities.PaymentType.TwoCO:
                    // If 2CO Set null value to 2CO
                    paymentSetting.GatewayTypeID = null;

                    // Vendor ID 
                    paymentSetting.GatewayUsername = encryption.EncryptData(txtGatewayUserName.Text);

                    // Secret word to use MD5
                    paymentSetting.GatewayPassword = encryption.EncryptData(txtGatewayPassword.Text);

                    // Sandbox
                    paymentSetting.TestMode = chkTestMode.Checked;

                    // Additional Fee, if any to add in all transaction, like service charge like that.
                    paymentSetting.Vendor = encryption.EncryptData(txtAdditionalFee.Text);
                    break;
                default:
                    // Purchase Order
                    paymentSetting.GatewayTypeID = null;
                    break;
            }

            bool isSuccess = false;

            // Update Payment setting into database
            if (this.itemId > 0)
            {
                isSuccess = settingsAdmin.UpdatePaymentSetting(paymentSetting);

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit Payment Option - " + lstPaymentType.SelectedItem.Text, lstPaymentType.SelectedItem.Text);
            }
            else
            {
                isSettingExists = settingsAdmin.PaymentSettingExists(int.Parse(lstProfile.SelectedValue), int.Parse(lstPaymentType.SelectedValue));
                if (isSettingExists)
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPaymentOption").ToString();
                    return;
                }

                isSuccess = settingsAdmin.AddPaymentSetting(paymentSetting);

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Added Payment Option - " + lstPaymentType.SelectedItem.Text, lstPaymentType.SelectedItem.Text);
            }

            if (isSuccess)
            {
                // Redirect to main page
                Response.Redirect("~/FranchiseAdmin/Secure/Setup/Checkout/Payments/");
            }
            else
            {
                // Display error message
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoteAdd").ToString();
            }

        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Setup/Checkout/Payments/");
        }

        /// <summary>
        /// Payment type change event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstPaymentType_SelectedIndexChanged(object sender, EventArgs e)
        {
            googleNote.Visible = false;
            var paymentType = (ZNode.Libraries.ECommerce.Entities.PaymentType) Enum.Parse(typeof(ZNode.Libraries.ECommerce.Entities.PaymentType), lstPaymentType.SelectedValue.ToString());
            switch (paymentType)
            {
                case ZNode.Libraries.ECommerce.Entities.PaymentType.CREDIT_CARD:
                    pnlCreditCard.Visible = true;
                    pnlCreditCardOptions.Visible = true;
                    pnlGatewayList.Visible = true;

                    // Hide Additional Fee
                    pnl2COGateway.Visible = false;
                    this.BindPanels();
                    break;
                case ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL :
                    // If Paypal gateway
                    lblTransactionKey.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextAPISignature").ToString();
                    pnlAuthorizeNet.Visible = true;
                    pnlPassword.Visible = true;
                    pnlCreditCard.Visible = true;
                    pnlCreditCardOptions.Visible = false;
                    pnlGatewayList.Visible = false;

                    // Hide Additional Fee
                    pnl2COGateway.Visible = false;
                    break;
                case ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT :
                    // if Google EC gateway
                    googleNote.Visible = true;
                    pnlCreditCardOptions.Visible = false;
                    pnlGatewayList.Visible = false;
                    pnlCreditCard.Visible = true;
                    pnlAuthorizeNet.Visible = false;
                    pnlPassword.Visible = true;
                    pnl2COGateway.Visible = false;
                    break;
                case ZNode.Libraries.ECommerce.Entities.PaymentType.TwoCO :
                    // If 2CO gateway
                    googleNote.Visible = false;
                    pnlCreditCardOptions.Visible = false;
                    pnlGatewayList.Visible = false;
                    pnlCreditCard.Visible = true;
                    pnlAuthorizeNet.Visible = false;
                    pnlPassword.Visible = true;
                    pnl2COGateway.Visible = true;
                    lblGatewayUserName.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "Text2coVendorId").ToString();
                    lblGatewaypassword.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextSecretWoesMDSVerification").ToString();
                    break;
                default :
                    pnlCreditCard.Visible = false;
                    break;

            }
        }

        /// <summary>
        /// Gateway changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstGateway_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindPanels();
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Check if this setting already exists for this profile
        /// </summary>
        /// <returns>Returns true if payment settings already exist else false.</returns>
        private bool IsPaymentSettingExists()
        {
            StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();
            return settingsAdmin.PaymentSettingExists(int.Parse(lstProfile.SelectedValue), int.Parse(lstPaymentType.SelectedValue));
        }

        /// <summary>
        /// Bind the panels.
        /// </summary>
        private void BindPanels()
        {
            // Initialize panels
            pnlLogin.Visible = true;
            pnlPassword.Visible = true;
            pnlVerisignGateway.Visible = false;
            pnlAuthorizeNet.Visible = false;
            pnlGatewayOptions.Visible = false;

            var gatewayType = (GatewayType)Enum.Parse(typeof(GatewayType), lstGateway.SelectedValue);
            switch (gatewayType)
            {
                  case GatewayType.AUTHORIZE:
                    // If authorize.net
                    lblTransactionKey.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnTitleTransactionKey").ToString();
                    pnlAuthorizeNet.Visible = true;
                    pnlLogin.Visible = true;
                    pnlPassword.Visible = false;
                    pnlGatewayOptions.Visible = true;
                    lblGatewayUserName.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextMerchantLogin").ToString();
                    lblGatewaypassword.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnTitleMerchangeAccountPassword").ToString();
                    break;

                case GatewayType.VERISIGN :
                    // If Verisign payflow pro
                    pnlVerisignGateway.Visible = true;
                    pnlAuthorizeNet.Visible = false;
                    pnlLogin.Visible = true;
                    pnlPassword.Visible = true;
                    pnlGatewayOptions.Visible = true;
                    break;

                case GatewayType.PAYPAL :
                    // If paypal direct Payment
                    lblTransactionKey.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextPayPalTransactionKey").ToString();
                    pnlAuthorizeNet.Visible = true;
                    pnlLogin.Visible = true;
                    pnlPassword.Visible = true;
                    break;

                case GatewayType.CYBERSOURCE :
                    // If CyberSource
                    pnlLogin.Visible = false;
                    pnlAuthorizeNet.Visible = false;
                    pnlPassword.Visible = false;
                    pnlVerisignGateway.Visible = false;
                    pnlGatewayOptions.Visible = true;
                    pnlTestMode.Visible = true;
                    break;

                case GatewayType.WORLDPAY :
                    lblGatewayUserName.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextMerchantCode").ToString();
                    lblGatewaypassword.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextAuthorizationPassword").ToString();
                    lblTransactionKey.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextWorldPayInstallationId").ToString();
                    pnlAuthorizeNet.Visible = true;
                    pnlPassword.Visible = true;
                    break;

                case GatewayType.CUSTOM :
                    pnlTestMode.Visible = false;
                    break;

            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields 
        /// </summary>
        private void BindData()
        {
            StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();

            txtDisplayOrder.Text = "1";

            TList<PaymentType> paymentTypeList = settingsAdmin.GetPaymentTypes();
            TList<PaymentType> activePaymentTypeList = paymentTypeList.FindAll(delegate(PaymentType paymentType)
            {
                return paymentType.ActiveInd == true;
            });

            // Get payment types
            lstPaymentType.DataSource = activePaymentTypeList;
            lstPaymentType.DataTextField = "Name";
            lstPaymentType.DataValueField = "PaymentTypeID";
            lstPaymentType.DataBind();
            lstPaymentType.SelectedIndex = 0;

            // Get gateways
            TList<Gateway> gateWayList = settingsAdmin.GetGateways();
            gateWayList.Filter = string.Concat("GatewayTypeID IN [", System.Configuration.ConfigurationManager.AppSettings["EnableGateway"], "]");
            lstGateway.DataSource = gateWayList;
            lstGateway.DataTextField = "GatewayName";
            lstGateway.DataValueField = "GatewayTypeID";
            lstGateway.DataBind();
            lstGateway.SelectedIndex = 0;

            // Get profiles
            lstProfile.DataSource = UserStoreAccess.CheckProfileAccess(settingsAdmin.GetProfiles());
            lstProfile.DataTextField = "Name";
            lstProfile.DataValueField = "ProfileID";
            lstProfile.DataBind();

            if (lstProfile.Items.Count > 1)
            {
                lstProfile.Items.Insert(0, new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAllProfiles").ToString(), "-1"));
                lstProfile.SelectedValue = "-1";
            }

            ProfilePanel.Visible = lstProfile.Items.Count > 1 && !Roles.IsUserInRole("VENDOR");

            try
            {
                if (this.itemId > 0)
                {
                    // Get payment setting
                    PaymentSetting paymentSetting = settingsAdmin.GetPaymentSettingByID(this.itemId);

                    txtDisplayOrder.Text = paymentSetting.DisplayOrder.ToString();

                    UserStoreAccess.CheckProfileAccess(paymentSetting.ProfileID.GetValueOrDefault(0), true);

                    if (paymentSetting.ProfileID.HasValue)
                    {
                        lstProfile.SelectedValue = paymentSetting.ProfileID.Value.ToString();
                    }

                    lstPaymentType.SelectedValue = paymentSetting.PaymentTypeID.ToString();
                    chkActiveInd.Checked = paymentSetting.ActiveInd;
                    ZNodeEncryption encryption = new ZNodeEncryption();
                   
                      var paymentType = (ZNode.Libraries.ECommerce.Entities.PaymentType) Enum.Parse(typeof(ZNode.Libraries.ECommerce.Entities.PaymentType), paymentSetting.PaymentTypeID.ToString());
                      switch (paymentType)
                      {
                          case ZNode.Libraries.ECommerce.Entities.PaymentType.CREDIT_CARD :
                               pnlCreditCard.Visible = true;

                                txtGatewayUserName.Text = encryption.DecryptData(paymentSetting.GatewayUsername);
                                lstGateway.SelectedValue = paymentSetting.GatewayTypeID.ToString();
                                var gatewayType = (GatewayType) Enum.Parse(typeof(GatewayType), lstGateway.SelectedValue);
                                switch (gatewayType)
                                {
                                    case GatewayType.AUTHORIZE :
                                            txtTransactionKey.Text = encryption.DecryptData(paymentSetting.TransactionKey);
                                            lblTransactionKey.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnTitleTransactionKey").ToString();
                                            pnlAuthorizeNet.Visible = true;
                                            pnlPassword.Visible = false;
                                            chkTestMode.Checked = paymentSetting.TestMode;
                                            chkPreAuthorize.Checked = paymentSetting.PreAuthorize;
                                            pnlGatewayOptions.Visible = true;
                                            break;
                                    case GatewayType.VERISIGN :
                                            // If verisign payflow pro
                                            txtVendor.Text = paymentSetting.Vendor;
                                            txtGatewayPassword.Text = encryption.DecryptData(paymentSetting.GatewayPassword);
                                            txtPartner.Text = paymentSetting.Partner;
                                            pnlPassword.Visible = true;
                                            pnlVerisignGateway.Visible = true;
                                            chkTestMode.Checked = paymentSetting.TestMode;
                                            pnlGatewayOptions.Visible = true;
                                            break;
                                    case GatewayType.PAYPAL :
                                            // If NSoftware Gateway
                                            txtTransactionKey.Text = paymentSetting.TransactionKey;
                                            txtGatewayPassword.Text = encryption.DecryptData(paymentSetting.GatewayPassword);
                                            lblTransactionKey.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextTransactionkey").ToString();
                                            pnlAuthorizeNet.Visible = true;
                                            pnlPassword.Visible = true;
                                            chkTestMode.Checked = paymentSetting.TestMode;
                                            break;
                                    case GatewayType.WORLDPAY :
                                            // If World Pay Gateway
                                            txtGatewayPassword.Text = encryption.DecryptData(paymentSetting.GatewayPassword);
                                            txtTransactionKey.Text = encryption.DecryptData(paymentSetting.TransactionKey);
                                            lblGatewayUserName.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextMerchantCode").ToString();
                                            lblGatewaypassword.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextAuthorizationPassword").ToString();
                                            lblTransactionKey.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextWorldPayInstallationId").ToString();
                                            pnlAuthorizeNet.Visible = true;
                                            pnlPassword.Visible = true;
                                            chkTestMode.Checked = paymentSetting.TestMode;
                                            break;
                                    case GatewayType.CYBERSOURCE :
                                            // If CyberSource
                                            pnlLogin.Visible = false;
                                            pnlAuthorizeNet.Visible = false;
                                            pnlPassword.Visible = false;
                                            pnlVerisignGateway.Visible = false;
                                            pnlGatewayOptions.Visible = true;
                                            pnlTestMode.Visible = true;
                                            chkTestMode.Checked = paymentSetting.TestMode;
                                            chkPreAuthorize.Checked = paymentSetting.PreAuthorize;
                                            break;
                                    default :
                                            pnlAuthorizeNet.Visible = false;
                                            pnlPassword.Visible = true;
                                            txtGatewayPassword.Text = encryption.DecryptData(paymentSetting.GatewayPassword);
                                            txtTransactionKey.Text = string.Empty;
                                            chkTestMode.Checked = paymentSetting.TestMode;
                                            break;
                                }
                                lstGateway.SelectedValue = paymentSetting.GatewayTypeID.ToString();
                                chkEnableAmex.Checked = (bool)paymentSetting.EnableAmex;
                                chkEnableDiscover.Checked = (bool)paymentSetting.EnableDiscover;
                                chkEnableMasterCard.Checked = (bool)paymentSetting.EnableMasterCard;
                                chkEnableVisa.Checked = (bool)paymentSetting.EnableVisa;

                                // Hide 2CO panel
                                pnl2COGateway.Visible = false;
                                break;
                          case ZNode.Libraries.ECommerce.Entities.PaymentType.PAYPAL :
                                pnlCreditCard.Visible = true;

                                txtGatewayUserName.Text = encryption.DecryptData(paymentSetting.GatewayUsername);
                                txtTransactionKey.Text = paymentSetting.TransactionKey;
                                txtGatewayPassword.Text = encryption.DecryptData(paymentSetting.GatewayPassword);
                                lblTransactionKey.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextAPISignature").ToString();
                                pnlAuthorizeNet.Visible = true;
                                pnlPassword.Visible = true;
                                pnlGatewayList.Visible = false;
                                pnlCreditCardOptions.Visible = false;
                                chkTestMode.Checked = paymentSetting.TestMode;
                                pnl2COGateway.Visible = false; // Hide 2CO panel
                                break;

                          case ZNode.Libraries.ECommerce.Entities.PaymentType.GOOGLE_CHECKOUT :
                                // If Google Checkout is Selected
                                txtGatewayUserName.Text = encryption.DecryptData(paymentSetting.GatewayUsername);
                                txtGatewayPassword.Text = encryption.DecryptData(paymentSetting.GatewayPassword);
                                chkTestMode.Checked = paymentSetting.TestMode;
                                // Hide CreditCard Options
                                pnlCreditCardOptions.Visible = false;
                                // Hide Gateway dropdownlist
                                pnlGatewayList.Visible = false;
                                // Enable settings
                                pnlCreditCard.Visible = true;
                                // Disable TransactionKey field
                                pnlAuthorizeNet.Visible = false;
                                // Enable PassWord field
                                pnlPassword.Visible = true;
                                // Hide 2CO panel
                                pnl2COGateway.Visible = false;
                                break;
                          case ZNode.Libraries.ECommerce.Entities.PaymentType.TwoCO :
                                // If 2CO is Selected                        
                                txtGatewayUserName.Text = encryption.DecryptData(paymentSetting.GatewayUsername);

                                // Secret word.
                                txtGatewayPassword.Attributes.Add("value", encryption.DecryptData(paymentSetting.GatewayPassword));

                                // Sandbox.
                                chkTestMode.Checked = paymentSetting.TestMode;

                                // Additional Fee
                                txtAdditionalFee.Text = encryption.DecryptData(paymentSetting.Vendor);

                                googleNote.Visible = false;
                                pnlCreditCardOptions.Visible = false;
                                pnlGatewayList.Visible = false;
                                pnlCreditCard.Visible = true;
                                pnlAuthorizeNet.Visible = false;
                                pnlPassword.Visible = true;
                                pnl2COGateway.Visible = true;
                                lblGatewayUserName.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "Text2coVendorId").ToString();
                                lblGatewaypassword.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextSecretWoesMDSVerification").ToString();
                                break;
                          default:
                                pnlCreditCard.Visible = false;
                                break;

                      }
                }
                else
                {
                    // Enable credit card option by default
                    pnlCreditCard.Visible = true;

                    lstGateway.SelectedValue = "1";
                    this.BindPanels();
                }
            }
            catch
            {
            }
        }
        #endregion
    }
}