using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Content.Payment
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_settings_payment_Delete class.
    /// </summary>
    public partial class PaymentDelete : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId;
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the screen
        /// </summary>
        protected void BindData()
        {
            StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();

            // Get payment setting
            PaymentSetting paymentSetting = settingsAdmin.GetPaymentSettingByID(this.itemId);

            UserStoreAccess.CheckProfileAccess(paymentSetting.ProfileID.GetValueOrDefault(0), true);                   
        }

        #endregion

        #region Events
        /// <summary>
        /// Page Load event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                ZNodeEncryption encryption = new ZNodeEncryption();
                this.itemId = int.Parse(encryption.DecryptData(Request.Params["itemid"]));
            }
            else
            {
                this.itemId = 0;
            }

            this.BindData();
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/setup/checkout/payments/");
        }

        /// <summary>
        /// Delete button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            PaymentSetting paymentSetting = storeAdmin.GetPaymentSettingByID(this.itemId);

            PaymentTypeService paymentTypeService = new PaymentTypeService();
            PaymentType paymentType = new PaymentType();
            paymentType = paymentTypeService.GetByPaymentTypeID(paymentSetting.PaymentTypeID);
            paymentTypeService.DeepLoad(paymentType);

            string associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeletePaymentOption").ToString() + paymentType.Name;
            bool isDeleted = false;
            try
            {
                isDeleted = storeAdmin.DeletePaymentSetting(this.itemId);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, paymentType.Name);
            }
            catch
            {
            }

            if (!isDeleted)
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteAction").ToString();
                lblMsg.CssClass = "Error";
            }
            else
            {
                Response.Redirect("~/FranchiseAdmin/Secure/setup/checkout/payments/");
            }
        }
        #endregion
    }
}