<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True"
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Checkout.Payments.PaymentDefault" Title="Untitled Page" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Localize ID="TitlePaymentOptions" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitlePaymentOptions %>'></asp:Localize></h1>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAdd" runat="server" CausesValidation="False"
                ButtonType="Button" OnClick="BtnAdd_Click" Text='<%$ Resources:ZnodeAdminResource, LinkAddNewPayment %>'
                ButtonPriority="Primary" />
        </div>
        <div>
            <uc1:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <p class="ClearBoth">
            <asp:Localize ID="TextPayment" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPayment %>'></asp:Localize>
        </p>
        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <h4 class="GridTitle">
            <asp:Localize ID="GridTitlePaymentOptionsList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitlePaymentOptionsList %>'></asp:Localize></h4>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
            OnRowDeleting="UxGrid_RowDeleting" AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyRecordText %>'>
              <Columns>
                <asp:BoundField DataField="PaymentSettingID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnPaymentOption %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='add.aspx?itemid=<%#GetEncryptID(DataBinder.Eval(Container.DataItem, "PaymentSettingID"))%>'>
                            <%# DataBinder.Eval(Container.DataItem, "PaymentTypeName").ToString()%></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ProfileName" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProfilesName %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleEnabled %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img id="Img1" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleActions %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div class="LeftFloat" style="width: 40%; text-align: left">
                            <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("PaymentSettingID") %>'
                                CommandName="Edit" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' CssClass="LinkButton" />
                        </div>
                        <div class="LeftFloat" style="width: 50%; text-align: left">
                            <asp:LinkButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("PaymentSettingID") %>'
                                CommandName="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CssClass="LinkButton" />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>
