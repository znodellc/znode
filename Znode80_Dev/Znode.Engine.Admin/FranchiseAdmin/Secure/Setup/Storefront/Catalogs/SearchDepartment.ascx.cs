using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Catalogs
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_catalogs_SearchDepartment class.
    /// </summary>
    public partial class SearchDepartment : System.Web.UI.UserControl
    {
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindCatalogData();
                this.BindGridData();
            }

            StringBuilder script = new StringBuilder();
            script.Append(@"<script>function Close(var1, var2){");

            if (Request.Params["sourceId"] != null && Request.Params["source"] != null)
            {
                script.Append("window.opener.document.getElementById('" + Request.Params["sourceId"].ToString() + "').value = var1;");
                script.Append("var control = window.opener.document.getElementById('" + Request.Params["source"].ToString() + "'); " + Environment.NewLine +
                            "if(control != null && control.type == \"text\") " + Environment.NewLine +
                            "{ " + Environment.NewLine +
                            "   //control is textbox " + Environment.NewLine +
                            "   control.value = unescape(var2); " + Environment.NewLine +
                            "} " + Environment.NewLine +
                            "else if(control != null && control.type == \"select-one\") " + Environment.NewLine +
                            "{ " + Environment.NewLine +
                            "   //control is dropdownlist  " + Environment.NewLine +
                            "   control.options.selectedIndex = control.options.indexOf(var1);" + Environment.NewLine +
                            "} ");
            }

            script.Append("window.close();}</script>");
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "ClosePopup", script.ToString());
        }
        #endregion       

        #region General Events
        /// <summary>
        /// Occurs when search button clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Occurs when clear button clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtCategoryName.Text = string.Empty;
            ddlCatalog.SelectedValue = "0";
            this.BindGridData();
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Occurs when page number selected from grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Get the parsed department name.
        /// </summary>
        /// <param name="Name">Department name to parse.</param>
        /// <returns>Returns parsed department name.</returns>
        protected string GetName(object Name)
        {
            string val = string.Empty;
            if (Name != null)
            {
                val = Name.ToString().Replace("'", "\\'");
            }

            return val;
        }
        #endregion

        #region Bind Grid
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            DataSet ds;
            if (ddlCatalog.SelectedValue != "0")
            {
                ds = categoryAdmin.GetCategoriesBySearchData(Server.HtmlEncode(txtCategoryName.Text.Trim()), ddlCatalog.SelectedValue);
            }
            else
            {
                ds = categoryAdmin.GetCategoriesBySearchData(Server.HtmlEncode(txtCategoryName.Text.Trim()), "0");
            }

            DataView dv = new DataView(UserStoreAccess.CheckStoreAccess(ds.Tables[0]));
            dv.Sort = "Name";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind Search data
        /// </summary>
        private void BindSearchData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            DataSet ds = categoryAdmin.GetCategoriesBySearchData(Server.HtmlEncode(txtCategoryName.Text.Trim()), "0");
            DataView dv = new DataView(UserStoreAccess.CheckStoreAccess(ds.Tables[0]));
            dv.Sort = "Name";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind Store Catalogs
        /// </summary>
        private void BindCatalogData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Catalog> catalogList = catalogAdmin.GetAllCatalogs();
            ddlCatalog.DataSource = UserStoreAccess.CheckStoreAccess(catalogList);
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();

            ListItem item = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextSelect").ToString(), "0");
            ddlCatalog.Items.Insert(0, item);
            ddlCatalog.SelectedIndex = 0;
        }
        #endregion
    }
}