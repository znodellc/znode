<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false"
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Catalogs.Add" CodeBehind="Add.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <asp:Panel ID="pnlCatalog" runat="server" DefaultButton="btnSubmit">
        <div>
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div>
            <div class="LeftFloat" style="width: 100%" align="right">
                <zn:Button runat="server" ID="btnBack" ButtonType="EditButton" Width="100px" CausesValidation="False" OnClick="BtnBack_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonBack %>' />
            </div>
        </div>
        <div class="ClearBoth">
        </div>
        <ajaxToolKit:TabContainer ID="tabCatalogSettings" runat="server"
            ActiveTabIndex="0">
            <ajaxToolKit:TabPanel ID="pnlSettings" Visible="false" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="Settings" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleSettings %>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <div class="FieldStyle">
                        <asp:Localize ID="CatalogName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCatalogName %>'></asp:Localize><span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtcatalogName" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcatalogName"
                            ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredCatalogName %>' CssClass="Error" Display="Dynamic"
                            ValidationGroup="grpTitle"></asp:RequiredFieldValidator>
                    </div>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlDepartment" runat="server" Visible="false">
                <HeaderTemplate>
                    <asp:Localize ID="Categories" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleCategory %>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <asp:Panel ID="pnlCategories" runat="server">
                        <div>
                            <div class="ButtonStyle" style="text-transform: none;">
                                <zn:LinkButton ID="btnAddCategory" runat="server" CausesValidation="False"
                                    ButtonType="Button" OnClick="BtnAddCategory_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAssociateCateogory %>'
                                    ButtonPriority="Primary" />
                            </div>
                        </div>
                        <div class="ClearBoth">
                            <br />
                            <br />
                            <p>
                                <asp:Localize ID="AssociateCategories" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextAssociateCategories %>'></asp:Localize></p>
                            <p style="color: red">
                                <asp:Localize ID="AssociateCategoriesIndex" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextAssociateCategoriesIndex %>'></asp:Localize></p>
                        </div>
                        <div class="SearchForm">
                            <h4 class="SubTitle">
                                <asp:Localize ID="SearchCategories" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchCategories %>'></asp:Localize></h4>
                            <asp:Panel ID="pnlsearch" DefaultButton="btnSearch" runat="server">
                                <div class="RowStyle">
                                    <div class="ItemStyle">
                                        <span class="FieldStyle">
                                            <asp:Localize ID="CategoryName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCategoryName %>'></asp:Localize></span><br />
                                        <span class="ValueStyle">
                                            <asp:TextBox ID="txtCategoryName" runat="server"></asp:TextBox></span>
                                    </div>
                                </div>
                                <div class="ClearBoth">
                                </div>
                                <div>
                                    <zn:Button runat="server" ID="btnClear" OnClick="BtnClear_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                                    <zn:Button runat="server" ID="btnSearch" OnClick="BtnSearch_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                                </div>
                            </asp:Panel>
                        </div>
                        <br />
                        <h4 class="GridTitle"><asp:Localize ID="CategoryList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleCategoryList %>'></asp:Localize></h4>
                        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
                            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" EnableSortingAndPagingCallbacks="False"
                            PageSize="25" OnRowDeleting="UxGrid_RowDeleting" AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundCategoriesNotAssociatedCatalog %>'>
                            <Columns>
                                <asp:BoundField DataField="CategoryID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="left" />
                                <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="left" />
                                <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="left" />
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleEnabled %>' HeaderStyle-HorizontalAlign="left">
                                    <ItemTemplate>
                                        <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                            runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnEdit" CssClass="actionlink" runat="server" CommandName="Edit"
                                            Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' CommandArgument='<%# Eval("CategoryNodeID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDelete" CssClass="actionlink" runat="server" CommandName="Delete" OnClientClick="return DeleteConfirmationAssociateCategory();"
                                            Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CommandArgument='<%# Eval("CategoryNodeID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle CssClass="FooterStyle" />
                            <RowStyle CssClass="RowStyle" />
                            <PagerStyle CssClass="PagerStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                        </asp:GridView>
                        <div>
                            <ZNode:Spacer ID="Spacer22" SpacerHeight="10" SpacerWidth="10" runat="server"></ZNode:Spacer>
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
        </ajaxToolKit:TabContainer>
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div>
            <asp:Label ID="lblCategoryError" runat="server" CssClass="Error" Text='<%$ Resources:ZnodeAdminResource, ErrorSelectRootNodeCategory %>'
                Visible="False" />
        </div>
        <br />
        <div style="display: none">
            <zn:Button runat="server" ID="btnSubmit" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancel" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
        </div>
    </asp:Panel>

     <script language="javascript" type="text/javascript">
         function DeleteConfirmationAssociateCategory() {
             return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ConfirmDeleteAssociateCategory")) %>');
        }
    </script>
</asp:Content>
