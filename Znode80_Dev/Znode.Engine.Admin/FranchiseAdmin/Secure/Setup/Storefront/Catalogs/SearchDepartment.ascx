<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Catalogs.SearchDepartment" Codebehind="SearchDepartment.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:UpdatePanel ID="UpdatePnlSearch" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <h1>
                <asp:Localize ID="SearchCategories" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchCategories %>'></asp:Localize></h1>
            <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
                <div class="SearchForm">
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle"><asp:Localize ID="Name" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnName %>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtCategoryName" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle" style="display:none">
                            <span class="FieldStyle"><asp:Localize ID="Catalog" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCatalog %>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:DropDownList ID="ddlCatalog"  runat="server"></asp:DropDownList></span></div>
                    </div>
                    <div class="ClearBoth">
                    </div>
                    <div>
                        <zn:Button runat="server" ID="btnClear" OnClick="BtnClear_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' />
                        <zn:Button runat="server" ID="btnSearch" OnClick="BtnSearch_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                    </div>
                </div>
            </asp:Panel>
            <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
            <h4 class="GridTitle">
                <asp:Localize ID="CategoryList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleCategoryList %>'></asp:Localize>
            </h4>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
                CellPadding="2" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                CaptionAlign="Left" Width="100%" EnableSortingAndPagingCallbacks="False" PageSize="10"
                EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyTextNoCategories %>'>
                <Columns>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <a href="javascript:Close('<%# Eval("CategoryID") %>','<%# GetName(Eval("Name")) %>')">
                                <%# DataBinder.Eval(Container.DataItem, "Name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsActive %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "VisibleInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div>
    <ZNode:Spacer ID="uxSpacer" runat="server" SpacerWidth="5" SpacerHeight="5" />
</div>
<div align="right">
     <zn:Button runat="server" ID="btnClose" OnClientClick="javascript: window.close();" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
</div>
