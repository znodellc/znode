using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Catalogs
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_catalogs_add class.
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Member Variables
        private static bool isSearchEnabled = false;

        private string catalogManagerPageLink = "~/FranchiseAdmin/Secure/Setup/Storefront/Catalogs/Default.aspx";
        private int itemId = 0;        
        private string associateName = string.Empty;
        private CatalogService catalogService = new CatalogService();
        private CategoryNodeService categoryNodeService = new CategoryNodeService();

        private List<string> _PreSelectedItems = new List<string>();

        private string editLink = "~/FranchiseAdmin/Secure/Setup/Storefront/Categories/AssociateCategory.aspx?itemid=";        
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the pre selected items.
        /// </summary>
        public List<string> PreSelectedItems
        {
            get
            {
                return this._PreSelectedItems;
            }

            set
            {
                this._PreSelectedItems = value;
            }
        }

        #endregion

        #region Page Load

        protected void Page_Load(object sender, EventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();

            this.itemId = UserStoreAccess.GetTurnkeyStoreCatalogs[0].CatalogID;

            if (!Page.IsPostBack)
            {
                this.BindEditData();

                if (this.itemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditCatalog").ToString() + Server.HtmlEncode(txtcatalogName.Text.Trim());
                    pnlDepartment.Visible = true;
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddCatalog").ToString();
                    pnlDepartment.Visible = false;
                }
            }
        }

        #endregion

        #region General Events - Settings
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            ZNode.Libraries.DataAccess.Entities.Catalog catalog = new ZNode.Libraries.DataAccess.Entities.Catalog();

            bool isSuccess = false;

            // If edit mode then get all the values first
            if (this.itemId > 0)
            {
                catalog = catalogAdmin.GetCatalogByCatalogId(this.itemId);
            }

            catalog.Name = Server.HtmlEncode(txtcatalogName.Text);
            catalog.PortalID = UserStoreAccess.GetTurnkeyStorePortalID;
            catalog.IsActive = true;

            if (this.itemId > 0)
            {
                isSuccess = catalogAdmin.Update(catalog);
            }
            else
            {
                UserStoreAccess.CheckStoreAccess(-1, true);
                isSuccess = catalogAdmin.Insert(catalog);
            }

            if (!isSuccess)
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCatalog").ToString();
                return;
            }
            else
            {
                if (this.itemId > 0)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit of Catalog - " + txtcatalogName.Text, txtcatalogName.Text);
                    Response.Redirect(this.catalogManagerPageLink);
                }
                else
                {
                    ZNodeEncryption encrypt = new ZNodeEncryption();

                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Creation of Catalog - " + txtcatalogName.Text, txtcatalogName.Text);
                    Response.Redirect(this.catalogManagerPageLink);
                }
            }
        }

        /// <summary> 
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.catalogManagerPageLink);
        }

        /// <summary> 
        /// Add Category Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddCategory_Click(object sender, EventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();

            Response.Redirect("~/FranchiseAdmin/Secure/Setup/Storefront/Categories/AssociateCategory.aspx?ItemId=" + HttpUtility.UrlEncode(encrypt.EncryptData(this.itemId.ToString())) + "&name=" + HttpUtility.UrlEncode(encrypt.EncryptData(txtcatalogName.Text)));
        }

        #endregion

        #region General Events - Department

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindGridData();
            isSearchEnabled = true;
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtCategoryName.Text = string.Empty;
            this.BindGridData();
        }

        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.catalogManagerPageLink);
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (isSearchEnabled)
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindGridData();
            }
            else
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindGridData();
            }
        }

        /// <summary>
        /// Even triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            if (e.CommandName == "Page")
            {
            }
            else
            {
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Edit")
                {
                    this.editLink = this.editLink + HttpUtility.UrlEncode(encrypt.EncryptData(this.itemId.ToString())) + "&nodeId=" + HttpUtility.UrlEncode(encrypt.EncryptData(Id)) + "&name=" + HttpUtility.UrlEncode(encrypt.EncryptData(txtcatalogName.Text));
                    Response.Redirect(this.editLink);
                }
                else if (e.CommandName == "Delete")
                {
                    lblMsg.Text = string.Empty;
                    CategoryAdmin categoryAdmin = new CategoryAdmin();

                    if (!categoryAdmin.IsChildCategoryExist(Convert.ToInt32(Id)))
                    {
                        CategoryNode node = new CategoryNode();
                        node.CategoryNodeID = Convert.ToInt32(Id);

                        node = this.categoryNodeService.GetByCategoryNodeID(Convert.ToInt32(Id));
                        this.categoryNodeService.DeepLoad(node);
                        string DepartmentName = node.CategoryIDSource.Name;

                        UserStoreAccess.CheckCatalogAccess(Convert.ToInt32(node.CatalogID), true);

                        bool isSuccess = categoryAdmin.DeleteNode(node);

                        if (!isSuccess)
                        {
                            lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteCategoryNode").ToString();
                        }
                        else
                        {
                            this.associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "AcitivityLogDeleteAssociationDepartment").ToString(), DepartmentName, txtcatalogName.Text);
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, txtcatalogName.Text);
                        }
                    }
                    else
                    {
                        lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteCategoryChild").ToString();
                    }
                }
            }
        }
        #endregion

        #region Bind Data

        private void BindEditData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            ZNode.Libraries.DataAccess.Entities.Catalog catalog = new ZNode.Libraries.DataAccess.Entities.Catalog();

            if (this.itemId > 0)
            {
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess).Split(',');
                    int found = Array.IndexOf(stores, this.itemId.ToString());
                    if (found == -1)
                    {
                        Response.Redirect("~/FranchiseAdmin/Secure/Setup/Storefront/Catalogs/default.aspx", true);
                    }
                }

                catalog = catalogAdmin.GetCatalogByCatalogId(this.itemId);
                UserStoreAccess.CheckStoreAccess(catalog.PortalID.GetValueOrDefault(0), true);
                lblTitle.Text += catalog.Name;
                txtcatalogName.Text = Server.HtmlDecode(catalog.Name);
                this.BindGridData();
            }
        }

        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            DataSet ds = categoryAdmin.GetCategoriesSearchByCatalog(Server.HtmlEncode(txtCategoryName.Text.Trim()), this.itemId);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                dr["Name"] = categoryAdmin.ParsePath(dr["Name"].ToString(), ">");
            }

            ds.Tables[0].AcceptChanges();
            DataView dv = new DataView(UserStoreAccess.CheckStoreAccess(ds.Tables[0]));
            dv.Sort = "DisplayOrder";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        #endregion
    }
}