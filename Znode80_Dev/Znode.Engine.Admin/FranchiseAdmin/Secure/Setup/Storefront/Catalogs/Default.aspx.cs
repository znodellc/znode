using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Catalogs
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_catalogs_list class.
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region protected member variables
        private string editLink = "~/FranchiseAdmin/Secure/Setup/Storefront/Catalogs/Add.aspx?ItemId=";        
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindGridData();
            }
        }  
        #endregion      

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Manage")
                {
                    Response.Redirect(this.editLink + HttpUtility.UrlEncode(encrypt.EncryptData(e.CommandArgument.ToString())));
                }
            }
        }
        #endregion        

        #region Bind Grid
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Catalog> catalogList = catalogAdmin.GetAllCatalogs();
            DataSet ds = catalogList.ToDataSet(false);
            uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(ds.Tables[0]);
            uxGrid.DataBind();
        }

        #endregion
    }
}