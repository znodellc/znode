<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" 
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Catalogs.Default" ValidateRequest="false" Codebehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div class="Form">
        <div>
            <div class="LeftFloat" style="width: 50%">
                <h1>
                    <asp:Localize ID="Catalogs" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleCatalog %>'></asp:Localize> 
                </h1> 
            </div>
            <div class="ClearBoth" align="left">
            <p><asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCatalogs %>'></asp:Localize></p>
            <br />
        </div>
        </div>
        <div class="ClearBoth" align="left">
            <br />
        </div>
        <h4 class="GridTitle">
           <asp:Localize ID="CatalogList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleCatalogList %>'></asp:Localize></h4>
            <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
                GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
                OnRowCommand="UxGrid_RowCommand" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundCatalog %>'
                Width="100%" CellPadding="4">
                <Columns>
                    <asp:BoundField DataField="CatalogID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="50%" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsActive %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsActive").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleAction %>' ItemStyle-Wrap="false">
                        <ItemTemplate>
                        <div class="LeftFloat" style="width: 25%; text-align: left">
                            <asp:LinkButton ID="Button1" CommandName="Manage" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"catalogID")%>'
                                Text='<%$ Resources:ZnodeAdminResource, LinkManage %>' runat="server" CssClass="actionlink" />
                        </div>                      
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
        </div>
</asp:Content>
