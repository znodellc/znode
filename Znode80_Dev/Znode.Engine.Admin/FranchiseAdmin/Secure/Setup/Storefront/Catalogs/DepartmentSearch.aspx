<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/popup.master" AutoEventWireup="True" ValidateRequest="false" 
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Catalogs.DepartmentSearch" Codebehind="DepartmentSearch.aspx.cs" %>

<%@ Register Src="SearchDepartment.ascx" TagName="DeptSearch" TagPrefix="ZNode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <ZNode:DeptSearch ID="uxDeptSearch" runat="server" />
</asp:Content>
