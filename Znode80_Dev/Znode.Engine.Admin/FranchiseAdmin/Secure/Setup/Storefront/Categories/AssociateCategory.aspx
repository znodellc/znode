<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false"
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Categories.AssociateCategory"
    Title="Untitled Page" CodeBehind="AssociateCategory.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/FranchiseAdmin/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">

    <script type="text/javascript">
        var hiddenTextValue; //alias to the hidden field: hideValue      

        function AutoComplete_ParentCategorySelected(source, eventArgs) {
            hiddenTextValue = $get("<%=ParentCategoryId.ClientID %>");
            hiddenTextValue.value = eventArgs.get_value();
        }

        function AutoComplete_ParentCategoryShowing(source, eventArgs) {
            hiddenTextValue = $get("<%=ParentCategoryId.ClientID %>");
            hiddenTextValue.value = 0;

        }
    </script>

    <asp:HiddenField ID="ParentCategoryId" runat="server"></asp:HiddenField>
    <div class="FormView DesignPageAdd">
        <div>
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>
        <div>
            <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <asp:Panel ID="pnlSearchDepartment" runat="server">
            <div class="FieldStyle" runat="server" id="titleSearch">
                <asp:Localize ID="SearchSelectCategory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSearchSelectCategory %>'></asp:Localize>
                <span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <div class="TextValueStyle">
                    <ZNode:CategoryAutoComplete runat="server" ID="txtCategory" Width="180px" ForceAutoCompleteTextBox="true" IsRequired="true" />
                    <asp:Image CssClass="SearchIcon" ImageUrl="~/FranchiseAdmin/Themes/images/enlarge.gif" ToolTip="Search Category"
                        AlternateText="Search" runat="server" ImageAlign="AbsMiddle" ID="btnShowPopup" />
                </div>
            </div>
        </asp:Panel>
        <h4 class="SubTitle">
            <asp:Localize ID="CategorySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCategorySetting %>'></asp:Localize>
        </h4>
        <div class="FieldStyle">
            <asp:Localize ID="ParentCategory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnParentCategory %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlParentCategory" runat="server" Width="160px"></asp:DropDownList>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="EnableCategory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableCategory %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkDepartment" runat="server" Checked="true" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableCategoryMenu %>'></asp:CheckBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="DisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>'></asp:Localize>
            <span class="Asterix">*</span><br />
            <small>
                <asp:Localize ID="DisplayOrderText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextDisplayOrderCategory %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtDisplayOrder" Text="99" runat="server" Width="60px" MaxLength="5"></asp:TextBox>
            <div>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" Display="Dynamic"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrder %>' ControlToValidate="txtDisplayOrder"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtDisplayOrder"
                    Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeValidDisplayOrder %>' MaximumValue="999999999"
                    MinimumValue="1" Type="Integer"></asp:RangeValidator>
            </div>
        </div>
        <asp:UpdatePanel ID="UpdatePnlSelecCustomer" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Panel ID="pnlAdvanced" runat="server" Visible="true">
                    <h4 class="SubTitle">
                        <asp:Localize ID="AdvancedSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleAdvancedSettings %>'></asp:Localize>
                    </h4>
                    <div class="FieldStyle">
                        <asp:Localize ID="CategoryPageTheme" runat="server" Text='<%$ Resources:ZnodeAdminResource,  ColumnCategoryPageTheme %>'></asp:Localize><br />
                        <small>
                            <asp:Localize ID="CategoryPageThemeText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCategoryPageTheme %>'></asp:Localize></small>
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="ddlThemeslist" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlThemeslist_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                    <asp:Panel ID="pnlTemplateList" runat="server" Visible="false">
                        <div class="FieldStyle">
                            <asp:Localize ID="MasterPageTemplate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnMasterPageTemplate %>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:DropDownList ID="ddlPageTemplateList" runat="server">
                            </asp:DropDownList>
                        </div>
                    </asp:Panel>
                    <asp:Panel ID="pnlCssList" runat="server" Visible="false">
                        <div class="FieldStyle">
                            <asp:Localize ID="CSS" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCSS %>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:DropDownList ID="ddlCSSList" runat="server">
                            </asp:DropDownList>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            <uc1:Spacer ID="Spacer9" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <zn:Button runat="server" ID="btnSubmit" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true" />
            <zn:Button runat="server" ID="btnCancel" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
        </div>
    </div>
</asp:Content>
