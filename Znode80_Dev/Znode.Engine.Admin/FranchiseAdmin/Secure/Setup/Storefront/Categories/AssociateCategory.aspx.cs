using System;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Categories
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_category_AssociateCategory class.
    /// </summary>
    public partial class AssociateCategory : System.Web.UI.Page
    {
        #region Private Variables
        private string associationName = string.Empty;
        private int catalogId = 0;
        private int nodeId = 0;
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            if (Request.Params["nodeId"] != null)
            {
                this.nodeId = int.Parse(HttpUtility.UrlDecode(encrypt.DecryptData(Request.Params["nodeId"].ToString())));               
            }

            if (Request.Params["itemid"] != null)
            {
                this.catalogId = int.Parse(HttpUtility.UrlDecode(encrypt.DecryptData(Request.Params["itemid"].ToString())));
              
            }

            if (Request.Params["name"] != null)
            {
                lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource","TitleAssociateCategoryCatalog") + " - \"" + HttpUtility.UrlDecode(encrypt.DecryptData(Request.Params["name"].ToString())) + "\"";
            }
            else
            {
                lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAssociateCategoryCatalog").ToString();
            }

           // AutoCompleteExtender2.ContextKey = this.catalogId.ToString() + "|" + this.nodeId.ToString();

            CatalogAdmin catalogAdmin = new CatalogAdmin();
            ZNode.Libraries.DataAccess.Entities.Catalog catalog = catalogAdmin.GetCatalogByCatalogId(this.catalogId);
            if (catalog != null)
            {
                UserStoreAccess.CheckStoreAccess(catalog.PortalID.GetValueOrDefault(0), true);
            }

            if (!IsPostBack)
            {
                this.BindParentCategory();
                this.BindThemeList();

                if (this.nodeId > 0)
                {
                    this.BindEditData();
                    btnShowPopup.Visible = false;
                    titleSearch.InnerText = "Category";
                }
                else
                {
                    pnlSearchDepartment.Visible = true;
                    btnShowPopup.Visible = true;
                }
            }

            btnShowPopup.Attributes.Add("onclick", "var newwindow = window.open('../Catalogs/DepartmentSearch.aspx?sourceId=" + txtCategory.ClientId + "&source=" + txtCategory.TxtClientId + "','RequiredDepartment','left=400, top=100, height=540, width= 650, status=n o, resizable= no, scrollbars=no, toolbar= no,location= no, menubar= no'); newwindow.focus(); return false;");
        }       

        #region General Events
        /// <summary>
        /// Theme DropDownList SelecyedIndexChanged event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlThemeslist_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlTemplateList.Visible = true;
            pnlCssList.Visible = true;
            ddlPageTemplateList.Items.Clear();
            ddlCSSList.Items.Clear();

			this.BindMasterPageTemplates();

			this.BindCssList();
        }

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                CategoryAdmin categoryAdmin = new CategoryAdmin();
                CategoryNode categoryInfo = new CategoryNode();

                bool isSuccess = false;
                int? parentNodeId = null;

                ZNodeEncryption encrypt = new ZNodeEncryption();
               
                if (this.nodeId > 0)
                {
                    categoryInfo = categoryAdmin.GetByCategoryNodeId(this.nodeId);
                }
                if (ddlParentCategory.SelectedValue == "0")
                {
                    ParentCategoryId.Value = "0";
                }
                else
                {
                    ParentCategoryId.Value = ddlParentCategory.SelectedValue;
                } 
                parentNodeId = int.Parse(ParentCategoryId.Value);

                categoryInfo.CategoryID = int.Parse(txtCategory.Value);
                categoryInfo.CatalogID = this.catalogId;

                if (categoryInfo.CategoryID == 0)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorValidCategory").ToString();
                    return;
                }

                // Set ParentCategoryNodeId is Null. If the user selects the Root Node.
                if (parentNodeId.Value == 0 && ddlParentCategory.SelectedValue != "0")
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorValidParentCategory").ToString();
                    return;
                }
                else if (parentNodeId.Value == 0 && ddlParentCategory.SelectedValue == "0")
                {
                    parentNodeId = null;
                }

                categoryInfo.ParentCategoryNodeID = parentNodeId;

                categoryInfo.CatalogID = this.catalogId;
                categoryInfo.DisplayOrder = int.Parse(txtDisplayOrder.Text);

                if (ddlThemeslist.SelectedValue != "0")
                {
                    categoryInfo.ThemeID = int.Parse(ddlThemeslist.SelectedValue);
                }
                else
                {
                    categoryInfo.ThemeID = null;
                    categoryInfo.MasterPageID = null;
                    categoryInfo.CSSID = null;
                }

                if (ddlThemeslist.SelectedValue != "0")
                {
                    if (ddlPageTemplateList.SelectedValue != "0")
                    {
                        categoryInfo.MasterPageID = int.Parse(ddlPageTemplateList.SelectedValue);
                    }
                    else
                    {
                        categoryInfo.MasterPageID = null;
                    }

                    if (ddlCSSList.SelectedValue != "0")
                    {
                        categoryInfo.CSSID = int.Parse(ddlCSSList.SelectedValue);
                    }
                    else
                    {
                        categoryInfo.CSSID = null;
                    }
                }

                categoryInfo.ActiveInd = chkDepartment.Checked;

                if (this.nodeId > 0)
                {
                    UserStoreAccess.CheckCatalogAccess(this.catalogId, true);
                    isSuccess = categoryAdmin.UpdateNode(categoryInfo);
                }
                else
                {                   
                    isSuccess = categoryAdmin.AddNode(categoryInfo);
                }

                if (isSuccess)
                {
                    this.associationName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAssociateCategory").ToString(), txtCategory.Text, HttpUtility.UrlDecode(encrypt.DecryptData(Request.Params["name"].ToString())));
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associationName, HttpUtility.UrlDecode(encrypt.DecryptData(Request.Params["name"].ToString())));

                    Response.Redirect("~/FranchiseAdmin/Secure/Setup/Storefront/Catalogs/Add.aspx?Mode=" + HttpUtility.UrlEncode(encrypt.EncryptData("Dept")) + "&itemid=" + HttpUtility.UrlEncode(encrypt.EncryptData(this.catalogId.ToString())),false);
                }
                else
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCategoryExistsInCatalog").ToString();
                }
            }
            catch (Exception ex)
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorOccured").ToString() + ex.Message;
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            Response.Redirect("~/FranchiseAdmin/Secure/Setup/Storefront/Catalogs/Add.aspx?Mode=" + HttpUtility.UrlEncode(encrypt.EncryptData("Dept")) + "&itemid=" + HttpUtility.UrlEncode(encrypt.EncryptData(this.catalogId.ToString())));
        }

        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind edit data.
        /// </summary>
        private void BindEditData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            CategoryNode categoryNode = categoryAdmin.GetByCategoryNodeId(this.nodeId);

            Category category = categoryAdmin.GetByCategoryId(categoryNode.CategoryID);

            if (categoryNode.ParentCategoryNodeID.HasValue)
            {
                ParentCategoryId.Value = categoryNode.ParentCategoryNodeID.Value.ToString();
            }
            else
            {
                ParentCategoryId.Value = "0";
            }

            txtDisplayOrder.Text = categoryNode.DisplayOrder.ToString();
            txtCategory.Text = Server.HtmlDecode(category.Name);
            txtCategory.Value = category.CategoryID.ToString();
            if (categoryNode.ThemeID != null)
            {
                ddlThemeslist.SelectedValue = categoryNode.ThemeID.ToString();
                pnlTemplateList.Visible = true;
                pnlCssList.Visible = true;

                this.BindMasterPageTemplates();

                this.BindCssList();
            }

            chkDepartment.Checked = categoryNode.ActiveInd;

            if (categoryNode.MasterPageID != null)
            {
                ddlPageTemplateList.SelectedValue = categoryNode.MasterPageID.ToString();
            }

            if (categoryNode.CSSID != null)
            {
                ddlCSSList.SelectedValue = categoryNode.CSSID.ToString();
            }
        }

        /// <summary>
        /// Binds the themes to the DropDownList
        /// </summary>
        private void BindThemeList()
        {
            ddlThemeslist.DataSource = DataRepository.ThemeProvider.GetAll();
            ddlThemeslist.DataTextField = "Name";
            ddlThemeslist.DataValueField = "ThemeID";
            ddlThemeslist.DataBind();
            ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownSiteTheme").ToString(), "0");
            ddlThemeslist.Items.Insert(0, li);
			try
			{
				ddlThemeslist.SelectedIndex = 0;
				pnlTemplateList.Visible = true;
				pnlCssList.Visible = true;
				this.BindMasterPageTemplates();
				this.BindCssList();
			}
			catch
			{
			}
        }

        /// <summary>
        /// Bind the CSS to the DropDownList
        /// </summary>
        private void BindCssList()
        {
            if (ddlThemeslist.SelectedIndex >= 0)
            {
                ddlCSSList.DataSource = DataRepository.CSSProvider.GetByThemeID(int.Parse(ddlThemeslist.SelectedValue));
                ddlCSSList.DataTextField = "Name";
                ddlCSSList.DataValueField = "CSSID";
                ddlCSSList.DataBind();
                ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownSameAsStore").ToString(), "0");
                ddlCSSList.Items.Insert(0, li);
            }
        }

        /// <summary>
        /// Bind the MasterPage templates
        /// </summary>
        private void BindMasterPageTemplates()
        {
            if (ddlThemeslist.SelectedIndex >= 0)
            {
                PortalAdmin portalAdmin = new PortalAdmin();

                ddlPageTemplateList.DataSource = portalAdmin.GetMasterPage(int.Parse(ddlThemeslist.SelectedValue), "Category");
                ddlPageTemplateList.DataTextField = "Name";
                ddlPageTemplateList.DataValueField = "MasterPageID";
                ddlPageTemplateList.DataBind();

                // Master template
                ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownSameAsTemplate").ToString(), "0");
                ddlPageTemplateList.Items.Insert(0, li);
            }
        }

        /// <summary>
        /// Bind the Parent Category list
        /// </summary>
        private void BindParentCategory()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            CategoryHelper categoryHelper = new CategoryHelper();

            CategoryNode categoryNode = categoryAdmin.GetByCategoryNodeId(this.nodeId);
            string parentCategory = string.Empty;
            string parentCategoryName = string.Empty; 
            int categoryId = 0;
            int parentCategoryId = 0;

            if (this.nodeId > 0)
            {
                categoryId = categoryNode.CategoryID;
                if (categoryNode.ParentCategoryNodeID.HasValue)
                {
                    parentCategoryId = categoryNode.ParentCategoryNodeID.Value;
                }
            }
            else
            {
                categoryId = 0;
            } 

            DataSet datasetCategory = categoryHelper.GetCategoryNodes(this.catalogId, categoryId);

            foreach (System.Data.DataRow dr in datasetCategory.Tables[0].Select())
            {
                dr["Name"] = categoryAdmin.ParsePath(dr["Name"].ToString(), ">");
                dr["Name"] = Server.HtmlDecode(dr["Name"].ToString());
            }
            ddlParentCategory.DataSource = datasetCategory.Tables[0];
            ddlParentCategory.DataTextField = "Name";
            ddlParentCategory.DataValueField = "CategoryNodeid";
            ddlParentCategory.DataBind();

            if (Request.Params["nodeId"] != null)
            {
                foreach (System.Data.DataRow dr in datasetCategory.Tables[0].Select("CategoryNodeid=" + parentCategoryId.ToString()))
                {
                    parentCategoryName = dr["Name"].ToString();
                    parentCategory = parentCategoryName + " >";
                }
                if (parentCategoryId != 0)
                {
                    ListItem item = ddlParentCategory.Items.FindByText(parentCategoryName);
                    if (item != null)
                    {
                        item.Selected = true;
                    }
                }

            }

            ddlParentCategory.Items.Insert(0, new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownNoParentRoot").ToString(), "0"));
		}

        #endregion
    }
}