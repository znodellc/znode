using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Categories
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_category_Addrelatedproducts class.
    /// </summary>
    public partial class AddRelatedProducts : System.Web.UI.Page
    {
        #region Private Member Variables
        private int _ItemId = 0;
        private string viewPage = "View.aspx?itemid=";
        private string portalIds = string.Empty;
        #endregion
        /// <summary>
        /// Gets or sets the item Id.
        /// </summary>
        public int ItemId
        {
            get { return this._ItemId; }
            set { this._ItemId = value; }
        }

        #region Protected Methods and Events
        /// <summary>
        /// Get thumbnail image relative path
        /// </summary>
        /// <param name="Imagefile">Image file name.</param>
        /// <returns>Returns the thumbnail image relative path.</returns>
        protected string GetImagePath(string Imagefile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(Imagefile);
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            this.portalIds = UserStoreAccess.GetAvailablePortals;

            if (Request.Params["itemid"] != null)
            {
                this._ItemId = int.Parse(HttpUtility.UrlDecode(encrypt.DecryptData(Request.Params["itemid"].ToString())));
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }

        /// <summary>
        /// Submit button event.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Submit_Click(object sender, EventArgs e)
        {
            this.RememberOldValues();

            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            Dictionary<int, string> productIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];

            if (productIdList != null)
            {
                StringBuilder sb = new StringBuilder();
                StringBuilder productName = new StringBuilder();

                foreach (KeyValuePair<int, string> pair in productIdList)
                {
                    // Get ProductId
                    int productId = pair.Key;
                    if (!productCategoryAdmin.ProductExists(productId, this._ItemId))
                    {
                        productCategoryAdmin.Insert(productId, this._ItemId);
                        productName.Append(pair.Value + ",");
                    }
                    else
                    {
                        sb.Append(pair.Value + ",");
                        lblError.Visible = true;
                    }
                }

                Session.Remove("CHECKEDITEMS");

                if (sb.ToString().Length > 0)
                {
                    sb.Remove(sb.ToString().Length - 1, 1);

                    // Display Error message
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductAlreadyAssociated").ToString() + sb.ToString();

                    foreach (GridViewRow row in uxGrid.Rows)
                    {
                        CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProduct") as CheckBox;

                        if (check != null)
                        {
                            check.Checked = false;
                        }
                    }
                }
                else
                {
                    CategoryAdmin categoryAdmin = new CategoryAdmin();
                    Category category = categoryAdmin.GetByCategoryId(this._ItemId);

                    string associationName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogProductCategory").ToString(), productName, category.Name);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associationName, category.Name);

                    ZNodeEncryption encrypt = new ZNodeEncryption();

                    Response.Redirect(this.viewPage + HttpUtility.UrlEncode(encrypt.EncryptData(this._ItemId.ToString())) + "&mode=" + HttpUtility.UrlEncode(encrypt.EncryptData("1")) + string.Empty);
                }
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            Response.Redirect(this.viewPage + HttpUtility.UrlEncode(encrypt.EncryptData(this._ItemId.ToString())) + "&mode=" + HttpUtility.UrlEncode(encrypt.EncryptData("1")) + string.Empty);
        }

        /// <summary>
        /// Search Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchProduct();
        }

        /// <summary>
        /// Clear Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            Response.Redirect("AddRelatedProducts.aspx?itemid=" + HttpUtility.UrlEncode(encrypt.EncryptData(this._ItemId.ToString())));
        }

        /// <summary>
        /// Grid PageIndexChanging Event.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.RememberOldValues();

            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchProduct();
        }

        /// <summary>
        /// Row Databound Event.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Dictionary<int, string> productIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];

                if (productIdList != null)
                {
                    CheckBox check = (CheckBox)e.Row.Cells[0].FindControl("chkProduct") as CheckBox;
                    int id = int.Parse(e.Row.Cells[1].Text);

                    if (productIdList.ContainsKey(id) && check != null)
                    {
                        check.Checked = true;
                    }
                }
            }
        }

        /// <summary>
        /// Back Button Click Event.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            Response.Redirect(this.viewPage + HttpUtility.UrlEncode(encrypt.EncryptData(this._ItemId.ToString())) + "&mode=" + HttpUtility.UrlEncode(encrypt.EncryptData("1")) + string.Empty);
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind category name.
        /// </summary>
        private void BindData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            Category category = categoryAdmin.GetByCategoryId(this._ItemId);
            lblTitle.Text = category.Name;
        }

        /// <summary>
        /// Search a Product by name,productnumber,sku,manufacturer,category,producttype
        /// </summary>
        private void BindSearchProduct()
        {
            ProductAdmin prodadmin = new ProductAdmin();
            DataSet ds = prodadmin.SearchProductByNames(Server.HtmlEncode(txtproductname.Text.Trim()), txtproductnumber.Text.Trim(), txtsku.Text.Trim(), string.Empty, string.Empty, txtCategory.Text.Trim(), this.portalIds);
           
            if (ds.Tables.Count > 0)
            {
                pnlProductList.Visible = true;             
                DataView dv = new DataView(UserStoreAccess.CheckProductAccess(ds.Tables[0].DefaultView.ToTable(), true));
                uxGrid.DataSource = dv;
                uxGrid.DataBind();
                butAddNew.Visible = true;
                Cancel.Visible = true;
                if (dv.Table.Rows.Count == 0)
                {
                    butAddNew.Visible = false;
                    Cancel.Visible = false;
                }
            }
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Remember old value.
        /// </summary>
        private void RememberOldValues()
        {
            Dictionary<int, string> productIdList = new Dictionary<int, string>();

            // Loop through the grid values
            foreach (GridViewRow row in uxGrid.Rows)
            {
                CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProduct") as CheckBox;

                // Check in the Session
                if (Session["CHECKEDITEMS"] != null)
                {
                    productIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];
                }

                int id = int.Parse(row.Cells[1].Text);

                if (check.Checked)
                {
                    if (!productIdList.ContainsKey(id))
                    {
                        productIdList.Add(id, row.Cells[3].Text);
                    }
                }
                else
                {
                    productIdList.Remove(id);
                }
            }

            if (productIdList.Count > 0)
            {
                Session["CHECKEDITEMS"] = productIdList;
            }
        }
       
        #endregion
    }
}