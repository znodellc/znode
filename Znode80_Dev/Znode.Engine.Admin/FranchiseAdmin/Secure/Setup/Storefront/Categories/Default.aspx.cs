using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Categories
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_category_List class.
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Private Variables
        private static bool isSearchEnabled = false;
        private string addLink = "~/FranchiseAdmin/Secure/Setup/Storefront/Categories/Add.aspx";
        private string editLink = "~/FranchiseAdmin/Secure/Setup/Storefront/Categories/View.aspx";
        private string deleteLink = "~/FranchiseAdmin/Secure/Setup/Storefront/Categories/ConfirmationPage.aspx";        
        #endregion

        #region Helper Methods
        /// <summary>
        /// Get the encrypted product category Id.
        /// </summary>
        /// <param name="productCategoryId">Product category Id to encrypt.</param>
        /// <returns>Returns the encrypted product category Id.</returns>
        protected string GetEncryptID(object productCategoryId)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            if (productCategoryId != null)
            {
                return HttpUtility.UrlEncode(encrypt.EncryptData(productCategoryId.ToString()));
            }
            else
            {
                return HttpUtility.UrlEncode(encrypt.EncryptData(string.Empty));
            }
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindCatalogData();
                this.BindGridData();
            }
        }
        #endregion        

        #region General Events

        protected void BtnAddCategory_Click(object sender, System.EventArgs e)
        {
            Response.Redirect(this.addLink);
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
            isSearchEnabled = true;
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Setup/Storefront/Categories/Default.aspx");
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (isSearchEnabled)
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindSearchData();
            }
            else
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindGridData();
            }
        }

        /// <summary>
        /// Even triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();

            if (e.CommandName == "Page")
            {
            }
            else
            {
                string Id = e.CommandArgument.ToString();
                if (e.CommandName == "Manage")
                {
                    this.editLink = this.editLink + "?itemid=" + HttpUtility.UrlEncode(encrypt.EncryptData(Id));
                    Response.Redirect(this.editLink);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.deleteLink + "?itemid=" + HttpUtility.UrlEncode(encrypt.EncryptData(Id)));
                }
            }
        }
        #endregion     
  
        #region Bind Grid

        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            DataSet ds;
            if (ddlCatalog.SelectedValue != "0")
            {
                ds = categoryAdmin.GetCategoriesBySearchData(Server.HtmlEncode(txtCategoryName.Text.Trim()), ddlCatalog.SelectedValue);
            }
            else
            {
                ds = categoryAdmin.GetCategoriesBySearchData(Server.HtmlEncode(txtCategoryName.Text.Trim()), "0");

            }

            DataView dv = new DataView(UserStoreAccess.CheckStoreAccess(ds.Tables[0]));
            dv.Sort = "DisplayOrder";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind Search data
        /// </summary>
        private void BindSearchData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            DataSet ds = categoryAdmin.GetCategoriesBySearchData(Server.HtmlEncode(txtCategoryName.Text.Trim()), "0");
            DataView dv = new DataView(UserStoreAccess.CheckStoreAccess(ds.Tables[0]));
            dv.Sort = "Name";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind Store Catalogs
        /// </summary>
        private void BindCatalogData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Catalog> catalogList = catalogAdmin.GetAllCatalogs();
            DataSet ds = catalogList.ToDataSet(false);

            ddlCatalog.DataSource = UserStoreAccess.CheckStoreAccess(catalogList);
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();

            ListItem item = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextSelect").ToString(), "0");
            ddlCatalog.Items.Insert(0, item);
            ddlCatalog.SelectedIndex = 0;
        }

        #endregion
    }
}