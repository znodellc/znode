<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True"
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Categories.ConfirmationPage" Title="Untitled Page" CodeBehind="ConfirmationPage.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h5>
        <asp:Localize ID="TitlePlease" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm %>'></asp:Localize>
    </h5>
    <p>
        <asp:Label ID="DeleteConfirmText" runat="server"></asp:Label>
    </p>
    <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label><br />
    <br />
    <zn:Button runat="server" ID="btnDelete" OnClick="BtnDelete_Click" ButtonType="CancelButton" CausesValidation="true" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>' />
    <zn:Button runat="server" ID="btnCancel" OnClick="BtnCancel_Click" CausesValidation="False" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
    <br />
    <br />
    <br />
</asp:Content>
