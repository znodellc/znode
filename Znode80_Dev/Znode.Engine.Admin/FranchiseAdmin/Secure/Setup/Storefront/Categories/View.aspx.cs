using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Categories
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_category_View class.
    /// </summary>
    public partial class View : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId;
        private int mode = 0;
        private string associateName = string.Empty;
        private string addRelatedProductLink = "AddRelatedProducts.aspx?itemid=";
        private ProductService productService = new ProductService();
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();

            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(HttpUtility.UrlDecode(encrypt.DecryptData(Request.Params["itemid"])));
            }
            else
            {
                this.itemId = 0;
            }

            // Get ItemId from querystring        
            if (Request.Params["Mode"] != null)
            {
                this.mode = int.Parse(HttpUtility.UrlDecode(encrypt.DecryptData(Request.Params["Mode"])));
            }
            else
            {
                this.mode = 0;
            }

            if (this.mode == 1)
            {
                tabCategory.ActiveTabIndex = 1;
            }

            if (Page.IsPostBack == false)
            {
                this.BindRelatedProducts();

                // If edit function then bind the data fields
                if (this.itemId > 0)
                {
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = "Add Category";
                }
            }

            this.uxManage.SubmitButtonClicked += new EventHandler(this.ModalPopClose_Click);
            this.uxManage.CancelButtonClicked += new EventHandler(this.ModalPopClose_Click);
        }
        #endregion        

        #region General Events

        /// <summary>
        /// Edit category button click event. 
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEditCategory_Click(object sender, EventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            Response.Redirect("~/FranchiseAdmin/Secure/Setup/Storefront/Categories/Add.aspx?itemid=" + HttpUtility.UrlEncode(encrypt.EncryptData(this.itemId.ToString())));
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Setup/Storefront/Categories/Default.aspx");
        }

        protected void Addrelatedproducts_Click(object sender, EventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            Response.Redirect(this.addRelatedProductLink + HttpUtility.UrlEncode(encrypt.EncryptData(this.itemId.ToString())));
        }

        protected void ModalPopClose_Click(object sender, EventArgs e)
        {
            this.BindRelatedProducts();
            updPnlRelatedProductGrid.Update();
            mdlPopup.Hide();
        }
        #endregion

        #region Grid Events
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindRelatedProducts();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Manage")
                {
                    uxManage.ItemId = int.Parse(e.CommandArgument.ToString());
                    uxManage.BindThemeList();
                    uxManage.BindMasterPageTemplates();
                    uxManage.BindData();
                    mdlPopup.Show();
                }
                else if (e.CommandName == "RemoveItem")
                {
                    CategoryAdmin categoryAdmin = new CategoryAdmin();
                    Category category = categoryAdmin.GetByCategoryId(this.itemId);

                    int Id = Convert.ToInt32(e.CommandArgument.ToString());

                    ZNode.Libraries.DataAccess.Entities.Product node = new ZNode.Libraries.DataAccess.Entities.Product();
                    node = this.productService.GetByProductID(Id);
                    this.productService.DeepLoad(node);
                    string productName = node.Name;

                    ProductCategoryAdmin prodCategoryAdmin = new ProductCategoryAdmin();

                    bool isRemoved = prodCategoryAdmin.RemoveProduct(int.Parse(e.CommandArgument.ToString()), this.itemId);
                    if (isRemoved)
                    {
                        this.associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteAsscociation").ToString(), productName, category.Name);
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, category.Name);

                        this.BindRelatedProducts();
                    }
                }
            }
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        private void BindEditData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            Category category = categoryAdmin.GetByCategoryId(this.itemId);

            if (category != null)
            {
                UserStoreAccess.CheckStoreAccess(category.PortalID.GetValueOrDefault(0), true);
                lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleManageCategory").ToString() + category.Name;
                lblName.Text = category.Name;
                lblshortdescription.Text = category.ShortDescription;
                lblLongDescription.Text = category.Description;
                lblAlternativeDesc.Text = category.AlternateDescription;
                lblDisplayOrder.Text = category.DisplayOrder.GetValueOrDefault(1).ToString();
                chkCategoryEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(category.VisibleInd.ToString()));
                lblCategoryTitle.Text = category.Title;
                chkDisplaySubCategory.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(category.SubCategoryGridVisibleInd.ToString()));
                lblSEOMetaDescription.Text = category.SEODescription;
                lblSEOMetaKeywords.Text = category.SEOKeywords;
                lblSEOTitle.Text = category.SEOTitle;
                lblSEOURL.Text = category.SEOURL;

                ZNodeImage znodeImage = new ZNodeImage();
                CategoryImage.ImageUrl = znodeImage.GetImageHttpPathMedium(category.ImageFile);
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoCategoryFound").ToString());
            }
        }

        /// <summary>
        /// Bind Related Products 
        /// </summary>
        private void BindRelatedProducts()
        {
            ProductCategoryAdmin prodCategoryAdmin = new ProductCategoryAdmin();
            DataSet categoryDataSet = prodCategoryAdmin.GetByCategoryID(this.itemId);
            DataView dv = new DataView(categoryDataSet.Tables[0]);
            dv.Sort = "DisplayOrder";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }
        #endregion
    }
}