using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Categories
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_category_ConfirmationPage class.
    /// </summary>
    public partial class ConfirmationPage : System.Web.UI.Page
    {
        #region Private Variables
        private int itemId;
        private string _ProductCategoryName = string.Empty;
        private string categoryName = string.Empty;
        private CategoryNodeService categoryNodeService = new CategoryNodeService();
        #endregion

        #region Public Property
        /// <summary>
        /// Gets or sets the product category name.
        /// </summary>
        public string ProductCategoryName
        {
            get { return this._ProductCategoryName; }
            set { this._ProductCategoryName = value; }
        } 
        #endregion

        #region page load
        protected void Page_Load(object sender, EventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();

            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(HttpUtility.UrlDecode(encrypt.DecryptData(Request.Params["itemid"])));
            }
            else
            {
                this.itemId = 0;
            }

           this.BindData();

           DeleteConfirmText.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextDeleteConfirmCategory").ToString(), this.ProductCategoryName);
        }
        #endregion       

        #region Events
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Setup/Storefront/Categories/Default.aspx");
        }

        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            bool isDeleted = false;

            try
            {
                Category category = new Category();
                CategoryNode categoryNode = new CategoryNode();
                CategoryAdmin categoryAdmin = new CategoryAdmin();

                category.CategoryID = this.itemId;

                TList<CategoryNode> categoryNodes = categoryAdmin.GetCategoryNodeId(this.itemId);

                foreach (CategoryNode node in categoryNodes)
                {
                    categoryNode.CategoryID = this.itemId;
                    categoryNode.CategoryNodeID = node.CategoryNodeID;
                    isDeleted = categoryAdmin.DeleteNode(categoryNode);
                }

                category = categoryAdmin.GetByCategoryId(this.itemId);
                this.categoryName = category.Name;

                isDeleted = categoryAdmin.Delete(category);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }

            if (isDeleted)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Delete Category - " + this.categoryName, this.categoryName);

                Response.Redirect("~/FranchiseAdmin/Secure/Setup/Storefront/Categories/Default.aspx");
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCategoryDelete").ToString();
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the screen
        /// </summary>
        private void BindData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            Category category = categoryAdmin.GetByCategoryId(this.itemId);

            if (category != null)
            {
                UserStoreAccess.CheckStoreAccess(category.PortalID.GetValueOrDefault(0), true);
                this.ProductCategoryName = category.Name;
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoCategory").ToString());
            }
        }
        #endregion
    }
}