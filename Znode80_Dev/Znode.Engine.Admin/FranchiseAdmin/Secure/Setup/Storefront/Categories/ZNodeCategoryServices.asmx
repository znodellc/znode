<%@ WebService Language="C#" Class="ZNodeCategoryServices" %>

using System;
using System.Collections;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Services.Protocols;
using System.Collections.Generic;
using System.Data;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Custom;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ZNodeCategoryServices : System.Web.Services.WebService
{

    /// <summary>
    /// Gets the category list data table.
    /// </summary>    
    public DataTable CategoryListTable
    {
        get
        {
            if (System.Web.HttpContext.Current.Session["CategoryList"] == null)
            {
                ZNode.Libraries.DataAccess.Custom.CategoryHelper categoryHelper = new ZNode.Libraries.DataAccess.Custom.CategoryHelper();

                int catalogId = 0;
                
                // Get Category
                if (Session["CatalogId"] != null)
                    catalogId = int.Parse(Session["CatalogId"].ToString());
                System.Data.DataSet ds = categoryHelper.GetCategories();

                System.Web.HttpContext.Current.Session["CategoryList"] = ds.Tables[0];
                return ds.Tables[0];
            }
            else
            {
                return (DataTable)System.Web.HttpContext.Current.Session["CategoryList"] as DataTable;
            }
        }
    }

    [System.Web.Services.WebMethod(true)]
    [System.Web.Script.Services.ScriptMethod]
    public string[] GetCompletionListWithContextAndValues(string prefixText, int count, string contextKey)
    {
        ArrayList items = new ArrayList(count);

        System.Data.DataView dv = new System.Data.DataView(CategoryListTable);
        dv.RowFilter = "Name like '" + prefixText + "%'";
        int counter = 1;

        foreach (System.Data.DataRowView dr in dv)
        {
            if (counter++ >= count)
            {
                break;
            }

            items.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(dr["Name"].ToString(), dr["CategoryId"].ToString()));
        }

        return (string[])items.ToArray(typeof(string));
    }

    [System.Web.Services.WebMethod(true)]
    [System.Web.Script.Services.ScriptMethod]
    public string[] GetParentCategories(string prefixText, int count, string contextKey)
    {
        ArrayList items = new ArrayList(count);
        int counter = 1;

        int catalogId = Convert.ToInt32(contextKey.Split('|')[0]);
        int nodeId = Convert.ToInt32(contextKey.Split('|')[1]);
        
        CategoryAdmin categoryAdmin = new CategoryAdmin();
        CategoryHelper categoryHelper = new CategoryHelper();

        CategoryNode categoryNode = categoryAdmin.GetByCategoryNodeId(nodeId);

        int categoryId = 0;

        if (nodeId > 0)
        {
            categoryId = categoryNode.CategoryID;
        }
        else
        {
            categoryId = 0;
        }

        DataSet categoryDataSet = categoryHelper.GetCategoryNodes(catalogId, categoryId);
        
        foreach (System.Data.DataRow dr in categoryDataSet.Tables[0].Select("Name LIKE '%" + prefixText + "%'"))
        {
            if (counter++ >= count)
            {
                break;
            }

            items.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(categoryAdmin.ParsePath(dr["Name"].ToString(), ">"), dr["CategoryNodeid"].ToString()));
        }

        return (string[])items.ToArray(typeof(string));
    }        
}
