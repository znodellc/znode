using System;
using System.Data;
using System.Web;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Categories
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_catalog_product_category_Add class.
    /// </summary>
    public partial class ProductCategoryAdd : System.Web.UI.Page
    {
        #region Private Variables        
        private int itemId;        
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();

            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(encrypt.DecryptData(Request.Params["itemid"]));
            }
            else
            {
                this.itemId = 0;
            }

            if (Page.IsPostBack == false)
            {
                // If edit function then bind the data fields
                if (this.itemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditCategory").ToString();
                    tblShowImage.Visible = true;
                    pnlImageUpload.Attributes["style"] += "padding-left: 10px; border-left: solid 1px #cccccc;";
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddCategory").ToString();
                    tblShowImage.Visible = true;
                    pnlImage.Visible = false;
                    pnlImageOption.Visible = false;
                    pnlImageUpload.Visible = true;
                    tblCategoryDescription.Visible = true;
                }
            }
        }
        #endregion        

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            string fileName = string.Empty;
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            Category category = new Category();
            CategoryNode categoryNode = new CategoryNode();
            ZNode.Libraries.DataAccess.Entities.Catalog catalog = new ZNode.Libraries.DataAccess.Entities.Catalog();
            PortalCatalog portalCatalog = new PortalCatalog();
            string mappedSEOUrl = string.Empty;

            if (this.itemId > 0)
            {
                category = categoryAdmin.GetByCategoryId(this.itemId);            
            }

            category.CategoryID = this.itemId;
            category.Name = Server.HtmlEncode(txtName.Text);
            category.ShortDescription = Server.HtmlEncode(txtshortdescription.Text);
            category.Description = ctrlHtmlText.Html;
            category.AlternateDescription = ctrlHtmlText1.Html;
            category.Title = Server.HtmlEncode(txtTitle.Text);
            category.SubCategoryGridVisibleInd = chkSubCategoryGridVisibleInd.Checked;
            category.SEOTitle = Server.HtmlEncode(txtSEOTitle.Text);
            category.SEOKeywords = Server.HtmlEncode(txtSEOMetaKeywords.Text);
            category.SEODescription = Server.HtmlEncode(txtSEOMetaDescription.Text);
            category.SEOURL = null;
            if (txtSEOURL.Text.Trim().Length > 0)
            {
                category.SEOURL = txtSEOURL.Text.Trim().Replace(" ", "-");
            }

            category.ImageAltTag = txtImageAltTag.Text.Trim();
            category.DisplayOrder = int.Parse(DisplayOrder.Text);
            category.VisibleInd = VisibleInd.Checked;
            category.PortalID = UserStoreAccess.GetTurnkeyStorePortalID;

            if (category.SEOURL != mappedSEOUrl)
            {
                if (urlRedirectAdmin.SeoUrlExists(category.SEOURL, category.CategoryID))
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSEOFriendlyURLAlreadyExist").ToString();
                    return;
                }
            }

            // Image Validation            
            if ((this.itemId == 0) || (RadioCategoryNewImage.Checked == true))
            {
                if (UploadCategoryImage.PostedFile != null)
                {
                    if (UploadCategoryImage.PostedFile.FileName != string.Empty)
                    {
                        // Check for Product Image
                        fileName = "Turnkey/" + UserStoreAccess.GetTurnkeyStorePortalID.Value.ToString() + "/" + System.IO.Path.GetFileNameWithoutExtension(UploadCategoryImage.PostedFile.FileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(UploadCategoryImage.PostedFile.FileName);
                        if (fileName != string.Empty)
                        {
                            category.ImageFile = fileName;
                        }
                    }
                }
            }
            else
            {
                category.ImageFile = category.ImageFile;
            }            

            // Upload File if this is a new product or the New Image option was selected for an existing product
            if (RadioCategoryNewImage.Checked || this.itemId == 0)
            {
                if (fileName != string.Empty)
                {
                    byte[] imageData = new byte[UploadCategoryImage.PostedFile.InputStream.Length];
                    UploadCategoryImage.PostedFile.InputStream.Read(imageData, 0, (int)UploadCategoryImage.PostedFile.InputStream.Length);
                    ZNodeStorageManager.WriteBinaryStorage(imageData, ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + fileName);
                    UploadCategoryImage.Dispose();
                }
            }

            bool isSuccess = false;

            // Get all CategoryNodes.
            CategoryHelper categoryHelper = new CategoryHelper();
            System.Data.DataSet ds = categoryHelper.GetAllCategoryNodes();
            DataView dv = new DataView();
            dv = new DataView(ds.Tables[0]);

            if (this.itemId > 0)
            {
                isSuccess = categoryAdmin.Update(category);
            }
            else
            {
                isSuccess = categoryAdmin.Add(category);
            }

            if (isSuccess)
            {
                urlRedirectAdmin.UpdateUrlRedirect(mappedSEOUrl);
                ZNodeEncryption encrypt = new ZNodeEncryption();
                if (this.itemId > 0)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditCategory") + txtName.Text, txtName.Text);
                    Response.Redirect("~/FranchiseAdmin/Secure/Setup/Storefront/Categories/View.aspx?ItemId=" + HttpUtility.UrlEncode(encrypt.EncryptData(this.itemId.ToString())));
                }
                else
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogCreateCategory") + txtName.Text, txtName.Text);
                    Response.Redirect("~/FranchiseAdmin/Secure/Setup/Storefront/Categories/View.aspx?ItemId=" + HttpUtility.UrlEncode(encrypt.EncryptData(category.CategoryID.ToString())));
                }
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCategoryChanges").ToString();
                return;
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            ZNodeEncryption encrypt = new ZNodeEncryption();
            if (this.itemId > 0)
            {
                Response.Redirect("~/FranchiseAdmin/Secure/Setup/Storefront/Categories/View.aspx?ItemId=" + HttpUtility.UrlEncode(encrypt.EncryptData(this.itemId.ToString())));
            }
            else
            {
                Response.Redirect("~/FranchiseAdmin/Secure/Setup/Storefront/Categories/Default.aspx");
            }
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioCategoryCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblCategoryDescription.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioCategoryNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblCategoryDescription.Visible = true;
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        private void BindEditData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            Category category = categoryAdmin.GetByCategoryId(this.itemId);

            if (category != null)
            {
                UserStoreAccess.CheckStoreAccess(category.PortalID.GetValueOrDefault(0), true);

                txtName.Text = Server.HtmlDecode(category.Name);
                txtshortdescription.Text = Server.HtmlDecode(category.ShortDescription);
                ctrlHtmlText.Html = category.Description;
                ctrlHtmlText1.Html = category.AlternateDescription;
                DisplayOrder.Text = category.DisplayOrder.GetValueOrDefault(1).ToString();
                VisibleInd.Checked = category.VisibleInd;
                txtTitle.Text = Server.HtmlDecode(category.Title);
                chkSubCategoryGridVisibleInd.Checked = category.SubCategoryGridVisibleInd;
                txtSEOMetaDescription.Text = Server.HtmlDecode(category.SEODescription);
                txtSEOMetaKeywords.Text = Server.HtmlDecode(category.SEOKeywords);
                txtSEOTitle.Text = Server.HtmlDecode(category.SEOTitle);
                txtSEOURL.Text = category.SEOURL;
                ZNodeImage znodeImage = new ZNodeImage();
                Image1.ImageUrl = znodeImage.GetImageHttpPathMedium(category.ImageFile);
                txtImageAltTag.Text = category.ImageAltTag;
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoCategory").ToString());
            }
        }
        #endregion
    }
}
