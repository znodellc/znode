<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false"
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Stores.EditShipping" CodeBehind="EditShipping.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right; padding-right: 10px;">
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmitTop" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelTop" />
        </div>
        <div align="left">
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="SubTitleOrigin" Text='<%$ Resources:ZnodeAdminResource, SubTitleShippingOrigin%>' runat="server"></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleAdd1" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOriginAdd1%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtShippingAddress1" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleAdd2" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOriginAdd2%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtShippingAddress2" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleCity" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOriginCity%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtShippingCity" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleCode" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOriginStateCode%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtShippingStateCode" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleZipCode" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOriginZipCode%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtShippingZipCode" runat="server" Width="152px" MaxLength="20"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleCountryCode" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOriginCountryCode%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="lstCountries" runat="server">
            </asp:DropDownList>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleOriginPhone" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOriginPhone%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtShippingPhone" runat="server" Width="152px"></asp:TextBox>
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="SubTitleFedEx" Text='<%$ Resources:ZnodeAdminResource, SubTitleFedEx%>' runat="server"></asp:Localize></h4>
        <p>
            <asp:Localize ID="SubTextFedex" Text='<%$ Resources:ZnodeAdminResource, SubTextFedex%>' runat="server"></asp:Localize>
            <br />
            <br />
        </p>
        <div class="FieldStyle">
            <asp:Localize ID="FedexAccount" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFedexAccount%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAccountNum" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="FedexMeter" Text='<%$ Resources:ZnodeAdminResource,  ColumnTitleFedexMeter%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMeterNum" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="FedExkey" Text='<%$ Resources:ZnodeAdminResource,  ColumnTitleFedExkey%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtProductionAccessKey" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="FedExCode" Text='<%$ Resources:ZnodeAdminResource,  ColumnTitleFedExCode%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSecurityCode" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="FedExType" Text='<%$ Resources:ZnodeAdminResource,  ColumnTitleFedExType%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddldropOffTypes" runat="server">
                <asp:ListItem Value="BUSINESS_SERVICE_CENTER" Text='<%$ Resources:ZnodeAdminResource,  DropDownTextBusiness%>'></asp:ListItem>
                <asp:ListItem Value="DROP_BOX" Text='<%$ Resources:ZnodeAdminResource,  DropDownTextDropbox%>'></asp:ListItem>
                <asp:ListItem Selected="true" Text='<%$ Resources:ZnodeAdminResource,  DropDownTextPickUp%>'></asp:ListItem>
                <asp:ListItem Value="REQUEST_COURIER" Text='<%$ Resources:ZnodeAdminResource,  DropDownTextCourier%>'></asp:ListItem>
                <asp:ListItem Value="STATION" Text='<%$ Resources:ZnodeAdminResource,  DropDownTextStation%>'></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="FedexPackaging" Text='<%$ Resources:ZnodeAdminResource,  SubTitleFedexPackaging%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlPackageTypeCodes" runat="server">
                <asp:ListItem Selected="true" Value="YOUR_PACKAGING" Text='<%$ Resources:ZnodeAdminResource,  DropDownTextYourPackage%>'></asp:ListItem>
                <asp:ListItem Value="FEDEX_10KG_BOX" Enabled="false" Text='<%$ Resources:ZnodeAdminResource,  DropDownText10KgBox%>'></asp:ListItem>
                <asp:ListItem Value="FEDEX_25KG_BOX" Enabled="false" Text='<%$ Resources:ZnodeAdminResource,  DropDownText25KgBox%>'></asp:ListItem>
                <asp:ListItem Value="FEDEX_BOX" Text='<%$ Resources:ZnodeAdminResource,  DropDownTextBox%>'></asp:ListItem>
                <asp:ListItem Value="FEDEX_ENVELOPE" Text='<%$ Resources:ZnodeAdminResource,  DropDownTextEnvelope%>'></asp:ListItem>
                <asp:ListItem Value="FEDEX_TUBE" Text='<%$ Resources:ZnodeAdminResource,  DropDownTextTube%>'></asp:ListItem>
                <asp:ListItem Value="FEDEX_PAK" Text='<%$ Resources:ZnodeAdminResource,  DropDownTextPak%>'></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="FieldStyle"></div>
        <div class="ValueStyleText">
            <asp:CheckBox ID="chkFedExDiscountRate" runat="server" Text='<%$ Resources:ZnodeAdminResource,  CheckBoxFedex%>' /></div>
        <div class="FieldStyle"></div>
        <div class="ValueStyleText">
            <asp:CheckBox ID="chkAddInsurance" runat="server" Text='<%$ Resources:ZnodeAdminResource,  CheckBoxFedexAdd%>' /></div>
        <div class="ClearBoth">
            <br />
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="SubTitleUps" Text='<%$ Resources:ZnodeAdminResource,  SubTitleUps%>' runat="server"></asp:Localize></h4>
        <p>
            <asp:Localize ID="SubTextUps" Text='<%$ Resources:ZnodeAdminResource,  SubTextUps%>' runat="server"></asp:Localize>
            <br />
            <br />
        </p>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleUps" Text='<%$ Resources:ZnodeAdminResource,  ColumnTitleUps%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtUPSUserName" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="UpsPassword" Text='<%$ Resources:ZnodeAdminResource,  ColumnTitleUpsPassword%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtUPSPassword" TextMode="Password" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleUpsKey" Text='<%$ Resources:ZnodeAdminResource,  ColumnTitleUpsKey%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtUPSKey" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <zn:Button runat="server" ID="btnSubmitBottom" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" />
            <zn:Button runat="server" ID="btnCancelBottom" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
        </div>
    </div>
</asp:Content>
