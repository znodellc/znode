using System;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_settings_Stores_View class.
    /// </summary>
    public partial class StoresView : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private string associateName = string.Empty;
        private string upsPassword = string.Empty;
        private string mode = string.Empty;
        private string addDomainPaheLink = "~/FranchiseAdmin/Secure/Setup/Storefront/Stores/Domain.aspx?itemid=";
        private DataSet catalogDataSet = new DataSet();
        private ZNodeEncryption encryption = new ZNodeEncryption();
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get mode value from querystring        
            if (Request.Params["mode"] != null)
            {
                this.mode = HttpUtility.UrlDecode(this.encryption.DecryptData(Request.Params["mode"]));
            }

            this.itemId = UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0);

            UserStoreAccess.CheckStoreAccess(this.itemId, true);

            if (!Page.IsPostBack)
            {
                this.ResetTab();

                if (this.itemId > 0)
                {
                    this.BindStoreSettings();

                    // Domain Settings
                    pnlDomainSettings.Visible = true;
                    this.BindDomainData();
                   
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleManageStore").ToString() + lblStoreName.Text.Trim();
                }
            }
        }
        #endregion      

        #region Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/FranchiseAdmin/Secure/Setup/Storefront/Stores/Default.aspx");
        }

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/FranchiseAdmin/Secure/Setup/Storefront/Stores/Edit.aspx?ItemId={0}", HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString()))));
        }

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEditDisplay_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/FranchiseAdmin/Secure/Setup/Storefront/Stores/EditDisplay.aspx?ItemId={0}", HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString()))));
        }

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSMTPSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/FranchiseAdmin/Secure/Setup/Storefront/Stores/EditSmtp.aspx?ItemId={0}", HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString()))));
        }

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEditShipping_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/FranchiseAdmin/Secure/Setup/Storefront/Stores/EditShipping.aspx?ItemId={0}", HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString()))));
        }

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEditAnalytics_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("~/FranchiseAdmin/Secure/Setup/Storefront/Stores/EditAnalytics.aspx?ItemId={0}", HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString()))));
        }
                
        /// <summary>
        /// Redirecting to Domain Add page
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddNewDomain_Click(object sender, EventArgs e)
        {
            if (!UserStoreAccess.IsMultiStoreAdminEnabled() && uxDomainGrid.Rows.Count >= 1)
            {
                lblDomainError.Text = UserStoreAccess.UpgradeMessage;
            }
            else
            {
                Response.Redirect(this.addDomainPaheLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())));
            }
        }
        #endregion

        #region Related to Domain Views Grid Events

        /// <summary>
        /// Domain Items Page Index Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxDomainGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxDomainGrid.PageIndex = e.NewPageIndex;
            this.BindDomainData();
        }

        /// <summary>
        /// Domain Item delete event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxDomainGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindDomainData();
        }

        protected void UxDomainGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {            
        }

        /// <summary>
        /// Domain Grid row command event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxDomainGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.addDomainPaheLink + HttpUtility.UrlEncode(this.encryption.EncryptData(this.itemId.ToString())) + "&domainid=" + HttpUtility.UrlEncode(this.encryption.EncryptData(e.CommandArgument.ToString())));
                }

                if (e.CommandName == "RemoveItem")
                {
                    ZNode.Libraries.Admin.DomainAdmin domainAdmin = new DomainAdmin();
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                    HiddenField hdnDomain = (HiddenField)row.FindControl("hdnDomainName");
                    string domainName = hdnDomain.Value;
                    this.associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteDomain").ToString(), domainName, lblStoreName.Text);
                    bool isDeleted = domainAdmin.Delete(int.Parse(e.CommandArgument.ToString()));

                    if (isDeleted)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, lblStoreName.Text);

                        lblDomainError.Text = string.Empty;
                        this.BindDomainData();
                    }
                }
            }
        }
        #endregion

        #region Helper Method              
        /// <summary>
        /// Hide the domain delete button 
        /// </summary>
        /// <param name="domainName">Domain name to check</param>
        /// <returns>Returns true if domain name is not current else false.</returns>
        protected bool HideDeleteButton(string domainName)
        {
            return ZNodeConfigManager.DomainConfig.DomainName != domainName;
        }

        /// <summary>
        /// Hide the catalog delete button for the current domain
        /// </summary>
        /// <param name="domainName">Domain name to check</param>
        /// <returns>Returns true if domain name is not current else false. </returns>
        protected bool HideCatalogDeleteButton(string domainName)
        {
            if (this.catalogDataSet.Tables[0].Rows.Count == 1)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// This will automatically pre-select the tab according the query string value(mode)
        /// </summary>
        private void ResetTab()
        {
            if (this.mode.Equals("Domain"))
            {
                tabStoreSettings.ActiveTabIndex = 1;
            }
            else if (this.mode.Equals("display"))
            {
                tabStoreSettings.ActiveTabIndex = 2;
            }
            else if (this.mode.Equals("ship"))
            {
                tabStoreSettings.ActiveTabIndex = 3;
            }
            else if (this.mode.Equals("analytics"))
            {
                tabStoreSettings.ActiveTabIndex = 4;
            }
        }

        /// <summary>
        /// Reset the default profile.
        /// </summary>
        /// <param name="portalId">Profile Id to load the store profile.</param>
        /// <param name="profileId">Portal Id to load the store.</param>
        private void ResetDefaultProfile(int portalId, int profileId)
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            PortalService portalService = new PortalService();
            Portal portal = portalAdmin.GetByPortalId(portalId);
            if (portal.DefaultAnonymousProfileID == profileId)
            {
                portal.DefaultAnonymousProfileID = null;
            }

            if (portal.DefaultRegisteredProfileID == profileId)
            {
                portal.DefaultRegisteredProfileID = null;
            }

            portalService.Update(portal);
        }

        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind the store settings
        /// </summary>
        private void BindStoreSettings()
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = storeAdmin.GetByPortalId(this.itemId);
            CatalogAdmin catalogAdmin = new CatalogAdmin();

            if (portal != null)
            {
                lblCompanyName.Text = Server.HtmlEncode(portal.CompanyName);
                lblStoreName.Text = Server.HtmlEncode(portal.StoreName);

                // Get the catalogId based on PortalID
                int catalogIdValue = catalogAdmin.GetCatalogIDByPortalID(this.itemId);

                // Set the Preselected Value.
                if (catalogIdValue != 0)
                {
                    PortalCatalogService portalCatalogService = new PortalCatalogService();
                    TList<PortalCatalog> portalCatalogList = portalCatalogService.GetByPortalID(this.itemId);

                    if (portalCatalogList.Count > 0)
                    {
                        portalCatalogService.DeepLoad(portalCatalogList, true, ZNode.Libraries.DataAccess.Data.DeepLoadType.IncludeChildren, typeof(ZNode.Libraries.DataAccess.Entities.Catalog));
                    }
                }

                lblAdminEmail.Text = portal.AdminEmail;
                lblSalesEmail.Text = portal.SalesEmail;
                lblCustomerServiceEmail.Text = portal.CustomerServiceEmail;
                lblSalesPhoneNumber.Text = portal.SalesPhoneNumber;
                lblCustomerServicePhoneNumber.Text = portal.CustomerServicePhoneNumber;

                // Load the resized store logo from Content folder.
                ZNodeImage znodeImage = new ZNodeImage();
                imgLogo.ImageUrl = znodeImage.GetImageHttpPathSmall("Turnkey/" + portal.PortalID + "/" + Path.GetFileName(portal.LogoPath));

                chkEnableSSL.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.UseSSL);
                chkInclusiveTax.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.InclusiveTax);
				imgEnableCustomerBasedPricing.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.EnableCustomerPricing.GetValueOrDefault(false));

                if (portal.DefaultOrderStateID.HasValue)
                {
                    OrderStateService orderStateService = new OrderStateService();
                    OrderState orderState = orderStateService.GetByOrderStateID(portal.DefaultOrderStateID.Value);

                    lblDefaultOrderStatus.Text = orderState.OrderStateName;
                }

                if (!string.IsNullOrEmpty(portal.DefaultReviewStatus))
                {
                    if (portal.DefaultReviewStatus == "A")
                    {
                        lblDefaultReviewStatus.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextPublishImmediately").ToString();
                    }
                    else if (portal.DefaultReviewStatus == "N")
                    {
                        lblDefaultReviewStatus.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextDoNotPublish").ToString();
                    }
                }

                // Display Tab
                lblMaxCatalogDisplayColumns.Text = portal.MaxCatalogDisplayColumns.ToString();
                lblMaxSmallThumbnailsDisplay.Text = portal.MaxCatalogCategoryDisplayThumbnails.ToString();
                chkDynamicDisplayOrder.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.UseDynamicDisplayOrder.GetValueOrDefault(false));
                lblMaxCatalogItemLargeWidth.Text = portal.MaxCatalogItemLargeWidth.ToString();
                lblMaxCatalogItemMediumWidth.Text = portal.MaxCatalogItemMediumWidth.ToString();
                lblMaxCatalogItemSmallWidth.Text = portal.MaxCatalogItemSmallWidth.ToString();
                lblMaxCatalogItemCrossSellWidth.Text = portal.MaxCatalogItemCrossSellWidth.ToString();
                lblMaxCatalogItemThumbnailWidth.Text = portal.MaxCatalogItemThumbnailWidth.ToString();
                lblMaxCatalogItemSmallThumbnailWidth.Text = portal.MaxCatalogItemSmallThumbnailWidth.ToString();

                ItemImage.ImageUrl = znodeImage.GetImageHttpPathSmall("Turnkey/" + portal.PortalID + "/" + Path.GetFileName(portal.ImageNotAvailablePath));


                // Shipping Tab
                lblShippingAddress1.Text = portal.ShippingOriginAddress1;
                lblShippingAddress2.Text = portal.ShippingOriginAddress2;
                lblShippingCity.Text = portal.ShippingOriginCity;
                lblShippingPhone.Text = portal.ShippingOriginPhone;
                lblShippingZipCode.Text = portal.ShippingOriginZipCode;
                lblShippingStateCode.Text = portal.ShippingOriginStateCode;
                lblOriginCountryCode.Text = portal.ShippingOriginCountryCode;

                chkAddInsurance.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.FedExAddInsurance.GetValueOrDefault());
                chkFedExDiscountRate.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.FedExUseDiscountRate.GetValueOrDefault());

                try
                {
                    // Set UPS Account details
                    if (portal.UPSUserName != null)
                    {
                        lblUPSUserName.Text = Server.HtmlEncode(this.encryption.DecryptData(portal.UPSUserName));
                    }

                    if (portal.UPSPassword != null)
                    {
                        this.upsPassword = Server.HtmlEncode(this.encryption.DecryptData(portal.UPSPassword));
                        lblUPSPassword.Text = this.upsPassword.Trim().Length > 0 ? new string('*', this.upsPassword.Length) : string.Empty;
                    }

                    if (portal.UPSKey != null)
                    {
                        lblUPSKey.Text = Server.HtmlEncode(this.encryption.DecryptData(portal.UPSKey));
                    }

                    // Bind FedEx Account details
                    if (portal.FedExAccountNumber != null)
                    {
                        lblAccountNum.Text = Server.HtmlEncode(this.encryption.DecryptData(portal.FedExAccountNumber));
                    }

                    if (portal.FedExMeterNumber != null)
                    {
                        lblMeterNum.Text = Server.HtmlEncode(this.encryption.DecryptData(portal.FedExMeterNumber));
                    }

                    if (portal.FedExProductionKey != null)
                    {
                        lblProductionAccessKey.Text = Server.HtmlEncode(this.encryption.DecryptData(portal.FedExProductionKey));
                    }

                    if (portal.FedExSecurityCode != null)
                    {
                        lblSecurityCode.Text = Server.HtmlEncode(this.encryption.DecryptData(portal.FedExSecurityCode));
                    }

                    if (portal.FedExPackagingType != null)
                    {
                        lblPackageTypeCodes.Text = portal.FedExPackagingType;
                    }

                    if (portal.FedExDropoffType != null)
                    {
                        lbldropOffTypes.Text = portal.FedExDropoffType;
                    }
                }
                catch
                {
                }

                // Analytics
                if (portal.SiteWideBottomJavascript != null)
                {
                    lblSiteWideBottomJavaScript.Text = portal.SiteWideBottomJavascript.ToString();
                }

                if (portal.SiteWideTopJavascript != null)
                {
                    lblSiteWideTopJavaScript.Text = portal.SiteWideTopJavascript.ToString();
                }

                if (portal.SiteWideAnalyticsJavascript != null)
                {
                    lblSiteWideAnalyticsJavascript.Text = portal.SiteWideAnalyticsJavascript.ToString();
                }

                if (portal.OrderReceiptAffiliateJavascript != null)
                {
                    lblOrderReceiptJavaScript.Text = portal.OrderReceiptAffiliateJavascript.ToString();
                }
            }
            else
            {
                Response.Redirect("Default.aspx");
            }
        }

        /// <summary>
        /// Bind Domain Data 
        /// </summary>
        private void BindDomainData()
        {
            DomainAdmin domainAdmin = new DomainAdmin();
            uxDomainGrid.DataSource = domainAdmin.DeepLoadDomainsByPortalID(this.itemId);
            uxDomainGrid.DataBind();
        }
       
        #endregion
    }
}
