<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" 
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Stores.Domain" ValidateRequest="false" Title="Untitled Page"
    Codebehind="Domain.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    
    <div><asp:Label ID="lblMessage" runat="server" CssClass="Error"></asp:Label> </div>
    
    <div class="FormView" id="DomainContent" runat="server">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right; padding-right: 10px;"> 
             <zn:Button ID="btnSubmitTop" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
             <zn:Button ID="btnCancelTop" runat="server" ButtonType="CancelButton"  CausesValidation="false" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
      </div>
        <div align="left" class="ClearBoth">
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label></div>
        <div>
            <ZNode:Spacer ID="Sapcer1" SpacerHeight="10" SpacerWidth="3" runat="server" />
        </div>
        <div class="ClearBoth">
        </div>
        <div class="FieldStyle">
              <asp:Localize ID="ColumnTitleURLName" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleURLName %>'></asp:Localize><span class="Asterix">*</span><br />
            <small> <asp:Localize ID="TextURL" runat ="server" Text='<%$ Resources:ZnodeAdminResource, TextURL %>'></asp:Localize></small></div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtDomainName" runat="server" Width="300px"></asp:TextBox>&nbsp;
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDomainName"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDomainName %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RecularExpressionValidator" runat="server" ControlToValidate="txtDomainName"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, TextQualifiedURL %>' CssClass="Error" Display="dynamic" ValidationExpression="^(http\:\/\/[a-zA-Z0-9_\-/]+(?:\.[a-zA-Z0-9_\-/]+)*)$"></asp:RegularExpressionValidator>
        </div>
        <div class="FieldStyle">
            </div>
        <div class="ValueStyleText">
            <asp:CheckBox ID="CheckIsDomainInd" runat="server" Checked="true" Text='<%$ Resources:ZnodeAdminResource, CheckBoxEnablethisURL %>'>
            </asp:CheckBox></div>
        <div class="ClearBoth"></div>
        <div>  
             <zn:Button ID="btnSubmitBottom" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
             <zn:Button ID="btnCancelBottom" runat="server" ButtonType="CancelButton"  CausesValidation="false" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
       </div>
    </div>
</asp:Content>
