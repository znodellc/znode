<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false"
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Stores.EditDisplay" CodeBehind="EditDisplay.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/FranchiseAdmin/Controls/Default/ImageUploader.ascx" TagName="UploadImage" TagPrefix="ZNode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView Size350">
        <div>
            <div class="LeftFloat" style="width: 70%; text-align: left">
                <h1>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h1>
            </div>
            <div style="text-align: right; padding-right: 10px;">
                <zn:Button ID="btnSubmitTop" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
                <zn:Button ID="btnCancelTop" runat="server" ButtonType="CancelButton" CausesValidation="false" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
            </div>
            <div align="left">
                <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
            </div>
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="PRODUCTGRIDSETTINGS" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleProductGridSettings %>'></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleNumberofProductColumns %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxCatalogDisplayColumns" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtMaxCatalogDisplayColumns"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredMaxDisplayColumns %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtMaxCatalogDisplayColumns"
                CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnternumberbetweenTen %>'
                MaximumValue="10" MinimumValue="1" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="NumberofThumbnailColumns" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleNumberofThumbnailColumns %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxSmallThumbnailsDisplay" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator17" runat="server" ControlToValidate="txtMaxSmallThumbnailsDisplay"
                CssClass="Error" Display="dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredMaxSmallThumbNailImage %>'></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="txtMaxSmallThumbnailsDisplay"
                CssClass="Error" Display="dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>' MaximumValue="1000"
                MinimumValue="1" SetFocusOnError="true" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ProductDisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductDisplayOrder %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkDynamicDisplayOrder" runat="server" Checked="false" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayPopularProducts %>'></asp:CheckBox>
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="AutoImageResizeSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleAutoImageResizeSettings %>'></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize ID="LargeImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleLargeImage %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxCatalogItemLargeWidth" runat="server"></asp:TextBox>&nbsp;pixels&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtMaxCatalogItemLargeWidth"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredMaxLargeImageWidth %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="txtMaxCatalogItemLargeWidth"
                CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeNumberMustonetoThousand %>'
                MaximumValue="1000" MinimumValue="1" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="MediumImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleMediumImage %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxCatalogItemMediumWidth" runat="server"></asp:TextBox>&nbsp;pixels&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtMaxCatalogItemMediumWidth"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredMediumImageWidth %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtMaxCatalogItemMediumWidth"
                CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeNumberMustonetoThousand %>'
                MaximumValue="1000" MinimumValue="1" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="SmallImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSmallImage %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxCatalogItemSmallWidth" runat="server"></asp:TextBox>&nbsp;pixels&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtMaxCatalogItemSmallWidth"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSmallImageWidth %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="txtMaxCatalogItemSmallWidth"
                CssClass="Error" Display="dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>' MaximumValue="1000"
                MinimumValue="1" SetFocusOnError="true" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="CrossSellImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCrossSellImage %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxCatalogItemCrossSellWidth" runat="server"></asp:TextBox>&nbsp;pixels&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtMaxCatalogItemSmallWidth"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSmallImageWidth %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator12" runat="server" ControlToValidate="txtMaxCatalogItemCrossSellWidth"
                CssClass="Error" Display="dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>' MaximumValue="1000"
                MinimumValue="1" SetFocusOnError="true" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ThumbnailImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleThumbnailImage %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxCatalogItemThumbnailWidth" runat="server"></asp:TextBox>&nbsp;pixels&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator15" runat="server" ControlToValidate="txtMaxCatalogItemThumbnailWidth"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredThumbnailImageWidth %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="txtMaxCatalogItemThumbnailWidth"
                CssClass="Error" Display="dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>' MaximumValue="1000"
                MinimumValue="1" SetFocusOnError="true" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="smallThumbnailImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSmallThumbnailImage %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtMaxCatalogItemSmallThumbnailWidth" runat="server"></asp:TextBox>&nbsp;pixels&nbsp;<asp:RequiredFieldValidator
                ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMaxCatalogItemSmallWidth"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSmallImageWidth %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtMaxCatalogItemSmallThumbnailWidth"
                CssClass="Error" Display="dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>' MaximumValue="1000"
                MinimumValue="1" SetFocusOnError="true" Type="Integer"></asp:RangeValidator>
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="DefaultProductImagelocal" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitmeDefaultProductImage %>'></asp:Localize></h4>
        <small>
            <asp:Localize ID="DefaultproductImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextDefaultproductImage %>'></asp:Localize></small>
        <div id="tblShowImage" runat="server" visible="true" style="margin: 10px 0px 10px 0px;">
            <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;" runat="server" id="pnlImage">
                <asp:Image ID="Image1" runat="server" />
            </div>
            <div class="StoreLeftFloat" style="width: 400px; margin: 10px;" runat="server" id="pnlUploadSection">
                <asp:Panel runat="server" ID="pnlShowImage">
                    <div class="FieldStyle" style="margin-bottom: 4px;">
                        <asp:Localize ID="SelectanOption" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectanOption %>'></asp:Localize>
                    </div>
                    <div class="ClearBoth"></div>
                    <div>
                        <asp:RadioButton ID="RadioProductCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonCurrentImage %>' runat="server"
                            GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductCurrentImage_CheckedChanged"
                            Checked="True" /><br />
                        <asp:RadioButton ID="RadioProductNewImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNewImage %>' runat="server"
                            GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductNewImage_CheckedChanged" />
                    </div>
                </asp:Panel>
                <br />
                <div id="tblProductDescription" runat="server" visible="false">
                    <div class="FieldStyle">
                        <asp:Localize ID="Localize15" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectImageColon %>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <ZNode:UploadImage ID="uxProductImage" runat="server"></ZNode:UploadImage>
                    </div>
                </div>
                <asp:Panel ID="pnlImageName" runat="server" Visible="false">
                    <div class="FieldStyle">
                        <asp:Localize ID="ColumnImageFileName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnImageFileName %>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtimagename" runat="server"></asp:TextBox>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <zn:Button ID="btnSubmitBottom" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
            <zn:Button ID="btnCancelBottom" runat="server" ButtonType="CancelButton" CausesValidation="false" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel %>" />
        </div>
    </div>
</asp:Content>
