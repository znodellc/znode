using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_settings_Stores_EditAnalytics class.
    /// </summary>
    public partial class EditAnalytics : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private string viewPageLink;
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ZNodeEncryption encryption = new ZNodeEncryption();
            this.viewPageLink = "view.aspx?ItemId={0}&mode=" + HttpUtility.UrlEncode(encryption.EncryptData("analytics"));

            // Get portalId value from querystring
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(encryption.DecryptData(Request.Params["itemid"].ToString()));
            }

            UserStoreAccess.CheckStoreAccess(this.itemId, true);

            if (!Page.IsPostBack)
            {
                this.BindAnalytics();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = null;
            bool isSuccess = false;

            if (this.itemId > 0)
            {
                portal = storeAdmin.GetByPortalId(this.itemId);
            }

            if (portal != null)
            {
                portal.SiteWideBottomJavascript = Server.HtmlEncode(txtSiteWideBottomJavaScript.Text);
                portal.SiteWideTopJavascript = Server.HtmlEncode(txtSiteWideTopJavaScript.Text);
                portal.SiteWideAnalyticsJavascript = Server.HtmlEncode(txtSiteWideAnalyticsJavascript.Text);
                portal.OrderReceiptAffiliateJavascript = Server.HtmlEncode(txtOrderReceiptJavaScript.Text);

                isSuccess = storeAdmin.UpdateStore(portal);
            }

            if (isSuccess)
            {
                // Log Activity
                string associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditofJavascriptcode").ToString() + portal.StoreName;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, portal.StoreName);
                Response.Redirect(string.Format(this.viewPageLink, HttpUtility.UrlEncode(this.itemId.ToString())));
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAnalyticsCode").ToString();

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeFailed, HttpContext.Current.User.Identity.Name);
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format(this.viewPageLink, HttpUtility.UrlEncode(this.itemId.ToString())));
        }
        #endregion
        #region Bind Methods
        /// <summary>
        /// Bind the google analytics code.
        /// </summary>
        private void BindAnalytics()
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();

            // Set default Value from Store1 For Add page
            Portal portal = storeAdmin.GetByPortalId(this.itemId);

            if (portal != null)
            {
                lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleJavaScript").ToString() + "\"" + Server.HtmlEncode(portal.StoreName) + "\"";

                if (portal.SiteWideBottomJavascript != null)
                {
                    txtSiteWideBottomJavaScript.Text = Server.HtmlDecode(portal.SiteWideBottomJavascript.ToString());
                }

                if (portal.SiteWideTopJavascript != null)
                {
                    txtSiteWideTopJavaScript.Text = Server.HtmlDecode(portal.SiteWideTopJavascript.ToString());
                }

                if (portal.SiteWideAnalyticsJavascript != null)
                {
                    txtSiteWideAnalyticsJavascript.Text = Server.HtmlDecode(portal.SiteWideAnalyticsJavascript.ToString());
                }

                if (portal.OrderReceiptAffiliateJavascript != null)
                {
                    txtOrderReceiptJavaScript.Text = Server.HtmlDecode(portal.OrderReceiptAffiliateJavascript.ToString());
                }
            }
        }
        #endregion
    }
}
