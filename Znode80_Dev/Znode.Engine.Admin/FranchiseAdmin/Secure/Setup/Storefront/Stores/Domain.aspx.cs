using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_settings_Stores_Domain class.
    /// </summary>
    public partial class Domain : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId;
        private int domainId;
        private string associateName = string.Empty;
        private string viewPageLink;
        #endregion

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ZNodeEncryption encryption = new ZNodeEncryption();
            this.viewPageLink = "~/FranchiseAdmin/Secure/Setup/Storefront/Stores/View.aspx?mode=" + HttpUtility.UrlEncode(encryption.EncryptData("Domain")) + "&itemid=";

            // Display upgrade warning message if singlestore edition.
            if (!UserStoreAccess.IsMultiStoreAdminEnabled())
            {
                lblMessage.Text = UserStoreAccess.UpgradeMessage;
                DomainContent.Visible = false;
                return;
            }

            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(encryption.DecryptData(Request.Params["itemid"]));
            }
            else
            {
                this.itemId = 0;
            }

            UserStoreAccess.CheckStoreAccess(this.itemId, true);

            // Get ItemId from querystring        
            if (Request.Params["domainid"] != null)
            {
                this.domainId = int.Parse(encryption.DecryptData(Request.Params["domainid"]));
            }
            else
            {
                this.domainId = 0;
            }

            if (!Page.IsPostBack)
            {
                // If edit func then bind the data fields
                if (this.domainId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditUrl").ToString();
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddNewURL").ToString();
                }
            }
        }

        #region Events

        /// <summary>
        /// Bind the domain data.
        /// </summary>
        protected void BindEditData()
        {
            DomainAdmin domainAdmin = new DomainAdmin();
            ZNode.Libraries.DataAccess.Entities.Domain domain = new ZNode.Libraries.DataAccess.Entities.Domain();

            if (this.domainId > 0)
            {
                domain = domainAdmin.GetDomainByDomainId(this.domainId);

                lblTitle.Text += domain.DomainName;
                txtDomainName.Text = "http://" + domain.DomainName;
                CheckIsDomainInd.Checked = domain.IsActive;
            }
        }

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            DomainAdmin domainAdmin = new DomainAdmin();
            ZNode.Libraries.DataAccess.Entities.Domain domain = new ZNode.Libraries.DataAccess.Entities.Domain();
            bool isSuccess = false;
            string domainName = string.Empty;

            // Display the upgrade message if store is singlefront and already one domain created.
            if (!UserStoreAccess.IsMultiStoreAdminEnabled() && domainAdmin.GetDomainByPortalID(this.itemId).Count >= 1)
            {
                lblMsg.Text = UserStoreAccess.UpgradeMessage;
                return;
            }

            // If edit mode then get all the values first
            if (this.domainId > 0)
            {
                domain = domainAdmin.GetDomainByDomainId(this.domainId);
            }

            // Set the domainName
            domainName = txtDomainName.Text.Trim().ToLower();

            if (domainName.Contains("http://") || domainName.Contains("https://"))
            {
                domainName = domainName.Replace("http://", string.Empty).Replace("https://", string.Empty);
            }

            if (domainName.Contains(":"))
            {
                // Strip off the port number so we won't conflict with debuggers and other services that may specify a different port.
                domainName = domainName.Substring(0, domainName.IndexOf(":"));
            }

            if (domainName.StartsWith("www."))
            {
                domainName = domainName.Remove(0, 4);
            }

            // Remove any trailing "/"
            if (domainName.EndsWith("/"))
            {
                domainName = domainName.Remove(domainName.Length - 1);
            }

            domain.PortalID = this.itemId;
            domain.DomainName = domainName;
            domain.IsActive = CheckIsDomainInd.Checked;
            try
            {
                StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
                Portal portal = new Portal();
                portal = storeAdmin.GetByPortalId(this.itemId);

                if (this.domainId > 0)
                {
                    isSuccess = domainAdmin.Update(domain);

                    // Log Activity
                    this.associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditDomain").ToString() + domainName + " - " + portal.StoreName;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, portal.StoreName);
                }
                else
                {
                    isSuccess = domainAdmin.Insert(domain);

                    // Log Activity
                    this.associateName =this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddDomain").ToString()  + domainName + " - " + portal.StoreName;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.associateName, portal.StoreName);
                }
            }
            catch
            {
            }

            if (!isSuccess)
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDomainNameShouldbeUnique").ToString();
            }
            else
            {
                Response.Redirect(this.viewPageLink + HttpUtility.UrlEncode(this.itemId.ToString()));
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.viewPageLink + HttpUtility.UrlEncode(this.itemId.ToString()));
        }
        #endregion
    }
}