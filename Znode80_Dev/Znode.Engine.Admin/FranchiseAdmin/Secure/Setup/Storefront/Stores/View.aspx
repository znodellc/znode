<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/edit.master" AutoEventWireup="True" 
    Inherits="Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Stores.StoresView" ValidateRequest="false" Codebehind="View.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
         <div class="ClearBoth"> 
                <p><asp:Localize ID="TextStoreName" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextStores%>'></asp:Localize>
                  </p>
            </div>
        <div style="text-align: right; display:none">
            <zn:Button runat="server" Width="100px" ButtonType="EditButton" OnClick="BtnBack_Click" CausesValidation="True" Text='<%$ Resources:ZnodeAdminResource, ButtonBack%>' ID="Button2" />
        </div>
        <div>
            <ZNode:Spacer ID="Spacer10" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div style="clear: both;">
            <ajaxToolKit:TabContainer ID="tabStoreSettings" runat="server">
                <ajaxToolKit:TabPanel ID="pnlGeneral" runat="server">
                    <HeaderTemplate>
                         <asp:Localize ID="TabTitleGeneral" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleGeneral%>'></asp:Localize>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="ViewForm200">
                            <div>
                                <ZNode:Spacer ID="Spacer11" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div>
                                <zn:Button runat="server" ButtonType="EditButton" Width="150px" OnClick="BtnEdit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonEditStore%>' ID="btnSubmitTop"   CausesValidation="False" />
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer18" runat="server" SpacerHeight="15" SpacerWidth="3"></ZNode:Spacer>
                            </div>
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleStoreIdentity" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleStoreIdentity%>'></asp:Localize></h4>
                            <div class="FieldStyleA">
                               <asp:Localize ID="ColumnStoreName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStoreName%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblStoreName" runat="server"></asp:Label>
                                &#160;
                            </div>    
                            <div class="FieldStyle">
                                 <asp:Localize ID="ColumnBrandName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBrandName%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblCompanyName" runat="server"></asp:Label>
                            </div>
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleStoreLogoImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleStoreLogoImage%>'></asp:Localize></h4>
                            <div class="Image">
                                <asp:Image ID="imgLogo" runat="server" />
                            </div>
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleSecurity" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSecurity%>'></asp:Localize></h4>
                            <div class="FieldStyleA">
                                <asp:Localize ID="ColumnEnableSSL" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableSSL%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <img id="chkEnableSSL" runat="server" />
                            &nbsp;</div>
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleStoreContact" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleStoreContact%>'></asp:Localize></h4>
                            <div class="FieldStyle">
                                 <asp:Localize ID="ColumnAdministratorEmail" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAdministratorEmail%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblAdminEmail" runat="server"></asp:Label>
                            </div>
                            <div class="FieldStyleA">
                               <asp:Localize ID="ColumnSalesDepartmentEmail" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSalesDepartmentEmail%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblSalesEmail" runat="server"></asp:Label>
                                &#160;
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnTitleCustomerServiceEmail" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCustomerServiceEmail%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblCustomerServiceEmail" runat="server"></asp:Label>
                            </div>
                            <div class="FieldStyleA">
                                <asp:Localize ID="ColumnTitleSalesDeptPhone" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSalesDeptPhone%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblSalesPhoneNumber" runat="server"></asp:Label>
                                &#160;
                            </div>
                            <div class="FieldStyle">
                               <asp:Localize ID="ColumnCustomerServicePhoneNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCustomerServicePhoneNumber%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblCustomerServicePhoneNumber" runat="server"></asp:Label>
                            </div>
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleDefaultSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDefaultSettings%>'></asp:Localize></h4>
                            <div class="FieldStyle">
                                <asp:Localize ID="SubTextCustomerReviewStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextCustomerReviewStatus%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblDefaultReviewStatus" runat="server"></asp:Label>
                            </div>
                            <div class="FieldStyleA">
                               <asp:Localize ID="ColumnDefaultOrderStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDefaultOrderStatus%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblDefaultOrderStatus" runat="server"></asp:Label>
                                &#160;
                            </div>
							 <div>
                                <div class="FieldStyle">
                                    <asp:Localize ID="ColumnIncludeTaxProductPrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnIncludeTaxProductPrice%>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <img id="chkInclusiveTax" runat="server" />
                                &nbsp;</div>
                            </div>
							<div>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="ColumnEnableCustomerBasedPricing" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableCustomerBasedPricing%>'></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <img id="imgEnableCustomerBasedPricing" runat="server" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>
                            </div>
                        </div>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlDomainSettings" runat="server">
                    <HeaderTemplate>
                        <asp:Localize ID="ColumnTitleURL" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleURL%>'></asp:Localize>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="ViewForm200">
                            <div>
                                <ZNode:Spacer ID="Spacer2" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <p class="ClearBoth">
                                   <asp:Localize ID="SubTextURL" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextURL%>'></asp:Localize>
                            </p>
                            <div class="TextLine">
                                <div class="ButtonStyle">
                                    <zn:LinkButton ID="Button1" runat="server"
                                        ButtonType="Button" OnClick="AddNewDomain_Click" ValidationGroup="grpUrl" Text='<%$ Resources:ZnodeAdminResource, ButtonAddURL%>'
                                        ButtonPriority="Primary" />
                                </div>
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div>
                                <div class="Error">
                                    <asp:Label ID="lblDomainError" runat="server" />
                                </div>
                            </div>
                            <div>
                                <div colspan="2" valign="middle">
                                    <asp:UpdatePanel ID="updPnlGrid" runat="server" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:GridView ID="uxDomainGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                                                runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                                                CssClass="Grid" Width="100%" GridLines="None" OnRowCommand="UxDomainGrid_RowCommand"
                                                AllowPaging="True"   OnPageIndexChanging="UxDomainGrid_PageIndexChanging" OnRowDeleting="UxDomainGrid_RowDeleting"
                                                PageSize="5" onrowdatabound="UxDomainGrid_RowDataBound">
                                                <Columns>
                                                    <asp:BoundField DataField="DomainId"  HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID%>' HeaderStyle-HorizontalAlign="Left" />
                                                    <asp:TemplateField  HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleURLName%>' HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <%# "http://" + Eval("DomainName")%>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleActive%>' HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <img id="Img1" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsActive").ToString()))%>'
                                                                runat="server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField  HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleAction%>' HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ValidationGroup="grpUrl" ID="EditProductView" Text='<%$ Resources:ZnodeAdminResource, LinkEdit%>'
                                                                CommandArgument='<%# Eval("DomainID") %>' Visible='<%# HideDeleteButton(Eval("DomainName").ToString()) %>'
                                                                CommandName="Edit" runat="Server" />&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <asp:LinkButton ID="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete%>' CommandArgument='<%# Eval("DomainID") %>'
                                                                CommandName="RemoveItem" Visible='<%# HideDeleteButton(Eval("DomainName").ToString()) %>'
                                                                runat="server" OnClientClick="return DeleteURLConfirmation();" />
                                                             <asp:HiddenField ID="hdnDomainName" runat="server" Value=<%# Eval("DomainName") %> />   
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <RowStyle CssClass="RowStyle" />
                                                <HeaderStyle CssClass="HeaderStyle" />
                                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                                <FooterStyle CssClass="FooterStyle" />
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                            </div>
                            </table>
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>                
                <ajaxToolKit:TabPanel ID="pnlDisplay" runat="server">
                    <HeaderTemplate>
                        <asp:Localize ID="TabTitleDisplay" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleDisplay%>'></asp:Localize>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div class="ViewForm300">
                            <div>
                                <ZNode:Spacer ID="Spacer13" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div>
                                 <zn:Button runat="server" Width="150px" ButtonType="EditButton" OnClick="BtnEditDisplay_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonEditDisplaySettings%>' ID="btnEditDisplay"  />
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer22" runat="server" SpacerHeight="15" SpacerWidth="3" />
                            </div>
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleProductGridSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleProductGridSettings%>'></asp:Localize></h4>
                            <div class="FieldStyle">
                               <asp:Localize ID="ColumnNumberOfProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnNumberOfProduct%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblMaxCatalogDisplayColumns" runat="server"></asp:Label>
                            </div>
                            <div class="FieldStyleA">
                                 <asp:Localize ID="ColumnNumberOfThumbnail" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnNumberOfThumbnail%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblMaxSmallThumbnailsDisplay" runat="server"></asp:Label>
                            </div>
                             <div class="FieldStyle">
                                 <asp:Localize ID="ColumnDisplayPopularProducts" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayPopularProducts%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                 <img id="chkDynamicDisplayOrder" runat="server" />
                            </div>
                            <h4 class="SubTitle">
                                <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleAutoImageResizeSettings%>'></asp:Localize></h4>
                            <div class="FieldStyle">
                              <asp:Localize ID="ColumnLargeImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLargeImage%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblMaxCatalogItemLargeWidth" runat="server"></asp:Label>&nbsp; <asp:Localize ID="ColumnTextpixels" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextpixels%>'></asp:Localize>
                            </div>
                            <div class="FieldStyleA">
                                 <asp:Localize ID="ColumnMediumImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnMediumImage%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblMaxCatalogItemMediumWidth" runat="server"></asp:Label>&nbsp;<asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextpixels%>'></asp:Localize>
                            </div>
                            <div class="FieldStyle">
                                 <asp:Localize ID="ColumnSmallImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSmallImage%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblMaxCatalogItemSmallWidth" runat="server"></asp:Label>&nbsp;<asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextpixels%>'></asp:Localize>
                            </div>
                            <div class="FieldStyleA">
                                 <asp:Localize ID="ColumnCrossSellImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCrossSellImage%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblMaxCatalogItemCrossSellWidth" runat="server"></asp:Label>&nbsp;<asp:Localize ID="Localize4" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextpixels%>'></asp:Localize>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnThumbnailImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnThumbnailImage%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblMaxCatalogItemThumbnailWidth" runat="server"></asp:Label>&nbsp;<asp:Localize ID="Localize5" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextpixels%>'></asp:Localize>
                            </div>
                            <div class="FieldStyleA">
                               <asp:Localize ID="ColumnSmallThumbnailImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSmallThumbnailImage%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblMaxCatalogItemSmallThumbnailWidth" runat="server"></asp:Label>&nbsp;<asp:Localize ID="Localize6" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextpixels%>'></asp:Localize>
                            </div>
                            <%-- <div class="FieldStyleA">
                                Image Not Available Path
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblImageNotAvailablePath" runat="server"></asp:Label>
                            </div>--%>
                             <h4 class="SubTitle">
                            <asp:Localize ID="SubTitleDefaultProductImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDefaultProductImage%>'></asp:Localize></h4>
                            <div class="Image">
                            <asp:Image ID="ItemImage" runat="server" /></div>   
                        </div>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="TabPanel1" HeaderText='<%$ Resources:ZnodeAdminResource, TabTitleShipping%>' runat="server">
                    <ContentTemplate>
                        <div class="ViewForm200">
                            <div>
                                <ZNode:Spacer ID="Spacer16" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <div>
                                 <zn:Button runat="server" ButtonType="EditButton" Width="150px" OnClick="BtnEditShipping_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonEditShippingSettings%>' ID="btnEditShipping" CausesValidation="false"/>
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer27" runat="server" SpacerHeight="15" SpacerWidth="3" />
                            </div>
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleStoreShipping" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleStoreShipping%>'></asp:Localize></h4>
                            <div class="FieldStyle">
                                <asp:Localize ID="Localize8" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingOrigingAddress1%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblShippingAddress1" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                <asp:Localize ID="Localize9" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingOrigingAddress2%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblShippingAddress2" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnShippingOriginCity" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingOriginCity%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblShippingCity" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                <asp:Localize ID="Localize11" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingOriginStateCode%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblShippingStateCode" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                    <asp:Localize ID="Localize12" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingOriginZipCode%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblShippingZipCode" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                <asp:Localize ID="Localize13" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingOriginCountryCode%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblOriginCountryCode" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="Localize14" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingOriginPhone%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblShippingPhone" runat="server"></asp:Label>&nbsp;
                            </div>
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleFedEx" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleFedEx%>'></asp:Localize></h4>
                            <div class="FieldStyle">
                               <asp:Localize ID="Localize15" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFedExAccountNumber%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblAccountNum" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                 <asp:Localize ID="ColumnFedExMeterNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFedExMeterNumber%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblMeterNum" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                 <asp:Localize ID="ColumnFedExProductionkey" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFedExProductionkey%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblProductionAccessKey" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                <asp:Localize ID="Localize16" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFedExSecurityCode%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblSecurityCode" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnFedExDropOffType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFedExDropOffType%>'></asp:Localize> 
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lbldropOffTypes" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                              <asp:Localize ID="Localize17" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFedExPackagingType%>'></asp:Localize> 
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblPackageTypeCodes" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleImg">
                               <asp:Localize ID="ColumnFedExDiscountRate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFedExDiscountRate%>'></asp:Localize> 
                            </div>
                            <div class="ValueStyleImg">
                                <img id="chkFedExDiscountRate" runat="server" alt="" src="" />
                            </div>
                            <div class="FieldStyleImgA">
                                <asp:Localize ID="Localize18" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnInsuranceEnabled%>'></asp:Localize> 
                            </div>
                            <div class="ValueStyleImgA">
                                <img id="chkAddInsurance" runat="server" alt="" src="" />&nbsp;
                            </div>
                            <h4 class="SubTitle">
                                 <asp:Localize ID="Localize20" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleUps%>'></asp:Localize> </h4>
                            <div class="FieldStyle">
                                 <asp:Localize ID="Localize19" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnUPSUserName%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblUPSUserName" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA">
                                 <asp:Localize ID="Localize21" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnUPSPassword%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblUPSPassword" runat="server"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                 <asp:Localize ID="Localize22" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnUPSAccessKey%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblUPSKey" runat="server"></asp:Label>&nbsp;
                            </div>
                        </div>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlGoogleAnalystics" HeaderText='<%$ Resources:ZnodeAdminResource, TabTitleJavaScript%>' runat="server">
                    <ContentTemplate>
                        <div class="ViewForm300">
                            <div class="ClearBoth">
                                <p><asp:Localize ID="SubTextStoreJavascript" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextStoreJavascript%>'></asp:Localize></p>
                            </div>
                            <div>
                                <zn:Button runat="server" Width="175px" ButtonType="EditButton" OnClick="BtnEditAnalytics_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonEditJavaScript%>' ID="btnEditAnalytics" CausesValidation="false" />
                            </div>
                            <h4 class="SubTitle">
                                <asp:Localize ID="Localize23" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleJavascript%>'></asp:Localize></h4>
                            <div class="FieldStyle">
                                <asp:Localize ID="Localize24" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSiteWideJavascriptTop%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblSiteWideTopJavaScript" runat="server"></asp:Label>
                            </div>
                            <div class="MainStyleA">
                            <div class="FieldStyleA">
                                <asp:Localize ID="Localize25" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSiteWideJavascriptBottom%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblSiteWideBottomJavaScript" runat="server"></asp:Label>&nbsp;
                            </div></div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnSiteWideJavascriptOrderReceipt" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSiteWideJavascriptOrderReceipt%>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblSiteWideAnalyticsJavascript" runat="server"></asp:Label>
                            </div>
                            <div class="MainStyleA">
                            <div class="FieldStyleA">
                                <asp:Localize ID="Localize26" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOrderReceiptJavaScript%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblOrderReceiptJavaScript" runat="server"></asp:Label>&nbsp;
                            </div></div>
                        </div>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
            </ajaxToolKit:TabContainer>
        </div>
    </div>
          <script language="javascript" type="text/javascript">
              function DeleteURLConfirmation() {
                  return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ConfirmDeleteURL")) %>');
        }
</script>
</asp:Content>
