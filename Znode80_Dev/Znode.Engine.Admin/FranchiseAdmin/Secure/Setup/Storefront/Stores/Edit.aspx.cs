using System;
using System.IO;
using System.Web;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;
using ZNode.Libraries.DataAccess.Data;

namespace  Znode.Engine.FranchiseAdmin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the Franchise Admin Admin_Secure_settings_Stores_Edit class.
    /// </summary>
    public partial class Edit : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private string viewPageLink = "~/FranchiseAdmin/Secure/Setup/Storefront/Stores/View.aspx?itemid=";
        private CatalogAdmin catalogAdmin = new CatalogAdmin();
        private string selectedTheme = string.Empty;
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                ZNodeEncryption encryption = new ZNodeEncryption();
                this.itemId = int.Parse(encryption.DecryptData(Request.Params["itemid"].ToString()));
            }

            UserStoreAccess.CheckStoreAccess(this.itemId, true);

            if (!Page.IsPostBack)
            {
                this.BindOrderStatus(true);

                // Catalog List
                this.BindCatalog();
                this.BindThemeList();
                this.BindCssList();

                if (this.itemId > 0)
                {
                    this.BindEditData();

                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleManageStore").ToString() + Server.HtmlEncode(txtStoreName.Text.Trim());
                }
            }
        }
        #endregion      

        #region Events

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = new Portal();
            string newFileName = string.Empty;

            if (this.itemId > 0)
            {
                portal = storeAdmin.GetByPortalId(this.itemId);
            }

            portal.CompanyName = txtCompanyName.Text;
            portal.ActiveInd = true;
            portal.StoreName = txtStoreName.Text;
            portal.UseSSL = chkEnableSSL.Checked;

            portal.AdminEmail = txtAdminEmail.Text;
            portal.CustomerServiceEmail = txtCustomerServiceEmail.Text;
            portal.CustomerServicePhoneNumber = txtCustomerServicePhoneNumber.Text;
            portal.SalesEmail = txtSalesEmail.Text;
            portal.SalesPhoneNumber = txtSalesPhoneNumber.Text;

            portal.InclusiveTax = chkInclusiveTax.Checked;
	        portal.EnableCustomerPricing = CheckEnableCustomerBasedPricing.Checked;
            portal.DefaultReviewStatus = ListReviewStatus.SelectedValue;

            if (chkPendingApproval.Checked)
            {
                portal.DefaultOrderStateID = (int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.PENDING_APPROVAL;
            }
            else
            {
                portal.DefaultOrderStateID = int.Parse(ddlOrderStateList.SelectedValue);
            }

            // Set logo path
            string fileName = string.Empty;
            bool isSuccess = false;

            if (radNewImage.Checked == true)
            {
                if (UploadImage.PostedFile.FileName != string.Empty)
                {
                    // Check for Product Image
                    fileName = System.IO.Path.GetFileNameWithoutExtension(UploadImage.PostedFile.FileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(UploadImage.PostedFile.FileName);

                    if (fileName != string.Empty)
                    {
                        byte[] imageData = new byte[UploadImage.PostedFile.InputStream.Length];
                        UploadImage.PostedFile.InputStream.Read(imageData, 0, (int)UploadImage.PostedFile.InputStream.Length);                        
                        ZNodeStorageManager.WriteBinaryStorage(imageData, ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + "Turnkey/" + portal.PortalID + "/" + fileName);
                        portal.LogoPath = ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + fileName;
                    }
                }
            }

            if (this.itemId > 0)
            {
                isSuccess = storeAdmin.UpdateStore(portal);
            }

            if (isSuccess)
            {
                PortalCatalog portalCatalog = new PortalCatalog();

                // Add the new Selection
                portalCatalog.PortalID = portal.PortalID;
                portalCatalog.CatalogID = Convert.ToInt32(ddlCatalog.SelectedValue);

                PortalCatalogService portalCatalogService = new PortalCatalogService();
                TList<PortalCatalog> portalCatalogList = portalCatalogService.GetByPortalID(this.itemId);

                portalCatalog.PortalCatalogID = portalCatalogList[0].PortalCatalogID;
                portalCatalog.LocaleID = portalCatalogList[0].LocaleID;

                if (ddlThemeslist.Items.Count > 0)
                {
                    portalCatalog.ThemeID = int.Parse(ddlThemeslist.SelectedValue);
                }
                else
                {
					portalCatalog.ThemeID = 1; // "Default";
                }

                if (ddlCSSList.Items.Count > 0)
                {
					portalCatalog.CSSID = int.Parse(ddlCSSList.SelectedValue);
                }

                this.catalogAdmin.UpdatePortalCatalog(portalCatalog);

                // Refresh of all the configuration information from the database & other config files
                ZNodeConfigManager.RefreshConfiguration();

                Response.Redirect(this.viewPageLink + HttpUtility.UrlEncode(Request.Params["itemid"]));

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeSuccess, HttpContext.Current.User.Identity.Name);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit of Store - " + txtStoreName.Text, txtStoreName.Text);
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorStoreSettings").ToString();

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeFailed, HttpContext.Current.User.Identity.Name);
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.viewPageLink + HttpUtility.UrlEncode(this.itemId.ToString()));
        }

        /// <summary>
        /// Keep Current Image radio button selected.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblLogoUpload.Visible = false;
        }

        /// <summary>
        /// Upload new Image radio button selected.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblLogoUpload.Visible = true;
        }
        
        /// <summary>
        /// Country option changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlThemeslist_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlCssList.Visible = true;
            ddlCSSList.Items.Clear();
            this.selectedTheme = ddlThemeslist.SelectedItem.Text;

            this.BindCssList();
        }

        /// <summary>
        /// If required PENDING APPROVAL option, then include in the dropdown and select that option.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChkPendingApproval_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPendingApproval.Checked)
            {
                this.BindOrderStatus(false);

                ddlOrderStateList.SelectedValue = ((int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.PENDING_APPROVAL).ToString();
                ddlOrderStateList.Enabled = false;
            }
            else
            {
                if (this.itemId > 0)
                {
                    this.BindOrderStatus(true);
                    ddlOrderStateList.Enabled = true;

                    // Select last selected items from Database or select default to SUBMITTED.
                    StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
                    Portal portal;
                    portal = storeAdmin.GetByPortalId(this.itemId);

                    ListItem listItem = ddlOrderStateList.Items.FindByValue(portal.DefaultOrderStateID.GetValueOrDefault(10).ToString());
                    if (listItem != null)
                    {
                        listItem.Selected = true;
                    }
                    else
                    {
                        ddlOrderStateList.SelectedValue = ((int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.SUBMITTED).ToString();
                    }
                }
            }
        }

        #endregion

        #region Helper Method

        /// <summary>
        /// Reset the default profile.
        /// </summary>
        /// <param name="portalId">Portal Id to get the portal object</param>
        /// <param name="profileId">Profile Id to get the profile object.</param>
        private void ResetDefaultProfile(int portalId, int profileId)
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            PortalService portalService = new PortalService();
            Portal portal = portalAdmin.GetByPortalId(portalId);

            if (portal.DefaultAnonymousProfileID == profileId)
            {
                portal.DefaultAnonymousProfileID = null;
            }

            if (portal.DefaultRegisteredProfileID == profileId)
            {
                portal.DefaultRegisteredProfileID = null;
            }

            portalService.Update(portal);
        }

        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Order Status
        /// </summary>
        /// <param name="isRemovePendingApproval"> Indicate whether to remove the prnding approval staus from the dropdown list.</param>
        private void BindOrderStatus(bool isRemovePendingApproval)
        {
            OrderAdmin orderAdmin = new OrderAdmin();
            ddlOrderStateList.DataSource = orderAdmin.GetAllOrderStates();
            ddlOrderStateList.DataTextField = "OrderStateName";
            ddlOrderStateList.DataValueField = "OrderStateID";
            ddlOrderStateList.DataBind();

            // Remove Pending Approval Status from the Dropdown, if required.
            if (isRemovePendingApproval)
            {
                ListItem listItem = ddlOrderStateList.Items.FindByValue(((int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.PENDING_APPROVAL).ToString());
                if (listItem != null)
                {
                    ddlOrderStateList.Items.Remove(listItem);
                }
            }
        }

        /// <summary>
        /// Bind Store Catalogs
        /// </summary>
        private void BindCatalog()
        {
            ddlCatalog.DataSource = this.catalogAdmin.GetAllCatalogs();
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();
        }

        /// <summary>
        /// Binds the themes to the DropDownList
        /// </summary>
        private void BindThemeList()
        {
            ddlThemeslist.DataSource = DataRepository.ThemeProvider.GetAll();
            ddlThemeslist.DataTextField = "Name";
            ddlThemeslist.DataValueField = "ThemeID";
            ddlThemeslist.DataBind();
            ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownSiteTheme").ToString(), "0");
            ddlThemeslist.Items.Insert(0, li);
            BindCssList();
        }

        /// <summary>
        /// Bind the CSS to the DropDownList
        /// </summary>
        private void BindCssList()
        {
            if (ddlThemeslist.SelectedIndex > 0)
            {
                ddlCSSList.DataSource = DataRepository.CSSProvider.GetByThemeID(int.Parse(ddlThemeslist.SelectedValue));
                ddlCSSList.DataTextField = "Name";
                ddlCSSList.DataValueField = "CSSID";
                ddlCSSList.DataBind();
                ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownSameAsStore").ToString(), "0");
                ddlCSSList.Items.Insert(0, li);
            }
        }

        /// <summary>
        /// Bind data to form fields
        /// </summary>
        private void BindEditData()
        {
            // Get the catalogId based on PortalID
            int catalogIdValue = this.catalogAdmin.GetCatalogIDByPortalID(this.itemId);

            // Set the Preselected Value.
            if (catalogIdValue != 0)
            {
                ddlCatalog.SelectedValue = catalogIdValue.ToString();
                this.BindThemeList();

                PortalCatalogService portalCatalogService = new PortalCatalogService();
                TList<PortalCatalog> portalCatalogList = portalCatalogService.GetByPortalID(this.itemId);

                if (portalCatalogList[0].ThemeID != null)
                {
                    ddlThemeslist.SelectedValue = portalCatalogList[0].ThemeID.ToString();

                    if (ddlThemeslist.SelectedValue != null)
                    {
                        pnlCssList.Visible = true;
                        this.selectedTheme = ddlThemeslist.SelectedItem.Text;
                        ddlCSSList.Items.Clear();

                        this.BindCssList();
                    }
                }

                // CSS
                if (portalCatalogList[0].CSSID != null)
                {
                    ddlCSSList.SelectedValue = portalCatalogList[0].CSSID.ToString();
                }
            }

            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = storeAdmin.GetByPortalId(this.itemId);
            txtCompanyName.Text = portal.CompanyName;
            txtStoreName.Text = portal.StoreName;
            chkEnableSSL.Checked = portal.UseSSL;
            txtAdminEmail.Text = portal.AdminEmail;
            txtSalesEmail.Text = portal.SalesEmail;
            txtCustomerServiceEmail.Text = portal.CustomerServiceEmail;
            txtSalesPhoneNumber.Text = portal.SalesPhoneNumber;
            txtCustomerServicePhoneNumber.Text = portal.CustomerServicePhoneNumber;

            chkInclusiveTax.Checked = portal.InclusiveTax;
            ListReviewStatus.SelectedValue = portal.DefaultReviewStatus;
			CheckEnableCustomerBasedPricing.Checked = portal.EnableCustomerPricing.GetValueOrDefault(false);
            // Load the resized store logo from Content folder.
            ZNodeImage znodeImage = new ZNodeImage();
            if (portal.LogoPath != null)
            {
                imgLogo.ImageUrl = znodeImage.GetImageHttpPathSmall("Turnkey/" + portal.PortalID + "/" + Path.GetFileName(portal.LogoPath));
            }
            else
            {
                imgLogo.ImageUrl = znodeImage.GetImageHttpPathSmall(string.Empty);
                tblLogoUpload.Style.Add("display", "block");
                radNewImage.Checked = true;
            }

            if (portal.DefaultOrderStateID.GetValueOrDefault(10).ToString() == ((int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.PENDING_APPROVAL).ToString())
            {
                this.BindOrderStatus(false);
                ddlOrderStateList.Enabled = false;
                chkPendingApproval.Checked = true;

                // Display tab
                ddlOrderStateList.SelectedValue = portal.DefaultOrderStateID.GetValueOrDefault(10).ToString();
            }
            else
            {
                this.BindOrderStatus(true);

                ddlOrderStateList.Enabled = true;
                chkPendingApproval.Checked = false;
                ddlOrderStateList.SelectedValue = portal.DefaultOrderStateID.GetValueOrDefault(10).ToString();
            }
        }

        #endregion
    }
}