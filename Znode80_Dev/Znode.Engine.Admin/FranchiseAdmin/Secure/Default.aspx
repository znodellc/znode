<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Secure.Default" Title="Untitled Page" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Dashboard">
        <div>
            <img id="Img1" src="~/FranchiseAdmin/Themes/images/clear.gif" runat="server" width="10" height="10"
                alt="" />
        </div>

        <!-- Welcome Message -->
        <div class="Caption">
            <asp:Localize ID="TitleWelcome" Text='<%$ Resources:ZnodeAdminResource,TitleWelcome%>' runat="server"></asp:Localize>
            <asp:LoginName ID="uxLoginName" runat="server" />
        </div>
        <p class="h2-sub">
            <asp:Localize ID="TextManageStores" Text='<%$ Resources:ZnodeAdminResource,TextManageStores%>' runat="server"></asp:Localize>
        </p>
        <ul class="horizontal-nav callouts">
            <li class="setting">
                <a href="/FranchiseAdmin/Secure/Setup/Default.aspx">
                    <span class="wrap-out">
                        <span class="wrap-in">
                            <span class="text"><span id="dashheading">
                                <asp:Localize ID="LinkSetup" Text='<%$ Resources:ZnodeAdminResource,LinkSetup%>' runat="server"></asp:Localize></span><br />
                                <div id="dashtext">
                                    <asp:Localize ID="SubTextCreateNewStores" Text='<%$ Resources:ZnodeAdminResource,SubTextManageStore%>' runat="server"></asp:Localize></div>
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="catalog">
                <a href="/FranchiseAdmin/Secure/Inventory/Default.aspx">
                    <span class="wrap-out">
                        <span class="wrap-in">
                            <span class="text"><span id="dashheading">
                                <asp:Localize ID="LinkInventory" Text='<%$ Resources:ZnodeAdminResource,LinkInventory%>' runat="server"></asp:Localize></span><br />
                                <div id="dashtext">
                                    <asp:Localize ID="SubTextManageProducts" Text='<%$ Resources:ZnodeAdminResource,SubTextManageProducts%>' runat="server"></asp:Localize></div>
                            </span>
                        </span>
                    </span>
                </a>
            </li>

            <li class="market">
                <a href="/FranchiseAdmin/Secure/Marketing/Default.aspx">
                    <span class="wrap-out">
                        <span class="wrap-in">
                            <span class="text"><span id="dashheading">
                                <asp:Localize ID="LinkMarketing" Text='<%$ Resources:ZnodeAdminResource,LinkMarketing%>' runat="server"></asp:Localize></span><br />
                                <div id="dashtext">
                                    <asp:Localize ID="SubTextManagePromotions" Text='<%$ Resources:ZnodeAdminResource,SubTextManagePromotions%>' runat="server"></asp:Localize></div>
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="service">
                <a href="/FranchiseAdmin/Secure/Orders/Default.aspx">
                    <span class="wrap-out">
                        <span class="wrap-in">
                            <span class="text"><span id="dashheading">
                                <asp:Localize ID="LinkOrders" Text='<%$ Resources:ZnodeAdminResource,LinkOrders%>' runat="server"></asp:Localize></span><br />
                                <div id="dashtext">
                                    <asp:Localize ID="SubTextManageorders" Text='<%$ Resources:ZnodeAdminResource,SubTextManageorders%>' runat="server"></asp:Localize></div>
                            </span>
                        </span>
                    </span>
                </a>
            </li>
            <li class="optimize">
                <a href="/FranchiseAdmin/Secure/reports/report.aspx">
                    <span class="wrap-out">
                        <span class="wrap-in">
                            <span class="text"><span id="dashheading">
                                <asp:Localize ID="LinkReports" Text='<%$ Resources:ZnodeAdminResource,LinkReports%>' runat="server"></asp:Localize></span><br />
                                <div id="dashtext">
                                    <asp:Localize ID="SubTextGeneratereports" Text='<%$ Resources:ZnodeAdminResource,SubTextGeneratereports%>' runat="server"></asp:Localize></div>
                            </span>
                        </span>
                    </span>
                </a>
            </li>
        </ul>
        <div class="DashBoardRow">
            <div class="clearwrap">
                <div class="tips-alerts">
                    <h2>
                        <asp:Localize ID="TitleAlerts" Text='<%$ Resources:ZnodeAdminResource,TitleAlerts%>' runat="server"></asp:Localize>
                    </h2>
                    <ul class="vertical-nav alerts">
                        <li class="alert">
                            <strong>
                                <asp:Label ID="inventoryText" runat="server"></asp:Label></strong>
                            <p>
                                <a href="/FranchiseAdmin/Secure/Reports/Report.aspx?mode=Inventory Re-Order">
                                    <asp:Localize ID="LinkManageInventory" Text='<%$ Resources:ZnodeAdminResource,LinkManageInventory%>' runat="server"></asp:Localize>
                                </a>
                            </p>
                        </li>
                        <li class="alert">
                            <strong>
                                <asp:Label ID="loginText" runat="server"></asp:Label></strong>
                            <p>
                                <a href="/FranchiseAdmin/Secure/Reports/Report.aspx?mode=Email Opt-In Customers">
                                    <asp:Localize ID="LinkViewReport" Text='<%$ Resources:ZnodeAdminResource,LinkTextViewReport%>' runat="server"></asp:Localize>
                                </a>
                            </p>
                        </li>
                    </ul>
                </div>
                <div class="orders">
                    <h2></h2>
                    <ul class="vertical-nav orders">
                        <li>
                            <strong>
                                <asp:Label ID="YTDSales" runat="server"></asp:Label></strong>
                            <p>
                                <asp:Localize ID="SubTextTotalYTD" Text='<%$ Resources:ZnodeAdminResource,SubTextTotalYTD%>' runat="server"></asp:Localize></p>
                        </li>
                        <li>
                            <strong>
                                <asp:Label ID="YTDOrders" runat="server"></asp:Label></strong>
                            <p>
                                <asp:Localize ID="SubTextOrderYTD" Text='<%$ Resources:ZnodeAdminResource,SubTextOrderYTD%>' runat="server"></asp:Localize></p>
                        </li>
                        <li>
                            <strong>
                                <asp:Label ID="YTDAccountsCreated" runat="server"></asp:Label></strong>
                            <p>
                                <asp:Localize ID="SubTextAccountYTD" Text='<%$ Resources:ZnodeAdminResource,SubTextAccountYTD%>' runat="server"></asp:Localize></p>
                        </li>
                        <li>
                            <p><a id="A4" href="~/FranchiseAdmin/Secure/reports/report.aspx" runat="server">
                                <asp:Localize ID="LinkRunReport" Text='<%$ Resources:ZnodeAdminResource,LinkRunReport%>' runat="server"></asp:Localize></a></p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
