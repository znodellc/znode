﻿using System;
using ZNode.Libraries.Framework.Business;
using System.Web;

namespace Znode.Engine.FranchiseAdmin
{
    public partial class popup : ZNode.Libraries.Framework.Business.ZNodeAdminTemplate
    {
        protected void Page_Init(object sender, EventArgs e)
        {
            Page.ViewStateUserKey = Session.SessionID;

            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {
                    string inURL = Request.Url.ToString();
                    string outURL = inURL.ToLower().Replace("http://", "https://");

                    Response.Redirect(outURL);
                }
            }
        }

        protected new void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
        }
    }
}
