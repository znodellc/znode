<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="Znode.Engine.FranchiseAdmin.Controls.Default.ShoppingCart" CodeBehind="ShoppingCart.ascx.cs" %>
<%@ Register Src="~/FranchiseAdmin/Controls/Default/SalesDepartmentPhone.ascx" TagName="SalesDepartmentPhoneNo"
    TagPrefix="ZNode" %>
<%@ Register Src="~/FranchiseAdmin/Controls/Default/CustomMessage.ascx" TagName="CustomMessage" TagPrefix="ZNode" %>
<div>
    <asp:Repeater ID="rptShiping" runat="server" OnItemDataBound="rptShiping_ItemDataBound">
        <ItemTemplate>
            <asp:GridView class="TableContainer" ID="uxCart" runat="server" AutoGenerateColumns="False"
                EmptyDataText="Shopping Cart Empty" GridLines="None" CellPadding="4" OnRowCommand="Cart_RowCommand"
                OnRowDataBound="UxCart_RowDataBound" CssClass="Grid">
                <Columns>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleQuantity %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="140px">
                        <ItemTemplate>
                            <asp:DropDownList ID="uxQty" runat="server" AutoPostBack="true" EnableViewState="true"
                                OnSelectedIndexChanged="Quantity_SelectedIndexChanged" ValidationGroup="groupPayment"
                                CausesValidation="false" CssClass="Quantity">
                            </asp:DropDownList>
                            <asp:RangeValidator ValidationGroup="groupPayment" ID="RangeValidator1" MinimumValue="0"
                                ControlToValidate="uxQty" ErrorMessage="*Out of Stock" Font-Bold="True" MaximumValue='<%# CheckInventory(DataBinder.Eval(Container.DataItem, "GUID").ToString()) %>'
                                Enabled='<%# EnableStockValidator(DataBinder.Eval(Container.DataItem, "GUID").ToString()) %>'
                                SetFocusOnError="true" runat="server" Type="Integer" Display="Dynamic"></asp:RangeValidator>
                            <asp:HiddenField ID="GUID" runat="server" Value='<%# DataBinder.Eval(Container.DataItem, "GUID").ToString()  + "|" + DataBinder.Eval(((RepeaterItem)((GridView)((GridViewRow)Container).NamingContainer).Parent).DataItem, "AddressID").ToString() %>' />
                            <br />
                            <br />
                            <asp:LinkButton ID="ibRemoveLineItem" runat="server" Text="Remove" CssClass="LeftFloat" CommandName="remove"
                                CommandArgument='<%# DataBinder.Eval(Container.DataItem, "GUID").ToString() + "|" + DataBinder.Eval(((RepeaterItem)((GridView)((GridViewRow)Container).NamingContainer).Parent).DataItem, "AddressID").ToString() %>' CausesValidation="false"></asp:LinkButton>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img id="Img1" alt='<%# DataBinder.Eval(Container.DataItem, "Product.ImageAltTag")%>'
                                border='0' src='<%# DataBinder.Eval(Container.DataItem, "Product.ThumbnailImageFilePath")%>'
                                runat="server" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProductCaps %>' HeaderStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <div class="ProductName">
                                <asp:Label ID="lblProductName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Product.Name").ToString()%>'></asp:Label>
                            </div>
                            <div class="Description">
                                Item#
                        <%# DataBinder.Eval(Container.DataItem, "Product.ProductNum").ToString()%><br>
                                <%# DataBinder.Eval(Container.DataItem, "Product.ShoppingCartDescription").ToString()%>
                            </div>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePrice %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "UnitPrice","{0:c}") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()%>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleTotal %>' HeaderStyle-HorizontalAlign="Right"
                        ItemStyle-HorizontalAlign="Right" meta:resourcekey="TemplateFieldResource4" ItemStyle-Width="100px">
                        <ItemTemplate>
                            <div class="Description">
                                <%# (GetQuantity(Container.DataItem, (int)DataBinder.Eval(((GridViewRow)Container).NamingContainer.Parent, "DataItem.AddressId")) * (decimal)DataBinder.Eval(Container.DataItem, "UnitPrice")).ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix()%>
                            </div>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Right"></HeaderStyle>
                        <ItemStyle HorizontalAlign="Right"></ItemStyle>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="Footer" />
                <RowStyle CssClass="Row" />
                <HeaderStyle CssClass="Header" />
                <AlternatingRowStyle CssClass="AlternatingRow" />
            </asp:GridView>
            <div>
                <div class="ShoppingCart Form" id="divMultipleShipping">
                    <div class="ShippingSection">
                        <div>
                            <div class="ShipAddressSection">
                                <div class="ShipToText">
                                    &nbsp;
                                </div>
                                <div class="SubTotalText">
                                    <asp:Label ID="lblSubtotalText" runat="server" EnableViewState="False" Text='<%$ Resources:ZnodeAdminResource, SubTextSubTotal %>' Visible="false"></asp:Label>
                                    <div style="float: right;">
                                        <asp:Label ID="lblSubtotal" runat="server" EnableViewState="False"
                                            Visible="false"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="ShipAddressSection">
                                <div class="ShipToText">
                                    <div id="Div1" runat="server">
                                        <b>
                                            <asp:Label ID="lblShipBy" runat="server" EnableViewState="False" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleShipBy %>'></asp:Label>
                                        </b>
                                        <span>
                                            <asp:DropDownList ID="lstShipping" runat="server" AutoPostBack="True"
                                                OnSelectedIndexChanged="LstShipping_SelectedIndexChanged"
                                                meta:resourcekey="lstShippingResource1">
                                            </asp:DropDownList></span>
                                        <div class="Error">
                                            <asp:Literal ID="uxErrorMsg" EnableViewState="False" runat="server"
                                                meta:resourcekey="uxErrorMsgResource1"></asp:Literal>
                                        </div>
                                    </div>
                                </div>
                                <div class="ShipText">
                                    <asp:Label ID="lblTaxCostText" runat="server" EnableViewState="False" Text='<%$ Resources:ZnodeAdminResource, SubTextTax %>' Visible="false"></asp:Label>
                                    <div style="float: right;">
                                        <asp:Label ID="lblTaxCost" runat="server" EnableViewState="False" Visible="false"></asp:Label>
                                    </div>
                                </div>
                            </div>

                            <div class="ShipAddressSection">
                                <div class="ShipToText">
                                    <div id="ShipAddressSection" runat="server">
                                        <b>
                                            <asp:Label ID="lblShippto" runat="server" EnableViewState="False" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleShipToWithSpage %>' Visible="false"></asp:Label>
                                        </b>
                                        <asp:Label ID="lblShippingAddress" runat="server" Text='<%#GetAddress(DataBinder.Eval(Container.DataItem, "AddressID").ToString())%>' Visible="false"></asp:Label>
                                        <asp:LinkButton ID="lnkChange" runat="server" Text="Change" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "AddressID").ToString() %>' OnClick="lnkChangeShipTo_Click" Visible="false"></asp:LinkButton>
                                    </div>
                                </div>
                                <div class="ShipText">
                                    <asp:Label ID="lblShippingText" runat="server" EnableViewState="False" Text='<%$ Resources:ZnodeAdminResource, SubTextShipping %>'></asp:Label>
                                    <div style="float: right;">
                                        <asp:Label ID="lblShipping" runat="server" EnableViewState="False"></asp:Label>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="ClearBoth">
                    <hr style="background-color: #808080;" />
                </div>
            </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</div>
<div style="margin-top: 10px;">
    <table cellpadding="0" cellspacing="0" class="TotalBoxFooter" border="0" width="100%">
        <tr>
            <td colspan="3" align="right">
                <asp:Label ID="PaymentErrorMsg" Text='<%$ Resources:ZnodeAdminResource, ErrorPayment %>'
                    CssClass="Error" runat="server" Visible="false" />
                <div>
                    <asp:Label ID="lblErrorMessage" CssClass="Error" runat="server"></asp:Label>
                </div>
                <div style="float: left; clear: both;">
                    <asp:Label ID="lblMinQuantiryErrorMsg" CssClass="Error" Visible="False" runat="server"></asp:Label>
                </div>
                <div style="float: left; clear: both;">
                    <asp:Label ID="lblMaxQuantity" CssClass="Error" Visible="False" runat="server"></asp:Label>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <div class="CustomMessage" style="display: none">
                    <span>
                        <ZNode:CustomMessage ID="CustomMessage2" MessageKey="ShoppingCartFooterText" runat="server" />
                    </span>
                </div>
            </td>
            <td>
                <div class="Apply">
                    <asp:Localize ID="ccode" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextEnterCouponCode %>'></asp:Localize>
                    <asp:TextBox runat="server" ID="ecoupon"></asp:TextBox>&nbsp;<asp:ImageButton ImageUrl="~/FranchiseAdmin/Themes/images/Apply.gif"
                        ID="ApplyCouponGo" CssClass="gobutton" runat="server" AlternateText="Apply" OnClick="Btnapply_click"
                        ValidationGroup="groupPayment" />
                </div>
                <div>
                    <asp:Label ID="lblPromoMessage" CssClass="Error" runat="server"></asp:Label>
                </div>
            </td>
            <td>
                <div class="TotalBox ShoppingCartTotal" align="right">
                    <table cellpadding="3" cellspacing="0" border="0px" width="100%">
                        <tr>
                            <td class="SubTotal">
                                <asp:Localize ID="SubTextSubTotal" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextSubTotal %>'></asp:Localize>
                            </td>
                            <td class="TotalValue">
                                <asp:Label ID="SubTotal" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr runat="server" visible="true" id="rowDiscount">
                            <td>
                                <asp:Localize ID="SubTextDiscount" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextDiscount %>'></asp:Localize>
                            </td>
                            <td>
                                <asp:Label ID="DiscountDisplay" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr id="tblRowTax" runat="server" visible="false">
                            <td>
                                <asp:Localize ID="SubTextTax" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextTax %>'></asp:Localize>
                                <asp:Label ID="TaxPct" runat="server" EnableViewState="false"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Tax" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr id="tblRowShipping" runat="server" visible="false" class="SubTotal">
                            <td>
                                <asp:Localize ID="TextShipping" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextShipping %>'></asp:Localize>
                            </td>
                            <td>
                                <asp:Label ID="Shipping" runat="server"></asp:Label>
                                <asp:Label ID="lblTotalShipping" runat="server" EnableViewState="False"
                                    meta:resourcekey="ShippingResource2"></asp:Label>
                            </td>
                        </tr>
                        <tr id="trGiftCardAmount" runat="server">
                            <td>
                                <asp:Localize ID="SubTextGiftCardAmount" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextGiftCardAmount %>'></asp:Localize>
                            </td>
                            <td>
                                <asp:Label ID="lblGiftCardAmount" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr class="bold">
                            <td class="SubTotal">
                                <asp:Localize ID="ColumnTitleTotal" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTotal %>'></asp:Localize>
                            </td>
                            <td class="TotalValue">
                                <asp:Label ID="Total" runat="server"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
    </table>
</div>
