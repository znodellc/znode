<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.FranchiseAdmin.Controls.Default.Register"
    CodeBehind="Register.ascx.cs" %>
<%@ Register Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>
<asp:Panel runat="server" ID="pnlCreateAccount">
    <div class="LoginPage">
        <div>
            <h1> <asp:Localize ID="LinkTextRequestFranchise" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextRequestFranchise %>'></asp:Localize></h1>
            <p>
                 <asp:Localize ID="TextManageFranchiseStore" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextManageFranchiseStore %>'></asp:Localize>
            </p>
        </div>
        <div class="FormView">
            <div>
                <ZNode:spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:spacer>
            </div>
            <div>
                <div align="left" colspan="2" class="Error">
                    <asp:Literal ID="ErrorMessage" runat="server" EnableViewState="False"></asp:Literal>
                </div>
            </div>
            <div class="h1Style">
                  <asp:Localize ID="SubTitleLogin" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleLogin %>'></asp:Localize>            
            </div>
            <div class="FieldStyle">
                  <asp:Localize ID="ColumnTitleFranchiseNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFranchiseNumber %>'></asp:Localize><span class="Asterix">*</span><br />
                <small><asp:Localize ID="TextFranchiseAdminAccount" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextFranchiseAdminAccount %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="FranchiseNumber" runat="server" CssClass="TextField" MaxLength="50"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="FranchiseNumberRequired" runat="server" ControlToValidate="FranchiseNumber"
                        ErrorMessage="Franchise Number is required." ToolTip="Franchise Number is required." ValidationGroup="uxCreateUserWizard"
                        CssClass="Error" Display="Dynamic">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnLoginUserName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLoginUserName %>'></asp:Localize><span class="Asterix">*</span><br />
                <small><asp:Localize ID="TextEnterLoginUserName" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextEnterLoginUserName %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="UserName" runat="server" autocomplete="off" CssClass="TextField"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                        ErrorMessage="User Name is required." ToolTip="User Name is required." ValidationGroup="uxCreateUserWizard"
                        CssClass="Error" Display="Dynamic">
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                  <asp:Localize ID="ColumnPassword" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPassword %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="Password" runat="server" autocomplete="off" TextMode="Password" CssClass="TextField"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredPassword %>'
                        ToolTip='<%$ Resources:ZnodeAdminResource, RequiredPassword %>' ControlToValidate="Password" ValidationGroup="uxCreateUserWizard"
                        CssClass="Error" Display="Dynamic">                      
                    </asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ValidationGroup="uxCreateUserWizard" ID="PasswordRegularExpression"
                        ControlToValidate="Password" runat="server" ValidationExpression="(?!^[0-9]*$)(?!^[a-zA-Z]*$)^([a-zA-Z0-9]{8,})"
                        Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularPasswordExpression %>'
                        ToolTip='<%$ Resources:ZnodeAdminResource, RegularPasswordExpression %>'
                        CssClass="Error"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="FieldStyle">
                  <asp:Localize ID="ColumnTitleConfirmPassword" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleConfirmPassword %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="ConfirmPassword" runat="server" autocomplete="off" TextMode="Password" CssClass="TextField"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="ConfirmPasswordRequired" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredConfirmPassword %>'
                        ToolTip="Confirm Password is Required" runat="server" ControlToValidate="ConfirmPassword"
                        ValidationGroup="uxCreateUserWizard" CssClass="Error" Display="Dynamic">
                    </asp:RequiredFieldValidator>
                </div>
                <div>
                    <asp:CompareValidator ID="PasswordCompare" runat="server" ControlToCompare="Password"
                        ControlToValidate="ConfirmPassword" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareConfirmationPassword %>'
                        ToolTip='<%$ Resources:ZnodeAdminResource, CompareConfirmationPassword %>' ValidationGroup="uxCreateUserWizard"
                        CssClass="Error" Display="Dynamic"></asp:CompareValidator>
                </div>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnTitleSecretQuestion" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSecretQuestion %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlSecretQuestions" runat="server">
                    <asp:ListItem Enabled="true" Selected="True" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFavoritePet %>'></asp:ListItem>
                    <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextCityBorn %>'></asp:ListItem>
                    <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextHighSchool %>'></asp:ListItem>
                    <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFavoriteMovie %>'></asp:ListItem>
                    <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextMotherMaiden %>'></asp:ListItem>
                    <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFirstCar %>'></asp:ListItem>
                    <asp:ListItem Enabled="true" Text='<%$ Resources:ZnodeAdminResource, DropDownTextFavoriteColor %>'></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnTitleSecretAnswer" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSecretAnswer %>'></asp:Localize> <span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="Answer" runat="server" CssClass="TextField"></asp:TextBox>
                <div>
                    <asp:RequiredFieldValidator ID="SecurityAnswerRequired" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSecurityAnswer %>'
                        ToolTip="Security Answer is required." ControlToValidate="Answer" ValidationGroup="uxCreateUserWizard"
                        CssClass="Error" Display="Dynamic">                      
                    </asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="h1Style">
                   <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSTORESETTING %>'></asp:Localize>      
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="TextStoreName" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextStoreName %>'></asp:Localize> <span class="Asterix">*</span><br />
            </div>

            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingCompanyName" runat="server" CssClass="TextField" Columns="30"
                    MaxLength="100"></asp:TextBox><div>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator7" ErrorMessage='<%$ Resources:ZnodeAdminResource, TextStoreNameisrequired %>'
                            ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingCompanyName"
                            runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                    </div>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnTitleTheme" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTheme %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlTheme" runat="server" />
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnTitleSiteURL" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSiteURL %>'></asp:Localize><br />
                <small>  <asp:Localize ID="TextStoreUrl" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextStoreUrl %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtStoreUrl" runat="server" CssClass="TextField" Columns="30" MaxLength="100"></asp:TextBox><div>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtStoreUrl" ValidationGroup="uxCreateUserWizard"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidURLStar %>' CssClass="Error" Display="dynamic" ValidationExpression="^(http\:\/\/[a-zA-Z0-9_\-/]+(?:\.[a-zA-Z0-9_\-/]+)*)$"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnTitleUploadLogo" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleUploadLogo %>'></asp:Localize><br />
                <small> <asp:Localize ID="TextUploadimagetypes" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextUploadimagetypes %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:FileUpload ID="RegisterUploadImage" runat="server" BorderStyle="Inset" EnableViewState="true" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="RegisterUploadImage"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidUploadImage %>' SetFocusOnError="True"
                    ValidationExpression="^.+\.((jpg)|(JPG)|(gif)|(GIF)|(jpeg)|(JPEG)|(png)|(PNG))$" Display="Dynamic"
                    ValidationGroup="uxCreateUserWizard"> </asp:RegularExpressionValidator>
            </div>

            <div class="ClearBoth">
            </div>
            <div class="h1Style">
                     <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleContactInformation %>'></asp:Localize>           
            </div>

            <div class="FieldStyle">
                  <asp:Localize ID="ColumnFirstName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFirstName %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingFirstName" runat="server" CssClass="TextField" Columns="30"
                    MaxLength="100"></asp:TextBox><div>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredFirstName %>'
                            ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingFirstName" runat="server"
                            Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnLastName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLastName %>'></asp:Localize> <span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingLastName" runat="server" CssClass="TextField" Columns="30"
                    MaxLength="100"></asp:TextBox><div>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredLastName %>'
                            ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingLastName" runat="server"
                            Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                  <asp:Localize ID="ColumnEmailAddress" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEmailAddress %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingEmail" runat="server" CssClass="TextField"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEmailAddress %>'
                        ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingEmail" runat="server"
                        Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="regemailID" runat="server" ControlToValidate="txtBillingEmail"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidEmail %>' Display="Dynamic" ValidationExpression='[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+'
                        CssClass="Error" ValidationGroup="uxCreateUserWizard"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="FieldStyle">
                  <asp:Localize ID="ColumnPhoneNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPhoneNumber %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingPhoneNumber" runat="server" CssClass="TextField" Columns="30"
                    MaxLength="100"></asp:TextBox><div>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredPhoneNumber %>'
                            ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingPhoneNumber"
                            runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnStreet1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStreet1 %>'></asp:Localize> <span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingStreet1" runat="server" CssClass="TextField" Columns="30"
                    MaxLength="100"></asp:TextBox><div>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredStreet1 %>'
                            ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingStreet1" runat="server"
                            Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnStreet2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStreet2 %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingStreet2" runat="server" CssClass="TextField" Columns="30"
                    MaxLength="100"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="txLiveChat" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCity %>' /> <span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingCity" runat="server" CssClass="TextField" Columns="30"
                    MaxLength="100"></asp:TextBox><div>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredCity %>'
                            ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingCity" runat="server"
                            Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnTitleState" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleState %>'></asp:Localize> <span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingState" runat="server" Width="40" Columns="10"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredState %>'
                        ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingState" runat="server"
                        Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                  <asp:Localize ID="ColumnPostalCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPostalCode %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBillingPinCode" runat="server" Width="40" Columns="10"></asp:TextBox><div>
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredPostalCode %>'
                        ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingPinCode" runat="server"
                        Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
                <ZNode:spacer ID="Spacer1" SpacerHeight="30" SpacerWidth="10" runat="server"></ZNode:spacer>
                <div class="ValueStyle">  
                    <zn:Button runat="server"  ID="ibRegister" ButtonType="SubmitButton" OnClick="IbRegister_Click" ValidationGroup="uxCreateUserWizard"  Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" />
                    <zn:Button runat="server" ID="btnClear"  ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="false" />
                 </div>
            </div>
        </div>
    </div>
</asp:Panel>
<asp:Panel ID="pnlConfirm" runat="server" Visible="False" CssClass="SuccessMsg">
    <br />
    <br />
    <p> <asp:Localize ID="TextActivateRequest" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextActivateRequest %>'></asp:Localize></p>
    <div class="Clear">
        <ZNode:spacer ID="Spacer9" EnableViewState="false" SpacerHeight="20" SpacerWidth="10"
            runat="server"></ZNode:spacer>
    </div>
</asp:Panel>

