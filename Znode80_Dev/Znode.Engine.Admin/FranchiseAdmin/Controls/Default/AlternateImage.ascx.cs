﻿//-----------------------------------------------------------------------
// <copyright file="AlternateImage.ascx.cs" company="Znode Inc">
//     Copyright ©, Znode Inc, All Rights Reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;

namespace  Znode.Engine.FranchiseAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Franchise Admin AlternateImage user control class.
    /// </summary>
    public partial class AlternateImage : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private string addImageLink = "~/FranchiseAdmin/Secure/Reviewer/add_view.aspx?itemid=";
        private int itemId;
        private ZNodeProductImageType _ImageType = ZNodeProductImageType.AlternateImage;
        
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the ImageType
        /// </summary>
        public ZNodeProductImageType ImageType
        {
            get
            {
                return this._ImageType;
            }

            set
            {
                this._ImageType = value;
            }
        }

        #endregion   

        #region Public Methods
        /// <summary>
        /// Get http image path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the image file path with name.</returns>
        public string GetImagePath(string imageFileName)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(imageFileName);
        }
        #endregion

        #region Page Load Methods
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get itemId (Product Id) from QueryString        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.itemId = 0;
            }

            if (!Page.IsPostBack)
            {
                // Set the button caption based on the user and Image type.
                UserStoreAccess uss = new UserStoreAccess();
				if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer)
                {
                    btnReject.Text = "Reject Checked";
                }
                else
                {
                    // Hide the Accept Checked button from vendor.
                    btnAccept.Visible = false;
                    if (this.ImageType == ZNodeProductImageType.AlternateImage)
                    {
                        btnReject.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonAddNewAlternateImage").ToString();
                        btnReject.CssClass = "Size175";
                        lblCaption.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextAddImage").ToString();
                    }
                    else
                    {
                        btnReject.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonAddNewSwatchImage").ToString();
                        btnReject.CssClass = "Size175";
                        lblCaption.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextAddProductImage").ToString();
                    }
                }

                if (this.itemId > 0)
                {
                    this.BindAlternateImages();
                }
                else
                {
                    throw new ApplicationException( this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotFoundProductRequested").ToString());
                }
            }
        }
        #endregion

        #region Protected Methods and Events
        protected void BtnReject_Click(object sender, EventArgs e)
        {
            UserStoreAccess uss = new UserStoreAccess();
			if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Vendor)
            {
                Response.Redirect(this.addImageLink + this.itemId + "&typeid=" + Convert.ToString((int)this._ImageType) + "&mode=alternateimages");
            }
            else
            {
                this.UpdateImageStatus(false);
            }
        }

        protected void BtnAccept_Click(object sender, EventArgs e)
        {
            UserStoreAccess uss = new UserStoreAccess();
			if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Vendor)
            {
                Response.Redirect(this.addImageLink + this.itemId + "&typeid=" + Convert.ToString((int)this._ImageType) + "&mode=alternateimages");
            }
            else
            {
                this.UpdateImageStatus(true);
            }
        }

        /// <summary>
        /// Row command 
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GridThumb_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                Response.Redirect(this.addImageLink + this.itemId + "&productimageid=" + e.CommandArgument.ToString() + "&typeid=" + Convert.ToString((int)this._ImageType) + "&mode=alternateimages");
            }

            if (e.CommandName == "RemoveItem")
            {
                ZNode.Libraries.Admin.ProductViewAdmin prodadmin = new ProductViewAdmin();
                bool status = prodadmin.Delete(int.Parse(e.CommandArgument.ToString()));
                if (status)
                {
                    this.BindAlternateImages();
                }
            }
        }

        protected string GetStatusImageIcon(object state)
        {
            string imageSrc = "~/FranchiseAdmin/Themes/Images/warning_small.gif";

            if (state.ToString().ToLower() == "30")
            {
                // Image rejected
                imageSrc = ZNode.Libraries.Admin.Helper.GetCheckMark(false);
            }

            if (state.ToString().ToLower() == "20")
            {
                // Image approved            
                imageSrc = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
            }

            return imageSrc;
        }

        protected void GridThumb_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            UserStoreAccess uss = new UserStoreAccess();
			if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer)
            {
                // If Revier then hide the Action (Edit and Remove) column
                if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Footer)
                {
                    e.Row.Cells[8].Visible = false;
                }
            }
            else
            {
                // If Vendor then hide the checkbox column
                if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Footer)
                {
                    e.Row.Cells[0].Visible = false;
                }
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Bind Alternate images
        /// </summary>
        private void BindAlternateImages()
        {
            ProductViewAdmin imageadmin = new ProductViewAdmin();
            DataSet ds = imageadmin.GetAllAlternateImage(this.itemId);
            string filterExpression = string.Empty;

            // Filter based on image types
            filterExpression = "ProductImageTypeID=" + Convert.ToString((int)this._ImageType);

            ds.Tables[0].DefaultView.RowFilter = filterExpression;
            GridThumb.DataSource = ds.Tables[0].DefaultView;
            GridThumb.DataBind();

            UserStoreAccess uss = new UserStoreAccess();

            // If there are not product alternate image or swatch image then hide both buttons.
			if (ds.Tables[0].DefaultView.Count == 0 && uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer)
            {
                btnAccept.Enabled = false;
                btnReject.Enabled = false;
            }
        }

        /// <summary>
        /// Update (Approved/Decline) the selected image status.
        /// </summary>
        /// <param name="status">Status to update.
        /// False: to update image as declined
        /// True: to update image as approved.
        /// </param>
        private void UpdateImageStatus(bool status)
        {
            List<string> selectedProductImagesIdList = new List<string>();
            for (int rowIndex = 0; rowIndex <= GridThumb.Rows.Count - 1; rowIndex++)
            {
                CheckBox chk = GridThumb.Rows[rowIndex].Cells[0].FindControl("chkSelect") as CheckBox;
                string productImageId = GridThumb.Rows[rowIndex].Cells[2].Text;
                if (chk.Checked)
                {
                    selectedProductImagesIdList.Add(productImageId);
                }
            }

            ZNode.Libraries.Admin.ProductViewAdmin productAdmin = new ProductViewAdmin();
            TList<ProductImage> productImageList = productAdmin.GetImageByProductID(this.itemId);
            foreach (string productImageId in selectedProductImagesIdList)
            {
                int imageId = Convert.ToInt32(productImageId);
                int reviewStateId = 10;
                TList<ProductImage> filteredProductImageList = productImageList.FindAll(delegate(ProductImage pi) { return pi.ProductImageID == imageId; });
                if (filteredProductImageList.Count == 1)
                {
                    ProductImage currentItem = filteredProductImageList[0];
                    reviewStateId = status == true ? Convert.ToInt32(ZNodeProductReviewState.Approved) : Convert.ToInt32(ZNodeProductReviewState.Declined);

                    // Update review state only if existing state is not same as current state.
                    if (reviewStateId != currentItem.ReviewStateID)
                    {
                        // Set the review state ID from enumeration
                        currentItem.ReviewStateID = reviewStateId;

                        // Set the product image as rejected.
                        productAdmin.Update(currentItem);
                    }
                }
            }

            // Reload the image list.
            this.BindAlternateImages();
        }

        #endregion
    }
}
