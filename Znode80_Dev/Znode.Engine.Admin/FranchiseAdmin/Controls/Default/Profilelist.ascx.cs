using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace  Znode.Engine.FranchiseAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Franchise Admin Profilelist user control class.
    /// </summary>
    public partial class Profilelist : System.Web.UI.UserControl
    {
        #region Private Variables
        private string _PreSelectValue = string.Empty;
        #endregion

        /// <summary>
        /// Gets the value of Drop Down List
        /// </summary>
        public int SelectedValue
        {
            get
            {
                if (ddlprofilleList.SelectedIndex != -1)
                {
                    return int.Parse(ddlprofilleList.SelectedValue);
                }

                return 0;
            }
        }

        /// <summary>
        /// Gets or sets the default value of Drop Down List
        /// </summary>
        public string PreSelectValue
        {
            get
            {
                return this._PreSelectValue;
            }

            set
            {
                this._PreSelectValue = value;

                ddlprofilleList.SelectedValue = this._PreSelectValue;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                // Bind data based on PortalId
                ZNode.Libraries.DataAccess.Service.ProfileService profileServ = new ZNode.Libraries.DataAccess.Service.ProfileService();
                TList<Profile> profileList = profileServ.GetAll();

                // Use Deepload to get a child table field name "Store Name"           
                foreach (Profile profile in profileList)
                {
                    ListItem li = new ListItem();
                    li.Text = profile.Name;
                    li.Value = profile.ProfileID.ToString();
                    ddlprofilleList.Items.Add(li);
                }

                // DropDownList Type                
                ddlprofilleList.SelectedValue = this._PreSelectValue;
                ddlprofilleList.Items.Insert(0, new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAllProfiles").ToString(), "-1"));
                ddlprofilleList.SelectedItem.Value = "-1";
            }
        }
    }
}