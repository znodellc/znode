using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace  Znode.Engine.FranchiseAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Franchise Admin Footer user control class.
    /// </summary>
    public partial class Footer : System.Web.UI.UserControl
    {
        #region Member Variables
        private string _FooterCopyrightText = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the footer copyright text.
        /// </summary>
        public string FooterCopyrightText
        {
            get { return this._FooterCopyrightText; }
            set { this._FooterCopyrightText = value; }
        } 
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Events
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (System.Configuration.ConfigurationManager.AppSettings["footerCopyrightText"] != null)
            {
                this._FooterCopyrightText = System.Configuration.ConfigurationManager.AppSettings["footerCopyrightText"].ToString();
            }
            else
            {
                this.FooterCopyrightText = this.GetGlobalResourceObject("ZnodeAdminResource", "TextFooter").ToString();
            }
        }
        #endregion
    }
}