using System;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.FranchiseAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Franchise Admin Vendor_Register user control class.
    /// </summary>
    public partial class Register : System.Web.UI.UserControl
    {
        #region Page Load Event

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // check if SSL is required
            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {
                    string inURL = Request.Url.ToString();
                    string outURL = inURL.ToLower().Replace("http://", "https://");

                    Response.Redirect(outURL);
                }
            }

            if (this.Page != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtAccountTitle.Text");
            }

            if (!Page.IsPostBack)
            {
                this.BindTheme();
            }
        }
        #endregion

        #region Protected Events
        /// <summary>
        /// Occurrs when register button clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void IbRegister_Click(object sender, EventArgs e)
        {
            this.RegisterUser();
        }

        /// <summary>
        /// Occurs when Clear button clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            string link = Request.Url.GetLeftPart(UriPartial.Authority) + Response.ApplyAppPathModifier("Default.aspx");
            Response.Redirect(link);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Bind theme file list
        /// </summary>
        private void BindTheme()
        {
            ddlTheme.DataSource = DataRepository.ThemeProvider.GetAll();
            ddlTheme.DataTextField = "Name";
            ddlTheme.DataValueField = "ThemeID";
            ddlTheme.DataBind();

            ddlTheme.SelectedIndex = 0;
        }

        /// <summary>
        /// This function registers a new user 
        /// </summary>
        private void RegisterUser()
        {
            TransactionManager tranManager = ConnectionScope.CreateTransaction();
            try
            {
                // Create Transaction
                Account newUserAccount = new Account();
                AccountService accountService = new AccountService();
                TList<Account> account = accountService.Find(string.Format("ExternalAccountNo='{0}'", FranchiseNumber.Text.Trim()));
                MembershipUser user;
                if (account.Count > 0)
                {
                    if (account[0].UserID != null)
                    {
                        user = Membership.GetUser(account[0].UserID);

                        ErrorMessage.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFranchiseNum").ToString(), FranchiseNumber.Text, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);
                        return;
                    }

                    newUserAccount = account[0];
                }

                int portalId = this.CreateStore();

                if (portalId == 0)
                {
                    return;
                }

                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();
                log.LogActivityTimerStart();

                int profileId = this.CreateStoreProfile(portalId);

                if (profileId == 0)
                {
                    if (tranManager.IsOpen)
                    {
                        tranManager.Rollback();
                    }

                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCreatingAccount").ToString();
                    return;
                }

                // Check if loginName already exists in DB
                ZNodeUserAccount userAccount = new ZNodeUserAccount();
                if (!userAccount.IsLoginNameAvailable(portalId, UserName.Text.Trim()))
                {
                    if (tranManager.IsOpen)
                    {
                        tranManager.Rollback();
                    }

                    log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "TextUsernameexists").ToString(), null);
                    ErrorMessage.Text =  this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUserNameExist").ToString();
                    return;
                }

                MembershipCreateStatus status = MembershipCreateStatus.ProviderError;

                user = Membership.CreateUser(UserName.Text.Trim(), Password.Text.Trim(), txtBillingEmail.Text.Trim(), ddlSecretQuestions.SelectedItem.Text, Answer.Text.Trim(), false, out status);

                if (user == null)
                {
                    if (tranManager.IsOpen)
                    {
                        tranManager.Rollback();
                    }

                    log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text);
                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCreatingAccount").ToString();

                    return;
                }

                log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, UserName.Text);

                // Create the user account               
                newUserAccount.UserID = (Guid)user.ProviderUserKey;
                user.ChangePasswordQuestionAndAnswer(Password.Text.Trim(), ddlSecretQuestions.SelectedItem.Text, Answer.Text.Trim());
                newUserAccount.CompanyName = txtBillingCompanyName.Text;
                newUserAccount.ExternalAccountNo = FranchiseNumber.Text.Trim();
                newUserAccount.Email = txtBillingEmail.Text.Trim();

                newUserAccount.CreateDte = DateTime.Now;
                newUserAccount.UpdateDte = DateTime.Now;
                newUserAccount.ProfileID = profileId;

                // Register account
                if (newUserAccount.AccountID > 0)
                {
                    accountService.Update(newUserAccount);
                }
                else
                {
                    accountService.Insert(newUserAccount);
                }

                AccountAdmin accountAdmin = new AccountAdmin();
                AddressService addressService = new AddressService();
                Address address = accountAdmin.GetDefaultBillingAddress(newUserAccount.AccountID);
                if (address == null)
                {
                    address = new Address();
                    address.IsDefaultShipping = true;
                    address.IsDefaultBilling = true;
                    address.AccountID = newUserAccount.AccountID;
                }

                // Copy info to billing
                address.FirstName = Server.HtmlEncode(txtBillingFirstName.Text);
                address.LastName = Server.HtmlEncode(txtBillingLastName.Text);
                address.CompanyName = Server.HtmlEncode(txtBillingCompanyName.Text);
                address.Street = Server.HtmlEncode(txtBillingStreet1.Text);
                address.Street1 = Server.HtmlEncode(txtBillingStreet2.Text);
                address.City = Server.HtmlEncode(txtBillingCity.Text);
                address.StateCode = Server.HtmlEncode(txtBillingState.Text);
                address.PhoneNumber = txtBillingPhoneNumber.Text;
                address.PostalCode = txtBillingPinCode.Text;

                // Default country code for United States
                address.CountryCode = this.GetGlobalResourceObject("ZnodeAdminResource", "TextUS").ToString();
                address.Name = this.GetGlobalResourceObject("ZnodeAdminResource", "TextDefaultAddress").ToString();

                // Insert shipping address as default billing address.
                if (address.AddressID > 0)
                {
                    addressService.Update(address);
                }
                else
                {
                    addressService.Insert(address);
                }

                // Add Account Profile
                AccountProfile accountProfile = new AccountProfile();
                accountProfile.AccountID = newUserAccount.AccountID;
                accountProfile.ProfileID = profileId;

                AccountProfileService accountProfileServ = new AccountProfileService();
                accountProfileServ.Insert(accountProfile);

                Roles.AddUserToRole(UserName.Text.Trim(), "FRANCHISE");

                // Log password
                ZNodeUserAccount.LogPassword((Guid)user.ProviderUserKey, Password.Text.Trim());

                // Create an Profile for the selected user
                ProfileCommon newProfile = (ProfileCommon)ProfileCommon.Create(UserName.Text, true);

                // Properties Value
                newProfile.StoreAccess = portalId.ToString();

                // Save profile - must be done since we explicitly created it 
                newProfile.Save();

                pnlCreateAccount.Visible = false;
                pnlConfirm.Visible = true;

                // Commit transaction
                tranManager.Commit();
            }
            catch (Exception ex)
            {
                // Error occurred so rollback transaction
                if (tranManager.IsOpen)
                {
                    tranManager.Rollback();
                }

                ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCreatingAccount").ToString();
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message.ToString());
            }
        }

        /// <summary>
        /// Create New Profile for new store based on StoreName_ProfileID.
        /// </summary>
        /// <param name="portalId">Create portal profile with Portal Id.</param>
        /// <returns>Returns the created profile Id.</returns>
        private int CreateStoreProfile(int portalId)
        {
            try
            {
                ProfileService profileService = new ProfileService();
                Profile profile = new Profile();
                profile.Name = string.Format("{0}_{1}", txtBillingCompanyName.Text, portalId);
                ZNodeProfile znodeProfile = new ZNodeProfile();
                profile.ShowPricing = znodeProfile.ShowPrice;
                profile.UseWholesalePricing = znodeProfile.UseWholesalePricing;
                profileService.Insert(profile);

                if (profile.ProfileID > 0)
                {
                    PortalProfileService service = new PortalProfileService();
                    PortalProfile portalProfile = new PortalProfile();
                    portalProfile.PortalID = portalId;
                    portalProfile.ProfileID = profile.ProfileID;
                    service.Insert(portalProfile);

                    PortalService portalService = new PortalService();
                    Portal portal = portalService.GetByPortalID(portalId);
                    portal.DefaultAnonymousProfileID = profile.ProfileID;
                    portal.DefaultRegisteredProfileID = profile.ProfileID;
                    portalService.Update(portal);
                }

                return profile.ProfileID;
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCreatingStoreProfile").ToString(), Environment.NewLine, ex.ToString()));
            }

            return 0;
        }

        /// <summary>
        /// Create new store setting.
        /// </summary>
        /// <returns>Returns the created portal Id.</returns>
        private int CreateStore()
        {
            try
            {
                StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
                Portal portal = new Portal();

                DomainAdmin domainAdmin = new DomainAdmin();
                Domain domain = new Domain();
                bool status;
                string domainName = string.Empty;

                // Domain Checking
                if (!string.IsNullOrEmpty(txtStoreUrl.Text.Trim()))
                {
                    // Set the domainName
                    domainName = txtStoreUrl.Text.Trim().ToLower();

                    if (domainName.Contains("http://") || domainName.Contains("https://"))
                    {
                        domainName = domainName.Replace("http://", string.Empty).Replace("https://", string.Empty);
                    }

                    if (domainName.Contains(":"))
                    {
                        // Strip off the port number so we won't conflict with debuggers and other services that may specify a different port.
                        domainName = domainName.Substring(0, domainName.IndexOf(":"));
                    }

                    if (domainName.StartsWith("www."))
                    {
                        domainName = domainName.Remove(0, 4);
                    }

                    // Remove any trailing "/"
                    if (domainName.EndsWith("/"))
                    {
                        domainName = domainName.Remove(domainName.Length - 1);
                    }

                    DomainService domainService = new DomainService();
                    TList<Domain> domains = domainService.Find(string.Format("DomainName='{0}'", domainName.Trim()));

                    if (domains != null && domains.Count > 0)
                    {
                        ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDomainNameShouldbeUnique").ToString();
                        return 0;
                    }
                }

                // Portal Creation: 

                // Get Exisitng Portal Settings By portalID
                Portal existingPortal = storeAdmin.GetByPortalId(ZNodeConfigManager.SiteConfig.PortalID);

                portal.FedExCSPKey = string.Empty;
                portal.FedExCSPPassword = string.Empty;
                portal.FedExClientProductVersion = string.Empty;
                portal.FedExClientProductId = string.Empty;

                portal.PortalID = 0;
                portal.AdminEmail = txtBillingEmail.Text;
                portal.CompanyName = txtBillingCompanyName.Text;
                portal.CustomerServiceEmail = txtBillingEmail.Text;
                portal.CustomerServicePhoneNumber = txtBillingPhoneNumber.Text;
                portal.InclusiveTax = false;
                portal.ActiveInd = true;
                portal.SalesEmail = txtBillingEmail.Text;
                portal.SalesPhoneNumber = txtBillingPhoneNumber.Text;
                portal.StoreName = txtBillingCompanyName.Text;
                portal.UseSSL = false;

                // Default Customer Review Status
                portal.DefaultReviewStatus = "N";

                // Default to English
                portal.LocaleID = 43;
                portal.FedExAccountNumber = string.Empty;
                portal.FedExAddInsurance = false;
                portal.FedExProductionKey = string.Empty;
                portal.FedExSecurityCode = string.Empty;
                portal.InclusiveTax = false;
                portal.SMTPPassword = string.Empty;
                portal.SMTPServer = string.Empty;
                portal.SMTPUserName = string.Empty;

                portal.SiteWideAnalyticsJavascript = string.Empty;
                portal.SiteWideBottomJavascript = string.Empty;
                portal.SiteWideTopJavascript = string.Empty;
                portal.OrderReceiptAffiliateJavascript = string.Empty;
                portal.GoogleAnalyticsCode = string.Empty;
                portal.UPSKey = string.Empty;
                portal.UPSPassword = string.Empty;
                portal.UPSUserName = string.Empty;

                // Default values from existing portal
                portal.SMTPPort = existingPortal.SMTPPort;
	            portal.SMTPPassword = existingPortal.SMTPPassword;
	            portal.SMTPServer = existingPortal.SMTPServer;
	            portal.SMTPUserName = existingPortal.SMTPUserName;
                portal.WeightUnit = existingPortal.WeightUnit;
                portal.DimensionUnit = existingPortal.DimensionUnit;
                portal.CurrencyTypeID = existingPortal.CurrencyTypeID;
                portal.DefaultOrderStateID = (int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.PENDING_APPROVAL;
                portal.DefaultProductReviewStateID = (int)ZNodeProductReviewState.PendingApproval;

                portal.MaxCatalogCategoryDisplayThumbnails = existingPortal.MaxCatalogCategoryDisplayThumbnails;
                portal.MaxCatalogDisplayColumns = existingPortal.MaxCatalogDisplayColumns;
                portal.MaxCatalogDisplayItems = existingPortal.MaxCatalogDisplayItems;
                portal.MaxCatalogItemCrossSellWidth = existingPortal.MaxCatalogItemCrossSellWidth;
                portal.MaxCatalogItemLargeWidth = existingPortal.MaxCatalogItemLargeWidth;
                portal.MaxCatalogItemMediumWidth = existingPortal.MaxCatalogItemMediumWidth;
                portal.MaxCatalogItemSmallThumbnailWidth = existingPortal.MaxCatalogItemSmallThumbnailWidth;
                portal.MaxCatalogItemSmallWidth = existingPortal.MaxCatalogItemSmallWidth;
                portal.MaxCatalogItemThumbnailWidth = existingPortal.MaxCatalogItemThumbnailWidth;
                portal.ImageNotAvailablePath = existingPortal.ImageNotAvailablePath;

                portal.ShopByPriceMin = 5;
                portal.ShopByPriceMax = 500;
                portal.ShopByPriceIncrement = 20;

                // Set logo path
                if (RegisterUploadImage.PostedFile.FileName == string.Empty)
                {
                    portal.LogoPath = string.Empty;
                }

                bool check = false;
                check = storeAdmin.InsertStore(portal);

                if (check)
                {
                    // Update LogoPath based on PortalId
                    Portal store = null;
                    store = storeAdmin.GetByPortalId(portal.PortalID);

                    if (RegisterUploadImage.PostedFile.FileName != string.Empty)
                    {
                        byte[] imageData = new byte[RegisterUploadImage.PostedFile.InputStream.Length];
                        RegisterUploadImage.PostedFile.InputStream.Read(imageData, 0, (int)RegisterUploadImage.PostedFile.InputStream.Length);

                        string fileName = ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + "Turnkey/" + portal.PortalID + "/" + System.IO.Path.GetFileNameWithoutExtension(RegisterUploadImage.PostedFile.FileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(RegisterUploadImage.PostedFile.FileName);
                        //string fileName = ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + System.IO.Path.GetFileNameWithoutExtension(RegisterUploadImage.PostedFile.FileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(RegisterUploadImage.PostedFile.FileName);

                        ZNodeStorageManager.WriteBinaryStorage(imageData, fileName);
                        store.LogoPath = fileName;
                        check = storeAdmin.Update(store);
                    }

                    // Portal Country Service
                    PortalCountryService portalCountryService = new PortalCountryService();
                    PortalCountry portalCountry = new PortalCountry();

                    // Add PortalCountry List
                    portalCountry.PortalID = portal.PortalID;
                    portalCountry.BillingActive = true;
                    portalCountry.ShippingActive = true;
                    portalCountry.CountryCode = "US";
                    check = portalCountryService.Insert(portalCountry);

                    // Delete the portalCatalog
                    CatalogAdmin catalogAdmin = new CatalogAdmin();
                    catalogAdmin.DeleteportalCatalog(portal.PortalID);

                    Catalog catalog = new Catalog();

                    // Create default catalog as StoreName_Catalog
                    catalog.Name = txtBillingCompanyName.Text + this.GetGlobalResourceObject("ZnodeAdminResource", "TextDefault").ToString();
                    catalog.PortalID = portal.PortalID;
                    catalog.IsActive = true;
                    catalogAdmin.Insert(catalog);

                    PortalCatalog portalCatalog = new PortalCatalog();

                    // Add the new Selection
                    portalCatalog.PortalID = portal.PortalID;
                    portalCatalog.CatalogID = catalog.CatalogID;

                    // default set NULL to new store
                    portalCatalog.ThemeID = int.Parse(ddlTheme.SelectedValue);
                    var cssList = DataRepository.CSSProvider.GetByThemeID(int.Parse(ddlTheme.SelectedValue));

                    if (cssList.Any())
                        portalCatalog.CSSID = cssList.First().CSSID; // "Default";


                    // default English language.
                    portalCatalog.LocaleID = 43;
                    catalogAdmin.AddPortalCatalog(portalCatalog);

                    // Domain Creation
                    if (!string.IsNullOrEmpty(txtStoreUrl.Text.Trim()))
                    {
                        // Domain creation.
                        domain.PortalID = portal.PortalID;
                        domain.DomainName = domainName;
                        domain.IsActive = true;
                        try
                        {
                            portal = storeAdmin.GetByPortalId(portal.PortalID);

                            status = domainAdmin.Insert(domain);

                            // Log Activity
                            string associatename = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddDomain").ToString() + domainName + " - " + portal.StoreName;
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associatename, portal.StoreName);
                        }
                        catch (Exception ex)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                            status = false;
                        }
                    }

                    // Add Default Content Pages
                    ContentPageService contentPageService = new ContentPageService();
                    ContentPage contentPage = new ContentPage();
                    ContentPageAdmin contentPageAdmin = new ContentPageAdmin();
                    ContentPageQuery filter = new ContentPageQuery();

                    // Get ContentPages list using existing portalID and LocaleId
                    filter.Append(ContentPageColumn.LocaleId, existingPortal.LocaleID.ToString());
                    filter.Append(ContentPageColumn.PortalID, existingPortal.PortalID.ToString());
                    TList<ContentPage> contentPageList = contentPageService.Find(filter.GetParameters());

                    if (contentPageList.Count > 0)
                    {
                        foreach (ContentPage page in contentPageList)
                        {
                            contentPage = page.Clone() as ContentPage;
                            contentPage.ContentPageID = -1;
                            contentPage.PortalID = portal.PortalID;
                            contentPage.LocaleId = portalCatalog.LocaleID;
                            contentPage.SEOURL = null;
                            contentPage.ThemeID = portalCatalog.ThemeID;
                            contentPageAdmin.AddPage(contentPage, string.Empty, portal.PortalID, portalCatalog.LocaleID.ToString(), HttpContext.Current.User.Identity.Name, null, false);
                        }
                    }

                    // Create Message Config
                    storeAdmin.CreateMessage(portal.PortalID.ToString(), "43");

                    // Default Tax Type
                    TaxClassService taxTypeService = new TaxClassService();
                    TaxClass taxClass = new TaxClass();
                    taxClass.Name = "Default";
                    taxClass.PortalID = portal.PortalID;
                    taxClass.DisplayOrder = 1;
                    taxClass.ActiveInd = true;
                    check = taxTypeService.Insert(taxClass);

                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Creation of Store - " + txtBillingCompanyName.Text, txtBillingCompanyName.Text);

                    // Log Activity
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeSuccess, HttpContext.Current.User.Identity.Name);

                    return portal.PortalID;
                }
                else
                {
                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCreatingAccount").ToString();

                    // Log Activity
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeFailed, HttpContext.Current.User.Identity.Name);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCreatingAccount").ToString();

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity(9002, HttpContext.Current.User.Identity.Name);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }

            return 0;
        }
        #endregion
    }
}