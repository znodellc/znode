﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace  Znode.Engine.FranchiseAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Franchise Admin ProductReviewStateAutoComplete user control class.
    /// </summary>
    public partial class ProductReviewStateAutoComplete : System.Web.UI.UserControl
    {
        #region Private Variables
        private int dropdownItemCount = int.Parse(ConfigurationManager.AppSettings["DropdownMaxItemCount"]);
        #endregion

        /// <summary>
        /// Occurs when the content of the auto complete text box changes between posts to the server.
        /// </summary>
        private event System.EventHandler SelectedIndexChangedHandler;

        #region Public Properties
        /// <summary>
        ///  Gets or sets the width of the auto complete control.
        /// </summary>
        public string Width
        {
            get { return txtProductReviewState.Width.ToString(); }
            set { txtProductReviewState.Width = new Unit(value); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to set AutoPostBack or not.
        /// </summary>
        public bool AutoPostBack
        {
            get
            {
                if (ddlProductReviewState.Visible)
                {
                    return ddlProductReviewState.AutoPostBack;
                }

                return bool.Parse(hdnAutoPostBack.Value);
            }

            set
            {
                if (ddlProductReviewState.Visible)
                {
                    ddlProductReviewState.AutoPostBack = value;
                }

                hdnAutoPostBack.Value = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the text content of the hidden field control.
        /// </summary>
        public string Value
        {
            get
            {
                if (ddlProductReviewState.Visible)
                {
                    return ddlProductReviewState.SelectedValue;
                }

                return ProductReviewStateId.Value;
            }

            set
            {
                if (ddlProductReviewState.Visible)
                {
                    ListItem item = ddlProductReviewState.Items.FindByValue(value);
                    if (item != null)
                    {
                        ddlProductReviewState.SelectedValue = value;
                    }
                    else
                    {
                        ddlProductReviewState.SelectedValue = string.Empty;
                    }
                }

                ProductReviewStateId.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the text content of the auto complete control.
        /// </summary>
        public string Text
        {
            get { return txtProductReviewState.Text; }
            set { txtProductReviewState.Text = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the auto complete control is required.
        /// </summary>
        public bool IsRequired
        {
            get
            {
                return RequiredFieldValidator2.Visible;
            }

            set
            {
                if (ddlProductReviewState.Visible)
                {
                    RequiredFieldValidator2.ControlToValidate = "ddlProductReviewState";
                }
                else
                {
                    RequiredFieldValidator2.ControlToValidate = "txtProductReviewState";
                }

                RequiredFieldValidator2.Visible = value;
            }
        }

        /// <summary>
        /// Sets the value for select index changed handler
        /// </summary>
        public System.EventHandler OnSelectedIndexChanged
        {
            set
            {
                if (ddlProductReviewState.Visible)
                {
                    ddlProductReviewState.SelectedIndexChanged += value;
                }
                else
                {
                    this.SelectedIndexChangedHandler = value;
                }
            }
        }

        /// <summary>
        /// Gets the list of product review states.
        /// </summary>
        private TList<ProductReviewState> ProductReviewStateListTable
        {
            get
            {
                ProductReviewStateService productReviewStateService = new ProductReviewStateService();
                TList<ProductReviewState> productReviewStates = productReviewStateService.GetAll();
                return productReviewStates;
            }
        }       

        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            ddlProductReviewState.Visible = false;

            if (this.ProductReviewStateListTable.Count < this.dropdownItemCount && !this.Page.IsPostBack)
            {
                ddlProductReviewState.Visible = true;
                ddlProductReviewState.AutoPostBack = bool.Parse(hdnAutoPostBack.Value);
                txtProductReviewState.Visible = false;
                autoCompleteExtender3.Enabled = false;
                ddlProductReviewState.DataSource = this.ProductReviewStateListTable;
                ddlProductReviewState.DataTextField = "ReviewStateName";
                ddlProductReviewState.DataValueField = "ReviewStateID";
                ddlProductReviewState.DataBind();

                ListItem allItem = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), string.Empty);
                ddlProductReviewState.Items.Insert(0, allItem);
                ddlProductReviewState.SelectedIndex = ddlProductReviewState.Items.IndexOf(ddlProductReviewState.Items.FindByValue(string.Empty));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder script = new StringBuilder();

            script.Append("<script language=\"JavaScript\" type=\"text/javascript\">\n");
            script.Append("<!--\n");
            script.Append("function documentOnKeyPress()\n");
            script.Append("{\n");
            script.Append(" var charCode = window.event.keyCode;\n");
            script.Append(" var elementType = window.event.srcElement.type;\n");
            script.Append("\n");
            script.Append(" if ( (charCode == 13) && (elementType == \"text\") )\n");
            script.Append("   {\n");
            script.Append("        // Cancel the keystroke completely\n");
            script.Append("        window.event.returnValue = false;\n");
            script.Append("        window.event.cancel = true;\n");
            script.Append("        // Or change it to a tab\n");
            script.Append("  //window.event.keyCode = 9;\n");
            script.Append("   }\n");
            script.Append("}\n");
            script.Append("document.onkeypress = documentOnKeyPress;\n");
            script.Append("// -->\n");
            script.Append("</script>\n");

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "OnKeyPressScript", script.ToString());
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the content of the auto complete text box changes
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AutoComplete_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.SelectedIndexChangedHandler != null && !ddlProductReviewState.Visible)
            {
                this.SelectedIndexChangedHandler(sender, e);
            }
        }
        #endregion
    }
}