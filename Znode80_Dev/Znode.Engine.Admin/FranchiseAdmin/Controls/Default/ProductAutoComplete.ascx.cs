﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace  Znode.Engine.FranchiseAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Franchise Admin ProductAutoComplete user control class.
    /// </summary>
    public partial class ProductAutoComplete : System.Web.UI.UserControl
    {
        #region Event Declaration
        private event System.EventHandler SelectedIndexChangedHandler;
        #endregion

        /// <summary>
        /// Gets or sets the control width.
        /// </summary>
        public string Width
        {
            get { return txtProduct.Width.ToString(); }
            set { txtProduct.Width = new Unit(value); }
        }

        public bool AutoPostBack
        {
            get
            {
                return bool.Parse(hdnAutoPostBack.Value);
            }

            set
            {
                hdnAutoPostBack.Value = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the selected product Id.
        /// </summary>
        public string Value
        {
            get
            {
                return ProductId.Value;
            }

            set
            {
                ProductId.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the selected product name.
        /// </summary>
        public string Text
        {
            get { return txtProduct.Text; }
            set { txtProduct.Text = value; }
        }

        public bool IsRequired
        {
            set { RequiredFieldValidator2.Visible = value; }
        }

        public string ContextKey
        {
            set { autoCompleteExtender3.ContextKey = value; }
        }

        /// <summary>
        /// Sets the selected index changed event.
        /// </summary>
        public System.EventHandler OnSelectedIndexChanged
        {
            set
            {
                this.SelectedIndexChangedHandler = value;
            }
        }

        protected void AutoComplete_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.SelectedIndexChangedHandler != null)
            {
                this.SelectedIndexChangedHandler(sender, e);
            }
        }
    }
}