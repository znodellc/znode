﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace  Znode.Engine.FranchiseAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Franchise Admin ManufacturerAutoComplete user control class.
    /// </summary>
    public partial class ManufacturerAutoComplete : System.Web.UI.UserControl
    {
        #region Properties

        private int dropdownItemCount = int.Parse(ConfigurationManager.AppSettings["DropdownMaxItemCount"]);

        /// <summary>
        /// Gets or sets the control width.
        /// </summary>
        public string Width
        {
            get
            {
                if (ddlManufacturer.Visible)
                {
                    return ddlManufacturer.Width.ToString();
                }

                return txtManufacturer.Width.ToString();
            }

            set
            {
                if (ddlManufacturer.Visible)
                {
                    ddlManufacturer.Width = new Unit(value);
                }
                else
                {
                    txtManufacturer.Width = new Unit(value);
                }
            }
        }

        /// <summary>
        /// Gets or sets the control value.
        /// </summary>
        public string Value
        {
            get
            {
                if (ddlManufacturer.Visible)
                {
                    return ddlManufacturer.SelectedValue;
                }

                return hdnManufacturerId.Value;
            }

            set
            {
                if (ddlManufacturer.Visible)
                {
                    ListItem item = ddlManufacturer.Items.FindByValue(value);
                    if (item != null)
                    {
                        ddlManufacturer.SelectedValue = value;
                    }
                    else
                    {
                        ddlManufacturer.SelectedIndex = 0;
                    }
                }
                else
                {
                    hdnManufacturerId.Value = value;
                }
            }
        }        

        /// <summary>
        /// Gets or sets the control text.
        /// </summary>
        public string Text
        {
            get { return txtManufacturer.Text; }
            set { txtManufacturer.Text = value; }
        }
        #endregion

        #region Private Properties
        private TList<Manufacturer> ManufacturerListTable
        {
            get
            {
                ManufacturerService manufacturerService = new ManufacturerService();
                ManufacturerQuery query = new ManufacturerQuery();
                query.Append(ManufacturerColumn.ActiveInd, "TRUE");

                TList<Manufacturer> manufacturerList = manufacturerService.Find(query.GetParameters());
                foreach (Manufacturer manufacture in manufacturerList)
                {
                    manufacture.Name = Server.HtmlDecode(manufacture.Name);
                }

                return manufacturerList;
            }
        } 
        #endregion             

        protected void Page_Init(object sender, EventArgs e)
        {
            ddlManufacturer.Visible = false;

            if (this.ManufacturerListTable.Count < this.dropdownItemCount && !this.Page.IsPostBack)
            {
                ddlManufacturer.Visible = true;
                txtManufacturer.Visible = false;
                ddlManufacturer.DataSource = UserStoreAccess.CheckStoreAccess(this.ManufacturerListTable);
                ddlManufacturer.DataTextField = "Name";
                ddlManufacturer.DataValueField = "ManufacturerID";
                ddlManufacturer.DataBind();

                ListItem allItem = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), string.Empty);
                ddlManufacturer.Items.Insert(0, allItem);
            }
        }
    }
}