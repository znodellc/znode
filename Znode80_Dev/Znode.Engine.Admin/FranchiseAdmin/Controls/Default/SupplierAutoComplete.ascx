﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Controls.Default.SupplierAutoComplete" Codebehind="SupplierAutoComplete.ascx.cs" %>


<script type="text/javascript">
    
    function AutoComplete_SupplierSelected(source, eventArgs) {
        var hiddenTextValue = $get("<%= SupplierId.ClientID %>");
        hiddenTextValue.value = eventArgs.get_value();


        var hAutoPostBack = $get("<%= hdnAutoPostBack.ClientID %>");
        if (hAutoPostBack.value == "True") {
            __doPostBack('AutoComplete_OnSelectedIndexChanged', hiddenTextValue, eventArgs);
        }
    }

    function AutoComplete_SupplierShowing(source, eventArgs) {
        var hiddenTextValue = $get("<%=SupplierId.ClientID %>");
        hiddenTextValue.value = "";
    }

    function Supplier_OnBlur(obj) {
        var hiddenTextValue = $get("<%=SupplierId.ClientID %>");        
        if (obj.value == "") {            
            hiddenTextValue.value = "";
        }
        if (hiddenTextValue.value == "") {
            obj.value = "";
        }        
    }
</script>

<asp:TextBox ID="txtSupplier" runat="server" onblur="Supplier_OnBlur(this)"></asp:TextBox>
<asp:HiddenField runat="server" ID="SupplierId" />
<asp:HiddenField runat="server" ID="hdnAutoPostBack" Value="false" />
<ajaxToolKit:AutoCompleteExtender ID="autoCompleteExtender3" runat="server" TargetControlID="txtSupplier"
    ServicePath="ZNodeMultifrontService.asmx" ServiceMethod="GetSuppliers" UseContextKey="true"
    MinimumPrefixLength="1" EnableCaching="false" CompletionSetCount="10" CompletionInterval="1"
    FirstRowSelected="false" OnClientItemSelected="AutoComplete_SupplierSelected"
    OnClientShowing="AutoComplete_SupplierShowing"  />
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtSupplier"
    ErrorMessage='<%$ Resources:ZnodeAdminResource, SelectProductType %>' CssClass="Error" Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
<asp:DropDownList runat="server" ID="ddlSupplier" Visible="false"></asp:DropDownList>
