﻿using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace Znode.Engine.Admin.FranchiseAdmin.Controls.Default.Search
{
	public class PersonalizationHelper
	{
		public static IEnumerable<ZNode.Libraries.DataAccess.Entities.Product> DoProductSearch(string productName, string productSku, int categoryId, int portalId)
		{
			var productHelper = new ProductHelper();
			var catalogService = new PortalCatalogService();

			var catalog = catalogService.GetByPortalID(portalId);
			var catalogId = catalog.FirstOrDefault() == null ? 0 : catalog.First().CatalogID;

			var products = productHelper.SearchProduct(productName, string.Empty, productSku, "0", "0",
			                                           categoryId.ToString(CultureInfo.InvariantCulture),
			                                           catalogId.ToString(CultureInfo.InvariantCulture));

			return
				products.Tables[0].Rows.Cast<DataRow>()
				                  .Select(
					                  row =>
					                  new ZNode.Libraries.DataAccess.Entities.Product {ProductID = int.Parse(row["ProductId"].ToString()), Name = row["Name"].ToString()})
				                  .ToList();
		}

		public static CatalogSearchResult GetCatalogCategories(int portalId)
		{
			var portalCatalogService = new PortalCatalogService();

			var storeCatalog = portalCatalogService.GetByPortalID(portalId);

			if (storeCatalog.Count == 0)
			{
				return new CatalogSearchResult {Catalog = string.Empty, Categories = new TList<Category>()};
			}

			var catalogService = new CatalogService();

			var catalog = catalogService.GetByCatalogID(storeCatalog.First().CatalogID);

			var categoryNodeService = new CategoryNodeService();

			var categoryNodes = categoryNodeService.GetByCatalogID(catalog.CatalogID);

			var categoryService = new CategoryService();
			var categories = new TList<Category>();

			foreach (var categoryNode in categoryNodes)
			{
				categories.Add(categoryService.GetByCategoryID(categoryNode.CategoryID));
			}

			return new CatalogSearchResult {Catalog = catalog.Name, Categories = categories};
		}

		public class CatalogSearchResult
		{
			public string Catalog { get; set; }
			public TList<Category> Categories { get; set; }
		}

		public static List<ListItem> GetDropdownCategories(int portalId)
		{
			var categoryService = new CategoryService();

			var categories = portalId == 0 ? categoryService.GetAll() : GetCatalogCategories(portalId).Categories;

			var orderedCategories = categories.OrderBy(category => category.Name);

			var categoryList = orderedCategories.OrderBy(category => category.Name).Select(
					category =>
					new ListItem { Text = category.Name, Value = category.CategoryID.ToString(CultureInfo.InvariantCulture) }).ToList();
			categoryList.Insert(0, new ListItem { Text = "ALL", Value = "0" });

			return categoryList;
		}

		public static void ApplyClearButtonAttributes(WebControl button, Page page)
		{
			button.Attributes.Add("onmouseover", "this.src='" + page.ResolveClientUrl("~/Themes/Images/buttons/button_clear_highlight.gif") + "'");
			button.Attributes.Add("onmouseout", "this.src='" + page.ResolveClientUrl("~/Themes/Images/buttons/button_clear.gif") + "'");
		}

		public static void ApplySearchButtonAttributes(WebControl button, Page page)
		{
			button.Attributes.Add("onmouseover", "this.src='" + page.ResolveClientUrl("~/Themes/Images/buttons/button_search_highlight.gif") + "'");
			button.Attributes.Add("onmouseout", "this.src='" + page.ResolveClientUrl("~/Themes/Images/buttons/button_search.gif") + "'");
		}
	}
}