﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FbtSelect.ascx.cs" Inherits="Znode.Engine.Admin.FranchiseAdmin.Controls.Default.Search.FbtSelect" %>
<h4 class="SubTitle">
    <asp:Localize runat="server" ID="TitleFBT" Text="<%$ Resources:ZnodeAdminResource, SubTitleFBT %>"></asp:Localize></h4>
<p>
    <asp:Localize runat="server" ID="TextFBTSearchProducts" Text="<%$ Resources:ZnodeAdminResource, SubTextFBTSearchProducts %>"></asp:Localize></p>
<hr />
<h4 class="GridTitle">
    <asp:Localize runat="server" ID="SubTitleStore" Text="<%$ Resources:ZnodeAdminResource, SubTitleStore %>"></asp:Localize></h4>
<div>
    <div style="display: inline-block;">
        <asp:DropDownList runat="server" ID="StoreList" AppendDataBoundItems="False" OnSelectedIndexChanged="StoreList_OnSelectedIndexChanged" AutoPostBack="True" Enabled="False" />
    </div>
    <div style="display: inline-block;">
        <asp:Label runat="server" ID="CatalogLabel"></asp:Label>
    </div>
    <div class="ClearAll"></div>
</div>
<hr />
<h4 class="SubTitle">
    <asp:Localize runat="server" ID="SubTitleSearchProducts" Text="<%$ Resources:ZnodeAdminResource, SubTitleSearchProducts %>"></asp:Localize></h4>
<div>
    <table>
        <tr>
            <td><span class="SearchTitle">
                <asp:Localize runat="server" ID="TitleProductName" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>"></asp:Localize></span></td>
            <td><span class="SearchTitle">
                <asp:Localize runat="server" ID="TitleSKU" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleSKU %>"></asp:Localize></span></td>
            <td><span class="SearchTitle">
                <asp:Localize runat="server" ID="TitleCategory" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleCategory %>"></asp:Localize></span></td>
        </tr>
        <tr>
            <td>
                <asp:TextBox runat="server" ID="SearchProductName" ClientIDMode="Static"></asp:TextBox></td>
            <td>
                <asp:TextBox runat="server" ID="SearchSKU" ClientIDMode="Static"></asp:TextBox></td>
            <td>
                <asp:DropDownList runat="server" ID="SearchCategory" ClientIDMode="Static" /></td>
        </tr>
    </table>
    <div class="ClearAll"></div>
    <br />
    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="SearchButton_OnClick" Text="<%$ Resources:ZnodeAdminResource, ButtonSearch %>" ID="SearchButton" />
    <zn:Button runat="server" ButtonType="CancelButton" OnClick="ClearButton_OnClick" Text="<%$ Resources:ZnodeAdminResource, ButtonClear %>" ID="ClearButton" />
</div>
<hr />
<h4 class="GridTitle">
    <asp:Localize runat="server" ID="GridTitleProductList" Text="<%$ Resources:ZnodeAdminResource, GridTitleProductList %>"></asp:Localize></h4>
<div class="FormView">
    <asp:GridView runat="server" ID="SearchResults" AutoGenerateColumns="False" OnRowDataBound="SearchResults_OnRowDataBound"
        CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText="<%$ Resources:ZnodeAdminResource, GridEmptyText %>"
        GridLines="None" Width="100%" OnPageIndexChanging="SearchResults_PageIndexChanging" AllowPaging="True">
        <Columns>
            <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProduct %>' DataField="Product" HeaderStyle-HorizontalAlign="Left">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleFBT1 %>' DataField="FBT1" HeaderStyle-HorizontalAlign="Left">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleFBT2 %>' DataField="FBT2" HeaderStyle-HorizontalAlign="Left">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:BoundField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton runat="server" ID="ManageLink" Text='<%$ Resources:ZnodeAdminResource, LinkManage %>' PostBackUrl="~/FranchiseAdmin/Secure/Marketing/Search/Personalization/ManageFbt.aspx?Manage={0}&Portal={1}&Category={2}"></asp:LinkButton>
                    <asp:LinkButton runat="server" ID="DeleteLink" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' OnClick="DeleteLink_OnClick" OnClientClick="return DeleteConfirmation();"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="RowStyle" />
        <HeaderStyle CssClass="HeaderStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
    </asp:GridView>
</div>

<script language="javascript" type="text/javascript">
    function DeleteConfirmation() {
        return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","DeleteConfirm")) %>');
    }
</script>
