﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace Znode.Engine.Admin.FranchiseAdmin.Controls.Default.Search
{
	public partial class YmalManage : System.Web.UI.UserControl
	{
		private string ReturnUrl
		{
			get
			{
				return string.Format("~/FranchiseAdmin/Secure/Marketing/Search/Personalization/Default.aspx?Mode={0}&Portal={1}&Category={2}", Enum.GetName(typeof(RelationType), RelationType.YMAL), int.Parse(Request.QueryString["Portal"]), Request.QueryString["Category"]);
			}
		}
		
		protected void Page_Load(object sender, EventArgs e)
		{
			if (SearchCategory.Items.Count > 0) return;

			PersonalizationHelper.ApplyClearButtonAttributes(ClearButton, Page);
			PersonalizationHelper.ApplySearchButtonAttributes(SearchButton, Page);

			var portalId = int.Parse(Request.QueryString["Portal"]);

			SearchCategory.DataTextField = "Text";
			SearchCategory.DataValueField = "Value";
			SearchCategory.DataSource = PersonalizationHelper.GetDropdownCategories(portalId);
			SearchCategory.DataBind();

			SearchCategory.SelectedIndex = SearchCategory.Items.IndexOf(SearchCategory.Items.FindByValue(Request.QueryString["Category"]));

			BindAssociatedProducts();

			SetListControlsDisplayState();
		}

		private void BindAssociatedProducts()
		{
			var productService = new ProductService();
			var productId = int.Parse(Request.QueryString["Manage"]);
			var product = productService.GetByProductID(productId);

            ProductName.Text = Server.HtmlDecode(product.Name);

			var crossSellService = new ProductCrossSellService();
			var existingCrossSell =
				crossSellService.Find(string.Format("ProductID = {0} AND RelationTypeID = {1}", productId, (int) RelationType.YMAL));
			var crossSellProducts = new TList<ZNode.Libraries.DataAccess.Entities.Product>();
			
			foreach (var productCrossSell in existingCrossSell)
			{
				crossSellProducts.Add(productService.GetByProductID(productCrossSell.RelatedProductId));
			}

			AssociatedProducts.DataTextField = "Text";
			AssociatedProducts.DataValueField = "Value";
			AssociatedProducts.DataSource =
				crossSellProducts.Join(existingCrossSell, product1 => product1.ProductID, sell => sell.RelatedProductId,
				                       (product1, sell) =>
				                       new {Name = Server.HtmlDecode(product1.Name), Value = product1.ProductID, DisplayOrder = sell.DisplayOrder})
				                 .OrderBy(arg => arg.DisplayOrder).Distinct()
				                 .Select(arg => new ListItem {Text = arg.Name, Value = arg.Value.ToString(CultureInfo.InvariantCulture)});
			AssociatedProducts.DataBind();

			SetListControlsDisplayState();
		}

		protected void SearchButton_OnClick(object sender, EventArgs e)
		{
			var products = PersonalizationHelper.DoProductSearch(Server.HtmlEncode(SearchProductName.Text),
			                                                     Server.HtmlEncode(SearchSKU.Text),
			                                                     int.Parse(SearchCategory.SelectedValue),
			                                                     int.Parse(Request.QueryString["Portal"]));

			var productArray = products as ZNode.Libraries.DataAccess.Entities.Product[] ?? products.ToArray();

			if (!productArray.Any())
			{
                string errorMsg = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorNoProductsFound").ToString();
				lblErrorMsg.Text = errorMsg + "' " + SearchProductName.Text + " " + SearchSKU.Text + " '";
				SearchCategory.SelectedIndex = 0;
				UnassociatedProducts.Items.Clear();
				return;
			}

			if (string.Equals(ProductName.Text, SearchProductName.Text, StringComparison.CurrentCultureIgnoreCase))
			{
                string errorMsg = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorAssociationFailed").ToString();
				lblErrorMsg.Text = errorMsg + "' " + ProductName.Text + " '";
				return;
			}

			
			lblErrorMsg.Text = String.Empty;
			var associatedProductIds = AssociatedProducts.Items.Cast<ListItem>().Select(item => int.Parse(item.Value)).ToList();
			associatedProductIds.Add(int.Parse(Request.QueryString["Manage"]));
			
			var filteredProducts = productArray.Where(product => associatedProductIds.Contains(product.ProductID));


			foreach (var product in filteredProducts)
			{
				if (string.Equals(product.Name, SearchProductName.Text, StringComparison.CurrentCultureIgnoreCase))
				{
                    string errorMsg = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorAlreadyAssociated").ToString();
					lblErrorMsg.Text =  "' " + product.Name + " '" + errorMsg ;
					return;
				}
			}

			var productItems =
				productArray.Except(filteredProducts)
				        .OrderBy(product => product.Name)
				        .Select(
					        product =>
					        new ListItem {Text = Server.HtmlDecode(product.Name), Value = product.ProductID.ToString(CultureInfo.InvariantCulture)});
		
			UnassociatedProducts.DataTextField = "Text";
			UnassociatedProducts.DataValueField = "Value";
			UnassociatedProducts.DataSource = productItems;
			UnassociatedProducts.DataBind();

			SetListControlsDisplayState();
		}

		protected void AddAssociation_OnClick(object sender, EventArgs e)
		{
			var selected = UnassociatedProducts.SelectedItem;
			lblErrorMsg.Text = string.Empty;

			if (selected == null)
			{
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorSelectProduct").ToString();
				return;
			}

            AddToList();
			UnassociatedProducts.ClearSelection();
			AssociatedProducts.ClearSelection();

			SetListControlsDisplayState();
		}

		protected void RemoveAssociation_OnClick(object sender, EventArgs e)
		{
			var selected = AssociatedProducts.SelectedItem;
			lblErrorMsg.Text = string.Empty;

			if (selected == null)
			{
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorRemoveProduct").ToString();
				return;
			}

            RemoveFromList();
			AssociatedProducts.ClearSelection();
			UnassociatedProducts.ClearSelection();

			var reorderedItems = UnassociatedProducts.Items.Cast<ListItem>().OrderBy(item => item.Text).ToArray();
			UnassociatedProducts.Items.Clear();
			UnassociatedProducts.Items.AddRange(reorderedItems);

			SetListControlsDisplayState();
		}

		protected void MoveUp_OnClick(object sender, EventArgs e)
		{
			var selected = AssociatedProducts.SelectedItem;

			if (selected == null) return;

			var index = AssociatedProducts.SelectedIndex;

			if (index == 0) return;

			AssociatedProducts.Items.Remove(selected);
			AssociatedProducts.Items.Insert(--index, selected);
		}

		protected void MoveDown_OnClick(object sender, EventArgs e)
		{
			var selected = AssociatedProducts.SelectedItem;

			if (selected == null) return;

			var index = AssociatedProducts.SelectedIndex;

			if (index == AssociatedProducts.Items.Count - 1) return;

			AssociatedProducts.Items.Remove(selected);
			AssociatedProducts.Items.Insert(++index, selected);
		}

		protected void ClearButton_OnClick(object sender, EventArgs e)
		{
			SearchCategory.SelectedIndex = 0;
			SearchProductName.Text = string.Empty;
			SearchSKU.Text = string.Empty;
			UnassociatedProducts.Items.Clear();
			lblErrorMsg.Text = String.Empty;
		}

		private void SetListControlsDisplayState()
		{
			AddAssociation.Enabled = UnassociatedProducts.Items.Count > 0;
			RemoveAssociation.Enabled = AssociatedProducts.Items.Count > 0;

			if (AssociatedProducts.Items.Count > 1)
			{
				MoveUp.Enabled = true;
				MoveDown.Enabled = true;
			}
			else
			{
				MoveUp.Enabled = false;
				MoveDown.Enabled = false;
			}
		}

		protected void btnSave_OnClick(object sender, EventArgs e)
		{
			var crossSellService = new ProductCrossSellService();

			var productId = int.Parse(Request.QueryString["Manage"]);

			var crossSellEntities =
				crossSellService.Find(string.Format("ProductID = {0} AND RelationTypeID = {1}", productId, (int) RelationType.YMAL));

			crossSellService.Delete(crossSellEntities);

			if (AssociatedProducts.Items.Count <= 0) return;

			var newEntities = new TList<ProductCrossSell>();

			var displayOrder = 1;

			foreach (ListItem item in AssociatedProducts.Items)
			{
				newEntities.Add(new ProductCrossSell{ProductId = productId, RelatedProductId = int.Parse(item.Value), RelationTypeId = (int)RelationType.YMAL, DisplayOrder = displayOrder});
				displayOrder++;
			}

			crossSellService.Insert(newEntities);

			Response.Redirect(ReturnUrl);
		}

		protected void btnCancel_OnClick(object sender, EventArgs e)
		{
			lblErrorMsg.Text = String.Empty;
			Response.Redirect(ReturnUrl);
		}

		protected void btnBack_OnClick(object sender, EventArgs e)
		{
			Response.Redirect(ReturnUrl);
		}

		protected void EnterOverride_OnClick(object sender, EventArgs e)
		{
			btnSave_OnClick(sender, (ImageClickEventArgs) e);
		}

        /// <summary>
        /// Add to List Box
        /// </summary>
        private void AddToList()
        {
            foreach (ListItem product in UnassociatedProducts.Items)
            {
                if (product.Selected)
                {
                    if (!AssociatedProducts.Items.Contains(product))
                    {
                        AssociatedProducts.Items.Add(product);
                    }
                }
            }

            foreach (ListItem product in AssociatedProducts.Items)
            {
                if (UnassociatedProducts.Items.Contains(product))
                {
                    UnassociatedProducts.Items.Remove(product);
                }
            }
        }

        /// <summary>
        /// Remove from the listBox
        /// </summary>
        private void RemoveFromList()
        {
            foreach (ListItem product in AssociatedProducts.Items)
            {
                if (product.Selected)
                {
                    if (!UnassociatedProducts.Items.Contains(product))
                    {
                        UnassociatedProducts.Items.Add(product);
                    }
                }
            }

            foreach (ListItem product in UnassociatedProducts.Items)
            {
                if (AssociatedProducts.Items.Contains(product))
                {
                    AssociatedProducts.Items.Remove(product);
                }
            }
        }
	}
}