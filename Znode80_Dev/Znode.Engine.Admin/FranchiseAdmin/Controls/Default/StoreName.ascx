<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.FranchiseAdmin.Controls.Default.StoreName" Codebehind="StoreName.ascx.cs" %>
<div class="FieldStyle">
    <asp:Localize ID="SelectStore" runat="server" Text='<%$ Resources:ZnodeAdminResource, SelectStore %>'></asp:Localize></div>
<div class="ValueStyle">
    <asp:DropDownList ID="StoreList" runat="server" OnSelectedIndexChanged="Storelist_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:CheckBoxList ID="StoreCheckList" runat="server" OnSelectedIndexChanged="Storelist_SelectedIndexChanged">
    </asp:CheckBoxList>
</div>
<asp:Panel ID="pnlLocale" runat="server">
<div class="FieldStyle">
     <asp:Localize ID="SelectLocale" runat="server" Text='<%$ Resources:ZnodeAdminResource, SelectLocale %>'></asp:Localize></div>
<div class="ValueStyle">
    <asp:DropDownList ID="StoreLocale" runat="server">
    </asp:DropDownList>
</div>
</asp:Panel>