﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace  Znode.Engine.FranchiseAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Franchise Admin ProductTypeAutoComplete user control class.
    /// </summary>
    public partial class ProductTypeAutoComplete : System.Web.UI.UserControl
    {
        private int dropdownItemCount = int.Parse(ConfigurationManager.AppSettings["DropdownMaxItemCount"]);
        private bool _IsEnabled = true;

        /// <summary>
        /// Occurs when the content of the auto complete text box changes between posts to the server.
        /// </summary>
        private event System.EventHandler SelectedIndexChangedHandler;

        #region Public Properties
        /// <summary>
        ///  Gets or sets the width of the auto complete control.
        /// </summary>
        public string Width
        {
            get
            {
                return txtProductType.Width.ToString();
            }

            set
            {
                txtProductType.Width = new Unit(value);
            }
        }
        public bool IsEnabled
        {
            get
            {
                return this._IsEnabled;
            }

            set
            {
                this._IsEnabled = value;
            }
        }
        /// <summary>
        /// Gets or sets a value indicating whether to set AutoPostBack or not.
        /// </summary>
        public bool AutoPostBack
        {
            get
            {
                if (ddlProductType.Visible)
                {
                    return ddlProductType.AutoPostBack;
                }

                return bool.Parse(hdnAutoPostBack.Value);
            }

            set
            {
                if (ddlProductType.Visible)
                {
                    ddlProductType.AutoPostBack = value;
                }

                hdnAutoPostBack.Value = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the text content of the hidden field control.
        /// </summary>
        public string Value
        {
            get
            {
                if (ddlProductType.Visible)
                {
                    return ddlProductType.SelectedValue;
                }

                return ProductTypeId.Value;
            }

            set
            {
                if (ddlProductType.Visible)
                {
                    ListItem item = ddlProductType.Items.FindByValue(value);
                    if (item != null)
                    {
                        ddlProductType.SelectedValue = value;
                    }
                    else
                    {
                        ddlProductType.SelectedIndex = 0;
                    }
                }

                ProductTypeId.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the text content of the auto complete control.
        /// </summary>
        public string Text
        {
            get { return txtProductType.Text; }
            set { txtProductType.Text = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the auto complete control is required.
        /// </summary>
        public bool IsRequired
        {
            get
            {
                return RequiredFieldValidator2.Visible;
            }

            set
            {
                if (ddlProductType.Visible)
                {
                    RequiredFieldValidator2.ControlToValidate = "ddlProductType";
                }
                else
                {
                    RequiredFieldValidator2.ControlToValidate = "txtProductType";
                }

                RequiredFieldValidator2.Visible = value;
            }
        }

        /// <summary>
        /// Sets the value for select index changed handler
        /// </summary>
        public System.EventHandler OnSelectedIndexChanged
        {
            set
            {
                if (ddlProductType.Visible)
                {
                    ddlProductType.SelectedIndexChanged += value;
                }
                else
                {
                    this.SelectedIndexChangedHandler = value;
                }
            }
        }

        /// <summary>
        /// Gets the list of product type.
        /// </summary>
        private TList<ProductType> ProductTypeListTable
        {
            get
            {
                ProductTypeService productTypeService = new ProductTypeService();
                TList<ProductType> productTypes = productTypeService.GetAll();
                productTypes.ApplyFilter(delegate(ProductType productType) { return productType.Franchisable; });
                foreach (ProductType pt in productTypes)
                {
                    if (pt.Name.Contains("_"))
                    {
                        pt.Name = pt.Name.Split(new char[] { '_' })[0];
                    }
                    
                    pt.Name = Server.HtmlDecode(pt.Name);
                }

                return productTypes;
            }
        }
        
        #endregion

        #region Page Events
        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>        
        protected void Page_Init(object sender, EventArgs e)
        {
            ddlProductType.Visible = false;

            if (this.ProductTypeListTable.Count < this.dropdownItemCount && !this.Page.IsPostBack)
            {
                ddlProductType.Visible = true;
                ddlProductType.AutoPostBack = bool.Parse(hdnAutoPostBack.Value);
                txtProductType.Visible = false;
                autoCompleteExtender3.Enabled = false;
                ddlProductType.DataSource = this.ProductTypeListTable;
                ddlProductType.DataTextField = "Name";
                ddlProductType.DataValueField = "ProductTypeID";
                ddlProductType.DataBind();
            }

            if (ddlProductType.Visible && !this.IsRequired && !this.Page.IsPostBack)
            {
                ListItem allItem = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), string.Empty);
                ddlProductType.Items.Insert(0, allItem);
                ddlProductType.SelectedIndex = 0;
            }
        }

        // <summary>
        /// Gets or sets a value indicating whether IsEnabled true or false
        /// </summary>
    
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>  
        protected void Page_Load(object sender, EventArgs e)
        {
            StringBuilder script = new StringBuilder();
            script.Append("<script language=\"JavaScript\" type=\"text/javascript\">\n");
            script.Append("<!--\n");
            script.Append("function documentOnKeyPress()\n");
            script.Append("{\n");
            script.Append(" var charCode = window.event.keyCode;\n");
            script.Append(" var elementType = window.event.srcElement.type;\n");
            script.Append("\n");
            script.Append(" if ( (charCode == 13) && (elementType == \"text\") )\n");
            script.Append("   {\n");
            script.Append("        // Cancel the keystroke completely\n");
            script.Append("        window.event.returnValue = false;\n");
            script.Append("        window.event.cancel = true;\n");
            script.Append("        // Or change it to a tab\n");
            script.Append("  //window.event.keyCode = 9;\n");
            script.Append("   }\n");
            script.Append("}\n");
            script.Append("document.onkeypress = documentOnKeyPress;\n");
            script.Append("// -->\n");
            script.Append("</script>\n");
            
            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "OnKeyPressScript", script.ToString());
            ddlProductType.Enabled = this.IsEnabled;
        }
        #endregion

        #region Events
        /// <summary>
        /// Occurs when the content of the auto complete text box changes
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AutoComplete_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.SelectedIndexChangedHandler != null && !ddlProductType.Visible)
            {
                this.SelectedIndexChangedHandler(sender, e);
            }
        }
        #endregion
    }
}