using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Franchise Admin CustomMessage user control class.
    /// </summary>
    public partial class CustomMessage : System.Web.UI.UserControl
    {
        #region Public Properties
        /// <summary>
        /// Sets the message key.
        /// </summary>
        public string MessageKey
        {
            set
            {
                string key = value;
                string msg = ZNodeCatalogManager.MessageConfig.GetMessage(key, ZNodeConfigManager.SiteConfig.PortalID, 43);

                if (msg != null)
                {
                    lblMsg.Text = msg;
                }
            }
        } 
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
        }
    }
}