﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Franchise Admin ImageUploader user control class.
    /// </summary>
    public partial class ImageUploader : System.Web.UI.UserControl
    {
        #region Private Member Variabled
        /// <summary>
        /// To store the full physical path.
        /// </summary>
        private string fullName = string.Empty;
        private string _AppendToFileName = string.Empty;

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets the Http Posted File.
        /// </summary>
        public HttpPostedFile PostedFile
        {
            get
            {
                return UploadImage.PostedFile;
            }
        }

        /// <summary>
        /// Gets or sets the Http Posted File.
        /// </summary>
        public string AppendToFileName
        {
            get
            {
                return this._AppendToFileName;
            }

            set
            {
                this._AppendToFileName = value;
            }
        }

        /// <summary>
        /// Gets or sets the posted file information
        /// </summary>
        public FileInfo FileInformation
        {
            get
            {
                if (ViewState["FileName"] != null)
                {
                    return ViewState["FileName"] as FileInfo;
                }
                else if (UploadImage.PostedFile != null && pnlSaveOption.Visible)
                {
                    return new FileInfo(txtFileName.Text + Path.GetExtension(UploadImage.PostedFile.FileName));
                }
                else if (UploadImage.PostedFile != null)
                {
                    return new FileInfo(Path.GetFileNameWithoutExtension(UploadImage.PostedFile.FileName) + Path.GetExtension(UploadImage.PostedFile.FileName));
                }
                else
                {
                    return null;
                }
            }

            set
            {
                ViewState["FileName"] = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Save Image Method
        /// </summary>
        /// <returns>Returns true if image saved otheriwse false</returns>
        public bool SaveImage()
        {
            return SaveImage(string.Empty, string.Empty);
        }

        public bool SaveImage(string userType, string portalID)
        {
            bool isSuccess = false;

            string fileName = pnlSaveOption.Visible && rdoNewFileName.Checked ? txtFileName.Text : Path.GetFileNameWithoutExtension(UploadImage.PostedFile.FileName);
            this.fullName = this.GetFullName(fileName + this._AppendToFileName + Path.GetExtension(UploadImage.PostedFile.FileName), portalID);            
            try
            {
                if (!ZNodeStorageManager.Exists(this.fullName))
                {
                    byte[] imageData = new byte[UploadImage.PostedFile.InputStream.Length];
                    UploadImage.PostedFile.InputStream.Read(imageData, 0, (int)UploadImage.PostedFile.InputStream.Length);

                    ZNodeStorageManager.WriteBinaryStorage(imageData, this.fullName);
                    isSuccess = true;
                }
                else
                {
                    // Message "Filename already exist" shown.
                    if (pnlSaveOption.Visible && this.IsPostBack)
                    {
                        if (rdoNewFileName.Checked)
                        {
                            this.fullName = this.GetFullName(txtFileName.Text + Path.GetExtension(UploadImage.PostedFile.FileName));
                        }

                        // If overwrite selected then we can use the same file name.
                        byte[] imageData = new byte[UploadImage.PostedFile.InputStream.Length];
                        UploadImage.PostedFile.InputStream.Read(imageData, 0, (int)UploadImage.PostedFile.InputStream.Length);
                        ZNodeStorageManager.WriteBinaryStorage(imageData, this.fullName);

                        isSuccess = true;
                    }
                    else
                    {
                        byte[] imageData = new byte[UploadImage.PostedFile.InputStream.Length];
                        UploadImage.PostedFile.InputStream.Read(imageData, 0, (int)UploadImage.PostedFile.InputStream.Length);
                        ZNodeStorageManager.WriteBinaryStorage(imageData, this.fullName);

                        isSuccess = true;
                    }
                }

                this.FileInformation = new FileInfo(this.fullName);
                pnlSaveOption.Visible = !isSuccess;
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
                isSuccess = false;
            }

            return isSuccess;
        }

        /// <summary>
        /// Check whether the same file name exist in original folder.
        /// </summary>
        /// <returns>Returns true if file name doesn't exist in original folder, otherwise false.</returns>    
        public bool IsFileNameValid()
        {
            bool status = false;
            if (UploadImage.PostedFile != null)
            {
                string fileName = pnlSaveOption.Visible ? txtFileName.Text : Path.GetFileNameWithoutExtension(UploadImage.PostedFile.FileName);
                if (UploadImage.PostedFile.FileName == string.Empty)
                {
                    // File not selected 
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidImage").ToString();
                    this.FileInformation = null;
                    pnlSaveOption.Visible = true;
                    UploadImage.Focus();
                }
                else
                {
                    this.fullName = this.GetFullName(fileName + this._AppendToFileName + Path.GetExtension(UploadImage.PostedFile.FileName));

                    if (ZNodeStorageManager.Exists(this.fullName))
                    {
                        // Duplicate file name found. Clear the stored view state information.
                        lblMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFileExist").ToString(), this.FileInformation.Name);
                        this.FileInformation = new FileInfo(this.fullName);
                        status = true;
                    }
                    else
                    {
                        this.FileInformation = new FileInfo(this.fullName);
                        status = true;
                    }
                }
            }

            return status;
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (UploadImage.PostedFile != null)
                {
                    UploadImage.Attributes.Add("Value", UploadImage.PostedFile.FileName);
                }

                rdoOverwrite.Attributes.Add("onClick", "setEnabled()");
                rdoNewFileName.Attributes.Add("onClick", "setEnabled()");
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Get the file and return the full physical path with filename.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>Returns the full physical path with file name.</returns>
        private string GetFullName(string fileName)
        {
            return ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + fileName;
        }
        /// <summary>
        /// Get the file and return the full physical path with filename.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>Returns the full physical path with file name.</returns>
        private string GetFullName(string fileName, string portalID)
        {
            //return ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + fileName;
            return ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + "Turnkey/" + portalID + "/" + fileName;            
        }
        #endregion
    }
}