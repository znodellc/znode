using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.FranchiseAdmin.Controls.Default
{
    /// <summary>
    /// Represents the Franchise Admin SalesDepartmentPhone user control class.
    /// </summary>
    public partial class SalesDepartmentPhone : System.Web.UI.UserControl
    {
        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            uxSalesDepartmentPhone.Text = ZNodeConfigManager.SiteConfig.SalesPhoneNumber;
        }
        #endregion
    }
}