﻿<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="Znode.Engine.FranchiseAdmin.Controls.Default.AlternateImage" Codebehind="AlternateImage.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<div>
    <uc1:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
</div>
<div>
    <zn:Button ID="btnAccept" runat="server" Text='<%$ Resources:ZnodeAdminResource, ButtonAccept %>' onclick="BtnAccept_Click" Width="125px" ButtonType="EditButton"/>
    <zn:Button ID="btnReject" runat="server" Text='<%$ Resources:ZnodeAdminResource, ButtonReject %>' onclick="BtnReject_Click" Width="125px" ButtonType="EditButton"/>
    <br />
    <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    <asp:Label ID="lblCaption" runat="server" Text="" />
</div>
<div>s
    <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
</div>
<div>
    <asp:GridView ID="GridThumb" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
        runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
        CssClass="Grid" Width="100%" GridLines="None"  PageSize="10"  
        OnRowCommand="GridThumb_RowCommand" OnRowDataBound="GridThumb_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:CheckBox ID="chkSelect" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus %>' ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img ID="imgStatus" src='<%# GetStatusImageIcon(DataBinder.Eval(Container.DataItem, "ReviewStateID"))%>' runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ProductImageId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField HeaderText="Image" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <img id="SwapImage" alt="" src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString()) %>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Name" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField HeaderText="Product Page" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <img id="Img1" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCategoryPage %>' HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <img id="Img2" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ShowOnCategoryPage").ToString()))%>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton CssClass="Button" ID="EditProductView" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' CommandArgument='<%# Eval("productimageid") %>'
                        CommandName="Edit" runat="Server" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:LinkButton ID="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CommandArgument='<%# Eval("productimageid") %>'
                        CommandName="RemoveItem" CssClass="Button" runat="server" OnClientClick="return confirm('Are you sure you want to delete this item?');" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
          <asp:Localize runat="server" ID="NoRecordFound" Text='<%$ Resources:ZnodeAdminResource, RecordNotFoundImage %>'></asp:Localize>
        </EmptyDataTemplate>
        <RowStyle CssClass="RowStyle" />
        <HeaderStyle CssClass="HeaderStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        <FooterStyle CssClass="FooterStyle" />
    </asp:GridView>
</div>
