﻿//     Copyright ©, Znode Inc, All Rights Reserved.
// </copyright>
//-----------------------------------------------------------------------

namespace  Znode.Engine.FranchiseAdmin.Controls.Default
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Web;
    using System.Web.UI;
    using System.Web.UI.WebControls;
    using Znode.Engine.Common;
    using ZNode.Libraries.Admin;
    using ZNode.Libraries.DataAccess.Entities;
    using ZNode.Libraries.DataAccess.Service;
    using ZNode.Libraries.ECommerce.Catalog;
    using ZNode.Libraries.Framework.Business;

    /// <summary>
    /// Represents the Franchise Admin AddOn Class.
    /// </summary>
    public partial class AddOn : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private int itemId;
        private string mode = string.Empty;

        /// <summary>
        /// Add Product AddOn page link.
        /// </summary>
        private string addProductAddOnLink = "~/FranchiseAdmin/Secure/Reviewer/add_addons.aspx?";

        #endregion        

        #region Helper Functions

        /// <summary>
        /// Gets the name of the Addon for this AddonId
        /// </summary>
        /// <param name="addOnId">Addon Id to get the name.</param>
        /// <returns>Returns the Addon name.</returns>
        public string GetAddOnName(object addOnId)
        {
            ProductAddOnAdmin adminAccess = new ProductAddOnAdmin();
            var addOn = adminAccess.GetByAddOnId(int.Parse(addOnId.ToString()));

            if (addOn != null)
            {
                return addOn.Name;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the title of the Addon for this AddonId
        /// </summary>
        /// <param name="addOnId">Addon Id to get the title.</param>
        /// <returns>Returns the add on title.</returns>
        public string GetAddOnTitle(object addOnId)
        {
            ProductAddOnAdmin adminAccess = new ProductAddOnAdmin();
            var addOn = adminAccess.GetByAddOnId(int.Parse(addOnId.ToString()));

            if (addOn != null)
            {
                return addOn.Title;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the AddonValues for this AddonId
        /// </summary>
        /// <param name="addOnId">Addon Id to get the value.</param>
        /// <returns>Returns the Addon value string.</returns>
        public string GetAddOnValues(object addOnId)
        {
            ProductAddOnAdmin addOnValueAdmin = new ProductAddOnAdmin();
            TList<AddOnValue> valueList = addOnValueAdmin.GetAddOnValuesByAddOnId(int.Parse(addOnId.ToString()));

            if (valueList.Count > 0)
            {
                StringBuilder addOnValueName = new StringBuilder();
                foreach (AddOnValue item in valueList.ToArray())
                {
                    addOnValueName.Append(item.Name + ", ");
                }

                addOnValueName.Remove(addOnValueName.ToString().Length - 2, 2);
                return addOnValueName.ToString();
            }

            return string.Empty;
        }
        #endregion

        #region Events for ProductAddon

        /// <summary>
        /// Add AddOn Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddNewAddOn_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.addProductAddOnLink + "&itemid=" + this.itemId);
        }
        #endregion

        #region Grid Events

        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridProductAddOns_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Header)
            {
                UserStoreAccess uss = new UserStoreAccess();
				if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer)
                {
                    e.Row.Cells[3].Visible = true;
                    e.Row.Cells[4].Visible = false;
                }
                else
                {
                    e.Row.Cells[3].Visible = false;
                    e.Row.Cells[4].Visible = true;
                }
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Seventh column.
                LinkButton deleteButton = (LinkButton)e.Row.Cells[2].FindControl("btnDelete");

                // Set the Button's CommandArgument property with the row's index.
                deleteButton.CommandArgument = e.Row.RowIndex.ToString();

                // Add Client Side confirmation
                deleteButton.OnClientClick = "return confirm('" + this.GetGlobalResourceObject("ZnodeAdminResource", "ConfirmDelete").ToString() + "');";
            }
        }   

        /// <summary>
        /// Product Add On Row command event - occurs when delete button is fired.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridProductAddOns_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate cell in the GridView control.
                GridViewRow selectedRow = uxGridProductAddOns.Rows[index];

                TableCell idCell = selectedRow.Cells[0];
                string addOnId = idCell.Text;
                if (e.CommandName == "Remove")
                {
                    ProductAddOnAdmin adminAccess = new ProductAddOnAdmin();
                    ProductAdmin prodAdmin = new ProductAdmin();
                    Product product = prodAdmin.GetByProductId(this.itemId);

                    ProductAddOnService productaddonService = new ProductAddOnService();
                    ProductAddOn addon = new ProductAddOn();
                    addon = productaddonService.GetByProductAddOnID(int.Parse(addOnId));
                    productaddonService.DeepLoad(addon);
                    string addonname = addon.AddOnIDSource.Name;

                    string associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddOn").ToString(), addonname, product.Name);

                    if (adminAccess.DeleteProductAddOn(int.Parse(addOnId)))
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, product.Name);
                    }

                    this.BindProductAddons();
                }
            }
        }

        /// <summary>
        /// Product AddOn grid Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGridProductAddOns_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGridProductAddOns.PageIndex = e.NewPageIndex;
            this.BindProductAddons();
        }

        #endregion

        #region PageLoad
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get mode value from querystring        
            if (Request.Params["mode"] != null)
            {
                this.mode = Request.Params["mode"];
            }

            // Get itemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.itemId = 0;
            }

            if (this.itemId > 0)
            {
                this.BindProductAddons();
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotFoundProductRequested").ToString());
            }
        }
        #endregion             

        #region Bind Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindProductAddons()
        {
            ProductAddOnAdmin prodAddonAdminAccess = new ProductAddOnAdmin();

            // Bind Associated Addons for this product
            uxGridProductAddOns.DataSource = prodAddonAdminAccess.GetByProductId(this.itemId);
            uxGridProductAddOns.DataBind();

            // Hide the Add button from Reviewer.
            UserStoreAccess uss = new UserStoreAccess();
			if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer)
            {
                btnAddNewAddOn.Visible = false;
            }
        }
        #endregion                          
    }
}
