﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/FranchiseAdmin/Themes/Standard/login.master" CodeBehind="ResetPassword.aspx.cs" Inherits="Znode.Engine.Common.FranchiseAdmin.ResetPassword" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="znode" Namespace="Znode.Engine.Common.CustomClasses.WebControls" Assembly="Znode.Engine.Common" %>
<%@ Register TagPrefix ="ZNode" TagName="ResetPassword" Src="~/FranchiseAdmin/Controls/Default/ResetPassword.ascx" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Login ResetPwdLogin"> 
        <div>
            <ZNode:ResetPassword runat="server"></ZNode:ResetPassword>
        </div>
    </div>
</asp:Content>
