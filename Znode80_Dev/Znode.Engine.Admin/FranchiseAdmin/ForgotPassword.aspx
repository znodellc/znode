﻿<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/login.master" AutoEventWireup="true" CodeBehind="ForgotPassword.aspx.cs" Inherits="Znode.Engine.Common.FranchiseAdmin.ForgotPassword" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="forgotpassword" Src="~/FranchiseAdmin/Controls/Default/ForgotPassword.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Login">
        <div>
            <ZNode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="10" runat="server" />
        </div>
        <div>
            <ZNode:forgotpassword runat="server"></ZNode:forgotpassword>
        </div>
    </div>
</asp:Content>
