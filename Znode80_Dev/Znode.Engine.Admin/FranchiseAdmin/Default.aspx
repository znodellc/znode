<%@ Page Language="C#" MasterPageFile="~/FranchiseAdmin/Themes/Standard/Login.master" AutoEventWireup="true" Inherits="Znode.Engine.FranchiseAdmin.Admin_Default" Title="Site Administration" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/FranchiseAdmin/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Login">
        <h1>
            <asp:Localize ID="TitleFranchise" Text='<%$ Resources:ZnodeAdminResource, TitleFranchiseLogin%>' runat="server"></asp:Localize></h1>
        <p>
            <asp:Localize runat="server" ID="TextLoginFranchise" Text='<%$ Resources:ZnodeAdminResource, TextLoginFranchise%>'></asp:Localize>
        </p>
        <div>
            <ZNode:Spacer ID="Spacer2" SpacerHeight="20" SpacerWidth="10" runat="server" />
        </div>
        <asp:Panel ID="LoginPanel" runat="server" DefaultButton="LoginButton">
            <div class="AccessDenied">
                <asp:Label ID="lblaccess" runat="server" Visible="False" Text='<%$ Resources:ZnodeAdminResource, ErrorAccessDenied%>' CssClass="Error"></asp:Label></div>
            <div class="FormView">
                <div class="FieldStyle" style="width: 100px;">
                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName"><b>
                        <asp:Localize runat="server" ID="ColumnTitleUsername" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleUsername%>'></asp:Localize></b></asp:Label>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="UserName" autocomplete="off" runat="server" Width="140px"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredLoginUserName%>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredLoginUserName%>' ValidationGroup="uxLogin"
                        Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
                <div class="FieldStyle" style="width: 100px;">
                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password"><b>
                        <asp:Localize runat="server" ID="ColumnPassword" Text='<%$ Resources:ZnodeAdminResource, ColumnPassword%>'></asp:Localize></b></asp:Label>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="Password" autocomplete="off" runat="server" TextMode="Password" Width="140px"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredPassword%>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredPassword%>' ValidationGroup="uxLogin"
                        Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
                <div class="ClearBoth">
                </div>
                <div class="Error">
                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                </div>
                <div>
                    <ZNode:Spacer ID="Spacer4" SpacerHeight="5" SpacerWidth="10" runat="server" />
                </div>
                <div style="padding-left: 105px; height: 20px;">
                    <div>
                        <zn:LinkButton ID="LoginButton" runat="server" CommandName="Login"
                            OnClick="LoginButton_Click" ButtonType="LoginButton" ValidationGroup="uxLogin"
                            ButtonPriority="Normal" Text='<%$ Resources:ZnodeAdminResource, ButtonLogin%>' />
                    </div>
                    <div>
                        <div>
                            <div>
                                <ZNode:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="10" runat="server" />
                            </div>
                            <div>
                                <a id="forgotPasswordLink" href='' runat="server" class="forgotpassword">
                                    <asp:Localize runat="server" ID="LinkTextPassword" Text='<%$ Resources:ZnodeAdminResource, LinkTextForgotPassword%>'></asp:Localize></a>
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer3" SpacerHeight="15" SpacerWidth="10" runat="server" />
                            </div>
                            <div>
                                <a id="SignupLink" href='' runat="server" class="forgotpassword">
                                    <asp:Localize runat="server" ID="LinkTextRequest" Text='<%$ Resources:ZnodeAdminResource, LinkTextRequestFranchise%>'></asp:Localize></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
