using System;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin
{
    public partial class ContinueTrial : System.Web.UI.Page
    {
        public string DaysRemaining = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

                ZNodeLicenseManager lm = new ZNodeLicenseManager();
                ZNodeLicenseType lt;
                lt = lm.Validate();

                DaysRemaining = lm.DemoDaysRemaining().ToString() + this.GetGlobalResourceObject("ZnodeAdminResource", "TitleTrialDaysRemaining");

            }
        }


    }
}