<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/activate.master"
    AutoEventWireup="true" Inherits="Znode.Engine.Admin.Admin_Activate" Title="Activate your Znode License"
    CodeBehind="default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <asp:ScriptManager ID="scriptmanager1" runat="server">
    </asp:ScriptManager>
    <div class="License">
        <!-- License Activation Intro panel -->
        <asp:Panel ID="pnlIntro" runat="server" Visible="true">
            <h1>
                <asp:Localize ID="TitleActivate" Text='<%$ Resources:ZnodeAdminResource, TitleActivateyourLicense %>' runat="server"></asp:Localize></h1>
            <div>
                <img src="~/Themes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" /></div>
          <asp:Localize ID="TextActivate" Text='<%$ Resources:ZnodeAdminResource, TextActivateLicense %>' runat="server"></asp:Localize>
            <div style="margin-bottom: 20px; margin-top: 20px;">
             <asp:Localize ID="TextFreeTrial" Text='<%$ Resources:ZnodeAdminResource, TextFreeTrial %>' runat="server"></asp:Localize>
            </div>
            <div style="margin-bottom: 20px;">
                 <asp:Localize ID="TextPurchasedLicense" Text='<%$ Resources:ZnodeAdminResource, TextPurchasedLicense %>' runat="server"></asp:Localize>
            </div>
            <div>
                <img src="~/Themes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" />&nbsp;</div>
            <div>
                <asp:CheckBox ID="chkIntro" runat="server" Text='' OnCheckedChanged="chkIntro_CheckedChanged" /></div>
            <div>
                <img src="~/Themes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" /></div>
            <div>
                <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error"></asp:Label></div>
            <div>
                <img src="~/Themes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" /></div>
            <div>
                <asp:Button ID="btnProceedToActivation" runat="server" Text='<%$ Resources:ZnodeAdminResource, ButtonActivation %>' 
                    CausesValidation="true" OnClick="btnProceedToActivation_Click" /></div>
            <div>
                <img src="~/Themes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" /></div>
        </asp:Panel>
        <!-- ACTIVATE LICENSE -->
        <asp:Panel ID="pnlLicenseActivate" runat="server" Visible="false">
            <h1>
               <asp:Localize ID="TitleLicenseActivation" Text='<%$ Resources:ZnodeAdminResource, TitleActivateyourLicense %>' runat="server"></asp:Localize></h1>
            <div>
                <img src="~/Themes/Images/clear.gif" width="1" runat="server" height="10"
                    alt="" /></div>
            <b><asp:Localize ID="TextPleaseNote" Text='<%$ Resources:ZnodeAdminResource, TextPleaseNote %>' runat="server"></asp:Localize></b>
            <ul>
                <li><asp:Localize ID="TextPleaseNoteToDo" Text='<%$ Resources:ZnodeAdminResource, TextPleaseNoteToDo %>' runat="server"></asp:Localize></li>
                <li><asp:Localize ID="TextNoteOneServer" Text='<%$ Resources:ZnodeAdminResource, TextNoteOneServer %>' runat="server"></asp:Localize></li>
                <li><asp:Localize ID="TextNote30DayTrial" Text='<%$ Resources:ZnodeAdminResource, TextNote30DayTrial %>' runat="server"></asp:Localize></li>
                <li><asp:Localize ID="TextReadWriteTrial" Text='<%$ Resources:ZnodeAdminResource, TextReadWriteTrial %>' runat="server"></asp:Localize><b>
                    <asp:Label ID="lblLicensePath" Text="Data" runat="server"></asp:Label></b> 
                   <asp:Localize ID="TextBeforeActivating" Text='<%$ Resources:ZnodeAdminResource, TextBeforeActivating %>' runat="server"></asp:Localize></li>
            </ul>
            <div>
                <img src="~/Themes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" /></div>
            <div>
                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></div>
            <div>
                <img src="~/Themes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" /></div>
            <div class="ClearBoth">
            </div>
            <div class="FormView">
                <div class="FieldStyle">
                     <asp:Localize ID="SelectLicense" Text='<%$ Resources:ZnodeAdminResource, TextSelectLicense %>' runat="server"></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:RadioButton ID="chkMarketPlace" GroupName="lic" Checked="false" Text='<%$ Resources:ZnodeAdminResource, RadioTextMarketplace %>'
                        runat="server" OnCheckedChanged="chkFreeTrial_CheckedChanged" AutoPostBack="true"
                        Visible="false" /><br />
                    <asp:RadioButton ID="chkSerLicense" GroupName="lic" Checked="false" Text='<%$ Resources:ZnodeAdminResource, RadioTextMultiFront %>'
                        runat="server" OnCheckedChanged="chkFreeTrial_CheckedChanged" AutoPostBack="true"
                        Visible="false" /><br />
                         <asp:RadioButton ID="chkServices" GroupName="lic" Checked="false" Text='<%$ Resources:ZnodeAdminResource, RadioTextStoreMultiplier %>'
                        runat="server" OnCheckedChanged="chkFreeTrial_CheckedChanged" AutoPostBack="true"
                        Visible="false" /><br />
                    <asp:RadioButton ID="chkSingleStoreLicense" GroupName="lic" Checked="false" Text='<%$ Resources:ZnodeAdminResource, RadioTextMultiFrontSingleStoreEdition %>'
                        runat="server" OnCheckedChanged="chkFreeTrial_CheckedChanged" AutoPostBack="true"
                        Visible="false" />
                    <asp:RadioButton ID="chkFreeTrial" GroupName="lic" Checked="false" Text='<%$ Resources:ZnodeAdminResource, RadioText30DayFreeTrial %>'
                        runat="server" OnCheckedChanged="chkFreeTrial_CheckedChanged" AutoPostBack="true" />
                </div>
                <asp:Panel ID="pnlSerial" runat="server" Visible="false">
                    <div class="FieldStyle">
                        <asp:Localize ID="SerialNumber" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSerialNumber %>' runat="server"></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtSerialNumber" runat="server" Width="300" CausesValidation="true"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="SerialReqd" runat="server" ControlToValidate="txtSerialNumber"
                            ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSerialNumber %>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredSerialNumber %>'
                            Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                    </div>
                </asp:Panel>
                <div class="FieldStyle">
                   <asp:Localize ID="FullName" Text='<%$ Resources:ZnodeAdminResource, ColumnFullName %>' runat="server"></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtName" runat="server" Width="200" CausesValidation="true"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredFullName %>' Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                </div>
                <div class="FieldStyle">
                     <asp:Localize ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, ColumnEmailAddress %>' runat="server"></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtEmail" runat="server" Width="200" CausesValidation="true"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmail"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEmail %>'  Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEmail"
                        CssClass="Error" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidEmailIdAddress %>' ToolTip='<%$ Resources:ZnodeAdminResource, ValidEmailIdAddress %>'
                        ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="Dynamic"></asp:RegularExpressionValidator>
                </div>                
                <div class="ValueStyle">
                    <textarea runat="server" id="txtEULA" style="width: 760px; height: 300px;"></textarea>
                </div>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="FieldStyle">
                <asp:CheckBox ID="chkEULA" runat="server" Text='<%$ Resources:ZnodeAdminResource, CheckBoxLicenseAgreement %>' />
            </div>
            <br />
            <br />
            <div class="FieldStyle">
                <asp:Button ID="btnActivateLicense" runat="server" Text='<%$ Resources:ZnodeAdminResource, ButtonActivateLicense %>'
                    CausesValidation="true" OnClick="btnActivateLicense_Click" />
            </div>
            <div>
                <img src="~/Themes/Images/clear.gif" runat="server" width="1" height="10"
                    alt="" />
            </div>
        </asp:Panel>
        <!-- CONFIRM -->
        <asp:Panel ID="pnlConfirm" runat="server" Visible="false">
            <h1>
              <asp:Localize ID="Confirmation" Text='<%$ Resources:ZnodeAdminResource, TextConfirmation %>' runat="server"></asp:Localize></h1>
            <p>
                <asp:Label ID="lblConfirm" runat="server"></asp:Label></p>
            <div>
                <img id="Img1" src="~/Themes/Images/clear.gif" runat="server" width="1" height="20"
                    alt="" /></div>
            <a id="A1" href="~/" runat="server"> <asp:Localize ID="TextGoToStore" Text='<%$ Resources:ZnodeAdminResource, LinkTextGoToStore %>' runat="server"></asp:Localize></a>
            <div>
                <img id="Img2" src="~/Themes/Images/clear.gif" runat="server" width="1" height="20"
                    alt="" /></div>
        </asp:Panel>
        <asp:Panel ID="pnlSingleStore" runat="server" Style="display: none;" CssClass="LicenseConfirmationPopupStyle">
            <div>
                <h4 class="SubTitle">
                   <asp:Localize ID="DomainConfirmation" Text='<%$ Resources:ZnodeAdminResource, TitleDomainConfirmation %>' runat="server"></asp:Localize></h4>
            </div>
            <div class="ConfirmationText">
                <asp:Label ID="lblSingleFrontActivationMsg" runat="server"></asp:Label>
            </div>
            <div>
             <%--   <asp:Button ID="btnActivateSingleFront" CssClass="Size75" runat="server" Text="Confirm"
                    OnClick="btnActivateSingleFront_click" />&nbsp;&nbsp;
                <asp:ImageButton ID="btnCancelConfirmation" ImageUrl="~/Themes/images/buttons/button_cancel_highlight.gif"
                    runat="server" />--%>

                <zn:Button runat="server" ButtonType="SubmitButton" CssClass="Size75" OnClick="btnActivateSingleFront_click" Text='<%$ Resources:ZnodeAdminResource, ButtonConfirm%>' CausesValidation="true" ID="btnActivateSingleFront"  />
            <zn:Button runat="server" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelConfirmation" />
            </div>
        </asp:Panel>
    </div>
    <div class="ClearBoth">
    </div>
    <div>
        <asp:Button ID="btnhidden" runat="server" Style="display: none;" />
        <ajaxToolKit:ModalPopupExtender ID="mdlPopup" CancelControlID="btnCancelConfirmation"
            runat="server" TargetControlID="btnhidden" PopupControlID="pnlSingleStore" BackgroundCssClass="modalBackground" />
    </div>
</asp:Content>
