using System;
using System.Web;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Common;

namespace Znode.Engine.Admin
{
    public partial class Admin_Activate : System.Web.UI.Page
    {
        # region Private Member Variables
        protected string DomainName = "";
        private string customerIPaddress = HttpContext.Current.Request.UserHostAddress;
        # endregion

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Check the EnableDiagnostics value in the config to allow user to run the diagnostics tool
            if (System.Configuration.ConfigurationManager.AppSettings["EnableActivationPage"].ToString() == "0")
            {
                throw (new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPageDisabled").ToString()));
            }
            else
            {
                // Set Domain name
                DomainName = Request.Url.Host + Request.ApplicationPath;

                ZNodeLicenseManager lm = new ZNodeLicenseManager();
                lblLicensePath.Text = lm.GetLicensePath();

                if (!Page.IsPostBack)
                {
                    CheckLicense();

                    chkFreeTrial.Visible = true;
                    chkFreeTrial.Checked = true;
                    pnlSerial.Visible = false;

                    txtEULA.InnerHtml = ZNodeEULA.GetEULA();
                    chkIntro.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "CheckBoxTextActivation").ToString();

                    // Log the Details
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.ActivationPage, customerIPaddress);
                }

                // set visible option based on config settings
                if (UserStoreAccess.IsMultiStoreAdminEnabled())
                {
                    chkSerLicense.Visible = true;
                    chkMarketPlace.Visible = true;
                    chkSingleStoreLicense.Visible = false;
                    chkServices.Visible = true;
                }
                else
                {
                    chkSingleStoreLicense.Visible = true;
                    chkSerLicense.Visible = false;
                    chkMarketPlace.Visible = false;
                    chkServices.Visible = false;
                }
            }
        }

        /// <summary>
        /// Initialize display
        /// </summary>
        private void InitDisplay()
        {
        }

        /// <summary>
        /// Activate button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActivateLicense_Click(object sender, EventArgs e)
        {
            //check if user agreed to EULA
            if (!chkEULA.Checked)
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorActivation").ToString();
                return;
            }

            //get the target license requested
            ZNodeLicenseType lt = ZNodeLicenseType.Trial; //default

            if (chkFreeTrial.Checked)
            {
                lt = ZNodeLicenseType.Trial;

                ActivateLicense(lt);
            }
            else if (chkSerLicense.Checked)
            {
                // Multifront
                lt = ZNodeLicenseType.Multifront;

                ActivateLicense(lt);
            }
            else if (chkMarketPlace.Checked)
            {
                // MarketPlace
                lt = ZNodeLicenseType.Marketplace;

                ActivateLicense(lt);
            }
            else if (chkSingleStoreLicense.Checked)
            {
                
                lblSingleFrontActivationMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextSinglefrontActivation").ToString(), DomainName);

                pnlSingleStore.Visible = true;

                mdlPopup.Show();

                // Single Store
                lt = ZNodeLicenseType.Enterprise;
            }
            else if (chkServices.Checked)
            {
                lt = ZNodeLicenseType.Services;
                ActivateLicense(lt);
            }
        }

        /// <summary>
        /// Activate button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnActivateSingleFront_click(object sender, EventArgs e)
        {
            //get the target license requested
            ZNodeLicenseType lt = ZNodeLicenseType.Trial; //default

            if (chkSingleStoreLicense.Checked)
            {
                // Single Store
                lt = ZNodeLicenseType.Enterprise;
            }

            ActivateLicense(lt);
        }

        /// <summary>
        /// Activate the license
        /// </summary>
        /// <param name="lt"></param>
        protected void ActivateLicense(ZNodeLicenseType lt)
        {
            //install license
            ZNodeLicenseManager lm = new ZNodeLicenseManager();
            bool retval = false;
            string ErrorMessage = "";
            retval = lm.InstallLicense(lt, txtSerialNumber.Text.Trim(), txtName.Text, txtEmail.Text, out ErrorMessage);
           
            //if success
            if (retval)
            {
                lblConfirm.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextLicenseActivate").ToString();
                pnlConfirm.Visible = true;
                pnlLicenseActivate.Visible = false;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.ActivationSuccess, customerIPaddress, DomainName, txtName.Text, txtEmail.Text, lblConfirm.Text);
            }
            //install failed
            else
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextLicensefailed").ToString();
                lblError.Text = lblError.Text + ErrorMessage;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.ActivationFailed, customerIPaddress, DomainName, txtName.Text, txtEmail.Text, lblError.Text);
            }
        }

        /// <summary>
        /// License option changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkFreeTrial_CheckedChanged(object sender, EventArgs e)
        {

            if (chkFreeTrial.Visible)
            {
                if (chkFreeTrial.Checked)
                {
                    pnlSerial.Visible = false;
                }
                else
                {
                    pnlSerial.Visible = true;
                }
            }
            else
            {
                pnlSerial.Visible = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnProceedToActivation_Click(object sender, EventArgs e)
        {
            // Check if user agreed to proceed with activation process
            if (!chkIntro.Checked)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProceedActivation").ToString();
                return;
            }

            pnlLicenseActivate.Visible = true;
            pnlIntro.Visible = false;
        }

        # region Helper Methods
        /// <summary>
        /// 
        /// </summary>
        protected void CheckLicense()
        {
            // Get the target license requested
            ZNodeLicenseType lt = ZNodeLicenseType.Invalid;

            //Create Instance for License Manager
            ZNodeLicenseManager LicenseManager = new ZNodeLicenseManager();

            // Remove the license from the cache so that we will be sure to re-check it.
            LicenseManager.FlushLicenses();

            lt = LicenseManager.Validate();

        }
        #endregion
        protected void chkIntro_CheckedChanged(object sender, EventArgs e)
        {
            btnActivateLicense.Enabled = true;
            btnProceedToActivation.Enabled = true;

            if (!chkIntro.Checked)
            {
                btnProceedToActivation.Enabled = false;
            }
        }
    }
}