<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="true" Inherits="Znode.Engine.Admin.ContinueTrial" Title="Znode Trial" Codebehind="continuetrial.aspx.cs" %>
<%@ Register Src="~/Controls/Default/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" Runat="Server">
<div class="License">
    <h1>  <asp:Localize ID="TitleZnodeTrial" Text='<%$ Resources:ZnodeAdminResource, TitleZnodeTrial %>' runat="server"></asp:Localize> <%=DaysRemaining %></h1>
    <div><img src="~/Themes/Images/clear.gif" runat="server" width="1" height="20" alt=""/></div>  
        
    <div class="Status" style=" margin-bottom: 30px;">
       <asp:Localize ID="ZnodeTrialMode" Text='<%$ Resources:ZnodeAdminResource, TextZnodeTrialMode %>' runat="server"></asp:Localize>
    </div>
        
      
    <div class="ActionLink" style="margin-left:50px; margin-bottom:20px;"><b><img id="Img3" alt="" src="~/Themes/Images/400-right.gif" runat="server" border="0" align="absmiddle" runat="server" />  <a id="A2" href="~/" runat="server"> <asp:Localize ID="ContinuewithTrial" Text='<%$ Resources:ZnodeAdminResource, TextContinuewithTrial %>' runat="server"></asp:Localize></a></b></div> 
        
        
    <div class="ActionLink" style="margin-left:50px; margin-bottom:20px;"><b><img id="Img4" alt=""  src="~/Themes/Images/400-right.gif" runat="server" border="0" align="absmiddle" runat="server" />  <a id="A4" href="~/" runat="server"> <asp:Localize ID="GotostoreAdmin" Text='<%$ Resources:ZnodeAdminResource, TextGotostoreAdmin %>' runat="server"></asp:Localize></a></b></div> 
        
      
    <div class="ActionLink" style="margin-left:50px; margin-bottom:100px;"><b><img id="Img5" alt="" src="~/Themes/Images/400-right.gif" runat="server" border="0" align="absmiddle" runat="server" />  <a id="A1" href="~/activate/default.aspx" runat="server"> <asp:Localize ID="ActivateyourLicense" Text='<%$ Resources:ZnodeAdminResource, TextActivateyourLicense %>' runat="server"></asp:Localize></a></b></div> 
        

    <div> <asp:Localize ID="IssuesOrQuestion" Text='<%$ Resources:ZnodeAdminResource, TextIssuesOrQuestion %>' runat="server"></asp:Localize> <a id="A3" href="http://www.znode.com/buy" target="_blank"><asp:Localize ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, TextPurchaseLicense %>' runat="server"></asp:Localize> </a></div>
</div> 
</asp:Content>

