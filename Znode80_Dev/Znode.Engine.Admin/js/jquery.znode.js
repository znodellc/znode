﻿

// Startup bindings
$j = jQuery.noConflict();
$j(document).ready(function () {
    var responsedata = null;
    var submitForm = false;
    // don't navigate away from the field when pressing tab on a selected item
    $j("#SearchText").keydown(function (event) {
        if (event.keyCode === $j.ui.keyCode.TAB && $j(this).data("uiAutocomplete").menu.active) {

          //  event.preventDefault();
          //  return false;
        }
        if (event.keyCode === $j.ui.keyCode.ENTER && $j(this).data("uiAutocomplete").menu.active) {

            // submitForm = true;
        }
    });

    $j("#SearchText").autocomplete({

        source: function (request, response) {
            $j.ajax({
                type: 'POST',
                url: searchURL,
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                data: '{ term:"' + $j("#SearchText").val() + '",category: "' + $j("#ddlCategory option:selected").val() + '"}',
                success: function (data) {
                    var parsed = $j.parseJSON(data.d);
                    if ($j("#ddlCategory option:selected").val() == "" && $j('#hdneditMode').val() == "false") {

                        $j('#hdneditMode').val("true");
                    }
                    
                    response(parsed);
                },
                error: function (XMLHttpRequest, textStatus, errorThrown) {
                    var errorMessage = "Ajax error: " + this.url + " : " + textStatus + " : " + errorThrown + " : " + XMLHttpRequest.statusText + " : " + XMLHttpRequest.status;

                    if (XMLHttpRequest.status != "0" || errorThrown != "abort") {
                        //  alert(errorMessage);
                    }
                }

            });
        },
        minLength: 2,
        delay: 500,
        focus: function (event, ui) {
            // prevent value inserted on focus
           
            $j("#SearchText").val(ui.item.name);
            if (ui.item.value != "") {
                if ($j("#ddlCategory option[value='" + ui.item.value + "'']").length <= 0) {
           
                    $j("#ddlCategory").prepend("<option value='" + ui.item.value + "'>" + ui.item.value + "</option>");
                }
                $j('#ddlCategory').selectbox('change', ui.item.value, ui.item.value);

            } else {
                if ($j('#hdneditMode').val() == "true") {
                    $j('#ddlCategory').selectbox('change', "", "All Departments");
                }
            }

            return false;
        },
        select: function (event, ui) {
            // prevent value inserted on focus
            $j("#SearchText").val(ui.item.name);
            if (ui.item.value != "") {

                if ($j("#ddlCategory option[value='" + ui.item.value + "'']").length <= 0) {

                    $j("#ddlCategory").prepend("<option value='" + ui.item.value + "'>" + ui.item.value + "</option>");
                }
                $j('#ddlCategory').selectbox('change', ui.item.value, ui.item.value);

            }
            else 
            {
                if ($j('#hdneditMode').val() == "true") {
                    $j('#ddlCategory').selectbox('change', "", "All Departments");
                }
            }

            if (event.keyCode != $j.ui.keyCode.TAB) {

                $j("#ctl00_ctl00_TopSearch_btnSearch").click();
            }
         

            event.preventDefault();
        },
    });

    $j(".SubmitJSAction").click(function () {
        $j(this).closest("form").validate();
        if ($j(this).closest("form").valid()) {
            $j(this).closest("form").submit();
        }
    });

    if (typeof (PageReadyEvent) != "undefined")
        PageReadyEvent();

    $j("select#SelectedItem").change(function (evt) {
        $j("form#localeForm").submit();
    });

    BindSearchCategory();
});


function BindSearchCategory() {
    $j("#ddlCategory").selectbox();
}