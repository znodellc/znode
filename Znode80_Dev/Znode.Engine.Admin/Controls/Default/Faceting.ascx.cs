﻿using System;
using System.Text;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace  Znode.Engine.Admin.Controls.Default
{
    /// <summary>
	/// Represents the Faceting user control class
    /// </summary>
	public partial class Faceting : System.Web.UI.UserControl
    {
        #region Private Member Variables

        private int ItemId = 0;
        private int SkuId = 0;
        private string _ItemType = string.Empty;
        private string AssociateName = string.Empty;

        /// <summary>
        /// Gets or sets the Item Type
        /// </summary>
        public string ItemType
        {
            get
            {
                return this._ItemType;
            }

            set
            {
                this._ItemType = value;
            }
        }

        #endregion

        #region Protected Methods and Events

        /// <summary>
        /// Bind Facet Group List
        /// </summary>
        /// <param name="facetGroupList">The Facet Group instance</param>
        protected void BindFacetGroup(TList<FacetGroup> facetGroupList)
        {
			dlFacets.DataSource = facetGroupList;
			dlFacets.DataBind();

            if (facetGroupList.Count == 0)
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotAssociatedFacetCategory").ToString();
                lblError.Visible = true;
				lblNoFacets.Visible = true;
            }
            else
            {
				lblNoFacets.Visible = false;
            }

            btnSubmitBottom.Visible = btnCancelBottom.Visible = facetGroupList.Count == 0 ? false : true;
        }

        /// <summary>
        /// Bind Facets Method
        /// </summary>
        /// <param name="FacetGroupID">The value of FacetGroupID</param>
        /// <returns>Returns the Facet TList</returns>
        protected TList<Facet> BindFacets(object FacetGroupID)
        {
            if (Convert.ToInt32(FacetGroupID) > 0)
            {
				FacetService ts = new FacetService();

				return ts.GetByFacetGroupID(Convert.ToInt32(FacetGroupID));
            }

            return null;
        }

        /// <summary>
        /// Get Facet Names Method
        /// </summary>
        /// <param name="facetGroupId">The value of Facet Group Id</param>
        /// <returns>Returns the Facet Names</returns>
        protected string GetFacetNames(object facetGroupId)
        {
			FacetProductSKUService tpss = new FacetProductSKUService();
			TList<FacetProductSKU> tpsList;

            if (this._ItemType == this.GetGlobalResourceObject("ZnodeAdminResource", "FacetItemTypeSKUID").ToString())
            {
                tpsList = tpss.GetBySKUID(this.SkuId);
            }
            else
            {
                tpsList = tpss.GetByProductID(this.ItemId);
            }

			FacetService ts = new FacetService();
            string facetNames = string.Empty;
            if (tpsList.Count > 0)
            {
				foreach (FacetProductSKU tps in tpsList)
                {
					Facet facet = ts.GetByFacetID(tps.FacetID.Value);
					if (facet.FacetGroupID == Convert.ToInt32(facetGroupId))
                    {
                        if (!string.IsNullOrEmpty(facetNames))
                        {
                            facetNames += ",";
                        }

                        facetNames += facet.FacetName;
                    }
                }
            }

            return facetNames;
        }

        /// <summary>
        /// Get Facets Method
        /// </summary>
        /// <param name="facetGroupId">The value of Facet Group Id</param>
        /// <returns>Returns the Facet TList</returns>
        protected TList<Facet> GetFacets(object facetGroupId)
        {
			FacetProductSKUService tpss = new FacetProductSKUService();
			TList<FacetProductSKU> tpsList;
            if (this._ItemType == this.GetGlobalResourceObject("ZnodeAdminResource", "FacetItemTypeSKUID").ToString())
            {
                tpsList = tpss.GetBySKUID(this.SkuId);
            }
            else
            {
                tpsList = tpss.GetByProductID(this.ItemId);
            }

			FacetService ts = new FacetService();
            TList<Facet> tfacet = new TList<Facet>();
            if (tpsList.Count > 0)
            {
                foreach (FacetProductSKU tps in tpsList)
                {
                    Facet facet = ts.GetByFacetID(tps.FacetID.Value);
					if (facet.FacetGroupID == Convert.ToInt32(facetGroupId))
                    {
                        tfacet.Add(facet);
                    }
                }
            }

            return tfacet;
        }

        /// <summary>
        /// Event is raised when Update button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Update_Click(object sender, EventArgs e)
        {
            StringBuilder sb = new StringBuilder();

            FacetProductSKUService service = new FacetProductSKUService();
            TList<FacetProductSKU> tpsList;
            if (this._ItemType == this.GetGlobalResourceObject("ZnodeAdminResource", "FacetItemTypeSKUID").ToString())
            {
                tpsList = service.GetBySKUID(this.SkuId);
            }
            else
            {
                tpsList = service.GetByProductID(this.ItemId);
            }

			for (int i = 0; i < dlFacets.Items.Count; i++)
            {
				CheckBoxList chklstFacets = (CheckBoxList)dlFacets.Items[i].FindControl("chklstFacets");

                foreach (ListItem li in chklstFacets.Items)
                {
                    if (li.Selected == true)
                    {
                        if (tpsList.Find("FacetID", Convert.ToInt32(li.Value)) == null)
                        {
							FacetProductSKU tps = new FacetProductSKU();
							tps.FacetID = Convert.ToInt32(li.Value);

                            if (this._ItemType == this.GetGlobalResourceObject("ZnodeAdminResource", "FacetItemTypeSKUID").ToString())
                            {
                                tps.SKUID = this.SkuId;
                            }
                            else
                            {
                                tps.ProductID = this.ItemId;
                            }

                            service.Save(tps);
                        }

                        sb.Append(li.Text + ",");
                    }
                    else
                    {
						FacetProductSKU tp;
                        if ((tp = tpsList.Find("FacetID", Convert.ToInt32(li.Value))) != null)
                        {
                            service.Delete(tp);
                        }
                    }
                }
            }

            ProductAdmin prodAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product entity = prodAdmin.GetByProductId(this.ItemId);
            if (sb.Length > 0)
            {
                sb.Remove(sb.Length - 1, 1);
            }

            this.AssociateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogFacetToProduct").ToString(), sb, entity.Name);
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, entity.Name);
            ProductReviewHistoryAdmin reviewAdmin = new ProductReviewHistoryAdmin();
            reviewAdmin.UpdateEditReviewHistory(this.ItemId, "Facets");

            tpsList.Dispose();
            service = null;
            this.BindGrid();
            this.pnlEditFacets.Visible = false;
        }

        /// <summary>
        /// Event is raised when Cancel button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.pnlEditFacets.Visible = false;
        }

        /// <summary>
        /// Edit Faceting Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
		protected void BtnEditFaceting_Click(object sender, EventArgs e)
        {
            pnlEditFacets.Visible = true;
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            ProductAdmin prodAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product entity = prodAdmin.GetByProductId(this.ItemId);
        }

        /// <summary>
        /// Grid Data Bound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_DataBound(object sender, EventArgs e)
        {
            bool visible = false;
            bool row = true;
            foreach (GridViewRow gvr in uxGrid.Rows)
            {
				gvr.Visible = (gvr.FindControl("dlFacetNames") as DataList).Items.Count > 0;

                if (gvr.Visible)
                {
                    gvr.CssClass = row ? "RowStyle" : "AlternatingRowStyle";
                    row = row ? false : true;
                }

                visible = visible || gvr.Visible;
            }

            uxGrid.Visible = visible;

            lblEmptyGrid.Visible = !uxGrid.Visible;

            if (lblEmptyGrid.Visible)
                lblEmptyGrid.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource","RecordNotAssociatedFacet").ToString(),
                                                  ItemType.Replace("Id", ""));
        }

        /// <summary>
        /// Event is raised when Check List Facets Pre Render is called
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
		protected void ChklstFacets_PreRender(object sender, EventArgs e)
        {
            CheckBoxList chklstFacets = (CheckBoxList)sender;

            FacetProductSKUService tpss = new FacetProductSKUService();
            TList<FacetProductSKU> tpsList;

            tpsList = tpss.GetByProductID(this.ItemId);
            if (tpsList.Count > 0)
            {
                foreach (ListItem li in chklstFacets.Items)
                {
                    li.Selected = tpsList.Find("FacetID", Convert.ToInt32(li.Value)) != null;
                }
            }
        }
        #endregion

        #region Page Events

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.ItemType = this.GetGlobalResourceObject("ZnodeAdminResource","FacetItemTypeProductID").ToString();

            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }

            // Get ItemId from querystring        
            if (Request.Params["skuid"] != null)
            {
                this.SkuId = int.Parse(Request.Params["skuid"]);
            }

            if (!this.IsPostBack)
            {
                this.BindGrid();
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Get Associated Facet Groups Method
        /// </summary>
        /// <returns>Returns a Facet Group TList</returns>
        private TList<FacetGroup> GetAssociatedFacetGroups()
        {
            FacetGroupService tgs = new FacetGroupService();
            TList<FacetGroup> tgg = new TList<FacetGroup>();

            if (this.ItemId > 0)
            {
                ProductService ps = new ProductService();

                ZNode.Libraries.DataAccess.Entities.Product product = ps.DeepLoadByProductID(this.ItemId, true, ZNode.Libraries.DataAccess.Data.DeepLoadType.IncludeChildren, typeof(TList<ProductCategory>));

                if (product != null && product.ProductCategoryCollection != null)
                {
                    foreach (ProductCategory pc in product.ProductCategoryCollection)
                    {
                        foreach (FacetGroup tg in tgs.GetByCategoryIDFromFacetGroupCategory(pc.CategoryID))
                        {
                            if (tgg.IndexOf(tg) == -1)
                            {
                                tgg.Add(tg);
                            }
                        }
                    }
                }
            }

            return tgg;
        }

        /// <summary>
        /// Bind Grid Method
        /// </summary>
        private void BindGrid()
        {
            TList<FacetGroup> tgg = this.GetAssociatedFacetGroups();
            this.BindFacetGroup(tgg);

            uxGrid.Visible = true;

            uxGrid.DataSource = tgg;

            uxGrid.DataBind();
        }

        /// <summary>
        /// Remove Facet Product SKU
        /// </summary>
        /// <param name="facet">The Facet Instance</param>
        private void RemoveFacetProductSku(Facet facet)
        {
            FacetProductSKUService service = new FacetProductSKUService();
            TList<FacetProductSKU> tps = service.GetByFacetID(facet.FacetID);

            foreach (FacetProductSKU tp in tps)
            {
                if (tp.ProductID == this.ItemId)
                {
                    service.Delete(tp);
                }
            }

            this.BindGrid();
        }
        #endregion
    }
}