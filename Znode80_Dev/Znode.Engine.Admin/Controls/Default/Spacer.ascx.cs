using System;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace  Znode.Engine.Admin.Controls.Default
{
    /// <summary>
    /// Represents a Spacer user control class
    /// </summary>
    public partial class Spacer : System.Web.UI.UserControl
    {
        private int _SpacerWidth;
        private int _SpacerHeight;

        /// <summary>
        /// Gets or sets the spacer control width.
        /// </summary>
        public int SpacerWidth
        {
            get { return this._SpacerWidth; }
            set { this._SpacerWidth = value; }
        }

        /// <summary>
        /// Gets or sets the spacer control height.
        /// </summary>
        public int SpacerHeight
        {
            get { return this._SpacerHeight; }
            set { this._SpacerHeight = value; }
        }

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            // Put user code to initialize the page here
            imgClear.Width = this.SpacerWidth;
            imgClear.Height = this.SpacerHeight;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            InitializeComponent();
            base.OnInit(e);
        }

        /// <summary>
        ///		Required method for Designer support - do not modify
        ///		the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {

        }
        #endregion
    }
}
