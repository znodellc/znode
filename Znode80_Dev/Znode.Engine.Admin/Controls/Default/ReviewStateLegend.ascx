﻿<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="Znode.Engine.Admin.Controls.Default.ReviewStateLegend" CodeBehind="ReviewStateLegend.ascx.cs" %>
<div class="Legend">
    <asp:Image ID="Image1" runat="server" ImageUrl="~/Themes/Images/222-point_approve.gif" /><span><asp:Localize runat="server" ID="Approved" Text='<%$ Resources:ZnodeAdminResource, TextApproved %>'></asp:Localize></span>
    <asp:Image CssClass="Item" ID="Image3" runat="server" ImageUrl="~/Themes/Images/221-point_decline.gif" /><span><asp:Localize runat="server" ID="Declined" Text='<%$ Resources:ZnodeAdminResource, TextDeclined %>'></asp:Localize></span>
    <asp:Image ID="Image2" runat="server" ImageUrl="~/Themes/Images/223-point_pendingapprove.gif" /><span><asp:Localize runat="server" ID="PendingApproval" Text='<%$ Resources:ZnodeAdminResource, TextPendingApproval %>'></asp:Localize></span>
    <asp:Image ID="Image4" runat="server" ImageUrl="~/Themes/Images/220-point_toedit.gif" /><span><asp:Localize runat="server" ID="ToEdit" Text='<%$ Resources:ZnodeAdminResource, TextToEdit %>'></asp:Localize></span>
</div>
