﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Controls.Default.AddOn" CodeBehind="AddOn.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<div align="right">
    <zn:Button runat="server" ButtonType="EditButton" OnClick="BtnAddNewAddOn_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddAddOn%>' ID="btnAddNewAddOn" Width="100px"  />
</div>
<div>
    <uc1:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
</div>
<!-- Update Panel for grid paging that are used to avoid the postbacks -->
<asp:UpdatePanel ID="updPnlProductAddOnsGrid" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:GridView ID="uxGridProductAddOns" OnRowDataBound="UxGridProductAddOns_RowDataBound"
            runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGridProductAddOns_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGridProductAddOns_RowCommand" Width="100%"
            EnableSortingAndPagingCallbacks="False" PageSize="15" AllowSorting="True">
            <Columns>
                <asp:BoundField DataField="ProductAddOnId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# GetAddOnName(Eval("AddonId")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitle %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# GetAddOnTitle(Eval("AddonId")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleValue %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# GetAddOnValues(Eval("AddonId")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton CommandName="Remove" CausesValidation="false" ID="btnDelete" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkRemove %>' CssClass="Button" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                <asp:Localize runat="server" Text='<%$ Resources:ZnodeAdminResource, RecordNotFoundAddOn %>'></asp:Localize>
            </EmptyDataTemplate>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
