using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Controls.Default
{
    /// <summary>
    /// Represents the CustomMessage user control class
    /// </summary>
    public partial class CustomMessage : System.Web.UI.UserControl
    {
        /// <summary>
        /// Sets the Message Key
        /// </summary>
        public string MessageKey
        {
            set
            {
                string key = value;
                ZNode.Libraries.Admin.MessageConfigAdmin admin = new ZNode.Libraries.Admin.MessageConfigAdmin();
                string msg = admin.GetMessage(key, ZNodeConfigManager.SiteConfig.PortalID, 43);

                if (msg != null)
                {
                    lblMsg.Text = msg;
                }
            }
        }
    }
}