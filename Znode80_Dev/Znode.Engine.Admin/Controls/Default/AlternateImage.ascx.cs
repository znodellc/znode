﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;

namespace  Znode.Engine.Admin.Controls.Default
{
    /// <summary>
    /// Represents the Alternate Images user control class
    /// </summary>
    public partial class AlternateImage : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private string AddImageLink = "~/Secure/Vendors/VendorProducts/AddView.aspx?itemid=";
        private int ItemId;
        private ZNodeProductImageType _ImageType = ZNodeProductImageType.AlternateImage;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the ImageType
        /// </summary>
        public ZNodeProductImageType ImageType
        {
            get
            {
                return this._ImageType;
            }

            set
            {
                this._ImageType = value;
            }
        }
        #endregion

        #region Event Methods

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddImage_click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddImageLink + this.ItemId + "&typeid=" + Convert.ToString((int)this._ImageType) + "&mode=alternateimages");
        }

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnReject_Click(object sender, EventArgs e)
        {
            UserStoreAccess uss = new UserStoreAccess();
			if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Vendor)
            {
                Response.Redirect(this.AddImageLink + this.ItemId + "&typeid=" + Convert.ToString((int)this._ImageType) + "&mode=alternateimages");
            }
            else
            {
                this.UpdateImageStatus(false);
            }
        }

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAccept_Click(object sender, EventArgs e)
        {
            UserStoreAccess uss = new UserStoreAccess();
			if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Vendor)
            {
                Response.Redirect(this.AddImageLink + this.ItemId + "&typeid=" + Convert.ToString((int)this._ImageType) + "&mode=alternateimages");
            }
            else
            {
                this.UpdateImageStatus(true);
            }
        }

        /// <summary>
        /// Row command 
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GridThumb_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Edit")
            {
                Response.Redirect(this.AddImageLink + this.ItemId + "&productimageid=" + e.CommandArgument.ToString() + "&typeid=" + Convert.ToString((int)this._ImageType) + "&mode=alternateimages");
            }

            if (e.CommandName == "RemoveItem")
            {
                ZNode.Libraries.Admin.ProductViewAdmin prodadmin = new ProductViewAdmin();
                bool Status = prodadmin.Delete(int.Parse(e.CommandArgument.ToString()));
                if (Status)
                {
                    this.BindAlternateImages();
                }
            }
        }

        /// <summary>
        /// Gets the Status Image Icon
        /// </summary>
        /// <param name="state">State object that raised the event.</param>
        /// <returns>Returns the Status Image Icon</returns>
        protected string GetStatusImageIcon(object state)
        {
            string imageSrc = "~/Themes/Images/223-point_pendingapprove.gif";

            if (state.ToString().ToLower() == "30")
            {
                // Image rejected
                imageSrc = ZNode.Libraries.Admin.Helper.GetCheckMark(false);
            }

            if (state.ToString().ToLower() == "20")
            {
                // Image approved            
                imageSrc = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
            }

            return imageSrc;
        }

        /// <summary>
        /// Grid Row Data Bound event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GridThumb_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            UserStoreAccess uss = new UserStoreAccess();
			if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer)
            {
                // If Revier then hide the Action (Edit and Remove) column
                if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Footer)
                {
                    e.Row.Cells[8].Visible = false;
                }
            }
            else
            {
                // If Vendor then hide the checkbox column
                if (e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Footer)
                {
                    e.Row.Cells[0].Visible = false;
                }
            }
        }

        #endregion

        #region Page Load Methods

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId (Product Id) from QueryString        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                // Set the button caption based on the user and Image type.
                UserStoreAccess uss = new UserStoreAccess();
				if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer)
				{
				    btnReject.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonReject").ToString();
				}

                if (this.ItemId > 0)
                {
                    this.BindAlternateImages();
                }
                else
                {
                    throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource","RecordNotFoundProductRequested").ToString());
                }
            }
        }

        #endregion

        #region Helper Methods

        /// <summary>
        /// Get http image path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the image file path with name.</returns>
        protected string GetImagePath(string imageFileName)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(imageFileName);
        }

        /// <summary>
        /// Bind Alternate images
        /// </summary>
        private void BindAlternateImages()
        {
            ProductViewAdmin imageadmin = new ProductViewAdmin();
            DataSet ds = imageadmin.GetAllAlternateImage(this.ItemId);

            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                }
            }

            string filterExpression = string.Empty;

            // Filter based on image types
            filterExpression = "ProductImageTypeID=" + Convert.ToString((int)this._ImageType);

            ds.Tables[0].DefaultView.RowFilter = filterExpression;
            GridThumb.DataSource = ds.Tables[0].DefaultView;
            GridThumb.DataBind();

            UserStoreAccess uss = new UserStoreAccess();

            // If there are not product alternate image or swatch image then hide both buttons.
			if (ds.Tables[0].DefaultView.Count == 0 && uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer)
            {
                btnAccept.Enabled = false;
                btnReject.Enabled = false;
            }

			if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer)
            {
                btnAddImage.Visible = false;
            }

			if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Admin)
            {
                btnAccept.Visible = false;
                btnReject.Visible = false;
            }

            if (this._ImageType == ZNodeProductImageType.Swatch)
            {
                btnAddImage.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ButtonAddNewSwatchImage").ToString();
            }
            else
            {
                btnAddImage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ButtonAddNewAlternateImage").ToString();
            }
        }

        /// <summary>
        /// Update (Approved/Decline) the selected image status.
        /// </summary>
        /// <param name="status">Status to update.
        /// False: to update image as declined
        /// True: to update image as approved.
        /// </param>
        private void UpdateImageStatus(bool status)
        {
            List<string> selectedProductImagesIdList = new List<string>();
            for (int rowIndex = 0; rowIndex <= GridThumb.Rows.Count - 1; rowIndex++)
            {
                CheckBox chk = GridThumb.Rows[rowIndex].Cells[0].FindControl("chkSelect") as CheckBox;
                string productImageId = GridThumb.Rows[rowIndex].Cells[2].Text;
                if (chk.Checked)
                {
                    selectedProductImagesIdList.Add(productImageId);
                }
            }

            ZNode.Libraries.Admin.ProductViewAdmin productAdmin = new ProductViewAdmin();
            TList<ProductImage> productImageList = productAdmin.GetImageByProductID(this.ItemId);
            foreach (string productImageId in selectedProductImagesIdList)
            {
                int imageId = Convert.ToInt32(productImageId);
                int reviewStateId = 10;
                TList<ProductImage> filteredProductImageList = productImageList.FindAll(delegate(ProductImage pi) { return pi.ProductImageID == imageId; });
                if (filteredProductImageList.Count == 1)
                {
                    ProductImage currentItem = filteredProductImageList[0];
                    reviewStateId = status == true ? Convert.ToInt32(ZNodeProductReviewState.Approved) : Convert.ToInt32(ZNodeProductReviewState.Declined);

                    // Update review state only if existing state is not same as current state.
                    if (reviewStateId != currentItem.ReviewStateID)
                    {
                        // Set the review state ID from enumeration
                        currentItem.ReviewStateID = reviewStateId;

                        // Set the product image as rejected.
                        productAdmin.Update(currentItem);
                    }
                }
            }

            // Reload the image list.
            this.BindAlternateImages();
        }
        #endregion
    }
}
