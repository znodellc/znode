﻿<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="Znode.Engine.Admin.Controls.Default.CategoryAutoComplete" CodeBehind="CategoryAutoComplete.ascx.cs" %>

<script type="text/javascript">
    var hiddenTextValue; //alias to the hidden field: hideValue
    function AutoComplete_CategorySelected(source, eventArgs) {
        hiddenTextValue = $get("<%=hdnCategoryId.ClientID %>");
        hiddenTextValue.value = eventArgs.get_value();
    }

    function AutoComplete_CategoryShowing(source, eventArgs) {
        hiddenTextValue = $get("<%=hdnCategoryId.ClientID %>");
        hiddenTextValue.value = "";
    }

    function Category_OnBlur(obj) {
        hiddenTextValue = $get("<%=hdnCategoryId.ClientID %>");
        if (obj.value == "") {
            hiddenTextValue.value = "";
        }
    }
</script>


<asp:HiddenField ID="hdnCategoryId" runat="server"></asp:HiddenField>
<asp:TextBox ID="txtCategory" runat="server" onblur="Category_OnBlur(this)"></asp:TextBox>
<ajaxToolKit:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" TargetControlID="txtCategory"
    ServicePath="ZNodeCategoryServices.asmx" ServiceMethod="GetCompletionListWithContextAndValues"
    UseContextKey="true" MinimumPrefixLength="1" EnableCaching="false" CompletionSetCount="10"
    CompletionInterval="1" OnClientShowing="AutoComplete_CategoryShowing"
    OnClientItemSelected="AutoComplete_CategorySelected" />
<asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSelectCategory %>'
    Display="Dynamic" ControlToValidate="txtCategory" CssClass="Error" Visible="false"></asp:RequiredFieldValidator>
<asp:DropDownList ID="ddlCategory" runat="server" Visible="false" Width="160px"></asp:DropDownList>
<asp:HiddenField runat="server" ID="ForceAutoComplete" Value="false" />



