<%@ Control Language="C#" AutoEventWireup="true" Inherits="Znode.Engine.Admin.Controls.Default.ForgotPassword" CodeBehind="ForgotPassword.ascx.cs" %>

<%@ Register Src="~/Controls/Default/spacer.ascx" TagName="spacer" TagPrefix="ZNode" %>

<div class="Form">
    <div class="FormView" style="margin-left: 10px; text-align: left; width: 100%">
        <div style="text-align: left;">
            <h1>
                <asp:Localize runat="server" ID="ForgotPasswordTitle" Text='<%$ Resources:ZnodeAdminResource, TitleForgotPassword %>'></asp:Localize></h1>
        </div>
        <asp:Wizard ID="PasswordRecoveryWizard" DisplaySideBar="False" runat="server" OnActiveStepChanged="PasswordRecoveryWizard_ActiveStepChanged"
            ActiveStepIndex="1">
            <WizardSteps>
                <asp:TemplatedWizardStep runat="server" StepType="Start" AllowReturn="true">
                    <ContentTemplate>

                        <div style="text-align: left;">
                            <asp:Label ID="Title" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextForgotPassword %>'></asp:Label>
                        </div>
                        <div class="ClearBoth">
                            <ZNode:spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="10" runat="server"></ZNode:spacer>
                        </div>
                        <asp:Panel ID="UserNamePanel" DefaultButton="ibSubmit" runat="server">
                            <div class="ClearBoth">
                                <ZNode:spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="10" runat="server"></ZNode:spacer>
                            </div>
                            <div class="FieldStyle LeftContent" style="width: 70px;">
                                <asp:Label ID="UserNameLabel" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleUsername %>'></asp:Label>
                            </div>

                            <div class="ValueStyle LeftContent">
                                <asp:TextBox ID="UserName" autocomplete="off" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredUsername %>' ValidationGroup="PasswordRecovery1" CssClass="Error"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                            <div class="FieldStyle LeftContent" style="width: 70px;">
                                <asp:Label ID="EmailLabel" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEmail %>'></asp:Label>
                            </div>
                            <div class="ValueStyle LeftContent">
                                <div>
                                    <asp:TextBox ID="Email" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Email"
                                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEmail %>' ToolTip="Email ID is required" CssClass="Error"
                                        ValidationGroup="PasswordRecovery1" Display="Dynamic"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="regemailID" runat="server" ControlToValidate="Email"
                                        ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidEmail %>' ValidationGroup="PasswordRecovery1"
                                        Display="Dynamic" ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"
                                        CssClass="Error"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="ClearBoth">
                            </div>
                            <p class="Error">
                                <asp:Literal ID="FailureText" runat="server" EnableViewState="false"></asp:Literal>
                            </p>
                            <div class="LeftContent">
                                &nbsp;
                            </div>
                            <div class="LeftContent">
                                <div class="ImageButtons">

                                    <zn:LinkButton ID="ibSubmit" runat="server" CommandName="MoveNext" ButtonType="LoginButton" ValidationGroup="PasswordRecovery1" CausesValidation="true"
                                        ButtonPriority="Normal" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
                                    &nbsp; &nbsp;  
 
                                     <zn:LinkButton ID="btnClear" runat="server" OnClick="BtnClear_Click" ButtonType="LoginButton"
                                         ButtonPriority="Normal" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
                                    <div class="Clear">
                                        <ZNode:spacer ID="Spacer18" SpacerHeight="0" SpacerWidth="10" runat="server"></ZNode:spacer>
                                    </div>
                                </div>
                        </asp:Panel>
                    </ContentTemplate>
                    <CustomNavigationTemplate>
                    </CustomNavigationTemplate>
                </asp:TemplatedWizardStep>
                <asp:TemplatedWizardStep runat="server" StepType="Finish" AllowReturn="False">
                    <ContentTemplate>
                        <div>
                            <ZNode:spacer ID="Spacer3" SpacerWidth="5" SpacerHeight="5" runat="server" />
                        </div>
                        <p>
                            <asp:Localize ID="Localize11" Text='<%$ Resources:ZnodeAdminResource, TextSecurityQuestion %>'
                                runat="server" />
                        </p>
                        <div>
                            <ZNode:spacer ID="Spacer5" SpacerWidth="5" SpacerHeight="10" runat="server" />
                        </div>

                        <asp:Panel ID="QuestionPanel" DefaultButton="FinishButton" runat="server">
                            <div class="ClearBoth">
                            </div>
                            <div class="FieldStyle LeftContent" style="width: 100px;">
                                <asp:Label ID="Localize4" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSecurityQuestion %>' runat="server" />
                            </div>
                            <div class="SecurityAnswer">
                                <asp:Literal ID="Question" runat="server"></asp:Literal>
                            </div>

                            <div class="ClearBoth">
                                <br />
                            </div>

                            <div class="FieldStyle LeftContent" style="width: 100px;">
                                <asp:Label ID="AnswerLabel" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSecurityAnswer %>'></asp:Label>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="Answer" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="AnswerRequired" runat="server" ControlToValidate="Answer"
                                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredAnswer %>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredAnswer %>' ValidationGroup="PasswordRecovery1"
                                    CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                                <div class="Error">
                                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                                </div>
                            </div>

                            <div class="ClearBoth">
                            </div>

                            <div class="LeftContent">
                                &nbsp;
                            </div>
                            <div class="LeftContent">
                                <div class="ImageButtons">

                                    <zn:LinkButton ID="FinishButton" runat="server" ButtonType="LoginButton" CommandName="MoveComplete" ValidationGroup="PasswordRecovery1"
                                        ButtonPriority="Normal" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
                                    &nbsp; &nbsp;
                                 <zn:LinkButton ID="btnClear" runat="server" OnClick="BtnClear_Click" ButtonType="LoginButton"
                                     ButtonPriority="Normal" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />

                                </div>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                    <CustomNavigationTemplate>
                    </CustomNavigationTemplate>
                </asp:TemplatedWizardStep>
                <asp:WizardStep ID="step3" runat="server" StepType="Complete">
                    <ZNode:spacer SpacerWidth="15" SpacerHeight="15" ID="Spacer1" runat="server" />
                    <div class="SuccessText">
                        <asp:Label Text='<%$ Resources:ZnodeAdminResource, TextPassword %>' ID="Message" runat="server"
                            EnableViewState="False"></asp:Label>
                    </div>
                    <ZNode:spacer SpacerWidth="15" SpacerHeight="10" ID="Spacer2" runat="server" />
                    <div class="BackLink">
                        <a id="BackLink" runat="server" href="~/Default.aspx">
                            <asp:Localize ID="Localize5" runat="server" /></a>
                    </div>
                    <div>
                        <ZNode:spacer ID="Spacer" SpacerWidth="5" SpacerHeight="25" runat="server" />
                    </div>
                </asp:WizardStep>
            </WizardSteps>
            <StepNextButtonStyle />
            <StartNextButtonStyle />
        </asp:Wizard>
        <!-- End -->
    </div>
</div>


