<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Controls.Default.HtmlTextBox" CodeBehind="HtmlTextBox.ascx.cs" %>

<script language="javascript" type="text/javascript">
    tinyMCE.init({
        // General options
        mode : "exact",
        elements: <%= "\"" + ZnodeEditBox.ClientID + "\"" %>,
	theme : "advanced",
	plugins : "safari,spellchecker,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template",

        // Theme options
	theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
	theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,|,forecolor,backcolor",
	theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,advhr,|,print,|,ltr,rtl,|,fullscreen,|,spellchecker",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing : true,
	visualblocks_default_state: true,
	spellchecker_rpc_url : "TinyMCE.ashx?module=SpellChecker",
        // Example content CSS (should be your site CSS)
	content_css : "css/content.css",
        // Schema is HTML5 instead of default HTML4
	schema: "html5",
        // End container block element when pressing enter inside an empty block
	end_container_on_empty_block: true,
        // HTML5 formats
	style_formats : [
            {title : 'h1', block : 'h1'},
            {title : 'h2', block : 'h2'},
            {title : 'h3', block : 'h3'},
            {title : 'h4', block : 'h4'},
            {title : 'h5', block : 'h5'},
            {title : 'h6', block : 'h6'},
            {title : 'p', block : 'p'},
            {title : 'div', block : 'div'},
            {title : 'pre', block : 'pre'},
            {title : 'section', block : 'section', wrapper: true, merge_siblings: false},
            {title : 'article', block : 'article', wrapper: true, merge_siblings: false},
            {title : 'blockquote', block : 'blockquote', wrapper: true},
            {title : 'hgroup', block : 'hgroup', wrapper: true},
            {title : 'aside', block : 'aside', wrapper: true},
            {title : 'figure', block : 'figure', wrapper: true}
	]

    });    
</script>

<asp:TextBox ID="ZnodeEditBox" runat="server" Rows="10" TextMode="MultiLine" Width="100%" Height="267px"></asp:TextBox>
