using System;
using System.Collections;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using ZNode.Libraries.ECommerce.Entities;
using Znode.Engine.Promotions;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Fulfillment;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using Address = ZNode.Libraries.DataAccess.Entities.Address;
using ZNodeShoppingCart = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCart;
using ZNodeShoppingCartItem = ZNode.Libraries.ECommerce.ShoppingCart.ZNodeShoppingCartItem;

namespace Znode.Engine.Admin.Controls.Default
{
	/// <summary>
	/// Represents the Shopping Cart user control class
	/// </summary>
	public partial class ShoppingCart : System.Web.UI.UserControl
	{
        #region Private Variables

		private ZNodeShoppingCart _shoppingCart = ZNodeShoppingCart.CurrentShoppingCart();
        private ZNodePortalCart _portalCart;
        private DateTime currentdate = System.DateTime.Now.Date;
        private ZnodeCoupon _Coupon = new ZnodeCoupon();
        private ZNodeOrderFulfillment _Order = new ZNodeOrderFulfillment();
        private int rowNumber = 0;
        private bool _ShowTaxShipping = false;
        private string productUrl = string.Empty;
        private int _PortalID;
        private System.EventHandler _ShippingSelectedIndexChanged;
        private System.EventHandler _ChangeShipToChanged;
        #endregion

        #region Public Events
        
        /// <summary>
        /// Gets or sets the shipping item selected event.
        /// </summary>
        public System.EventHandler ShippingSelectedIndexChanged
        {
            get { return this._ShippingSelectedIndexChanged; }
            set { this._ShippingSelectedIndexChanged = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public System.EventHandler ChangeShipToChanged
        {
            get { return this._ChangeShipToChanged; }
            set { this._ChangeShipToChanged = value; }
        }
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the Portal Id. 
        /// </summary>
        public int PortalID
        {
            get { return this._PortalID; }
            set { this._PortalID = value; }
        }

        /// <summary>
        /// Gets or sets the current shopping cart object
        /// </summary>
        public ZNodeShoppingCart ShoppingCartObject
        {
            get { return this._shoppingCart; }
            set { this._shoppingCart = (ZNodeShoppingCart)ZNodeShoppingCart.CreateFromSession(ZNodeSessionKeyType.ShoppingCart); }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the to display/hide the shipping and tax fields in shopping cart sub total section.
        /// </summary>
        public bool ShowTaxShipping
        {
            get { return this._ShowTaxShipping; }
            set { this._ShowTaxShipping = value; }
        }

        /// <summary>
        /// Gets the coupon code
        /// </summary>
        public string CouponCode
        {
            get { return ecoupon.Text.Trim(); } 
        }

        /// <summary>
        /// Gets or sets the Account Object
        /// </summary>
		public ZNodeUserAccount UserAccount
        {
            get
            {
                if (ViewState["AccountObject"] != null)
                {
                    // The account info with 'AccountObject' is retrieved from the session and
                    // It is converted to a Entity Account object
					return (ZNodeUserAccount)ViewState["AccountObject"];
                }

				return new ZNodeUserAccount();
            }

            set
            {
                // Customer account object is placed in the session and assigned a key, AccountObject.            
                ViewState["AccountObject"] = value;
            }
        }
        #endregion

        #region Bind Method

        /// <summary>
        /// Represents the BindGrid method
        /// </summary>
        public void Bind()
        {
            this._portalCart = this._shoppingCart.PortalCarts.FirstOrDefault();
			if (this._portalCart != null)
			{
				rptShiping.DataSource = this._portalCart.AddressCarts;
				rptShiping.DataBind();

				BindTotals();

				foreach (RepeaterItem repeateritem in rptShiping.Items)
				{
					GridView uxCart = repeateritem.FindControl("uxCart") as GridView;

					// Set the value for the Dropdown list
					foreach (GridViewRow row in uxCart.Rows)
					{
						HiddenField hdnGUID = row.FindControl("GUID") as HiddenField;
						string[] GUID = hdnGUID.Value.Split('|');

						// Get Shopping cart item using GUID value
						ZNodeShoppingCartItem item = this._portalCart.AddressCarts.FirstOrDefault(x => x.AddressID.ToString() == GUID[1]).ShoppingCartItems.Cast<ZNodeShoppingCartItem>().FirstOrDefault(x => x.GUID == GUID[0]);
						DropDownList hdnQty = row.FindControl("uxQty") as DropDownList;

						// If Min quantity is not set in admin, set it to 1
						int minQty = item.Product.MinQty == 0 ? 1 : item.Product.MinQty;

						// If Max quantity is not set in admin , set it to 10
						int maxQty = item.Product.MaxQty == 0 ? 10 : item.Product.MaxQty;

						ArrayList quantityList = new ArrayList();

						for (int itemIndex = minQty; itemIndex <= maxQty; itemIndex++)
						{
							quantityList.Add(itemIndex);
						}

						// Bind Quantity drop down list
						hdnQty.DataSource = quantityList;
						hdnQty.DataBind();
						hdnQty.SelectedValue = Convert.ToString(item.Quantity);
					}

				}
				
			}  
        }

        /// <summary>
        ///  Represents the BindTotals method
        /// </summary>
        private void BindTotals()
        {
			// Bind totals
			Shipping.Text = GetShippingTotal().ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();            
            SubTotal.Text = this._portalCart.SubTotal.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            DiscountDisplay.Text = "-" + this._portalCart.Discount.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            lblGiftCardAmount.Text = "-" + this._portalCart.GiftCardAmount.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
			Tax.Text = GetTaxTotal().ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            Total.Text = this._portalCart.Total.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
            rowDiscount.Visible = this._portalCart.Discount > 0 ? true : false;
            trGiftCardAmount.Visible = this._portalCart.GiftCardAmount > 0 ? true : false;
			tblRowTax.Visible = this._portalCart.TaxCost > 0 ? true : false;
			tblRowShipping.Visible = this._portalCart.ShippingCost > 0 ? true : false;
        }

		private decimal GetShippingTotal()
		{
			_portalCart.Calculate();

			// Show messages
			lblErrorMessage.Text = _portalCart.ErrorMessage;
			lblPromoMessage.Text = _portalCart.PromoDescription;

			_portalCart.AddressCarts.ForEach(x => { SetPaymentControlData(x); x.Calculate(); });
			return _portalCart.ShippingCost;
		}


		private decimal GetTaxTotal()
		{
			_portalCart.Calculate();
			_portalCart.AddressCarts.ForEach(x => { SetPaymentControlData(x); x.Calculate(); });
			return _portalCart.AddressCarts.Sum(x => x.OrderLevelTaxes);
		}

		/// <summary>
		/// Bind data to payment control
		/// </summary>
		protected void SetPaymentControlData(ZNodeMultipleAddressCart addressCart)
		{
			ZNodePayment Payment = new ZNodePayment();
			Payment.BillingAddress = this.UserAccount.BillingAddress;
			Payment.ShippingAddress = this.UserAccount.Addresses.FirstOrDefault(x => x.AddressID == addressCart.AddressID);
			addressCart.Payment = (ZNodePayment)Payment;
		}

        /// <summary>
        /// Update the shopping cart quantity.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        public void UpdateQuantity(object sender, EventArgs e)
        {
	        lblMaxQuantity.Text = string.Empty;
            DropDownList ddl = (DropDownList)sender;
            HiddenField hdnGUID = ddl.Parent.FindControl("GUID") as HiddenField;

            // Set the Dropdown list value
            int Qty = Convert.ToInt32(ddl.Text);
            string[] cartId = hdnGUID.Value.Split('|');
			string GUID = cartId[0];

            // Get Shopping cart item using GUID value
             var addressCart = this.ShoppingCartObject.PortalCarts.Select(x => x.AddressCarts
												.FirstOrDefault(y => y.AddressID.ToString() == cartId[1])).FirstOrDefault();
			 ZNodeShoppingCartItem item = addressCart.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().FirstOrDefault(x => x.GUID == GUID);

             // Update quantity in the shopping cart manager
             if (Qty == 0)
             {
                 this._shoppingCart.RemoveFromCart(GUID);
             }
             else
             {
                 if (item != null)
                 {
	                 //this._portalCart = null;

                     // check if already multiple ship to address, then update corresponding shipment only
                     if (this.ShoppingCartObject.PortalCarts.SelectMany(x => x.AddressCarts).Any())
                     {
	                     var existingShipment =
		                     item.OrderShipments.FirstOrDefault(x => x.AddressID == addressCart.AddressID);

						 var shoppingCartItem = this._shoppingCart.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().FirstOrDefault(x => x.GUID == GUID);

						 if (shoppingCartItem != null)
						 {
							 //Validate for maximum quantity reached 
						    if (item.Product.MaxQty < (item.OrderShipments.Count(x => x.AddressID != addressCart.AddressID) + Qty))
							{
								 lblMaxQuantity.Visible = true;
								 lblMaxQuantity.ForeColor = System.Drawing.Color.Red;
                                 lblMaxQuantity.Text = item.Product.Name + this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorMaxQuantity");
							}
                            else if (!CheckCartInventory(item, (item.OrderShipments.Count(x => x.AddressID != addressCart.AddressID) + Qty)))
							 {
								 lblMaxQuantity.Visible = true;
								 lblMaxQuantity.ForeColor = System.Drawing.Color.Red;
                                 lblMaxQuantity.Text = item.Product.Name + this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateItem");
							 }                             
							 else
							 {
								 lblMaxQuantity.Visible = false;

								 shoppingCartItem
									 .Quantity += Qty -
									              item.OrderShipments.Where(x => x.AddressID == addressCart.AddressID)
									                  .Sum(x => x.Quantity);

								 var ordershipment = new ZNodeOrderShipment()
									 {
										 SlNo = existingShipment.SlNo,
										 Quantity = Qty,
										 ItemGUID = GUID,
										 AddressID = addressCart.AddressID
									 };

								 this._shoppingCart.UpdateShipmentQuantity(ordershipment);
							 }
						 }
						  
	                     
                     }
                     // If Product is set to Allow back order or Track inventory is not enable then Update the quantity
                     else if (item.Product.AllowBackOrder)
                     {
                         // Update Quantity for this product
                         this._shoppingCart.UpdateItemQuantity(GUID, Qty);
                     }
                     else if (item.Product.AllowBackOrder == false && item.Product.TrackInventoryInd == false)
                     {
                         // Update Quantity for this product
                         this._shoppingCart.UpdateItemQuantity(GUID, Qty);
                     }
                     else
                     {
                         // Check for available quantity (which included both Available + Quantity Ordered)
                         if (this.UpdateQuantity(item))
                         {
                             this._shoppingCart.UpdateItemQuantity(GUID, Qty);
                         }
                     }
                 }
             }
        }

        private bool CheckCartInventory(ZNodeShoppingCartItem item, int Qty)
        {
            bool hasInventory = true;
            if (item != null)
            {                
                // Get Current Qunatity by Subtracting QunatityOrdered from Quantity On Hand(Inventory)
                int orderedQuantity = Qty;
                int currentQuantity = item.Product.QuantityOnHand - orderedQuantity;

                if (currentQuantity < 0)
                {
                    return false;
                }
                else
                {
                    foreach (ZNodeAddOn AddOn in item.Product.SelectedAddOns.AddOnCollection)
                    {
                        // Loop through the Add-on values for each Add-on
                        foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                        {


                            // Check for back-order and track inventory settings
                            if ((!AddOn.AllowBackOrder) && AddOn.TrackInventoryInd)
                            {
                                int addonQuantityOnHand = ProductAdmin.GetQuantity(AddOnValue.SKU);
                                int currentAddonQuantity = addonQuantityOnHand;

                                if (this._shoppingCart != null)
                                {
                                    // Get Current Qunatity 
                                    currentAddonQuantity -= Qty;
                                }

                                hasInventory = hasInventory && currentAddonQuantity >= 0;
                            }
                        }
                    }

                    return currentQuantity >= 0 && hasInventory;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Bind Shipping Method
        /// </summary>
        public void BindShipping(DropDownList ddlShipping, string destinationCountryCode)
        {
            ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();
            if (_userAccount.AccountID > 0)
            {
                if (ddlShipping.Items.Count == 0)
                {
					ZNodeUserAccount userAccount = this.UserAccount;

                    int profileID = ZNodeProfile.CurrentUserProfileId;

                    ShippingService shipServ = new ShippingService();
					TList<ZNode.Libraries.DataAccess.Entities.Shipping> shippingList = shipServ.GetAll();
                    shippingList.Sort("DisplayOrder Asc");
                    shippingList.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.Shipping shipping) { return (shipping.ActiveInd == true) && (shipping.ProfileID == profileID || !shipping.ProfileID.HasValue) && (shipping.DestinationCountryCode == destinationCountryCode || string.IsNullOrEmpty(shipping.DestinationCountryCode)); });
                    DataSet ds = shippingList.ToDataSet(false);
                    DataView dv = new DataView(ds.Tables[0]);
                  

                    if (dv.ToTable().Rows.Count > 0)
                    {
                        foreach (DataRow dr in dv.ToTable().Rows)
                        {
                            string description = dr["Description"].ToString();

                            System.Text.RegularExpressions.Regex regex = new System.Text.RegularExpressions.Regex("&reg;");
                            description = regex.Replace(description, "�");
                            ListItem li = new ListItem(description, dr["ShippingID"].ToString());
                            ddlShipping.Items.Add(li);
                        }

                        ddlShipping.SelectedIndex = 0;
                    }
                    if (Request.Form[ddlShipping.UniqueID] != null)
                    {
                        int shipId = 0;
                        if (int.TryParse(Request.Form[ddlShipping.UniqueID], out shipId))
                        {
                            ListItem li = ddlShipping.Items.FindByValue(shipId.ToString());
                            if (li != null)
                            {
                                ddlShipping.SelectedIndex = ddlShipping.Items.IndexOf(li);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Method to Get Quantity
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        /// <summary>
        public int GetQuantity(object item, int addressId)
        {
            if (item is ZNodeShoppingCartItem)
            {
                var cartItem = item as ZNodeShoppingCartItem;
                return cartItem.OrderShipments.Where(x => x.AddressID == addressId).Sum(x => x.Quantity);
            }

            return 0;
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Returns boolean value to enable Validator to check against the Quantity textbox
        /// It checks for AllowBackOrder and TrackInventory settings for this product,and returns the value
        /// </summary>
        /// <param name="guid">GUID of the shopping cart item.</param>
        /// <returns>Returns true if stock validator enabled else false.</returns>
        public bool EnableStockValidator(string guid)
        {
            // Get Shopping Cart Item
            ZNodeShoppingCartItem item = this._shoppingCart.GetItem(guid);

            if (item != null)
            {
                // Allow Back Order
                if (item.Product.AllowBackOrder && item.Product.TrackInventoryInd)
                {
                    return false;
                }
                else if (item.Product.AllowBackOrder == false && item.Product.TrackInventoryInd == false)
                {
                    // Don't track inventory
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Returns Quantity on Hand (inventory stock value)
        /// </summary>
        /// <param name="guid">GUID of the shopping cart item.</param>
        /// <returns>Returns true if enough quantity avaiable else false.</returns>
        public int CheckInventory(string guid)
        {
            // Get Shopping Cart Item
            ZNodeShoppingCartItem item = this._shoppingCart.GetItem(guid);
            
            if (item != null)
            {
                // Get Current Qunatity by Subtracting QunatityOrdered from Quantity On Hand(Inventory)
                int orderedQuantity = this._shoppingCart.GetQuantityOrdered(item);
                int currentQuantity = item.Product.QuantityOnHand - orderedQuantity;

                if (currentQuantity <= 0)
                {
                    return 0;
                }
                else
                {
                    foreach (ZNodeAddOn AddOn in item.Product.SelectedAddOns.AddOnCollection)
                    {
                        // Loop through the Add-on values for each Add-on
                        foreach (ZNodeAddOnValue AddOnValue in AddOn.ZNodeAddOnValueCollection)
                        {


                            // Check for back-order and track inventory settings
                            if ((!AddOn.AllowBackOrder) && AddOn.TrackInventoryInd)
                            {
                                int addonQuantityOnHand = ProductAdmin.GetQuantity(AddOnValue.SKU);
                                int currentAddonQuantity = addonQuantityOnHand;

                                if (this._shoppingCart != null)
                                {
                                    // Get Current Qunatity 
                                    currentAddonQuantity -= this._shoppingCart.GetQuantityAddOnOrdered(item);
                                }

                                if (currentQuantity > currentAddonQuantity)
                                    return currentAddonQuantity <= 0 ? 0 : currentAddonQuantity;
                            }
                        }
                    }

                    return currentQuantity;
                }
            }
            else
            {
                return 1;
            }
        }


        #endregion

        #region Page Load


        /// <summary>
		/// Page load event
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void Page_Load(object sender, EventArgs e)
        {
	        lblMinQuantiryErrorMsg.Text = string.Empty;
			lblMaxQuantity.Text = string.Empty;
			ApplyCouponGo.ImageUrl = "~/Themes/images/Apply.gif";

			// Get the user account from session
			ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();

			ZNode.Libraries.DataAccess.Service.PaymentSettingService _pmtServ = new ZNode.Libraries.DataAccess.Service.PaymentSettingService();
			ZNode.Libraries.DataAccess.Entities.TList<ZNode.Libraries.DataAccess.Entities.PaymentSetting> _pmtSetting = _pmtServ.GetAll();

			PaymentErrorMsg.Visible = false;

			// Check whether this profile has payment options
			if (Request.QueryString["ErrorMsg"] != null)
			{
				if (Request.QueryString["ErrorMsg"] == "1")
				{
                    PaymentErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPayment").ToString();
				}

				PaymentErrorMsg.Visible = true;

			}
		 
		}
		#endregion

		#region General Events
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkChangeShipTo_Click(object sender, EventArgs e)
        {
            if (this.ChangeShipToChanged != null)
            {
                this.ChangeShipToChanged(sender, e);
            }
        }
        /// <summary>
        /// Occurs when Apply button clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Btnapply_click(object sender, EventArgs e)
        {
            if (this._shoppingCart != null)
            {
                this._shoppingCart.AddCouponCode(this.CouponCode); 
            }
        }

        /// <summary>
        /// DropDown Index change event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Quantity_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this._shoppingCart != null)
            {
                this.UpdateQuantity(sender, e);

				if (this.ShippingSelectedIndexChanged != null)
				{
					this.ShippingSelectedIndexChanged(sender, e);
				}
            }
        }

        /// <summary>
        /// Shipping option selected index changed event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstShipping_SelectedIndexChanged(object sender, EventArgs e)
        {
			var ddl = sender as DropDownList;

			this.SetShippingSettings(ddl, this._portalCart.AddressCarts.Find(x => x.AddressID == Convert.ToInt32(ddl.ID.Replace("lstShipping_", ""))));

            if (this.ShippingSelectedIndexChanged != null)
            {
                this.ShippingSelectedIndexChanged(sender, e);
            }
        }
        #endregion

        #region Grid Events

        /// <summary>
        /// Grid command event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cart_RowCommand(object sender, GridViewCommandEventArgs e)
        {
			if (this._portalCart == null)
			{
				this._portalCart = this._shoppingCart.PortalCarts.FirstOrDefault();
			}

			// The remove link was clicked
			if (e.CommandName.Equals("remove"))
			{
				string[] guidAddressId = e.CommandArgument.ToString().Split('|');


				bool status = this._portalCart.RemoveAddressCartItem(guidAddressId[1], guidAddressId[0]);

				if (status)
				{
					// Triggers parent page event
					this.Bind();

					if (this.ShippingSelectedIndexChanged != null)
					{
						this.ShippingSelectedIndexChanged(sender, e);
					}
				}
				else
				{
					if (this._portalCart != null)
					{
						lblMinQuantiryErrorMsg.Visible = true;
                        lblMinQuantiryErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorMinimumQuantity").ToString();
					}
					 
				}
			}
        }

        protected void UxCart_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this._shoppingCart != null)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    // Retrieve the dropdownlist control from the first column
                    var qty = e.Row.Cells[0].FindControl("uxQty") as DropDownList;
                    if (qty != null)
                    {
                        qty.Attributes.Add("rowid", this.rowNumber.ToString());
                    }

                    this.rowNumber++;
                }
            }
        }

        /// <summary>
        /// Repeater ItemData bound event - Grouping items based on Shipping Address
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rptShiping_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var uxCart = (GridView)e.Item.FindControl("uxCart");

                var addressitem = e.Item.DataItem as ZNodeMultipleAddressCart;
				var taxTotal = (Label)e.Item.FindControl("lblTaxCost");
				var taxTotalText = (Label)e.Item.FindControl("lblTaxCostText");
                var ddlShipping = (DropDownList)e.Item.FindControl("lstShipping");
                ddlShipping.ID = "lstShipping_" + addressitem.AddressID;
                var shipping = (Label)e.Item.FindControl("lblShipping");
                var shippingText = (Label)e.Item.FindControl("lblShippingText");
                var shippingBy = (Label)e.Item.FindControl("lblShipBy");
				var subTotal = (Label)e.Item.FindControl("lblSubtotal");
				var subTotalText = (Label)e.Item.FindControl("lblSubtotalText");
                var shippingTo = (Label)e.Item.FindControl("lblShippto");
                var lblShippingAddress = (Label)e.Item.FindControl("lblShippingAddress");
                var linkChange = (LinkButton)e.Item.FindControl("lnkChange");
                //linkChange.HRef = "~/address.aspx?ItemId=" + addressitem.AddressID;


                if (ddlShipping.Items.Count == 0)
                {
                    var shippingAddress =
                       this.UserAccount.Addresses.FirstOrDefault(x => x.AddressID == addressitem.AddressID);

                    string countryCode = shippingAddress != null ? shippingAddress.CountryCode : string.Empty;
                    this.BindShipping(ddlShipping, countryCode);
                
                    this.SetShippingSettings(ddlShipping, addressitem);
                }

                //var addressCart = this._portalCart.AddressCarts.FirstOrDefault(x => x.AddressID == addressitem.AddressID);
                if (this._portalCart.AddressCarts.Count > 1)
                {
					shippingText.Visible = addressitem.ShippingCost > 0 ? true : false;
					shipping.Visible = addressitem.ShippingCost > 0 ? true : false;
                    shippingTo.Visible = true;
                    lblShippingAddress.Visible = true;
                    linkChange.Visible = true;
					subTotal.Visible = true;
					subTotalText.Visible = true;
					taxTotal.Visible = addressitem.TaxCost > 0 ? true : false;
					taxTotalText.Visible = addressitem.TaxCost > 0 ? true : false;
					shipping.Text = addressitem.ShippingCost.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
					subTotal.Text = addressitem.SubTotal.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
					taxTotal.Text = addressitem.TaxCost.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                }
                else
                {
					lblShippingAddress.Visible = false;
					shipping.Text = addressitem.ShippingCost.ToString("c") + ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencySuffix();
                }

				uxCart.DataSource = addressitem.ShoppingCartItems;
                uxCart.ShowHeader = e.Item.ItemIndex == 0;
                uxCart.DataBind();
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Check for Qunatity ordered against Quantity On Hand
        /// </summary>
        /// <param name="item">ZNodeShoppingCartItem object</param>
        /// <returns>Returns true if quantity updated else false.</returns>
		private bool UpdateQuantity(ZNodeShoppingCartItem item)
		{
			ZNodeShoppingCartItem _ShoppingCartItem = null;
			int QunatityOrdered = 0;

			foreach (RepeaterItem repeateritem in rptShiping.Items)
			{
				GridView uxCart = repeateritem.FindControl("uxCart") as GridView;

				// Loop through the grid rows
				foreach (GridViewRow row in uxCart.Rows)
				{
					DropDownList hdnQty = row.FindControl("uxQty") as DropDownList;
					int Qty = int.Parse(hdnQty.Text);

					// Find the hidden form field and get product GUID
					HiddenField hdnGUID = row.FindControl("GUID") as HiddenField;
					string[] cartId = hdnGUID.Value.Split('|');
					string GUID = cartId[0];

					// Get Shopping cart item using GUID value
					var addressCart = this.ShoppingCartObject.PortalCarts.Select(x => x.AddressCarts
													   .FirstOrDefault(y => y.AddressID.ToString() == cartId[1])).FirstOrDefault();
					_ShoppingCartItem = addressCart.ShoppingCartItems.Cast<ZNodeShoppingCartItem>().FirstOrDefault(x => x.GUID == GUID);					

					// If product is removed or update with 0,then this object is set to null.
					// (Above GetItem() method will return the value for this object)
					if (_ShoppingCartItem != null)
					{
						// Match Product
						if (item.Product.ProductID == _ShoppingCartItem.Product.ProductID)
						{
							// Check Product has attributes or not 
							if (item.Product.SelectedSKU.SKUID > 0)
							{
								// If Product has attributes then check for SKUID
								if (item.Product.SelectedSKU.SKUID == _ShoppingCartItem.Product.SelectedSKU.SKUID)
								{
									QunatityOrdered += Qty;
								}
							}
							else
							{
								// Product has no attributes
								// Increment QuantityOrdered value
								QunatityOrdered += Qty;
							}
						}
					}
				}
			}
			// Check Qunatity on hand is greater than Qunatity Ordered value
			if (item.Product.QuantityOnHand < QunatityOrdered)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		/// <summary>
		/// Re-calculate shipping cost
		/// </summary>
		private void SetShippingSettings(DropDownList ddlShipping, ZNodeMultipleAddressCart objShoppingCart)
		{
			//var objShoppingCart = this._portalCart.AddressCarts.FirstOrDefault(x => x.AddressID == addressId);

			if (ddlShipping.Items.Count > 0)
			{
				int shippingID = int.Parse(ddlShipping.SelectedValue);

				if (objShoppingCart != null)
				{
					// Set shipping name in shopping cart object
					objShoppingCart.Shipping.ShippingName = ddlShipping.SelectedItem.Text;
					objShoppingCart.Shipping.ShippingID = shippingID;
					SetPaymentControlData(objShoppingCart);
					objShoppingCart.Calculate();
					//Session[string.Format("{0}_{1}_{2}", ZNodeSessionKeyType.ShoppingCart.ToString(), this.PortalID, addressId)] = objShoppingCart;
				}
				else
				{
					// Set shipping name in shopping cart object
					this._shoppingCart.Shipping.ShippingName = ddlShipping.SelectedItem.Text;
					this._shoppingCart.Shipping.ShippingID = shippingID;
					this._shoppingCart.Calculate();
				}
			}
		}

		/// <summary>
		/// Get the user address.
		/// </summary>
		/// <param name="addressId">Address ID</param>
		/// <returns>Returns the Address Text</returns>
		public string GetAddress(string addressId)
		{
			string addressText = string.Empty;

			AddressService addressService = new AddressService();
			Address address = addressService.GetByAddressID(Convert.ToInt32(addressId));
			if (address != null)
			{
				addressText = address.ToString().Replace("<BR>", ",");
			}

			return addressText;
		}

        #endregion
    }
}