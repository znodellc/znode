﻿using System;
using System.Configuration;
using System.Data;
using System.Web.UI.WebControls;
using Znode.Engine.Common;

namespace  Znode.Engine.Admin.Controls.Default
{
    /// <summary>
    /// Represents the Category Auto Complete user control class
    /// </summary>
    public partial class CategoryAutoComplete : System.Web.UI.UserControl
    {
        #region Properties

        private int DropdownItemCount = int.Parse(ConfigurationManager.AppSettings["DropdownMaxItemCount"]);

        /// <summary>
        /// Gets or sets the Width
        /// </summary>
        public string Width
        {
            get
            {
                return txtCategory.Width.ToString();
            }

            set
            {
                txtCategory.Width = new Unit(value);
            }
        }

        /// <summary>
        /// Gets or sets the Value
        /// </summary>
        public string Value
        {
            get
            {
                if (ddlCategory.Visible)
                {
                    return ddlCategory.SelectedValue;
                }
                else
                {
                    bool assigned = false;

                    // Empty, if hidden value is 0 or -1
                    if (string.Equals(hdnCategoryId.Value, "0") && string.Equals(hdnCategoryId.Value, "-1"))
                    {
                        hdnCategoryId.Value = string.Empty;
                    }

                    // Check the name is in value;
                    if (!string.IsNullOrEmpty(txtCategory.Text.Trim()) && string.IsNullOrEmpty(hdnCategoryId.Value))
                    {
                        foreach (DataRow drow in this.CategoryListTable.Select(string.Concat("Name='", txtCategory.Text.Trim(), "'")))
                        {
                            hdnCategoryId.Value = drow["CategoryId"].ToString();
                            assigned = true;
                            break;
                        }
                    }
                    else if (!string.IsNullOrEmpty(txtCategory.Text.Trim()) && !string.IsNullOrEmpty(hdnCategoryId.Value))
                    {
                        foreach (DataRow drow in this.CategoryListTable.Select(string.Concat("Name='", txtCategory.Text.Trim().Replace("'", "''"), "' AND CategoryID=", hdnCategoryId.Value)))
                        {
                            hdnCategoryId.Value = drow["CategoryId"].ToString();
                            assigned = true;
                            break;
                        }
                    }

                    if (string.IsNullOrEmpty(txtCategory.Text.Trim()))
                    {
                        hdnCategoryId.Value = "0";
                        assigned = true;
                    }

                    // If it is not valid, assign to -1 to select nil record.
                    if (!assigned)
                    {
                        hdnCategoryId.Value = this.ForceAutoCompleteTextBox ? "0" : "-1";
                    }
                }

                return hdnCategoryId.Value;
            }

            set
            {
                if (ddlCategory.Visible)
                {
                    ListItem item = ddlCategory.Items.FindByValue(value);
                    if (item != null)
                    {
                        ddlCategory.SelectedValue = value;
                    }
                    else
                    {
                        ddlCategory.SelectedIndex = 0;
                    }
                }
                else
                {
                    hdnCategoryId.Value = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the Text
        /// </summary>
        public string Text
        {
            get
            {
                return txtCategory.Text;
            }

            set
            {
                txtCategory.Text = value;
            }
        }

        /// <summary>
        /// Gets the Client Id
        /// </summary>
        public string ClientId
        {
            get
            {
                return hdnCategoryId.ClientID;
            }
        }

        /// <summary>
        /// Gets the txtClientId
        /// </summary>
        public string txtClientId
        {
            get
            {
                if (ddlCategory.Visible)
                {
                    return ddlCategory.ClientID;
                }

                return txtCategory.ClientID;
            }
        }

        /// <summary>
        /// Sets the ContextKey
        /// </summary>
        public string ContextKey
        {
            set
            {
                if (Session["CatalogId"] != null)
                {
                    if (Session["CatalogId"].ToString() != value)
                    {
                        System.Web.HttpContext.Current.Session["CategoryList"] = null;
                    }
                }

                AutoCompleteExtender2.ContextKey = value;
                Session["CatalogId"] = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether Required Field validator is needed or not
        /// </summary>
        public bool IsRequired
        {
            get
            {
                return RequiredFieldValidator9.Visible;
            }

            set
            {
                if (ddlCategory.Visible)
                {
                    RequiredFieldValidator9.ControlToValidate = "ddlCategory";
                }

                RequiredFieldValidator9.Visible = value;
            }
        }

        public bool ForceAutoCompleteTextBox
        {
            get
            {
                return bool.Parse(ForceAutoComplete.Value);
            }

            set
            {
                ForceAutoComplete.Value = value.ToString();
            }
        }

        public DataTable CategoryListTable
        {
            get
            {
                if (System.Web.HttpContext.Current.Session["CategoryList"] == null)
                {
                    ZNode.Libraries.DataAccess.Custom.CategoryHelper categoryHelper = new ZNode.Libraries.DataAccess.Custom.CategoryHelper();

                    int CatalogId = 0;
                    System.Data.DataSet ds;

                    // Get Category
                    if (Session["CatalogId"] != null)
                    {
                        CatalogId = int.Parse(Session["CatalogId"].ToString());
                    }

                    if (CatalogId > 0)
                    {
                        ds = categoryHelper.GetCategoryByCatalogID(CatalogId);
                    }
                    else
                    {
                        ds = categoryHelper.GetCategories();
                    }

                    for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                    {
                        ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                    }

                    System.Web.HttpContext.Current.Session["CategoryList"] = ds.Tables[0];
                    return ds.Tables[0];
                }
                else
                {
                    return (DataTable)System.Web.HttpContext.Current.Session["CategoryList"] as DataTable;
                }
            }
        }
        #endregion

        /// <summary>
        /// Populate DropDown Method
        /// </summary>
        public void PopulateDropDown()
        {
            if (System.Web.HttpContext.Current.Session["CategoryList"] == null)
            {
                ZNode.Libraries.DataAccess.Custom.CategoryHelper categoryHelper = new ZNode.Libraries.DataAccess.Custom.CategoryHelper();

                int CatalogId = 0;
                System.Data.DataSet ds;

                // Get Category
                if (Session["CatalogId"] != null)
                {
                    CatalogId = int.Parse(Session["CatalogId"].ToString());
                }

                if (CatalogId > 0)
                {
                    ds = categoryHelper.GetCategoryByCatalogID(CatalogId);
                }
                else
                {
                    ds = categoryHelper.GetCategories();
                }

                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                }

                System.Web.HttpContext.Current.Session["CategoryList"] = ds.Tables[0];

                ddlCategory.Visible = false;

                if (!this.ForceAutoCompleteTextBox && this.CategoryListTable.Rows.Count < this.DropdownItemCount)
                {
                    ddlCategory.Visible = true;
                    txtCategory.Visible = false;
                    AutoCompleteExtender2.Enabled = false;
                    ddlCategory.DataSource = UserStoreAccess.CheckStoreAccess(this.CategoryListTable, true);
                    ddlCategory.DataTextField = "Name";
                    ddlCategory.DataValueField = "CategoryID";
                    ddlCategory.DataBind();

                    if (!this.IsRequired)
                    {
                        ListItem allItem = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource","DropdownTextAll").ToString().ToUpper(), string.Empty);
                        ddlCategory.Items.Insert(0, allItem);
                    }
                }
            }
        }

        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                this.PopulateDropDown();
            }
        }
    }
}