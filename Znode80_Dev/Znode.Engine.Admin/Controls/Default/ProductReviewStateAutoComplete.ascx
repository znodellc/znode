﻿<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="Znode.Engine.Admin.Controls.Default.ProductReviewStateAutoComplete" CodeBehind="ProductReviewStateAutoComplete.ascx.cs" %>

<script type="text/javascript">

    function AutoComplete_ProductReviewStateSelected(source, eventArgs) {
        var hiddenTextValue = $get("<%= ProductReviewStateId.ClientID %>");
        hiddenTextValue.value = eventArgs.get_value();


        var hAutoPostBack = $get("<%= hdnAutoPostBack.ClientID %>");
        if (hAutoPostBack.value == "True") {
            __doPostBack('AutoComplete_OnSelectedIndexChanged', hiddenTextValue, eventArgs);
        }
    }

    function AutoComplete_ProductReviewStateShowing(source, eventArgs) {
        var hiddenTextValue = $get("<%=ProductReviewStateId.ClientID %>");
        hiddenTextValue.value = "";
    }

    function ProductReviewState_OnBlur(obj) {
        var hiddenTextValue = $get("<%=ProductReviewStateId.ClientID %>");
        if (obj.value == "") {
            hiddenTextValue.value = "";
        }
        if (hiddenTextValue.value == "") {
            obj.value = "";
        }
    }
</script>

<asp:TextBox ID="txtProductReviewState" runat="server" onblur="ProductReviewState_OnBlur(this)"></asp:TextBox>
<asp:HiddenField runat="server" ID="ProductReviewStateId" />
<asp:HiddenField runat="server" ID="hdnAutoPostBack" Value="false" />
<ajaxToolKit:AutoCompleteExtender ID="autoCompleteExtender3" runat="server" TargetControlID="txtProductReviewState"
    ServicePath="ZNodeMultifrontService.asmx" ServiceMethod="GetProductReviewStates" UseContextKey="true"
    MinimumPrefixLength="1" EnableCaching="false" CompletionSetCount="10" CompletionInterval="1"
    FirstRowSelected="false" OnClientItemSelected="AutoComplete_ProductReviewStateSelected"
    OnClientShowing="AutoComplete_ProductReviewStateShowing" />
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtProductReviewState"
    ErrorMessage='<%$ Resources:ZnodeAdminResource, SelectProductType %>' CssClass="Error" Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
<asp:DropDownList runat="server" ID="ddlProductReviewState" Visible="false"></asp:DropDownList>
