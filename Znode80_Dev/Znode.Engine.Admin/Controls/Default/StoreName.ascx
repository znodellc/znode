<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Controls.Default.StoreName" CodeBehind="StoreName.ascx.cs" %>
<div class="FieldStyle">
    <asp:Localize runat="server" ID="SelectStore" Text='<%$ Resources:ZnodeAdminResource, SelectStore %>'></asp:Localize>
</div>
<div class="ValueStyle">
    <asp:DropDownList ID="StoreList" runat="server" OnSelectedIndexChanged="Storelist_SelectedIndexChanged">
    </asp:DropDownList>
    <asp:CheckBoxList ID="StoreCheckList" runat="server" OnSelectedIndexChanged="Storelist_SelectedIndexChanged">
    </asp:CheckBoxList>
</div>
<asp:Panel ID="pnlLocale" runat="server">
    <div class="FieldStyle">
        <asp:Localize runat="server" ID="SelectLocale" Text='<%$ Resources:ZnodeAdminResource, SelectLocale %>'></asp:Localize>
    </div>
    <div class="ValueStyle">
        <asp:DropDownList ID="StoreLocale" runat="server">
        </asp:DropDownList>
    </div>
</asp:Panel>
