﻿using System;
using System.IO;
using System.Web;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Controls.Default
{
    /// <summary>
    /// Represents the Image Uploader user control class
    /// </summary>
    public partial class ImageUploader : System.Web.UI.UserControl
    {
        private string FullName = string.Empty;
        private string _AppendToFileName = string.Empty;
        private bool _UseSiteConfig = false;

        #region Public Properties

        /// <summary>
        /// Gets the Http Posted File.
        /// </summary>
        public HttpPostedFile PostedFile
        {
            get
            {
                return UploadImage.PostedFile;
            }
        }
        
        /// <summary>
        /// Gets or sets the Http Posted File.
        /// </summary>
        public string AppendToFileName
        {
            get
            {
                return this._AppendToFileName;
            }
           
            set
            {
                this._AppendToFileName = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to use Site Config Path.
        /// </summary>
        public bool UseSiteConfig
        {
            get
            {
                return this._UseSiteConfig;
            }
            
            set
            {
                this._UseSiteConfig = value;
            }
        }

        /// <summary>
        /// Gets or sets the posted file information
        /// </summary>
        public FileInfo FileInformation
        {
            get
            {
                if (ViewState["FileName"] != null)
                {
                    return ViewState["FileName"] as FileInfo;
                }
                else if (UploadImage.PostedFile != null && pnlSaveOption.Visible)
                {
                    return new FileInfo(txtFileName.Text + Path.GetExtension(UploadImage.PostedFile.FileName));
                }
                else if (UploadImage.PostedFile != null && !string.IsNullOrEmpty(UploadImage.PostedFile.FileName))
                {
                    return new FileInfo(Path.GetFileNameWithoutExtension(UploadImage.PostedFile.FileName) + Path.GetExtension(UploadImage.PostedFile.FileName));
                }
                else
                {
                    return null;
                }
            }
            
            set
            {
                ViewState["FileName"] = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Save Image Method
        /// </summary>
        /// <returns>Returns true if image saved otheriwse false</returns>
        public bool SaveImage()
        {
            return SaveImage(string.Empty,string.Empty, string.Empty);
        }

        /// <summary>
        /// Save Image Method
        /// </summary>
        /// <returns>Returns true if image saved otheriwse false</returns>
        public bool SaveImage(string userType, string portalID, string accountID)
        {
            bool isSuccess = false;

            string fileName = pnlSaveOption.Visible && rdoNewFileName.Checked ? txtFileName.Text : Path.GetFileNameWithoutExtension(UploadImage.PostedFile.FileName);
            //this.FullName = this.GetFullName(fileName + this._AppendToFileName + Path.GetExtension(UploadImage.PostedFile.FileName));
            this.FullName = this.GetFullName(fileName + this._AppendToFileName + Path.GetExtension(UploadImage.PostedFile.FileName), userType, portalID, accountID);

            try
            {
                if (!ZNodeStorageManager.Exists(this.FullName))
                {
                    byte[] imageData = new byte[UploadImage.PostedFile.InputStream.Length];
                    UploadImage.PostedFile.InputStream.Read(imageData, 0, (int)UploadImage.PostedFile.InputStream.Length);
                    ZNodeStorageManager.WriteBinaryStorage(imageData, this.FullName);
                    isSuccess = true;
                }
                else
                {
                    // Message "Filename already exist" shown.
                    if (pnlSaveOption.Visible && this.IsPostBack)
                    {
                        if (rdoNewFileName.Checked)
                        {
                            this.FullName = this.GetFullName(txtFileName.Text + Path.GetExtension(UploadImage.PostedFile.FileName));
                        }

                        // If overwrite selected then we can use the same file name.
                        byte[] imageData = new byte[UploadImage.PostedFile.InputStream.Length];
                        UploadImage.PostedFile.InputStream.Read(imageData, 0, (int)UploadImage.PostedFile.InputStream.Length);
                        ZNodeStorageManager.WriteBinaryStorage(imageData, this.FullName);
                        isSuccess = true;
                    }
                    else
                    {
                        byte[] imageData = new byte[UploadImage.PostedFile.InputStream.Length];
                        UploadImage.PostedFile.InputStream.Read(imageData, 0, (int)UploadImage.PostedFile.InputStream.Length);
                        ZNodeStorageManager.WriteBinaryStorage(imageData, this.FullName);
                        isSuccess = true;
                    }
                }
                
                this.FileInformation = new FileInfo(this.FullName);
                pnlSaveOption.Visible = !isSuccess;
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
                isSuccess = false;
            }

            return isSuccess;
        }

        /// <summary>
        /// Check whether the same file name exist in original folder.
        /// </summary>
        /// <returns>Returns true if file name doesn't exist in original folder, otherwise false.</returns>    
        public bool IsFileNameValid()
        {
            bool status = false;
            if (UploadImage.PostedFile != null)
            {
                string fileName = pnlSaveOption.Visible ? txtFileName.Text : Path.GetFileNameWithoutExtension(UploadImage.PostedFile.FileName);
                if (UploadImage.PostedFile.FileName == string.Empty)
                {
                    // File not selected 
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidImage").ToString();
                    this.FileInformation = null;
                    pnlSaveOption.Visible = true;
                    UploadImage.Focus();
                }
                else
                {
                    this.FileInformation = new FileInfo(fileName + this._AppendToFileName + Path.GetExtension(UploadImage.PostedFile.FileName));
                    this.FullName = this.GetFullName(this.FileInformation.Name);

                    if (ZNodeStorageManager.Exists(this.FullName))
                    {
                        // Duplicate file name found. Clear the stored view state information.
                        lblMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFileExist").ToString(), this.FileInformation.Name);
                        this.FileInformation = new FileInfo(this.FullName);
                        status = true;
                    }
                    else
                    {
                        this.FileInformation = new FileInfo(this.FullName);
                        status = true;
                    }
                }
            }
            
            return status;
        }
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (UploadImage.PostedFile != null)
                {
                    UploadImage.Attributes.Add("Value", UploadImage.PostedFile.FileName);
                }

                rdoOverwrite.Attributes.Add("onClick", "setEnabled()");
                rdoNewFileName.Attributes.Add("onClick", "setEnabled()");
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Get the file and return the full physical path with filename.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>Returns the full physical path with file name.</returns>
        private string GetFullName(string fileName)
        {
            if (this._UseSiteConfig)
            {
                return ZNodeConfigManager.SiteConfig.ImageNotAvailablePath + fileName;
            }
            else
            {
                return ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + fileName;
            }
        }
        /// <summary>
        /// Get the file and return the full physical path with filename.
        /// </summary>
        /// <param name="fileName">Name of the file.</param>
        /// <returns>Returns the full physical path with file name.</returns>
        private string GetFullName(string fileName, string userType, string portalID, string accountID)
        {
            if (this._UseSiteConfig)
            {
                return ZNodeConfigManager.SiteConfig.ImageNotAvailablePath + fileName;
            }
            else
            {
                if (userType == "sadmin")
                {
                    //return ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + fileName;
                    return ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + "Turnkey/" + portalID + "/" + fileName;
                }
                else if (userType == "madmin")
                {
                    return ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + "Mall/" + accountID + "/" + fileName;
                }
                else if (userType == "fadmin")
                {
                    return ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + "Turnkey/" + portalID + "/" + fileName;
                }
                else
                {
                    return ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + fileName;
                }
            }
        }
        #endregion
    }
}