﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Controls.Default.ProductAutoComplete" CodeBehind="ProductAutoComplete.ascx.cs" %>

<script type="text/javascript">

    function AutoComplete_ProductSelected(source, eventArgs) {
        var hiddenTextValue = $get("<%= ProductId.ClientID %>");
        hiddenTextValue.value = eventArgs.get_value();

        var hAutoPostBack = $get("<%= hdnAutoPostBack.ClientID %>");
        if (hAutoPostBack.value == "True") {
            __doPostBack('AutoComplete_OnSelectedIndexChanged', hiddenTextValue, eventArgs);
        }
    }

    function AutoComplete_ProductShowing(source, eventArgs) {
        var hiddenTextValue = $get("<%=ProductId.ClientID %>");
        hiddenTextValue.value = "";
    }

    function Product_OnBlur(obj) {
        var hiddenTextValue = $get("<%=ProductId.ClientID %>");
        if (obj.value == "") {
            hiddenTextValue.value = "";
        }
        if (hiddenTextValue.value == "") {
            obj.value = "";
        }
    }
</script>

<asp:TextBox ID="txtProduct" runat="server"></asp:TextBox>
<div>
    <asp:RegularExpressionValidator ID="regProductName" runat="server" ControlToValidate="txtProduct"
        ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidProductName %>' Display="Dynamic" ValidationExpression="[0-9A-Za-z\s\',.:&%#$@_-]+"
        CssClass="Error"></asp:RegularExpressionValidator>
</div>
<asp:HiddenField runat="server" ID="ProductId" />
<asp:HiddenField runat="server" ID="hdnAutoPostBack" Value="false" />
<ajaxToolKit:AutoCompleteExtender ID="autoCompleteExtender3" runat="server" TargetControlID="txtProduct"
    ServicePath="~/Controls/Default/ZNodeMultifrontService.asmx" ServiceMethod="GetProducts" UseContextKey="true"
    MinimumPrefixLength="1" EnableCaching="false" CompletionSetCount="10" CompletionInterval="1"
    FirstRowSelected="false" OnClientItemSelected="AutoComplete_ProductSelected"
    OnClientShowing="AutoComplete_ProductShowing" />
<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtProduct"
    ErrorMessage='<%$ Resources:ZnodeAdminResource, SelectProductType %>' CssClass="Error" Display="Dynamic" Visible="false"></asp:RequiredFieldValidator>
