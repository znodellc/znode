﻿using System.Web;
using System.Web.UI.WebControls;
using Znode.Engine.Promotions;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Controls.Default.Providers
{
	public partial class ProviderBase : System.Web.UI.UserControl
	{
		protected ZNodeEncryption Encryption { get; set; }
		protected bool IsFranchiseAdmin { get; set; }

		public ProviderBase()
		{
			Encryption = new ZNodeEncryption();
			IsFranchiseAdmin = HttpContext.Current.Request.RawUrl.ToLower().Contains("franchiseadmin");
		}

		protected string GetEncryptedId(object id)
		{
			return IsFranchiseAdmin ? HttpUtility.UrlEncode(Encryption.EncryptData(id.ToString())) : id.ToString();
		}

		protected string GetDecryptedId(object id)
		{
			return IsFranchiseAdmin ? HttpUtility.UrlEncode(Encryption.DecryptData(id.ToString())) : id.ToString();
		}

		protected void DecodeListItems(DropDownList dropDownList)
		{
			foreach (ListItem item in dropDownList.Items)
			{
				item.Text = Server.HtmlDecode(item.Text);
			}
		}

		protected TList<DiscountType> GetDiscountTypes()
		{
			var list = new TList<DiscountType>();

			// First get types from the database
			var promoAdmin = new PromotionAdmin();
			var discountTypes = promoAdmin.GetAllDiscountTypes().FindAll(DiscountTypeColumn.ActiveInd, true);

			// If franchise admin, cross reference with cached types to check for AvailableForFranchise property
			if (IsFranchiseAdmin)
			{
				var availablePromoTypes = ZnodePromotionManager.GetAvailablePromotionTypes();
				foreach (var p in availablePromoTypes)
				{
					foreach (var item in discountTypes)
					{
						if (p.ClassName == item.ClassName && p.AvailableForFranchise)
						{
							list.Add(new DiscountType { DiscountTypeID = item.DiscountTypeID, Name = item.Name, ClassName = item.ClassName });
						}
					}
				}
			}
			else
			{
				// Not in franchise admin, so just use all types from the database
				list = discountTypes;
			}

			return list;
		}
	}
}