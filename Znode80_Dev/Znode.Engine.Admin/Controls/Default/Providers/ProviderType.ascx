﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Controls.Default.Providers.ProviderType" CodeBehind="ProviderType.ascx.cs" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<div class="LeftFloat" style="width: 70%">
    <h1>
        <asp:Label ID="lblTitle" runat="server" /></h1>
</div>

<div class="LeftFloat" align="right" style="width: 30%">
    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="btnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" CausesValidation="True" ID="btnTopSubmit" />
    <zn:Button runat="server" ButtonType="CancelButton" OnClick="btnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel %>" CausesValidation="False" ID="btnTopCancel" />

</div>

<div class="ClearBoth" align="left"></div>

<div class="FormView">
    <div>
        <zn:Spacer ID="Spacer1" runat="server" SpacerHeight="5" SpacerWidth="3" />
    </div>

    <div>
        <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error" EnableViewState="false" />
    </div>

    <div class="ClearBoth">
        <p>
            <asp:Label ID="lblDescription" runat="server" /></p>
    </div>

    <div>
        <zn:Spacer ID="Spacer2" runat="server" SpacerHeight="15" SpacerWidth="3" />
    </div>

    <asp:Panel ID="pnlAvailablePromotionTypes" runat="server" Visible="false">
        <div class="FieldStyle"><asp:Label runat="server" ID="lblAvailablePromotionTypes" Text="<%$ Resources:ZnodeAdminResource, DropdownTextAvailablePromotionTypes %>"></asp:Label></div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlAvailablePromotionTypes" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAvailablePromotionTypes_SelectedIndexChanged" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAvailableShippingTypes" runat="server" Visible="false">
        <div class="FieldStyle"><asp:Label runat="server" ID="lblAvailableShippingTypes" Text="<%$ Resources:ZnodeAdminResource, DropdownTextAvailableShippingTypes %>"></asp:Label></div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlAvailableShippingTypes" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAvailableShippingTypes_SelectedIndexChanged" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAvailableSupplierTypes" runat="server" Visible="false">
        <div class="FieldStyle"><asp:Label runat="server" ID="lblAvailableSupplierTypes" Text="<%$ Resources:ZnodeAdminResource, DropdownTextAvailableSupplierTypes %>"></asp:Label></div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlAvailableSupplierTypes" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAvailableSupplierTypes_SelectedIndexChanged" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlAvailableTaxTypes" runat="server" Visible="false">
        <div class="FieldStyle"><asp:Label runat="server" ID="lblAvailableTaxTypes" Text="<%$ Resources:ZnodeAdminResource, DropdownTextAvailableTaxTypes %>"></asp:Label></div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlAvailableTaxTypes" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlAvailableTaxTypes_SelectedIndexChanged" />
        </div>
    </asp:Panel>

    <div class="FieldStyle">
        <asp:Label runat="server" ID="lblClassName" Text="<%$ Resources:ZnodeAdminResource, TextClassName %>"></asp:Label> <span class="Asterix">*</span>
    </div>
    <div class="ValueStyle">
        <asp:TextBox ID="txtTypeClassName" runat="server" Width="350px" Enabled="False" />
        <asp:RequiredFieldValidator ID="rfvTypeClassName" runat="server" ControlToValidate="txtTypeClassName" ErrorMessage=" Required" CssClass="Error" Display="dynamic" />
    </div>

    <div class="FieldStyle">
        <asp:Label runat="server" ID="lblName" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleName %>"></asp:Label> <span class="Asterix">*</span>
    </div>
    <div class="ValueStyle">
        <asp:TextBox ID="txtTypeName" runat="server" Width="350px" />
        <asp:RequiredFieldValidator ID="rfvTypeName" runat="server" ControlToValidate="txtTypeName" ErrorMessage=" Required" CssClass="Error" Display="dynamic" />
    </div>

    <div class="FieldStyle">
        <asp:Label runat="server" ID="lblDesc" Text="<%$ Resources:ZnodeAdminResource, TextDescription %>"></asp:Label> <span class="Asterix">*</span>
    </div>
    <div class="ValueStyle">
        <asp:TextBox ID="txtTypeDesc" runat="server" Width="350px" Rows="4" TextMode="MultiLine" />
        <asp:RequiredFieldValidator ID="rfvTypeDesc" runat="server" ControlToValidate="txtTypeDesc" ErrorMessage=" Required" CssClass="Error" Display="dynamic" />
    </div>

    <div class="FieldStyle"></div>
    <div class="ValueStyleText">
        <asp:CheckBox ID="chkEnable" runat="server" Text="<%$ Resources:ZnodeAdminResource, SelectEnableType %>" Checked="True" />
    </div>
    <div>
        <zn:Spacer ID="Spacer3" runat="server" SpacerHeight="10" SpacerWidth="3" />
    </div>

    <div class="ClearBoth" align="left">
        <br />
    </div>

    <div>
        <zn:Button runat="server" ButtonType="SubmitButton" OnClick="btnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" CausesValidation="True" ID="btnBottomSubmit" />
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="btnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel %>" CausesValidation="False" ID="btnBottomCancel" />

    </div>

    <div>
        <zn:Spacer ID="Spacer4" runat="server" SpacerHeight="15" SpacerWidth="3" />
    </div>
</div>
