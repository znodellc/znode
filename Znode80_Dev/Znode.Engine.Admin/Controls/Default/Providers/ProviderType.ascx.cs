﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using Znode.Engine.Promotions;
using Znode.Engine.Shipping;
using Znode.Engine.Suppliers;
using Znode.Engine.Taxes;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Entities;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Admin.Controls.Default.Providers
{
    public partial class ProviderType : System.Web.UI.UserControl
    {
        private int _itemId;
        private string _providerType = "promotions";
        private int _activityEditTypeId = (int)ZNodeLogging.ErrorNum.EditObject;
        private int _activityCreateTypeId = (int)ZNodeLogging.ErrorNum.CreateObject;
        private ProviderTypeAdmin _providerTypeAdmin = new ProviderTypeAdmin();
        private List<IZnodePromotionType> _availablePromoTypes;
        private List<IZnodeShippingType> _availableShippingTypes;
        private List<IZnodeSupplierType> _availableSupplierTypes;
        private List<IZnodeTaxType> _availableTaxTypes;

        protected string RedirectUrl
        {
            get
            {
                var redirectUrl = "~/Secure/Advanced/Accounts/ProviderEngine/Default.aspx";

                if (_providerType == "promotions") redirectUrl += "?type=promotions";
                if (_providerType == "shipping") redirectUrl += "?type=shipping";
                if (_providerType == "suppliers") redirectUrl += "?type=suppliers";
                if (_providerType == "taxes") redirectUrl += "?type=taxes";

                return redirectUrl;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _availablePromoTypes = ZnodePromotionManager.GetAvailablePromotionTypes();
            _availableShippingTypes = ZnodeShippingManager.GetAvailableShippingTypes();
            _availableSupplierTypes = ZnodeSupplierManager.GetAvailableSupplierTypes();
            _availableTaxTypes = ZnodeTaxManager.GetAvailableTaxTypes();

            if (Request.Params["itemid"] != null)
            {
                _itemId = int.Parse(Request.Params["itemid"]);
            }

            if (Request.Params["type"] != null)
            {
                _providerType = Request.Params["type"];
            }

            if (!Page.IsPostBack)
            {
                if (_providerType == "promotions") LoadPromotionType();
                if (_providerType == "shipping") LoadShippingType();
                if (_providerType == "suppliers") LoadSupplierType();
                if (_providerType == "taxes") LoadTaxType();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            var saved = false;

            if (_providerType == "promotions") saved = SavePromotionType();
            if (_providerType == "shipping") saved = SaveShippingType();
            if (_providerType == "suppliers") saved = SaveSupplierType();
            if (_providerType == "taxes") saved = SaveTaxType();

            if (saved)
            {
                Response.Redirect(RedirectUrl);
            }
            else
            {
                lblErrorMsg.Text = GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnableToProcessRequest").ToString();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(RedirectUrl);
        }

        protected void ddlAvailablePromotionTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnTypesSelectedIndexChanged(_availablePromoTypes.ToProviderTypeList(), ddlAvailablePromotionTypes);
        }

        protected void ddlAvailableShippingTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnTypesSelectedIndexChanged(_availableShippingTypes.ToProviderTypeList(), ddlAvailableShippingTypes);
        }

        protected void ddlAvailableSupplierTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnTypesSelectedIndexChanged(_availableSupplierTypes.ToProviderTypeList(), ddlAvailableSupplierTypes);
        }

        protected void ddlAvailableTaxTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            OnTypesSelectedIndexChanged(_availableTaxTypes.ToProviderTypeList(), ddlAvailableTaxTypes);
        }

        private void OnTypesSelectedIndexChanged(List<IZnodeProviderType> availableProviderTypes, DropDownList providerTypesDropDownList)
        {
            if (providerTypesDropDownList.SelectedIndex == 0)
            {
                txtTypeClassName.Text = String.Empty;
                txtTypeName.Text = String.Empty;
                txtTypeDesc.Text = String.Empty;
            }
            else
            {
                foreach (var t in availableProviderTypes)
                {
                    if (t.ClassName == providerTypesDropDownList.SelectedValue)
                    {
                        txtTypeClassName.Text = t.ClassName;
                        txtTypeName.Text = t.Name;
                        txtTypeDesc.Text = t.Description;
                    }
                }
            }
        }

        private void BindAvailableTypes(List<IZnodeProviderType> availableProviderTypes, DropDownList providerTypesDropDownList, string providerType, string firstItemText, string firstItemValue)
        {
            var list = new List<IZnodeProviderType>();
            var databaseProviderTypes = new List<IZnodeProviderType>();

            if (providerType == "promotions")
            {
                var databasePromoTypes = _providerTypeAdmin.GetDiscountTypes();
                foreach (var item in databasePromoTypes)
                {
                    databaseProviderTypes.Add(new ZnodePromotionType { ClassName = item.ClassName, Name = item.Name, Description = item.Description });
                }
            }

            if (providerType == "shipping")
            {
                var databaseShippingTypes = _providerTypeAdmin.GetShippingTypes();
                foreach (var item in databaseShippingTypes)
                {
                    databaseProviderTypes.Add(new ZnodeShippingType { ClassName = item.ClassName, Name = item.Name, Description = item.Description });
                }
            }

            if (providerType == "suppliers")
            {
                var databaseSupplierTypes = _providerTypeAdmin.GetSupplierTypes();
                foreach (var item in databaseSupplierTypes)
                {
                    databaseProviderTypes.Add(new ZnodeSupplierType { ClassName = item.ClassName, Name = item.Name, Description = item.Description });
                }
            }

            if (providerType == "taxes")
            {
                var databaseTaxTypes = _providerTypeAdmin.GetTaxRuleTypes();
                foreach (var item in databaseTaxTypes)
                {
                    databaseProviderTypes.Add(new ZnodeTaxType { ClassName = item.ClassName, Name = item.Name, Description = item.Description });
                }
            }

            // Go through the lists and only add provider types not already in the database
            foreach (var x in availableProviderTypes)
            {
                var found = false;
                foreach (var y in databaseProviderTypes)
                {
                    if (!found)
                    {
                        if (x.ClassName == y.ClassName)
                        {
                            found = true;
                        }
                    }
                }

                if (!found)
                {
                    list.Add(x);
                }
            }

            // Bind the dropdown list
            providerTypesDropDownList.DataSource = list;
            providerTypesDropDownList.DataValueField = "ClassName";
            providerTypesDropDownList.DataTextField = "Name";
            providerTypesDropDownList.DataBind();

            // Add blank item to top of list
            providerTypesDropDownList.Items.Insert(0, new ListItem(firstItemText, firstItemValue));
            providerTypesDropDownList.SelectedIndex = 0;
        }

        private void LoadPromotionType()
        {
            BindAvailableTypes(_availablePromoTypes.ToProviderTypeList(), ddlAvailablePromotionTypes, "promotions",
                               "-- " + GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextSelectPromotionType").ToString() +
                               " --", "");

            lblTitle.Text = GetGlobalResourceObject("ZnodeAdminResource", "TitleConfigurePromotionType").ToString();
            lblDescription.Text = GetGlobalResourceObject("ZnodeAdminResource", "SubTextConfigurePromotionTypeMessage").ToString();

            pnlAvailablePromotionTypes.Visible = true;
            pnlAvailableShippingTypes.Visible = false;
            pnlAvailableSupplierTypes.Visible = false;
            pnlAvailableTaxTypes.Visible = false;

            // Check for edit mode
            if (_itemId > 0)
            {
                var promoType = _providerTypeAdmin.GetDiscountTypeById(_itemId);
                txtTypeClassName.Text = Server.HtmlDecode(promoType.ClassName);
                txtTypeName.Text = Server.HtmlDecode(promoType.Name);
                txtTypeDesc.Text = Server.HtmlDecode(promoType.Description);
                chkEnable.Checked = promoType.ActiveInd;
                pnlAvailablePromotionTypes.Visible = false;

                lblTitle.Text += " - " + promoType.Name.Trim();
                lblDescription.Text = String.Empty;
            }
        }

        private void LoadShippingType()
        {
            BindAvailableTypes(_availableShippingTypes.ToProviderTypeList(), ddlAvailableShippingTypes, "shipping", "-- " + GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextSelectShippingType").ToString() + " --", "");

            lblTitle.Text = GetGlobalResourceObject("ZnodeAdminResource", "TitleConfigureShippingType").ToString(); ;
            lblDescription.Text = GetGlobalResourceObject("ZnodeAdminResource", "SubTextConfigureShippingTypeMessage").ToString();

            pnlAvailablePromotionTypes.Visible = false;
            pnlAvailableShippingTypes.Visible = true;
            pnlAvailableSupplierTypes.Visible = false;
            pnlAvailableTaxTypes.Visible = false;

            // Check for edit mode
            if (_itemId > 0)
            {
                var shippingType = _providerTypeAdmin.GetShippingTypeById(_itemId);
                txtTypeClassName.Text = Server.HtmlDecode(shippingType.ClassName);
                txtTypeName.Text = Server.HtmlDecode(shippingType.Name);
                txtTypeDesc.Text = Server.HtmlDecode(shippingType.Description);
                chkEnable.Checked = shippingType.IsActive;
                pnlAvailableShippingTypes.Visible = false;

                lblTitle.Text += " - " + shippingType.Name.Trim();
                lblDescription.Text = String.Empty;
            }
        }

        private void LoadSupplierType()
        {
            BindAvailableTypes(_availableSupplierTypes.ToProviderTypeList(), ddlAvailableSupplierTypes, "suppliers", "-- " + GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextSelectSupplierType").ToString() + " --", "");

            lblTitle.Text = GetGlobalResourceObject("ZnodeAdminResource", "TitleConfigureSupplierType").ToString();
            lblDescription.Text = GetGlobalResourceObject("ZnodeAdminResource", "SubTextConfigureSupplierTypeMessage").ToString();

            pnlAvailablePromotionTypes.Visible = false;
            pnlAvailableShippingTypes.Visible = false;
            pnlAvailableSupplierTypes.Visible = true;
            pnlAvailableTaxTypes.Visible = false;

            // Check for edit mode
            if (_itemId > 0)
            {
                var supplierType = _providerTypeAdmin.GetSupplierTypeById(_itemId);
                txtTypeClassName.Text = Server.HtmlDecode(supplierType.ClassName);
                txtTypeName.Text = Server.HtmlDecode(supplierType.Name);
                txtTypeDesc.Text = Server.HtmlDecode(supplierType.Description);
                chkEnable.Checked = supplierType.ActiveInd;
                pnlAvailableSupplierTypes.Visible = false;

                lblTitle.Text += " - " + supplierType.Name.Trim();
                lblDescription.Text = String.Empty;
            }
        }

        private void LoadTaxType()
        {
            BindAvailableTypes(_availableTaxTypes.ToProviderTypeList(), ddlAvailableTaxTypes, "taxes", "-- " + GetGlobalResourceObject("ZnodeAdminResource", "SelectTaxType").ToString() + " --", "");

            lblTitle.Text = GetGlobalResourceObject("ZnodeAdminResource", "TitleConfigureTaxType").ToString();
            lblDescription.Text = GetGlobalResourceObject("ZnodeAdminResource", "SubTextConfigureTaxTypeMessage").ToString();

            pnlAvailablePromotionTypes.Visible = false;
            pnlAvailableShippingTypes.Visible = false;
            pnlAvailableSupplierTypes.Visible = false;
            pnlAvailableTaxTypes.Visible = true;

            // Check for edit mode
            if (_itemId > 0)
            {
                var taxType = _providerTypeAdmin.GetTaxRuleTypeById(_itemId);
                txtTypeClassName.Text = Server.HtmlDecode(taxType.ClassName);
                txtTypeName.Text = Server.HtmlDecode(taxType.Name);
                txtTypeDesc.Text = Server.HtmlDecode(taxType.Description);
                chkEnable.Checked = taxType.ActiveInd;
                pnlAvailableTaxTypes.Visible = false;

                lblTitle.Text += " - " + taxType.Name.Trim();
                lblDescription.Text = String.Empty;
            }
        }

        private bool SavePromotionType()
        {
            var saved = false;
            var promoType = _itemId > 0 ? _providerTypeAdmin.GetDiscountTypeById(_itemId) : new DiscountType();

            promoType.ClassType = GetPromotionClassType(txtTypeClassName.Text.Trim());
            promoType.ClassName = Server.HtmlEncode(txtTypeClassName.Text.Trim());
            promoType.Name = Server.HtmlEncode(txtTypeName.Text.Trim());
            promoType.Description = Server.HtmlEncode(txtTypeDesc.Text.Trim());
            promoType.ActiveInd = chkEnable.Checked;

            if (_itemId > 0)
            {
                saved = _providerTypeAdmin.UpdateDiscountType(promoType);
                LogActivity(_activityEditTypeId, GetGlobalResourceObject("ZnodeAdminResource", "TitleEditPromotionType").ToString());
            }
            else
            {
                saved = _providerTypeAdmin.AddDiscountType(promoType);
                LogActivity(_activityCreateTypeId, GetGlobalResourceObject("ZnodeAdminResource", "TitleCreatePromotionType").ToString());
            }

            return saved;
        }

        private bool SaveShippingType()
        {
            var saved = false;
            var shipType = _itemId > 0 ? _providerTypeAdmin.GetShippingTypeById(_itemId) : new ShippingType();

            shipType.ClassName = Server.HtmlEncode(txtTypeClassName.Text.Trim());
            shipType.Name = Server.HtmlEncode(txtTypeName.Text.Trim());
            shipType.Description = Server.HtmlEncode(txtTypeDesc.Text.Trim());
            shipType.IsActive = chkEnable.Checked;

            if (_itemId > 0)
            {
                saved = _providerTypeAdmin.UpdateShippingType(shipType);
                LogActivity(_activityEditTypeId, GetGlobalResourceObject("ZnodeAdminResource", "TitleEditShippingType").ToString());
            }
            else
            {
                saved = _providerTypeAdmin.AddShippingType(shipType);
                LogActivity(_activityCreateTypeId, GetGlobalResourceObject("ZnodeAdminResource", "TitleCreateShippingType").ToString());
            }

            return saved;
        }

        private bool SaveSupplierType()
        {
            var saved = false;
            var supplierType = _itemId > 0 ? _providerTypeAdmin.GetSupplierTypeById(_itemId) : new SupplierType();

            supplierType.ClassName = Server.HtmlEncode(txtTypeClassName.Text.Trim());
            supplierType.Name = Server.HtmlEncode(txtTypeName.Text.Trim());
            supplierType.Description = Server.HtmlEncode(txtTypeDesc.Text.Trim());
            supplierType.ActiveInd = chkEnable.Checked;

            if (_itemId > 0)
            {
                saved = _providerTypeAdmin.UpdateSupplierType(supplierType);
                LogActivity(_activityEditTypeId, GetGlobalResourceObject("ZnodeAdminResource", "TitleEditSupplierType").ToString());
            }
            else
            {
                saved = _providerTypeAdmin.AddSupplierType(supplierType);
                LogActivity(_activityCreateTypeId, GetGlobalResourceObject("ZnodeAdminResource", "TitleCreateShippingType").ToString());
            }

            return saved;
        }

        private bool SaveTaxType()
        {
            var saved = false;
            var taxRuleType = _itemId > 0 ? _providerTypeAdmin.GetTaxRuleTypeById(_itemId) : new TaxRuleType();

            taxRuleType.ClassName = Server.HtmlEncode(txtTypeClassName.Text.Trim());
            taxRuleType.Name = Server.HtmlEncode(txtTypeName.Text.Trim());
            taxRuleType.Description = Server.HtmlEncode(txtTypeDesc.Text.Trim());
            taxRuleType.ActiveInd = chkEnable.Checked;

            if (_itemId > 0)
            {
                saved = _providerTypeAdmin.UpdateTaxRuleType(taxRuleType);
                LogActivity(_activityEditTypeId, GetGlobalResourceObject("ZnodeAdminResource", "TitleEditTaxType").ToString());
            }
            else
            {
                saved = _providerTypeAdmin.AddTaxRuleType(taxRuleType);
                LogActivity(_activityCreateTypeId, GetGlobalResourceObject("ZnodeAdminResource", "TitleCreateTaxType").ToString());
            }

            return saved;
        }

        private string GetPromotionClassType(string className)
        {
            var classType = String.Empty;

            foreach (var t in _availablePromoTypes)
            {
                if (t.ClassName == className)
                {
                    if (t.GetType().BaseType == typeof(ZnodeCartPromotionType)) { classType = "CART"; }
                    if (t.GetType().BaseType == typeof(ZnodePricePromotionType)) { classType = "PRICE"; }
                    if (t.GetType().BaseType == typeof(ZnodeProductPromotionType)) { classType = "PRODUCT"; }
                }
            }

            return classType;
        }

        private void LogActivity(int activityTypeId, string message)
        {
            var data1 = UserStoreAccess.GetCurrentUserName();
            string data2 = null;
            string data3 = null;
            string status = null;
            string longData = null;
            var source = message + " - " + txtTypeName.Text.Trim();
            var target = txtTypeName.Text.Trim();
            ZNodeLogging.LogActivity(activityTypeId, data1, data2, data3, status, longData, source, target);
        }
    }
}