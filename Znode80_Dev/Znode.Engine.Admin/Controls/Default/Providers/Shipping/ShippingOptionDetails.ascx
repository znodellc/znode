﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShippingOptionDetails.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Shipping.ShippingOptionDetails" %>

<div>
	<div class="ShippingHeader">
		<h1><asp:Localize ID="TitleShippingOptionView" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleShippingOptionView%>'></asp:Localize><asp:Label ID="lblDescription" runat="server" /></h1>
	</div>

	<div align="right" class="RightFloat">
        <zn:Button runat="server" ID="btnEditShipping" OnClick="BtnEditShipping_Click"  Width="110px" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonEditShipping %>'/>
        <zn:Button runat="server" ID="btnBack" OnClick="BtnBack_Click"  Width="100px" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonBackToList %>'/>
	</div>

	<div class="ClearBoth">
		<br />
	</div>

	<h4 class="SubTitle"> <asp:Localize ID="SubTitleGeneralInfo" runat="server" Text="<%$ Resources:ZnodeAdminResource, SubTitleGeneralInformation%>"></asp:Localize></h4>

	<div class="ViewForm200">
		<div class="FieldStyle">
			<asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnProfileName%>"></asp:Localize>
		</div>
		<div class="ValueStyle">
			<asp:Label ID="lblProfileName" runat="server" />
		</div>

		<div class="FieldStyleA">
            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnShippingCode%>"></asp:Localize>
		</div>
		<div class="ValueStyleA">
			<asp:Label ID="lblShippingCode" runat="server" />
		</div>

		<div class="FieldStyle">
			<asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnShippingType%>"></asp:Localize>
		</div>
		<div class="ValueStyle">
			<asp:Label ID="lblShippingType" runat="server" />
		</div>

		<div class="FieldStyleA">
			<asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnDestinationCountry%>"></asp:Localize>
		</div>
		<div class="ValueStyleA">
			<asp:Label ID="lblDestinationCountry" runat="server" />
		</div>

		<div class="FieldStyle">
			<asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnHandlingCharge%>"></asp:Localize>
		</div>
		<div class="ValueStyle">
			<%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %><asp:Label ID="lblHandlingCharge" runat="server" />
		</div>

		<div class="FieldStyleA">
			<asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder%>"></asp:Localize>
		</div>
		<div class="ValueStyleA">
			<asp:Label ID="lblDisplayOrder" runat="server" />
		</div>

		<div class="FieldStyle">
			<asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnEnable%>"></asp:Localize>
		</div>
		<div class="ValueStyle">
			<img id="imgActive" runat="server" alt="" />
		</div>

		<asp:Panel ID="pnlShippingRules" runat="server" Visible="false">
			<h4 class="SubTitle"><asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:ZnodeAdminResource, SubTitleShippingRules%>"></asp:Localize></h4>

			<div style="float: left; width: 70%;">
				<asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:ZnodeAdminResource, SubTextShippingRules%>"></asp:Localize>
			</div>

			<div class="ButtonStyle">
				<zn:LinkButton ID="btnAddRule" runat="server" ButtonType="Button" OnClick="BtnAddRule_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddRule %>' ButtonPriority="Primary" />
			</div>

			<br />

			<div style="clear: both;">
				<asp:GridView ID="uxGrid" runat="server"
					CssClass="Grid"
					AllowSorting="True"
					AllowPaging="True"
					PageSize="25"
					AutoGenerateColumns="False"
					Width="100%"
					CellPadding="4"
					GridLines="None"
					CaptionAlign="Left"
					OnPageIndexChanging="UxGrid_PageIndexChanging"
					OnRowCommand="UxGrid_RowCommand"
					OnRowDeleting="UxGrid_RowDeleting"
					EmptyDataText="<%$ Resources:ZnodeAdminResource, RecordNotFoundShippingRules%>">
					<Columns>
						<asp:BoundField DataField="ShippingRuleID" HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleID %>"  HeaderStyle-HorizontalAlign="Left" />
						<asp:BoundField DataField="ShippingRuleTypeName" HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleRuleType%>" HeaderStyle-HorizontalAlign="Left" />
						<asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleBaseCost%>" HeaderStyle-HorizontalAlign="Left">
							<ItemTemplate>
								<%# DataBinder.Eval(Container.DataItem, "BaseCost","{0:c}")%>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:TemplateField  HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitlePerUnitCost%>" HeaderStyle-HorizontalAlign="Left">
							<ItemTemplate>
								<%# DataBinder.Eval(Container.DataItem, "PerItemCost","{0:c}")%>
							</ItemTemplate>
						</asp:TemplateField>
						<asp:BoundField DataField="LowerLimit" HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleLowerLimit%>" HeaderStyle-HorizontalAlign="Left" />
						<asp:BoundField DataField="UpperLimit"  HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleUpperLimit%>" HeaderStyle-HorizontalAlign="Left" />
						<asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleActions%>" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="150">
							<ItemTemplate>
								<div class="LeftFloat" style="width: 50%; text-align: left">
									<asp:LinkButton ID="btnEdit" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkEdit%>' CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ShippingRuleID")%>' />
								</div>
								<div>
									<asp:LinkButton ID="btnDelete" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkDelete%>' CommandName="Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ShippingRuleID")%>' />
								</div>
							</ItemTemplate>
						</asp:TemplateField>
					</Columns>
					<FooterStyle CssClass="FooterStyle" />
					<RowStyle CssClass="RowStyle" />
					<PagerStyle CssClass="PagerStyle" />
					<HeaderStyle CssClass="HeaderStyle" />
					<AlternatingRowStyle CssClass="AlternatingRowStyle" />
					<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
				</asp:GridView>
			</div>
		</asp:Panel>
	</div>
</div>
