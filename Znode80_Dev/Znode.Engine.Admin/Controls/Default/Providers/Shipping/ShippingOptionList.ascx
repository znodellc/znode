﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShippingOptionList.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Shipping.ShippingOptionList" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<div class="Form">
	<div class="LeftFloat" style="width: 70%; text-align: left">
		<h1><asp:Localize ID="ShippingOptionTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleShippingOption %>'></asp:Localize></h1>
	</div>

	<div class="ButtonStyle">
		<zn:LinkButton ID="btnAddShippingOption" runat="server" CausesValidation="False" ButtonType="Button" OnClick="BtnAddShippingOption_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddNewShippingOption %>' ButtonPriority="Primary" />
	</div>

	<div class="ClearBoth">
		<p><asp:Localize ID="ShippingOptionSubText" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextShippingOption %>'></asp:Localize></p>
	</div>

	<div>
		<zn:Spacer ID="Spacer1" runat="server" SpacerHeight="15" SpacerWidth="3" />
	</div>

	<h4 class="GridTitle"><asp:Localize ID="ShippingOptionGridTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleShippingOption %>'></asp:Localize></h4>

	<asp:GridView ID="uxGrid" runat="server"
		CssClass="Grid"
		AllowSorting="True"
		AllowPaging="True"
		PageSize="25"
		AutoGenerateColumns="False"
		Width="100%"
		CellPadding="4"
		GridLines="None"
		CaptionAlign="Left"
		OnPageIndexChanging="UxGrid_PageIndexChanging"
		OnRowCommand="UxGrid_RowCommand"
		OnRowDeleting="UxGrid_RowDeleting"
		EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundShippingOption %>'>
		<Columns>
			<asp:BoundField DataField="ShippingID" HeaderText='<%$ Resources:ZnodeAdminResource,  ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
			<asp:BoundField DataField="ShippingtypeName" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleShippingType %>' HeaderStyle-HorizontalAlign="Left" />
			<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleShippingCode %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<a href='View.aspx?shippingid=<%# GetEncryptedId(DataBinder.Eval(Container.DataItem, "ShippingID").ToString())%>'>
						<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "ShippingCode").ToString())%>
					</a>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDescription %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<a href='View.aspx?shippingid=<%# GetEncryptedId(DataBinder.Eval(Container.DataItem, "ShippingID").ToString())%>'>
						<%# Server.HtmlDecode(DataBinder.Eval(Container.DataItem, "Description").ToString())%>
					</a>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="ProfileName" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProfileName %>' HeaderStyle-HorizontalAlign="Left" />
			<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCountryCode %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<%# DestinationCountryCode(DataBinder.Eval(Container.DataItem, "DestinationCountryCode"))%>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleHandlingCharge %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<%# DataBinder.Eval(Container.DataItem, "HandlingCharge","{0:c}")%>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
			<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleEnabled %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<img runat="server" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>' alt="" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderStyle-Width="250px" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleActions %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<div class="LeftFloat" style="width: 25%; text-align: left">
						<asp:LinkButton ID="btnView" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkView %>' CommandName="View" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ShippingID")%>' />
					</div>
					<div class="LeftFloat" style="width: 25%; text-align: left">
						<asp:LinkButton ID="btnEdit" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ShippingID")%>' />
					</div>
					<div class="LeftFloat" style="width: 25%; text-align: left">
						<asp:LinkButton ID="btnDelete" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CommandName="Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "ShippingID")%>' />
					</div>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
		<FooterStyle CssClass="FooterStyle" />
		<RowStyle CssClass="RowStyle" />
		<PagerStyle CssClass="PagerStyle" />
		<HeaderStyle CssClass="HeaderStyle" />
		<AlternatingRowStyle CssClass="AlternatingRowStyle" />
		<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
	</asp:GridView>
</div>

<div>
	<zn:Spacer ID="Spacer2" runat="server" SpacerHeight="10" SpacerWidth="10" />
</div>
