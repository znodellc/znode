﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;
using Znode.Engine.Shipping;

namespace Znode.Engine.Admin.Controls.Default.Providers.Shipping
{
	public partial class ShippingOptionAdd : ProviderBase
	{
		private int _shippingId;
		private List<IZnodeShippingType> _availableShippingTypes;
		private ShippingAdmin _shippingAdmin = new ShippingAdmin();
		private StoreSettingsAdmin _storeSettingsAdmin = new StoreSettingsAdmin();

		public string ListPageUrl { get; set; }
		public string ViewPageUrl { get; set; }
		public string AddRulePageUrl { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			_availableShippingTypes = ZnodeShippingManager.GetAvailableShippingTypes();
			_shippingId = Request.Params["shippingid"] != null ? int.Parse(GetDecryptedId(Request.Params["shippingid"])) : 0;

			if (!Page.IsPostBack)
			{
				lblTitle.Text = _shippingId > 0 ? this.GetGlobalResourceObject("ZnodeAdminResource","TitleEditShippingOption").ToString() : this.GetGlobalResourceObject("ZnodeAdminResource","TitleAddShippingOption").ToString();

				txtHandlingCharge.Text = 0.0.ToString("N");
			    cpvHandlingCharge.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "CompareHandlineCharge").ToString(), 123.45.ToString("N")); 

				if (!IsFranchiseAdmin)
				{
					BindProfiles();
				}

				BindShippingTypes();
				BindCountries();
				BindData();
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			if (!IsShippingSetupValid())
			{
				lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorShippingValid").ToString();
				return;
			}

			if (IsDuplicateShipping())
			{
				lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorShippingExists").ToString();
				return;
			}

			var shippingOption = _shippingId > 0 ? _shippingAdmin.GetShippingOptionById(_shippingId) : new ZNode.Libraries.DataAccess.Entities.Shipping();
			shippingOption.ShippingTypeID = _shippingAdmin.GetShippingTypeId(ddlShippingType.SelectedValue);

			if (pnlProfile.Visible && ddlProfile.SelectedValue != "-1")
			{
				shippingOption.ProfileID = int.Parse(ddlProfile.SelectedValue);
			}
			else if (pnlProfile.Visible && ddlProfile.SelectedValue == "-1")
			{
				shippingOption.ProfileID = null;
			}

			if (!pnlProfile.Visible)
			{
				// We're in franchise admin
				shippingOption.ProfileID = UserStoreAccess.GetTurnkeyStoreProfileID;
			}

			if (pnlDisplayName.Visible)
			{
				shippingOption.Description = Server.HtmlEncode(txtDescription.Text);
			}
			else if (pnlServiceCodes.Visible)
			{
				shippingOption.Description = Server.HtmlEncode(ddlServiceCodes.SelectedItem.Text);
			}
			else
			{
				shippingOption.Description = String.Empty;
			}

			if (pnlInternalCode.Visible)
			{
				shippingOption.ShippingCode = Server.HtmlEncode(txtShippingCode.Text);
			}
			else if (pnlServiceCodes.Visible)
			{
				shippingOption.ShippingCode = Server.HtmlEncode(ddlServiceCodes.SelectedItem.Value);
			}
			else
			{
				shippingOption.ShippingCode = String.Empty;
			}

			if (pnlHandlingCharge.Visible)
			{
				shippingOption.HandlingCharge = txtHandlingCharge.Text.Length > 0 ? decimal.Parse(txtHandlingCharge.Text) : 0;
			}

			if (pnlCountries.Visible)
			{
				shippingOption.DestinationCountryCode = ddlCountries.SelectedValue.Equals("*") ? null : ddlCountries.SelectedValue;
			}

			shippingOption.ActiveInd = chkActiveInd.Checked;
			shippingOption.DisplayOrder = int.Parse(txtDisplayOrder.Text);

			var saved = false;
			var isUpdate = false;
			if (_shippingId > 0)
			{
				saved = _shippingAdmin.UpdateShippingOption(shippingOption);
				isUpdate = true;
				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, GetGlobalResourceObject("ZnodeAdminResource","ActivityLogEditShipping").ToString() + txtDescription.Text, txtDescription.Text);
			}
			else
			{
				saved = _shippingAdmin.AddShippingOption(shippingOption);
				_shippingId = shippingOption.ShippingID;
				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, GetGlobalResourceObject("ZnodeAdminResource","ActivityLogAddShipping").ToString() + txtDescription.Text, txtDescription.Text);
			}

			if (saved)
			{
				if (ddlShippingType.SelectedItem.Text.Contains("Custom"))
				{
					if (isUpdate)
					{
						Response.Redirect(ViewPageUrl + "?shippingid=" + GetEncryptedId(_shippingId));
					}
					else
					{
						Response.Redirect(AddRulePageUrl + "?shippingid=" + GetEncryptedId(_shippingId));
					}
				}
				else
				{
					Response.Redirect(ViewPageUrl + "?shippingid=" + GetEncryptedId(_shippingId));
				}
			}

			else
			{
				lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorShipping").ToString();
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			if (_shippingId > 0)
			{
				Response.Redirect(ViewPageUrl + "?shippingid=" + GetEncryptedId(_shippingId));
			}
			else
			{
				Response.Redirect(ListPageUrl);
			}
		}

		protected void ddlShippingType_SelectedIndexChanged(object sender, EventArgs e)
		{
			ToggleDisplay();
		}

		protected void ddlShippingType_DataBound(object sender, EventArgs e)
		{
			foreach (ListItem item in ((DropDownList)sender).Items)
			{
				item.Text = Server.HtmlDecode(item.Text);
			}
		}

		private void BindProfiles()
		{
			var profiles = _storeSettingsAdmin.GetProfiles();
			ddlProfile.DataSource = UserStoreAccess.CheckProfileAccess(profiles);
			ddlProfile.DataTextField = "Name";
			ddlProfile.DataValueField = "ProfileID";
			ddlProfile.DataBind();

			// Add "All Profiles" to top of the list
            ddlProfile.Items.Insert(0, new ListItem(GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAllProfiles").ToString(), "-1"));
			ddlProfile.SelectedValue = "-1";
		}

		private void BindShippingTypes()
		{
			var shippingTypes = _shippingAdmin.GetShippingTypes();
			ddlShippingType.DataSource = shippingTypes.FindAll(ShippingTypeColumn.IsActive, true);
			ddlShippingType.DataTextField = "Name";
			ddlShippingType.DataValueField = "ClassName";
			ddlShippingType.DataBind();
		}

		private void BindCountries()
		{
			var countries = _shippingAdmin.GetDestinationCountries();
			ddlCountries.DataSource = countries;
			ddlCountries.DataTextField = "Name";
			ddlCountries.DataValueField = "Code";
			ddlCountries.DataBind();
			ddlCountries.SelectedValue = "*";
		}

		private void BindServiceCodes()
		{
			var shippingTypeId = _shippingAdmin.GetShippingTypeId(ddlShippingType.SelectedValue);
			var ds = _shippingAdmin.GetShippingServiceCodes(shippingTypeId);

			// Check if FedEx was selected
			if (ddlShippingType.SelectedItem.Text.Contains("FedEx"))
			{
				// Reset items and add back in
				ddlServiceCodes.Items.Clear();
				foreach (DataRow dr in ds.Tables[0].Rows)
				{
					var description = dr["Description"].ToString();
					var regex = new Regex("&reg;");
					description = regex.Replace(description, "®");
					ddlServiceCodes.Items.Add(new ListItem(description, dr["Code"].ToString()));
				}
			}
			else
			{
				ddlServiceCodes.DataSource = ds;
				ddlServiceCodes.DataTextField = "Description";
				ddlServiceCodes.DataValueField = "Code";
				ddlServiceCodes.DataBind();
			}
		}

		/// <summary>
		/// Checks if at least one shipping option is enabled.
		/// </summary>
		/// <returns>True if at least one shipping option is enabled; otherwise, false.</returns>
		private bool IsShippingSetupValid()
		{
			var allShippingOptions = _shippingAdmin.GetAll();

			var inactiveShippingOptions = allShippingOptions.FindAll(shipping => shipping.ActiveInd == false);
			if (inactiveShippingOptions.Count == (allShippingOptions.Count - 1) && !chkActiveInd.Checked)
			{
				return false;
			}

			// Otherwise
			return true;
		}

		private void BindData()
		{
			if (_shippingId > 0)
			{
				var shippingOption = _shippingAdmin.GetShippingOptionById(_shippingId);
				if (shippingOption != null)
				{
					if (IsFranchiseAdmin)
					{
						UserStoreAccess.CheckProfileAccess(shippingOption.ProfileID.GetValueOrDefault(0), true);
					}

					ddlShippingType.SelectedValue = GetShippingTypeClassName();
					ddlShippingType.Enabled = false;

					ToggleDisplay();

					if (pnlProfile.Visible)
					{
						if (shippingOption.ProfileID.HasValue)
						{
							ddlProfile.SelectedValue = shippingOption.ProfileID.Value.ToString();
						}
					}

					if (pnlServiceCodes.Visible)
					{
						ddlServiceCodes.SelectedValue = shippingOption.ShippingCode;
					}

					if (pnlDisplayName.Visible)
					{
						txtDescription.Text = Server.HtmlDecode(shippingOption.Description);
					}

					if (pnlInternalCode.Visible)
					{
						txtShippingCode.Text = Server.HtmlDecode(shippingOption.ShippingCode);
					}

					if (pnlHandlingCharge.Visible)
					{
						txtHandlingCharge.Text = shippingOption.HandlingCharge > 0 ? shippingOption.HandlingCharge.ToString("N2") : "0.00";
					}

					if (pnlCountries.Visible)
					{
						if (shippingOption.DestinationCountryCode != null)
						{
							var listItem = ddlCountries.Items.FindByValue(shippingOption.DestinationCountryCode);
							if (listItem != null)
							{
								ddlCountries.SelectedIndex = ddlCountries.Items.IndexOf(listItem);
							}
						}
					}

					chkActiveInd.Checked = shippingOption.ActiveInd;
					txtDisplayOrder.Text = shippingOption.DisplayOrder.ToString();
				}
			}
            else
            {
                // To hide by default the service code panel for custom shipping 
                ToggleDisplay();
            }
		}

		private string GetShippingTypeClassName()
		{
			var shippingTypeId = _shippingAdmin.GetShippingTypeId(_shippingId);
			var shippingTypes = _shippingAdmin.GetShippingTypes();

			foreach (var item in shippingTypes.Where(item => item.ShippingTypeID == shippingTypeId))
			{
				return item.ClassName;
			}

			return String.Empty;
		}

		private void ToggleDisplay()
		{
			// First hide everything
			pnlProfile.Visible = false;
			pnlServiceCodes.Visible = false;
			pnlDisplayName.Visible = false;
			pnlInternalCode.Visible = false;
			pnlHandlingCharge.Visible = false;
			pnlCountries.Visible = false;

			// Then loop through to show only controls that are needed
			foreach (var t in _availableShippingTypes)
			{
				if (t.ClassName == ddlShippingType.SelectedValue)
				{
					foreach (var control in t.Controls)
					{
						ShowControl(control);

						// For franchise admin, we don't show the profile
						if (IsFranchiseAdmin) pnlProfile.Visible = false;
					}
				}
			}

			// Check to build the service codes list
			if (pnlServiceCodes.Visible) BindServiceCodes();
		}

		private void ShowControl(ZnodeShippingControl control)
		{
			if (control == ZnodeShippingControl.Profile) pnlProfile.Visible = true;
			if (control == ZnodeShippingControl.ServiceCodes) pnlServiceCodes.Visible = true;
			if (control == ZnodeShippingControl.DisplayName) pnlDisplayName.Visible = true;
			if (control == ZnodeShippingControl.InternalCode) pnlInternalCode.Visible = true;
			if (control == ZnodeShippingControl.HandlingCharge) pnlHandlingCharge.Visible = true;
			if (control == ZnodeShippingControl.Countries) pnlCountries.Visible = true;
		}

		private bool IsDuplicateShipping()
		{
			var profileId = !pnlProfile.Visible ? UserStoreAccess.GetTurnkeyStoreProfileID : int.Parse(ddlProfile.SelectedValue);
			var serviceCode = pnlInternalCode.Visible ? Server.HtmlEncode(txtShippingCode.Text) : ddlServiceCodes.SelectedValue;

			var shippingAdmin = new ShippingAdmin();
			return shippingAdmin.IsDuplicateShipping(_shippingId, profileId, serviceCode);
		}
	}
}