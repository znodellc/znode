﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShippingOptionDelete.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Shipping.ShippingOptionDelete" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<h1>
    <asp:Label ID="lblDeleteShippingOption" runat="server">
        <asp:Localize ID="PleaseConfirm" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextDeleteShippingConfirm%>"></asp:Localize><%=ShippingOptionName%></asp:Label>
</h1>

<div>
    <zn:Spacer ID="Spacer1" runat="server" SpacerHeight="10" SpacerWidth="3" />
</div>

<p>
    <asp:Localize ID="TextDeleteShipping" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextDeleteShipping%>"></asp:Localize></p>

<p>
    <asp:Label ID="lblErrorMsg" runat="server" Visible="False" CssClass="Error" /></p>

<div>
    <zn:Spacer ID="Spacer2" runat="server" SpacerHeight="10" SpacerWidth="3" />
</div>

<div>
    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnDelete_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonDelete%>" CausesValidation="true" ID="btnDelete" />
    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel%>" CausesValidation="False" ID="btnCancel" />
</div>

<div>
    <zn:Spacer ID="Spacer3" runat="server" SpacerHeight="15" SpacerWidth="3" />
</div>
