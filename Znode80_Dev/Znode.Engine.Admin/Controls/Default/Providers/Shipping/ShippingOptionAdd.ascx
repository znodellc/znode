﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ShippingOptionAdd.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Shipping.ShippingOptionAdd" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<div class="FormView">
	<div class="LeftFloat" style="width: 70%; text-align: left;">
		<h1><asp:Label ID="lblTitle" runat="server" /></h1>
	</div>

	<div style="text-align: right;">
	    <zn:Button runat="server" ID="btnSubmitTop" OnClick="BtnSubmit_Click" CausesValidation="True" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>'/>
        <zn:Button runat="server" ID="btnCancelTop" OnClick="BtnCancel_Click" CausesValidation="False" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>'/>
	</div>

	<div class="ClearBoth">
		<asp:Label ID="lblMsg" runat="server" CssClass="Error" />
	</div>

	<div>
		<zn:Spacer id="Spacer1" runat="server" SpacerHeight="5" SpacerWidth="3" />
	</div>

	<h4 class="SubTitle"><asp:Localize ID="GeneralSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralSettings %>'></asp:Localize></h4>

	<div class="FieldStyle">
		<asp:Localize ID="ShippingType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingType %>'></asp:Localize> <span class="Asterix">*</span>
	</div>
	<div class="ValueStyle">
		<asp:DropDownList ID="ddlShippingType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlShippingType_SelectedIndexChanged" OnDataBound="ddlShippingType_DataBound" />
	</div>
	
	<asp:Panel ID="pnlProfile" runat="server">
		<div class="FieldStyle">
			<asp:Localize ID="ProfileName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProfileName %>'></asp:Localize> <span class="Asterix">*</span>
		</div>
		<div class="ValueStyle">
			<asp:DropDownList ID="ddlProfile" runat="server" />
		</div>
	</asp:Panel>

	<asp:Panel ID="pnlServiceCodes" runat="server">
		<div class="FieldStyle">
			<asp:Localize ID="Service" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnService %>'></asp:Localize><span class="Asterix">*</span>
		</div>
		<div class="ValueStyle">
			<asp:DropDownList ID="ddlServiceCodes" runat="server" AutoPostBack="false" />
		</div>
	</asp:Panel>

	<asp:Panel ID="pnlDisplayName" runat="server">
		<div class="FieldStyle">
			<asp:Localize ID="DisplayName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayName %>'></asp:Localize> <span class="Asterix">*</span><br/>
			<small><asp:Localize ID="ColumnTextDisplayName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextDisplayName %>'></asp:Localize></small>
		</div>
		<div class="ValueStyle">
			<asp:TextBox ID="txtDescription" runat="server" MaxLength="50" Columns="50" />
			<asp:RequiredFieldValidator ID="rfvDescription" runat="server" ControlToValidate="txtDescription" Display="Dynamic" CssClass="Error" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredShippingOptionName %>' SetFocusOnError="True" />
		</div>
	</asp:Panel>
	
	<asp:Panel ID="pnlInternalCode" runat="server">
		<div class="FieldStyle">
			<asp:Localize ID="InternalCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnInternalCode %>'></asp:Localize> <span class="Asterix">*</span><br/>
			<small><asp:Localize ID="InternalCodeText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextInternalCode %>'></asp:Localize></small>
		</div>
		<div class="ValueStyle">
			<asp:TextBox ID="txtShippingCode" runat="server" MaxLength="10" Columns="23" />
			<asp:RequiredFieldValidator ID="rfvShippingCode" runat="server" ControlToValidate="txtShippingCode" Display="Dynamic" CssClass="Error" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredShippingCode %>' SetFocusOnError="True" />
		</div>
	</asp:Panel>
	
	<asp:Panel ID="pnlHandlingCharge" runat="server">
		<div class="FieldStyle">
			<asp:Localize ID="HandlingCharge" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnHandlingCharge %>'></asp:Localize> <asp:Localize ID="OptionalHandling" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOptional %>'></asp:Localize>
		</div>
		<div class="ValueStyle">
			<%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %><asp:TextBox ID="txtHandlingCharge" runat="server" Columns="12" />
            <asp:RequiredFieldValidator
                ID="rfvHandlingCharge" runat="server" ControlToValidate="txtHandlingCharge"
                Display="Dynamic" CssClass="Error" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredHandlingCharge %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
			<asp:CompareValidator ID="cpvHandlingCharge" runat="server" ControlToValidate="txtHandlingCharge" CssClass="Error" Type="Currency" Operator="DataTypeCheck" />
		</div>
	</asp:Panel>

	<asp:Panel ID="pnlCountries" runat="server">
		<div class="FieldStyle">
			<asp:Localize ID="DestinationCountry" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDestinationCountry %>'></asp:Localize> <asp:Localize ID="OptinalCountry" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOptional %>'></asp:Localize><br />
			<small><asp:Localize ID="DestinationCountryText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextDestinationCountry %>'></asp:Localize></small>
		</div>
		<div class="ValueStyle">
			<asp:DropDownList ID="ddlCountries" runat="server" />
		</div>
	</asp:Panel>

	<div>
		<zn:Spacer id="Spacer2" runat="server" SpacerHeight="15" SpacerWidth="3" />
	</div>

	<h4 class="SubTitle"><asp:Localize ID="DisplaySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDisplaySettings %>'></asp:Localize></h4>

	<div class="FieldStyle">
		<asp:Localize ID="Enable" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnable %>'></asp:Localize>
	</div>
	<div class="ValueStyle">
		<asp:CheckBox ID="chkActiveInd" runat="server" Checked="true" Text='<%$ Resources:ZnodeAdminResource, ColumnTextEnable %>' />
	</div>

	<div class="FieldStyle">
		<asp:Localize ID="DisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder %>'></asp:Localize> <span class="Asterix">*</span><br />
		<small><asp:Localize ID="DisplayOrderText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextShippingDisplayOrder %>'></asp:Localize>.</small>
	</div>
	<div class="ValueStyle">
		<asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="9" Columns="5" />
		<asp:RequiredFieldValidator ID="rfvDisplayOrder" runat="server" ControlToValidate="txtDisplayOrder" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrder %>' CssClass="Error" />
		<asp:RangeValidator ID="rgvDisplayOrder" runat="server" ControlToValidate="txtDisplayOrder" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeWholeNumber %>' CssClass="Error" MinimumValue="1" MaximumValue="999999999" Type="Integer" />
	</div>

	<div class="ClearBoth">
		<zn:Spacer id="Spacer3" runat="server" SpacerHeight="15" SpacerWidth="3" />
	</div>

	<div>
	    <zn:Button runat="server" ID="btnSubmitBottom" OnClick="BtnSubmit_Click" CausesValidation="True" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>'/>
        <zn:Button runat="server" ID="btnCancelBottom" OnClick="BtnCancel_Click" CausesValidation="False" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>'/>
	</div>
</div>
