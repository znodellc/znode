﻿using System;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace Znode.Engine.Admin.Controls.Default.Providers.Shipping
{
	public partial class ShippingRuleDelete : ProviderBase
	{
		private int _shippingRuleId;
		private int _shippingId;

		public string ShippingOption { get; set; }
		public string RedirectUrl { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request.Params["shippingruleid"] != null)
			{
				_shippingRuleId = IsFranchiseAdmin ? Convert.ToInt32(GetDecryptedId(Request.Params["shippingruleid"])) : int.Parse(Request.Params["shippingruleid"]);
			}
			else
			{
				_shippingRuleId = 0;
			}

			if (Request.Params["shippingid"] != null)
			{
				_shippingId = IsFranchiseAdmin ? Convert.ToInt32(GetDecryptedId(Request.Params["shippingid"])) : int.Parse(Request.Params["shippingid"]);
			}
			else
			{
				_shippingId = 0;
			}

			BindData();
		}

		protected void BtnDelete_Click(object sender, EventArgs e)
		{
			// Get shipping option
			var shippingAdmin = new ShippingAdmin();
			var shippingOption = shippingAdmin.GetShippingOptionById(_shippingId);

			// Get the shipping rule and delete it
			var shippingRule = new ShippingRule { ShippingRuleID = _shippingRuleId };
			var deleted = shippingAdmin.DeleteShippingRule(shippingRule);

			if (deleted)
			{
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteShippingRule").ToString(), _shippingRuleId, shippingOption.Description), shippingOption.Description);
				Response.Redirect(RedirectUrl + "?shippingid=" + GetEncryptedId(_shippingId));
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect(RedirectUrl + "?shippingid=" + GetEncryptedId(_shippingId));
		}

		public void BindData()
		{
			var shippingAdmin = new ShippingAdmin();
			var shippingOption = shippingAdmin.GetShippingOptionById(_shippingId);

			if (shippingOption != null)
			{
				ShippingOption = shippingOption.Description;
			}
		}
	}
}