﻿using System;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using Znode.Engine.Common;

namespace Znode.Engine.Admin.Controls.Default.Providers.Shipping
{
	public partial class ShippingOptionDetails : ProviderBase
	{
		private int _shippingId;

		public string ListPageUrl { get; set; }
		public string EditPageUrl { get; set; }
		public string AddRulePageUrl { get; set; }
		public string EditRulePageUrl { get; set; }
		public string DeleteRulePageUrl { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			_shippingId = Request.Params["shippingid"] != null ? int.Parse(GetDecryptedId(Request.Params["shippingid"])) : 0;

			if (!Page.IsPostBack)
			{
				BindData();
				BindGrid();
			}
		}

		protected void BtnEditShipping_Click(object sender, EventArgs e)
		{
			Response.Redirect(EditPageUrl + "?shippingid=" + GetEncryptedId(_shippingId));
		}

		protected void BtnAddRule_Click(object sender, EventArgs e)
		{
			Response.Redirect(AddRulePageUrl + "?shippingid=" + GetEncryptedId(_shippingId));
		}

		protected void BtnBack_Click(object sender, EventArgs e)
		{
			Response.Redirect(ListPageUrl);
		}

		protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			uxGrid.PageIndex = e.NewPageIndex;
			BindGrid();
		}

		protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName != "Page")
			{
				var shippingRuleId = e.CommandArgument.ToString();

				if (e.CommandName == "Edit")
				{
					Response.Redirect(EditRulePageUrl + "?shippingruleid=" + GetEncryptedId(shippingRuleId) + "&shippingid=" + GetEncryptedId(_shippingId));
				}
				else if (e.CommandName == "Delete")
				{
					Response.Redirect(DeleteRulePageUrl + "?shippingruleid=" + GetEncryptedId(shippingRuleId) + "&shippingid=" + GetEncryptedId(_shippingId));
				}
			}
		}

		protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
		{
			BindGrid();
		}

		private void BindData()
		{
			if (_shippingId > 0)
			{
				var shippingAdmin = new ShippingAdmin();
				var shippingOption = shippingAdmin.GetShippingOptionById(_shippingId);

				if (shippingOption != null)
				{
					if (IsFranchiseAdmin)
					{
						UserStoreAccess.CheckProfileAccess(shippingOption.ProfileID.GetValueOrDefault(0), true);
					}

					lblDescription.Text = Server.HtmlDecode(shippingOption.Description);
					lblProfileName.Text = shippingOption.ProfileID.HasValue ? shippingAdmin.GetProfileName((int)shippingOption.ProfileID) : "All Profiles";
					lblShippingCode.Text = Server.HtmlDecode(shippingOption.ShippingCode);
					lblShippingType.Text = Server.HtmlDecode(shippingAdmin.GetShippingTypeName(shippingOption.ShippingTypeID));
					lblDestinationCountry.Text = !String.IsNullOrEmpty(shippingOption.DestinationCountryCode) ? shippingOption.DestinationCountryCode : "All Countries";
					lblHandlingCharge.Text = shippingOption.HandlingCharge > 0 ? shippingOption.HandlingCharge.ToString("N2") : "0.00";
					lblDisplayOrder.Text = shippingOption.DisplayOrder.ToString();
					imgActive.Src = Helper.GetCheckMark(shippingOption.ActiveInd);

					// Only show shipping rules for custom shipping types (ShippingTypeID = 1)
					if (shippingOption.ShippingTypeID == 1)
					{
						pnlShippingRules.Visible = true;
					}
				}
			}
		}

		private void BindGrid()
		{
			var shippingAdmin = new ShippingAdmin();
			uxGrid.DataSource = shippingAdmin.GetShippingRules(_shippingId);
			uxGrid.DataBind();
		}
	}
}