﻿using System;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using Znode.Engine.Shipping;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Admin.Controls.Default.Providers.Shipping
{
    public partial class ShippingRuleAdd : ProviderBase
    {
        public string ViewPageUrl { get; set; }

        protected string TierLabel { get; set; }

        private int _shippingRuleId;
        private int _shippingId;
        private ShippingAdmin _shippingAdmin = new ShippingAdmin();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Visible = false;
            _shippingRuleId = Request.Params["shippingruleid"] != null ? int.Parse(GetDecryptedId(Request.Params["shippingruleid"])) : 0;
            _shippingId = Request.Params["shippingid"] != null ? int.Parse(GetDecryptedId(Request.Params["shippingid"])) : 0;

            if (!Page.IsPostBack)
            {

                // Set title
                lblTitle.Text = _shippingRuleId > 0 ? this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditShippingRule").ToString() : this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddShippingRule").ToString();

                // Set base cost text and validator
                txtBaseCost.Text = 0.00.ToString("N");
                cpvBaseCost.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnTitleBaseCostText").ToString(), 123.45.ToString("N"));

                // Set per item cost text and validator
                txtPerItemCost.Text = 0.00.ToString("N");
                cpvPerItemCost.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnTitlePerUnitCostText").ToString(), 123.45.ToString("N"));

                BindData();
            }
        }

        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            var shippingRule = _shippingRuleId > 0 ? _shippingAdmin.GetShippingRule(_shippingRuleId) : new ShippingRule { ShippingID = _shippingId };

            shippingRule.ShippingRuleTypeID = int.Parse(ddlShippingRuleType.SelectedValue);
            shippingRule.BaseCost = decimal.Parse(txtBaseCost.Text);
            shippingRule.PerItemCost = decimal.Parse(txtPerItemCost.Text);

            if (shippingRule.ShippingRuleTypeID != (int)ZnodeShippingRuleType.FlatRatePerItem && shippingRule.ShippingRuleTypeID != (int)ZnodeShippingRuleType.FixedRatePerItem)
            {
                shippingRule.LowerLimit = decimal.Parse(txtLowerLimit.Text);
                shippingRule.UpperLimit = decimal.Parse(txtUpperLimit.Text);
            }
            else
            {
                shippingRule.LowerLimit = null;
                shippingRule.UpperLimit = null;
                if (shippingRule.ShippingRuleTypeID != (int)ZnodeShippingRuleType.FlatRatePerItem)
                {
                    shippingRule.PerItemCost = 0;
                }
            }

            if (IsRuleExists(shippingRule))
            {
                lblMsg.Visible = true;
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorShippingRuleType").ToString();
                return;
            }

            var saved = false;
            var shippingService = new ShippingService();
            var shippingOption = shippingService.GetByShippingID(_shippingId);
            var shippingOptionName = shippingOption.Description;
            var source = String.Empty;

            if (_shippingRuleId > 0)
            {
                saved = _shippingAdmin.UpdateShippingRule(shippingRule);
                source = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditShippingRule").ToString() + ddlShippingRuleType.SelectedItem.Text + " - " + shippingOptionName;
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, source, shippingOptionName);
            }
            else
            {
                saved = _shippingAdmin.AddShippingRule(shippingRule);
                source = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddShippingRule").ToString() + ddlShippingRuleType.SelectedItem.Text + " - " + shippingOptionName;
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, source, shippingOptionName);
            }

            if (saved)
            {
                Response.Redirect(ViewPageUrl + "?shippingid=" + GetEncryptedId(_shippingId));
            }
            else
            {
                lblMsg.Visible = true;
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorShippingRule").ToString();
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(ViewPageUrl + "?shippingid=" + GetEncryptedId(_shippingId));
        }

        protected void ddlShippingRuleType_SelectedIndexChanged(object sender, EventArgs e)
        {
            SetShippingTypeOptions(int.Parse(ddlShippingRuleType.SelectedItem.Value));
        }

        /// <summary>
        /// Checks if the shipping rule already exists for the shipping option.
        /// </summary>
        /// <param name="shippingRule">The shipping rule to check for.</param>
        /// <returns>True if the shipping rule already exists for the shipping option; otherwise, false.</returns>
        private bool IsRuleExists(ShippingRule shippingRule)
        {
            var service = new ShippingRuleService();
            var query = new ShippingRuleQuery();

            query.Append(ShippingRuleColumn.ShippingID, shippingRule.ShippingID.ToString());
            query.Append(ShippingRuleColumn.ShippingRuleTypeID, shippingRule.ShippingRuleTypeID.ToString());

            if (_shippingRuleId > 0)
            {
                query.AppendNotEquals(ShippingRuleColumn.ShippingRuleID, shippingRule.ShippingRuleID.ToString());
            }

            var list = service.Find(query.GetParameters());

            if ((shippingRule.ShippingRuleTypeID == 0 || shippingRule.ShippingRuleTypeID == 3) && list.Count > 0)
            {
                return true;
            }


            if (list.Count > 0)
            {
                foreach (var item in list)
                {
                    if ((shippingRule.LowerLimit >= item.LowerLimit) && (shippingRule.LowerLimit <= item.UpperLimit))
                    {
                        return true;
                    }

                    if ((shippingRule.UpperLimit >= item.LowerLimit) && (shippingRule.UpperLimit <= item.UpperLimit))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void BindData()
        {
            var shippingOption = _shippingAdmin.GetShippingOptionById(_shippingId);

            if (IsFranchiseAdmin)
            {
                UserStoreAccess.CheckProfileAccess(shippingOption.ProfileID.GetValueOrDefault(0), true);
            }

            var shippingRuleTypes = _shippingAdmin.GetShippingRuleTypes();

            // Set title
            lblTitle.Text = lblTitle.Text + Server.HtmlEncode(shippingOption.Description);

            // Get shipping rule types for non-FedEx and non-UPS shipping options
            foreach (var ruleType in shippingRuleTypes)
            {
                // FedEx = ShippingTypeID 2, UPS = ShippingTypeID 3
                if (shippingOption.ShippingTypeID != 2 || shippingOption.ShippingTypeID != 3)
                {
                    var li = new ListItem(ruleType.Description, ruleType.ShippingRuleTypeID.ToString());
                    ddlShippingRuleType.Items.Add(li);
                }
            }

            ddlShippingRuleType.SelectedIndex = 0;

            if (_shippingRuleId > 0)
            {
                var shippingRule = _shippingAdmin.GetShippingRule(_shippingRuleId);

                ddlShippingRuleType.SelectedValue = shippingRule.ShippingRuleTypeID.ToString();

                SetShippingTypeOptions(shippingRule.ShippingRuleTypeID);

                txtBaseCost.Text = shippingRule.BaseCost != 0 ? shippingRule.BaseCost.ToString("N2") : "0";
                txtPerItemCost.Text = shippingRule.PerItemCost != 0 ? shippingRule.PerItemCost.ToString("N2") : "0";
                txtLowerLimit.Text = shippingRule.LowerLimit != null ? shippingRule.LowerLimit.ToString() : "0";
                txtUpperLimit.Text = shippingRule.UpperLimit != null ? shippingRule.UpperLimit.ToString() : "0";
            }
            else
            {
                pnlNonFlatRate.Visible = false;
            }
        }

        private void SetShippingTypeOptions(int shippingTypeId)
        {
            switch (shippingTypeId)
            {
                case (int)ZnodeShippingRuleType.FixedRatePerItem:
                    pnlNonFixedRate.Visible = false;
                    pnlNonFlatRate.Visible = false;
                    break;
                case (int)ZnodeShippingRuleType.FlatRatePerItem:
                    pnlNonFixedRate.Visible = true;
                    pnlNonFlatRate.Visible = false;
                    break;
                case (int)ZnodeShippingRuleType.RateBasedOnQuantity:
                    pnlNonFixedRate.Visible = true;
                    pnlNonFlatRate.Visible = true;
                    TierLabel = "# items";
                    break;
                case (int)ZnodeShippingRuleType.RateBasedOnWeight:
                    pnlNonFixedRate.Visible = true;
                    pnlNonFlatRate.Visible = true;
                    TierLabel = "lbs";
                    break;
            }
        }
    }
}