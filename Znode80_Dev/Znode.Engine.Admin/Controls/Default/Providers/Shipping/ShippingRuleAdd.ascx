﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="ShippingRuleAdd.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Shipping.ShippingRuleAdd" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<div class="FormView">
    <div style="text-align: right">
        <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="True" ID="btnSubmitTop" />
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelTop" />
    </div>

    <div class="LeftFloat" style="width: auto; text-align: left; margin-top: -20px;">
        <h1>
            <asp:Label ID="lblTitle" runat="server" /></h1>
        <p>
            <asp:Localize ID="TextShippingRuleAdd" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextShippingRuleAdd%>'></asp:Localize></p>
    </div>

    <div class="ClearBoth">
        <asp:Label ID="lblMsg" runat="server" CssClass="Error" />
    </div>

    <div>
        <zn:Spacer ID="Spacer1" runat="server" SpacerHeight="5" SpacerWidth="3" />
    </div>

    <h4 class="SubTitle">
        <asp:Localize ID="SubTitleShippingRuleAdd" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSbippingRuleType%>'></asp:Localize></h4>

    <div class="FieldStyle">
        <asp:Localize ID="ColumnRuleType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRuleType%>'></asp:Localize>
    </div>
    <div class="ValueStyle">
        <asp:DropDownList ID="ddlShippingRuleType" runat="server" OnSelectedIndexChanged="ddlShippingRuleType_SelectedIndexChanged" AutoPostBack="true" />
    </div>

    <h4 class="SubTitle">
        <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSetPricing%>'></asp:Localize></h4>

    <div class="FieldStyle">
        <asp:Localize ID="ColumnBaseCost" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBaseCost%>'></asp:Localize><br />
        <small>
            <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextBaseCost%>'></asp:Localize></small>
    </div>
    <div class="ValueStyle">
        <span class="DollarSymbol">$</span>
        <asp:TextBox ID="txtBaseCost" runat="server" />
        <asp:RequiredFieldValidator ID="rfvBaseCost" runat="server" ControlToValidate="txtBaseCost" Display="Dynamic" ErrorMessage=" Enter a Base Cost" SetFocusOnError="True" />
        <asp:CompareValidator ID="cpvBaseCost" runat="server" ControlToValidate="txtBaseCost" Display="Dynamic" Type="Currency" Operator="DataTypeCheck" />
    </div>

    <asp:Panel ID="pnlNonFixedRate" runat="server">
        <div class="FieldStyle">
            <asp:Localize ID="ColumnPerUnitCost" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPerUnitCost%>'></asp:Localize><br />
            <small>
                <asp:Localize ID="ColumnTextPerUnitCost" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextPerUnitCost%>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <span class="DollarSymbol">$</span>
            <asp:TextBox ID="txtPerItemCost" runat="server" />
            <asp:RequiredFieldValidator ID="rfvPerItemCost" runat="server" ControlToValidate="txtPerItemCost" Display="Dynamic" ErrorMessage=" Enter a Per Item Cost" SetFocusOnError="True" />
            <asp:CompareValidator ID="cpvPerItemCost" runat="server" ControlToValidate="txtPerItemCost" Display="Dynamic" Type="Currency" Operator="DataTypeCheck" />
        </div>
    </asp:Panel>

    <asp:Panel ID="pnlNonFlatRate" runat="server" Visible="false">
        <h4 class="SubTitle">
            <asp:Localize ID="ColumnPostalCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleTieredPricingLimits%>'></asp:Localize></h4>

        <p>
            <asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextTieredPricingLimits%>'></asp:Localize></p>

        <div>
            <zn:Spacer ID="Spacer2" runat="server" SpacerHeight="10" SpacerWidth="3" />
        </div>

        <div class="FieldStyle">
            <asp:Localize ID="Localize4" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLowerLimit%>'></asp:Localize>(<%= TierLabel %>)
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtLowerLimit" runat="server">0.00</asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvLowerLimit" runat="server" ControlToValidate="txtLowerLimit" Display="Dynamic" ErrorMessage=" Enter a Lower Limit" SetFocusOnError="True" />
            <asp:CompareValidator ID="cpvLowerLimit" runat="server" ControlToValidate="txtLowerLimit" Display="Dynamic" ErrorMessage=" Must enter a valid lower limit (ex: 2)" Type="Double" Operator="DataTypeCheck" />
        </div>

        <div class="FieldStyle">
            <asp:Localize ID="Localize5" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnUpperLimit%>'></asp:Localize>(<%= TierLabel %>)
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtUpperLimit" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="rfvUpperLimit" runat="server" ControlToValidate="txtUpperLimit" Display="Dynamic" ErrorMessage=" Enter a Upper Limit" SetFocusOnError="True" />
            <asp:CompareValidator ID="cpvUpperLimit" runat="server" ControlToValidate="txtUpperLimit" Display="Dynamic" ErrorMessage=" Must enter a valid upper limit (ex: 2)" Type="Double" Operator="DataTypeCheck" />
        </div>
    </asp:Panel>

    <div class="ClearBoth">
        <br />
    </div>

    <div>
        <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="True" ID="btnSubmitBottom" />
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelBottom" />
    </div>
</div>
