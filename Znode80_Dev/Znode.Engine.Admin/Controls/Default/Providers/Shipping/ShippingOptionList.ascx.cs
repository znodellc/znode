﻿using System;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using Znode.Engine.Common;

namespace Znode.Engine.Admin.Controls.Default.Providers.Shipping
{
	public partial class ShippingOptionList : ProviderBase
	{
		public string ViewPageUrl { get; set; }
		public string AddPageUrl { get; set; }
		public string EditPageUrl { get; set; }
		public string DeletePageUrl { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				BindGrid();
			}
		}

		protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			uxGrid.PageIndex = e.NewPageIndex;
			BindGrid();
		}

		protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName != "Page")
			{
				var shippingId = e.CommandArgument.ToString();

				if (e.CommandName == "Edit")
				{
					Response.Redirect(EditPageUrl + "?shippingid=" + GetEncryptedId(shippingId));
				}
				else if (e.CommandName == "View")
				{
					Response.Redirect(ViewPageUrl + "?shippingid=" + GetEncryptedId(shippingId));
				}
				else if (e.CommandName == "Delete")
				{
					Response.Redirect(DeletePageUrl + "?shippingid=" + GetEncryptedId(shippingId));
				}
			}
		}

		protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
		{
			BindGrid();
		}

		protected void BtnAddShippingOption_Click(object sender, EventArgs e)
		{
			Response.Redirect(AddPageUrl);
		}

		protected string DestinationCountryCode(object countryCode)
		{
		    var result = this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString();

			if (countryCode.ToString().Trim() != "")
			{
				result = countryCode.ToString();
			}

			return result;
		}

		private void BindGrid()
		{
			var shippingAdmin = new ShippingAdmin();

			if (IsFranchiseAdmin)
			{
				uxGrid.DataSource = UserStoreAccess.CheckProfileAccess(shippingAdmin.GetAllShippingOptions().Tables[0]);
			}
			else
			{
				uxGrid.DataSource = UserStoreAccess.CheckProfileAccess(shippingAdmin.GetAllShippingOptions().Tables[0], true);
			}
			
			uxGrid.DataBind();
		}
	}
}