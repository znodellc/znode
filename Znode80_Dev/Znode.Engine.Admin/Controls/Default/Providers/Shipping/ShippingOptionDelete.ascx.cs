﻿using System;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Admin.Controls.Default.Providers.Shipping
{
    public partial class ShippingOptionDelete : ProviderBase
    {
        private int _shippingId;
        private ShippingAdmin _shippingAdmin = new ShippingAdmin();

        public string ShippingOptionName { get; set; }
        public string RedirectUrl { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["shippingid"] != null)
            {
                _shippingId = IsFranchiseAdmin ? Convert.ToInt32(GetDecryptedId(Request.Params["shippingid"])) : int.Parse(Request.Params["shippingid"]);
            }
            else
            {
                _shippingId = 0;
            }

            BindData();
        }

        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            var deleted = false;

            try
            {
                if (!IsShippingOptionDeletable())
                {
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidDeleteShipping").ToString();
                    lblErrorMsg.Visible = true;
                    return;
                }

                var shippingOption = _shippingAdmin.GetShippingOptionById(_shippingId);
                if (shippingOption != null)
                {
                    // Get the name then do the delete
                    var shippingOptionName = shippingOption.Description;
                    deleted = _shippingAdmin.DeleteShippingOption(shippingOption);
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Delete Shipping Option - " + shippingOptionName, shippingOptionName);
                }
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage(ex.Message);
            }

            if (!deleted)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidDeleteShippingRule").ToString();
                lblErrorMsg.Visible = true;
            }
            else
            {
                Response.Redirect(RedirectUrl);
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(RedirectUrl);
        }

        /// <summary>
        /// Checks whether or not the shipping option is deletable.
        /// </summary>
        /// <returns>True if the shipping option is deletable; otherwise, false.</returns>
        private bool IsShippingOptionDeletable()
        {
            var isDeletable = false;
            var allShippingOptions = _shippingAdmin.GetAll();

            // Check if there are other shipping options that are associated to the current shipping option profile
            if (IsSameProfileExist(_shippingId))
            {
                isDeletable = true;
            }
            else
            {
                // Find all shipping options associated with "All Profiles" except current item
                var tempShippingOptions = allShippingOptions.FindAll(s => s.ProfileID == null && s.ShippingID != _shippingId);
                if (tempShippingOptions.Count > 0)
                {
                    // If "All Profile" shipping option found then deletable
                    isDeletable = true;
                }
                else
                {
                    // Get all the "All Profiles" shipping options
                    tempShippingOptions = allShippingOptions.FindAll(s => s.ProfileID == null);
                    var allProfilesCount = tempShippingOptions.Count;

                    // Get all profiles
                    var profileAdmin = new ProfileAdmin();
                    var allProfiles = profileAdmin.GetAll();

                    // Get the shipping options except those associated with "All Profiles"
                    tempShippingOptions = allShippingOptions.FindAll(s => s.ProfileID != null);
                    var profileShippingOptions = tempShippingOptions.FindAllDistinct("ProfileID");

                    if (profileShippingOptions.Count == allProfiles.Count)
                    {
                        isDeletable = allProfilesCount > 0;
                    }
                }
            }

            return isDeletable;
        }

        /// <summary>
        /// Checks whether or not there are multiple shipping options associated with the same profile ID.
        /// </summary>
        /// <param name="shippingId">The ID of the shipping option.</param>
        /// <returns>True if there are multiple shipping options associated to the same profile ID; otherwise, false.</returns>
        private bool IsSameProfileExist(int shippingId)
        {
            // Get the shipping option
            var shipping = _shippingAdmin.GetShippingOptionById(shippingId);

            // Find the shipping options with the same profile ID
            var allShippingOptions = _shippingAdmin.GetAll();
            var shippingOptionsWithProfileId = allShippingOptions.FindAll(s => s.ProfileID == shipping.ProfileID);

            // Return whether or not there are more than 1 shipping options with the same profile ID
            return shippingOptionsWithProfileId.Count > 1;
        }

        private void BindData()
        {
            var shippingOption = _shippingAdmin.GetShippingOptionById(_shippingId);
            if (shippingOption != null)
            {
                if (IsFranchiseAdmin)
                {
                    UserStoreAccess.CheckProfileAccess(shippingOption.ProfileID.GetValueOrDefault(0), true);
                }

                ShippingOptionName = Server.HtmlDecode(shippingOption.Description);
            }
        }
    }
}