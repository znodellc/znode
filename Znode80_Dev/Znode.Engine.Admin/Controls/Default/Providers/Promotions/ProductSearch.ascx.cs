﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using Znode.Engine.Common;

namespace Znode.Engine.Admin.Controls.Default.Providers.Promotions
{
	public partial class ProductSearch : ProviderBase
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				BindManufacturersDropDown();
				BindCategoriesDropDown();
				BindProductGrid();
			}

			RegisterCloseScript();
		}

		private void RegisterCloseScript()
		{
			var script = @"<script>function selectProduct(var1, var2) { ";

			if (Request.Params["sourceid"] != null && Request.Params["source"] != null)
			{
				script += "window.opener.document.getElementById('" + Request.Params["sourceid"] + "').value = var1;";
				script += "window.opener.document.getElementById('" + Request.Params["source"] + "').value = var2.replace(\":\",\"'\");";

				if (Request.Params["sku"] != null)
				{
					script += "window.opener.document.getElementById('" + Request.Params["sku"] + "').click();";
				}
			}

			script += "window.close(); }</script>";

			Page.ClientScript.RegisterClientScriptBlock(GetType(), "SelectProductPopup", script);
		}

		private void BindManufacturersDropDown()
		{
			// Only show brand/manufacturer if not in Franchise Admin
			if (!IsFranchiseAdmin)
			{
				var mfgAdmin = new ManufacturerAdmin();
				ddlManufacturer.DataSource = mfgAdmin.GetAll();
				ddlManufacturer.DataTextField = "Name";
				ddlManufacturer.DataValueField = "ManufacturerID";
				ddlManufacturer.DataBind();

				// Set "ALL" as default selection
                ddlManufacturer.Items.Insert(0, new ListItem(GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString().ToUpper(), "0"));
				ddlManufacturer.SelectedIndex = 0;
			}
		}

		private void BindCategoriesDropDown()
		{
			var categoryAdmin = new CategoryAdmin();
			ddlProductCategory.DataSource = IsFranchiseAdmin ? UserStoreAccess.CheckStoreAccess(categoryAdmin.GetAllCategories()) : categoryAdmin.GetAllCategories();
			ddlProductCategory.DataTextField = "Name";
			ddlProductCategory.DataValueField = "CategoryID";
			ddlProductCategory.DataBind();

			// Set "ALL" as default selection
            ddlProductCategory.Items.Insert(0, new ListItem(GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString().ToUpper(), "0"));
			ddlProductCategory.SelectedIndex = 0;
		}

		private void BindProductGrid()
		{
			var productAdmin = new ProductAdmin();
			var ds = productAdmin.SearchProductsByKeyword(Server.HtmlEncode(txtKeyword.Text.Trim()), ddlManufacturer.SelectedValue, String.Empty, ddlProductCategory.SelectedValue);
			var dv = IsFranchiseAdmin ? new DataView(UserStoreAccess.CheckStoreAccess(ds.Tables[0])) : new DataView(ds.Tables[0]);
			dv.Sort = "DisplayOrder";

			uxGrid.DataSource = dv;
			uxGrid.DataBind();
		}

		protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			uxGrid.PageIndex = e.NewPageIndex;
			BindProductGrid();
		}

		protected void BtnSearch_Click(object sender, EventArgs e)
		{
			BindProductGrid();
		}

		protected void BtnClear_Click(object sender, EventArgs e)
		{
			txtKeyword.Text = String.Empty;
			ddlManufacturer.SelectedIndex = 0;
			ddlProductCategory.SelectedIndex = 0;
			BindProductGrid();
		}

		protected string GetName(object name)
		{
			var val = String.Empty;

			if (name != null)
			{
				val = name.ToString().Replace("'", ":");
			}

			return val;
		}
	}
}