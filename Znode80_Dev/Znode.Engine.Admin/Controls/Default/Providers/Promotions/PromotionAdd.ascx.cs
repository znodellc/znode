﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using Znode.Engine.Promotions;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Admin.Controls.Default.Providers.Promotions
{
	public partial class PromotionAdd : ProviderBase
	{
		private int _itemId;
		private List<IZnodePromotionType> _availablePromoTypes;

		protected void Page_Load(object sender, EventArgs e)
		{
			_availablePromoTypes = ZnodePromotionManager.GetAvailablePromotionTypes();

			if (Request.Params["itemid"] != null)
			{
				_itemId = IsFranchiseAdmin ? Convert.ToInt32(Encryption.DecryptData(Request.Params["itemid"])) : int.Parse(Request.Params["itemid"]);
			}

			if (!Page.IsPostBack)
			{
				BindPromotionTypes();

				if (!IsFranchiseAdmin)
				{
					BindStores();
					BindProfiles();
					BindCatalogs();
                    BindCategories();
				}

				BindBrands();
				BindQuantityLists();
				ToggleDisplay();

                rgvDiscountAmount.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "RangeDiscountAmount").ToString();
                rgvDiscountPercent.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "RangeDiscountPercent").ToString(); 
                 
                
				if (_itemId > 0)
				{
                    // Edit mode 
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditPromotion").ToString(); 
					BindEditData();
				}
				else
				{
					txtPromoStartDate.Text = DateTime.Now.ToShortDateString();
					txtPromoEndDate.Text = DateTime.Now.AddDays(30).ToShortDateString();
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddNewPromotion").ToString(); 
				}
			}

			btnRequiredProductShowPopup.Attributes.Add("onclick", "var newwindow = window.open('SearchProduct.aspx?sourceid=" + txtRequiredProductId.ClientID + "&source=" + txtRequiredProduct.ClientID + "','RequiredProduct','left=300, top=100, height=720, width=650, status=no, resizable=no, scrollbars=yes, toolbar=no, location=0, menubar=no'); newwindow.focus(); return false;");
			btnDiscountedProductShowPopup.Attributes.Add("onclick", "var newwindow = window.open('SearchProduct.aspx?sourceid=" + txtDiscountedProductId.ClientID + "&source=" + txtDiscountedProduct.ClientID + "','DiscountedProduct','left=300, top=100, height=720, width= 650, status=no, resizable=no, scrollbars=yes, toolbar=no, location=0, menubar=no');  return false;");
			btnCallForPriceProductShowPopup.Attributes.Add("onclick", "var newwindow = window.open('SearchProduct.aspx?sourceid=" + txtCallForPriceProductId.ClientID + "&source=" + txtCallForPriceProduct.ClientID + "&sku=" + btnCallForPriceSku.ClientID + "','CallForPricingProduct','left=300, top=100, height=720, width=650, status=no, resizable=no, scrollbars=yes, toolbar=no, location=0, menubar=no'); newwindow.focus(); return false;");
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			decimal discountAmt = 0;
			decimal minOrderAmt = 0;
			var productAdmin = new ProductAdmin();
			var promoAdmin = new PromotionAdmin();
			var promotion = _itemId > 0 ? promoAdmin.GetByPromotionId(_itemId) : new Promotion();

			// These are nullable
			promotion.ProductID = null;
			promotion.SKUID = null;
			promotion.AddOnValueID = null;
			promotion.AccountID = null;

			// General info
			promotion.Name = Server.HtmlEncode(txtPromoName.Text.Trim());
			promotion.Description = Server.HtmlEncode(txtPromoDesc.Text.Trim());
			promotion.StartDate = Convert.ToDateTime(txtPromoStartDate.Text.Trim());
			promotion.EndDate = Convert.ToDateTime(txtPromoEndDate.Text.Trim());
			promotion.PromoCode = txtPromoCode.Text.Trim();
			promotion.DisplayOrder = int.Parse(txtPromoDisplayOrder.Text.Trim());

			if (ddlDiscountType.SelectedIndex != -1)
			{
				promotion.DiscountTypeID = promoAdmin.GetDiscountTypeId(ddlDiscountType.SelectedValue);
			}

			// Check what to use for PortalID
			if (pnlPortal.Visible)
			{
				promotion.PortalID = ddlPortals.SelectedValue == "0" ? (int?)null : int.Parse(ddlPortals.SelectedValue);
			}
			else
			{
				// We're in franchise admin
				promotion.PortalID = UserStoreAccess.GetTurnkeyStorePortalID;
			}

			// Check what to use for ProfileID
			if (pnlProfile.Visible)
			{
				promotion.ProfileID = ddlProfileTypes.SelectedValue == "0" ? (int?)null : int.Parse(ddlProfileTypes.SelectedValue);
			}
			else
			{
				// We're in franchise admin
				promotion.ProfileID = UserStoreAccess.GetTurnkeyStoreProfileID;
			}

			if (pnlDiscountAmount.Visible)
			{
				decimal.TryParse(txtDiscountAmount.Text, out discountAmt);
			}
			promotion.Discount = discountAmt;

			if (pnlDiscountPercent.Visible)
			{
				decimal discountPct = 0;
				decimal.TryParse(txtDiscountPercent.Text, out discountPct);
				promotion.Discount = discountPct;
			}

			if (pnlMinimumOrderAmount.Visible)
			{
				decimal.TryParse(txtMinimumOrderAmount.Text, out minOrderAmt);
			}
			promotion.OrderMinimum = minOrderAmt;

			if (pnlRequiredBrand.Visible)
			{
				promotion.ManufacturerID = ddlBrands.SelectedValue == "0" ? (int?)null : int.Parse(ddlBrands.SelectedValue);
			}

			if (pnlRequiredBrandMinimumQuantity.Visible)
			{
				promotion.QuantityMinimum = int.Parse(ddlRequiredBrandMinimumQuantity.SelectedValue);
			}

			if (pnlRequiredCatalog.Visible)
			{
				promotion.CatalogID = ddlCatalogs.SelectedValue == "0" ? (int?)null : int.Parse(ddlCatalogs.SelectedValue);
			}

			if (pnlRequiredCatalogMinimumQuantity.Visible)
			{
				promotion.QuantityMinimum = int.Parse(ddlRequiredCatalogMinimumQuantity.SelectedValue);
			}

			if (pnlRequiredCategory.Visible)
			{
				promotion.CategoryID = ddlCategories.SelectedValue == "0" ? (int?)null : int.Parse(ddlCategories.SelectedValue);
			}

			if (pnlRequiredCategoryMinimumQuantity.Visible)
			{
				promotion.QuantityMinimum = int.Parse(ddlRequiredCategoryMinimumQuantity.SelectedValue);
			}

			if (pnlRequiredProduct.Visible)
			{
				promotion.ProductID = txtRequiredProductId.Text != "0" ? int.Parse(txtRequiredProductId.Text.Trim()) : productAdmin.GetProductIdByName(txtRequiredProduct.Text.Trim());
			}

			if (pnlRequiredProductMinimumQuantity.Visible)
			{
				promotion.QuantityMinimum = int.Parse(ddlRequiredProductMinimumQuantity.SelectedValue);
			}

			if (pnlDiscountedProduct.Visible)
			{
				promotion.PromotionProductID = txtDiscountedProductId.Text != "0" ? int.Parse(txtDiscountedProductId.Text.Trim()) : productAdmin.GetProductIdByName(txtDiscountedProduct.Text.Trim());
			}

			if (pnlDiscountedProductQuantity.Visible)
			{
				promotion.PromotionProductQty = int.Parse(ddlDiscountedProductQuantity.SelectedValue);
			}

			if (pnlCallForPriceProduct.Visible)
			{
				promotion.ProductID = Convert.ToInt32(txtCallForPriceProductId.Text);
			}

			if (pnlCallForPriceSku.Visible)
			{
				promotion.PromotionProductID = Convert.ToInt32(ddlCallForPriceSkus.SelectedValue);
			}
			
			// Coupon info
			if (chkCouponInd.Checked && pnlCoupon.Visible)
			{
				promotion.CouponInd = chkCouponInd.Checked;
				promotion.CouponCode = txtCouponCode.Text;
				promotion.PromotionMessage = Server.HtmlEncode(txtPromotionMessage.Text);
				promotion.CouponQuantityAvailable = int.Parse(txtCouponQuantityAvailable.Text);
				promotion.EnableCouponUrl = chkCouponUrl.Checked;
			}
			else
			{
				promotion.CouponInd = false;
				promotion.CouponCode = String.Empty;
				promotion.PromotionMessage = String.Empty;
				promotion.CouponQuantityAvailable = 0;
				promotion.EnableCouponUrl = false;
			}

			if (pnlCallForPriceMessage.Visible)
			{
				promotion.PromotionMessage = Server.HtmlEncode(txtCallForPriceMessage.Text);
			}

			// Check for duplicate promotion
			if (promoAdmin.IsPromotionOverlapped(promotion))
			{
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDuplicatePrommotion").ToString();
				ddlDiscountType.SelectedIndex = -1;
				rfvRequiredProduct.Visible = false;
				return;
			}

			// Now that we've built the promotion, try to edit/add it
			var success = false;

			if (_itemId > 0)
			{
				success = promoAdmin.UpdatePromotion(promotion);

                string editPromotion = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddOn").ToString(), txtPromoName.Text.Trim(), txtPromoName.Text.Trim());
                 
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null,editPromotion,txtPromoName.Text.Trim());
               
            }
			else
			{
				success = promoAdmin.AddPromotion(promotion);

                string addPromotion = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddOn").ToString(), txtPromoName.Text.Trim(), txtPromoName.Text.Trim());

                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, addPromotion, txtPromoName.Text.Trim());
			}

			if (success)
			{
				Response.Redirect("Default.aspx");
			}
			else
			{
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSavePromotion").ToString();
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect("Default.aspx");
		}

		protected void ddlDiscountType_SelectedIndexChanged(object sender, EventArgs e)
		{
			ToggleDisplay();
		}

		protected void ddlPortals_SelectedIndexChanged(object sender, EventArgs e)
		{
			BindProfiles();
			BindCatalogs();
            BindCategories();
		}

		protected void btnCallForPriceSku_Click(object sender, EventArgs e)
		{
			if (!String.IsNullOrEmpty(txtCallForPriceProductId.Text))
			{
				var productId = 0;
				int.TryParse(txtCallForPriceProductId.Text, out productId);

				BindSkus(productId);
			}
		}

		private void BindPromotionTypes()
		{
			var discountTypes = GetDiscountTypes();
			discountTypes.Sort("Name");

			ddlDiscountType.DataSource = discountTypes;
			ddlDiscountType.DataTextField = "Name";
			ddlDiscountType.DataValueField = "ClassName";
			ddlDiscountType.DataBind();

			DecodeListItems(ddlDiscountType);
		}

		private void BindStores()
		{
			var portalAdmin = new PortalAdmin();
			var portals = portalAdmin.GetAllPortals();

			ddlPortals.DataSource = portals;
			ddlPortals.DataTextField = "StoreName";
			ddlPortals.DataValueField = "PortalID";
			ddlPortals.DataBind();

			// Add "All Stores" to top of the list and decode item text
			ddlPortals.Items.Insert(0, new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAllStores").ToString(), "0"));
			ddlPortals.SelectedValue = "0";

			DecodeListItems(ddlPortals);
		}

		private void BindProfiles()
		{
			var profileAdmin = new ProfileAdmin();

			if (ddlPortals.SelectedValue == "0")
			{
				var profiles = profileAdmin.GetAll();
				ddlProfileTypes.DataSource = profiles;
			}
			else if (ddlPortals.SelectedValue != "0" || _itemId != 0)
			{
				var profiles = profileAdmin.GetAssociatedProfilesByPortalID(Convert.ToInt32(ddlPortals.SelectedValue));
				ddlProfileTypes.DataSource = UserStoreAccess.CheckProfileAccess(profiles.Tables[0]);
			}

			ddlProfileTypes.DataTextField = "Name";
			ddlProfileTypes.DataValueField = "ProfileID";
			ddlProfileTypes.DataBind();

            // Add "All Profiles" to top of the list and decode item text  
            ddlProfileTypes.Items.Insert(0, new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAllProfiles").ToString(), "0"));
			DecodeListItems(ddlProfileTypes);
		}

		private void BindBrands()
		{
			var mfgAdmin = new ManufacturerAdmin();
			var brands = IsFranchiseAdmin ? UserStoreAccess.CheckStoreAccess(mfgAdmin.GetAll()) : mfgAdmin.GetAll();
			brands.Sort("Name");

			ddlBrands.DataSource = brands;
			ddlBrands.DataTextField = "Name";
			ddlBrands.DataValueField = "ManufacturerID";
			ddlBrands.DataBind();

			// Set "All Brands" to top of the list and decode item text
            ddlBrands.Items.Insert(0, new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownAllBrands").ToString(), "0"));
			ddlBrands.SelectedIndex = 0;

			DecodeListItems(ddlBrands);
		}

		private void BindCatalogs()
		{
			var catalogAdmin = new CatalogAdmin();
			var catalogHelper = new CatalogHelper();

			if (ddlPortals.SelectedValue != "0" || _itemId != 0)
			{
				var ds = catalogHelper.GetCatalogsByPortalId(Convert.ToInt32(ddlPortals.SelectedValue));
				var dr = ds.Tables[0].NewRow();
				ddlCatalogs.DataSource = ds.Tables[0];
			}
			else
			{
				var catalogs = IsFranchiseAdmin ? UserStoreAccess.CheckStoreAccess(catalogAdmin.GetAllCatalogs()) : catalogAdmin.GetAllCatalogs();
				catalogs.Sort("Name");
				ddlCatalogs.DataSource = catalogs;
			}

			ddlCatalogs.DataTextField = "Name";
			ddlCatalogs.DataValueField = "CatalogID";
			ddlCatalogs.DataBind();

            // Set "All Catalogs" to top of the list and decode item text 
			if (ddlPortals.SelectedValue == "0")
			{
                ddlCatalogs.Items.Insert(0, new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownAllCatalogs").ToString(), "0"));
			}

			ddlCatalogs.SelectedIndex = 0;
			DecodeListItems(ddlCatalogs);
		}

		private void BindCategories()
		{
			var categoryAdmin = new CategoryAdmin();
            var categoryHelper = new CategoryHelper();
            var catalogAdmin = new CatalogAdmin();
            int catalogId = 0;

            if (ddlPortals.SelectedValue == "0")
            {
                var categories = IsFranchiseAdmin ? UserStoreAccess.CheckStoreAccess(categoryAdmin.GetAllCategories()) : categoryAdmin.GetAllCategories();
                categories.Sort("Name");
                ddlCategories.DataSource = categories;
            }
            else if (ddlPortals.SelectedValue != "0" || _itemId != 0)
            {
                catalogId = catalogAdmin.GetCatalogIDByPortalID(Convert.ToInt32(ddlPortals.SelectedValue));
                var categories = categoryHelper.GetCategoryByCatalogID(catalogId);
                ddlCategories.DataSource = categories;
            }
			ddlCategories.DataTextField = "Name";
			ddlCategories.DataValueField = "CategoryID";
			ddlCategories.DataBind();

			// Set "All Categories" to top of the list and decode item text
			ddlCategories.Items.Insert(0, new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownAllCategories").ToString(), "0"));
			ddlCategories.SelectedIndex = 0;
			DecodeListItems(ddlCategories);
		}

		private void BindQuantityLists()
		{
			for (var i = 1; i < 15; i++)
			{
				ddlDiscountedProductQuantity.Items.Add(i.ToString());
				ddlRequiredBrandMinimumQuantity.Items.Add(i.ToString());
				ddlRequiredCatalogMinimumQuantity.Items.Add(i.ToString());
				ddlRequiredCategoryMinimumQuantity.Items.Add(i.ToString());
				ddlRequiredProductMinimumQuantity.Items.Add(i.ToString());
			}
		}

		private void BindEditData()
		{
			var promoAdmin = new PromotionAdmin();
			var promotion = promoAdmin.DeepLoadByPromotionId(_itemId);

			if (promotion != null)
			{
				if (IsFranchiseAdmin)
				{
					UserStoreAccess.CheckProfileAccess(promotion.ProfileID.GetValueOrDefault(0), true);
				}

				// General section
				txtPromoName.Text = Server.HtmlDecode(promotion.Name);
				txtPromoDesc.Text = Server.HtmlDecode(promotion.Description);
				txtPromoStartDate.Text = promotion.StartDate.ToShortDateString();
				txtPromoEndDate.Text = promotion.EndDate.ToShortDateString();
				txtPromoDisplayOrder.Text = promotion.DisplayOrder.ToString();

				if (!String.IsNullOrEmpty(promotion.PromoCode))
				{
					txtPromoCode.Text = promotion.PromoCode;
				}

				// Discount             
				txtDiscountAmount.Text = promotion.Discount.ToString();
				txtDiscountPercent.Text = promotion.Discount.ToString();
				ddlDiscountType.SelectedValue = promotion.DiscountTypeIDSource.ClassName;
				ToggleDisplay();

				var productAdmin = new ProductAdmin();

				if (!String.IsNullOrEmpty(promotion.DiscountTypeIDSource.ClassName))
				{
					if (!IsFranchiseAdmin)
					{
						if (promotion.PortalID.HasValue)
						{
							ddlPortals.SelectedValue = promotion.PortalID.Value.ToString();
							BindProfiles();
							BindCatalogs();
                            BindCategories();
						}

						if (promotion.ProfileID.HasValue)
						{
							ddlProfileTypes.SelectedValue = promotion.ProfileID.Value.ToString();
						}
					}

					if (promotion.ManufacturerID.HasValue)
					{
						ddlBrands.SelectedValue = promotion.ManufacturerID.Value.ToString();
					}

					if (promotion.CatalogID.HasValue)
					{
						ddlCatalogs.SelectedValue = promotion.CatalogID.Value.ToString();
					}

					if (promotion.CategoryID.HasValue)
					{
						ddlCategories.SelectedValue = promotion.CategoryID.Value.ToString();
					}

					if (promotion.ProductID.HasValue)
					{
						txtRequiredProductId.Text = promotion.ProductID.Value.ToString();
						txtRequiredProduct.Text = promotion.ProductIDSource.Name;
					}

					if (promotion.PromotionProductID.HasValue)
					{
						txtDiscountedProductId.Text = promotion.PromotionProductID.Value.ToString();
						txtDiscountedProduct.Text = Server.HtmlEncode(productAdmin.GetProductName(promotion.PromotionProductID.Value));
						ddlDiscountedProductQuantity.SelectedValue = promotion.PromotionProductQty.GetValueOrDefault(1).ToString();
					}

					txtDiscountedProductId.Text = promotion.PromotionProductID.GetValueOrDefault(0).ToString();
					ddlDiscountedProductQuantity.SelectedValue = promotion.PromotionProductQty.GetValueOrDefault(0).ToString();
					ddlRequiredBrandMinimumQuantity.SelectedValue = promotion.QuantityMinimum.GetValueOrDefault(1).ToString();
					ddlRequiredCatalogMinimumQuantity.SelectedValue = promotion.QuantityMinimum.GetValueOrDefault(1).ToString();
					ddlRequiredCategoryMinimumQuantity.SelectedValue = promotion.QuantityMinimum.GetValueOrDefault(1).ToString();
					ddlRequiredProductMinimumQuantity.SelectedValue = promotion.QuantityMinimum.GetValueOrDefault(1).ToString();

					// Coupon info
					chkCouponInd.Checked = promotion.CouponInd;
					chkCouponUrl.Checked = promotion.EnableCouponUrl.GetValueOrDefault();
					txtCouponCode.Text = promotion.CouponCode;

					if (chkCouponInd.Checked)
					{
						pnlCouponInfo.Visible = true;
						pnlCouponInfo.Attributes.Clear();
					}

					if (chkCouponUrl.Checked)
					{
						pnlCouponUrl.Visible = true;

					    if (txtCouponCode.Text.Length > 0)
					    {

					        string Couponcode = txtCouponCode.Text;
                            lblCouponName.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "SubTextCouponCodeName").ToString(),
					                                           Couponcode);
                             
                           
					    }
					    else
					    {
                            lblCouponName.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTextCouponCode").ToString();
					    }
					}

					txtPromotionMessage.Text = Server.HtmlDecode(promotion.PromotionMessage);

					if (pnlCallForPriceProduct.Visible)
					{
						txtCallForPriceProductId.Text = promotion.ProductID.GetValueOrDefault(0).ToString();
						txtCallForPriceProduct.Text = Server.HtmlEncode(productAdmin.GetProductName(promotion.ProductID.GetValueOrDefault(0)));
					}

					if (pnlCallForPriceSku.Visible)
					{
						BindSkus(promotion.ProductID.GetValueOrDefault(0));
						ddlCallForPriceSkus.SelectedValue = promotion.PromotionProductID.GetValueOrDefault(0).ToString();
					}

					if (pnlCallForPriceMessage.Visible)
					{
						txtCallForPriceMessage.Text = Server.HtmlDecode(promotion.PromotionMessage);
					}

					if (promotion.CouponQuantityAvailable.HasValue)
					{
						txtCouponQuantityAvailable.Text = promotion.CouponQuantityAvailable.Value.ToString();
					}

					if (promotion.OrderMinimum.HasValue)
					{
						txtMinimumOrderAmount.Text = promotion.OrderMinimum.Value.ToString("N2");
					}

					// Set page title
					lblTitle.Text += promotion.Name;
				}
			}
		}

		private void BindSkus(int productId)
		{
			if (productId > 0)
			{
				var skuAdmin = new SKUAdmin();
				var skus = skuAdmin.GetByProductID(productId);

				ddlCallForPriceSkus.DataSource = skus;
				ddlCallForPriceSkus.DataTextField = "SKU";
				ddlCallForPriceSkus.DataValueField = "SKUID";
				ddlCallForPriceSkus.DataBind();
			}
		}

		private void ToggleDisplay()
		{
			// First hide everything
			pnlPortal.Visible = false;
			pnlProfile.Visible = false;
			pnlDiscountAmount.Visible = false;
			pnlDiscountPercent.Visible = false;
			pnlRequiredBrand.Visible = false;
			pnlRequiredBrandMinimumQuantity.Visible = false;
			pnlRequiredCatalog.Visible = false;
			pnlRequiredCatalogMinimumQuantity.Visible = false;
			pnlRequiredCategory.Visible = false;
			pnlRequiredCategoryMinimumQuantity.Visible = false;
			pnlRequiredProduct.Visible = false;
			pnlRequiredProductMinimumQuantity.Visible = false;
			pnlDiscountedProduct.Visible = false;
			pnlDiscountedProductQuantity.Visible = false;
			pnlMinimumOrderAmount.Visible = false;
			pnlCallForPriceProduct.Visible = false;
			pnlCallForPriceSku.Visible = false;
			pnlCallForPriceMessage.Visible = false;
			pnlCoupon.Visible = false;

			// Then loop through to show only controls that are needed
			foreach (var promo in _availablePromoTypes)
			{
				if (promo.ClassName == ddlDiscountType.SelectedValue)
				{
					foreach (var control in promo.Controls)
					{
						ShowControl(control);

						// For franchise admin, we don't show the store or profile
						if (IsFranchiseAdmin)
						{
							pnlPortal.Visible = false;
							pnlProfile.Visible = false;
						}
					}
				}
			}
		}

		private void ShowControl(ZnodePromotionControl control)
		{
			if (control == ZnodePromotionControl.Store) pnlPortal.Visible = true;
			if (control == ZnodePromotionControl.Profile) pnlProfile.Visible = true;
			if (control == ZnodePromotionControl.DiscountAmount) pnlDiscountAmount.Visible = true;
			if (control == ZnodePromotionControl.DiscountPercent) pnlDiscountPercent.Visible = true;
			if (control == ZnodePromotionControl.RequiredBrand) pnlRequiredBrand.Visible = true;
			if (control == ZnodePromotionControl.RequiredBrandMinimumQuantity) pnlRequiredBrandMinimumQuantity.Visible = true;
			if (control == ZnodePromotionControl.RequiredCatalog) pnlRequiredCatalog.Visible = true;
			if (control == ZnodePromotionControl.RequiredCatalogMinimumQuantity) pnlRequiredCatalogMinimumQuantity.Visible = true;
			if (control == ZnodePromotionControl.RequiredCategory) pnlRequiredCategory.Visible = true;
			if (control == ZnodePromotionControl.RequiredCategoryMinimumQuantity) pnlRequiredCategoryMinimumQuantity.Visible = true;
			if (control == ZnodePromotionControl.RequiredProduct) pnlRequiredProduct.Visible = true;
			if (control == ZnodePromotionControl.RequiredProductMinimumQuantity) pnlRequiredProductMinimumQuantity.Visible = true;
			if (control == ZnodePromotionControl.DiscountedProduct) { pnlDiscountedProduct.Visible = true; pnlCoupon.Visible = false; }
			if (control == ZnodePromotionControl.DiscountedProductQuantity) pnlDiscountedProductQuantity.Visible = true;
			if (control == ZnodePromotionControl.MinimumOrderAmount) pnlMinimumOrderAmount.Visible = true;
			if (control == ZnodePromotionControl.CallForPriceProduct) { pnlCallForPriceProduct.Visible = true; pnlCoupon.Visible = false; }
			if (control == ZnodePromotionControl.CallForPriceSku) pnlCallForPriceSku.Visible = true;
			if (control == ZnodePromotionControl.CallForPriceMessage) pnlCallForPriceMessage.Visible = true;
			if (control == ZnodePromotionControl.Coupon) pnlCoupon.Visible = true;
		}
	}
}