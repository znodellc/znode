﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PromotionAdd.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Promotions.PromotionAdd" %>
<%@ Import Namespace="ZNode.Libraries.ECommerce.Catalog" %>

<script type="text/javascript">
	var hiddenTextValue;
	var couponUrlText = "<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","TextPromotionCouponMessage")) %>";
	var cc = "&lt;COUPON CODE&gt;";

	function AutoComplete_RequiredProductSelected(source, eventArgs) {
		hiddenTextValue = $get("<%=txtRequiredProductId.ClientID %>");
		hiddenTextValue.value = eventArgs.get_value();
	}

	function AutoComplete_RequiredProductShowing(source, eventArgs) {
		hiddenTextValue = $get("<%=txtRequiredProductId.ClientID %>");
        hiddenTextValue.value = 0;
    }

    function AutoComplete_DiscountedProductSelected(source, eventArgs) {
        hiddenTextValue = $get("<%=txtDiscountedProductId.ClientID %>");
        hiddenTextValue.value = eventArgs.get_value();
    }

    function AutoComplete_DiscountedProductShowing(source, eventArgs) {
        hiddenTextValue = $get("<%=txtDiscountedProductId.ClientID %>");
        hiddenTextValue.value = 0;
    }

    function SetupCouponSection() {
        var chkCouponInd = document.getElementById("<%=chkCouponInd.ClientID %>");
        var pnlCouponInfo = document.getElementById("<%=pnlCouponInfo.ClientID %>");
        var regCouponCode = document.getElementById("<%=regCouponCode.ClientID %>");
        var rfvCouponCode = document.getElementById("<%=rfvCouponCode.ClientID %>");
        var rfvCouponQuantityAvailable = document.getElementById("<%=rfvCouponQuantityAvailable.ClientID %>");
        var rgvCouponQuantityAvailable = document.getElementById("<%=rgvCouponQuantityAvailable.ClientID %>");
        var state = false;

        if (chkCouponInd) {
        	state = chkCouponInd.checked;

        	ValidatorEnable(regCouponCode, state);
        	ValidatorEnable(rfvCouponCode, state);
        	ValidatorEnable(rfvCouponQuantityAvailable, state);
        	ValidatorEnable(rgvCouponQuantityAvailable, state);

        	if (state == true) {
        		pnlCouponInfo.style.display = '';
        	}
        	else {
        		pnlCouponInfo.style.display = 'none';
        	}
        }
    }

    function UpdateCouponUrl() {
        var chkCouponUrl = document.getElementById("<%=chkCouponUrl.ClientID %>");
        var txtCouponCode = document.getElementById("<%=txtCouponCode.ClientID %>");
        var lblCouponName = document.getElementById("<%=lblCouponName.ClientID %>");

        if (chkCouponUrl.checked) {

        	if (txtCouponCode.value.length > 0) {
        		cc = txtCouponCode.value;
        	}

        	lblCouponName.innerHTML = couponUrlText + cc + "</br>";
        }
    }

    // Enable or disable the coupon code regular expression validator
    function ToggleCouponUrl() {
        var chkCouponUrl = document.getElementById("<%=chkCouponUrl.ClientID %>");
        var pnlCouponUrl = document.getElementById("<%=pnlCouponUrl.ClientID %>");
        var txtCouponCode = document.getElementById("<%=txtCouponCode.ClientID %>");
        var lblCouponName = document.getElementById("<%=lblCouponName.ClientID %>");

        if (chkCouponUrl.checked == true) {
        	// Show the coupon url panel
        	pnlCouponUrl.style.display = '';

        	if (txtCouponCode.value.length > 0) {
        		cc = txtCouponCode.value;
        	}

        	lblCouponName.innerHTML = couponUrlText + cc + "</br>";
        }
        else {
        	// Hide the coupon url panel
        	pnlCouponUrl.style.display = 'none';
        }
    }
</script>

<div class="FormView">
	<div class="LeftFloat" style="width: auto; text-align: left">
		<h1>
			<asp:Label ID="lblTitle" runat="server" />
		</h1>
	</div>

	<div style="text-align: right">
        <zn:Button ID="btnSubmitTop" runat="server" ButtonType="SubmitButton" OnClientClick="SetupCouponSection()" CausesValidation="True" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
        <zn:Button ID="btnCancelTop" runat="server" ButtonType="CancelButton" OnClientClick="SetupCouponSection()" CausesValidation="False" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel %>" />

	</div>

	<div class="ClearBoth"></div>

	<div class="Error">
		<asp:Label ID="lblError" runat="server" Visible="true" />
	</div>

	<h4 class="SubTitle"><asp:Localize ID="TitleSearchProducts" runat="server" Text="<%$ Resources:ZnodeAdminResource, TitleGeneralInformation %>" /></h4>

	<div class="FieldStyle">
		<asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitlePromotionName %>" /><span class="Asterix"> *</span>
	</div>
	<div class="ValueStyle">
		<asp:TextBox ID="txtPromoName" runat="server" Columns="25" />
		<asp:RequiredFieldValidator CssClass="Error" ID="rfvPromoName" runat="server" ControlToValidate="txtPromoName" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, Required %>" SetFocusOnError="True" />
	</div>

	<div class="FieldStyle">
		<asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnPromotionDescription %>" />
	</div>
	<div class="ValueStyle">
		<asp:TextBox ID="txtPromoDesc" runat="server" Columns="25" />
	</div>

	<div class="FieldStyle">
		<asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleStartDateFormat %>" /><span class="Asterix"> *</span>
	</div>
	<div class="ValueStyle">
		<asp:TextBox ID="txtPromoStartDate" runat="server" Text="01/01/2007" />
		<asp:ImageButton ID="imgBtnPromoStartDate" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" CausesValidation="false" />
		<asp:RegularExpressionValidator ID="regPromoStartDate" runat="server" ControlToValidate="txtPromoStartDate" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, RegularValidStartDate %>" ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))">
        </asp:RegularExpressionValidator>
		<asp:RequiredFieldValidator ID="rfvPromoStartDate" runat="server" ControlToValidate="txtPromoStartDate" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, Required %>" SetFocusOnError="True">
        </asp:RequiredFieldValidator>
	</div>

	<ajaxToolKit:CalendarExtender ID="calPromoStartDate" runat="server" Enabled="true" PopupButtonID="imgBtnPromoStartDate" TargetControlID="txtPromoStartDate" />

	<div class="FieldStyle">
		<asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleEndDateFormat %>" /> <span class="Asterix">*</span>
	</div>
	<div class="ValueStyle">
		<asp:TextBox ID="txtPromoEndDate" runat="server" Text="01/01/2007" />
		<asp:ImageButton ID="imgBtnPromoEndDate" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" CausesValidation="false" />
		<asp:RegularExpressionValidator ID="regPromoEndDate" runat="server" ControlToValidate="txtPromoEndDate" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, RegularValidEndDate %>" ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))">
        </asp:RegularExpressionValidator>
		<asp:RequiredFieldValidator ID="rfvPromoEndDate" runat="server" ControlToValidate="txtPromoEndDate" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, Required %>" SetFocusOnError="True" />
		<asp:CompareValidator ID="cmpPromoEndDate" runat="server" ControlToCompare="txtPromoStartDate" ControlToValidate="txtPromoEndDate" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, CompareDate %>" Operator="GreaterThanEqual" Type="Date">
        </asp:CompareValidator>
	</div>

	<ajaxToolKit:CalendarExtender ID="calPromoEndDate" runat="server" Enabled="true" PopupButtonID="imgBtnPromoEndDate" TargetControlID="txtPromoEndDate" />

	<div class="FieldStyle">
		<asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitlePromotionCode %>" /> <span class="Asterix">*</span>
	</div>
	<div class="ValueStyle">
		<asp:TextBox ID="txtPromoCode" runat="server" />
		<asp:RequiredFieldValidator ID="rfvPromoCode" runat="server" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, Required %>" ControlToValidate="txtPromoCode" />
	</div>

	<div class="FieldStyle">
        <asp:Localize ID="lblDisplayorder" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>">
        </asp:Localize> <span class="Asterix">*</span><br />
	</div>
	<div class="ValueStyle">
		<asp:TextBox ID="txtPromoDisplayOrder" runat="server" MaxLength="9" Columns="9" />
		<asp:RequiredFieldValidator ID="rfvPromoDisplayOrder" runat="server" ControlToValidate="txtPromoDisplayOrder" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, Required %>" />
		<asp:RangeValidator ID="rgvPromoDisplayOrder" runat="server" ControlToValidate="txtPromoDisplayOrder" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, RangeWholeNumber %>" Type="Integer" MinimumValue="1" MaximumValue="999999999" />
	</div>

	<h4 class="SubTitle">
	    <asp:Localize ID="lblDiscount" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextDiscount %>" />
        </h4>

	<div class="FieldStyle">
	    <asp:Localize ID="lblDiscountType" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleDiscountType %>" />
        
	</div>
	<div class="ValueStyle">
		<asp:DropDownList ID="ddlDiscountType" runat="server" OnSelectedIndexChanged="ddlDiscountType_SelectedIndexChanged" AutoPostBack="True" />
	</div>

	<asp:Panel ID="pnlPortal" runat="server">
		<div class="FieldStyle">
			<asp:Localize ID="lblStore" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleStore %>" /> <span class="Asterix">*</span><br />
			<small><asp:Localize ID="lblHintStore" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintStore %>" /></small>
		</div>
		<div class="ValueStyle">
			<asp:DropDownList ID="ddlPortals" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlPortals_SelectedIndexChanged" />
		</div>
	</asp:Panel>

	<asp:Panel ID="pnlProfile" runat="server">
		<div class="FieldStyle">
			<asp:Localize ID="lblProfile" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleProfile %>" /> <span class="Asterix">*</span><br />
			<small><asp:Localize ID="lblHintProfile" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintProfile %>" /></small>
		</div>
		<div class="ValueStyle">
			<asp:DropDownList ID="ddlProfileTypes" runat="server" />
		</div>
	</asp:Panel>

	<asp:Panel ID="pnlDiscountAmount" runat="server">
		<div class="FieldStyle">
			<asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleDiscountAmount %>" /> <span class="Asterix">*</span><br />
			<small>
                <asp:Localize ID="lbl" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintDiscountAmount %>">
                </asp:Localize></small>
		</div>
		<div class="ValueStyle">
			<asp:TextBox ID="txtDiscountAmount" runat="server" MaxLength="7" Columns="25" />
			<asp:RequiredFieldValidator ID="rfvDiscountAmount" runat="server" ControlToValidate="txtDiscountAmount" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, Required %>" SetFocusOnError="True" />
			<asp:RangeValidator ID="rgvDiscountAmount" runat="server" ControlToValidate="txtDiscountAmount" CssClass="Error" Display="Dynamic" MinimumValue="0.01" MaximumValue="9999999" CultureInvariantValues="True" Type="Currency" SetFocusOnError="True" ErrorMessage="<%$ Resources:ZnodeAdminResource, RangeDiscountAmount %>" />
		</div>
	</asp:Panel>
		
	<asp:Panel ID="pnlDiscountPercent" runat="server">
		<div class="FieldStyle">
			<asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleDiscountPercent %>" /> <span class="Asterix">*</span><br />
			<small><asp:Localize ID="lblHintDiscountPercent" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintDiscountPercent %>" /></small>
		</div>
		<div class="ValueStyle">
			<asp:TextBox ID="txtDiscountPercent" runat="server" MaxLength="7" Columns="25" />
			<asp:RequiredFieldValidator ID="rfvDiscountPercent" runat="server" ControlToValidate="txtDiscountPercent" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, Required %>" SetFocusOnError="True" />
			<asp:RangeValidator ID="rgvDiscountPercent" runat="server" ControlToValidate="txtDiscountPercent" CssClass="Error" Display="Dynamic" MinimumValue="0.01" MaximumValue="100" CultureInvariantValues="True" Type="Double" SetFocusOnError="True" ErrorMessage="<%$ Resources:ZnodeAdminResource, RangeDiscountPercent %>" />
		</div>
	</asp:Panel>

	<asp:UpdatePanel ID="updPnlProducts" runat="server" UpdateMode="Conditional">
		<ContentTemplate>
			<asp:Panel ID="pnlRequiredBrand" runat="server">
				<div class="FieldStyle">
					<asp:Localize ID="lblRequiredBrand" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleRequiredBrand %>" /> <span class="Asterix">*</span><br />
					<small><asp:Localize ID="lblhintRequiredBrand" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintRequiredBrand %>" /></small>
				</div>
				<div class="ValueStyle">
					<asp:DropDownList ID="ddlBrands" runat="server" />
				</div>
			</asp:Panel>
			
			<asp:Panel ID="pnlRequiredBrandMinimumQuantity" runat="server">
				<div class="FieldStyle">
					<asp:Localize ID="lblMinimumQuantityBrand" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleMinimumQuantity %>" /><br />
					<small><asp:Localize ID="lblHintMinQuantityBrand" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintMinimumQuantity %>" /></small>
				</div>
				<div class="ValueStyle">
					<asp:DropDownList ID="ddlRequiredBrandMinimumQuantity" runat="server" Width="50" />
				</div>
			</asp:Panel>
			
			<asp:Panel ID="pnlRequiredCatalog" runat="server">
				<div class="FieldStyle">
					<asp:Localize ID="lblRequiredCatalog" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleRequiredCatalog %>" /> <span class="Asterix">*</span><br />
					<small><asp:Localize ID="lblHintRequiredCatalog" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintRequiredCatalog %>" /></small>
				</div>
				<div class="ValueStyle">
					<asp:DropDownList ID="ddlCatalogs" runat="server" />
				</div>
			</asp:Panel>
			
			<asp:Panel ID="pnlRequiredCatalogMinimumQuantity" runat="server">
				<div class="FieldStyle">
					<asp:Localize ID="lblMinQtyCatalog" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleMinimumQuantity %>" /><br />
					<small><asp:Localize ID="lblHintMinQtyCatalog" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintMinimumQuantity %>" /></small>
				</div>
				<div class="ValueStyle">
					<asp:DropDownList ID="ddlRequiredCatalogMinimumQuantity" runat="server" Width="50" />
				</div>
			</asp:Panel>

			<asp:Panel ID="pnlRequiredCategory" runat="server">
				<div class="FieldStyle">
					<asp:Localize ID="lblRequiredCategory" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleRequiredCategory %>" /> <span class="Asterix">*</span><br />
					<small><asp:Localize ID="lblHintRequiredCategory" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintRequiredCategory %>" /></small>
				</div>
				<div class="ValueStyle">
					<asp:DropDownList ID="ddlCategories" runat="server" />
				</div>
			</asp:Panel>

			<asp:Panel ID="pnlRequiredCategoryMinimumQuantity" runat="server">
				<div class="FieldStyle">
					<asp:Localize ID="lblMinQuantityCategory" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleMinimumQuantity %>" /><br />
					<small><asp:Localize ID="lblHintMinQtyCategory" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintMinimumQuantity %>" /></small>
				</div>
				<div class="ValueStyle">
					<asp:DropDownList ID="ddlRequiredCategoryMinimumQuantity" runat="server" Width="50" />
				</div>
			</asp:Panel>
			
			<asp:Panel ID="pnlRequiredProduct" runat="server">
				<asp:TextBox ID="txtRequiredProductId" runat="server" CssClass="HiddenFieldStyle" Text="0" />

				<div class="FieldStyle">
					<asp:Localize ID="lblRequiredProduct" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleRequiredProduct %>" /> <span class="Asterix">*</span><br />
					<small><asp:Localize ID="lblHintRequiredProduct" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintRequiredProduct %>" /></small>
				</div>
				<div class="ValueStyle">
					<span class="LeftFloat">
						<asp:TextBox ID="txtRequiredProduct" runat="server" ReadOnly="true" AutoPostBack="true" AutoCompleteType="None" autocomplete="off" />
					</span>
					<span>
						<asp:Image ID="btnRequiredProductShowPopup" runat="server" AlternateText="<%$ Resources:ZnodeAdminResource, ColumnTitleProduct %>" CssClass="SearchIcon" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/enlarge.gif" ToolTip="<%$ Resources:ZnodeAdminResource, SubTitleSearchProducts %>" />
					</span>
					<asp:RequiredFieldValidator ID="rfvRequiredProduct" runat="server" ControlToValidate="txtRequiredProduct" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredSelectProduct %>">
                    </asp:RequiredFieldValidator>
				</div>

				<ajaxToolKit:AutoCompleteExtender ID="aceRequiredProduct" runat="server"
					TargetControlID="txtRequiredProduct"
					ServicePath="ZNodeCatalogServices.asmx"
					ServiceMethod="GetCompletionListWithContextAndValues"
					UseContextKey="true"
					MinimumPrefixLength="2"
					EnableCaching="false"
					CompletionSetCount="100"
					CompletionInterval="1"
					DelimiterCharacters=";, :"
					BehaviorID="autoCompleteBehavior3"
					OnClientPopulating="AutoComplete_RequiredProductShowing"
					OnClientItemSelected="AutoComplete_RequiredProductSelected"
				/>  
			</asp:Panel>
				
			<asp:Panel ID="pnlRequiredProductMinimumQuantity" runat="server">
				<div class="FieldStyle">
					<asp:Localize ID="lblMinQtyProduct" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleMinimumQuantity %>" /><br />
					<small><asp:Localize ID="lblHintMinQtyProduct" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintMinimumQuantity %>" /></small>
				</div>
				<div class="ValueStyle">
					<asp:DropDownList ID="ddlRequiredProductMinimumQuantity" runat="server" Width="50" />
				</div>
			</asp:Panel>

			<asp:Panel ID="pnlDiscountedProduct" runat="server">
				<asp:TextBox ID="txtDiscountedProductId" runat="server" CssClass="HiddenFieldStyle" Text="0" />

				<div class="FieldStyle">
					<asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleProductDiscount %>" /><span class="Asterix">*</span><br/>
					<small><asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintProductDiscount %>" /></small>
				</div>
				<div class="ValueStyle">
					<span class="LeftFloat">
						<asp:TextBox ID="txtDiscountedProduct" runat="server" ReadOnly="true" AutoPostBack="true" AutoCompleteType="None" autocomplete="off" />
					</span>
					<span>
						<asp:Image ID="btnDiscountedProductShowPopup" runat="server" AlternateText="<%$ Resources:ZnodeAdminResource, TextSearch %>" CssClass="SearchIcon" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/enlarge.gif" ToolTip="<%$ Resources:ZnodeAdminResource, SubTitleSearchProducts %>" />
					</span> 
					<asp:RequiredFieldValidator ID="rfvDiscountedProduct" runat="server" ControlToValidate="txtDiscountedProduct" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, Required %>" />
				 
				</div>

				<ajaxToolKit:AutoCompleteExtender ID="aceDiscountedProduct" runat="server"
					TargetControlID="txtDiscountedProduct"
					ServicePath="ZNodeCatalogServices.asmx"
					ServiceMethod="GetCompletionListWithContextAndValues"
					UseContextKey="true"
					MinimumPrefixLength="2"
					EnableCaching="false"
					CompletionSetCount="100"
					CompletionInterval="1"
					DelimiterCharacters=";, :"
					BehaviorID="autoCompleteBehavior4"
					OnClientShowing="AutoComplete_DiscountedProductShowing"
					OnClientItemSelected="AutoComplete_DiscountedProductSelected"
				/> 
				</span> 
			</asp:Panel>

			<asp:Panel ID="pnlDiscountedProductQuantity" runat="server">
				<div class="FieldStyle">
					<asp:Localize ID="lblProductDiscount" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleProductQuantity %>" /><br />
					<small><asp:Localize ID="lblHintProductDiscount" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintProductQuantity %>" /></small>
				</div>
				<div class="ValueStyle">
					<asp:DropDownList ID="ddlDiscountedProductQuantity" runat="server" Width="50" />
				</div>
			</asp:Panel>
		</ContentTemplate>
	</asp:UpdatePanel>

	<asp:Panel ID="pnlMinimumOrderAmount" runat="server">
		<div class="FieldStyle">
			<asp:Localize ID="lblMinOrderAmount" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleMinimumOrderAmount %>" /> <span class="Asterix">*</span><br />
			<small><asp:Localize ID="lblHintMinOrderAmount" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintMinimumOrderAmount %>" /></small>
		</div>
		<div class="ValueStyle">
			<%= ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox ID="txtMinimumOrderAmount" runat="server" MaxLength="7" Columns="25" Width="170px">0</asp:TextBox>
			<asp:RequiredFieldValidator ID="rfvMinimumOrderAmount" runat="server" ControlToValidate="txtMinimumOrderAmount" CssClass="Error" Display="Dynamic" ErrorMessage=" <%$ Resources:ZnodeAdminResource, Required %>" SetFocusOnError="True">
            </asp:RequiredFieldValidator>
			<asp:RegularExpressionValidator ID="regMinimumOrderAmount" runat="server" ControlToValidate="txtMinimumOrderAmount" CssClass="Error" Display="Dynamic" ErrorMessage=" <%$ Resources:ZnodeAdminResource, RegularValidAmount %>" SetFocusOnError="true" ValidationExpression="(^N/A$)|(^[-]?(\d+)(\.\d{0,3})?$)|(^[-]?(\d{1,3},(\d{3},)*\d{3}(\.\d{1,3})?|\d{1,3}(\.\d{1,3})?)$)">
            </asp:RegularExpressionValidator>
		</div>
	</asp:Panel>
		
	<asp:Panel ID="pnlCallForPriceProduct" runat="server">
		<asp:TextBox ID="txtCallForPriceProductId" runat="server" CssClass="HiddenFieldStyle" Text="0" />

		<div class="FieldStyle">
		    <asp:Localize ID="lblProduct" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleProduct %>" />
            <br />
			<small>
			    <asp:Localize ID="lblhintProduct" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintProduct %>" />
                </small>
		</div>
		<div class="ValueStyle">
			<span class="LeftFloat">
				<asp:TextBox ID="txtCallForPriceProduct" runat="server" AutoPostBack="true" />
			</span>
			<span>
				<asp:Image ID="btnCallForPriceProductShowPopup" runat="server" AlternateText="<%$ Resources:ZnodeAdminResource, TextSearch %>" CssClass="SearchIcon" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/enlarge.gif" ToolTip="Search Product" />
			</span>
		</div>

		<ajaxToolKit:AutoCompleteExtender ID="aceCallForPriceProduct" runat="server"
			TargetControlID="txtCallForPriceProduct"
			ServicePath="ZNodeCatalogServices.asmx"
			ServiceMethod="GetCompletionListWithContextAndValues"
			UseContextKey="true"
			MinimumPrefixLength="2"
			EnableCaching="false"
			CompletionSetCount="100"
			CompletionInterval="1"
			DelimiterCharacters=";, :"
			BehaviorID="autoCompleteBehavior3"
			OnClientPopulating="AutoComplete_RequiredProductShowing"
			OnClientItemSelected="AutoComplete_RequiredProductSelected"
		/>

		<div>
			<asp:RequiredFieldValidator ID="rfvCallForPriceProduct" runat="server" ControlToValidate="txtCallForPriceProduct" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, Required %>" />
		</div>
	</asp:Panel>
		
	<asp:Panel ID="pnlCallForPriceSku" runat="server">
		<div class="FieldStyle">
		    <asp:Localize ID="lblProductsku" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleProductSKU %>" />
            <br />
			<small>
			    <asp:Localize ID="lblHintProductSku" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintProductSKU %>" />
                </small>
		</div>
		<div class="ValueStyle">
			<asp:DropDownList runat="server" ID="ddlCallForPriceSkus" />
		</div>
	</asp:Panel>
		
	<asp:Panel ID="pnlCallForPriceMessage" runat="server">
		<div class="FieldStyle">
            <asp:Localize ID="lblCallForPrice" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumntitleCallForPriceMessage %>">
            </asp:Localize><br />
			<small>
                <asp:Localize ID="lblHintCallforMessage" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintCallForPriceMessage %>">
                </asp:Localize></small>
		</div>
		<div class="ValueStyle">
			<asp:TextBox ID="txtCallForPriceMessage" runat="server" Columns="25" />
		</div>
        
        <zn:Button ID="btnCallForPriceSku" runat="server" OnClick="btnCallForPriceSku_Click" CausesValidation="False" Style="display: none;" />
	</asp:Panel>

	<asp:Panel ID="pnlCoupon" runat="server">
		<h4 class="SubTitle">
            <asp:Localize ID="lblSubTitleCouponInformation" runat="server" Text="<%$ Resources:ZnodeAdminResource, SubTitleCouponInformation %>">
            </asp:Localize></h4>

		<div class="FieldStyle">
			<asp:CheckBox ID="chkCouponInd" onclick="SetupCouponSection()" runat="server" Text="Requires Coupon" />
		</div>

		<div class="ClearBoth">
			<br />
		</div>

		<asp:Panel ID="pnlCouponInfo" runat="server" Style="display: none;">
			<div class="FieldStyle">
                <asp:Localize ID="lblCouponCode" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleCouponCode %>">
                </asp:Localize> <span class="Asterix">*</span>
			</div>
			<div class="ValueStyle">
				<asp:TextBox ID="txtCouponCode" onkeyup="UpdateCouponUrl()" runat="server" Columns="25" />
				<asp:RegularExpressionValidator ID="regCouponCode" runat="server" ControlToValidate="txtCouponCode" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, RegularValidCouponCode%>" SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_]+)" Enabled="false" />
				<asp:RequiredFieldValidator ID="rfvCouponCode" runat="server" ControlToValidate="txtCouponCode" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, Required %>" SetFocusOnError="True" />
			</div>

			<div class="FieldStyle">
			    <asp:Localize ID="lblPromotionMessage" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitlePromotionMessage %>" />
                <br />
				<small>
                    <asp:Localize ID="lblHintPromotionMessage" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintPromotionMessage %>">
                    </asp:Localize></small>
			</div>
			<div class="ValueStyle">
				<asp:TextBox ID="txtPromotionMessage" runat="server" Columns="25" />
			</div>

			<div class="FieldStyle">
                <asp:Localize ID="lblAvailableQty" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleAvailableQuantity %>">
                </asp:Localize> <span class="Asterix">*</span>
			</div>
			<div class="ValueStyle">
				<asp:TextBox ID="txtCouponQuantityAvailable" runat="server" MaxLength="4" Columns="25" Width="173px">99</asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvCouponQuantityAvailable" runat="server" ControlToValidate="txtCouponQuantityAvailable" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, Required %>" SetFocusOnError="True" />
				<asp:RangeValidator ID="rgvCouponQuantityAvailable" runat="server" ControlToValidate="txtCouponQuantityAvailable" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, RegularValidQuantity %>" MinimumValue="0" MaximumValue="9999" Type="Integer" />
			</div>

			<div class="FieldStyle"></div>
			<div class="ValueStyleText">
				<asp:CheckBox ID="chkCouponUrl" runat="server" onclick="ToggleCouponUrl()" Text="<%$ Resources:ZnodeAdminResource, SelectEnableCouponURL %>" />
			</div>

			<asp:Panel ID="pnlCouponUrl" runat="server">
				<div class="FieldStyle"></div>
				<div class="ValueStyle">
					<asp:Label ID="lblCouponName" runat="server" />
				</div>
			</asp:Panel>
		</asp:Panel>
	</asp:Panel>

	<div class="ClearBoth">
		<br />
	</div>

	<div>
		  <zn:Button ID="btnSubmitBottom" runat="server" ButtonType="SubmitButton" OnClientClick="SetupCouponSection()" CausesValidation="True" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
        <zn:Button ID="btnCancelBottom" runat="server" ButtonType="CancelButton" OnClientClick="SetupCouponSection()" CausesValidation="False" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel %>" />

	</div>
</div>
