﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using Znode.Engine.Common;

namespace Znode.Engine.Admin.Controls.Default.Providers.Promotions
{
	public partial class PromotionList : ProviderBase
	{
		private static bool _searchEnabled;

		public string ListPageUrl { get; set; }
		public string AddPageUrl { get; set; }
		public string DeletePageUrl { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				_searchEnabled = false;
				BindDiscountTypeList();
				BindGridData();
			}
		}

		protected void btnAddPromotion_Click(object sender, EventArgs e)
		{
			Response.Redirect(AddPageUrl);
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			_searchEnabled = true;
			ViewState.Remove("PromotionsList");
			SearchPromotions();
		}

		protected void btnClear_Click(object sender, EventArgs e)
		{
			Response.Redirect(ListPageUrl);
		}

		protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			uxGrid.PageIndex = e.NewPageIndex;

			if (_searchEnabled)
			{
				SearchPromotions();
			}
			else
			{
				BindGridData();
			}
		}

		protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName != "Page")
			{
				var index = Convert.ToInt32(e.CommandArgument);

				var selectedRow = uxGrid.Rows[index];
				var tableCell = selectedRow.Cells[0];
				var id = tableCell.Text;

				if (e.CommandName == "Edit")
				{
					var redirectUrl = AddPageUrl + "?itemid=" + GetEncryptedId(id);
					Response.Redirect(redirectUrl);
				}
				else if (e.CommandName == "Delete")
				{
					var redirectUrl = DeletePageUrl + "?itemid=" + GetEncryptedId(id);
					Response.Redirect(redirectUrl);
				}
			}
		}

		protected string DiscountTypeName(object discountTypeId)
		{
			if (discountTypeId != null)
			{
				var typeId = int.Parse(discountTypeId.ToString());
				var promoAdmin = new PromotionAdmin();
				var promoType = promoAdmin.GetByDiscountTypeID(typeId);

				if (promoType != null)
				{
					return promoType.Name;
				}
			}

			return String.Empty;
		}

		protected string GetPortalName(object portalId)
		{
			if (portalId != null)
			{
				if (!String.IsNullOrEmpty(portalId.ToString()))
				{
					var portalAdmin = new PortalAdmin();
					return portalAdmin.GetStoreNameByPortalID(portalId.ToString());
				}
			}

			return  this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAllStores").ToString();
		}

		protected string GetProfileName(object profileId)
		{
			if (profileId != null)
			{
				if (!String.IsNullOrEmpty(profileId.ToString()))
				{
					var profileAdmin = new ProfileAdmin();
					return profileAdmin.GetProfileNameByProfileID(int.Parse(profileId.ToString()));
				}
			}

            return this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAllProfiles").ToString();
		}

		private void BindGridData()
		{
			var promotions = new PromotionAdmin();
			var promolist = promotions.GetAllPromotions();

			if (IsFranchiseAdmin)
			{
				uxGrid.DataSource = UserStoreAccess.CheckProfileAccess(promolist);
			}
			else
			{
				var ds = promolist.ToDataSet(false);
				uxGrid.DataSource = UserStoreAccess.CheckProfileAccess(ds.Tables[0], true).DefaultView;
			}
			
			uxGrid.DataBind();
		}

		private void BindDiscountTypeList()
		{
			var discountTypes = GetDiscountTypes();
			discountTypes.Sort("Name");

			ddlDiscountTypes.DataSource = discountTypes;
			ddlDiscountTypes.DataTextField = "Name";
			ddlDiscountTypes.DataValueField = "DiscountTypeID";
			ddlDiscountTypes.DataBind();

			// Add "All" to the top of the list
            ddlDiscountTypes.Items.Insert(0, new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), "0"));
			ddlDiscountTypes.SelectedIndex = 0;

			DecodeListItems(ddlDiscountTypes);
		}

		private void SearchPromotions()
		{
			var dv = new DataView();

			if (ViewState["PromotionsList"] != null)
			{
				dv = new DataView(ViewState["PromotionsList"] as DataTable);
			}
			else
			{
				var promoAdmin = new PromotionAdmin();
				var ds = promoAdmin.GetPromotions();
				var filterQuery = String.Empty;

				dv = IsFranchiseAdmin ? new DataView(UserStoreAccess.CheckProfileAccess(ds.Tables[0])) : new DataView(ds.Tables[0]);
				dv.Sort = "DisplayOrder ASC";

				
				if (txtStartDate.Text.Trim().Length > 0 && txtEndDate.Text.Trim().Length == 0)
				{
					filterQuery = "StartDate >= '" + txtStartDate.Text.Trim() + " 00:00" + "' AND ";
				}
				else if (txtEndDate.Text.Trim().Length > 0 && txtStartDate.Text.Trim().Length == 0)
				{
					filterQuery = "EndDate <= '" + txtEndDate.Text.Trim() + " 23:59" + "' AND ";
				}
				else if (txtStartDate.Text.Trim().Length > 0 && txtEndDate.Text.Trim().Length > 0)
				{
					filterQuery = "StartDate >= '" + txtStartDate.Text.Trim() + " 00:00" + "' AND EndDate <= '" + txtEndDate.Text.Trim() + " 23:59" + "' AND ";
				}

				if (ddlDiscountTypes.SelectedValue != "0")
				{
					filterQuery += "DiscountTypeId = " + ddlDiscountTypes.SelectedValue + " AND ";
				} 

				if (txtName.Text.Trim().Length > 0)
				{
					filterQuery += "Name LIKE '%" + Server.HtmlEncode(txtName.Text.Trim()) + "%' AND ";
				}				 

				if (txtAmount.Text.Trim().Length > 0)
				{
					filterQuery += "Discount >= " + txtAmount.Text.Trim() + " AND ";
				}

				if (txtCouponCode.Text.Trim().Length > 0)
				{
					filterQuery += "CouponCode LIKE '%" + txtCouponCode.Text.Trim() + "%' AND ";
				}

				// Check to apply filter
				if (filterQuery.Length > 0)
				{
					dv.RowFilter = filterQuery + "Description LIKE '%'";
				}

				ViewState.Add("PromotionsList", dv.ToTable());

				uxGrid.DataSource = dv;
				uxGrid.DataBind();
			}
		}
	}
}