﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PromotionList.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Promotions.PromotionList" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<div>
    <div class="LeftFloat" style="width: 70%; text-align: left">
        <h1>
            <asp:Literal ID="lblDeletePromoTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitlePromotionandCoupons %>'></asp:Literal>
        </h1>

    </div>

    <div class="ButtonStyle">
        <zn:LinkButton ID="btnAddPromotion" runat="server" ButtonType="Button" OnClick="btnAddPromotion_Click" Text='<%$ Resources:ZnodeAdminResource, LinkAddNewPromotion %>' ButtonPriority="Primary" />
    </div>

    <div class="ClearBoth" align="left">
        <p>
            <asp:Literal ID="PromotionandCoupons" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPromotionandCoupons %>'></asp:Literal>
        </p>
    </div>

    <h4 class="SubTitle">
        <asp:Literal ID="SearchPromotion" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchPromotion %>'></asp:Literal></h4>

    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
        <div class="SearchForm">
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">
                        <asp:Literal ID="TitleName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>'></asp:Literal></span>
                    <br />
                    <span class="ValueStyle">
                        <asp:TextBox ID="txtName" runat="server" />
                    </span>
                </div>

                <div class="ItemStyle">
                    <span class="FieldStyle">
                        <asp:Literal ID="Amount" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleAmount %>'></asp:Literal>
                    </span>
                    <br />
                    <span class="ValueStyle">
                        <asp:TextBox ID="txtAmount" runat="server" MaxLength="7" /><br />
                        <asp:RegularExpressionValidator ID="reqtxtAmount" runat="server" ControlToValidate="txtAmount" Display="Dynamic" CssClass="Error" ErrorMessage='<%$ Resources:ZnodeAdminResource, ErrorNumericmessage %>' ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidNumericExpression%>' />
                    </span>
                </div>
            </div>

            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">
                        <asp:Literal ID="TitleCouponCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCouponCode %>'></asp:Literal></span>
                    <br />
                    <span class="ValueStyle">
                        <asp:TextBox ID="txtCouponCode" runat="server" />
                    </span>
                </div>

                <div class="ItemStyle">
                    <span class="FieldStyle">
                        <asp:Literal ID="ColumnTitleDiscountType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDiscountType %>'></asp:Literal></span>
                    <br />
                    <span class="ValueStyle">
                        <asp:DropDownList ID="ddlDiscountTypes" runat="server" />
                    </span>
                </div>
            </div>

            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">
                        <asp:Literal ID="TitleBeginDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBeginDate %>'></asp:Literal></span>
                    <br />
                    <span class="ValueStyle">
                        <asp:TextBox ID="txtStartDate" runat="server" />
                        <asp:ImageButton ID="imgBtnStartDate" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" />
                        <ajaxToolKit:CalendarExtender ID="calStartDate" runat="server" TargetControlID="txtStartDate" Enabled="true" PopupButtonID="imgBtnStartDate" />
                        <asp:RegularExpressionValidator ID="regExStartDate" runat="server" ControlToValidate="txtStartDate" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidStartDate%>' ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidReportDate %>' />
                    </span>
                </div>

                <div class="ItemStyle">
                    <span class="FieldStyle">
                        <asp:Literal ID="Literal2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEndDate %>'></asp:Literal></span>
                    <br />
                    <span class="ValueStyle">
                        <asp:TextBox ID="txtEndDate" runat="server" />
                        <asp:ImageButton ID="imgBtnEndDate" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" />
                        <ajaxToolKit:CalendarExtender ID="calEndDate" runat="server" TargetControlID="txtEndDate" Enabled="true" PopupButtonID="imgBtnEndDate" />
                        <asp:RegularExpressionValidator ID="regExEndDate" runat="server" ControlToValidate="txtEndDate" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidEndDate%>' ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidReportDate %>' />
                        <asp:CompareValidator ID="cmpPromoEndDate" runat="server" ControlToCompare="txtStartDate" ControlToValidate="txtEndDate" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareStartDate%>' Operator="GreaterThanEqual" Type="Date" />
                    </span>
                </div>
            </div>

            <div class="ClearBoth" style="margin-top: 60px;">
                <zn:Button ID="btnSearch" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="btnSearch_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSearch %>" />

                <zn:Button ID="btnClearSearch" runat="server" ButtonType="CancelButton" CausesValidation="false" OnClick="btnClear_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonClear %>" />

            </div>
        </div>
    </asp:Panel>

    <h4 class="SubTitle">
        <asp:Literal ID="Literal1" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitlePromotionList %>'></asp:Literal></h4>

    <asp:GridView ID="uxGrid" runat="server"
        CssClass="Grid"
        AllowSorting="True"
        AllowPaging="True"
        PageSize="25"
        AutoGenerateColumns="False"
        Width="100%"
        CellPadding="4"
        GridLines="None"
        CaptionAlign="Left"
        OnPageIndexChanging="UxGrid_PageIndexChanging"
        OnRowCommand="UxGrid_RowCommand"
        EmptyDataText='<%$ Resources:ZnodeAdminResource,GridEmptyTextPromotionList %>'>
        <Columns>
            <asp:BoundField DataField="PromotionID" HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField HeaderText="Name" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <a href='Add.aspx?itemid=<%# GetEncryptedId(DataBinder.Eval(Container.DataItem, "PromotionID"))%>'>
                        <%# DataBinder.Eval(Container.DataItem, "Name").ToString()%>
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleStore %>' HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <div>
                        <%# GetPortalName((DataBinder.Eval(Container.DataItem, "PortalID")))%>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleProfile %>' HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <div>
                        <%# GetProfileName((DataBinder.Eval(Container.DataItem, "ProfileID")))%>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleAmount %>' HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <div align="left">
                        <%# (DataBinder.Eval(Container.DataItem, "Discount"))%>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleDiscountTypeName %>' HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <%# DiscountTypeName(DataBinder.Eval(Container.DataItem, "DiscountTypeID"))%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <%# DataBinder.Eval(Container.DataItem, "DisplayOrder")%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="StartDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleStartDate%>' HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="EndDate" DataFormatString="{0:MM/dd/yyyy}" HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleEndDate %>' HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="CouponCode" HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleCouponCode %>' HeaderStyle-HorizontalAlign="Left" />
            <asp:ButtonField CommandName="Edit" Text='<%$ Resources:ZnodeAdminResource,LinkEdit %>' ButtonType="Link">
                <ControlStyle CssClass="actionlink" Width="35" />
            </asp:ButtonField>
            <asp:ButtonField CommandName="Delete" Text='<%$ Resources:ZnodeAdminResource,LinkDelete %>' ButtonType="Link">
                <ControlStyle CssClass="actionlink" Width="55" />
            </asp:ButtonField>
        </Columns>
        <FooterStyle CssClass="FooterStyle" />
        <RowStyle CssClass="RowStyle" />
        <PagerStyle CssClass="PagerStyle" />
        <HeaderStyle CssClass="HeaderStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
    </asp:GridView>

    <div>
        <zn:Spacer ID="Spacer1" runat="server" SpacerHeight="10" SpacerWidth="10" />
    </div>
</div>
