﻿using System;
using System.Web;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace Znode.Engine.Admin.Controls.Default.Providers.Promotions
{
	public partial class PromotionDelete : ProviderBase
	{
		private int _itemId;

		public string PromotionName { get; set; }
		public string RedirectUrl { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request.Params["itemid"] != null)
			{
				_itemId = IsFranchiseAdmin ? Convert.ToInt32(Encryption.DecryptData(Request.Params["itemid"])) : int.Parse(Request.Params["itemid"]);
			}
			else
			{
				_itemId = 0;
			}

			if (!Page.IsPostBack)
			{
				BindData();
			}
		}

		protected void BtnDelete_Click(object sender, EventArgs e)
		{
			var promoAdmin = new PromotionAdmin();
			var promotion = promoAdmin.GetByPromotionId(_itemId);
			var deleted = promoAdmin.DeletePromotion(_itemId);

			if (deleted)
			{
				// Replace the promotion cache with new active promotions
				HttpContext.Current.Application["PromotionCache"] = promoAdmin.GetActivePromotions();
				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null,  this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeletePromotion")+ promotion.Name, promotion.Name);
				Response.Redirect(RedirectUrl);
			}
			else
			{
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPromotionDeletion").ToString();
				lblErrorMsg.Visible = true;
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect(RedirectUrl);
		}

		private void BindData()
		{
			var promoAdmin = new PromotionAdmin();
			var promotion = promoAdmin.GetByPromotionId(_itemId);

			if (promotion != null)
			{
				PromotionName = promotion.Name;

				if (IsFranchiseAdmin && promotion.AccountIDSource != null)
				{
					var portalId = promotion.AccountIDSource.ProfileID.GetValueOrDefault(0);
					UserStoreAccess.CheckStoreAccess(portalId, true);					
				}
			}
		}
	}
}