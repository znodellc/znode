﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PromotionDelete.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Promotions.PromotionDelete" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<h1>
    <asp:Label ID="lblDeletePromo" runat="server">
        <asp:Literal ID="lblDeletePromoTitle" runat="server" Text="<%$ Resources:ZnodeAdminResource, TitleDeletePromotion %>"></asp:Literal>
        - <%=PromotionName%></asp:Label>
</h1>

<div>
    <zn:Spacer ID="Spacer1" runat="server" SpacerHeight="10" SpacerWidth="3" />
</div>

<p>
    <asp:Localize runat="server" ID="DeleteConfirmation" Text="<%$ Resources:ZnodeAdminResource, TextDeleteConfirmPromotion %>"></asp:Localize>
</p>

<p>
    <asp:Label ID="lblErrorMsg" runat="server" Visible="False" CssClass="Error" /></p>

<div>
    <zn:Spacer ID="Spacer2" runat="server" SpacerHeight="10" SpacerWidth="3" />
</div>

<div>

    <zn:Button ID="btnDelete" runat="server" ButtonType="CancelButton" CausesValidation="True" OnClick="BtnDelete_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonDelete %>" />

    <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" CausesValidation="false" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel %>" />

</div>

<div>
    <zn:Spacer ID="Spacer3" runat="server" SpacerHeight="15" SpacerWidth="3" />
</div>
