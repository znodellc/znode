﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSearch.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Promotions.ProductSearch" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<div class="Form">
    <asp:UpdatePanel ID="updSearchPanel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <h1>
                <asp:Localize ID="TitleSearchProducts" runat="server" Text="<%$ Resources:ZnodeAdminResource, TitleSearchProducts %>" /></h1>

            <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
                <div>
                    <zn:Spacer ID="Spacer1" runat="server" SpacerWidth="5" SpacerHeight="10" />
                </div>

                <div class="FormView">
                    <div class="FieldStyle">
                        <asp:Localize ID="lblKeyword" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleKeyword %>" />
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtKeyword" runat="server" />
                    </div>

                    <% if (!IsFranchiseAdmin)
        { %>
                    <div class="FieldStyle">
                        <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleBrand %>" />
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="ddlManufacturer" runat="server" />
                    </div>
                    <% } %>

                    <div class="FieldStyle">
                        <asp:Localize ID="lblProductCategory" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextProductCategory %>" />
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="ddlProductCategory" runat="server" />
                    </div>

                    <div class="ClearBoth"></div>

                    <div class="ValueStyle">
                        <zn:Button ID="btnSearch" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSearch_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSearch %>" />
                        <zn:Button ID="btnClear" runat="server" ButtonType="CancelButton" CausesValidation="False" OnClick="BtnClear_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonClear %>" />
                    </div>
                </div>

                <div class="ClearBoth"></div>
            </asp:Panel>

            <h4>
                <asp:Localize ID="lblProductList" runat="server" Text="<%$ Resources:ZnodeAdminResource, GridTitleProductList %>" /></h4>

            <div class="HintStyle">
                <asp:Localize ID="lblHintText" runat="server" Text="<%$ Resources:ZnodeAdminResource, SubTextProductList%>" />
            </div>

            <div>
                <zn:Spacer ID="Spacer2" runat="server" SpacerWidth="5" SpacerHeight="5" />
            </div>

            <asp:GridView ID="uxGrid" runat="server"
                AllowPaging="True"
                AutoGenerateColumns="False"
                CellPadding="0"
                CssClass="Grid"
                EmptyDataText="<%$ Resources:ZnodeAdminResource, RecordNotFoundProducts %>"
                GridLines="None"
                OnPageIndexChanging="UxGrid_PageIndexChanging"
                Width="100%">
                <Columns>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleName %>">
                        <ItemTemplate>
                            <a href="javascript:selectProduct('<%# Eval("ProductID") %>','<%# GetName(Eval("Name")) %>')">
                                <%# DataBinder.Eval(Container.DataItem,"Name") %>
                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ProductNum" HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleProductsNumber %>" />
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:ZnodeAdminResource, GridColumnTitleImage %>">
                        <ItemTemplate>
                            <img id="img1" runat="server" src='<%# (new ZNode.Libraries.ECommerce.Utilities.ZNodeImage()).GetImageHttpPathThumbnail(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>' alt="" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleActive %>">
                        <ItemTemplate>
                            <img id="imgActive" runat="server" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>' alt="" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EditRowStyle CssClass="EditRowStyle" />
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <SelectedRowStyle CssClass="SelectedRowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>

    <div>
        <zn:Spacer ID="Spacer3" runat="server" SpacerWidth="5" SpacerHeight="10" />
    </div>

    <div align="right">
        <zn:Button ID="btnClose" runat="server" ButtonType="CancelButton" OnClientClick="javascript:window.close();" CausesValidation="False" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel %>" />

    </div>
</div>
