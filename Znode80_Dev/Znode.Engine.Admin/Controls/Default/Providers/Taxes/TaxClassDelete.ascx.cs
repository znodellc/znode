﻿using System;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace Znode.Engine.Admin.Controls.Default.Providers.Taxes
{
	public partial class TaxClassDelete : ProviderBase
	{
		private int _taxClassId;

		public string TaxClassName { get; set; }
		public string RedirectUrl { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request.Params["taxclassid"] != null)
			{
				_taxClassId = IsFranchiseAdmin ? Convert.ToInt32(GetDecryptedId(Request.Params["taxclassid"])) : int.Parse(Request.Params["taxclassid"]);
			}
			else
			{
				_taxClassId = 0;
			}

			BindData();

		    lblDeleteTaxClass.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleDeleteTaxClass").ToString() + TaxClassName;
		}

		protected void BtnDelete_Click(object sender, EventArgs e)
		{
			try
			{
				var taxRuleAdmin = new TaxRuleAdmin();
				var deleted = taxRuleAdmin.DeleteTaxClass(_taxClassId);

				if (deleted)
				{
                    ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteTaxClass").ToString(),TaxClassName), TaxClassName);
					Response.Redirect(RedirectUrl);
				}
				else
				{
					lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteTaxClassInUse").ToString();
					lblErrorMsg.Visible = true;
				}
			}
			catch
			{
				lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteTaxClass").ToString();
				lblErrorMsg.Visible = true;
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect(RedirectUrl);
		}

		private void BindData()
		{
			var taxRuleAdmin = new TaxRuleAdmin();
			var taxClass = taxRuleAdmin.GetByTaxClassID(_taxClassId);

			if (taxClass != null)
			{
				TaxClassName = taxClass.Name;
			}
		}
	}
}