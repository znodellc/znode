﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaxRuleAdd.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Taxes.TaxRuleAdd" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<div class="FormView">
    <div class="LeftFloat" style="width: 70%; text-align: left">
        <h1 class="LeftFloat">
            <asp:Label ID="lblTitle" runat="server" />
        </h1>
        <div class="tooltip ToolTipLeftAlign">
            <a href="javascript:void(0);" class="learn-more">
                <span>
                    <asp:Localize ID="Localize" runat="server" /></span>
            </a>
            <div class="content">
                <h6><asp:Localize runat="server" ID="Help" Text='<%$ Resources:ZnodeAdminResource, HintHelp %>' /></h6>
                <p><asp:Localize runat="server" ID="TaxRuleHintSteps" Text='<%$ Resources:ZnodeAdminResource, TaxRuleHintSteps %>' /></p>
                <p>
                    <asp:Localize runat="server" ID="TaxRuleHintSteps1" Text='<%$ Resources:ZnodeAdminResource, TaxRuleHintSteps1 %>' /><br />
                    <asp:Localize runat="server" ID="TaxRuleHintSteps2" Text='<%$ Resources:ZnodeAdminResource, TaxRuleHintSteps2 %>' /><br />
                </p>
            </div>
        </div>
    </div>

	<div style="text-align: right">
	    <zn:Button runat="server" ID="btnSubmitTop" OnClick="BtnSubmit_Click" CausesValidation="True" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
        <zn:Button runat="server" ID="btnCancelTop" OnClick="BtnCancel_Click" CausesValidation="False" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
	</div>

	<div class="ClearBoth">
		<asp:Label ID="lblMsg" runat="server" CssClass="Error" />
	</div>

	<div>
		<zn:Spacer ID="Spacer1" runat="server" SpacerHeight="5" SpacerWidth="3" />
	</div>

	<h4 class="SubTitle"><asp:Localize runat="server" ID="General" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneral %>'></asp:Localize></h4>

	<div class="FieldStyle">
		<asp:Localize runat="server" ID="TaxType" Text='<%$ Resources:ZnodeAdminResource, ColumnTaxType %>'></asp:Localize>
	</div>
	<div class="ValueStyle">
		<asp:DropDownList ID="ddlTaxTypes" runat="server" OnSelectedIndexChanged="ddlTaxTypes_SelectedIndexChanged" OnDataBound="ddlTaxTypes_DataBound" AutoPostBack="true" />
	</div>

	<asp:Panel runat="server" ID="pnlTaxRegion">
		<h4 class="SubTitle"><asp:Localize runat="server" ID="TaxRegion" Text='<%$ Resources:ZnodeAdminResource, SubTitleTaxRegion %>'></asp:Localize></h4>

		<div>
			<p><asp:Localize runat="server" ID="TaxRegionSubText" Text='<%$ Resources:ZnodeAdminResource, SubTextTaxRegion %>'></asp:Localize></p>
			<zn:Spacer ID="Spacer2" runat="server" SpacerHeight="20" SpacerWidth="3" />
		</div>

		<asp:UpdatePanel ID="UpdatePanelTaxRegion" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true" RenderMode="Inline">
			<ContentTemplate>
				<div class="FieldStyle">
					<asp:Localize runat="server" ID="DestinationCountry" Text='<%$ Resources:ZnodeAdminResource, ColumnDestinationCountry %>'></asp:Localize>
				</div>
				<div class="ValueStyle">
					<asp:DropDownList ID="ddlCountries" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountries_SelectedIndexChanged" />
				</div>

				<div class="FieldStyle">
					<asp:Localize runat="server" ID="DestinationState" Text='<%$ Resources:ZnodeAdminResource, ColumnDestinationState %>'></asp:Localize>
				</div>
				<div class="ValueStyle">
					<asp:DropDownList ID="ddlStates" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlStates_SelectedIndexChanged" Width="160px">
						<asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextAllState %>' Value="0" />
					</asp:DropDownList>
				</div>

				<div class="FieldStyle">
					<asp:Localize runat="server" ID="DestinationCounty" Text='<%$ Resources:ZnodeAdminResource, ColumnDestinationCounty %>'></asp:Localize>
				</div>
				<div class="ValueStyle">
					<asp:DropDownList ID="ddlCounties" runat="server" Width="160px">
						<asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextAllCounty %>' Value="0" />
					</asp:DropDownList>
				</div>
			</ContentTemplate>
		</asp:UpdatePanel>

		<h4 class="SubTitle"><asp:Localize runat="server" ID="TexRate" Text='<%$ Resources:ZnodeAdminResource, SubTitleTaxRate %>'></asp:Localize></h4>
		
		<p><asp:Localize runat="server" ID="TaxRateSubText" Text='<%$ Resources:ZnodeAdminResource, SubTextTaxRate %>'></asp:Localize></p>
		<zn:Spacer ID="Spacer3" runat="server" SpacerHeight="20" SpacerWidth="3" />
		
		<asp:Panel ID="pnlSalesTax" runat="server">
			<div class="FieldStyle">
				<asp:Localize runat="server" ID="SalesTax" Text='<%$ Resources:ZnodeAdminResource, ColumnSalesTax %>'></asp:Localize>
			</div>
			<div class="ValueStyle">
				<asp:TextBox ID="txtSalesTax" runat="server" Columns="6" MaxLength="5">0.00</asp:TextBox>%&nbsp;&nbsp;
				<asp:RequiredFieldValidator ID="rfvSalesTax" runat="server" ControlToValidate="txtSalesTax" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, Required %>' CssClass="Error" SetFocusOnError="True" />
				<asp:RangeValidator ID="rgvSalesTax" runat="server" ControlToValidate="txtSalesTax" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeTax %>' CssClass="Error" MinimumValue="0" MaximumValue="100" Type="Double" SetFocusOnError="True" />
			</div>
		</asp:Panel>
		
		<asp:Panel ID="pnlVAT" runat="server">
			<div class="FieldStyle">
				<asp:Localize runat="server" ID="VATTax" Text='<%$ Resources:ZnodeAdminResource, ColumnVAT %>'></asp:Localize>
			</div>
			<div class="ValueStyle">
				<asp:TextBox ID="txtVAT" runat="server" Columns="6" MaxLength="5">0.00</asp:TextBox>%&nbsp;&nbsp;
				<asp:RequiredFieldValidator ID="rfvVAT" runat="server" ControlToValidate="txtVAT" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, Required %>' CssClass="Error" SetFocusOnError="True" />
				<asp:RangeValidator ID="rgvVAT" runat="server" ControlToValidate="txtVAT" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeTax %>' CssClass="Error" MinimumValue="0" MaximumValue="100" Type="Double" SetFocusOnError="True" />
			</div>
		</asp:Panel>
		
		<asp:Panel ID="pnlGST" runat="server">
			<div class="FieldStyle">
				<asp:Localize runat="server" ID="GSTTax" Text='<%$ Resources:ZnodeAdminResource, ColumnGST %>'></asp:Localize>
			</div>
			<div class="ValueStyle">
				<asp:TextBox ID="txtGST" runat="server" Columns="6" MaxLength="5">0.00</asp:TextBox>%&nbsp;&nbsp;
				<asp:RequiredFieldValidator ID="rfvGST" runat="server" ControlToValidate="txtGST" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, Required %>' CssClass="Error" SetFocusOnError="True" />
				<asp:RangeValidator ID="rgvGST" runat="server" ControlToValidate="txtGST" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeTax %>' CssClass="Error" MinimumValue="0" MaximumValue="100" Type="Double" SetFocusOnError="True" />
			</div>
		</asp:Panel>
		
		<asp:Panel ID="pnlPST" runat="server">
			<div class="FieldStyle">
				<asp:Localize runat="server" ID="PSTTax" Text='<%$ Resources:ZnodeAdminResource, ColumnPST %>'></asp:Localize>
			</div>
			<div class="ValueStyle">
				<asp:TextBox ID="txtPST" runat="server" Columns="6" MaxLength="5">0.00</asp:TextBox>%&nbsp;&nbsp;
				<asp:RequiredFieldValidator ID="rfvPST" runat="server" ControlToValidate="txtPST" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, Required %>' CssClass="Error" SetFocusOnError="True" />
				<asp:RangeValidator ID="rgvPST" runat="server" ControlToValidate="txtPST" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeTax %>' CssClass="Error" MinimumValue="0" MaximumValue="100" Type="Double" SetFocusOnError="True" />
			</div>
		</asp:Panel>
		
		<asp:Panel ID="pnlHST" runat="server">
			<div class="FieldStyle">
				<asp:Localize runat="server" ID="HSTTax" Text='<%$ Resources:ZnodeAdminResource, ColumnHST %>'></asp:Localize>
			</div>
			<div class="ValueStyle">
				<asp:TextBox ID="txtHST" runat="server" Columns="6" MaxLength="5">0.00</asp:TextBox>%&nbsp;&nbsp;
				<asp:RequiredFieldValidator ID="rfvHST" runat="server" ControlToValidate="txtHST" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, Required %>' CssClass="Error" SetFocusOnError="True" />
				<asp:RangeValidator ID="rgvHST" runat="server" ControlToValidate="txtHST" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeTax %>' CssClass="Error" MinimumValue="0" MaximumValue="100" Type="Double" SetFocusOnError="True" />
			</div>
		</asp:Panel>
		
		<asp:Panel ID="pnlPrecedence" runat="server">
			<div class="FieldStyle">
				<asp:Localize runat="server" ID="Precedence" Text='<%$ Resources:ZnodeAdminResource, ColumnPrecedence %>'></asp:Localize> <span class="Asterix">*</span><br />
				<small><asp:Localize runat="server" ID="PrecedenceSubText" Text='<%$ Resources:ZnodeAdminResource, ColumnTextPrecedence %>'></asp:Localize></small>
			</div>
			<div class="ValueStyle">
				<asp:TextBox ID="txtPrecedence" runat="server" MaxLength="9" Columns="5">1</asp:TextBox>
				<asp:RequiredFieldValidator ID="rfvPrecedence" runat="server" ControlToValidate="txtPrecedence" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, Required %>' CssClass="Error" SetFocusOnError="True" />
				<asp:RangeValidator ID="rgvPrecedence" runat="server" ControlToValidate="txtPrecedence" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeWholeNumber %>' CssClass="Error" MinimumValue="1" MaximumValue="999999999" Type="Integer" SetFocusOnError="True" />
			</div>
		</asp:Panel>
		
		<asp:Panel ID="pnlInclusive" runat="server">
			<div class="FieldStyle">
				<asp:Localize runat="server" ID="InclusiveTax" Text='<%$ Resources:ZnodeAdminResource, ColumnInclusiveTax %>'></asp:Localize><br />
				<small><asp:Localize runat="server" ID="InclusiveTaxSubText" Text='<%$ Resources:ZnodeAdminResource, ColumnTextInclusiveTax %>'></asp:Localize></small>
			</div>
			<div class="ValueStyle">
				<asp:CheckBox ID="chkInclusiveTax" runat="server" />
			</div>
		</asp:Panel>
	</asp:Panel>

	<div class="ClearBoth">
		<br />
	</div>

	<div>
	    <zn:Button runat="server" ID="btnSubmitBottom" OnClick="BtnSubmit_Click" CausesValidation="True" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
        <zn:Button runat="server" ID="btnCancelBottom" OnClick="BtnCancel_Click" CausesValidation="False" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
	</div>
</div>
