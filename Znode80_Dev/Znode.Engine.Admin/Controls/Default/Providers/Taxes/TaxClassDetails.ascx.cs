﻿using System;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using Znode.Engine.Common;

namespace Znode.Engine.Admin.Controls.Default.Providers.Taxes
{
	public partial class TaxClassDetails : ProviderBase
	{
		private int _taxClassId;
		private TaxRuleAdmin _taxRuleAdmin = new TaxRuleAdmin();

		public string DefaultPageUrl { get; set; }
		public string EditPageUrl { get; set; }
		public string AddRulePageUrl { get; set; }
		public string EditRulePageUrl { get; set; }
		public string DeleteRulePageUrl { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			_taxClassId = Request.Params["taxclassid"] != null ? int.Parse(GetDecryptedId(Request.Params["taxclassid"])) : 0;

			if (!Page.IsPostBack)
			{
				BindData();
				lblTitle.Text = lblTaxClassName.Text;
			}
		}

		protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			uxGrid.PageIndex = e.NewPageIndex;
			BindData();
		}

		protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
		{
			BindData();
		}

		protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName != "Page")
			{
				var taxRuleId = e.CommandArgument.ToString();

				if (e.CommandName == "Edit")
				{
					Response.Redirect(EditRulePageUrl + "?taxruleid=" + GetEncryptedId(taxRuleId) + "&taxclassid=" + GetEncryptedId(_taxClassId));
				}
				else if (e.CommandName == "Delete")
				{
					Response.Redirect(DeleteRulePageUrl + "?taxruleid=" + GetEncryptedId(taxRuleId) + "&taxclassid=" + GetEncryptedId(_taxClassId));
				}
			}
		}

		protected void BtnAddTaxRule_Click(object sender, EventArgs e)
		{
			Response.Redirect(AddRulePageUrl + "?taxclassid=" + GetEncryptedId(_taxClassId));
		}

		protected void BtnEdit_Click(object sender, EventArgs e)
		{
			Response.Redirect(EditPageUrl + "?taxclassid=" + GetEncryptedId(_taxClassId));
		}

		protected void BtnBackToList_Click(object sender, EventArgs e)
		{
			Response.Redirect(DefaultPageUrl);
		}

		protected string GetDefaultRegionCode(object regionCode)
		{
			if (regionCode == null || String.IsNullOrEmpty(regionCode.ToString()))
			{
			    return GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString().ToUpper();
			}

			// Otherwise
			return regionCode.ToString();
		}

		protected string GetCountyName(object regionCode)
		{
			string countyName;

			if (regionCode == null || String.IsNullOrEmpty(regionCode.ToString()))
			{
                countyName = GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString().ToUpper();
			}
			else
			{
				countyName = _taxRuleAdmin.GetNameByCountyFIPS(regionCode.ToString());
			}

			return countyName;
		}

		protected string GetInclusiveTaxYesNo(object inclusiveIndicator)
		{
			string result;

			if (inclusiveIndicator == null || String.IsNullOrEmpty(inclusiveIndicator.ToString()))
			{
				result = String.Empty;
			}
			else
			{
				result = Convert.ToBoolean(inclusiveIndicator.ToString()) ? this.GetGlobalResourceObject("ZnodeAdminResource","DropDownTextYes").ToString() : this.GetGlobalResourceObject("ZnodeAdminResource","DropDownTextNo").ToString();
			}

			return result;
		}

		private void BindData()
		{
			if (_taxClassId > 0)
			{
				var taxClass = _taxRuleAdmin.DeepLoadByTaxClassId(_taxClassId);

				if (IsFranchiseAdmin)
				{
					UserStoreAccess.CheckStoreAccess(taxClass.PortalID.GetValueOrDefault(0), true);	
				}

				taxClass.TaxRuleCollection.Sort("Precedence");
				lblTaxClassName.Text = Server.HtmlEncode(taxClass.Name);

				if (taxClass.DisplayOrder.HasValue)
				{
					lblDisplayOrder.Text = taxClass.DisplayOrder.ToString();
				}

				imgActive.Src = Helper.GetCheckMark(taxClass.ActiveInd);
				uxGrid.DataSource = taxClass.TaxRuleCollection;
				uxGrid.DataBind();
			}
		}
	}
}