﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaxClassList.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Taxes.TaxClassList" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<div class="LeftFloat" style="width: 70%; text-align: left">
    <h1 class="LeftFloat"><asp:Localize runat="server" ID="TaxClassListTitle" Text='<%$ Resources:ZnodeAdminResource, TitleTaxClassList %>'></asp:Localize></h1>
    <div class="tooltip TaxClass">
        <a href="javascript:void(0);" class="learn-more">
            <span>
                <asp:Localize ID="Localize" runat="server" /></span>
        </a>
        <div class="content">
            <h6><asp:Localize runat="server" ID="Help" Text='<%$ Resources:ZnodeAdminResource, HintHelp %>'></asp:Localize></h6>
            <p>
                <b><asp:Localize runat="server" ID="HintSteps" Text='<%$ Resources:ZnodeAdminResource, TaxClassHintSteps %>'></asp:Localize></b><br />
                <br />
                <asp:Localize runat="server" ID="HintHelp1" Text='<%$ Resources:ZnodeAdminResource, TaxClassHintSteps1 %>'></asp:Localize><br />
                <asp:Localize runat="server" ID="HintHelp2" Text='<%$ Resources:ZnodeAdminResource, TaxClassHintSteps2 %>'></asp:Localize><br />
                <asp:Localize runat="server" ID="HintHelp3" Text='<%$ Resources:ZnodeAdminResource, TaxClassHintSteps3 %>'></asp:Localize><br />
            </p>
        </div>
    </div>
</div>

<div class="ButtonStyle">
	<zn:LinkButton ID="btnAddTaxClass" runat="server" CausesValidation="False" ButtonType="Button" OnClick="BtnAddTaxClass_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddTaxClass %>' ButtonPriority="Primary" />
</div>

<div class="ClearBoth">
	<p><asp:Localize runat="server" ID="TaxClassListText" Text='<%$ Resources:ZnodeAdminResource, SubTextTaxClassList %>'></asp:Localize></p>
</div>

<div>
	<zn:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server" />
</div>

<div>
	<asp:Label ID="lblErrorMsg" EnableViewState="false" runat="server" CssClass="Error" />
</div>

<div>
	<zn:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server" />
</div>

<h4 class="GridTitle"><asp:Localize runat="server" ID="TaxClassListGridTitle" Text='<%$ Resources:ZnodeAdminResource, GridTitleTaxClassList %>'></asp:Localize></h4>

<asp:GridView ID="uxGrid" runat="server"
	CssClass="Grid"
	AllowSorting="True"
	AllowPaging="True"
	PageSize="25"
	AutoGenerateColumns="False"
	Width="100%"
	CellPadding="4"
	GridLines="None"
	CaptionAlign="Left"
	OnPageIndexChanging="UxGrid_PageIndexChanging"
	OnRowCommand="UxGrid_RowCommand"
	OnRowDeleting="UxGrid_RowDeleting"
	EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundTaxClass %>'>
	<Columns>
		<asp:BoundField DataField="TaxClassID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
		<asp:BoundField DataField="Name" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left" />
		<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStore %>' HeaderStyle-HorizontalAlign="Left">
			<ItemTemplate>
				<div>
					<%# GetStoreName((DataBinder.Eval(Container.DataItem, "PortalID")))%>
				</div>
			</ItemTemplate>
		</asp:TemplateField>
		<asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
		<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleEnabled %>' HeaderStyle-HorizontalAlign="Left">
			<ItemTemplate>
				<img runat="server" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>' alt="" />
			</ItemTemplate>
		</asp:TemplateField>
		<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleActions %>' HeaderStyle-HorizontalAlign="Left">
			<ItemTemplate>
				<div class="LeftFloat" style="width: 25%; text-align: left">
					<asp:LinkButton ID="btnView" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkView %>' CommandName="View" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TaxClassID")%>' />
				</div>
				<div class="LeftFloat" style="width: 25%; text-align: left">
					<asp:LinkButton ID="btnEdit" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' CommandName="Edit" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TaxClassID")%>' />
				</div>
				<div class="LeftFloat" style="width: 25%; text-align: left">
					<asp:LinkButton ID="btnDelete" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CommandName="Delete" CommandArgument='<%# DataBinder.Eval(Container.DataItem, "TaxClassID")%>' />
				</div>
			</ItemTemplate>
		</asp:TemplateField>
	</Columns>
	<FooterStyle CssClass="FooterStyle" />
	<RowStyle CssClass="RowStyle" />
	<PagerStyle CssClass="PagerStyle" Font-Underline="True" />
	<HeaderStyle CssClass="HeaderStyle" />
	<AlternatingRowStyle CssClass="AlternatingRowStyle" />
	<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
</asp:GridView>

<div>
	<zn:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="10" runat="server" />
</div>
