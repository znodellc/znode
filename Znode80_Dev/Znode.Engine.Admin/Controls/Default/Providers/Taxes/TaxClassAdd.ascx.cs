﻿using System;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace Znode.Engine.Admin.Controls.Default.Providers.Taxes
{
	public partial class TaxClassAdd : ProviderBase
	{
		private int _taxClassId;

		public string DefaultPageUrl { get; set; }
		public string ViewPageUrl { get; set; }
		public string AddRulePageUrl { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			_taxClassId = Request.Params["taxclassid"] != null ? int.Parse(GetDecryptedId(Request.Params["taxclassid"])) : 0;

			if (!Page.IsPostBack)
			{
				if (_taxClassId > 0)
				{
					lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource","TitleEditTaxClass").ToString();
					BindEditData();
				}
				else
				{
					lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource","TitleAddTaxClass").ToString();
				}
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			var taxRuleAdmin = new TaxRuleAdmin();
			var taxClass = _taxClassId > 0 ? taxRuleAdmin.GetByTaxClassID(_taxClassId) : new TaxClass();

			taxClass.Name = txtTaxClassName.Text;
			taxClass.DisplayOrder = int.Parse(txtDisplayOrder.Text);
			taxClass.ActiveInd = chkActiveInd.Checked;

			if (IsFranchiseAdmin)
			{
				taxClass.PortalID = UserStoreAccess.GetTurnkeyStorePortalID;
			}

			var success = false;

			if (_taxClassId > 0)
			{
				success = taxRuleAdmin.UpdateTaxClass(taxClass);
				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogEditTaxClass").ToString() + txtTaxClassName.Text, txtTaxClassName.Text);
			}
			else
			{
				success = taxRuleAdmin.InsertTaxClass(taxClass);
				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null,  this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogAddTaxClass").ToString() + txtTaxClassName.Text, txtTaxClassName.Text);
			}

			if (success)
			{
				if (_taxClassId > 0)
				{
					// Redirect to view page  
					Response.Redirect(ViewPageUrl + "?taxclassid=" + GetEncryptedId(taxClass.TaxClassID));
				}
				else
				{
					// Redirect to add rule page  
					Response.Redirect(AddRulePageUrl + "?taxclassid=" + GetEncryptedId(taxClass.TaxClassID));
				}
			}
			else
			{
				// Display error message
				lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorTaxClass").ToString();
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			if (_taxClassId > 0)
			{
				Response.Redirect(ViewPageUrl + "?taxclassid=" + GetEncryptedId(_taxClassId));
			}
			else
			{
				Response.Redirect(DefaultPageUrl);
			}
		}

		private void BindEditData()
		{
			if (_taxClassId > 0)
			{
				var taxRuleAdmin = new TaxRuleAdmin();
				var taxClass = taxRuleAdmin.GetByTaxClassID(_taxClassId);

				if (IsFranchiseAdmin)
				{
					UserStoreAccess.CheckStoreAccess(taxClass.PortalID.GetValueOrDefault(0), true);	
				}

				lblTitle.Text = lblTitle.Text + this.GetGlobalResourceObject("ZnodeAdminResource","Hyphen").ToString() + Server.HtmlEncode(taxClass.Name);
				txtTaxClassName.Text = Server.HtmlDecode(taxClass.Name);
				txtDisplayOrder.Text = taxClass.DisplayOrder.ToString();
				chkActiveInd.Checked = taxClass.ActiveInd;
			}
		}
	}
}