﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaxClassDetails.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Taxes.TaxClassDetails" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<div class="Form">
	<div class="LeftFloat" style="width: 65%; text-align: left">
		<h1><asp:Localize runat="server" ID="TaxClassDetailsTitle" Text='<%$ Resources:ZnodeAdminResource, TitleTaxClassDetails %>'></asp:Localize><asp:Label ID="lblTitle" runat="server" /></h1>
	</div>

	<div align="right">
	    <zn:Button runat="server" ID="btnEdit" Width="130px" ButtonType="EditButton" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonEditTaxClass %>' OnClick="BtnEdit_Click" />
        <zn:Button runat="server" ID="btnBackToList" Width="130px" ButtonType="EditButton" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonBackToList %>' OnClick="BtnBackToList_Click" />
	</div>

	<div class="ClearBoth">
		<br />
	</div>

	<h4 class="SubTitle"><asp:Localize runat="server" ID="GeneralInformation" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralInformation %>'></asp:Localize></h4>

	<div class="ViewForm200">
		<div class="FieldStyle">
		    <asp:Localize runat="server" ID="Name" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' />
		</div>
		<div class="ValueStyle">
			<asp:Label ID="lblTaxClassName" runat="server" />
		</div>

		<div class="FieldStyle">
			<asp:Localize runat="server" ID="DispalyOrder" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder %>' />
		</div>
		<div class="ValueStyle">
			<asp:Label ID="lblDisplayOrder" runat="server" />
		</div>

		<div class="FieldStyle">
			<asp:Localize runat="server" ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEnabled %>' />
		</div>
		<div class="ValueStyle">
			<img id="imgActive" runat="server" alt="" />
		</div>
	</div>

	<div class="ClearBoth"></div>

	<h4 class="SubTitle"><asp:Localize runat="server" ID="TaxRuleList" Text='<%$ Resources:ZnodeAdminResource, SubTitleTaxRuleList %>' /></h4>

	<p>
	    <asp:Localize runat="server" ID="SubTextTaxRuleList" Text='<%$ Resources:ZnodeAdminResource, SubTextTaxRuleList %>'></asp:Localize>
	</p>
	
	<ul style="font-size: 11pt; font-family: Georgia; padding-left: 20px;">
		<li><asp:Localize runat="server" ID="SubTextFirstTaxRule" Text='<%$ Resources:ZnodeAdminResource, SubTextFirstTaxRule %>'></asp:Localize></li>
		<li><asp:Localize runat="server" ID="SubTextSecondTaxRule" Text='<%$ Resources:ZnodeAdminResource, SubTextSecondTaxRule %>'></asp:Localize></li>
	</ul>

	<div>
		<asp:Label ID="lblMsg" runat="server" CssClass="Error" />
	</div>

	<div class="ButtonStyle">
		<zn:LinkButton ID="BtnAddTaxRule" runat="server" ButtonType="Button" OnClick="BtnAddTaxRule_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonAddTaxRule %>' ButtonPriority="Primary" />
	</div>

	<br />

	<asp:GridView ID="uxGrid" runat="server"
		CssClass="Grid"
		AllowSorting="True"
		AllowPaging="True"
		PageSize="25"
		AutoGenerateColumns="False"
		Width="100%"
		CellPadding="4"
		GridLines="None"
		CaptionAlign="Left"
		OnPageIndexChanging="UxGrid_PageIndexChanging"
		OnRowCommand="UxGrid_RowCommand"
		OnRowDeleting="UxGrid_RowDeleting"
		EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundTaxClassDetails %>'>
		<Columns>
			<asp:BoundField DataField="TaxRuleID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
			<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleTax %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<%# DataBinder.Eval(Container.DataItem, "SalesTax")%>%
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePrecedence %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<%# DataBinder.Eval(Container.DataItem, "Precedence")%>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleInclusiveTax %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<%# GetInclusiveTaxYesNo(DataBinder.Eval(Container.DataItem, "InclusiveInd"))%>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCountryCode %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<%# GetDefaultRegionCode(DataBinder.Eval(Container.DataItem, "DestinationCountryCode"))%>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStateCode %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<%# GetDefaultRegionCode(DataBinder.Eval(Container.DataItem, "DestinationStateCode"))%>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCounty %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<%# GetCountyName(DataBinder.Eval(Container.DataItem, "CountyFIPS"))%>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleActions %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<div class="LeftFloat" style="width: 40%; text-align: left">
						<asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("TaxRuleID") %>' CommandName="Edit" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' CssClass="LinkButton" />
					</div>
					<div class="LeftFloat" style="width: 50%; text-align: left">
						<asp:LinkButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("TaxRuleID") %>' CommandName="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CssClass="LinkButton" />
					</div>
				</ItemTemplate>
			</asp:TemplateField>
		</Columns>
		<FooterStyle CssClass="FooterStyle" />
		<RowStyle CssClass="RowStyle" />
		<PagerStyle CssClass="PagerStyle" />
		<HeaderStyle CssClass="HeaderStyle" />
		<AlternatingRowStyle CssClass="AlternatingRowStyle" />
		<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
	</asp:GridView>

	<div>
		<zn:Spacer ID="Spacer1" runat="server" SpacerHeight="10" SpacerWidth="10" />
	</div>
</div>
