﻿using System;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using Znode.Engine.Common;

namespace Znode.Engine.Admin.Controls.Default.Providers.Taxes
{
	public partial class TaxClassList : ProviderBase
	{
		public string ViewPageUrl { get; set; }
		public string EditPageUrl { get; set; }
		public string DeletePageUrl { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				BindGridData();
			}
		}

		protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			uxGrid.PageIndex = e.NewPageIndex;
			BindGridData();
		}

		protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName != "Page")
			{
				var id = e.CommandArgument.ToString();
				var queryParams = "?taxclassid=" + GetEncryptedId(id);

				if (e.CommandName == "View")
				{
					Response.Redirect(ViewPageUrl + queryParams);
				}
				else if (e.CommandName == "Edit")
				{
					Response.Redirect(EditPageUrl + queryParams);
				}
				else if (e.CommandName == "Delete")
				{
					Response.Redirect(DeletePageUrl + queryParams);
				}
			}
		}

		protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
		{
			BindGridData();
		}

		protected void BtnAddTaxClass_Click(object sender, EventArgs e)
		{
			Response.Redirect(EditPageUrl);
		}

		protected string GetStoreName(object portalId)
		{
			if (portalId != null)
			{
				if (!String.IsNullOrEmpty(portalId.ToString()))
				{
					var portalAdmin = new PortalAdmin();
					return portalAdmin.GetStoreNameByPortalID(portalId.ToString());
				}
			}

		    return GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAllStores").ToString();
		}

		private void BindGridData()
		{
			var taxRuleAdmin = new TaxRuleAdmin();
			uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(taxRuleAdmin.GetAllTaxClass());
			uxGrid.DataBind();
		}
	}
}