﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaxClassAdd.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Taxes.TaxClassAdd" %>

<div class="FormView">
    <div class="LeftFloat" style="width: 70%; text-align: left;">
        <h1>
            <asp:Label ID="lblTitle" runat="server" /></h1>
    </div>

    <div style="text-align: right">
        <zn:Button runat="server" OnClick="BtnSubmit_Click" CausesValidation="True" ID="btnSubmitTop" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
        <zn:Button runat="server" OnClick="BtnCancel_Click" CausesValidation="False" ID="btnCancelTop" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
    </div>

    <div class="Error">
        <asp:Label ID="lblMsg" runat="server" />
    </div>

    <div class="FieldStyle">
        <asp:Localize runat="server" ID="TaxClassName" Text='<%$ Resources:ZnodeAdminResource, ColumnTaxClassName %>'></asp:Localize>
        <span class="Asterix">*</span>
    </div>
    <div class="ValueStyle">
        <asp:TextBox ID="txtTaxClassName" runat="server" MaxLength="15" />
        <asp:RequiredFieldValidator ID="rfvTaxClassName" runat="server" ControlToValidate="txtTaxClassName" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredTaxClassName %>' CssClass="Error" SetFocusOnError="True" />
    </div>

    <div class="FieldStyle">
        <asp:Localize runat="server" ID="DisplayOrder" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder %>'></asp:Localize> <span class="Asterix">*</span><br />
        <small><asp:Localize runat="server" ID="TaxClassDisplayOrder" Text='<%$ Resources:ZnodeAdminResource, ColumnTextTaxClassDisplayOrder %>'></asp:Localize></small>
    </div>
    <div class="ValueStyle">
        <asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="9" Columns="5" />
        <asp:RequiredFieldValidator ID="rfvDisplayOrder" runat="server" ControlToValidate="txtDisplayOrder" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrder %>' CssClass="Error" />
        <asp:RangeValidator ID="rgvDisplayOrder" runat="server" ControlToValidate="txtDisplayOrder" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeDisplayOrder %>' CssClass="Error" MinimumValue="1" MaximumValue="999999999" Type="Integer" />
    </div>

    <div class="FieldStyle">
        <asp:Localize runat="server" ID="Enable" Text='<%$ Resources:ZnodeAdminResource, ColumnEnable %>'></asp:Localize>
    </div>
    <div class="ValueStyle">
        <asp:CheckBox ID="chkActiveInd" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableTaxClass %>' Checked="true" />
    </div>

    <div class="ClearBoth">
        <br />
    </div>

    <div>
        <zn:Button runat="server" OnClick="BtnSubmit_Click" CausesValidation="True" ID="btnSubmitBottom" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
        <zn:Button runat="server" OnClick="BtnCancel_Click" CausesValidation="False" ID="btnCancelBottom" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>'/>
    </div>
</div>
