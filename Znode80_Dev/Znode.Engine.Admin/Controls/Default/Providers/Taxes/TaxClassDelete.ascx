﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TaxClassDelete.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Taxes.TaxClassDelete" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<h1>
	<asp:Label ID="lblDeleteTaxClass" runat="server"></asp:Label>
</h1>

<div>
	<zn:Spacer ID="Spacer1" runat="server" SpacerHeight="10" SpacerWidth="3" />
</div>

<p><asp:Localize runat="server" ID="ConfirmDeleteTaxClass" Text='<%$ Resources:ZnodeAdminResource, TextDeleteConfirmTaxClass %>'></asp:Localize></p>

<p><asp:Label ID="lblErrorMsg" runat="server" Visible="False" CssClass="Error" /></p>

<div>
	<zn:Spacer ID="Spacer2" runat="server" SpacerHeight="10" SpacerWidth="3" />
</div>

<div>
    <zn:Button runat="server" ID="btnDelete" OnClick="BtnDelete_Click" CausesValidation="True" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>'/>
    <zn:Button runat="server" ID="btnCancel" OnClick="BtnCancel_Click" CausesValidation="False" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>'/>
</div>

<div>
	<zn:Spacer ID="Spacer3" runat="server" SpacerHeight="15" SpacerWidth="3" />
</div>