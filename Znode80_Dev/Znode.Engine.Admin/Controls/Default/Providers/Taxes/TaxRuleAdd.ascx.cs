﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Common;
using Znode.Engine.Taxes;

namespace Znode.Engine.Admin.Controls.Default.Providers.Taxes
{
	public partial class TaxRuleAdd : ProviderBase
	{
		private int _taxRuleId;
		private int _taxClassId;
		private List<IZnodeTaxType> _availableTaxTypes;

		public string ViewPageUrl { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			_availableTaxTypes = ZnodeTaxManager.GetAvailableTaxTypes();

			_taxRuleId = Request.Params["taxruleid"] != null ? int.Parse(GetDecryptedId(Request.Params["taxruleid"])) : 0;
			_taxClassId = Request.Params["taxclassid"] != null ? int.Parse(GetDecryptedId(Request.Params["taxclassid"])) : 0;

			if (!Page.IsPostBack)
			{
				BindTaxTypes();
				BindCountries();

				ToggleDisplay();

				if (_taxRuleId > 0)
				{
                    lblTitle.Text = GetGlobalResourceObject("ZnodeAdminResource", "TitleEditTaxRule").ToString();
					BindEditData();
				}
				else
				{
                    lblTitle.Text = GetGlobalResourceObject("ZnodeAdminResource", "TitleAddTaxRule").ToString();
				}
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			var taxRuleAdmin = new TaxRuleAdmin();
			var taxRule = _taxRuleId > 0 ? taxRuleAdmin.GetTaxRule(_taxRuleId) : new TaxRule();
			taxRule.TaxClassID = _taxClassId;

			// Portal ID
			taxRule.PortalID = IsFranchiseAdmin ? UserStoreAccess.GetTurnkeyStorePortalID.GetValueOrDefault(0) : ZNodeConfigManager.SiteConfig.PortalID;

			// Tax type
			if (ddlTaxTypes.SelectedIndex != -1)
			{
				taxRule.TaxRuleTypeID = taxRuleAdmin.GetTaxRuleTypeId(ddlTaxTypes.SelectedValue);
			}

			// Destination country, state, and county
			taxRule.DestinationCountryCode = ddlCountries.SelectedValue != "*" ? ddlCountries.SelectedValue : null;
			taxRule.DestinationStateCode = ddlStates.SelectedValue != "0" ? ddlStates.SelectedValue : null;
			taxRule.CountyFIPS = ddlCounties.SelectedValue != "0" ? ddlCounties.SelectedValue : null;

			// Tax rates
			if (pnlSalesTax.Visible) taxRule.SalesTax = Convert.ToDecimal(txtSalesTax.Text);
			if (pnlVAT.Visible) taxRule.VAT = Convert.ToDecimal(txtVAT.Text);
			if (pnlGST.Visible) taxRule.GST = Convert.ToDecimal(txtGST.Text);
			if (pnlPST.Visible) taxRule.PST = Convert.ToDecimal(txtPST.Text);
			if (pnlHST.Visible) taxRule.HST = Convert.ToDecimal(txtHST.Text);

			// Tax preferences        
			if (pnlPrecedence.Visible) taxRule.Precedence = int.Parse(txtPrecedence.Text);
			if (pnlInclusive.Visible) taxRule.InclusiveInd = chkInclusiveTax.Checked;

			var success = false;
			var taxClass = taxRuleAdmin.GetByTaxClassID(_taxClassId);

			if (_taxRuleId > 0)
			{
				success = taxRuleAdmin.UpdateTaxRule(taxRule);
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditTaxRule").ToString() + taxClass.Name, taxClass.Name);
			}
			else
			{
				success = taxRuleAdmin.AddTaxRule(taxRule);
                ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddTaxRule").ToString() + taxClass.Name, taxClass.Name);
			}

			if (success)
			{
				// Remove inclusive tax rules from cache and redirect
				HttpContext.Current.Cache.Remove("InclusiveTaxRules");
				Response.Redirect(ViewPageUrl + "?taxclassid=" + GetEncryptedId(_taxClassId));
			}
			else
			{
				// Display error message
                lblMsg.Text = GetGlobalResourceObject("ZnodeAdminResource", "ErrorTaxRule").ToString();
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect(ViewPageUrl + "?taxclassid=" + GetEncryptedId(_taxClassId));
		}

		protected void ddlTaxTypes_SelectedIndexChanged(object sender, EventArgs e)
		{
			ToggleDisplay();
		}

		protected void ddlTaxTypes_DataBound(object sender, EventArgs e)
		{
			foreach (ListItem item in ((DropDownList)sender).Items)
			{
				item.Text = Server.HtmlDecode(item.Text);
			}
		}

		protected void ddlCountries_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (ddlCountries.SelectedValue.Equals("*"))
			{
				ddlStates.Items.Clear();
                ddlStates.Items.Insert(0, new ListItem(GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAllState").ToString(), "0"));
			}
			else
			{
				BindStates();
			}
		}

		protected void ddlStates_SelectedIndexChanged(object sender, EventArgs e)
		{
			BindCounties();
		}

		private void BindEditData()
		{
			if (_taxRuleId > 0)
			{
				var taxRuleAdmin = new TaxRuleAdmin();
				var taxRule = taxRuleAdmin.GetTaxRule(_taxRuleId);

				if (taxRule != null)
				{
					if (IsFranchiseAdmin)
					{
						UserStoreAccess.CheckStoreAccess(taxRule.PortalID, true);
					}

					// Tax type
					var taxRuleTypeService = new TaxRuleTypeService();
					if (taxRule.TaxRuleTypeID.HasValue)
					{
						var taxRuleType = taxRuleTypeService.DeepLoadByTaxRuleTypeID(taxRule.TaxRuleTypeID.Value, true, DeepLoadType.Ignore, null);
						ddlTaxTypes.SelectedValue = taxRuleType.ClassName;
					}
					else
					{
						ddlTaxTypes.SelectedIndex = 0;
					}

					ToggleDisplay();

					// Destination country
					if (taxRule.DestinationCountryCode != null)
					{
						var listItem = ddlCountries.Items.FindByValue(taxRule.DestinationCountryCode);
						if (listItem != null)
						{
							ddlCountries.SelectedIndex = ddlCountries.Items.IndexOf(listItem);
						}

						if (ddlCountries.SelectedIndex != -1)
						{
							BindStates();
						}
					}

					// Destination state
					if (taxRule.DestinationStateCode != null)
					{
						var listItem = ddlStates.Items.FindByValue(taxRule.DestinationStateCode);
						if (listItem != null)
						{
							ddlStates.SelectedIndex = ddlStates.Items.IndexOf(listItem);
						}

						if (ddlStates.SelectedIndex != -1)
						{
							BindCounties();
						}
					}

					// County FIPS
					if (taxRule.CountyFIPS != null)
					{
						var listItem = ddlCounties.Items.FindByValue(taxRule.CountyFIPS);
						if (listItem != null)
						{
							ddlCounties.SelectedIndex = ddlCounties.Items.IndexOf(listItem);
						}
					}

					// Tax rates
					if (pnlSalesTax.Visible) txtSalesTax.Text = taxRule.SalesTax.ToString();
					if (pnlVAT.Visible) txtVAT.Text = taxRule.VAT.ToString();
					if (pnlGST.Visible) txtGST.Text = taxRule.GST.ToString();
					if (pnlPST.Visible) txtPST.Text = taxRule.PST.ToString();
					if (pnlHST.Visible) txtHST.Text = taxRule.HST.ToString();

					// Tax preferences
					if (pnlPrecedence.Visible) txtPrecedence.Text = taxRule.Precedence.ToString();
					if (pnlInclusive.Visible) chkInclusiveTax.Checked = taxRule.InclusiveInd;
				}
			}
		}

		private void BindTaxTypes()
		{
			var taxRuleAdmin = new TaxRuleAdmin();
			var taxRuleTypes = taxRuleAdmin.GetTaxRuleTypes();

			ddlTaxTypes.DataSource = taxRuleTypes.FindAll(TaxRuleTypeColumn.ActiveInd, true);
			ddlTaxTypes.DataTextField = Server.HtmlDecode("Name");
			ddlTaxTypes.DataValueField = "ClassName";
			ddlTaxTypes.DataBind();
		}

		private void BindCountries()
		{
			var shippingAdmin = new ShippingAdmin();
			var countries = shippingAdmin.GetDestinationCountries();

			ddlCountries.DataSource = countries;
			ddlCountries.DataTextField = "Name";
			ddlCountries.DataValueField = "Code";
			ddlCountries.DataBind();
		}

		private void BindStates()
		{
			var stateService = new StateService();
			var filters = new StateQuery();

			// Parameters
			filters.Append(StateColumn.CountryCode, ddlCountries.SelectedItem.Value);

			// Get states
			var states = stateService.Find(filters.GetParameters());

			ddlStates.DataSource = states;
			ddlStates.DataTextField = "Name";
			ddlStates.DataValueField = "Code";
			ddlStates.DataBind();

			// Add to top of list
            ddlStates.Items.Insert(0, new ListItem(GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAllState").ToString(), "0"));
		}

		private void BindCounties()
		{
			var taxRuleAdmin = new TaxRuleAdmin();
			var counties = taxRuleAdmin.GetCountyCodeByStateAbbr(ddlStates.SelectedValue);

			ddlCounties.DataSource = counties;
			ddlCounties.DataTextField = "CountyName";
			ddlCounties.DataValueField = "CountyFIPS";
			ddlCounties.DataBind();

			// Add to top of list
			ddlCounties.Items.Insert(0, new ListItem(GetGlobalResourceObject("ZnodeAdminResource","DropDownTextAllCounty").ToString(), "0"));
		}

		private void ToggleDisplay()
		{
			// First hide everything
			pnlSalesTax.Visible = false;
			pnlVAT.Visible = false;
			pnlGST.Visible = false;
			pnlPST.Visible = false;
			pnlHST.Visible = false;
			pnlPrecedence.Visible = false;
			pnlInclusive.Visible = false;

			// Then loop through to show only controls that are needed
			foreach (var tax in _availableTaxTypes)
			{
				if (tax.ClassName == ddlTaxTypes.SelectedValue)
				{
					foreach (var control in tax.Controls)
					{
						ShowControl(control);
					}
				}
			}
		}

		private void ShowControl(ZnodeTaxRuleControl control)
		{
			if (control == ZnodeTaxRuleControl.SalesTax) pnlSalesTax.Visible = true;
			if (control == ZnodeTaxRuleControl.VAT) pnlVAT.Visible = true;
			if (control == ZnodeTaxRuleControl.GST) pnlGST.Visible = true;
			if (control == ZnodeTaxRuleControl.PST) pnlPST.Visible = true;
			if (control == ZnodeTaxRuleControl.HST) pnlHST.Visible = true;
			if (control == ZnodeTaxRuleControl.Precedence) pnlPrecedence.Visible = true;
			if (control == ZnodeTaxRuleControl.Inclusive) pnlInclusive.Visible = true;
		}
	}
}