﻿using System;
using System.Web;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace Znode.Engine.Admin.Controls.Default.Providers.Taxes
{
	public partial class TaxRuleDelete : ProviderBase
	{
		private int _taxRuleId;
		private int _taxClassId;
		private TaxRuleAdmin _taxRuleAdmin = new TaxRuleAdmin();

		public string TaxRuleName { get; set; }
		public string RedirectUrl { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request.Params["taxruleid"] != null)
			{
				_taxRuleId = IsFranchiseAdmin ? Convert.ToInt32(GetDecryptedId(Request.Params["taxruleid"])) : int.Parse(Request.Params["taxruleid"]);
			}
			else
			{
				_taxRuleId = 0;
			}

			if (Request.Params["taxclassid"] != null)
			{
				_taxClassId = IsFranchiseAdmin ? Convert.ToInt32(GetDecryptedId(Request.Params["taxclassid"])) : int.Parse(Request.Params["taxclassid"]);
			}
			else
			{
				_taxClassId = 0;
			}

			BindData();

            lblDeleteTaxRule.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleDeleteTaxRule").ToString() + TaxRuleName;
		}

		protected void BtnDelete_Click(object sender, EventArgs e)
		{
			var taxRule = _taxRuleAdmin.GetTaxRule(_taxRuleId);
			if (taxRule != null)
			{
				var taxClass = _taxRuleAdmin.GetByTaxClassID(_taxClassId);
				var taxClassService = new TaxClassService();
				taxClassService.DeepLoad(taxClass);

				if ((taxClass.AddOnValueCollection.Count > 0 || taxClass.ProductCollection.Count > 0) && taxClass.TaxRuleCollection.Count == 1)
				{
					lblErrorMsg.Visible = true;
				    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorTaxRuleAssociated").ToString();
					return;
				}

				taxRule.TaxRuleID = _taxRuleId;

				// Get tax class name and tax rule name
				var taxRuleService = new TaxRuleService();
				var taxRule2 = taxRuleService.GetByTaxRuleID(_taxRuleId);
				taxRuleService.DeepLoad(taxRule2);

				var deleted = _taxRuleAdmin.DeleteTaxRule(taxRule);
				if (deleted)
				{
				    var source = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteTaxRule").ToString(), taxRule2.TaxRuleTypeIDSource.Name, taxClass.Name);
					ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, source, taxClass.Name);
					HttpContext.Current.Cache.Remove("InclusiveTaxRules");
					Response.Redirect(RedirectUrl + "?taxclassid=" + GetEncryptedId(_taxClassId));
				}
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect(RedirectUrl + "?taxclassid=" + GetEncryptedId(_taxClassId));
		}

		private void BindData()
		{
			var taxRuleService = new TaxRuleService();
			var taxRule = taxRuleService.GetByTaxRuleID(_taxRuleId);
			taxRuleService.DeepLoad(taxRule);

			if (taxRule != null)
			{
				TaxRuleName = taxRule.TaxRuleTypeIDSource.Name;
			}
		}
	}
}