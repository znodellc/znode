﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using Znode.Engine.Suppliers;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Admin.Controls.Default.Providers.Suppliers
{
	public partial class SupplierAdd : ProviderBase
	{
		private int _itemId;
		private List<IZnodeSupplierType> _availableSupplierTypes;

		protected void Page_Load(object sender, EventArgs e)
		{
			_availableSupplierTypes = ZnodeSupplierManager.GetAvailableSupplierTypes();

			if (Request.Params["itemid"] != null)
			{
				_itemId = int.Parse(Request.Params["itemid"]);
			}

			if (!Page.IsPostBack)
			{
				BindSupplierTypes();
				ToggleDisplay();

				if (_itemId > 0)
				{
					BindEditData();
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditSupplier").ToString();
				}
				else
				{
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddSupplier").ToString();
				}
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			var supplierAdmin = new SupplierAdmin();
			var supplier = _itemId > 0 ? supplierAdmin.GetBySupplierID(_itemId) : new Supplier();

			supplier.Name = txtName.Text.Trim();
			supplier.ExternalSupplierNo = txtCode.Text.Trim();
			supplier.Description = txtDescription.Text.Trim();
			supplier.ContactFirstName = txtContactFirstName.Text.Trim();
			supplier.ContactLastName = txtContactLastName.Text.Trim();
			supplier.ContactPhone = txtContactPhone.Text;
			supplier.ContactEmail = txtContactEmail.Text;
			supplier.EnableEmailNotification = chkEnableEmailNotification.Checked;

			if (pnlNotificationEmail.Visible)
			{
				supplier.NotificationEmailID = txtNotificationEmail.Text;	
			}

			if (pnlEmailNotificationTemplate.Visible)
			{
				supplier.EmailNotificationTemplate = txtEmailNotificationTemplate.Text;	
			}

			if (pnlDisplayOrder.Visible)
			{
				supplier.DisplayOrder = int.Parse(txtDisplayOrder.Text.Trim());	
			}

			supplier.ActiveInd = chkEnabled.Checked;
			supplier.Custom1 = txtCustom1.Text.Trim();
			supplier.Custom2 = txtCustom2.Text.Trim();
			supplier.Custom3 = txtCustom3.Text.Trim();
			supplier.Custom4 = txtCustom4.Text.Trim();
			supplier.Custom5 = txtCustom5.Text.Trim();

			if (ddlSupplierType.SelectedIndex != -1)
			{
				supplier.SupplierTypeID = supplierAdmin.GetSupplierTypeId(ddlSupplierType.SelectedValue);
			}

			var success = false;

			if (_itemId > 0)
			{
				success = supplierAdmin.Update(supplier);
				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditSupplier") + txtName.Text, txtName.Text);
			}
			else
			{
				success = supplierAdmin.Insert(supplier);
				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogCreateSupplier") + txtName.Text, txtName.Text);
			}

			if (success)
			{
				System.Web.HttpContext.Current.Session["SupplierList"] = null;
				Response.Redirect("Default.aspx");
			}
			else
			{
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSavingSupplier").ToString();
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect("Default.aspx");
		}

		protected void ddlSupplierType_SelectedIndexChanged(object sender, EventArgs e)
		{
			ToggleDisplay();
		}

		protected void ddlSupplierType_DataBound(object sender, EventArgs e)
		{
			foreach (ListItem item in ((DropDownList)sender).Items)
			{
				item.Text = Server.HtmlDecode(item.Text);
			}
		}

		private void BindEditData()
		{
			var supplierAdmin = new SupplierAdmin();
			var supplier = supplierAdmin.DeepLoadBySupplierId(_itemId);

			if (supplier != null)
			{
				txtName.Text = supplier.Name;
				txtCode.Text = supplier.ExternalSupplierNo;
				txtDescription.Text = supplier.Description;
				txtContactFirstName.Text = supplier.ContactFirstName;
				txtContactLastName.Text = supplier.ContactLastName;
				txtContactPhone.Text = supplier.ContactPhone;
				txtContactEmail.Text = supplier.ContactEmail;
				ddlSupplierType.SelectedValue = supplier.SupplierTypeIDSource.ClassName;
				txtNotificationEmail.Text = supplier.NotificationEmailID;
				txtEmailNotificationTemplate.Text = supplier.EmailNotificationTemplate;
				chkEnableEmailNotification.Checked = supplier.EnableEmailNotification;
				txtDisplayOrder.Text = supplier.DisplayOrder.ToString();
				chkEnabled.Checked = supplier.ActiveInd;
				txtCustom1.Text = supplier.Custom1;
				txtCustom2.Text = supplier.Custom2;
				txtCustom3.Text = supplier.Custom3;
				txtCustom4.Text = supplier.Custom4;
				txtCustom5.Text = supplier.Custom5;

				ToggleDisplay();
			}
		}

		private void BindSupplierTypes()
		{
			var supplierAdmin = new SupplierAdmin();
			var supplierTypes = supplierAdmin.GetSupplierTypes();
			ddlSupplierType.DataSource = supplierTypes.FindAll(SupplierTypeColumn.ActiveInd, true);
			ddlSupplierType.DataTextField = "Name";
			ddlSupplierType.DataValueField = "ClassName";
			ddlSupplierType.DataBind();
		}

		private void ToggleDisplay()
		{
			// First hide everything
			pnlNotificationEmail.Visible = false;
			pnlEmailNotificationTemplate.Visible = false;
			pnlDisplayOrder.Visible = false;

			// Then loop through to show only controls that are needed
			foreach (var supplier in _availableSupplierTypes)
			{
				if (supplier.ClassName == ddlSupplierType.SelectedValue)
				{
					foreach (var control in supplier.Controls)
					{
						ShowControl(control);
					}
				}
			}
		}

		private void ShowControl(ZnodeSupplierControl control)
		{
			if (control == ZnodeSupplierControl.NotificationEmail) pnlNotificationEmail.Visible = true;
			if (control == ZnodeSupplierControl.EmailNotificationTemplate) pnlEmailNotificationTemplate.Visible = true;
			if (control == ZnodeSupplierControl.DisplayOrder) pnlDisplayOrder.Visible = true;
		}
	}
}