﻿using System;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Admin.Controls.Default.Providers.Suppliers
{
	public partial class SupplierDelete : ProviderBase
	{
		private int _itemId;

		public string SupplierName { get; set; }
		public string RedirectUrl { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (Request.Params["itemid"] != null)
			{
				_itemId = int.Parse(Request.Params["itemid"]);
			}
			else
			{
				_itemId = 0;
			}

			if (!Page.IsPostBack)
			{
				BindData();
			}
		}

		protected void BtnDelete_Click(object sender, EventArgs e)
		{
			var supplierAdmin = new SupplierAdmin();
			var supplier = supplierAdmin.GetBySupplierID(_itemId);
			var deleted = supplierAdmin.Delete(_itemId);

			if (deleted)
			{
				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogSupplierDelete") + supplier.Name, supplier.Name);
				Response.Redirect(RedirectUrl);
			}
			else
			{
			    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSupplierDelete").ToString();
				lblErrorMsg.Visible = true;
                if (supplier != null)
                {
                    SupplierName = supplier.Name;
                }
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect(RedirectUrl);
		}

		private void BindData()
		{
			var supplierAdmin = new SupplierAdmin();
			var supplier = supplierAdmin.GetBySupplierID(_itemId);

			if (supplier != null)
			{
				SupplierName = supplier.Name;
			}
		}
	}
}