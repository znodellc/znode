﻿using System;
using System.Data;	
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;

namespace Znode.Engine.Admin.Controls.Default.Providers.Suppliers
{
	public partial class SupplierList : ProviderBase
	{
		private static bool _searchEnabled;

		public string ListPageUrl { get; set; }
		public string AddPageUrl { get; set; }
		public string DeletePageUrl { get; set; }

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				_searchEnabled = false;
				BindGridData();
			}
		}

		protected void btnAddSupplier_Click(object sender, EventArgs e)
		{
			Response.Redirect(AddPageUrl);
		}

		protected void btnSearch_Click(object sender, EventArgs e)
		{
			_searchEnabled = true;
			SearchSuppliers();
		}

		protected void btnClear_Click(object sender, EventArgs e)
		{
			Response.Redirect(ListPageUrl);
		}

		protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			uxGrid.PageIndex = e.NewPageIndex;

			if (_searchEnabled)
			{
				SearchSuppliers();
			}
			else
			{
				BindGridData();
			}
		}

		protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName != "Page")
			{
				var index = Convert.ToInt32(e.CommandArgument);

				var selectedRow = uxGrid.Rows[index];
				var tableCell = selectedRow.Cells[0];
				var id = tableCell.Text;

				if (e.CommandName == "Edit")
				{
					var redirectUrl = AddPageUrl + "?itemid=" + id;
					Response.Redirect(redirectUrl);
				}
				else if (e.CommandName == "Delete")
				{
					var redirectUrl = DeletePageUrl + "?itemid=" + id;
					Response.Redirect(redirectUrl);
				}
			}
		}

		protected string SupplierTypeName(object supplierTypeId)
		{
			if (supplierTypeId != null)
			{
				var typeId = int.Parse(supplierTypeId.ToString());
				var supplierAdmin = new SupplierAdmin();
				var supplierType = supplierAdmin.GetBySupplierTypeID(typeId);

				if (supplierType != null)
				{
					return supplierType.Name;
				}
			}

			return String.Empty;
		}

		private void BindGridData()
		{
			var supplierAdmin = new SupplierAdmin();
			var suppliers = supplierAdmin.GetAll();

			if (suppliers != null)
			{
				suppliers.Sort("Name Asc");

				if (suppliers.Count > 0)
				{
					foreach (var supplier in suppliers)
					{
						supplier.Name = Server.HtmlEncode(supplier.Name);
					}
				}
			}

			uxGrid.DataSource = suppliers;
			uxGrid.DataBind();
		}

		private void SearchSuppliers()
		{
			var supplierAdmin = new SupplierAdmin();
			var ds = supplierAdmin.SearchSupplier(txtName.Text, ddlSupplierStatus.SelectedValue);
			var dv = new DataView(ds.Tables[0]) { Sort = "DisplayOrder Asc" };

			uxGrid.DataSource = dv;
			uxGrid.DataBind();
		}
	}
}