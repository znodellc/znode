﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SupplierList.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Suppliers.SupplierList" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<div>
	<div class="LeftFloat" style="width: 70%; text-align: left">
		<h1>
            <asp:Localize runat="server" ID="TitleSuppliers" Text='<%$ Resources:ZnodeAdminResource, LinkTextSuppliers %>'></asp:Localize>
		</h1>
	</div>

	<div class="ButtonStyle">
		<zn:LinkButton ID="btnAddSupplier" runat="server" CausesValidation="False" ButtonType="Button" OnClick="btnAddSupplier_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddSupplier %>' ButtonPriority="Primary" />
	</div>
	
	<div class="ClearBoth" align="left">
		<p>
            <asp:Localize runat="server" ID="TextManageVendors" Text='<%$ Resources:ZnodeAdminResource, TextSuppliers %>'></asp:Localize>
		</p>
	</div>

	<h4 class="SubTitle">
        <asp:Localize runat="server" ID="SubTitleSearchSuppliers" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchSuppliers %>'></asp:Localize>
	</h4>
	
	<asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
		<div class="SearchForm">
			<div class="RowStyle">
				<div class="ItemStyle">
					<span class="FieldStyle">
                        <asp:Localize runat="server" ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>'></asp:Localize>
					</span>
					<br />
					<span class="ValueStyle">
						<asp:TextBox ID="txtName" runat="server" />
					</span>
				</div>

				<div class="ItemStyle">
					<span class="FieldStyle">
                        <asp:Localize runat="server" ID="Localize2" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus %>'></asp:Localize>
					</span>
					<br />
					<span class="ValueStyle">
						<asp:DropDownList ID="ddlSupplierStatus" runat="server">
							<asp:ListItem Selected="True" Text='<%$ Resources:ZnodeAdminResource, DropdownTextAll %>' Value="" />
							<asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropdownTextActive %>' Value="1" />
							<asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropdownTextInactive %>' Value="0" />
						</asp:DropDownList>
					</span>
				</div>
			</div>

			<div class="ClearBoth" style="padding: 10px 0px 20px 0px;">
			    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="btnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' ID="btnSearch" />
                <zn:Button runat="server" ButtonType="CancelButton" OnClick="btnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" ID="btnClearSearch" />
			</div>
		</div>
	</asp:Panel>

	<h4 class="GridTitle" style="padding: 20px 0px 10px 0px;">
        <asp:Localize runat="server" ID="Localize3" Text='<%$ Resources:ZnodeAdminResource, GridTitleSupplierList %>'></asp:Localize>
	</h4>
	
	<asp:GridView ID="uxGrid" runat="server"
		CssClass="Grid"
		AllowSorting="True"
		AllowPaging="True"
		PageSize="25"
		AutoGenerateColumns="False"
		Width="100%"
		CellPadding="4"
		GridLines="None"
		CaptionAlign="Left"
		OnPageIndexChanging="UxGrid_PageIndexChanging"
		OnRowCommand="UxGrid_RowCommand"
		EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptySupplierList %>'>
		<Columns>
			<asp:BoundField DataField="SupplierID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
			<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<a href='Add.aspx?itemid=<%# DataBinder.Eval(Container.DataItem, "SupplierID").ToString()%>'>
						<%# DataBinder.Eval(Container.DataItem, "Name").ToString()%>
					</a>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSupplierType %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<%# SupplierTypeName(DataBinder.Eval(Container.DataItem, "SupplierTypeID"))%>
				</ItemTemplate>
			</asp:TemplateField>
			<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsActive %>' HeaderStyle-HorizontalAlign="Left">
				<ItemTemplate>
					<img runat="server" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>' alt="" />
				</ItemTemplate>
			</asp:TemplateField>
			<asp:ButtonField CommandName="Edit" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' ButtonType="Link">
				<ControlStyle CssClass="actionlink" />
			</asp:ButtonField>
			<asp:ButtonField CommandName="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' ButtonType="Link">
				<ControlStyle CssClass="actionlink" />
			</asp:ButtonField>
		</Columns>
		<FooterStyle CssClass="FooterStyle" />
		<RowStyle CssClass="RowStyle" />
		<PagerStyle CssClass="PagerStyle" Font-Underline="True" />
		<HeaderStyle CssClass="HeaderStyle" />
		<AlternatingRowStyle CssClass="AlternatingRowStyle" />
		<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
	</asp:GridView>
	
	<div>
		<zn:Spacer id="Spacer1" runat="server" spacerheight="10" spacerwidth="10" />
	</div>
</div>
