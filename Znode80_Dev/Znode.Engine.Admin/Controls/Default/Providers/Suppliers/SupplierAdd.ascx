﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SupplierAdd.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Suppliers.SupplierAdd" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<div>
    <div class="LeftFloat" style="width: 70%; text-align: left">
        <h1>
            <asp:Label ID="lblTitle" runat="server" /></h1>
    </div>

    <div style="text-align: right">
        <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' ID="btnSubmitTop" />
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="false" ID="btnCancelTop" />
    </div>

    <div class="ClearBoth">
        <zn:Spacer ID="Spacer1" runat="server" SpacerHeight="10" SpacerWidth="10" />
    </div>

    <div style="margin-bottom: 20px;">
        <asp:Label ID="lblError" runat="server" CssClass="Error" EnableViewState="false" />
    </div>

    <div class="FormView">
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleSupplierName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSupplierName %>'></asp:Localize>
            <span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtName" runat="server" MaxLength="50" Columns="50" />
            <asp:RequiredFieldValidator ID="rfvName" runat="server" CssClass="Error" ControlToValidate="txtName" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSupplierName %>' SetFocusOnError="True" />
        </div>

        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleSupplierCode" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSupplierCode %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtCode" runat="server" MaxLength="100" Columns="50" />
        </div>

        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleDescription" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDescription %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtDescription" runat="server" Rows="10" MaxLength="4000" TextMode="MultiLine" Columns="50" />
        </div>

        <div class="FieldStyle">
        </div>
        <div class="ValueStyleText">
            <asp:CheckBox ID="chkEnabled" runat="server" Text='<%$ Resources:ZnodeAdminResource, CheckBoxEnableSupplier %>' CssClass="FieldStyle" Checked="true" />
        </div>

        <div class="ClearBoth"></div>

        <h4 class="SubTitle">
            <asp:Localize runat="server" ID="SubTitleSupplierContact" Text='<%$ Resources:ZnodeAdminResource, SubTitleSupplierContact %>'></asp:Localize>
        </h4>

        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleSupplierFirstName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSupplierFirstName %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtContactFirstName" runat="server" />
        </div>

        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleSupplierLastName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSupplierLastName %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtContactLastName" runat="server" />
        </div>

        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleContactPhone" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleContactPhone %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtContactPhone" runat="server" />
        </div>

        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleContactEmail" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleContactEmail %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtContactEmail" runat="server" MaxLength="50" Columns="50" />
            <asp:RegularExpressionValidator ID="regContactEmail" runat="server" ControlToValidate="txtContactEmail" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ErrorValidEmail %>' SetFocusOnError="True" ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularValidEmail %>' />
        </div>

        <div class="ClearBoth"></div>

        <h4 class="SubTitle">
            <asp:Localize runat="server" ID="SubTitleSupplierNotification" Text='<%$ Resources:ZnodeAdminResource, SubTitleSupplierNotification %>'></asp:Localize>
        </h4>

        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleNotificationMethod" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleNotificationMethod %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlSupplierType" runat="server" OnSelectedIndexChanged="ddlSupplierType_SelectedIndexChanged" OnDataBound="ddlSupplierType_DataBound" AutoPostBack="True" />
        </div>

        <asp:Panel ID="pnlNotificationEmail" runat="server">
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleNotificationMail" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleNotificationMail %>'></asp:Localize><br />
                <small>
                    <asp:Localize runat="server" ID="HintSubTextNotificationMail" Text='<%$ Resources:ZnodeAdminResource, HintSubTextNotificationMail %>'></asp:Localize><br />
                </small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtNotificationEmail" runat="server" Columns="50" TextMode="MultiLine" />
                <asp:RegularExpressionValidator ID="regNotificationEmail" runat="server" ControlToValidate="txtNotificationEmail" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ErrorValidEmail %>' SetFocusOnError="True" ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularNotificationEmail %>' />
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlEmailNotificationTemplate" runat="server">
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleNotificationTemplate" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleNotificationTemplate %>'></asp:Localize><br />
                <small>
                    <asp:Localize runat="server" ID="HintSubTextNotificationTemplate" Text='<%$ Resources:ZnodeAdminResource, HintSubTextNotificationTemplate %>'></asp:Localize>
                </small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtEmailNotificationTemplate" runat="server" Rows="15" Columns="50" TextMode="MultiLine" />
            </div>
        </asp:Panel>

        <asp:Panel ID="pnlDisplayOrder" runat="server">
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleDisplayOrder" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>'></asp:Localize>
                <span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="9" Columns="5">500</asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvDisplayOrder" runat="server" ControlToValidate="txtDisplayOrder" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrder %>' />
                <asp:RangeValidator ID="rngDisplayOrder" runat="server" CssClass="Error" ControlToValidate="txtDisplayOrder" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeDisplayOrder %>' MinimumValue="1" MaximumValue="999999999" Type="Integer" />
            </div>
        </asp:Panel>

        <div class="FieldStyle">
            <%-- Notice that this checkbox is never shown --%>
        </div>
        <div class="ValueStyleText">
            <asp:CheckBox ID="chkEnableEmailNotification" runat="server" Text='<%$ Resources:ZnodeAdminResource, CheckBoxEnableNotification %>' CssClass="FieldStyle" Checked="true" Visible="False" />
        </div>

        <h4 class="SubTitle">
            <asp:Localize runat="server" ID="SubTitleCustomInformation" Text='<%$ Resources:ZnodeAdminResource, SubTitleCustomInformation %>'></asp:Localize>
        </h4>

        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleCustom1" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCustom1 %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtCustom1" runat="server" TextMode="MultiLine" Width="400" Height="100" MaxLength="2" />
        </div>

        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleCustom2" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCustom2 %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtCustom2" runat="server" TextMode="MultiLine" Width="400" Height="100" MaxLength="2" />
        </div>

        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleCustom3" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCustom3 %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtCustom3" runat="server" TextMode="MultiLine" Width="400" Height="100" MaxLength="2" />
        </div>

        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleCustom4" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCustom4 %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtCustom4" runat="server" Height="100" MaxLength="2" TextMode="MultiLine" Width="400" />
        </div>

        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleCustom5" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCustom5 %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtCustom5" runat="server" Height="100" MaxLength="2" TextMode="MultiLine" Width="400" />
        </div>
    </div>

    <div class="ClearBoth"></div>

    <zn:Spacer ID="Spacer2" runat="server" SpacerHeight="10" SpacerWidth="10" />

    <div class="ClearBoth">
        <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' ID="Button1" />
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="false" ID="Button2" />
    </div>
</div>
