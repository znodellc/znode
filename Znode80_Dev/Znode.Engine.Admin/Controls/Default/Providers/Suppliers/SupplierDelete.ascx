﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SupplierDelete.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Providers.Suppliers.SupplierDelete" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<h1>
	<asp:Label ID="lblDeleteSupplier" runat="server">
        <asp:Localize runat="server" ID="TitleDeleteSupplier" Text='<%$ Resources:ZnodeAdminResource, TitleDeleteSupplier %>'></asp:Localize><%=SupplierName%>
	</asp:Label>
</h1>

<div>
	<zn:Spacer ID="Spacer1" runat="server" SpacerHeight="10" SpacerWidth="3" />
</div>

<p>
    <asp:Localize runat="server" ID="TextDeleteSupplier" Text='<%$ Resources:ZnodeAdminResource, TextDeleteSupplier %>'></asp:Localize>
</p>

<p><asp:Label ID="lblErrorMsg" runat="server" Visible="False" CssClass="Error" /></p>

<div>
	<zn:Spacer ID="Spacer2" runat="server" SpacerHeight="10" SpacerWidth="3" />
</div>

<div>
    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnDelete_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>' CausesValidation="true" ID="btnDelete" />
    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="false" ID="btnCancel" />
</div>

<div>
	<zn:Spacer ID="Spacer3" runat="server" SpacerHeight="15" SpacerWidth="3" />
</div>
