﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Controls.Default.Faceting" CodeBehind="Faceting.ascx.cs" %>
<%@ Register TagPrefix="znode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<div>
    <znode:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></znode:Spacer>
</div>
<!-- Product List -->
<asp:UpdatePanel runat="server" ID="upProductFacets">
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSubmitBottom" />
        <asp:PostBackTrigger ControlID="btnCancelBottom" />
    </Triggers>
    <ContentTemplate>
        <asp:Panel ID="pnlFacetList" runat="server">
            <div align="left">
                <zn:Button runat="server" ButtonType="EditButton" ID="btnEditFaceting" OnClick="BtnEditFaceting_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonEditFacets %>' Width="100px" />
            </div>
            <div>
                <znode:Spacer ID="Spacer14" SpacerHeight="10" SpacerWidth="3" runat="server"></znode:Spacer>
            </div>
            <znode:Spacer ID="Spacer5" runat="server" SpacerHeight="10" />
            <h4 class="GridTitle"><asp:Localize runat="server" ID="FacetGridTitle" Text='<%$ Resources:ZnodeAdminResource, GridTitleFacets %>'></asp:Localize></h4>
            <asp:GridView ID="uxGrid" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                CaptionAlign="Left" CellPadding="4" CssClass="Grid" GridLines="None" Width="100%"
                OnRowCommand="UxGrid_RowCommand" OnDataBound="UxGrid_DataBound">
                <Columns>
                    <asp:BoundField DataField="FacetGroupLabel" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleFacetGroupName %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleFacetNames %>' ItemStyle-Width="60%">
                        <ItemTemplate>
                            <asp:DataList runat="server" ID="dlFacetNames" DataSource='<%# GetFacets(Eval("FacetGroupId")) %>'
                                RepeatDirection="Horizontal" RepeatLayout="Flow">
                                <ItemTemplate>
                                    <asp:Label runat="server" ID="lblFacetNames" Text='<%# Eval("FacetName") %>'></asp:Label>,
                                </ItemTemplate>
                            </asp:DataList>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="RowStyle" />
                <EditRowStyle CssClass="EditRowStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <EmptyDataTemplate>
                </EmptyDataTemplate>
            </asp:GridView>
            <asp:Label runat="server" ID="lblEmptyGrid" Visible="false"></asp:Label>
        </asp:Panel>
        <div>
            <znode:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></znode:Spacer>
        </div>
        <asp:Panel runat="server" ID="pnlEditFacets" Visible="false" CssClass="FormView">
            <div>
                <znode:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></znode:Spacer>
            </div>
            <h4 class="SubTitle">
                <asp:Label runat="server" ID="lblAddEdit" Text='<%$ Resources:ZnodeAdminResource, SubTitleFacetProducts %>'></asp:Label></h4>
            <p>
                <asp:Localize runat="server" ID="FacetProducts" Text='<%$ Resources:ZnodeAdminResource, SubTitleTextFacetProducts %>'></asp:Localize>
            </p>
            <div>
                <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                    ForeColor="red"></asp:Label>
            </div>
            <znode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></znode:Spacer>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleFacets" Text='<%$ Resources:ZnodeAdminResource, ColumnFacets %>'></asp:Localize><br />
            </div>
            <div class="ValueStyle">
                <asp:DataList runat="server" ID="dlFacets" RepeatDirection="Horizontal" RepeatLayout="Flow">
                    <ItemTemplate>
                        <div class="Bold">
                            <asp:Label runat="server" ID="lblFacetGroup" Text='<%# Eval("FacetGroupLabel") %>'></asp:Label>
                        </div>
                        <znode:Spacer ID="Spacer3" SpacerHeight="3" SpacerWidth="3" runat="server"></znode:Spacer>
                        <asp:CheckBoxList ID="chklstFacets" OnPreRender="ChklstFacets_PreRender" RepeatLayout="Table"
                            DataTextField="FacetName" DataValueField="FacetID" DataSource='<%# BindFacets(Eval("FacetGroupId")) %>'
                            RepeatDirection="Horizontal" runat="server">
                        </asp:CheckBoxList>
                        <znode:Spacer ID="Spacer14" SpacerHeight="10" SpacerWidth="3" runat="server"></znode:Spacer>
                    </ItemTemplate>
                </asp:DataList>
                <asp:Label runat="server" ID="lblNoFacets" Visible="false" Text='<%$ Resources:ZnodeAdminResource, RecordNotFoundFacet %>'></asp:Label>
            </div>
            <znode:Spacer ID="Spacer1" runat="server" SpacerHeight="10" />
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <zn:Button runat="server" ButtonType="SubmitButton" OnClick="Update_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="True" ID="btnSubmitBottom" />
                <zn:Button runat="server" ButtonType="CancelButton" OnClick="Cancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" ID="btnCancelBottom"/>
            </div>
            <znode:Spacer ID="Spacer4" runat="server" SpacerHeight="10" />
        </asp:Panel>
        <asp:UpdateProgress ID="uxFacetUpdateProgress" runat="server" AssociatedUpdatePanelID="upProductFacets"
            DisplayAfter="10">
            <ProgressTemplate>
                <div id="ajaxProgressBg">
                </div>
                <div id="ajaxProgress">
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </ContentTemplate>
</asp:UpdatePanel>

