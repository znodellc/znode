﻿using System;
using System.Web.UI.WebControls;

namespace  Znode.Engine.Admin.Controls.Default
{
    /// <summary>
    /// Represents the Product AutoComplete user control class
    /// </summary>
    public partial class ProductAutoComplete : System.Web.UI.UserControl
    {
        private event System.EventHandler onSelectedIndexChanged;

        /// <summary>
        /// Gets or sets the Width
        /// </summary>
        public string Width
        {
            get
            {
                return txtProduct.Width.ToString();
            }

            set
            {
                txtProduct.Width = new Unit(value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the Auto post Back is true or false
        /// </summary>
        public bool AutoPostBack
        {
            get
            {
                return bool.Parse(hdnAutoPostBack.Value);
            }

            set
            {
                hdnAutoPostBack.Value = value.ToString();
            }
        }

        /// <summary>
        /// Gets or sets the value
        /// </summary>
        public string Value
        {
            get
            {
                return ProductId.Value;
            }

            set
            {
                ProductId.Value = value;
            }
        }

        /// <summary>
        /// Gets or sets the Text
        /// </summary>
        public string Text
        {
            get
            {
                return txtProduct.Text;
            }

            set
            {
                txtProduct.Text = value;
            }
        }

        /// <summary>
        /// Sets a value indicating whether IsRequired is true or false
        /// </summary>
        public bool IsRequired
        {
            set
            {
                RequiredFieldValidator2.Visible = value;
            }
        }

        /// <summary>
        /// Sets the Context Key
        /// </summary>
        public string ContextKey
        {
            set
            {
                autoCompleteExtender3.ContextKey = value;
            }
        }

        // Sets the Event Handler On Selected Index Changed
        public System.EventHandler OnSelectedIndexChanged
        {
            set
            {
                this.onSelectedIndexChanged = value;
            }
        }

        /// <summary>
        /// Event is raised when AutoComplete Selected Index is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AutoComplete_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.onSelectedIndexChanged != null)
            {
                this.onSelectedIndexChanged(sender, e);
            }
        }
    }
}