﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Service;

namespace Znode.Engine.Admin.Controls.Default.Search
{
    public partial class YmalSelect : System.Web.UI.UserControl
    {
        public int CurrentPage = 0;

        protected void Page_Load(object sender, EventArgs e)
        {
            PersonalizationHelper.ApplyClearButtonAttributes(ClearButton, Page);
            PersonalizationHelper.ApplySearchButtonAttributes(SearchButton, Page);

            var portalService = new PortalService();
            var portals = portalService.GetAll();
            var preselectedPortal = string.IsNullOrEmpty(Request.QueryString["Portal"])
                                        ? 0
                                        : int.Parse(Request.QueryString["Portal"]);

            if (!this.IsPostBack)
            {
                var orderedPortals = portals.OrderBy(portal => portal.StoreName);

                var portalList = orderedPortals.Select(
                    portal =>
                    new ListItem
                    {
                        Text = portal.StoreName,
                        Value = portal.PortalID.ToString(CultureInfo.InvariantCulture)
                    }).ToList();

                StoreList.DataTextField = "Text";
                StoreList.DataValueField = "Value";
                StoreList.DataSource = portalList;
                StoreList.DataBind();
                StoreList.SelectedIndex = 0;

                SetCategory(int.Parse(StoreList.SelectedValue));

                this.BindData();
            }

            if (Request.QueryString["Mode"] != Enum.GetName(typeof(RelationType), RelationType.YMAL) || preselectedPortal <= 0)
                return;
        }

        private void BindAllCategories()
        {
            var categoryService = new CategoryService();
            var categories = categoryService.GetAll();

            var orderedCategories = categories.OrderBy(category => category.Name);
            var categoryList = orderedCategories.Select(
                    category =>
                    new ListItem { Text = category.Name, Value = category.CategoryID.ToString(CultureInfo.InvariantCulture) }).ToList();
            categoryList.Insert(0, new ListItem { Text = this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), Value = "0" });

            SearchCategory.DataTextField = "Text";
            SearchCategory.DataValueField = "Value";
            SearchCategory.DataSource = categoryList;
            SearchCategory.DataBind();

            if (Request.QueryString.AllKeys.Contains("Category"))
            {
                SearchCategory.SelectedIndex =
                    SearchCategory.Items.IndexOf(SearchCategory.Items.FindByValue(Request.QueryString["Category"]));
            }
        }

        protected void SearchButton_OnClick(object sender, EventArgs e)
        {
            this.BindData();
        }

        protected void DeleteLink_OnClick(object sender, EventArgs e)
        {
            var productId = ((LinkButton)sender).CommandArgument;

            int iTry;
            if (productId.Length == 0 || !int.TryParse(productId, out iTry))
            {
                throw new ArgumentException(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInvalidProductId").ToString(), productId));
            }

            var crossSellService = new ProductCrossSellService();

            var recordsToDelete = crossSellService.Find(string.Format("ProductID = {0} AND RelationTypeId = {1}", productId, (int)RelationType.YMAL));

            crossSellService.Delete(recordsToDelete);

            SearchButton_OnClick(null, null);
        }

        protected void StoreList_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            if (int.Parse(StoreList.SelectedValue) == 0)
            {
                BindAllCategories();
                CatalogLabel.Text = string.Empty;
            }
            else
            {
                SetCategory(int.Parse(StoreList.SelectedValue));
            }

            // since store id is baked into the manage link the search needs redone so that id can get updated
            if (SearchResults.Rows.Count > 0)
            {
                SearchButton_OnClick(null, null);
            }
        }

        protected void ClearButton_OnClick(object sender, EventArgs e)
        {
            SearchCategory.SelectedIndex = 0;
            StoreList.SelectedIndex = 0;
            SearchProductName.Text = string.Empty;
            SearchSKU.Text = string.Empty;
            SetCategory(int.Parse(StoreList.SelectedValue));
            this.BindData();
        }

        protected void SearchResults_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType != DataControlRowType.DataRow) return;
            if (e.Row.Cells[1].Text == "&nbsp;")
            {
                e.Row.FindControl("DeleteLink").Visible = false;
            }
            else
            {
                ((LinkButton)e.Row.FindControl("DeleteLink")).CommandArgument = ((DataRowView)e.Row.DataItem)["ProductId"].ToString();
            }

            var urlLink = ((LinkButton)e.Row.FindControl("ManageLink"));
            var url = urlLink.PostBackUrl;
            url = string.Format(url, ((DataRowView)e.Row.DataItem)["ProductId"], StoreList.SelectedValue, SearchCategory.SelectedValue);
            urlLink.PostBackUrl = url;
        }

        protected void SearchResults_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            CurrentPage = e.NewPageIndex;
            this.BindData();
        }


        #region Private Methods
        private void SetCategory(int catalogID)
        {
            var storeCategories = PersonalizationHelper.GetCatalogCategories(catalogID);

            if (storeCategories.Catalog.Length == 0)
            {
                CatalogLabel.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCatalogAssociation").ToString();
                return;
            }
            else
            {
                CatalogLabel.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "CategoriesLoaded").ToString(), storeCategories.Catalog);
            }

            var categoryList = storeCategories.Categories.OrderBy(category => category.Name).Select(
                category =>
                new ListItem { Text = category.Name, Value = category.CategoryID.ToString(CultureInfo.InvariantCulture) }).ToList();
            categoryList.Insert(0, new ListItem { Text = this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), Value = "0" });

            SearchCategory.DataTextField = "Text";
            SearchCategory.DataValueField = "Value";
            SearchCategory.DataSource = categoryList;
            SearchCategory.DataBind();
        }


        /// <summary>
        /// Binds the data
        /// </summary>
        private void BindData()
        {
            var products = PersonalizationHelper.DoProductSearch(Server.HtmlEncode(SearchProductName.Text), Server.HtmlEncode(SearchSKU.Text),
                                                    int.Parse(SearchCategory.SelectedValue), int.Parse(StoreList.SelectedValue));

            var productService = new ProductService();

            var results = new DataTable();

            results.Columns.Add("Product");
            results.Columns.Add("Associated");
            results.Columns.Add(new DataColumn { ColumnName = "ProductID", DataType = typeof(int) });

            results.PrimaryKey = new[] { results.Columns[2] };

            var distinctProducts = products.Distinct().OrderBy(product => product.Name).ToList();

            foreach (var product in distinctProducts)
            {
                var crossSellService = new ProductCrossSellService();
                var crossSellItems = crossSellService.Find(string.Format("ProductID = {0} AND RelationTypeID = {1}", product.ProductID, (int)RelationType.YMAL));

                if (crossSellItems.Count == 0) continue;

                var row = new List<object> { Server.HtmlDecode(product.Name) };

                var associatedIds = crossSellItems.OrderBy(sell => sell.DisplayOrder).Select(sell => sell.RelatedProductId).Take(5);
                var productNames = associatedIds.Select(associatedId => productService.GetByProductID(associatedId).Name).ToList();

                row.Add(String.Join(", ", productNames) + (crossSellItems.Count > 5 ? " <span title='" + this.GetGlobalResourceObject("ZnodeAdminResource", "CheckAssociations") + "'>...</span>" : string.Empty));
                row.Add(product.ProductID);

                results.Rows.Add(row.ToArray());
            }

            foreach (var distinctProduct in distinctProducts)
            {
                if (!results.Rows.Contains(distinctProduct.ProductID))
                {
                    results.Rows.Add(Server.HtmlDecode(distinctProduct.Name), null, distinctProduct.ProductID);
                }
            }

            SearchResults.DataSource = results;

            SearchResults.PageIndex = CurrentPage;

            SearchResults.DataBind();
        }
        #endregion
    }
}