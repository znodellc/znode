﻿using System;

namespace  Znode.Engine.Admin.Controls.Default.Search
{
    /// <summary>
    /// Represents the SiteAdmin -  SiteAdmin.Controls.Default.Search.FieldBoostSelect class
    /// </summary> 
    /// 
    public partial class FieldBoostSelect : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {

            }
            else
            {
                ClearSaveResultLabel();
            }
        }

        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            ClearSaveResultLabel();

            ucFieldBoostList.CancelChanges();
        }

        protected void BtnSave_Click(object sender, EventArgs e)
        {
            try
            {
                var saveResult = ucFieldBoostList.SaveChages();
                if (saveResult)
                {
                    lblSaveResult.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ChangesSaved").ToString();
                    lblSaveResult.CssClass = "Success";
                }
                else
                {
                    lblSaveResult.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "NoChanges").ToString();
                    lblSaveResult.CssClass = "Error";
                }
            }
            catch (Exception ex)
            {
                lblSaveResult.Text = string.Format(lblSaveResult.Text, this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSavingChanges"), ex.Message);
                lblSaveResult.CssClass = "Error";
            }
            
        }

        protected void ClearSaveResultLabel()
        {
            lblSaveResult.Text = string.Empty;
        }
    }
}