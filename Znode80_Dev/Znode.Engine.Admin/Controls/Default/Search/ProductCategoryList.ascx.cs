﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Controls.Default.Search
{
    
    /// <summary>
    /// Represents the SiteAdmin -  SiteAdmin.Controls.Default.Search.ProductCategoryList class
    /// </summary> 
    ///  
    public partial class ProductCategoryList : System.Web.UI.UserControl
    {
        /// <summary>
        /// Gets or sets the Total Records
        /// </summary>
        public bool IsCountExist
        {
            get
            {
                if (gvProductCategoryList.Rows.Count > 0)
                {
                    return true;
                }

                return false;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void Bind(string productName, string productNumber, string sku, string manufacturerId, string productTypeId, string categoryId, string catalogId)
        {
            var dtProduct = new DataTable();
            try
            {
                Session["searchProductName"] = productName;
                Session["searchProductNumber"] = productNumber;
                Session["searchSKU"] = sku;
                Session["searchManufacturerId"] = manufacturerId;
                Session["searchProductTypeId"] = productTypeId;
                Session["searchCategoryId"] = categoryId;
                Session["searchCatalogId"] = catalogId;

				dtProduct = GetProductCategoryBoosts(productName, productNumber, sku, manufacturerId, productTypeId, categoryId, catalogId);

				gvProductCategoryList.DataSource = dtProduct;

                gvProductCategoryList.PageIndex = 0;
                Session["productCategoryBoostDS"] = gvProductCategoryList.DataSource;
                gvProductCategoryList.DataBind();
                StoreOriginalValues(dtProduct); 
                if (gvProductCategoryList.Rows.Count > 0)
                {
                    gvProductCategoryList.Visible = true;
                    
                }
                

            }
            catch (System.Data.SqlClient.SqlException sqlex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(sqlex.ToString());
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }
        }

        public void CancelChanges()
        {

            ClearStoredData();
            Bind(Session["searchProductName"].ToString(), Session["searchProductNumber"].ToString(), Session["searchSKU"].ToString(), Session["searchManufacturerId"].ToString(),
                Session["searchProductTypeId"].ToString(), Session["searchCategoryId"].ToString(), Session["searchCatalogId"].ToString());
            gvProductCategoryList.Visible = false;
           
            
        }

        public void ClearDataSource()
        {
            gvProductCategoryList.DataSource = null;
            gvProductCategoryList.DataBind();
            ClearStoredData();
        }

        protected void ClearStoredData()
        {
            Session["boostChanges"] = null;
        }

        protected string FormatBoostValue(string boostValue)
        {
            if (!boostValue.Contains("."))
            {
                return boostValue + ".00";
            }
            return boostValue;
        }

        protected DataTable GetProductCategoryBoosts(string productName, string productNumber, string sku, string manufacturerId,
                                      string productTypeId, string categoryId, string catalogId)
        {
            var prodadmin = new ProductAdmin();
            var categoryadmin = new CategoryAdmin();
            var searchAdmin = new SearchAdmin();
            var ds = prodadmin.SearchProduct(productName, productNumber, sku, manufacturerId, productTypeId, categoryId, catalogId);
            var dt = new DataTable();
            dt.Columns.Add("ProductID");
            dt.Columns.Add("ProductCategoryID");
            dt.Columns.Add("ProductName");
            dt.Columns.Add("CategoryName");
            dt.Columns.Add("BoostValue");
            int index = 0;
           
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    var productId = Convert.ToInt32(row["ProductID"]);
                    var intCategoryId = Convert.ToInt32(categoryId);
                    var productCategories = categoryId.Equals("0") ? prodadmin.GetProductCategoriesByProductID(productId) : prodadmin.GetProductCategoriesByProductID(productId).Where(pc => pc.CategoryID == intCategoryId);
                    row["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index++]["Name"].ToString());
                    if (productCategories != null)
                    {
                        foreach (ProductCategory productCategory in productCategories)
                        {

                            var productCategoryBoost = searchAdmin.GetLuceneGlobalProductCategoryBoost(productCategory.ProductCategoryID);
                            dt.Rows.Add(
                                productId,
                                productCategory.ProductCategoryID,
                                row["Name"],
                               Server.HtmlDecode(categoryadmin.GetByCategoryId(productCategory.CategoryID).Name),
                                productCategoryBoost == null
                                    ? "1.00"
                                    : FormatBoostValue(productCategoryBoost.Boost.ToString()));
                        }
                    }
                }
            var dv = dt.DefaultView;
            dv.Sort = "BoostValue desc, ProductName asc, CategoryName asc";
            return dv.ToTable();
        }


        protected void gvProductCategoryList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            StoreNewValues();
             
            gvProductCategoryList.PageIndex = e.NewPageIndex;
            if (Session["productCategoryBoostDS"] != null)
            {
                var dtProduct = (DataTable) Session["productCategoryBoostDS"];
                gvProductCategoryList.DataSource = dtProduct;
                StoreOriginalValues(dtProduct);
            }
            gvProductCategoryList.DataBind();
            
        }

        public bool SaveChages()
        {
            var saveResult = false;
            StoreNewValues();
            if (Session["boostChanges"] != null)
            {
                var boostChanges = (Dictionary<int, double>)Session["boostChanges"];

                var searchAdmin = new SearchAdmin();

                foreach (var boostChange in boostChanges)
                {
                    var productCategoryBoost = searchAdmin.GetLuceneGlobalProductCategoryBoost(boostChange.Key);
                    if (productCategoryBoost == null)
                    {
                        var globalProductId = 0;
                        saveResult = searchAdmin.AddLuceneGlobalProductCategoryBoost(new LuceneGlobalProductCategoryBoost
                        {
                                ProductCategoryID = boostChange.Key,
                                Boost = boostChange.Value
                            }, out globalProductId);
                    }
                    else
                    {
                        productCategoryBoost.Boost = FormatForTwoDecimalSpots(boostChange.Value);
                        saveResult = searchAdmin.UpdateLuceneGlobalProductCategoryBoost(productCategoryBoost);
                    }
                }
                ClearStoredData();
                Bind(Session["searchProductName"].ToString(), Session["searchProductNumber"].ToString(), Session["searchSKU"].ToString(), Session["searchManufacturerId"].ToString(),
                    Session["searchProductTypeId"].ToString(), Session["searchCategoryId"].ToString(), Session["searchCatalogId"].ToString());
            }

            return saveResult;
        }

        public double FormatForTwoDecimalSpots(double value)
        {
            string truncValue = value.ToString("####.##");
            double result;

            if (double.TryParse(truncValue, out result))
            {
                return result;
            }
            else
            {
                return new double();
            }
        }

        public void StoreNewValues()
        {
			var dtOriginalData = (DataTable)Session["originalValues"];
			var originalBoostValue = "0.00";

            foreach (GridViewRow row in gvProductCategoryList.Rows)
            {
				var productCategoryID = Convert.ToInt32(gvProductCategoryList.DataKeys[row.RowIndex]["ProductCategoryID"]);

				var originalRows = dtOriginalData.Select(string.Format("ProductCategoryID='{0}'", productCategoryID));

				if (originalRows.Any())
				{					 
					var newBoostValue = ((TextBox)row.FindControl("txtBoostValue")).Text;

					originalBoostValue = originalRows[0]["BoostValue"].ToString();

					if (!string.IsNullOrEmpty(newBoostValue))
					{
						if (!newBoostValue.Equals(originalBoostValue))
						{
							if (Session["boostChanges"] == null)
							{
								Session["boostChanges"] = new Dictionary<int, double>();
							}

							var boostChanges = (Dictionary<int, double>)Session["boostChanges"];
							boostChanges.Add(productCategoryID, Convert.ToDouble(newBoostValue));
						}
					}
				}
			}
        }

        public void StoreOriginalValues(DataTable dtProduct)
        {
			Session["originalValues"] = dtProduct;
        }
    }
}