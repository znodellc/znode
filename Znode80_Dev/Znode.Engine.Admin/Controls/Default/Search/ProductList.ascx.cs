﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Controls.Default.Search
{
    /// <summary>
    /// Represents the SiteAdmin -  SiteAdmin.Controls.Default.Search.ProductList class
    /// </summary> 
    /// 
    public partial class ProductList : System.Web.UI.UserControl
    {

        /// <summary>
        /// Gets or sets the Total Records
        /// </summary>
        public bool IsCountExist
        {
            get
            {
                if (gvProductList.Rows.Count > 0)
                {
                    return true;
                }

                return false;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
           if (!IsPostBack)
                Bind("","","","0","0","0","0");
        }
        
        public void Bind(string productName, string productNumber, string sku, string manufacturerId, string productTypeId, string categoryId, string catalogId)
        {
            try
            {
                Session["searchProductName"] = productName;
                Session["searchProductNumber"] = productNumber;
                Session["searchSKU"] = sku;
                Session["searchManufacturerId"] = manufacturerId;
                Session["searchProductTypeId"] = productTypeId;
                Session["searchCategoryId"] = categoryId;
                Session["searchCatalogId"] = catalogId;

                var productBoostDS = GetProductBoosts(productName, productNumber, sku, manufacturerId, productTypeId, categoryId, catalogId);
                gvProductList.PageIndex = 0;
                Session["productBoostDS"] = productBoostDS;
                gvProductList.DataSource = productBoostDS;
                gvProductList.DataBind();
                StoreOriginalValues(productBoostDS);
                if (gvProductList.Rows.Count > 0)
                {
                    gvProductList.Visible = true;
                }
               

            }
            catch (System.Data.SqlClient.SqlException sqlex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(sqlex.ToString());
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }
        }

        public void CancelChanges()
        {

            ClearStoredData();
            Bind(Session["searchProductName"].ToString(), Session["searchProductNumber"].ToString(), Session["searchSKU"].ToString(), Session["searchManufacturerId"].ToString(),
                Session["searchProductTypeId"].ToString(), Session["searchCategoryId"].ToString(), Session["searchCatalogId"].ToString());
        }

        public void ClearDataSource()
        {
            gvProductList.DataSource = null;
            gvProductList.DataBind();
            ClearStoredData();
        }

        protected void ClearStoredData()
        {
            Session["boostChanges"] = null;
        }

        protected string FormatBoostValue(string boostValue)
        {
            if (!boostValue.Contains("."))
            {
                return boostValue + ".00";
            }
            return boostValue;
        }

        protected DataTable GetProductBoosts(string productName, string productNumber, string sku, string manufacturerId,
                                      string productTypeId, string categoryId, string catalogId)
        {
            var prodadmin = new ProductAdmin();
            var searchAdmin = new SearchAdmin();
            var ds = prodadmin.SearchProduct(productName, productNumber, sku, manufacturerId, productTypeId, categoryId, catalogId);
            ds.Tables[0].Columns.Add("BoostValue");
            int index = 0;
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                var productId = Convert.ToInt32(row["ProductID"]);
                var productBoost = searchAdmin.GetLuceneGlobalProductBoost(productId);
                row["BoostValue"] = productBoost == null ? "1.00" : FormatBoostValue(productBoost.Boost.ToString());
                row["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index++]["Name"].ToString());
            }
            var dt = ds.Tables[0];
            var dv = dt.DefaultView;
            dv.Sort = "BoostValue desc, Name asc";
            return dv.ToTable();
        }


        protected void gvProductList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            StoreNewValues();
            gvProductList.PageIndex = e.NewPageIndex;
            if (Session["productBoostDS"] != null)
            {
                var productBoostDS = (DataTable)Session["productBoostDS"];
                gvProductList.DataSource = productBoostDS;
                StoreOriginalValues(productBoostDS);
            }
            gvProductList.DataBind();
            
        }

        public bool SaveChages()
        {
            var saveResult = false;
            StoreNewValues();
            if (Session["boostChanges"] != null)
            {
                var boostChanges = (Dictionary<int, double>)Session["boostChanges"];

                var searchAdmin = new SearchAdmin();

                foreach (var boostChange in boostChanges)
                {
                    var productBoost = searchAdmin.GetLuceneGlobalProductBoost(boostChange.Key);
                    if (productBoost == null)
                    {
                        var globalProductId = 0;
                        saveResult = searchAdmin.AddLuceneGlobalProductBoost(new LuceneGlobalProductBoost
                            {
                                ProductID = boostChange.Key,
                                Boost = boostChange.Value
                            }, out globalProductId);
                    }
                    else
                    {
                        productBoost.Boost = FormatForTwoDecimalSpots(boostChange.Value);
                        saveResult = searchAdmin.UpdateLuceneGlobalProductBoost(productBoost);
                    }
                }
                ClearStoredData();
                Bind(Session["searchProductName"].ToString(), Session["searchProductNumber"].ToString(), Session["searchSKU"].ToString(), Session["searchManufacturerId"].ToString(),
                    Session["searchProductTypeId"].ToString(), Session["searchCategoryId"].ToString(), Session["searchCatalogId"].ToString());
            }

            return saveResult;
        }

        public double FormatForTwoDecimalSpots(double value)
        {
            string truncValue = value.ToString("####.##");
            double result;

            if (double.TryParse(truncValue, out result))
            {
                return result;
            }
            else
            {
                return new double();
            }
        }

		public void StoreNewValues()
		{
			var dtOriginalData = (DataTable)Session["originalProductValues"];
			var originalBoostValue = "0.00";

			foreach (GridViewRow row in gvProductList.Rows)
			{
				var productID = Convert.ToInt32(gvProductList.DataKeys[row.RowIndex]["ProductID"]);

                var originalRows = dtOriginalData.Select(string.Format("ProductID='{0}'", productID));

				if (originalRows.Any())
				{
					var newBoostValue = ((TextBox)row.FindControl("txtBoostValue")).Text;

					originalBoostValue = originalRows[0]["BoostValue"].ToString();

					if (!string.IsNullOrEmpty(newBoostValue))
					{
						if (!newBoostValue.Equals(originalBoostValue))
						{
							if (Session["boostChanges"] == null)
							{
								Session["boostChanges"] = new Dictionary<int, double>();
							}

							var boostChanges = (Dictionary<int, double>)Session["boostChanges"];
							if (!boostChanges.ContainsKey(Convert.ToInt32(gvProductList.DataKeys[row.RowIndex]["ProductID"])))
							{
								boostChanges.Add(productID, Convert.ToDouble(newBoostValue));
							}
						}
					}
				}
			}
		}

        public void StoreOriginalValues(DataTable table)
        {
            Session["originalProductValues"] = table;
        }
    }
}