﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductCategoryList.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Search.ProductCategoryList" %>

<asp:GridView ID="gvProductCategoryList" CellPadding="0" CssClass="Grid" runat="server" AutoGenerateColumns="False"
    AllowPaging="True" Width="100%" GridLines="Both" DataKeyNames="ProductCategoryID" ViewStateMode="Enabled" OnPageIndexChanging="gvProductCategoryList_PageIndexChanging">
    <Columns>
        <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleProductID %>" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100" HeaderStyle-Width="100" ItemStyle-CssClass="TextSpace">
            <ItemTemplate>
                <asp:Label ID="lblProductId" Text='<%# DataBinder.Eval(Container.DataItem, "ProductID").ToString() %>' runat="server"></asp:Label>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>" DataField="ProductName" HeaderStyle-HorizontalAlign="Left" />
        <asp:BoundField HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleCategoryName %>" DataField="CategoryName" HeaderStyle-HorizontalAlign="Left" />
        <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleCategoryLevelBoostValue %>">
            <ItemStyle HorizontalAlign="Left"></ItemStyle>
            <ItemTemplate>
                <asp:TextBox ID="txtBoostValue" Text='<%# DataBinder.Eval(Container.DataItem, "BoostValue").ToString() %>'
                    runat="server">
                </asp:TextBox>
                <asp:RangeValidator ID="rvBoostValue" runat="server" Type="Double" MinimumValue="0.00" MaximumValue="1000.00"
                    ControlToValidate="txtBoostValue" Display="Dynamic"
                    ErrorMessage="<%$ Resources:ZnodeAdminResource, RangeBoostValue %>">
                </asp:RangeValidator>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EditRowStyle CssClass="EditRowStyle" />
    <FooterStyle CssClass="FooterStyle" />
    <RowStyle CssClass="RowStyle" />
    <SelectedRowStyle CssClass="SelectedRowStyle" />
    <PagerStyle CssClass="PagerStyle" />
    <HeaderStyle CssClass="HeaderStyle" />
    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
</asp:GridView>
