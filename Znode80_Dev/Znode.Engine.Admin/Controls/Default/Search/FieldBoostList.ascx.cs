﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;

namespace  Znode.Engine.Admin.Controls.Default.Search
{
    /// <summary>
    /// Represents the SiteAdmin -  SiteAdmin.Controls.Default.Search.FieldBoostList class
    /// </summary> 
    /// 
    public partial class FieldBoostList : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Bind();
            }
        }

        public void Bind()
        {
            
            try
            {
               var dtProduct = GetFieldLevelBoosts();
                gvFieldBoostList.PageIndex = 0;

                gvFieldBoostList.DataSource = dtProduct;

                Session["fieldLevelBoostDS"] = dtProduct;
                gvFieldBoostList.DataBind();
                StoreOriginalValues(dtProduct);
            }
            catch (System.Data.SqlClient.SqlException sqlex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(sqlex.ToString());
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }
        }

        public void CancelChanges()
        {

            ClearStoredData();
        }
        
        protected void ClearStoredData()
        {
            Session["boostChanges"] = null;
            Bind();
        }

        protected string FormatBoostValue(string boostValue)
        {
            if (!boostValue.Contains("."))
            {
                return boostValue + ".00";
            }
            return boostValue;
        }

        protected DataTable GetFieldLevelBoosts()
        {
            var searchAdmin = new SearchAdmin();
            var dt = new DataTable();
            dt.Columns.Add("LuceneDocumentMappingID");
            dt.Columns.Add("DocumentName");
            dt.Columns.Add("BoostValue");
            foreach (var fieldLevelBoost in searchAdmin.GetLuceneFieldLevelBoosts())
            {
                var boost = fieldLevelBoost.Boost.HasValue
                    ? FormatBoostValue(fieldLevelBoost.Boost.ToString()) : "1.00";
                dt.Rows.Add(fieldLevelBoost.LuceneDocumentMappingID, fieldLevelBoost.DocumentName, boost);
            }

            var dv = dt.DefaultView;
            dv.Sort = "BoostValue desc, DocumentName asc";
            return dv.ToTable();
        }


        protected void gvFieldBoostList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            StoreNewValues();
            gvFieldBoostList.PageIndex = e.NewPageIndex;
            if (Session["fieldLevelBoostDS"] != null)
            {
                var dtProduct = (DataTable) Session["fieldLevelBoostDS"];
                gvFieldBoostList.DataSource =dtProduct ;
                StoreOriginalValues(dtProduct);
            }
            gvFieldBoostList.DataBind();
            
        }

        public bool SaveChages()
        {
            var saveResult = false;
            StoreNewValues();
            if (Session["boostChanges"] != null)
            {
                var boostChanges = (Dictionary<int, double>)Session["boostChanges"];

                var searchAdmin = new SearchAdmin();

                foreach (var boostChange in boostChanges)
                {
                    var fieldLevelBoost = searchAdmin.GetLuceneFieldLevelBoost(boostChange.Key);
                    if (fieldLevelBoost == null)
                    {
                        saveResult = false;
                    }
                    else
                    {
                        fieldLevelBoost.Boost = FormatForTwoDecimalSpots(boostChange.Value);
                        saveResult = searchAdmin.UpdateLuceneFieldLevelBoost(fieldLevelBoost);
                    }
                }
                ClearStoredData();
            }

            return saveResult;
        }

        public double FormatForTwoDecimalSpots(double value)
        {
            string truncValue = value.ToString("####.##");
            double result;

            if (double.TryParse(truncValue, out result))
            {
                return result;
            }
            else
            {
                return new double();
            }
        }

		public void StoreNewValues()
		{
			var dtOriginalData = (DataTable)Session["originalfieldValues"];
			var originalBoostValue = "0.00";

			foreach (GridViewRow row in gvFieldBoostList.Rows)
			{
				var luceneDocumentMappingID = Convert.ToInt32(gvFieldBoostList.DataKeys[row.RowIndex]["LuceneDocumentMappingID"]);

				var originalRows = dtOriginalData.Select(string.Format("LuceneDocumentMappingID='{0}'", luceneDocumentMappingID));

				if (originalRows.Any())
				{
					var newBoostValue = ((TextBox)row.FindControl("txtBoostValue")).Text;

					originalBoostValue = originalRows[0]["BoostValue"].ToString();

					if (!string.IsNullOrEmpty(newBoostValue))
					{
						if (!newBoostValue.Equals(originalBoostValue))
						{
							if (Session["boostChanges"] == null)
							{
								Session["boostChanges"] = new Dictionary<int, double>();
							}

							var boostChanges = (Dictionary<int, double>)Session["boostChanges"];
							if (!boostChanges.ContainsKey(Convert.ToInt32(gvFieldBoostList.DataKeys[row.RowIndex]["LuceneDocumentMappingID"])))
							{
								boostChanges.Add(luceneDocumentMappingID, Convert.ToDouble(newBoostValue));
							}
						}
					}
				}
			}
		}

        public void StoreOriginalValues(DataTable dtProduct)
        {
            Session["originalfieldValues"] = dtProduct;
        }
    }
}