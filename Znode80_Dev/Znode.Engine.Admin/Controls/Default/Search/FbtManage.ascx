﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FbtManage.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Search.FbtManage" %>

<%--Added to prevent shifting back to previous page when ENTER key is pressed.--%>
<script src='<%= Page.ResolveClientUrl("/js/jquery-1.9.0.min.js") %>' type="text/javascript"></script>
<script type="text/javascript">
    $(document).keypress(function (e) {
        var keycode = (e.keyCode ? e.keyCode : e.which);
        if (keycode == '13') {
            e.preventDefault();
            $("#ctl00_ctl00_uxMainContent_uxMainContent_FbtManage_SearchButton").click();
        }
    });
</script>

<div style="width: 100%;">
    <div style="display: inline-block; width: 70%;">
        <h1>
            <asp:Localize runat="server" ID="TitleAssociatedProducts" Text="<%$ Resources:ZnodeAdminResource, TitleAssociatedProducts %>"></asp:Localize>
            <asp:Label runat="server" ID="ProductName"></asp:Label></h1>
        <p>
            <asp:Localize runat="server" ID="ManageFBTItems" Text="<%$ Resources:ZnodeAdminResource, TextManageFBTItems %>"></asp:Localize>
        </p>
        <asp:Label ID="lblErrorMsg" runat="server" Text="" CssClass="Error"></asp:Label>
    </div>
    <div style="display: inline-block; width: 29%; text-align: right; vertical-align: top;">
        <zn:Button runat="server" ButtonType="EditButton" OnClick="btnBack_OnClick" Text="<%$ Resources:ZnodeAdminResource, ButtonBackToFBT %>" ID="btnBack" />
    </div>
    <div class="ClearBoth"></div>
</div>
<h4 class="SubTitle">
    <asp:Localize runat="server" ID="SearchProducts" Text="<%$ Resources:ZnodeAdminResource, SubTitleSearchProducts %>"></asp:Localize>
</h4>
<div>
    <table>
        <tr>
            <td><span class="SearchTitle">
                <asp:Localize runat="server" ID="TitleProductName" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>"></asp:Localize>
            </span></td>
            <td><span class="SearchTitle">
                <asp:Localize runat="server" ID="TitleSKU" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleSKU %>"></asp:Localize>
            </span></td>
            <td><span class="SearchTitle">
                <asp:Localize runat="server" ID="TitleCategory" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleCategory %>"></asp:Localize>
            </span></td>
        </tr>
        <tr>
            <td>
                <asp:TextBox runat="server" ID="SearchProductName" ClientIDMode="Static"></asp:TextBox></td>
            <td>
                <asp:TextBox runat="server" ID="SearchSKU" ClientIDMode="Static"></asp:TextBox></td>
            <td>
                <asp:DropDownList runat="server" ID="SearchCategory" ClientIDMode="Static" AppendDataBoundItems="False" /></td>
        </tr>
    </table>
    <div class="ClearAll"></div>
    <br />
    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="SearchButton_OnClick" Text="<%$ Resources:ZnodeAdminResource, ButtonSearch %>" ID="SearchButton" />
    <zn:Button runat="server" ButtonType="CancelButton" OnClick="ClearButton_OnClick" Text="<%$ Resources:ZnodeAdminResource, ButtonClear %>" ID="ClearButton" />
</div>
<h4 class="SubTitle"></h4>
<div style="display: block;">
    <div>
        <div class="LeftFloat" style="width: 200px;">
            <span class="SearchFieldStyle">
                <asp:Localize runat="server" ID="TitleUnassociatedProducts" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleUnassociatedProducts %>"></asp:Localize>
            </span>
            <br />
            <span>
                <asp:ListBox ID="UnassociatedProducts" runat="server" AppendDataBoundItems="False" SelectionMode="Multiple" Width="150px"
                    Height="150px"></asp:ListBox>
            </span>
        </div>
        <div class="LeftFloat" style="width: 150px; padding-top: 50px;">
            <span>
                <zn:Button runat="server" ButtonType="EditButton" OnClick="AddAssociation_OnClick" Text="<%$ Resources:ZnodeAdminResource, ButtonAdd %>"
                    ID="AddAssociation" Width="100px" /><br />
                <br />
                <zn:Button runat="server" ButtonType="EditButton" OnClick="RemoveAssociation_OnClick" Text="<%$ Resources:ZnodeAdminResource, ButtonAssociationRemove %>"
                    ID="RemoveAssociation" Width="100px" />
            </span>
        </div>
        <div class="LeftFloat" style="width: 170px">
            <span class="SearchFieldStyle">
                <asp:Localize runat="server" ID="TitleAssociatedProduct" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleAssociatedProduct %>"></asp:Localize>
            </span>
            <br />
            <span>
                <asp:ListBox ID="AssociatedProducts" runat="server" AppendDataBoundItems="False" SelectionMode="Multiple" Width="150px"
                    Height="150px"></asp:ListBox>
                <br />
            </span>
        </div>
        <div class="LeftFloat" style="width: 50px; padding-top: 50px;" align="left">
            <asp:ImageButton ImageUrl="~/Themes/images/403-up.gif" ID="MoveUp" runat="server"
                OnClick="MoveUp_OnClick" />
            <br />
            <br />
            <asp:ImageButton ImageUrl="~/Themes/images/402-down.gif" ID="MoveDown" runat="server"
                OnClick="MoveDown_OnClick" />
        </div>
    </div>
</div>
<div class="ClearBoth"></div>
<br />
<zn:Button runat="server" ButtonType="SubmitButton" OnClick="btnSave_OnClick" Text="<%$ Resources:ZnodeAdminResource, ButtonSave %>" ID="btnSave" />
<zn:Button runat="server" ButtonType="CancelButton" OnClick="btnCancel_OnClick" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel %>" ID="btnCancel" />