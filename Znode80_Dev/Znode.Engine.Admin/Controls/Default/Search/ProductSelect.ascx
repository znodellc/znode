﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSelect.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Search.ProductSelect" %>
<%@ Register Src="~/Controls/Default/Search/ProductList.ascx" TagPrefix="uc1" TagName="ProductList" %>
<asp:Panel ID="SearchPanel" DefaultButton="btnSearch" runat="server">
    <table border="0">
        <tr>
            <td>
                <span class="SearchTitle">
                    <asp:Localize runat="server" ID="ProductName" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>"></asp:Localize>
                </span>
            </td>
            <td>
                <span class="SearchTitle">
                    <asp:Localize runat="server" ID="ProductNumber" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleProductNumber %>"></asp:Localize>
                </span>
            </td>
            <td>
                <span class="SearchTitle">
                    <asp:Localize runat="server" ID="ProductSKU" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleSKU %>"></asp:Localize>
                </span>
            </td>
            <td>
                <span class="SearchTitle">
                    <asp:Localize runat="server" ID="Catalog" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleCatalog %>"></asp:Localize>
                </span>
            </td>
        </tr>
        <tr>
            <td>
                <asp:TextBox ID="txtProductName" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtProductNumber" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:TextBox ID="txtSKU" runat="server"></asp:TextBox>
            </td>
            <td>
                <asp:DropDownList ID="ddCatalog" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddCatalog_SelectedIndexChanged">
                    <Items>
                        <asp:ListItem Selected="True" Text="" Value="-1" />
                    </Items>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                <span class="SearchTitle">
                    <asp:Localize runat="server" ID="Brand" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleBrand %>"></asp:Localize>
                </span>
            </td>
            <td>
                <span class="SearchTitle">
                    <asp:Localize runat="server" ID="ProductType" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleProductType %>"></asp:Localize>
                </span>
            </td>
            <td>
                <span class="SearchTitle">
                    <asp:Localize runat="server" ID="ProductCategory" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleProductCategory %>"></asp:Localize>
                </span>
            </td>
            <td>&#160;</td>
        </tr>
        <tr>
            <td>
                <asp:DropDownList ID="ddBrand" runat="server">
                    <Items>
                        <asp:ListItem Text="" Value="-1" Selected="True" />
                    </Items>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddProductType" runat="server">
                    <Items>
                        <asp:ListItem Text="" Value="-1" Selected="True" />
                    </Items>
                </asp:DropDownList>
            </td>
            <td>
                <asp:DropDownList ID="ddProductCategory" runat="server" OnDataBound="ddProductCategory_DataBound">
                    <Items>
                        <asp:ListItem Text="" Value="-1" Selected="True" />
                    </Items>
                </asp:DropDownList>
            </td>
            <td>&#160;</td>
        </tr>
    </table>
</asp:Panel>
<p>
    &nbsp;
</p>

<div>
    <div class="ValueStyle">
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnClear_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonClear %>" CausesValidation="False" ID="btnClear" />
        <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSearch %>" ID="btnSearch" />
        <div class="BoostSaveResults">
            <asp:Label ID="lblSaveResult" runat="server" Visible="true"></asp:Label>
        </div>
    </div>
</div>
<hr />
<asp:Panel ID="Savepanel" DefaultButton="btnSave" runat="server">
    <h4 class="GridTitle">
        <asp:Label ID="lblTitle" runat="server" Visible="false" Text="<%$ Resources:ZnodeAdminResource, GridTitleProductList %>"></asp:Label>
    </h4>
    <uc1:ProductList runat="server" ID="ucProductList" />
    <br />
    <div>
        <asp:Label ID="lblMsg" runat="server" Visible="false" Text="<%$ Resources:ZnodeAdminResource, ErrorNoProducts %>"></asp:Label>
    </div>
    <div class="ValueStyle">
        <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSave_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSave %>" ID="btnSave" Visible="True" />
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel %>" Visible="True" ID="btnCancel" />
    </div>
</asp:Panel>



