﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductList.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Search.ProductList" %>

<asp:GridView ID="gvProductList" CellPadding="0" CssClass="Grid" runat="server" AutoGenerateColumns="False"  
    AllowPaging="True" Width="100%" GridLines="Both" DataKeyNames="ProductID" ViewStateMode="Enabled" OnPageIndexChanging="gvProductList_PageIndexChanging">
    <Columns>
        <asp:BoundField HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleProductID %>" DataField="ProductID" HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="100" HeaderStyle-Width="100" ItemStyle-CssClass ="TextSpace"/>
        <asp:BoundField HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleName %>" DataField="Name" HeaderStyle-HorizontalAlign="Left" />
        <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleGlobalBoostValue %>">
            <ItemStyle HorizontalAlign="Left"></ItemStyle>
            <ItemTemplate>
                <asp:TextBox ID="txtBoostValue" Text='<%# DataBinder.Eval(Container.DataItem, "BoostValue").ToString() %>' 
                    runat="server">
                </asp:TextBox>
                <asp:RangeValidator ID="rvBoostValue" runat="server" Type="Double" MinimumValue="0.00" MaximumValue="1000.00" 
                    ControlToValidate="txtBoostValue" Display="Dynamic" 
                    ErrorMessage="<%$ Resources:ZnodeAdminResource, RangeBoostValue %>">
                </asp:RangeValidator>
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
    <EditRowStyle CssClass="EditRowStyle" />
    <FooterStyle CssClass="FooterStyle" />
    <RowStyle CssClass="RowStyle" />
    <SelectedRowStyle CssClass="SelectedRowStyle" />
    <PagerStyle CssClass="PagerStyle" />
    <HeaderStyle CssClass="HeaderStyle" />
    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
</asp:GridView>