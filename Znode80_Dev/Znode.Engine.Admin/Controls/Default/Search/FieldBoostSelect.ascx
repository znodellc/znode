﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="FieldBoostSelect.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Search.FieldBoostSelect" %>
<%@ Register Src="~/Controls/Default/Search/FieldBoostList.ascx" TagPrefix="uc1" TagName="FieldBoostList" %>
<div class="BoostSaveResults">
    <asp:Label ID="lblSaveResult" runat="server"></asp:Label>
</div>
<uc1:FieldBoostList runat="server" ID="ucFieldBoostList" />
<div>
    <div class="ValueStyle" style="height: 30px; vertical-align: bottom;">
        <br />
        <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSave_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSave %>" ID="btnSave" />
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel %>" ID="btnCancel" />
    </div>
</div>
