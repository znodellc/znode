﻿<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="Znode.Engine.Admin.Controls.Default.AlternateImage" CodeBehind="AlternateImage.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<div>
    <uc1:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
</div>
<div>
    <zn:Button runat="server" ButtonType="EditButton" Width="200px" OnClick="BtnAddImage_click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddNewAlternateImage%>' ID="btnAddImage" />
    <zn:Button runat="server" ButtonType="EditButton" OnClick="BtnAccept_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAccept%>' ID="btnAccept" />
    <zn:Button runat="server" ButtonType="EditButton" OnClick="BtnReject_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonReject%>' ID="btnReject" />
    <br />
    <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
    <asp:Label ID="lblCaption" runat="server" Text="" />
</div>
<div>
    <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
</div>
<div>
    <asp:GridView ID="GridThumb" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
        runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
        CssClass="Grid" Width="100%" GridLines="None" PageSize="10"
        OnRowCommand="GridThumb_RowCommand" OnRowDataBound="GridThumb_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <asp:CheckBox ID="chkSelect" runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus %>' ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <img id="imgStatus" src='<%# GetStatusImageIcon(DataBinder.Eval(Container.DataItem, "ReviewStateID"))%>' runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ProductImageId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, TitleImage %>' HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <img id="SwapImage" alt="" src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Name" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, TitleProductPage %>' HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <img id="Img1" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCategoryPage %>' HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <img id="Img2" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ShowOnCategoryPage").ToString()))%>'
                        runat="server" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton CssClass="Button" ID="EditProductView" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' CommandArgument='<%# Eval("productimageid") %>'
                        CommandName="Edit" runat="Server" />&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:LinkButton ID="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CommandArgument='<%# Eval("productimageid") %>'
                        CommandName="RemoveItem" CssClass="Button" runat="server" OnClientClick="return DeleteConfirmation();" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            <asp:Localize runat="server" ID="NoRecordFound" Text='<%$ Resources:ZnodeAdminResource, RecordNotFoundImage %>'></asp:Localize>
        </EmptyDataTemplate>
        <RowStyle CssClass="RowStyle" />
        <HeaderStyle CssClass="HeaderStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        <FooterStyle CssClass="FooterStyle" />
    </asp:GridView>
</div>

<script language="javascript" type="text/javascript">
    function DeleteConfirmation() {
        return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ConfirmDelete")) %>');
    }
</script>
