<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Controls.Default.Product.ProductTabs" CodeBehind="ProductTabs.ascx.cs" %>

<div id="Tab">
    <ajaxToolKit:TabContainer runat="server" ID="ProductTabstab" CssClass="CustomTabStyle"
        ScrollBars="Vertical" ActiveTabIndex="0">
        <ajaxToolKit:TabPanel ID="pnlFeatures" runat="server">
            <HeaderTemplate>
                <asp:Localize ID="lblFeatures" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextDetails %>"></asp:Localize>
            </HeaderTemplate>
            <ContentTemplate>
                <div class="Features">
                    <asp:Label ID="ProductFeatureDesc" EnableViewState="false" runat="server"></asp:Label>
                </div>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>

        <ajaxToolKit:TabPanel ID="pnlAdditionalInformation" runat="server">
            <HeaderTemplate>
                <asp:Localize ID="lblAdditionalInformation" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextShipping %>"></asp:Localize>
            </HeaderTemplate>
            <ContentTemplate>
                <div class="ShippingInfo">
                    <asp:Label ID="ProductAdditionalInformation" EnableViewState="false" runat="server"></asp:Label>
                </div>
            </ContentTemplate>
        </ajaxToolKit:TabPanel>
    </ajaxToolKit:TabContainer>
</div>
