<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Controls.Default.Product.SwatchImages" CodeBehind="SwatchImages.ascx.cs" %>

<div class="ProductViews">
    <asp:DataList ID="DataListAlternateImages" runat="server" RepeatDirection="horizontal" OnItemDataBound="DataListAlternateImages_ItemDataBound">
        <ItemTemplate>
            <div class="Swatches">
                <img id="imgSwatch" runat="server" alt='<%# GetImageAltTagText(DataBinder.Eval(Container.DataItem, "ImageAltTag"),DataBinder.Eval(Container.DataItem, "Name").ToString()) %>'
                    class="SwatchImage" src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString(), false) %>'
                    title='<%# GetImageName(DataBinder.Eval(Container.DataItem, "Name").ToString(), DataBinder.Eval(Container.DataItem, "ImageFile").ToString()) %>'
                    visible='<%#  ShowImage(DataBinder.Eval(Container.DataItem, "ShowOnCategoryPage")) %>' />
            </div>
        </ItemTemplate>
        <ItemStyle CssClass="ItemStyle" />
    </asp:DataList>

    <div class="Swatches">
        <asp:DataList ID="DataListSwatches" runat="server" RepeatDirection="horizontal" OnItemDataBound="DataListSwatches_ItemDataBound">
            <ItemTemplate>
                <img id="imgSwatch" runat="server" alt='<%# GetImageAltTagText(DataBinder.Eval(Container.DataItem, "ImageAltTag").ToString(),DataBinder.Eval(Container.DataItem, "Name").ToString()) %>'
                    class="SwatchImage"
                    src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString(), true) %>'
                    title='<%# GetImageName(DataBinder.Eval(Container.DataItem, "Name").ToString(), DataBinder.Eval(Container.DataItem, "ImageFile").ToString()) %>'
                    visible='<%#  ShowImage(DataBinder.Eval(Container.DataItem, "ShowOnCategoryPage")) %>' />

            </ItemTemplate>
            <ItemStyle CssClass="ItemStyle" />
        </asp:DataList>
    </div>
</div>
