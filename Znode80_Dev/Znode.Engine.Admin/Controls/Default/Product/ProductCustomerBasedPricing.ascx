﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductCustomerBasedPricing.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Product.ProductCustomerBasedPricing" %>

<%@ Register TagPrefix="znode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<div>
    <div>
        <asp:Panel ID="pnlCustomerPricing" runat="server" DefaultButton="btnSearch">
            <h4 class="GridTitle">
                <asp:Localize ID="CustomerSearchTitle" runat="server" Text="<%$ Resources:ZnodeAdminResource, TitleCustomerSearch %>" /></h4>
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Label ID="lbStoreName" Text="<%$ Resources:ZnodeAdminResource, TextStoreName %>" runat="server"></asp:Label></span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlPortal" runat="server">
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Label ID="lblExternalAccountID" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextExternalAccountId %>" /></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtExtAccountID" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Label ID="lblName" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleName %>" /></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Label ID="lblCompanyName" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextCompanyName %>" /></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtComapnyName" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnClearSearch_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonClear %>" CausesValidation="False" ID="btnClearSearch" />
                    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSearch %>" CausesValidation="True" ID="btnSearch" />
                </div>
            </div>
        </asp:Panel>
        <div align="right">
            <zn:Button ID="BtnDownloadToExcel" runat="server" ButtonType="EditButton" Text="<%$ Resources:ZnodeAdminResource, ButtonDownloadtoExcel %>" OnClick="DownloadToExcel_Click" />
        </div>
        <br />

        <h4 class="GridTitle">
            <asp:Label ID="lblGridTitle" runat="server" Text="<%$ Resources:ZnodeAdminResource, GridTitleCustomerBasedPricingCustomerList %>"></asp:Label></h4>
    </div>
    <div>
        <asp:GridView ID="uxCustomerPricingGrid" runat="server" CssClass="Grid" Width="100%" ForeColor="#333333"
            CellPadding="4" DataKeyNames="accountid" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyCustomerBasedPricing %>'
            OnPageIndexChanging="uxCustomerPricingGrid_PageIndexChanging" PageSize="10" AllowPaging="True" GridLines="None" AutoGenerateColumns="False"
            CaptionAlign="Left">
            <Columns>
                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource, TextExternalAccountId %>" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblExternalAccountID" Text='<%# Eval("ExternalAccountNo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource, ColumnFullName %>" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblFullName" runat="Server" Text='<%# Eval("FullName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource, TextCompanyName %>" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblCompanyName" runat="Server" Text='<%# Eval("CompanyName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource, ColumnBasePrice %>" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblBasePrice" runat="Server" Text='<%# Eval("BasePrice", "{0:C}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource, ColumnCustomerNegotiatedPrice %>" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblCustomerNegotiatedPrice" runat="Server" Text='<%# Eval("NegotiatedPrice", "{0:C}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource, TextDiscount %>" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblDiscount" runat="Server" Text='<%# Eval("Discount", "{0:C}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
    </div>
    <asp:Label ID="lblError" runat="server" Text="" CssClass="Error"></asp:Label>

</div>
