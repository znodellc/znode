<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Controls.Default.Product.ProductRelated"
    CodeBehind="ProductRelated.ascx.cs" %>
<%@ Register Src="~/Controls/Default/CustomMessage.ascx" TagName="CustomMessage"
    TagPrefix="uc1" %>
<asp:Panel ID="pnlRelated" runat="server" Visible="true">

    <script language="javascript" type="text/javascript">
        var prm = Sys.WebForms.PageRequestManager.getInstance();
        prm.add_pageLoaded(PageLoadedEventHandler);

        //
        function PageLoadedEventHandler() {
            var crossSellItems;
            window.addEvents({
                'domready': function () {
                    /* thumbnails example , div containers */
                    crossSellItems = new SlideItMoo({
                        overallContainer: 'CrossSellItems_outer',
                        elementScrolled: 'CrossSellItems_inner',
                        thumbsContainer: 'CrossSellItems',
                        itemsVisible: 3,
                        elemsSlide: 1,
                        duration: 500,
                        itemsSelector: '.CrossSellItem',
                        showControls: 1
                    });
                }
            });
        }
    </script>

    <div id="CrossSellItems_outer">
        <div id="CrossSellItems_inner">
            <div id="CrossSellItems">
                <asp:DataList ID="DataListRelated" runat="server" RepeatDirection="Horizontal" RepeatLayout="Flow">
                    <ItemTemplate>
                        <div class="CrossSellItem">
                            <div class="DetailLink">
                                <a href='<%# GetViewProductPageUrl(DataBinder.Eval(Container.DataItem, "ViewProductLink").ToString()) %>'
                                    visible='<%# ShowName %>' runat="server">
                                    <%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a>
                            </div>
                            <div class="Image">
                                <img alt='<%# DataBinder.Eval(Container.DataItem, "ImageAltTag")%>' visible='<%# ShowImage %>'
                                    src='<%# GetImageHttpPathThumbnail(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                                    border="0" runat='server' />
                            </div>
                            <div class="ShortDescription">
                                <asp:Label ID="ShortDescription" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ShortDescription").ToString() %>'
                                    Visible='<%# ShowDescription %>' />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:DataList>
            </div>
        </div>
    </div>
</asp:Panel>
