using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Controls.Default.Product
{
    /// <summary>
    /// Represents the Best Sellers user control class
    /// </summary>
    public partial class BestSellers : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProductList _ProductList;
        private string _Title = string.Empty;
        private ZNodeProfile _Profile = new ZNodeProfile();
        private string BuyImage = "~/themes/" + ZNodeCatalogManager.Theme + "/images/buynow.gif";
        private int _DisplayItem = 6;
        private int zcid = 0;
        private bool _IsQuickWatch = false;
        private bool _ShowReviews = false;
        private bool _ShowDescription = false;
        private bool _ShowAddToCart = false;
        private bool _ShowPrice = false;
        private bool _ShowName = false;
        private bool _ShowImage = false;
        private bool _HasItems = false;
        private ZNodeImage znodeImage = new ZNodeImage();
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Product list
        /// </summary>
        public ZNodeProductList ProductList
        {
            get
            {
                return this._ProductList;
            }

            set
            {
                this._ProductList = value;
            }
        }

        /// <summary>
        /// Gets or sets the title for this control
        /// </summary>
        public string Title
        {
            get
            {
                return this._Title;
            }

            set
            {
                this._Title = value;
            }
        }

        /// <summary>
        /// Gets or sets the number of items to be displayed
        /// </summary>
        public int DisplayItem
        {
            get
            {
                return this._DisplayItem;
            }

            set
            {
                this._DisplayItem = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Name
        /// </summary>
        public bool ShowName
        {
            get
            {
                return this._ShowName;
            }

            set
            {
                this._ShowName = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Reviews
        /// </summary>
        public bool ShowReviews
        {
            get
            {
                return this._ShowReviews;
            }

            set
            {
                this._ShowReviews = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Image
        /// </summary>
        public bool ShowImage
        {
            get
            {
                return this._ShowImage;
            }

            set
            {
                this._ShowImage = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Description
        /// </summary>
        public bool ShowDescription
        {
            get
            {
                return this._ShowDescription;
            }

            set
            {
                this._ShowDescription = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show Price
        /// </summary>
        public bool ShowPrice
        {
            get
            {
                return this._ShowPrice;
            }

            set
            {
                this._ShowPrice = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to Show AddToCart
        /// </summary>
        public bool ShowAddToCart
        {
            get
            {
                return this._ShowAddToCart;
            }

            set
            {
                this._ShowAddToCart = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsQuickWatch enabled or not
        /// </summary>
        public bool IsQuickwatch
        {
            get
            {
                return this._IsQuickWatch;
            }

            set
            {
                this._IsQuickWatch = value;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether Has Items enabled or not
        /// </summary>
        public bool HasItems
        {
            get
            {
                return this._HasItems;
            }

            set
            {
                this._HasItems = value;
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Get the thumbnail image relative path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the thumbnail image relative path.</returns>
        public string GetImageHttpPathThumbnail(string imageFileName)
        {
            return znodeImage.GetRelativeImageUrl(ZNodeConfigManager.SiteConfig.MaxCatalogItemThumbnailWidth, imageFileName, false);
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // get categoryId from query string
            if (Request.Params["zcid"] != null)
            {
                this.zcid = int.Parse(Request.Params["zcid"]);
            }
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Get View Product Page Url
        /// </summary>
        /// <param name="link">The value of link</param>
        /// <returns>Returns the View Product Page Url</returns>
        protected string GetViewProductPageUrl(string link)
        {
            if (this._IsQuickWatch)
            {
                return "javascript:self.parent.location ='" + ResolveUrl(link) + "';";
            }

            return link;
        }

        /// <summary>
        /// Check For Call For Pricing Method
        /// </summary>
        /// <param name="fieldValue">The field value</param>
        /// <returns>Returns the Call for Pricing message</returns>
        private string CheckForCallForPricing(object fieldValue)
        {
            bool Status = bool.Parse(fieldValue.ToString());

            if (Status || !this._Profile.ShowPrice)
            {
                return ZNodeCatalogManager.MessageConfig.GetMessage("ProductCallForPricing");
            }
            else
            {
                return string.Empty;
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind display based on a product list
        /// </summary>
        private void BindProducts()
        {
            this._ProductList = ZNodeProductList.GetBestSellers(ZNodeCatalogManager.CatalogConfig.CatalogID, this.DisplayItem, this.zcid, ZNodeConfigManager.SiteConfig.PortalID);

            if (this._ProductList != null)
            {
                this._HasItems = this._ProductList.ZNodeProductCollection.Count > 0;

                if (this._ProductList.ZNodeProductCollection.Count == 0)
                {
                    pnlBestSellerList.Visible = false;
                }
                else
                {
                    pnlBestSellerList.Visible = true;
                    this._HasItems = true;
                }

                DataListBestSeller.DataSource = this._ProductList.ZNodeProductCollection;
                DataListBestSeller.DataBind();

                // Disable view state for data list item
                foreach (DataListItem item in DataListBestSeller.Items)
                {
                    item.EnableViewState = false;
                }
            }
        }
        #endregion
    }
}
