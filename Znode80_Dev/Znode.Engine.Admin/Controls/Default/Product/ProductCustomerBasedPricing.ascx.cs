﻿using System;
using System.Data;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Controls.Default.Product
{
    /// <summary>
    /// Represents the admin Znode.Engine.Admin.Controls.Default.Product
    /// </summary>
    public partial class ProductCustomerBasedPricing : System.Web.UI.UserControl
    {
        #region Private and Public Variables
        private static bool _checkSearch = false;
        private string _portalIds = string.Empty;
        private ZNodeEncryption encrypt = new ZNodeEncryption();
        #endregion

        #region Private and Public Properties
        /// <summary>
        /// Gets or sets a value for RoleName
        /// </summary>
        public string RoleName { get; set; }

        public int ItemID { get; set; }
        #endregion

        #region Page Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Roles.IsUserInRole("FRANCHISE"))
            {
                if (Request.QueryString["itemid"] != null)
                {
                    this.ItemID = int.Parse(this.encrypt.DecryptData(Request.Params["itemid"].ToString()));
                }
            }
            else
            {
                if (Request.QueryString["itemid"] != null)
                {
                    this.ItemID = int.Parse(Request.Params["itemid"].ToString());
                }
            }


            btnClearSearch.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_clear_highlight.gif") + "'");
            btnClearSearch.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_clear.gif") + "'");
            btnSearch.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_search_highlight.gif") + "'");
            btnSearch.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_search.gif") + "'");
            // Get Portals
            this._portalIds = UserStoreAccess.GetAvailablePortals;

            // Hide store name dropdown control for Franchise admin
            if (RoleName == "FRANCHISE")
            {
                lbStoreName.Visible = false;
                ddlPortal.Visible = false;
            }

            if (!Page.IsPostBack)
            {
                ddlPortal.Enabled = Roles.IsUserInRole("ADMIN");

                this.BindPortal();

                this.BindSearchCustomer();

                if (Session["CustomerList"] != null)
                {
                    Session.Remove("CustomerList");
                }

                _checkSearch = false;
                if (this._portalIds.Length == 0)
                {
                    lblError.Text = GetGlobalResourceObject(@"ZnodeAdminResource", "ErrorNeedAdministratorPermission").ToString();
                    BtnDownloadToExcel.Enabled = false;
                    uxCustomerPricingGrid.Visible = false;
                    return;
                }
            }
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchCustomer();
        }

        /// <summary>
        /// Clear Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            // Clear Search and Redirect to same page
            txtExtAccountID.Text = string.Empty;
            txtName.Text = string.Empty;
            txtComapnyName.Text = string.Empty;
            ddlPortal.SelectedIndex = 0;
            this.BindSearchCustomer();
        }

        /// <summary>
        /// Download Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DownloadToExcel_Click(object sender, EventArgs e)
        {
            var _DataDownloadAdmin = new DataDownloadAdmin();
            var HelperAccess = new ProductHelper();
            var _dataset = new DataSet();

            _dataset = HelperAccess.SearchCustomerBasedPricingByProductID(int.Parse(ddlPortal.SelectedValue), txtExtAccountID.Text.Trim(),
                txtName.Text.Trim(),
                txtComapnyName.Text.Trim(),
                this.ItemID
                );

            var StrData = _DataDownloadAdmin.Export(_dataset, true);

            byte[] data = ASCIIEncoding.ASCII.GetBytes(StrData);

            // Release Resources
            _dataset.Dispose();

            Response.Clear();

            // Set as Excel as the primary format
            Response.AddHeader("Content-Type", "application/Excel");

            Response.AddHeader("Content-Disposition", "attachment;filename=Customer.csv");
            Response.ContentType = "application/vnd.xls";

            Response.BinaryWrite(data);

            Response.End();
        }

        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void uxCustomerPricingGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxCustomerPricingGrid.PageIndex = e.NewPageIndex;

            if (_checkSearch)
            {
                if (Session["CustomerList"] != null)
                {
                    var dataset = Session["CustomerList"] as DataSet;

                    uxCustomerPricingGrid.DataSource = dataset;
                    uxCustomerPricingGrid.DataBind();

                    // Release resources
                    dataset.Dispose();
                }
            }
            else
            {
                this.BindSearchCustomer();
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Return DataSet for a given Input
        /// </summary>
        private void BindSearchCustomer()
        {
            // Create Instance for Customer HElper class
            var HelperAccess = new ProductHelper();
            var Dataset = HelperAccess.SearchCustomerBasedPricingByProductID(int.Parse(ddlPortal.SelectedValue), txtExtAccountID.Text.Trim(),
                txtName.Text.Trim(),
                txtComapnyName.Text.Trim(),
                this.ItemID
                );

            Session["CustomerList"] = Dataset;
            if (Dataset.Tables[0].Rows.Count > 0)
            {
                _checkSearch = true;
                BtnDownloadToExcel.Enabled = true;
            }
            else
            {
                BtnDownloadToExcel.Enabled = false;
            }

            uxCustomerPricingGrid.DataSource = Dataset.Tables[0];
            uxCustomerPricingGrid.DataBind();
        }

        /// <summary>
        /// Bind Portals
        /// </summary>
        private void BindPortal()
        {
            var portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            var profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
                {
                if (profiles.StoreAccess != string.Empty)
                {
                    portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
                }
            }
            else
            {
                Portal po = new Portal();
                po.StoreName = GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAllStores").ToString().ToUpper();
                portals.Insert(0, po);
            }

            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();
        }

        #endregion
    }
}