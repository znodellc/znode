﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.ShoppingCart;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;

namespace  Znode.Engine.Admin.Controls.Default.Product
{
    /// <summary>
    /// Represents the Catalog Image user control class
    /// </summary>
    public partial class CatalogImage : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct product;
        private int ProductId = 0;
        #endregion

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this.ProductId = int.Parse(Request.Params["zpid"]);
            }
            else if (Request.Params["itemid"] != null)
            {
                this.ProductId = int.Parse(Request.Params["itemid"]);
            }
            else if (Session["itemid"] != null)
            {
                this.ProductId = int.Parse(Session["itemid"].ToString());
            }
            else
            {
                return;
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this.product = (ZNodeProduct)HttpContext.Current.Items["Product"];

            this.Bind();
        }

        /// <summary>
        /// Bind Method
        /// </summary>
        protected void Bind()
        {
            if (this.product != null)
            {
                string element = "<div class=\"CatalogImageReview_element\">{0}</div>";

                uxPlaceHolder.Controls.Add(ParseControl(string.Format(element, "<img border='0'  EnableViewState='false' ID='CatalogItemReviewImage' alt='" + this.product.ImageAltTag + "' src=\"" + this.GetImagePath(this.product.ImageFile) + "\" />")));

                ZNode.Libraries.DataAccess.Service.ProductImageService service = new ZNode.Libraries.DataAccess.Service.ProductImageService();

                // AlternateImages
                TList<ProductImage> productImages = service.GetByProductIDActiveIndProductImageTypeID(this.product.ProductID, true, 1);

                productImages.Sort("DisplayOrder");
                productImages.Filter = "ReviewStateId = 20";

                foreach (ProductImage productImage in productImages)
                {
                    uxPlaceHolder.Controls.Add(ParseControl(string.Format(element, "<img border='0' EnableViewState='false' alt='" + this.GetImageAltTag(productImage.ImageAltTag, this.product.Name) + "' src=\"" + this.GetImagePath(productImage.ImageFile) + "\" />")));
                }
            }
        }

        /// <summary>
        /// Get the path to the product image
        /// </summary>
        /// <param name="ImageFile">Returns the Image File</param>
        /// <returns>Returns the Image Path</returns>
        protected string GetImagePath(string ImageFile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return ResolveClientUrl(znodeImage.GetImageHttpPathMedium(ImageFile));
        }

        /// <summary>
        /// Get product image alternative text
        /// </summary>
        /// <param name="AltTagText">The Alternate Facet text</param>
        /// <param name="ProductName">The Product Name</param>
        /// <returns>Returns product image alternative text</returns>
        protected string GetImageAltTag(string AltTagText, string ProductName)
        {
            if (string.IsNullOrEmpty(AltTagText))
            {
                return ProductName;
            }
            else
            {
                return AltTagText;
            }
        }
    }
}
