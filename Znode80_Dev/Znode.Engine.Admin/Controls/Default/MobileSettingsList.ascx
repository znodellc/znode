﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MobileSettingsList.ascx.cs"
    Inherits="Znode.Engine.Admin.Controls.Default.MobileSettingsList" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<div class="Form">
    <div class="ClearBoth" align="left">
    </div>
    <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
</div>
<br />
<asp:GridView ID="uxGrid" runat="server" PageSize="100" CssClass="Grid" AutoGenerateColumns="False"
    CellPadding="4" GridLines="None" CaptionAlign="Left" Width="100%" AllowSorting="True"
    EmptyDataText="No mobile settings available." OnRowCommand="UxGrid_RowCommand">
    <FooterStyle CssClass="FooterStyle" />
    <RowStyle CssClass="RowStyle" />
    <PagerStyle CssClass="PagerStyle" />
    <HeaderStyle CssClass="HeaderStyle" />
    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
    <Columns>
        <asp:BoundField DataField="MobileTheme" HeaderText="Mobile Theme" HeaderStyle-HorizontalAlign="Left" />
        <asp:TemplateField>
            <ItemTemplate>
                <asp:LinkButton ID="Button" runat="server" Text="Edit Mobile Setting &raquo" CommandName="Manage"
                    CommandArgument='<%#DataBinder.Eval(Container.DataItem,"PortalId") %>' CssClass="actionlink" />
            </ItemTemplate>
        </asp:TemplateField>
    </Columns>
</asp:GridView>

