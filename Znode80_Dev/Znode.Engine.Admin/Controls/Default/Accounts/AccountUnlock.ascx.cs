﻿using System;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;


namespace Znode.Engine.Admin.Controls.Default.Accounts
{
    public partial class AccountUnlock : System.Web.UI.UserControl
    {
        #region Private Variables
        private int ItemId = 0;
        private AccountAdmin accountAdmin = new AccountAdmin();
        private string pageFrom = string.Empty;
        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            btnCancelBottom.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancelBottom.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel.gif") + "'");

            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            if (Request.Params["pagefrom"] != null)
            {
                this.pageFrom = Request.Params["pagefrom"].ToString();
            }
            if (!Page.IsPostBack)
            {
                Account _account = this.accountAdmin.GetByAccountID(this.ItemId);

                MembershipUser _user = Membership.GetUser(_account.UserID.Value);
                lblAccountName.Text = _user.UserName;
                string roleList = string.Empty;

                // Get roles for this User account
                string[] roles = Roles.GetRolesForUser(_user.UserName);

                foreach (string Role in roles)
                {
                    roleList += Role + "<br>";
                }

                string rolename = roleList;

                // Hide the Delete button if a NonAdmin user has entered this page
                if (!Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "ADMIN"))
                {
                    if (Roles.IsUserInRole(_user.UserName, "ADMIN"))
                    {
                        btnUnlock.Visible = false;
                    }
                    else if (Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "CUSTOMER SERVICE REP"))
                    {
                        if (rolename == Convert.ToString("USER<br>") || rolename == Convert.ToString(string.Empty))
                        {
                            btnUnlock.Visible = true;
                        }
                        else
                        {
                            btnUnlock.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Unlock User button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnUnlockUSer_Click(object sender, EventArgs e)
        {
            Account _account = this.accountAdmin.GetByAccountID(this.ItemId);

            bool status = false;
            lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidUnlockAccountFailed").ToString();

            if (_account != null)
            {
                if (_account.UserID.HasValue)
                {
                    // Get user by Unique GUID - User Id
                    MembershipUser user = Membership.GetUser(_account.UserID.Value);
                    if (user != null)
                    {
                        // If the user is locked out then unlock them so they will be able to log in.
                        status = user.UnlockUser();

                        // Update user
                        user.IsApproved = true;
                        Membership.UpdateUser(user);
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.AccountUnLock, user.UserName);

                        this.SendAccountActivatedNotificationMail(this.ItemId);
                    }
                }
            }

            if (status)
            {
                if (pageFrom.ToLower() == "admin")
                {
                    Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/Default.aspx");
                }
                else if (pageFrom.ToLower() == "franchise")
                {
                    Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/Default.aspx");
                }
                else if (pageFrom.ToLower() == "vendor")
                {
                    Response.Redirect("~/Secure/Vendors/VendorAccounts/Default.aspx");
                }
                else
                {
                    Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/Default.aspx");
                }
            }
        }

        /// <summary>
        /// Cancel Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (pageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/Default.aspx");
            }
            else if (pageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/Default.aspx");
            }
            else if (pageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/Secure/Vendors/VendorAccounts/Default.aspx");
            }
            else
            {
                Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/Default.aspx");
            }
        }
        #endregion

        #region Helper Method
        /// <summary>
        /// Send the account activated notification mail to the customer.
        /// </summary>
        /// <param name="accountId">The value of Accoount Id</param>
        protected void SendAccountActivatedNotificationMail(int accountId)
        {
            AccountAdmin accountAdmin = new AccountAdmin();
            Account account = accountAdmin.GetByAccountID(accountId);

            MembershipUser user = Membership.GetUser(account.UserID.Value);
            System.Web.Profile.ProfileBase profile = System.Web.Profile.ProfileBase.Create(user.UserName, true);
            string stores = (string)profile.GetPropertyValue("StoreAccess");

            // Get the accounts portal Id
            int portalId = 1;
            if (stores == "AllStores" || stores == string.Empty)
            {
                portalId = 1;
            }
            else
            {
                portalId = Convert.ToInt32(stores.Split(',')[0]);
            }

            string storeName = string.Empty;
            string customerServicePhoneNo = string.Empty;
            PortalAdmin portalAdmin = new PortalAdmin();
            Portal portal = portalAdmin.GetByPortalId(portalId);

            if (portal != null)
            {
                // If Franchise account then use Franchise account company name as store name.
                if (Roles.IsUserInRole(user.UserName, "FRANCHISE"))
                {
                    ZNode.Libraries.DataAccess.Entities.Address address = accountAdmin.GetDefaultBillingAddress(account.AccountID);
                    if (address == null)
                    {
                        address = new ZNode.Libraries.DataAccess.Entities.Address();
                    }

                    if (!string.IsNullOrEmpty(portal.CompanyName))
                    {
                        storeName = portal.CompanyName;
                    }
                    else if (!string.IsNullOrEmpty(address.FirstName) && !string.IsNullOrEmpty(address.LastName))
                    {
                        storeName = String.Format("{0} {1}", address.FirstName, address.LastName);
                    }
                    else
                    {
                        storeName = user.UserName;
                    }
                }
                else
                {
                    storeName = portal.StoreName;
                }

                customerServicePhoneNo = portal.CustomerServicePhoneNumber;
            }

            ZNodeEncryption encrypt = new ZNodeEncryption();
            string smtpServer = string.Empty;
            string smtpUsername = string.Empty;
            string smtpPassword = string.Empty;

            // To get the SMTP settings based on PortalId
            ZNodeConfigManager.AliasSiteConfig(portalId);

            if (!string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SMTPServer) && !string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SMTPUserName) && !string.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SMTPPassword))
            {
                smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
                smtpUsername = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.SMTPUserName);
                smtpPassword = encrypt.DecryptData(ZNodeConfigManager.SiteConfig.SMTPPassword);

                if (string.IsNullOrEmpty(account.Email))
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidUnlockAccountEmail").ToString();
                    return;
                }

                MailMessage mailMessage = new MailMessage(smtpUsername, account.Email);
                mailMessage.IsBodyHtml = true;
                mailMessage.Subject = string.Format("{0} - Account Activation", storeName);

                string body = string.Empty;
                string templateFile = Path.Combine(HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath), "AccountActivatiedNotification.htm");

                // Check whether the template file exist or not.
                if (!File.Exists(templateFile))
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidUnlockAccountTemplateFile").ToString();
                    return;
                }

                using (StreamReader rw = new StreamReader(templateFile))
                {
                    body = rw.ReadToEnd();
                   
                }

                ZNode.Libraries.DataAccess.Entities.Address address = accountAdmin.GetDefaultBillingAddress(accountId);
                string billingFirstName = string.Empty;
                if (!string.IsNullOrEmpty(address.FirstName))
                {
                    billingFirstName = address.FirstName;
                }
                else
                {
                    billingFirstName = user.UserName;
                }


                Regex regularExpName = new Regex("#BillingFirstName#", RegexOptions.IgnoreCase);
                body = regularExpName.Replace(body, billingFirstName);


                Regex regularExpCustomerServiceNo = new Regex("#CUSTOMERSUPPORT#", RegexOptions.IgnoreCase);
                body = regularExpCustomerServiceNo.Replace(body, customerServicePhoneNo);
                mailMessage.Body = body;

                try
                {
                    // Create mail client and send email
                    System.Net.Mail.SmtpClient emailClient = new System.Net.Mail.SmtpClient();
                    emailClient.Host = smtpServer;

                    emailClient.Credentials = new NetworkCredential(smtpUsername, smtpPassword);

                    // Send mail
                    emailClient.Send(mailMessage);
                }
                catch (Exception ex)
                {
                    string msg = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ValidUnlockAccountException").ToString(), ex.Message);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(msg);
                }
            }
            else
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ValidUnlockLogException").ToString(), storeName));
            }
        }
        #endregion
    }
}