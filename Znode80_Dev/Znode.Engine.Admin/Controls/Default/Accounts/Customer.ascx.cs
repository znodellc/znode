﻿using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Controls.Default.Accounts
{
    public partial class Customer : System.Web.UI.UserControl
    {

        #region Protected Member Variables
        private static bool CheckSearch = false;
        private string PortalIds = string.Empty;
        private string _RoleName = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value for RoleName
        /// </summary>
        public string RoleName
        {
            get
            {
                return this._RoleName;
            }

            set
            {
                this._RoleName = value;
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Gets the Name of the Role for this ProfileID
        /// </summary>
        /// <param name="userId">The value of UserId</param>
        /// <returns>Returns the Role Name</returns>
        public string GetRoleName(object userId)
        {
            if (userId != null)
            {
                string roleList = string.Empty;

                if (userId.ToString().Length > 0)
                {
                    MembershipUser _user = Membership.GetUser(userId);

                    if (_user == null)
                    {
                        return string.Empty;
                    }

                    // Get roles for this User account
                    string[] roles = Roles.GetRolesForUser(_user.UserName);

                    foreach (string Role in roles)
                    {
                        roleList += Role + ",";
                    }
                }

                return roleList;
            }
            else
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// Gets the Name of the Role for this ProfileID
        /// </summary>
        /// <param name="userId">The value of UserId</param>
        /// <returns>Returns the Role Name</returns>
        public string GetActiveInd(object userId)
        {
            if (userId != null)
            {
                string roleList = string.Empty;

                if (userId.ToString().Length > 0)
                {
                    MembershipUser _user = Membership.GetUser(userId);

                    if (_user == null)
                    {
                        return string.Empty;
                    }
                    if (!_user.IsApproved || _user.IsLockedOut)
                        return "ENABLE";
                    else
                        return "DISABLE";
                }

                return string.Empty;
            }
            else
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the Name of the Profile for this ProfileID
        /// </summary>
        /// <param name="profileID">The value of profileID</param>
        /// <returns>Returns the Profile Name</returns>
        public string GetProfileName(object profileID)
        {
            ProfileAdmin AdminAccess = new ProfileAdmin();
            if (profileID == null)
            {
                return "All Profile";
            }
            else
            {
                ZNode.Libraries.DataAccess.Entities.Profile profile = AdminAccess.GetByProfileID(int.Parse(profileID.ToString()));

                if (profile != null)
                {
                    return profile.Name;
                }
            }

            return string.Empty;
        }
        #endregion

        #region Page Load Event

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            btnClearSearch.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_clear_highlight.gif") + "'");
            btnClearSearch.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_clear.gif") + "'");
            btnSearch.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_search_highlight.gif") + "'");
            btnSearch.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_search.gif") + "'");
            // Get Portals
            this.PortalIds = UserStoreAccess.GetAvailablePortals;

            // Hide store name dropdown control for Franchise admin
            if (RoleName == "FRANCHISE")
            {
                lbStoreName.Visible = false;
                ddlPortal.Visible = false;
            }

            if (!Page.IsPostBack)
            {
                if (Roles.IsUserInRole("ADMIN"))
                {
                    ddlPortal.Enabled = true;
                }
                else
                {
                    ddlPortal.Enabled = false;
                }

                this.BindData();
                this.BindPortal();
                this.BindTrackingStatus();
                this.RoleValidation();

                if (!Page.IsPostBack)
                {
                    this.BindSearchCustomer();
                }
                if (Session["CustomerPricingList"] != null)
                {
                    Session.Remove("CustomerPricingList");
                }

                if (Session["ContactList"] != null)
                {
                    Session.Remove("ContactList");
                }

                CheckSearch = false;
                if (this.PortalIds.Length == 0)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccount").ToString();
                    //AddContact.Enabled = false;
                    download.Enabled = false;
                    uxGrid.Visible = false;
                    return;
                }

            }
        }

        #endregion

        #region Protected Methods and Events

        /// <summary>
        /// Portal Dropdown control Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlPortal_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProfileAdmin settingsAdmin = new ProfileAdmin();

            // Get profiles
            TList<ZNode.Libraries.DataAccess.Entities.Profile> profileList;

            // All Profiles based on PortalId
            DataSet ds = new DataSet();
            if (Convert.ToInt32(ddlPortal.SelectedValue) != 0)
            {
                ds = settingsAdmin.GetAssociatedProfilesByPortalID(Convert.ToInt32(ddlPortal.SelectedValue));
                lstProfile.DataSource = UserStoreAccess.CheckProfileAccess(ds.Tables[0]);
            }
            else
            {
                profileList = settingsAdmin.GetAll();
                lstProfile.DataSource = UserStoreAccess.CheckProfileAccess(profileList);
            }

            lstProfile.DataTextField = "Name";
            lstProfile.DataValueField = "ProfileID";
            lstProfile.DataBind();

            // Insert New Item 
            ListItem item = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), "0");
            lstProfile.Items.Insert(0, item);

            // Set Default as "ALL"
            lstProfile.SelectedIndex = 0;
        }

        /// <summary>
        /// Concate firstname and lastname 
        /// </summary>
        /// <param name="firstName">The value of firstname</param>
        /// <param name="lastName">The value of lastname</param>
        /// <returns>Returns the concatenated value</returns>
        protected string ConcatName(object firstName, object lastName)
        {
            string fullName = string.Concat(firstName, " ", lastName);
            return fullName;
        }


        /// <summary>
        /// Concate firstname and lastname 
        /// </summary>
        /// <param name="firstName">The value of firstname</param>
        /// <param name="lastName">The value of lastname</param>
        /// <returns>Returns the concatenated value</returns>
        protected string GetNavigateURL(object accountId)
        {
            string url = string.Empty;
            if (RoleName.ToLower() == "admin")
            {
                url = "~/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + accountId + "&pagefrom=admin";
            }
            else if (RoleName.ToLower() == "franchise")
            {
                url = "~/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + accountId + "&pagefrom=franchise";
            }
            else if (RoleName.ToLower() == "vendor")
            {
                url = "~/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + accountId + "&pagefrom=vendor";
            }
            else
            {
                url = "~/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + accountId + "&pagefrom=customer";
            }
            return url;
        }


        /// <summary>
        /// Hide Delete Button Method
        /// </summary>
        /// <param name="userId">The value of UserId</param>
        /// <returns>Returns bool value true or false to show or hide delete button</returns>
        protected bool HideDeleteButton(object userId)
        {
            //if (userId != null)
            //{
            //    if (userId.ToString().Length > 0)
            //    {
            //        MembershipUser _user = Membership.GetUser(userId);

            //        if (_user == null)
            //        {
            //            return true;
            //        }

            //        if (_user.UserName.ToLower().Equals(HttpContext.Current.User.Identity.Name))
            //        {
            //            return false;
            //        }

            //        string roleList = string.Empty;

            //        // Get roles for this User account
            //        string[] roles = Roles.GetRolesForUser(_user.UserName);

            //        foreach (string Role in roles)
            //        {
            //            roleList += Role + ",";
            //        }

            //        string rolename = roleList;

            //        if (Roles.IsUserInRole("CUSTOMER SERVICE REP"))
            //        {
            //            if (rolename == Convert.ToString("USER,") || rolename == Convert.ToString(string.Empty))
            //            {
            //                return true;
            //            }
            //            else
            //            {
            //                return false;
            //            }
            //        }
            //        else if (roleList.Contains("ADMIN"))
            //        {
            //            if (_user.UserName.Equals(HttpContext.Current.User.Identity.Name))
            //            {
            //                return false;
            //            }
            //            else if (Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "ADMIN"))
            //            {
            //                return true;
            //            }
            //            else
            //            {
            //                return false;
            //            }
            //        }
            //    }
            //}

            return true;
        }

        /// <summary>
        /// Hides the disable or enable button
        /// </summary>
        /// <param name="userId">The value of userId</param>
        /// <param name="isEnableButton">The value of IsEnableButton</param>
        /// <returns>Returns true or false to lock or unlock the button</returns>
        protected bool HideLockUnlockButton(object userId, bool isEnableButton)
        {
            bool status = false;

            if (userId != null)
            {
                if (userId.ToString().Length > 0)
                {
                    bool isAdmin = Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "ADMIN");

                    if (isAdmin
                        || Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "CUSTOMER SERVICE REP"))
                    {
                        MembershipUser _user = Membership.GetUser(userId);

                        // Hide the button if the account is not found.
                        if (_user != null)
                        {
                            // Hide the button if I am editing my own account so I can't lock myself out.
                            if (!_user.UserName.ToLower().Equals(HttpContext.Current.User.Identity.Name))
                            {
                                if (isEnableButton)
                                {
                                    // We are looking at the enable button, Enable it if the user is locked out.
                                    status = _user.IsLockedOut || !_user.IsApproved;
                                }
                                else
                                {
                                    // The Unlock button must always be the oposite of the Enable button.
                                    status = !_user.IsLockedOut || !_user.IsApproved;
                                }
                            }

                            string roleList = string.Empty;

                            // Get roles for this User account
                            string[] roles = Roles.GetRolesForUser(_user.UserName);

                            foreach (string Role in roles)
                            {
                                roleList += Role + "<br>";
                            }

                            string rolename = roleList;

                            if (Roles.IsUserInRole("CUSTOMER SERVICE REP"))
                            {
                                if (rolename == Convert.ToString("USER<br>") || rolename == Convert.ToString(string.Empty))
                                {
                                    status = true;
                                }
                                else
                                {
                                    status = false;
                                }
                            }

                            // Don't let non-Admins disable Admin Accounts.
                            if (!isAdmin && Roles.IsUserInRole(_user.UserName, "ADMIN"))
                            {
                                status = false;
                            }
                        }
                    }
                }
            }

            return status;
        }

        /// <summary>
        /// Hide the Edit button for admin accounts if the user is not an Admin
        /// </summary>
        /// <param name="userId">The value of userId</param>
        /// <returns>Returns bool value to show or hide Edit Button</returns>
        protected bool HideEditButton(object userId)
        {
            if (userId != null)
            {
                if (userId.ToString().Length > 0)
                {
                    MembershipUser _user = Membership.GetUser(userId);

                    if (_user == null)
                    {
                        return true;
                    }

                    string roleList = string.Empty;

                    // Get roles for this User account
                    string[] roles = Roles.GetRolesForUser(_user.UserName);

                    foreach (string Role in roles)
                    {
                        roleList += Role + "<br>";
                    }

                    string rolename = roleList;

                    if (!Roles.IsUserInRole("ADMIN"))
                    {
                        if (Roles.IsUserInRole("CUSTOMER SERVICE REP"))
                        {
                            if (rolename == Convert.ToString("USER<br>") || rolename == Convert.ToString(string.Empty))
                            {
                                return true;
                            }
                            else
                            {
                                return false;
                            }
                        }
                        else if (rolename == Convert.ToString("ADMIN<br>"))
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchCustomer();
        }

        /// <summary>
        /// Clear Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            // Clear Search and Redirect to same page

            if (RoleName.ToLower() == "admin")
            {
                Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/Default.aspx");
            }
            else if (RoleName.ToLower() == "franchise")
            {
                Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/Default.aspx");
            }
            else if (RoleName.ToLower() == "vendor")
            {
                Response.Redirect("~/Secure/Vendors/VendorAccounts/Default.aspx");
            }
            else
            {
                Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/Default.aspx");
            }
        }

        /// <summary>
        /// Download Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Download_Click(object sender, EventArgs e)
        {
            DataDownloadAdmin _DataDownloadAdmin = new DataDownloadAdmin();
            CustomerHelper HelperAccess = new CustomerHelper();
            DataSet _dataset = new DataSet();

	        string filename = string.Empty;

            // Check for Search enabled
            if (CheckSearch)
            {
                if (Session["ContactList"] != null)
                {
                    _dataset = Session["ContactList"] as DataSet;
                }
            }
            else
            {
                if (Roles.IsUserInRole("ADMIN"))
                {
					 filename = "StoreAdmins.csv";
                    _dataset = HelperAccess.SearchCustomer(String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, "0", String.Empty, ddlPortal.SelectedValue.ToString(), RoleName);
			
                    if (RoleName == string.Empty)
                    {
                        int count = _dataset.Tables[0].Rows.Count;
                        for (int index = 0; index < count; index++)
                        {
                            string rolename = _dataset.Tables[0].Rows[index]["ROLENAME"].ToString();
                            if (rolename.ToLower() == "admin" || rolename.ToLower() == "franchise" || rolename.ToLower() == "vendor")
                            {
                                _dataset.Tables[0].Rows[index].Delete();
                            }
							
                        }
                    }
                }
                else
                {
                    _dataset = HelperAccess.SearchCustomer(String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, String.Empty, "0", String.Empty, ZNodeConfigManager.SiteConfig.PortalID.ToString(), RoleName);
                }
            }


		
			if (RoleName.ToLower() == "franchise")
			{
				filename = "FranchiseAdmins.csv";
			}
			else if (RoleName.ToLower() == "vendor")
			{
				filename = "Vendors.csv";
			}
		

	        string StrData = _DataDownloadAdmin.Export(_dataset, true);

            byte[] data = ASCIIEncoding.ASCII.GetBytes(StrData);

            // Release Resources
            _dataset.Dispose();

            Response.Clear();

            // Set as Excel as the primary format
            Response.AddHeader("Content-Type", "application/Excel");
			Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
            Response.ContentType = "application/vnd.xls";

            Response.BinaryWrite(data);

            Response.End();
        }


        /// <summary>
        /// DdlRoles Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlRoles_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindSearchCustomer();
        }


        /// <summary>
        /// RowDataBound Event for the Gridview Enable and Disable button.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void uxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton viewButton = (LinkButton)e.Row.Cells[5].FindControl("UnlockCustomer");

                if (RoleName.ToLower() == "admin" || RoleName.ToLower() == "franchise" || RoleName.ToLower() == "vendor")
                {
                    viewButton.Visible = true;
                }
                else
                {
                    viewButton.Visible = false;
                }
            }
        }
        #endregion

        #region Grid Events

        /// <summary>
        /// Grid Row Command Event 
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
                // Do nothing
            }
            else
            {
                // Get the Account id  stored in the CommandArgument
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Edit")
                {
                    if (RoleName.ToLower() == "admin")
                    {
                        Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + Id + "&pagefrom=admin");
                    }
                    else if (RoleName.ToLower() == "franchise")
                    {
                        Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + Id + "&pagefrom=franchise");
                    }
                    else if (RoleName.ToLower() == "vendor")
                    {
                        Response.Redirect("~/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + Id + "&pagefrom=vendor");
                    }
                    else
                    {
                        Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + Id + "&pagefrom=customer");
                    }
                }

                if (e.CommandName == "Delete")
                {
                    if (RoleName.ToLower() == "admin")
                    {
                        Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/Delete.aspx?itemid=" + Id + "&pagefrom=admin");
                    }
                    else if (RoleName.ToLower() == "franchise")
                    {
                        Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/Delete.aspx?itemid=" + Id + "&pagefrom=franchise");
                    }
                    else if (RoleName.ToLower() == "vendor")
                    {
                        Response.Redirect("~/Secure/Vendors/VendorAccounts/Delete.aspx?itemid=" + Id + "&pagefrom=vendor");
                    }
                    else
                    {
                        Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/Delete.aspx?itemid=" + Id + "&pagefrom=customer");
                    }

                }
                else if (e.CommandName == "View")
                {
                    if (RoleName.ToLower() == "admin")
                    {
                        Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + Id + "&pagefrom=admin");
                    }
                    else if (RoleName.ToLower() == "franchise")
                    {
                        Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + Id + "&pagefrom=franchise");
                    }
                    else if (RoleName.ToLower() == "vendor")
                    {
                        Response.Redirect("~/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + Id + "&pagefrom=vendor");
                    }
                    else
                    {
                        Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + Id + "&pagefrom=customer");
                    }
                }
                else if (e.CommandName == "DISABLE")
                {
                    if (RoleName.ToLower() == "admin")
                    {
                        Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/Disable.aspx?itemid=" + Id + "&pagefrom=admin");
                    }
                    else if (RoleName.ToLower() == "franchise")
                    {
                        Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/Disable.aspx?itemid=" + Id + "&pagefrom=franchise");
                    }
                    else if (RoleName.ToLower() == "vendor")
                    {
                        Response.Redirect("~/Secure/Vendors/VendorAccounts/Disable.aspx?itemid=" + Id + "&pagefrom=vendor");
                    }
                    else
                    {
                        Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/Disable.aspx?itemid=" + Id + "&pagefrom=customer");
                    }
                }
                else if (e.CommandName == "ENABLE")
                {
                    if (RoleName.ToLower() == "admin")
                    {
                        Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/Unlock.aspx?itemid=" + Id + "&pagefrom=admin");
                    }
                    else if (RoleName.ToLower() == "franchise")
                    {
                        Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/Unlock.aspx?itemid=" + Id + "&pagefrom=franchise");
                    }
                    else if (RoleName.ToLower() == "vendor")
                    {
                        Response.Redirect("~/Secure/Vendors/VendorAccounts/Unlock.aspx?itemid=" + Id + "&pagefrom=vendor");
                    }
                    else
                    {
                        Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/Unlock.aspx?itemid=" + Id + "&pagefrom=customer");
                    }
                }
            }
        }

        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;

            if (CheckSearch)
            {
                if (Session["ContactList"] != null)
                {
                    DataSet _dataset = Session["ContactList"] as DataSet;

                    uxGrid.DataSource = _dataset;
                    uxGrid.DataBind();

                    // Release resources
                    _dataset.Dispose();
                }
            }
            else
            {
                this.BindSearchCustomer();
            }
        }

        #endregion

        #region Private Methods


        private void RoleValidation()
        {
            if (RoleName.ToLower() == "admin")
            {
                lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTitleSearchStoreAdministrators").ToString();
                lblContactID.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTitleAccountID").ToString();
                lblGridTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "GridTitleAdmin").ToString();
                lblAffiliateStatus.Visible = false;
                lstReferralStatus.Visible = false;
            }
            else if (RoleName.ToLower() == "franchise")
            {
                lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTitleSearchFranchiseAdmins").ToString();
                lblContactID.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTitleAccountID").ToString();
                lblGridTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "GridTitleFranchise").ToString(); 
                lblAffiliateStatus.Visible = false;
                lstReferralStatus.Visible = false;
            }
            else if (RoleName.ToLower() == "vendor")
            {
                lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTitleSearchVendors").ToString();
                lblContactID.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTitleAccountID").ToString();
                lblGridTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "GridTitleVendor").ToString(); 
				lblAffiliateStatus.Visible = false;
				lstReferralStatus.Visible = false;
            }
            else
            {
                lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTitleSearchCustomers").ToString();
                lblContactID.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTitleContactID").ToString();
                lblGridTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "GridTitleCustomer").ToString(); 
            }
        }


        /// <summary>
        /// Bind Profile drop down list
        /// </summary>
        private void BindData()
        {
            ProfileAdmin settingsAdmin = new ProfileAdmin();

            // Get profiles
            TList<ZNode.Libraries.DataAccess.Entities.Profile> profileList;

            // All Profiles
            profileList = settingsAdmin.GetAll();

            lstProfile.DataSource = UserStoreAccess.CheckProfileAccess(profileList);
            lstProfile.DataTextField = "Name";
            lstProfile.DataValueField = "ProfileID";
            lstProfile.DataBind();

            // Insert New Item 
            ListItem item = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), "0");
            lstProfile.Items.Insert(0, item);

            // Set Default as "ALL"
            lstProfile.SelectedIndex = 0;
        }

        /// <summary>
        /// Binds Tracking Status drop-down list
        /// </summary>
        private void BindTrackingStatus()
        {
            Array names = Enum.GetNames(typeof(ZNodeApprovalStatus.ApprovalStatus));
            Array values = Enum.GetValues(typeof(ZNodeApprovalStatus.ApprovalStatus));

            // Clear list items
            lstReferralStatus.Items.Clear();

            // Add default value to the item
            lstReferralStatus.Items.Add(new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), string.Empty));

            for (int i = 0; i < names.Length; i++)
            {
                ListItem listitem = new ListItem(ZNodeApprovalStatus.GetEnumValue(values.GetValue(i)), names.GetValue(i).ToString());
                lstReferralStatus.Items.Add(listitem);
            }
        }

        /// <summary>
        /// Bind List Method
        /// </summary>
        private void BindList()
        {
            DataSet ds = new DataSet();
            AccountAdmin accountAdmin = new AccountAdmin();
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                ds = accountAdmin.GetAccountByPortal(profiles.StoreAccess);
            }
            else
            {
                ds = accountAdmin.GetAllCustomers();
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                uxGrid.DataSource = ds.Tables[0];
                uxGrid.DataBind();
            }
        }

        /// <summary>
        /// Bind Portals
        /// </summary>
        private void BindPortal()
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                if (profiles.StoreAccess != string.Empty)
                {
                    portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
                }
            }
            else
            {
                Portal po = new Portal();
                po.StoreName = "ALL STORES";
                portals.Insert(0, po);
            }

            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();
        }

        /// <summary>
        /// Return DataSet for a given Input
        /// </summary>
        private void BindSearchCustomer()
        {
            string tempRoleName = string.Empty;

            if (RoleName.ToUpper() == "FRANCHISE")
                tempRoleName = RoleName;

            if (RoleName.ToUpper() == "VENDOR")
                tempRoleName = RoleName;

            // Create Instance for Customer HElper class
            CustomerHelper HelperAccess = new CustomerHelper();
            DataSet Dataset = HelperAccess.SearchCustomer(
                txtFName.Text.Trim(),
                txtLName.Text.Trim(),
                txtComapnyNM.Text.Trim(),
                txtLoginName.Text.Trim(),
                txtExternalaccountno.Text.Trim(),
                txtContactID.Text.Trim(),
                txtStartDate.Text.Trim(),
                txtEndDate.Text.Trim(),
                txtPhoneNumber.Text.Trim(),
                txtEmailID.Text.Trim(),
                lstProfile.SelectedValue,
                lstReferralStatus.SelectedValue,
                ddlPortal.SelectedValue,
                tempRoleName);

            if (RoleName.ToLower() == "franchise" || RoleName.ToLower() == "vendor")
            {

                Session["ContactList"] = Dataset;
                if (Dataset.Tables[0].Rows.Count > 0)
                {
                    CheckSearch = true;
                }

                uxGrid.DataSource = Dataset;
                uxGrid.DataBind();


            }
            else if (RoleName.ToUpper() == "ADMIN")
            {

                DataTable dt = Dataset.Tables[0];
                DataView dv = new DataView(dt);
                dv.AllowDelete = true;

                foreach (DataRowView drv in dv)
                {
                    string rolename = string.Empty;
                    rolename = drv["RoleName"].ToString();

                    if (!(rolename.ToUpper() == "ADMIN" || rolename.ToUpper() == "CATALOG EDITOR" || rolename.ToUpper() == "CONTENT EDITOR" || rolename.ToUpper() == "CUSTOMER SERVICE REP" || rolename.ToUpper() == "EXECUTIVE" || rolename.ToUpper() == "ORDER APPROVER" || rolename.ToUpper() == "ORDER ONLY" || rolename.ToUpper() == "REVIEWER" || rolename.ToUpper() == "SEO"))
                    {
                        drv.Delete();
                    }

                }

                uxGrid.DataSource = dv;
                uxGrid.DataBind();
            }
            else
            {

                DataTable dt = Dataset.Tables[0];
                DataView dv = new DataView(dt);
                dv.AllowDelete = true;

                foreach (DataRowView drv in dv)
                {
                    string rolename = string.Empty;
                    rolename = drv["RoleName"].ToString();
                    if (rolename.Length > 0)
                    {
						//Removing this role, to show in Orders/Customers on SiteAdmin
						//|| rolename.ToUpper() == "CUSTOMER SERVICE REP"

                        if (rolename.ToUpper() == "ADMIN" || rolename.ToUpper() == "FRANCHISE" || rolename.ToUpper() == "VENDOR" ||
                            rolename.ToUpper() == "CATALOG EDITOR" || rolename.ToUpper() == "CONTENT EDITOR" || rolename.ToUpper() == "EXECUTIVE" || rolename.ToUpper() == "ORDER APPROVER" || rolename.ToUpper() == "ORDER ONLY" || rolename.ToUpper() == "REVIEWER" || rolename.ToUpper() == "SEO")
                        {
                            drv.Delete();
                        }
                    }
                }

                uxGrid.DataSource = dv;
                uxGrid.DataBind();
                uxGrid.Columns[1].Visible = ZNodeConfigManager.SiteConfig.EnableCustomerPricing.GetValueOrDefault();
            }
        }

        #endregion


    }
}