﻿using System;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Controls.Default.Accounts
{
    public partial class View : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private int _accountId;
        private string _mode = string.Empty;
        private string _pageFrom = string.Empty;
	    private bool? isCustomerPricing;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value for RoleName
        /// </summary>
        public string PageFrom
        {
            get
            {
                return this._pageFrom;
            }

            set
            {
                this._pageFrom = value;
            }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Bind the user account list.
        /// </summary>
        /// <param name="addressId"> The value of AddressId</param>
        /// <returns>Returns the Address</returns>
        public string GetAddress(string addressId)
        {
            string addressText = string.Empty;

            var addressService = new AddressService();
            ZNode.Libraries.DataAccess.Entities.Address address = addressService.GetByAddressID(Convert.ToInt32(addressId));
            if (address != null)
            {
                addressText = address.ToString();
            }

            return addressText;
        }

        /// <summary>
        /// Gets the name of the Addon for this AddonId
        /// </summary>
        /// <param name="portalId">The value of PortalId</param>
        /// <returns>Returns the Store Name</returns>
        public string GetStoreName(object portalId)
        {
            var adminAccess = new PortalAdmin();
            var portal = adminAccess.GetByPortalId(int.Parse(portalId.ToString()));

            if (portal != null)
            {
                return portal.StoreName;
            }

            return string.Empty;
        }

        /// <summary>
        /// Gets the Store Title for this Portal Id
        /// </summary>
        /// <param name="portalId">The value of Portal Id</param>
        /// <returns>Returns the Store Title</returns>
        public string GetStoreTitle(object portalId)
        {
            var adminAccess = new PortalAdmin();
            var portal = adminAccess.GetByPortalId(int.Parse(portalId.ToString()));

            return portal != null ? portal.CompanyName : string.Empty;
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Bind Data Method
        /// </summary>
        protected void BindData()
        {
            var accountAdmin = new AccountAdmin();
            var customerAdmin = new CustomerAdmin();

            var account = accountAdmin.GetByAccountID(this._accountId);

			isCustomerPricing = ZNodeConfigManager.SiteConfig.EnableCustomerPricing;

            if (account == null) return;
            var defaultBillingAddress = accountAdmin.GetDefaultBillingAddress(this._accountId) ??
                                                                                new ZNode.Libraries.DataAccess.Entities.Address();

            // General Information
            lblName.Text = defaultBillingAddress.FirstName + " " + defaultBillingAddress.LastName;
            lblPhone.Text = defaultBillingAddress.PhoneNumber;
            lblAccountID.Text = account.AccountID.ToString();
            lblExternalAccNumber.Text = account.ExternalAccountNo;
            lblDescription.Text = account.Description;
            lblLogin.Text = lblLoginName.Text = customerAdmin.GetByUserID(int.Parse(this._accountId.ToString()));
	        lblCompanyName.Text = defaultBillingAddress.CompanyName; //account.CompanyName;
            CustomerBasedPriceEnable.Src = Helper.GetCheckMark(account.EnableCustomerPricing.GetValueOrDefault());

            pnlCustomerPricing.Visible = false;
			pnlLoginName.Visible = false;

            switch (this.PageFrom.ToLower())
            {
                case "admin":
                    lblCustomerDetails.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleCustomerAdmin") + account.AccountID.ToString() + " - " + defaultBillingAddress.FirstName + " " + defaultBillingAddress.LastName;
                    lblSiteLoginName.Text = customerAdmin.GetByUserID(int.Parse(this._accountId.ToString()));
                    lblSiteAccountNumber.Text = account.AccountID.ToString(CultureInfo.InvariantCulture);
                    lblSiteEmail.Text = account.Email;
                    SiteEmailOptin.Src = Helper.GetCheckMark(account.EmailOptIn);
                    lblSiteCreateDate.Text = account.CreateDte.ToShortDateString();
                    lblSiteCreateUser.Text = account.CreateUser;
                    lblSiteUpdateUser.Text = account.UpdateUser;
                    if (account.UpdateDte != null)
                    {
                        lblSiteUpdateDate.Text = account.UpdateDte.Value.ToShortDateString();
                    }
                    break;
                case "franchise":
                    lblCustomerDetails.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleCustomerFranchise") + account.AccountID.ToString() + " - " + defaultBillingAddress.FirstName + " " + defaultBillingAddress.LastName;
                    break;
                case "vendor":
                    lblCustomerDetails.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleCustomerVendor") + account.AccountID.ToString() + " - " + defaultBillingAddress.FirstName + " " + defaultBillingAddress.LastName;
                    break;
                default:
		            if (isCustomerPricing == true)
		            {
			            pnlCustomerPricing.Visible = true;
			            tabPricing.Visible = true;
		            }
		            else
		            {
						pnlLoginName.Visible = true;
		            }
                    lblCustomerDetails.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleCustomer") + account.AccountID.ToString() + " - " + defaultBillingAddress.FirstName + " " + defaultBillingAddress.LastName;
                    break;
            }
            lblWebSite.Text = account.Website;
            lblSource.Text = account.Source;
            lblCreatedDate.Text = account.CreateDte.ToShortDateString();
            lblCreatedUser.Text = account.CreateUser;

            // Get Referral Type Name for a Account
            if (account.ReferralCommissionTypeID != null)
            {
                lblReferralType.Text = this.GetCommissionTypeById(account.ReferralCommissionTypeID.ToString());
            }
            else
            {
                lblReferralType.Text = string.Empty;
            }

            if (account.ReferralStatus == "A")
            {
				// Get the portalid of the account
					var accountprofileService = new AccountProfileService();
					var portalProfileService = new PortalProfileService();
					var accountProfile = new TList<AccountProfile>();
	                var portalProfile = new TList<PortalProfile>();
	                var domainURL = new TList<Domain>();
					var domainAdmin = new DomainAdmin();
					string domainName = string.Empty;
					
					accountProfile = accountprofileService.GetByAccountID(this._accountId);

	                if (accountProfile!= null && accountProfile.Any())
	                {
		                portalProfile =
			                portalProfileService.GetByProfileID(accountProfile[0].ProfileID.GetValueOrDefault(0));
	                }

					if (portalProfile!=null && portalProfile.Any())

	                domainURL = domainAdmin.GetDomainByPortalID(portalProfile[0].PortalID);
					if (domainURL!=null && domainURL.Any())
					{
						domainName = "http://" + domainURL.FirstOrDefault().DomainName;
						string affiliateLink =  domainName + "/default.aspx?affiliate_id=" +this._accountId;
						hlAffiliateLink.Text = affiliateLink;
						hlAffiliateLink.NavigateUrl = affiliateLink;
					}
					else
					{
						dvTrackingLink.Visible = false;
						lblAffiliateLinkError.Text = string.Empty;
						lblAffiliateLinkError.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorAffiliateLink").ToString();
					}
            }
            else
            {
                hlAffiliateLink.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "HyperLinkNA").ToString();
            }

            lblReferralCommission.Text = account.ReferralCommission != null ? account.ReferralCommission.Value.ToString(account.ReferralCommissionTypeID == 1 ? "N" : "c") : string.Empty;

            lblTaxId.Text = account.TaxID;
            lblReferralStatus.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTextInactive").ToString(); 
            if (account.ReferralStatus != null)
            {
                // Getting the Status Description 
                var values = Enum.GetValues(typeof(ZNodeApprovalStatus.ApprovalStatus));
                var names = Enum.GetNames(typeof(ZNodeApprovalStatus.ApprovalStatus));
                for (int i = 0; i < names.Length; i++)
                {
                    if (names.GetValue(i).ToString() != account.ReferralStatus) continue;
                    lblReferralStatus.Text = ZNodeApprovalStatus.GetEnumValue(values.GetValue(i));
                    break;
                }

                this.BindPayments(accountAdmin);
            }
            else
            {
                pnlAffiliatePayment.Visible = false;
            }

            if (account.UpdateDte != null)
            {
                lblUpdatedDate.Text = account.UpdateDte.Value.ToShortDateString();
            }

            // Email Opt-In
            EmailOptin.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(account.EmailOptIn);

            lblEmail.Text = account.Email;
            lblUpdatedUser.Text = account.UpdateUser;
            lblCustom1.Text = account.Custom1;
            lblCustom2.Text = account.Custom2;
            lblCustom3.Text = account.Custom3;

            // To get Amount owed 
            var helperAccess = new AccountHelper();
            var myDataSet = helperAccess.GetCommisionAmount(ZNodeConfigManager.SiteConfig.PortalID, account.AccountID.ToString());

            lblAmountOwed.Text = "$" + myDataSet.Tables[0].Rows[0]["CommissionOwed"].ToString();

            // Orders Grid
            this.BindGrid();

            this.BindReferralCommission();

            // Retrieves the Role for User using Userid
            if (account.UserID != null)
            {
                Guid UserKey;
                UserKey = account.UserID.Value;
                var user = Membership.GetUser(UserKey);
                var roleList = string.Empty;

                // Get Profile Store Access using userName.
                if (user != null)
                {
                    var newProfile = ProfileCommon.Create(user.UserName, true);

                    if (newProfile.StoreAccess == "AllStores" || newProfile.StoreAccess == string.Empty)
                    {
                        lblstores.Text = newProfile.StoreAccess;
                    }
                    else
                    {
                        var portalAdmin = new PortalAdmin();
                        lblstores.Text = portalAdmin.GetStoreNameByPortalIDs(newProfile.StoreAccess);
                    }
                }

                // Get roles for this User account
                if (user != null)
                {
                    var roles = Roles.GetRolesForUser(user.UserName);

                    roleList = roles.Aggregate(roleList, (current, role) => current + (role + "<br>"));
                }

                lblRoles.Text = roleList;

                var rolename = roleList;

                // If the user is Admin role, then show permissions tab
                if (Roles.IsUserInRole("ADMIN"))
                {
					if (rolename == Convert.ToString("FRANCHISE<br>") || rolename == Convert.ToString("VENDOR<br>"))
					{
						tabpnlOrders.Enabled = false;
					}
					else
					{
						tabpnlOrders.Enabled = true;
					}
                    permissionPanel.Enabled = true;
				
                }

                // Hide the Edit button if a NonAdmin user has entered this page
                if (!Roles.IsUserInRole("ADMIN"))
                {
                    if (user != null && Roles.IsUserInRole(user.UserName, "ADMIN"))
                    {
                        EditCustomer.Visible = false;
                    }
                    else if (Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "CUSTOMER SERVICE REP"))
                    {
                        if (rolename == Convert.ToString("USER<br>") || rolename == Convert.ToString(string.Empty))
                        {
                            EditCustomer.Visible = true;
                        }
                        else
                        {
                            EditCustomer.Visible = false;
                        }
                    }
                }
            }

            // Bind address list
            this.BindAddresses();
        }

        /// <summary>
        /// Get the commission type by Id
        /// </summary>
        /// <param name="commissionTypeId">Commission Type ID</param>
        /// <returns>Returns the commission type name.</returns>
        protected string GetCommissionTypeById(object commissionTypeId)
        {
            string commissionType = string.Empty;
            if (commissionTypeId.ToString() != string.Empty)
            {
                var referralCommissionAdmin = new ReferralCommissionAdmin();
                var referralType = referralCommissionAdmin.GetByReferralID(Convert.ToInt32(commissionTypeId));
                commissionType = referralType.Name;
            }

            return commissionType;
        }

        /// <summary>
        /// Bind Order Grid
        /// </summary>
        protected void BindGrid()
        {
            var orderAdmin = new OrderAdmin();
            var orders = orderAdmin.GetByAccountID(this._accountId);

            uxGrid.DataSource = orders;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind payments Grid
        /// </summary>
        protected void BindPayments()
        {
            var accountAdmin = new AccountAdmin();
            this.BindPayments(accountAdmin);
        }

        /// <summary>
        /// Bind payments Grid
        /// </summary>    
        /// <param name="accountAdmin">The AccountAdmin instanceS</param>
        protected void BindPayments(AccountAdmin accountAdmin)
        {
            uxPaymentList.DataSource = accountAdmin.GetPaymentsByAccountId(this._accountId);
            uxPaymentList.DataBind();

            pnlAffiliatePayment.Visible = true;
        }

        /// <summary>
        /// //Bind Repeater
        /// </summary>
        protected void BindNotes()
        {
            var noteAdmin = new NoteAdmin();
            var noteList = noteAdmin.GetByAccountID(this._accountId);
            noteList.Sort("NoteID Desc");
            CustomerNotes.DataSource = noteList;
            CustomerNotes.DataBind();
        }

        /// <summary>
        /// CustomerList Button  Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CustomerList_Click(object sender, EventArgs e)
        {
            if (PageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/Default.aspx");
            }
            else if (PageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/Default.aspx");
            }
            else if (PageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/Secure/Vendors/VendorAccounts/Default.aspx");
            }
            else
            {
                Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/Default.aspx");
            }
        }

        /// <summary>
        /// Edit Customer Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CustomerEdit_Click(object sender, EventArgs e)
        {
            if (PageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/Edit.aspx?mode=0&itemid=" + this._accountId + "&pagefrom=admin");
            }
            else if (PageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/Edit.aspx?mode=0&itemid=" + this._accountId + "&pagefrom=franchise");
            }
            else if (PageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/Secure/Vendors/VendorAccounts/Edit.aspx?mode=0&itemid=" + this._accountId + "&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/EditCustomer.aspx?mode=0&itemid=" + this._accountId + "&pagefrom=customer");
            }
        }

        /// <summary>
        /// Edit Tracking Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void TrackingEdit_Click(object sender, EventArgs e)
        {
            if (PageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/AddAffiliateSettings.aspx?mode=1&itemid=" + this._accountId + "&pagefrom=admin");
            }
            else if (PageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/AddAffiliateSettings.aspx?mode=1&itemid=" + this._accountId + "&pagefrom=franchise");
            }
            else if (PageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/Secure/Vendors/VendorAccounts/AddAffiliateSettings.aspx?mode=1&itemid=" + this._accountId + "&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/AddAffiliateSettings.aspx?mode=1&itemid=" + this._accountId + "&pagefrom=customer");
            }
        }

        /// <summary>
        /// Edit Permission Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void PermissionEdit_Click(object sender, EventArgs e)
        {
            if (PageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/AddPermission.aspx?itemid=" + this._accountId + "&pagefrom=admin");
            }
            else if (PageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/AddPermission.aspx?itemid=" + this._accountId + "&pagefrom=franchise");
            }
            else if (PageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/Secure/Vendors/VendorAccounts/AddPermission.aspx?itemid=" + this._accountId + "&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/AddPermission.aspx?itemid=" + this._accountId + "&pagefrom=customer");
            }
        }

        /// <summary>
        /// Add Note Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddNewNote_Click(object sender, EventArgs e)
        {
            if (PageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/AddNote.aspx?itemid=" + this._accountId + "&pagefrom=admin");
            }
            else if (PageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/AddNote.aspx?itemid=" + this._accountId + "&pagefrom=franchise");
            }
            else if (PageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/Secure/Vendors/VendorAccounts/AddNote.aspx?itemid=" + this._accountId + "&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/AddNote.aspx?itemid=" + this._accountId + "&pagefrom=customer");
            }
        }

        /// <summary>
        /// Add Payment button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddAffiliatePayment_Click(object sender, EventArgs e)
        {
            switch (PageFrom.ToLower())
            {
                case "admin":
                    Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/AddAffiliatePayment.aspx?accId=" + this._accountId + "&pagefrom=admin");
                    break;
                case "franchise":
                    Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/AddAffiliatePayment.aspx?accId=" + this._accountId + "&pagefrom=franchise");
                    break;
                case "vendor":
                    Response.Redirect("~/Secure/Vendors/VendorAccounts/AddAffiliatePayment.aspx?accId=" + this._accountId + "&pagefrom=vendor");
                    break;
                default:
                    Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/AddAffiliatePayment.aspx?accId=" + this._accountId + "&pagefrom=customer");
                    break;
            }
        }

        /// <summary>
        /// Add Profile Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddProfile_Click(object sender, EventArgs e)
        {
            switch (PageFrom.ToLower())
            {
                case "admin":
                    Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/AddProfile.aspx?itemid=" + this._accountId + "&pagefrom=admin");
                    break;
                case "franchise":
                    Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/AddProfile.aspx?itemid=" + this._accountId + "&pagefrom=franchise");
                    break;
                case "vendor":
                    Response.Redirect("~/Secure/Vendors/VendorAccounts/AddProfile.aspx?itemid=" + this._accountId + "&pagefrom=vendor");
                    break;
                default:
                    Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/AddProfile.aspx?itemid=" + this._accountId + "&pagefrom=customer");
                    break;
            }
        }

        /// <summary>
        /// Add New Addres button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddNewAddress_Click(object sender, EventArgs e)
        {
            if (PageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/EditAddress.aspx?accountid=" + this._accountId + "&pagefrom=admin");
            }
            else if (PageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/EditAddress.aspx?accountid=" + this._accountId + "&pagefrom=franchise");
            }
            else if (PageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/Secure/Vendors/VendorAccounts/EditAddress.aspx?accountid=" + this._accountId + "&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/EditAddress.aspx?accountid=" + this._accountId + "&pagefrom=customer");
            }
        }

        /// <summary>
        /// Profile Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxProfileGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxProfileGrid.PageIndex = e.NewPageIndex;
            this.BindAccountProfile();
        }

        /// <summary>
        /// Referral commission Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxReferralCommission_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxReferralCommission.PageIndex = e.NewPageIndex;
            this.BindReferralCommission();
        }

        /// <summary>
        /// Profile Grid Row Deleting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxProfileGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindAccountProfile();
        }

        /// <summary>
        /// Profile Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxProfileGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Remove")
                {
                    var accountProfileAdmin = new AccountProfileAdmin();

                    var row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                    var hdnProfile = (HiddenField)row.FindControl("hdnProfileName");
                    var profileName = hdnProfile.Value;

                    var accountAdmin = new AccountAdmin();
                    var account = accountAdmin.GetByAccountID(this._accountId);
                    if (account != null)
                    {
                        var user = Membership.GetUser(account.UserID);
                        if (user != null)
                        {
                            string userName = user.UserName;

                            string associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogProfile").ToString(), profileName, userName); 

                            bool status = accountProfileAdmin.Delete(int.Parse(e.CommandArgument.ToString()));

                            if (status)
                            {
                                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, userName);

                                this.BindAccountProfile();
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Format the Price with two decimal
        /// </summary>
        /// <param name="fieldvalue">The value of fieldvalue</param>
        /// <returns>Returns the formatted price</returns>
        protected string FormatPrice(object fieldvalue)
        {
            if (fieldvalue == DBNull.Value)
            {
                return string.Empty;
            }
            else
            {
                if (fieldvalue.ToString().Length == 0)
                {
                    return string.Empty;
                }
                else
                {
                    return "$" + fieldvalue.ToString().Substring(0, fieldvalue.ToString().Length - 2);
                }
            }
        }

        /// <summary>
        /// Format the Price with two decimal
        /// </summary>
        /// <returns>Returns the formatted price</returns>
        protected string GetPageFrom()
        {
            return "~/Secure/Orders/OrderManagement/ViewOrders/View.aspx?itemid={0}&from=account&pagefrom=" + PageFrom.ToString();
        }

        /// <summary>
        /// Display the Order State name for a Order state
        /// </summary>
        /// <param name="fieldValue">The value of FieldValue</param>
        /// <returns>Returns the Display order status</returns>
        protected string DisplayOrderStatus(object fieldValue)
        {
            var orderStateAdmin = new OrderAdmin();
            var orderState = orderStateAdmin.GetByOrderStateID(int.Parse(fieldValue.ToString()));
            return orderState.OrderStateName;
        }

        /// <summary>
        ///  Format Customer Note
        /// </summary>
        /// <param name="field1">The value of field1</param>
        /// <param name="field2">The value of field2</param>
        /// <param name="field3">The value of field3</param>
        /// <returns>Returns the Formatted Customer Note</returns>
        protected string FormatCustomerNote(object field1, object field2, object field3)
        {
            return "<b>" + field1.ToString() + "</b>  [created by " + field2.ToString() + " on " + Convert.ToDateTime(field3).ToShortDateString() + "]";
        }

        /// <summary>
        /// Grid page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        /// <summary>
        /// Grid Row DataBound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var hdnField1 = e.Row.Cells[0].FindControl("HdnId") as HiddenField;
                var hlname1 = e.Row.Cells[0].FindControl("hlinkOrder") as HyperLink;
                if (hlname1 != null)
                    hlname1.NavigateUrl = string.Format("~/Secure/Orders/OrderManagement/ViewOrders/View.aspx?itemid={0}&from=account&pagefrom={1}", hdnField1.Value, PageFrom.ToLower().ToString());
            }
        }

        /// <summary>
        /// Customer Pricing Grid page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void uxCustomerPricing_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxCustomerPricing.PageIndex = e.NewPageIndex;
            this.BindSearchCustomerPricingProduct();
        }

        /// <summary>
        /// Payment List Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxPaymentList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxPaymentList.PageIndex = e.NewPageIndex;
            this.BindPayments();
        }

        /// <summary>
        /// Address Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GvAddress_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvAddress.PageIndex = e.NewPageIndex;
            this.BindAddresses();
        }

        /// <summary>
        /// Address Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GvAddress_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var addressId = Convert.ToInt32(e.CommandArgument);

            if (e.CommandName == "Edit")
            {
                Response.Redirect("editaddress.aspx?mode=0&itemid=" + addressId + "&accountid=" + this._accountId +
                                  "&pagefrom=" + this.PageFrom);
            }

            if (e.CommandName == "Remove")
            {
                this.DeleteAddress(addressId);
            }

        }

        /// <summary>
        /// download button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnDownload_Click(object sender, EventArgs e)
        {
            var csv = new DataDownloadAdmin();
            if (Session["CustomerPricingList"] == null)
                BindSearchCustomerPricingProduct();

            var ds = (DataSet) Session["CustomerPricingList"];
            // Set Formatted Data from DataSet           
            var strData = csv.Export(ds, true, ",");

            var data = Encoding.ASCII.GetBytes(strData);

            Response.Clear();

            // Set as Excel as the primary format
            Response.AddHeader("Content-Type", "application/Excel");

            Response.AddHeader("Content-Disposition", "attachment;filename=CustomerPricing.csv");
            Response.ContentType = "application/vnd.xls";
            Response.BinaryWrite(data);

            Response.End();
        }

        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchCustomerPricingProduct();
        }

        /// <summary>
        /// Clear button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtproductname.Text = string.Empty;
            txtProductCode.Text = string.Empty;
            txtSKU.Text = string.Empty;
            txtCategory.Text = string.Empty;
            txtCategory.Value = "0";
            txtManufacturer.Text = string.Empty;
            txtManufacturer.Value = "0";
            ddlPortal.SelectedIndex = 0;
            this.BindSearchCustomerPricingProduct();
        }

        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this._accountId = Request.Params["itemid"] != null ? int.Parse(Request.Params["itemid"].ToString()) : 0;

            if (Request.Params["Mode"] != null)
            {
                this._mode = Request.Params["Mode"].ToString();
            }

            if (Request.Params["pagefrom"] != null)
            {
                this.PageFrom = Request.Params["pagefrom"].ToString();
            }

            if (IsPostBack) return;
            this.BindData();
            this.BindNotes();
            this.BindAccountProfile();
            this.ResetTabs();
            this.RoleValidation();
           
           
        }
        #endregion

        #region Private Methods

        private void ClearBillingDetails()
        {
            lblName.Text = string.Empty;
            lblPhone.Text = string.Empty;
        }

        private void RoleValidation()
        {
            if (this.PageFrom.ToLower() == "admin")
            {
                tabCustomerDetails.Tabs[0].Enabled = false;
                tabCustomerDetails.Tabs[2].Enabled = false;
                tabCustomerDetails.Tabs[3].Enabled = false;
                tabCustomerDetails.Tabs[4].Enabled = false;
                tabCustomerDetails.Tabs[5].Enabled = false;
            }
            else if (this.PageFrom.ToLower() == "franchise")
            {
                tabCustomerDetails.Tabs[1].Enabled = false;
                tabCustomerDetails.Tabs[3].Enabled = false;
                tabCustomerDetails.Tabs[4].Enabled = false;
                tabCustomerDetails.Tabs[5].Enabled = false;
            }
            else if (this.PageFrom.ToLower() == "vendor")
            {
                tabCustomerDetails.Tabs[1].Enabled = false;
                tabCustomerDetails.Tabs[3].Enabled = false;
                tabCustomerDetails.Tabs[4].Enabled = false;
                tabCustomerDetails.Tabs[5].Enabled = false;
            }
            else
            {
	            if (isCustomerPricing == true)
	            {
		            tabPricing.Enabled = true;
	            }
	            this.BindPortal();
                this.BindSearchCustomerPricingProduct();
                tabCustomerDetails.Tabs[1].Enabled = false;
                tabCustomerDetails.Tabs[6].Enabled = false;
                if (tabCustomerDetails.ActiveTab.TabIndex > 0)
                    tabCustomerDetails.ActiveTabIndex = tabCustomerDetails.ActiveTab.TabIndex - 1;
            }
        }

        /// <summary>
        /// Bind the account referral commision grid 
        /// </summary>    
        private void BindReferralCommission()
        {
            var referralCommissionAdmin = new ReferralCommissionAdmin();
            var referralCommissionList = new TList<ReferralCommission>();

            // Gets the referral commission details
            if (this._accountId != 0)
            {
                referralCommissionList = referralCommissionAdmin.GetReferralCommissionByAccountID(this._accountId);
            }

            uxReferralCommission.DataSource = referralCommissionList;
            uxReferralCommission.DataBind();
        }

        /// <summary>
        /// Bind Account Profile Method
        /// </summary>
        private void BindAccountProfile()
        {
            uxProfileGrid.DataSource = null;
            uxProfileGrid.DataBind();
            var accountProfileAdmin = new AccountProfileAdmin();
            uxProfileGrid.DataSource = accountProfileAdmin.DeepLoadProfilesByAccountID(this._accountId);
            uxProfileGrid.DataBind();
        }

        /// <summary>
        /// Bind the user account list.
        /// </summary>
        private void BindAddresses()
        {
            var addressService = new AddressService();
            var addressList = addressService.GetByAccountID(this._accountId);

            addressList.Sort("Name ASC");
            gvAddress.DataSource = addressList;
            gvAddress.DataBind();
        }

        /// <summary>
        /// Reset Tabs Method
        /// </summary>
        private void ResetTabs()
        {
            tabCustomerDetails.ActiveTabIndex = this.PageFrom.ToLower() == "admin" ? 1 : 0;

            if (this._mode.Equals("profiles"))
            {
                tabCustomerDetails.ActiveTabIndex = 4;
            }
            else if (this._mode.Equals("affiliate"))
            {
                tabCustomerDetails.ActiveTabIndex = 5;
            }
            else if (this._mode.Equals("notes"))
            {
                var accountAdmin = new AccountAdmin();
                var account = accountAdmin.GetByAccountID(this._accountId);
                tabCustomerDetails.ActiveTabIndex = account.UserID != null ? 3 : 2;
            }
            else if (this._mode.Equals("permissions"))
            {
                tabCustomerDetails.ActiveTabIndex = 6;
            }
            else if (this._mode.Equals("order"))
            {
                tabCustomerDetails.ActiveTab = tabpnlOrders;
            }
        }

        /// <summary>
        /// Delete the selected address.
        /// </summary>
        /// <param name="addressId">Address Id</param>
        private void DeleteAddress(int addressId)
        {
            var addressService = new AddressService();
            var accountAdmin = new AccountAdmin();
            var addressList = addressService.GetByAccountID(this._accountId);
            var addressCount = addressList.Count;

            // If account has more than one address then validate for default address.
            if (addressCount > 0)
            {
                // Validate default shipping address.
                addressList.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.Address a) { return a.IsDefaultBilling == true && a.AddressID == addressId; });
                if (addressList.Count == 1)
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDefaultBilling").ToString();
                    return;
                }
                addressList.RemoveFilter();

                // Validate default shipping address.
                addressList.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.Address a) { return a.IsDefaultShipping == true && a.AddressID == addressId; });
                if (addressList.Count == 1)
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDefaultShipping").ToString();
                    return;
                }
                if (addressList.Count == 0)
                {
                    lblMsg.Text = string.Empty;
                }
            }
            addressService.Delete(addressId);

            this.BindAddresses();
            this.ClearBillingDetails();
        }

        /// <summary>
        /// Bind Portals
        /// </summary>
        private void BindPortal()
        {
            var portalAdmin = new PortalAdmin();
            var portals = portalAdmin.GetAllPortals();

            var profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (System.String.CompareOrdinal(profiles.StoreAccess, "AllStores") != 0)
            {
                if (profiles.StoreAccess != string.Empty)
                {
                    portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
                }
            }
            else
            {
                var po = new Portal();
                po.StoreName = "ALL STORES";
                portals.Insert(0, po);
            }

            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();
        }

        /// <summary>
        /// Bind Customer pricing Product list
        /// </summary>
        private void BindSearchCustomerPricingProduct()
        {

            // Create Instance for Customer HElper class
            var helperAccess = new ProductHelper();
            var ds = helperAccess.SearchCustomerPricingProduct(
                Server.HtmlEncode(txtproductname.Text.Trim()),
                txtProductCode.Text.Trim(),
                txtSKU.Text.Trim(),
                txtManufacturer.Value,
                txtCategory.Value,
                Convert.ToInt32(ddlPortal.SelectedValue),
                this._accountId);

            Session["CustomerPricingList"] = ds;
            uxCustomerPricing.DataSource = ds.Tables[0];
            uxCustomerPricing.DataBind();

            btnDownload.Visible = ds.Tables[0].Rows.Count > 0;
        }

        #endregion
    }
}