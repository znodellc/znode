﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Address.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Accounts.Address" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<div class="FormView" align="center">
    <asp:UpdatePanel ID="UpdatePnlUserAddress" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div>
                <div class="LeftFloat" style="width: 30%; text-align: left">
                    <h1>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
                </div>
                <div style="float: right">
                    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit%>" CausesValidation="true" ID="btnSubmitTop" ValidationGroup="EditContact" />
                    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel%>" CausesValidation="False" ID="btnCancelTop" />
                </div>
                <div class="ClearBoth">
                </div>
                <div align="left">
                    <div>
                        <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label>
                    </div>
                    <div>
                        <uc1:Spacer ID="Spacer8" SpacerHeight="2" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <div class="ClearBoth">
                        <div id="tblAddress" runat="server" class="LeftFloat">
                            <div class="FieldStyle">
                                <asp:Localize ID="AddressName" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnAddressName%>"></asp:Localize><br />
                                <small>
                                    <asp:Localize ID="AddressNameHint" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTextAddressName%>"></asp:Localize></small>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtName" runat="server" Width="180" Columns="30" MaxLength="100" />
                                <asp:RequiredFieldValidator ID="rfvAddressName" runat="server" ControlToValidate="txtName"
                                    ValidationGroup="EditContact" ErrorMessage="Address Name required." CssClass="Error"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="FirstName" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnFirstName%>"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtFirstName" runat="server" Width="180" Columns="30" MaxLength="100" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFirstName"
                                    ValidationGroup="EditContact" ErrorMessage="First Name required." CssClass="Error"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="LastName" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnLastName%>"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtLastName" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtLastName"
                                    ValidationGroup="EditContact" ErrorMessage="Last Name required." CssClass="Error"
                                    Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="CompanyName" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnCompanyName%>"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtCompanyName" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="Street1" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnStreet1%>"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtStreet1" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="Street2" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnStreet2%>"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtStreet2" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="City" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnCity%>"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtCity" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="State" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnState%>"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtState" runat="server" Width="180" Columns="10" MaxLength="2"></asp:TextBox>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="PostalCode" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnPostalCode%>"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtPostalCode" runat="server" Width="180" Columns="30" MaxLength="20"></asp:TextBox>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="Country" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnCountry%>"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:DropDownList ID="ddlCountryCode" Width="185" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="PhoneNumber" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnPhoneNumber%>"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtPhoneNumber" runat="server" Width="180" Columns="30" MaxLength="100"></asp:TextBox>
                            </div>
                            <div class="FieldStyle">
                            </div>
                            <div class="ValueStyle">
                                <asp:CheckBox ID="chkUseAsDefaultBillingAddress" Text="<%$ Resources:ZnodeAdminResource, CheckboxTextBillingAddress%>"
                                    TextAlign="Right" runat="server" Checked="false" />
                            </div>
                            <div class="FieldStyle">
                            </div>
                            <div class="ValueStyle">
                                <asp:CheckBox ID="chkUseAsDefaultShippingAddress" Text="<%$ Resources:ZnodeAdminResource, CheckboxTextShippingAddress%>"
                                    TextAlign="Right" runat="server" Checked="true" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ClearBoth">
                </div>
            </div>
            <br />
            <uc1:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="3" runat="server"></uc1:Spacer>
            <div class="ClearBoth" style="text-align: left">
                <br />
                <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit%>" CausesValidation="True" ID="btnSubmitBottom" ValidationGroup="EditContact" />
                <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel%>" CausesValidation="False" ID="btnCancelBottom" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
