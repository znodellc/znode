﻿using System;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace  Znode.Engine.Admin.Controls.Default.Accounts
{
    public partial class Permission : System.Web.UI.UserControl
    {
        #region Private Variables
        private int ItemId = 0;
        private string _RoleName = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value for RoleName
        /// </summary>
        public string RoleName
        {
            get
            {
                return this._RoleName;
            }

            set
            {
                this._RoleName = value;
            }
        }
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            btnAddSelectedStore.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnAddSelectedStore.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit.gif") + "'");
            btnBottomCancel.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnBottomCancel.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel.gif") + "'");

            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (Request.Params["pagefrom"] != null)
            {
                this.RoleName = Request.Params["pagefrom"].ToString();
            }
            bool flag = false;
            if (!Page.IsPostBack)
            {
                this.BindRoles();

                this.BindStores();

                RoleValidation();
                if (this.ItemId > 0)
                {
                    ZNode.Libraries.Admin.AccountAdmin _UserAccountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
                    ZNode.Libraries.DataAccess.Entities.Account _UserAccount = _UserAccountAdmin.GetByAccountID(this.ItemId);

                    _UserAccount = _UserAccountAdmin.GetByAccountID(this.ItemId);
                    
                    if (_UserAccount.UserID.HasValue)
                    {
                        MembershipUser user = Membership.GetUser(_UserAccount.UserID.Value, false);
                        lblPermissionName.Text = user.UserName;
                        // Create Profile for selected account associated with Store.
                        ProfileCommon profilecommon = ProfileCommon.Create(user.UserName, true) as ProfileCommon;

                        // Retrieve Stores
                        string[] stores = profilecommon.StoreAccess.Split(',');

                        foreach (string store in stores)
                        {
                            foreach (ListItem li in StoreCheckList.Items)
                            {
                                if (store == li.Value)
                                {
                                    li.Selected = true;
                                    pnlSpecificStore.Visible = true;
                                }

                                if (store.Equals("AllStores"))
                                {
                                    chkAllStores.Checked = true;
                                    pnlSpecificStore.Visible = false;
                                }
                                else
                                {
                                    pnlSpecificStore.Visible = true;
                                }
                            }
                        }

                        // Retrieve roles associated with this user
                        string[] roles = Roles.GetRolesForUser(user.UserName);

                        foreach (string Role in roles)
                        {
                            // Loop through the roles list
                            foreach (ListItem li in RolesCheckboxList.Items)
                            {
                                if (li.Text == Role)
                                {
                                    li.Selected = true;
                                    if (li.Text == "FRANCHISE")
                                    {
                                        li.Enabled = false;
                                        flag = true;
                                    }
                                }

                                // Admin User should not remove the Admin role from their own account.
                                if (HttpContext.Current.User.Identity.Name.Equals(user.UserName.ToLower()) && li.Text == "ADMIN")
                                {
                                    li.Enabled = false;
                                }


                            }
                        }
                    }
                }
                if (!flag)
                    RemoveCheckBoxes();
            }

        }
        #endregion

        #region Events

        /// <summary>
        /// Add Selected Portals Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddSelectedPortals_Click(object sender, EventArgs e)
        {
            string StoreAccessValue = string.Empty;
            string RoleAccessValue = string.Empty;
            CustomerAdmin customerAdmin = new CustomerAdmin();
            string UserName = customerAdmin.GetByUserID(this.ItemId);
            ZNode.Libraries.Admin.AccountAdmin _UserAccountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Account _UserAccount = _UserAccountAdmin.GetByAccountID(this.ItemId);

            if (this.ItemId > 0)
            {
                _UserAccount = _UserAccountAdmin.GetByAccountID(this.ItemId);
            }

            MembershipUser user = Membership.GetUser(_UserAccount.UserID.Value, false);

            if (_UserAccount.UserID.HasValue)
            {
                // Retrieve roles associated with this user
                string[] roles = Roles.GetRolesForUser(user.UserName);

                // Loop through the roles
                foreach (string Role in roles)
                {
                    // Unassign this user from that role
                    Roles.RemoveUserFromRole(user.UserName, Role);
                }

                // Taking the selected role
                foreach (ListItem li in RolesCheckboxList.Items)
                {
                    if (li.Selected)
                    {
                        RoleAccessValue += li.Value + ",";
                    }
                }

                // Associate this user with the selected store
                foreach (ListItem li in StoreCheckList.Items)
                {
                    if (li.Selected)
                    {
                        StoreAccessValue += li.Value + ",";
                    }
                }

                // If you select All Stores, then it saves NULL string Value                        
                if (chkAllStores.Checked == true)
                {
                    StoreAccessValue = "AllStores";
                }
                else if (StoreAccessValue.Length > 0)
                {
                    StoreAccessValue = StoreAccessValue.Remove(StoreAccessValue.Length - 1);
                }
                else
                {
                    if (RoleAccessValue.Length > 0)
                    {
                        lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorSelectStore").ToString();
                        return;
                    }
                }

                // Associate this user with the selected role
                foreach (ListItem li in RolesCheckboxList.Items)
                {
                    if (li.Selected)
                    {
                        // Associate the User with the selected role
                        Roles.AddUserToRole(user.UserName, li.Text);
                    }
                }

                // Create an Profile for the selected user
                ProfileCommon newProfile = (ProfileCommon)ProfileCommon.Create(UserName, true);

                // Properties Value
                newProfile.StoreAccess = StoreAccessValue;

                // Save profile - must be done since we explicitly created it 
                newProfile.Save();
            }

            string AssociateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditPermission").ToString(), UserName);

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, UserName);

            if (RoleName.ToLower() == "admin")
            {
                Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + this.ItemId + "&Mode=permissions&pagefrom=admin");
            }
            else if (RoleName.ToLower() == "franchise")
            {
                Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + this.ItemId + "&Mode=permissions&pagefrom=franchise");
            }
            else if (RoleName.ToLower() == "vendor")
            {
                Response.Redirect("~/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + this.ItemId + "&Mode=permissions&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + this.ItemId + "&Mode=permissions&pagefrom=customer");
            }

        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (RoleName.ToLower() == "admin")
            {
                Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + this.ItemId + "&Mode=permissions&pagefrom=admin");
            }
            else if (RoleName.ToLower() == "franchise")
            {
                Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + this.ItemId + "&Mode=permissions&pagefrom=franchise");
            }
            else if (RoleName.ToLower() == "vendor")
            {
                Response.Redirect("~/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + this.ItemId + "&Mode=permissions&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + this.ItemId + "&Mode=permissions&pagefrom=customer");
            }

        }

        /// <summary>
        /// Check All Stores Checked Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChkAllStores_checkedChanged(object sender, EventArgs e)
        {
            if (chkAllStores.Checked == true)
            {
                pnlSpecificStore.Visible = false;
            }
            else
            {
                pnlSpecificStore.Visible = true;
            }
        }

        #endregion

        #region Helper Methods

        private void RoleValidation()
        {
            if (RoleName.ToLower() == "admin")
            {
                lblSelectStores.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTitleAdminSelectStores").ToString(); 
            }
            else if (RoleName.ToLower() == "franchise")
            {
                lblSelectStores.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTitleSelectStores").ToString();
            }
            else if (RoleName.ToLower() == "vendor")
            {
                lblSelectStores.Text = this.GetGlobalResourceObject("ZnodeAdminResource","SubTitleVendorSelectStores").ToString();
            }
            else
            {

            }
        }

        /// <summary>
        /// Gets the name of the Portal for this PortalId
        /// </summary>
        /// <param name="portalId">The value of portalId</param>
        /// <returns>Returns the Portal Name</returns>
        private string GetPortalName(object portalId)
        {
            PortalAdmin AdminAccess = new PortalAdmin();
            Portal _Portal = AdminAccess.GetByPortalId(int.Parse(portalId.ToString()));

            if (_Portal != null)
            {
                return _Portal.StoreName;
            }

            return string.Empty;
        }

        /// <summary>
        /// Binds user roles checkbox list
        /// </summary>
        private void BindRoles()
        {
            // Get all roles
            string[] roles = Roles.GetAllRoles();

            // Bind Checkbox list
            RolesCheckboxList.DataSource = roles;
            RolesCheckboxList.DataBind();

            if (Roles.IsUserInRole("ADMIN"))
            {
                RolesCheckboxList.Enabled = true;
            }

            // When the admin user sets the admin role to the end user then pop up a message box 
            ListItem item = RolesCheckboxList.Items.FindByText("ADMIN");
            item.Attributes.Add("OnClick", "if(this.checked == true)alert('"+this.GetGlobalResourceObject("ZnodeAdminResource","WarningAdminAccess")+"')");
        }

        /// <summary>
        /// Disable user roles checkbox list
        /// </summary>
        private void RemoveCheckBoxes()
        {
            // Remove franchise role.
            RolesCheckboxList.Items.Remove("FRANCHISE");
        }

        /// <summary>
        /// Binds Stores in a checkbox list
        /// </summary>
        private void BindStores()
        {
            PortalService portalServ = new PortalService();

            // CheckboxList Type
            StoreCheckList.Visible = true;
            StoreCheckList.DataSource = portalServ.GetAll();
            StoreCheckList.DataTextField = "StoreName";
            StoreCheckList.DataValueField = "PortalID";
            StoreCheckList.DataBind();

            // Set Vertical Or Horizontal view
            StoreCheckList.RepeatDirection = RepeatDirection.Vertical;
        }
        #endregion
    }
}