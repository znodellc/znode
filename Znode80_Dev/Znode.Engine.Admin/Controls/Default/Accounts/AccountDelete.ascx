﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountDelete.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Accounts.AccountDelete" %>

<h5>
    <asp:Localize ID="PleaseConfirm" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextPleaseConfirm%>"></asp:Localize>
</h5>
<p>
    <asp:Localize ID="DeleteAccountConfirm" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextDeleteAccount%>"></asp:Localize>


</p>
<div class="LeftFloat">
    <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label></div>
<br />
<br />
<div>
    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnDelete_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonDelete%>" CausesValidation="True" ID="btnDelete" />
    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel%>" CausesValidation="False" ID="btnCancel" />
</div>
<br />
