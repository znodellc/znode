﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Profile.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Accounts.Profile" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<div>
    <div class="LeftFloat" style="width: 40%; text-align: left">
        <h1 style="width: 700px;">
            <asp:Localize runat="server" ID="TitleProfile" Text="<%$ Resources:ZnodeAdminResource,TitleProfile %>"></asp:Localize><asp:Label ID="lblName" runat="server" /></h1>
    </div>
    <div style="float: right">
        <zn:Button ID="btnBack" runat="server" Width="100px" ButtonType="EditButton" OnClick="BtnBack_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonBack %>" />
    </div>
    <!-- Search profile panel -->
    <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server" Visible="false">
        <div class="SearchForm">
            <h4 class="SubTitle">
                <asp:Localize ID="SubTitleProfile" runat="server" Text="<%$ Resources:ZnodeAdminResource,SubTitleProfile %>"></asp:Localize></h4>
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle">
                        <asp:Localize ID="ColumnTitleKeyword" runat="server" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleKeyword %>"></asp:Localize></span><br />
                    <span class="ValueStyle">
                        <asp:TextBox ID="txtprofilename" runat="server"></asp:TextBox></span>
                </div>
            </div>
            <div class="ClearSearch">
                <zn:Button ID="btnSearch" runat="server" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonSearch %>" />
                <zn:Button ID="btnClear" runat="server" CausesValidation="false" ButtonType="CancelButton" OnClick="BtnClear_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonClearSearch %>" />
            </div>
        </div>
    </asp:Panel>
    <!-- Profile List -->
    <asp:Panel ID="pnlProfileList" runat="server">
        <h4 class="GridTitle">
            <asp:Localize ID="GridTitleProfile" runat="server" Text="<%$ Resources:ZnodeAdminResource,GridTitleProfile%>"></asp:Localize></h4>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
            GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
            EmptyDataText="<%$ Resources:ZnodeAdminResource,GridEmptyText %>" Width="100%" CellPadding="4">
            <Columns>
                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleSelect %>" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkProfile" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="ProfileId" HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleID %>" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="Name" HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleName %>" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleWholeSalePricing %>" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="UseWholeSalePricingImg" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "UseWholeSalePricing").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleShowPrice %>" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="ShowPricingImg" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ShowPricing").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Weighting" HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleWeighting %>" HeaderStyle-HorizontalAlign="Left" />
            </Columns>
            <EmptyDataTemplate>
                <asp:Localize runat="server" ID="GridEmptyData" Text="<%$ Resources:ZnodeAdminResource,GridEmptyData %>"></asp:Localize>
            </EmptyDataTemplate>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div>
            <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                ForeColor="red"></asp:Label>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div>
            <zn:Button runat="server" ID="btnSubmitBottom" ButtonType="SubmitButton" OnClick="Submit_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonSubmit %>" CausesValidation="True" />
            <zn:Button runat="server" ID="btnCancelBottom" ButtonType="CancelButton" OnClick="Cancel_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonCancel %>" CausesValidation="False" />
        </div>
    </asp:Panel>
    <div>
        <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
</div>
