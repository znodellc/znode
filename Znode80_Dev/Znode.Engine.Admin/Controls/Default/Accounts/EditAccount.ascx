﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EditAccount.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Accounts.EditAccount" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<div class="FormView" align="center">
    <div>
        <div class="LeftFloat" style="width: auto; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>

        <div style="float: right">
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit%>" ID="btnSubmitTop" ValidationGroup="EditContact" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel%>" CausesValidation="False" ID="btnCancelTop" />
        </div>
        <div align="left">
            <div class="ClearBoth" style="float: left">
                <asp:Label ID="lblErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label>
            </div>
            <div>
                <uc1:Spacer ID="Spacer8" SpacerHeight="6" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <div class="FieldStyle1">
                <b style="text-decoration: underline">
                    <asp:Localize ID="TextNote" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextNote%>"></asp:Localize></b> <asp:Label ID="lblInstructions" runat="server"></asp:Label>
            </div>
            <asp:Panel runat="server" ID="pnlSubTitle">
            </asp:Panel>

            <!-- Update Panel for profiles drop down list to avoid the postbacks -->
            <asp:UpdatePanel ID="updPnlRealtedGrid" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel runat="server" ID="pnlAccount">
                        <h4 class="SubTitle">
                            <asp:Localize ID="PhoneNumber" runat="server" Text="<%$ Resources:ZnodeAdminResource, SubTitleGeneralInformation%>"></asp:Localize></h4>
                        <div class="FieldStyle">
                            <asp:Localize ID="ExternalID" runat="server" Text="<%$ Resources:ZnodeAdminResource, SubTitleExternalID%>"></asp:Localize>
                            <asp:Label ID="lblaccntnumastric" runat="server" ForeColor="Red" Text="*"
                                Visible="False"></asp:Label>
                            <br />
                            <small>
                                <asp:Localize ID="ExternalIDHint" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnExternalID%>"></asp:Localize></small>
                        </div>

                        <div class="ValueStyle">
                            <asp:TextBox ID="txtExternalAccNumber" runat="server" Width="150px" MaxLength="50"></asp:TextBox>
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlCustomerPricing" Visible="false">
                        <div class="FieldStyle">
                            <asp:Localize ID="CustomerBasedPricingEnabled" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnCustomerBasedPricingEnabled%>"></asp:Localize>
                            <asp:Label ID="lblcustomerpriceastric" runat="server" ForeColor="Red" Text="*"
                                Visible="False"></asp:Label>
                            <br />
                            <small>
                                <asp:Localize ID="CustomerBasedPricingEnabledHint" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTextCustomerBasedPricingEnabled%>"></asp:Localize></small>
                        </div>
                        <div class="ValueStyle">
                            <asp:CheckBox ID="chkCustomerPricing" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnEnableCustomerBasedPricing%>" />
                        </div>
                    </asp:Panel>
                    <h4 class="SubTitle">
                        <asp:Localize ID="LoginInformation" runat="server" Text="<%$ Resources:ZnodeAdminResource, SubTitleLoginInformation%>"></asp:Localize></h4>
                    <div class="FieldStyle">
                        <asp:Localize ID="ColumnUserID" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnUserID%>"></asp:Localize>
                        <span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="UserID" runat="server" Columns="40" Width="150px" />
                        <asp:RequiredFieldValidator ID="UseridRequired" runat="server"
                            ControlToValidate="UserID" ToolTip="User Id is required." ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredAccountUserID%>"
                            ValidationGroup="EditContact" Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>

                    <asp:Label ID="lblUserIDErrorMsg" runat="server" CssClass="Error" EnableViewState="false"></asp:Label>
                    <asp:Panel ID="pnlResetPassword" runat="server">
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnResetPassword" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnResetPassword%>"></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnResetPassword_Click" OnClientClick="return ResetPasswordMailConfirmation();" CausesValidation="true" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnResetPassword" />
                        </div>
                    </asp:Panel>
                    <asp:Panel runat="server" ID="pnlCustomerInformation">

                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnEmailAddress" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnEmailAddress%>"></asp:Localize><span class="Asterix">*</span>
                            <asp:Label ID="lblEMailastric" runat="server" ForeColor="Red" Text="*" Visible="False"></asp:Label>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtEmail" runat="server" Width="131px"></asp:TextBox>
                            &nbsp;<asp:RequiredFieldValidator ID="EmailAddressRequired" runat="server" ControlToValidate="txtEmail" CssClass="Error" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredEmailAddress%>" ToolTip="<%$ Resources:ZnodeAdminResource, RequiredEmailAddress%>" ValidationGroup="EditContact" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            <div>
                                <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" ControlToValidate="txtEmail"
                                    ErrorMessage="<%$ Resources:ZnodeAdminResource, RegularEmailAddress%>" Display="Dynamic" ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"
                                    ValidationGroup="EditContact" CssClass="Error" SetFocusOnError="True"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                        </div>

                        <div class="ValueStyle">
                            <asp:CheckBox ID="chkOptIn" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnEmailOptn%>" />
                        </div>
                        <h4 class="SubTitle">
                            <asp:Localize ID="SubTitleCustomInformation" runat="server" Text="<%$ Resources:ZnodeAdminResource, SubTitleCustomInformation%>"></asp:Localize></h4>
                        <div>
                            <div class="FieldStyle">
                                <asp:Localize ID="Localize1" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnCompanyName%>"></asp:Localize>
                                <asp:Label ID="lblaccntnumastric0" runat="server" ForeColor="Red" Text="*"
                                    Visible="False"></asp:Label>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtCompanyName" runat="server" />&nbsp;&nbsp;<asp:RequiredFieldValidator
                                    ID="CmpnyNameRequired" runat="server" ControlToValidate="txtCompanyName"
                                    CssClass="Error" Display="Dynamic" Enabled="False"
                                    ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredCompanyName%>" ToolTip="<%$ Resources:ZnodeAdminResource, RequiredCompanyName%>"
                                    ValidationGroup="EditContact" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnWebsite" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnWebsite%>"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtWebSite" runat="server" />
                            </div>
                        </div>
                        <div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnSource" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnSource%>"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:TextBox ID="txtSource" runat="server" />
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnCustom1" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnCustom1%>"></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtCustom1" runat="server" TextMode="MultiLine" Width="400" Height="100"
                                MaxLength="2"></asp:TextBox>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnCustom2" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnCustom2%>"></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtCustom2" runat="server" TextMode="MultiLine" Width="400" Height="100"
                                MaxLength="2"></asp:TextBox>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnCustom3" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnCustom3%>"></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtCustom3" runat="server" TextMode="MultiLine" Width="400" Height="100"
                                MaxLength="2"></asp:TextBox>
                        </div>
                        <uc1:Spacer ID="Spacer4" SpacerHeight="1" SpacerWidth="3" runat="server"></uc1:Spacer>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnDescription" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnDescription%>"></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Width="400"
                                Height="200" MaxLength="2" />
                        </div>
                    </asp:Panel>
                    <uc1:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="3" runat="server"></uc1:Spacer>
                </ContentTemplate>
            </asp:UpdatePanel>
            <uc1:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            <uc1:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
            <div class="ClearBoth">
                <br />

                <%--NEW STUFF--%>

                <asp:UpdatePanel runat="server" ID="pnlVendorInformation">
                    <ContentTemplate>
                        <h4 class="SubTitle">
                            <asp:Localize ID="ColumnVendorInformation" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnVendorInformation%>"></asp:Localize></h4>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnFranchiseAccountNumber" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnFranchiseAccountNumber%>"></asp:Localize><br />
                            <small>
                                <asp:Localize ID="ColumnTextFranchiseAccountNumber" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTextFranchiseAccountNumber%>"></asp:Localize></small>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtFranchiseNumber" runat="server" CssClass="TextField"></asp:TextBox>

                        </div>
                        <div class="ClearBoth">
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnFirstName" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnFirstName%>"></asp:Localize>
                            <span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingFirstName" runat="server" CssClass="TextField" Columns="30"
                                MaxLength="100"></asp:TextBox>
                            <div>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredFirstName%>"
                                    ValidationGroup="EditContact" ControlToValidate="txtBillingFirstName" runat="server"
                                    Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize2" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnLastName%>"></asp:Localize>
                            <span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingLastName" runat="server" CssClass="TextField" Columns="30"
                                MaxLength="100"></asp:TextBox>
                            <div>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredLastName%>"
                                    ValidationGroup="EditContact" ControlToValidate="txtBillingLastName" runat="server"
                                    Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize3" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnCompanyName%>"></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingCompanyName" runat="server" CssClass="TextField" Columns="30"
                                MaxLength="100"></asp:TextBox>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColEmailAddress" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnEmailAddress%>"></asp:Localize>
                            <span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingEmail" runat="server" CssClass="TextField"></asp:TextBox><div>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredEmail%>"
                                    ValidationGroup="EditContact" ControlToValidate="txtBillingEmail" runat="server"
                                    Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regemailID" runat="server" ControlToValidate="txtBillingEmail"
                                    ErrorMessage="<%$ Resources:ZnodeAdminResource, RegularEmailAddress%>" Display="Dynamic" ValidationExpression='[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+'
                                    CssClass="Error" ValidationGroup="EditContact"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnPhoneNumber" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnPhoneNumber%>"></asp:Localize>
                            <span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingPhoneNumber" runat="server" CssClass="TextField" Columns="30"
                                MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredPhoneNumber%>"
                                        ValidationGroup="EditContact" ControlToValidate="txtBillingPhoneNumber"
                                        runat="server" Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnStreet1" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnStreet1%>"></asp:Localize>
                            <span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingStreet1" runat="server" CssClass="TextField" Columns="30"
                                MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredStreet1%>"
                                        ValidationGroup="EditContact" ControlToValidate="txtBillingStreet1" runat="server"
                                        Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnStreet2" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnStreet2%>"></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingStreet2" runat="server" CssClass="TextField" Columns="30"
                                MaxLength="100"></asp:TextBox>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="txLiveChat" runat="server" Text="City" />
                            <span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingCity" runat="server" CssClass="TextField" Columns="30"
                                MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredCity%>"
                                        ValidationGroup="EditContact" ControlToValidate="txtBillingCity" runat="server"
                                        Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnState" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnState%>"></asp:Localize>
                            <span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtBillingState" runat="server" Width="40" Columns="10" MaxLength="2"></asp:TextBox><div>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredState%>"
                                    ValidationGroup="EditContact" ControlToValidate="txtBillingState" runat="server"
                                    Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnPostalCode" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnPostalCode%>"></asp:Localize>
                            <span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle" style="height: 49px">
                            <asp:TextBox ID="txtBillingPinCode" runat="server" Width="40" Columns="10"></asp:TextBox><div>
                                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" gErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredPostalCode%>"
                                    ValidationGroup="EditContact" ControlToValidate="txtBillingPinCode" runat="server"
                                    Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                                <br />
                                <br />
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <%--FRANCHISE ADMIN EDIT UI PANEL--%>
                <asp:UpdatePanel runat="server" ID="pnlFranchiseAdmin">
                    <ContentTemplate>
                        <%--				<div class="FieldStyle">
                    Franchise Number<span class="Asterix">*</span><br />
                    <small>Use this field to associate this Franchise with your internal accounting system.</small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtFranchiseAdminNumber" runat="server" CssClass="TextField"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="FranchiseNumberRequired1" runat="server" ControlToValidate="txtFranchiseAdminNumber"
                            ErrorMessage="Franchise Number is required." ToolTip="Franchise Number is required." ValidationGroup="EditContact"
                            CssClass="Error" Display="Dynamic">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
				<div class="ClearBoth"></div>

               <h4 class="SubTitle">STORE SETTINGS </h4>

                <div class="FieldStyle">
                    Store Name<span class="Asterix">*</span><br />
                    <small>Specify Store Name / Company name.</small>
                </div>

                <div class="ValueStyle">
                    <asp:TextBox ID="txtFranchiseCompanyName" runat="server" CssClass="TextField" Columns="30"
                        MaxLength="100" Enabled="False"></asp:TextBox>
                    <br />
                    <asp:RequiredFieldValidator ID="franchiseCompanyNameValidator" runat="server"
                        ControlToValidate="txtFranchiseCompanyName" CssClass="Error" Display="Dynamic"
                        ErrorMessage="Store Name is required" ValidationGroup="EditContact"></asp:RequiredFieldValidator>
                    <br />
                </div>
                <div class="FieldStyle">
                    Theme
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlTheme" runat="server" Enabled="False" />
                </div>
                <%--<div class="FieldStyle">
                    Site URL<br />
                    <small>Specify Store URL (e.g. http://www.yourstore.com). This URL must be set up to point to our servers</small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtStoreUrl" runat="server" CssClass="TextField" Columns="30" MaxLength="100"></asp:TextBox><div>
                        <%-- <asp:RequiredFieldValidator ID="Requiredfieldvalidator8" ErrorMessage="Store URL is required"
                        ValidationGroup="uxCreateUserWizard" ControlToValidate="txtStoreUrl" runat="server"
                        Display="Dynamic" CssClass="Error"></asp:RequiredFieldValidator>
                       <asp:RegularExpressionValidator ID="txtStoreUrlFieldValidator" runat="server" ControlToValidate="txtStoreUrl" ValidationGroup="EditContact"
                            ErrorMessage="* Enter Valid URL" CssClass="Error" Display="dynamic" ValidationExpression="^(http\:\/\/[a-zA-Z0-9_\-/]+(?:\.[a-zA-Z0-9_\-/]+)*)$"></asp:RegularExpressionValidator>
                    </div>
                </div>
             <div class="FieldStyle">
                    Select a Logo<br />
                    <small>Choose a logo that will be displayed on web site.</small>
                </div>
                <div class="ValueStyle">
                    <asp:FileUpload ID="RegisterUploadImage" runat="server" BorderStyle="Inset" EnableViewState="true" />
                    <asp:RegularExpressionValidator ID="uploadImageValidator" runat="server" ControlToValidate="RegisterUploadImage"
                        ErrorMessage="Please specify an image with a jpeg, jpg, gif or png file format." SetFocusOnError="True"
                        ValidationExpression="^.+\.((jpg)|(JPG)|(gif)|(GIF)|(jpeg)|(JPEG)|(png)|(PNG))$" Display="Dynamic"
                        ValidationGroup="EditContact"> </asp:RegularExpressionValidator>
                </div>--%>

                        <div class="ClearBoth">
                        </div>
                        <h4 class="SubTitle">
                            <asp:Localize ID="SubTitleContactInformation" runat="server" Text="<%$ Resources:ZnodeAdminResource, SubTitleContactInformation%>"></asp:Localize></h4>
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize4" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnFirstName%>"></asp:Localize>
                            <span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtFranchiseAdminBillingFirstName" runat="server" CssClass="TextField" Columns="30"
                                MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="txtFranchiseAdminBillingFirstNameValidator" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredFirstName%>"
                                        ValidationGroup="EditContact" ControlToValidate="txtFranchiseAdminBillingFirstName" runat="server"
                                        Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize5" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnLastName%>"></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtFranchiseBillingLastName" runat="server" CssClass="TextField" Columns="30"
                                MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="franchiseBillingLastNameValidator" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredLastName%>"
                                        ValidationGroup="EditContact" ControlToValidate="txtFranchiseBillingLastName" runat="server"
                                        Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize6" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnEmailAddress%>"></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtFranchiseAdminBillingEmailID" runat="server" CssClass="TextField"></asp:TextBox><div>
                                <asp:RequiredFieldValidator ID="franchiseAdminBillingEmailIDValidator" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredEmailAddress%>"
                                    ValidationGroup="EditContact" ControlToValidate="txtFranchiseAdminBillingEmailID" runat="server"
                                    Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="franchiseEmailIDExpressionValidator" runat="server" ControlToValidate="txtFranchiseAdminBillingEmailID"
                                    ErrorMessage="<%$ Resources:ZnodeAdminResource, RegularEmailAddress%>" Display="Dynamic" ValidationExpression='[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+'
                                    CssClass="Error" ValidationGroup="EditContact" SetFocusOnError="True"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize7" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnPhoneNumber%>"></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtFranchiseBillingPhoneNumber" runat="server" CssClass="TextField" Columns="30"
                                MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="franchiseBillingNmbrValidator" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredPhoneNumber%>"
                                        ValidationGroup="EditContact" ControlToValidate="txtFranchiseBillingPhoneNumber"
                                        runat="server" Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize8" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnStreet1%>"></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtFranchiseBillingStreet1" runat="server" CssClass="TextField" Columns="30"
                                MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="franchiseBillingStreet1Validator" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredStreet1%>"
                                        ValidationGroup="EditContact" ControlToValidate="txtFranchiseBillingStreet1" runat="server"
                                        Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize9" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnSteet2%>"></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtFranchiseBillingStreet2" runat="server" CssClass="TextField" Columns="30"
                                MaxLength="100"></asp:TextBox>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="txtFranchiseLiveChat" runat="server" Text="City" /><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtFranchiseBillingCity" runat="server" CssClass="TextField" Columns="30"
                                MaxLength="100"></asp:TextBox><div>
                                    <asp:RequiredFieldValidator ID="franchiseBillingCityValidator" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredCity%>"
                                        ValidationGroup="EditContact" ControlToValidate="txtFranchiseBillingCity" runat="server"
                                        Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                                </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize10" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnState%>"></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtFranchiseBillingState" runat="server" Width="40" Columns="10" MaxLength="2"></asp:TextBox><div>
                                <asp:RequiredFieldValidator ID="franchiseBillingStateValidator" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredState%>"
                                    ValidationGroup="EditContact" ControlToValidate="txtFranchiseBillingState" runat="server"
                                    Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize11" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnPostalCode%>"></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtFranchiseBillingPinCode" runat="server" Width="40" Columns="10"></asp:TextBox><div>
                                <asp:RequiredFieldValidator ID="franchiseBillingPinCodeValidator" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredPostalCode%>"
                                    ValidationGroup="EditContact" ControlToValidate="txtFranchiseBillingPinCode" runat="server"
                                    Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            </div>
                    </ContentTemplate>
                </asp:UpdatePanel>

                <asp:UpdatePanel ID="pnlAdminInfo" runat="server">
                    <ContentTemplate>
                        <div class="FieldStyle">
                            <asp:Localize ID="Localize12" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnEmailAddress%>"></asp:Localize>
                            <asp:Label ID="lblAdminEmailastric" runat="server" ForeColor="Red" Text="*" Visible="False"></asp:Label>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtAdminEmail" runat="server" Width="150px"></asp:TextBox>
                            &nbsp;
						<asp:RequiredFieldValidator ID="AdminEmailRequiredValidator" runat="server"
                            ControlToValidate="txtAdminEmail" CssClass="Error" Display="Dynamic"
                            ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredAdminEmail%>" ToolTip="<%$ Resources:ZnodeAdminResource, RequiredEmail%>"
                            ValidationGroup="EditContact" SetFocusOnError="True"></asp:RequiredFieldValidator>
                            <div>
                                <asp:RegularExpressionValidator ID="AdminEmailExpressionValidator" runat="server" ControlToValidate="txtAdminEmail"
                                    ErrorMessage="<%$ Resources:ZnodeAdminResource, RegularEmailAddress%>" Display="Dynamic" ValidationExpression="[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+"
                                    ValidationGroup="EditContact" CssClass="Error" SetFocusOnError="True"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <%--END NEW STUFF--%>
            </div>
        </div>
    </div>
</div>

<div style="float: left; clear: both">
    <br />
    <br />
    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit%>" ID="btnSubmitBottom" ValidationGroup="EditContact" />
    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel%>" CausesValidation="False" ID="btnCancelBottom" />
</div>


<script language="javascript">
    function ResetPasswordMailConfirmation() {
        return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ValidResetPasswordButton")) %>');
    }
</script>
