﻿using System;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;
using System.Linq;

namespace Znode.Engine.Admin.Controls.Default.Accounts
{
    public partial class EditAccount : UserControl
    {
        #region Private Variables
        private int _accountId;
        private int _mode;
        private string _roleName = string.Empty;
        private MembershipProvider _adminMembershipProvider = Membership.Providers["ZNodeAdminMembershipProvider"];
        //TODO: Encryption for itemid - needs to be implemented, but isn't currently done on ...\Znode.Engine.Admin\Controls\Default\Accounts\View.ascx
        private readonly ZNodeEncryption _encrypt = new ZNodeEncryption();
        #endregion

        #region Private Methods

        private void RoleValidation()
        {
            pnlCustomerPricing.Visible = false;
            pnlResetPassword.Visible = false;
            pnlCustomerInformation.Visible = false;
            pnlAccount.Visible = false;
            pnlSubTitle.Visible = false;
            pnlFranchiseAdmin.Visible = false;
            pnlVendorInformation.Visible = false;
            pnlAdminInfo.Visible = false;

            switch (RoleName.ToLower())
            {
                #region CUSTOMER
                default:
                    pnlCustomerPricing.Visible = true;
                    pnlAccount.Visible = true;
                    pnlCustomerInformation.Visible = true;

                    if (_accountId > 0)
                    {
                        lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditCustomerInformation").ToString();
                        lblInstructions.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextEditCustomerInstruction").ToString();
                        pnlResetPassword.Visible = true;
                    }
                    else
                    {
                        lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddCustomerInformation").ToString();
                        lblInstructions.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextAddCustomerInstuction").ToString();
                        pnlResetPassword.Visible = false;
                    }
                    break;
                #endregion
                #region VENDOR
                case "vendor":
                    if (_accountId > 0)
                    {
                        lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditVendorAccount").ToString();
                        lblInstructions.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextEditVendorInstruction").ToString();
                        pnlResetPassword.Visible = true;
                        pnlVendorInformation.Visible = true;
                        pnlCustomerInformation.Visible = false;
                    }
                    else
                    {
                        lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddVendorAccount").ToString();
                        lblInstructions.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextAddVendorInstruction").ToString();
                        pnlResetPassword.Visible = false;
                    }
                    break;
                #endregion
                #region FRANCHISE
                case "franchise":
                    if (_accountId > 0)
                    {
                        pnlFranchiseAdmin.Visible = true;
                        lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditFranchiseAccount").ToString();
                        lblInstructions.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextEditFranchiseInstruction").ToString();
                        pnlResetPassword.Visible = true;
                    }
                    else
                    {
                        lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddFranchiseAccount").ToString();
                        lblInstructions.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextAddFranchiseInstruction").ToString();
                        pnlResetPassword.Visible = false;
                    }
                    break;
                #endregion
                #region ADMIN
                case "admin":
                    pnlAccount.Visible = false;
                    pnlSubTitle.Visible = false;
                    pnlAdminInfo.Visible = true;
                    if (_accountId > 0)
                    {
                        lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditStoreAdministrator").ToString();
                        lblInstructions.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextEditStoreAdminInstruction").ToString();
                        pnlResetPassword.Visible = true;
                    }
                    else
                    {
                        lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddStoreAdministrator").ToString();
                        lblInstructions.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextAddStoreAdminInstruction").ToString();
                        pnlResetPassword.Visible = false;
                    }
                    break;
                #endregion
            }
        }

        /// <summary>
        /// Bind Account Details
        /// </summary>
        private void BindData()
        {
            var userAccountAdmin = new AccountAdmin();
            var userAccount = userAccountAdmin.GetByAccountID(_accountId);

            if (userAccount == null) return;
            // Login Info
            if (userAccount.UserID.HasValue)
            {
                UserID.Enabled = false;
                var user = Membership.GetUser(userAccount.UserID.Value);
                if (user != null) UserID.Text = user.UserName;
            }

            switch (RoleName.ToLower())
            {
                #region CUSTOMER
                default:
                    /**************** < CUSTOMER INFORMATION > *******************/
                    txtExternalAccNumber.Text = string.IsNullOrEmpty(userAccount.ExternalAccountNo)
                                                    ? string.Empty
                                                    : userAccount.ExternalAccountNo;
                    txtCompanyName.Text = Server.HtmlDecode(userAccount.CompanyName);
                    txtWebSite.Text = userAccount.Website;
                    txtSource.Text = Server.HtmlDecode(userAccount.Source);
                    txtDescription.Text = Server.HtmlDecode(userAccount.Description);
                    txtEmail.Text = userAccount.Email;
                    chkOptIn.Checked = userAccount.EmailOptIn;
                    chkCustomerPricing.Checked = userAccount.EnableCustomerPricing.GetValueOrDefault();
                    txtCustom1.Text = Server.HtmlDecode(userAccount.Custom1);
                    txtCustom2.Text = Server.HtmlDecode(userAccount.Custom2);
                    txtCustom3.Text = Server.HtmlDecode(userAccount.Custom3);
                    break;

                #endregion
                #region VENDOR
                case "vendor":
                    /**************** < VENDOR INFORMATION > *******************/
                    var vendorBillingAddress = userAccountAdmin.GetDefaultBillingAddress(userAccount.AccountID);
                    txtBillingEmail.Text = userAccount.Email;
                    txtFranchiseNumber.Text = userAccount.ExternalAccountNo;
                    txtBillingFirstName.Text = vendorBillingAddress.FirstName;
                    txtBillingLastName.Text = vendorBillingAddress.LastName;
                    txtBillingPhoneNumber.Text = vendorBillingAddress.PhoneNumber;
                    txtBillingStreet1.Text = vendorBillingAddress.Street;
                    txtBillingStreet2.Text = vendorBillingAddress.Street1;
                    txtBillingCity.Text = vendorBillingAddress.City;
                    txtBillingCompanyName.Text = vendorBillingAddress.CompanyName;
                    txtBillingState.Text = vendorBillingAddress.StateCode;
                    txtBillingPinCode.Text = vendorBillingAddress.PostalCode;
                    break;

                #endregion
                #region FRANCHISE
                case "franchise":
                    /**************** < FRANCHISE INFORMATION > *******************/
                    //TODO: Needs adjustment to pull data for FranchiseAdmin Form
                    var franchiseBillingAddress = userAccountAdmin.GetDefaultBillingAddress(userAccount.AccountID);
                    //txtFranchiseAdminNumber.Text = userAccount.ExternalAccountNo;
                    //txtFranchiseCompanyName.Text = storeName;
                    //ddlTheme.SelectedIndex = portalCatalog.ThemeID ?? 1;
                    //portalCatalog.CSSID = ddlTheme.SelectedIndex;
                    //txtStoreUrl.Visible = domain.DomainName; 
                    //sitelogo here
                    txtFranchiseAdminBillingFirstName.Text = franchiseBillingAddress.FirstName;
                    txtFranchiseBillingLastName.Text = franchiseBillingAddress.LastName;
                    txtFranchiseAdminBillingEmailID.Text = userAccount.Email;
                    txtFranchiseBillingPhoneNumber.Text = franchiseBillingAddress.PhoneNumber;
                    txtFranchiseBillingStreet1.Text = franchiseBillingAddress.Street;
                    txtFranchiseBillingStreet2.Text = franchiseBillingAddress.Street1;
                    txtFranchiseBillingCity.Text = franchiseBillingAddress.City;
                    txtFranchiseBillingState.Text = franchiseBillingAddress.StateCode;
                    txtFranchiseBillingPinCode.Text = franchiseBillingAddress.PostalCode;
                    break;

                #endregion
                #region ADMIN
                case "admin":
                    /**************** < ADMINISTRATOR INFORMATION > *******************/
                    txtAdminEmail.Text = userAccount.Email;
                    break;

                #endregion
            }

        }

        #endregion

        #region Protected Methods & Events

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //Adding UI features for given buttons
            btnSubmitTop.Attributes.Add("onmouseover",
                                        "this.src='" +
                                        Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnSubmitTop.Attributes.Add("onmouseout",
                                        "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit.gif") + "'");
            btnCancelTop.Attributes.Add("onmouseover",
                                        "this.src='" +
                                        Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancelTop.Attributes.Add("onmouseout",
                                        "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel.gif") + "'");
            btnResetPassword.Attributes.Add("onmouseover",
                                            "this.src='" +
                                            Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnResetPassword.Attributes.Add("onmouseout",
                                            "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit.gif") +
                                            "'");
            btnSubmitBottom.Attributes.Add("onmouseover",
                                           "this.src='" +
                                           Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnSubmitBottom.Attributes.Add("onmouseout",
                                           "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit.gif") +
                                           "'");
            btnCancelBottom.Attributes.Add("onmouseover",
                                           "this.src='" +
                                           Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancelBottom.Attributes.Add("onmouseout",
                                           "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel.gif") +
                                           "'");

            _accountId = HttpContext.Current.Request.Params["itemid"] == null ? 0 : (Convert.ToInt32(Request.Params["itemid"]));

            _mode = HttpContext.Current.Request.Params["mode"] == null ? 0 : int.Parse(Request.Params["mode"]);

            if (Request.Params["pagefrom"] != null)
            {
                RoleName = Request.Params["pagefrom"];

                // To validate siteadmin account
                if (RoleName == "ADMIN" || RoleName == "admin")
                {
                    lblAdminEmailastric.Visible = true;
                    lblEMailastric.Visible = true;
                    UseridRequired.Enabled = true;
                    EmailAddressRequired.Enabled = true;
                    lblAdminEmailastric.Enabled = true;
                }
                else if (RoleName.ToLower() == "franchise")
                {
                    CmpnyNameRequired.Enabled = true;
                }

            }

            if (Page.IsPostBack) return;

            BindData();
            RoleValidation();
        }
        #endregion

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            var logger = new ZNodeLogging();
            var accountAdmin = new AccountAdmin();
            var account = accountAdmin.GetByAccountID(_accountId);
            var transactionManager = ConnectionScope.CreateTransaction();

            //TODO: UPDATE ZNodeUserAccount
            if (_accountId != 0 && _accountId > 0)
            {
                var userAccount = new ZNodeUserAccount(account);

                logger.LogActivityTimerStart();

                var accountUpdated = UpdateZNodeUserAccount(userAccount);

                if (!accountUpdated)
                {
                    if (transactionManager.IsOpen)
                        transactionManager.Rollback();

                    if (string.IsNullOrEmpty(lblErrorMsg.Text))
                        lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountUpdate").ToString();

                    logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateFailed, UserID.Text.Trim());
                    return;
                }

                MembershipUser existingMembershipUser;

                if (userAccount.UserID.HasValue)
                {
                    existingMembershipUser = AdminMembershipProvider.GetUser(userAccount.UserID, false);

                    if (existingMembershipUser == null)
                    {
                        if (transactionManager.IsOpen)
                            transactionManager.Rollback();

                        if (string.IsNullOrEmpty(lblErrorMsg.Text))
                            lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountMembership").ToString();
                        logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateFailed, UserID.Text.Trim());
                        return;
                    }
                }
                else //User won't have value if created by QuickOrder or Demosite Non-reg checkout
                {
                    if (UserID.Text.Trim().Length == 0)
                    {
                        lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "RequiredUserID").ToString();
                        return;
                    }

                    // Check if loginName already exists
                    if (!userAccount.IsLoginNameAvailable(ZNodeConfigManager.SiteConfig.PortalID, UserID.Text.Trim()))
                    {
                        lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "LoginExistError").ToString();
                        return;
                    }

                    var generatedPassword = Membership.GeneratePassword(8, 1);
                    var createStatus = MembershipCreateStatus.ProviderError;

                    existingMembershipUser = AdminMembershipProvider.CreateUser(Server.HtmlEncode(UserID.Text.Trim()),
                                                                       generatedPassword, txtEmail.Text.Trim(),
                                                                       null, null, true, Guid.NewGuid(),
                                                                       out createStatus);

                    if (existingMembershipUser == null || (createStatus != MembershipCreateStatus.Success || existingMembershipUser.ProviderUserKey == null))
                    {
                        lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorMembershipCreate").ToString();
                        return;
                    }

                    try
                    {
                        //Remove password log
                        new AccountHelper().DeletePasswordLogByUserId(((Guid)existingMembershipUser.ProviderUserKey).ToString());
                        //Add password log
                        ZNodeUserAccount.LogPassword((Guid)existingMembershipUser.ProviderUserKey, generatedPassword);
                    }
                    catch (Exception ex)
                    {
                        if (transactionManager.IsOpen)
                            transactionManager.Rollback();

                        ZNodeLoggingBase.LogMessage(ex.Message);
                        lblErrorMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource","ErrorPasswordLog").ToString(), ex.Message);
                        return;
                    }

                    userAccount.UserID = new Guid(existingMembershipUser.ProviderUserKey.ToString());
                }

                //Save
                userAccount.UpdateUserAccount();

                var associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogUpdateAccount").ToString(), UserID.Text.Trim());

                //Redirects / MembershipUpdate / Log Activity / Commit Transaction
                switch (RoleName.ToLower())
                {
                    #region CUSTOMER
                    default:
                        //Customer Update MembershipUser EmailAddress
                        existingMembershipUser.Email = txtEmail.Text.Trim();

                        //Update db with MembershipUser Data
                        Membership.UpdateUser(existingMembershipUser);

                        //End Timer
                        logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateSuccess, UserID.Text.Trim());

                        ZNodeLoggingBase.LogActivity((int)ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(),
                                                             null, null, null, null, associateName, "Account");
                        transactionManager.Commit();

                        Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=" + _mode + "&itemid=" +
                                          userAccount.AccountID + "&pagefrom=customer");
                        break;
                    #endregion
                    #region VENDOR
                    case "vendor":
                        //Update MembershipProvider with Vendor EmailAddress
                        existingMembershipUser.Email = txtBillingEmail.Text.Trim();

                        //Update db with MembershipUser Data
                        Membership.UpdateUser(existingMembershipUser);

                        //End Timer
                        logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateSuccess, UserID.Text.Trim());

                        ZNodeLoggingBase.LogActivity((int)ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(),
                                     null, null, null, null, associateName, "Account");

                        transactionManager.Commit();

                        Response.Redirect("~/Secure/Vendors/VendorAccounts/View.aspx?mode=" + _mode + "&itemid=" +
                                          userAccount.AccountID + "&pagefrom=vendor");
                        break;
                    #endregion
                    #region FRANCHISE
                    case "franchise":
                        //Update FranchiseAdmin MembershipUser EmailAddress
                        existingMembershipUser.Email = txtFranchiseAdminBillingEmailID.Text.Trim();

                        //Update db with MembershipUser Data
                        Membership.UpdateUser(existingMembershipUser);

                        //End Timer
                        logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateSuccess, UserID.Text.Trim());
                        ZNodeLoggingBase.LogActivity((int)ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(),
                                     null, null, null, null, associateName, "Account");

                        transactionManager.Commit();

                        Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/View.aspx?mode=" + _mode + "&itemid=" +
                                          userAccount.AccountID + "&pagefrom=franchise");
                        break;
                    #endregion
                    #region ADMIN
                    case "admin":
                        //Customer Update MembershipUser EmailAddress
                        existingMembershipUser.Email = txtAdminEmail.Text.Trim();

                        //Update db with MembershipUser Data
                        Membership.UpdateUser(existingMembershipUser);

                        //End Timer
                        logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateSuccess, UserID.Text.Trim());
                        ZNodeLoggingBase.LogActivity((int)ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(),
                                     null, null, null, null, associateName, "Account");

                        transactionManager.Commit();

                        Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?mode=" + _mode + "&itemid=" +
                                          userAccount.AccountID + "&pagefrom=admin");
                        break;
                    #endregion
                }

            }
            //TODO: ADD ZNodeUserAccount
            else if (_accountId == 0)
            {
                var userAccount = new Account();

                logger.LogActivityTimerStart();

                var accountAdded = AddZNodeUserAccount(userAccount);

                if (!accountAdded)
                {
                    if (transactionManager.IsOpen)
                        transactionManager.Rollback();

                    if (string.IsNullOrEmpty(lblErrorMsg.Text))
                        lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorUserAdd").ToString();
                    logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateFailed, UserID.Text.Trim());
                    return;
                }

                var emailSent = SendNewUserEmail(userAccount.AccountID);

                if (!emailSent)
                {
                    //If email fails, still save the user & provide error message
                    if (transactionManager.IsOpen)
                        transactionManager.Commit();

                    if (string.IsNullOrEmpty(lblErrorMsg.Text))
                        lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCredential").ToString();

                    logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateFailed, UserID.Text.Trim());
                    return;
                }

                var associateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogCreateAccount").ToString(), UserID.Text.Trim());

                //Redirect / Log Activity / Commit Transaction
                switch (RoleName.ToLower())
                {
                    #region CUSTOMER
                    default:
                        logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateSuccess, UserID.Text.Trim());
                        ZNodeLoggingBase.LogActivity((int)ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(),
                                     null, null, null, null, associateName, "Account");
                        transactionManager.Commit();
                        Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=" + _mode + "&itemid=" +
                        userAccount.AccountID + "&pagefrom=customer");
                        break;
                    #endregion
                    #region VENDOR
                    case "vendor":
                        logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateSuccess, UserID.Text.Trim());
                        ZNodeLoggingBase.LogActivity((int)ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(),
                                     null, null, null, null, associateName, "Account");
                        transactionManager.Commit();
                        Response.Redirect("~/Secure/Vendors/VendorAccounts/View.aspx?mode=" + _mode + "&itemid=" +
                        userAccount.AccountID + "&pagefrom=vendor");
                        break;
                    #endregion
                    #region FRANCHISE
                    case "franchise":
                        logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateSuccess, UserID.Text.Trim());
                        ZNodeLoggingBase.LogActivity((int)ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(),
                                                     null, null, null, null, associateName, "Account");
                        transactionManager.Commit();
                        Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/View.aspx?mode=" + _mode + "&itemid=" +
                        userAccount.AccountID + "&pagefrom=franchise");
                        break;
                    #endregion
                    #region ADMIN
                    case "admin":
                        logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateSuccess, UserID.Text.Trim());
                        ZNodeLoggingBase.LogActivity((int)ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(),
                             null, null, null, null, associateName, "Account");
                        transactionManager.Commit();
                        Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?mode=" + _mode + "&itemid=" +
                        userAccount.AccountID + "&pagefrom=admin");
                        break;
                    #endregion
                }

            }
            else
            {
                if (transactionManager.IsOpen)
                    transactionManager.Rollback();

                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorInvalidAccount").ToString();
                logger.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateFailed, UserID.Text.Trim());
            }
        }

        /// <summary>
        /// Reset Password Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contans event data.</param>
        protected void BtnResetPassword_Click(object sender, EventArgs e)
        {
            var transactionManager = ConnectionScope.CreateTransaction();
            var helperAccess = new AccountHelper();
            var accountAdmin = new AccountAdmin();

            if (_accountId <= 0)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorResetPassword").ToString();
                ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                return;
            }

            var userAccount = accountAdmin.GetByAccountID(_accountId);

            if (userAccount == null || !userAccount.UserID.HasValue)
            {
                if (transactionManager.IsOpen)
                    transactionManager.Rollback();

                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSelectUserAccount").ToString();
                ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                return;
            }

            var adminMembershipProvider = Membership.Providers["ZNodeAdminMembershipProvider"] ?? Membership.Provider;

            var currentUser = adminMembershipProvider.GetUser(userAccount.UserID.Value, false);

            if (currentUser == null)
            {
                if (transactionManager.IsOpen)
                    transactionManager.Rollback();

                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorMembershipRetrieve").ToString();
                ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                return;
            }

            var currentPassword = currentUser.GetPassword();
            var newlyCreatedPassword = Membership.GeneratePassword(8, 1);
            var passwordChanged = currentUser.ChangePassword(currentPassword, newlyCreatedPassword);

            if (!passwordChanged)
            {
                if (transactionManager.IsOpen)
                    transactionManager.Rollback();

                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorChangePassword").ToString();
                ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                return;
            }

            accountAdmin.UpdateLastPasswordChangedDate(userAccount.UserID.ToString());

            var userUpdated = accountAdmin.Update(userAccount);

            if (!userUpdated)
            {
                if (transactionManager.IsOpen)
                    transactionManager.Rollback();

                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPasswordUpdate").ToString();
                ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                return;
            }

            if (currentUser.ProviderUserKey == null)
            {
                if (transactionManager.IsOpen)
                    transactionManager.Rollback();

                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountRetrieve").ToString();
                ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                return;
            }

            try
            {
                //Remove password log
                helperAccess.DeletePasswordLogByUserId(((Guid)currentUser.ProviderUserKey).ToString());
                //Add password log
                ZNodeUserAccount.LogPassword((Guid)currentUser.ProviderUserKey, newlyCreatedPassword);
            }
            catch (Exception ex)
            {
                if (transactionManager.IsOpen)
                    transactionManager.Rollback();

                ZNodeLoggingBase.LogMessage(ex.Message);
                lblErrorMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource","ErrorPasswordLog").ToString(), ex.Message);
                return;
            }

            var emailSuccess = SendPasswordResetEmail(userAccount.AccountID);

            if (!emailSuccess)
            {
                if (transactionManager.IsOpen)
                    transactionManager.Rollback();

                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorPasswordEmail").ToString();
                ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                return;
            }

            //Redirect User
            switch (RoleName.ToLower())
            {
                #region CUSTOMER
                default:
                    transactionManager.Commit();
                    Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=" + _mode + "&itemid=" +
                    userAccount.AccountID + "&pagefrom=customer");
                    break;
                #endregion
                #region VENDOR
                case "vendor":
                    transactionManager.Commit();
                    Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=" + _mode + "&itemid=" +
                    userAccount.AccountID + "&pagefrom=vendor");
                    break;
                #endregion
                #region FRANCHISE
                case "franchise":
                    transactionManager.Commit();
                    Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=" + _mode + "&itemid=" +
                    userAccount.AccountID + "&pagefrom=franchise");
                    break;
                #endregion
                #region ADMIN
                case "admin":
                    transactionManager.Commit();
                    Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=" + _mode + "&itemid=" +
                    userAccount.AccountID + "&pagefrom=admin");
                    break;
                #endregion
            }
        }

        /// <summary>
        /// Sends a new password in an email to the existing account by id
        /// </summary>
        /// <param name="accountId">Takes an integer for the accountId parameter.</param>
        protected bool SendPasswordResetEmail(int accountId)
        {
            var accountAdmin = new AccountAdmin();
            var portalId = ZNodeConfigManager.SiteConfig.PortalID;
            var portalAdmin = new PortalAdmin();
            var portal = portalAdmin.GetByPortalId(portalId);
            var storeName = portal.StoreName;
            var currentCulture = ZNodeCatalogManager.CultureInfo;
            var franchiseStoreDomain = new DomainService().GetByPortalID(portalId).FirstOrDefault();

            if (franchiseStoreDomain == null)
            {
                lblErrorMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPasswordResetNotification").ToString(), storeName);
                ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                return false;
            }
            var account = accountAdmin.GetByAccountID(accountId);

            if (account == null || !account.UserID.HasValue) return false;

            var userToEmail = AdminMembershipProvider.GetUser(account.UserID.Value, false);

            if (userToEmail == null)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorMembershipFind").ToString();
                ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                return false;
            }

            if (String.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SMTPServer))
            {
                lblErrorMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPasswordResetSMTP").ToString(), storeName);
                ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                return false;
            }

            var senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            var subject = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextPasswordResetSubject").ToString(), storeName);
            var toAddress = account.Email;

            var defaultTemplatePath =
                Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "AdminResetPassword_en.htm"));

            var templatePath = currentCulture != string.Empty
                                   ? Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "AdminResetPassword_" +
                                                    currentCulture + ".htm")
                                   : Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath,
                                                                 "AdminResetPassword_en.htm"));
            // Check for language specific file existence.
            if (!File.Exists(templatePath))
            {
                // Check the default template file existence.
                if (!File.Exists(defaultTemplatePath))
                {
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorTemplateFile").ToString();
                    ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                    return false;
                }
                templatePath = defaultTemplatePath;
            }
            try
            {
                string body;
                using (var streamReader = new StreamReader(templatePath))
                {
                    body = streamReader.ReadToEnd();
                }

                var userName = userToEmail.UserName;
                var regularExpName = new Regex("#UserName#", RegexOptions.IgnoreCase);
                body = regularExpName.Replace(body, userName);

                var userPassword = userToEmail.GetPassword();
                var regularExpPassword = new Regex("#Password#", RegexOptions.IgnoreCase);
                body = regularExpPassword.Replace(body, userPassword);

                var regularUrl = new Regex("#Url#", RegexOptions.IgnoreCase);


                switch (RoleName.ToLower())
                {
                    #region CUSTOMER
                    default:
                        var loginUrl = string.Empty;
                        AccountProfileService actprofileService = new AccountProfileService();
                        var accountProfile = actprofileService.GetByProfileID(int.Parse(account.ProfileID.ToString()));
                        if (accountProfile != null)
                        {
                            var portalProfile = new PortalProfileService().GetByProfileID(int.Parse(accountProfile.FirstOrDefault().ProfileID.ToString()));

                            if (portalProfile != null)
                            {
                                var storeDomain = new DomainService().GetByPortalID(portalProfile.FirstOrDefault().PortalID)[0];
                                if (storeDomain != null)
                                {
                                    loginUrl = "http://" + storeDomain.DomainName + "/login.aspx";
                                }
                            }
                        }
                        body = regularUrl.Replace(body, loginUrl);
                        break;
                    #endregion
                    #region VENDOR
                    case "vendor":
                        var vendorLoginUrl = WebConfigurationManager.AppSettings["AdminWebsiteUrl"] + "/malladmin";
                        if (string.IsNullOrEmpty(WebConfigurationManager.AppSettings["AdminWebsiteUrl"]))
                        {
                            vendorLoginUrl = "http://" + ZNodeConfigManager.DomainConfig.DomainName + "/malladmin";
                        }
                        body = regularUrl.Replace(body, vendorLoginUrl);
                        break;
                    #endregion
                    #region FRANCHISE
                    case "franchise":
                        var franchiseLoginUrl = WebConfigurationManager.AppSettings["AdminWebsiteUrl"] + "/franchiseadmin";
                        if (string.IsNullOrEmpty(WebConfigurationManager.AppSettings["AdminWebsiteUrl"]))
                        {
                            franchiseLoginUrl = "http://" + ZNodeConfigManager.DomainConfig.DomainName + "/franchiseadmin";
                        }
                        body = regularUrl.Replace(body, franchiseLoginUrl);
                        break;
                    #endregion
                    #region ADMIN
                    case "admin":
                        var adminLoginUrl = WebConfigurationManager.AppSettings["AdminWebsiteUrl"];
                        if (string.IsNullOrEmpty(WebConfigurationManager.AppSettings["AdminWebsiteUrl"]))
                        {
                            adminLoginUrl = "http://" + ZNodeConfigManager.DomainConfig.DomainName;
                        }
                        body = regularUrl.Replace(body, adminLoginUrl);
                        break;
                    #endregion
                }

                ZNodeEmail.SendEmail(toAddress, senderEmail, String.Empty, subject, body, true);
            }
            catch (Exception ex)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorResetPasswordEmail").ToString();
                ZNodeLoggingBase.LogMessage(ex.Message);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Sends a new password in an email to the newly created account by id
        /// </summary>
        /// <param name="accountId">Takes an integer for the accountId parameter.</param>
        protected bool SendNewUserEmail(int accountId)
        {
            var accountAdmin = new AccountAdmin();
            var portalId = ZNodeConfigManager.SiteConfig.PortalID;
            var portalAdmin = new PortalAdmin();
            var portal = portalAdmin.GetByPortalId(portalId);
            var storeName = portal.StoreName;
            var franchiseStoreDomain = new DomainService().GetByPortalID(portalId).FirstOrDefault();

            var account = accountAdmin.GetByAccountID(accountId);

            if (account == null || !account.UserID.HasValue) return false;

            var user = AdminMembershipProvider.GetUser(account.UserID.Value, false);

            if (user == null)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCreateEmail").ToString();
                ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                return false;
            }

            if (String.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SMTPServer))
            {
                lblErrorMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCreateSMTP").ToString(), storeName);
                ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                return false;
            }
            if (franchiseStoreDomain == null)
            {
                lblErrorMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCreateURL").ToString(), storeName);
                ZNodeLoggingBase.LogMessage(lblErrorMsg.Text);
                return false;
            }
            var senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            var subject = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextNewUserCreation").ToString(), storeName);
            var toAddress = account.Email;

            var currentCulture = ZNodeCatalogManager.CultureInfo;

            var defaultTemplatePath =
                Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "NewUserAccount_en.htm"));

            var templatePath = currentCulture != string.Empty
                                   ? Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "NewUserAccount_" +
                                                    currentCulture + ".htm")
                                   : Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath,
                                                                 "NewUserAccount_en.htm"));

            // Check for language specific file existence.
            if (!File.Exists(templatePath))
            {
                // Check the default template file existence.
                if (!File.Exists(defaultTemplatePath))
                {
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCreateTemplateFile").ToString();
                    return false;
                }
                templatePath = defaultTemplatePath;
            }
            try
            {
                string body;
                using (var streamReader = new StreamReader(templatePath))
                {
                    body = streamReader.ReadToEnd();
                    
                }

                var userName = user.UserName;
                var regularExpName = new Regex("#UserName#", RegexOptions.IgnoreCase);
                body = regularExpName.Replace(body, userName);

                var userPassword = user.GetPassword();
                var regularExpPassword = new Regex("#Password#", RegexOptions.IgnoreCase);
                body = regularExpPassword.Replace(body, userPassword);

                var regularUrl = new Regex("#Url#", RegexOptions.IgnoreCase);

                switch (RoleName.ToLower())
                {
                    #region CUSTOMER
                    default:
                        var loginUrl = string.Empty;
                        AccountProfileService actprofileService = new AccountProfileService();
                        var accountProfile = actprofileService.GetByProfileID(int.Parse(account.ProfileID.ToString()));
                        if (accountProfile != null)
                        {
                            var portalProfile = new PortalProfileService().GetByProfileID(int.Parse(accountProfile.FirstOrDefault().ProfileID.ToString()));

                            if (portalProfile != null)
                            {
                                var storeDomain = new DomainService().GetByPortalID(portalProfile.FirstOrDefault().PortalID)[0];
                                if (storeDomain != null)
                                {
                                    loginUrl = "http://" + storeDomain.DomainName + "/login.aspx";
                                }
                            }
                        }
                        body = regularUrl.Replace(body, loginUrl);
                        break;
                    #endregion
                    #region VENDOR
                    case "vendor":
                        var vendorLoginUrl = WebConfigurationManager.AppSettings["AdminWebsiteUrl"] + "/malladmin";
                        if (string.IsNullOrEmpty(WebConfigurationManager.AppSettings["AdminWebsiteUrl"]))
                        {
                            vendorLoginUrl = "http://" + ZNodeConfigManager.DomainConfig.DomainName + "/malladmin";
                        }
                        body = regularUrl.Replace(body, vendorLoginUrl);
                        break;
                    #endregion
                    #region FRANCHISE
                    case "franchise":
                        var franchiseLoginUrl = WebConfigurationManager.AppSettings["AdminWebsiteUrl"] + "/franchiseadmin";
                        if (string.IsNullOrEmpty(WebConfigurationManager.AppSettings["AdminWebsiteUrl"]))
                        {
                            franchiseLoginUrl = "http://" + ZNodeConfigManager.DomainConfig.DomainName + "/franchiseadmin";
                        }
                        body = regularUrl.Replace(body, franchiseLoginUrl);
                        break;
                    #endregion
                    #region ADMIN
                    case "admin":
                        var adminLoginUrl = WebConfigurationManager.AppSettings["AdminWebsiteUrl"];
                        if (string.IsNullOrEmpty(WebConfigurationManager.AppSettings["AdminWebsiteUrl"]))
                        {
                            adminLoginUrl = "http://" + ZNodeConfigManager.DomainConfig.DomainName;
                        }
                        body = regularUrl.Replace(body, adminLoginUrl);
                        break;
                    #endregion
                }

                ZNodeEmail.SendEmail(toAddress, senderEmail, String.Empty, subject, body, true);
            }
            catch (Exception ex)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCreatePasswordSMTP").ToString();
                ZNodeLoggingBase.LogMessage(ex.Message);
                return false;
            }

            return true;
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (_accountId > 0)
            {
                switch (RoleName.ToLower())
                {
                    case "admin":
                        Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?mode=" + this._mode + "&itemid=" +
                                          this._accountId + "&pagefrom=admin");
                        break;
                    case "franchise":
                        Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/View.aspx?mode=" + this._mode + "&itemid=" +
                                          this._accountId + "&pagefrom=franchise");
                        break;
                    case "vendor":
                        Response.Redirect("~/Secure/Vendors/VendorAccounts/View.aspx?mode=" + this._mode + "&itemid=" + this._accountId +
                                          "&pagefrom=vendor");
                        break;
                    default:
                        Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/View.aspx?mode=" + this._mode + "&itemid=" +
                                          this._accountId + "&pagefrom=customer");
                        break;
                }
            }
            else
            {
                switch (RoleName.ToLower())
                {
                    case "admin":
                        Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/Default.aspx");
                        break;
                    case "franchise":
                        Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/Default.aspx");
                        break;
                    case "vendor":
                        Response.Redirect("~/Secure/Vendors/VendorAccounts/Default.aspx");
                        break;
                    default:
                        Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/Default.aspx");
                        break;
                }
            }
        }

        /// <summary>
        /// Adds the current Account given by param
        /// </summary>
        /// <param name="userAccount"></param>
        /// <returns>Returns true if add is successful.</returns>
        protected bool AddZNodeUserAccount(Account userAccount)
        {
            MembershipCreateStatus createStatus;
            var znodeUserAccount = new ZNodeUserAccount();
            var accountAdmin = new AccountAdmin();
            var accountService = new AccountService();
            var accountProfileService = new AccountProfileService();
            var addressService = new AddressService();
            var profileId = ZNodeProfile.DefaultRegisteredProfileId;
            var newlyCreatedPassword = Membership.GeneratePassword(8, 1);

            userAccount.CreateDte = DateTime.Now;
            userAccount.CreateUser = HttpContext.Current.User.Identity.Name;
            userAccount.ActiveInd = true;
            userAccount.UpdateDte = DateTime.Now;

            if (UserID.Text.Trim().Length <= 0)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUserID").ToString();
                return false;
            }

            if (profileId == 0)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountProfile").ToString();
                return false;
            }

            //TODO: Database can have emptystring for ExternalAccountNo, so adding length check, then proceeding
            if (txtExternalAccNumber.Text.Length > 0)
            {
                TList<Account> account = accountService.Find(string.Format("ExternalAccountNo='{0}'", txtExternalAccNumber.Text.Trim()));

                if (account.Count > 0)
                {
                    if (account[0].UserID != null)
                    {
                        lblErrorMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorExternalNumberAssociate").ToString(), txtExternalAccNumber.Text, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);
                        return false;
                    }
                }
            }

            // Check if loginName already exists
            if (!znodeUserAccount.IsLoginNameAvailable(ZNodeConfigManager.SiteConfig.PortalID, UserID.Text.Trim()))
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUserIdLoginExist").ToString();
                return false;
            }

            if (string.IsNullOrEmpty(txtEmail.Text))
                txtEmail.Text = txtAdminEmail.Text.Trim();

            var newMembershipUser = AdminMembershipProvider.CreateUser(Server.HtmlEncode(UserID.Text.Trim()),
                                                                       newlyCreatedPassword.Trim(), txtEmail.Text.Trim(),
                                                                       null, null, true, Guid.NewGuid(),
                                                                       out createStatus);

            if (newMembershipUser == null ||
                (createStatus != MembershipCreateStatus.Success || newMembershipUser.ProviderUserKey == null))
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorMembershipCreate").ToString();
                return false;
            }

            //TODO: Removed due to Logging issue with password, interupting prompt for reseting pwd & questions
            //try
            //{
            //	// Log password
            //	ZNodeUserAccount.LogPassword((Guid)newMembershipUser.ProviderUserKey, newlyCreatedPassword);
            //}
            //catch (Exception ex)
            //{
            //	lblErrorMsg.Text = ex.Message + " Logging of the password failed upon insertion.";
            //	return false;
            //}

            //Save current account with Role
            switch (RoleName.ToLower())
            {
                #region CUSTOMER

                default:
                    userAccount.ActiveInd = true;
                    userAccount.ProfileID = profileId;
                    userAccount.Email = txtEmail.Text;
                    userAccount.EmailOptIn = chkOptIn.Checked;
                    userAccount.EnableCustomerPricing = chkCustomerPricing.Checked;
                    userAccount.ExternalAccountNo = string.IsNullOrEmpty(txtExternalAccNumber.Text.Trim())
                                                        ? null
                                                        : Server.HtmlEncode(txtExternalAccNumber.Text.Trim());
                    userAccount.Description = Server.HtmlEncode(txtDescription.Text);
                    userAccount.CompanyName = Server.HtmlEncode(txtCompanyName.Text);
                    userAccount.Website = Server.HtmlEncode(txtWebSite.Text.Trim());
                    userAccount.Source = Server.HtmlEncode(txtSource.Text.Trim());
                    userAccount.AccountProfileCode = userAccount.AccountProfileCode ??
                                                     DBNull.Value.ToString(CultureInfo.InvariantCulture);
                    userAccount.CreateUser = HttpContext.Current.User.Identity.Name;
                    userAccount.CreateDte = userAccount.CreateDte;
                    userAccount.UpdateUser = HttpContext.Current.User.Identity.Name;
                    userAccount.UpdateDte = DateTime.Now;
                    userAccount.UserID = new Guid(newMembershipUser.ProviderUserKey.ToString());
                    userAccount.Custom1 = Server.HtmlEncode(txtCustom1.Text.Trim());
                    userAccount.Custom2 = Server.HtmlEncode(txtCustom2.Text.Trim());
                    userAccount.Custom3 = Server.HtmlEncode(txtCustom3.Text.Trim());
                    userAccount.AccountID = _accountId;

                    accountService.Insert(userAccount);

                    var customerAddress = accountAdmin.GetDefaultBillingAddress(userAccount.AccountID) ??
                                  new ZNode.Libraries.DataAccess.Entities.Address
                                  {
                                      IsDefaultBilling = true,
                                      IsDefaultShipping = true,
                                      AccountID = userAccount.AccountID,
                                  };

                    //Setting Default Values
                    customerAddress.FirstName = String.Empty;
                    customerAddress.LastName = String.Empty;
                    customerAddress.PhoneNumber = String.Empty;
                    customerAddress.Street = String.Empty;
                    customerAddress.Street1 = String.Empty;
                    customerAddress.City = String.Empty;
                    customerAddress.CompanyName = String.Empty;
                    customerAddress.StateCode = String.Empty;
                    customerAddress.PostalCode = String.Empty;
                    customerAddress.CountryCode = "US";
                    customerAddress.Name = "Default Address";

                    addressService.Insert(customerAddress);

                    var customerAccountProfile = new AccountProfile { AccountID = userAccount.AccountID, ProfileID = profileId };

                    accountProfileService.Insert(customerAccountProfile);

                    if (RoleName.ToLower() != "customer")
                    {
                        Roles.AddUserToRole(UserID.Text.Trim(), "CUSTOMER SERVICE REP");
                    }

                    var customerProfile = ProfileCommon.Create(UserID.Text, true);

                    customerProfile.StoreAccess = "AllStores";

                    customerProfile.Save();

                    accountService.Update(userAccount);
                    break;
                #endregion
                #region VENDOR
                case "vendor":
                    userAccount.UserID = (Guid)newMembershipUser.ProviderUserKey;
                    userAccount.CompanyName = txtBillingCompanyName.Text;
                    userAccount.ExternalAccountNo = txtFranchiseNumber.Text.Trim();
                    userAccount.Email = txtBillingEmail.Text.Trim();

                    userAccount.CreateDte = DateTime.Now;
                    userAccount.UpdateDte = DateTime.Now;
                    userAccount.CreateUser = HttpContext.Current.User.Identity.Name;

                    accountService.Insert(userAccount);

                    var vendorAddress = accountAdmin.GetDefaultBillingAddress(userAccount.AccountID) ??
                                  new ZNode.Libraries.DataAccess.Entities.Address
                                  {
                                      IsDefaultBilling = true,
                                      IsDefaultShipping = true,
                                      AccountID = userAccount.AccountID
                                  };

                    // Copy info to billing                
                    vendorAddress.FirstName = txtBillingFirstName.Text;
                    vendorAddress.LastName = txtBillingLastName.Text;
                    vendorAddress.PhoneNumber = txtBillingPhoneNumber.Text.Trim();
                    vendorAddress.Street = txtBillingStreet1.Text.Trim();
                    vendorAddress.Street1 = txtBillingStreet2.Text.Trim();
                    vendorAddress.City = txtBillingCity.Text.Trim();
                    vendorAddress.CompanyName = txtBillingCompanyName.Text.Trim();
                    vendorAddress.StateCode = txtBillingState.Text.Trim();
                    vendorAddress.PostalCode = txtBillingPinCode.Text;

                    // Default country code for United States
                    vendorAddress.CountryCode = "US";
                    vendorAddress.Name = "Default Address";

                    addressService.Insert(vendorAddress);

                    // Add Account Profile
                    var vendorAccountProfile = new AccountProfile { AccountID = userAccount.AccountID, ProfileID = profileId };

                    accountProfileService.Insert(vendorAccountProfile);

                    Roles.AddUserToRole(UserID.Text.Trim(), "VENDOR");

                    // Create an Profile for the selected user
                    var vendorProfile = ProfileCommon.Create(UserID.Text, true);

                    // Properties Value
                    vendorProfile.StoreAccess = ZNodeConfigManager.SiteConfig.PortalID.ToString(CultureInfo.InvariantCulture);

                    // Save profile - must be done since we explicitly created it 
                    vendorProfile.Save();

                    accountService.Update(userAccount);
                    break;
                #endregion
                #region FRANCHISE
                case "franchise":
                    //Response.Redirect("~/Secure/Vendors/VendorAccounts/View.aspx?mode=0&itemid=" + userAccount.AccountID + "&pagefrom=franchise");
                    break;
                #endregion
                #region ADMIN

                case "admin":
                    userAccount.UserID = (Guid)newMembershipUser.ProviderUserKey;
                    userAccount.Email = txtAdminEmail.Text.Trim();
                    userAccount.CreateDte = DateTime.Now;
                    userAccount.UpdateDte = DateTime.Now;
                    userAccount.CreateUser = HttpContext.Current.User.Identity.Name;
                    userAccount.AccountID = _accountId;
                    userAccount.ProfileID = profileId;

                    accountService.Insert(userAccount);

                    var adminAddress = accountAdmin.GetDefaultBillingAddress(userAccount.AccountID) ??
                                  new ZNode.Libraries.DataAccess.Entities.Address
                                  {
                                      IsDefaultBilling = true,
                                      IsDefaultShipping = true,
                                      AccountID = userAccount.AccountID
                                  };

                    //Set Defaults              
                    adminAddress.FirstName = String.Empty;
                    adminAddress.LastName = String.Empty;
                    adminAddress.PhoneNumber = String.Empty;
                    adminAddress.Street = String.Empty;
                    adminAddress.Street1 = String.Empty;
                    adminAddress.City = String.Empty;
                    adminAddress.CompanyName = String.Empty;
                    adminAddress.StateCode = String.Empty;
                    adminAddress.PostalCode = String.Empty;

                    //Default country code for United States
                    adminAddress.CountryCode = "US";
                    adminAddress.Name = "Default Address";

                    addressService.Insert(adminAddress);

                    //Add Account Profile
                    var adminAccountProfile = new AccountProfile { AccountID = userAccount.AccountID, ProfileID = profileId };

                    accountProfileService.Insert(adminAccountProfile);

                    Roles.AddUserToRole(UserID.Text.Trim(), "ADMIN");

                    // Create an Profile for the selected user
                    var adminProfile = ProfileCommon.Create(UserID.Text, true);

                    // Properties Value
                    adminProfile.StoreAccess = "AllStores";

                    // Save profile - must be done since we explicitly created it 
                    adminProfile.Save();

                    accountService.Update(userAccount);
                    break;
                #endregion
            }

            return true;
        }

        /// <summary>
        /// Updates the current ZNodeUserAccount given by param
        /// </summary>
        /// <param name="userAccount"></param>
        /// <returns>Returns true if update is successful.</returns>
        protected bool UpdateZNodeUserAccount(ZNodeUserAccount userAccount)
        {
            if (userAccount.UserID == null && !UserID.Enabled)
                return false;

            var userAccountAdmin = new AccountAdmin();
            var addressService = new AddressService();

            switch (RoleName.ToLower())
            {
                #region CUSTOMER
                default:
                    /**************** < CUSTOMER INFORMATION > *******************/
                    userAccount.EmailID = txtEmail.Text;
                    userAccount.EmailOptIn = chkOptIn.Checked;
                    userAccount.EnableCustomerPricing = chkCustomerPricing.Checked;
                    userAccount.ExternalAccountNo = string.IsNullOrEmpty(txtExternalAccNumber.Text.Trim())
                                                        ? null
                                                        : Server.HtmlEncode(txtExternalAccNumber.Text.Trim());
                    userAccount.Description = Server.HtmlEncode(txtDescription.Text);
                    userAccount.CompanyName = Server.HtmlEncode(txtCompanyName.Text);
                    userAccount.Website = Server.HtmlEncode(txtWebSite.Text.Trim());
                    userAccount.Source = Server.HtmlEncode(txtSource.Text.Trim());
                    userAccount.AccountProfileCode = userAccount.AccountProfileCode ??
                                                     DBNull.Value.ToString(CultureInfo.InvariantCulture);
                    userAccount.CreateUser = userAccount.CreateUser ?? HttpContext.Current.User.Identity.Name;
                    userAccount.CreateDte = userAccount.CreateDte;
                    userAccount.UpdateUser = HttpContext.Current.User.Identity.Name;
                    userAccount.UpdateDte = DateTime.Now;
                    userAccount.UserID = userAccount.UserID;
                    userAccount.AccountID = _accountId;
                    userAccount.Custom1 = Server.HtmlEncode(txtCustom1.Text.Trim());
                    userAccount.Custom2 = Server.HtmlEncode(txtCustom2.Text.Trim());
                    userAccount.Custom3 = Server.HtmlEncode(txtCustom3.Text.Trim());
                    break;
                #endregion
                #region VENDOR
                case "vendor":
                    /**************** < VENDOR INFORMATION > *******************/
                    var vendorAddress = userAccountAdmin.GetDefaultBillingAddress(userAccount.AccountID);

                    userAccount.CreateDte = userAccount.CreateDte;
                    userAccount.UpdateDte = DateTime.Now;
                    userAccount.CreateUser = userAccount.CreateUser ?? HttpContext.Current.User.Identity.Name;
                    userAccount.EmailID = txtBillingEmail.Text;

                    userAccount.ExternalAccountNo = string.IsNullOrEmpty(txtFranchiseNumber.Text.Trim())
                                                        ? null
                                                        : txtFranchiseNumber.Text.Trim();

                    vendorAddress.FirstName = txtBillingFirstName.Text.Trim();
                    vendorAddress.LastName = txtBillingLastName.Text.Trim();
                    vendorAddress.PhoneNumber = txtBillingPhoneNumber.Text.Trim();
                    vendorAddress.Street = txtBillingStreet1.Text.Trim();
                    vendorAddress.Street1 = txtBillingStreet2.Text.Trim();
                    vendorAddress.City = txtBillingCity.Text.Trim();
                    vendorAddress.CompanyName = txtBillingCompanyName.Text.Trim();
                    vendorAddress.StateCode = txtBillingState.Text.Trim();
                    vendorAddress.PostalCode = txtBillingPinCode.Text.Trim();

                    vendorAddress.CountryCode = string.IsNullOrEmpty(vendorAddress.CountryCode)
                                                    ? "US"
                                                    : vendorAddress.CountryCode;

                    if (vendorAddress.AddressID > 0)
                        addressService.Update(vendorAddress);

                    break;
                #endregion
                #region FRANCHISE
                case "franchise":
                    var franchiseAddress = userAccountAdmin.GetDefaultBillingAddress(userAccount.AccountID);
                    userAccount.CreateDte = userAccount.CreateDte;
                    userAccount.UpdateDte = DateTime.Now;
                    userAccount.CreateUser = userAccount.CreateUser ?? HttpContext.Current.User.Identity.Name;
                    franchiseAddress.FirstName = txtFranchiseAdminBillingFirstName.Text.Trim();
                    franchiseAddress.LastName = txtFranchiseBillingLastName.Text.Trim();
                    userAccount.EmailID = txtFranchiseAdminBillingEmailID.Text.Trim();
                    franchiseAddress.PhoneNumber = txtFranchiseBillingPhoneNumber.Text.Trim();
                    franchiseAddress.Street = txtFranchiseBillingStreet1.Text.Trim();
                    franchiseAddress.Street1 = txtFranchiseBillingStreet2.Text.Trim();
                    franchiseAddress.City = txtFranchiseBillingCity.Text.Trim();
                    franchiseAddress.StateCode = txtFranchiseBillingState.Text.Trim();
                    franchiseAddress.PostalCode = txtFranchiseBillingPinCode.Text.Trim();

                    if (franchiseAddress.AddressID > 0)
                        addressService.Update(franchiseAddress);
                    var storeAdmin = new StoreSettingsAdmin();
                    var existingPortal = storeAdmin.GetByPortalId(ZNodeConfigManager.SiteConfig.PortalID);
                    //// Set logo path
                    //if (RegisterUploadImage.PostedFile.FileName != string.Empty)
                    //{
                    //	// Check for Product Image
                    //	var imageData = new byte[RegisterUploadImage.PostedFile.InputStream.Length];
                    //	RegisterUploadImage.PostedFile.InputStream.Read(imageData, 0, (int)RegisterUploadImage.PostedFile.InputStream.Length);

                    //	var fileName = ZNodeConfigManager.EnvironmentConfig.OriginalImagePath +
                    //				   Path.GetFileNameWithoutExtension(RegisterUploadImage.PostedFile.FileName) + "_" +
                    //				   DateTime.Now.ToString("ddMMyyhhmmss") + Path.GetExtension(RegisterUploadImage.PostedFile.FileName);

                    //	ZNodeStorageManager.WriteBinaryStorage(imageData, fileName);
                    //	existingPortal.LogoPath = fileName;
                    //}

                    //var portalCatalogService = new PortalCatalogService();
                    //var pc = portalCatalogService.GetByPortalID(existingPortal.PortalID);
                    //var currentCatalog = pc[0];
                    //ddlTheme.SelectedIndex = (int)currentCatalog.ThemeID;
                    break;
                #endregion
                #region ADMIN
                case "admin":
                    userAccount.EmailID = txtAdminEmail.Text;
                    userAccount.UpdateUser = HttpContext.Current.User.Identity.Name;
                    userAccount.UpdateDte = DateTime.Now;
                    break;
                #endregion
            }

            return true;
        }
        #endregion

        #region Public Variables
        /// <summary>
        /// Gets or sets a value for RoleName
        /// </summary>
        public string RoleName
        {
            get { return _roleName; }

            set { _roleName = value; }
        }

        /// <summary>
        /// AdminProvider used to create a user without password nor questions nor answers.
        /// </summary>
        public MembershipProvider AdminMembershipProvider
        {
            get { return _adminMembershipProvider; }
            set { _adminMembershipProvider = value; }
        }
        #endregion
    }
}