﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Permission.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Accounts.Permission" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<h1>
    <asp:Localize ID="TitlePermission" runat="server" Text="<%$ Resources:ZnodeAdminResource,TitlePermission%>"></asp:Localize><asp:Label ID="lblPermissionName" runat="server" Text=""></asp:Label></h1>
<div>
    <asp:Label CssClass="Error" ID="lblPortalAcountErrorMessage" runat="server" EnableViewState="false"
        Visible="false"></asp:Label>
</div>
<h4 class="SubTitle">
    <asp:Label ID="lblSelectStores" runat="server" Text=""></asp:Label></h4>
<div style="margin-left: 7px; margin-top: 5px;">
    <asp:CheckBox ID="chkAllStores" runat="server" Text="<%$ Resources:ZnodeAdminResource,CheckBoxTextAllStores %>" AutoPostBack="true"
        OnCheckedChanged="ChkAllStores_checkedChanged" />
</div>
<br />
<asp:Panel ID="pnlSpecificStore" runat="server" Visible="false">
    <div>
        <asp:CheckBoxList ID="StoreCheckList" runat="server" RepeatDirection="Horizontal"
            RepeatColumns="4" CellPadding="5" />
    </div>
</asp:Panel>
<h4 class="SubTitle">
    <asp:Localize ID="SubTitleRoles" runat="server" Text="<%$ Resources:ZnodeAdminResource,SubTitleRoles%>"></asp:Localize></h4>
<div>
    <asp:CheckBoxList ID="RolesCheckboxList" runat="server" Enabled="false" RepeatDirection="Horizontal"
        RepeatColumns="3" CellPadding="5">
    </asp:CheckBoxList>
</div>
<div>
    <ZNode:Spacer ID="Spacer1" runat="server" SpacerHeight="15" SpacerWidth="3" />
</div>
<div>

    <zn:Button runat="server" ButtonType="SubmitButton" Text="<%$ Resources:ZnodeAdminResource,ButtonSubmit%>" ValidationGroup="grpAssociateAddOn" OnClick="BtnAddSelectedPortals_Click" CausesValidation="true" ID="btnAddSelectedStore" />
    <zn:Button runat="server" ButtonType="CancelButton" Text="<%$ Resources:ZnodeAdminResource,ButtonCancel%>" OnClick="BtnCancel_Click" CausesValidation="False" ID="btnBottomCancel" />

    <div>
        <ZNode:Spacer ID="Spacer9" SpacerHeight="15" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
</div>
<asp:Label CssClass="Error" ID="lblError" runat="server"></asp:Label>