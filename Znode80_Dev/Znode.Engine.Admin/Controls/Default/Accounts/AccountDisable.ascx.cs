﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Controls.Default.Accounts
{
    public partial class AccountDisable : System.Web.UI.UserControl
    {
        #region Procted Member Variables
        private int ItemId = 0;
        private AccountAdmin accountAdmin = new AccountAdmin();
        private string pageFrom = string.Empty;
        #endregion

        #region Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            if (Request.Params["pagefrom"] != null)
            {
                this.pageFrom = Request.Params["pagefrom"].ToString();
            }

            if (!Page.IsPostBack)
            {
                Account _account = this.accountAdmin.GetByAccountID(this.ItemId);
                

                MembershipUser _user = Membership.GetUser(_account.UserID.Value);

                lblAccountName.Text = _user.UserName;

                string roleList = string.Empty;

                // Get roles for this User account
                string[] roles = Roles.GetRolesForUser(_user.UserName);

                foreach (string Role in roles)
                {
                    roleList += Role + "<br>";
                }

                string rolename = roleList;

                // Hide the Delete button if a NonAdmin user has entered this page
                if (!Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "ADMIN"))
                {
                    if (Roles.IsUserInRole(_user.UserName, "ADMIN"))
                    {
                        btnDelete.Visible = false;
                    }
                    else if (Roles.IsUserInRole(HttpContext.Current.User.Identity.Name, "CUSTOMER SERVICE REP"))
                    {
                        if (rolename == Convert.ToString("USER<br>") || rolename == Convert.ToString(string.Empty))
                        {
                            btnDelete.Visible = true;
                        }
                        else
                        {
                            btnDelete.Visible = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Disable button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDisable_Click(object sender, EventArgs e)
        {
            Account _account = this.accountAdmin.GetByAccountID(this.ItemId);

            if (_account != null)
            {
                if (_account.UserID.HasValue)
                {
                    // Get user by Unique GUID using UserId
                    MembershipUser user = Membership.GetUser(_account.UserID.Value);
                    if (user != null)
                    {
                        // Disable online account
                        user.IsApproved = false;
                        Membership.UpdateUser(user);
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.AccountLock, user.UserName);
                    }
                }
            }
            else
            {
                // throw exception here
            }

            if (pageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/Default.aspx");
            }
            else if (pageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/Default.aspx");
            }
            else if (pageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/Secure/Vendors/VendorAccounts/Default.aspx");
            }
            else
            {
                Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/Default.aspx");
            }
        }

        /// <summary>
        /// Cancel Button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (pageFrom.ToLower() == "admin")
            {
                Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/Default.aspx");
            }
            else if (pageFrom.ToLower() == "franchise")
            {
                Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/Default.aspx");
            }
            else if (pageFrom.ToLower() == "vendor")
            {
                Response.Redirect("~/Secure/Vendors/VendorAccounts/Default.aspx");
            }
            else
            {
                Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/Default.aspx");
            }
        }
        #endregion
    }
}