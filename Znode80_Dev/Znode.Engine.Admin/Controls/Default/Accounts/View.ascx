﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="View.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Accounts.View" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ManufacturerAutoComplete" Src="~/Controls/Default/ManufacturerAutoComplete.ascx" %>
<script language="javascript" type="text/javascript">
    function DeleteConfirmation() {
        return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ConfirmDelete")) %>');
    }
</script>


<div>
    <div>
        <div class="LeftFloat" style="width: 30%; text-align: left">
            <h1 style="width: 700px;">
                <asp:Label ID="lblCustomerDetails" runat="server" Text="Label"></asp:Label></h1>
        </div>
        <div style="float: right">
            <zn:Button ID="CustomerList" runat="server" Width="100px" ButtonType="EditButton" OnClick="CustomerList_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonBackToProductType %>" />
        </div>
        <div class="ClearBoth">
        </div>
        <div align="left">
            <ajaxToolKit:TabContainer ID="tabCustomerDetails" runat="server">
                <ajaxToolKit:TabPanel ID="pnlGeneral" runat="server">
                    <HeaderTemplate>
                        <asp:Localize ID="General" Text="<%$ Resources:ZnodeAdminResource,SubTitleGeneral%>" runat="server"></asp:Localize>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div>
                            <ZNode:Spacer ID="Spacer7" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div>
                            <zn:Button runat="server" Width="130px" ButtonType="EditButton" OnClick="CustomerEdit_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonEditAccount %>" ID="EditCustomer" />
                        </div>
                        <br />
                        <h4 class="SubTitle">
                            <asp:Localize ID="SubTitleContact" Text="<%$ Resources:ZnodeAdminResource,SubTitleContact %>" runat="server"></asp:Localize></h4>
                        <div class="ViewForm200">
                            <div class="FieldStyleA">
                                <asp:Localize ID="ColumnTitleName" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleNameBilling %>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblName" runat="server" Text="Label"></asp:Label>&nbsp;
                            </div>

                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnTitleCompany" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleCompany %>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblCompanyName" runat="server" Text="Label"></asp:Label>&nbsp;
                            </div>

                            <div class="FieldStyleA">
                                <asp:Localize ID="ColumnTitlePhone" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleBillingPhone %>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblPhone" runat="server" Text="Label"></asp:Label>&nbsp;
                            </div>

                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnTitleEmail" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleEmail %>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblEmail" runat="server" Text="Label"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyleA" nowrap="nowrap">
                                <asp:Localize ID="EmailOpt" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleEmailOptIn %>" runat="server"></asp:Localize>
                            </div>
                            <div valign="top" class="ValueStyleA">
                                <img id="EmailOptin" runat="server" />
                            </div>

                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleAccount" Text="<%$ Resources:ZnodeAdminResource,SubTitleAccountInfo %>" runat="server"></asp:Localize></h4>
                            <div class="FieldStyleA">
                                <asp:Localize ID="SubTitleAccountID" Text="<%$ Resources:ZnodeAdminResource,SubTitleAccountID %>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblAccountID" runat="server" Text="Label"></asp:Label>&nbsp;
                            </div>

                            <div class="FieldStyle">
                                <asp:Localize ID="ExternalID" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleExternalID %>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblExternalAccNumber" runat="server" Text="Label" Width="98px"></asp:Label>&nbsp;
                            </div>

                            <asp:Panel runat="server" ID="pnlCustomerPricing" Visible="False">
                                <div class="FieldStyleA" nowrap="nowrap">
                                    <asp:Localize ID="ColumnTitleCustomer" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleCustomerBasedPriceEnable %>" runat="server"></asp:Localize>
                                </div>
                                <div valign="top" class="ValueStyleA">
                                    <img id="CustomerBasedPriceEnable" runat="server" />
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                </div>

                                <div class="FieldStyle">
                                    <asp:Localize ID="ColumnLoginName" Text="<%$ Resources:ZnodeAdminResource,ColumnLoginName %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblLoginName" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                            </asp:Panel>
                            <asp:Panel runat="server" ID="pnlLoginName">
                                <div class="FieldStyleA">
                                    <asp:Localize ID="ColumnLogin" Text="<%$ Resources:ZnodeAdminResource,ColumnLoginName %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblLogin" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                            </asp:Panel>
                            <ZNode:Spacer ID="Spacer11" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>

                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleAddresses" Text="<%$ Resources:ZnodeAdminResource,SubTitleAddresses %>" runat="server"></asp:Localize></h4>
                            <div>
                                <div>
                                    <zn:Button runat="server" Width="210px" ButtonType="EditButton" OnClick="BtnAddNewAddress_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonAddNewAddress %>" ID="btnAddNewAddress" />
                                </div>
                                <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>

                                <asp:GridView ID="gvAddress" ShowFooter="True" CaptionAlign="Left" runat="server"
                                    CellPadding="4" AutoGenerateColumns="False" CssClass="Grid" Width="100%" GridLines="None"
                                    AllowPaging="True" EmptyDataText="<%$ Resources:ZnodeAdminResource,GridEmptyAddressText %>" PageSize="5" OnPageIndexChanging="GvAddress_PageIndexChanging"
                                    OnRowCommand="GvAddress_RowCommand">
                                    <Columns>
                                        <asp:BoundField DataField="Name" HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleName %>">
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleDefaultShipping %>">
                                            <ItemTemplate>
                                                <img id="Img11" runat="server" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsDefaultShipping").ToString()))%>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleDefaultBilling %>">
                                            <ItemTemplate>
                                                <img alt="" id="Img12" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsDefaultBilling").ToString()))%>'
                                                    runat="server" />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleAddress %>">
                                            <ItemTemplate>
                                                <asp:Label ID="lblAddress" runat="server" Text='<%# GetAddress(DataBinder.Eval(Container.DataItem, "AddressID").ToString())%>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="Edit" runat="server" CommandArgument='<%# Eval("AddressID") %>'
                                                    CommandName="Edit" CssClass="actionlink" Text="<%$ Resources:ZnodeAdminResource, LinkEdit %>" />
                                                <asp:LinkButton ID="Delete" runat="server" CommandArgument='<%# Eval("AddressID") %>'
                                                    CommandName="Remove" CssClass="actionlink" Text="<%$ Resources:ZnodeAdminResource, LinkRemove %>" OnClientClick="return DeleteConfirmation();" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <RowStyle CssClass="RowStyle" />
                                    <HeaderStyle CssClass="HeaderStyle" />
                                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                    <FooterStyle CssClass="FooterStyle" />
                                </asp:GridView>

                            </div>
                            <div class="Error">
                                <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
                            </div>
                            <div class="ClearBoth"></div>
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleAdditional" Text="<%$ Resources:ZnodeAdminResource,SubTitleAdditionalInfo %>" runat="server"></asp:Localize>
                            </h4>
                            <div>

                                <div class="FieldStyleA">
                                    <asp:Localize ID="ColumnTitleWebsite" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleWebsite %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblWebSite" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize ID="ColumnTitleDescription" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleDescription %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblDescription" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="ColumnTitleSource" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleSource %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblSource" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>

                                <div class="FieldStyle">
                                    <asp:Localize ID="ColumnTitleCreateDate" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleCreateDate %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblCreatedDate" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="ColumnTitleCreateUser" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleCreateUser %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblCreatedUser" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize ID="ColumnTitleUpdateDate" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleUpdateDate %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblUpdatedDate" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="ColumnTitleUpdateUser" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleUpdateUser %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblUpdatedUser" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>

                                <div class="FieldStyle">
                                    <asp:Localize ID="ColumnTitleCustom1" Text="<%$ Resources:ZnodeAdminResource,ColumnCustom1 %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblCustom1" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="ColumnTitleCustom2" Text="<%$ Resources:ZnodeAdminResource,ColumnCustom2 %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblCustom2" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize ID="ColumnTitleCustom3" Text="<%$ Resources:ZnodeAdminResource,ColumnCustom3 %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblCustom3" runat="server" Text="Label"></asp:Label>&nbsp;
                                </div>

                            </div>
                        </div>

                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlSiteAdminGeneral" runat="server">
                    <HeaderTemplate>
                        <asp:Localize ID="SubTitleGeneral1" Text="<%$ Resources:ZnodeAdminResource,SubTitleGeneral%>" runat="server"></asp:Localize>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div>
                            <ZNode:Spacer ID="Spacer124" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div>
                            <zn:Button runat="server" ID="Button1" ButtonType="EditButton" OnClick="CustomerEdit_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonEditAccount %>" Width="100px" />
                        </div>
                        <br />
                        <h4 class="SubTitle">
                            <asp:Localize ID="SubTitleGeneralInfo" Text="<%$ Resources:ZnodeAdminResource,SubTitleGeneralInformation %>" runat="server"></asp:Localize></h4>
                        <div class="ViewForm200">
                            <div class="FieldStyleA">
                                <asp:Localize ID="ColumnTitleLoginName" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleLoginName %>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblSiteLoginName" runat="server" Text=""></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnAccountNumber" Text="<%$ Resources:ZnodeAdminResource,ColumnAccountNumber %>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblSiteAccountNumber" runat="server" Text="" Width="98px"></asp:Label>&nbsp;
                            </div>
                            <ZNode:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleContact1" Text="<%$ Resources:ZnodeAdminResource,SubTitleContact %>" runat="server"></asp:Localize></h4>
                            <div class="FieldStyleA">
                                <asp:Localize ID="ColumnTitleEmail1" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleEmail %>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblSiteEmail" runat="server" Text=""></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle" nowrap="nowrap">
                                <asp:Localize ID="ColumnTitleEmailOptIn" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleEmailOptIn %>" runat="server"></asp:Localize>
                            </div>
                            <div valign="top" class="ValueStyle">
                                <img id="SiteEmailOptin" runat="server" />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                            <ZNode:Spacer ID="Spacer14" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            <div>
                                <ZNode:Spacer ID="Spacer18" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleAdditionalInfo1" Text="<%$ Resources:ZnodeAdminResource,SubTitleAdditionalInfo %>" runat="server"></asp:Localize></h4>
                            <div>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="ColumnTitleCreateDate1" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleCreateDate %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblSiteCreateDate" runat="server" Text=""></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize ID="ColumnTitleCreateUser1" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleCreateUser %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblSiteCreateUser" runat="server" Text=""></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="ColumnTitleUpdateDate1" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleUpdateDate %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblSiteUpdateDate" runat="server" Text=""></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize ID="ColumnTitleUpdateUser1" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleUpdateUser %>" runat="server"></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblSiteUpdateUser" runat="server" Text=""></asp:Label>&nbsp;
                                </div>
                            </div>
                        </div>
                        <asp:Label ID="Label19" runat="server" CssClass="Error"></asp:Label>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="tabpnlOrders" runat="server" Enabled="false">
                    <HeaderTemplate>
                        <asp:Localize ID="SubTitleOrders" Text="<%$ Resources:ZnodeAdminResource,SubTitleOrders%>" runat="server"></asp:Localize>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <h4 class="SubTitle">
                            <asp:Localize ID="SubTitleOrderList" Text="<%$ Resources:ZnodeAdminResource,SubTitleOrderList%>" runat="server"></asp:Localize>
                        </h4>
                        <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText="<%$ Resources:ZnodeAdminResource,GridEmptyOrdersText %>"
                            GridLines="None" Width="100%" OnRowDataBound="UxGrid_RowDataBound" OnPageIndexChanging="UxGrid_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText=" <%$ Resources:ZnodeAdminResource,ColumnTitleOrderID%>" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="hlinkOrder" runat="server" Text='<%# Eval("orderid") %>'></asp:HyperLink>
                                        <asp:HiddenField ID="HdnId" runat="server" Value='<%# Eval("orderid") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:BoundField DataField="Orderdate" DataFormatString="{0:MM/dd/yyyy hh:mm}" HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleOrderDate%>"
                                    HtmlEncode="False" SortExpression="OrderDate" HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleOrderAmount%>" ItemStyle-Width="100px" ItemStyle-HorizontalAlign="Right" SortExpression="total" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="TotalAmount" Text='<%# FormatPrice(Eval("total")) %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleOrderStatus%> " SortExpression="OrderStateID" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="OrderStatus" Text='<%# DisplayOrderStatus(Eval("OrderStateID")) %>'
                                            runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="RowStyle" />
                            <EditRowStyle CssClass="EditRowStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                        </asp:GridView>
                        <div>
                            <ZNode:Spacer ID="Spacer15" SpacerHeight="1" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="tabpnlNotes" runat="server">
                    <HeaderTemplate>
                        <asp:Localize ID="SubTitleNotes" Text="<%$ Resources:ZnodeAdminResource,SubTitleNotes%>" runat="server"></asp:Localize>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div>
                            <ZNode:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div class="ButtonStyle">
                            <zn:LinkButton ID="AddNewNote" runat="server"
                                ButtonType="Button" OnClick="AddNewNote_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonAddNote%>"
                                ButtonPriority="Primary" />
                        </div>
                        <div>
                            <ZNode:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <h4 class="SubTitle">
                            <asp:Localize ID="SubTitleNoteList" Text="<%$ Resources:ZnodeAdminResource,SubTitleNoteList%>" runat="server"></asp:Localize>
                        </h4>
                        <asp:Repeater ID="CustomerNotes" runat="server">
                            <ItemTemplate>
                                <asp:Label ID="Label1" CssClass="ValueStyle" runat="Server" Text='<%# FormatCustomerNote(Eval("NoteTitle"),Eval("CreateUser"),Eval("CreateDte")) %>' />
                                <ZNode:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                                <asp:Label ID="Label2" CssClass="ValueStyle" runat="Server" Text='<%# Eval("NoteBody") %>' />
                                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </ItemTemplate>
                        </asp:Repeater>
                        <div>
                            <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlProfile" runat="server">
                    <HeaderTemplate>
                        <asp:Localize ID="SubTitleProfiles" Text="<%$ Resources:ZnodeAdminResource,SubTitleProfiles%>" runat="server"></asp:Localize>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div>
                            <ZNode:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div class="ButtonStyle">
                            <zn:LinkButton ID="btnAddProfile" runat="server"
                                ButtonType="Button" OnClick="AddProfile_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonAssociateProfile%>"
                                ButtonPriority="Primary" />
                        </div>
                        <div>
                            <ZNode:Spacer ID="Spacer16" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div>
                            <div class="Error">
                                <asp:Label ID="lblProfileError" runat="server" Text="" />
                            </div>
                            <div>
                                <!--Update Panel for grid paging that are used to avoid the postbacks-->
                                <asp:UpdatePanel ID="updProfileGrid" runat="server" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:GridView ID="uxProfileGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                                            runat="server" CellPadding="4" AutoGenerateColumns="False" CssClass="Grid" Width="100%"
                                            GridLines="None" OnRowCommand="UxProfileGrid_RowCommand" AllowPaging="True" OnPageIndexChanging="UxProfileGrid_PageIndexChanging"
                                            OnRowDeleting="UxProfileGrid_RowDeleting" PageSize="5">
                                            <Columns>
                                                <asp:BoundField DataField="ProfileID" HeaderText="ID" HeaderStyle-HorizontalAlign="Left" />
                                                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleName %>" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <%# Eval("ProfileIDSource.Name")%>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleWholeSale %>" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <img alt="" id="UseWholeSalePricingImg" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ProfileIDSource.UseWholeSalePricing").ToString()))%>'
                                                            runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleShowPricing %>" HeaderStyle-HorizontalAlign="Left">
                                                    <ItemTemplate>
                                                        <img alt="" id="ShowPricingImg" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ProfileIDSource.ShowPricing").ToString()))%>'
                                                            runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="Delete" Text="<%$ Resources:ZnodeAdminResource, LinkRemove %>" CommandArgument='<%# Eval("AccountProfileID") %>'
                                                            CommandName="Remove" CssClass="actionlink" runat="server" OnClientClick="return DeleteConfirmation();" />
                                                        <asp:HiddenField ID="hdnProfileName" runat="server" Value='<%# Eval("ProfileIDSource.Name")%>' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EmptyDataTemplate>
                                                <asp:Localize runat="server" ID="GridEmptyData" Text="<%$ Resources:ZnodeAdminResource,GridEmptyData %>"></asp:Localize>
                                            </EmptyDataTemplate>
                                            <RowStyle CssClass="RowStyle" />
                                            <HeaderStyle CssClass="HeaderStyle" />
                                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                            <FooterStyle CssClass="FooterStyle" />
                                        </asp:GridView>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </div>
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="tabpnlAffiliate" runat="server">
                    <HeaderTemplate>
                        <asp:Localize ID="SubTitleAffiliate" Text="<%$ Resources:ZnodeAdminResource,SubTitleAffiliate%>" runat="server"></asp:Localize>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div>
                            <ZNode:Spacer ID="Spacer12" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div>
                             <zn:Button runat="server" Width="130px" ButtonType="EditButton" CssClass="Size210" OnClick="TrackingEdit_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonEditTracking %>" ID="TrackingEdit" />
                        </div>
                        <br />
                        <h4 class="SubTitle">
                            <asp:Localize ID="SubTitleTracking" Text="<%$ Resources:ZnodeAdminResource,SubTitleTracking%>" runat="server"></asp:Localize></h4>
                        <div class="ViewForm200">
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnTitleTracking" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleTracking%>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyle" runat="server" id="dvTrackingLink">
                                <asp:Label ID="lblAffiliateLinkError" runat="server" CssClass="Error"></asp:Label>
                                <asp:HyperLink ID="hlAffiliateLink" runat="server" Target="_blank"></asp:HyperLink>
                            </div>
                            <div class="FieldStyleA">
                                <asp:Localize ID="ColumnTitleCommissiong" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleCommissionType%>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblReferralType" runat="server" Text=""></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnTitleCommission" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleCommission%>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblReferralCommission" runat="server" Text="" Width="98px"></asp:Label>
                            </div>
                            <div class="FieldStyleA">
                                <asp:Localize ID="ColumnTitleTaxID" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleTaxID%>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblTaxId" runat="server" Text="Label"></asp:Label>&nbsp;
                            </div>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnTitlePartner" Text="<%$ Resources:ZnodeAdminResource,ColumnTitlePartner%>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblReferralStatus" runat="server" Text="Label"></asp:Label>
                            </div>
                            <div class="FieldStyleA">
                                <asp:Localize ID="ColumnTitleAmount" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleAmountOwed%>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyleA">
                                <asp:Label ID="lblAmountOwed" runat="server"></asp:Label>&nbsp;
                            </div>
                        </div>
                        <br />
                        <h4 class="SubTitle">
                            <asp:Localize ID="SubTitleReferral" Text="<%$ Resources:ZnodeAdminResource,SubTitleReferral%>" runat="server"></asp:Localize></h4>
                        <asp:GridView ID="uxReferralCommission" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            CaptionAlign="Left" CellPadding="4" CssClass="Grid" PageSize="10" OnPageIndexChanging="UxReferralCommission_PageIndexChanging"
                            EmptyDataText="<%$ Resources:ZnodeAdminResource,GridEmptyreferralcommissionsText%>" GridLines="None"
                            Width="100%">
                            <Columns>
                                <asp:HyperLinkField DataNavigateUrlFields="OrderId" DataNavigateUrlFormatString="~/Secure/Orders/OrderManagement/ViewOrders/View.aspx?itemid={0}"
                                    DataTextField="OrderId" HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleOrderID%>" SortExpression="OrderId" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="ReferralCommission" HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleCommission%>" DataFormatString="{0:0.00}"
                                    HeaderStyle-HorizontalAlign="Right" />
                                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleCommissionType%>" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="ReferralCommissionTypeID" Text='<%# GetCommissionTypeById(Eval("ReferralCommissionTypeID")) %>'
                                            runat="server"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="ReferralAccountID" Visible="false" HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleReferralAccount%>"
                                    HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="TransactionID" HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleTransactionID%>" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="Description" HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleDescription%>" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="CommissionAmount" ItemStyle-HorizontalAlign="Right" HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleCommissionAmount%>"
                                    DataFormatString="{0:c}" />
                            </Columns>
                            <RowStyle CssClass="RowStyle" />
                            <EditRowStyle CssClass="EditRowStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                        </asp:GridView>
                        <div>
                            <ZNode:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <asp:Panel ID="pnlAffiliatePayment" runat="server">
                            <div class="ButtonStyle">
                                <zn:LinkButton ID="btnAddPayment" runat="server"
                                    ButtonType="Button" OnClick="AddAffiliatePayment_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonAddPayment%>"
                                    ButtonPriority="Primary" />
                            </div>
                            <div>
                                <ZNode:Spacer ID="Spacer10" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                            </div>
                            <h4 class="GridTitle">
                                <asp:Localize ID="GridTitleAffiliate" Text="<%$ Resources:ZnodeAdminResource,GridTitleAffiliate%>" runat="server"></asp:Localize></h4>
                            <asp:GridView ID="uxPaymentList" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText="<%$ Resources:ZnodeAdminResource,GridEmptyPaymentText%>"
                                ForeColor="#333333" GridLines="None" Width="100%" OnPageIndexChanging="UxPaymentList_PageIndexChanging">
                                <Columns>
                                    <asp:BoundField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitlePaymentDescription%>" DataField="Description" HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitlePaymentDate%>" DataField="ReceivedDate" DataFormatString="{0:d}"
                                        HeaderStyle-HorizontalAlign="Left" />
                                    <asp:BoundField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleAmount%>" DataField="Amount" DataFormatString="{0:c}" HeaderStyle-HorizontalAlign="Left" />
                                </Columns>
                                <RowStyle CssClass="RowStyle" />
                                <EditRowStyle CssClass="EditRowStyle" />
                                <HeaderStyle CssClass="HeaderStyle" />
                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            </asp:GridView>
                        </asp:Panel>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="permissionPanel" runat="server" Enabled="false">
                    <HeaderTemplate>
                        <asp:Localize ID="SubTitleStorePermissions" Text="<%$ Resources:ZnodeAdminResource,SubTitleStorePermissions%>" runat="server"></asp:Localize>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <div>
                            <ZNode:Spacer ID="Spacer9" runat="server" SpacerHeight="10" SpacerWidth="3" />
                        </div>
                        <div>
                             <zn:Button  ID="PermissionEdit" ButtonType="EditButton" Width="180px" runat="server" OnClick="PermissionEdit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonEditRoles%>"/>
                        </div>
                        <br />
                        <h4 class="SubTitle">
                            <asp:Localize ID="SubTitleStoreAccess" Text="<%$ Resources:ZnodeAdminResource,SubTitleStoreAccess%>" runat="server"></asp:Localize></h4>
                        <div class="ViewForm200">
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnTitleStores" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleStores%>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblstores" runat="server"></asp:Label>
                            </div>
                            <h4 class="SubTitle">
                                <asp:Localize ID="SubTitleUserRoles" Text="<%$ Resources:ZnodeAdminResource,SubTitleUserRoles%>" runat="server"></asp:Localize></h4>
                            <div class="FieldStyle">
                                <asp:Localize ID="ColumnTitleRole" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleRole%>" runat="server"></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:Label ID="lblRoles" runat="server"></asp:Label>
                            </div>
                        </div>
                        <div class="ClearBoth">
                        </div>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="tabPricing" runat="server" Enabled="false">
                    <HeaderTemplate>
                        <asp:Localize ID="ColumnTitleCustomerBased" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleCustomerBasedPricing%>" runat="server"></asp:Localize>
                    </HeaderTemplate>
                    <ContentTemplate>
                        <h4 class="SubTitle">
                            <asp:Localize ID="SubTitleSearchProduct" Text="<%$ Resources:ZnodeAdminResource,SubTitleSearchProducts%>" runat="server"></asp:Localize></h4>
                        <asp:Panel ID="SearchPanel" DefaultButton="btnSearch" runat="server">
                            <div class="SearchForm">
                                <div class="RowStyle">
                                    <div class="ItemStyle">
                                        <span class="FieldStyle">
                                            <asp:Label ID="lbStoreName" Text="Store Name" runat="server"></asp:Label></span><br />
                                        <span class="ValueStyle">
                                            <asp:DropDownList ID="ddlPortal" runat="server" AutoPostBack="true">
                                            </asp:DropDownList>
                                        </span>
                                    </div>
                                </div>
                                <div class="RowStyle">
                                    <div class="ItemStyle">
                                        <span class="FieldStyle">
                                            <asp:Localize ID="ColumnTitleSkuorPart" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleSkuorPart%>" runat="server"></asp:Localize></span><br />
                                        <span class="ValueStyle">
                                            <asp:TextBox ID="txtSKU" runat="server"></asp:TextBox></span>
                                    </div>
                                    <div class="ItemStyle">
                                        <span class="FieldStyle">
                                            <asp:Localize ID="ColumnTitleExternal" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleExternal%>" runat="server"></asp:Localize></span><br />
                                        <span class="ValueStyle">
                                            <asp:TextBox ID="txtProductCode" runat="server"></asp:TextBox></span>
                                    </div>
                                    <div class="ItemStyle">
                                        <span class="FieldStyle">
                                            <asp:Localize ID="ColumnTitleProductName" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleProductName%>" runat="server"></asp:Localize></span><br />
                                        <span class="ValueStyle">
                                            <asp:TextBox ID="txtproductname" runat="server"></asp:TextBox></span>
                                    </div>

                                </div>
                                <div class="RowStyle">

                                    <div class="ItemStyle">
                                        <span class="FieldStyle">
                                            <asp:Localize ID="ColumnTitleProductCategory" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleProductCategory%>" runat="server"></asp:Localize></span><br />
                                        <span class="ValueStyle">
                                            <ZNode:CategoryAutoComplete ID="txtCategory" runat="server" />
                                        </span>
                                    </div>
                                    <div class="ItemStyle">
                                        <span class="FieldStyle">
                                            <asp:Localize ID="ColumnTitleProductBrand" Text="<%$ Resources:ZnodeAdminResource,ColumnTitleProductBrand%>" runat="server"></asp:Localize></span><br />
                                        <span class="ValueStyle">
                                            <ZNode:ManufacturerAutoComplete runat="server" ID="txtManufacturer" />
                                        </span>
                                    </div>

                                </div>
                                <div class="ClearBoth">
                                    <br />
                                </div>
                                <div>
                                    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnClear_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonClear%>" CausesValidation="False" ID="btnClear" />
                                    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonSubmit%>" ID="btnSearch" />
                                    <div style="float: right">
                                        <zn:Button runat="server" Width="200px" ButtonType="EditButton" OnClick="btnDownload_Click" Text="<%$ Resources:ZnodeAdminResource,ButtonDownloadToExcel%>" ID="btnDownload" />                                        
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                        <br />
                        <h4 class="GridTitle">
                            <asp:Localize ID="GridTitleCustomerBased" Text="<%$ Resources:ZnodeAdminResource,GridTitleCustomerBased%>" runat="server"></asp:Localize></h4>
                        <asp:Label ID="Label3" runat="server" CssClass="Error"></asp:Label>
                        <asp:GridView ID="uxCustomerPricing" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
                            GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="uxCustomerPricing_PageIndexChanging"
                            EmptyDataText="<%$ Resources:ZnodeAdminResource,GridEmptyCustomerBasedText%>"
                            Width="100%" CellPadding="4">
                            <Columns>
                                <asp:BoundField DataField="ExternalID" HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleExtProductCode%>" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleSkuorPart%>" DataField="SKU" HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleProductName%>" DataField="Name" HeaderStyle-HorizontalAlign="Left" />

                                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleBasePrice%>" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# FormatPrice(DataBinder.Eval(Container.DataItem,"BasePrice")) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource,ColumnTitleCustomerNegotiatedPrice%>" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# FormatPrice(DataBinder.Eval(Container.DataItem,"NegotiatedPrice")) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource,ColumnTitleDiscount%>' HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# FormatPrice(DataBinder.Eval(Container.DataItem,"Discount")) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle CssClass="FooterStyle" />
                            <RowStyle CssClass="RowStyle" />
                            <PagerStyle CssClass="PagerStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                        </asp:GridView>
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
            </ajaxToolKit:TabContainer>
        </div>
    </div>
</div>
