﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Controls.Default.Accounts
{
    public partial class Address : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private int AddressId;
        private int AccountId;
        private int Mode;
        private string _RoleName = string.Empty;
        #endregion

        #region Public Events
        public event System.EventHandler ButtonSubmitClick;
        public event System.EventHandler ButtonCancelClick;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a value for RoleName
        /// </summary>
        public string RoleName
        {
            get
            {
                return this._RoleName;
            }

            set
            {
                this._RoleName = value;
            }
        }

        /// <summary>
        /// Gets or sets the User Account
        /// </summary>
        public ZNodeUserAccount UserAccount
        {
            get
            {
                if (ViewState["AccountObject"] != null)
                {
                    // The account info with 'AccountObject' is retrieved from the cache and
                    // It is converted to a Entity Account object
                    return (ZNodeUserAccount)ViewState["AccountObject"];
                }

                return new ZNodeUserAccount();
            }

            set
            {
                // Customer account object is placed in the cache and assigned a key, AccountObject.            
                ViewState["AccountObject"] = value;
            }
        }

        /// <summary>
        /// Gets or sets the billing address
        /// </summary>
        public int BillingAddressID
        {
            get
            {
                if (ViewState["BillingAddressID"] != null)
                {
                    return (int)ViewState["BillingAddressID"];
                }

                return 0;
            }

            set
            {
                ViewState["BillingAddressID"] = value;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            btnSubmitTop.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnSubmitTop.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit.gif") + "'");
            btnCancelTop.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancelTop.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel.gif") + "'");

            btnSubmitBottom.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnSubmitBottom.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit.gif") + "'");
            btnCancelBottom.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancelBottom.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel.gif") + "'");

            if (HttpContext.Current.Request.Params["itemid"] == null)
            {
                this.AddressId = 0;
            }
            else
            {
                this.AddressId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (HttpContext.Current.Request.Params["accountid"] == null)
            {
                this.AccountId = 0;

                if (this.ButtonSubmitClick != null)
                {
                    this.AccountId = this.UserAccount.AccountID;
                }
            }
            else
            {
                this.AccountId = int.Parse(Request.Params["accountid"].ToString());
            }

            if (HttpContext.Current.Request.Params["mode"] == null)
            {
                this.Mode = 0;
            }
            else
            {
                this.Mode = int.Parse(Request.Params["mode"].ToString());
            }

            if (HttpContext.Current.Request["pagefrom"] != null)
            {
                this.RoleName = Request.Params["pagefrom"].ToString();
            }


            if (!Page.IsPostBack)
            {
                this.BindCountry();

                if (this.AddressId > 0)
                {
                    this.BindData();

                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditAddress").ToString();
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddAddress").ToString();
                }
            }
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.ButtonCancelClick != null)
            {
                this.ClearFields();
                // Triggers parent control event
                this.ButtonCancelClick(sender, e);
            }
            else
            {
                this.RediretPage();
            }
        }

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {


            this.SubmitPage();
            if (this.ButtonSubmitClick != null)
            {
                // Triggers parent control event
                this.ButtonSubmitClick(sender, e);
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Bind Account Details
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.DataAccess.Service.AddressService addressService = new ZNode.Libraries.DataAccess.Service.AddressService();
            ZNode.Libraries.DataAccess.Entities.Address address = addressService.GetByAddressID(this.AddressId);

            if (address != null)
            {
                // Set field values
                txtName.Text = address.Name;
                txtFirstName.Text = address.FirstName;
                txtLastName.Text = address.LastName;
                txtCompanyName.Text = address.CompanyName;
                txtStreet1.Text = address.Street;
                txtStreet2.Text = address.Street1;
                txtCity.Text = address.City;
                txtState.Text = address.StateCode;
                txtPostalCode.Text = address.PostalCode;
                ddlCountryCode.SelectedIndex = ddlCountryCode.Items.IndexOf(ddlCountryCode.Items.FindByValue(address.CountryCode));
                txtPhoneNumber.Text = address.PhoneNumber;
                chkUseAsDefaultBillingAddress.Checked = address.IsDefaultBilling;
                chkUseAsDefaultShippingAddress.Checked = address.IsDefaultShipping;
            }
        }

        /// <summary>
        /// Binds country drop-down list
        /// </summary>
        private void BindCountry()
        {
            // PortalCountry
            PortalCountryHelper portalCountryHelper = new PortalCountryHelper();
            DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

            DataView View = new DataView(countryDs.Tables[0]);
            View.RowFilter = "ActiveInd = 1";

            // Drop Down List
            ddlCountryCode.DataSource = View;
            ddlCountryCode.DataTextField = "Name";
            ddlCountryCode.DataValueField = "Code";
            ddlCountryCode.DataBind();
            ddlCountryCode.SelectedIndex = ddlCountryCode.Items.IndexOf(ddlCountryCode.Items.FindByValue("US"));
        }

        public bool ValidateCountry()
        {
            bool flag = false;
            ZNode.Libraries.DataAccess.Custom.PortalCountryHelper portalCountryHelper =
                new ZNode.Libraries.DataAccess.Custom.PortalCountryHelper();
            DataSet countryDs = portalCountryHelper.GetCountriesByPortalID(ZNodeConfigManager.SiteConfig.PortalID);

            DataTable BillingView = countryDs.Tables[0];
            DataRow[] drBillingView = BillingView.Select("CountryCode='" + this.ddlCountryCode.Text + "'");
            foreach (DataRow drView in drBillingView)
            {
                if (drView["ShippingActive"].ToString() != "True")
                {
                    flag = false;
                }
                else
                {
                    flag = true;
                }
            }
            return flag;
        }

        /// <summary>
        /// Submit the page data.
        /// </summary>
        private void SubmitPage()
        {
            ZNode.Libraries.DataAccess.Service.AddressService addressService = new ZNode.Libraries.DataAccess.Service.AddressService();
            ZNode.Libraries.DataAccess.Entities.Address address = new ZNode.Libraries.DataAccess.Entities.Address();
            if (this.AddressId > 0)
            {
                address = addressService.GetByAddressID(this.AddressId);
            }

            if (!this.HasDefaultAddress(this.AccountId, true))
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorBilling").ToString();
                return;
            }

            if (!this.HasDefaultAddress(this.AccountId, false))
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorShipping").ToString();
                return;
            }

            if (chkUseAsDefaultShippingAddress.Checked)
            {
                if (!ValidateCountry())
                {
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCountry").ToString();
                    return;
                }
            }

            address.AccountID = this.AccountId;
            address.Name = txtName.Text;
            address.FirstName = txtFirstName.Text;
            address.LastName = txtLastName.Text;
            address.CompanyName = txtCompanyName.Text;
            address.Street = txtStreet1.Text;
            address.Street1 = txtStreet2.Text;
            address.City = txtCity.Text;
            address.StateCode = txtState.Text.ToUpper();
            address.PostalCode = txtPostalCode.Text;
            address.CountryCode = ddlCountryCode.SelectedValue;
            address.PhoneNumber = txtPhoneNumber.Text;

            TList<ZNode.Libraries.DataAccess.Entities.Address> addressList = addressService.GetByAccountID(this.AccountId);
            if (chkUseAsDefaultBillingAddress.Checked)
            {
                // Clear all previous default billing address flag.
                foreach (ZNode.Libraries.DataAccess.Entities.Address currentItem in addressList)
                {
                    currentItem.IsDefaultBilling = false;
                }

                addressService.Update(addressList);
            }

            if (chkUseAsDefaultShippingAddress.Checked)
            {
                // Clear all previous default billing address flag.
                foreach (ZNode.Libraries.DataAccess.Entities.Address currentItem in addressList)
                {
                    currentItem.IsDefaultShipping = false;
                }

                addressService.Update(addressList);
            }

            address.IsDefaultBilling = chkUseAsDefaultBillingAddress.Checked;
            address.IsDefaultShipping = chkUseAsDefaultShippingAddress.Checked;

            bool isSuccess = false;
            if (this.AddressId > 0)
            {
                isSuccess = addressService.Update(address);
            }
            else
            {
                isSuccess = addressService.Insert(address);
            }

            if (isSuccess && this.ButtonSubmitClick == null)
            {
                this.RediretPage();
            }
            if (this.ButtonSubmitClick != null)
            {
                if (chkUseAsDefaultBillingAddress.Checked)
                {
                    this.BillingAddressID = address.EntityId.AddressID;
                }
                this.ClearFields();
            }
        }

        /// <summary>
        /// Check whether the account has default billing or shipping address.
        /// </summary>
        /// <param name="accountId">Account Id</param>
        /// <param name="isBillingAddress">True to check billing address, false to check shipping address.</param>
        /// <returns>Returns true if account has default address or false.</returns>
        private bool HasDefaultAddress(int accountId, bool isBillingAddress)
        {
            bool hasDefault = true;

            AccountAdmin accountAdmin = new AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Address addresses = null;
            if (isBillingAddress)
            {
                addresses = accountAdmin.GetDefaultBillingAddress(accountId);
                hasDefault = this.ValidateDefaultAddress(addresses, chkUseAsDefaultBillingAddress);
            }
            else
            {
                addresses = accountAdmin.GetDefaultShippingAddress(accountId);
                hasDefault = this.ValidateDefaultAddress(addresses, chkUseAsDefaultShippingAddress);
            }

            return hasDefault;
        }
        /// <summary>
        /// Method to clear all the fields
        /// </summary>
        private void ClearFields()
        {
            txtName.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtLastName.Text = string.Empty;
            txtCompanyName.Text = string.Empty;
            txtStreet1.Text = string.Empty;
            txtStreet2.Text = string.Empty;
            txtCity.Text = string.Empty;
            txtState.Text = string.Empty;
            txtPostalCode.Text = string.Empty;
            ddlCountryCode.ClearSelection();
            txtPhoneNumber.Text = string.Empty;
            chkUseAsDefaultBillingAddress.Checked = false;
            chkUseAsDefaultShippingAddress.Checked = true;
        }


        /// <summary>
        /// Validate account had default address.
        /// </summary>
        /// <param name="address">Address object</param>
        /// <param name="cb">Check box control</param>
        /// <returns>Returns true if account has default address.</returns>
        private bool ValidateDefaultAddress(ZNode.Libraries.DataAccess.Entities.Address address, CheckBox cb)
        {
            if (address == null && !cb.Checked)
            {
                return false;
            }
            else
            {
                if (!cb.Checked && address != null && address.AddressID == this.AddressId)
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Redirect to the account view page.
        /// </summary>
        private void RediretPage()
        {
            if (this.AccountId > 0)
            {
                if (RoleName.ToLower() == "admin")
                {
                    Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + this.AccountId + "&mode=" + this.Mode + "&pagefrom=admin");
                }
                else if (RoleName.ToLower() == "franchise")
                {
                    Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + this.AccountId + "&mode=" + this.Mode + "&pagefrom=franchise");
                }
                else if (RoleName.ToLower() == "vendor")
                {
                    Response.Redirect("~/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + this.AccountId + "&mode=" + this.Mode + "&pagefrom=vendor");
                }
                else
                {
                    Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + this.AccountId + "&mode=" + this.Mode + "&pagefrom=customer");
                }

            }
            else
            {
                if (RoleName.ToLower() == "admin")
                {
                    Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/Default.aspx");
                }
                else if (RoleName.ToLower() == "franchise")
                {
                    Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/Default.aspx");
                }
                else if (RoleName.ToLower() == "vendor")
                {
                    Response.Redirect("~/Secure/Vendors/VendorAccounts/Default.aspx");
                }
                else
                {
                    Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/Default.aspx");
                }
            }
        }
        #endregion
    }
}