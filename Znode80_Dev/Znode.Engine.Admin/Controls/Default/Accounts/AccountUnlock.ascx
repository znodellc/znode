﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountUnlock.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Accounts.AccountUnlock" %>

<h5>
    <asp:Localize ID="PleaseConfirm" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextPleaseConfirm%>"></asp:Localize></h5>
<p>
    <asp:Localize ID="UnlockAccountConfirm" runat="server" Text="<%$ Resources:ZnodeAdminResource,TextUnlockAccount%>"></asp:Localize>
    <asp:Label ID="lblAccountName" runat="server" Text=""></asp:Label>.
</p>
<asp:Label ID="lblMsg" runat="server" Width="450px" CssClass="Error"></asp:Label><br />
<br />
<div>
    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnUnlockUSer_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonEnableAccount%>" CausesValidation="False" ID="btnUnlock" />
    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel%>" CausesValidation="False" ID="btnCancelBottom" />

</div>
<br />
<br />
<br />

