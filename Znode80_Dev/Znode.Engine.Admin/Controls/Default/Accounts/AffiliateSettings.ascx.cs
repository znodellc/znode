﻿using System;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Controls.Default.Accounts
{
    public partial class AffiliateSettings : System.Web.UI.UserControl
    {
        #region Protected Member Variables
        private int AccountId;
        private ZNodeAddress _billingAddress = new ZNodeAddress();
        private ZNodeAddress _shippingAddress = new ZNodeAddress();
        private decimal DiscountAmount;
        private int Mode;
        private string _RoleName = string.Empty;
        #endregion


        #region Public Properties
        /// <summary>
        /// Gets or sets a value for RoleName
        /// </summary>
        public string RoleName
        {
            get
            {
                return this._RoleName;
            }

            set
            {
                this._RoleName = value;
            }
        }
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            btnSubmitTop.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnSubmitTop.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit.gif") + "'");
            btnCancelTop.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancelTop.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel.gif") + "'");

            btnSubmitBottom.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            btnSubmitBottom.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit.gif") + "'");
            btnCancelBottom.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnCancelBottom.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel.gif") + "'");

            if (HttpContext.Current.Request.Params["itemid"] == null)
            {
                this.AccountId = 0;
            }
            else
            {
                this.AccountId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (Request.Params["pagefrom"] != null)
            {
                this.RoleName = Request.Params["pagefrom"].ToString();
            }

            if (HttpContext.Current.Request.Params["mode"] == null)
            {
                this.Mode = 0;
            }
            else
            {
                this.Mode = int.Parse(Request.Params["mode"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindTrackingStatus();
                this.BindReferral();
                this.BindData();
                discAmountValidator.ErrorMessage = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDiscountAmount").ToString(), 0.ToString("N") + "- 9999999");
                discPercentageValidator.ErrorMessage = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPercentageAmount").ToString(), 0.ToString("N") + "- 100");

                if (this.AccountId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditAffiliateSettings").ToString();
                    amountOwed.Visible = true;
                    lblAmountOwed.Visible = true;
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddAffiliateSettings").ToString();
                    amountOwed.Visible = false;
                    lblAmountOwed.Visible = false;
                }
            }
        }

        /// <summary>
        /// Discount Type select index change event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DiscountType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ToggleDiscountValidator();
        }

        /// <summary>
        /// Referral Status List Control Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstReferralStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lstReferralStatus.SelectedValue == "A" && this.AccountId > 0)
            {
                // Get the portalid of the account
                var accountprofileService = new AccountProfileService();
                var portalProfileService = new PortalProfileService();
                var accountProfile = new TList<AccountProfile>();
                var portalProfile = new TList<PortalProfile>();
                var domainURL = new TList<Domain>();
                var domainAdmin = new DomainAdmin();
                string domainName = string.Empty;

                accountProfile = accountprofileService.GetByAccountID(this.AccountId);

                if (accountProfile != null && accountProfile.Any())
                {
                    portalProfile =
                        portalProfileService.GetByProfileID(accountProfile[0].ProfileID.GetValueOrDefault(0));
                }

                if (portalProfile != null && portalProfile.Any())

                    domainURL = domainAdmin.GetDomainByPortalID(portalProfile[0].PortalID);
                if (domainURL != null && domainURL.Any())
                {
                    domainName = "http://" + domainURL.FirstOrDefault().DomainName;
                    string affiliateLink = domainName + "/default.aspx?affiliate_id=" + this.AccountId;
                    hlAffiliateLink.Text = affiliateLink;
                    hlAffiliateLink.NavigateUrl = affiliateLink;
                }
                else
                {
                    dvTrackingLink.Visible = false;
                    lblAffiliateLinkError.Text = string.Empty;
                    lblAffiliateLinkError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAffiliateLink").ToString();
                }
            }
            else
            {
                hlAffiliateLink.Text = "NA";
                hlAffiliateLink.NavigateUrl = string.Empty;
            }
        }

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            Page.Validate();
            if (!Page.IsValid)
            {
                return;
            }

            ZNode.Libraries.Admin.AccountAdmin _UserAccountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Account _UserAccount = new Account();

            decimal discountAmt = 0;
            if (Discount.Text != string.Empty && decimal.TryParse(Discount.Text, out discountAmt))
            {
                _UserAccount.ReferralCommission = discountAmt;
                this.DiscountAmount = discountAmt;
            }
            else if (!string.IsNullOrEmpty(Discount.Text))
            {
                return;
            }

            this.ToggleDiscountValidator();

            if (this.AccountId > 0)
            {
                _UserAccount = _UserAccountAdmin.GetByAccountID(this.AccountId);
            }

            if (this.AccountId == 0)
            {
                _UserAccount.CreateDte = System.DateTime.Now;
            }

            // Referral Detail        
            if (lstReferral.SelectedIndex != -1)
            {
                _UserAccount.ReferralCommissionTypeID = Convert.ToInt32(lstReferral.SelectedValue);
            }

            _UserAccount.TaxID = Server.HtmlEncode(txtTaxId.Text);

            if (Discount.Text != string.Empty)
            {
                _UserAccount.ReferralCommission = Convert.ToDecimal(Discount.Text);
            }
            else
            {
                _UserAccount.ReferralCommission = null;
            }

            if (lstReferralStatus.SelectedValue == "I")
            {
                _UserAccount.ReferralStatus = "I";
            }
            else if (lstReferralStatus.SelectedValue == "A")
            {
                _UserAccount.ReferralStatus = "A";
            }
            else if (lstReferralStatus.SelectedValue == "N")
            {
                _UserAccount.ReferralStatus = "N";
            }
            else
            {
                _UserAccount.ReferralStatus = string.Empty;
            }

            bool Check = false;

            if (this.AccountId > 0)
            {
                // Set update date
                _UserAccount.UpdateDte = System.DateTime.Now;

                Check = _UserAccountAdmin.Update(_UserAccount);
            }

            // Check Boolean Value
            if (Check)
            {
                AccountAdmin _accountAdmin = new AccountAdmin();
                Account _account = new Account();
                _account = _accountAdmin.GetByAccountID(this.AccountId);
                string UserName = string.Format("AccoountID: {0}", this.AccountId.ToString());
                if (_account.UserID.HasValue)
                {
                    MembershipUser _user = Membership.GetUser(_account.UserID);
                    UserName = _user.UserName;
                }

                string AssociateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditAffiliateSettings").ToString(), UserName + " Account ");

                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, UserName);

                if (RoleName.ToLower() == "admin")
                {
                    Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=admin");
                }
                else if (RoleName.ToLower() == "franchise")
                {
                    Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=franchise");
                }
                else if (RoleName.ToLower() == "vendor")
                {
                    Response.Redirect("~/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=vendor");
                }
                else
                {
                    Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=customer");
                }
            }
            else
            {
                lblErrorMsg.Text = string.Empty;
            }
        }

        /// <summary>
        ///  Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (RoleName.ToLower() == "admin")
            {
                Response.Redirect("~/Secure/Advanced/Accounts/StoreAdministrators/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=admin");
            }
            else if (RoleName.ToLower() == "franchise")
            {
                Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=franchise");
            }
            else if (RoleName.ToLower() == "vendor")
            {
                Response.Redirect("~/Secure/Vendors/VendorAccounts/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=vendor");
            }
            else
            {
                Response.Redirect("~/Secure/Orders/CustomerManagement/Customers/View.aspx?itemid=" + this.AccountId + "&Mode=affiliate&pagefrom=customer");
            }

        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Bind Account Details
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.Admin.AccountAdmin _UserAccountAdmin = new ZNode.Libraries.Admin.AccountAdmin();
            ZNode.Libraries.DataAccess.Entities.Account _UserAccount = _UserAccountAdmin.GetByAccountID(this.AccountId);

            if (_UserAccount != null)
            {
                // Referral Details
                if (_UserAccount.ReferralCommissionTypeID != null)
                {
                    lstReferral.SelectedValue = _UserAccount.ReferralCommissionTypeID.ToString();
                }

                if (_UserAccount.ReferralCommission != null)
                {
                    Discount.Text = _UserAccount.ReferralCommission.Value.ToString("N");
                    this.DiscountAmount = Convert.ToDecimal(_UserAccount.ReferralCommission);
                }

                if (_UserAccount.ReferralStatus != null)
                {
                    lstReferralStatus.SelectedValue = _UserAccount.ReferralStatus;
                }
                else
                {
                    lstReferralStatus.SelectedValue = _UserAccount.ReferralStatus;
                }

                if (_UserAccount.ReferralStatus == "A")
                {
                    // Get the portalid of the account
                    var accountprofileService = new AccountProfileService();
                    var portalProfileService = new PortalProfileService();
                    var accountProfile = new TList<AccountProfile>();
                    var portalProfile = new TList<PortalProfile>();
                    var domainURL = new TList<Domain>();
                    var domainAdmin = new DomainAdmin();
                    string domainName = string.Empty;

                    accountProfile = accountprofileService.GetByAccountID(this.AccountId);

                    if (accountProfile != null && accountProfile.Any())
                    {
                        portalProfile =
                            portalProfileService.GetByProfileID(accountProfile[0].ProfileID.GetValueOrDefault(0));
                    }

                    if (portalProfile != null && portalProfile.Any())

                        domainURL = domainAdmin.GetDomainByPortalID(portalProfile[0].PortalID);
                    if (domainURL != null && domainURL.Any())
                    {
                        domainName = "http://" + domainURL.FirstOrDefault().DomainName;
                        string affiliateLink = domainName + "/default.aspx?affiliate_id=" + this.AccountId;
                        hlAffiliateLink.Text = affiliateLink;
                        hlAffiliateLink.NavigateUrl = affiliateLink;
                    }
                    else
                    {
                        dvTrackingLink.Visible = false;
                        lblAffiliateLinkError.Text = string.Empty;
                        lblAffiliateLinkError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAffiliateLink").ToString();
                    }
                }
                else
                {
                    hlAffiliateLink.Text = "NA";
                }

                AccountHelper helperAccess = new AccountHelper();
                DataSet MyDataSet = helperAccess.GetCommisionAmount(ZNodeConfigManager.SiteConfig.PortalID, this.AccountId.ToString());

                lblAmountOwed.Text = "$" + MyDataSet.Tables[0].Rows[0]["CommissionOwed"].ToString();
                this.ToggleDiscountValidator();
                txtTaxId.Text = Server.HtmlDecode(_UserAccount.TaxID);
            }
        }

        /// <summary>
        /// Binds Referral type drop-down list
        /// </summary>
        private void BindReferral()
        {
            ZNode.Libraries.Admin.ReferralCommissionAdmin ReferralType = new ReferralCommissionAdmin();
            lstReferral.DataSource = ReferralType.GetAll();
            lstReferral.DataTextField = "Name";
            lstReferral.DataValueField = "ReferralCommissiontypeID";
            lstReferral.DataBind();
        }

        /// <summary>
        /// Binds Tracking Status drop-down list
        /// </summary>
        private void BindTrackingStatus()
        {
            Array names = Enum.GetNames(typeof(ZNodeApprovalStatus.ApprovalStatus));
            Array values = Enum.GetValues(typeof(ZNodeApprovalStatus.ApprovalStatus));

            // Clear list items
            lstReferralStatus.Items.Clear();

            // Add default value to the item
            lstReferralStatus.Items.Add(new ListItem("Inactive", string.Empty));

            for (int i = 0; i < names.Length; i++)
            {
                ListItem listitem = new ListItem(ZNodeApprovalStatus.GetEnumValue(values.GetValue(i)), names.GetValue(i).ToString());
                lstReferralStatus.Items.Add(listitem);
            }
        }

        /// <summary>
        /// Enable/Disable Percentage/Amount validator on Discount field
        /// </summary>
        private void ToggleDiscountValidator()
        {
            int Id = int.Parse(lstReferral.SelectedValue);

            discAmountValidator.Enabled = false;
            discPercentageValidator.Enabled = false;

            if (Id == 1)
            {
                // Percentage Discount 
                if ((this.DiscountAmount <= 1) || (this.DiscountAmount >= 100))
                {
                    discPercentageValidator.Enabled = true;
                    discPercentageValidator.SetFocusOnError = true;
                    discAmountValidator.Enabled = false;
                }
            }
            else if (Id == 2)
            {
                // Amount Discount
                if ((this.DiscountAmount <= 1) || (this.DiscountAmount >= 9999999))
                {
                    discAmountValidator.Enabled = true;
                    discAmountValidator.SetFocusOnError = true;
                    discPercentageValidator.Enabled = false;
                }
            }
        }
        #endregion
    }
}