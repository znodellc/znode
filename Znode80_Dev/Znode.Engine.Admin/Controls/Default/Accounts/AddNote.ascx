﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AddNote.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Accounts.AddNote" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>


<div class="FormView">
    <div class="LeftFloat" style="width: 70%; text-align: left">
        <h1></h1>
    </div>
    <div style="text-align: right; padding-right: 10px;">
        <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit%>" CausesValidation="true" ID="btnSubmitTop" />
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel%>" CausesValidation="False" ID="btnCancelTop" />
    </div>
    <div class="ClearBoth">
    </div>
    <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    <div class="FieldStyle">
        <asp:Localize ID="NoteTitle" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnNoteTitle%>"></asp:Localize>
        <span class="Asterix">*</span>
    </div>
    <div class="ValueStyle">
        <asp:TextBox ID='txtNoteTitle' runat='server' MaxLength="50" Columns="50"></asp:TextBox>
        <asp:Label ID="lblErrorTitle" runat="server" CssClass="Error" Text="Label" Visible="False"></asp:Label>
    </div>
    <div class="FieldStyle">
        <asp:Localize ID="NoteBody" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnNoteBody%>"></asp:Localize>
        <span class="Asterix">*</span>
    </div>
    <div class="ValueStyle">
        <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
    </div>
    <div class="FieldStyle"></div>
    <div class="ValueStyle">
        <asp:Label ID="lblError" CssClass="Error" runat="server" Text="Label"
            Visible="False"></asp:Label>
    </div>
    <div class="ClearBoth">
        <br />
    </div>
    <div>
        <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit%>" CausesValidation="true" ID="btnSubmitBottom" />
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel%>" CausesValidation="False" ID="btnCancelBottom" />
    </div>

</div>
