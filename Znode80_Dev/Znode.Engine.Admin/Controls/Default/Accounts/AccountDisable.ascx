﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AccountDisable.ascx.cs" Inherits="Znode.Engine.Admin.Controls.Default.Accounts.AccountDisable" %>
<h5><asp:Localize ID="PleaseConfirm" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextPleaseConfirm%>"></asp:Localize></h5>
<p>
    <asp:Localize ID="DisableAccountConfirm" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextDisableAccount%>"></asp:Localize>
    <asp:Label ID="lblAccountName" runat="server" Text=""></asp:Label>.
</p>
<br />
<br />
<div>
    <zn:Button runat="server" Width="170px" ButtonType="EditButton" OnClick="BtnDisable_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonDisableAccount%>" CausesValidation="False" ID="btnDelete" />
    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel%>" CausesValidation="False" ID="btnCancel" />
</div>
<br />
<br />
<br />

