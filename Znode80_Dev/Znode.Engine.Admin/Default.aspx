<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/Login.master" AutoEventWireup="true" Inherits="Znode.Engine.Admin.Admin_Default" Title="Site Administration" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Login">
        <h1><asp:Localize ID="MerchantLogin" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleMerchantLogin %>'></asp:Localize></h1>
        <p>
            <asp:Localize ID="TextTitleMerchantLogin" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextTitleMerchantLogin %>'></asp:Localize>
        </p>
        <div>
            <znode:Spacer ID="Spacer2" SpacerHeight="20" SpacerWidth="10" runat="server" />
        </div>
        <asp:Panel ID="LoginPanel" runat="server" DefaultButton="LoginButton">
            <div class="AccessDenied">
                <asp:Label ID="lblaccess" runat="server" Visible="False" Text='<%$ Resources:ZnodeAdminResource, ErrorAccessDenied %>' CssClass="Error"></asp:Label></div>
            <div class="FormView">
                <div class="FieldStyle" style="width: 100px;">
                    <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName"><b><asp:Localize ID="LoginUserName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnUserName %>'></asp:Localize></b></asp:Label>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="UserName" runat="server" autocomplete="off" Width="140px"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredLoginUserName %>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredLoginUserName %>' ValidationGroup="uxLogin"
                        Display="Dynamic" CssClass="Error"><asp:Localize ID="RequiredLoginUserName" runat="server" Text='<%$ Resources:ZnodeAdminResource, RequiredLoginUserName %>'></asp:Localize></asp:RequiredFieldValidator>
                </div>
                <div class="FieldStyle" style="width: 100px;">
                    <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password"><b><asp:Localize ID="LoginPassword" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPassword %>'></asp:Localize></b></asp:Label>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="Password" runat="server" autocomplete="off" TextMode="Password" Width="140px"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredPassword %>' ToolTip='<%$ Resources:ZnodeAdminResource, RequiredPassword %>' ValidationGroup="uxLogin"
                        Display="Dynamic" CssClass="Error"><asp:Localize ID="PasswordRequiredText" runat="server" Text='<%$ Resources:ZnodeAdminResource, RequiredPassword %>'></asp:Localize></asp:RequiredFieldValidator>
                </div>
                <div class="ClearBoth">
                </div>
                <div class="Error">
                    <asp:Literal ID="FailureText" runat="server" EnableViewState="False"></asp:Literal>
                </div>
                <div>
                    <znode:Spacer ID="Spacer4" SpacerHeight="5" SpacerWidth="10" runat="server" />
                </div>
                <div style="padding-left: 105px; height: 45px;">
                    <div>
                        <zn:LinkButton ID="LoginButton" runat="server" CommandName="Login" OnClick="LoginButton_Click" ButtonType="LoginButton" ValidationGroup="uxLogin" ButtonPriority="Normal" Text='<%$ Resources:ZnodeAdminResource, ButtonLogin %>' />
                    </div>
                    <div>
                        <div>
                            <div>
                                <znode:Spacer ID="Spacer5" SpacerHeight="15" SpacerWidth="10" runat="server" />
                            </div>
                            <div>
                                <a id="forgotPasswordLink" href='' class="forgotpassword" runat="server"><asp:Localize ID="ForgotPasswordLinkText" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextForgotPassword %>'></asp:Localize></a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
