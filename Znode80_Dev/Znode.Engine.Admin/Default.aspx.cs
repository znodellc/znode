using System;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin
{
    public partial class Admin_Default : System.Web.UI.Page
    {

        #region Variables
        //Znode Version 7.2.2
        //Site Admin Section, Error message for account gets locked - Start 
        //Gets the membership provider details for the key "ZNodeMembershipProvider".

        #region Private Variables
        private MembershipProvider _membershipProvider = Membership.Providers["ZNodeMembershipProvider"];
        #endregion

        #region Public Variables
        public MembershipProvider MembershipProvider
        {
            get { return _membershipProvider; }
            set { _membershipProvider = value; }
        }
        #endregion

        //Site Admin Section, Error message for account gets locked - End
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                Session.Abandon();

                FormsAuthentication.SignOut();
            }

            //instantiated just to trigger licensing > Do not remove!
            ZNodeHelper hlp = new ZNodeHelper();

            //Set input focus on the page load
            UserName.Focus();

            string inURL = Request.Url.ToString();

            //check if SSL is required
            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {                    
                    string outURL = inURL.ToLower().Replace("http://", "https://");

                    Response.Redirect(outURL);
                }
            }

            //Forgot password link 
            string link = "~/ForgotPassword.aspx";
            forgotPasswordLink.HRef = link.ToLower().Replace("https://", "http://");

            if (Page.User.Identity.IsAuthenticated)
            {
                lblaccess.Visible = true;
            }            
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            ZNodeUserAccount userAcct = new ZNodeUserAccount();
            bool loginSuccess = userAcct.Login(ZNodeConfigManager.SiteConfig.PortalID, UserName.Text.Trim(), Password.Text.Trim());

            if (loginSuccess)
            {
                string retValue = string.Empty;

                bool status = ZNodeUserAccount.CheckLastPasswordChangeDate((Guid)userAcct.UserID, out retValue);

                if (!status)
                {
                    ZNode.Libraries.DataAccess.Service.AccountService acctService = new ZNode.Libraries.DataAccess.Service.AccountService();
                    ZNode.Libraries.DataAccess.Entities.Account account = acctService.GetByAccountID(userAcct.AccountID);

                    // Set Error Code to session Object
                    Session.Add("ErrorCode", retValue);

                    // Get account and set to session
                    Session.Add("AccountObject", account);

                    Response.Redirect("~/ResetPassword.aspx");
                }

                // Set CurrentUserProfile ProfileID
                userAcct.ProfileID = ZNodeProfile.CurrentUserProfileId;

                //get account and set to session
                Session.Add(ZNodeSessionKeyType.UserAccount.ToString(), userAcct);

                FormsAuthentication.SetAuthCookie(UserName.Text.Trim(), false);

                if (Roles.IsUserInRole(UserName.Text, "ORDER APPROVER") && !Roles.IsUserInRole(UserName.Text, "ADMIN"))
                    Response.Redirect("~/Secure/Orders/OrderManagement/ViewOrders/Default.aspx", true);

                if ((Roles.IsUserInRole(UserName.Text.Trim(), "REVIEWER") || Roles.IsUserInRole(UserName.Text.Trim(), "REVIEWER MANAGER")) && !Roles.IsUserInRole(UserName.Text, "ADMIN"))
                {
                    Response.Redirect("~/Secure/Vendors/VendorProducts/Default.aspx", true);
                }

                // if user is an admin, then redirect to the admin dashboard
                if (Roles.IsUserInRole(UserName.Text.Trim(), "ADMIN") && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
                {
                    Response.Redirect("~/secure/default.aspx", true);
                }
                else if (Roles.IsUserInRole(UserName.Text.Trim(), "FRANCHISE") && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
                {
                    Response.Redirect("~/franchiseAdmin/secure/default.aspx", true);
                }
                else if (Roles.IsUserInRole(UserName.Text.Trim(), "CUSTOMER SERVICE REP") || Roles.IsUserInRole(UserName.Text.Trim(), "CATALOG EDITOR") || Roles.IsUserInRole(UserName.Text.Trim(), "EXECUTIVE") || Roles.IsUserInRole(UserName.Text.Trim(), "ORDER ONLY") || Roles.IsUserInRole(UserName.Text.Trim(), "SEO") || Roles.IsUserInRole(UserName.Text.Trim(), "CONTENT EDITOR") && string.IsNullOrEmpty(Request.QueryString["returnurl"]))
                {  
                    Response.Redirect("~/secure/default.aspx", true);
                }
                FormsAuthentication.RedirectFromLoginPage(UserName.Text.Trim(), false);
            }
            else
            {
                //Znode Version 7.2.2
                //Site Admin Section, Error message for account gets locked - Start 
                //Set the Error messages in case of Account gets locked. And Also display warning messages before the final last two attempts.


                //Gets the user details based on username.
                MembershipUser membershipUser = Membership.GetUser(UserName.Text.Trim());
               
                if (membershipUser != null)
                {
                    //Check whether user accoung is locked or not.
                    if (membershipUser.IsLockedOut)
                    {
                        FailureText.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountLockedOut").ToString();
                    }
                    else
                    {
                        //Gets current failed password atttemt count for setting the message.
                        AccountHelper accountHelper = new AccountHelper();
                        int inValidAttemtCount = accountHelper.GetUserFailedPasswordAttemptCount((Guid)membershipUser.ProviderUserKey);
                        if (inValidAttemtCount > 0)
                        {
                            //Gets Maximum failed password atttemt count from web.config
                            int maxInvalidPasswordAttemptCount = _membershipProvider.MaxInvalidPasswordAttempts;

                            //Set warning error at (MaxFailureAttemptCount - 2) & (MaxFailureAttemptCount - 1) attempt.
                            FailureText.Text = ((maxInvalidPasswordAttemptCount - inValidAttemtCount) == 2)
                                ? this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountLockedOutAfterTwoAttempt").ToString()
                                : ((maxInvalidPasswordAttemptCount - inValidAttemtCount) == 1)
                                    ? this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountLockedOutAfterOneAttempt").ToString()
                                    : this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorLoginFailure").ToString();

                        }
                        else
                        {
                            FailureText.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorLoginFailure").ToString();
                        }
                    }
                }//Site Admin Section, Error message for account gets locked - End
                else
                {
                    FailureText.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorLoginFailure").ToString();
                }
            }
        }
    }
}