﻿<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Marketing.SEO.GoogleSiteMap.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div class="LeftFloat" style="width: 70%">
        <h1>
            <asp:Localize ID="LinkTextGoogleSiteMap" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextGoogleSiteMap%>'></asp:Localize></h1>
    </div>
    <div class="LeftFloat" align="right" style="width: 30%">
        <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmitTop" ValidationGroup="sitemap" />
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelTop" />
    </div>
    <div class="ClearBoth" align="left">
    </div>
    <div class="YellowBox" runat="server" id="dvMsg" visible="false">
        <div>
            <asp:Label ID="lblmsg" Font-Bold="true" runat="server"></asp:Label><br />
        </div>
        <div>
            <asp:PlaceHolder runat="server" ID="plfileNames"></asp:PlaceHolder>
        </div>
    </div>
    <div class="FormView">
        <p>
            <asp:Localize ID="TextGoogleSiteMap" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextGoogleSiteMap%>'></asp:Localize>
        </p>
        <br />
        <div class="FieldStyle">
            <asp:Localize ID="ColumnFrequency" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFrequency%>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlFrequency" runat="server">
                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextDaily%>' Value="Daily"></asp:ListItem>
                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextAlways%>' Value="Always"></asp:ListItem>
                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextHourly%>' Value="Hourly"></asp:ListItem>
                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextWeekly%>' Value="Weekly"></asp:ListItem>
                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextMonthly%>' Value="Monthly"></asp:ListItem>
                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextYearly%>' Value="Yearly"></asp:ListItem>
                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextNever%>' Value="Never"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <asp:UpdatePanel ID="updLasMod" runat="server">
            <ContentTemplate>
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnLastmodification" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLastmodification%>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:RadioButtonList ID="rdbLastModified" runat="server" RepeatDirection="Vertical"
                        AutoPostBack="true" OnSelectedIndexChanged="RdbLastModified_SelectedIndexChanged">
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, RadioTextNone%>' Value="None" Selected="True"></asp:ListItem>
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, RadioTextDbUpdate%>' Value="DB"></asp:ListItem>
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, RadioTextCustomUpdate%>' Value="CUSTOM"></asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:TextBox ID="txtLastModDateTime" runat="server" Visible="false"></asp:TextBox>
                    <asp:CustomValidator runat="server" ID="valDateRange" Display="Dynamic" ControlToValidate="txtLastModDateTime"
                        OnServerValidate="valDateRange_ServerValidate" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidDate%>'
                        SetFocusOnError="True" ValidationGroup="sitemap" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnPriority" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPriority%>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlPrioroty" runat="server">
                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextSelectPriority%>' Value="0"></asp:ListItem>
                <asp:ListItem Text="1.0" Value="1.0"></asp:ListItem>
                <asp:ListItem Text="1.1" Value="1.1"></asp:ListItem>
                <asp:ListItem Text="1.2" Value="1.2"></asp:ListItem>
                <asp:ListItem Text="1.3" Value="1.3"></asp:ListItem>
                <asp:ListItem Text="1.4" Value="1.4"></asp:ListItem>
                <asp:ListItem Text="1.5" Value="1.5"></asp:ListItem>
                <asp:ListItem Text="1.6" Value="1.6"></asp:ListItem>
                <asp:ListItem Text="1.7" Value="1.7"></asp:ListItem>
                <asp:ListItem Text="1.8" Value="1.8"></asp:ListItem>
                <asp:ListItem Text="1.9" Value="1.9"></asp:ListItem>
                <asp:ListItem Text="2.0" Value="2.0"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <asp:UpdatePanel ID="updType" runat="server">
            <ContentTemplate>
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnTypeXMLSiteMap" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTypeXMLSiteMap%>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlType" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DdlType_SelectedIndexChanged">
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextXMLSiteMap%>' Value="GoogleXML"></asp:ListItem>
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextGoogleProductFeed%>' Value="GoogleProduct"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterType%>'
                        ToolTip='<%$ Resources:ZnodeAdminResource, RequiredEnterType%>' ControlToValidate="ddlType" ValidationGroup="sitemap"
                        CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div visible="false" runat="server" id="dvSiteMap">
                    <div class="FieldStyle">
                        <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnXMLSiteMap%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:RadioButtonList ID="chkXMLSiteMapType" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, RadioTextCategory%>' Value="Category"></asp:ListItem>
                            <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, RadioTextContentPages%>' Value="Content"></asp:ListItem>
                            <%--<asp:ListItem Text="All" Value="All"></asp:ListItem>--%>
                        </asp:RadioButtonList>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnXMLFileName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnXMLFileName%>'></asp:Localize>
            <br />
            <small>
                <asp:Localize ID="ColumnTextXMLFileName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextXMLFileName%>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtXMLFileName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterFileName%>'
                ToolTip='<%$ Resources:ZnodeAdminResource, RequiredEnterFileName%>' ControlToValidate="txtXMLFileName" ValidationGroup="sitemap"
                CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtXMLFileName"
                CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularXMLFileName%>'
                ValidationGroup="sitemap" SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_]+)"></asp:RegularExpressionValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="SubTitleSelectStores" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSelectStores%>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <div style="margin-left: 7px; margin-top: 5px;">
                <asp:CheckBox ID="chkAllStores" runat="server" Text="All Stores" AutoPostBack="true"
                    OnCheckedChanged="ChkAllStores_checkedChanged" />
            </div>
        </div>
        <div class="FieldStyle">
        </div>
        <div class="ValueStyle">
            <asp:CheckBoxList ID="StoreCheckList" runat="server" RepeatDirection="Horizontal" RepeatColumns="3"
                CellPadding="5" />
        </div>
        <div class="ClearBoth" align="left">
            <br />
        </div>
        <div>
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmit" ValidationGroup="sitemap" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="ImageButton1" />
        </div>
    </div>
</asp:Content>
