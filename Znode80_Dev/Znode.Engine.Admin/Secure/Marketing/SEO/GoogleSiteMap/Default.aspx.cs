﻿using System;
using System.Configuration;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.SEO;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Secure.Marketing.SEO.GoogleSiteMap
{
    /// <summary>
    /// Generate the google site map
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Private Variables
        private string strNamespace = ConfigurationManager.AppSettings["SiteMapNameSpace"];
        private ZNodeUrl znodeURL = new ZNodeUrl();
        #endregion

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                BindStores();

                if (ddlType.SelectedValue == "GoogleXML")
                {
                    dvSiteMap.Visible = true;
                    chkXMLSiteMapType.SelectedValue = "Category";
                }
            }
        }

        #region General Events
        /// <summary>
        /// Confirm Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            if (!Page.IsValid) return;

            try
            {
                string strModDate = string.Empty;
                string portalid = "";
                if (!chkAllStores.Checked)
                {
                    foreach (ListItem li in StoreCheckList.Items)
                    {
                        if (li.Selected)
                        {
                            portalid += li.Value + ",";
                        }
                    }
                }

                if (!chkAllStores.Checked && portalid.Length == 0)
                {
                    dvMsg.Visible = true;
                    lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidSelectStoreContinue").ToString();
                    lblmsg.ForeColor = System.Drawing.Color.Red;
                    return;
                }
                // If you select All Stores, then it saves NULL string Value                        
                if (chkAllStores.Checked == true)
                {
                    //portalid = ZNodeConfigManager.SiteConfig.PortalID;
                    portalid = "0";
                }
                else if (portalid.Length > 1)
                {
                    portalid = portalid.Remove(portalid.Length - 1);
                }



                ZNodeXmlSitemap znodeXMLSiteMap = new ZNodeXmlSitemap();
                int fileNameCount = 0;

                if (rdbLastModified.SelectedValue == "CUSTOM")
                {
                    strModDate = txtLastModDateTime.Text;
                }
                else if (rdbLastModified.SelectedValue == "None")
                {
                    strModDate = "None";
                }
                else
                {
                    strModDate = "DB";
                }

                if (ddlType.SelectedValue == "GoogleXML")
                {
                    if (chkXMLSiteMapType.SelectedValue == "Category")
                    {
                        fileNameCount = znodeXMLSiteMap.GetCategoryList(portalid, ddlFrequency.SelectedValue.ToString(), Convert.ToDecimal(ddlPrioroty.SelectedValue), "urlset", this.strNamespace, txtXMLFileName.Text, strModDate);
                    }
                    else if (chkXMLSiteMapType.SelectedValue == "Content")
                    {
                        fileNameCount = znodeXMLSiteMap.GetContentPagesList(portalid, ddlFrequency.SelectedValue.ToString(), Convert.ToDecimal(ddlPrioroty.SelectedValue), "urlset", this.strNamespace, txtXMLFileName.Text, strModDate);
                    }
                }
                else
                {
                    fileNameCount = znodeXMLSiteMap.GetProductList(portalid, ddlFrequency.SelectedValue.ToString(), Convert.ToDecimal(ddlPrioroty.SelectedValue), "urlset", this.strNamespace, txtXMLFileName.Text, strModDate);
                }

                dvMsg.Visible = true;
                lblmsg.ForeColor = System.Drawing.Color.Green;
                lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SuccessXMLSiteMap").ToString();

                if (fileNameCount > 0)
                {
                    string siteMapFile = znodeXMLSiteMap.GenerateGoogleSiteMapIndexFiles(fileNameCount, txtXMLFileName.Text);
                    System.Web.UI.WebControls.HyperLink fileLink = new System.Web.UI.WebControls.HyperLink();
                    fileLink.Text = Request.Url.GetLeftPart(UriPartial.Authority) + Page.ResolveUrl(siteMapFile);
                    fileLink.NavigateUrl = ZNodeStorageManager.HttpPath(siteMapFile);
                    fileLink.Target = "_blank";
                    plfileNames.Controls.Add(fileLink);
                    plfileNames.Controls.Add(new System.Web.UI.HtmlControls.HtmlGenericControl("br"));
                }
                else
                {
                    lblmsg.ForeColor = System.Drawing.Color.Red;
                    lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErroValidNoFilesUploaded").ToString();
                }
            }
            catch (Exception ex)
            {
                // Display error message
                lblmsg.ForeColor = System.Drawing.Color.Red;
                lblmsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateException").ToString(), ex.Message);
            }
        }
        /// <summary>
        /// chkAllStore checked change Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChkAllStores_checkedChanged(object sender, EventArgs e)
        {
            if (chkAllStores.Checked == true)
            {
                StoreCheckList.Visible = false;
            }
            else
            {
                StoreCheckList.Visible = true;
            }
        }
        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            // Redirect to Dashboard
            Response.Redirect("~/Secure/Marketing/Default.aspx");
        }

        /// <summary>
        /// Event fired when lstFilter Selected Index is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RdbLastModified_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rdbLastModified.SelectedValue == "CUSTOM")
            {
                txtLastModDateTime.Visible = true;
                txtLastModDateTime.Text = DateTime.Today.ToString();
            }
            else
            {
                txtLastModDateTime.Visible = false;
            }
        }

        /// <summary>
        /// Event fired when lstFilter Selected Index is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlType.SelectedValue == "GoogleXML")
            {
                dvSiteMap.Visible = true;
                chkXMLSiteMapType.SelectedValue = "Category";
            }
            else
            {
                dvSiteMap.Visible = false;
            }
        }

        protected void valDateRange_ServerValidate(object source, ServerValidateEventArgs args)
        {
            DateTime dt;

            args.IsValid = (DateTime.TryParse(args.Value, out dt)
                            && dt <= DateTime.MaxValue
                            && dt >= DateTime.MinValue);
        }

        #endregion General Events

        #region  Helper Methods

        /// <summary>
        /// Binds Stores in a checkbox list
        /// </summary>
        private void BindStores()
        {
            PortalService portalService = new PortalService();

            // CheckboxList Type
            StoreCheckList.Visible = true;
            StoreCheckList.DataSource = portalService.GetAll();
            StoreCheckList.DataTextField = "StoreName";
            StoreCheckList.DataValueField = "PortalID";
            StoreCheckList.DataBind();

            // Set Vertical Or Horizontal view
            StoreCheckList.RepeatDirection = RepeatDirection.Vertical;
        }


        #endregion
    }
}
