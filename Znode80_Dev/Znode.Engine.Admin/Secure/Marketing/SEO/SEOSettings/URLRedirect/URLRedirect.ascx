﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="URLRedirect.ascx.cs" Inherits="Znode.Engine.Common.Znode.Engine.Admin.Secure.Marketing.SEO.SEOSettings.URLRedirect.URLRedirect" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:UpdatePanel ID="updatepanelProductList" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <div class="LeftFloat" style="width: 60%">
                <h1>
                    <asp:Localize runat="server" ID="TitleURLRedirect" Text='<%$ Resources:ZnodeAdminResource, TitleURLRedirect %>'></asp:Localize>
                </h1>
            </div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="btnAddUrlRedirect" runat="server" CausesValidation="False"
                    ButtonType="Button" OnClick="BtnAddUrlRedirect_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAdd301Redirect %>'
                    ButtonPriority="Primary" />
            </div>
        </div>
        <div class="ClearBoth" align="left">
            <br />
        </div>
        <div class="SearchForm">
            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="SubTitleURLRedirect" Text='<%$ Resources:ZnodeAdminResource, SubTitleURLRedirect %>'></asp:Localize>
            </h4>
            <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="FromURL" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFromURL %>'></asp:Localize>
                        </span>
                        <br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtOldUrl" runat="server"></asp:TextBox></span>
                        <div>
                            <asp:RegularExpressionValidator ID="regProductName" runat="server" ControlToValidate="txtOldUrl"
                                ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidURL %>' Display="Dynamic"
                                CssClass="Error" ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidURL %>'></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="ToURL" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleToURL %>'></asp:Localize>
                        </span>
                        <br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtNewUrl" runat="server"></asp:TextBox></span>
                        <div>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtNewUrl"
                                ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidURL %>' Display="Dynamic"
                                CssClass="Error" ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidURL %>'></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="ColumnTitleURLStatus" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleURLStatus %>'></asp:Localize>
                        </span>
                        <br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlURLStatus" runat="server">
                                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropdownTextAll %>' Value="0"></asp:ListItem>
                                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropdownTextEnabled %>' Value="true"></asp:ListItem>
                                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropdownTextDisabled %>' Value="false"></asp:ListItem>
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                </div>
                <div>
                    <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClearSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                    <zn:Button runat="server" ID="btnSearch" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                </div>
            </asp:Panel>
            <div class="ClearBoth">
            </div>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <h4 class="GridTitle">
            <asp:Localize runat="server" ID="GridTitleURLRedirect" Text='<%$ Resources:ZnodeAdminResource, GridTitleURLRedirect %>'></asp:Localize>
        </h4>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
            AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyTextNoURL %>' OnRowDeleting="UxGrid_RowDeleting"
            OnRowDataBound="UxGrid_RowDataBound">
            <Columns>
                <asp:BoundField DataField="UrlRedirectID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleFromURL %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "OldUrl").ToString().Replace("~/",string.Empty) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleToURL %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "NewUrl").ToString().Replace("~/",string.Empty) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleEnabled %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "IsActive").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:ButtonField CommandName="Edit" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' ButtonType="Link">
                    <ControlStyle CssClass="Button" />
                </asp:ButtonField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnDelete" CssClass="Button" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>'
                            CommandName="Delete" CommandArgument="Delete" />

                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
