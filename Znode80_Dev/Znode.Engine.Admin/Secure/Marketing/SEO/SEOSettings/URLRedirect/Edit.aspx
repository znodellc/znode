<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/Themes/Standard/edit.master" Inherits="Znode.Engine.Admin.Secure.Marketing.SEO.SEOSettings.URLRedirect.Edit" CodeBehind="Edit.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="LeftFloat" style="width: 70%">
        <h1>
            <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
    </div>
    <div class="LeftFloat" align="right" style="width: 30%">
        <zn:Button runat="server" ID="btnSubmitTop" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
        <zn:Button runat="server" ID="btnCancelTop" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
    </div>
    <div class="ClearBoth" align="left">
    </div>
    <div class="FormView">
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleRedirectFrom" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleRedirectFrom %>'></asp:Localize><span class="Asterix">*</span><br />
            <small><asp:Localize ID="ColumnTextOldUrl" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextOldUrl%>'></asp:Localize></small><br />
            <small><asp:Localize ID="ColumnTextCategoryURL" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextCategoryURL%>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtOldUrl" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="OldSeourlFieldValidator" CssClass="Error" ControlToValidate="txtOldUrl"
                runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredFromSeoURL %>'></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleRedirectTo" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleRedirectTo %>'></asp:Localize><span class="Asterix">*</span><br />
            <small><asp:Localize ID="ColumnTextNewUrl" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextNewUrl%>'></asp:Localize></small><br />
            <small><asp:Localize ID="ColumnTextProductNewURL" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextProductURL%>'></asp:Localize></small><br />
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtNewUrl" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="NewSeourlFieldValidator" CssClass="Error" ControlToValidate="txtNewUrl"
                runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredToSeoURL %>'></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleEnableRedirection" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEnableRedirection %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:CheckBox ID='chkIsActive' Checked="true" runat="server" Text="" />
        </div>
        <div class="ClearBoth" align="left">
            <br />
        </div>
        <div>
            <zn:Button runat="server" ID="btnSubmit" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancel" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
        </div>
    </div>
</asp:Content>
