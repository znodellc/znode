<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" ValidateRequest="false"
    AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.SEO.SEOSettings.Content.Edit" CodeBehind="Edit.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="LeftFloat" style="width: 70%">
        <h1>
            <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
    </div>
    <div class="LeftFloat" align="right" style="width: 30%">
        <zn:Button runat="server" ID="btnSubmitTop"  ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
        <zn:Button runat="server" ID="btnCancelTop" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
    </div>
    <div class="ClearBoth" align="left">
    </div>
    <div class="FormView">
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="SEOProductPageAdd">
            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="SubTitleGeneralSettings" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralSettings %>'></asp:Localize>
            </h4>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitlePage" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePageTitle %>'></asp:Localize>
                 <span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTitle"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredPageTitle %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
			<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Display="Dynamic"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularPageTitle %>'
                ControlToValidate="txtTitle" ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidPageTitle %>'></asp:RegularExpressionValidator>
            </div>
            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="SubTitleSEOSettings" Text='<%$ Resources:ZnodeAdminResource, SubTitleSEOSettings %>'></asp:Localize>
            </h4>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEOTitle" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOTitle %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEOKeywords" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOKeywords %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOMetaKeywords" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEODescription" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEODescription %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOMetaDescription" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEOPageName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOPageName %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOUrl" runat="server" MaxLength="100" Columns="50"></asp:TextBox>
                 <div class="tooltip SEOSettings">
                          <a href="javascript:void(0);" class="learn-more"><span>
                              <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                          <div class="content">
                              <h6><asp:Localize runat="server" ID="Help" Text='<%$ Resources:ZnodeAdminResource, HintHelp %>'></asp:Localize></h6>
                              <p>
                                  <asp:Localize runat="server" ID="SEOHint" Text='<%$ Resources:ZnodeAdminResource, SEOHint %>'></asp:Localize>
                              </p>
                          </div>
                      </div>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtSEOUrl"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularSEOPageName %>'
                    SetFocusOnError="True" ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidSEOUrl %>'></asp:RegularExpressionValidator>
            </div>
            <div class="ValueStyle" style="display: none">
                <asp:CheckBox ID="chkAddURLRedirect" runat="server" Text='' />
            </div>
            <div class="ClearBoth" align="left">
                <br />
            </div>
            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="GridTitlePageContent" Text='<%$ Resources:ZnodeAdminResource, SubTitlePageContent %>'></asp:Localize>
            </h4>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
            </div>
            <div class="ClearBoth" align="left">
                <br />
            </div>
        </div>
        <div>
        <zn:Button runat="server" ID="btnSubmit"  ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
        <zn:Button runat="server" ID="btnCancel" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
        </div>
    </div>
</asp:Content>
