<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" ValidateRequest="false"
    AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.SEO.SEOSettings.Categories.Edit" CodeBehind="Edit.aspx.cs" %>

<%@ Register Src="~/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="LeftFloat" style="width: 70%">
        <h1>
            <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
    </div>
    <div class="LeftFloat" align="right" style="width: 30%">
        <zn:Button runat="server" ID="btnSubmitTop" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
        <zn:Button runat="server" ID="btnCancelTop" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
    </div>
    <div class="ClearBoth" align="left">
    </div>
    <div class="FormView">
        <asp:Label ID="lblError" CssClass="Error" runat="server"></asp:Label>
        <div class="SEOProductPageAdd">
            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="SubTitleGeneralSettings" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralSettings %>'></asp:Localize>
            </h4>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleCategoryTitle" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCategoryTitle %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID='txtTitle' runat='server' MaxLength="255" Columns="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtTitle"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredCategoryTitle %>' CssClass="Error" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </div>
            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="SubTitleSEOSettings" Text='<%$ Resources:ZnodeAdminResource, SubTitleSEOSettings %>'></asp:Localize>
            </h4>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEOTitle" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOTitle %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEOKeywords" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOKeywords %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOMetaKeywords" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEODescription" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEODescription %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOMetaDescription" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEOPageName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOPageName %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOURL" runat="server" MaxLength="100" Columns="50"></asp:TextBox>
                <div class="tooltip SEOSettings">
                    <a href="javascript:void(0);" class="learn-more"><span>
                        <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                    <div class="content">
                        <h6>
                            <asp:Localize runat="server" ID="Help" Text='<%$ Resources:ZnodeAdminResource, HintHelp %>'></asp:Localize></h6>
                        <p>
                            <asp:Localize runat="server" ID="SEOHint" Text='<%$ Resources:ZnodeAdminResource, SEOHint %>'></asp:Localize>
                        </p>
                    </div>
                </div>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtSEOURL"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularSEOPageName %>'
                    SetFocusOnError="True" ValidationExpression='<%$ Resources:ZnodeAdminResource,  ValidSEOUrl %>'></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitle301Redirect" Text='<%$ Resources:ZnodeAdminResource, ColumnTitle301Redirect %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkAddURLRedirect" runat="server" Text='<%$ Resources:ZnodeAdminResource,  CheckBoxEnable301Redirect %>' />
            </div>
            <div class="ClearBoth" align="left">
                <br />
            </div>
            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="SubTitleDescription" Text='<%$ Resources:ZnodeAdminResource, SubTitleDescription %>'></asp:Localize>
            </h4>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleShortDescription" Text='<%$ Resources:ZnodeAdminResource, ColumnShortDescription %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtshortdescription" runat="server" Width="300px" TextMode="MultiLine"
                    Height="75px" MaxLength="100"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleLongDescription" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleLongDescription %>'></asp:Localize>
                <span class="Asterix">*</span>
            </div>
            <div class="ValueStyle" style="width: 70%">
                <ZNode:HtmlTextBox ID="ctrlHtmlText" runat="server"></ZNode:HtmlTextBox>
            </div>
            <div class="ClearBoth" align="left">
                <br />
            </div>
        </div>
        <div>
            <zn:Button runat="server" ID="btnSubmit" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancel" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
        </div>
    </div>
</asp:Content>
