using System;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Marketing.SEO.SEOSettings.Categories
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Marketing.SEO.SEOSettings.Categories.Edit class
    /// </summary>
    public partial class Edit : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        private string ManagePageLink = "~/Secure/Marketing/SEO/SEOSettings/Default.aspx?mode=category";
        #endregion

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                // If edit func then bind the data fields
                if (this.ItemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditCategorySEO").ToString();
                    this.BindEditData();
                }
            }
        }

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        protected void BindEditData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            Category category = categoryAdmin.GetByCategoryId(this.ItemId);

            if (category != null)
            {
                // Checking the user profile for roles and permission and then loading the data based on roles
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (!UserStoreAccess.CheckUserRoleInCategory(profiles, this.ItemId))
                {
                    Response.Redirect(this.ManagePageLink);
                }
                
                lblTitle.Text += category.Title;
                txtTitle.Text = Server.HtmlDecode(category.Title);
                txtshortdescription.Text = Server.HtmlDecode(category.ShortDescription);
                ctrlHtmlText.Html = Server.HtmlDecode(category.Description);
                txtSEOMetaDescription.Text = Server.HtmlDecode(category.SEODescription);
                txtSEOMetaKeywords.Text = Server.HtmlDecode(category.SEOKeywords);
                txtSEOTitle.Text = Server.HtmlDecode(category.SEOTitle);
                txtSEOURL.Text = category.SEOURL;
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorRequestCategory").ToString());
            }
        }

        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            Category category = new Category();
            string mappedSEOUrl = string.Empty;

            if (this.ItemId > 0)
            {
                category = categoryAdmin.GetByCategoryId(this.ItemId);

                if (category.SEOURL != null)
                {
                    mappedSEOUrl = category.SEOURL;
                }
            }

            category.CategoryID = this.ItemId;
            category.ShortDescription = Server.HtmlEncode(txtshortdescription.Text);
            category.Description = ctrlHtmlText.Html;
            category.Title = Server.HtmlEncode(txtTitle.Text);
            category.SEOTitle = Server.HtmlEncode(txtSEOTitle.Text);
            category.SEOKeywords = Server.HtmlEncode(txtSEOMetaKeywords.Text);
            category.SEODescription = Server.HtmlEncode(txtSEOMetaDescription.Text);
            category.SEOURL = null;
            if (txtSEOURL.Text.Trim().Length > 0)
            {
                category.SEOURL = txtSEOURL.Text.Trim().Replace(" ", "-");
            }

            if (string.Compare(category.SEOURL, mappedSEOUrl, true) != 0)
            {
                if (urlRedirectAdmin.SeoUrlExists(category.SEOURL, category.CategoryID))
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSEOPageName").ToString();
                    return;
                }
            }

            bool retval = false;

            if (this.ItemId > 0)
            {
                retval = categoryAdmin.Update(category);

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditCategorySEO") + category.Title, category.Title);
            }

            if (retval)
            {
                if (chkAddURLRedirect.Checked)
                {
                    urlRedirectAdmin.AddUrlRedirectTable(SEOUrlType.Category, mappedSEOUrl, category.SEOURL, category.CategoryID.ToString());
                }

                urlRedirectAdmin.UpdateUrlRedirect(mappedSEOUrl);

                Response.Redirect(this.ManagePageLink);
            }
            else
            {
                if (this.ItemId > 0)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSEOCategory").ToString();
                }

                return;
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ManagePageLink);
        }

        #endregion
    }
}