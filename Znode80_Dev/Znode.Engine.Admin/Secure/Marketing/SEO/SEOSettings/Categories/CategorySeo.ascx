﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CategorySeo.ascx.cs" Inherits="Znode.Engine.Common.Znode.Engine.Admin.Secure.Marketing.SEO.SEOSettings.Categories.CategorySeo" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:UpdatePanel ID="updatepanelProductList" runat="server">
    <ContentTemplate>
        <div class="SEO">
            <div class="SearchForm">
                <p>
                    <asp:Localize runat="server" ID="TextSEOCategory" Text='<%$ Resources:ZnodeAdminResource, TextSEOCategory %>'></asp:Localize>
                </p>
                <h4 class="SubTitle">
                    <asp:Localize runat="server" ID="SubTitleSearchCategories" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchCategories %>'></asp:Localize>
                </h4>
                <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">
                                <asp:Localize runat="server" ID="ColumnTitleName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>'></asp:Localize>
                            </span>
                            <br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtCategoryName" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">
                                <asp:Localize runat="server" ID="ColumnTitleCatalog" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCatalog %>'></asp:Localize>
                            </span>
                            <br />
                            <span class="ValueStyle">
                                <asp:DropDownList ID="ddlCatalog" runat="server">
                                </asp:DropDownList>
                            </span>
                        </div>
                    </div>
                    <div class="ClearBoth">
                        <br />
                    </div>
                    <div>
                        <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                        <zn:Button runat="server" ID="btnSearch" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                    </div>
                </asp:Panel>
                <div class="ClearBoth">
                    <br />
                </div>
                <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
                <h4 class="GridTitle">
                    <asp:Localize runat="server" ID="GridTitleCategoryList" Text='<%$ Resources:ZnodeAdminResource, GridTitleCategoryList %>'></asp:Localize>
                </h4>
                <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" OnSorting="UxGrid_Sorting"
                    AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                    OnPageIndexChanging="UxGrid_PageIndexChanging" CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand"
                    Width="100%" EnableSortingAndPagingCallbacks="False" PageSize="25" AllowSorting="True"
                    EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyTextNoCategories %>'>
                    <Columns>
                        <asp:BoundField DataField="CategoryID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="60%">
                            <ItemTemplate>
                                <a href='Categories/Edit.aspx?itemid=<%# DataBinder.Eval(Container.DataItem,"CategoryId").ToString()%>'>
                                    <%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleEnabled %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <img id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "VisibleInd").ToString()))%>'
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="btnEdit" CssClass="actionlink" runat="server" CommandName="Edit"
                                    Text='<%$ Resources:ZnodeAdminResource, LinkManageSEO %>' CommandArgument='<%# Eval("CategoryID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                </asp:GridView>
                <div>
                    <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
