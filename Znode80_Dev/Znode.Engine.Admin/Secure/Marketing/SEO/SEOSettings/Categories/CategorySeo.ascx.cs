﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;


namespace Znode.Engine.Common.Znode.Engine.Admin.Secure.Marketing.SEO.SEOSettings.Categories
{
    public partial class CategorySeo : System.Web.UI.UserControl
    {
        #region Protected Variables

        private static bool IsSearchEnabled = false;
        private string CatalogImagePath = string.Empty;
        private string EditLink = "~/Secure/Marketing/SEO/SEOSettings/Categories/Edit.aspx";

        #endregion

        #region Protected properties
        /// <summary>
        /// Gets or sets the GridViewSortDirection
        /// </summary>
        private string GridViewSortDirection
        {
            get
            {
                return ViewState["SortDirection"] as string ?? "ASC";
            }

            set
            {
                ViewState["SortDirection"] = value;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindCatalogData();
                this.BindGridData();
            }
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Search data
        /// </summary>
        protected void BindSearchData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            DataSet ds = categoryAdmin.GetCategoriesBySearchData(txtCategoryName.Text.Trim(), ddlCatalog.SelectedValue);
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = "Name";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind Store Catalogs
        /// </summary>
        protected void BindCatalogData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            TList<Catalog> catalogList = catalogAdmin.GetAllCatalogs();
            DataSet ds = catalogList.ToDataSet(false);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                }
            }

            DataView dataView = ds.Tables[0].DefaultView;
            string catalogIds = "0";
            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                catalogIds = catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess);
                dataView.RowFilter = string.Concat("CatalogID IN (", catalogIds, ")");
            }

            ddlCatalog.DataSource = dataView;
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();

            if (string.Compare(profiles.StoreAccess, "AllStores") == 0)
            {
                ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), "0");
                ddlCatalog.Items.Insert(0, li);
            }
            else
            {
                ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), string.Concat(catalogIds, ",0"));
                ddlCatalog.Items.Insert(0, li);
            }
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (IsSearchEnabled)
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindSearchData();
            }
            else
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindGridData();
            }
        }

        /// <summary>
        /// Grid Sorting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            DataSet categoryList = categoryAdmin.GetCategoriesBySearchData(txtCategoryName.Text.Trim(), ddlCatalog.SelectedValue);
            uxGrid.DataSource = this.SortDataTable(categoryList, e.SortExpression, true);
            uxGrid.DataBind();

            if (this.GetSortDirection() == "DESC")
            {
                e.SortDirection = SortDirection.Descending;
            }
            else
            {
                e.SortDirection = SortDirection.Ascending;
            }
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            string Id = e.CommandArgument.ToString();

            if (e.CommandName == "Edit")
            {
                this.EditLink = this.EditLink + "?itemid=" + Id;
                Response.Redirect(this.EditLink);
            }
        }

        #endregion

        #region General Events

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
            IsSearchEnabled = true;
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtCategoryName.Text = string.Empty;
            ddlCatalog.SelectedIndex = 0;
            this.BindGridData();
        }

        #endregion

        #region Helper methods

        /// <summary>
        /// Sorting Data
        /// </summary>
        /// <param name="dataSet">The value of dataSet</param>
        /// <param name="gridViewSortExpression">The value of GridViewSortExpression</param>
        /// <param name="isPageIndexChanging">The value of Page Index Changing</param>
        /// <returns>Returns teh sorted DataView</returns>
        protected DataView SortDataTable(DataSet dataSet, string gridViewSortExpression, bool isPageIndexChanging)
        {
            if (dataSet != null)
            {
                DataView dataView = new DataView(dataSet.Tables[0]);

                if (gridViewSortExpression.Length > 0)
                {
                    if (isPageIndexChanging)
                    {
                        dataView.Sort = string.Format("{0} {1}", gridViewSortExpression, this.GridViewSortDirection);
                    }
                    else
                    {
                        dataView.Sort = string.Format("{0} {1}", gridViewSortExpression, this.GetSortDirection());
                    }
                }

                return dataView;
            }
            else
            {
                return new DataView();
            }
        }

        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            DataSet ds = categoryAdmin.GetCategoriesBySearchData(txtCategoryName.Text.Trim(), ddlCatalog.SelectedValue);
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = "DisplayOrder";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Get Sort Direction
        /// </summary>
        /// <returns>Returns the sort direction</returns>
        private string GetSortDirection()
        {
            switch (this.GridViewSortDirection)
            {
                case "ASC":
                    this.GridViewSortDirection = "DESC";
                    break;

                case "DESC":
                    this.GridViewSortDirection = "ASC";
                    break;
            }

            return this.GridViewSortDirection;
        }

        #endregion
    }
}