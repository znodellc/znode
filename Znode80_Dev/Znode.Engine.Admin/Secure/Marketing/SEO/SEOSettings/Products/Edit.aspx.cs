using System;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Marketing.SEO.SEOSettings.Products
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Marketing.SEO.SEOSettings.Products.Edit class
    /// </summary>
    public partial class Edit : System.Web.UI.Page
    {
        #region Private Variables
        private string ManagePageLink = "~/Secure/Marketing/SEO/SEOSettings/Default.aspx?mode=product";
        private string ProductImageName = string.Empty;
        private int ItemId;        
        #endregion

        #region Bind Edit Data
        /// <summary>
        /// Bind value for Particular Product
        /// </summary>
        public void BindEditData()
        {
            ProductAdmin _ProductAdmin = new ProductAdmin();
            Product _Products = new Product();

            // If edit mode then get all the values first
            if (this.ItemId > 0)
            {
                // Checking the user profile for roles and permission and then loading the data based on roles
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (!UserStoreAccess.CheckUserRoleInProduct(profiles, this.ItemId))
                {
                    Response.Redirect("~/Secure/Marketing/SEO/SEOSettings/Default.aspx", true);
                }
                
                _Products = _ProductAdmin.GetByProductId(this.ItemId);
            }

            lblTitle.Text += _Products.Name;
            txtProductName.Text = Server.HtmlDecode(_Products.Name);
            txtSEOTitle.Text = Server.HtmlDecode(_Products.SEOTitle);
            txtSEOMetaKeywords.Text = Server.HtmlDecode(_Products.SEOKeywords);
            txtSEOMetaDescription.Text = Server.HtmlDecode(_Products.SEODescription);
            txtSEOUrl.Text = _Products.SEOURL;
            ctrlHtmlDescription.Html = Server.HtmlDecode(_Products.Description);
            txtshortdescription.Text = Server.HtmlDecode(_Products.ShortDescription);
            ctrlHtmlPrdFeatures.Html = _Products.FeaturesDesc;
            ctrlHtmlProdInfo.Html = _Products.AdditionalInformation;
            ctrlHtmlPrdSpec.Html = _Products.Specifications;
        }
        #endregion

        #region Page_Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get product id value from query string
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditProductSEOSetting").ToString();
                this.BindEditData();
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            ProductAdmin productAdmin = new ProductAdmin();
            Product product = new Product();
            string mappedSEOUrl = string.Empty;

            // If edit mode then get all the values first
            if (this.ItemId > 0)
            {
                product = productAdmin.GetByProductId(this.ItemId);

                if (product.SEOURL != null)
                {
                    mappedSEOUrl = product.SEOURL;
                }
            }

            // Passing Values 
            product.ProductID = this.ItemId;
            product.Name = Server.HtmlEncode(txtProductName.Text);
            product.Description = ctrlHtmlDescription.Html.Trim();
            product.ShortDescription = Server.HtmlEncode(txtshortdescription.Text);
            product.AdditionalInformation = ctrlHtmlProdInfo.Html.Trim();
            product.Specifications = ctrlHtmlPrdSpec.Html.Trim();
            product.FeaturesDesc = ctrlHtmlPrdFeatures.Html.Trim();

            product.SEOTitle = Server.HtmlEncode(txtSEOTitle.Text.Trim());
            product.SEOKeywords = Server.HtmlEncode(txtSEOMetaKeywords.Text.Trim());
            product.SEODescription = Server.HtmlEncode(txtSEOMetaDescription.Text.Trim());
            product.SEOURL = null;

            if (txtSEOUrl.Text.Trim().Length > 0)
            {
                product.SEOURL = txtSEOUrl.Text.Trim().Replace(" ", "-");
            }

            if (string.Compare(product.SEOURL, mappedSEOUrl, true) != 0)
            {
                if (urlRedirectAdmin.SeoUrlExists(product.SEOURL, product.ProductID))
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSEOPageName").ToString();
                    return;
                }
            }

            bool status = false;

            // PRODUCT UPDATE
            if (this.ItemId > 0) 
            {
                status = productAdmin.Update(product);
                
                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogProductSEOSetting") + product.Name, product.Name);
            }

            if (status)
            {
                if (chkAddURLRedirect.Checked)
                {
                    urlRedirectAdmin.AddUrlRedirectTable(SEOUrlType.Product, mappedSEOUrl, product.SEOURL, product.ProductID.ToString());
                }

                urlRedirectAdmin.UpdateUrlRedirect(mappedSEOUrl);

                Response.Redirect(this.ManagePageLink);
            }
            else
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductSEOSetting").ToString(); 
            }
        }

        /// <summary> 
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ManagePageLink);
        }
        #endregion
    }
}