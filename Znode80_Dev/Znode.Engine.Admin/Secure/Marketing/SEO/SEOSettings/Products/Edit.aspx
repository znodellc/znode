<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/Themes/Standard/edit.master" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Marketing.SEO.SEOSettings.Products.Edit" CodeBehind="Edit.aspx.cs" %>

<%@ Register TagPrefix="uc2" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="LeftFloat" style="width: 70%">
        <h1>
            <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
    </div>
    <div class="LeftFloat" align="right" style="width: 30%">
        <zn:Button runat="server" ID="btnSubmitTop" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
        <zn:Button runat="server" ID="btnCancelTop" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
    </div>
    <div class="ClearBoth" align="left">
    </div>
    <div class="FormView">
        <asp:Label ID="lblError" CssClass="Error" runat="server"></asp:Label>
        <div>
            <div class="SEOProductPageAdd">
                <h4 class="SubTitle">
                    <asp:Localize runat="server" ID="SubTitleGeneralInformation" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralInformation %>'></asp:Localize>
                </h4>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ColumnTitleProductName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>'></asp:Localize><span class="Asterix">*</span>
                </div>
                <div>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtProductName"
                        ErrorMessage="Enter Product Name" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtProductName" runat="server" Width="152px"></asp:TextBox>
                </div>
                <h4 class="SubTitle">
                    <asp:Localize runat="server" ID="SeoSettings" Text='<%$ Resources:ZnodeAdminResource, SubTitleSeoSettings %>'></asp:Localize>
                </h4>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="SEOTitle" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOTitle %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="SEOKeywords" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOKeywords %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOMetaKeywords" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="SEODescription" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEODescription %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOMetaDescription" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="SEOPageName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOPageName %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOUrl" runat="server" MaxLength="100" Columns="50"></asp:TextBox>
                    <div class="tooltip SEOSettings">
                        <a href="javascript:void(0);" class="learn-more"><span></span></a>
                        <div class="content">
                            <h6>
                                <asp:Localize runat="server" ID="Help" Text='<%$ Resources:ZnodeAdminResource, HintHelp %>'></asp:Localize></h6>
                            <p>
                                <asp:Localize runat="server" ID="SEOHint" Text='<%$ Resources:ZnodeAdminResource, SEOHint %>'></asp:Localize>
                            </p>
                        </div>
                    </div>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSEOUrl"
                        CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularSEOPageName %>'
                        SetFocusOnError="True" ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidSEOUrl %>'></asp:RegularExpressionValidator>
                </div>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="Redirect" Text='<%$ Resources:ZnodeAdminResource, ColumnTitle301Redirect %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID="chkAddURLRedirect" runat="server" Text='Enable 301 redirects for URL changes' />
                </div>
                <div class="ClearBoth" align="left">
                    <br />
                </div>
                <h4 class="SubTitle">
                    <asp:Localize runat="server" ID="Description" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDescription %>'></asp:Localize>
                </h4>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ShortDescription" Text='<%$ Resources:ZnodeAdminResource, ColumnShortDescription %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtshortdescription" runat="server" Width="500px" TextMode="MultiLine"
                        Height="75px" MaxLength="100"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="LongDescription" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleLongDescription %>'></asp:Localize>
                </div>
                <div class="ValueStyle" style="width: 70%">
                    <ZNode:HtmlTextBox ID="ctrlHtmlDescription" runat="server"></ZNode:HtmlTextBox>
                </div>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ProductFeatures" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductFeatures %>'></asp:Localize>
                </div>
                <div class="ValueStyle" style="width: 70%">
                    <ZNode:HtmlTextBox ID="ctrlHtmlPrdFeatures" runat="server"></ZNode:HtmlTextBox>
                </div>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ProductSpecifications" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductSpecifications %>'></asp:Localize>
                </div>
                <div class="ValueStyle" style="width: 70%">
                    <ZNode:HtmlTextBox ID="ctrlHtmlPrdSpec" runat="server"></ZNode:HtmlTextBox>
                </div>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="AdditionalInfo" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleAdditionalInfo %>'></asp:Localize>
                </div>
                <div class="ValueStyle" style="width: 70%">
                    <ZNode:HtmlTextBox ID="ctrlHtmlProdInfo" runat="server"></ZNode:HtmlTextBox>
                </div>
            </div>
            <div>
                <uc2:Spacer ID="Spacer8" SpacerHeight="20" SpacerWidth="3" runat="server"></uc2:Spacer>
            </div>
        </div>
        <div class="ClearBoth" align="left">
            <br />
        </div>
        <div>
            <zn:Button runat="server" ID="btnSubmit" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancel" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
        </div>
    </div>
</asp:Content>
