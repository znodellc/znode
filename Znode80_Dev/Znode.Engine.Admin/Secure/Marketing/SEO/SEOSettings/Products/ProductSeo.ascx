﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductSeo.ascx.cs" Inherits="Znode.Engine.Common.Znode.Engine.Admin.Secure.Marketing.SEO.SEOSettings.Products.ProductSeo" %>
<%@ Register TagPrefix="ZNode" TagName="ManufacturerAutoComplete" Src="~/Controls/Default/ManufacturerAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ProductTypeAutoComplete" Src="~/Controls/Default/ProductTypeAutoComplete.ascx" %>

<asp:UpdatePanel ID="updatepanelProductList" runat="server">
    <ContentTemplate>
        <div class="SEO">
            <div class="SearchForm">
                <p>
                    <asp:Localize runat="server" ID="ProductSEOSettings" Text='<%$ Resources:ZnodeAdminResource, TextProductSEOSettings %>'></asp:Localize>
                </p>
                <h4 class="SubTitle">
                    <asp:Localize runat="server" ID="SearchProducts" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchProducts %>'></asp:Localize>
                </h4>
                <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">
                                <asp:Localize runat="server" ID="ProductName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>'></asp:Localize>
                            </span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtproductname" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">
                                <asp:Localize runat="server" ID="ProductsNumber" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductsNumber %>'></asp:Localize>
                            </span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtproductnumber" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">
                                <asp:Localize runat="server" ID="SKU" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSKU %>'></asp:Localize>
                            </span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtsku" runat="server"></asp:TextBox></span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">
                                <asp:Localize runat="server" ID="Catalog" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCatalog %>'></asp:Localize>
                            </span><br />
                            <span class="ValueStyle">
                                <asp:DropDownList ID="ddlCatalog" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCatalog_SelectedIndexChanged"></asp:DropDownList></span>
                        </div>
                    </div>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">
                                <asp:Localize runat="server" ID="Brand" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBrand %>'></asp:Localize>
                            </span><br />
                            <span class="ValueStyle">
                                <znode:manufacturerautocomplete id="dmanufacturer" runat="server" />
                            </span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">
                                <asp:Localize runat="server" ID="ProductType" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductType %>'></asp:Localize>
                            </span><br />
                            <span class="ValueStyle">
                                <znode:producttypeautocomplete id="dproducttype" runat="server" />
                            </span>
                        </div>
                        <div class="ItemStyle">
                            <span class="FieldStyle">
                                <asp:Localize runat="server" ID="ProductCategory" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductCategory %>'></asp:Localize>
                            </span><br />
                            <span class="ValueStyle">
                                <znode:categoryautocomplete id="dproductcategory" runat="server" />
                            </span>
                        </div>
                    </div>
                    <div class="ClearBoth">
                        <br />
                    </div>
                    <div>
                        <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                        <zn:Button runat="server" ID="btnSearch" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                    </div>
                </asp:Panel>
                <br />
            </div>
            <h4 class="GridTitle">
                <asp:Localize runat="server" ID="ProductList" Text='<%$ Resources:ZnodeAdminResource, GridTitleProductList %>'></asp:Localize>
            </h4>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
                GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
                OnRowCommand="UxGrid_RowCommand" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyText %>'
                Width="100%" CellPadding="4">
                <Columns>
                    <asp:BoundField DataField="productid" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleImage %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <a href='<%# "Products/Edit.aspx?itemid=" + DataBinder.Eval(Container.DataItem,"productid")%>'
                                id="LinkView">
                                <img id="Img1" alt=" " src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>' runat="server" style="border: none" />
                            </a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:HyperLinkField DataNavigateUrlFields="productid" DataNavigateUrlFormatString="Edit.aspx?itemid={0}"
                        DataTextField="name" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="50%" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsActive %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="Img2" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                        <ItemTemplate>
                            <asp:LinkButton ID="Button1" CommandName="Manage" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"productid")%>'
                                Text='<%$ Resources:ZnodeAdminResource, LinkManageSEO %>' runat="server" CssClass="actionlink" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
