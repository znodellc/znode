﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="DefaultSeo.ascx.cs" Inherits="Znode.Engine.Admin.Secure.Marketing.SEO.SEOSettings.DefaultSeo.DefaultSeo" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:UpdatePanel ID="updatepanelProductList" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div class="FormView">
            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="SubtitleStoreSettings" Text='<%$ Resources:ZnodeAdminResource, SubtitleStoreSettings %>'></asp:Localize>
            </h4>
            <div style="margin: 10px 0px 15px 0px;">
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ColumnTitleSelectStore" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectStore %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="StoreList" AutoPostBack="true" runat="server" OnSelectedIndexChanged="Storelist_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="SubTitleProductSettings" Text='<%$ Resources:ZnodeAdminResource, SubTitleProductSettings %>'></asp:Localize>
            </h4>
            <div class="tooltip DefaultSEOSettings">
                <a href="javascript:void(0);" class="learn-more"><span></span></a>
                <div class="content">
                    <h6>
                        <asp:Localize runat="server" ID="Help" Text='<%$ Resources:ZnodeAdminResource, HintHelp %>'></asp:Localize></h6>
                    <p>
                        <asp:Localize runat="server" ID="ProductSettingsHint" Text='<%$ Resources:ZnodeAdminResource, SettingsHint %>'></asp:Localize>
                    </p>
                    <p>
                        <asp:Localize runat="server" ID="ProductSettingsHintName" Text='<%$ Resources:ZnodeAdminResource, ProductSettingsHintName %>'></asp:Localize><br />
                        <asp:Localize runat="server" ID="ProductSettingsHintProductNum" Text='<%$ Resources:ZnodeAdminResource, ProductSettingsHintProductNum %>'></asp:Localize><br />
                        <asp:Localize runat="server" ID="ProductSettingsHintSKU" Text='<%$ Resources:ZnodeAdminResource, ProductSettingsHintSKU %>'></asp:Localize><br />
                        <asp:Localize runat="server" ID="ProductSettingsHintBrand" Text='<%$ Resources:ZnodeAdminResource, ProductSettingsHintBrand %>'></asp:Localize>
                    </p>
                </div>
            </div>
            <br />
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEOProductTitle" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOProductTitle %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOProductTitle" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEOProductDescription" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOProductDescription %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOProductDescription" runat="server" Width="450px" TextMode="MultiLine"
                    Height="150px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEOProductKeyword" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOProductKeyword %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOProductKeyword" runat="server" Width="152px"></asp:TextBox>
            </div>
            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="SubTitleCategorySettings" Text='<%$ Resources:ZnodeAdminResource, SubTitleCategorySettings %>'></asp:Localize>
            </h4>
            <div class="tooltip DefaultSEOSettings">
                <a href="javascript:void(0);" class="learn-more"><span></span></a>
                <div class="content">
                    <h6>
                        <asp:Localize runat="server" ID="HelpHint" Text='<%$ Resources:ZnodeAdminResource, HintHelp %>'></asp:Localize></h6>
                    <p>
                        <asp:Localize runat="server" ID="SettingsHint" Text='<%$ Resources:ZnodeAdminResource, SettingsHint %>'></asp:Localize>
                    </p>
                    <p>
                        <asp:Localize runat="server" ID="CategorySettingsHintName" Text='<%$ Resources:ZnodeAdminResource, CategorySettingsHintName %>'></asp:Localize>
                    </p>
                </div>
            </div>
            <br />
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEOCategoryTitle" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOCategoryTitle %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOCategoryTitle" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEOCategoryDescription" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOCategoryDescription %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOCategoryDescription" runat="server" Width="450px" TextMode="MultiLine"
                    Height="150px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEOCategoryKeyword" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOCategoryKeyword %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOCategoryKeyword" runat="server" Width="152px"></asp:TextBox>
            </div>
            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="SubTitleContentPageSetting" Text='<%$ Resources:ZnodeAdminResource, SubTitleContentPageSetting %>'></asp:Localize>
            </h4>
            <div class="tooltip DefaultSEOContentSettings">
                <a href="javascript:void(0);" class="learn-more"><span></span></a>
                <div class="content">
                    <h6>
                        <asp:Localize runat="server" ID="ContentHintHelp" Text='<%$ Resources:ZnodeAdminResource, HintHelp %>'></asp:Localize></h6>
                    <p>
                        <asp:Localize runat="server" ID="ContentSettingsHint" Text='<%$ Resources:ZnodeAdminResource, SettingsHint %>'></asp:Localize>
                    </p>
                    <p>
                        <asp:Localize runat="server" ID="ContentSettingsHintName" Text='<%$ Resources:ZnodeAdminResource, ContentSettingsHintName %>'></asp:Localize>
                    </p>
                </div>
            </div>
            <br />
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEOContentTitle" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOContentTitle %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOContentTitle" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEOContentDescription" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOContentDescription %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOContentDescription" runat="server" Width="450px" TextMode="MultiLine"
                    Height="150px"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSEOContentKeyword" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOContentKeyword %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOContentKeyword" runat="server" Width="152px"></asp:TextBox>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth" align="left">
                <br />
            </div>
            <div>
                <zn:Button runat="server" ID="btnSubmit" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="True" />
                <zn:Button runat="server" ID="btnCancel" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
