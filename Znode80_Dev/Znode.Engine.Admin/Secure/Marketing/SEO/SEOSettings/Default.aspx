﻿<%@ Page Language="C#" AutoEventWireup="True" ValidateRequest="false" MasterPageFile="~/Themes/Standard/content.master" CodeBehind="Default.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Marketing.SEO.SEOSettings.Default" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="DefaultSeo" Src="~/Secure/Marketing/SEO/SEOSettings/DefaultSeo/DefaultSeo.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ProductSeo" Src="~/Secure/Marketing/SEO/SEOSettings/Products/ProductSeo.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="CategorySeo" Src="~/Secure/Marketing/SEO/SEOSettings/Categories/CategorySeo.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ContentSeo" Src="~/Secure/Marketing/SEO/SEOSettings/Content/ContentSeo.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Redirect301" Src="~/Secure/Marketing/SEO/SEOSettings/URLRedirect/UrlRedirect.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <script type="text/javascript">
        function ActiveTabChanged(sender, e) {
            var tab = document.getElementById('<%=tabSeoSettings.ClientID%>');
            var activeTabIndex = document.getElementById('<%=hdnActiveTabIndex.ClientID%>');
            activeTabIndex.value = sender.get_activeTabIndex();
        }
    </script>
    <asp:HiddenField ID="hdnActiveTabIndex" runat="server" Value="0" />
    <div>
        <h1>
            <asp:Localize runat="server" ID="TitleManageSEO" Text='<%$ Resources:ZnodeAdminResource, TitleManageSEO %>'></asp:Localize>
        </h1>
    </div>
    <div class="ClearBoth">
        <p>
            <asp:Localize runat="server" ID="TextManageSEO" Text='<%$ Resources:ZnodeAdminResource, TextManageSEO %>'></asp:Localize>
        </p>
    </div>
    <div>
        <ZNode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>

    <div style="clear: both;">
        <ajaxToolKit:TabContainer ID="tabSeoSettings" runat="server" ActiveTabIndex="0" OnClientActiveTabChanged="ActiveTabChanged">
            <ajaxToolKit:TabPanel ID="pnlDefaultSeo" runat="server">
                <HeaderTemplate>
                    <asp:Localize runat="server" ID="TabTitleDefaultSettings" Text='<%$ Resources:ZnodeAdminResource, TabTitleDefaultSettings %>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <ZNode:DefaultSeo ID="DefaultSeo" runat="server"></ZNode:DefaultSeo>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlProductSeo" runat="server">
                <HeaderTemplate>
                    <asp:Localize runat="server" ID="TabTitleProducts" Text='<%$ Resources:ZnodeAdminResource, TabTitleProducts %>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <ZNode:ProductSeo ID="ProductSeo" runat="server"></ZNode:ProductSeo>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlCategorySeo" runat="server">
                <HeaderTemplate>
                    <asp:Localize runat="server" ID="TabTitleCategories" Text='<%$ Resources:ZnodeAdminResource, TabTitleCategories %>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <ZNode:CategorySeo ID="CategorySeo" runat="server"></ZNode:CategorySeo>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlContentSeo" runat="server">
                <HeaderTemplate>
                    <asp:Localize runat="server" ID="TabTitleContentPage" Text='<%$ Resources:ZnodeAdminResource, TabTitleContentPage %>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <ZNode:ContentSeo ID="ContentSeo" runat="server" />
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnl301Redirect" runat="server">
                <HeaderTemplate>
                    <asp:Localize runat="server" ID="TabTitleURLRedirect" Text='<%$ Resources:ZnodeAdminResource, TabTitleURLRedirect %>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <ZNode:Redirect301 ID="Redirect301" runat="server" />
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
        </ajaxToolKit:TabContainer>
    </div>
</asp:Content>
