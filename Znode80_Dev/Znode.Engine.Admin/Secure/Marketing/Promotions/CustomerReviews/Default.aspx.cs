using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Marketing.Promotions.CustomerReviews
{
    /// <summary>
    /// Represents the SiteAdmin - Znode.Engine.Admin.Secure.Marketing.Promotion.CustomerReviews.Default class
    /// </summary>
    public partial class  Default : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string ChangeStatusPageLink = "~/Secure/Marketing/Promotions/CustomerReviews/ReviewStatus.aspx?ItemId=";
        private string EditReviewPageLink = "~/Secure/Marketing/Promotions/CustomerReviews/EditReview.aspx?ItemId=";
        private string DeleteReviewPageLink = "~/Secure/Marketing/Promotions/CustomerReviews/Delete.aspx?ItemId=";
        private string ListReviewPageLink = "~/Secure/Marketing/Promotions/CustomerReviews/Default.aspx";


        #endregion

        #region Events
        /// <summary>
        /// Occurs when the server control is loaded into the Page object. 
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // This is to indicate whether the page is being loaded in response to a client postback, 
            // or if it is being loaded and accessed for the first time.
            if (!Page.IsPostBack)
            {
                this.SearchReviews();
            }
        }

        /// <summary>
        /// The Click event is raised when the "Search" Button control is clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.SearchReviews();
        }

        /// <summary>
        /// The Click event is raised when the "Clear" Button control is clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListReviewPageLink);
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Occurs when a button is clicked in a GridView control.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            // Multiple buttons are used in a GridView control, use the
            // CommandName property to determine which button was clicked
            if (e.CommandName == "Edit")
            {
                int reviewID = int.Parse(e.CommandArgument.ToString());

                // Redirect to change review status page
                Response.Redirect(this.EditReviewPageLink + reviewID);
            }
            else if (e.CommandName == "Delete")
            {
                int reviewID = int.Parse(e.CommandArgument.ToString());

                // Redirect to change review delete confirmation page
                Response.Redirect(this.DeleteReviewPageLink + reviewID);
            }
            else if (e.CommandName == "Status")
            {
                int reviewID = int.Parse(e.CommandArgument.ToString());

                // Redirect to change review status page
                Response.Redirect(this.ChangeStatusPageLink + reviewID);
            }
        }

        /// <summary>
        /// Occurs when one of the pager buttons is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.SearchReviews();
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind filtered collection list to grid control
        /// </summary>
        private void SearchReviews()
        {
            ReviewAdmin reviewAdmin = new ReviewAdmin();
            string status = ListReviewStatus.SelectedIndex == 0 ? string.Empty : ListReviewStatus.SelectedValue;
            DataSet ds = reviewAdmin.SearchReview(Server.HtmlEncode(ReviewTitle.Text.Trim()), Server.HtmlEncode(Name.Text.Trim()), Server.HtmlEncode(ddlProductNames.Text.Trim()), status);

            uxGrid.DataSource = ds.Tables[0].DefaultView;
            uxGrid.DataBind();
        }
        #endregion
    }
}