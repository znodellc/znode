<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" ValidateRequest="false" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.Promotions.CustomerReviews.ReviewStatus" CodeBehind="ReviewStatus.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <h1>
            <asp:Localize runat="server" ID="ReviewTitle" Text='<%$ Resources:ZnodeAdminResource, TitleReviewTitle %>'></asp:Localize>
            <asp:Label ID="lblReviewHeader" runat="server" /></h1>

        <div class="HintStyle">
            <asp:Localize runat="server" ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, TextReviewStatus %>'></asp:Localize></div>
        <br />
        <div class="FieldStyle">
            <asp:DropDownList ID="ListReviewStatus" runat="server">
                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropdownTextActive %>' Value="A"></asp:ListItem>
                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropdownTextInactive %>' Value="I"></asp:ListItem>
                <asp:ListItem Selected="True" Text='<%$ Resources:ZnodeAdminResource, DropDownTextNew %>' Value="N"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer1" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="ValueField">
            <zn:Button ID="btnSubmit" runat="server" ButtonType="SubmitButton" OnClick="UpdateReviewStatus_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" OnClick="CancelStatus_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
        </div>
        <div>
            <ZNode:Spacer ID="LongSpace" SpacerHeight="200" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
    </div>
</asp:Content>

