using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Marketing.Promotions.CustomerReviews
{
    /// <summary>
    /// Represents the SiteAdmin - Znode.Engine.Admin.Secure.Marketing.Promotion.CustomerReviews.Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Variables
        private string _ProductReviewTitle = string.Empty;
        private int ItemId;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Product Review Title
        /// </summary>
        public string ProductReviewTitle
        {
            get
            {
                return this._ProductReviewTitle;
            }

            set
            {
                this._ProductReviewTitle = value;
            }
        }
        
        #endregion

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            this.Bind();
        }

        #region Events
       
        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Marketing/Promotions/CustomerReviews/Default.aspx");
        }

        /// <summary>
        /// Delete Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            ReviewAdmin reviewAdmin = new ReviewAdmin();

            Review _Review = new Review();
            _Review = reviewAdmin.GetByReviewID(this.ItemId);

            string AssociateName = this.GetGlobalResourceObject("ZnodeAdminResource", "TextDeleteCustomerReview").ToString() + _Review.Subject;
            string SubjectName = _Review.Subject;
            bool retval = reviewAdmin.Delete(this.ItemId);

            if (retval)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, SubjectName);
                Response.Redirect("~/Secure/Marketing/Promotions/CustomerReviews/Default.aspx");
            }
            else
            {
                // Please ensure that this Add-On does not contain Add-On Values or products. If it does, then delete the Add-On values and products first.";
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteReview").ToString();
            }
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Bind Method
        /// </summary>
        private void Bind()
        {
            ReviewAdmin reviewAdmin = new ReviewAdmin();
            Review Entity = reviewAdmin.GetByReviewID(this.ItemId);

            if (Entity != null)
            {
                this.ProductReviewTitle = Entity.Subject;
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoReviewRequest").ToString());
            }
        }
        #endregion
    }
}