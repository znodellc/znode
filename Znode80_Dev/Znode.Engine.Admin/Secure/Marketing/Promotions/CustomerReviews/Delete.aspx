<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master"  AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.Promotions.CustomerReviews.Delete" Codebehind="Delete.aspx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">    
   <h5><asp:Localize runat="server" ID="PleaseConfirm" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm %>'></asp:Localize></h5>
    <p><asp:Localize runat="server" ID="PleaseConfirmReview" Text='<%$ Resources:ZnodeAdminResource, TextDeleteConfirmReview %>'></asp:Localize>"<b><%=ProductReviewTitle%></b>"<asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextChangeUndone %>'></asp:Localize>
    </p>
    <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label><br /><br />
              <zn:Button ID="btnDelete" runat="server" ButtonType="CancelButton" OnClick="BtnDelete_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>' />
              <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
    <div><ZNode:spacer id="Spacer1" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:spacer></div>
</asp:Content>

