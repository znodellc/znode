using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Marketing.Promotions.CustomerReviews
{
    /// <summary>
    /// Represents the SiteAdmin - Admin.Secure.catalog.product.CustomerReviews.ReviewStatus class
    /// </summary>
    public partial class ReviewStatus : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId;
        private string ListPageLink = "~/Secure/Marketing/Promotions/CustomerReviews/Default.aspx";
        #endregion

        #region Page Load Event
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.Bind();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Update Review Status Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UpdateReviewStatus_Click(object sender, EventArgs e)
        {
            ReviewAdmin reviewAdmin = new ReviewAdmin();
            Review review = reviewAdmin.GetByReviewID(this.ItemId);

            if (review != null)
            {
                if (ListReviewStatus.SelectedValue == "I")
                {
                    review.Status = "I";
                }
                else if (ListReviewStatus.SelectedValue == "A")
                {
                    review.Status = "A";
                }
                else
                {
                    review.Status = "N";
                }
            }

            reviewAdmin.Update(review);

            string AssociateName = this.GetGlobalResourceObject("ZnodeAdminResource", "TextEditCustomerReviewStatus").ToString() + review.Subject;

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, review.Subject);

            Response.Redirect(this.ListPageLink);
        }

        /// <summary>
        /// Cancel Status Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CancelStatus_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListPageLink);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Bind Method
        /// </summary>
        private void Bind()
        {
            ReviewAdmin reviewAdmin = new ReviewAdmin();
            Review review = reviewAdmin.GetByReviewID(this.ItemId);

            if (review != null)
            {
                lblReviewHeader.Text = review.Subject;
                ListReviewStatus.SelectedValue = review.Status;
            }
        }
        #endregion
    }
}