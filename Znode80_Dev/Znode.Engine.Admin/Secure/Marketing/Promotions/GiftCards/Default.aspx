﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/content.master"
    AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.Promotions.GiftCards.Default" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1><asp:Localize ID="GiftCards" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextGiftCards %>'></asp:Localize></h1>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAddCoupon" runat="server" ButtonType="Button" OnClick="BtnAddCoupon_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddGiftCard %>' ButtonPriority="Primary" />
        </div>
        <div class="ClearBoth" align="left">
            <p>
                <asp:Localize ID="GiftCardsText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextGiftCards %>'></asp:Localize>
            </p>
            <br />
        </div>
        <h4 class="SubTitle"><asp:Localize ID="SearchGiftCards" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchGiftCards %>'></asp:Localize></h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="Name" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnName %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtName" runat="server" ValidationGroup="grpSearch"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="Balance" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBalance %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtBalance" runat="server" ValidationGroup="grpSearch"></asp:TextBox>
                            <div>
                                <asp:RangeValidator ID="rvBalance" runat="server" ControlToValidate="txtBalance"
                                    CssClass="Error" Display="Dynamic" MaximumValue="99999" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredBalance %>'
                                    MinimumValue="0.01" CultureInvariantValues="true" Type="Currency"></asp:RangeValidator>
                            </div>
                        </span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="CardNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCardNumber %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtCardNumber" runat="server" ValidationGroup="grpSearch"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="AccountID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAccountID %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAccountId" runat="server" ValidationGroup="grpSearch"></asp:TextBox>
                            <div>
                                <asp:RangeValidator ID="rvAccountID" runat="server" ControlToValidate="txtAccountId"
                                    Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeAccountNumber %>' CssClass="Error"
                                    MaximumValue="999999999" MinimumValue="1" Type="Integer"></asp:RangeValidator>
                            </div>
                        </span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:CheckBox ID="chkDontShowExpired" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnExcludeExpiredCards %>'
                                Checked="true" ValidationGroup="grpSearch"></asp:CheckBox>
                        </span>
                        <br />
                        <span class="ValueStyle"></span>
                    </div>
                    <div class="ItemStyle">
                    </div>
                </div>
                <div class="ClearBoth">
                    <zn:Button runat="server" ID="btnClearSearch" CausesValidation="False" OnClick="BtnClearSearch_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' />
                    <zn:Button runat="server" ID="btnSearch" OnClick="BtnSearch_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                </div>
            </div>
        </asp:Panel>
        <br />
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <h4 class="GridTitle"><asp:Localize ID="GiftCardsList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleGiftCard %>'></asp:Localize>
        </h4>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
            AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundGiftCards %>'>
            <Columns>
                <asp:BoundField DataField="GiftCardId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='add.aspx?ItemID=<%# DataBinder.Eval(Container.DataItem, "GiftCardId").ToString()%>'>
                            <%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="CardNumber" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnCardNumber %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="CreateDate" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCreateDate %>' DataFormatString="{0:MM/dd/yyyy}"
                    HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="ExpirationDate" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleExpirationDate %>' DataFormatString="{0:MM/dd/yyyy}"
                    HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleAmount %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div align="left">
                            <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix()%>&nbsp;<%#String.Format("{0, 0:N2}",(DataBinder.Eval(Container.DataItem, "Amount")))%>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:ButtonField CommandName="Edit" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' ButtonType="Link">
                    <ControlStyle CssClass="actionlink" />
                </asp:ButtonField>
                <asp:ButtonField CommandName="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' ButtonType="Link">
                    <ControlStyle CssClass="actionlink" />
                </asp:ButtonField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>
