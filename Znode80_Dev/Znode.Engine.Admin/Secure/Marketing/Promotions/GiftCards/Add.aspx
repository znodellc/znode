﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/content.master"
    AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.Promotions.GiftCards.Add" ValidateRequest="false"
    CodeBehind="Add.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <asp:HiddenField ID="hdnReturnItem" runat="server" Value="" />
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ID="btnSubmitTop" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true"/>
            <zn:Button runat="server" ID="btnCancelTop" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <h4 class="SubTitle"><asp:Localize ID="GeneralInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralInformation %>'></asp:Localize></h4>
        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="FieldStyle">
                    <asp:Localize ID="CardNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCardNumber %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtCardNumber" Enabled="false" runat="server"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="GiftCardName" runat="server" Text='<%$ Resources:ZnodeAdminResource,  ColumnGiftCardName %>'></asp:Localize><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtGiftCardName" runat="server" Columns="25" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                        CssClass="Error" ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtGiftCardName"
                        Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredGiftCardName %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="StoreName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStoreName %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlStoreName" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="GiftCardAmount" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnGiftCardAmount %>'></asp:Localize><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator CssClass="Error" ID="rfvBalance" runat="server" ControlToValidate="txtAmount"
                        Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredGiftCardAmount %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <asp:RangeValidator ID="rvBalance" runat="server" ControlToValidate="txtAmount" CssClass="Error"
                        Display="Dynamic" MaximumValue="99999" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeGiftCardAmount %>'
                        MinimumValue="0.01" CultureInvariantValues="true" Type="Currency"></asp:RangeValidator>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="ExpirationDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnExpirationDateFormat %>'></asp:Localize><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtExpiryDate" runat="server" />
                    <asp:ImageButton ID="imgbtnStartDt" runat="server" CausesValidation="false" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" />
                    <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                        runat="server" TargetControlID="txtExpiryDate">
                    </ajaxToolKit:CalendarExtender>
                    <asp:RequiredFieldValidator
                        CssClass="Error" ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtExpiryDate"
                        Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredExpirationDate %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtExpiryDate"
                        CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidExpirationDate %>'
                        ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"></asp:RegularExpressionValidator>
                    <asp:RangeValidator ID="rvExpirationDate"
                        runat="server"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeExpirationDate %>'
                        ControlToValidate="txtExpiryDate"
                        Display="Dynamic"
                        CssClass="Error"
                        Type="Date"></asp:RangeValidator>
                </div>
                <div class="FieldStyle">
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID="chkUseSingleAccount" AutoPostBack="true" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableGiftCard %>'
                        runat="server" OnCheckedChanged="ChkUseSingleAccount_CheckedChanged" />
                </div>
                <div runat="server" id="divAccountID" visible="false">
                    <div class="FieldStyle">
                        <asp:Localize ID="AccountIDColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAccountID %>'></asp:Localize><span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtAccountID" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator CssClass="Error" ID="rfvAccountID" Enabled="false" runat="server"
                            ControlToValidate="txtAccountID" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredAccountID %>'
                            SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="rvAmount" runat="server" ControlToValidate="txtAccountID"
                            Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeWholeNumber %>' MaximumValue="999999999"
                            MinimumValue="1" Type="Integer"></asp:RangeValidator>
                    </div>
                </div>

            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="ClearBoth">
        </div>
        <div class="Error">
            <asp:Label ID="lblError" runat="server" Visible="true"></asp:Label>
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <zn:Button runat="server" ID="btnSubmitBottom" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true"/>
            <zn:Button runat="server" ID="btnCancelBottom" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
        </div>
    </div>
    <!--During Update Process -->
    <asp:UpdateProgress ID="UpdateProgressMozilla" runat="server" DisplayAfter="0" DynamicLayout="true">
        <ProgressTemplate>
            <asp:Panel ID="Panel1" CssClass="overlay" runat="server">
                <asp:Panel ID="Panel2" CssClass="loader" runat="server">
                    <asp:Localize ID="LoadingMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, MessageLoading %>'></asp:Localize><img id="Img1" align="absmiddle" src="~/Themes/images/loading.gif"
                        runat="server" />
                </asp:Panel>
            </asp:Panel>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
