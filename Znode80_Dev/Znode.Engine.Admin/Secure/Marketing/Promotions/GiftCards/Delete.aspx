﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.Promotions.GiftCards.Delete" CodeBehind="Delete.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">

    <h5>
        <asp:Label ID="deletecoupon" runat="server"><asp:Localize ID="DeleteGiftCard" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleDeleteGiftCard %>'></asp:Localize><b><%=GiftCardName%></b></asp:Label></h5>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <p>
        <asp:Localize ID="DeleteGiftCardText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextDeleteGiftCard %>'></asp:Localize>
    </p>
    <p>
        <asp:Label ID="lblErrorMsg" runat="server" Visible="False" CssClass="Error"></asp:Label>
    </p>
    <div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <div>
        <zn:Button runat="server" ID="btnDelete" OnClick="BtnDelete_Click" CausesValidation="True" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>' />
        <zn:Button runat="server" ID="btnCancel" OnClick="BtnCancel_Click" CausesValidation="False" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
    </div>
    <div>
        <uc1:Spacer ID="Spacer8" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>

</asp:Content>

