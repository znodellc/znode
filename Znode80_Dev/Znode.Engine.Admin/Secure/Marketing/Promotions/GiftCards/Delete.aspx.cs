﻿using System;
using System.Web.UI;

namespace  Znode.Engine.Admin.Secure.Marketing.Promotions.GiftCards
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Marketing.Promotion.GiftCards.Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Variables
        private string _GiftCardName = string.Empty;        
        private int ItemId;
        private string RedirectLink = "~/Secure/Marketing/Promotions/GiftCards/Default.aspx";
        #endregion

        /// <summary>
        /// Gets or sets the gift card name.
        /// </summary>
        public string GiftCardName
        {
            get { return _GiftCardName; }
            set { _GiftCardName = value; }
        }

        #region General Events

        /// <summary>
        /// Delete Button click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            bool check = false;
            ZNode.Libraries.Admin.GiftCardAdmin giftCardAdmin = new ZNode.Libraries.Admin.GiftCardAdmin();
           ZNode.Libraries.DataAccess.Entities.GiftCard giftCard = giftCardAdmin.GetByGiftCardId(this.ItemId);

            check = giftCardAdmin.DeleteGiftCard(this.ItemId);

            if (check)
            {
                Response.Redirect(this.RedirectLink);
            }
            else
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDelete").ToString();
                lblErrorMsg.Visible = true;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.RedirectLink);
        }

        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemID from querystring        
            if (Request.Params["ItemID"] != null)
            {
                this.ItemId = int.Parse(Request.Params["ItemID"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }

        #endregion
        
        #region Bind Data
        /// <summary>
        /// Search and Display the Gift Cards.
        /// </summary>
        private void BindData()
        {
            ZNode.Libraries.Admin.GiftCardAdmin giftCardAdmin = new ZNode.Libraries.Admin.GiftCardAdmin();
            ZNode.Libraries.DataAccess.Entities.GiftCard giftCard = giftCardAdmin.GetByGiftCardId(this.ItemId);

            if (giftCard != null)
            {
                this.GiftCardName = giftCard.Name;
            }
        }

        #endregion
    }
}
