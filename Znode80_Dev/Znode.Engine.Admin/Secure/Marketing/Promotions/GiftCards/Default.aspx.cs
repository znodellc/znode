﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Custom;

namespace  Znode.Engine.Admin.Secure.Marketing.Promotions.GiftCards
{
    /// <summary>
    /// Represents the SiteAdmin Admin.Secure.catalog.GiftCards.List class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string CatalogImagePath = string.Empty;
        private string ListPage = "~/Secure/Marketing/Promotions/GiftCards/Default.aspx";
        private string AddLink = "~/Secure/Marketing/Promotions/GiftCards/Add.aspx";
        private string DeleteLink = "~/Secure/Marketing/Promotions/GiftCards/Delete.aspx?ItemID=";
        #endregion

        #region General Events

        /// <summary>
        /// Add Promtion Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddCoupon_Click(object sender, EventArgs e)
        {
            // Redirect to Add promotion Page    
            Response.Redirect(this.AddLink);
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindGridData();
        }

        /// <summary>
        /// Clear Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListPage);
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGridData();
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate cell in the GridView control.
                GridViewRow selectedRow = uxGrid.Rows[index];

                TableCell Idcell = selectedRow.Cells[0];
                string Id = Idcell.Text;

                if (e.CommandName == "Edit")
                {
                    this.AddLink = this.AddLink + "?ItemID=" + Id;
                    Response.Redirect(this.AddLink);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.DeleteLink + Id);
                }
            }
        }

        #endregion

        #region Page load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindGridData();
            }
        }
        #endregion

        #region Bind Methods

        // Bind data to grid   
        private void BindGridData()
        {
            ZNode.Libraries.Admin.GiftCardAdmin giftCardAdmin = new ZNode.Libraries.Admin.GiftCardAdmin();
            DataSet ds = giftCardAdmin.Search(this.BuildSearchParameter());
            uxGrid.DataSource = ds.Tables[0];
            uxGrid.DataBind();
        }

        /// <summary>
        /// Build the Search Parameter.
        /// </summary>
        /// <returns>Returns the GiftCardSearchAdmin object</returns>
        private GiftCardSearchParam BuildSearchParameter()
        {
            GiftCardSearchParam searchParam = new GiftCardSearchParam();
            searchParam.Name = Server.HtmlEncode(txtName.Text.Trim());
            searchParam.Balance = txtBalance.Text.Trim().Length > 0 ? Convert.ToDecimal(txtBalance.Text) : 0;
            searchParam.CardNumber = Server.HtmlEncode(txtCardNumber.Text.Trim());
            searchParam.AccountID = txtAccountId.Text.Trim().Length > 0 ? Convert.ToInt32(txtAccountId.Text) : 0;
            searchParam.DontShowExpired = chkDontShowExpired.Checked;
            return searchParam;
        }
        #endregion
    }
}