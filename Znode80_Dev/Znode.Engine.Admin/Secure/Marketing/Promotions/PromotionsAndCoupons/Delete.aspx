<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.Promotions.PromotionsAndCoupons.Delete" CodeBehind="Delete.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="PromotionDelete" Src="~/Controls/Default/Providers/Promotions/PromotionDelete.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
	<zn:PromotionDelete ID="ctlPromotionDelete" runat="server" RedirectUrl="~/Secure/Marketing/Promotions/PromotionsAndCoupons/Default.aspx" />
</asp:Content>
