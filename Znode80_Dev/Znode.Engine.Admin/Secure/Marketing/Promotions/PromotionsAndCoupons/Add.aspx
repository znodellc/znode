<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.Promotions.PromotionsAndCoupons.Add" ValidateRequest="false" CodeBehind="Add.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="PromotionAdd" Src="~/Controls/Default/Providers/Promotions/PromotionAdd.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:PromotionAdd ID="ctlPromotionAdd" runat="server" />
</asp:Content>
