<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.Promotions.PromotionsAndCoupons.Default" ValidateRequest="false" CodeBehind="Default.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="PromotionList" Src="~/Controls/Default/Providers/Promotions/PromotionList.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
	<zn:PromotionList ID="ctlPromotionList" runat="server"
		ListPageUrl="~/Secure/Marketing/Promotions/PromotionsAndCoupons/Default.aspx"
		AddPageUrl="~/Secure/Marketing/Promotions/PromotionsAndCoupons/Add.aspx"
		DeletePageUrl="~/Secure/Marketing/Promotions/PromotionsAndCoupons/Delete.aspx"
	/>
</asp:Content>
