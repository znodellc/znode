<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True"
    Inherits="Znode.Engine.Admin.Secure.Marketing.Default" Title="Marketing - Default"
    CodeBehind="Default.aspx.cs" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="LeftMargin">

        <h1> <asp:Localize runat="server" ID="Promotions" Text='<%$ Resources:ZnodeAdminResource, TabTitlePromotions %>'></asp:Localize></h1>
        
        <hr /> 
      
        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/promotions.png" />
            </div>
            <div class="Shortcut"><a id="A2" href="~/Secure/Marketing/Promotions/PromotionsAndCoupons/Default.aspx" runat="server"><asp:Localize runat="server" ID="PromotionandCoupons" Text='<%$ Resources:ZnodeAdminResource, TitlePromotionandCoupons %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="TextPromotionandCoupons" Text='<%$ Resources:ZnodeAdminResource, TextPromotionandCoupons %>'></asp:Localize></p>
            </div>
        </div>


        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/GiftCards.png" />
            </div>
            <div class="Shortcut"><a id="A11" href="~/Secure/Marketing/Promotions/GiftCards/Default.aspx" runat="server"><asp:Localize runat="server" ID="GiftCard" Text='<%$ Resources:ZnodeAdminResource, LinkTextGiftCards %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="TextGiftCards" Text='<%$ Resources:ZnodeAdminResource, TextGiftCards %>'></asp:Localize></p>
            </div>
        </div>

          <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/customer-reviews.png" /></div>
            <div class="Shortcut"><a id="A1" href="~/Secure/Marketing/Promotions/CustomerReviews/Default.aspx" runat="server"><asp:Localize runat="server" ID="CustomerReviews" Text='<%$ Resources:ZnodeAdminResource, LinkTextCustomerReviews %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="TextCustomerReviews" Text='<%$ Resources:ZnodeAdminResource, TextCustomerReviews %>'></asp:Localize></p>
            </div>
        </div> 
        
        <h1><asp:Localize runat="server" ID="TitleSearchPersonalization" Text='<%$ Resources:ZnodeAdminResource, TitleSearchPersonalization %>'></asp:Localize></h1>        
        <hr />
        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/tags.png" />
            </div>
            <div class="Shortcut"><a id="A10" href="~/Secure/Marketing/Search/Facets/Default.aspx" runat="server"><asp:Localize runat="server" ID="Facets" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFacets %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="TextFacets" Text='<%$ Resources:ZnodeAdminResource, TextFacets %>'></asp:Localize></p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/GlobalBoost.png" />
            </div>
            <div class="Shortcut"><a id="A3" href="~/Secure/Marketing/Search/ProductSearchSettings/Default.aspx" runat="server"><asp:Localize runat="server" ID="ProductSearchSettings" Text='<%$ Resources:ZnodeAdminResource, LinkTextProductSearchSettings %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="TextProductSearchSettings" Text='<%$ Resources:ZnodeAdminResource, TextProductSearchSettings %>'></asp:Localize></p>
            </div>
        </div>
		
		<div class="ImageAlign">
			<div class="Image">
				<img src="../../Themes/Images/personalization.png" />
			</div>
			<div class="Shortcut"><a id="A22" href="~/Secure/Marketing/Search/Personalization/Default.aspx" runat="server"><asp:Localize runat="server" ID="Personalization" Text='<%$ Resources:ZnodeAdminResource, LinkTextPersonalization %>'></asp:Localize></a></div>
			<div class="LeftAlign">
				<p><asp:Localize runat="server" ID="TextPersonalization" Text='<%$ Resources:ZnodeAdminResource, TextPersonalization %>'></asp:Localize></p>
			</div>
		</div>


        <h1><asp:Localize runat="server" ID="SEO" Text='<%$ Resources:ZnodeAdminResource, SubTitleSEO %>'></asp:Localize></h1>        
        <hr />

         <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/DefaultSEOSettings.png" />
            </div>
            <div class="Shortcut"><a id="A4" href="~/Secure/Marketing/SEO/SEOSettings/Default.aspx" runat="server"><asp:Localize runat="server" ID="SeoSettings" Text='<%$ Resources:ZnodeAdminResource, SubTitleSeoSettings %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="TextSEOSettings" Text='<%$ Resources:ZnodeAdminResource, TextSEOSettings %>'></asp:Localize></p>
            </div>
        </div>
        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/DownloadTrackingData.png" />
            </div>
            <div class="Shortcut"><a id="A14" href="~/Secure/Marketing/SEO/AffiliateTracking/Default.aspx" runat="server"><asp:Localize runat="server" ID="AffiliateTracking" Text='<%$ Resources:ZnodeAdminResource, LinkTextAffiliateTracking %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="TextAffiliateTracking" Text='<%$ Resources:ZnodeAdminResource, TextAffiliateTracking %>'></asp:Localize></p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/GenerateSiteMap.png" />
            </div>
            <div class="Shortcut"><a id="A15" href="~/Secure/Marketing/SEO/GoogleSiteMap/Default.aspx" runat="server"><asp:Localize runat="server" ID="GenerateSiteMap" Text='<%$ Resources:ZnodeAdminResource, LinkTextGoogleSiteMap %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="TextGoogleSiteMap" Text='<%$ Resources:ZnodeAdminResource, TextGoogleSiteMap %>'></asp:Localize></p>
            </div>
        </div>

        
        <h1><asp:Localize runat="server" ID="Other" Text='<%$ Resources:ZnodeAdminResource, LinkTextOther %>'></asp:Localize></h1>        
        <hr />

            <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/store-locator.png" />
            </div>
            <div class="Shortcut"><a id="A16" href="~/Secure/Marketing/Other/StoreLocator/Default.aspx" runat="server"><asp:Localize runat="server" ID="StoreLocator" Text='<%$ Resources:ZnodeAdminResource, LinkTextStoreLocator %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize runat="server" ID="TextStoreLocator" Text='<%$ Resources:ZnodeAdminResource, TextStoreLocator %>'></asp:Localize></p>
            </div>
        </div>
    

    </div>
</asp:Content>
