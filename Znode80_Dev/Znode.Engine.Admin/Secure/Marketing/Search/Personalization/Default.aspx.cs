﻿using System;
using AjaxControlToolkit;

namespace Znode.Engine.Admin.Secure.Marketing.Search.Personalization
{
	public partial class Default : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(Request.QueryString["Mode"])) return;

			var tab = ((TabPanel) PersonalizationTabs.FindControl(string.Format("{0}Tab", Request.QueryString["Mode"])));
			PersonalizationTabs.ActiveTab = tab;
		}
	}
}