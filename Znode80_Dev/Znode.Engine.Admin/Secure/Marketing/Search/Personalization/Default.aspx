﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Themes/Standard/edit.master" Title="Personalization" CodeBehind="Default.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Marketing.Search.Personalization.Default" %>

<%@ Register Src="~/Controls/Default/Search/FbtSelect.ascx" TagPrefix="uc1" TagName="FbtSelect" %>
<%@ Register Src="~/Controls/Default/Search/YmalSelect.ascx" TagPrefix="uc1" TagName="YmalSelect" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
	<script type="text/javascript">
		function NavigateToTab(tab) {
			window.location = '<%=Page.ResolveUrl("~/Secure/Marketing/Search/Personalization/Default.aspx?Mode=")%>' + tab;
		}
		
		function NavigateToFbtTab() {
			NavigateToTab('FBT');
		}
		
		function NavigateToYmalTab() {
			NavigateToTab('YMAL');
		}
	</script>
	<h1>Personalization</h1>
	<ajaxToolKit:TabContainer ID="PersonalizationTabs" runat="server">
		<ajaxToolKit:TabPanel runat="server" ID="YMALTab" OnClientClick="NavigateToYmalTab">
			<HeaderTemplate><asp:Localize ID="YMALTabText" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleYMAL %>'></asp:Localize></HeaderTemplate>
			<ContentTemplate>
				<uc1:YmalSelect runat="server" ID="YmalSelect" />
			</ContentTemplate>
		</ajaxToolKit:TabPanel>
		<ajaxToolKit:TabPanel ID="FBTTab" runat="server" OnClientClick="NavigateToFbtTab">
			<HeaderTemplate><asp:Localize ID="FBTTabText" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleFBT %>'></asp:Localize></HeaderTemplate>
			<ContentTemplate>
				<uc1:FbtSelect runat="server" ID="FbtSelect" />
			</ContentTemplate>
		</ajaxToolKit:TabPanel>
	</ajaxToolKit:TabContainer>
</asp:Content>
