using System;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace  Znode.Engine.Admin.Secure.Marketing.Search.Facets
{
    /// <summary>
    /// Represents the SiteAdmin - Znode.Engine.Admin.Secure.Marketing.Search.Facets.Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Variables

        private int ItemId;
        private string RedirectLink = "~/Secure/Marketing/Search/Facets/Default.aspx";
        private string _FacetGroupName = string.Empty;

        #endregion

        /// <summary>
        /// Gets or sets the Product Review Title
        /// </summary>
        public string FacetGroupName
        {
            get
            {
                return this._FacetGroupName;
            }

            set
            {
                this._FacetGroupName = value;
            }
        }

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }

            DeleteConfirmText.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextConfirmDeleteFacetGroup").ToString(), FacetGroupName);
        }

        #endregion

        #region General Events

        /// <summary>
        /// Delete Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
			FacetAdmin _facetAdmin = new FacetAdmin();
            FacetGroup _facetGroup = _facetAdmin.GetByFacetGroupID(this.ItemId);

            bool Check = false;
            string FacetName = _facetGroup.FacetGroupLabel;

            FacetService facetService = new FacetService();
            TList<Facet> facet = facetService.GetByFacetGroupID(this.ItemId);
            if (facet.Count == 0)
            {
                Check = _facetAdmin.DeleteFacetGroup(_facetGroup);
            }

            if (Check)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogDeleteFacetGroup").ToString() + FacetName, FacetName);

                Response.Redirect(this.RedirectLink);
            }
            else
            {
                this.FacetGroupName = _facetGroup.FacetGroupLabel;
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorDeleteFacetGroup").ToString();
                lblErrorMsg.Visible = true;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.RedirectLink);
        }

        #endregion

        #region Bind Data

        /// <summary>
        /// Bind Data Method
        /// </summary>
        private void BindData()
        {
			FacetAdmin _facetAdmin = new FacetAdmin();
            FacetGroup _facetGroup = _facetAdmin.GetByFacetGroupID(this.ItemId);

            if (_facetGroup != null)
            {
                CatalogAdmin catalogAdmin = new CatalogAdmin();
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess).Split(',');
                    int found = Array.IndexOf(stores, _facetGroup.CatalogID.ToString());
                    if (found == -1)
                    {
                        Response.Redirect("Default.aspx", true);
                    }
                }

                this.FacetGroupName = _facetGroup.FacetGroupLabel;
            }
        }

        #endregion
    }
}