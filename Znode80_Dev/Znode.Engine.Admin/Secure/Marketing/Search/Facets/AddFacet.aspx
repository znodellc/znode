<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" ValidateRequest="false" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.Search.Facets.AddFacet" CodeBehind="AddFacet.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>
        <div class="LeftFloat" align="right" style="width: 30%">
            <zn:Button runat="server" ID="btnSubmitTop" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true"/>
            <zn:Button runat="server" ID="btnCancelTop" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
        </div>
        <div class="ClearBoth" align="left">
        </div>
        <asp:Label ID="lblError" runat="server" CssClass="Error" />
        <div class="FieldStyle">
            <asp:Localize ID="FacetValue" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFacetValue %>'></asp:Localize> <span class="Asterix">*</span><br />
            <small><asp:Localize ID="FacetValueText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextFacetValue %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="tagName" runat="server" MaxLength="50" Columns="30"></asp:TextBox>&nbsp;&nbsp;
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tagName"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredFacetName %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator
                    ID="RegularExpressionValidator2" runat="server" ControlToValidate="tagName"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidFacetName %>'
                    SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_ <>]+)"></asp:RegularExpressionValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="DisplayOrderColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder %>'></asp:Localize><span class="Asterix">*</span><br />
            <small><asp:Localize ID="DisplayOrderText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextAddOnDisplayOrder %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="DisplayOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrder %>' ControlToValidate="DisplayOrder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="DisplayOrder"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeWholeNumber %>' MaximumValue="999999999"
                MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="ClearBoth" align="left">
        </div>
        <asp:Panel runat="server" ID="pnlIconImage">
            <h4 class="SubTitle"><asp:Localize ID="IconImageColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnIconImage %>'></asp:Localize></h4>
            <small><asp:Localize ID="IconImageText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextIconImage %>'></asp:Localize></small>
            <div>
                <div class="LeftFloat" id="tblShowImage" runat="server" visible="false" style="width: 150px; border-right: solid 1px #cccccc;">
                    <asp:Image ID="IconImage" runat="server" />
                </div>
                <div class="LeftFloat" style="margin-left: -1px; margin-top: 5px;">
                    <asp:Panel runat="server" ID="pnlShowImage">
                        <div>
                            <asp:Localize ID="SelectAnOption" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectanOption %>'></asp:Localize>
                        </div>
                        <div>
                            <asp:RadioButton ID="RadioCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonCurrentImage %>' runat="server"
                                GroupName="TagIconImage" AutoPostBack="True" OnCheckedChanged="RadioCurrentImage_CheckedChanged"
                                Checked="True" /><br />
                            <asp:RadioButton ID="RadioNewImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNewImage %>' runat="server"
                                GroupName="TagIconImage" AutoPostBack="True" OnCheckedChanged="RadioNewImage_CheckedChanged" />
                        </div>
                        <br />
                    </asp:Panel>
                    <div id="tblUploadImage" runat="server" visible="false">
                        <div class="FieldStyle">
                            <asp:Localize ID="SelectImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectImage1 %>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:FileUpload ID="UploadIconImage" runat="server" BorderStyle="Inset" EnableViewState="true" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UploadIconImage"
                                CssClass="Error" Display="dynamic" ValidationExpression=".*(\.[Jj][Pp][Gg]|\.[Gg][Ii][Ff]|\.[Jj][Pp][Ee][Gg]|\.[Pp][Nn][Gg])"
                                ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidImage %>'></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div>
                        <asp:Label ID="lblImageError" runat="server" CssClass="Error" ForeColor="Red"
                            Text="" Visible="False"></asp:Label>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div class="ClearBoth">
            <br />
            <br />
        </div>
        <div>
            <zn:Button runat="server" ID="btnSubmit" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true"/>
            <zn:Button runat="server" ID="btnCancel" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
        </div>
    </div>
</asp:Content>
