<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" ValidateRequest="false" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.Search.Facets.AddFacetGroup" CodeBehind="AddFacetGroup.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="viewtag" Src="~/Secure/Marketing/Search/Facets/viewfacet.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <script type="Text/JavaScript">
        function ListBox(sender, args) {
            var ctlListbox = document.getElementById(sender.controltovalidate);
            args.IsValid = ctlListbox.options.length > 0;
        }
    </script>
    <div class="FormView">
        <h1>
            <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        <div>
            <ZNode:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <ajaxToolKit:TabContainer ID="tabFacetGroupSettings" runat="server">
            <ajaxToolKit:TabPanel ID="pnlSettings" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="FacetGroupInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleFacetGroupInformation %>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <h4 class="SubTitle"><asp:Localize ID="GgeneralSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralSettings %>'></asp:Localize></h4>
                    <div class="FieldStyle">
                        <asp:Localize ID="FacetGroupName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFacetGroupName %>'></asp:Localize><span class="Asterix">*</span><br />
                        <small><asp:Localize ID="FacetGroupNameText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextFacetGroupName %>'></asp:Localize></small>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtFacetGroupName" runat="server" Width="152px"></asp:TextBox>&nbsp;
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFacetGroupName"
                            ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredFacetGroupName %>' CssClass="Error" Display="Dynamic"
                            ValidationGroup="grpTitle"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtFacetGroupName"
                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidFacetGroupName %>'
                            SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_ ]+)" ValidationGroup="grpTitle"></asp:RegularExpressionValidator>
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="DisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder %>'></asp:Localize><span class="Asterix">*</span><br />
                        <small style="display: none;"><asp:Localize ID="DisplayOrderText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextDisplayOrder %>'></asp:Localize></small>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="9" Columns="5">500</asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="grpTitle" ControlToValidate="txtDisplayOrder"
                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrder %>'></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtDisplayOrder"
                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>' MaximumValue="999999999" ValidationGroup="grpTitle"
                            MinimumValue="1" Type="Integer"></asp:RangeValidator>
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="ddlTagControlType" runat="server" Visible="false"></asp:DropDownList>
                    </div>
                    <asp:Panel ID="pnlCatalog" runat="server">
                        <div class="FieldStyle"><asp:Localize ID="SelectCatalog" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectCatalog %>'></asp:Localize></div>
                        <div class="ValueStyle">
                            <asp:DropDownList ID="ddlCatalog" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DdlCatalog_SelectedIndexChanged"></asp:DropDownList>
                        </div>
                        <br />
                    </asp:Panel>
                    <asp:UpdatePanel ID="UpdatePnlProductDetail" runat="server">
                        <ContentTemplate>
                            <h4 class="SubTitle"><asp:Localize ID="AssociateFacetsGroup" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleAssociateFacetGroups %>'></asp:Localize></h4>
                            <div style="display: block;">
                                <div>
                                    <div class="LeftFloat" style="width: 200px;">
                                        <span class="FieldStyle"><asp:Localize ID="UnAssociatedCategories" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnUnAssociatedCategory %>'></asp:Localize></span><br />
                                        <span>
                                            <asp:ListBox ID="lstboxCategories" runat="server" SelectionMode="Multiple" Width="150px" Height="150px"></asp:ListBox>
                                            <br />
                                        </span>
                                        <br />
                                        <asp:Label ID="lblerror" runat="server" CssClass="Error" Visible="false"></asp:Label>
                                    </div>

                                    <div class="LeftFloat" style="width: 150px; padding-top: 50px;">
                                        <span>
                                            <zn:Button runat="server" ButtonType="EditButton" OnClick="BtnAddlist_Onclick" Text='<%$ Resources:ZnodeAdminResource, ButtonAdd%>' ID="btnAddlist" Width="100px" />
                                            <br />
                                            <br />
                                            <zn:Button runat="server" ButtonType="EditButton" OnClick="BtnRemovelist_Onclick" Text='<%$ Resources:ZnodeAdminResource, ButtonAssociationRemove%>' ID="btnRemovelist" Width="100px" />
                                        </span>
                                    </div>
                                    <div class="LeftFloat" style="width: 170px">
                                        <span class="FieldStyle"><asp:Localize ID="AssociatedCategory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAssociatedCategory %>'></asp:Localize></span><br />
                                        <span>
                                            <asp:ListBox ID="lstboxTagCategories" runat="server" SelectionMode="Multiple" Width="150px" Height="150px"></asp:ListBox>
                                            <br />
                                        </span>
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="lstboxTagCategories" CssClass="Error"
                                            ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidAssociateCategory %>' OnServerValidate="CustomValidator1_ServerValidate" SetFocusOnError="True"
                                            ValidateEmptyText="True" ValidationGroup="grpTitle" ClientValidationFunction="ListBox"></asp:CustomValidator>


                                    </div>
                                    <div class="LeftFloat" style="width: 50px; padding-top: 50px;" align="left">
                                        <asp:ImageButton ImageUrl="~/Themes/images/403-up.gif" ID="btnUp" runat="server" OnClick="BtnUp_Onclick" />
                                        <br />
                                        <br />
                                        <asp:ImageButton ImageUrl="~/Themes/images/402-down.gif" ID="btndown" runat="server" OnClick="Btndown_Onclick" />
                                    </div>
                                </div>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div class="ClearBoth">
                    </div>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlDepartment" runat="server" Visible="false">
                <HeaderTemplate>
                    <asp:Localize ID="AssociatedFacets" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleAssociatedFacets %>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <ZNode:viewtag runat="server"></ZNode:viewtag>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
        </ajaxToolKit:TabContainer>
        <br />
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <br />
        <div>
            <zn:Button runat="server" ID="btnSubmit" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true" ValidationGroup="grpTitle"/>
            <zn:Button runat="server" ID="btnCancel" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
        </div>
    </div>

</asp:Content>
