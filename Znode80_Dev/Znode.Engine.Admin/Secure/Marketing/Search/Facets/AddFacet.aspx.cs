using System;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Marketing.Search.Facets
{
    /// <summary>
    /// Represents the SiteAdmin - Znode.Engine.Admin.Secure.Marketing.Search.AddFacet class
    /// </summary>
    public partial class AddFacet : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId = 0;
        private int TagId = 0;
        private string ViewLink = "~/Secure/Marketing/Search/Facets/addfacetgroup.aspx?mode=at&itemid=";
        private FacetGroup tg;

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);

                if (this.ItemId > 0)
                {
                    ZNode.Libraries.DataAccess.Service.FacetGroupService tgs = new ZNode.Libraries.DataAccess.Service.FacetGroupService();
                    this.tg = tgs.GetByFacetGroupID(this.ItemId);

                    CatalogAdmin catalogAdmin = new CatalogAdmin();
                    ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                    if (profiles.StoreAccess != "AllStores")
                    {
                        string[] stores = catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess).Split(',');
                        int found = Array.IndexOf(stores, this.tg.CatalogID.ToString());
                        if (found == -1)
                        {
                            Response.Redirect("Default.aspx", true);
                        }
                    }

                    if (this.tg.ControlTypeID == (int)ZNode.Libraries.ECommerce.Tagging.ZNodeFacetControlType.ICONS)
                    {
                        pnlIconImage.Visible = true;
                    }
                    else
                    {
                        pnlIconImage.Visible = false;
                    }
                }
            }

            // Get AttributeId from QueryString
            if (Request.Params["FacetId"] != null)
            {
                this.TagId = int.Parse(Request.Params["FacetId"]);
            }

            if (!Page.IsPostBack)
            {
                // Check for Edit Mode
                if (this.TagId > 0)
                {
                    // Bind Data into fields
                    this.BindData();
                    tblShowImage.Visible = true;
                    pnlShowImage.Visible = true;
                    tblUploadImage.Visible = false;
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource","TitleEditFacet").ToString();
                }
                else
                {
                    tblShowImage.Visible = false;
                    pnlShowImage.Visible = false;
                    tblUploadImage.Visible = true;
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddFacet").ToString();
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            // Declarations
            string fileName = string.Empty;
			FacetAdmin _AdminAccess = new FacetAdmin();
			Facet _Tag = new Facet();

            // Check for Edit Mode
            if (this.TagId > 0)
            {
                _Tag = _AdminAccess.GetByFacetID(this.TagId);
            }
            else
            {
				TList<Facet> tagList = _AdminAccess.GetFacetsByFacetGroupID(this.ItemId);
				Facet tag = tagList.Find("TagName", Server.HtmlEncode(tagName.Text.Trim()));
                if (tag != null)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorFacetExists").ToString();
                    return;
                }
            }

            // Set Values
            _Tag.FacetName = Server.HtmlEncode(tagName.Text.Trim());
			_Tag.FacetDisplayOrder = int.Parse(DisplayOrder.Text.Trim());
            _Tag.FacetGroupID = this.ItemId;

            // Image Process
            if ((this.TagId == 0) || (RadioNewImage.Checked == true))
            {
                if (UploadIconImage.PostedFile != null)
                {
                    if (UploadIconImage.PostedFile.FileName != string.Empty)
                    {
                        // Check for Product Image
                        fileName =  System.IO.Path.GetFileNameWithoutExtension(UploadIconImage.PostedFile.FileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(UploadIconImage.PostedFile.FileName);

                        // Check for Product Image
                        if (fileName != string.Empty)
                        {
                            _Tag.IconPath = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + fileName;
                        }
                    }
                }
            }
            else
            {
                _Tag.IconPath = _Tag.IconPath;
            }

            // Upload File if this is a new tag or the New Image option was selected for an existing product
            if (RadioNewImage.Checked || this.TagId == 0)
            {
                if (fileName != string.Empty)
                {
                    byte[] imageData = new byte[UploadIconImage.PostedFile.InputStream.Length];
                    UploadIconImage.PostedFile.InputStream.Read(imageData, 0, (int)UploadIconImage.PostedFile.InputStream.Length);
                    ZNodeStorageManager.WriteBinaryStorage(imageData, ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + fileName);
                    UploadIconImage.Dispose();
                }
            }

            bool status = false;

            if (this.TagId > 0)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditFacet").ToString() + tagName.Text, tagName.Text);

                // Update Product Attribute
				status = _AdminAccess.UpdateFacet(_Tag);
            }
            else
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogAddFacet").ToString() + tagName.Text, tagName.Text);

				status = _AdminAccess.AddFacet(_Tag);
            }

            if (status)
            {
                // Redirect to main page
                Response.Redirect(this.ViewLink + this.ItemId);
            }
            else
            {
                // Display error message
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoteAdd").ToString();
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            // Redirect to main page
            Response.Redirect(this.ViewLink + this.ItemId);
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblUploadImage.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblUploadImage.Visible = true;
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Edit Attribute Datas
        /// </summary>
        private void BindData()
        {
            // Declarations
			FacetAdmin _AdminAccess = new FacetAdmin();
			Facet _Tag = _AdminAccess.GetByFacetID(this.TagId);

            // Check Tag for null
            if (_Tag != null)
            {
                tagName.Text = Server.HtmlDecode(_Tag.FacetName);
				DisplayOrder.Text = _Tag.FacetDisplayOrder.ToString();
                ZNodeImage znodeImage = new ZNodeImage();
                IconImage.ImageUrl = znodeImage.GetImageHttpPathSmall(_Tag.IconPath);
            }
        }
        #endregion
    }
}