<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.Search.Facets.Delete" CodeBehind="Delete.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h5><asp:Localize ID="PleaseConfirmText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm %>'></asp:Localize></h5>
    <p>
        <asp:label ID="DeleteConfirmText" runat="server"></asp:label>
    </p>
    <p>
        <asp:Label ID="lblErrorMsg" runat="server" Visible="False" CssClass="Error"></asp:Label>
    </p>
    <zn:Button runat="server" ID="btnDelete" OnClick="BtnDelete_Click" CausesValidation="True" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>' />
    <zn:Button runat="server" ID="btnCancel" OnClick="BtnCancel_Click" CausesValidation="False" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
    <br />
    <br />
    <br />
</asp:Content>
