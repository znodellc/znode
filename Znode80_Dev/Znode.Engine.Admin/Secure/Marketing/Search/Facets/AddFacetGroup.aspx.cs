using System;
using System.Collections;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace  Znode.Engine.Admin.Secure.Marketing.Search.Facets
{
    /// <summary>
    /// Represents the SiteAdmin - Znode.Engine.Admin.Secure.Marketing.Search.Facets.AddFacetGroup class
    /// </summary>
    public partial class AddFacetGroup : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private string ListLink = "~/Secure/Marketing/Search/Facets/Default.aspx";

		private FacetAdmin tagAdmin = new FacetAdmin();
        private CategoryAdmin categoryAdmin = new CategoryAdmin();
        private CatalogAdmin catalogAdmin = new CatalogAdmin();
        private FacetGroup tagGroup = new FacetGroup();
		private FacetGroupCategory tagGroupCategory = new FacetGroupCategory();

        private ArrayList lstCategoryArraylist = new ArrayList();
        private ArrayList lstTagCategoryArraylist = new ArrayList();
        #endregion

        #region Helper Methods

        /// <summary>
        /// Add to List Box
        /// </summary>
        public void AddToList()
        {

            foreach (ListItem CategoryItem in lstboxCategories.Items)
            {
                if (CategoryItem.Selected)
                {
                    if (!lstboxTagCategories.Items.Contains(CategoryItem))
                    {
                        lstboxTagCategories.Items.Add(CategoryItem);
	                    lblerror.Text = string.Empty;
                    }
                }
            }

            
            foreach (ListItem TagCategoryItem in lstboxTagCategories.Items)
            {
                if (lstboxCategories.Items.Contains(TagCategoryItem))
                {
                    lstboxCategories.Items.Remove(TagCategoryItem);
                }
            }

			
        }

        /// <summary>
        /// Remove from the listBox
        /// </summary>
        public void RemoveFromList()
        {
            foreach (ListItem TagCategoryItem in lstboxTagCategories.Items)
            {
                if (TagCategoryItem.Selected)
                {
                    if (!lstboxCategories.Items.Contains(TagCategoryItem))
                    {
                        lstboxCategories.Items.Add(TagCategoryItem);
						lblerror.Text = string.Empty;
                    }
                }
            }

            foreach (ListItem CategoryItem in lstboxCategories.Items)
            {
                if (lstboxTagCategories.Items.Contains(CategoryItem))
                {
                    lstboxTagCategories.Items.Remove(CategoryItem);
                }
            }
        }

        /// <summary>
        /// Move the selected listItem up and down
        /// </summary>
        /// <param name="isMoveUp">Boolean value either true or false</param>
        public void MoveUpdown(bool isMoveUp)
        {
            if (lstboxTagCategories.SelectedItem != null)
            {
                int Index = lstboxTagCategories.SelectedIndex;

                // Selected Item
                ListItem li = new ListItem();
                li.Text = lstboxTagCategories.SelectedItem.Text;
                li.Value = lstboxTagCategories.SelectedItem.Value;

                if (Index != -1 && lstboxTagCategories.Items.Count > 1)
                {
                    // Move up
                    if (isMoveUp)
                    {
                        lstboxTagCategories.Items.RemoveAt(Index);
                        lstboxTagCategories.Items.Insert(Index - 1, li);
                    }
                    else
                    {
                        // Move down
                        lstboxTagCategories.Items.RemoveAt(Index);
                        lstboxTagCategories.Items.Insert(Index + 1, li);
                    }
                }
            }
        }

        #endregion

        #region Page_Load

        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId value from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }
            
            // Get ItemId value from querystring        
            if (Request.Params["Mode"] != null)
            {
                if (Request.Params["Mode"].ToString().Equals("Dept"))
                {
                    tabFacetGroupSettings.ActiveTabIndex = 1;
                }
            }

            if (!Page.IsPostBack)
            {
                this.BindControlType();
                this.BindCatalogData();
                this.BindEditData();
                this.AddToList();

                if (ddlCatalog.SelectedValue != "0")
                {
                    this.BindCategoryData();
                }

                if (this.ItemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource","TitleEditFacetGroup").ToString() + txtFacetGroupName.Text.Trim();
                    pnlDepartment.Visible = true;
                    pnlCatalog.Visible = false;
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource","TitleAddFacetGroup").ToString();
                    pnlDepartment.Visible = false;
                }
            }

            if (Convert.ToString(Request.QueryString["mode"]) == "at")
            {
                tabFacetGroupSettings.ActiveTabIndex = 1;
            }
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind Edit Data Method
        /// </summary>
        protected void BindEditData()
        {
            if (this.ItemId > 0)
            {
                this.tagGroup = this.tagAdmin.GetByFacetGroupID(this.ItemId);

                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = this.catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess).Split(',');
                    int found = Array.IndexOf(stores, this.tagGroup.CatalogID.ToString());
                    if (found == -1)
                    {
                        Response.Redirect("Default.aspx", true);
                    }
                }

                lblTitle.Text += this.tagGroup.FacetGroupLabel;
                txtFacetGroupName.Text = Server.HtmlDecode(this.tagGroup.FacetGroupLabel);
                txtDisplayOrder.Text = this.tagGroup.DisplayOrder.ToString();
                ddlCatalog.SelectedValue = this.tagGroup.CatalogID.ToString();
                ddlTagControlType.SelectedValue = this.tagGroup.ControlTypeID.ToString();
            }
        }

        /// <summary>
        /// Bind the list box controls based on 
        /// categories assocaited with this Tag Group
        /// </summary>
        protected void BindCategoryData()
        {
            // Return the categories which is NOT associated 
            // with specific tagGroup
            DataSet ds = new DataSet();
            ds = this.tagAdmin.GetCategoriesByFacetGroupID(this.ItemId, Convert.ToInt32(ddlCatalog.SelectedValue), false);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                }
            }
            
            lstboxCategories.DataSource = ds;
            lstboxCategories.DataTextField = "Name";
            lstboxCategories.DataValueField = "CategoryID";
            lstboxCategories.DataBind();

            // Return the categories which is associated 
            // With specific tagGroup
            DataSet tagGroupDataset = this.tagAdmin.GetCategoriesByFacetGroupID(this.ItemId, Convert.ToInt32(ddlCatalog.SelectedValue), true);
            if (tagGroupDataset.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < tagGroupDataset.Tables[0].Rows.Count; index++)
                {
                    tagGroupDataset.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(tagGroupDataset.Tables[0].Rows[index]["Name"].ToString());
                }
            }
            
            if (tagGroupDataset != null)
            {
                // Bind Category only associated with this tagGroup
                lstboxTagCategories.DataSource = tagGroupDataset;
                lstboxTagCategories.DataTextField = "Name";
                lstboxTagCategories.DataValueField = "CategoryID";
                lstboxTagCategories.DataBind();
            }
        }

        /// <summary>
        /// Bind Control Type
        /// </summary>
        protected void BindControlType()
        {
			ddlTagControlType.DataSource = this.tagAdmin.GetAllFacetControlType();
            ddlTagControlType.DataTextField = "ControlName";
            ddlTagControlType.DataValueField = "ControlTypeID";
            ddlTagControlType.DataBind();
        }

        /// <summary>
        /// Bind Store Catalogs
        /// </summary>
        protected void BindCatalogData()
        {
            TList<Catalog> catalogList = this.catalogAdmin.GetAllCatalogs();
            DataSet ds = catalogList.ToDataSet(false);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                }
            }

            DataView dataView = ds.Tables[0].DefaultView;
            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                dataView.RowFilter = string.Concat("CatalogID IN (", this.catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess), ")");
            }
           
            ddlCatalog.DataSource = dataView;
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();
        }

        #endregion

        #region General Events

        protected void DdlCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindCategoryData();
        }

        /// <summary>
        /// AddList Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddlist_Onclick(object sender, EventArgs e)
        {
			if (lstboxCategories.SelectedItem == null)
			{
				lblerror.Visible = true;
                lblerror.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAssociateCategory").ToString();
			}
			else
			{
				this.AddToList();
			}
			
        }

        /// <summary>
        /// Remove Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnRemovelist_Onclick(object sender, EventArgs e)
        {
			if (lstboxTagCategories.SelectedItem == null)
			{
				lblerror.Visible = true;
                lblerror.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAssociateCategoryRemove").ToString(); 
			}
			else
			{
				this.RemoveFromList();
			}
           
        }

        /// <summary>
        ///  Up Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnUp_Onclick(object sender, EventArgs e)
        {
            this.MoveUpdown(true);
        }

        /// <summary>
        /// Down Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Btndown_Onclick(object sender, EventArgs e)
        {
            this.MoveUpdown(false);
        }

        /// <summary>
        /// On Submit event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
	      
            bool status = false;

            if (this.ItemId > 0)
            {
                this.tagGroup = this.tagAdmin.GetByFacetGroupID(this.ItemId);
            }

            this.tagGroup.FacetGroupLabel = Server.HtmlEncode(txtFacetGroupName.Text);
            this.tagGroup.DisplayOrder = int.Parse(txtDisplayOrder.Text);
            this.tagGroup.CatalogID = int.Parse(ddlCatalog.SelectedValue);
            this.tagGroup.ControlTypeID = int.Parse(ddlTagControlType.SelectedValue);
            TransactionManager transactionManager = null;

            try
            {
               transactionManager = ConnectionScope.CreateTransaction();
                if (this.ItemId > 0)
                {
                    status = this.tagAdmin.UpdateFacetGroup(this.tagGroup);
                }
                else
                {
                    status = this.tagAdmin.AddFacetGroup(this.tagGroup);
                }

                int tagGroupId = this.tagGroup.FacetGroupID;
                  
                foreach (var tGroupCategory in this.tagGroup.FacetGroupCategoryCollection)
                {
                    this.tagAdmin.DeleteFacetGroupCategegory(tGroupCategory);
                } 

                // Need to code here for 
                // Add/Update FacetGroupCategory data        
                int displayOrderValue = 0;

                foreach (ListItem Item in lstboxTagCategories.Items)
                {
                    displayOrderValue = displayOrderValue + 1;
                    this.tagGroupCategory.FacetGroupID = tagGroupId;
                    this.tagGroupCategory.CategoryID = int.Parse(Item.Value);
                    this.tagGroupCategory.CategoryDisplayOrder = displayOrderValue;

                    status = this.tagAdmin.AddFacetGroupCategory(this.tagGroupCategory);
                }
            }
            catch
            {
                 transactionManager.Rollback();

                 lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateFacetGroup").ToString();
                return;
            }

            if (!status)
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateFacetGroup").ToString();
                return;
            }
            else
            {
               transactionManager.Commit();

                if (this.ItemId > 0)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditFacetGroup").ToString() + txtFacetGroupName.Text, txtFacetGroupName.Text);
                }
                else
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddFacetGroup").ToString() + txtFacetGroupName.Text, txtFacetGroupName.Text);
                }
                
                Response.Redirect(this.ListLink);
            }
        }

        /// <summary>
        /// On Cancel event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListLink);
        }


        protected void CustomValidator1_ServerValidate(object source, ServerValidateEventArgs args)
        {
            args.IsValid = lstboxTagCategories.Items.Count > 0;
        }

        #endregion
    }
}