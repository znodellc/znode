﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Marketing.Search.Facets
{
    /// <summary>
    /// Represents the SiteAdmin - Znode.Engine.Admin.Secure.Marketing.Search.Facets class
    /// </summary>
    /// 

    public partial class ViewFacet : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private int ItemId = 0;
        private string AddTagValueLink = "~/Secure/Marketing/Search/Facets/addfacet.aspx?itemid=";
        private string ListLink = "~/Secure/Marketing/Search/Facets/list.aspx";
        private string EditLink = "~/Secure/Marketing/Search/Facets/addfacet.aspx?itemid=";

        #endregion

        #region Page Load
        /// <summary>
        /// page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }

            if (!Page.IsPostBack)
            {
                this.BindGrid();
                this.BindData();
            }
        }

        #endregion

        #region General Events

        /// <summary>
        /// List Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void FacetGroupTypeList_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListLink);
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
		protected void AddFacet_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddTagValueLink + this.ItemId);
        }

        /// <summary>
        /// Edit Attribute Type Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditAttributeType_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.EditLink + this.ItemId);
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Grid Page Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                // Get the Value from the command argument
                string Id = e.CommandArgument.ToString();
                if (e.CommandName == "Edit")
                {
                    // Redirect to Attribute Edit page
                    Response.Redirect(this.AddTagValueLink + this.ItemId + "&FacetId=" + Id);
                }

                if (e.CommandName == "Delete")
                {
					FacetAdmin adminAccess = new FacetAdmin();
					Facet tag = adminAccess.GetByFacetID(int.Parse(Id));
                    string TagName = tag.FacetName;

					if (adminAccess.DeleteFacet(tag))
                    {
                        // Nothing todo here
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogDeleteFacet").ToString() + TagName, TagName);
                    }
                    else
                    {
                        FailureText.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorDeleteFacetValue").ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Grid Row Deleting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGrid();
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind Data Method
        /// </summary>
        private void BindData()
        {
			FacetAdmin adminAccess = new FacetAdmin();
            FacetGroup tagGroup = adminAccess.GetByFacetGroupID(this.ItemId);

            if (tagGroup != null)
            {
                lblFacetGroupType.Text = tagGroup.FacetGroupLabel;
            }
        }

        #endregion

        #region Bind Grid

        /// <summary>
        /// Bind Grid method
        /// </summary>
        private void BindGrid()
        {
			FacetAdmin adminAccess = new FacetAdmin();
			TList<Facet> tagList = new TList<Facet>();
			tagList = adminAccess.GetFacetsByFacetGroupID(this.ItemId);
			foreach (Facet tag in tagList)
            {
                tag.FacetName = Server.HtmlDecode(tag.FacetName);
            }

            uxGrid.DataSource = tagList;
            uxGrid.DataBind();
        }

        #endregion
    }
}