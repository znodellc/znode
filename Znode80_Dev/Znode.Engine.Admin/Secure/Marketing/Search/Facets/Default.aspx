<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.Search.Facets.Default" CodeBehind="Default.aspx.cs" Title="Manage Facets - Default" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div>
            <div class="LeftFloat" style="width: 50%">
                <h1><asp:Localize ID="FacetsGroups" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleFacetGroups %>'></asp:Localize></h1>
            </div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="btnAddFacetGroup" runat="server" ButtonType="Button" OnClick="BtnAdd_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonAddFacetGroup %>' ButtonPriority="Primary" />
            </div>
        </div>
        <p class="ClearBoth">
            <asp:Localize ID="FacetGroupsText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextFacets %>'></asp:Localize>
        </p>
        <div class="ClearBoth" align="left">
            <br />
        </div>

        <h4 class="SubTitle"><asp:Localize ID="SearchFacetGroups" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchFacetGroups %>'></asp:Localize></h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="FacetGroupName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFacetGroupName %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtFacetGroupName" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="Catalog" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCatalog %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlCatalog" runat="server"></asp:DropDownList></span>
                    </div>
                </div>
                <div class="ClearBoth">
                </div>
                <div>
                    <zn:Button runat="server" ID="btnClear" OnClick="BtnClear_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                    <zn:Button runat="server" ID="btnSearch" OnClick="BtnSearch_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle"><asp:Localize ID="GridTitleFacetGroup" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleFacetGroup %>'></asp:Localize></h4>
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnSorting="UxGrid_Sorting" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" EnableSortingAndPagingCallbacks="False"
            PageSize="25" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundFacetGroups %>'>
            <Columns>
                <asp:BoundField DataField="FacetGroupID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField SortExpression="FacetGroupLabel" DataField="FacetGroupLabel" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="60%" />
                <asp:BoundField SortExpression="DisplayOrder" DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource,  ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnEdit" CssClass="actionlink" runat="server" CommandName="Manage" Text='<%$ Resources:ZnodeAdminResource, LinkManage %>'
                            ButtonType="Button" CommandArgument='<%# Eval("FacetGroupID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btndelete" CssClass="actionlink" runat="server" CommandName="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CommandArgument='<%# Eval("FacetGroupID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false">
                    <ItemTemplate>
                        <asp:LinkButton ID="btnView" CssClass="actionlink" runat="server" CommandName="Tags" Text='<%$ Resources:ZnodeAdminResource, LinkFacets %>' CommandArgument='<%# Eval("FacetGroupID") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:spacer id="Spacer2" spacerheight="10" spacerwidth="10" runat="server"></uc1:spacer>
        </div>
    </div>
</asp:Content>
