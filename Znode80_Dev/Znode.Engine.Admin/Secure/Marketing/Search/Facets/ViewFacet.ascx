﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.Search.Facets.ViewFacet" CodeBehind="ViewFacet.ascx.cs" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<div class="FormView">
    <div>
        <asp:Label ID="lblFacetGroupType" Visible="false" runat="server"></asp:Label>
        <div class="ButtonStyle">
            <zn:LinkButton ID="AddFacet" runat="server" ButtonType="Button" OnClick="AddFacet_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddFacet %>' ButtonPriority="Primary" />
        </div>
    </div>
    <div class="ClearBoth" align="left">
        <br />
    </div>
    <asp:Label ID="FailureText" runat="Server" EnableViewState="false" CssClass="Error" />
    <h4 class="GridTitle"><asp:Localize ID="GridTitleFacet" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleFacet %>'></asp:Localize></h4>
    <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False" CaptionAlign="Left" CellPadding="4" CssClass="Grid" 
        EmptyDataText='<%$ Resources:ZnodeAdminResource, NoRecordFoundFacets %>' GridLines="None" Width="100%" OnPageIndexChanging="UxGrid_PageIndexChanging" 
        OnRowCommand="UxGrid_RowCommand" OnRowDeleting="UxGrid_RowDeleting">
        <Columns>
            <asp:BoundField DataField="FacetID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>'
                HeaderStyle-HorizontalAlign="Left">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="FacetName" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleFacetName %>'
                HeaderStyle-HorizontalAlign="Left">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:BoundField>
            <asp:BoundField DataField="FacetDisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>'
                HeaderStyle-HorizontalAlign="Left">
                <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
            </asp:BoundField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="EditFacet" CommandName="Edit" CommandArgument='<%# Eval("FacetID") %>'
                        runat="server" CssClass="actionlink" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:LinkButton ID="DeleteFacet" CommandName="Delete" CommandArgument='<%# Eval("FacetID") %>'
                        runat="server" CssClass="actionlink" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>'
                        OnClientClick="return DeleteConfirmationFacet();" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <RowStyle CssClass="RowStyle" />
        <EditRowStyle CssClass="EditRowStyle" />
        <HeaderStyle CssClass="HeaderStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
    </asp:GridView>
    <uc1:Spacer ID="Spacer5" SpacerHeight="1" SpacerWidth="3" runat="server"></uc1:Spacer>
</div>

<script language="javascript" type="text/javascript">
    function DeleteConfirmationFacet() {
        return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ConfirmDeleteFacet")) %>');
    }
</script>