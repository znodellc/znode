<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/Themes/Standard/content.master"
    Inherits="Znode.Engine.Admin.Secure.Marketing.Search.ProductSearchSettings.Default" Title="Manage Global Boost Settings"
    CodeBehind="Default.aspx.cs" %>

<%@ Register Src="~/Controls/Default/Search/ProductSelect.ascx" TagPrefix="uc1" TagName="ProductSelect" %>
<%@ Register Src="~/Controls/Default/Search/ProductCategorySelect.ascx" TagPrefix="uc3" TagName="ProductCategorySelect" %>
<%@ Register Src="~/Controls/Default/Search/FieldBoostSelect.ascx" TagPrefix="uc4" TagName="FieldBoostSelect" %>


<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <script type="text/javascript">
        function ActiveTabChanged(sender, e) {
            var activeTabIndex = document.getElementById('<%=hdnActiveTabIndex.ClientID%>');
            activeTabIndex.value = sender.get_activeTabIndex();
        }
    </script>
    <div>
        <h1> <asp:Localize ID="ColumnPostalCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextProductSearchSettings%>'></asp:Localize>
        </h1>
        <p>
           <asp:Localize ID="TextProductSearchSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextProductSearchSettings%>'></asp:Localize>
        </p>

        <div style="clear: both;">

            <asp:HiddenField ID="hdnActiveTabIndex" runat="server" Value="0" />
            <ajaxToolKit:TabContainer ID="tabBoostSettings" runat="server">
                <ajaxToolKit:TabPanel ID="pnlBoostProduct" runat="server">
                    <HeaderTemplate><asp:Localize ID="AddOns" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleProductLevel%>'></asp:Localize></HeaderTemplate>
                    <ContentTemplate>
                        <h4 class="SubTitle"><asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleProductLevelSettings%>'></asp:Localize>	
                        </h4>
                        <p>
                            <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextProductLevel%>'></asp:Localize>
                        </p>
                        <h4 class="SubTitle"><asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchProducts%>'></asp:Localize></h4>
                        <uc1:ProductSelect runat="server" ID="ucProductSelect_ProductBoost" />
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlBoostCategory" runat="server">
                    <HeaderTemplate><asp:Localize ID="Localize4" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleCategoryLevel%>'></asp:Localize></HeaderTemplate>
                    <ContentTemplate>
                        <h4 class="SubTitle"><asp:Localize ID="Localize5" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCategoryLevelSettings%>'></asp:Localize>
                        </h4>
                        <p>
                            <asp:Localize ID="TextCategoryLevel" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCategoryLevel%>'></asp:Localize>
                        </p>
                        <h4 class="SubTitle"><asp:Localize ID="SubTitleSearchProducts" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchProducts%>'></asp:Localize>
                        </h4>
                        <uc3:ProductCategorySelect runat="server" ID="ucProductCategorySelect_ProductCategoryBoost" />
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
                <ajaxToolKit:TabPanel ID="pnlBoostField" runat="server">
                    <HeaderTemplate><asp:Localize ID="Localize6" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleFieldLevel%>'></asp:Localize></HeaderTemplate>
                    <ContentTemplate>
                        <h4 class="SubTitle"><asp:Localize ID="SubTitleFieldLevelSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleFieldLevelSettings%>'></asp:Localize>
                        </h4>
                        <p>
                           <asp:Localize ID="TextFieldLevel" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextFieldLevel%>'></asp:Localize>
                        </p>

                        <uc4:FieldBoostSelect runat="server" ID="FieldBoostSelect" />
                    </ContentTemplate>
                </ajaxToolKit:TabPanel>
            </ajaxToolKit:TabContainer>
        </div>
    </div>
</asp:Content>
