<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.Other.StoreLocator.Default" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 70%;">
            <h1 class="LeftFloat"><asp:Localize runat="server" ID="StoreLocator" Text='<%$ Resources:ZnodeAdminResource, LinkTextStoreLocator %>'></asp:Localize></h1>
            <div class="tooltip ToolTipLeftAlign">
                <a href="javascript:void(0);" class="learn-more"><span>
                    <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                <div class="content">
                    <h6><asp:Localize runat="server" ID="Help" Text='<%$ Resources:ZnodeAdminResource, HintHelp %>'></asp:Localize></h6>
                    <p>
                      <asp:Localize runat="server" ID="HelpText" Text='<%$ Resources:ZnodeAdminResource, HelpTextStoreLocator %>'></asp:Localize>
                    </p>
                </div>
            </div>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAddStore" runat="server" CausesValidation="False"
                ButtonType="Button" OnClick="BtnAddStore_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddNewStoreLocation %>'
                ButtonPriority="Primary" />
        </div>
        <div class="ClearBoth">
            <p>
                <asp:Localize runat="server" ID="TextStoreLocator" Text='<%$ Resources:ZnodeAdminResource, TextStoreLocator %>'></asp:Localize>
            </p>
        </div>
        <h4 class="SubTitle"> <asp:Localize runat="server" ID="SearchStore" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchStore %>'></asp:Localize></h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize runat="server" ID="StoreName" Text='<%$ Resources:ZnodeAdminResource, TextStoreName %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtstorename" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize runat="server" ID="ZipCode" Text='<%$ Resources:ZnodeAdminResource, SubTextZipCode %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtzipcode" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize runat="server" ID="City" Text='<%$ Resources:ZnodeAdminResource, SubTextCity %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtcity" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize runat="server" ID="State" Text='<%$ Resources:ZnodeAdminResource, SubTextState %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtstate" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="ClearBoth">

              <zn:Button ID="btnClear" runat="server" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False"/>
              <zn:Button ID="btnSearch" runat="server" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch%>'  />

                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle"><asp:Localize runat="server" ID="StoreList" Text='<%$ Resources:ZnodeAdminResource, GridTitleStoreList %>'></asp:Localize></h4>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" EnableSortingAndPagingCallbacks="False"
            PageSize="25" AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyDataStore %>'>
            <Columns>
                <asp:BoundField DataField="StoreID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText="Store Name" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='Add.aspx?itemid=<%# DataBinder.Eval(Container.DataItem, "StoreId").ToString()%>'>
                            <%# Eval("Name") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="City" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, SubTextCity %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="State" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, SubTextState %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="Zip" HeaderText='<%$ Resources:ZnodeAdminResource, SubTextZipCode %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsActive %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleActions %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <div class="LeftFloat" style="width: 40%; text-align: left">
                            <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("StoreId") %>'
                                CommandName="Manage" Text='<%$ Resources:ZnodeAdminResource, LinkManage %>' CssClass="LinkButton" />
                        </div>
                        <div class="LeftFloat" style="width: 50%; text-align: left">
                            <asp:LinkButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("StoreId") %>'
                                CommandName="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CssClass="LinkButton" />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>
