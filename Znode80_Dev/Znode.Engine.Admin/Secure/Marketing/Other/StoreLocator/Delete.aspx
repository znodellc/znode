<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Marketing.Other.StoreLocator.Delete" CodeBehind="Delete.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h5>
        <asp:Label ID="deletestore" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleDeleteStore %>'></asp:Label>
    </h5>
    <p>
        <asp:Localize runat="server" ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, DeleteConfirmationStore %>'></asp:Localize> "<b><%=StoreName%></b>".
    </p>
    <p>
        <asp:Label ID="lblErrorMsg" runat="server" Visible="False" CssClass="Error"></asp:Label>
    </p>
              <zn:Button ID="btnDelete" runat="server" ButtonType="CancelButton" OnClick="BtnDelete_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>' />
              <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />

    <br />
    <br />
    <br />
</asp:Content>
