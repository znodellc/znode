<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Marketing.Other.StoreLocator.Add" CodeBehind="Add.aspx.cs" %>

<%@ Register Src="~/Controls/Default/StoreName.ascx" TagName="StoreName" TagPrefix="ZNode" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right">
              <zn:Button ID="btnSubmitTop" runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
              <zn:Button ID="btnCancelTop" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
        </div>
        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle"><asp:Localize runat="server" ID="GeneralInformation" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralInformation %>'></asp:Localize>
        </h4>
        <div class="FieldStyle"  style="display:none">
           <asp:Localize runat="server" ID="SelectAccountID" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectAccountID %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle" style="display:none">
            <asp:DropDownList ID="ListAccounts" runat="server" Width="141px">
            </asp:DropDownList>
        </div>
        <div>
            <ZNode:StoreName ID="uxStoreName" runat="server" VisibleLocaleDropdown="false"/>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="StoreName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStoreName %>'></asp:Localize><span class="Asterix">*</span><br />
            <small><asp:Localize runat="server" ID="TextStoreName" Text='<%$ Resources:ZnodeAdminResource, CoulmnTextStoreName %>'></asp:Localize></small>

        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtstorename" runat="server" MaxLength="30" Columns="25"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtstorename"
                CssClass="Error" Display="Dynamic" ErrorMessage="* Add Store Name"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
           <asp:Localize runat="server" ID="AddressLine1" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleAddressLine1 %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtaddress1" runat="server" MaxLength="35" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
           <asp:Localize runat="server" ID="AddressLine2" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleAddressLine2 %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtaddress2" runat="server" MaxLength="35" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
           <asp:Localize runat="server" ID="AddressLine3" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleAddressLine3 %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtaddress3" runat="server" MaxLength="35" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="City" Text='<%$ Resources:ZnodeAdminResource, ColumnCity %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtcity" runat="server" MaxLength="15" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
             <asp:Localize runat="server" ID="StateProvince" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStateProvince %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtstate" runat="server" MaxLength="15" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
           <asp:Localize runat="server" ID="ZipPostalCode" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleZipPostalCode %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtzip" runat="server" MaxLength="15" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
           <asp:Localize runat="server" ID="PhoneNumber" Text='<%$ Resources:ZnodeAdminResource, ColumnPhoneNumber %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtphone" runat="server" MaxLength="25" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="FaxNumber" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFaxNumber %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtfax" runat="server" MaxLength="25" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
          <asp:Localize runat="server" ID="ContactName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleContactName %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtcname" runat="server" MaxLength="30" Columns="25"></asp:TextBox>
        </div>
        <div class="FieldStyle">
             <asp:Localize runat="server" ID="DisplayOrder" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>'></asp:Localize><span class="Asterix">*</span><br />
            <small><asp:Localize runat="server" ID="TextDisplayOrder" Text='<%$ Resources:ZnodeAdminResource, TextDisplayOrder %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtdisplayorder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrder %>' ControlToValidate="txtdisplayorder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtdisplayorder"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeDisplayOrder %>' MaximumValue="999999999"
                MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
           <asp:Localize runat="server" ID="DisplayStore" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayStore %>'></asp:Localize><br />
            <small><asp:Localize runat="server" ID="TextDisplayStore" Text='<%$ Resources:ZnodeAdminResource, TextDisplayStore %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkActiveInd" runat="server" Checked="true" Text='<%$ Resources:ZnodeAdminResource, CheckTextDisplayStore %>'/>
        </div>
        <h4 class="SubTitle">
            <asp:Localize runat="server" ID="TitleTextStoreImage" Text='<%$ Resources:ZnodeAdminResource, TitleStoreImage %>'></asp:Localize><br />
        </h4>
        <small> <asp:Localize runat="server" ID="TextStoreImage" Text='<%$ Resources:ZnodeAdminResource, TextStoreImage %>'></asp:Localize></small>
        <asp:Panel ID="ImagePanel" runat="server">
            <div id="tblShowImage" runat="server" visible="false" style="margin: 10px;">
                <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;">
                    <asp:Image ID="StoreImage" runat="server" />
                </div>
                <div class="LeftFloat" style="padding-left: 10px; margin-left: -1px; border-left: solid 1px #cccccc;">
                    <asp:Panel runat="server" ID="pnlShowOption">
                        <div>
                           <asp:Localize runat="server" ID="SelectAnOption" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectanOption %>'></asp:Localize>
                        </div>
                        <div>
                            <asp:RadioButton ID="RadioStoreCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioTextKeepCurrentImage %>' runat="server"
                                GroupName="Store Image" AutoPostBack="True" OnCheckedChanged="RadioStoreCurrentImage_CheckedChanged" Checked="True" /><br />
                            <asp:RadioButton ID="RadioStoreNewImage" Text='<%$ Resources:ZnodeAdminResource, RadioTextUploadNewImage %>' runat="server" GroupName="Store Image"
                                AutoPostBack="True" OnCheckedChanged="RadioStoreNewImage_CheckedChanged" /><br />
                            <asp:RadioButton ID="RadioStoreNoImage" Text='<%$ Resources:ZnodeAdminResource, RadioTextNoImage %>' runat="server" GroupName="Store Image"
                                AutoPostBack="True" OnCheckedChanged="RadioStoreNoImage_CheckedChanged" />
                        </div>
                    </asp:Panel>
                    <br />
                    <div id="tblStoreDescription" runat="server" visible="false">
                        <div class="FieldStyle">
                           <asp:Localize runat="server" ID="SelectImage" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectImage1 %>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:FileUpload ID="UploadStoreImage" runat="server" BorderStyle="Inset" EnableViewState="true" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="UploadStoreImage"
                                CssClass="Error" Display="dynamic" ValidationExpression=".*(\.[Jj][Pp][Gg]|\.[Gg][Ii][Ff]|\.[Jj][Pp][Ee][Gg]|\.[Pp][Nn][Gg])"
                                ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidImage %>'></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="ClearBoth">
                        <asp:Label ID="lblStoreImageError" runat="server" CssClass="Error" ForeColor="Red"
                            Text="" Visible="False"></asp:Label>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <div>
            <asp:Label ID="lblError" runat="server" Visible="true"></asp:Label>
        </div>
        <div class="ClearBoth">
        </div>
        <br />
        <div>
          
              <zn:Button ID="btnSubmitBottom" runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
              <zn:Button ID="btnCancelBottom" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />

        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="30" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>
