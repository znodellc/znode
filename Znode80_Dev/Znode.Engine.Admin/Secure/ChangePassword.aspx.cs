using System;
using System.Web;
using System.Web.Security;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.ECommerce.UserAccount;

namespace  Znode.Engine.Admin.Secure
{
    /// <summary>
    /// Represents the Change password in site admin
    /// </summary>
    public partial class ChangePassword : System.Web.UI.Page
    {
        #region Private Member Variables
        private string _RedirectUrl = "~/Secure/Default.aspx";         
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Redirect URL
        /// </summary>
        protected string RedirectUrl
        {
            get { return this._RedirectUrl; }
            set { this._RedirectUrl = value; }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["Mode"] != null)
            {
                this.RedirectUrl = "~/Secure/";
            }

            if (!Page.IsPostBack)
            {
                // Retrieve the information from the database
                MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            }
        }

        /// <summary>
        /// Continue Push Button Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ContinuePushButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.RedirectUrl);
        }

        /// <summary>
        /// Cancel Push Button Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CancelPushButton_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.RedirectUrl);
        }

        /// <summary>
        /// Change password error event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChangePassword1_ChangePasswordError(object sender, EventArgs e)
        {
            (AdminChangePassword.ChangePasswordTemplateContainer.FindControl("PasswordFailureText") as Literal).Text =this.GetGlobalResourceObject("ZnodeAdminResource", "TextPasswordFailure").ToString();
                
        }

        /// <summary>
        /// Admin Change password event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AdminChangePassword_ChangingPassword(object sender, LoginCancelEventArgs e)
        {
            e.Cancel = true;
            this.ChangePasswrd();
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Custom method to updates the password
        /// </summary>
        protected void ChangePasswrd()
        {
            var user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            var passwordQuestion = (AdminChangePassword.ChangePasswordTemplateContainer.FindControl("ddlSecretQuestions") as DropDownList).SelectedItem.Text;
            var passwordAnswer = (AdminChangePassword.ChangePasswordTemplateContainer.FindControl("Answer") as TextBox).Text.Trim();


            if (user != null)
            {
                // verify if the new password specified by the user is in the list of the last 4 passwords used.
                bool ret = ZNodeUserAccount.VerifyNewPassword((Guid)user.ProviderUserKey, AdminChangePassword.NewPassword);

                if (!ret)
                {
                    (AdminChangePassword.ChangePasswordTemplateContainer.FindControl("PasswordFailureText") as Literal).Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextPreviousPasswordFailure").ToString();
                    return;
                }

                // Updates the password for this user
                if (user.ChangePassword(AdminChangePassword.CurrentPassword, AdminChangePassword.NewPassword))
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordChangeSuccess, HttpContext.Current.User.Identity.Name, Request.UserHostAddress.ToString(), null, this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogIncorrectPassword").ToString(), null);

                    // Log password for further debugging
                    ZNodeUserAccount.LogPassword((Guid)user.ProviderUserKey, AdminChangePassword.NewPassword);

                    // Log Activity
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogChangePassword").ToString() + HttpContext.Current.User.Identity.Name, HttpContext.Current.User.Identity.Name);

                    if (passwordAnswer.Length > 0)
                    {
                        // Updates the password question and answer for this User
                        user.ChangePasswordQuestionAndAnswer(AdminChangePassword.NewPassword, passwordQuestion, passwordAnswer);
                    }

                    // Redirect to account page
                    Response.Redirect("~/Secure/Default.aspx");
                }
                else
                {
                    // Display Error message
                    (AdminChangePassword.ChangePasswordTemplateContainer.FindControl("PasswordFailureText") as Literal).Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextPasswordFailure").ToString();
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.PasswordChangeFailed, HttpContext.Current.User.Identity.Name, Request.UserHostAddress.ToString(), null, this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogIncorrectPassword").ToString(), null);
                }
            }
        }
        #endregion
    }
}