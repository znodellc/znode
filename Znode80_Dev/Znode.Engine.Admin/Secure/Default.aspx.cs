using System;
using System.Diagnostics;
using System.Web;
using System.Web.Security;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin
{
    /// <summary>
    /// Represents the Default page in the Site Admin
    /// </summary>
    public partial class Admin_Secure_Default : System.Web.UI.Page
    {
        #region Private and Protected Variables
        private DashboardAdmin _DashAdmin = new DashboardAdmin();
        private string _DaysToExpire = "0";
        private string _MultifrontUrl = string.Empty;
        #endregion

        #region Protected properties
        /// <summary>
        /// Gets or sets the Dashboard admin
        /// </summary>
        public DashboardAdmin DashAdmin
        {
            get { return this._DashAdmin; }
            set { this._DashAdmin = value; }
        }

        /// <summary>
        /// Gets or sets the days to expire
        /// </summary>
        public string DaysToExpire
        {
            get { return this._DaysToExpire; }
            set { this._DaysToExpire = value; }
        }

        /// <summary>
        /// Gets or sets the multi front URL
        /// </summary>
        public string MultifrontUrl
        {
            get { return this._MultifrontUrl; }
            set { this._MultifrontUrl = value; }
        }
        #endregion

        #region Public and Protected Methods
        /// <summary>
        /// Get the product Version
        /// </summary>
        /// <returns>Returns the product version</returns>
        public string GetProductVersion()
        {
            try
            {
                string path = ZNode.Libraries.Framework.Business.ZNodeConfigManager.EnvironmentConfig.ApplicationPath + "/Bin/ZNode.Libraries.Framework.Business.dll";

                // Create Instance for FileVersionInfo object
                FileVersionInfo info = FileVersionInfo.GetVersionInfo(Server.MapPath(path));
                if (info != null)
                {
                    return info.FileVersion;
                }
                else
                {
                    return string.Empty;
                }
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["SelectedMenuItem"] = null;
            this.BindData();
        }

        /// <summary>
        /// Bind data to the dashboard
        /// </summary>
        protected void BindData()
        {
            ZNodeUserAccount userAcct = ZNodeUserAccount.CurrentAccount();

            MembershipUser _user = Membership.GetUser(userAcct.UserID);

            if (_user != null)
            {
                string roleList = string.Empty;

                // Get roles for this User account
                string[] roles = Roles.GetRolesForUser(_user.UserName);

                foreach (string Role in roles)
                {
                    roleList += Role + ",";
                }

                try
                {
                    inventoryalert.Visible = true;
                    activityalert.Visible = true;
                                    
                    this.DashAdmin.GetDashboardItems(ZNodeConfigManager.SiteConfig.PortalID);
                    inventoryText.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource","TextLowInventory").ToString() , DashAdmin.TotalLowInventoryItems.ToString());
                    loginText.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextFailedLogin").ToString(), DashAdmin.TotalFailedLoginsToday.ToString());
                    YTDSales.Text = DashAdmin.YTDRevenue.ToString("C2");
                    YTDOrders.Text = DashAdmin.TotalOrders.ToString();
                    YTDAccountsCreated.Text = DashAdmin.TotalAccounts.ToString();

                    if (DashAdmin.TotalLowInventoryItems == 0)
                    {
                        inventoryalert.Visible = false;
                    }
                    if (DashAdmin.TotalFailedLoginsToday == 0)
                    {
                        activityalert.Visible = false;
                    }
                    if (DashAdmin.TotalLowInventoryItems == 0 && DashAdmin.TotalFailedLoginsToday == 0)
                    {
                        lblAlertError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextNoAlert").ToString();
                        lblAlertError.Visible = true;
                    }
                }
                catch
                {
                    // non critical - ignore        
                }
            }

            // Set the IFrame to https if we are on a secure connection.
            string prefix = "http://";

            if (Request.IsSecureConnection)
            {
                prefix = "https://";
            }

            // get multifront path
            this.MultifrontUrl = prefix + HttpContext.Current.Request.ServerVariables.Get("HTTP_HOST") + HttpContext.Current.Request.ApplicationPath;
        }

        /// <summary>
        /// Concate Firstname, Lastname and UserRole
        /// </summary>
        /// <returns>Returns the concat name</returns>
        protected string ConcatName()
        {
            ZNodeUserAccount userAcct = ZNodeUserAccount.CurrentAccount();
            MembershipUser _user = Membership.GetUser(userAcct.UserID);
            string[] roles = Roles.GetRolesForUser(_user.UserName);
            string roleList = string.Empty;

            foreach (string Role in roles)
            {
                roleList += " " + Role + ",";
            }

            roleList = " " + roleList.TrimEnd(',');

            string rolename = roleList;

            MembershipUser user = Membership.GetUser(HttpContext.Current.User.Identity.Name);
            TimeSpan span = System.DateTime.Now.Subtract(user.LastPasswordChangedDate);
            this.DaysToExpire = (60 - span.Days).ToString();

            return this.GetGlobalResourceObject("ZnodeAdminResource", "TitleWelcome") + "<span> <b>" + userAcct.FirstName + " " + userAcct.LastName + "</b>.</span>" + this.GetGlobalResourceObject("ZnodeAdminResource", "TextLogged") + "<span> <b>" + rolename + "</b>.</span>";
        }
        #endregion
    }
}