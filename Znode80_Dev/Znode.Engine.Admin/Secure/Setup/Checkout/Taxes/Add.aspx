<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Checkout.Taxes.Add" ValidateRequest="false" Title="Manage Taxes - Add" CodeBehind="Add.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="TaxClassAdd" Src="~/Controls/Default/Providers/Taxes/TaxClassAdd.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:TaxClassAdd ID="ctlTaxClassAdd" runat="server"
		DefaultPageUrl="~/Secure/Setup/Checkout/Taxes/Default.aspx"
		ViewPageUrl="~/Secure/Setup/Checkout/Taxes/View.aspx"
		AddRulePageUrl="~/Secure/Setup/Checkout/Taxes/AddRule.aspx"
	/>
</asp:Content>
