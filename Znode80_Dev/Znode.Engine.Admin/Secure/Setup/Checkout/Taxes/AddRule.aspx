<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Checkout.Taxes.AddRule" ValidateRequest="false" Title="Manage Taxes - Add Rule" CodeBehind="AddRule.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="TaxRuleAdd" Src="~/Controls/Default/Providers/Taxes/TaxRuleAdd.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:TaxRuleAdd ID="ctlTaxRuleAdd" runat="server" ViewPageUrl="~/Secure/Setup/Checkout/Taxes/View.aspx" />
</asp:Content>
