﻿<%@ Page Language="C#" Title="Manage Taxes - Delete" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="true" CodeBehind="DeleteRule.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Setup.Checkout.Taxes.DeleteRule" %>
<%@ Register TagPrefix="zn" TagName="TaxRuleDelete" Src="~/Controls/Default/Providers/Taxes/TaxRuleDelete.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
	<zn:TaxRuleDelete ID="ctlTaxRuleDelete" runat="server" RedirectUrl="~/Secure/Setup/Checkout/Taxes/View.aspx" />
</asp:Content>
