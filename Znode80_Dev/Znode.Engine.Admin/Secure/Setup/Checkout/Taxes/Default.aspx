<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Checkout.Taxes.Default" Title="Manage Taxes - List" CodeBehind="Default.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="TaxClassList" Src="~/Controls/Default/Providers/Taxes/TaxClassList.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:TaxClassList ID="ctlTaxClassList" runat="server"
		ViewPageUrl="~/Secure/Setup/Checkout/Taxes/View.aspx"
		EditPageUrl="~/Secure/Setup/Checkout/Taxes/Add.aspx"
		DeletePageUrl="~/Secure/Setup/Checkout/Taxes/Delete.aspx"
	/>
</asp:Content>
