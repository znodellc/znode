<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Checkout.Taxes.View" ValidateRequest="false" Title="Manage Taxes - View" CodeBehind="View.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="TaxClassDetails" Src="~/Controls/Default/Providers/Taxes/TaxClassDetails.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:TaxClassDetails ID="ctlTaxClassDetails" runat="server"
		DefaultPageUrl="~/Secure/Setup/Checkout/Taxes/Default.aspx"
		EditPageUrl="~/Secure/Setup/Checkout/Taxes/Add.aspx"
		AddRulePageUrl="~/Secure/Setup/Checkout/Taxes/AddRule.aspx"
		EditRulePageUrl="~/Secure/Setup/Checkout/Taxes/AddRule.aspx"
		DeleteRulePageUrl="~/Secure/Setup/Checkout/Taxes/DeleteRule.aspx"
	/>
</asp:Content>
