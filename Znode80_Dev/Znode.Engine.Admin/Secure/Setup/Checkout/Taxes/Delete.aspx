﻿<%@ Page Language="C#" Title="Manage Taxes - Delete" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="true" CodeBehind="Delete.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Setup.Checkout.Taxes.Delete" %>
<%@ Register TagPrefix="zn" TagName="TaxClassDelete" Src="~/Controls/Default/Providers/Taxes/TaxClassDelete.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
	<zn:TaxClassDelete ID="ctlTaxClassDelete" runat="server" RedirectUrl="~/Secure/Setup/Checkout/Taxes/Default.aspx" />
</asp:Content>
