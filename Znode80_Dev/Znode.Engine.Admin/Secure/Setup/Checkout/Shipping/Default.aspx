<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Checkout.Shipping.Default" Title="Manage Shipping - List" CodeBehind="Default.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="ShippingOptionList" Src="~/Controls/Default/Providers/Shipping/ShippingOptionList.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:ShippingOptionList ID="ctlShippingOptionList" runat="server"
		ViewPageUrl="~/Secure/Setup/Checkout/Shipping/View.aspx"
		AddPageUrl="~/Secure/Setup/Checkout/Shipping/Add.aspx"
		EditPageUrl="~/Secure/Setup/Checkout/Shipping/Add.aspx"
		DeletePageUrl="~/Secure/Setup/Checkout/Shipping/Delete.aspx"
	/>
</asp:Content>
