<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Checkout.Shipping.Delete" Title="Manage Shipping - Delete" CodeBehind="Delete.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="ShippingOptionDelete" Src="~/Controls/Default/Providers/Shipping/ShippingOptionDelete.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:ShippingOptionDelete ID="ctlShippingOptionDelete" runat="server" RedirectUrl="~/Secure/Setup/Checkout/Shipping/Default.aspx" />
</asp:Content>
