<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Setup.Checkout.Shipping.View" Title="Untitled Page" Codebehind="View.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="ShippingOptionDetails" Src="~/Controls/Default/Providers/Shipping/ShippingOptionDetails.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:ShippingOptionDetails ID="ctlShippingOptionDetails" runat="server"
		ListPageUrl="~/Secure/Setup/Checkout/Shipping/Default.aspx"
		EditPageUrl="~/Secure/Setup/Checkout/Shipping/Add.aspx"
		AddRulePageUrl="~/Secure/Setup/Checkout/Shipping/AddRule.aspx"
		EditRulePageUrl="~/Secure/Setup/Checkout/Shipping/AddRule.aspx"
		DeleteRulePageUrl="~/Secure/Setup/Checkout/Shipping/DeleteRule.aspx"
	/>
</asp:Content>
