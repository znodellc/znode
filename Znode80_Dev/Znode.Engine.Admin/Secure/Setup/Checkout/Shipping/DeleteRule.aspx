﻿<%@ Page Language="C#" Title="Manage Shipping - Delete" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="true" CodeBehind="DeleteRule.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Setup.Checkout.Shipping.DeleteRule" %>
<%@ Register TagPrefix="zn" TagName="ShippingRuleDelete" Src="~/Controls/Default/Providers/Shipping/ShippingRuleDelete.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
	<zn:ShippingRuleDelete ID="ctlShippingRuleDelete" runat="server" RedirectUrl="~/Secure/Setup/Checkout/Shipping/View.aspx" />
</asp:Content>
