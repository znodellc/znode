<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Setup.Checkout.Shipping.Add" Title="Manage Shipping - Add" CodeBehind="Add.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="ShippingOptionAdd" Src="~/Controls/Default/Providers/Shipping/ShippingOptionAdd.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:ShippingOptionAdd ID="ctlShippingOptionAdd" runat="server"
		ListPageUrl="~/Secure/Setup/Checkout/Shipping/Default.aspx"
		ViewPageUrl="~/Secure/Setup/Checkout/Shipping/View.aspx"
		AddRulePageUrl="~/Secure/Setup/Checkout/Shipping/AddRule.aspx"
	/>
</asp:Content>
