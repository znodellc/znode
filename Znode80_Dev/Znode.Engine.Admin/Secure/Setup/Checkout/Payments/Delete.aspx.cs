using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Setup.Checkout.Payments
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Setup.Storefront.Payments - Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        #endregion

        #region Events
        /// <summary>
        /// Page Load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Checkout/Payments/Default.aspx");
        }

        /// <summary>
        /// Delete button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            PaymentSetting _pmtSettings = storeAdmin.GetPaymentSettingByID(this.ItemId);

            PaymentTypeService _pmtTypeService = new PaymentTypeService();
            PaymentType _pmtType = new PaymentType();
            _pmtType = _pmtTypeService.GetByPaymentTypeID(_pmtSettings.PaymentTypeID);
            _pmtTypeService.DeepLoad(_pmtType);

            string AssociateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeletePaymentOption").ToString() + _pmtType.Name;
            bool retval = false;
            try
            {
                retval = storeAdmin.DeletePaymentSetting(this.ItemId);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, _pmtType.Name);
            }
            catch
            {
            }

            if (!retval)
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteAction").ToString();
                lblMsg.CssClass = "Error";
            }
            else
            {
                Response.Redirect("~/Secure/Setup/Checkout/Payments/Default.aspx");
            }
        }
        #endregion
    }
}