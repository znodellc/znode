<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Checkout.Payments.Default" Title="Manage Payments" CodeBehind="Default.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
	<div class="Form">
		<div class="LeftFloat" style="width: 70%; text-align: left">
			<h1>
				<asp:Localize ID="TitlePaymentOptions" runat="server" Text="<%$ Resources:ZnodeAdminResource, TitlePaymentOptions %>" />
			</h1>
		</div>

		<div class="ButtonStyle">
			<zn:LinkButton ID="btnAdd" runat="server" CausesValidation="False" ButtonType="Button" OnClick="BtnAdd_Click" Text="<%$ Resources:ZnodeAdminResource, LinkAddNewPayment %>" ButtonPriority="Primary" />
		</div>

		<p class="ClearBoth">
			<asp:Localize ID="TextPayment" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextPayment %>" />
		</p>

		<div>
			<zn:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server" />
		</div>

		<h4 class="GridTitle">
			<asp:Localize ID="GridTitlePaymentOptionsList" runat="server" Text="<%$ Resources:ZnodeAdminResource, GridTitlePaymentOptionsList %>" />
		</h4>

		<asp:GridView ID="uxGrid" runat="server"
			AllowPaging="True"
			AllowSorting="True"
			AutoGenerateColumns="False"
			CaptionAlign="Left"
			CellPadding="4"
			CssClass="Grid"
			EmptyDataText="<%$ Resources:ZnodeAdminResource, GridEmptyRecordText %>"
			GridLines="None"
			OnPageIndexChanging="UxGrid_PageIndexChanging"
			OnRowCommand="UxGrid_RowCommand"
			OnRowDeleting="UxGrid_RowDeleting"
			PageSize="25"
			Width="100%">
			<Columns>
				<asp:BoundField DataField="PaymentSettingID" HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleID %>" HeaderStyle-HorizontalAlign="Left" />
				<asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource, ColumnPaymentOption %>" HeaderStyle-HorizontalAlign="Left">
					<ItemTemplate>
						<a href='add.aspx?itemid=<%# DataBinder.Eval(Container.DataItem, "PaymentSettingID").ToString()%>'>
							<%# DataBinder.Eval(Container.DataItem, "PaymentTypeName").ToString()%>
						</a>
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="ProfileName" HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleProfilesName %>" HeaderStyle-HorizontalAlign="Left" />
				<asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleEnabled %>" HeaderStyle-HorizontalAlign="Left">
					<ItemTemplate>
						<img runat="server" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField DataField="DisplayOrder" HeaderText="<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder %>" HeaderStyle-HorizontalAlign="Left" />
				<asp:TemplateField HeaderText="<%$ Resources:ZnodeAdminResource, ColumnTitleActions %>" HeaderStyle-HorizontalAlign="Left">
					<ItemTemplate>
						<div class="LeftFloat" style="width: 40%; text-align: left">
							<asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("PaymentSettingID") %>' CommandName="Edit" Text="<%$ Resources:ZnodeAdminResource, LinkEdit %>" CssClass="LinkButton" />
						</div>
						<div class="LeftFloat" style="width: 50%; text-align: left">
							<asp:LinkButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("PaymentSettingID") %>' CommandName="Delete" Text="<%$ Resources:ZnodeAdminResource, LinkDelete %>" CssClass="LinkButton" />
						</div>
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
			<HeaderStyle CssClass="HeaderStyle" />
			<FooterStyle CssClass="FooterStyle" />
			<RowStyle CssClass="RowStyle" />
			<AlternatingRowStyle CssClass="AlternatingRowStyle" />
			<PagerStyle CssClass="PagerStyle" Font-Underline="True" />
			<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
		</asp:GridView>

		<div>
			<zn:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server" />
		</div>
	</div>
</asp:Content>
