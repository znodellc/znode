<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Checkout.Payments.Add" ValidateRequest="false" Title="Manage Shipping - Add" CodeBehind="Add.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="Spacer" Src="~/Controls/Default/Spacer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
	<div class="FormView Size350">
		<div class="LeftFloat" style="width: 70%; text-align: left">
			<h1>
				<asp:Label ID="lblTitle" runat="server" />
			</h1>
		</div>

		<div style="text-align: right">
			<zn:Button ID="btnSubmitTop" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
			<zn:Button ID="btnCancelTop" runat="server" ButtonType="CancelButton" CausesValidation="False" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel %>" />
		</div>

		<div class="ClearBoth">
			<asp:Label ID="lblMsg" CssClass="Error" runat="server" />
		</div>

		<div>
			<div>
				<zn:Spacer ID="Spacer1" SpacerHeight="15" SpacerWidth="3" runat="server" />
			</div>

			<h4 class="SubTitle">
				<asp:Localize ID="SubTitleGeneralSettings" runat="server" Text="<%$ Resources:ZnodeAdminResource, SubTitleGeneralSettings %>" />
			</h4>

			<div class="FieldStyle">
				<asp:Localize ID="ColumnTitleSelectProfiles" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleSelectProfiles %>" /><span class="Asterix">*</span>
			</div>

			<div class="ValueStyle">
				<asp:DropDownList ID="ddlProfile" runat="server" />
			</div>

			<div class="FieldStyle">
				<asp:Localize ID="ColumnTitleSelectPayMentType" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleSelectPayMentType %>" /><span class="Asterix">*</span>
			</div>

			<div class="ValueStyle">
				<asp:DropDownList ID="ddlPaymentType" runat="server" OnSelectedIndexChanged="ddlPaymentType_SelectedIndexChanged" AutoPostBack="true" />
			</div>

			<div class="GoogleCheckout" ID="googleNote" runat="server" visible="false">
				<p class="ClearBoth">
					<asp:Localize ID="TextGoogleCheckout" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextGoogleCheckout %>" />
				</p>
			</div>

			<div class="FieldStyle">
				<asp:Localize ID="ColumnTitleDisplaySettings" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleDisplaySetting %>" />
			</div>

			<div class="ValueStyle">
				<asp:CheckBox ID="chkActiveInd" runat="server" Checked="true" Text="<%$ Resources:ZnodeAdminResource, CheckboxEnablePaymentOption %>" />
			</div>

			<div class="FieldStyle">
				<asp:Localize ID="ColumnDisplayOrder" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder %>" /><span class="Asterix">*</span>
				<br />
				<small>
					<asp:Localize ID="ColumnTextAddOnDisplayOrder" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTextAddOnDisplayOrder %>" />
				</small>
			</div>

			<div class="ValueStyle">
				<asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="9" Columns="5" />
				<asp:RequiredFieldValidator ID="rfvDisplayOrder" runat="server" ControlToValidate="txtDisplayOrder" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredDisplayOrderwithStar %>" />
				<asp:RangeValidator ID="rgvDisplayOrder" runat="server" ControlToValidate="txtDisplayOrder" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>" MinimumValue="1" MaximumValue="999999999" Type="Integer" />
			</div>

			<asp:Panel ID="pnlCreditCard" runat="server" Visible="false">
				<h4 class="SubTitle">
					<asp:Localize ID="SubTitleMerchantGateWaySettings" runat="server" Text="<%$ Resources:ZnodeAdminResource, SubTitleMerchantGateWaySettings %>" />
				</h4>

				<asp:Panel ID="pnlGatewayList" runat="server" Visible="true">
					<div class="FieldStyle">
						<asp:Localize ID="ColumnTitleSelectGateWay" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleSelectGateWay %>" /><span class="Asterix">*</span>
					</div>

					<div class="ValueStyle">
						<asp:DropDownList ID="ddlGateway" runat="server" OnSelectedIndexChanged="ddlGateway_SelectedIndexChanged" AutoPostBack="true" />
					</div>
				</asp:Panel>

				<asp:Panel ID="pnlLogin" runat="server" Visible="true">
					<div class="FieldStyle">
						<asp:Label ID="lblGatewayUserName" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextMerchantLogin %>" /><span class="Asterix">*</span>
					</div>

					<div class="ValueStyle">
						<asp:TextBox ID="txtGatewayUserName" runat="server" MaxLength="50" Columns="50" />
						<asp:RequiredFieldValidator ID="rfvGatewayUserName" runat="server" ControlToValidate="txtGatewayUserName" Display="Dynamic" CssClass="Error" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredEnterMerchantLogin %>" SetFocusOnError="True" />
					</div>
				</asp:Panel>

				<asp:Panel ID="pnlPassword" runat="server" Visible="true">
					<div class="FieldStyle">
						<asp:Label ID="lblGatewayPassword" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleMerchangeAccountPassword %>" /><span class="Asterix">*</span>
					</div>

					<div class="ValueStyle" style="font-weight: bold">
						<asp:TextBox ID="txtGatewayPassword" runat="server" MaxLength="50" Columns="50" TextMode="Password" />
						<asp:RequiredFieldValidator ID="rfvGatewayPassword" runat="server" ControlToValidate="txtGatewayPassword" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredMerchantPassword %>" SetFocusOnError="True" />
					</div>
				</asp:Panel>

				<asp:Panel ID="pnlAuthorizeNet" runat="server" Visible="false">
					<div class="FieldStyle">
						<asp:Label ID="lblTransactionKey" runat="server">
							<asp:Localize ID="ColumnTitleTransactionKey" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleTransactionKey %>" />
						</asp:Label>
					</div>

					<div class="ValueStyle">
						<asp:TextBox ID="txtTransactionKey" runat="server" MaxLength="70" Columns="50" />
					</div>
				</asp:Panel>

				<asp:Panel ID="pnlVerisignGateway" runat="server" Visible="false">
					<div class="FieldStyle">
						<asp:Label ID="lblPartner" runat="server">
							<asp:Localize ID="ColumnTitlesPartner" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitlesPartner %>" />
						</asp:Label>
						<span class="Asterix">*</span>
						<br />
						<small>
							<asp:Localize ID="HintTextPartner" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintTextPartner %>" />
						</small>
					</div>

					<div class="ValueStyle">
						<asp:TextBox ID="txtPartner" runat="server" MaxLength="70" Columns="50" />
						<asp:RequiredFieldValidator ID="rfvPartner" runat="server" ControlToValidate="txtPartner" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequirPartnerName %>" SetFocusOnError="True" />
					</div>

					<div class="FieldStyle">
						<asp:Label ID="lblVendor" runat="server">
							<asp:Localize ID="ColumnTitleVendor" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleVendor %>" />
						</asp:Label>
						<span class="Asterix">*</span>
						<br />
						<small>
							<asp:Localize ID="HintTextVendor" runat="server" Text="<%$ Resources:ZnodeAdminResource, HintTextVendor %>" />
						</small>
					</div>

					<div class="ValueStyle">
						<asp:TextBox ID="txtVendor" runat="server" MaxLength="70" Columns="50" />
						<asp:RequiredFieldValidator ID="rfvVendor" runat="server" ControlToValidate="txtVendor" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, RequiredVendorName %>" SetFocusOnError="True" />
					</div>
				</asp:Panel>

				<asp:Panel ID="pnlRMA" runat="server" Visible="true">
					<div class="FieldStyle">
						<asp:Localize ID="ColumnTitleReturnMerchandiseAuthorization" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleReturnMerchandiseAuthorization %>" />
						<br />
						<small>
							<asp:Localize ID="TextRMARefunds" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextRMARefunds %>" />
						</small>
					</div>

					<div class="ValueStyle">
						<asp:CheckBox ID="chkRMA" runat="server" Checked="false" Text="<%$ Resources:ZnodeAdminResource, CheckBoxEnableRMA %>" />
					</div>
				</asp:Panel>

				<asp:Panel ID="pnlTestMode" runat="server">
					<div class="FieldStyle">
						<asp:Localize ID="ColumnTitleGatewayTESTmode" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleGatewayTESTmode %>" />
					</div>

					<div class="ValueStyle">
						<asp:CheckBox ID="chkTestMode" runat="server" Checked="false" Text="<%$ Resources:ZnodeAdminResource, TextEnableTestMode %>" />
					</div>
				</asp:Panel>

				<asp:Panel ID="pnlGatewayOptions" runat="server" Visible="false">
					<div class="FieldStyle">
						<asp:Localize ID="ColumnTitleCreditCardAuthorization" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleCreditCardAuthorization %>" />
					</div>
					<div class="ValueStyle">
						<asp:CheckBox ID="chkPreAuthorize" runat="server" Checked="false" Text="<%$ Resources:ZnodeAdminResource, CheckBoxPreAuthorize %>" />
					</div>
				</asp:Panel>

				<asp:Panel ID="pnlCreditCardOptions" runat="server">
					<div class="FieldStyle">
						<asp:Localize ID="Localize14" runat="server" Text="<%$ Resources:ZnodeAdminResource, ColumnTitleAcceptedCards %>" />
					</div>

					<div class="ValueStyle">
						<asp:CheckBox ID="chkEnableVisa" runat="server" Checked="true" Text="<%$ Resources:ZnodeAdminResource, CheckBoxVisa %>" />
						&nbsp;&nbsp;
						<asp:CheckBox ID="chkEnableMasterCard" runat="server" Checked="true" Text="<%$ Resources:ZnodeAdminResource, CheckBoxMasterCard %>" />
						&nbsp;&nbsp;
						<asp:CheckBox ID="chkEnableAmex" runat="server" Checked="true" Text="<%$ Resources:ZnodeAdminResource, CheckBoxAmericanExpress %>" />
						&nbsp;&nbsp;
						<asp:CheckBox ID="chkEnableDiscover" runat="server" Checked="true" Text="<%$ Resources:ZnodeAdminResource, CheckBoxDiscover %>" />
					</div>
				</asp:Panel>

				<asp:Panel ID="pnl2COGateway" runat="server" Visible="false">
					<div class="FieldStyle">
						<asp:Label ID="Label1" runat="server" Text="<%$ Resources:ZnodeAdminResource, TextAdditionalFee %>" />
					</div>

					<div class="ValueStyle">
						<asp:TextBox ID="txtAdditionalFee" runat="server" MaxLength="11" Columns="50" />
						<asp:RangeValidator ID="rgvAdditionalFee" runat="server" ControlToValidate="txtAdditionalFee" Display="Dynamic" ErrorMessage="<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>" MinimumValue="0" MaximumValue="999999999" Type="Double" />
					</div>
				</asp:Panel>
			</asp:Panel>

			<div class="ClearBoth"></div>

			<div>
				<zn:Button ID="btnSubmitBottom" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
				<zn:Button ID="btnCancelBottom" runat="server" ButtonType="CancelButton" CausesValidation="False" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel %>" />
			</div>
		</div>
	</div>
</asp:Content>
