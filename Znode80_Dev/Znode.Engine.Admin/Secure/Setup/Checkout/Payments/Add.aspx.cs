using System;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;
using DataEntities = ZNode.Libraries.DataAccess.Entities;
using EcomEntities = ZNode.Libraries.ECommerce.Entities;

namespace Znode.Engine.Admin.Secure.Setup.Checkout.Payments
{
	public partial class Add : System.Web.UI.Page
	{
		private readonly StoreSettingsAdmin _storeAdmin = new StoreSettingsAdmin();
		private readonly ZNodeEncryption _encryption = new ZNodeEncryption();

		private int _itemId;

		protected void Page_Load(object sender, EventArgs e)
		{
			_itemId = Request.Params["itemid"] != null ? int.Parse(Request.Params["itemid"]) : 0;

			if (Page.IsPostBack == false)
			{
				if (_itemId > 0)
				{
					lblTitle.Text = GetGlobalResourceObject("ZnodeAdminResource", "TitleEditPaymentOption").ToString();
				}
				else
				{
					lblTitle.Text = GetGlobalResourceObject("ZnodeAdminResource", "TitleAddPaymentOption").ToString();
				}

				BindData();
			}
		}

		private void BindData()
		{
			txtDisplayOrder.Text = "1";

			// Get profiles
			ddlProfile.DataSource = UserStoreAccess.CheckProfileAccess(_storeAdmin.GetProfiles());
			ddlProfile.DataTextField = "Name";
			ddlProfile.DataValueField = "ProfileID";
			ddlProfile.DataBind();
			ddlProfile.Items.Insert(0, new ListItem(GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAllProfiles").ToString(), "-1"));
			ddlProfile.SelectedValue = "-1";

			// Get payment types
			ddlPaymentType.DataSource = _storeAdmin.GetPaymentTypes();
			ddlPaymentType.DataTextField = "Name";
			ddlPaymentType.DataValueField = "PaymentTypeID";
			ddlPaymentType.DataBind();
			ddlPaymentType.SelectedIndex = 0;

			// Get gateways
			ddlGateway.DataSource = _storeAdmin.GetGateways();
			ddlGateway.DataTextField = "GatewayName";
			ddlGateway.DataValueField = "GatewayTypeID";
			ddlGateway.DataBind();
			ddlGateway.SelectedIndex = 0;

			var orderAdmin = new OrderAdmin();
			var ordersCount = orderAdmin.GetTotalOrderItemsByPaymentId(_itemId);

			if (ordersCount > 0)
			{
				ddlGateway.Enabled = false;
				ddlPaymentType.Enabled = false;
				ddlProfile.Enabled = false;
			}

			try
			{
				if (_itemId > 0)
				{
					// Get payment setting
					var paymentSetting = _storeAdmin.GetPaymentSettingByID(_itemId);

					txtDisplayOrder.Text = paymentSetting.DisplayOrder.ToString();

					if (paymentSetting.ProfileID.HasValue)
					{
						ddlProfile.SelectedValue = paymentSetting.ProfileID.Value.ToString();
					}

					ddlPaymentType.SelectedValue = paymentSetting.PaymentTypeID.ToString();
					chkActiveInd.Checked = paymentSetting.ActiveInd;

					var paymentType = (EcomEntities.PaymentType)Enum.Parse(typeof(EcomEntities.PaymentType), paymentSetting.PaymentTypeID.ToString());
					switch (paymentType)
					{
						case EcomEntities.PaymentType.CREDIT_CARD:
							pnlCreditCard.Visible = true;

							txtGatewayUserName.Text = _encryption.DecryptData(paymentSetting.GatewayUsername);
							ddlGateway.SelectedValue = paymentSetting.GatewayTypeID.ToString();

							if (paymentSetting.IsRMACompatible.HasValue)
							{
								chkRMA.Checked = paymentSetting.IsRMACompatible.Value;
							}

							var gatewayType = (GatewayType)Enum.Parse(typeof(GatewayType), ddlGateway.SelectedValue);
							switch (gatewayType)
							{
								case GatewayType.AUTHORIZE:
									lblTransactionKey.Text = GetGlobalResourceObject("ZnodeAdminResource", "ColumnTitleTransactionKey").ToString();
									txtTransactionKey.Text = _encryption.DecryptData(paymentSetting.TransactionKey);
									pnlAuthorizeNet.Visible = true;
									pnlPassword.Visible = false;
									chkTestMode.Checked = paymentSetting.TestMode;
									chkPreAuthorize.Checked = paymentSetting.PreAuthorize;
									pnlGatewayOptions.Visible = true;
									break;
								case GatewayType.VERISIGN:
									txtVendor.Text = paymentSetting.Vendor;
									txtGatewayPassword.Text = _encryption.DecryptData(paymentSetting.GatewayPassword);
									txtPartner.Text = paymentSetting.Partner;
									pnlPassword.Visible = true;
									pnlVerisignGateway.Visible = true;
									chkTestMode.Checked = paymentSetting.TestMode;
									pnlGatewayOptions.Visible = true;
									break;
								case GatewayType.PAYPAL:
									txtTransactionKey.Text = paymentSetting.TransactionKey;
									txtGatewayPassword.Text = _encryption.DecryptData(paymentSetting.GatewayPassword);
									lblTransactionKey.Text = GetGlobalResourceObject("ZnodeAdminResource", "TextTransactionkey").ToString();
									pnlAuthorizeNet.Visible = true;
									pnlPassword.Visible = true;
									chkTestMode.Checked = paymentSetting.TestMode;
									break;
								case GatewayType.WORLDPAY:
									txtGatewayPassword.Text = _encryption.DecryptData(paymentSetting.GatewayPassword);
									txtTransactionKey.Text = _encryption.DecryptData(paymentSetting.TransactionKey);
									lblGatewayUserName.Text = GetGlobalResourceObject("ZnodeAdminResource", "TextMerchantCode").ToString();
									lblGatewayPassword.Text = GetGlobalResourceObject("ZnodeAdminResource", "TextAuthorizationPassword").ToString();
									lblTransactionKey.Text = GetGlobalResourceObject("ZnodeAdminResource", "TextWorldPayInstallationId").ToString();
									pnlAuthorizeNet.Visible = true;
									pnlPassword.Visible = true;
									chkTestMode.Checked = paymentSetting.TestMode;
									break;
								case GatewayType.CYBERSOURCE:
									pnlLogin.Visible = false;
									pnlAuthorizeNet.Visible = false;
									pnlPassword.Visible = false;
									pnlVerisignGateway.Visible = false;
									pnlGatewayOptions.Visible = true;
									pnlTestMode.Visible = true;
									chkTestMode.Checked = paymentSetting.TestMode;
									chkPreAuthorize.Checked = paymentSetting.PreAuthorize;
									break;
								default:
									pnlAuthorizeNet.Visible = false;
									pnlPassword.Visible = true;
									txtGatewayPassword.Text = _encryption.DecryptData(paymentSetting.GatewayPassword);
									txtTransactionKey.Text = String.Empty;
									chkTestMode.Checked = paymentSetting.TestMode;
									break;
							}

							ddlGateway.SelectedValue = paymentSetting.GatewayTypeID.ToString();
							chkEnableAmex.Checked = (bool)paymentSetting.EnableAmex;
							chkEnableDiscover.Checked = (bool)paymentSetting.EnableDiscover;
							chkEnableMasterCard.Checked = (bool)paymentSetting.EnableMasterCard;
							chkEnableVisa.Checked = (bool)paymentSetting.EnableVisa;
							pnl2COGateway.Visible = false;
							break;
						case EcomEntities.PaymentType.PAYPAL:
							pnlCreditCard.Visible = true;
							txtGatewayUserName.Text = _encryption.DecryptData(paymentSetting.GatewayUsername);
							txtTransactionKey.Text = paymentSetting.TransactionKey;
							txtGatewayPassword.Text = _encryption.DecryptData(paymentSetting.GatewayPassword);
							lblTransactionKey.Text = GetGlobalResourceObject("ZnodeAdminResource", "TextAPISignature").ToString();
							pnlAuthorizeNet.Visible = true;
							pnlPassword.Visible = true;
							pnlGatewayList.Visible = false;
							pnlCreditCardOptions.Visible = false;
							chkTestMode.Checked = paymentSetting.TestMode;
							pnl2COGateway.Visible = false;
							break;
						case EcomEntities.PaymentType.GOOGLE_CHECKOUT:
							txtGatewayUserName.Text = _encryption.DecryptData(paymentSetting.GatewayUsername);
							txtGatewayPassword.Text = _encryption.DecryptData(paymentSetting.GatewayPassword);
							chkTestMode.Checked = paymentSetting.TestMode;
							pnlCreditCardOptions.Visible = false;
							pnlGatewayList.Visible = false;
							pnlCreditCard.Visible = true;
							pnlAuthorizeNet.Visible = false;
							pnlPassword.Visible = true;
							pnl2COGateway.Visible = false;
							break;
						case EcomEntities.PaymentType.TwoCO:
							txtGatewayUserName.Text = _encryption.DecryptData(paymentSetting.GatewayUsername);
							txtGatewayPassword.Attributes.Add("value", _encryption.DecryptData(paymentSetting.GatewayPassword));
							chkTestMode.Checked = paymentSetting.TestMode;
							txtAdditionalFee.Text = _encryption.DecryptData(paymentSetting.Vendor);
							googleNote.Visible = false;
							pnlCreditCardOptions.Visible = false;
							pnlGatewayList.Visible = false;
							pnlCreditCard.Visible = true;
							pnlAuthorizeNet.Visible = false;
							pnlPassword.Visible = true;
							pnl2COGateway.Visible = true;
							lblGatewayUserName.Text = GetGlobalResourceObject("ZnodeAdminResource", "Text2coVendorId").ToString();
							lblGatewayPassword.Text = GetGlobalResourceObject("ZnodeAdminResource", "TextSecretWoesMDSVerification").ToString();
							break;
						default:
							pnlCreditCard.Visible = false;
							break;
					}
				}
				else
				{
					// Enable credit card option by default
					pnlCreditCard.Visible = true;

					ddlGateway.SelectedValue = "1";
					BindPanels();
				}
			}
			catch
			{
				// Ignore
			}
		}

		protected void BtnSubmit_Click(object sender, EventArgs e)
		{
			var paymentSetting = new DataEntities.PaymentSetting();

			// If edit mode then retrieve data first
			if (_itemId > 0)
			{
				paymentSetting = _storeAdmin.GetPaymentSettingByID(_itemId);

				var settingExists = PaymentSettingExists();
				if (settingExists)
				{
					lblMsg.Text = GetGlobalResourceObject("ZnodeAdminResource", "ErrorPaymentOption").ToString();
					return;
				}
			}

			// Set values based on user input
			paymentSetting.ActiveInd = chkActiveInd.Checked;
			paymentSetting.PaymentTypeID = int.Parse(ddlPaymentType.SelectedValue);

			if (ddlProfile.SelectedValue == "-1")
			{
				// If "All Profiles" is selected
				paymentSetting.ProfileID = null;
			}
			else
			{
				paymentSetting.ProfileID = int.Parse(ddlProfile.SelectedValue);
			}

			paymentSetting.DisplayOrder = Convert.ToInt32(txtDisplayOrder.Text);

			var paymentType = (EcomEntities.PaymentType)Enum.Parse(typeof(EcomEntities.PaymentType), paymentSetting.PaymentTypeID.ToString());
			switch (paymentType)
			{
				case EcomEntities.PaymentType.CREDIT_CARD:
					paymentSetting.GatewayTypeID = int.Parse(ddlGateway.SelectedValue);
					paymentSetting.EnableAmex = chkEnableAmex.Checked;
					paymentSetting.EnableDiscover = chkEnableDiscover.Checked;
					paymentSetting.EnableMasterCard = chkEnableMasterCard.Checked;
					paymentSetting.EnableVisa = chkEnableVisa.Checked;
					paymentSetting.TestMode = chkTestMode.Checked;
					paymentSetting.PreAuthorize = chkPreAuthorize.Checked;
					paymentSetting.GatewayPassword = String.Empty;
					paymentSetting.TransactionKey = String.Empty;
					paymentSetting.IsRMACompatible = chkRMA.Checked;
					paymentSetting.GatewayUsername = _encryption.EncryptData(txtGatewayUserName.Text);

					var gatewayType = (GatewayType)Enum.Parse(typeof(GatewayType), paymentSetting.GatewayTypeID.ToString());
					switch (gatewayType)
					{
						case GatewayType.AUTHORIZE:
							paymentSetting.TransactionKey = _encryption.EncryptData(txtTransactionKey.Text);
							break;
						case GatewayType.VERISIGN:
							paymentSetting.GatewayPassword = _encryption.EncryptData(txtGatewayPassword.Text);
							paymentSetting.Partner = txtPartner.Text.Trim();
							paymentSetting.Vendor = txtVendor.Text.Trim();
							break;
						case GatewayType.PAYPAL:
							paymentSetting.GatewayPassword = _encryption.EncryptData(txtGatewayPassword.Text);
							paymentSetting.TransactionKey = txtTransactionKey.Text;
							break;
						case GatewayType.WORLDPAY:
							paymentSetting.GatewayPassword = _encryption.EncryptData(txtGatewayPassword.Text);
							paymentSetting.TransactionKey = _encryption.EncryptData(txtTransactionKey.Text);
							break;
						default:
							paymentSetting.TransactionKey = String.Empty;
							paymentSetting.GatewayPassword = _encryption.EncryptData(txtGatewayPassword.Text);
							break;
					}
					break;
				case EcomEntities.PaymentType.PAYPAL:
					paymentSetting.GatewayTypeID = null;
					paymentSetting.GatewayUsername = _encryption.EncryptData(txtGatewayUserName.Text);
					paymentSetting.TransactionKey = txtTransactionKey.Text;
					paymentSetting.GatewayPassword = _encryption.EncryptData(txtGatewayPassword.Text);
					paymentSetting.TestMode = chkTestMode.Checked;
					paymentSetting.PreAuthorize = chkPreAuthorize.Checked;
					break;
				case EcomEntities.PaymentType.GOOGLE_CHECKOUT:
					paymentSetting.GatewayTypeID = null;
					paymentSetting.GatewayUsername = _encryption.EncryptData(txtGatewayUserName.Text);
					paymentSetting.GatewayPassword = _encryption.EncryptData(txtGatewayPassword.Text);
					paymentSetting.TestMode = chkTestMode.Checked;
					break;
				case EcomEntities.PaymentType.TwoCO:
					paymentSetting.GatewayTypeID = null;
					paymentSetting.GatewayUsername = _encryption.EncryptData(txtGatewayUserName.Text);
					paymentSetting.GatewayPassword = _encryption.EncryptData(txtGatewayPassword.Text);
					paymentSetting.TestMode = chkTestMode.Checked;
					paymentSetting.Vendor = _encryption.EncryptData(txtAdditionalFee.Text);
					break;
				default:
					// Purchase Order
					paymentSetting.GatewayTypeID = null;
					break;
			}

			var retval = false;

			// Update payment setting in the database
			if (_itemId > 0)
			{
				retval = _storeAdmin.UpdatePaymentSetting(paymentSetting);
				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit Payment Option - " + ddlPaymentType.SelectedItem.Text, ddlPaymentType.SelectedItem.Text);
			}
			else
			{
				bool settingExists;
				if (int.Parse(ddlPaymentType.SelectedValue) == (int)EcomEntities.PaymentType.CREDIT_CARD)
				{
					settingExists = _storeAdmin.PaymentSettingExists(int.Parse(ddlProfile.SelectedValue), int.Parse(ddlPaymentType.SelectedValue), int.Parse(ddlGateway.SelectedValue), 0);
				}
				else
				{
					settingExists = _storeAdmin.PaymentSettingExists(int.Parse(ddlProfile.SelectedValue), int.Parse(ddlPaymentType.SelectedValue));
				}

				if (settingExists)
				{
					lblMsg.Text = GetGlobalResourceObject("ZnodeAdminResource", "ErrorPaymentOption").ToString();
					return;
				}

				retval = _storeAdmin.AddPaymentSetting(paymentSetting);
				ZNodeLogging.LogActivity((int)ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Added Payment Option - " + ddlPaymentType.SelectedItem.Text, ddlPaymentType.SelectedItem.Text);
			}

			if (retval)
			{
				// Redirect to main page
				Response.Redirect("~/Secure/Setup/Checkout/Payments/Default.aspx");
			}
			else
			{
				// Display error message
				lblMsg.Text = GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoteAdd").ToString();
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect("~/Secure/Setup/Checkout/Payments/Default.aspx");
		}

		protected void ddlPaymentType_SelectedIndexChanged(object sender, EventArgs e)
		{
			googleNote.Visible = false;

			var paymentType = (EcomEntities.PaymentType)Enum.Parse(typeof(EcomEntities.PaymentType), ddlPaymentType.SelectedValue);
			switch (paymentType)
			{
				case EcomEntities.PaymentType.CREDIT_CARD:
					pnlCreditCard.Visible = true;
					pnlCreditCardOptions.Visible = true;
					pnlGatewayList.Visible = true;
					pnl2COGateway.Visible = false;
					BindPanels();
					break;
				case EcomEntities.PaymentType.PAYPAL:
					lblTransactionKey.Text = GetGlobalResourceObject("ZnodeAdminResource", "TextAPISignature").ToString();
					pnlAuthorizeNet.Visible = true;
					pnlPassword.Visible = true;
					pnlCreditCard.Visible = true;
					pnlCreditCardOptions.Visible = false;
					pnlGatewayList.Visible = false;
					pnl2COGateway.Visible = false;
					break;
				case EcomEntities.PaymentType.GOOGLE_CHECKOUT:
					googleNote.Visible = true;
					pnlCreditCardOptions.Visible = false;
					pnlGatewayList.Visible = false;
					pnlCreditCard.Visible = true;
					pnlAuthorizeNet.Visible = false;
					pnlPassword.Visible = true;
					pnl2COGateway.Visible = false;
					break;
				case EcomEntities.PaymentType.TwoCO:
					googleNote.Visible = false;
					pnlCreditCardOptions.Visible = false;
					pnlGatewayList.Visible = false;
					pnlCreditCard.Visible = true;
					pnlAuthorizeNet.Visible = false;
					pnlPassword.Visible = true;
					pnl2COGateway.Visible = true;
					lblGatewayUserName.Text = GetGlobalResourceObject("ZnodeAdminResource", "Text2coVendorId").ToString();
					lblGatewayPassword.Text = GetGlobalResourceObject("ZnodeAdminResource", "TextSecretWoesMDSVerification").ToString();
					break;
				default:
					pnlCreditCard.Visible = false;
					break;
			}
		}

		protected void ddlGateway_SelectedIndexChanged(object sender, EventArgs e)
		{
			BindPanels();
		}

		private void BindPanels()
		{
			// Initialize panels
			pnlLogin.Visible = true;
			pnlPassword.Visible = true;
			pnlVerisignGateway.Visible = false;
			pnlAuthorizeNet.Visible = false;
			pnlGatewayOptions.Visible = false;

			var gatewayType = (GatewayType)Enum.Parse(typeof(GatewayType), ddlGateway.SelectedValue);
			switch (gatewayType)
			{
				case GatewayType.AUTHORIZE:
					lblTransactionKey.Text = GetGlobalResourceObject("ZnodeAdminResource", "ColumnTitleTransactionKey").ToString();
					pnlAuthorizeNet.Visible = true;
					pnlLogin.Visible = true;
					pnlPassword.Visible = false;
					pnlGatewayOptions.Visible = true;
					lblGatewayUserName.Text = GetGlobalResourceObject("ZnodeAdminResource", "TextMerchantLogin").ToString();
					lblGatewayPassword.Text = GetGlobalResourceObject("ZnodeAdminResource", "ColumnTitleMerchangeAccountPassword").ToString();
					break;
				case GatewayType.VERISIGN:
					pnlVerisignGateway.Visible = true;
					pnlAuthorizeNet.Visible = false;
					pnlLogin.Visible = true;
					pnlPassword.Visible = true;
					pnlGatewayOptions.Visible = true;
					break;
				case GatewayType.PAYPAL:
					lblTransactionKey.Text = GetGlobalResourceObject("ZnodeAdminResource", "TextPayPalTransactionKey").ToString();
					pnlAuthorizeNet.Visible = true;
					pnlLogin.Visible = true;
					pnlPassword.Visible = true;
					break;
				case GatewayType.CYBERSOURCE:
					pnlLogin.Visible = false;
					pnlAuthorizeNet.Visible = false;
					pnlPassword.Visible = false;
					pnlVerisignGateway.Visible = false;
					pnlGatewayOptions.Visible = true;
					pnlTestMode.Visible = true;
					break;
				case GatewayType.WORLDPAY:
					lblGatewayUserName.Text = GetGlobalResourceObject("ZnodeAdminResource", "TextMerchantCode").ToString();
					lblGatewayPassword.Text = GetGlobalResourceObject("ZnodeAdminResource", "TextAuthorizationPassword").ToString();
					lblTransactionKey.Text = GetGlobalResourceObject("ZnodeAdminResource", "TextWorldPayInstallationId").ToString();
					pnlAuthorizeNet.Visible = true;
					pnlPassword.Visible = true;
					break;
				case GatewayType.CUSTOM:
					pnlTestMode.Visible = false;
					break;
			}
		}

		private bool PaymentSettingExists()
		{
			return _storeAdmin.PaymentSettingExists(int.Parse(ddlProfile.SelectedValue), int.Parse(ddlPaymentType.SelectedValue), int.Parse(ddlGateway.SelectedValue), _itemId);
		}
	}
}