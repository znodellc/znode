using System;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;

namespace Znode.Engine.Admin.Secure.Setup.Checkout.Payments
{
	public partial class Default : System.Web.UI.Page
	{
		private string _addLink = "~/Secure/Setup/Checkout/Payments/Add.aspx";
		private string _editLink = "~/Secure/Setup/Checkout/Payments/Add.aspx";
		private string _deleteLink = "~/Secure/Setup/Checkout/Payments/Delete.aspx";

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Page.IsPostBack)
			{
				BindGridData();
			}
		}

		protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			uxGrid.PageIndex = e.NewPageIndex;
			BindGridData();
		}

		protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
		{
			BindGridData();
		}

		protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName != "page")
			{
				var paymentSettingId = e.CommandArgument.ToString();

				if (e.CommandName == "Edit")
				{
					_editLink = _editLink + "?itemid=" + paymentSettingId;
					Response.Redirect(_editLink);
				}

				 if (e.CommandName == "Delete")
				{
					Response.Redirect(_deleteLink + "?itemid=" + paymentSettingId);
				}
			}
		}

		protected void BtnAdd_Click(object sender, EventArgs e)
		{
			Response.Redirect(_addLink);
		}

		private void BindGridData()
		{
			var settingsAdmin = new StoreSettingsAdmin();
			uxGrid.DataSource = UserStoreAccess.CheckProfileAccess(settingsAdmin.GetAllPaymentSettings().Tables[0], true);
			uxGrid.DataBind();
		}
	}
}