using System;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;

namespace  Znode.Engine.Admin.Secure.Setup.Storefront.Catalogs
{
    /// <summary>
    /// Represents the Catalog Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string _CatalogName = string.Empty;
        private int ItemId;
        private string cancelLink = "~/Secure/Setup/Storefront/Catalogs/Default.aspx";
        private CatalogAdmin catalogAdmin = new CatalogAdmin();
        #endregion

        /// <summary>
        /// Gets or sets the catalog name.
        /// </summary>
        public string CatalogName
        {
            get { return _CatalogName; }
            set { _CatalogName = value; }
        }

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring   
            if (Request.Params["Itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["Itemid"].ToString());
            }
            else
            {
                this.ItemId = 0;
            }

            // To redirect when directly delete the current catalog using query string
            if (ZNodeCatalogManager.CatalogConfig.CatalogID == this.ItemId)
            {
                Response.Redirect(this.cancelLink);
            }

            if (!Page.IsPostBack)
            {
                if (this.ItemId > 0)
                {
                    Catalog catalog = this.catalogAdmin.GetCatalogByCatalogId(this.ItemId);
                    this.CatalogName = catalog.Name;
                }
            }

            DeleteConfirmText.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextConfirmDeleteCatalog").ToString(), CatalogName);
        }
        #endregion

        #region General Events
        /// <summary>
        /// Delete Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            bool Status = false;

            try
            {
				PortalCatalogService portalCatalogService = new PortalCatalogService();
				PortalCatalog portalCatalog = new PortalCatalog();
				TList<PortalCatalog> portalCatalogList = portalCatalogService.GetByCatalogID(this.ItemId);
				if (portalCatalogList.Count == 0)
			    {
		            bool preserve = true;
		            bool.TryParse(rdoPreserve.SelectedValue, out preserve);
		            Status = this.catalogAdmin.DeleteCatalog(this.ItemId, preserve);
	            }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }

            if (Status)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Delete Catalog - " + this.CatalogName, this.CatalogName);
                Response.Redirect(this.cancelLink);
            }
            else
            {
                Catalog catalog = this.catalogAdmin.GetCatalogByCatalogId(this.ItemId);
                this.CatalogName = catalog.Name;
                lblErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorDeleteCatalog").ToString();
            }
        }

        /// <summary>
        /// Cancel Button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.cancelLink);
        }
        #endregion
    }
}