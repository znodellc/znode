<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" Title="Manage Catalogs - Delete" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Catalogs.Delete" CodeBehind="Delete.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h5><asp:Localize ID="PleaseConfirmText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm %>'></asp:Localize></h5>
    <p>
        <asp:label ID="DeleteConfirmText" runat="server"></asp:label>
    </p>
    <p style="font-weight: bold; color: Red"><asp:Localize ID="TextWarningChange" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextWarningChange %>'></asp:Localize></p>
    <asp:Label CssClass="Error" ID="lblErrorMessage" runat="server" Text=''></asp:Label>
    <div>
        <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>
    <div>
        <asp:Localize ID="SelectOption" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextSelectOption %>'></asp:Localize>
        <asp:RadioButtonList runat="server" ID="rdoPreserve">
            <asp:ListItem Value="true" Selected='True' Text='<%$ Resources:ZnodeAdminResource, OptionDeleteCatalog %>'></asp:ListItem>
            <asp:ListItem Value="false" Text='<%$ Resources:ZnodeAdminResource, OptionDeleteAssociatedCategoryProduct %>'></asp:ListItem>
        </asp:RadioButtonList>
    </div>
    <div>
        <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>
    <div>
        <zn:Button runat="server" ID="btnDelete" OnClick="BtnDelete_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>' />
        <zn:Button runat="server" ID="btnCancel" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
    </div>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="25" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>
</asp:Content>
