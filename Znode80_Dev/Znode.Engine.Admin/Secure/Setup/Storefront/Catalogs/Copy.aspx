﻿<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/Themes/Standard/edit.master"
    Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Catalogs.Copy" ValidateRequest="false" Title="Manage Catalogs - Copy" CodeBehind="Copy.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <asp:Panel ID="pnlCatalog" runat="server" DefaultButton="btnSubmit">
        <div>
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <ajaxToolKit:TabContainer ID="tabCatalogSettings" runat="server">
            <ajaxToolKit:TabPanel ID="pnlSettings" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="Settings" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleSettings %>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <div class="FieldStyle">
                        <asp:Localize ID="CatalogName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCatalogName %>'></asp:Localize><span class="Asterix">*</span>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtcatalogName" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                            ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcatalogName"
                            ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredCatalogName %>' CssClass="Error" Display="dynamic" ValidationGroup="grpTitle"></asp:RequiredFieldValidator>
                    </div>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
        </ajaxToolKit:TabContainer>
        <div class="ClearBoth">
            <br />
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div>
            <zn:Button runat="server" ID="btnSubmit" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancel" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
        </div>
    </asp:Panel>
</asp:Content>
