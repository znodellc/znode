﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace  Znode.Engine.Admin.Secure.Setup.Storefront.Catalogs
{
    /// <summary>
    /// Represents the Catalog Add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Member Variables
        private static bool IsSearchEnabled = false;
        private string Link = "~/Secure/Setup/Storefront/Catalogs/Default.aspx";
        private int ItemId = 0;
        private string AssociateName = string.Empty;
        private CatalogService catalogService = new CatalogService();
        private CategoryNodeService categoryNodeService = new CategoryNodeService();

        private List<string> _PreSelectedItems = new List<string>();

        private string EditLink = "~/Secure/Setup/Storefront/Catalogs/AssociateCatagory.aspx?itemid=";

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Pre Selected Items
        /// </summary>
        public List<string> PreSelectedItems
        {
            get
            {
                return this._PreSelectedItems;
            }

            set
            {
                this._PreSelectedItems = value;
            }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        public void BindGridData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            DataSet ds = categoryAdmin.GetCategoriesSearchByCatalog(Server.HtmlEncode(txtCategoryName.Text.Trim()), this.ItemId);
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                dr["Name"] = categoryAdmin.ParsePath(dr["Name"].ToString(), ">");
            }

            ds.Tables[0].AcceptChanges();
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = "DisplayOrder";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId value from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            // Get ItemId value from querystring        
            if (Request.Params["Mode"] != null)
            {
                if (Request.Params["Mode"].ToString().Equals("Dept"))
                {
                    tabCatalogSettings.ActiveTabIndex = 1;
                }
            }

            if (!Page.IsPostBack)
            {
                this.BindEditData();

                if (this.ItemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource","TitleEditCatalog").ToString() + Server.HtmlEncode(txtcatalogName.Text.Trim());
                    pnlDepartment.Visible = true;
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource","TitleAddCatalog").ToString();
                    pnlDepartment.Visible = false;
                }
            }
        }

        #endregion

        #region Bind Data

        protected void BindEditData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            Catalog catalog = new Catalog();

            if (this.ItemId > 0)
            {
                catalog = catalogAdmin.GetCatalogByCatalogId(this.ItemId);

                lblTitle.Text += catalog.Name;
                txtcatalogName.Text = Server.HtmlDecode(catalog.Name);
                this.BindGridData();
            }
        }

        #endregion

        #region General Events - Settings
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            Catalog catalog = new Catalog();

            bool Status;

            // If edit mode then get all the values first
            if (this.ItemId > 0)
            {
                catalog = catalogAdmin.GetCatalogByCatalogId(this.ItemId);
            }

            catalog.Name = Server.HtmlEncode(txtcatalogName.Text);

            catalog.IsActive = true;

            if (this.ItemId > 0)
            {
                Status = catalogAdmin.Update(catalog);
            }
            else
            {
                Status = catalogAdmin.Insert(catalog);
            }

            if (!Status)
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorCatalog").ToString();
                return;
            }
            else
            {
                if (this.ItemId > 0)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogEditCatalog").ToString() + txtcatalogName.Text, txtcatalogName.Text);
                    Response.Redirect(this.Link);
                }
                else
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddCatalog").ToString() + txtcatalogName.Text, txtcatalogName.Text);
                    Response.Redirect("~/Secure/Setup/Storefront/Catalogs/add.aspx?Mode=Dept&itemid=" + catalog.CatalogID);
                }
            }
        }

        /// <summary> 
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.Link);
        }

        /// <summary> 
        /// Add Category Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddCategory_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Storefront/Catalogs/AssociateCatagory.aspx?ItemId=" + this.ItemId + "&name=" + Server.UrlEncode(Server.HtmlEncode(txtcatalogName.Text)));
        }

        #endregion

        #region General Events - Department

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindGridData();
            IsSearchEnabled = true;
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtCategoryName.Text = string.Empty;
            this.BindGridData();
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (IsSearchEnabled)
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindGridData();
            }
            else
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindGridData();
            }
        }

        /// <summary>
        /// Even triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Edit")
                {
                    this.EditLink = this.EditLink + this.ItemId + "&nodeId=" + Id + "&name=" + Server.UrlEncode(txtcatalogName.Text);
                    Response.Redirect(this.EditLink);
                }
                else if (e.CommandName == "Delete")
                {
                    lblMsg.Text = string.Empty;
                    CategoryAdmin categoryAdmin = new CategoryAdmin();

                    if (!categoryAdmin.IsChildCategoryExist(Convert.ToInt32(Id)))
                    {
                        CategoryNode node = new CategoryNode();
                        node.CategoryNodeID = Convert.ToInt32(Id);

                        node = this.categoryNodeService.GetByCategoryNodeID(Convert.ToInt32(Id));
                        this.categoryNodeService.DeepLoad(node);
                        string DepartmentName = node.CategoryIDSource.Name;

                        bool status = categoryAdmin.DeleteNode(node);

                        if (!status)
                        {
                            lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorDeleteCategoryNode").ToString();
                        }
                        else
                        {
                            this.AssociateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "AcitivityLogDeleteAssociationDepartment").ToString(), DepartmentName, txtcatalogName.Text);
                                
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, txtcatalogName.Text);
                        }
                    }
                    else
                    {
                        lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteCategoryChild").ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Event is raised when Back button is clicked
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.Link);
        }

        #endregion
    }
}