<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/popup.master" Title="Manage Catalogs - Department Search" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Catalogs.DepartmentSearch" CodeBehind="DepartmentSearch.aspx.cs" %>

<%@ Register Src="SearchDepartment.ascx" TagName="DeptSearch" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <ZNode:DeptSearch ID="uxDeptSearch" runat="server" />
</asp:Content>
