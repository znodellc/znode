using System;
using System.Globalization;
using System.Web;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the Edit Units
    /// </summary>
    public partial class EditUnits : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int _ItemId = 0;
        private string _RedirectUrl = "view.aspx?ItemId={0}&mode=unit";
        #endregion

        #region Protected Properties
        /// <summary>
        /// Gets or sets the Item Id
        /// </summary>
        protected int ItemId
        {
            get { return this._ItemId; }
            set { this._ItemId = value; }
        }

        /// <summary>
        /// Gets or sets the Redirect URL
        /// </summary>
        protected string RedirectUrl
        {
            get { return this._RedirectUrl; }
            set { this._RedirectUrl = value; }
        }
        #endregion

        #region Page Events
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get portalId value from querystring
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindCurrencyTypes();
                this.Bind();
            }
        }
        #endregion

        #region Protected Events
        /// <summary>
        /// Submit Button Click
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = null;
            bool status = false;

            if (this.ItemId > 0)
            {
                portal = storeAdmin.GetByPortalId(this.ItemId);
            }

            if (portal != null)
            {
                portal.CurrencyTypeID = null;

                if (ddlCurrencyTypes.SelectedIndex != -1)
                {
                    // Currency Settings
                    portal.CurrencyTypeID = int.Parse(ddlCurrencyTypes.SelectedValue);
                }

                // Units 
                portal.WeightUnit = ddlWeightUnits.SelectedItem.Text;
                portal.DimensionUnit = ddlDimensions.SelectedItem.Text;

                status = storeAdmin.Update(portal);
            }

            if (status)
            {
                // Set currency
                CurrencyType currencyType = storeAdmin.GetByCurrencyTypeID(portal.CurrencyTypeID.GetValueOrDefault(0));

                if (currencyType != null)
                {
                    currencyType.CurrencySuffix = Server.HtmlEncode(txtCurrencySuffix.Text.Trim());

                    storeAdmin.UpdateCurrencyType(currencyType);
                }

                // Log Activity
                string associatename = this.GetGlobalResourceObject("ZnodeAdminResource", "LogActivityEditUnit") + portal.StoreName;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associatename, portal.StoreName);
                Response.Redirect(string.Format(this.RedirectUrl, this.ItemId));
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorStoreUnit").ToString();

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeFailed, HttpContext.Current.User.Identity.Name);
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format(this.RedirectUrl, this.ItemId));
        }

        /// <summary>
        /// Currency Type dropdown list selected index changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlCurrencyTypes_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.ShowPriceFormat();
        }

        /// <summary>
        /// Currency Suffix TextBox Change Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void TxtCurrencySuffix_TextChanged(object sender, EventArgs e)
        {
            string val = lblPrice.Text.Trim();

            if (lblPrice.Text.Contains("("))
            {
                val = lblPrice.Text.Remove(lblPrice.Text.IndexOf('('));
            }

            if (txtCurrencySuffix.Text.Trim().Length > 0)
            {
                lblPrice.Text = val + " (" + txtCurrencySuffix.Text.Trim() + ")";
                return;
            }

            lblPrice.Text = val;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Binds the data
        /// </summary>
        private void Bind()
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();

            // Set default Value from Store1 For Add page
            Portal portal = storeAdmin.GetByPortalId(this.ItemId);

            if (portal != null)
            {
                lblTitle.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditUnit").ToString(), portal.StoreName);

                // Currency Type settings
                if (portal.CurrencyTypeID.HasValue)
                {
                    ddlCurrencyTypes.SelectedValue = portal.CurrencyTypeID.Value.ToString();
                }

                // Units
                ddlWeightUnits.SelectedValue = portal.WeightUnit;
                ddlDimensions.SelectedValue = portal.DimensionUnit;

                // Show Example
                this.ShowPriceFormat();
            }
        }

        /// <summary>
        /// Bind Currency Types to dropdown list
        /// </summary>
        private void BindCurrencyTypes()
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            ddlCurrencyTypes.DataSource = storeAdmin.GetAll();
            ddlCurrencyTypes.DataTextField = "Description";
            ddlCurrencyTypes.DataValueField = "CurrencyTypeId";
            ddlCurrencyTypes.DataBind();

            ddlCurrencyTypes.SelectedValue = "107"; // By Default US-English
        }

        /// <summary>
        /// Display Price in specified culture format
        /// </summary>
        private void ShowPriceFormat()
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            CurrencyType _currencyType = storeAdmin.GetByCurrencyTypeID(int.Parse(ddlCurrencyTypes.SelectedValue));
            txtCurrencySuffix.Text = Server.HtmlDecode(_currencyType.CurrencySuffix);
            string currencySymbol = _currencyType.Name;

            CultureInfo info = new CultureInfo(currencySymbol);

            decimal price = 100.12M;

            lblPrice.Text = price.ToString("c", info.NumberFormat);

            if (txtCurrencySuffix.Text.Trim().Length > 0)
            {
                lblPrice.Text += " (" + _currencyType.CurrencySuffix + ")";
            }
        }

        #endregion
    }
}