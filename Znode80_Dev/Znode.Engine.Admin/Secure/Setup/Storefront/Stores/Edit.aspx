<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True"
    ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Stores.Edit"
    Title="Manage Stores - Edit" CodeBehind="Edit.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <script type="text/javascript">
        function HideFileUpload(hideUploadControl) {
            var fu = document.getElementById("<%=tblLogoUpload.ClientID %>");
        var rfvLogo = document.getElementById("<%=rfvStoreLogo.ClientID %>");
        var revFileType = document.getElementById("<%=revFileType.ClientID %>");

        if (fu != null) {
            if (hideUploadControl == true) {
                fu.style.display = '';
                ValidatorEnable(revFileType, true);
                ValidatorEnable(rfvLogo, true);
            }
            else {
                ValidatorEnable(revFileType, false);
                ValidatorEnable(rfvLogo, false);
                fu.style.display = 'none';
            }
        }
    }
    function SetAddressValidationSectionEnabled() {

        var chkEnableAddressValidation = document.getElementById("<%=chkEnableAddressValidation.ClientID %>");
        var chkRequireValidatedAddress = document.getElementById("<%=chkRequireValidatedAddress.ClientID %>");

        if (!chkEnableAddressValidation.checked) {
            chkRequireValidatedAddress.checked = false;
            chkRequireValidatedAddress.disabled = true;
        }
        else {
            chkRequireValidatedAddress.disabled = false;
        }

    }
    </script>

    <div>
        <div class="FormView">
            <div>
                <div class="LeftFloat" style="width: 70%; text-align: left">
                    <h1>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label>
                    </h1>
                </div>
                <div style="text-align: right; padding-right: 10px;">
                    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text="SUBMIT" CausesValidation="True" ID="btnSubmitTop" />
                    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="CANCEL" CausesValidation="False" ID="btnCancelTop"/>
                </div>
                <div align="left" class="ClearBoth">
                    <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
                </div>
                <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
                    ShowMessageBox="False" ShowSummary="False" />
            </div>
            <div>
                <ZNode:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <h4 class="SubTitle"><asp:Localize ID="ColumnTitleStoreIdentity" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStoreIdentity %>'></asp:Localize></h4>
             <div class="FieldStyle">
                <asp:Localize ID="ColumnTitleStoreName" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStoreName %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtStoreName" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtStoreName"
                    ErrorMessage="* Enter Store Name" CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnBrandName" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBrandName %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCompanyName" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCompanyName"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiresBrandName %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            </div>
           
            <div class="FieldStyle" style="display:none;">
                <asp:Localize ID="ColumnTitleLocale" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleLocale %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle" style="display:none;">
                <asp:DropDownList ID="ddlLocaleList" runat="server" Width="155px">
                </asp:DropDownList>
            </div>
            <div class="FieldStyle" visible="false" runat="server">
                 <asp:Localize ID="ColumnTitleCatalog" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCatalog %>'></asp:Localize>
            </div>
            <div class="ValueStyle" visible="false" runat="server">
                <asp:DropDownList ID="ddlCatalog" runat="server">
                </asp:DropDownList>
            </div>
            <asp:Panel ID="pnlThemes" runat="server" Visible="false">
                <div class="FieldStyle" runat="server" visible="false">
                     <asp:Localize ID="ColumnTitleTheme" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTheme %>'></asp:Localize>
                </div>
                <div class="ValueStyle" runat="server" visible="false">
                    <asp:DropDownList ID="ddlThemeslist" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlThemeslist_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlCssList" runat="server" Visible="false">
                <div class="FieldStyle" runat="server" visible="false">
                     <asp:Localize ID="ColumnTitleCSS" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCSS %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlCSSList" runat="server" Visible="false">
                    </asp:DropDownList>
                </div>
            </asp:Panel>
            <div class="ClearBoth">
            </div>
            <div id="tblShowImage" runat="server" style="margin: 10px;">
                <div class="LeftFloat" style="width: 195px; border-right: solid 1px #cccccc;">
                    <asp:Image ID="imgLogo" runat="server" />
                </div>
                <div class="LeftFloat" style="padding-left: 10px; margin-left: -1px; border-left: solid 1px #cccccc;">
                    <asp:Panel runat="server" ID="pnlShowSwatchOption">
                        <div class="FieldStyle"  style="margin-bottom:4px;">
                             <asp:Localize ID="TextOption" runat ="server" Text='<%$ Resources:ZnodeAdminResource, TextOption %>'></asp:Localize>
                        </div>
                        <div class="ClearBoth"></div>
                        <div>
                            <asp:RadioButton ID="radCurrentImage" onclick="return HideFileUpload(false);" OnCheckedChanged="RadCurrentImage_CheckedChanged" Text='<%$ Resources:ZnodeAdminResource, RadioButtonCurrentImage %>' runat="server" GroupName="Product Swatch Image" Checked="True" /><br />
                            <asp:RadioButton ID="radNewImage" onclick="return HideFileUpload(true);" OnCheckedChanged="RadNewImage_CheckedChanged" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNewImage %>' runat="server" GroupName="Product Swatch Image" /><br />
                        </div>
                    </asp:Panel>
                    <br />
                    <div id="tblLogoUpload" runat="server" style="display: none;">
                       <%-- <div class="FieldStyle">
                            Select a Logo<span class="Asterix">*</span>
                        </div>--%>
                        <div class="ValueStyle">
                            <asp:FileUpload ID="UploadImage" runat="server" BorderStyle="Inset" EnableViewState="true" /><br />
                            <div>
                                <asp:RequiredFieldValidator CssClass="Error" ID="rfvStoreLogo" runat="server"
                                    ControlToValidate="UploadImage" Enabled="false" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredUploadImage %>' Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="revFileType" Enabled="false" runat="server" ControlToValidate="UploadImage"
                                    ErrorMessage= '<%$ Resources:ZnodeAdminResource, RegularValidUploadImage %>'
                                    CssClass="Error"
                                    SetFocusOnError="True" ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidExpressionImage %>'
                                    Display="None"> </asp:RegularExpressionValidator>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <div class="ClearBoth">
            </div>
            <br />
            <h4 class="SubTitle"><asp:Localize ID="SubTitleSecurity" runat ="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSecurity %>'></asp:Localize></h4>
            <div class="ValueStyleText">
                <asp:CheckBox ID="chkEnableSSL" runat="server" Text='<%$ Resources:ZnodeAdminResource, CheckboxTextEnableSSL %>' />
            </div>
            <div class="ClearBoth">
            </div>
            <br />
            <h4 class="SubTitle"><asp:Localize ID="SubTitleStoreContactInformation" runat ="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleStoreContactInformation %>'></asp:Localize></h4>
            <p> 
                <asp:Localize ID="TextStoreContactInformation" runat ="server" Text='<%$ Resources:ZnodeAdminResource, TextStoreContactInformation %>'></asp:Localize>
            </p>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnTitleAdministratorEmail" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleAdministratorEmail %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtAdminEmail" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtAdminEmail"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, ReauiredAdminEmail %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="valRegEx" runat="server" ControlToValidate="txtAdminEmail"
                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidEmailIdwithStar %>'
                    Display="dynamic" CssClass="Error"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnTitleSalesDepartmentEmail" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSalesDepartmentEmail %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSalesEmail" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtSalesEmail"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSalesDepartmentEmail %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSalesEmail"
                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidEmailIdwithStar %>'
                    CssClass="Error" Display="dynamic"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnTitleCustomerServiceEmail" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCustomerServiceEmail %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCustomerServiceEmail" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtCustomerServiceEmail"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredCustomerServiceEmail %>'  CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtCustomerServiceEmail"
                    ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*([,;]\s*\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidEmailIdwithStar %>'
                    Display="dynamic" CssClass="Error"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnTitleSalesDeptPhone" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSalesDeptPhone %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSalesPhoneNumber" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtSalesPhoneNumber"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSalesDeptPhone %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnTitleCustomerServicePhone" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCustomerServicePhone %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCustomerServicePhoneNumber" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtCustomerServicePhoneNumber"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredCustomerServicePhone %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer2" runat="server" SpacerHeight="20" SpacerWidth="3" />
            </div>
            <h4 class="SubTitle"><asp:Localize ID="SubTitleDefaultSettings" runat ="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDefaultSettings %>'></asp:Localize></h4>
            <div class="FieldStyle">
                  <asp:Localize ID="ColumnTitleDefaultCusReviewStatus" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDefaultCusReviewStatus %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ListReviewStatus" runat="server">
                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownListPublishImmediately %>' Value="A"></asp:ListItem>
                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextDoNotPublish %>' Value="N" Selected="true"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <div class="FieldStyle">
                         <asp:Localize ID="ColumnTitleDefaultOrderStatus" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDefaultOrderStatus %>'></asp:Localize><br />
                        <small><asp:Localize ID="HintTextWhenDefaultOrder" runat ="server" Text='<%$ Resources:ZnodeAdminResource, HintTextWhenDefaultOrder %>'></asp:Localize>
                        <br />
                           <asp:Localize ID="HintTextDefaultOrder" runat ="server" Text='<%$ Resources:ZnodeAdminResource, HintTextDefaultOrder %>'></asp:Localize><br />
                           <asp:Localize ID="HintTextOrderStatus" runat ="server" Text='<%$ Resources:ZnodeAdminResource, HintTextOrderStatus %>'></asp:Localize></small>
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="ddlOrderStateList" runat="server" Width="160px" />
                        <asp:CheckBox ID="chkPendingApproval" runat="server" Text='<%$ Resources:ZnodeAdminResource, CheckBoxRequireManualApproval %>'
                            AutoPostBack="True" OnCheckedChanged="ChkPendingApproval_CheckedChanged" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="FieldStyle">
                <asp:Localize ID="IncludeTaxesInproductPrice" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleIncludeTaxesInproductPrice %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkInclusiveTax" runat="server" Text="" />
            </div>
            <div class="FieldStyle">
               <asp:Localize ID="ColumnTitleEnablePersistentCart" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEnablePersistentCart %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkPersistentCart" runat="server" Text="" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnTitleEnableAddressValidation" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEnableAddressValidation %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkEnableAddressValidation" runat="server" onclick="SetAddressValidationSectionEnabled()" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="Localize23" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleRequireValidatedAddress %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkRequireValidatedAddress" runat="server" />
            </div>
			<div class="FieldStyle">
                 <asp:Localize ID="Localize24" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEnableCBP %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="CheckEnableCustomerBasedPricing" runat="server" />
            </div>
            <%--<div class="FieldStyle">
                Enable PIMS
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkEnablePIMS" runat="server" />
            </div>--%>
            <div class="FieldStyle">
               <asp:Localize ID="ProductReviewStatus" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductReviewStatus %>'></asp:Localize>
                <br />
                <small style="width: 153px;"><asp:Localize ID="ReviewStatus" runat ="server" Text='<%$ Resources:ZnodeAdminResource, HindTextReviewStatus %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlProductReviewStateID" runat="server" Width="160px" />
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit%>" CausesValidation="True" ID="btnSubmitBottom" />
                <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel%>" CausesValidation="False" ID="btnCancelBottom"/>
            </div>
        </div>
    </div>
</asp:Content>
