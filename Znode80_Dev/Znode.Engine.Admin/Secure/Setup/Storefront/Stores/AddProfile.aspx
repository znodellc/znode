<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/Themes/Standard/edit.master" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Stores.AddProfile" CodeBehind="AddProfile.aspx.cs" Title="Manage Stores - Add Profile" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1><asp:Localize ID="TitleAddProfilesToStore" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleAddProfilesToStore%>'></asp:Localize><asp:Label ID="lblTitle" runat="server" /></h1>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ButtonType="EditButton" Width="100px" OnClick="BtnBack_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonBack%>' ID="btnBack" />
        </div>
        <div class="ClearBoth">
        </div>

        <!-- Profile List -->
        <asp:Panel ID="pnlProfileList" runat="server">
            <div class="FieldStyle">
                <asp:Localize ID="ColumnTitleSelectProfiles" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectProfiles%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlProfileList" runat="server" />
            </div>
            <div class="FieldStyle"></div>
            <div class="ValueStyleText">
                <asp:CheckBox ID="chkIsDefaultAnonymous" runat="server" Checked="false" Text='<%$ Resources:ZnodeAdminResource, CheckBoxTextDefaultAnonymous%>'></asp:CheckBox>
            </div>

            <div class="FieldStyle"></div>
            <div class="ValueStyleText">
                <asp:CheckBox ID="chkIsDefaultRegistered" runat="server" Checked="false" Text='<%$ Resources:ZnodeAdminResource, CheckBoxTextDefaultRegistered%>' /></asp:CheckBox>
            </div>
            <div>
                <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                    ForeColor="red"></asp:Label>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer2" SpacerHeight="0" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <zn:Button runat="server" ButtonType="SubmitButton" OnClick="Submit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmitBottom" CausesValidation="true" />
                <zn:Button runat="server" ButtonType="CancelButton" OnClick="Cancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelBottom" />
            </div>
        </asp:Panel>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
    </div>
</asp:Content>
