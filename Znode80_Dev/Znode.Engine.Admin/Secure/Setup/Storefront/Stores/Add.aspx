<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Stores.Add" ValidateRequest="false" Title="Manage Stores - Add"
    CodeBehind="Add.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">

    <script type="text/javascript">
        function SetAddressValidationSectionEnabled() {

            var chkEnableAddressValidation = document.getElementById("<%=chkEnableAddressValidation.ClientID %>");
            var chkRequireValidatedAddress = document.getElementById("<%=chkRequireValidatedAddress.ClientID %>");

            if (!chkEnableAddressValidation.checked) {
                chkRequireValidatedAddress.checked = false;
                chkRequireValidatedAddress.disabled = true;
            }
            else {
                chkRequireValidatedAddress.disabled = false;
            }

        }
    </script>
    <div>
        <asp:Label ID="lblMessage" runat="server" CssClass="Error"></asp:Label>
    </div>
    <div class="FormView" id="StoreContent" runat="server">
        <div>
            <div class="LeftFloat" style="width: 50%; text-align: left">
                <h1>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h1>
            </div>
            <div style="text-align: right; padding-right: 10px;">
               <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit%>" CausesValidation="True" ID="btnSubmitTop" />
               <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel%>" CausesValidation="False" ID="btnCancelTop"  />
            </div>
            <div align="left">
                <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
            </div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
                ShowMessageBox="False" ShowSummary="False" />
        </div>
        <div class="ClearBoth">
            <h4 class="SubTitle"><asp:Localize ID="SubTitleStoreIdentity" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleStoreIdentity%>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnBrandName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBrandName%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCompanyName" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtCompanyName"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredBrandName%>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
               <asp:Localize ID="ColumnStoreName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStoreName%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtStoreName" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtStoreName"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterStoreName%>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle" runat="server">
                <asp:Localize ID="ColumnCatalog" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCatalog%>'></asp:Localize>
            </div>
            <div class="ValueStyle" runat="server">
                <asp:DropDownList ID="ddlCatalog" runat="server" AutoPostBack="true">
                </asp:DropDownList>
            </div>
            <asp:Panel ID="pnlThemes" runat="server">
                <div class="FieldStyle" runat="server">
                    <asp:Localize ID="ColumnTheme" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTheme%>'></asp:Localize>
                </div>
                <div class="ValueStyle" runat="server">
                    <asp:DropDownList ID="ddlThemeslist" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlThemeslist_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlCssList" runat="server" Visible="false">
                <div class="FieldStyle" runat="server">
                    <asp:Localize ID="ColumnCSS" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCSS%>'></asp:Localize>
                </div>
                <div class="ValueStyle" runat="server">
                    <asp:DropDownList ID="ddlCSSList" runat="server">
                    </asp:DropDownList>
                </div>
            </asp:Panel>
            <div class="FieldStyle"> 
                <asp:Label ID="lblLocale" runat="server" Text="Locale" Visible="false" ></asp:Label>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlLocale" runat="server" Visible="false" />
            </div>
            <asp:Panel ID="tblShowImage" runat="server" Visible="true" Width="100%">
                <div>
                    <asp:Image ID="imgLogo" runat="server" />
                </div>
                <div class="FieldStyle">
                   <asp:Localize ID="TextOption" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextOption%>'></asp:Localize>
                </div>
                &nbsp;
                <asp:RadioButton ID="radCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonCurrentImage%>' runat="server" GroupName="LogoImage"
                    AutoPostBack="True" OnCheckedChanged="RadCurrentImage_CheckedChanged" Checked="True" />
                <asp:RadioButton ID="radNewImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNewImage%>' runat="server" GroupName="LogoImage"
                    AutoPostBack="True" OnCheckedChanged="RadNewImage_CheckedChanged" />
            </asp:Panel>
            <asp:Panel ID="tblLogoUpload" Width="100%" runat="server" Visible="false">
                <div class="FieldStyle">
                   <asp:Localize ID="ColumnSelectLogo" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectLogo%>'></asp:Localize><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:FileUpload ID="UploadImage" runat="server" Width="100%" />
                    <asp:RequiredFieldValidator CssClass="Error" ID="RequiredFieldValidator14" runat="server"
                        ControlToValidate="UploadImage" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSelectImage%>' Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:Label ID="lblImageError" runat="server" CssClass="Error" ForeColor="Red" Text='<%$ Resources:ZnodeAdminResource, ValidSelectImage%>'
                        Visible="False"></asp:Label>
                </div>
            </asp:Panel>
            <h4 class="SubTitle"><asp:Localize ID="SubTitleSecurity" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSecurity%>'></asp:Localize></h4>

            <div class="ValueStyleText">
                <asp:CheckBox ID="chkEnableSSL" runat="server" Text='<%$ Resources:ZnodeAdminResource, CheckboxTextEnableSSL%>' />
            </div>
            <div class="ClearBoth">
            </div>
            <br />
            <h4 class="SubTitle"><asp:Localize ID="SubTitleStoreContact" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleStoreContact%>'></asp:Localize></h4>
            <p>
                <asp:Localize ID="SubTextStoreContact" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextStoreContact%>'></asp:Localize>
            </p>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnAdministratorEmail" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAdministratorEmail%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtAdminEmail" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtAdminEmail"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredAdministratorEmail%>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="valRegEx" runat="server" ControlToValidate="txtAdminEmail"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidEmailAddress%>'
                    Display="dynamic" CssClass="Error"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnSalesDepartmentEmail" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSalesDepartmentEmail%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSalesEmail" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtSalesEmail"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSalesEmail%>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSalesEmail"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidEmailAddress%>'
                    CssClass="Error" Display="dynamic"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnCustomerServiceEmail" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCustomerServiceEmail%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCustomerServiceEmail" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtCustomerServiceEmail"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredCustomerServiceEmail%>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtCustomerServiceEmail"
                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidEmailAddress%>'
                    Display="dynamic" CssClass="Error"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnSalesDepartmentPhoneNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSalesDepartmentPhoneNumber%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSalesPhoneNumber" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtSalesPhoneNumber"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSalesPhoneNumber%>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnCustomerServicePhoneNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCustomerServicePhoneNumber%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtCustomerServicePhoneNumber" runat="server" Width="152px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                    ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtCustomerServicePhoneNumber"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredCustomerServicePhoneNumber%>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            </div>
            <h4 class="SubTitle"><asp:Localize ID="SubTitleDefaultSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDefaultSettings%>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="SubTextCustomerReviewStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextCustomerReviewStatus%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ListReviewStatus" runat="server">
                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextPublishImmediately%>' Value="A"></asp:ListItem>
                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextDoNotPublish%>' Value="N" Selected="true"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <div class="FieldStyle">
                        <asp:Localize ID="ColumnDefaultOrderStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDefaultOrderStatus%>'></asp:Localize><br />
                        <small><asp:Localize ID="ColumnTextDefaultOrderStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextDefaultOrderStatus%>'></asp:Localize></small>
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="ddlOrderStateList" runat="server" Width="160px" />
                        <asp:CheckBox ID="chkPendingApproval" runat="server" Text='<%$ Resources:ZnodeAdminResource, CheckBoxRequireManualApproval%>'
                            AutoPostBack="True" OnCheckedChanged="ChkPendingApproval_CheckedChanged" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnIncludeTaxProductPrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnIncludeTaxProductPrice%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkInclusiveTax" runat="server" Text="" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnEnablePersistentCart" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnablePersistentCart%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkPersistentCart" runat="server" Text="" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnEnableAddressValidation" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableAddressValidation%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkEnableAddressValidation" runat="server" onclick="SetAddressValidationSectionEnabled()" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRequireValidatedAddress%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkRequireValidatedAddress" runat="server" />
            </div>
         <%--   <div class="FieldStyle">
                Enable PIMS
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkEnablePIMS" runat="server" />
            </div>--%>
			
			<div class="FieldStyle">
                 <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableCustomerBasedPricing%>'></asp:Localize>
            </div>
			  <div class="ValueStyle">
                <asp:CheckBox ID="CheckEnableCustomerBasedPricing" runat="server" />
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDefaultProductReviewStatus%>'></asp:Localize>
                <br />
                <small style="width: 153px;"> <asp:Localize ID="Localize4" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextDefaultProductReviewStatus%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlProductReviewStateID" runat="server" Width="160px" />
            </div>
            <div class="ClearBoth">
                <br />
                <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="True" ID="btnSubmitBottom" />
                <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelBottom"/>
            </div>
        </div>
    </div>
</asp:Content>
