using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the SiteAdmin Admin_Secure_settings_Stores_Countries class
    /// </summary>
    public partial class Countries : System.Web.UI.Page
    {
        #region Private Member Variables
        private int ItemId = 0;
        private string StorePage = "~/Secure/Setup/Storefront/Stores/View.aspx?itemid=";
        #endregion

        #region Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindSearchCountry();
            }
        }

        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Add New Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddNew_Click(object sender, EventArgs e)
        {
            PortalCountryAdmin _portalCountryAdmin = new PortalCountryAdmin();
            bool status = true;

            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = storeAdmin.GetByPortalId(this.ItemId);
            StringBuilder countryName = new StringBuilder();

            // Loop through the grid values
            foreach (GridViewRow row in uxCountryGrid.Rows)
            {
                CheckBox BillingActiveSelected = (CheckBox)row.Cells[1].FindControl("chkBillingActive") as CheckBox;
                CheckBox ShippingActiveSelected = (CheckBox)row.Cells[2].FindControl("chkShippingActive") as CheckBox;

                if (BillingActiveSelected.Checked || ShippingActiveSelected.Checked)
                {
                    // Get Code
                    string Code = row.Cells[3].Text;
                    string Name = row.Cells[4].Text;

                    // PortalCountry Entity
                    PortalCountry portalCountry = new PortalCountry();
                    portalCountry.PortalID = this.ItemId;
                    portalCountry.CountryCode = Code;
                    portalCountry.BillingActive = BillingActiveSelected.Checked;
                    portalCountry.ShippingActive = ShippingActiveSelected.Checked;
                    countryName.Append(Name + ",");
                    status &= _portalCountryAdmin.Insert(portalCountry);
                }
            }

            if (countryName.Length != 0)
            {
                countryName.Remove(countryName.Length - 1, 1);

                // Log Activity
                string associatename = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddedCountries").ToString(), countryName, portal.StoreName);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associatename, portal.StoreName);
                Response.Redirect(this.StorePage + this.ItemId + "&mode=countries");
            }
            else
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSelectCountry").ToString();
                lblError.Visible = true;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.StorePage + this.ItemId + "&mode=countries");
        }

        /// <summary>
        /// Search Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchCountry();
        }

        /// <summary>
        /// Clear Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("Countries.aspx?itemid=" + this.ItemId);
        }

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.StorePage + this.ItemId + "&mode=countries");
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Search a Product by name,productnumber,sku,manufacturer,category,producttype
        /// </summary>
        private void BindSearchCountry()
        {
            StoreSettingsHelper storeSettingsHelper = new StoreSettingsHelper();
            DataSet ds = storeSettingsHelper.SearchCountryByCodeAndName(txtkeyword.Text.Trim().Replace(" ", "%"));
            DataColumn[] keys = new DataColumn[1] { ds.Tables[0].Columns["Code"] };

            ds.Tables[0].PrimaryKey = keys;
            DataRow dataRowDelete = ds.Tables[0].Rows.Find("US");

            DataRow dataRowInsert = null;

            if (dataRowDelete != null)
            {
                dataRowInsert = ds.Tables[0].NewRow();
                dataRowInsert.ItemArray = dataRowDelete.ItemArray;
                dataRowDelete.Delete();
            }

            if (dataRowInsert != null)
            {
                dataRowInsert["EnableCountry"] = "true";
                dataRowInsert["EnableShipping"] = "true";
                ds.Tables[0].Rows.InsertAt(dataRowInsert, 0);
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                pnlCountryList.Visible = true;
                uxCountryGrid.DataSource = ds;
                uxCountryGrid.DataBind();
                lblError.Visible = false;
            }
            else
            {
                pnlCountryList.Visible = false;
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoCountries").ToString();
                lblError.Visible = true;
            }
        }
        #endregion
    }
}