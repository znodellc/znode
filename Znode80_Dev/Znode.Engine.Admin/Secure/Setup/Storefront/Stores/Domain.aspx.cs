using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the SiteAdmin Admin_Secure_settings_Stores_Domain class
    /// </summary>
    public partial class AddDomain : System.Web.UI.Page
    {
        #region Private Member Variables
        private int ItemId;
        private int DomainId;
        private string AssociateName = string.Empty;
        private string Link = "~/Secure/Setup/Storefront/Stores/View.aspx?mode=Domain&itemid=";
        #endregion

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Display upgrade warning message if singlestore edition.
            if (!UserStoreAccess.IsMultiStoreAdminEnabled())
            {
                lblMessage.Text = UserStoreAccess.UpgradeMessage;
                DomainContent.Visible = false;
                return;
            }

            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            // Get ItemId from querystring        
            if (Request.Params["domainid"] != null)
            {
                this.DomainId = int.Parse(Request.Params["domainid"]);
            }
            else
            {
                this.DomainId = 0;
            }

            if (!Page.IsPostBack)
            {
                // If edit func then bind the data fields
                if (this.DomainId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditUrl").ToString();
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddNewURL").ToString();
                }
            }
        }

        #region Protected Methods and Events

        /// <summary>
        /// Bind Edit Data Method
        /// </summary>
        protected void BindEditData()
        {
            DomainAdmin domainAdmin = new DomainAdmin();
            Domain domain = new Domain();

            if (this.DomainId > 0)
            {
                domain = domainAdmin.GetDomainByDomainId(this.DomainId);

                lblTitle.Text += domain.DomainName;
                txtDomainName.Text = "http://" + domain.DomainName;
                CheckIsDomainInd.Checked = domain.IsActive;
            }
        }

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            DomainAdmin domainAdmin = new DomainAdmin();
            Domain domain = new Domain();
            bool Status;
            string DomainName = string.Empty;

            // Display the upgrade message if store is singlefront and already one domain created.
            if (!UserStoreAccess.IsMultiStoreAdminEnabled() && domainAdmin.GetDomainByPortalID(this.ItemId).Count >= 1)
            {
                lblMsg.Text = UserStoreAccess.UpgradeMessage;
                return;
            }

            // If edit mode then get all the values first
            if (this.DomainId > 0)
            {
                domain = domainAdmin.GetDomainByDomainId(this.DomainId);
            }

            // Set the DomainName
            DomainName = txtDomainName.Text.Trim().ToLower();

            if (DomainName.Contains("http://") || DomainName.Contains("https://"))
            {
                DomainName = DomainName.Replace("http://", string.Empty).Replace("https://", string.Empty);
            }

            if (DomainName.Contains(":"))
            {
                // Strip off the port number so we won't conflict with debuggers and other services that may specify a different port.
                DomainName = DomainName.Substring(0, DomainName.IndexOf(":"));
            }

            if (DomainName.StartsWith("www."))
            {
                DomainName = DomainName.Remove(0, 4);
            }

            // Remove any trailing "/"
            if (DomainName.EndsWith("/"))
            {
                DomainName = DomainName.Remove(DomainName.Length - 1);
            }

            domain.PortalID = this.ItemId;
            domain.DomainName = DomainName;
            domain.IsActive = CheckIsDomainInd.Checked;
            domain.ApiKey =  Guid.NewGuid().ToString();
            try
            {
                StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
                Portal portal = new Portal();
                portal = storeAdmin.GetByPortalId(this.ItemId);

                if (this.DomainId > 0)
                {
                    Status = domainAdmin.Update(domain);

                    // Log Activity
                    string associatename = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditDomain").ToString() + DomainName + " - " + portal.StoreName;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associatename, portal.StoreName);
                }
                else
                {
                    Status = domainAdmin.Insert(domain);

                    // Log Activity
                    string associatename = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddDomain").ToString() + DomainName + " - " + portal.StoreName;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associatename, portal.StoreName);
                }
            }
            catch
            {
                Status = false;
            }

            if (!Status)
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDomainNameShouldbeUnique").ToString();
            }
            else
            {
                Response.Redirect(this.Link + this.ItemId);
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.Link + this.ItemId);
        }

        #endregion
    }
}