using System;
using System.IO;
using System.Web;
using System.Web.UI;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace  Znode.Engine.Admin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the Admin_Secure_settings_Stores_EditDisplay class.
    /// </summary>
    public partial class EditDisplay : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private string redirectUrl = "view.aspx?ItemId={0}&mode=display";
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get portalId value from querystring
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.Bind();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = null;
            bool isUpdated = false;
            string noImageFileName = string.Empty;
            System.IO.FileInfo fileInfo = null;

            if (this.itemId > 0)
            {
                portal = storeAdmin.GetByPortalId(this.itemId);
            }

            if (portal != null)
            {
                portal.MaxCatalogDisplayColumns = byte.Parse(txtMaxCatalogDisplayColumns.Text);
                portal.MaxCatalogCategoryDisplayThumbnails = int.Parse(txtMaxSmallThumbnailsDisplay.Text);

                portal.MaxCatalogItemLargeWidth = int.Parse(txtMaxCatalogItemLargeWidth.Text);
                portal.MaxCatalogItemMediumWidth = int.Parse(txtMaxCatalogItemMediumWidth.Text);
                portal.MaxCatalogItemSmallWidth = int.Parse(txtMaxCatalogItemSmallWidth.Text);
                portal.MaxCatalogItemCrossSellWidth = int.Parse(txtMaxCatalogItemCrossSellWidth.Text);
                portal.MaxCatalogItemThumbnailWidth = int.Parse(txtMaxCatalogItemThumbnailWidth.Text);
                portal.MaxCatalogItemSmallThumbnailWidth = int.Parse(txtMaxCatalogItemSmallThumbnailWidth.Text);

                if (RadioProductNewImage.Checked)
                {
                    if (uxProductImage.PostedFile != null && !string.IsNullOrEmpty(uxProductImage.PostedFile.FileName))
                    {
                        bool isSuccess = uxProductImage.SaveImage("sadmin", portal.PortalID.ToString(), string.Empty);
                        fileInfo = uxProductImage.FileInformation;

                        if (isSuccess)
                        {
                            if (uxProductImage.FileInformation.Name != string.Empty)
                            {
                                //portal.ImageNotAvailablePath = Path.Combine(ZNodeConfigManager.EnvironmentConfig.OriginalImagePath, uxProductImage.FileInformation.Name);
                                portal.ImageNotAvailablePath = ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + "Turnkey/" + portal.PortalID + "/" + uxProductImage.FileInformation.Name;
                            }
                        }
                    }
                }
                portal.UseDynamicDisplayOrder = chkDynamicDisplayOrder.Checked;
                isUpdated = storeAdmin.UpdateStore(portal);
            }

            if (isUpdated)
            {
                // Log Activity
                string associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditDisplaySettings").ToString() + portal.StoreName;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, portal.StoreName);

                Response.Redirect(string.Format(this.redirectUrl, this.itemId, string.Empty));
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorStoreDisplaySettings").ToString();

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeFailed, HttpContext.Current.User.Identity.Name);
            }
        }

        /// <summary>
        /// Cancel button click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format(this.redirectUrl, this.itemId, string.Empty));
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>        
        protected void RadioProductCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = true;
        }

        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind the store display settings.
        /// </summary>
        private void Bind()
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();

            // Set default Value from Store1 For Add page
            Portal portal = storeAdmin.GetByPortalId(this.itemId);

            if (portal != null)
            {
                lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditDisplaySettings").ToString() + "\"" + portal.StoreName + "\"";

                txtMaxCatalogDisplayColumns.Text = portal.MaxCatalogDisplayColumns.ToString();
                txtMaxSmallThumbnailsDisplay.Text = portal.MaxCatalogCategoryDisplayThumbnails.ToString();
                chkDynamicDisplayOrder.Checked = portal.UseDynamicDisplayOrder.GetValueOrDefault(false);

                txtMaxCatalogItemLargeWidth.Text = portal.MaxCatalogItemLargeWidth.ToString();
                txtMaxCatalogItemMediumWidth.Text = portal.MaxCatalogItemMediumWidth.ToString();
                txtMaxCatalogItemSmallWidth.Text = portal.MaxCatalogItemSmallWidth.ToString();
                txtMaxCatalogItemCrossSellWidth.Text = portal.MaxCatalogItemCrossSellWidth.ToString();
                txtMaxCatalogItemThumbnailWidth.Text = portal.MaxCatalogItemThumbnailWidth.ToString();
                txtMaxCatalogItemSmallThumbnailWidth.Text = portal.MaxCatalogItemSmallThumbnailWidth.ToString();
                ZNodeImage znodeImage = new ZNodeImage();
                //Image1.ImageUrl = znodeImage.GetImageHttpPathMedium(Path.GetFileName(portal.ImageNotAvailablePath));
                Image1.ImageUrl = znodeImage.GetImageHttpPathMedium("Turnkey/" + portal.PortalID + "/" + Path.GetFileName(portal.ImageNotAvailablePath));
            }
        }
        #endregion
    }
}