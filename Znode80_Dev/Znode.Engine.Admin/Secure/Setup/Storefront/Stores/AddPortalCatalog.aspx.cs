﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the SiteAdmin  Admin_PortalCatalog class
    /// </summary>
    public partial class AddPortalCatalog : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int PortalId = 0;
        private int PortalCatalogId = 0;
        private string ViewPage = "View.aspx?itemid=";
        private string AddCatalogPage = "~/Secure/Setup/Storefront/Catalogs/add.aspx";
        private string SelectedTheme = string.Empty;
        private string AssociateName = string.Empty;
        private int Localeid = 43;
        private CatalogAdmin catalogAdmin = new CatalogAdmin();
        #endregion

        #region Page Load Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemId"] != null)
            {
                this.PortalId = int.Parse(Request.Params["itemId"].ToString());
            }

            if (Request.Params["PortalCatalogId"] != null)
            {
                this.PortalCatalogId = int.Parse(Request.Params["PortalCatalogId"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindCatalog();
                this.BindTheme();
                this.BindLocale();
                this.BindPortalCatalog();
            }
        }
        #endregion

        #region Form Events
        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancelBottom_Click(object sender, EventArgs e)
        {
            this.RedirectToViewPage();
        }

        /// <summary>
        /// Theme Dropdown list Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlTheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlCSS.Items.Clear();
            pnlCSS.Visible = true;
            this.SelectedTheme = ddlTheme.SelectedItem.Text;
            this.BindCss();
        }

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            this.RedirectToViewPage();
        }

        /// <summary>
        /// Catalog Dropdown Control Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlCatalog.SelectedValue == "0")
            {
                Response.Redirect(this.AddCatalogPage);
            }
        }

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmitBottom_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();

            lblError.Visible = true;

            if (ddlCatalog.SelectedValue == string.Empty)
            {
                // Display Error message
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidSelectCatalog").ToString();
                return;
            }

            if (ddlCSS.Items.Count == 0)
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidSelectCSSFile").ToString();
                return;
            }

            if (ddlLocale.Items.Count == 0)
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidSelectLocale").ToString();
                return;
            }

            if (this.IsCatalogExist())
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidAssociateCatalog").ToString();
                return;
            }

            if (this.IsCatalogLocaleExist())
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPotalCatalogExist").ToString();
                return;
            }

            PortalCatalogService portalService = new PortalCatalogService();
            PortalCatalog portalCatalog = new PortalCatalog();
            portalCatalog.PortalCatalogID = this.PortalCatalogId;
            portalCatalog.PortalID = this.PortalId;
            portalCatalog.CatalogID = int.Parse(ddlCatalog.SelectedValue);


            //portalCatalog.LocaleID = Localeid;

            //TODO SM
            portalCatalog.LocaleID = int.Parse(ddlLocale.SelectedValue);
            portalCatalog.ThemeID = int.Parse(ddlTheme.SelectedValue);
            portalCatalog.CSSID = int.Parse(ddlCSS.SelectedValue);

            if (this.PortalCatalogId == 0)
            {
                portalService.Save(portalCatalog);

                // Add Default Content Pages
                ContentPageService contentPageService = new ContentPageService();
                ContentPage contentPage = new ContentPage();
                ContentPageAdmin contentPageAdmin = new ContentPageAdmin();
                ContentPageQuery filter = new ContentPageQuery();

                // Get Portal By portalID
                Portal portal = storeAdmin.GetByPortalId(this.PortalId);

                //portal.LocaleID = Localeid;

                //TODO SM
                portal.LocaleID = int.Parse(ddlLocale.SelectedValue);


                // Get ContentPages list using portalID and LocaleId
                filter.Append(ContentPageColumn.LocaleId, ddlLocale.SelectedValue.ToString()); //SM
                filter.Append(ContentPageColumn.PortalID, this.PortalId.ToString());
                TList<ContentPage> contentPageList = contentPageService.Find(filter.GetParameters());

                // Get ContentPages list using portalID and LocaleId
                filter.Clear();
                filter.Append(ContentPageColumn.PortalID, this.PortalId.ToString());
                filter.Append(ContentPageColumn.LocaleId, ddlLocale.SelectedValue.ToString());//SM
                TList<ContentPage> contentPageAllList = contentPageService.Find(filter.GetParameters());

                if (contentPageList.Count > 0)
                {
                    foreach (ContentPage page in contentPageList)
                    {
                        ContentPage cp = contentPageAllList.Find("Name", page.Name);

                        if (cp == null)
                        {
                            contentPage = page.Clone() as ContentPage;
                            contentPage.ContentPageID = -1;
                            contentPage.PortalID = this.PortalId;
                            contentPage.LocaleId = int.Parse(ddlLocale.SelectedValue);//SM
                            contentPage.SEOURL = null;
                            contentPage.ThemeID = int.Parse(ddlTheme.SelectedValue);
                            contentPage.CSSID = int.Parse(ddlCSS.SelectedValue);
                            contentPageAdmin.AddPage(contentPage, contentPageAdmin.GetPageHTMLByName(page.Name, page.PortalID, page.LocaleId.ToString()), this.PortalId, ddlLocale.SelectedValue, HttpContext.Current.User.Identity.Name, null, false);
                        }
                    }
                }
                else
                {
                    // Add Default Home Page
                    ContentPage defaultContentPage = new ContentPage();

                    defaultContentPage.Name = "Home";
                    defaultContentPage.PortalID = this.PortalId;
                    defaultContentPage.AllowDelete = false;
                    defaultContentPage.ActiveInd = true;
                    defaultContentPage.Title = "Home";
                    defaultContentPage.SEOMetaDescription = "Home";
                    defaultContentPage.SEOMetaKeywords = "Home";
                    defaultContentPage.SEOTitle = "Home";
                    defaultContentPage.TemplateName = "home.master";
                    defaultContentPage.LocaleId = int.Parse(ddlLocale.SelectedValue);//SM
                    defaultContentPage.SEOURL = null;
                    defaultContentPage.ThemeID = int.Parse(ddlTheme.SelectedValue);
                    defaultContentPage.CSSID = int.Parse(ddlCSS.SelectedValue);

                    // Add code here
                    contentPageAdmin.AddPage(defaultContentPage, string.Empty, this.PortalId, ddlLocale.SelectedValue, HttpContext.Current.User.Identity.Name, null, false);

                    portal.LocaleID = int.Parse(ddlLocale.SelectedValue);//SM;

                    bool check = storeAdmin.UpdateStore(portal);
                }

                // Add Custom Messages
                // Create Custom Messages
                storeAdmin.CreateMessage(this.PortalId.ToString(), ddlLocale.SelectedValue);

                // Log Activity
                this.AssociateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAssociateCatalog").ToString(), ddlCatalog.SelectedItem.Text, portal.StoreName);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, portal.StoreName);
            }
            else
            {
                Portal portal = storeAdmin.GetByPortalId(this.PortalId);
                portalService.Update(portalCatalog);
                string AssociateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditAssociateCatalog").ToString(), ddlCatalog.SelectedItem.Text, portal.StoreName);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, portal.StoreName);
            }

            this.RedirectToViewPage();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Redirect to Product catalog view page
        /// </summary>
        private void RedirectToViewPage()
        {
            Response.Redirect(this.ViewPage + this.PortalId + "&Mode=catalog");
        }

        /// <summary>
        /// Check whether the selected catalog already associated with the store.
        /// </summary>
        /// <returns>Returns bool value true or false</returns>
        private bool IsCatalogExist()
        {
            PortalCatalogService portalCatalogService = new PortalCatalogService();

            int selectedCatalogId = Convert.ToInt32(ddlCatalog.SelectedValue);

            PortalCatalog portalCatalog = portalCatalogService.GetByPortalIDCatalogID(this.PortalId, selectedCatalogId);

            if (portalCatalog == null)
            {
                return false;
            }

            return !(portalCatalog.PortalCatalogID == this.PortalCatalogId);
        }

        /// <summary>
        /// Check whether the portal catalog-locale theme already exist.
        /// </summary>
        /// <returns>Returns True if exist, else False</returns>
        private bool IsCatalogLocaleExist()
        {
            PortalCatalogHelper portalCatalogHelper = new PortalCatalogHelper();
            DataSet ds = portalCatalogHelper.GetPortalCatalog(this.PortalId);
            int catalogId = int.Parse(ddlCatalog.SelectedValue);
            int localeId = int.Parse(ddlLocale.SelectedValue);//SM;
            string expression = "PortalCatalogId<>" + this.PortalCatalogId.ToString() + " AND CatalogId=" + catalogId + "  AND LocaleId=" + localeId.ToString();
            ds.Tables[0].DefaultView.RowFilter = expression;

            if (ds.Tables[0].DefaultView.Count > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Bind catalog theme
        /// </summary>
        private void BindPortalCatalog()
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = storeAdmin.GetByPortalId(this.PortalId);
            lblTitle.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAssociateCatalog").ToString(), portal.StoreName);

            if (this.PortalCatalogId > 0)
            {
                ddlLocale.Enabled = false;
                PortalCatalogService portalCatalogService = new PortalCatalogService();
                PortalCatalog portalCatalog = portalCatalogService.GetByPortalCatalogID(this.PortalCatalogId);

                ddlCatalog.SelectedValue = portalCatalog.CatalogID.ToString();
                ddlTheme.SelectedValue = portalCatalog.ThemeID.ToString();
                this.SelectedTheme = ddlTheme.SelectedItem.Text;
                ddlCSS.Items.Clear();
                pnlCSS.Visible = true;
                this.BindCss();

                ddlCSS.SelectedIndex = ddlCSS.Items.IndexOf(ddlCSS.Items.FindByValue(portalCatalog.CSSID.ToString()));
                ddlLocale.SelectedIndex = ddlLocale.Items.IndexOf(ddlLocale.Items.FindByValue(portalCatalog.LocaleID.ToString()));
            }
        }

        /// <summary>
        /// Bind the catalog by selected portal
        /// </summary>
        private void BindCatalog()
        {
            ddlCatalog.DataSource = this.catalogAdmin.GetAllCatalogs();
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();

            foreach (ListItem licatalog in ddlCatalog.Items)
            {
                licatalog.Text = Server.HtmlDecode(licatalog.Text);
            }

            ListItem separator = new ListItem("------------------------------", "");
            ddlCatalog.Items.Insert(ddlCatalog.Items.Count, separator);

            ListItem item = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAddNewCatalog").ToString(), "0");
            ddlCatalog.Items.Insert(ddlCatalog.Items.Count, item);
            ddlCatalog.SelectedIndex = 0;

        }

        /// <summary>
        /// Bind locale by selected portal Id
        /// </summary>
        private void BindLocale()
        {
            // Load all locales
            LocaleService localeService = new LocaleService();
            TList<Locale> localeList = localeService.GetAll();
            localeList.Sort("LocaleDescription");
            ddlLocale.DataValueField = "LocaleId";
            ddlLocale.DataTextField = "LocaleDescription";
            ddlLocale.DataSource = localeList;
            ddlLocale.DataBind();
            // Remove the existing locale from DropDownList
            PortalCatalogService portalCatalogService = new PortalCatalogService();
            TList<PortalCatalog> portalCatalogList = portalCatalogService.GetByPortalID(this.PortalId);
            if (this.PortalCatalogId == 0)
            {
                foreach (PortalCatalog pc in portalCatalogList)
                {
                    ListItem li = ddlLocale.Items.FindByValue(pc.LocaleID.ToString());
                    if (li != null)
                    {
                        ddlLocale.Items.Remove(li);
                    }
                }
            }
            else
            {
                PortalCatalog portalCatalog = portalCatalogService.GetByPortalCatalogID(this.PortalCatalogId);
                int localeId = portalCatalog.LocaleID;
                foreach (PortalCatalog pc in portalCatalogList)
                {
                    ListItem li = ddlLocale.Items.FindByValue(pc.LocaleID.ToString());
                    if (li != null && localeId != pc.LocaleID)
                    {
                        ddlLocale.Items.Remove(li);
                    }
                }
            }
            if (ddlLocale.SelectedValue != null)
            {
                this.ddlLocale.SelectedValue = 43.ToString();
            }
        }


        /// <summary>
        /// Binds the themes to the DropDownList
        /// </summary>
        private void BindTheme()
        {
            ddlTheme.DataSource = DataRepository.ThemeProvider.GetAll();
            ddlTheme.DataTextField = "Name";
            ddlTheme.DataValueField = "ThemeID";
            ddlTheme.DataBind();

            BindCss();
        }

        /// <summary>
        /// Bind the CSS to the DropDownList
        /// </summary>
        private void BindCss()
        {
            ddlCSS.DataSource = DataRepository.CSSProvider.GetByThemeID(int.Parse(ddlTheme.SelectedValue));
            ddlCSS.DataTextField = "Name";
            ddlCSS.DataValueField = "CSSID";
            ddlCSS.DataBind();

        }
        #endregion


    }
}