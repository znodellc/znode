using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the SiteAdmin Admin_Secure_settings_Stores_Add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Private Members
        private string SelectedTheme = string.Empty;
        private CatalogAdmin catalogAdmin = new CatalogAdmin();
        private int Localeid = 0;
        #endregion
        
        #region Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Display upgrade warning message if singlestore edition.
            if (!UserStoreAccess.IsMultiStoreAdminEnabled())
            {
                lblMessage.Text = UserStoreAccess.UpgradeMessage;
                StoreContent.Visible = false;
                return;
            }

            if (!Page.IsPostBack)
            {
                lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleStoreAdd").ToString();

                this.BindOrderStatus(true);

                this.BindCatalogData();

                this.BindThemeList();

                this.BindCssList();

                this.BindLocale();

                this.BindProductReviewStates();

                tblShowImage.Visible = false;
                tblLogoUpload.Visible = true;
                radNewImage.Checked = true;
            }
        }


        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = new Portal();

            if (!UserStoreAccess.IsMultiStoreAdminEnabled() && storeAdmin.GetAllStores().Count >= 1)
            {
                lblMsg.Text = "<BR />" + UserStoreAccess.UpgradeMessage;
                return;
            }

            // Get Exisitng Portal Settings By portalID
            Portal existingPortal = storeAdmin.GetByPortalId(ZNodeConfigManager.SiteConfig.PortalID);

            // Get Fedex Key and Password from Existing Portal
            StoreSettingsHelper storeHelper = new StoreSettingsHelper();
            DataSet ds = storeHelper.GetFedExKey();

            if (ds.Tables[0].Rows.Count > 0)
            {
                portal.FedExCSPKey = ds.Tables[0].Rows[0]["FedExCSPKey"].ToString();
                portal.FedExCSPPassword = ds.Tables[0].Rows[0]["FedExCSPPassword"].ToString();
                portal.FedExClientProductVersion = ds.Tables[0].Rows[0]["FedExClientProductVersion"].ToString();
                portal.FedExClientProductId = ds.Tables[0].Rows[0]["FedExClientProductId"].ToString();
            }

            portal.PortalID = 0;
            portal.AdminEmail = txtAdminEmail.Text;
            portal.CompanyName = Server.HtmlEncode(txtCompanyName.Text);
            portal.CustomerServiceEmail = txtCustomerServiceEmail.Text;
            portal.CustomerServicePhoneNumber = txtCustomerServicePhoneNumber.Text;
            portal.InclusiveTax = chkInclusiveTax.Checked;
            portal.ActiveInd = true;
            portal.SalesEmail = txtSalesEmail.Text;
            portal.SalesPhoneNumber = txtSalesPhoneNumber.Text;
            portal.StoreName = Server.HtmlEncode(txtStoreName.Text);
            portal.UseSSL = chkEnableSSL.Checked;
            portal.DefaultReviewStatus = ListReviewStatus.SelectedValue;

            if (!string.IsNullOrEmpty(ddlLocale.SelectedValue))
            {
                Localeid = int.Parse(ddlLocale.SelectedValue);
            }
            else
            {
                Localeid = 43;
            }
            portal.LocaleID = Localeid;
            portal.PersistentCartEnabled = chkPersistentCart.Checked;

            portal.FedExAccountNumber = string.Empty;
            portal.FedExAddInsurance = false;
            portal.FedExProductionKey = string.Empty;
            portal.FedExSecurityCode = string.Empty;
            //portal.InclusiveTax = false;
            portal.SMTPPassword = string.Empty;
            portal.SMTPServer = string.Empty;
            portal.SMTPUserName = string.Empty;

            portal.SiteWideAnalyticsJavascript = string.Empty;
            portal.SiteWideBottomJavascript = string.Empty;
            portal.SiteWideTopJavascript = string.Empty;
            portal.OrderReceiptAffiliateJavascript = string.Empty;
            portal.GoogleAnalyticsCode = string.Empty;
            portal.UPSKey = string.Empty;
            portal.UPSPassword = string.Empty;
            portal.UPSUserName = string.Empty;

            // Default values from existing portal
            portal.SMTPPort = existingPortal.SMTPPort;
            portal.WeightUnit = existingPortal.WeightUnit;
            portal.DimensionUnit = existingPortal.DimensionUnit;
            portal.CurrencyTypeID = existingPortal.CurrencyTypeID;
            portal.DefaultOrderStateID = int.Parse(ddlOrderStateList.SelectedValue);

            portal.MaxCatalogCategoryDisplayThumbnails = existingPortal.MaxCatalogCategoryDisplayThumbnails;
            portal.MaxCatalogDisplayColumns = existingPortal.MaxCatalogDisplayColumns;
            portal.MaxCatalogDisplayItems = existingPortal.MaxCatalogDisplayItems;
            portal.MaxCatalogItemCrossSellWidth = existingPortal.MaxCatalogItemCrossSellWidth;
            portal.MaxCatalogItemLargeWidth = existingPortal.MaxCatalogItemLargeWidth;
            portal.MaxCatalogItemMediumWidth = existingPortal.MaxCatalogItemMediumWidth;
            portal.MaxCatalogItemSmallThumbnailWidth = existingPortal.MaxCatalogItemSmallThumbnailWidth;
            portal.MaxCatalogItemSmallWidth = existingPortal.MaxCatalogItemSmallWidth;
            portal.MaxCatalogItemThumbnailWidth = existingPortal.MaxCatalogItemThumbnailWidth;
            portal.ImageNotAvailablePath = existingPortal.ImageNotAvailablePath;

            portal.ShopByPriceMax = existingPortal.ShopByPriceMax;
            portal.ShopByPriceMin = existingPortal.ShopByPriceMin;
            portal.ShopByPriceIncrement = existingPortal.ShopByPriceIncrement;

            portal.EnableAddressValidation = chkEnableAddressValidation.Checked;
            portal.RequireValidatedAddress = chkRequireValidatedAddress.Checked;
            //portal.EnablePIMS = chkEnablePIMS.Checked;
			portal.EnableCustomerPricing = CheckEnableCustomerBasedPricing.Checked;
            portal.DefaultProductReviewStateID = Convert.ToInt32(ddlProductReviewStateID.SelectedValue);
            portal.MobileTheme = "Mobile";
            // Set logo path
            string fileName = string.Empty;

            if (radNewImage.Checked == true)
            {
                // Check for Product Image
                fileName = System.IO.Path.GetFileNameWithoutExtension(UploadImage.PostedFile.FileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(UploadImage.PostedFile.FileName);
                string fileExtension = string.Empty;
                fileExtension = System.IO.Path.GetExtension(UploadImage.PostedFile.FileName);

                if (fileName != string.Empty)
                {
                    if ((fileExtension.ToLower() == ".jpeg") || (fileExtension.ToLower().Equals(".jpg")) || (fileExtension.ToLower().Equals(".png")) || (fileExtension.ToLower().Equals(".gif")))
                    {
                        portal.LogoPath = ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + fileName;
                    }
                    else
                    {
                        lblImageError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidStoreImage").ToString();
                        return;
                    }
                }
            }

            bool check = false;
            check = storeAdmin.InsertStore(portal);

            if (check)
            {
                // Update LogoPath based on PortalId
                Portal store = null;
                store = storeAdmin.GetByPortalId(portal.PortalID);

                store.LogoPath = ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + "Turnkey/" + portal.PortalID + "/" + fileName;
                byte[] imageData1 = new byte[UploadImage.PostedFile.InputStream.Length];
                UploadImage.PostedFile.InputStream.Read(imageData1, 0, (int)UploadImage.PostedFile.InputStream.Length);
                ZNodeStorageManager.WriteBinaryStorage(imageData1, ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + "Turnkey/" + portal.PortalID + "/" + fileName);
                check = storeAdmin.Update(store);

                // Portal Country Service
                PortalCountryService portalCountryService = new PortalCountryService();
                PortalCountry portalCountry = new PortalCountry();

                // Add PortalCountry List
                portalCountry.PortalID = portal.PortalID;
                portalCountry.BillingActive = true;
                portalCountry.ShippingActive = true;
                portalCountry.CountryCode = "US";
                check = portalCountryService.Insert(portalCountry);

                // Delete the portalCatalog
                this.catalogAdmin.DeleteportalCatalog(portal.PortalID);

                PortalCatalog portalCatalog = new PortalCatalog();

                // Add the new Selection
                portalCatalog.PortalID = portal.PortalID;
                portalCatalog.CatalogID = Convert.ToInt32(ddlCatalog.SelectedValue);
                if (ddlThemeslist.Items.Count > 0 && ddlThemeslist.SelectedIndex != -1)
                {
                    portalCatalog.ThemeID = int.Parse(ddlThemeslist.SelectedValue);
                }

                if (ddlCSSList.Items.Count > 0 && ddlCSSList.SelectedIndex != -1)
                {
                    portalCatalog.CSSID = int.Parse(ddlCSSList.SelectedValue);
                }
                portalCatalog.LocaleID = Localeid;



                this.catalogAdmin.AddPortalCatalog(portalCatalog);

                // Add Default Content Pages
                ContentPageService contentPageService = new ContentPageService();
                ContentPage contentPage = new ContentPage();
                ContentPageAdmin contentPageAdmin = new ContentPageAdmin();
                ContentPageQuery filter = new ContentPageQuery();

                // Get ContentPages list using existing portalID and LocaleId
                filter.Append(ContentPageColumn.LocaleId, existingPortal.LocaleID.ToString());
                filter.Append(ContentPageColumn.PortalID, existingPortal.PortalID.ToString());
                TList<ContentPage> contentPageList = contentPageService.Find(filter.GetParameters());

                if (contentPageList.Count > 0)
                {
                    foreach (ContentPage page in contentPageList)
                    {
                        contentPage = page.Clone() as ContentPage;
                        contentPage.ContentPageID = -1;
                        contentPage.PortalID = portal.PortalID;
                        contentPage.LocaleId = portalCatalog.LocaleID;
                        contentPage.SEOURL = null;
                        contentPage.ThemeID = portalCatalog.ThemeID;
                        contentPageAdmin.AddPage(contentPage, string.Empty, portal.PortalID, portalCatalog.LocaleID.ToString(), HttpContext.Current.User.Identity.Name, null, false);
                    }
                }

                // Create Message Config
                storeAdmin.CreateMessage(portal.PortalID.ToString(), ddlLocale.SelectedValue);


                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddStore").ToString(), txtStoreName.Text), txtStoreName.Text);

                Response.Redirect("~/Secure/Setup/Storefront/Stores/View.aspx?itemid=" + portal.PortalID.ToString());

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeSuccess, HttpContext.Current.User.Identity.Name);
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorStoreSettings").ToString();

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeFailed, HttpContext.Current.User.Identity.Name);
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Storefront/Stores/Default.aspx");
        }

        /// <summary>
        /// Current Image Radio Button Checked Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblLogoUpload.Visible = false;
        }

        /// <summary>
        /// New Image Radio Button Checked Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblLogoUpload.Visible = true;
        }

        /// <summary>
        /// Country option changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlThemeslist_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SelectedTheme = ddlThemeslist.SelectedItem.Text;
            ddlCSSList.Items.Clear();
            this.BindCssList();
        }

        /// <summary>
        /// If required PENDING APPROVAL option, then include in the dropdown and select that option.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChkPendingApproval_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPendingApproval.Checked)
            {
                this.BindOrderStatus(false);
                ddlOrderStateList.SelectedValue = ((int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.PENDING_APPROVAL).ToString();
                ddlOrderStateList.Enabled = false;
            }
            else
            {
                this.BindOrderStatus(true);
                ddlOrderStateList.Enabled = true;

                ddlOrderStateList.SelectedValue = ((int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.SUBMITTED).ToString();
            }
        }

        #endregion

        #region Bind Methods

        /// <summary>
        /// Bind Order Status
        /// </summary>
        /// <param name="IsRemovePendingApproval">The value of IsRemovePendingApproval</param>
        protected void BindOrderStatus(bool IsRemovePendingApproval)
        {
            OrderAdmin orderAdmin = new OrderAdmin();
            ddlOrderStateList.DataSource = orderAdmin.GetAllOrderStates();
            ddlOrderStateList.DataTextField = "OrderStateName";
            ddlOrderStateList.DataValueField = "OrderStateID";
            ddlOrderStateList.DataBind();

            // Remove Pending Approval Status from the Dropdown, if required.
            if (IsRemovePendingApproval)
            {
                ListItem li = ddlOrderStateList.Items.FindByValue(((int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.PENDING_APPROVAL).ToString());
                if (li != null)
                {
                    ddlOrderStateList.Items.Remove(li);
                }
            }
        }

        /// <summary>
        /// Bind Store Catalogs
        /// </summary>
        protected void BindCatalogData()
        {
            ddlCatalog.DataSource = this.catalogAdmin.GetAllCatalogs();
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();

            foreach (ListItem licatalog in ddlCatalog.Items)
            {
                licatalog.Text = Server.HtmlDecode(licatalog.Text);
            }
        }

        /// <summary>
        /// Binds the themes to the DropDownList
        /// </summary>
        private void BindThemeList()
        {
            ddlThemeslist.DataSource = DataRepository.ThemeProvider.GetAll();
            ddlThemeslist.DataTextField = "Name";
            ddlThemeslist.DataValueField = "ThemeID";
            ddlThemeslist.DataBind();
            ddlThemeslist.SelectedIndex = 0;
           
            pnlCssList.Visible = true;
            this.BindCssList();
        }

        /// <summary>
        /// Bind the CSS to the DropDownList
        /// </summary>
        private void BindCssList()
        {
            ddlCSSList.DataSource = DataRepository.CSSProvider.GetByThemeID(int.Parse(ddlThemeslist.SelectedValue));
            ddlCSSList.DataTextField = "Name";
            ddlCSSList.DataValueField = "CSSID";
            ddlCSSList.DataBind();
        }

        /// <summary>
        /// Bind Locale list
        /// </summary>
        private void BindLocale()
        {
            LocaleService localeService = new LocaleService();
            TList<Locale> localeList = localeService.GetAll();
            localeList.Sort("LocaleDescription");

            // Bind the dropdown.
            ddlLocale.DataSource = localeList;
            ddlLocale.DataValueField = "LocaleId";
            ddlLocale.DataTextField = "LocaleDescription";
            ddlLocale.DataBind();
            if (ddlLocale.Items.Count != 0)
            {
                lblLocale.Visible = false;
                ddlLocale.Visible = false;
            }
            ddlLocale.Items.FindByText("English").Selected = true;
        }

        /// <summary>
        /// Bind Product Review States
        /// </summary>
        private void BindProductReviewStates()
        {
            TList<ProductReviewState> stateList = ZNode.Libraries.DataAccess.Data.DataRepository.ProductReviewStateProvider.GetAll();

            // Bind the dropdown.
            ddlProductReviewStateID.DataSource = stateList;
            ddlProductReviewStateID.DataValueField = "ReviewStateID";
            ddlProductReviewStateID.DataTextField = "ReviewStateName";
            ddlProductReviewStateID.DataBind();
        }
        #endregion
    }
}