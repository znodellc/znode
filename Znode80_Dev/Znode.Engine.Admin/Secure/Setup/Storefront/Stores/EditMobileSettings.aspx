﻿<%@ Page Language="C#"
    MasterPageFile="~/Themes/Standard/edit.master"
    AutoEventWireup="true" Title="Manage Stores - Edit MobileSettings"
    Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Stores.EditMobileSettings" CodeBehind="EditMobileSettings.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/Controls/Default/ImageUploader.ascx" TagName="UploadImage"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/CategoryAutoComplete.ascx" TagPrefix="ZNode"
    TagName="CategoryAutoComplete" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">

    <script type="text/javascript">
        var hiddenTextValue; //alias to the hidden field: hideValue      

        function AutoComplete_ParentCategorySelected(source, eventArgs) {
            hiddenTextValue = $get("<%=ParentCategoryId.ClientID %>");
            hiddenTextValue.value = eventArgs.get_value();
        }

        function AutoComplete_ParentCategoryShowing(source, eventArgs) {
            hiddenTextValue = $get("<%=ParentCategoryId.ClientID %>");
            hiddenTextValue.value = 0;

        }
        function HideFileUpload(hideUploadControl) {

            var fu = document.getElementById("<%=tblLogoUpload.ClientID %>");
            var rfvLogo = document.getElementById("<%=rfvStoreLogo.ClientID %>");
            var revFileType = document.getElementById("<%=revFileType.ClientID %>");

            if (fu != null) {
                if (hideUploadControl == true) {
                    fu.style.display = '';
                    ValidatorEnable(revFileType, true);
                    ValidatorEnable(rfvLogo, true);
                }
                else {
                    ValidatorEnable(revFileType, false);
                    ValidatorEnable(rfvLogo, false);
                    fu.style.display = 'none';
                }
            }
        }
    </script>

    <div>
        <asp:HiddenField ID="ParentCategoryId" runat="server"></asp:HiddenField>
        <div class="FormView">
            <div>
                <div class="LeftFloat" style="width: 70%; text-align: left">
                    <h1>
                        <asp:Label ID="lblTitle" Text="Mobile Settings" runat="server"></asp:Label>
                    </h1>
                </div>
                <div style="text-align: right; padding-right: 10px;">
                    <asp:ImageButton ID="btnSubmitTop" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/Themes/images/buttons/button_submit.gif"
                        runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" />
                    <asp:ImageButton ID="btnCancelTop" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                        onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/Themes/images/buttons/button_cancel.gif"
                        runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
                </div>
                <div align="left" class="ClearBoth">
                    <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
                </div>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <h4 class="SubTitle">Store Identity</h4>
            <div class="FieldStyle">
                <asp:Label ID="lblStoreNameTitle" runat="server" Text="Store Name"></asp:Label>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblStoreName" runat="server"></asp:Label>
            </div>
            <div class="FieldStyle">
                <asp:Label ID="Label1" runat="server" Text="Mobile Theme"></asp:Label>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlMobileThemes" runat="server"></asp:DropDownList>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <h4 class="SubTitle">HOME BANNER IMAGE</h4>
            <div class="ClearBoth">
            </div>
            <div id="tblShowImage" runat="server" style="margin: 10px;">
                <div class="LeftFloat" style="width: 350px; border-right: solid 1px #cccccc;">
                    <asp:Image ID="imgLogo" runat="server" />
                </div>
                <div class="LeftFloat" style="padding-left: 10px; margin-left: -1px; border-left: solid 1px #cccccc;">
                    <asp:Panel runat="server" ID="pnlShowSwatchOption">
                        <div>
                            Select an Option
                        </div>
                        <div>
                            <asp:RadioButton ID="radCurrentImage" onclick="return HideFileUpload(false)" Text="Keep Current Image" runat="server" GroupName="Product Swatch Image"
                                Checked="True" /><br />
                            <asp:RadioButton ID="rbRemoveCurrentImage" onclick="return HideFileUpload(false)" Text="Remove Current Image" runat="server" GroupName="Product Swatch Image" /><br />
                            <asp:RadioButton ID="radNewImage" onclick="return HideFileUpload(true)" Text="Upload New Image" runat="server" GroupName="Product Swatch Image" /><br />

                        </div>
                    </asp:Panel>
                    <br />
                    <div id="tblLogoUpload" runat="server" style="display: none;">
                        <div class="FieldStyle">
                            Select a Home Banner Image<span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle" style="width: 150px;">
                            <asp:FileUpload ID="UploadImage" runat="server" BorderStyle="Inset" EnableViewState="true" />
                            <br />
                            <asp:RequiredFieldValidator CssClass="Error" Enabled="false" ID="rfvStoreLogo" runat="server"
                                ControlToValidate="UploadImage" ErrorMessage="* Select an Image to Upload" Display="Dynamic"></asp:RequiredFieldValidator>
                            <br />
                            <asp:RegularExpressionValidator ID="revFileType" runat="server" ControlToValidate="UploadImage"
                                CssClass="Error"
                                ErrorMessage="* Specify an image with a jpeg, jpg, gif or png file format."
                                SetFocusOnError="True" ValidationExpression="^.+\.((jpg)|(JPG)|(gif)|(GIF)|(jpeg)|(JPEG)|(png)|(PNG))$"> </asp:RegularExpressionValidator>

                            <asp:Label ID="lblImageError" runat="server" CssClass="Error" ForeColor="Red" Text=""
                                Visible="False"></asp:Label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ClearBoth">
            </div>
            <br />
            <br />
            <%-- <asp:Panel ID="pnlSearchDepartment" runat="server">
                <div class="FieldStyle" runat="server" id="titleSearch">
                    <ZNode:Spacer ID="Spacer2" spacerheight="5" spacerwidth="3" runat="server"></ZNode:Spacer>
                    Search & Select Category</div>
                <div class="ValueStyle">
                    <div class="TextValueStyle">
                        <ZNode:CategoryAutoComplete runat="server" ID="txtCategory" Width="180px" ForceAutoCompleteTextBox="true"
                            IsRequired="false" />
                        <asp:Image CssClass="SearchIcon" ImageUrl="~/Themes/images/enlarge.gif" ToolTip="Search Category"
                            AlternateText="Search" runat="server" ImageAlign="AbsMiddle" ID="btnShowPopup" />
                    </div>
                </div>
            </asp:Panel>--%>
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
            </div>
            <div>
                <asp:ImageButton ID="btnSubmitBottom" onmouseover="this.src='../../../../Themes/Images/buttons/button_submit_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_submit.gif';" ImageUrl="~/Themes/images/buttons/button_submit.gif"
                    runat="server" AlternateText="Submit" OnClick="BtnSubmit_Click" CausesValidation="true" />
                <asp:ImageButton ID="btnCancelBottom" CausesValidation="False" onmouseover="this.src='../../../../Themes/Images/buttons/button_cancel_highlight.gif';"
                    onmouseout="this.src='../../../../Themes/Images/buttons/button_cancel.gif';" ImageUrl="~/Themes/images/buttons/button_cancel.gif"
                    runat="server" AlternateText="Cancel" OnClick="BtnCancel_Click" />
            </div>
        </div>
    </div>
</asp:Content>
