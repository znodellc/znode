<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Title="Manage Stores - Edit Units" Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Stores.EditUnits" CodeBehind="EditUnits.aspx.cs" ValidateRequest="false" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right; padding-right: 10px;">
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" ID="btnSubmitTop" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelTop" />
        </div>
        <div align="left">
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="SubTitleUnit" Text='<%$ Resources:ZnodeAdminResource, SubTitleUnit%>' runat="server"></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleWeight" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleWeight%>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlWeightUnits" runat="server">
                <asp:ListItem Selected="True" Text='<%$ Resources:ZnodeAdminResource, DropDownTextLbs%>'></asp:ListItem>
                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextKgs%>'></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleDimension" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDimension%>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlDimensions" runat="server">
                <asp:ListItem Selected="True" Text='<%$ Resources:ZnodeAdminResource, DropDownTextIn%>'></asp:ListItem>
                <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextCm%>'></asp:ListItem>
            </asp:DropDownList>
        </div>
        <h4 class="SubTitle">
            <asp:Localize runat="server" ID="SubTitleCurrency" Text='<%$ Resources:ZnodeAdminResource, SubTitleCurrencySettings%>'></asp:Localize></h4>
        <asp:UpdatePanel ID="updPnlcurrencySettings" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ColumnTitleActiveCurrency" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleActiveCurrency%>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlCurrencyTypes" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DdlCurrencyTypes_SelectedIndexChanged">
                    </asp:DropDownList>
                </div>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ColumnTitleCurrency" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCurrency%>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtCurrencySuffix" runat="server" AutoPostBack="True" OnTextChanged="TxtCurrencySuffix_TextChanged"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ColumnTitlePreview" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePreview%>'></asp:Localize>
                    <asp:Label ID="lblPrice" runat="server"></asp:Label>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="ClearBoth">
            <ZNode:Spacer ID="Spacer8" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div>
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" ID="btnSubmitBottom" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelBottom" />
        </div>
    </div>
</asp:Content>
