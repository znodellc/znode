using System;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;
using ZNode.Libraries.DataAccess.Data;

namespace  Znode.Engine.Admin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the Admin_Secure_settings_Stores_Edit class.
    /// </summary>
    public partial class Edit : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private string viewPageLink = "~/Secure/Setup/Storefront/Stores/View.aspx?itemid=";
        private CatalogAdmin catalogAdmin = new CatalogAdmin();
        private string selectedTheme = string.Empty;
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindOrderStatus(true);

                // Catalog List
                this.BindCatalog();
                this.BindThemeList();
                this.BindCssList();
                this.BindLocale();
                this.BindProductReviewStates();

                if (this.itemId > 0)
                {
                    this.BindEditData();
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleManageStore").ToString()+" \"" + Server.HtmlEncode(txtStoreName.Text.Trim()) + "\""; 
                }
            }
        }
        #endregion

        #region Events

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = new Portal();
            if (this.itemId > 0)
            {
                portal = storeAdmin.GetByPortalId(this.itemId);
            }
            else
            {
                // Set Mobile theme only for new entry. Do not override the Mobile Settings tab value.
                portal.MobileTheme = ddlThemeslist.SelectedItem.Text;
            }

            portal.CompanyName = txtCompanyName.Text;
            portal.ActiveInd = true;
            portal.StoreName = txtStoreName.Text;
            portal.UseSSL = chkEnableSSL.Checked;

            portal.AdminEmail = txtAdminEmail.Text;
            portal.CustomerServiceEmail = txtCustomerServiceEmail.Text;
            portal.CustomerServicePhoneNumber = txtCustomerServicePhoneNumber.Text;
            portal.SalesEmail = txtSalesEmail.Text;
            portal.SalesPhoneNumber = txtSalesPhoneNumber.Text;
            portal.LocaleID = Int32.Parse(ddlLocaleList.SelectedValue);
            portal.InclusiveTax = chkInclusiveTax.Checked;
            portal.DefaultReviewStatus = ListReviewStatus.SelectedValue;
            portal.PersistentCartEnabled = chkPersistentCart.Checked;
            portal.EnableAddressValidation = chkEnableAddressValidation.Checked;
            portal.RequireValidatedAddress = chkRequireValidatedAddress.Checked;
           // portal.EnablePIMS = chkEnablePIMS.Checked;
            portal.DefaultProductReviewStateID = Convert.ToInt32(ddlProductReviewStateID.SelectedValue);
	        portal.EnableCustomerPricing = CheckEnableCustomerBasedPricing.Checked;

            if (chkPendingApproval.Checked)
            {
                portal.DefaultOrderStateID = (int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.PENDING_APPROVAL;
            }
            else
            {
                portal.DefaultOrderStateID = int.Parse(ddlOrderStateList.SelectedValue);
            }

            // Set logo path
            string fileName = string.Empty;
            bool isUpdated = false;

            if (radNewImage.Checked == true)
            {
                // Check for Product Image
                fileName = System.IO.Path.GetFileNameWithoutExtension(UploadImage.PostedFile.FileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(UploadImage.PostedFile.FileName);

                if (fileName != string.Empty)
                {
                    byte[] imageData = new byte[UploadImage.PostedFile.InputStream.Length];
                    UploadImage.PostedFile.InputStream.Read(imageData, 0, (int)UploadImage.PostedFile.InputStream.Length);
                    ZNodeStorageManager.WriteBinaryStorage(imageData, ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + "Turnkey/" + portal.PortalID + "/" + fileName);
                    portal.LogoPath = ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + fileName;
                }
            }

            if (this.itemId > 0)
            {
                isUpdated = storeAdmin.UpdateStore(portal);
            }

            if (isUpdated)
            {
                PortalCatalog portalCatalog = new PortalCatalog();

                // Add the new Selection
                portalCatalog.PortalID = portal.PortalID;
                portalCatalog.CatalogID = Convert.ToInt32(ddlCatalog.SelectedValue);
                portalCatalog.LocaleID = Int32.Parse(ddlLocaleList.SelectedValue);

                if (ddlThemeslist.Items.Count > 0)
                {
                    portalCatalog.ThemeID = int.Parse(ddlThemeslist.SelectedValue);
                }
                else
                {
                    portalCatalog.ThemeID = 1; // default theme
                }

                if (ddlCSSList.Items.Count > 0)
                {
                    portalCatalog.CSSID = int.Parse(ddlCSSList.SelectedValue);
                }

                this.catalogAdmin.UpdatePortalCatalog(portalCatalog);

                // Refresh of all the configuration information from the database & other config files
                ZNodeConfigManager.RefreshConfiguration();

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeSuccess, HttpContext.Current.User.Identity.Name);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit of Store - " + txtStoreName.Text, txtStoreName.Text);

                Response.Redirect(this.viewPageLink + portal.PortalID);
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorStoreSettings").ToString();

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeFailed, HttpContext.Current.User.Identity.Name);
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.viewPageLink + this.itemId);
        }

        protected void RadCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblLogoUpload.Visible = false;
        }

        /// <summary>
        /// Upload New Image selected
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblLogoUpload.Visible = true;
        }

        /// <summary>
        /// Country option changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlThemes.Visible = true;
            ddlThemeslist.Items.Clear();
            this.BindThemeList();
        }

        /// <summary>
        /// Country option changed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlThemeslist_SelectedIndexChanged(object sender, EventArgs e)
        {
            pnlCssList.Visible = true;
            ddlCSSList.Items.Clear();
            this.selectedTheme = ddlThemeslist.SelectedItem.Text;
            this.BindCssList();
        }

        /// <summary>
        /// If required PENDING APPROVAL option, then include in the dropdown and select that option.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChkPendingApproval_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPendingApproval.Checked)
            {
                this.BindOrderStatus(false);
                ddlOrderStateList.SelectedValue = ((int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.PENDING_APPROVAL).ToString();
                ddlOrderStateList.Enabled = false;
            }
            else
            {
                if (this.itemId > 0)
                {
                    this.BindOrderStatus(true);
                    ddlOrderStateList.Enabled = true;

                    // Select last selected items from Database or select default to SUBMITTED.
                    StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
                    Portal portal;
                    portal = storeAdmin.GetByPortalId(this.itemId);

                    ListItem listItem = ddlOrderStateList.Items.FindByValue(portal.DefaultOrderStateID.GetValueOrDefault(10).ToString());
                    if (listItem != null)
                    {
                        listItem.Selected = true;
                    }
                    else
                    {
                        ddlOrderStateList.SelectedValue = ((int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.SUBMITTED).ToString();
                    }
                }
            }
        }

        #endregion

        #region Helper Method

        /// <summary>
        /// Resset the defaut profile.
        /// </summary>
        /// <param name="portalId">Portal Id to reset the default profile.</param>
        /// <param name="profileId">Profile Id to reset the default profile.</param>
        private void ResetDefaultProfile(int portalId, int profileId)
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            PortalService portalService = new PortalService();
            Portal portal = portalAdmin.GetByPortalId(portalId);

            if (portal.DefaultAnonymousProfileID == profileId)
            {
                portal.DefaultAnonymousProfileID = null;
            }

            if (portal.DefaultRegisteredProfileID == profileId)
            {
                portal.DefaultRegisteredProfileID = null;
            }

            portalService.Update(portal);
        }

        #endregion

        #region Bind Methods

        /// <summary>
        /// Bind the locales
        /// </summary>
        private void BindLocale()
        {
            PortalCatalogHelper portalCatalogHelper = new PortalCatalogHelper();
            DataSet ds = portalCatalogHelper.GetPortalCatalog(this.itemId);
            ddlLocaleList.DataValueField = "LocaleId";
            ddlLocaleList.DataTextField = "LocaleDescription";
            ddlLocaleList.DataSource = ds;
            ddlLocaleList.DataBind();
            ddlLocaleList.SelectedItem.Text = "English";
        }

        /// <summary>
        /// Bind Order Status
        /// </summary>
        /// <param name="isPendingApprovalRemoved">Indicates whether to remove the Pending Approval status from the list.</param>
        private void BindOrderStatus(bool isPendingApprovalRemoved)
        {
            OrderAdmin orderAdmin = new OrderAdmin();
            ddlOrderStateList.DataSource = orderAdmin.GetAllOrderStates();
            ddlOrderStateList.DataTextField = "OrderStateName";
            ddlOrderStateList.DataValueField = "OrderStateID";
            ddlOrderStateList.DataBind();

            // Remove Pending Approval Status from the Dropdown, if required.
            if (isPendingApprovalRemoved)
            {
                ListItem listItem = ddlOrderStateList.Items.FindByValue(((int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.PENDING_APPROVAL).ToString());
                if (listItem != null)
                {
                    ddlOrderStateList.Items.Remove(listItem);
                }
            }
        }

        /// <summary>
        /// Bind Store Catalogs
        /// </summary>
        private void BindCatalog()
        {
            ddlCatalog.DataSource = this.catalogAdmin.GetAllCatalogs();
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();
        }

		/// <summary>
		/// Binds the themes to the DropDownList
		/// </summary>
		private void BindThemeList()
		{
			ddlThemeslist.DataSource = DataRepository.ThemeProvider.GetAll();
			ddlThemeslist.DataTextField = "Name";
			ddlThemeslist.DataValueField = "ThemeID";
			ddlThemeslist.DataBind();
		
			try
			{
				ddlThemeslist.SelectedIndex = 0;
				pnlCssList.Visible = true;
				this.BindCssList();
			}
			catch
			{
			}
		}

		/// <summary>
		/// Bind the CSS to the DropDownList
		/// </summary>
		private void BindCssList()
		{
			if (ddlThemeslist.SelectedIndex > 0)
			{
				ddlCSSList.DataSource = DataRepository.CSSProvider.GetByThemeID(int.Parse(ddlThemeslist.SelectedValue));
				ddlCSSList.DataTextField = "Name";
				ddlCSSList.DataValueField = "CSSID";
				ddlCSSList.DataBind();			
			}
		}


        /// <summary>
        /// Bind data to form fields
        /// </summary>
        private void BindEditData()
        {
            // Get the catalogId based on Portal Id
            int catalogIdValue = this.catalogAdmin.GetCatalogIDByPortalID(this.itemId);

            // Set the Preselected Value.
            if (catalogIdValue != 0)
            {
                ddlCatalog.SelectedValue = catalogIdValue.ToString();

                this.BindThemeList();

                PortalCatalogService portalCatalogService = new PortalCatalogService();
                TList<PortalCatalog> portalCatalogList = portalCatalogService.GetByPortalID(this.itemId);

                if (portalCatalogList[0].ThemeID != null)
                {
					ddlThemeslist.SelectedValue = portalCatalogList[0].ThemeID.ToString();

                    if (ddlThemeslist.SelectedValue != null)
                    {
                        pnlCssList.Visible = true;
                        this.selectedTheme = ddlThemeslist.SelectedItem.Text;
                        ddlCSSList.Items.Clear();
                        this.BindCssList();
                    }
                }

                // CSS
                if (portalCatalogList[0].CSSID != null)
                {
					ddlCSSList.SelectedValue = portalCatalogList[0].CSSID.ToString();
                }
            }

            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = storeAdmin.GetByPortalId(this.itemId);
            txtCompanyName.Text = Server.HtmlDecode(portal.CompanyName);
            txtStoreName.Text = Server.HtmlDecode(portal.StoreName);
            chkEnableSSL.Checked = portal.UseSSL;
            txtAdminEmail.Text = portal.AdminEmail;
            txtSalesEmail.Text = portal.SalesEmail;
            txtCustomerServiceEmail.Text = portal.CustomerServiceEmail;
            txtSalesPhoneNumber.Text = portal.SalesPhoneNumber;
            txtCustomerServicePhoneNumber.Text = portal.CustomerServicePhoneNumber;
            ddlLocaleList.SelectedValue = portal.LocaleID.ToString();
            chkInclusiveTax.Checked = portal.InclusiveTax;
            ListReviewStatus.SelectedValue = portal.DefaultReviewStatus;
            chkPersistentCart.Checked = portal.PersistentCartEnabled.GetValueOrDefault(false);
            chkEnableAddressValidation.Checked = portal.EnableAddressValidation.GetValueOrDefault(false);
            chkRequireValidatedAddress.Checked = portal.RequireValidatedAddress.GetValueOrDefault(false);
           // chkEnablePIMS.Checked = portal.EnablePIMS.GetValueOrDefault(false);
            ddlProductReviewStateID.SelectedValue = portal.DefaultProductReviewStateID.GetValueOrDefault((int)ZNodeProductReviewState.PendingApproval).ToString();
	        CheckEnableCustomerBasedPricing.Checked = portal.EnableCustomerPricing.GetValueOrDefault(false);

            // Load the resized store logo from Content folder.
            ZNodeImage znodeImage = new ZNodeImage();
            if (portal.LogoPath != null)
            {
                if (portal.LogoPath.Contains("/content/"))
                {
                    imgLogo.ImageUrl = znodeImage.GetImageHttpPathSmall(Path.GetFileName(portal.LogoPath));
                }
                else
                {
                    imgLogo.ImageUrl = znodeImage.GetImageHttpPathSmall(string.Format("Turnkey/{0}/{1}", portal.PortalID, Path.GetFileName(portal.LogoPath)));
                }
                //imgLogo.ImageUrl = znodeImage.GetImageHttpPathSmall("Turnkey/" + portal.PortalID + "/" + Path.GetFileName(portal.LogoPath));
            }
            else
            {
                imgLogo.ImageUrl = znodeImage.GetImageHttpPathSmall(string.Empty);
                tblLogoUpload.Style.Add("display", "block");
                radNewImage.Checked = true;
            }

            if (portal.DefaultOrderStateID.GetValueOrDefault(10).ToString() == ((int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.PENDING_APPROVAL).ToString())
            {
                this.BindOrderStatus(false);
                ddlOrderStateList.Enabled = false;
                chkPendingApproval.Checked = true;

                // Display tab
                ddlOrderStateList.SelectedValue = portal.DefaultOrderStateID.GetValueOrDefault(10).ToString();
            }
            else
            {
                this.BindOrderStatus(true);
                ddlOrderStateList.Enabled = true;
                chkPendingApproval.Checked = false;
                ddlOrderStateList.SelectedValue = portal.DefaultOrderStateID.GetValueOrDefault(10).ToString();
            }
        }

        /// <summary>
        /// Bind Product Review States
        /// </summary>
        private void BindProductReviewStates()
        {
            TList<ProductReviewState> stateList = ZNode.Libraries.DataAccess.Data.DataRepository.ProductReviewStateProvider.GetAll();

            // Bind the dropdown.
            ddlProductReviewStateID.DataSource = stateList;
            ddlProductReviewStateID.DataValueField = "ReviewStateID";
            ddlProductReviewStateID.DataTextField = "ReviewStateName";
            ddlProductReviewStateID.DataBind();
        }
        #endregion
    }
}