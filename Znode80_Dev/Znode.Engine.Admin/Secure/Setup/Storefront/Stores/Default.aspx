<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" Title="Manage Stores - List" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Stores.Default" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>


<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">

    <div>
        <div>
            <div class="LeftFloat" style="width: 70%; text-align: left">
                <h1 class="LeftFloat">
                    <asp:Localize runat="server" ID="TitleManageStores" Text='<%$ Resources:ZnodeAdminResource, TitleManageStores %>'></asp:Localize>
                </h1>
                <div class="tooltip StoreToolTip">
                    <a href="javascript:void(0);" class="learn-more"><span>
                        <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                    <div class="content">
                        <h6>
                            <asp:Localize runat="server" ID="Help" Text='<%$ Resources:ZnodeAdminResource, HintHelp %>'></asp:Localize></h6>
                        <p>
                            <asp:Localize runat="server" ID="NewStorefront" Text='<%$ Resources:ZnodeAdminResource, HintSubTextNewStorefront %>'></asp:Localize>
                        </p>
                        <p>
                            <asp:Localize runat="server" ID="ManageStoreHintStep1" Text='<%$ Resources:ZnodeAdminResource, ManageStoreHintStep1 %>'></asp:Localize><br />
                            <asp:Localize runat="server" ID="ManageStoreHintStep2" Text='<%$ Resources:ZnodeAdminResource, ManageStoreHintStep2 %>'></asp:Localize><br />
                            <asp:Localize runat="server" ID="ManageStoreHintStep3" Text='<%$ Resources:ZnodeAdminResource, ManageStoreHintStep3 %>'></asp:Localize><br />
                            <asp:Localize runat="server" ID="ManageStoreHintStep4" Text='<%$ Resources:ZnodeAdminResource, ManageStoreHintStep4 %>'></asp:Localize>
                            <br />
                            <asp:Localize runat="server" ID="ManageStoreHintStep5" Text='<%$ Resources:ZnodeAdminResource, ManageStoreHintStep5 %>'></asp:Localize>
                        </p>
                    </div>
                </div>
            </div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="btnAdd" runat="server" CommandName="Login"
                    ButtonType="Button" OnClick="BtnAdd_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddStore %>'
                    ButtonPriority="Primary" />
            </div>
            <div class="ClearBoth">
                <p>
                    <asp:Localize runat="server" ID="TextStores" Text='<%$ Resources:ZnodeAdminResource, TextStores %>'></asp:Localize>
                </p>
            </div>

            <!-- Search product panel -->
            <div>
                <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
                    <h4 class="SubTitle">
                        <asp:Localize runat="server" ID="SubTitleSearchStores" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchStores %>'></asp:Localize>
                    </h4>
                    <div class="SearchForm">
                        <div class="RowStyle">
                            <div class="ItemStyle">
                                <span class="FieldStyle">
                                    <asp:Localize runat="server" ID="ColumnTitleKeyword" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleKeyword %>'></asp:Localize>
                                </span>
                                <br />
                                <span class="ValueStyle">
                                    <asp:TextBox ID="txtkeyword" runat="server"></asp:TextBox></span>
                            </div>
                        </div>
                    </div>
                    <div class="ClearBoth">
                        <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                        <zn:Button runat="server" ID="btnSearch" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                    </div>
                </asp:Panel>
            </div>
            <br />

            <div>
                <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
            </div>
            <br />

            <h4 class="GridTitle">
                <asp:Localize runat="server" ID="GridTitleStoreList" Text='<%$ Resources:ZnodeAdminResource, GridTitleStoreList %>'></asp:Localize>
            </h4>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
                CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
                OnRowDeleting="UxGrid_RowDeleting" AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyRecordText %>'
                OnRowDataBound="UxGrid_RowDataBound">
                <Columns>
                    <asp:BoundField DataField="PortalID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="StoreName" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStoreName %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="CompanyName" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleBrand %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleAction %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <div class="LeftFloat" style="width: 25%; text-align: left">
                                <asp:LinkButton ID="btnPreview" CommandName="Preview" Text='<%$ Resources:ZnodeAdminResource, LinkPreview %>' CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PortalID")%>'
                                    runat="server"></asp:LinkButton>
                            </div>
                            <div class="LeftFloat" style="width: 25%; text-align: left; text-transform: uppercase">
                                <asp:LinkButton ID="btnView" CommandName="Edit" Text='<%$ Resources:ZnodeAdminResource, LinkManage %>' CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PortalID")%>'
                                    runat="server"></asp:LinkButton>
                            </div>
                            <div class="LeftFloat" style="width: 25%; text-align: left; text-transform: uppercase">
                                <asp:LinkButton ID="btnEdit" CommandName="Copy" Text='<%$ Resources:ZnodeAdminResource, LinkCopy %>' CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PortalID")%>'
                                    runat="server"></asp:LinkButton>
                            </div>
                            <div class="LeftFloat" style="width: 25%; text-align: left; text-transform: uppercase">
                                <asp:LinkButton ID="btnDelete" CommandName="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CommandArgument='<%# DataBinder.Eval(Container.DataItem, "PortalID")%>'
                                    runat="server" Visible='<%# HideDeleteButton(Eval("PortalID").ToString()) %>'></asp:LinkButton>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
            <div>
                <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
            </div>
        </div>
    </div>
</asp:Content>
