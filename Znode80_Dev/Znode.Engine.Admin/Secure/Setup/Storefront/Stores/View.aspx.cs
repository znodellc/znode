using System;
using System.Data;
using System.Globalization;
using System.IO;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace  Znode.Engine.Admin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the stores view page in site admin
    /// </summary>
    public partial class View : System.Web.UI.Page
    {
        #region Private Member Variables
        private int _ItemId = 0;
        private string _SelectedTheme = string.Empty;
        private string _AssociateName = string.Empty;
        private string _UPSPassword = string.Empty;
        private string _Mode = string.Empty;
        private string AddDomainlink = "~/Secure/Setup/Storefront/Stores/Domain.aspx?itemid=";
        private string Profilelink = "~/Secure/Setup/Storefront/Stores/AddProfile.aspx?itemid=";
        private string AddCountriesLink = "~/Secure/Setup/Storefront/Stores/Countries.aspx?itemid=";
        private string AddPortalCatalogLink = "~/Secure/Setup/Storefront/Stores/AddPortalCatalog.aspx?itemid=";
        private CatalogAdmin catalogAdmin = new CatalogAdmin();
        private DataSet Catalogds = new DataSet();
        #endregion

        #region Protected, Public Properties
        /// <summary>
        /// Gets or sets the UPS Password
        /// </summary>
        public string UPSPassword
        {
            get { return this._UPSPassword; }
            set { this._UPSPassword = value; }
        }

        /// <summary>
        /// Gets or sets the ItemId
        /// </summary>
        protected int ItemId
        {
            get { return this._ItemId; }
            set { this._ItemId = value; }
        }

        /// <summary>
        /// Gets or sets the selected theme
        /// </summary>
        protected string SelectedTheme
        {
            get { return this._SelectedTheme; }
            set { this._SelectedTheme = value; }
        }

        /// <summary>
        /// Gets or sets the Associate Name
        /// </summary>
        protected string AssociateName
        {
            get { return this._AssociateName; }
            set { this._AssociateName = value; }
        }

        /// <summary>
        /// Gets or sets the Mode
        /// </summary>
        protected string Mode
        {
            get { return this._Mode; }
            set { this._Mode = value; }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Bind Store Countries
        /// </summary>
        public void BindStoreCountries()
        {
            PortalCountryAdmin portalCountryAdmin = new PortalCountryAdmin();
            uxCountryGrid.DataSource = portalCountryAdmin.GetPortalCountryByPortalID(this.ItemId);
            uxCountryGrid.DataBind();
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get mode value from querystring        
            if (Request.Params["mode"] != null)
            {
                this.Mode = Request.Params["mode"];
            }

            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.ResetTab();

                if (this.ItemId > 0)
                {
                    this.Bind();

                    this.BindStoreCountries();

                    // Domain Settings
                    pnlDomainSettings.Visible = true;
                    this.BindDomainData();

                    this.SetMobileSettingsVisibility();

                    this.BindAssociatedProfiles();
                    this.BindCatalogTheme();
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleManageStore").ToString() + lblStoreName.Text.Trim();

                }
            }
        }
        #endregion

        #region Protected Events
        /// <summary>
        /// Back button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Storefront/Stores/Default.aspx");
        }

        /// <summary>
        /// Edit click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEdit_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Storefront/Stores/edit.aspx?ItemId=" + this.ItemId);
        }

        /// <summary>
        /// Edit Display button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEditDisplay_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Storefront/Stores/EditDisplay.aspx?ItemId=" + this.ItemId);
        }

        /// <summary>
        /// Edit Unit Settings click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEditUnitSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Storefront/Stores/EditUnits.aspx?ItemId=" + this.ItemId);
        }

        /// <summary>
        /// SMTP Settings click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSMTPSettings_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Storefront/Stores/EditSmtp.aspx?ItemId=" + this.ItemId);
        }

        /// <summary>
        /// SEdit Shipping click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEditShipping_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Storefront/Stores/EditShipping.aspx?ItemId=" + this.ItemId);
        }

        /// <summary>
        /// Edit Analytics button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEditAnalytics_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Storefront/Stores/EditAnalytics.aspx?ItemId=" + this.ItemId);
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Storefront/Stores/Default.aspx");
        }

        /// <summary>
        /// Edit catalog theme
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnEditCatalogTheme_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Storefront/Stores/AddCatalogTheme.aspx?ItemId=" + this.ItemId);
        }

        /// <summary>
        /// Add catalog theme
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LbAddCatalogTheme_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddPortalCatalogLink + this.ItemId);
        }

        /// <summary>
        /// Redirecting to Domain Add page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddNewDomain_Click(object sender, EventArgs e)
        {
            if (!UserStoreAccess.IsMultiStoreAdminEnabled() && uxDomainGrid.Rows.Count >= 1)
            {
                lblDomainError.Text = UserStoreAccess.UpgradeMessage;
            }
            else
            {
                Response.Redirect(this.AddDomainlink + this.ItemId);
            }
        }

        /// <summary>
        /// Redirect to catalog theme page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LbAddCatalogThemen_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddPortalCatalogLink + this.ItemId);
        }

        /// <summary>
        /// Add Profile Button click
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddProfile_Click(object sender, EventArgs e)
        {
            ProfileAdmin profileAdmin = new ProfileAdmin();
            DataSet ds = profileAdmin.GetProfilesByPortalID(this.ItemId);

            if (ds.Tables[0].Rows.Count != 0)
            {
                Response.Redirect(this.Profilelink + this.ItemId);
            }
            else
            {
                lblProfileError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ValidStoreProfileAdd").ToString();
            }
        }

        /// <summary>
        /// Redirecting to add new Related Item for a Product
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddCountries_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddCountriesLink + this.ItemId);
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxCountryGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "RemoveItem")
                {
                    PortalCountryAdmin portalCountryAdmin = new PortalCountryAdmin();

                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                    HiddenField hdnCountry = (HiddenField)row.FindControl("hdnCountryName");
                    string countryName = hdnCountry.Value;
                    this.AssociateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteCountryStore").ToString(), countryName, lblStoreName.Text);

                    GridViewRow row1 = (GridViewRow)((Control)e.CommandSource).NamingContainer;
                    string countryCode = row1.Cells[1].Text;
                    bool canDelete = portalCountryAdmin.IsDeletable(countryCode);
                    if (canDelete)
                    {
                        canDelete = portalCountryAdmin.Delete(int.Parse(e.CommandArgument.ToString()));
                        if (canDelete)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, lblStoreName.Text);
                            this.BindStoreCountries();
                        }
                    }
                    else
                    {
                        lblCountryMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteCountryAssociation").ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Related Store Countries Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxCountryGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxCountryGrid.PageIndex = e.NewPageIndex;
            this.BindStoreCountries();
        }

        /// <summary>
        /// Domain Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxDomainGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxDomainGrid.PageIndex = e.NewPageIndex;
            this.BindDomainData();
        }

        /// <summary>
        /// Doamin Item delete event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxDomainGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindDomainData();
        }

        /// <summary>
        /// Domain Row command
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxDomainGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.AddDomainlink + this.ItemId + "&domainid=" + e.CommandArgument.ToString());
                }

                if (e.CommandName == "RemoveItem")
                {
                    ZNode.Libraries.Admin.DomainAdmin domainAdmin = new DomainAdmin();
                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                    HiddenField hdnDomain = (HiddenField)row.FindControl("hdnDomainName");
                    string domainName = hdnDomain.Value;
                    this.AssociateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteDomain").ToString(), domainName, lblStoreName.Text);
                    bool Status = domainAdmin.Delete(int.Parse(e.CommandArgument.ToString()));
                    if (Status)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, lblStoreName.Text);

                        lblDomainError.Text = string.Empty;
                        this.BindDomainData();
                    }
                }
            }
        }

        /// <summary>
        /// Profile Items Page Index Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxProfileGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxProfileGrid.PageIndex = e.NewPageIndex;
            this.BindAssociatedProfiles();
        }

        /// <summary>
        /// Profile Grid Delete
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxProfileGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindAssociatedProfiles();
        }

        /// <summary>
        /// Catalog Theme Row command
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GvCatalogTheme_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.AddPortalCatalogLink + this.ItemId + "&PortalCatalogId=" + e.CommandArgument.ToString());
                }

                if (e.CommandName == "RemoveItem")
                {
                    MessageConfigAdmin messageadmin = new MessageConfigAdmin();
                    MessageConfig messageconfig = new MessageConfig();

                    GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                    int portalId = int.Parse(row.Cells[5].Text);

                    // Delete the Portal catalog by portal and locale
                    PortalAdmin portalAdmin = new PortalAdmin();
                    this.AssociateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteCatalogStore").ToString(), row.Cells[1].Text, lblStoreName.Text);
                    bool Status = portalAdmin.DeletePortalCatalog(int.Parse(row.Cells[5].Text), int.Parse(row.Cells[6].Text));

                    if (Status)
                    {
                        messageadmin.DeleteMessagesByPortalIDLocalID(int.Parse(row.Cells[5].Text), int.Parse(row.Cells[6].Text));
                        this.BindCatalogTheme();

                        if (this.Catalogds.Tables[0].Rows.Count == 1)
                        {
                            PortalService portalService = new PortalService();
                            Portal portal = new Portal();
                            portal = portalService.GetByPortalID(portalId);
                            portal.LocaleID = int.Parse(this.Catalogds.Tables[0].Rows[0]["LocaleID"].ToString());
                            portalService.Update(portal);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Catalog Theme Row
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GvCatalogTheme_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[5].Visible = false;
                e.Row.Cells[6].Visible = false;
            }
        }

        /// <summary>
        /// Catalog Them next page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GvCatalogTheme_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCatalogTheme.PageIndex = e.NewPageIndex;
            this.BindCatalogTheme();
        }

        /// <summary>
        /// Profile Grid Row command
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxProfileGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                int ProfileId = int.Parse(row.Cells[0].Text);
                HiddenField hdnProfile = (HiddenField)row.FindControl("hdnProfileName");
                string ProfileName = hdnProfile.Value;
                string AssociateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteProfileStore").ToString(), ProfileName, lblStoreName.Text);

                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.Profilelink + this.ItemId + "&Nodeid=" + ProfileId);
                }
                else if (e.CommandName == "Remove")
                {
                    PortalProfileAdmin portalProfileAdmin = new PortalProfileAdmin();

                    bool Status = portalProfileAdmin.Delete(int.Parse(e.CommandArgument.ToString()));

                    if (Status)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, lblStoreName.Text);

                        this.ResetDefaultProfile(this.ItemId, ProfileId);
                    }

                    this.BindAssociatedProfiles();
                }
            }
        }

        /// <summary>
        /// Hide the delete button
        /// </summary>
        /// <param name="DomainName">Domain Name</param>
        /// <returns>Hides the delete button</returns>
        protected bool HideDeleteButton(string DomainName)
        {
            return ZNodeConfigManager.DomainConfig.DomainName != DomainName;
        }

        /// <summary>
        /// Hide Catalog Delete Button
        /// </summary>
        /// <param name="DomainName">Domain name</param>
        /// <returns>Hides the catalog delete button</returns>
        protected bool HideCatalogDeleteButton(string DomainName)
        {
            if (this.Catalogds.Tables[0].Rows.Count == 1)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Bind the data
        /// </summary>
        protected void Bind()
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = storeAdmin.GetByPortalId(this.ItemId);

            if (portal != null)
            {
                lblCompanyName.Text = portal.CompanyName;
                lblStoreName.Text = portal.StoreName;

                // Get the catalogId based on PortalID
                int CatalogIdValue = this.catalogAdmin.GetCatalogIDByPortalID(this.ItemId);

                // Set the Preselected Value.
                if (CatalogIdValue != 0)
                {
                    PortalCatalogService portalCatalogService = new PortalCatalogService();
                    TList<PortalCatalog> portalCatalogList = portalCatalogService.GetByPortalID(this.ItemId);

                    if (portalCatalogList.Count > 0)
                    {
                        portalCatalogService.DeepLoad(portalCatalogList, true, ZNode.Libraries.DataAccess.Data.DeepLoadType.IncludeChildren, typeof(Catalog));
                    }
                }

                if (portal.LocaleID > 0)
                {
                    // Load default locale
                    LocaleService localeService = new LocaleService();
                    Locale locale = localeService.GetByLocaleID(Convert.ToInt32(portal.LocaleID));
                    lblDefaultLocale.Text = locale.LocaleDescription;
                }
                else
                {
                    lblDefaultLocale.Text = string.Empty;
                }

                lblAdminEmail.Text = portal.AdminEmail;
                lblSalesEmail.Text = portal.SalesEmail;
                lblCustomerServiceEmail.Text = portal.CustomerServiceEmail;
                lblSalesPhoneNumber.Text = portal.SalesPhoneNumber;
                lblCustomerServicePhoneNumber.Text = portal.CustomerServicePhoneNumber;

                // Load the resized store logo from Content folder.
                ZNodeImage znodeImage = new ZNodeImage();
                System.Drawing.Size dimension = new System.Drawing.Size();
                dimension.Width = ZNodeConfigManager.SiteConfig.MaxCatalogItemSmallWidth;
                dimension.Height = ZNodeConfigManager.SiteConfig.MaxCatalogItemSmallWidth;

                if (!string.IsNullOrEmpty(portal.LogoPath))
                {
                    if (portal.LogoPath.Contains("~/Content/"))
                    {
                        imgLogo.ImageUrl = znodeImage.GetImageHttpPathSmall(Path.GetFileName(portal.LogoPath));
                    }
                    else
                    {
                        imgLogo.ImageUrl = znodeImage.GetImageHttpPathSmall(string.Format("Turnkey/{0}/{1}", portal.PortalID, Path.GetFileName(portal.LogoPath)));
                    }
                }
                else
                {
                    imgLogo.ImageUrl = znodeImage.GetImageHttpPathSmall(string.Empty);
                }

                chkEnableSSL.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.UseSSL);
                chkInclusiveTax.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.InclusiveTax);

                chkPersistentCart.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.PersistentCartEnabled.GetValueOrDefault(false));
                imgEnableAddressValidation.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.EnableAddressValidation.GetValueOrDefault(false));
                imgRequireValidatedAddress.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.RequireValidatedAddress.GetValueOrDefault(false));
				imgEnableCustomerBasedPricing.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.EnableCustomerPricing.GetValueOrDefault(false));
              //  imgEnablePIMS.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.EnablePIMS.GetValueOrDefault(false));

                if (portal.DefaultOrderStateID.HasValue)
                {
                    OrderStateService serv = new OrderStateService();
                    OrderState ordState = serv.GetByOrderStateID(portal.DefaultOrderStateID.Value);
                    lblDefaultOrderStatus.Text = ordState.OrderStateName;
                }

                if (!string.IsNullOrEmpty(portal.DefaultReviewStatus))
                {
                    if (portal.DefaultReviewStatus == "A")
                    {
                        lblDefaultReviewStatus.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextPublishImmediately").ToString();
                    }
                    else if (portal.DefaultReviewStatus == "N")
                    {
                        lblDefaultReviewStatus.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextDoNotPublish").ToString();
                    }
                }

                // If default product review state is null the set it to PendingApproval.
                if (portal.DefaultProductReviewStateID == null)
                {
                    portal.DefaultProductReviewStateID = (int)ZNodeProductReviewState.PendingApproval;
                }

                switch ((ZNodeProductReviewState)portal.DefaultProductReviewStateID)
                {
                    case ZNodeProductReviewState.PendingApproval:
                        lblDefaultProductReviewStatus.Text = "PENDING APPROVAL";
                        break;
                    case ZNodeProductReviewState.Approved:
                        lblDefaultProductReviewStatus.Text = "APPROVED";
                        break;
                    case ZNodeProductReviewState.Declined:
                        lblDefaultProductReviewStatus.Text = "DECLINED";
                        break;
                    case ZNodeProductReviewState.ToEdit:
                        lblDefaultProductReviewStatus.Text = "TO EDIT";
                        break;
                }

                // Display Tab
                lblMaxCatalogDisplayColumns.Text = portal.MaxCatalogDisplayColumns.ToString();
                lblMaxSmallThumbnailsDisplay.Text = portal.MaxCatalogCategoryDisplayThumbnails.ToString();
                chkDynamicDisplayOrder.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.UseDynamicDisplayOrder.GetValueOrDefault(false));
                lblMaxCatalogItemLargeWidth.Text = portal.MaxCatalogItemLargeWidth.ToString();
                lblMaxCatalogItemMediumWidth.Text = portal.MaxCatalogItemMediumWidth.ToString();
                lblMaxCatalogItemSmallWidth.Text = portal.MaxCatalogItemSmallWidth.ToString();
                lblMaxCatalogItemCrossSellWidth.Text = portal.MaxCatalogItemCrossSellWidth.ToString();
                lblMaxCatalogItemThumbnailWidth.Text = portal.MaxCatalogItemThumbnailWidth.ToString();
                lblMaxCatalogItemSmallThumbnailWidth.Text = portal.MaxCatalogItemSmallThumbnailWidth.ToString();
                ItemImage.ImageUrl = znodeImage.GetImageHttpPathSmall("Turnkey/" + portal.PortalID + "/" + Path.GetFileName(portal.ImageNotAvailablePath));

                // Units Tab
                lblWeightUnit.Text = portal.WeightUnit;
                lblDimensionUnit.Text = portal.DimensionUnit;
                lblCurrency.Text = this.PriceFormat(portal.CurrencyTypeID.GetValueOrDefault(0));

                // Shipping Tab
                lblShippingAddress1.Text = portal.ShippingOriginAddress1;
                lblShippingAddress2.Text = portal.ShippingOriginAddress2;
                lblShippingCity.Text = portal.ShippingOriginCity;
                lblShippingPhone.Text = portal.ShippingOriginPhone;
                lblShippingZipCode.Text = portal.ShippingOriginZipCode;
                lblShippingStateCode.Text = portal.ShippingOriginStateCode;
                lblOriginCountryCode.Text = portal.ShippingOriginCountryCode;

                chkAddInsurance.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.FedExAddInsurance.GetValueOrDefault());
                chkFedExDiscountRate.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(portal.FedExUseDiscountRate.GetValueOrDefault());

                try
                {
                    ZNodeEncryption encrypt = new ZNodeEncryption();

                    // SMTP
                    lblSMTPPort.Text = portal.SMTPPort.GetValueOrDefault(25).ToString();

                    if (portal.SMTPServer != null)
                    {
                        lblSMTPServer.Text = portal.SMTPServer;
                    }

                    if (portal.SMTPUserName != null)
                    {
                        lblSMTPUserName.Text = Server.HtmlEncode(encrypt.DecryptData(portal.SMTPUserName));
                    }

                    if (portal.SMTPPassword != null)
                    {
                        string password = encrypt.DecryptData(portal.SMTPPassword);

                        // Display the masked password 
                        lblSMTPPassword.Text = password.Trim().Length > 0 ? new string('*', password.Length) : string.Empty;
                    }

                    // Set UPS Account details
                    if (portal.UPSUserName != null)
                    {
                        lblUPSUserName.Text = Server.HtmlEncode(encrypt.DecryptData(portal.UPSUserName));
                    }

                    if (portal.UPSPassword != null)
                    {
                        this.UPSPassword = encrypt.DecryptData(portal.UPSPassword);
                        lblUPSPassword.Text = this.UPSPassword.Trim().Length > 0 ? new string('*', this.UPSPassword.Length) : string.Empty;
                    }

                    if (portal.UPSKey != null)
                    {
                        lblUPSKey.Text = Server.HtmlEncode(encrypt.DecryptData(portal.UPSKey));
                    }

                    // Bind FedEx Account details
                    if (portal.FedExAccountNumber != null)
                    {
                        lblAccountNum.Text = Server.HtmlEncode(encrypt.DecryptData(portal.FedExAccountNumber));
                    }

                    if (portal.FedExMeterNumber != null)
                    {
                        lblMeterNum.Text = Server.HtmlEncode(encrypt.DecryptData(portal.FedExMeterNumber));
                    }

                    if (portal.FedExProductionKey != null)
                    {
                        lblProductionAccessKey.Text = Server.HtmlEncode(encrypt.DecryptData(portal.FedExProductionKey));
                    }

                    if (portal.FedExSecurityCode != null)
                    {
                        lblSecurityCode.Text = Server.HtmlEncode(encrypt.DecryptData(portal.FedExSecurityCode));
                    }

                    if (portal.FedExPackagingType != null)
                    {
                        lblPackageTypeCodes.Text = portal.FedExPackagingType;
                    }

                    if (portal.FedExDropoffType != null)
                    {
                        lbldropOffTypes.Text = portal.FedExDropoffType;
                    }
                }
                catch
                {
                    // ignore decryption errors
                }

                // Analytics
                if (portal.SiteWideBottomJavascript != null)
                {
                    lblSiteWideBottomJavaScript.Text = Server.HtmlEncode(portal.SiteWideBottomJavascript.ToString());
                }

                if (portal.SiteWideTopJavascript != null)
                {
                    lblSiteWideTopJavaScript.Text = Server.HtmlEncode(portal.SiteWideTopJavascript.ToString());
                }

                if (portal.SiteWideAnalyticsJavascript != null)
                {
                    lblSiteWideAnalyticsJavascript.Text = Server.HtmlEncode(portal.SiteWideAnalyticsJavascript.ToString());
                }

                if (portal.OrderReceiptAffiliateJavascript != null)
                {
                    lblOrderReceiptJavaScript.Text = Server.HtmlEncode(portal.OrderReceiptAffiliateJavascript.ToString());
                }
            }
            else
            {
                Response.Redirect("list.aspx");
            }
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Bind Domain Data 
        /// </summary>
        private void BindDomainData()
        {
            DomainAdmin domainAdmin = new DomainAdmin();
            uxDomainGrid.DataSource = domainAdmin.DeepLoadDomainsByPortalID(this.ItemId);
            uxDomainGrid.DataBind();
        }

        /// <summary>
        /// Bind Catalog Theme
        /// </summary>
        private void BindCatalogTheme()
        {
            PortalCatalogHelper portalCatalogHelper = new PortalCatalogHelper();
            this.Catalogds = portalCatalogHelper.GetPortalCatalog(this.ItemId);
            gvCatalogTheme.DataSource = this.Catalogds;
            gvCatalogTheme.DataBind();
        }

        /// <summary>
        /// Bind Associated Profiles
        /// </summary>
        private void BindAssociatedProfiles()
        {
            ProfileAdmin profileAdmin = new ProfileAdmin();
            uxProfileGrid.DataSource = profileAdmin.GetAssociatedProfilesByPortalID(this.ItemId);
            uxProfileGrid.DataBind();
        }

        /// <summary>
        /// Set the mobile settings page visibility. If service(*.svc) doesn't exist then hide the Mobile Settings tab.
        /// </summary>
        private void SetMobileSettingsVisibility()
        {
            string catalogServiceFileName = Path.Combine(Server.MapPath("~/Services"), "Catalog.svc");
            tabMobileSettings.Visible = File.Exists(catalogServiceFileName);
        }

        /// <summary>
        /// Display Price in specified culture format
        /// </summary>
        /// <param name="CurrencyTypeId">Currency Type Id</param>
        /// <returns>Returns the price format</returns>
        private string PriceFormat(int CurrencyTypeId)
        {
            string val = string.Empty;
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            CurrencyType currencyType = storeAdmin.GetByCurrencyTypeID(CurrencyTypeId);

            if (currencyType != null)
            {
                string currencySymbol = currencyType.Name;

                CultureInfo info = new CultureInfo(currencySymbol);

                decimal price = 100.12M;

                val = price.ToString("c", info.NumberFormat);

                if (!string.IsNullOrEmpty(currencyType.CurrencySuffix))
                {
                    val += " (" + currencyType.CurrencySuffix + ")";
                }
            }

            return val;
        }

        /// <summary>
        /// Reset the default profile
        /// </summary>
        /// <param name="PortalID">Portal Id for the profile</param>
        /// <param name="ProfileID">Profile Id for the profile</param>
        private void ResetDefaultProfile(int PortalID, int ProfileID)
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            PortalService portalService = new PortalService();
            Portal portal = portalAdmin.GetByPortalId(PortalID);

            if (portal.DefaultAnonymousProfileID == ProfileID)
            {
                portal.DefaultAnonymousProfileID = null;
            }

            if (portal.DefaultRegisteredProfileID == ProfileID)
            {
                portal.DefaultRegisteredProfileID = null;
            }

            portalService.Update(portal);
        }

        /// <summary>
        /// This will automatically pre-select the tab according the query string value(mode)
        /// </summary>
        private void ResetTab()
        {
            if (this.Mode.Equals("Domain"))
            {
                tabStoreSettings.ActiveTabIndex = 2;
            }
            else if (this.Mode.Equals("catalog"))
            {
                tabStoreSettings.ActiveTabIndex = 1;
            }
            else if (this.Mode.Equals("countries"))
            {
                tabStoreSettings.ActiveTabIndex = 6;
            }
            else if (this.Mode.Equals("profiles"))
            {
                tabStoreSettings.ActiveTabIndex = 3;
            }
            else if (this.Mode.Equals("display"))
            {
                tabStoreSettings.ActiveTabIndex = 4;
            }
            else if (this.Mode.Equals("smtp"))
            {
                tabStoreSettings.ActiveTabIndex = 9;
            }
            else if (this.Mode.Equals("unit"))
            {
                tabStoreSettings.ActiveTabIndex = 5;
            }
            else if (this.Mode.Equals("ship"))
            {
                tabStoreSettings.ActiveTabIndex = 7;
            }
            else if (this.Mode.Equals("analytics"))
            {
                tabStoreSettings.ActiveTabIndex = 8;
            }
            else if (this.Mode.Equals("mobilesettings"))
            {
                tabStoreSettings.ActiveTabIndex = 10;
            }
        }
        #endregion
    }
}
