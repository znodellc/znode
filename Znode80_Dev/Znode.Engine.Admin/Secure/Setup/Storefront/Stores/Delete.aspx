<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" Title="Manage Stores - Delete" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Stores.Delete" CodeBehind="Delete.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h5>
        <asp:Localize runat="server" ID="TextPleaseConfirm" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm %>'></asp:Localize>
    </h5>

    <p>
        <asp:Localize runat="server" ID="TextDeleteStoreConfirm" Text='<%$ Resources:ZnodeAdminResource, TextDeleteStoreConfirm %>'></asp:Localize><b><%=StoreName%></b><asp:Localize runat="server" ID="TextChangeUndone" Text='<%$ Resources:ZnodeAdminResource, TextChangeUndone %>'></asp:Localize>
    </p>
    <p>
        <asp:Localize runat="server" ID="TextItemsIncluded" Text='<%$ Resources:ZnodeAdminResource, TextItemsIncluded %>'></asp:Localize>
    </p>
    <ul>
        <li>
            <asp:Localize runat="server" ID="TextOrders" Text='<%$ Resources:ZnodeAdminResource, TextOrders %>'></asp:Localize></li>
        <li>
            <asp:Localize runat="server" ID="TextAccountPayments" Text='<%$ Resources:ZnodeAdminResource, TextAccountPayments %>'></asp:Localize></li>
        <li>
            <asp:Localize runat="server" ID="TextTaxRuleAssociation" Text='<%$ Resources:ZnodeAdminResource, TextTaxRuleAssociation %>'></asp:Localize></li>
        <li>
            <asp:Localize runat="server" ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, TextStoreTracking %>'></asp:Localize></li>
        <li>
            <asp:Localize runat="server" ID="Localize2" Text='<%$ Resources:ZnodeAdminResource, TextCaseRequests %>'></asp:Localize></li>
        <li>
            <asp:Localize runat="server" ID="Localize3" Text='<%$ Resources:ZnodeAdminResource, TextContentPages %>'></asp:Localize></li>
        <li>
            <asp:Localize runat="server" ID="Localize4" Text='<%$ Resources:ZnodeAdminResource, TextStoreInfo %>'></asp:Localize></li>
    </ul>
    <asp:Label CssClass="Error" ID="lblErrorMessage" runat="server" Text=''></asp:Label>
    <div>
        <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>
    <div>
        <zn:Button runat="server" ID="btnDelete" ButtonType="CancelButton" OnClick="BtnDelete_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>' CausesValidation="true" />
        <zn:Button runat="server" ID="btnCancel" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
    </div>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="25" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>
</asp:Content>
