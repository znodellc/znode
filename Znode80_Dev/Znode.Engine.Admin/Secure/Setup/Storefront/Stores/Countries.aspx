<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/Themes/Standard/edit.master"
    Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Stores.Countries" Title="Manage Stores - Add Countries" CodeBehind="Countries.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Localize runat="server" ID="TitleAddCountries" Text='<%$ Resources:ZnodeAdminResource, TitleAddCountries %>'></asp:Localize>
            </h1>
        </div>
        <div style="text-align: right; padding-right: 10px;">
            <zn:Button runat="server" ID="btnBack" ButtonType="EditButton" OnClick="BtnBack_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonBack %>' CausesValidation="False" Width="100px" />
        </div>

        <!-- Search product panel -->
        <asp:Panel ID="pnlSearch" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <h4 class="SubTitle">
                    <asp:Localize runat="server" ID="SubtitleSearchCountry" Text='<%$ Resources:ZnodeAdminResource, SubtitleSearchCountry %>'></asp:Localize>
                </h4>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="ColumnTitleKeyword" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleKeyword %>'></asp:Localize>
                        </span>
                        <br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtkeyword" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                    <zn:Button runat="server" ID="btnSearch" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                </div>
            </div>
        </asp:Panel>
        <br />
        <div>
            <asp:Label ID="lblError" runat="server" Text="" Visible="false" CssClass="Error"
                ForeColor="red"></asp:Label>
        </div>
        <!-- Product List -->
        <asp:Panel ID="pnlCountryList" runat="server" Visible="false">
            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="SubTitleCountryList" Text='<%$ Resources:ZnodeAdminResource, SubTitleCountryList %>'></asp:Localize>
            </h4>
            <p>
                <asp:Localize runat="server" ID="TextCountryList" Text='<%$ Resources:ZnodeAdminResource, TextCountryList %>'></asp:Localize>
            </p>
            <div align="right">
                <zn:Button runat="server" ID="Button1" ButtonType="SubmitButton" OnClick="AddNew_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
                <zn:Button runat="server" ID="Button2" ButtonType="CancelButton" OnClick="Cancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
            </div>
            <div>
                <ZNode:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>

            <div>
                <ZNode:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <asp:GridView ID="uxCountryGrid" runat="server" CssClass="Grid" CaptionAlign="Left"
                AutoGenerateColumns="False" GridLines="None" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyNoCountriesText %>'
                Width="100%" CellPadding="4">
                <Columns>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSelect %>' Visible="false">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkCountry" runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayCountry %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkBillingActive" runat="server" Checked='<%# bool.Parse(DataBinder.Eval(Container.DataItem,"EnableCountry").ToString())%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleAllowShipping %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkShippingActive" runat="server" Checked='<%# bool.Parse(DataBinder.Eval(Container.DataItem,"EnableShipping").ToString())%>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Code" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCountryCode %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="Name" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCountryName %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsActive %>' Visible="false" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="chMark" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <zn:Button runat="server" ID="btnAddNew" ButtonType="SubmitButton" OnClick="AddNew_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
                <zn:Button runat="server" ID="btnCancel" ButtonType="CancelButton" OnClick="Cancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
            </div>
        </asp:Panel>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
    </div>
</asp:Content>
