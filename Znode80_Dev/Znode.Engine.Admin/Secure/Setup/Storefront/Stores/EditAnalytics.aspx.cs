using System;
using System.Web;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the Admin_Secure_settings_Stores_EditAnalytics class.
    /// </summary>
    public partial class EditAnalytics : System.Web.UI.Page
    {
        #region Private Member Variables
        private int itemId = 0;
        private string redirectUrl = "view.aspx?ItemId={0}&mode=analytics";
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get portalId value from querystring
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.Bind();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click Evnet
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = null;
            bool isUpdated = false;

            if (this.itemId > 0)
            {
                portal = storeAdmin.GetByPortalId(this.itemId);
            }

            if (portal != null)
            {
                portal.SiteWideBottomJavascript = Server.HtmlDecode(txtSiteWideBottomJavaScript.Text);
                portal.SiteWideTopJavascript = Server.HtmlDecode(txtSiteWideTopJavaScript.Text);
                portal.SiteWideAnalyticsJavascript = Server.HtmlDecode(txtSiteWideAnalyticsJavascript.Text);
                portal.OrderReceiptAffiliateJavascript = Server.HtmlDecode(txtOrderReceiptJavaScript.Text);

                isUpdated = storeAdmin.UpdateStore(portal);
            }

            if (isUpdated)
            {
                // Log Activity
                string associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditofJavascriptcode").ToString() + portal.StoreName;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, portal.StoreName);

                Response.Redirect(string.Format(this.redirectUrl, this.itemId));
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAnalyticsCode").ToString();

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeFailed, HttpContext.Current.User.Identity.Name);
            }
        }

        /// <summary>
        /// Cancel button click event.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format(this.redirectUrl, this.itemId));
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind the google analytics data.
        /// </summary>
        private void Bind()
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();

            // Set default Value from Store1 For Add page
            Portal portal = storeAdmin.GetByPortalId(this.itemId);

            if (portal != null)
            {
                lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleJavaScript").ToString() + " \"" + portal.StoreName + "\"";
                if (portal.SiteWideBottomJavascript != null)
                {
                    txtSiteWideBottomJavaScript.Text = Server.HtmlDecode(portal.SiteWideBottomJavascript.ToString());
                }

                if (portal.SiteWideTopJavascript != null)
                {
                    txtSiteWideTopJavaScript.Text = Server.HtmlDecode(portal.SiteWideTopJavascript.ToString());
                }

                if (portal.SiteWideAnalyticsJavascript != null)
                {
                    txtSiteWideAnalyticsJavascript.Text = Server.HtmlDecode(portal.SiteWideAnalyticsJavascript.ToString());
                }

                if (portal.OrderReceiptAffiliateJavascript != null)
                {
                    txtOrderReceiptJavaScript.Text = Server.HtmlDecode(portal.OrderReceiptAffiliateJavascript.ToString());
                }
            }
        }
        #endregion
    }
}
