<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" Title="Manage Stores - Edit SMTP" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Stores.EditSmtp" CodeBehind="EditSmtp.aspx.cs" ValidateRequest="false" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right; padding-right: 10px;">
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" ID="btnSubmitTop" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelTop" />
        </div>
        <div class="ClearBoth">
        </div>
        <div align="left">
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <br />
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleSmtp" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSmtp%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSMTPPort" runat="server" Width="152px" Text="25"></asp:TextBox>
            <asp:RegularExpressionValidator ID="Regularexpressionvalidator1" runat="server" ControlToValidate="txtSMTPPort"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularSmtpPort%>'
                Display="Dynamic" ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularValidSmtp%>'
                ValidationGroup="smtp" CssClass="Error"></asp:RegularExpressionValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleServer" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSmtpServer%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSMTPServer" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleSmtpUser" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSmtpUser%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSMTPUserName" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ColumnTitleSmtpPassword" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSmtpPassword%>' runat="server"></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSMTPPassword" TextMode="Password" runat="server" Width="152px"></asp:TextBox>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" ValidationGroup="smtp" ID="btnSubmitBottom" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelBottom" />
        </div>
    </div>
</asp:Content>
