using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Setup.Storefront.Stores
{
    /// <summary>
    /// Represents the Admin_Secure_settings_Stores_EditShipping class.
    /// </summary>
    public partial class EditShipping : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int itemId = 0;
        private string redirectUrl = "view.aspx?ItemId={0}&mode=ship";
        #endregion

        #region Page Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get portalId value from querystring
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindCountry();
                this.Bind();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit Button click Event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
            Portal portal = null;
            bool isUpdated = false;

            if (this.itemId > 0)
            {
                portal = storeAdmin.GetByPortalId(this.itemId);
            }

            if (portal != null)
            {
                ZNodeEncryption encryption = new ZNodeEncryption();

                // Shipping Settings        
                portal.ShippingOriginAddress1 = Server.HtmlEncode(txtShippingAddress1.Text.Trim());
                portal.ShippingOriginAddress2 = Server.HtmlEncode(txtShippingAddress2.Text.Trim());
                portal.ShippingOriginCity = Server.HtmlEncode(txtShippingCity.Text.Trim());
                portal.ShippingOriginPhone = Server.HtmlEncode(txtShippingPhone.Text.Trim());

                portal.ShippingOriginZipCode = Server.HtmlEncode(txtShippingZipCode.Text.Trim());
                portal.ShippingOriginStateCode = Server.HtmlEncode(txtShippingStateCode.Text.Trim());
                portal.ShippingOriginCountryCode = lstCountries.SelectedValue;

                portal.FedExDropoffType = ddldropOffTypes.SelectedItem.Value;
                portal.FedExPackagingType = ddlPackageTypeCodes.SelectedItem.Value;
                portal.FedExUseDiscountRate = chkFedExDiscountRate.Checked;
                portal.FedExAddInsurance = chkAddInsurance.Checked;

                // UPS Shipping Settings
                portal.UPSUserName = encryption.EncryptData(txtUPSUserName.Text.Trim());
                portal.UPSPassword = encryption.EncryptData(txtUPSPassword.Text.Trim());
                portal.UPSKey = encryption.EncryptData(txtUPSKey.Text.Trim());

                // FedEx Shipping Settings
                portal.FedExAccountNumber = encryption.EncryptData(txtAccountNum.Text.Trim());
                portal.FedExMeterNumber = encryption.EncryptData(txtMeterNum.Text.Trim());
                portal.FedExProductionKey = encryption.EncryptData(txtProductionAccessKey.Text.Trim());
                portal.FedExSecurityCode = encryption.EncryptData(txtSecurityCode.Text.Trim());

                isUpdated = storeAdmin.UpdateStore(portal);
            }

            if (isUpdated)
            {
                // Log Activity
                string associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "LogActivityEditShipping") + portal.StoreName;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, portal.StoreName);
                Response.Redirect(string.Format(this.redirectUrl, this.itemId));
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorShippingSetting").ToString();

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeFailed, HttpContext.Current.User.Identity.Name);
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format(this.redirectUrl, this.itemId));
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind the store shipping details.
        /// </summary>
        private void Bind()
        {
            StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();

            // Set default Value from Store1 For Add page
            Portal portal = storeAdmin.GetByPortalId(this.itemId);

            if (portal != null)
            {
                try
                {
                    lblTitle.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditShipping").ToString(), portal.StoreName); 
                    ZNodeEncryption encryption = new ZNodeEncryption();

                    // Set UPS Account details
                    if (portal.UPSUserName != null)
                    {
                        txtUPSUserName.Text = encryption.DecryptData(portal.UPSUserName);
                    }

                    if (portal.UPSPassword != null)
                    {
                        txtUPSPassword.Text = encryption.DecryptData(portal.UPSPassword);
                    }

                    if (portal.UPSKey != null)
                    {
                        txtUPSKey.Text = encryption.DecryptData(portal.UPSKey);
                    }

                    // Bind FedEx Account details
                    if (portal.FedExAccountNumber != null)
                    {
                        txtAccountNum.Text = encryption.DecryptData(portal.FedExAccountNumber);
                    }

                    if (portal.FedExMeterNumber != null)
                    {
                        txtMeterNum.Text = encryption.DecryptData(portal.FedExMeterNumber);
                    }

                    if (portal.FedExProductionKey != null)
                    {
                        txtProductionAccessKey.Text = encryption.DecryptData(portal.FedExProductionKey);
                    }

                    if (portal.FedExSecurityCode != null)
                    {
                        txtSecurityCode.Text = encryption.DecryptData(portal.FedExSecurityCode);
                    }

                    if (portal.FedExUseDiscountRate.HasValue)
                    {
                        chkFedExDiscountRate.Checked = portal.FedExUseDiscountRate.Value;
                    }

                    if (portal.FedExAddInsurance.HasValue)
                    {
                        chkAddInsurance.Checked = portal.FedExAddInsurance.Value;
                    }

                    if (portal.FedExPackagingType != null)
                    {
                        ddlPackageTypeCodes.SelectedValue = portal.FedExPackagingType;
                    }

                    if (portal.FedExDropoffType != null)
                    {
                        ddldropOffTypes.SelectedValue = portal.FedExDropoffType;
                    }
                }
                catch
                {
                    // Ignore decryption errors
                }

                txtShippingAddress1.Text = Server.HtmlDecode(portal.ShippingOriginAddress1);
                txtShippingAddress2.Text = Server.HtmlDecode(portal.ShippingOriginAddress2);
                txtShippingCity.Text = Server.HtmlDecode(portal.ShippingOriginCity);
                txtShippingPhone.Text = Server.HtmlDecode(portal.ShippingOriginPhone);
                txtShippingZipCode.Text = Server.HtmlDecode(portal.ShippingOriginZipCode);
                txtShippingStateCode.Text = Server.HtmlDecode(portal.ShippingOriginStateCode);
                ListItem listItem = lstCountries.Items.FindByValue(portal.ShippingOriginCountryCode);
                if (listItem != null)
                {
                    lstCountries.SelectedIndex = lstCountries.Items.IndexOf(listItem);
                }
            }
        }

        /// <summary>
        /// Binds country drop-down list
        /// </summary>
        private void BindCountry()
        {
            ShippingAdmin shipAdmin = new ShippingAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Country> countries = shipAdmin.GetDestinationCountries();

            lstCountries.DataSource = countries;
            lstCountries.DataTextField = "Code";
            lstCountries.DataValueField = "Code";
            lstCountries.DataBind();
            lstCountries.Items.RemoveAt(0);
        }
        #endregion
    }
}