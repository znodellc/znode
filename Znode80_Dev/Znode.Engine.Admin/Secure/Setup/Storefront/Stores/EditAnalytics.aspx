<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True"
    ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Stores.EditAnalytics" Title="Manage Stores - Edit Analytics"
    CodeBehind="EditAnalytics.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView Size350">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right; padding-right: 10px;"> 

            <zn:Button ID="btnSubmitTop" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
             <zn:Button ID="btnCancelTop" runat="server" ButtonType="CancelButton"  CausesValidation="false" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
       
        </div>
        <div align="left">
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="SubTitleJavaScript" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleJavaScript %>'></asp:Localize></h4>


        <div class="FieldStyle">
            <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSiteWideJaveScript %>'></asp:Localize><br />
            <small>
                <asp:Localize ID="SiteWideJscriptContents" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextSiteWideJscriptContents %>'></asp:Localize><br />
                <asp:Localize ID="SiteWideJscriptUsing" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextSiteWideJscriptUsing %>'></asp:Localize>
                <strong>
                    <asp:Localize ID="TextGoogleAnalytics" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextGoogleAnalytics %>'></asp:Localize></strong><asp:Localize ID="TextEnableEcommerce" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextEnableEcommerce %>'></asp:Localize>
                <br />
                <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextSiteWideJscriptUsing %>'></asp:Localize>
                <strong>
                    <asp:Localize ID="TextListrak" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextListrak %>'></asp:Localize></strong>
                <asp:Localize ID="TextTrackCustomer" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextTrackCustomer %>'></asp:Localize><br />
                <asp:Localize ID="TextJavaScripttop" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextJavaScripttop %>'></asp:Localize></small>
        </div>


        <div class="ValueStyle">
            <asp:TextBox ID="txtSiteWideTopJavaScript" runat="server" Height="150px" TextMode="MultiLine"
                Width="450px"></asp:TextBox><br />
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="SiteWideJaveScriptBottom" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSiteWideJaveScriptBottom %>'></asp:Localize>
            <br />
            <small>
                <asp:Localize ID="TextContentBottom" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextContentBottom %>'></asp:Localize>
                <br />

                <asp:Localize ID="WideJscriptUsing" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextSiteWideJscriptUsing %>'></asp:Localize>

                <strong>
                    <asp:Localize ID="Localize4" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextGoogleAnalytics %>'></asp:Localize>

                </strong>

                <asp:Localize ID="EnableEcommerce" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextEnableEcommerce %>'></asp:Localize>
                <br />
                <asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextSiteWideJscriptUsing %>'></asp:Localize>


                <strong>
                    <asp:Localize ID="Localize5" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextListrak %>'></asp:Localize>

                </strong>
                <asp:Localize ID="TrackCustomer" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextTrackCustomer %>'></asp:Localize>

                <br />
                <asp:Localize ID="JavaScripttop" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextJavaScripttop %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSiteWideBottomJavaScript" runat="server" Height="150px" TextMode="MultiLine"
                Width="450px"></asp:TextBox><br />
        </div>
        <div class="FieldStyle"> 
             <asp:Localize ID="OrderReceiptPage" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderReceiptPage %>'></asp:Localize><br />
            <small>   <asp:Localize ID="Localize9" runat ="server" Text='<%$ Resources:ZnodeAdminResource, TextJavaScriptContents %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtSiteWideAnalyticsJavascript" runat="server" Height="150px" TextMode="MultiLine"
                Width="450px"></asp:TextBox><br />
        </div>
        <div class="FieldStyle">
              <asp:Localize ID="Localize7" runat ="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderReceiptJavascript %>'></asp:Localize><br />
            <small> 
 <asp:Localize ID="ContentOrderReceipt" runat ="server" Text='<%$ Resources:ZnodeAdminResource, TextContentOrderReceipt %>'></asp:Localize>
            </small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtOrderReceiptJavaScript" runat="server" Height="150px" TextMode="MultiLine"
                Width="450px"></asp:TextBox><br />
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
          
            <zn:Button ID="btnSubmitBottom" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
             <zn:Button ID="btnCancelBottom" runat="server" ButtonType="CancelButton"  CausesValidation="false" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
       
        </div>
    </div>
</asp:Content>
