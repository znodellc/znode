<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Stores.AddPortalCatalog" ValidateRequest="false"
    Title="Manage Stores - Add PortalCatalog" CodeBehind="AddPortalCatalog.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server" /></h1>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnBack_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonBack%>' ID="btnBack" />
        </div>
        <div class="ClearBoth">
        </div>
        <!-- Profile List -->
        <asp:Panel ID="pnlProfileList" runat="server">
            <asp:UpdatePanel ID="updCatalog" runat="server">
                <ContentTemplate>
                    <div class="FieldStyle">
                        <asp:Localize ID="ColumnCatalog" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCatalog%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:DropDownList ID="ddlCatalog" runat="server" OnSelectedIndexChanged="DdlCatalog_SelectedIndexChanged" AutoPostBack="true" />
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnTheme" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTheme%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlTheme" runat="server" OnSelectedIndexChanged="DdlTheme_SelectedIndexChanged"
                    AutoPostBack="True" />
            </div>
            <asp:Panel ID="pnlCSS" runat="server">
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnStyleSheet" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStyleSheet%>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlCSS" runat="server" />
                </div>
            </asp:Panel>
            <div class="FieldStyle" style="display: none;">
                <asp:Localize ID="ColumnLocale" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLocale%>'></asp:Localize>
            </div>
            <div class="ValueStyle" style="display: none;">
                <asp:DropDownList ID="ddlLocale" runat="server" Visible="false" />
            </div>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                    ForeColor="red"></asp:Label>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmitBottom_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmitBottom" CausesValidation="true" />
                <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancelBottom_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelBottom" />
            </div>
        </asp:Panel>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
    </div>
</asp:Content>
