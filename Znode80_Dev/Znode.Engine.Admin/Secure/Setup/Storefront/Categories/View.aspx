<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Categories.View" ValidateRequest="false"
    Title="Manage Categories - View" CodeBehind="View.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<%@ Register Src="Manage.ascx" TagName="Manage" TagPrefix="ZNode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <script language="javascript" type="text/javascript">
        function ProductDeleteConfirmation() {
            return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","TextConfirmDeleteProduct")) %>');
         }
    </script>
    <asp:UpdatePanel ID="UpdatePanelView" runat="server" UpdateMode="Conditional" ChildrenAsTriggers="true"
        RenderMode="Inline">
        <ContentTemplate>
            <div>
                <div class="LeftFloat" style="width: 80%">
                    <h1>
                        <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
                </div>
                <div class="LeftFloat" style="width: 19%" align="right">
                    <zn:Button runat="server" ID="btnCancelTop" Width="150px" OnClick="BtnCancel_Click" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonBackToList %>' CausesValidation="False" />
                </div>
            </div>
            <div class="ClearBoth" align="left">
                <br />
            </div>
            <div>
                <asp:Label ID="lblMsg" CssClass="Error" runat="server"></asp:Label>
            </div>
            <div>
                <uc1:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <div>
                <ajaxToolKit:TabContainer ID="tabCategory" runat="server">
                    <ajaxToolKit:TabPanel ID="pnlCategory" runat="server">
                        <HeaderTemplate>
                            <asp:Localize ID="CategoryInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleCategoryInformation %>'></asp:Localize>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <div class="ViewForm200">
                                <uc1:Spacer ID="Spacer15" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                                <div>
                                    <zn:Button runat="server" ID="btnEditCategory" OnClick="BtnEditCategory_Click" Width="130px" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonEditInformation %>' CausesValidation="False" />
                                </div>
                                <br />
                                <h4 class="SubTitle">
                                    <asp:Localize ID="GeneralSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralSettings %>'></asp:Localize></h4>
                                <div class="FieldStyle">
                                    <asp:Localize ID="CategoryName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCategoryName %>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID='lblName' runat='server'></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="CategoryTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCategoryTitle %>'></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID='lblCategoryTitle' runat='server'></asp:Label>&nbsp;
                                </div>
                                <div class="ClearBoth">
                                </div>
                                <br />
                                <h4 class="SubTitle">
                                    <asp:Localize ID="DisplaySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplaySettings %>'></asp:Localize></h4>
                                <div class="FieldStyle">
                                    <asp:Localize ID="DisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleDisplayOrder %>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblDisplayOrder" runat="server"></asp:Label>&nbsp;
                                </div>
                                <div style="display: none">
                                    <div class="FieldStyleImgA">
                                        <asp:Localize ID="EnableCategory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableCategoryCheck %>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyleImgA">
                                        <img id="chkCategoryEnabled" runat="server" />
                                        &nbsp;
                                    </div>
                                </div>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="ChildCategory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleChildCategory %>'></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <img id="chkDisplaySubCategory" runat="server" />
                                </div>
                                <br />
                                <h4 class="SubTitle">
                                    <asp:Localize ID="CategoryImageText" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCategoryImage %>'></asp:Localize></h4>
                                <div class="Image">
                                    <asp:Image ID="CategoryImage" runat="server" />
                                </div>
                                <br />
                                <h4 class="SubTitle">
                                    <asp:Localize ID="SEOSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSeoSettings %>'></asp:Localize></h4>
                                <div class="FieldStyle">
                                    <asp:Localize ID="SEOTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOTitle %>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblSEOTitle" runat="server"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="SEOKeywords" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOKeywords %>'></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblSEOMetaKeywords" runat="server"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize ID="SEODescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEODescription %>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblSEOMetaDescription" runat="server"></asp:Label>&nbsp;
                                </div>
                                <div class="FieldStyleA">
                                    <asp:Localize ID="SEOURLText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOURL %>'></asp:Localize>
                                </div>
                                <div class="ValueStyleA">
                                    <asp:Label ID="lblSEOURL" runat="server"></asp:Label>&nbsp;
                                </div>
                                <br />
                                <h4 class="SubTitle">
                                    <asp:Localize ID="Description" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDescriptions %>'></asp:Localize></h4>
                                <div class="FieldStyle">
                                    <asp:Localize ID="ShortDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShortDescription %>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblshortdescription" runat="server"></asp:Label>&nbsp;
                                </div>
                                <div class="MainStyleA">
                                    <div class="FieldStyleA">
                                        <asp:Localize ID="LongDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLongDescription %>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Label ID="lblLongDescription" runat="server"></asp:Label>&#160
                                    </div>
                                </div>
                                <div class="FieldStyle">
                                    <asp:Localize ID="AdditionalDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAdditionalDescription %>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblAlternativeDesc" runat="server"></asp:Label>&nbsp;
                                </div>
                                <h4 class="SubTitle">
                                    <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCategoryBanner %>'></asp:Localize></h4>
                                <div class="FieldStyle">
                                    <asp:Localize ID="CategoryBanner" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCategoryBanner %>'></asp:Localize>
                                </div>
                                <div class="ValueStyle">
                                    <asp:Label ID="lblCategoryBanner" runat="server"></asp:Label>&nbsp;
                                </div>
                            </div>
                            <div class="ClearBoth">
                            </div>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                    <ajaxToolKit:TabPanel ID="pnlProduct" runat="server">
                        <HeaderTemplate>
                            <asp:Localize ID="AssociatedProducts" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleAssociatedProducts %>'></asp:Localize>
                        </HeaderTemplate>
                        <ContentTemplate>
                            <uc1:Spacer ID="Spacer1" SpacerHeight="7" SpacerWidth="3" runat="server"></uc1:Spacer>
                            <div>
                                <div class="TabDescription" style="width: 80%;">
                                    <p>
                                        <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTextAssociatedProducts %>'></asp:Localize></p>
                                </div>
                                <div class="ButtonStyle" style="clear: right; float: right; padding-top: 10px; padding-right: 10px;">
                                    <zn:LinkButton ID="Addrelatedproducts" runat="server" ButtonType="Button" OnClick="Addrelatedproducts_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonAddProduct %>' ButtonPriority="Primary" />
                                </div>

                            </div>
                            <div class="ClearBoth"></div>
                            <br />
                            <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                            <asp:UpdatePanel ID="updPnlRelatedProductGrid" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:GridView ID="uxGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                                        runat="server" CellPadding="4" AutoGenerateColumns="False" CssClass="Grid" Width="100%"
                                        OnPageIndexChanging="UxGrid_PageIndexChanging" OnRowCommand="UxGrid_RowCommand"
                                        GridLines="None" AllowPaging="True" PageSize="10">
                                        <Columns>
                                            <asp:BoundField DataField="productid" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="ProductCategoryID" Visible="false" />
                                            <asp:BoundField DataField="name" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>' HeaderStyle-HorizontalAlign="Left" />
                                            <asp:BoundField DataField="displayorder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
                                            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsActive %>' HeaderStyle-HorizontalAlign="Left">
                                                <ItemTemplate>
                                                    <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                                        runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Edit" Text='<%$ Resources:ZnodeAdminResource, LinkSettings %>' ValidationGroup="grpProduct" CommandArgument='<%# Eval("ProductCategoryID") %>'
                                                        CommandName="Manage" CssClass="actionlink" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkRemove %>' ValidationGroup="grpProduct" CommandArgument='<%# Eval("productid") %>' OnClientClick="return ProductDeleteConfirmation();"
                                                        CommandName="RemoveItem" CssClass="actionlink" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <EmptyDataTemplate>
                                            <asp:Localize ID="NoProducts" runat="server" Text='<%$ Resources:ZnodeAdminResource, RecordNotFoundProduct %>'></asp:Localize>
                                        </EmptyDataTemplate>
                                        <RowStyle CssClass="RowStyle" />
                                        <HeaderStyle CssClass="HeaderStyle" />
                                        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                        <FooterStyle CssClass="FooterStyle" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </ajaxToolKit:TabPanel>
                </ajaxToolKit:TabContainer>
            </div>
            <div>
                <uc1:Spacer ID="Spacer9" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <zn:Button ID="btnShowPopup" runat="server" Style="display: none" />
            <ajaxToolKit:ModalPopupExtender ID="mdlPopup" runat="server" TargetControlID="btnShowPopup"
                PopupControlID="pnlManage" BackgroundCssClass="modalBackground" />
            <asp:Panel runat="server" ID="pnlManage" Style="display: none;" CssClass="PopupStyle"
                Width="400">
                <div>
                    <ZNode:Manage ID="uxManage" runat="server" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
