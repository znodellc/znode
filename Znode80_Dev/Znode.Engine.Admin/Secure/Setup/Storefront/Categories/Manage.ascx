<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Categories.Manage" CodeBehind="Manage.ascx.cs" %>
<asp:UpdatePanel ID="updatepanelManage" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="FormView">
            <h4 class="SubTitle">
                <asp:Label ID="lblTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleEditProductSettings%>'></asp:Label></h4>
            <div>
                <asp:Label ID="lblMsg" CssClass="Error" runat="server"></asp:Label>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnTitleSelectTheme" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectTheme%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlThemeslist" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlThemeslist_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <asp:Panel ID="pnlTemplateList" runat="server" Visible="false">
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnMasterPageTemplate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnMasterPageTemplate%>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlPageTemplateList" runat="server">
                    </asp:DropDownList>
                    <div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ValidationGroup="grpTitle" ControlToValidate="ddlPageTemplateList"
                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredMasterPageTemplate%>' InitialValue="0">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
            </asp:Panel>
            <asp:Panel ID="pnlCssList" runat="server" Visible="false">
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnTitleCSS" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCSS%>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlCSSList" runat="server">
                    </asp:DropDownList>
                </div>
            </asp:Panel>
            <div class="RowStyle">
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnDisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder%>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtDisplayOrder" Text="" runat="server" />
                    <div>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ValidationGroup="grpTitle" ControlToValidate="txtDisplayOrder"
                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrderwithStar%>'></asp:RequiredFieldValidator>
                        <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtDisplayOrder"
                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnteraWholeNumber%>' MaximumValue="999999999" ValidationGroup="grpTitle"
                            MinimumValue="1" Type="Integer"></asp:RangeValidator>
                    </div>
                </div>
            </div>
            <div class="RowStyle">
                <div class="FieldStyle">
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox Checked="true" ID="CheckPrdtCategoryEnabled" Text='<%$ Resources:ZnodeAdminResource, CheckBoxEnableProduct%>' runat="server" />
                </div>
            </div>
            <!-- Ends Product Category Part -->
            <div class="ClearBoth">
            </div>
            <div>
                <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmitBottom" CausesValidation="true" ValidationGroup="grpTitle" />
                <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelBottom" />
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
