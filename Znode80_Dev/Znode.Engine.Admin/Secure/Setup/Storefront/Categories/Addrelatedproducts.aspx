<%@ Page Language="C#" AutoEventWireup="True" Title="Manage Categories - Add Related Products" MasterPageFile="~/Themes/Standard/edit.master" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Categories.Addrelatedproducts" CodeBehind="Addrelatedproducts.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">

    <script type="text/javascript">

        function AutoCompleteSelected_Category(source, eventArgs) {

        }

        function AutoCompleteSelected_Brands(source, eventArgs) {

        }

        function AutoCompleteSelected_ProductType(source, eventArgs) {
            var hiddenTextValue = $get("<%= ProductTypeId.ClientID %>");
            hiddenTextValue.value = eventArgs.get_value();
        }

    </script>

    <div class="Form">
        <div>
            <div class="LeftFloat" style="width: 50%">
                <h1 style="width: 562px;">
                    <asp:Localize ID="TitleAssociateProducts" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleAssociateProducts %>'></asp:Localize>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
            </div>
            <div class="LeftFloat" style="width: 49%" align="right">
                <zn:Button runat="server" ID="btnBack" OnClick="BtnBack_Click" Width="150px" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonBackToList %>' CausesValidation="False" />

            </div>
        </div>
        <div class="ClearBoth" align="left">
            <br />
        </div>
        <!-- Search product panel -->
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <h4 class="SubTitle">
                <asp:Localize ID="SearchProductsAdd" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchProductsAdd %>'></asp:Localize></h4>
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ProductName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductname" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ProductNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductsNumber %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductnumber" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="SKU" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductsSKU %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtsku" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="Brand" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBrand %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtManufacturer" runat="server"></asp:TextBox></span>
                        <!-- Auto Complete Extender -->
                        <ajaxToolKit:AutoCompleteExtender ID="autoCompleteExtender1" runat="server" TargetControlID="txtManufacturer"
                            ServicePath="~/Controls/Default/ZNodeMultifrontService.asmx" ServiceMethod="GetManufacturer" UseContextKey="true"
                            MinimumPrefixLength="1" EnableCaching="false" CompletionSetCount="10" CompletionInterval="1"
                            FirstRowSelected="false" OnClientItemSelected="AutoCompleteSelected_Brands" />
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ProductType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductType %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtProductType" runat="server"></asp:TextBox>
                            <asp:HiddenField runat="server" ID="ProductTypeId" />
                        </span>
                        <ajaxToolKit:AutoCompleteExtender ID="autoCompleteExtender2" runat="server" TargetControlID="txtProductType"
                            ServicePath="~/Controls/Default/ZNodeMultifrontService.asmx" ServiceMethod="GetProductTypes" UseContextKey="true"
                            MinimumPrefixLength="1" EnableCaching="false" CompletionSetCount="10" CompletionInterval="1"
                            FirstRowSelected="false" OnClientItemSelected="AutoCompleteSelected_ProductType" />
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="Category" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCategory %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtCategory" runat="server"></asp:TextBox></span>
                        <!-- Auto Complete Extender -->
                        <ajaxToolKit:AutoCompleteExtender ID="autoCompleteExtender" runat="server" TargetControlID="txtCategory"
                            ServicePath="~/Controls/Default/ZNodeMultifrontService.asmx" ServiceMethod="GetCategory" UseContextKey="true"
                            MinimumPrefixLength="1" EnableCaching="false" CompletionSetCount="10" CompletionInterval="1"
                            FirstRowSelected="false" OnClientItemSelected="AutoCompleteSelected_Category" />
                    </div>
                </div>
                <div class="ClearBoth">
                </div>
                <div>
                    <zn:Button runat="server" ID="btnClear" OnClick="BtnClear_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                    <zn:Button runat="server" ID="btnSearch" OnClick="BtnSearch_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' CausesValidation="true" />
                </div>
            </div>
        </asp:Panel>
        <br />
        <!-- Product List -->
        <asp:Panel ID="pnlProductList" runat="server" Visible="false">
            <h4 class="GridTitle"> <asp:Localize ID="ProductList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleProductList %>'></asp:Localize>          
            </h4>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
                GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
                EmptyDataText='<%$ Resources:ZnodeAdminResource, ErrorNoProducts %>' Width="100%" CellPadding="4" OnRowDataBound="UxGrid_RowDataBound">
                <Columns>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSelect %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkProduct" runat="server" Enabled='<%# !(int.Parse(DataBinder.Eval(Container.DataItem, "ProductId").ToString()) == ItemId) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ProductId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleImage %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img id="prodImage" alt=" " src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                                runat="server" style="border: none" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HtmlEncode="false" HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsActive %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="chMark" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                    ForeColor="red"></asp:Label>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <div class="LeftFloat" style="width: 185px;">
                    <zn:Button runat="server"  Width="175px" ID="butAddNew" OnClick="Submit_Click" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonAddSelectedProducts %>' CausesValidation="true" />
                </div>
                <div>
                      <zn:Button runat="server" ID="Cancel" OnClick="Cancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
                </div>
            </div>
        </asp:Panel>
    </div>
</asp:Content>
