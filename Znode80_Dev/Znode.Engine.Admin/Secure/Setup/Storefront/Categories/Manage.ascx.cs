using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Setup.Storefront.Categories
{
    /// <summary>
    /// Represents the Site admin - Admin_Secure_catalog_product_category_Manage user control class
    /// </summary>
    public partial class Manage : System.Web.UI.UserControl
    {
        #region Public Events
        public System.EventHandler SubmitButtonClicked;
        public System.EventHandler CancelButtonClicked;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the ItemId
        /// </summary>
        public int ItemId
        {
            get
            {
                if (ViewState["ItemId"] != null)
                {
                    return int.Parse(ViewState["ItemId"].ToString());
                }
                
                return 0;
            }

            set
            {
                ViewState["ItemId"] = value;
            }
        }

        #endregion

        #region Public Methods
        /// <summary>
        /// Bind Data Method
        /// </summary>
        public void BindData()
        {
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            ProductCategory productCategory = productCategoryAdmin.GetByProductCategoryID(this.ItemId);

            if (productCategory != null)
            {
                ddlPageTemplateList.SelectedValue = productCategory.MasterPageID.ToString();
                if (!string.IsNullOrEmpty(productCategory.DisplayOrder.ToString()))
                {
                    txtDisplayOrder.Text = productCategory.DisplayOrder.GetValueOrDefault().ToString();
                }
                else
                {
                    txtDisplayOrder.Text = "99";
                }
                CheckPrdtCategoryEnabled.Checked = productCategory.ActiveInd;

                if (productCategory.ThemeID != null)
                {
                    ddlThemeslist.SelectedValue = productCategory.ThemeID.ToString();
                }

                if (ddlThemeslist.SelectedValue != "0")
                {
                    pnlTemplateList.Visible = true;
                    pnlCssList.Visible = true;
                    ddlPageTemplateList.Items.Clear();
                    ddlCSSList.Items.Clear();
                    this.BindMasterPageTemplates();
                    this.BindCssList();

                    if (productCategory.CSSID != null)
                    {
                        ddlCSSList.SelectedValue = productCategory.CSSID.ToString();
                    }

                    if (productCategory.MasterPageID != null)
                    {
                        ddlPageTemplateList.SelectedValue = productCategory.MasterPageID.ToString();
                    }
                }
                else
                {
                    pnlTemplateList.Visible = false;
                    pnlCssList.Visible = false;
                }
            }

            updatepanelManage.Update();
        }        

        #endregion
        
        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindThemeList();
                this.BindData();
            }
        }

        #endregion

        #region Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            bool status = false;
            if (this.ItemId > 0)
            {
                ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
                ProductCategory productCategory = productCategoryAdmin.GetByProductCategoryID(this.ItemId);
                
                // Theme
                if (ddlThemeslist.Items.Count > 0 && ddlThemeslist.SelectedIndex != 0)
                {
                    productCategory.ThemeID = int.Parse(ddlThemeslist.SelectedValue);
                }
                else
                {
                    productCategory.ThemeID = null;
                }

                // MasterPage
                if (ddlPageTemplateList.Items.Count > 0 && ddlPageTemplateList.SelectedIndex != 0)
                {
                    productCategory.MasterPageID = int.Parse(ddlPageTemplateList.SelectedValue);
                }
                else
                {
                    productCategory.MasterPageID = null;
                }

                // CSS
                if (ddlCSSList.Items.Count > 0 && ddlCSSList.SelectedIndex != 0)
                {
                    productCategory.CSSID = int.Parse(ddlCSSList.SelectedValue);
                }
                else
                {
                    productCategory.CSSID = null;
                }

                productCategory.DisplayOrder = int.Parse(txtDisplayOrder.Text.Trim());
                productCategory.ActiveInd = CheckPrdtCategoryEnabled.Checked;

                status = productCategoryAdmin.Update(productCategory);
            }

            if (status)
            {
                if (this.SubmitButtonClicked != null)
                {
                    this.SubmitButtonClicked(sender, e);
                }
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateProductTemplate").ToString();
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.CancelButtonClicked != null)
            {
                this.CancelButtonClicked(sender, e);
            }
        }
        
        /// <summary>
        /// Country option changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlThemeslist_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlThemeslist.SelectedIndex > 0)
            {
                pnlTemplateList.Visible = true;
                pnlCssList.Visible = true;
                ddlPageTemplateList.Items.Clear();
                ddlCSSList.Items.Clear();
                this.BindMasterPageTemplates();
                this.BindCssList();
            }
            else
            {
                pnlTemplateList.Visible = false;
                pnlCssList.Visible = false;
            }
        }

        #endregion

        #region Private Methods
		/// <summary>
		/// Binds the themes to the DropDownList
		/// </summary>
		public void BindThemeList()
		{
			ddlThemeslist.DataSource = DataRepository.ThemeProvider.GetAll();
			ddlThemeslist.DataTextField = "Name";
			ddlThemeslist.DataValueField = "ThemeID";
			ddlThemeslist.DataBind();
            ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownSameAsStore").ToString(), "0");
			ddlThemeslist.Items.Insert(0, li);

			try
			{
				ddlThemeslist.SelectedIndex = 0;
				pnlCssList.Visible = true;
				this.BindCssList();
			}
			catch
			{
			}
		}

		/// <summary>
		/// Bind the CSS to the DropDownList
		/// </summary>
		public void BindCssList()
		{
			if (ddlThemeslist.SelectedIndex > 0)
			{
				ddlCSSList.DataSource = DataRepository.CSSProvider.GetByThemeID(int.Parse(ddlThemeslist.SelectedValue));
				ddlCSSList.DataTextField = "Name";
				ddlCSSList.DataValueField = "CSSID";
				ddlCSSList.DataBind();
                ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownSameAsStore").ToString(), "0");
				ddlCSSList.Items.Insert(0, li);
			}
		}

		/// <summary>
		/// Bind the MasterPage templates
		/// </summary>
        public void BindMasterPageTemplates()
        {
            if (ddlThemeslist.SelectedIndex > 0)
            {
                PortalAdmin portalAdmin = new PortalAdmin();

                ddlPageTemplateList.DataSource = portalAdmin.GetMasterPage(int.Parse(ddlThemeslist.SelectedValue), "Product");
                ddlPageTemplateList.DataTextField = "Name";
                ddlPageTemplateList.DataValueField = "MasterPageID";
                ddlPageTemplateList.DataBind();

                // Master template
                ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownSameAsTemplate").ToString(), "0");
                ddlPageTemplateList.Items.Insert(0, li);
            }
        }

        #endregion
    }
}