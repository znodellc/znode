using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Setup.Storefront.Categories
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_catalog_product_category_Add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (Page.IsPostBack == false)
            {
                // If edit function then bind the data fields
                if (this.ItemId > 0)
                {
                    lblTitle.Text = "Edit Category";
                    tblShowImage.Visible = true;
                    pnlImageUpload.Attributes["style"] += "padding-left: 10px; border-left: solid 1px #cccccc;";
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddCategory").ToString();
                    tblShowImage.Visible = true;
                    pnlImage.Visible = false;
                    pnlImageOption.Visible = false;
                    pnlImageUpload.Visible = true;
                    tblCategoryDescription.Visible = true;
                }
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        protected void BindEditData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            Category category = categoryAdmin.GetByCategoryId(this.ItemId);

            if (category != null)
            {
                // Checking the user profile for roles and permission and then loading the data based on roles
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (!UserStoreAccess.CheckUserRoleInCategory(profiles, this.ItemId))
                {
                    Response.Redirect("list.aspx", true);
                }

                txtName.Text = Server.HtmlDecode(category.Name);
                txtshortdescription.Text = Server.HtmlDecode(category.ShortDescription);
                ctrlHtmlText.Html = category.Description;
                ctrlHtmlText1.Html = category.AlternateDescription;
                DisplayOrder.Text = category.DisplayOrder.GetValueOrDefault(1).ToString();
                VisibleInd.Checked = category.VisibleInd;
                //Znode version 7.2.2 To get Category Banner
                ctrlHtmltxtBanner.Html = category.CategoryBanner;
                txtTitle.Text = Server.HtmlDecode(category.Title);
                chkSubCategoryGridVisibleInd.Checked = category.SubCategoryGridVisibleInd;
                txtSEOMetaDescription.Text = Server.HtmlDecode(category.SEODescription);
                txtSEOMetaKeywords.Text = Server.HtmlDecode(category.SEOKeywords);
                txtSEOTitle.Text = Server.HtmlDecode(category.SEOTitle);
                txtSEOURL.Text = category.SEOURL;
                ZNodeImage znodeImage = new ZNodeImage();
                Image1.ImageUrl = znodeImage.GetImageHttpPathMedium(category.ImageFile);
                txtImageAltTag.Text = category.ImageAltTag;
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoCategory").ToString());
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            string fileName = string.Empty;
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            Category category = new Category();
            CategoryNode categoryNode = new CategoryNode();
            Catalog catalog = new Catalog();
            PortalCatalog portalCatalog = new PortalCatalog();
            string mappedSEOUrl = string.Empty;

            if (this.ItemId > 0)
            {
                category = categoryAdmin.GetByCategoryId(this.ItemId);

                if (category.SEOURL != null)
                {
                    mappedSEOUrl = category.SEOURL;
                }
            }

            category.CategoryID = this.ItemId;
            category.Name = Server.HtmlEncode(txtName.Text);
            category.ShortDescription = Server.HtmlEncode(txtshortdescription.Text);
            category.Description = ctrlHtmlText.Html;
            category.AlternateDescription = ctrlHtmlText1.Html;
            category.Title = Server.HtmlEncode(txtTitle.Text);
            category.SubCategoryGridVisibleInd = chkSubCategoryGridVisibleInd.Checked;
            category.SEOTitle = Server.HtmlEncode(txtSEOTitle.Text);
            category.SEOKeywords = Server.HtmlEncode(txtSEOMetaKeywords.Text);
            category.SEODescription = Server.HtmlEncode(txtSEOMetaDescription.Text);
            //Znode version 7.2.2 To get Category Banner
            category.CategoryBanner = ctrlHtmltxtBanner.Html;
            category.SEOURL = null;
            if (txtSEOURL.Text.Trim().Length > 0)
            {
                category.SEOURL = txtSEOURL.Text.Trim().Replace(" ", "-");
            }

            category.ImageAltTag = txtImageAltTag.Text.Trim();
            category.DisplayOrder = int.Parse(DisplayOrder.Text);
            category.VisibleInd = VisibleInd.Checked;

            if (category.SEOURL != mappedSEOUrl)
            {
                if (urlRedirectAdmin.SeoUrlExists(category.SEOURL, category.CategoryID))
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSEOFriendlyURLAlreadyExist").ToString();
                    return;
                }
            }

            // Validate image
            if ((this.ItemId == 0) || (RadioCategoryNewImage.Checked == true))
            {
                if (UploadCategoryImage.PostedFile != null)
                {
                    if (UploadCategoryImage.PostedFile.FileName != string.Empty)
                    {
                        // Check for Product Image
                        //fileName = System.IO.Path.GetFileNameWithoutExtension(UploadCategoryImage.PostedFile.FileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(UploadCategoryImage.PostedFile.FileName);

                        fileName = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + System.IO.Path.GetFileNameWithoutExtension(UploadCategoryImage.PostedFile.FileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(UploadCategoryImage.PostedFile.FileName);

                        if (fileName != string.Empty)
                        {
                            category.ImageFile = fileName;
                        }
                    }
                }
            }
            else
            {
                category.ImageFile = category.ImageFile;
            }

            // Upload File if this is a new product or the New Image option was selected for an existing product
            if (RadioCategoryNewImage.Checked || this.ItemId == 0)
            {
                if (fileName != string.Empty)
                {
                    byte[] imageData = new byte[UploadCategoryImage.PostedFile.InputStream.Length];
                    UploadCategoryImage.PostedFile.InputStream.Read(imageData, 0, (int)UploadCategoryImage.PostedFile.InputStream.Length);
                    ZNodeStorageManager.WriteBinaryStorage(imageData, ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + fileName);
                    //UploadCategoryImage.Dispose();
                }
            }

            bool retval = false;

            // Get all CategoryNodes.
            CategoryHelper categoryHelper = new CategoryHelper();
            System.Data.DataSet ds = categoryHelper.GetAllCategoryNodes();
            DataView dv = new DataView();
            dv = new DataView(ds.Tables[0]);

            if (this.ItemId > 0)
            {
                retval = categoryAdmin.Update(category);
            }
            else
            {
                retval = categoryAdmin.Add(category);
            }

            // Clear CategoryList Cache used by autocomplete control so it gets rebuilt with new category
            if (System.Web.HttpContext.Current.Session["CategoryList"] != null)
            {
                System.Web.HttpContext.Current.Session["CategoryList"] = null;
            }

            if (retval)
            {
                if (chkAddURLRedirect.Checked)
                {
                    urlRedirectAdmin.AddUrlRedirectTable(SEOUrlType.Category, mappedSEOUrl, category.SEOURL, category.CategoryID.ToString());
                }

                urlRedirectAdmin.UpdateUrlRedirect(mappedSEOUrl);

                if (this.ItemId > 0)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit of Category - " + txtName.Text, txtName.Text);
                    Response.Redirect("~/Secure/Setup/Storefront/Categories/View.aspx?ItemId=" + this.ItemId);
                }
                else
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Creation of Category - " + txtName.Text, txtName.Text);
                    Response.Redirect("~/Secure/Setup/Storefront/Categories/View.aspx?ItemId=" + category.CategoryID);
                }
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCategoryChanges").ToString();
                return;
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.ItemId > 0)
            {
                Response.Redirect("~/Secure/Setup/Storefront/Categories/View.aspx?ItemId=" + this.ItemId);
            }
            else
            {
                Response.Redirect("~/Secure/Setup/Storefront/Categories/Default.aspx");
            }
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioCategoryCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblCategoryDescription.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioCategoryNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblCategoryDescription.Visible = true;
        }

        #endregion
    }
}