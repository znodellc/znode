<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" Title="Manage Categories - List" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Categories.Default" ValidateRequest="false" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
    <div class="Form" align="center">
        <div>
            <div class="LeftFloat" style="width: auto; text-align: left">
                <h1>
                    <asp:Localize ID="Categories" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextCategories %>'></asp:Localize>
                    <%--<div class="tooltip" >
                        <a href="javascript:void(0);" class="learn-more" ><span>
                            <asp:Localize ID="Localize2" runat="server"></asp:Localize></span></a>
                        <div class="content">
                            <h6>
                                Help</h6>
                            <p>
                          Example of Categories are Gifts, Clothing, Jewelry and etc...
                            </p>
                        </div>
              </div>--%>
                </h1>
                <p>
                    <asp:Localize ID="TextCategories" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCategories %>'></asp:Localize>
                </p>
                <p style="color: red">
                    <asp:Localize ID="TextNoteCatgories" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextNoteCatgories %>'></asp:Localize></p>

            </div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="btnAddCategory" runat="server"
                    ButtonType="Button" OnClick="BtnAddCategory_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonAddNewCategory %>'
                    ButtonPriority="Primary" />
            </div>
            <div align="left" class="SearchForm">
                <asp:Panel ID="Test" runat="server" DefaultButton="btnSearch">
                    <h4 class="SubTitle">
                        <asp:Localize ID="SearchCategories" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchCategories %>'></asp:Localize></h4>
                    <div class="RowStyle">
                        <div class="ItemStyle">
                            <span class="FieldStyle">
                                <asp:Localize ID="CategoryName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCategoryName %>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:TextBox ID="txtCategoryName" runat="server"></asp:TextBox></span>
                        </div>

                        <div class="ItemStyle">
                            <span class="FieldStyle">
                                <asp:Localize ID="Catalog" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCatalog %>'></asp:Localize></span><br />
                            <span class="ValueStyle">
                                <asp:DropDownList ID="ddlCatalog" runat="server" EnableViewState="true"></asp:DropDownList></span>
                        </div>
                    </div>
                    <div class="ClearBoth">
                    </div>
                    <div>
                        <zn:Button runat="server" ID="btnClear" OnClick="BtnClear_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                        <zn:Button runat="server" ID="btnSearch" OnClick="BtnSearch_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' CausesValidation="true" />

                    </div>
                </asp:Panel>
            </div>
            <uc1:Spacer ID="Spacer3" runat="server" SpacerHeight="30" />
            <div align="left">
                <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
                <h4 class="GridTitle">
                    <asp:Localize ID="CategoryList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleCategoryList %>'></asp:Localize></h4>
                <div align="center">
                    <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                        CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyTextNoCategories %>'
                        EnableSortingAndPagingCallbacks="False" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                        OnRowCommand="UxGrid_RowCommand" OnRowDeleting="UxGrid_RowDeleting" PageSize="25"
                        Width="100%">
                        <Columns>
                            <asp:BoundField DataField="CategoryID" HeaderStyle-HorizontalAlign="Left" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' />
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>'>
                                <ItemTemplate>
                                    <a href='<%# "View.aspx?itemid=" + DataBinder.Eval(Container.DataItem,"CategoryId").ToString()%>'>
                                        <%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="CatalogName" HtmlEncode="false" HeaderStyle-HorizontalAlign="Left" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCatalogName %>' />
                            <asp:BoundField DataField="DisplayOrder" HeaderStyle-HorizontalAlign="Left" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' />
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleEnabled %>' Visible="false">
                                <ItemTemplate>
                                    <img id="Img1" runat="server" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "VisibleInd").ToString()))%>' />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleActions %>' HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <div class="LeftFloat" style="width: 40%; text-align: left">
                                        <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("CategoryID") %>'
                                            CommandName="Manage" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' CssClass="LinkButton" />
                                    </div>
                                    <div class="LeftFloat" style="width: 50%; text-align: left">
                                        <asp:LinkButton ID="btnDelete" runat="server" CommandArgument='<%# Eval("CategoryID") %>'
                                            CommandName="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CssClass="LinkButton" />
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <FooterStyle CssClass="FooterStyle" />
                        <RowStyle CssClass="RowStyle" />
                        <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                        <HeaderStyle CssClass="HeaderStyle" />
                        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                    </asp:GridView>
                </div>
            </div>
            <div>
                <uc1:Spacer ID="Spacer2" runat="server" />
            </div>
        </div>
    </div>
</asp:Content>
