using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Setup.Storefront.Categories
{
    /// <summary>
    /// Represents the Site Admin - Admin_Secure_catalog_product_category_List class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Variables
        private static bool IsSearchEnabled = false;
        private string CatalogImagePath = string.Empty;
        private string AddLink = "~/Secure/Setup/Storefront/Categories/Add.aspx";
        private string EditLink = "~/Secure/Setup/Storefront/Categories/View.aspx";
        private string DeleteLink = "~/Secure/Setup/Storefront/Categories/ConfirmationPage.aspx";

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindCatalogData();
                this.BindGridData();
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Add Category Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddCategory_Click(object sender, System.EventArgs e)
        {
            Response.Redirect(this.AddLink);
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
            IsSearchEnabled = true;
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Storefront/Categories/Default.aspx");
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (IsSearchEnabled)
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindSearchData();
            }
            else
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindGridData();
            }
        }

        /// <summary>
        /// Even triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Manage")
                {
                    this.EditLink = this.EditLink + "?itemid=" + Id;
                    Response.Redirect(this.EditLink);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.DeleteLink + "?itemid=" + Id);
                }
            }
        }
        #endregion

        #region Bind Grid

        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            DataSet ds = categoryAdmin.GetCategoriesBySearchData(Server.HtmlEncode(txtCategoryName.Text.Trim()), ddlCatalog.SelectedValue, (UserStoreAccess.GetAvailablePortals != "0"));
            DataView dv = new DataView(UserStoreAccess.CheckStoreAccess(ds.Tables[0], true));
            dv.Sort = "DisplayOrder";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind Search data
        /// </summary>
        private void BindSearchData()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            DataSet ds = categoryAdmin.GetCategoriesBySearchData(Server.HtmlEncode(txtCategoryName.Text.Trim()), ddlCatalog.SelectedValue, (ddlCatalog.SelectedIndex == 0));
            DataView dv = new DataView(UserStoreAccess.CheckStoreAccess(ds.Tables[0], true));
            dv.Sort = "Name";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind Store Catalogs
        /// </summary>
        private void BindCatalogData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            TList<Catalog> catalogList = catalogAdmin.GetAllCatalogs();
            DataSet ds = catalogList.ToDataSet(false);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                }
            }

            ddlCatalog.DataSource = UserStoreAccess.CheckStoreAccess(ds.Tables[0]);
            DataView dataView = ds.Tables[0].DefaultView;
            string catalogIds = "0";
            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                catalogIds = catalogAdmin.GetCatalogIDsByPortalIDs(profiles.StoreAccess);
                dataView.RowFilter = string.Concat("CatalogID IN (", catalogIds, ")");
            }

            ddlCatalog.DataSource = dataView;
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();

            if (string.Compare(profiles.StoreAccess, "AllStores") == 0)
            {
                ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString().ToUpper(), "0");
                ddlCatalog.Items.Insert(0, li);
            }
            else
            {
                ListItem li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString().ToUpper(), string.Concat(catalogIds, ",0"));
                ddlCatalog.Items.Insert(0, li);
            }
        }

        #endregion
    }
}