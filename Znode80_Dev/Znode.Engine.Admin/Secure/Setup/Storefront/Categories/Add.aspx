<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Storefront.Categories.Add" Title="Manage Catagories - Add" ValidateRequest="false" CodeBehind="Add.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div>
            <div class="LeftFloat" style="width: 70%; text-align: left">
                <h1>
                    <asp:Label ID="lblTitle" runat="server"></asp:Label>
                </h1>
            </div>
            <div style="text-align: right">

                <zn:Button runat="server" ID="btnSubmitTop" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true" />
                <zn:Button runat="server" ID="btnCancelTop" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />

            </div>
            <div style="clear: both">
                <asp:Label ID="lblMsg" CssClass="Error" runat="server"></asp:Label>
            </div>
            <div>
                <uc1:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <h4 class="SubTitle">
                <asp:Localize ID="GeneralSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralSettings %>'></asp:Localize>
            </h4>
            <div class="FieldStyle">
                <asp:Localize ID="CategoryName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCategoryName %>'></asp:Localize>
                <span class="Asterix">*</span>
                <br />
                <small>
                    <asp:Localize ID="TextCategoryName" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCategoryName %>'></asp:Localize>
                </small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID='txtName' runat='server' MaxLength="50" Columns="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                    Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredCategoryName %>'
                    CssClass="Error"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="CategoryTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCategoryTitle %>'></asp:Localize><span class="Asterix">*</span>
                <br />
                <small>
                    <asp:Localize ID="TextCategoryTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCategoryTitle %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID='txtTitle' runat='server' MaxLength="255" Columns="50"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvTitle" runat="server" ControlToValidate="txtTitle"
                    Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredCategoryTitle %>'
                    CssClass="Error"></asp:RequiredFieldValidator>
            </div>
            <h4 class="SubTitle">
                <asp:Localize ID="DisplaySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDisplaySettings %>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="TitleDisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleDisplayOrder %>'></asp:Localize><span class="Asterix">*</span>
                <br />
                <small>
                    <asp:Localize ID="DisplayOrderCategory" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextDisplayOrderCategory %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="DisplayOrder" runat="server" MaxLength="9" Columns="9" Text="1"></asp:TextBox>
                <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" runat="server" Display="Dynamic"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrderwithStar %>' ControlToValidate="DisplayOrder"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="DisplayOrder"
                    Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>' MaximumValue="999999999"
                    MinimumValue="1" Type="Integer"></asp:RangeValidator>
            </div>
            <div style="display: none">
                <div class="FieldStyle">
                    <asp:Localize ID="EnableCategory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEnableCategory %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID='VisibleInd' runat='server' Checked="true" Text='<%$ Resources:ZnodeAdminResource, CheckboxEnableCategory %>'></asp:CheckBox>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ChildCategory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleChildCategory %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID='chkSubCategoryGridVisibleInd' runat='server' Checked="true" Text='<%$ Resources:ZnodeAdminResource, CheckboxChildCategory %>'></asp:CheckBox>
            </div>
            <h4 class="SubTitle">
                <asp:Localize ID="CategoryImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCategoryImage %>'></asp:Localize></h4>
            <small>
                <asp:Localize ID="TextCategoryImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCategoryImage %>'></asp:Localize></small>
            <div id="tblShowImage" runat="server" visible="false" style="margin: 10px 0px 10px 0px;">
                <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;" runat="server" id="pnlImage">
                    <asp:Image ID="Image1" runat="server" />
                </div>
                <div class="LeftFloat" style="margin-left: -1px;" runat="server" id="pnlImageUpload">
                    <asp:Panel runat="server" ID="pnlImageOption">
                        <div>
                            <asp:Localize ID="SelectOption" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextOption %>'></asp:Localize>
                        </div>
                        <div>
                            <asp:RadioButton ID="RadioCategoryCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonCurrentImage %>' runat="server"
                                GroupName="Category Image" AutoPostBack="True" OnCheckedChanged="RadioCategoryCurrentImage_CheckedChanged"
                                Checked="True" /><br />
                            <asp:RadioButton ID="RadioCategoryNewImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNewImage %>' runat="server"
                                GroupName="Category Image" AutoPostBack="True" OnCheckedChanged="RadioCategoryNewImage_CheckedChanged" />
                        </div>
                    </asp:Panel>
                    <br />
                    <div id="tblCategoryDescription" runat="server" visible="false">
                        <div class="FieldStyle">
                            <asp:Localize ID="SelectImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectImage %>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:FileUpload ID="UploadCategoryImage" runat="server" BorderStyle="Inset" EnableViewState="true" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="UploadCategoryImage"
                                CssClass="Error" Display="dynamic" ValidationExpression=".*(\.[Jj][Pp][Gg]|\.[Gg][Ii][Ff]|\.[Jj][Pp][Ee][Gg]|\.[Pp][Nn][Gg])"
                                ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidImage %>'></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="ImageALT" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSKUImageALTText %>'></asp:Localize><br />
                        <small>
                            <asp:Localize ID="ImageALTText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextImageALTText %>'></asp:Localize></small>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtImageAltTag" runat="server"></asp:TextBox>
                    </div>
                    <div>
                        <asp:Label ID="lblCategoryImageError" runat="server" CssClass="Error" ForeColor="Red"
                            Text="" Visible="False"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <h4 class="SubTitle">
                <asp:Localize ID="SEOSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSeoSettings %>'></asp:Localize></h4>

            <div class="DesignPageAdd">
                <div class="FieldStyle">
                    <asp:Localize ID="SEOTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOTitle %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="SEOKeywords" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOKeywords %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOMetaKeywords" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="SEODescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEODescription %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOMetaDescription" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="SEOFriendlyPageName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOFriendlyPageName %>'></asp:Localize><br />
                    <small>
                        <asp:Localize ID="HintSEOFriendlyPageName" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextSEOfriendlyPageName %>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtSEOURL" runat="server" MaxLength="100" Columns="50"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtSEOURL"
                        CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidSEOFriendlyURL %>'
                        SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_]+)"></asp:RegularExpressionValidator>
                </div>
                <div class="FieldStyle" style="display: none"></div>
                <div class="ValueStyleText" style="display: none">
                    <asp:CheckBox ID="chkAddURLRedirect" runat="server" Text='<%$ Resources:ZnodeAdminResource, Checkbox301Url %>' Height="30px" />
                </div>
            </div>
            <br />
            <h4 class="SubTitle">
                <asp:Localize ID="Description" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDescription %>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="ShortDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShortDescription %>'></asp:Localize>
                <br />
                <small>
                    <asp:Localize ID="TextShortDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextShortDescription %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtshortdescription" runat="server" Width="300px" TextMode="MultiLine"
                    Height="75px" MaxLength="100"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="LongDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLongDescription %>'></asp:Localize><span>
                    <br />
                    <small>
                        <asp:Localize ID="TextLongDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextLongDescription %>'></asp:Localize></small></span>
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="AdditionalDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAdditionalDescription %>'></asp:Localize><br />
                <small>
                    <asp:Localize ID="TextAdditionalDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextAdditionalDescription %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="ctrlHtmlText1" runat="server"></uc1:HtmlTextBox>
            </div>
            <h4 class="SubTitle">
                <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCategoryBanner %>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="CategoryBanner" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCategoryBanner %>'></asp:Localize><br />
                <small>
                    <asp:Localize ID="TextCategoryBanner" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCategoryBanner %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="ctrlHtmltxtBanner" runat="server"></uc1:HtmlTextBox>
            </div>
            <div>
                <uc1:Spacer ID="Spacer9" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <div class="ClearBoth">
            </div>            
            <div>
                <zn:Button runat="server" ID="btnSubmitBottom" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true" />
                <zn:Button runat="server" ID="btnCancelBottom" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
            </div>
        </div>
    </div>
</asp:Content>
