﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.Framework.Business;
using System.Data;
using Znode.Engine.Common;

namespace  Znode.Engine.Admin.Secure.Setup.Storefront.Categories
{
    public partial class ManageCategoryProfile : System.Web.UI.UserControl
    {
        #region Properties

        private int CategoryProfileId
        {
            get
            {
                if (ViewState["CategoryProfileId"] != null)
                {
                    return Convert.ToInt32(ViewState["CategoryProfileId"]);
                }

                return 0;
            }

            set
            {
                ViewState["CategoryProfileId"] = value;
            }
        }

        protected int CategoryID
        {
            get
            {
                if (Request.QueryString["nodeid"] != null &&
                    (ViewState["CategoryID"] == null || Convert.ToInt32(ViewState["CategoryID"]) == 0))
                {
                    int id = 0;
                    int.TryParse(Request.QueryString["nodeid"], out id);
                    CategoryNode cateoryNode = DataRepository.CategoryNodeProvider.GetByCategoryNodeID(id);
                    if (cateoryNode != null)
                    {
                        ViewState["CategoryID"] = cateoryNode.CategoryID;
                    }
                    else
                    {
                        ViewState["CategoryID"] = 0;
                    }
                }

                return Convert.ToInt32(ViewState["CategoryID"]);
            }
        }

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            this.pnlCategoryProfile.Visible = CategoryID > 0;

            if (!this.IsPostBack && CategoryID > 0)
            {
                BindGridData();
            }
        }

        #endregion

        #region Control Events

        protected void Update_Click(object sender, EventArgs e)
        {
            CategoryProfile categoryProfile = new CategoryProfile();
            if (this.CategoryProfileId > 0)
            {
                categoryProfile = DataRepository.CategoryProfileProvider.GetByCategoryProfileID(this.CategoryProfileId);
            }

            categoryProfile.CategoryID = this.CategoryID;
            categoryProfile.ProfileID = Convert.ToInt32(this.ddlProfile.SelectedValue);
            categoryProfile.EffectiveDate = Convert.ToDateTime(this.txtEffectiveDate.Text);
            DataRepository.CategoryProfileProvider.Save(categoryProfile);

            // rebind the grid.
            BindGridData();
            this.pnlEditProfile.Visible = false;
            ((Znode.Engine.Admin.Secure.Setup.Storefront.Catalogs.AssociateCatagory)this.Page).ShowSubmitButton = true;
            AddProfile.Enabled = true;
            this.uxGrid.Enabled = true;
        }

        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.pnlEditProfile.Visible = false;
            ((Znode.Engine.Admin.Secure.Setup.Storefront.Catalogs.AssociateCatagory)this.Page).ShowSubmitButton = true;
            BindGridData();
            AddProfile.Enabled = true;
            this.uxGrid.Enabled = true;
        }

        protected void Add_Click(object sender, EventArgs e)
        {
            lblError.Text = string.Empty;
            this.CategoryProfileId = 0;
            this.pnlEditProfile.Visible = true;
            if (BindProfile())
            {
                if (this.ddlProfile.Items.Count > 0)
                {
                    this.ddlProfile.SelectedIndex = 0;
                }

                this.BindEditData();
            }
            this.uxGrid.Enabled = false;
        }

        protected void uxGrid_EditCommand(object sender, GridViewEditEventArgs e)
        {
            lblError.Text = string.Empty;
            this.CategoryProfileId = Convert.ToInt32(uxGrid.DataKeys[e.NewEditIndex].Value);
            this.pnlEditProfile.Visible = true;
            BindProfile();
            BindEditData();
            AddProfile.Enabled = false;
            this.uxGrid.Enabled = false;
        }


        protected void uxGrid_DeleteCommand(object sender, GridViewDeleteEventArgs e)
        {
            this.CategoryProfileId = Convert.ToInt32(uxGrid.DataKeys[e.RowIndex].Value);
            DataRepository.CategoryProfileProvider.Delete(CategoryProfileId);
            BindGridData();
        }

        protected void uxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                LinkButton delButton = (LinkButton)e.Row.Cells[3].Controls[0];
                
                delButton.Attributes.Add("onclick", "return confirm('" + this.GetGlobalResourceObject("ZnodeAdminResource", "DeleteConfirmProfileEffective") + "')");
            }
        }

        #endregion

        #region Helper Methods

        private void BindGridData()
        {
            uxGrid.EditIndex = -1;
            CategoryAdmin catAdmin = new CategoryAdmin();
            DataSet catProfiles = catAdmin.GetCategoryProfileByCategoryID(CategoryID);
            this.uxGrid.DataSource = catProfiles;
            this.uxGrid.DataBind();           
        }

        private void BindEditData()
        {
            if (this.CategoryProfileId > 0)
            {
                lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTitleEditDetails").ToString();
                CategoryProfile skuProfile = DataRepository.CategoryProfileProvider.GetByCategoryProfileID(CategoryProfileId);
                this.ddlProfile.SelectedValue = skuProfile.ProfileID.ToString();
                this.txtEffectiveDate.Text = skuProfile.EffectiveDate.ToShortDateString();
            }
            else
            {
                lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTitleAddDetails").ToString();
                this.txtEffectiveDate.Text = DateTime.Now.ToShortDateString();
            }           
            ((Znode.Engine.Admin.Secure.Setup.Storefront.Catalogs.AssociateCatagory)this.Page).ShowSubmitButton = false;
        }

        private bool BindProfile()
        {
            // Get profiles
            StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();
            TList<Profile> profiles = UserStoreAccess.CheckProfileAccess(settingsAdmin.GetProfiles());
            TList<CategoryProfile> categoryProfiles = DataRepository.CategoryProfileProvider.GetByCategoryID(CategoryID);
            categoryProfiles.ApplyFilter(delegate(CategoryProfile cp) { return cp.CategoryProfileID != CategoryProfileId; });

            profiles.ApplyFilter(delegate(Profile p)
            {
                return categoryProfiles.FirstOrDefault(delegate(CategoryProfile cp)
                {
                    return cp.ProfileID == p.ProfileID;
                }) == null;
            });
            this.ddlProfile.DataSource = profiles;
            this.ddlProfile.DataTextField = "Name";
            this.ddlProfile.DataValueField = "ProfileID";
            this.ddlProfile.DataBind();

            if (ddlProfile.Items.Count == 0)
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAlreadyAssociatedProfile").ToString();
                pnlEditProfile.Visible = false;
                return false;
            }

            return true;
        }

        #endregion

    }
}