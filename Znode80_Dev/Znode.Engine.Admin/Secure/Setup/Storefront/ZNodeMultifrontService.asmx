<%@ WebService Language="C#" Class="ZNodeMultifrontService" %>

using System;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.Admin;
using System.Web.Script.Services;

/// <summary>
/// Summary description for ViewpointService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class ZNodeMultifrontService : System.Web.Services.WebService
{

    /// <summary>
    /// 
    /// </summary>
    public ZNodeMultifrontService()
    { }

    /// <summary>
    /// Getting Category name
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <param name="contextKey"></param>
    /// <returns></returns>    
    [System.Web.Services.WebMethod(true)]
    [System.Web.Script.Services.ScriptMethod]
    public string[] GetCategory(string prefixText, int count, string contextKey)
    {
        ArrayList items = new ArrayList(count);

        if (!prefixText.StartsWith("%") && !prefixText.StartsWith("-"))
        {
            prefixText = prefixText.Replace("''", "-");
            prefixText = prefixText.Replace("'", "%");
            prefixText = prefixText.Replace(" ", "%");

            CategoryService categoryService = new CategoryService();
            CategoryQuery query = new CategoryQuery();
            query.Append(CategoryColumn.Name, prefixText + "%");

            TList<Category> category = categoryService.Find(query.GetParameters());

            int counter = 1;

            foreach (Category entity in category)
            {
                if (counter++ >= count)
                    break;

                items.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(entity.Name, entity.CategoryID.ToString()));
            }

            category.Dispose();
        }

        return (string[])items.ToArray(typeof(string));
    }

    /// <summary>
    /// Getting Manufacturer Name
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <param name="contextKey"></param>
    /// <returns></returns>    
    [System.Web.Services.WebMethod(true)]
    [System.Web.Script.Services.ScriptMethod]
    public string[] GetManufacturer(string prefixText, int count, string contextKey)
    {
        ArrayList items = new ArrayList(count);

        if (!prefixText.StartsWith("%") && !prefixText.StartsWith("-"))
        {
            prefixText = prefixText.Replace("''", "-");
            prefixText = prefixText.Replace("'", "%");
            prefixText = prefixText.Replace(" ", "%");

            ManufacturerService  manufacturerService = new ManufacturerService();
            ManufacturerQuery query = new ManufacturerQuery();
            query.Append(ManufacturerColumn.Name, prefixText + "%");

            TList<Manufacturer> manufacturer = manufacturerService.Find(query.GetParameters());

            int counter = 1;

            foreach (Manufacturer entity in manufacturer)
            {
                if (counter++ >= count)
                    break;

                items.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(entity.Name, entity.ManufacturerID.ToString()));
            }

            manufacturer.Dispose();
        }

        return (string[])items.ToArray(typeof(string));
    }

    /// <summary>
    /// Get Product Types for Autocomplete.
    /// </summary>
    /// <param name="prefixText"></param>
    /// <param name="count"></param>
    /// <param name="contextKey"></param>
    /// <returns></returns>    
    [System.Web.Services.WebMethod(true)]
    [System.Web.Script.Services.ScriptMethod]
    public string[] GetProductTypes(string prefixText, int count, string contextKey)
    {
        ArrayList items = new ArrayList(count);

        if (!prefixText.StartsWith("%") && !prefixText.StartsWith("-"))
        {
            prefixText = prefixText.Replace("''", "-");
            prefixText = prefixText.Replace("'", "%");
            prefixText = prefixText.Replace(" ", "%");

            ProductTypeService productTypeService = new ProductTypeService();
            ProductTypeQuery query = new ProductTypeQuery();
            query.Append(ProductTypeColumn.Name, prefixText + "%");

            TList<ProductType> productTypes = productTypeService.Find(query.GetParameters());

            int counter = 1;

            foreach (ProductType entity in productTypes)
            {
                if (counter++ >= count)
                    break;

                items.Add(AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(entity.Name, entity.ProductTypeId.ToString()));
            }

            productTypes.Dispose();
        }

        return (string[])items.ToArray(typeof(string));
    }
}