﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Setup.Content.Banners
{
    /// <summary>
    /// Represents the SiteAdmin -  AddBanner class
    /// </summary>
    public partial class AddBanner : System.Web.UI.Page
    {
        #region Protected Variables
        private string ItemId = string.Empty;
        private string PortalId = string.Empty;
        private string LocaleId = string.Empty;
        #endregion

        #region Public Methods
        /// <summary>
        /// Bind Method
        /// </summary>
        public void Bind()
        {
            MessageConfigAdmin messageadmin = new MessageConfigAdmin();
            MessageConfig messageconfig = new MessageConfig();
            if (!string.IsNullOrEmpty(this.ItemId))
            {
                messageconfig = messageadmin.GetByMessageID(int.Parse(this.ItemId));
                ctrlHtmlText.Html = HttpUtility.HtmlDecode(messageconfig.Value);
                string TitleMsg = messageconfig.Description;
                hdnMessageKey.Value = messageconfig.Key;

                lbltitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleManageBanner").ToString() +TitleMsg.Replace("-", string.Empty);
                uxStoreName.PreSelectValue = Convert.ToString(messageconfig.PortalID);
                uxStoreName.LocalePreSelectValue = Convert.ToString(messageconfig.LocaleID);
                hdnMessageID.Value = messageconfig.MessageID.ToString();
                txtSEOName.Text = messageconfig.PageSEOName;
                txtDescription.Text =Server.HtmlDecode(messageconfig.Description);
            }
            else
            {
                uxStoreName.EnableControl = true;
            }

            TList<MessageConfig> allBanners = messageadmin.GetAllBanners().FindAllDistinct(MessageConfigColumn.Key);
            allBanners = allBanners.FindAll(delegate(MessageConfig mc) { return !string.IsNullOrEmpty(mc.Key); });
            allBanners.Sort("Key");            
            ddlMessageKey.DataSource = allBanners;
            ddlMessageKey.DataTextField = "Key";
            ddlMessageKey.DataValueField = "Key";
            ddlMessageKey.DataBind();

            if (ddlMessageKey.Items.Count == 0)
            {
                ddlMessageKey.Style.Add("display", "none");
            }
            else
            {
                pnlMessageKey.Style.Add("display", "none");
                rfvMessageKey.Enabled = false;

                ListItem item = new ListItem();

                item.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextMessageKey").ToString();
                item.Value = "0";
                ddlMessageKey.Items.Insert(0, item);

                item = new ListItem();
                item.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextAddLocation").ToString(); 
                item.Value = "+1";
                ddlMessageKey.Items.Add(item);

                if (!string.IsNullOrEmpty(hdnMessageID.Value) && ddlMessageKey.Items.FindByValue(hdnMessageKey.Value) != null)
                {
                    ddlMessageKey.SelectedValue = hdnMessageKey.Value;
                }

                ddlMessageKey.Attributes.Add("onchange", "if (this.value == '+1') { this.style.display='none'; document.getElementById('" + rfvMessageKey.ClientID + "').enabled = 'True'; document.getElementById('" + pnlMessageKey.ClientID + "').style.display = 'block'; } ");
                btnMessageKey.OnClientClick = "document.getElementById('" + pnlMessageKey.ClientID + "').style.display = 'none';  document.getElementById('" + rfvMessageKey.ClientID + "').enabled = 'False'; document.getElementById('" +
                                                    ddlMessageKey.ClientID + "').style.display = 'block'; if (parseInt(document.getElementById('" +
                                                    hdnMessageKey.ClientID + "').value) > 0) document.getElementById('" + ddlMessageKey.ClientID +
                                                    "').value = document.getElementById('" + hdnMessageKey.ClientID + "').value; else document.getElementById('" +
                                                    ddlMessageKey.ClientID + "').selectedIndex = 0; ";
            }
        }

        #endregion

        #region Page load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.QueryString["itemid"] != null)
            {
                this.ItemId = Request.QueryString["itemid"];
            }
            
            if (Request.QueryString["portalid"] != null)
            {
                this.PortalId = Request.QueryString["portalid"];
            }
            
            if (Request.QueryString["localeId"] != null)
            {
                this.LocaleId = Request.QueryString["localeId"];
            }
            
            if (!Page.IsPostBack)
            {
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = profiles.StoreAccess.Split(',');
                    Array Stores = (Array)stores;
                    int found = Array.IndexOf(Stores, this.PortalId);
                    if (found == -1)
                    {
                        Response.Redirect("default.aspx", true);
                    }
                }

                if (string.IsNullOrEmpty(this.ItemId))
                {
                    lbltitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddBanner").ToString();
                }
                
                this.Bind();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            MessageConfigAdmin messageadmin = new MessageConfigAdmin();
            MessageConfig messageconfig = new MessageConfig();
            bool Status = false;

            if (!string.IsNullOrEmpty(hdnMessageID.Value) && int.Parse(hdnMessageID.Value) > 0)
            {
                messageconfig.MessageID = Convert.ToInt32(hdnMessageID.Value);
                messageconfig = messageadmin.GetByMessageID(messageconfig.MessageID);
            }

            if (string.IsNullOrEmpty(txtMessageKey.Text) && ddlMessageKey.SelectedValue != "0")
            {
                txtMessageKey.Text = ddlMessageKey.SelectedItem.Text;
            }

            messageconfig.Key = txtMessageKey.Text;
            messageconfig.PageSEOName = txtSEOName.Text;
            messageconfig.Description = HttpUtility.HtmlEncode(txtDescription.Text);
            messageconfig.Value = HttpUtility.HtmlEncode(ctrlHtmlText.Html);
            messageconfig.PortalID = uxStoreName.SelectedValue;
            messageconfig.LocaleID = uxStoreName.LocaleSelectedValue;

            // Set the message type is banner.
            messageconfig.MessageTypeID = (int)ZNodeMessageType.Banner; 
            try
            {
                if (messageconfig.MessageID != 0)
                {
                    Status = messageadmin.Update(messageconfig);
                }
                else
                {
                    int messageId;
                    Status = messageadmin.Insert(messageconfig, out messageId);
                }
                
                if (!Status)
                {
                    lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorMessageConfig").ToString();
                    return;
                }
                else
                {
                    string extractStr = ctrlHtmlText.Html.ToString();
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit Messages ", extractStr.Length > 255 ? extractStr.Substring(0,255) : extractStr);
                }

                Response.Redirect("~/Secure/Setup/Content/Banners/Default.aspx");
            }
            catch (Exception)
            {
                // Display error message
                lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateException").ToString();
            }                     
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Content/Banners/Default.aspx");
        }

        #endregion
    }
}
