﻿using System;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Admin.Secure.Setup.Content.Banners
{
	public partial class Delete : System.Web.UI.Page
	{

		#region Protected Member Variables
		private int ItemId;
		private string CancelLink = "~/Secure/Setup/Content/Banners/Default.aspx";
		#endregion

		#region Public Properties
		/// <summary>
		/// Gets or sets the Description
		/// </summary>
		public string Description { get; set; }
		#endregion

		#region General Events
		/// <summary>
		/// Page Load event
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			// Get ItemId from querystring        
			if (Request.QueryString["itemid"] != null)
			{
				this.ItemId = int.Parse(Request.QueryString["Itemid"]); 
			}
			else
			{
				this.ItemId = 0;
			}
	
		}

		/// <summary>
		/// Delete Button Click event
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect(this.CancelLink);
		}


		protected void BtnDelete_Click(object sender, EventArgs e)
		{
			MessageConfigAdmin messageConfigAdmin = new MessageConfigAdmin();
			MessageConfig messageconfig = new MessageConfig();
			messageconfig = messageConfigAdmin.GetByMessageID(this.ItemId);
			this.Description = messageconfig.Description;
			bool isDeleted = messageConfigAdmin.Delete(this.ItemId);

            if (isDeleted)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Delete Banners - " + this.Description, this.Description);
                Response.Redirect(this.CancelLink);
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteBanner").ToString();
            }
	}
		#endregion
	}
}