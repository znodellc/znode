﻿<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="true" CodeBehind="Delete.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Setup.Content.Banners.Delete" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
	<h5>
        <asp:Localize ID="TitlePlease" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm %>'></asp:Localize></h5>
    <p>
        <asp:Localize ID="SubTextDeleteBanner" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextDeleteBanner %>'></asp:Localize></p>
    <div>
        <asp:Label ID="lblMsg" runat="server" Width="450px" CssClass="Error"></asp:Label><br />
        <br />
    </div>
    <div>
          <zn:Button runat="server" ID="btnDelete" OnClick="BtnDelete_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>' CausesValidation="true"/>
            <zn:Button runat="server" ID="btnCancel" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
    </div>
    <br />
    <br />
    <br />
</asp:Content>