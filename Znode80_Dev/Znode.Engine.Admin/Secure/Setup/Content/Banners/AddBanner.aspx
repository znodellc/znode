﻿<%@ Page Title="Edit Banner" Language="C#" MasterPageFile="~/Themes/Standard/edit.master"
    ValidateRequest="false" AutoEventWireup="true" CodeBehind="AddBanner.aspx.cs"
    Inherits="Znode.Engine.Admin.Secure.Setup.Content.Banners.AddBanner" %>

<%@ Register Src="~/Controls/Default/StoreName.ascx" TagName="StoreName"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div class="Form">
        <asp:XmlDataSource ID="XmlDataSource1" runat="server"></asp:XmlDataSource>
        <br />
        <div style="width: 870px; display: table-cell;">
            <div class="LeftFloat">
                <h1>
                    <asp:Label ID="lbltitle" runat="server"></asp:Label></h1>
            </div>
            <div class="RightFloat"> 
            <zn:Button runat="server" ID="ImageButton1" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true" ValidationGroup="Messages"/>
            <zn:Button runat="server" ID="ImageButton2" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
            </div>

        </div>
        <br />
        <div class="FormView">
            <div class="ValueStyle">
                <ZNode:StoreName ID="uxStoreName" runat="server" VisibleLocaleDropdown="false"/>
            </div>
            <div class="FieldStyle" style="margin-top:-13px;">
                <asp:Localize ID="Location" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleLocation %>'></asp:Localize><br />
                <small> <asp:Localize ID="SubTextLocation" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextLocation %>'></asp:Localize> </small>
            </div>
            <asp:UpdatePanel runat="server" ID="upBanner">
                <ContentTemplate>
                    <div class="ValueStyle" style="margin-top:-10px;">
                        <asp:DropDownList ID="ddlMessageKey" runat="server" Width="350">
                        </asp:DropDownList>
                        <asp:Panel runat="server" ID="pnlMessageKey">
                            <asp:TextBox ID="txtMessageKey" runat="server" Width="350"> </asp:TextBox>
         
                             <zn:Button runat="server" ID="btnMessageKey" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
                            <asp:RequiredFieldValidator ID="rfvMessageKey" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredMessageKey %>'
                                ControlToValidate="txtMessageKey" CssClass="Error" Display="Dynamic" SetFocusOnError="True"
                                ValidationGroup="Messages"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtMessageKey"
                                CssClass="Error" Display="Dynamic" ValidationGroup="Messages" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidMessageKey %>'
                                SetFocusOnError="True" ValidationExpression="([A-Za-z0-9]+)"></asp:RegularExpressionValidator>
                        </asp:Panel>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="ClearBoth" align="left">
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="SEOPageURL" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOPageURL %>'></asp:Localize><br />
                <small> <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextSEOPageURL %>'></asp:Localize></small>

            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOName" runat="server" Width="350"> </asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtSEOName"
                    CssClass="Error" Display="Dynamic" ErrorMessage="Enter valid SEO firendly URL"
                    SetFocusOnError="True" ValidationExpression="([A-Za-z0-9-_]+)" ValidationGroup="Messages"></asp:RegularExpressionValidator>
            </div>
            <div class="ValueStyle">
                <small><asp:Localize ID="TextSEONote" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextSEONote %>'></asp:Localize></small>
            </div>
            <div class="ClearBoth" align="left">
            </div>
            <div class="FieldStyle">
               <asp:Localize ID="Description" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDescription %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDescription" runat="server" Width="350"> </asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="BannerContent" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBannerContent %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
            </div>
            <div class="ClearBoth" align="left">
            </div>
        </div>
        <br />
        <div>
          
              <zn:Button runat="server" ID="SubmitButton" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true" ValidationGroup="Messages"/>
            <zn:Button runat="server" ID="CancelButton" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>

        </div>
        <br />
        <div>
            <asp:Label ID="lblmsg" runat="server" Font-Bold="True" CssClass="Error"></asp:Label>
        </div>
        <asp:HiddenField ID="hdnMessageID" runat="server" Value="0" />
        <asp:HiddenField ID="hdnMessageKey" runat="server" Value="" />
        <asp:HiddenField ID="hdnDescription" runat="server" />
    </div>
</asp:Content>
