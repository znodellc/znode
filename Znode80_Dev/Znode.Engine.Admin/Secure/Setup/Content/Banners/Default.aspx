﻿<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master"
	AutoEventWireup="True" CodeBehind="Default.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Setup.Content.Banners.Default"
	Title="Manage Banners" %>

<%@ Register Src="~/Controls/Default/StoreName.ascx" TagName="StoreName"
	TagPrefix="ZNode" %>
<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
	TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
	<div class="Form">
		<div class="ManageMessageHeading">
			<h1> <asp:Localize ID="ManageBanners" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextManageBanners %>'></asp:Localize></h1>
			<p class="ManageMessageText">
				 <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextManageBanner %>'></asp:Localize>
			</p>
			<div>
				<uc1:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
			</div>
		</div>
		<div class="ButtonStyle">
			<zn:LinkButton ID="btnAdd" runat="server" CausesValidation="False"
				ButtonType="Button" OnClick="BtnAdd_Click" Text='<%$ Resources:ZnodeAdminResource, TitleAddBanner %>'
				ButtonPriority="Primary" />
		</div>
		<div>
			<h4 class="SubTitle"> <asp:Localize ID="SubTitleBanner" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleBanner %>'></asp:Localize></h4>
			<div class="SearchForm">
				<div class="RowStyle">
					<div class="ItemStyle">
						<span class="FieldStyle"><asp:Localize ID="StoreName" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextStoreName %>'></asp:Localize></span><br />
						<span class="ValueStyle">
							<asp:DropDownList ID="ddlPortal" runat="server">
							</asp:DropDownList>
						</span>
					</div>
					<div class="ItemStyle">
						<span class="FieldStyle"><asp:Localize ID="Banner" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBanner %>'></asp:Localize></span><br />
						<span class="ValueStyle">
							<asp:TextBox ID="txtMessageKey" runat="server"></asp:TextBox>
							<div>
								<asp:RegularExpressionValidator ID="MessageValidator" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidSearch %>'
									ControlToValidate="txtMessageKey" Display="Dynamic" ValidationExpression='[A-Za-z0-9\s]*' CssClass="Error"></asp:RegularExpressionValidator>
							</div>
						</span>
					</div>
				</div>
			</div>
			<div class="ClearBoth" align="left">
			</div>
			<div>
				
            <zn:Button runat="server" ID="btnSearch" OnClick="BtnSearch_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' CausesValidation="true"/>
            <zn:Button runat="server" ID="btnClear" OnClick="BtnClear_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False"/>
			</div>
			<div>
				<asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
			</div>
			<br />
			<h4 class="GridTitle"><asp:Localize ID="CustomBanners" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCustomBanners %>'></asp:Localize></h4>
			<asp:GridView ID="uxGrid" runat="server" PageSize="50" CssClass="Grid" AllowPaging="True"
				AutoGenerateColumns="False" CellPadding="4" OnRowCommand="UxGrid_RowCommand"
				GridLines="None" CaptionAlign="Left" Width="100%" AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundBanners %>'
				OnRowCreated="UxGrid_RowCreated" OnRowDataBound="UxGrid_DataBound" OnPageIndexChanging="UxGrid_PageIndexChanging"
				OnRowDeleting="UxGrid_RowDeleting">
				<FooterStyle CssClass="FooterStyle" />
				<RowStyle CssClass="RowStyle" />
				<PagerStyle CssClass="PagerStyle" />
				<HeaderStyle CssClass="HeaderStyle" />
				<AlternatingRowStyle CssClass="AlternatingRowStyle" />
				<PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
				<Columns>
					<asp:BoundField DataField="Description" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, SubTitleBanner %>' HeaderStyle-HorizontalAlign="Left" />
					<asp:BoundField DataField="Key" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleLocation %>' HeaderStyle-HorizontalAlign="Left" />
					<asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnStoreName %>' HeaderStyle-HorizontalAlign="Left">
						<ItemTemplate>
							<asp:Label ID="lblStoreName" runat="server" Text='<%# GetStoreName(DataBinder.Eval(Container.DataItem,"PortalID").ToString()) %>'></asp:Label>
						</ItemTemplate>
					</asp:TemplateField>

					<asp:TemplateField>
						<ItemTemplate>
							<asp:LinkButton ID="Button" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkManage %>' CommandName="Edit"
								CommandArgument='<%#DataBinder.Eval(Container.DataItem,"MessageID") %>' CssClass="actionlink" />
							<asp:LinkButton ID="lbDelete" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkDeleteBanner %>' CommandName="Delete"
								CommandArgument='<%#DataBinder.Eval(Container.DataItem,"MessageID") %>' CssClass="actionlink" />
						</ItemTemplate>
					</asp:TemplateField>
					<asp:BoundField DataField="PortalID" HeaderStyle-HorizontalAlign="Left" />
					<asp:BoundField DataField="LocaleID" HeaderStyle-HorizontalAlign="Left" />
				</Columns>
			</asp:GridView>
		</div>
	</div>
</asp:Content>
