using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Setup.Content.Pages
{
    /// <summary>
    /// Represents the SiteAdmin -Revert class
    /// </summary>
    public partial class Revert : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId;
        private string ListLink = "~/Secure/Setup/Content/Pages/Default.aspx";
        private string _PageName = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the PageName
        /// </summary>
        public string PageName
        {
            get
            {
                return this._PageName;
            }

            set
            {
                this._PageName = value;
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindGridData();
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGridData();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // Property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // cell in the GridView control.
                GridViewRow selectedRow = uxGrid.Rows[index];

                TableCell Idcell = selectedRow.Cells[0];
                string Id = Idcell.Text;

                if (e.CommandName == "Revert")
                {
                    ContentPageAdmin pageAdmin = new ContentPageAdmin();
                    string oldcontent = string.Empty;
                    if (ViewState["oldcontent"] != null)
                    {
                        oldcontent = ViewState["oldcontent"].ToString();
                    }

                    bool retval = pageAdmin.RevertToRevision(int.Parse(Id), HttpContext.Current.User.Identity.Name, oldcontent);

                    if (retval)
                    {
                        this.BindGridData();
                        lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSuccessRevertedPage").ToString();
                    }
                    else
                    {
                        lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUanabletoRevert").ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListLink);
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            ContentPageAdmin pageAdmin = new ContentPageAdmin();
            TList<ContentPageRevision> revisionList = pageAdmin.GetPageRevisions(this.ItemId);
            revisionList.Sort("UpdateDate Desc");

            ContentPage page = pageAdmin.GetPageByID(this.ItemId);
            ViewState["oldcontent"] = pageAdmin.GetPageHTMLByName(page.Name, page.PortalID, page.LocaleId.ToString());

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                string[] stores = profiles.StoreAccess.Split(',');
                Array Stores = (Array)stores;
                int found = Array.IndexOf(Stores, page.PortalID.ToString());
                if (found == -1)
                {
                    Response.Redirect("default.aspx", true);
                }
            }

            this.PageName = page.Name;

            uxGrid.DataSource = revisionList;
            uxGrid.DataBind();
        }
        #endregion
    }
}