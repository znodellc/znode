<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Content.Pages.Default" Title="Manage Pages" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 50%">
            <h1> <asp:Localize ID="TitleManagePages" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleManagePages %>'></asp:Localize></h1>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAdd" runat="server" CausesValidation="False"
                ButtonType="Button" OnClick="BtnAdd_Click" Text='<%$ Resources:ZnodeAdminResource, LikButtonAddNewPage %>'
                ButtonPriority="Primary" />
        </div>
        <div>
            <uc1:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
    </div>
    <div class="SearchForm">
        <div class="RowStyle">
            <div class="ItemStyle">
                <span class="FieldStyle"> <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStoreName %>'></asp:Localize></span><br />
                <span class="ValueStyle">
                    <asp:DropDownList ID="ddlPortal" runat="server"></asp:DropDownList></span>
            </div>
           
            <div class="ItemStyle">
                <span class="FieldStyle"> <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePageName %>'></asp:Localize></span><br />
                <span class="ValueStyle">
                    <asp:TextBox ID="txtPageName" runat="server"></asp:TextBox></span>
                <div>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularPageNameContaints %>'
                        ControlToValidate="txtPageName" ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidPageTitle %>'></asp:RegularExpressionValidator>
                </div>
            </div>
        </div>
    </div>

    <div class="ClearBoth" align="left"></div>

    <div>
      
            <zn:Button ID="BtnSearch" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSearch_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSearch %>" />
             <zn:Button ID="BtnClear" runat="server" ButtonType="CancelButton"  CausesValidation="false" OnClick="BtnClear_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonClear %>" />
          

    </div>

    <div>
        <asp:Label ID="Label1" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label></div>
    <br />

    <h4 class="GridTitle"> <asp:Localize ID="GridTitlePageList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitlePageList %>'></asp:Localize></h4>
    <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
    <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
        CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
        CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
        OnRowDeleting="UxGrid_RowDeleting" AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyNoContentPageText %>'>
        <Columns>
            <asp:BoundField DataField="ContentPageID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="Name" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePageName %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="25%" />

            <asp:BoundField DataField="MasterPageIDSource.Name" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePageTemplate %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="20%" />
            <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStoreName %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="20%">
                <ItemTemplate>
                    <%# GetStoreName(DataBinder.Eval(Container.DataItem,"PortalID").ToString()) %>
                </ItemTemplate>
            </asp:TemplateField>
        

            <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                <ItemTemplate>
                    <asp:LinkButton ID="Button1" CommandName="Edit" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ContentPageID")%>'
                        Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' runat="server" CssClass="actionlink" />
                    &nbsp;<asp:LinkButton ID="LinkButton1" CommandName="Revert" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ContentPageID")%>'
                        Text='<%$ Resources:ZnodeAdminResource, LinkRevisions %>' runat="server" CssClass="actionlink" />
                    &nbsp;<asp:LinkButton ID="Button5" CommandName="Delete" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"ContentPageID")%>'
                        Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' runat="server" CssClass="actionlink" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle CssClass="FooterStyle" />
        <RowStyle CssClass="RowStyle" />
        <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
        <HeaderStyle CssClass="HeaderStyle" />
        <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
    </asp:GridView>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
    </div>
</asp:Content>
