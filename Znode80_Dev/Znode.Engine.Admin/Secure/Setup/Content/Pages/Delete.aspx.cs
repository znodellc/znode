using System;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Setup.Content.Pages
{
    /// <summary>
    /// Represents the SiteAdmin - Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        private string _PageName = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the PageName
        /// </summary>
        public string PageName
        {
            get
            {
                return this._PageName;
            }

            set
            {
                this._PageName = value;
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the screen
        /// </summary>
        protected void BindData()
        {
            ContentPageAdmin pageAdmin = new ContentPageAdmin();
            ContentPage contentPage = pageAdmin.GetPageByID(this.ItemId);

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                string[] stores = profiles.StoreAccess.Split(',');
                Array Stores = (Array)stores;
                int found = Array.IndexOf(Stores, contentPage.PortalID.ToString());
                if (found == -1)
                {
                    Response.Redirect("Default.aspx", true);
                }
            }

            this.PageName = contentPage.Name;

            if (!contentPage.AllowDelete)
            {
                btnDelete.Enabled = false;
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorPageCannotDelete").ToString();
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Page Load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            this.BindData();
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Content/Pages/Default.aspx");
        }

        /// <summary>
        /// Delete button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            ContentPageAdmin pageAdmin = new ContentPageAdmin();
            ContentPage contentPage = pageAdmin.GetPageByID(this.ItemId);

            string AssociateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeletepage").ToString(), contentPage.Name);
            string PageName = contentPage.Name;

            bool retval = pageAdmin.DeletePage(contentPage);

            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociateName, PageName);

            if (!retval)
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteactionCouldNotbeCompleted").ToString();
            }
            else
            {
                Response.Redirect("~/Secure/Setup/Content/Pages/Default.aspx");
            }
        }
        #endregion
    }
}