<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="Znode.Engine.Admin.Secure.Setup.Content.Messages.AddMessage" CodeBehind="AddMessage.aspx.cs"
    ValidateRequest="false" Title="Manage Messages - Edit" %>

<%@ Register Src="~/Controls/Default/StoreName.ascx" TagName="StoreName"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <asp:XmlDataSource ID="XmlDataSource1" runat="server"></asp:XmlDataSource>
        <br />
        <h1>
            <asp:Label ID="lbltitle" runat="server"></asp:Label></h1>
        <br />
        <div class="FormView">
            <div class="FieldStyle">
                <asp:Localize ID="ColumnMessageKey" Text='<%$ Resources:ZnodeAdminResource, ColumnMessageKey%>' runat="server"></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtMessageKey" runat="server" Width="350"> </asp:TextBox>
                <asp:RequiredFieldValidator ID="rfvMessageKey" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, ErrorMessageKey%>'
                    ControlToValidate="txtMessageKey" CssClass="Error" Display="Dynamic" SetFocusOnError="True" ValidationGroup="Messages"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                    ControlToValidate="txtMessageKey" CssClass="Error" Display="Dynamic" ValidationGroup="Messages" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularMessageKey %>'
                    SetFocusOnError="True" ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularValidMessageKey%>'></asp:RegularExpressionValidator>
            </div>
            <div class="ClearBoth" align="left">
            </div>

            <div class="FieldStyle">
                <asp:Localize ID="ColumnDescription" Text='<%$ Resources:ZnodeAdminResource, ColumnDescription%>' runat="server"></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDescription" runat="server" Width="350"> </asp:TextBox>
            </div>
            <div class="ClearBoth" align="left">
            </div>
            <div class="ValueStyle">
                <ZNode:StoreName ID="uxStoreName" runat="server" VisibleLocaleDropdown="false" />
            </div>
        </div>
        <div class="ClearBoth" align="left">
        </div>
        <div class="ValueStyle">
            <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
        </div>
        <br />
        <div>
            <zn:Button runat="server" ID="SubmitButton" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ValidationGroup="Messages" CausesValidation="True" />
            <zn:Button runat="server" ID="CancelButton" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
        </div>
        <br />
        <div>
            <asp:Label ID="lblmsg" runat="server" Font-Bold="True" CssClass="Error"></asp:Label>
        </div>
        <asp:HiddenField ID="hdnMessageID" runat="server" />
        <asp:HiddenField ID="hdnDescription" runat="server" />
    </div>
</asp:Content>
