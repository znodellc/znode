﻿using System;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Admin.Secure.Setup.Content.Messages
{
	public partial class Delete : System.Web.UI.Page
	{
		#region Protected Member Variables
		private int ItemId;
		private string CancelLink = "~/Secure/Setup/Content/Messages/Default.aspx";
		MessageConfigAdmin messageConfigAdmin = new MessageConfigAdmin();
		MessageConfig messageconfig = new MessageConfig();
		#endregion

		#region Public Properties
		/// <summary>
		/// Gets or sets the Key
		/// </summary>
		public string Key { get; set; }
		#endregion
		
		

		#region General Events
		/// <summary>
		/// Page Load event
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void Page_Load(object sender, EventArgs e)
		{
			// Get ItemId from querystring        
			if (Request.QueryString["itemid"] != null)
			{
				this.ItemId = int.Parse(Request.QueryString["Itemid"]);
			}
			else
			{
				this.ItemId = 0;
			}
			this.BindData();
		}

		protected void BtnDelete_Click(object sender, EventArgs e)
		{
			messageconfig = messageConfigAdmin.GetByMessageID(this.ItemId);
			this.Key = messageconfig.Key;
			bool isDeleted = messageConfigAdmin.Delete(this.ItemId);
			if (isDeleted)
			{
				ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ErrrorDeleteMessage") + this.Key, this.Key);
				Response.Redirect(this.CancelLink);
			}
			else
			{
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrrorDeleteMessage").ToString();
			}
		}

		protected void BtnCancel_Click(object sender, EventArgs e)
		{
			Response.Redirect(this.CancelLink);
		}
		#endregion

		#region Bind Data
		/// <summary>
		/// Bind data to the fields on the screen
		/// </summary>
		protected void BindData()
		{
			messageconfig = messageConfigAdmin.GetByMessageID(this.ItemId);
			this.Key = messageconfig.Key;
		}
		#endregion
	}
}