using System;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Setup.Content.Messages
{
    /// <summary>
    /// Represents the SiteAdmin - AddMessage class
    /// </summary>
    public partial class AddMessage : System.Web.UI.Page
    {
        #region Private Variables
        private string ItemId = string.Empty;
        private string PortalId = string.Empty;
        private string LocaleId = string.Empty;
        #endregion

        #region Public Methods
        /// <summary>
        /// Bind Method
        /// </summary>
        public void Bind()
        {
            MessageConfigAdmin messageadmin = new MessageConfigAdmin();
            MessageConfig messageconfig = new MessageConfig();
            if (!string.IsNullOrEmpty(this.ItemId))
            {
                messageconfig = messageadmin.GetByKeyPortalIDLocaleID(this.ItemId, Convert.ToInt32(this.PortalId), Convert.ToInt32(this.LocaleId));

                ctrlHtmlText.Html = messageconfig.Value;
                string TitleMsg = messageconfig.Description;
                txtMessageKey.Text = messageconfig.Key;
                txtMessageKey.Enabled = false;
                lbltitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource","TitleEditMessage") + TitleMsg.Replace("-", string.Empty);
                uxStoreName.PreSelectValue = Convert.ToString(messageconfig.PortalID);
                uxStoreName.LocalePreSelectValue = Convert.ToString(messageconfig.LocaleID);
                hdnMessageID.Value = messageconfig.MessageID.ToString();
                txtDescription.Text =Server.HtmlDecode(messageconfig.Description);
                uxStoreName.EnableControl = false;
            }
            else
            {
                uxStoreName.EnableControl = true;
            }
        }
        #endregion

        #region Page load Evemt
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDataSource1.DataFile = Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "/MessageConfig.xml");

            // Get ItemId from querystring        
            if (Request.QueryString["itemid"] != null)
            {
                this.ItemId = Request.QueryString["itemid"];
            }
            
            if (Request.QueryString["portalid"] != null)
            {
                this.PortalId = Request.QueryString["portalid"];
            }
            
            if (Request.QueryString["localeId"] != null)
            {
                this.LocaleId = Request.QueryString["localeId"];
            }
            
            if (!Page.IsPostBack)
            {
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = profiles.StoreAccess.Split(',');
                    Array Stores = (Array)stores;
                    int found = Array.IndexOf(Stores, this.PortalId);
                    if (found == -1)
                    {
                        Response.Redirect("default.aspx", true);
                    }
                }

                if (string.IsNullOrEmpty(this.ItemId))
                {
                    lbltitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddMessage").ToString();
                }
                
                this.Bind();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            MessageConfigAdmin messageadmin = new MessageConfigAdmin();
            MessageConfig messageconfig = new MessageConfig();
            bool Status = false;

            if (!string.IsNullOrEmpty(hdnMessageID.Value))
            {
                messageconfig.MessageID = Convert.ToInt32(hdnMessageID.Value);
                messageconfig = messageadmin.GetByMessageID(messageconfig.MessageID);

                if (txtMessageKey.Text != messageconfig.Key)
                {
                    MessageConfig existMessageConfig = messageadmin.GetByKeyPortalIDLocaleID(txtMessageKey.Text, messageconfig.PortalID, messageconfig.LocaleID.GetValueOrDefault(43));
                    if (existMessageConfig != null && existMessageConfig.MessageID != messageconfig.MessageID)
                    {
                        lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorKeyAlreadyExist").ToString();
                        return;
                    }
                }
            }
            else
            {
                // Check if there is a key with same name and selected portal and locale name.
                MessageConfig mc = messageadmin.GetByKeyPortalIDLocaleID(txtMessageKey.Text, Convert.ToInt32(uxStoreName.SelectedValue), Convert.ToInt32(uxStoreName.LocaleSelectedValue));
                if (mc != null)
                {
                    lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorKeyAlreadyExist").ToString();
                    return;
                }
            }

            messageconfig.Key = txtMessageKey.Text;
            messageconfig.Description =Server.HtmlEncode(txtDescription.Text);
            messageconfig.Value = ctrlHtmlText.Html;
            messageconfig.PortalID = uxStoreName.SelectedValue;
            messageconfig.LocaleID = uxStoreName.LocaleSelectedValue;
            
            // For default messages
            messageconfig.MessageTypeID = (int)ZNodeMessageType.Message;
            try
            {
                if (messageconfig.MessageID != 0)
                {
                    Status = messageadmin.Update(messageconfig);
                }
                else
                {
                    int messageId;
                    Status = messageadmin.Insert(messageconfig, out messageId);
                }
                
                if (!Status)
                {
                    lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorMessageConfig").ToString();
                    return;
                }
                else
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit Messages ", txtMessageKey.Text);
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                
                // Display error message
                lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoteAdd").ToString();
                return;
            }

            Response.Redirect("~/Secure/Setup/Content/Messages/Default.aspx");
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Setup/Content/Messages/Default.aspx");
        }

        #endregion
    }
}