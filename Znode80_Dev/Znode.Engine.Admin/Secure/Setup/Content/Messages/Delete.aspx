﻿<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="true" CodeBehind="Delete.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Setup.Content.Messages.Delete" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h5>
        <asp:Localize ID="TextPleaseConfirm" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm%>' runat="server"></asp:Localize></h5>
    <p>
        <asp:Localize ID="TextConfirmDeleteMessage" Text='<%$ Resources:ZnodeAdminResource, TextConfirmDeleteMessage%>' runat="server"></asp:Localize><b>"<% =Key%>"</b>.
        <asp:Localize ID="TextDeleteMessage" Text='<%$ Resources:ZnodeAdminResource, TextDeleteMessage%>' runat="server"></asp:Localize>
    </p>
    <div>
        <asp:Label ID="lblMsg" runat="server" Width="450px" CssClass="Error"></asp:Label><br />
        <br />
    </div>
    <div>
        <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnDelete_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete%>' CausesValidation="true" ID="btnDelete" />
        <zn:Button runat="server" ID="btnCancel" ButtonType="CancelButton" OnClick="BtnCancel_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' />
    </div>
    <br />
    <br />
    <br />
</asp:Content>
