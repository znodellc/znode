<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master"
    AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Content.Messages.Default" Title="Manage Messages"
    CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <div class="ManageMessageHeading">
            <h1>
                <asp:Localize ID="TitleManageMessages" Text='<%$ Resources:ZnodeAdminResource, TitleManageMessages%>' runat="server"></asp:Localize>
            </h1>
            <p class="ManageMessageText">
                <asp:Localize ID="TextMessages" Text='<%$ Resources:ZnodeAdminResource, TextManageMessages%>' runat="server"></asp:Localize>
            </p>
            <div>
                <uc1:Spacer ID="Spacer6" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAdd" runat="server" CausesValidation="False" ButtonType="Button" OnClick="BtnAdd_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddMessage %>' ButtonPriority="Primary" />
        </div>
        <div>

            <h4 class="SubTitle">
                <asp:Localize ID="SubTitleSearch" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchMessage%>' runat="server"></asp:Localize></h4>
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnStoreName" Text='<%$ Resources:ZnodeAdminResource, ColumnStoreName%>' runat="server"></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlPortal" runat="server">
                            </asp:DropDownList>
                        </span>
                    </div>

                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnMessage" Text='<%$ Resources:ZnodeAdminResource, ColumnMessage%>' runat="server"></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtMessageKey" runat="server"></asp:TextBox>
                            <div>
                                <asp:RegularExpressionValidator ID="MessageValidator" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidSearch%>'
                                    ControlToValidate="txtMessageKey" Display="Dynamic" ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularValidSearchMessage%>' CssClass="Error"></asp:RegularExpressionValidator>
                            </div>
                        </span>
                    </div>
                </div>
            </div>
            <div class="ClearBoth" align="left">
            </div>
            <div>
                <zn:Button ID="btnClear" runat="server" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear%>' CausesValidation="False" />
                <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch%>' CausesValidation="True" ID="btnSearch" />
            </div>
            <div>
                <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
            </div>
            <br />
            <h4 class="GridTitle">
                <asp:Localize ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, GridTitleCustomMessage%>' runat="server"></asp:Localize>
            </h4>
            <asp:GridView ID="uxGrid" runat="server" PageSize="50" CssClass="Grid" AllowPaging="True"
                AutoGenerateColumns="False" CellPadding="4" OnRowCommand="UxGrid_RowCommand"
                GridLines="None" CaptionAlign="Left" Width="100%" AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyTextMessage%>'
                OnRowCreated="UxGrid_RowCreated" OnRowDataBound="UxGrid_DataBound"
                OnPageIndexChanging="UxGrid_PageIndexChanging" OnRowDeleting="UxGrid_RowDeleting">
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                <Columns>
                    <asp:BoundField DataField="Description" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnMessage%>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="Key" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnLocation%>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnStoreName%>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label ID="lblStoreName" runat="server" Text='<%# GetStoreName(DataBinder.Eval(Container.DataItem,"PortalID").ToString()) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="Button" runat="server" Text='<%$ Resources:ZnodeAdminResource,LinkManage %>' CommandName="Edit"
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem,"Key") %>' CssClass="actionlink" />
                            <asp:LinkButton ID="lbDelete" runat="server" Text='<%$ Resources:ZnodeAdminResource,LinkDeleteMessage %>' CommandName="Delete"
                                CommandArgument='<%#DataBinder.Eval(Container.DataItem,"MessageID") %>' CssClass="actionlink" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="PortalID" HeaderStyle-HorizontalAlign="Left" />
                    <asp:BoundField DataField="LocaleID" HeaderStyle-HorizontalAlign="Left" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
