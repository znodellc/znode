using System;

namespace  Znode.Engine.Admin.Secure.Setup
{
    /// <summary>
    /// Represents the Default Page for Setup
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        /// <summary>
        /// Page Init event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            Session["SelectedMenuItem"] = Request.Url.PathAndQuery;
        }
    }
}