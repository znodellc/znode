<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Setup.Default" Title="Untitled Page" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">


    <div class="LeftMargin">

        <h1><asp:Localize ID="TitleStorefront" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleStorefront %>'></asp:Localize>
</h1>
        <hr />
        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/stores.png" />
            </div>
            <div class="Shortcut"><a id="A10" href="~/Secure/Setup/Storefront/Stores/Default.aspx" runat="server"><asp:Localize ID="LinkTextStores" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextStores %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize ID="TextStores" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextStores %>'></asp:Localize></p>
            </div>
        </div>
        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/catalog.png" />
            </div>
            <div class="Shortcut"><a id="A1" href="~/Secure/Setup/Storefront/Catalogs/Default.aspx" runat="server"><asp:Localize ID="LinkTextCatalogs" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextCatalogs %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCatalogs %>'></asp:Localize></p>
            </div>

        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/departments.png" />
            </div>
            <div class="Shortcut"><a id="A8" href="~/Secure/Setup/Storefront/Categories/Default.aspx" runat="server"><asp:Localize ID="LinkTextCategories" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextCategories %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize ID="TextCategories" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCategories %>'></asp:Localize></p>
            </div>
        </div>

          <h1><asp:Localize ID="TitleCheckout" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleCheckout %>'></asp:Localize></h1>
        <hr />





        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/payments.png" />
            </div>
            <div class="Shortcut"><span class="Icon"><a id="A4" href="~/Secure/Setup/Checkout/Payments/Default.aspx" runat="server"><asp:Localize ID="LinkTextPayments" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextPayments %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize ID="TextPayment" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPayment %>'></asp:Localize></p>
            </div>
        </div>


        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/Shipping.png" />
            </div>
           
			<div class="Shortcut"><span class="Icon"><a id="A6" href="~/Secure/Setup/Checkout/Shipping/default.aspx" runat="server"><asp:Localize ID="LinkTextShipping" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextShipping %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize ID="TextShippings" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextShippings %>'></asp:Localize></p>
            </div>
        </div>


        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/taxclasses.png" />
            </div>
            <div class="Shortcut"><span class="Icon"><a id="A7" href="~/Secure/Setup/Checkout/Taxes/default.aspx" runat="server"><asp:Localize ID="LinkTextTaxes" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextTaxes %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize ID="TextTaxes" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextTaxes %>'></asp:Localize></p>
            </div>
        </div>

        <h1><asp:Localize ID="TitleContent" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleContent %>'></asp:Localize></h1>
        <hr />

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/ManageMessages.png" />
            </div>
            <div class="Shortcut">
                <a id="A11" href="~/Secure/Setup/Content/Messages/Default.aspx" runat="server"><asp:Localize ID="LinkTextManageMessages" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextManageMessages %>'></asp:Localize></a>
            </div>
            <div class="LeftAlign">
                <p>
                    <asp:Localize ID="TextManageMessages" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextManageMessages %>'></asp:Localize>
                </p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/ManageBanners.png" />
            </div>
            <div class="Shortcut">
                <a id="A13" href="~/Secure/Setup/Content/Banners/Default.aspx" runat="server"><asp:Localize ID="LinkTextManageBanners" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextManageBanners %>'></asp:Localize></a>
            </div>
            <div class="LeftAlign">
                <p>
                    <asp:Localize ID="TextManageBanners" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextManageBanners %>'></asp:Localize>
                </p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/ManageContent.png" />
            </div>
            <div class="Shortcut">
                <a id="A14" href="~/Secure/Setup/Content/Pages/Default.aspx" runat="server"><asp:Localize ID="LinkTextManageContent" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextManageContent %>'></asp:Localize></a>
            </div>
            <div class="LeftAlign">
                <p>
                    <asp:Localize ID="TextManageContent" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextManageContent %>'></asp:Localize>
                </p>
            </div>
        </div>
    </div>

   
    </span></span></span>

   
</asp:Content>

