﻿using System;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Vendors.RejectionMessages
{
    /// <summary>
    /// Represents the SiteAdmin - Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Variables
        private string EditLink = "AddRejectionmessages.aspx?itemid=";
        #endregion

        #region Public methods

        /// <summary>
        /// Get Store Name by PortalId
        /// </summary>
        /// <param name="portalID">The value of PortalId</param>
        /// <returns>Returns the Store Name</returns>
        public string GetStoreName(string portalID)
        {
            PortalAdmin portalAdmin = new PortalAdmin();

            return portalAdmin.GetStoreNameByPortalID(portalID);
        }

        /// <summary>
        /// Get Store Name by PortalId
        /// </summary>
        /// <param name="localeID">The value of localeID</param>
        /// <returns>Returns the Locale Name</returns>
        public string GetLocaleName(string localeID)
        {
            LocaleAdmin localeAdmin = new LocaleAdmin();

            return localeAdmin.GetLocaleNameByLocaleID(localeID);
        }

        /// <summary>
        /// Return the Searchdata
        /// </summary>
        public void BindSearchData()
        {
            DataSet ds = new DataSet();
            
            // Get the path for the XML file
            string path = ZNodeConfigManager.EnvironmentConfig.ConfigPath + "RejectionMessageConfig.xml";

            string xmldata = ZNodeStorageManager.ReadTextStorage(path);

            byte[] xmlArray = Encoding.ASCII.GetBytes(xmldata);
            MemoryStream stream = new MemoryStream(xmlArray);
            stream.Seek(0, SeekOrigin.Begin);
            ds.ReadXml(stream);
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = "MsgKey ASC";

            // Filter the Search data.
            if (ddlPortal.SelectedValue == "0" && ddlLocale.SelectedValue == "0")
            {
                dv.RowFilter = "MsgKey like '%" + txtMessageKey.Text + "%'";
            }
            else if (ddlPortal.SelectedValue != "0" && ddlLocale.SelectedValue == "0")
            {
                dv.RowFilter = "MsgKey like '%" + txtMessageKey.Text + "%' and PortalID <> '0' and PortalID = '" + ddlPortal.SelectedValue + "'";
            }
            else if (ddlPortal.SelectedValue == "0" && ddlLocale.SelectedValue != "0")
            {
                dv.RowFilter = "MsgKey like '%" + txtMessageKey.Text + "%' and LocaleID <> '0' and LocaleID = '" + ddlLocale.SelectedValue + "'";
            }
            else
            {
                dv.RowFilter = "MsgKey like '%" + txtMessageKey.Text + "%' and PortalID <> '0' and PortalID = '" + ddlPortal.SelectedValue + "' and LocaleID <> '0' and LocaleID = '" + ddlLocale.SelectedValue + "'";
            }

            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindPortal();
                this.BindLocale();
                this.BindSearchData();
            }
        }
        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                GridViewRow row = (GridViewRow)((Control)e.CommandSource).Parent.Parent;
                int idx = row.DataItemIndex;

                string key = e.CommandArgument.ToString().Replace("&", "-").Replace("[", "*").Replace("]", "~");                
                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.EditLink + Server.HtmlEncode(key) + "&portalid=" + row.Cells[4].Text + "&localeId=" + row.Cells[5].Text);
                }
            }
        }

        /// <summary>
        /// Grid Row Created Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[4].Visible = false;
                e.Row.Cells[5].Visible = false;
            }
        }

        /// <summary>
        /// Grid Row Data Bound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_DataBound(object sender, GridViewRowEventArgs e)
        {
            Label lblstore = (Label)e.Row.FindControl("lblStoreName");
            if (lblstore != null)
            {
                if (lblstore.Text == string.Empty)
                {
                    e.Row.Visible = false;
                }
            }
        }

        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }
       
        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }
        
        /// <summary>
        /// Add Rejection Message button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Vendors/RejectionMessages/AddRejectionmessages.aspx");
        }

        /// <summary>
        /// Clear button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            ddlPortal.SelectedValue = "0";
            ddlLocale.SelectedValue = "0";
            txtMessageKey.Text = string.Empty;
            this.BindSearchData();
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Bind Portals
        /// </summary>
        private void BindPortal()
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
            }
            else
            {
                Portal po = new Portal();
                po.StoreName = this.GetGlobalResourceObject("ZnodeAdminResource","DropDownTextAllStores").ToString().ToUpper();
                portals.Insert(0, po);
            }

            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();

            foreach (ListItem listItem in ddlPortal.Items)
            {
                listItem.Text = Server.HtmlDecode(listItem.Text);
            }
        }

        /// <summary>
        /// Bind Locale
        /// </summary>
        private void BindLocale()
        {
            LocaleAdmin localeAdmin = new LocaleAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Locale> locales = new TList<Locale>();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (profiles.StoreAccess != "AllStores")
            {
                foreach (string storeid in profiles.StoreAccess.Split(','))
                {
                    if (!string.IsNullOrEmpty(storeid))
                    {
                        locales.AddRange(localeAdmin.GetLocaleByPortalID(Convert.ToInt32(storeid)));
                    }
                }
            }
            else
            {
                locales = localeAdmin.GetAllLocaleByPortalCatalog();

                Locale li = new Locale();
                li.LocaleDescription = this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(); 
                li.LocaleID = 0;
                locales.Insert(0, li);
            }

            ddlLocale.DataSource = locales.FindAllDistinct(LocaleColumn.LocaleCode);
            ddlLocale.DataTextField = "LocaleDescription";
            ddlLocale.DataValueField = "LocaleID";
            ddlLocale.DataBind();
            if (locales.Count > 0)
            {
                ddlLocale.SelectedIndex = 0;
            }
        }
       
        #endregion
    }
}