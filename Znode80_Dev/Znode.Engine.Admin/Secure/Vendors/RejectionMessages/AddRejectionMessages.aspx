﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Vendors.RejectionMessages.AddRejectionMessages" CodeBehind="AddRejectionMessages.aspx.cs" %>

<%@ Register Src="~/Controls/Default/StoreName.ascx" TagName="StoreName" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <asp:XmlDataSource ID="XmlDataSource1" runat="server"></asp:XmlDataSource>
        <br />
        <h1>
            <asp:Label ID="lbltitle" runat="server"></asp:Label></h1>
        <br />
        <div class="FormView">
            <div class="ValueStyle">
                <ZNode:StoreName ID="uxStoreName" runat="server" VisibleLocaleDropdown="false" />
            </div>
            <div class="ClearBoth">
            </div>
            <div class="FieldStyle" style="margin-top: -13px;">
                <asp:Localize ID="Message" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnMessageKey %>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle" style="margin-top: -15px;">
                <asp:TextBox ID="txtMessageKey" runat="server"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtMessageKey"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredMessageKey %>' CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtMessageKey"
                    CssClass="Error" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidMessageKey %>' ToolTip='<%$ Resources:ZnodeAdminResource, ValidMessageKey %>'
                    ValidationExpression="([A-Za-z0-9-_ ]+)" Display="Dynamic"></asp:RegularExpressionValidator>
            </div>
            <div class="ClearBoth" align="left">
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="MessageValue" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnMessageValue %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtMessage" runat="server"></asp:TextBox>
            </div>
            <div class="ClearBoth" align="left">
            </div>
            <div>
                <asp:Label ID="lblError" runat="server" Text="" CssClass="Error" ForeColor="red"></asp:Label>
            </div>
            <br />
            <div class="FieldStyle">
                <zn:Button runat="server" ID="SubmitButton" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true"/>
                <zn:Button runat="server" ID="CancelButton" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
            </div>
            <br />
            <div>
                <asp:Label ID="lblmsg" runat="server" Font-Bold="True" ForeColor="LimeGreen"></asp:Label>
            </div>
        </div>
    </div>
</asp:Content>
