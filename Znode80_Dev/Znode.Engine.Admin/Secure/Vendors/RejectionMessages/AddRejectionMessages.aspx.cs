﻿using System;
using System.Web.UI;
using System.Xml;
using Znode.Engine.Common;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Vendors.RejectionMessages
{
    /// <summary>
    /// Represents the SiteAdmin - AddRejectionMessages class
    /// </summary>
    public partial class AddRejectionMessages : System.Web.UI.Page
    {
        #region Private Variables
        private string ItemId = string.Empty;
        private string PortalId = string.Empty;
        private string LocaleId = string.Empty;
        private string xmlPath = ZNodeConfigManager.EnvironmentConfig.ConfigPath + "RejectionMessageConfig.xml";
        #endregion

        #region Public Methods
        /// <summary>
        /// Bind Method
        /// </summary>
        public void Bind()
        {
            string xmldata = ZNodeStorageManager.ReadTextStorage(this.xmlPath);
            XmlDocument data = new XmlDocument();
            data.LoadXml(xmldata);

            XmlNode node = data.SelectSingleNode("//Messages/Message[@PortalID='" + this.PortalId + "'][@LocaleID='" + this.LocaleId + "'][@MsgKey='" + this.ItemId + "']");

            txtMessage.Text = Server.HtmlDecode(node.Attributes["MsgValue"].Value);
            txtMessageKey.Text = node.Attributes["MsgKey"].Value;
            uxStoreName.PreSelectValue = node.Attributes["PortalID"].Value;
            uxStoreName.LocalePreSelectValue = node.Attributes["LocaleID"].Value;
            uxStoreName.EnableControl = false;
            lbltitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource","TitleEditRejectionMessages").ToString() + txtMessageKey.Text.Replace("-", string.Empty);
        }
        #endregion

        #region Page load Event
        
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            XmlDataSource1.DataFile = Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "/RejectionMessageConfig.xml");

            // Get ItemId from querystring        
            if (Request.QueryString["itemid"] != null)
            {
                this.ItemId = Server.HtmlDecode(Request.QueryString["itemid"]);
                this.ItemId = this.ItemId.Replace("-", "&").Replace("*", "[").Replace("~", "]");
            }
            
            if (Request.QueryString["portalid"] != null)
            {
                this.PortalId = Request.QueryString["portalid"];
            }
            
            if (Request.QueryString["localeId"] != null)
            {
                this.LocaleId = Request.QueryString["localeId"];
            }
            
            if (!Page.IsPostBack)
            {
                lblError.Text = "";
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (profiles.StoreAccess != "AllStores")
                {
                    string[] stores = profiles.StoreAccess.Split(',');
                    Array Stores = (Array)stores;
                    int found = Array.IndexOf(Stores, this.PortalId);
                    if (found == -1)
                    {
                        Response.Redirect("Default.aspx", true);
                    }
                }
                
                if (this.ItemId.Length > 0)
                {
                    // Do not allow to edit the message key.
                    txtMessageKey.Enabled = false;
                    this.Bind();
                }
                else
                {
                    lbltitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource","TitleAddRejectionMessages").ToString();
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            string xmldata = ZNodeStorageManager.ReadTextStorage(this.xmlPath);
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(xmldata);

            if (this.ItemId.Length > 0)
            {
                XmlNode xmlnode = xmldoc.SelectSingleNode("Messages/Message[@MsgKey='" + this.ItemId + "']");
                xmlnode.Attributes["MsgKey"].Value = txtMessageKey.Text;
                xmlnode.Attributes["PortalID"].Value = uxStoreName.SelectedValue.ToString();
                xmlnode.Attributes["LocaleID"].Value = uxStoreName.LocaleSelectedValue.ToString();
                xmlnode.Attributes["MsgValue"].Value = txtMessage.Text;
            }
            else
            {
                XmlNode xmlnodeexist = xmldoc.SelectSingleNode("Messages/Message[@MsgKey='" + txtMessageKey.Text.Trim() + "']");
                if (xmlnodeexist != null)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorRejectionMessageExist").ToString(); 
                    return;
                }
                XmlNode xmlnode = xmldoc.SelectSingleNode("Messages");
                XmlElement element = xmldoc.CreateElement("Message");

                element.SetAttribute("MsgKey", txtMessageKey.Text.Trim());
                element.SetAttribute("PortalID", uxStoreName.SelectedValue.ToString());
                element.SetAttribute("LocaleID", uxStoreName.LocaleSelectedValue.ToString());
                element.SetAttribute("MsgValue", txtMessage.Text);
                xmldoc.DocumentElement.AppendChild(element);
            }

            try
            {
                ZNodeStorageManager.WriteTextStorage(xmldoc.OuterXml, this.xmlPath);

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditRejectionMessages").ToString(), txtMessage.Text);

                // Invalidate message config
                ZNodeConfigManager.MessageConfig = null;
            }
            catch
            {
                // Display error message
                lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorNoteAdd").ToString();
            }

            Response.Redirect("~/secure/Vendors/RejectionMessages/Default.aspx");
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/secure/Vendors/RejectionMessages/Default.aspx");
        }

        #endregion
    }
}