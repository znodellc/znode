﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Vendors.RejectionMessages.Default" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <div class="RejectionLeftFloat">
            <h1><asp:Localize ID="RejectionMessages" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextRejectionMessages %>'></asp:Localize></h1>
            <p class="RejectionText">
                <asp:Localize ID="RejectionMessagesText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextRejectionMessages %>'></asp:Localize>
            </p>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAdd" runat="server" CausesValidation="False" ButtonType="Button" OnClick="BtnAdd_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddRejectionMessage %>' ButtonPriority="Primary" />
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle"><asp:Localize ID="SearchMessages" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchMessage %>'></asp:Localize></h4>

        <div class="SearchForm">
            <div class="RowStyle">
                <div class="ItemStyle">
                    <span class="FieldStyle"><asp:Localize ID="StoreName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStoreName %>'></asp:Localize></span><br />
                    <span class="ValueStyle">
                        <asp:DropDownList ID="ddlPortal" runat="server"></asp:DropDownList></span>
                </div>
                <div class="ItemStyle" style="display: none">
                    <span class="FieldStyle"><asp:Localize ID="SelectLocale" runat="server" Text='<%$ Resources:ZnodeAdminResource, SelectLocale %>'></asp:Localize></span><br />
                    <span class="ValueStyle">
                        <asp:DropDownList ID="ddlLocale" runat="server"></asp:DropDownList></span>
                </div>
                <div class="ItemStyle">
                    <span class="FieldStyle"><asp:Localize ID="Message" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnMessage %>'></asp:Localize></span><br />
                    <span class="ValueStyle">
                        <asp:TextBox ID="txtMessageKey" runat="server"></asp:TextBox>
                        <div>
                            <asp:RegularExpressionValidator ID="regName" runat="server" ControlToValidate="txtMessageKey" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidMessageKey %>' Display="Dynamic" ValidationExpression="([A-Za-z0-9-_ ]+)" CssClass="Error"></asp:RegularExpressionValidator>
                        </div>
                    </span>
                </div>
            </div>
        </div>

        <div class="ClearBoth" align="left"></div>

        <div>
            <zn:Button runat="server" ID="btnSearch" OnClick="BtnSearch_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' CausesValidation="False"/>
            <zn:Button runat="server" ID="btnClear" OnClick="BtnClear_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
        </div>

        <div>
            <asp:Label ID="lblMsg" runat="server" ForeColor="Red" Font-Bold="True"></asp:Label>
        </div>
        <br />

        <h4 class="GridTitle"><asp:Localize ID="RejectionMessagesList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleRejectionMessages %>'></asp:Localize></h4>
        <asp:GridView ID="uxGrid" runat="server" PageSize="50" CssClass="Grid" AllowPaging="True"
            AutoGenerateColumns="False" CellPadding="4" OnRowCommand="UxGrid_RowCommand"
            GridLines="None" CaptionAlign="Left" Width="100%" AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundRejectionMessages %>'
            OnRowCreated="UxGrid_RowCreated" OnRowDataBound="UxGrid_DataBound" OnPageIndexChanging="UxGrid_PageIndexChanging">
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            <Columns>
                <asp:BoundField DataField="MsgKey" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnMessage %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStoreName %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="lblStoreName" runat="server" Text='<%# GetStoreName(DataBinder.Eval(Container.DataItem,"PortalID").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleLocale %>' Visible="False">
                    <ItemTemplate>
                        <asp:Label ID="lblLocale" runat="server" Text='<%# GetLocaleName(DataBinder.Eval(Container.DataItem,"LocaleID").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="Button" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkEditMessage %>' CommandName="Edit" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"MsgKey") %>' CssClass="actionlink" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="PortalID" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="LocaleID" HeaderStyle-HorizontalAlign="Left" />
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
