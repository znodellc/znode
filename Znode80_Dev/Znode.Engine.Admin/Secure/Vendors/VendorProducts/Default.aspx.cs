using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Threading;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Secure.Vendors.VendorProducts
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_reviewer_list class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Variables
        private string AddLink = "Add.aspx";
        private string PreviewLink = string.Empty;
        private string DeleteLink = "Delete.aspx?itemid=";
        private string DetailsLink = "View.aspx?itemid=";
        private string portalIds = string.Empty;
        private string selectedItemsKey = "SelectedItemsKey";
        #endregion

        #region Page Load

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            lblmsg.Text = string.Empty;
            this.PreviewLink = "http://" + UserStoreAccess.DomainName + "/product.aspx";
            this.portalIds = UserStoreAccess.GetAvailablePortals;

            if (!Page.IsPostBack)
            {
                UserStoreAccess uss = new UserStoreAccess();
                if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Admin || uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer || uss.UserType == Znode.Engine.Common.ZNodeUserType.ReviewerManager)
                {
                    btnApproveAll.Visible = true;
                    btnDeclineall.Visible = true;
                    ReviewerSearch.Visible = true;
                }
                else
                {
                    btnApproveAll.Visible = false;
                    btnDeclineall.Visible = false;
                    ReviewerSearch.Visible = false;
                    div2CO.Visible = false;
                }

                // Clear the previously selected items.
                Session[this.selectedItemsKey] = null;

                this.BindPortal();

                this.BindSearchProduct();

                Session.Remove("itemid");
            }
        }
        #endregion

        #region Grid Events

        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.RememberOldValues();
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchProduct();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Manage")
                {
                    Response.Redirect(this.DetailsLink + e.CommandArgument);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.DeleteLink + e.CommandArgument);
                }
            }
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_DataBound(object sender, EventArgs e)
        {
            UserStoreAccess uss = new UserStoreAccess();
            if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer || uss.UserType == Znode.Engine.Common.ZNodeUserType.Admin || uss.UserType == Znode.Engine.Common.ZNodeUserType.ReviewerManager)
            {
                uxGrid.Columns[0].Visible = true;
                uxGrid.Columns[6].Visible = true;
                uxGrid.Columns[7].Visible = true;
            }
            else
            {
                uxGrid.Columns[0].Visible = false;
                uxGrid.Columns[6].Visible = false;
                uxGrid.Columns[7].Visible = false;
            }
        }

        #endregion

        #region General Events
        /// <summary>
        /// AddNew Product button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddProduct_Click(object sender, EventArgs e)
        {
            Session["itemid"] = null;
            Response.Redirect(this.AddLink);
        }

        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            Session[this.selectedItemsKey] = null;
            this.BindSearchProduct();
        }

        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnApproveAll_Click(object sender, EventArgs e)
        {
            this.ChangeProductStatus(ZNodeProductReviewState.Approved);
        }

        /// <summary>
        /// Search button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDeclineall_Click(object sender, EventArgs e)
        {
            this.ChangeProductStatus(ZNodeProductReviewState.Declined);
        }

        /// <summary>
        /// Clear button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            txtproductname.Text = string.Empty;
            txtVendor.Text = string.Empty;
            txtVendorId.Text = string.Empty;
            txtVendorproductId.Text = string.Empty;
            txtProductStatus.Value = "0";
            txt2coid.Text = string.Empty;
            this.BindPortal();
            this.BindSearchProduct();
        }

        /// <summary>
        /// Review Images Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnReviewImages_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReviewImage.aspx");
        }

        /// <summary>
        /// Toggle checkbox selection
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkSelectAll = sender as CheckBox;
            for (int rowIndex = 0; rowIndex <= uxGrid.Rows.Count - 1; rowIndex++)
            {
                CheckBox chkProduct = uxGrid.Rows[rowIndex].Cells[0].FindControl("chkProduct") as CheckBox;
                if (chkProduct != null)
                {
                    chkProduct.Checked = chkSelectAll.Checked;
                }
            }
        }

        #endregion

        #region FormatPrice Helper method
        /// <summary>
        /// Returns formatted price to the grid
        /// </summary>
        /// <param name="productPrice">The value of Product Price</param>
        /// <returns>Returns the formatted price</returns>
        protected string FormatPrice(object productPrice)
        {
            if (productPrice != null)
            {
                if (productPrice.ToString().Length > 0)
                {
                    return decimal.Parse(productPrice.ToString()).ToString("c");
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Get the products changes field
        /// </summary>
        /// <param name="vendor">vendor Name</param>
        /// <param name="portalId">Portal Id to get the vendor name.</param>
        /// <returns>Returns Vendor Name for Mall Admin and Store name for Franchise Admin</returns>
        protected string GetVendorStoreName(object vendor, object portalId)
        {
            string storeName = string.Empty;

            if (!string.IsNullOrEmpty(vendor.ToString()))
            {
                return vendor.ToString();
            }
            else
            {
                PortalAdmin portalAdmin = new PortalAdmin();
                storeName = portalAdmin.GetStoreNameByPortalID(Convert.ToString(portalId));
                return storeName.ToString();
            }
        }

        /// <summary>
        /// Get the products changes field
        /// </summary>
        /// <param name="currentProductId">Product ID</param>
        /// <returns>Returns changed fields, If there are no changes then returns empty string.</returns>
        protected string GetChangedFields(object currentProductId)
        {
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            return productReviewHistoryAdmin.GetChangedFields(Convert.ToInt32(currentProductId));
        }

        /// <summary>
        /// Grid Row Data Bound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Dictionary<int, string> productIdList = (Dictionary<int, string>)Session[this.selectedItemsKey];

                if (productIdList != null)
                {
                    CheckBox check = (CheckBox)e.Row.Cells[0].FindControl("chkProduct") as CheckBox;
                    int id = int.Parse(e.Row.Cells[1].Text);

                    if (productIdList.ContainsKey(id) && check != null)
                    {
                        check.Checked = true;
                    }
                }
            }
        }
        #endregion

        #region Helper Functions
        /// <summary>
        /// Change the product status as Approved or Declined.
        /// </summary>
        /// <param name="state">ZNodeProductReviewState enum</param>    
        protected void ChangeProductStatus(ZNodeProductReviewState state)
        {
            this.RememberOldValues();

            Dictionary<int, string> productIdList = (Dictionary<int, string>)Session[this.selectedItemsKey];



            if (productIdList == null || productIdList.Count == 0)
            {
                lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoProductSelected").ToString();
                return;
            }

            if (state == ZNodeProductReviewState.Approved)
            {
                ProductAdmin AdminAccess = new ProductAdmin();
                ProductReviewStateAdmin reviewstateAdmin = new ProductReviewStateAdmin();
                ProductReviewState reviewState = reviewstateAdmin.GetProductReviewStateByName(state.ToString());
                StringBuilder sb = new StringBuilder();

                // Loop through the grid values
                if (productIdList != null && reviewState != null)
                {
                    //Get all product details 
                    List<SupplierAdmin> listProductDetails = new List<SupplierAdmin>();
                    AccountService accountService = new AccountService();
                    foreach (KeyValuePair<int, string> pair in productIdList)
                    {
                        // Get ProductId
                        int productId = pair.Key;
                        string productName = pair.Value;

                        ZNode.Libraries.DataAccess.Entities.Product entity = AdminAccess.GetByProductId(productId);

                        // Update only if existing status is not same as current status
                        if (entity.ReviewStateID != reviewState.ReviewStateID)
                        {
                            if (entity.AccountID > 0)
                            {
                                //Get productId, productName, vendorEmail
                                string vendorEmail = accountService.GetByAccountID((int)entity.AccountID).Email;
                                listProductDetails.Add(new SupplierAdmin { AccountId = (int)entity.AccountID, ProductId = productId, ProductName = productName, VendorEmail = vendorEmail });
                            }
                            entity.ReviewStateID = reviewState.ReviewStateID;
                            AdminAccess.Update(entity);
                        }

                        this.UpdateReviewHistory(productId, (ZNodeProductReviewState)entity.ReviewStateID, Convert.ToInt32(entity.AccountID));
                    }

                    //Send Mail 
                    if (listProductDetails.Count > 0)
                    {
                        this.SendMailBySiteAdmin(listProductDetails);
                    }
                }

                // Clear the selection.
                Session[this.selectedItemsKey] = null;

                this.BindSearchProduct();
            }
            else
            {
                // If product review state is Declined then redirect to Reject product page. List if product ID values stored in session.            
                Response.Redirect("RejectProduct.aspx?ReturnUrl=List.aspx");
            }
        }

        /// <summary>
        /// Format the Price of a product and return in string format
        /// </summary>
        /// <param name="reviewStateID">The value of ReviewStateID</param>
        /// <returns>Returns the Review Status</returns>
        protected string GetStausMark(string reviewStateID)
        {
            string imageSrc = "~/Themes/Images/223-point_pendingapprove.gif";

            if (reviewStateID != string.Empty)
            {
                int stateId = Convert.ToInt32(reviewStateID);

                if (stateId == Convert.ToInt32(ZNodeProductReviewState.Approved))
                {
                    imageSrc = "~/Themes/Images/222-point_approve.gif";
                }

                if (stateId == Convert.ToInt32(ZNodeProductReviewState.Declined))
                {
                    imageSrc = "~/Themes/Images/221-point_decline.gif";
                }

                if (stateId == Convert.ToInt32(ZNodeProductReviewState.ToEdit))
                {
                    imageSrc = "~/Themes/Images/220-point_toedit.gif";
                }
            }

            return imageSrc;
        }

        /// <summary>
        /// Format the Price of a product and return in string format
        /// </summary>
        /// <param name="fieldvalue">The value of fieldvalue</param>
        /// <returns>Returns the Price</returns>
        protected string DisplayPrice(object fieldvalue)
        {
            if (fieldvalue == DBNull.Value)
            {
                return "$0.00";
            }
            else
            {
                if (Convert.ToInt32(fieldvalue) == 0)
                {
                    return "$0.00";
                }
                else
                {
                    return String.Format("${0:#.00}", fieldvalue);
                }
            }
        }

        /// <summary>
        /// Get ImagePath
        /// </summary>
        /// <param name="Imagefile">The value of Image File</param>
        /// <returns>Returns the Image Path</returns>
        protected string GetImagePath(string Imagefile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(Imagefile);
        }

        /// <summary>
        /// Remember Old Values Method
        /// </summary>
        private void RememberOldValues()
        {
            Dictionary<int, string> productIdList = new Dictionary<int, string>();

            // Loop through the grid values
            foreach (GridViewRow row in uxGrid.Rows)
            {
                CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProduct") as CheckBox;

                // Check in the Session
                if (Session[this.selectedItemsKey] != null)
                {
                    productIdList = (Dictionary<int, string>)Session[this.selectedItemsKey];
                }

                int id = int.Parse(row.Cells[1].Text);

                if (check.Checked)
                {
                    if (!productIdList.ContainsKey(id))
                    {
                        HyperLink hlnk = row.Cells[3].Controls[0] as HyperLink;
                        productIdList.Add(id, hlnk.Text);
                    }
                }
                else
                {
                    productIdList.Remove(id);
                }
            }

            if (productIdList.Count > 0)
            {
                Session[this.selectedItemsKey] = productIdList;
            }
        }

        /// <summary>
        /// Bind Portals
        /// </summary>
        private void BindPortal()
        {
            PortalAdmin portalAdmin = new PortalAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.Portal> portals = portalAdmin.GetAllPortals();

            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                if (profiles.StoreAccess != string.Empty)
                {
                    portals.Filter = string.Concat("PortalID IN [", profiles.StoreAccess, "]");
                }
            }
            else
            {
                Portal po = new Portal();
                po.StoreName = this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownTextAllStores").ToString().ToUpper();
                portals.Insert(0, po);
            }

            // Portal Drop Down List
            ddlPortal.DataSource = portals;
            ddlPortal.DataTextField = "StoreName";
            ddlPortal.DataValueField = "PortalID";
            ddlPortal.DataBind();
        }

        /// <summary>
        /// Search a Product by name,productnumber,sku,manufacturer,category,producttype
        /// </summary>
        private void BindSearchProduct()
        {
            ProductAdmin prodadmin = new ProductAdmin();
            string VendorId = string.Empty;
            UserStoreAccess uss = new UserStoreAccess();

            AccountService accountService = new AccountService();
            bool isVendorIdValid = true;

            if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Vendor)
            {
                TList<Account> accountList = accountService.GetByUserID((Guid)Membership.GetUser().ProviderUserKey);
                if (accountList != null && accountList.Count > 0)
                {
                    VendorId = accountList[0].AccountID.ToString();
                }
            }
            else if (!string.IsNullOrEmpty(txtVendorId.Text))
            {
                AccountQuery query = new AccountQuery();
                query.Append(AccountColumn.ExternalAccountNo, txtVendorId.Text);
                query.AppendIsNotNull(AccountColumn.UserID);
                TList<Account> accountList = accountService.Find(query.GetParameters());
                if (accountList != null && accountList.Count > 0)
                {
                    VendorId = accountList[0].AccountID.ToString();
                }
                else
                {
                    // Given Account number doesn't found.
                    isVendorIdValid = false;
                }
            }

            DataSet ds = null;
            DataView dv = null;
            if (isVendorIdValid)
            {
                ds = prodadmin.SearchVendorProduct(Server.HtmlEncode(txtproductname.Text.Trim()), txtVendorproductId.Text.Trim(), txtVendor.Text.Trim(), VendorId, txtProductStatus.Value, txt2coid.Text);
                dv = new DataView(ds.Tables[0]);
                dv.Sort = "DisplayOrder";
                if (ddlPortal.SelectedValue == string.Empty)
                {
                    dv.RowFilter = "Vendor IS NOT NULL";
                }
                else if (ddlPortal.SelectedValue == "0")
                {
                    ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                    if (string.Compare(profiles.StoreAccess, "AllStores") == 0)
                    {
                        if (profiles.StoreAccess != string.Empty)
                        {
                            dv.RowFilter = "Vendor IS NOT NULL OR PortalId IS NOT NULL";
                        }
                    }
                    else
                    {
                        dv.RowFilter = "PortalId IN (" + profiles.StoreAccess + ")";
                    }
                }
                else
                {
                    dv.RowFilter = "PortalId = " + ddlPortal.SelectedValue;
                }
            }
            else
            {
                dv = new DataView();
            }

            btnApproveAll.Enabled = btnDeclineall.Enabled = dv.Count > 0 ? true : false;
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
            if (uxGrid.Rows.Count == 0)
            {
                divButton.Visible = false;
                return;
            }
            divButton.Visible = true;
        }

        #endregion

        #region Private Methods
        /// <summary>
        /// Update Review History Method
        /// </summary>
        /// <param name="itemID">The value of ItemID</param>
        /// <param name="state">The value of Product Review State</param>
        /// <param name="vendorId">The value of Vendor Id</param>
        private void UpdateReviewHistory(int itemID, ZNodeProductReviewState state, int vendorId)
        {
            // Add entry to Product Review history table.
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            ProductReviewHistory productReviewHistory = new ProductReviewHistory();
            productReviewHistory.ProductID = itemID;
            productReviewHistory.VendorID = vendorId;
            productReviewHistory.LogDate = DateTime.Now;
            productReviewHistory.Username = this.Page.User.Identity.Name;
            productReviewHistory.Status = state.ToString();
            productReviewHistory.Reason = state.ToString();
            productReviewHistory.Description = state.ToString();
            bool isSuccess = productReviewHistoryAdmin.Add(productReviewHistory);
        }

        #endregion

        #region Znode version 7.2.2 Send Mail

        /// <summary>
        /// Znode version 7.2.2 
        /// This function will send  mail notification to vendor.
        /// To inform product is approved by Site Admin.
        /// </summary>
        /// <param name="productList">productList</param>
        protected void SendMailBySiteAdmin(List<SupplierAdmin> productList)
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.AdminEmail;
            string siteAdmin = ZNodeConfigManager.SiteConfig.StoreName;
            string currentCulture = string.Empty;
            string templatePath = string.Empty;
            string defaultTemplatePath = string.Empty;

            int productId = 0;
            string productName = string.Empty;
            string vendorEmail = string.Empty;

            foreach (SupplierAdmin pair in productList)
            {
                productId = pair.ProductId;
                productName = pair.ProductName;
                string productId2 = productId.ToString();

                //Get Vendor Email Id
                string receiverEmail = pair.VendorEmail;

                //Subject is set from global resources of admin.
                string subject = productName + " " + HttpContext.GetGlobalResourceObject("CommonCaption", "SubjectProductApprove");

                // Template selection
                currentCulture = ZNodeCatalogManager.CultureInfo;
                defaultTemplatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ApproveMultipleProducts.html"));

                templatePath = (!string.IsNullOrEmpty(currentCulture))
                    ? Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ApproveMultipleProducts_" + currentCulture + ".html")
                    : Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ApproveMultipleProducts.html"));

                // Check the default template file existence.
                if (!File.Exists(defaultTemplatePath))
                {
                    return;
                }
                else
                {
                    // If language specific template not available then load default template.
                    templatePath = defaultTemplatePath;
                }

                StreamReader rw = new StreamReader(templatePath);
                string messageText = rw.ReadToEnd();

                Regex rx1 = new Regex("#PRODUCTID#", RegexOptions.IgnoreCase);
                messageText = rx1.Replace(messageText, productId2);

                Regex rx2 = new Regex("#PRODUCTNAME#", RegexOptions.IgnoreCase);
                messageText = rx2.Replace(messageText, productName);

                Regex rx3 = new Regex("#SITEADMIN#", RegexOptions.IgnoreCase);
                messageText = rx3.Replace(messageText, siteAdmin);

                Regex rx4 = new Regex("#SITEADMINEMAIL#", RegexOptions.IgnoreCase);
                messageText = rx4.Replace(messageText, senderEmail);

                Regex RegularExpressionLogo = new Regex("#LOGO#", RegexOptions.IgnoreCase);
                messageText = RegularExpressionLogo.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);

                try
                {
                    // Send mail
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(receiverEmail, senderEmail, string.Empty, subject, messageText, true);

                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralMessage, receiverEmail, Request.UserHostAddress.ToString(), null, ex.Message, null);
                }
            }
        }
        #endregion
    }

}