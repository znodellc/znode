<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Vendors.VendorProducts.AddView" CodeBehind="AddView.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/Controls/Default/ImageUploader.ascx" TagName="UploadImage"
    TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblHeading" runat="server" />
            </h1>
        </div>
        <div style="text-align: right"> 
             <zn:Button runat="server" ID="btnSubmitTop"  ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" />
             <zn:Button runat="server"  ID="btnCancelTop" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
           
        </div>
        <div class="ClearBoth">
            <p> 
                 <asp:Localize ID="TextThumbNil" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextThumbNil %>'></asp:Localize>
            </p>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <asp:Label ID="lblError" CssClass="Error" runat="server"></asp:Label>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="FormView">
            <div class="FieldStyle">
                  <asp:Localize ID="ColumnTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitle %>'></asp:Localize><br />
                <small>  <asp:Localize ID="TextLeaveblankfornoTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextLeaveblankfornoTitle %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txttitle" runat="server" Columns="30" MaxLength="30"></asp:TextBox>
            </div>

            <div id="divImageType" runat="server">
                <div class="FieldStyle">
                      <asp:Localize ID="ColumnTitleImageType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleImageType %>'></asp:Localize><br />
                    <small> <asp:Localize ID="TextAlterNateImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextAlterNateImage %>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlImageType" runat="server"
                        AutoPostBack="false">
                    </asp:DropDownList>
                </div>
            </div>

            <div class="ClearBoth" style="width: 100%">
                <span style="font-size: 10.5pt; font-weight: bold; color: #404040; font-family: Calibri, arial;">
                    <asp:Label ID="lblImage" runat="server"></asp:Label></span><span class="Asterix">*</span><br />
                <div id="ProductHint" runat="server" visible="false" style="width: 100%">
                    <small> <asp:Localize ID="HintTextProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextProduct %>'></asp:Localize></small>
                </div>
                <div id="SwatchHint" runat="server" visible="false" style="width: 100%">
                    <small><asp:Localize ID="Localize6" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintSubtextUploadSuitableSwatchImage %>'></asp:Localize></small>
                </div>
            </div>
            <div class="ClearBoth">
            </div>
            <div id="tblShowImage" runat="server" visible="true" style="margin: 10px;">
                <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;">
                    <asp:Image ID="Image1" runat="server" />
                </div>
                <div class="LeftFloat" style="padding-left: 10px; margin-left: -1px; border-left: solid 1px #cccccc;">
                    <asp:Panel runat="server" ID="pnlShowOption">
                        <div>
                             <asp:Localize ID="ColumnTitleSelectanOption" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectanOption %>'></asp:Localize>
                        </div>
                        <div>
                            <asp:RadioButton ID="RadioProductCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonCurrentImage %>' runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductCurrentImage_CheckedChanged"
                                Checked="True" /><br />
                            <asp:RadioButton ID="RadioProductNewImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNewImage %>' runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductNewImage_CheckedChanged" />
                        </div>
                    </asp:Panel>
                    <br />
                    <div id="tblProductDescription" runat="server" visible="false">
                        <div class="FieldStyle">
                              <asp:Localize ID="ColumnSelectImageColon" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectImageColon %>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <ZNode:UploadImage ID="UploadProductImage" runat="server"></ZNode:UploadImage>
                        </div>
                    </div>
                    <div class="FieldStyle">
                          <asp:Localize ID="Localize9" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductImageALTText %>'></asp:Localize><br />
                        <small>  <asp:Localize ID="Localize10" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextShortDescriptive %>'></asp:Localize></small>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtImageAltTag" runat="server"></asp:TextBox>
                    </div>
                    <div>
                        <asp:Label ID="lblProductImageError" runat="server" CssClass="Error" ForeColor="Red"
                            Text="" Visible="False"></asp:Label>
                    </div>
                    <div id="AlternateThumbnail" runat="server" visible="false">
                        <div class="FieldStyle">
                              <asp:Localize ID="Localize11" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextImageName %>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtAlternateThumbnail" runat="server" Columns="30" MaxLength="30"></asp:TextBox>
                        </div>
                    </div>
                    <asp:Panel ID="ImagefileName" runat="server" Visible="false">
                        <div class="FieldStyle">
                            <asp:Label ID="lblImageName" runat="server"></asp:Label>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtimagename" runat="server"></asp:TextBox>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <asp:Panel ID="ProductSwatchPanel" runat="server" Visible="false">
                <div class="FieldStyle">
                    <asp:Label ID="lblProductSwatchImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductImage %>'></asp:Label>
                </div>
                <div class="ClearBoth">
                </div>
                <div>
                    <small> 
                         <asp:Localize ID="HintTextUploadProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextUploadProduct %>'></asp:Localize>

                    </small>
                </div>
                <div class="ClearBoth">
                </div>
                <div id="tblShowProductSwatchImage" runat="server" visible="false" style="margin: 10px;">
                    <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;">
                        <asp:Image ID="ProductSwatchImage" runat="server" />
                    </div>
                    <div class="LeftFloat" style="padding-left: 10px; margin-left: -1px; border-left: solid 1px #cccccc;">
                        <asp:Panel runat="server" ID="pnlShowSwatchOption">
                            <div>
                                 <asp:Localize ID="SelectanOption" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectanOption %>'></asp:Localize>
                            </div>
                            <div>
                                <asp:RadioButton ID="RbtnProductSwatchCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonCurrentImage %>' runat="server"
                                    GroupName="Product Swatch Image" AutoPostBack="True" OnCheckedChanged="RbtnProductSwatchCurrentImage_CheckedChanged"
                                    Checked="True" /><br />
                                <asp:RadioButton ID="RbtnProductSwatchNewImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNewImage %>' runat="server"
                                    GroupName="Product Swatch Image" AutoPostBack="True" OnCheckedChanged="RbtnProductSwatchNewImage_CheckedChanged" /><br />
                                <asp:RadioButton ID="RbtnProductSwatchNoImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNoImage %>' runat="server" GroupName="Product Swatch Image"
                                    AutoPostBack="True" OnCheckedChanged="RbtnProductSwatchNoImage_CheckedChanged" />
                            </div>
                        </asp:Panel>
                        <br />
                        <div id="tblProductSwatchDescription" runat="server" visible="false">
                            <div class="FieldStyle">
                                  <asp:Localize ID="TextColumnSelectImageColon" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectImageColon %>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <ZNode:UploadImage ID="UploadProductSwatchImage" runat="server"></ZNode:UploadImage>
                            </div>
                        </div>
                        <div class="ClearBoth">
                            <asp:Label ID="Label1" runat="server" CssClass="Error" ForeColor="Red" Text="" Visible="False"></asp:Label>
                        </div>
                    </div>
                </div>
            </asp:Panel>
            <div class="ClearBoth">
                <br />
            </div>
            <div class="FieldStyle">
                  <asp:Localize ID="Localize15" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayonProductPage %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID='VisibleInd' runat='server' Checked="true" Text='<%$ Resources:ZnodeAdminResource, CheckBoxShowThumbnail %>'></asp:CheckBox>
            </div>
            <div class="FieldStyle">
                  <asp:Localize ID="Localize16" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayonCategoryPage %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID='VisibleCategoryInd' runat='server' Text= '<%$ Resources:ZnodeAdminResource, CheckBoxDisplayCategoryPage %>'/>
            </div>
            <div class="FieldStyle">
                  <asp:Localize ID="Localize17" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>'></asp:Localize><span class="Asterix">*</span><br />
                <small>  <asp:Localize ID="HintTextEnteranumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, HintTextEnteranumber %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="DisplayOrder" runat="server" MaxLength="5">500</asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="DisplayOrder"
                    CssClass="Error" Display="Dynamic" ErrorMessage= '<%$ Resources:ZnodeAdminResource, RequiredProductDisplayOrder %>'></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="DisplayOrder"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>' ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                <asp:RangeValidator ID="rvDisplayOrder" CssClass="Error"
                    MinimumValue="1" ControlToValidate="DisplayOrder" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnteraWholeNumber %>' MaximumValue="10000"
                    Display="Dynamic" SetFocusOnError="true" runat="server" Type="Integer"></asp:RangeValidator>
            </div>
        </div>
        <div class="ClearBoth">
            <br />
        </div>
        <div> 
             <zn:Button runat="server" ID="btnSubmitBottom"  ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" />
             <zn:Button runat="server"  ID="btnCancelBottom" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
        </div>
    </div>
</asp:Content>
