﻿<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Vendors.VendorProducts.AcceptProduct"
    Title="Manage Vendor Products - AcceptProduct" CodeBehind="AcceptProduct.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <div class="LeftFloat">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div class="Display">
            <div>
                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
            </div>
            <div class="ValueStyle">
                <asp:Localize runat="server" ID="ColumnTitleProductID" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductID%>'></asp:Localize>:
                <asp:Label ID="lblProductID" runat="server"></asp:Label>
                <br />
                <asp:Label ID="lblVendor" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleVendors%>'></asp:Label>
            </div>
            <div class="ClearBoth">
            </div>
            <div style="text-align: center">
                <asp:Label ID="lblApprovedMsg" runat="server" Text=""></asp:Label>
            </div>
            <div>
                <br />
            </div>
            <div>
                <div class="FieldStyle" style="text-align: right; width: 60%">
                    <div>
                        <zn:Button ID="btnBack" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonBackToProduct%>'
                            runat="server" OnClick="BtnBack_Click" Width="210px" ButtonType="EditButton"/>
                    </div>
                </div>
                <div class="ValueStyle" style="padding-top: 3px;">
                    <div runat="server" id="divNextProduct">
                         <zn:Button ID="btnNextProduct" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonReview%>'
                            runat="server" OnClick="BtnNextProduct_Click" Width="175px" ButtonType="EditButton"/>
                    </div>
                </div>
            </div>
            <div>
                <br />
            </div>
        </div>
    </div>
</asp:Content>
