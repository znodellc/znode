using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.ECommerce.Utilities;
using Znode.Engine.Common;

namespace  Znode.Engine.Admin.Secure.Vendors.VendorProducts
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_Reviewer_add_view class
    /// </summary>
    public partial class AddView : System.Web.UI.Page
    {
        #region Protected member variable
        private int ItemId = 0;
        private int ProductImageId = 0;
        private int ProductTypeId = 0;
        private string mode = string.Empty;
        private string AssociateName = string.Empty;
        private string AddPageLink = "~/Secure/Vendors/VendorProducts/Add.aspx?itemid=";
        private string ViewPageLink = "~/Secure/Vendors/VendorProducts/View.aspx?itemid=";
        private string returnUrl = string.Empty;
        private ProductViewAdmin imageAdmin = new ProductViewAdmin();
        private ProductImage productImage = new ProductImage();
        #endregion

        #region page load
        protected void Page_Load(object sender, EventArgs e)
        {
            // Product ID
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }
            else if (Session["itemid"] != null)
            {
                this.ItemId = int.Parse(Session["itemid"].ToString());
            }

            // Uniue Product Image ID
            if (Request.Params["productimageid"] != null)
            {
                this.ProductImageId = int.Parse(Request.Params["productimageid"].ToString());
            }

            // Product Image Type Id(1- Alternate Image View, 2- Swatch
            if (Request.Params["typeid"] != null)
            {
                this.ProductTypeId = int.Parse(Request.Params["typeid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                divImageType.Visible = false;

                this.BindImageType();

                if ((this.ItemId > 0) && (this.ProductImageId > 0))
                {
                    if (ddlImageType.SelectedValue == "2")
                    {
                        lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditSwatchImage").ToString();
                    }
                    else
                    {
                        lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditProductImage").ToString();
                    }

                    this.BindDatas();

                    tblShowImage.Visible = true;
                    tblShowProductSwatchImage.Visible = true;
                    txtimagename.Visible = true;
                    ImagefileName.Visible = true;
                }
                else
                {
                    // Show the image upload control, when adding new product.
                    tblShowImage.Visible = true;
                    tblProductDescription.Visible = true;
                    pnlShowOption.Visible = false;
                    Image1.Visible = false;
                    if (ddlImageType.SelectedValue == "2")
                    {
                        lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddSwatchImage").ToString();
                    }
                    else
                    {
                        lblHeading.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddProductImage").ToString();
                    }
                }

                ddlImageType.SelectedIndex = ddlImageType.Items.IndexOf(ddlImageType.Items.FindByValue(this.ProductTypeId.ToString()));

                this.SetVisibleData();
            }
        }
        #endregion

        #region General events
        /// <summary>
        /// Submit Button click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();

            // Add up to 10 additional images of product such as front and back views or closeups that show extra detail.
            DataSet ds = this.imageAdmin.GetAllAlternateImage(this.ItemId);

            // Filter based on image types
            string filterExpression = "ProductImageTypeID=" + Convert.ToString(this.ProductTypeId);
            ds.Tables[0].DefaultView.RowFilter = filterExpression;

            System.IO.FileInfo fileInfo = null;
            System.IO.FileInfo swatchfileInfo = null;
            bool swatchSelected = false;
            bool newProductImage = false;

            if (this.ProductImageId > 0)
            {
                this.productImage = this.imageAdmin.GetByProductImageID(this.ProductImageId);
            }
            else
            {
                // Check image validation only for new image
                if (ds.Tables[0].DefaultView.Count >= 5)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorOnlyfivealternateImagesAllowed").ToString();
                    return;
                }
            }

            if (this.productImage.ProductImageTypeID.GetValueOrDefault(0) != Convert.ToInt32(ddlImageType.SelectedValue))
            {
                // If swatch is selected
                swatchSelected = true && Convert.ToInt32(ddlImageType.SelectedValue) == 2;
            }

            this.productImage.Name = Server.HtmlEncode(txttitle.Text);
            this.productImage.ActiveInd = VisibleInd.Checked;
            this.productImage.ShowOnCategoryPage = VisibleCategoryInd.Checked;
            this.productImage.ProductID = this.ItemId;
            this.productImage.ProductImageTypeID = Convert.ToInt32(ddlImageType.SelectedValue);

            this.productImage.DisplayOrder = int.Parse(DisplayOrder.Text.Trim());
            this.productImage.ImageAltTag = txtImageAltTag.Text.Trim();
            this.productImage.AlternateThumbnailImageFile = txtAlternateThumbnail.Text.Trim();
            this.productImage.ReviewStateID = Convert.ToInt32(ZNodeProductReviewState.Approved);
            UploadProductImage.AppendToFileName = "-" + this.ItemId.ToString();
            UploadProductSwatchImage.AppendToFileName = "-" + this.ItemId.ToString();

            // Validate image
            if ((this.ProductImageId == 0) || (RadioProductNewImage.Checked == true))
            {
                if (this.GetCurrentProductImageType() == ZNodeProductImageType.Swatch)
                {
                    if (!UploadProductSwatchImage.IsFileNameValid())
                    {
                        return;
                    }

                    // Check for Product View Image
                    fileInfo = UploadProductSwatchImage.FileInformation;
                }
                else
                {
                    // UploadProductImage.AppendToFileName = DateTime.Now.ToString("ddMMyyyyhhmmss");
                    if (!UploadProductImage.IsFileNameValid())
                    {
                        return;
                    }

                    // Check for Product View Image
                    fileInfo = UploadProductImage.FileInformation;
                }

                if (fileInfo != null)
                {
                    this.productImage.ImageFile = fileInfo.Name;
                }
            }
            else
            {
                this.productImage.ImageFile = this.productImage.ImageFile;
            }

            if (this.GetCurrentProductImageType() == ZNodeProductImageType.Swatch)
            {
                if (!RbtnProductSwatchNoImage.Checked)
                {
                    // Validate Product Swatch image
                    if ((this.ProductImageId == 0) || this.productImage.AlternateThumbnailImageFile.Length == 0 || RbtnProductSwatchNewImage.Checked)
                    {
                        UploadProductSwatchImage.AppendToFileName = DateTime.Now.ToString("ddMMyyyyhhmmss");
                        if (!UploadProductSwatchImage.IsFileNameValid())
                        {
                            return;
                        }

                        if (UploadProductSwatchImage.PostedFile.FileName.Trim().Length != 0)
                        {
                            swatchfileInfo = UploadProductSwatchImage.FileInformation;

                            if (swatchfileInfo != null)
                            {
                                this.productImage.AlternateThumbnailImageFile = swatchfileInfo.Name;
                                newProductImage = true;
                            }

                            RbtnProductSwatchNewImage.Checked = true;
                        }
                    }
                    else
                    {
                        this.productImage.AlternateThumbnailImageFile = this.productImage.AlternateThumbnailImageFile;
                    }
                }
                else
                {
                    this.productImage.AlternateThumbnailImageFile = string.Empty;
                    this.productImage.ImageFile = string.Empty;
                    this.imageAdmin.Update(this.productImage);
                }
            }

            // Upload File if this is a new product or the New Image option was selected for an existing product
            if (RadioProductNewImage.Checked || this.ProductImageId == 0)
            {
                if (fileInfo != null)
                {
                    if (this.GetCurrentProductImageType() == ZNodeProductImageType.AlternateImage)
                    {
                        UploadProductImage.SaveImage();
                    }
                }
            }

            // Upload File if this is a new product or the New Image option was selected for an existing product
            if (RbtnProductSwatchNewImage.Checked || this.ProductImageId == 0)
            {
                if (swatchfileInfo != null)
                {
                    UploadProductSwatchImage.SaveImage();

                    // Create fileInfo for Original product View Image
                    swatchfileInfo = UploadProductSwatchImage.FileInformation;
                    this.productImage.AlternateThumbnailImageFile = swatchfileInfo.Name;
                    this.productImage.ImageFile = swatchfileInfo.Name;

                    // Release all resources used
                    UploadProductSwatchImage.Dispose();
                }
            }

            if (swatchSelected && this.ProductImageId > 0 && !newProductImage)
            {
                this.productImage.AlternateThumbnailImageFile = this.productImage.ImageFile;

                // Create fileInfo for Original product View Image
                fileInfo = UploadProductImage.FileInformation;
            }

            bool isSuccess = false;

            string AlternateImage = string.Empty;

            product = productAdmin.GetByProductId(this.ItemId);

            if (this.ProductImageId > 0)
            {
                // Update the Imageview
                this.AssociateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditAlternateImage").ToString() + this.productImage.ImageFile + " - " + product.Name;
                isSuccess = this.imageAdmin.Update(this.productImage);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, product.Name);
            }
            else
            {
                this.AssociateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddedAlternateImages").ToString() + this.productImage.ImageFile + " - " + product.Name;
                isSuccess = this.imageAdmin.Insert(this.productImage);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, product.Name);
            }

            // Add to product review history.
            this.AddToProductReviewHistory(Convert.ToInt32(product.SupplierID));

            if (isSuccess)
            {
                Response.Redirect(this.GetReturnPageUrl());
            }
            else
            {
                // Display error message
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoteAdd").ToString();
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.GetReturnPageUrl());
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = true;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RbtnProductSwatchCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductSwatchDescription.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RbtnProductSwatchNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductSwatchDescription.Visible = true;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RbtnProductSwatchNoImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductSwatchDescription.Visible = false;
            txtAlternateThumbnail.Text = string.Empty;
            txtimagename.Text = string.Empty;
        }

        /// <summary>
        /// Dropdown list Seclected Index changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ImageType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.SetVisibleData();
        }

        /// <summary>
        /// Set Visible Data Method
        /// </summary>
        protected void SetVisibleData()
        {
            AlternateThumbnail.Visible = false;
            SwatchHint.Visible = false;
            ProductHint.Visible = false;
            ProductSwatchPanel.Visible = false;
            if (this.GetCurrentProductImageType() == ZNodeProductImageType.Swatch)
            {
                ddlImageType.SelectedValue = Convert.ToInt32(ZNodeProductImageType.Swatch).ToString();
            }
            else
            {
                ddlImageType.SelectedValue = Convert.ToInt32(ZNodeProductImageType.AlternateImage).ToString();
            }

            if (this.GetCurrentProductImageType() == ZNodeProductImageType.Swatch)
            {
                AlternateThumbnail.Visible = true;
                lblImage.Text =  this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnSwatchImage").ToString();
                lblImageName.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextSwatchImageFileName").ToString();
                ProductSwatchPanel.Visible = true;
                tblShowImage.Visible = false;
                ProductHint.Visible = false;
                lblProductSwatchImage.Visible = false;
                if (this.ProductImageId <= 0)
                {
                    pnlShowSwatchOption.Visible = false;
                    ProductSwatchImage.Visible = false;
                    tblShowProductSwatchImage.Visible = true;
                }

                tblProductSwatchDescription.Visible = true;

                if (this.ProductImageId > 0)
                {
                    tblProductSwatchDescription.Visible = false;
                }
            }
            else
            {
                AlternateThumbnail.Visible = false;
                lblImage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnProductImage").ToString();
                lblImageName.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextImageName").ToString();
                ProductHint.Visible = true;
            }
        }
        #endregion

        #region Bind event
        /// <summary>
        /// Bind Datas
        /// </summary>
        private void BindDatas()
        {
            this.productImage = this.imageAdmin.GetByProductImageID(this.ProductImageId);
            ZNodeImage znodeImage = new ZNodeImage();
            if (this.productImage != null)
            {
                txtimagename.Text = Server.HtmlDecode(this.productImage.ImageFile);
                txttitle.Text = Server.HtmlDecode(this.productImage.Name);
                VisibleInd.Checked = this.productImage.ActiveInd;
                VisibleCategoryInd.Checked = this.productImage.ShowOnCategoryPage;
                Image1.ImageUrl = znodeImage.GetImageHttpPathSmall(this.productImage.ImageFile);
                ddlImageType.SelectedValue = this.productImage.ProductImageTypeID.ToString();
                DisplayOrder.Text = this.productImage.DisplayOrder.GetValueOrDefault().ToString();
                txtImageAltTag.Text = this.productImage.ImageAltTag;
                txtAlternateThumbnail.Text = this.productImage.AlternateThumbnailImageFile;

                if (txtAlternateThumbnail.Text != string.Empty)
                {
                    ProductSwatchImage.ImageUrl = znodeImage.GetImageHttpPathSmall(this.productImage.AlternateThumbnailImageFile);
                    tblShowProductSwatchImage.Visible = true;
                }
                else
                {
                    RbtnProductSwatchNoImage.Checked = true;
                    RbtnProductSwatchCurrentImage.Visible = false;
                }

                if (string.IsNullOrEmpty(ProductSwatchImage.ImageUrl))
                {
                    ProductSwatchImage.ImageUrl = znodeImage.GetImageHttpPathSmall(string.Empty);
                }
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Add an entry to product review history
        /// </summary>
        /// <param name="vendorId">The value of Vendor ID</param>
        /// <returns>Returns true if success else false.</returns>
        private bool AddToProductReviewHistory(int vendorId)
        {
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            ProductReviewHistory productReviewHistory = new ProductReviewHistory();
            productReviewHistory.ProductID = this.ItemId;
            productReviewHistory.VendorID = vendorId;
            productReviewHistory.LogDate = DateTime.Now;
            productReviewHistory.Username = this.Page.User.Identity.Name;
            productReviewHistory.Status = ZNodeProductReviewState.Approved.ToString();
            productReviewHistory.Reason = string.Empty;
            productReviewHistory.Description = this.ProductTypeId == 1 ? "Alternate Image" : "Swatch Image";
            return productReviewHistoryAdmin.Add(productReviewHistory);
        }

        /// <summary>
        /// Get the return page Url using query string parameter.
        /// </summary>
        /// <returns>Returns the Page Url</returns>
        private string GetReturnPageUrl()
        {
            // Mode value represetnts the which tab to be highlighted in view page.
            if (Request.Params["mode"] != null)
            {
                this.returnUrl = this.ViewPageLink + this.ItemId + "&mode=alternateimages";
            }
            else
            {
                // Set Product Wizard Step Index
                Session["stepid"] = 1;

                // Request comes from Add Product Wizard Page.
                this.returnUrl = this.AddPageLink + this.ItemId;
            }

            return this.returnUrl;
        }

        /// <summary>
        /// Get the current product image type using query string value. If query string null it will return alternate image type.
        /// </summary>
        /// <returns>Returns ZNodeProductImageType enum object</returns>
        private ZNodeProductImageType GetCurrentProductImageType()
        {
            // Alternate image is Default
            int productImageTypeId = 1;

            // Product Image Type Id(1- Alternate Image View, 2- Swatch
            if (Request.Params["typeid"] != null)
            {
                productImageTypeId = int.Parse(Request.Params["typeid"].ToString());
            }

            ZNodeProductImageType imageType = (ZNodeProductImageType)Enum.ToObject(typeof(ZNodeProductImageType), productImageTypeId);
            return imageType;
        }

        /// <summary>
        /// Binds ImageType Type drop-down list
        /// </summary>
        private void BindImageType()
        {
            ZNode.Libraries.Admin.ProductViewAdmin imageadmin = new ProductViewAdmin();
            ddlImageType.DataSource = imageadmin.GetImageType();
            ddlImageType.DataTextField = "Name";
            ddlImageType.DataValueField = "ProductImageTypeID";
            ddlImageType.DataBind();

            this.SetVisibleData();
        }
        #endregion
    }
}