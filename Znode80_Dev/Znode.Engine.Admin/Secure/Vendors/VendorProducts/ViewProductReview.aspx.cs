﻿using System;
using System.Web;
using System.Web.UI;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Vendors.VendorProducts
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_Product_ViewProductReview  class
    /// </summary>
    public partial class ViewProductReview : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private int ReviewHistoryId = 0;
        private string ManagePageLink = "~/Secure/Vendors/VendorProducts/View.aspx?ItemId=";
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (Request.Params["historyid"] != null)
            {
                this.ReviewHistoryId = int.Parse(Request.Params["historyid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                if (this.ReviewHistoryId > 0)
                {
                    // Bind review history Details
                    this.Bind();
                }
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ManagePageLink + this.ItemId);
        }

        #endregion

        #region Private Methods
        private void Bind()
        {
            // Add entry to Product Review history table.
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            ProductReviewHistory productReviewHistory = productReviewHistoryAdmin.GetByReviewHistoryID(this.ReviewHistoryId);

            if (productReviewHistory != null)
            {
                ProductAdmin productAdmin = new ProductAdmin();
                if (this.ItemId > 0)
                {
                    ZNode.Libraries.DataAccess.Entities.Product product = productAdmin.GetByProductId(this.ItemId);
                    int? vendorId = 0;
                    if (product != null)
                    {
                        lblTitle.Text += this.GetGlobalResourceObject("ZnodeAdminResource","TitleRejectHistoryDetails").ToString() + "\"" + product.Name + "\"";
                        lblProductName.Text = product.Name;
                        vendorId = product.AccountID;

                        if (vendorId != null)
                        {
                            AccountAdmin accountAdmin = new AccountAdmin();
                            Address address = accountAdmin.GetDefaultBillingAddress(Convert.ToInt32(vendorId));
                            if (address != null)
                            {
                                lblVendor.Text = address.FirstName + " " + address.LastName;
                            }
                        }
                    }
                }
                
                lblDateModified.Text = productReviewHistory.LogDate.ToString();
                lblUserName.Text = productReviewHistory.Username;
                lblReason.Text = productReviewHistory.Reason.Trim().Length == 0 ? this.GetGlobalResourceObject("ZnodeAdminResource","ColumnNotApplicable").ToString() : productReviewHistory.Reason.Trim();
                lblStatus.Text = productReviewHistory.Status;
                if (productReviewHistory.Description.Trim().Length > 0)
                {
                    lblDescription.Text = HttpUtility.HtmlDecode(productReviewHistory.Description);
                }
                else
                {
                    lblDescription.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnNotApplicable").ToString();
                }
            }
        }

        #endregion
    }
}