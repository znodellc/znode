<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Vendors.VendorProducts.AddAddons" CodeBehind="AddAddons.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div style="text-align: right; margin-top: 0px;">
            <zn:Button runat="server" ButtonType="EditButton" OnClick="BtnAddSelectedAddons_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddSelectedItem%>' ID="btntopAddSelectedAddons" Width="150px" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelTop" />
        </div>
        <h1>
            <asp:Label ID="lblTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleAddProductAddOns%>'></asp:Label></h1>

        <div class="ClearBoth">
        </div>
        <div>
            <asp:Label CssClass="Error" ID="lblAddOnErrorMessage" runat="server" EnableViewState="false"
                Visible="false"></asp:Label>
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="SubTitleAddOnSearch" Text='<%$ Resources:ZnodeAdminResource, SubTitleAddOnSearch%>' runat="server"></asp:Localize></h4>
        <asp:Panel ID="pnlAddOnSearch" DefaultButton="btnAddOnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleName%>' runat="server"></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAddonName" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleTitle" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTitle%>' runat="server"></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAddOnTitle" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnSKUorProduct" Text='<%$ Resources:ZnodeAdminResource, ColumnSKUorProduct%>' runat="server"></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAddOnsku" runat="server"></asp:TextBox></span>
                    </div>

                </div>
                <div class="ClearBoth">
                </div>
                <div>
                    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnAddOnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear%>' CausesValidation="False" ID="btnAddOnClear" />
                    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnAddOnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch%>' ID="btnAddOnSearch" />
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle">
            <asp:Localize ID="GridTitleAddOnList" Text='<%$ Resources:ZnodeAdminResource, GridTitleProductAddOnList%>' runat="server"></asp:Localize></h4>
        <br />

        <!-- Update Panel for grid paging that are used to avoid the postbacks -->
        <asp:UpdatePanel ID="updPnlAddOnGrid" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="uxAddOnGrid" runat="server" CssClass="Grid" AllowPaging="True"
                    AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None"
                    OnPageIndexChanging="UxAddOnGrid_PageIndexChanging" CaptionAlign="Left" Width="100%"
                    EnableSortingAndPagingCallbacks="False" PageSize="15" AllowSorting="True" PagerSettings-Visible="true"
                    OnRowCommand="UxAddOnGrid_RowCommand" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundProductAddOnExist%>'>
                    <Columns>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSelect%>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkProductAddon" runat="server" CausesValidation="True" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="AddOnId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID%>' HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="Title" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleTitle%>' HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName%>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# Eval("Name") %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder%>' HeaderStyle-HorizontalAlign="Left" />
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div>
            <uc1:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div>
            <zn:Button runat="server" ButtonType="EditButton" OnClick="BtnAddSelectedAddons_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddSelectedItem%>' ID="btnAddSelectedAddons"  Width="150px" />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnBottomCancel" />

        </div>
    </div>
</asp:Content>
