﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Utilities;

namespace  Znode.Engine.Admin.Secure.Vendors.VendorProducts
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_Reviewer_ReviewImage class
    /// </summary>
    public partial class ReviewImage : System.Web.UI.Page
    {
        #region Protected Member Variables
        private readonly string SelectedImagesSessionKey = "SelectedImagesSessionKey";        
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindAlternateImages();
            }
        }
        #endregion

        #region Display Status Icons
        /// <summary>
        /// Get Status Image Icons Method
        /// </summary>
        /// <param name="state">The value of state</param>
        /// <returns>Returns the Product Status Image Icon</returns>
        protected string GetStatusImageIcon(object state)
        {
            string imageSrc = "~/Themes/Images/223-point_pendingapprove.gif";

            if (Convert.ToString(state) != string.Empty)
            {
                int stateId = Convert.ToInt32(state);

                if (stateId == Convert.ToInt32(ZNodeProductReviewState.Approved))
                {
                    imageSrc = "~/Themes/Images/222-point_approve.gif";
                }
                
                if (stateId == Convert.ToInt32(ZNodeProductReviewState.Declined))
                {
                    imageSrc = "~/Themes/Images/221-point_decline.gif";
                }
                
                if (stateId == Convert.ToInt32(ZNodeProductReviewState.ToEdit))
                {
                    imageSrc = "~/Themes/Images/220-point_toedit.gif";
                }
            }

            return imageSrc;
        }

        /// <summary>
        /// Get http image path.
        /// </summary>
        /// <param name="imageFileName">Image file name.</param>
        /// <returns>Returns the image file path with name.</returns>
        protected string GetImagePath(string imageFileName)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(imageFileName);
        }
        #endregion

        #region Event Methods

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }

        /// <summary>
        /// Grid Row DataBound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GvProductImages_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Dictionary<int, string> productImagesIdList = (Dictionary<int, string>)ViewState[this.SelectedImagesSessionKey];
                if (productImagesIdList != null)
                {
                    CheckBox check = (CheckBox)e.Row.Cells[0].FindControl("chkProduct") as CheckBox;
                    int id = int.Parse(e.Row.Cells[2].Text);
                    if (productImagesIdList.ContainsKey(id) && check != null)
                    {
                        check.Checked = true;
                    }
                }
            }
        }

        /// <summary>
        /// Reject Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnReject_Click(object sender, EventArgs e)
        {
            this.UpdateImageStatus(false);
        }

        /// <summary>
        /// Accept Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAccept_Click(object sender, EventArgs e)
        {
            this.UpdateImageStatus(true);
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("ReviewImage.aspx");
        }
        
        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindAlternateImages();
        }

        /// <summary>
        /// Toggle checkbox selection
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox chkSelectAll = sender as CheckBox;
            for (int rowIndex = 0; rowIndex <= gvProductImages.Rows.Count - 1; rowIndex++)
            {
                CheckBox chkProduct = gvProductImages.Rows[rowIndex].Cells[0].FindControl("chkProduct") as CheckBox;
                if (chkProduct != null)
                {
                    chkProduct.Checked = chkSelectAll.Checked;
                }
            }
        }

        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GvProductImages_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.RememberCheckedItems();

            gvProductImages.PageIndex = e.NewPageIndex;
            this.BindAlternateImages();
        }
        #endregion

        #region Update Product image revirew state.
        /// <summary>
        /// Update (Approved/Decline) the selected image status.
        /// </summary>
        /// <param name="status">Status to update.
        /// False: to update image as declined
        /// True: to update image as approved.
        /// </param>
        private void UpdateImageStatus(bool status)
        {
            this.RememberCheckedItems();

            List<string> selectedProductImagesIdList = new List<string>();
            Dictionary<int, string> productImagesIdList = (Dictionary<int, string>)ViewState[this.SelectedImagesSessionKey];
            if (productImagesIdList == null || productImagesIdList.Count == 0)
            {
                lblMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoImage").ToString();
                return;
            }

            ZNode.Libraries.Admin.ProductViewAdmin productAdmin = new ProductViewAdmin();
            foreach (KeyValuePair<int, string> productImageId in productImagesIdList)
            {
                ProductImage productImage = productAdmin.GetByProductImageID(productImageId.Key);
                int reviewStateId = 10;
                reviewStateId = status == true ? Convert.ToInt32(ZNodeProductReviewState.Approved) : Convert.ToInt32(ZNodeProductReviewState.Declined);

                // Update review state only if existing state is not same as current state.
                if (productImage != null && (productImage.ReviewStateID != reviewStateId))
                {
                    // Set the review state ID from enumeration
                    productImage.ReviewStateID = reviewStateId;

                    // Set the product image as rejected.
                    productAdmin.Update(productImage);
                }
            }

            // Clear the selcted Items
            ViewState[this.SelectedImagesSessionKey] = null;

            // Reload the image list.
            this.BindAlternateImages();
        }
        #endregion

        #region Bind Images
        /// <summary>
        /// Bind Alternate images
        /// </summary>
        private void BindAlternateImages()
        {
            ProductViewAdmin imageadmin = new ProductViewAdmin();
            AccountService accountService = new AccountService();

            int productId = 0;
            string productNum = this.EncodeInputString(txtVendorproductId.Text);
            string productName = this.EncodeInputString(txtProductName.Text);
            string vendor = this.EncodeInputString(txtVendor.Text);
            string vendorId = this.EncodeInputString(txtVendorId.Text);
            string productStateId = this.EncodeInputString(uxProductStatus.Value);
            string COId = this.EncodeInputString(txtSKU.Text);

            DataView dv = new DataView();
            DataSet ds = new DataSet();
            bool isVendorIdValid = true;

            // Get vendor AccountID using Vendor Account Number.
            if (!string.IsNullOrEmpty(txtVendorId.Text))
            {
                TList<Account> accountList = accountService.Find(string.Format("ExternalAccountNo='{0}'", vendorId));
                if (accountList != null && accountList.Count > 0)
                {
                    vendorId = accountList[0].AccountID.ToString();
                }
                else
                {
                    isVendorIdValid = false;
                }
                
                gvProductImages.DataSource = dv;
            }

            if (isVendorIdValid)
            {
                dv = imageadmin.GetAllAlternateImage(productId, productNum, productName, vendor, vendorId, productStateId, COId).Tables[0].DefaultView;
            }

           

            gvProductImages.DataSource = dv;
            gvProductImages.DataBind();

            btnAccept.Enabled = btnReject.Enabled = gvProductImages.Rows.Count == 0 ? false : true;
        }

        /// <summary>
        /// Html encode the given string
        /// </summary>
        /// <param name="input">String to HTML encode</param>
        /// <returns>Returns the HTML encoded string.</returns>
        private string EncodeInputString(string input)
        {
            return Server.HtmlEncode(input.Trim());
        }
        #endregion

        #region Save Checked Items
        /// <summary>
        /// Remember Checked Items Method
        /// </summary>
        private void RememberCheckedItems()
        {
            Dictionary<int, string> productImagesIdList = new Dictionary<int, string>();

            // Loop through the grid values
            foreach (GridViewRow row in gvProductImages.Rows)
            {
                CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProduct") as CheckBox;

                // Check in the ViewState
                if (ViewState[this.SelectedImagesSessionKey] != null)
                {
                    productImagesIdList = (Dictionary<int, string>)ViewState[this.SelectedImagesSessionKey];
                }

                int id = int.Parse(row.Cells[2].Text);

                if (check.Checked)
                {
                    if (!productImagesIdList.ContainsKey(id))
                    {
                        productImagesIdList.Add(id, row.Cells[2].Text);
                    }
                }
                else
                {
                    productImagesIdList.Remove(id);
                }
            }

            if (productImagesIdList.Count > 0)
            {
                ViewState[this.SelectedImagesSessionKey] = productImagesIdList;
            }
        }
        #endregion
    }
}