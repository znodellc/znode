﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Xml;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Secure.Vendors.VendorProducts
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_Product_RejectProduct class
    /// </summary>
    public partial class RejectProduct : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private string ManagePageLink = "~/Secure/Vendors/VendorProducts/View.aspx?ItemId=";
        private string ListPageLink = "~/Secure/Vendors/VendorProducts/Default.aspx";
        private string Associatename = string.Empty;
        private string selectedItemsKey = "SelectedItemsKey";
        #endregion

        #region Page Load

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            xmlRejectMessageDataSource.DataFile = ZNodeStorageManager.HttpPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "/RejectionMessageConfig.xml");

            if (!Page.IsPostBack)
            {
                this.BindRejectionReason();

                // Bind requested product detals 
                this.Bind();
            }
        }

        #endregion

        #region Event Methods
        /// <summary>
        /// Submit Button Click Event - Fires when Submit button is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            this.RejectVendorProduct();
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["ReturnUrl"] != null)
            {
                // Request comes from product List page
                Response.Redirect("Default.aspx");
            }
            else
            {
                // Request comes from product view page.
                Response.Redirect(this.ManagePageLink + this.ItemId);
            }
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Bind the rejection reason.
        /// </summary>
        private void BindRejectionReason()
        {
            XmlNodeList nodeList = xmlRejectMessageDataSource.GetXmlDocument().SelectNodes("//Messages/Message[@PortalID='" + ZNodeConfigManager.SiteConfig.PortalID + "'][@LocaleID='" + ZNodeConfigManager.SiteConfig.LocaleID + "']");
            List<string> reasonList = new List<string>();
            foreach (XmlNode node in nodeList)
            {
                reasonList.Add(node.Attributes["MsgValue"].Value);
            }

            ddlReason.DataSource = reasonList;
            ddlReason.DataBind();
        }

        /// <summary>
        /// Update the product status as rejected.
        /// </summary>
        private void RejectVendorProduct()
        {
            bool isSuccess = false;
            ProductAdmin productAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();
            string updateFailed = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateProductReview").ToString();
            StringBuilder historyUpdateFailedMsg = new StringBuilder();
            StringBuilder reviewStateUpdateFailedMsg = new StringBuilder();

            if (this.ItemId > 0)
            {
                product = productAdmin.GetByProductId(this.ItemId);

                if (product.AccountID == null)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorVendorID").ToString();
                    return;
                }

                // This is approval reject page, so set product review state as Rejected
                product.ReviewStateID = Convert.ToInt32(ZNodeProductReviewState.Declined);

                if (this.ItemId > 0)
                {
                    isSuccess = productAdmin.Update(product);
                    if (isSuccess)
                    {
                        if (product.AccountID == null)
                        {
                            //Method To Get Franchise AccountID
                            ProductReviewHistoryAdmin productReviewAdmin = new ProductReviewHistoryAdmin();
                            int accountID = productReviewAdmin.GetFranchiseAccountIDByPortalID(product.PortalID.GetValueOrDefault(0));
                            product.AccountID = accountID;
                        }

                        isSuccess = this.SaveProductHistory(this.ItemId, Convert.ToInt32(product.AccountID));

                        //Get productId, emailid and productName for single item
                        List<SupplierAdmin> listProductDetail = new List<SupplierAdmin>();
                        AccountService accountService = new AccountService();

                        string vendorEmail = accountService.GetByAccountID((int)product.AccountID).Email;
                        listProductDetail.Add(new SupplierAdmin { AccountId = (int)product.AccountID, ProductId = ItemId, ProductName = product.Name, VendorEmail = vendorEmail });

                        //Send Mail for single item
                        if (listProductDetail.Count > 0)
                            this.SendMailBySiteAdmin(listProductDetail);
                    }

                    if (isSuccess)
                    {
                        Response.Redirect(this.ManagePageLink + this.ItemId, false);
                    }
                    else
                    {
                        lblError.Text = updateFailed;
                    }
                }
            }
            else
            {
                ProductReviewStateAdmin reviewstateAdmin = new ProductReviewStateAdmin();

                // This is Product Reject page, So directly assign the state
                ProductReviewState reviewState = reviewstateAdmin.GetProductReviewStateByName(ZNodeProductReviewState.Declined.ToString());
                StringBuilder sb = new StringBuilder();
                Dictionary<int, string> productIdList = (Dictionary<int, string>)Session[this.selectedItemsKey];

                //Get all product details 
                List<SupplierAdmin> listProductDetails = new List<SupplierAdmin>();
                AccountService accountService = new AccountService();

                // Loop through the grid values
                if (productIdList != null && reviewState != null)
                {
                    foreach (KeyValuePair<int, string> pair in productIdList)
                    {
                        // Get ProductId
                        int productId = pair.Key;
                        string productName = pair.Value;

                        ZNode.Libraries.DataAccess.Entities.Product entity = productAdmin.GetByProductId(productId);
                        //Checking for franchise Admin AccountId
                        if (entity.AccountID == null)
                        {

                            //Method To Get Franchise AccountID
                            ProductReviewHistoryAdmin productReviewAdmin = new ProductReviewHistoryAdmin();
                            int accountID = productReviewAdmin.GetFranchiseAccountIDByPortalID(entity.PortalID.GetValueOrDefault(0));
                            entity.AccountID = accountID;
                        }

                        // Update only if existing status is not same as current status
                        if (entity.ReviewStateID != reviewState.ReviewStateID)
                        {
                            if (entity.AccountID > 0)
                            {
                                //Get productId, productName, vendorEmail
                                string vendorEmail = accountService.GetByAccountID((int)entity.AccountID).Email;
                                listProductDetails.Add(new SupplierAdmin { AccountId = (int)entity.AccountID, ProductId = productId, ProductName = productName, VendorEmail = vendorEmail });
                            }

                            entity.ReviewStateID = reviewState.ReviewStateID;
                            isSuccess = productAdmin.Update(entity);
                            if (!isSuccess)
                            {
                                reviewStateUpdateFailedMsg.Append("<BR>" + product.ShortDescription);
                            }
                        }

                        isSuccess = this.SaveProductHistory(productId, Convert.ToInt32(entity.AccountID));                       
                    }

                    //Send mail for multiple rejected products 
                    if (listProductDetails.Count > 0)
                    {
                        this.SendMailBySiteAdmin(listProductDetails);
                    }

                    if (!isSuccess)
                    {
                        historyUpdateFailedMsg.Append("<BR>" + product.Name);
                    }
                }

                if (reviewStateUpdateFailedMsg.Length > 0)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorReviewState") + reviewStateUpdateFailedMsg.ToString();
                    return;
                }

                if (historyUpdateFailedMsg.Length > 0)
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorReviewHistory") + historyUpdateFailedMsg.ToString();
                    return;
                }

                // Clear the selection.
                Session[this.selectedItemsKey] = null;

                Response.Redirect(this.ListPageLink);
            }
        }

        /// <summary>
        /// Save the entry to product history table.
        /// </summary>
        /// <param name="productId">The value of Product ID</param>
        /// <param name="vendorId">The value of Vendor ID</param>
        /// <returns>Returns true if success else False.</returns>
        private bool SaveProductHistory(int productId, int vendorId)
        {
            // Add entry to Product Review history table.
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            ProductReviewHistory productReviewHistory = new ProductReviewHistory();
            productReviewHistory.ProductID = productId;
            productReviewHistory.VendorID = vendorId;
            productReviewHistory.LogDate = DateTime.Now;
            productReviewHistory.Username = this.Page.User.Identity.Name;
            productReviewHistory.Status = ZNodeProductReviewState.Declined.ToString();
            if (ddlReason.SelectedItem != null)
            {
                productReviewHistory.Reason = ddlReason.SelectedItem.Text;
            }

            productReviewHistory.Description = HttpUtility.HtmlEncode(txtDescription.Text);
            return productReviewHistoryAdmin.Add(productReviewHistory);
        }

        /// <summary>
        /// Bind Method
        /// </summary>
        private void Bind()
        {
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ProductAdmin();
            AccountAdmin accountAdmin = new AccountAdmin();
            PortalAdmin portalAdmin = new PortalAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = null;
            string unknownVendor = this.GetGlobalResourceObject("ZnodeAdminResource", "TextUnknown").ToString();
            Dictionary<int, string> productIdList = new Dictionary<int, string>();
            if (this.ItemId > 0)
            {
                productIdList.Add(this.ItemId, this.ItemId.ToString());
            }

            if (Session[this.selectedItemsKey] != null)
            {
                // Reject request comes from List page
                productIdList = (Dictionary<int, string>)Session[this.selectedItemsKey];
            }

            if (productIdList != null)
            {
                if (productIdList.Count == 1)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleRejectProduct").ToString();
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleRejectMultipleProductSelect").ToString();
                }

                StringBuilder commaSeparatedIdList = new StringBuilder();
                List<int> distinctSupplierIdList = new List<int>();
                StringBuilder vendorList = new StringBuilder();
                foreach (KeyValuePair<int, string> pair in productIdList)
                {
                    commaSeparatedIdList.Append(pair.Key + ", ");
                }

                // Find distinct account Ids of the product
                foreach (KeyValuePair<int, string> pair in productIdList)
                {
                    product = ProdAdmin.GetByProductId(pair.Key);
                    if (product != null && product.AccountID != null)
                    {
                        // If only one product then display the name
                        if (productIdList.Count == 1)
                        {
                            lblTitle.Text += string.Format(" - \"{0}\"", product.Name);
                        }

                        int vendorId = Convert.ToInt32(product.AccountID);
                        if (!distinctSupplierIdList.Contains(vendorId))
                        {
                            distinctSupplierIdList.Add(vendorId);
                        }
                    }
                }

                foreach (int vendorId in distinctSupplierIdList)
                {
                    Address address = accountAdmin.GetDefaultBillingAddress(vendorId);
                    if (address != null)
                    {
                        vendorList.Append(address.FirstName + " " + address.LastName + ", ");
                    }
                }

                lblProductID.Text = commaSeparatedIdList.ToString().Substring(0, commaSeparatedIdList.ToString().LastIndexOf(","));
                if (vendorList.ToString().IndexOf(",") > 1)
                {
                    lblVendor.Text = vendorList.ToString().Substring(0, vendorList.ToString().LastIndexOf(","));
                }
                else if (!string.IsNullOrEmpty(product.PortalID.ToString()))
                {
                    Portal portal = portalAdmin.GetByPortalId(Convert.ToInt32(product.PortalID));
                    lblVendor.Text = string.Format(lblVendor.Text, portal.CompanyName);
                }
                else
                {
                    lblVendor.Text = unknownVendor;
                }
            }
        }
        #endregion

        #region Znode version 7.2.2 Send Mail

        /// <summary>
        /// Znode version 7.2.2 
        /// This function will send  mail notification to vendor.
        /// To inform product is declined by Site Admin.
        /// </summary>
        /// <param name="productId">string productId to send productId in email</param>
        /// <param name="productName">string productName to send productName in email</param>
        protected void SendMailBySiteAdmin(List<SupplierAdmin> productList)
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.AdminEmail;

            int productId = 0;
            string productName = string.Empty;
            string vendorEmail = string.Empty;

            //send multiple email
            foreach (SupplierAdmin pair in productList)
            {
                productId = pair.ProductId;
                productName = pair.ProductName;
                string productId2 = productId.ToString();
                vendorEmail = pair.VendorEmail;

                //Subject is set from global resources of admin.
                string subject = productName + " " + HttpContext.GetGlobalResourceObject("CommonCaption", "SubjectProductDecline");

                //Get vendor Email ID
                string receiverEmail = pair.VendorEmail;

                string siteAdmin = ZNodeConfigManager.SiteConfig.StoreName;
                string currentCulture = string.Empty;
                string templatePath = string.Empty;
                string defaultTemplatePath = string.Empty;

                // Template selection
                currentCulture = ZNodeCatalogManager.CultureInfo;
                defaultTemplatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "RejectProductBySiteAdmin.html"));

                templatePath = (!string.IsNullOrEmpty(currentCulture))
                    ? Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "RejectProductBySiteAdmin_" + currentCulture + ".html")
                    : Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "RejectProductBySiteAdmin.html"));

                // Check the default template file existence.
                if (!File.Exists(defaultTemplatePath))
                {
                    return;
                }
                else
                {
                    // If language specific template not available then load default template.
                    templatePath = defaultTemplatePath;
                }

                StreamReader rw = new StreamReader(templatePath);
                string messageText = rw.ReadToEnd();

                Regex rx1 = new Regex("#PRODUCTID#", RegexOptions.IgnoreCase);
                messageText = rx1.Replace(messageText, productId2);

                Regex rx2 = new Regex("#PRODUCTNAME#", RegexOptions.IgnoreCase);
                messageText = rx2.Replace(messageText, productName);

                Regex rx3 = new Regex("#SITEADMIN#", RegexOptions.IgnoreCase);
                messageText = rx3.Replace(messageText, siteAdmin);

                Regex rx4 = new Regex("#SITEADMINEMAIL#", RegexOptions.IgnoreCase);
                messageText = rx4.Replace(messageText, senderEmail);

                Regex RegularExpressionLogo = new Regex("#LOGO#", RegexOptions.IgnoreCase);
                messageText = RegularExpressionLogo.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);

                try
                {
                    // Send mail
                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(receiverEmail, senderEmail, string.Empty, subject, messageText, true);
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralMessage, receiverEmail, Request.UserHostAddress.ToString(), null, ex.Message, null);
                }
            }
        }
        #endregion
    }
}