﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Vendors.VendorProducts
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_Reviewer_EditMarketing  class
    /// </summary>
    public partial class EditMarketing : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private string ManagePageLink = "~/Secure/Vendors/VendorProducts/View.aspx?mode=marketing&itemid=";
        private string CancelPageLink = "~/Secure/Vendors/VendorProducts/View.aspx?mode=marketing&itemid=";
        private string AssociateName = string.Empty;
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                if (this.ItemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextEditMarketingInfo").ToString();

                    // Bind Sku Details
                    this.Bind();
                }
            }
        }
        #endregion

        #region Event Methods
        /// <summary>
        /// Submit Button Click Event - Fires when Submit button is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            this.SaveMarketing();
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelPageLink + this.ItemId);
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Save or Update the marketing data.
        /// </summary>
        private void SaveMarketing()
        {
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            ProductAdmin productAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();
            string mappedSEOUrl = string.Empty;

            // If edit mode then get all the values first
            if (this.ItemId > 0)
            {
                product = productAdmin.GetByProductId(this.ItemId);

                if (product.SEOURL != null)
                {
                    mappedSEOUrl = product.SEOURL;
                }
            }

            // Set properties
            product.DisplayOrder = txtDisplayOrder.Text.Trim().Length > 0 ? Convert.ToInt32(txtDisplayOrder.Text) : 0;
            product.HomepageSpecial = chkShowOnHomePage.Checked;
            product.FeaturedInd = chkFeaturedProduct.Checked;
            product.NewProductInd = chkIsNewItem.Checked;
            product.SEOTitle = Server.HtmlEncode(txtSEOTitle.Text.Trim());
            product.SEOKeywords = Server.HtmlEncode(txtSEOMetaKeywords.Text.Trim());
            product.SEODescription = Server.HtmlEncode(txtSEOMetaDescription.Text.Trim());
            product.SEOURL = null;

            if (txtSEOUrl.Text.Trim().Length > 0)
            {
                product.SEOURL = txtSEOUrl.Text.Trim().Replace(" ", "-");
            }

            if (string.Compare(product.SEOURL, mappedSEOUrl, true) != 0)
            {
                if (urlRedirectAdmin.SeoUrlExists(product.SEOURL, product.ProductID))
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSEOFriendlyURLAlreadyExist").ToString();
                    return;
                }
            }

            bool status = false;

            int? reviewStateId = productAdmin.UpdateReviewStateID(product, ZNodeConfigManager.SiteConfig.PortalID, this.ItemId);

            // PRODUCT UPDATE
            if (this.ItemId > 0)
            {
                product.ReviewStateID = reviewStateId;
                status = productAdmin.Update(product);
            }

            if (status)
            {
                if (chkAddURLRedirect.Checked)
                {
                    urlRedirectAdmin.AddUrlRedirectTable(SEOUrlType.Product, mappedSEOUrl, product.SEOURL, product.ProductID.ToString());
                }

                urlRedirectAdmin.UpdateUrlRedirect(mappedSEOUrl);

                product = productAdmin.GetByProductId(this.ItemId);

                this.AssociateName = this.GetGlobalResourceObject("ZnodeAdminResource", "TextEditMarketingInfo").ToString() + product.Name;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, product.Name);

                Response.Redirect(this.ManagePageLink + this.ItemId);
            }
            else
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductSEOSetting").ToString();
            }
        }

        /// <summary>
        /// Bind Method
        /// </summary>
        private void Bind()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product _Product = ProdAdmin.GetByProductId(this.ItemId);

            if (_Product != null)
            {
                // Set Display Options value
                txtDisplayOrder.Text = _Product.DisplayOrder == null ? string.Empty : _Product.DisplayOrder.ToString();
                chkShowOnHomePage.Checked = _Product.HomepageSpecial;
                chkFeaturedProduct.Checked = _Product.FeaturedInd;
                chkIsNewItem.Checked = _Product.NewProductInd == null ? false : Convert.ToBoolean(_Product.NewProductInd.Value);

                // Set SEO Options value
                txtSEOTitle.Text = Server.HtmlDecode(_Product.SEOTitle);
                txtSEOMetaKeywords.Text = Server.HtmlDecode(_Product.SEOKeywords);
                txtSEOMetaDescription.Text = Server.HtmlDecode(_Product.SEODescription);
                txtSEOUrl.Text = _Product.SEOURL;
                lblTitle.Text += "\"" + _Product.Name + "\"";
            }
        }
        #endregion
    }
}