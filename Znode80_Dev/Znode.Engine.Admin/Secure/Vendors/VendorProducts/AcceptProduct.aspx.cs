﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Secure.Vendors.VendorProducts
{
    /// <summary>
    /// Represents the Site Admin - Admin_Secure_Product_AcceptProduct class
    /// </summary>
    public partial class AcceptProduct : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        private string ListPageLink = "~/Secure/Vendors/VendorProducts/Default.aspx";
        private string ViewPageLink = "~/Secure/Vendors/VendorProducts/View.aspx?ItemId=";
        #endregion
        #region Private Property
        /// <summary>
        /// Gets or sets the review next product Id.
        /// </summary>
        private string NextItemID
        {
            get
            {
                return ViewState["ViewState"].ToString();
            }

            set
            {
                ViewState["ViewState"] = value;
            }
        }

        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListPageLink);
        }

        /// <summary>
        /// Next product Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnNextProduct_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ViewPageLink + this.NextItemID);
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                if (this.ItemId > 0)
                {
                    // Bind Product Details
                    this.Bind();

                    this.SetNextReviewableProduct();
                }
            }
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Set next product to review.
        /// </summary>
        private void SetNextReviewableProduct()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = ProdAdmin.GetNextProductToReview(ZNodeProductReviewState.ToEdit);

            if (product == null)
            {
                // There is no reviewable product available. So Hide the "Review Next Product" button
                divNextProduct.Visible = false;
            }
            else
            {
                // Store the review next product Id in viewstate.
                this.NextItemID = product.ProductID.ToString();
            }
        }

        /// <summary>
        /// Bind Method
        /// </summary>
        private void Bind()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = ProdAdmin.GetByProductId(this.ItemId);

            if (product != null)
            {
                lblTitle.Text += string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAcceptProduct").ToString(), product.Name);

                lblApprovedMsg.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextProductAccepted").ToString(), product.Name);

                // Set Product and Vendor Details
                lblProductID.Text = product.ProductID.ToString();

                // Get Vendor Name
                AccountAdmin accountAdmin = new AccountAdmin();
                PortalAdmin portalAdmin = new PortalAdmin();

                // For Franchise Admin
                if (!string.IsNullOrEmpty(product.PortalID.ToString()))
                {
                    Portal portal = portalAdmin.GetByPortalId(Convert.ToInt32(product.PortalID));
                    lblVendor.Text = string.Format(lblVendor.Text, portal.CompanyName);
                }

                // For Mall Admin
                else if (!string.IsNullOrEmpty(product.AccountID.ToString()))
                {
                    Address address = accountAdmin.GetDefaultBillingAddress(Convert.ToInt32(product.AccountID));
                    lblVendor.Text = string.Format(lblVendor.Text, address.FirstName + " " + address.LastName + " " + address.CompanyName);
                }


                else
                {
                    lblVendor.Visible = false;
                }
            }

            // Send Mail function
            this.SendMailBySiteAdmin(ItemId.ToString(), product.Name.ToString());

            // Create Instance for Product Admin and Product entity
            bool isSuccess = false;

            if (product != null)
            {
                product.ReviewStateID = Convert.ToInt32(ZNodeProductReviewState.Approved);

                // Update product state in ZNodeProduct
                isSuccess = ProdAdmin.Update(product);
                ZNodeUserAccount _userAccount = ZNodeUserAccount.CurrentAccount();

                // Add entry to Product Review history table if product update is success.
                if (isSuccess)
                {
                    ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
                    ProductReviewHistory productReviewHistory = new ProductReviewHistory();
                    productReviewHistory.ProductID = this.ItemId;
                    productReviewHistory.VendorID = _userAccount.AccountID;
                    productReviewHistory.LogDate = DateTime.Now;
                    productReviewHistory.Username = this.Page.User.Identity.Name;
                    productReviewHistory.Status = ZNodeProductReviewState.Approved.ToString();
                    productReviewHistory.Reason = string.Empty;
                    productReviewHistory.Description = string.Empty;

                    isSuccess = productReviewHistoryAdmin.Add(productReviewHistory);                    
                }

                else
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateProductReview").ToString();
                }
            }
        }
        #endregion

        #region Znode version 7.2.2 Send Mail

        /// <summary>
        /// Znode version 7.2.2 
        /// This function will send  mail notification to vendor.
        /// To inform product is approved by Site Admin.
        /// </summary>
        /// <param name="productId">string productId to send productId in email</param>
        /// <param name="productName">string productName to send productName in email</param>
        protected void SendMailBySiteAdmin(string productId, string productName)
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.AdminEmail;

            //Subject is set from global resources of admin.
            string subject = productName + " " + HttpContext.GetGlobalResourceObject("CommonCaption", "SubjectProductApprove");

            //Get Vendor Email
            string receiverEmail = GetVendorEmailId();

            string currentCulture = string.Empty;
            string templatePath = string.Empty;
            string defaultTemplatePath = string.Empty;
            string siteAdmin = ZNodeConfigManager.SiteConfig.StoreName;

            // Template selection
            currentCulture = ZNodeCatalogManager.CultureInfo;
            defaultTemplatePath = Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ApproveSingleProduct.html"));

            templatePath = (!string.IsNullOrEmpty(currentCulture))
                ? Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "ApproveSingleProduct_" + currentCulture + ".html")
                : Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "ApproveSingleProduct.html"));

            // Check the default template file existence.
            if (!File.Exists(defaultTemplatePath))
            {
                return;
            }
            else
            {
                // If language specific template not available then load default template.
                templatePath = defaultTemplatePath;
            }

            StreamReader rw = new StreamReader(templatePath);
            string messageText = rw.ReadToEnd();

            Regex rx1 = new Regex("#PRODUCTID#", RegexOptions.IgnoreCase);
            messageText = rx1.Replace(messageText, productId);

            Regex rx2 = new Regex("#PRODUCTNAME#", RegexOptions.IgnoreCase);
            messageText = rx2.Replace(messageText, productName);

            Regex rx3 = new Regex("#SITEADMIN#", RegexOptions.IgnoreCase);
            messageText = rx3.Replace(messageText, siteAdmin);

            Regex rx4 = new Regex("#SITEADMINEMAIL#", RegexOptions.IgnoreCase);
            messageText = rx4.Replace(messageText, senderEmail);

            Regex RegularExpressionLogo = new Regex("#LOGO#", RegexOptions.IgnoreCase);
            messageText = RegularExpressionLogo.Replace(messageText, ZNodeConfigManager.SiteConfig.StoreName);

            try
            {
                // Send mail
                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(receiverEmail, senderEmail, string.Empty, subject, messageText, true);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.GeneralMessage, receiverEmail, Request.UserHostAddress.ToString(), null, ex.Message, null);
            }
        }

        /// <summary>
        /// Znode version 7.2.2 
        /// This function will return vendor emailId for sending mail to vendor.        
        /// </summary>
        /// <returns>vendorEmail for mail notification</returns>                                                  
        private string GetVendorEmailId()
        {
            string vendorEmailId = string.Empty;

            ProductAdmin prodAdmin = new ProductAdmin();
            AccountService accountService = new AccountService();

            ZNode.Libraries.DataAccess.Entities.Product product = prodAdmin.GetByProductId(ItemId);

            if (!product.Equals(null) && !product.AccountID.Equals(null) && product.AccountID > 0)
            {
                vendorEmailId = accountService.GetByAccountID(int.Parse(product.AccountID.ToString())).Email;
            }

            return vendorEmailId;
        }
        #endregion
    }
}