﻿<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Vendors.VendorProducts.RejectProduct"
    Title="Manage Vendor Products - Reject Product" CodeBehind="RejectProduct.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <asp:XmlDataSource ID="xmlRejectMessageDataSource" runat="server"></asp:XmlDataSource>
        <div class="RejectProductLeftFloat">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="clear: both">
        </div>
        <div class="FormView">
            <div>
                <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
            </div>

            <div class="FieldStyle">
                <asp:Localize runat="server" ID="TitleProductID" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductID %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblProductID" runat="server"></asp:Label>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="TitleVendor" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleVendor %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:Label ID="lblVendor" runat="server" Text="{0}"></asp:Label>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="TitleRejectionReason" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleRejectionReason %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlReason" runat="server">
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="TitleDetailedReason" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDetailedReason %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox Height="250px" Width="450px" TextMode="MultiLine" ID="txtDescription"
                    runat="server"></asp:TextBox>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="FieldStyle">
            </div>
            <div class="ValueStyle">
                <zn:Button runat="server" ID="btnSubmit" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
                <zn:Button runat="server" ID="btnCancel" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
            </div>
        </div>
    </div>
</asp:Content>
