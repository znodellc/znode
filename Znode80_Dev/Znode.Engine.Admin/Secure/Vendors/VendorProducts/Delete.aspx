<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Vendors.VendorProducts.Delete" CodeBehind="Delete.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h5>
        <asp:Localize ID="TitlePlease" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm %>'></asp:Localize></h5>
    <p>
        <asp:Label ID="TitlePleaseConfirm" runat="server"></asp:Label>
    </p>
    <asp:Label CssClass="Error" ID="lblErrorMessage" runat="server" Text=''></asp:Label>
    <br />
    <br />
    <div>
        <zn:Button runat="server" ID="btnDelete" OnClick="BtnDelete_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>' CausesValidation="true" />
        <zn:Button runat="server" ID="btnCancel" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
    </div>
</asp:Content>
