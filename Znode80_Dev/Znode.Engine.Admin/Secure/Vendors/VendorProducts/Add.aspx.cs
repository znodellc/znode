using System;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Vendors.VendorProducts
{
    /// <summary>
    /// Represents the SiteAdmiun - Admin_Secure_Reviewer_add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region protected Member Variables
        private string CancelLink = "View.aspx?itemid=";
        private string ListLink = "Default.aspx";
        private string ProductImageName = string.Empty;
        private int ItemId;
        private int stepid = 0;
        private int SKUId = 0;
        private int ProductId = 0;
        private StringBuilder SelectedNodes = null;
        private StringBuilder DeleteNodes = null;
        private int ProductTypeId = 0;
        private ZNodeImage znodeImage = new ZNodeImage();
        private string Associatename = string.Empty;
        #endregion

        #region Bind Category Tree view
        /// <summary>
        /// Bind TreeView with Categories
        /// </summary>
        public void BindTreeViewCategory()
        {
            this.PopulateAdminTreeView(string.Empty);
        }

        /// <summary>
        /// Populates a treeview with store categories
        /// </summary>
        /// <param name="selectedCategoryId">The value of selectedCategoryId</param>
        public void PopulateAdminTreeView(string selectedCategoryId)
        {
			ProductAdmin productAdmin = new ProductAdmin();
			ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();
			product = productAdmin.GetByProductId(this.ItemId);
            int portalId = product.PortalID == null ? ZNodeConfigManager.SiteConfig.PortalID : Convert.ToInt32(product.PortalID);


			CategoryHelper categoryHelper = new CategoryHelper();
			System.Data.DataSet ds = categoryHelper.GetNavigationItems(portalId, ZNodeConfigManager.SiteConfig.LocaleID);

            // Add the hierarchical relationship to the dataset
            ds.Relations.Add("NodeRelation", ds.Tables[0].Columns["CategoryNodeId"], ds.Tables[0].Columns["ParentCategoryNodeId"]);

            foreach (DataRow dataRow in ds.Tables[0].Rows)
            {
                if (dataRow.IsNull("ParentCategoryNodeID"))
                {
                    if ((bool)dataRow["VisibleInd"])
                    {
                        // Create new tree node
                        TreeNode tn = new TreeNode();
                        tn.Text = dataRow["Name"].ToString();
                        tn.Value = dataRow["categoryid"].ToString();

                        string categoryId = dataRow["CategoryId"].ToString();

                        // Add Root Node to Category Tree view
                        CategoryTreeView.Nodes.Add(tn);

                        if (selectedCategoryId.Equals(categoryId))
                        {
                            tn.Selected = true;
                            this.RecursivelyExpandTreeView(tn);
                        }

                        this.RecursivelyPopulateTreeView(dataRow, tn, selectedCategoryId);
                    }
                }
            }
        }
        #endregion

        #region Bind Edit Data
        /// <summary>
        /// Bind value for Particular Product
        /// </summary>
        public void BindEditData()
        {
            ProductAdmin productAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();
            ProductCategory productCategory = new ProductCategory();
            SKUAdmin skuAdmin = new SKUAdmin();

            if (this.ItemId > 0)
            {
                // Checking the user profile for roles and permission and then loading the data based on roles
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);

                product = productAdmin.GetByProductId(this.ItemId);
                ProductService productService = new ProductService();
                productService.DeepLoad(product);
                productCategory = productAdmin.GetProductCategoryByProductID(this.ItemId);

                // General Information - Section1
                txtProductName.Text = Server.HtmlDecode(product.Name);
                txtProductNum.Text = Server.HtmlDecode(product.ProductNum);

                // Product Description and Image - Section2
                if (product.Description != null)
                {
                    htmlPortalDesc.Html = Server.HtmlDecode(product.Description);
                }

                if (product.ShortDescription != null)
                {
                    txtShortDesc.Text = Server.HtmlDecode(product.ShortDescription);
                }

                if (product.AdditionalInformation != null)
                {
                    htmlShippingDesc.Html = Server.HtmlDecode(product.AdditionalInformation);
                }

                if (product.FeaturesDesc != null)
                {
                    htmlProductDetail.Html = Server.HtmlDecode(product.FeaturesDesc);
                }

                Image1.ImageUrl = this.znodeImage.GetImageHttpPathMedium(product.ImageFile);

                // Displaying the Image file name in a textbox           
                txtimagename.Text = product.ImageFile;

                txtAffiliateLink.Text = product.AffiliateUrl;
                chkActiveInd.Checked = product.ActiveInd;
                chkRecurringInd.Checked = product.RecurringBillingInd;

                if (product.RecurringBillingInd)
                {
                    if (product.RecurringBillingInitialAmount.HasValue)
                    {
                        txtStartupfee.Text = product.RecurringBillingInitialAmount.Value.ToString("N");
                    }

                    chkActiveInd.Checked = product.RecurringBillingInd;

                    if (product.RecurringBillingFrequency.Length > 0)
                    {
                        string[] freq = product.RecurringBillingFrequency.Split('-');
                        txtBillevery.Text = freq[0];
                        ddlBillEvery.Text = freq[1];
                    }

                    if (product.RecurringBillingPeriod.Length > 0)
                    {
                        string[] period = product.RecurringBillingPeriod.Split('-');
                        txtContinueBill.Text = period[0];
                    }

                    txtStartupfee.Enabled = true;
                    txtBillevery.Enabled = true;
                    ddlBillEvery.Enabled = true;
                    txtContinueBill.Enabled = true;
                }

                if (product.IsShippable.HasValue)
                {
                    chkShipInd.Checked = product.IsShippable.Value;
                    txtProductWeight.Enabled = true;
                    txtHandling.Enabled = true;
                }

                if (product.ShippingRate.HasValue)
                {
                    txtHandling.Text = product.ShippingRate.Value.ToString("N");
                }

                // Product properties
                if (product.RetailPrice.HasValue)
                {
                    txtproductRetailPrice.Text = product.RetailPrice.Value.ToString("N");
                }

                if (product.SalePrice.HasValue)
                {
                    txtproductSalePrice.Text = product.SalePrice.Value.ToString("N");
                }

                if (product.Weight.HasValue)
                {
                    txtproductshipweight.Text = product.Weight.Value.ToString("N2");
                }

                if (product.Height.HasValue)
                {
                    txtProductHeight.Text = product.Height.Value.ToString("N2");
                }

                if (product.Width.HasValue)
                {
                    txtProductWidth.Text = product.Width.Value.ToString("N2");
                }

                if (product.Length.HasValue)
                {
                    txtProductLength.Text = product.Length.Value.ToString("N2");
                }

                txtOutofStock.Text = Server.HtmlDecode(product.OutOfStockMsg);
                txtInStockMsg.Text = Server.HtmlDecode(product.InStockMsg);
                chkActiveInd.Checked = product.ActiveInd;

                // Inventory Setting - Out of Stock Options
                if (product.AllowBackOrder.HasValue && product.TrackInventoryInd.HasValue)
                {
                    if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value == false)
                    {
                        InvSettingRadiobtnList.Items[0].Selected = true;
                    }
                    else if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value)
                    {
                        InvSettingRadiobtnList.Items[1].Selected = true;
                    }
                    else if (product.TrackInventoryInd.Value == false && product.AllowBackOrder.Value == false)
                    {
                        InvSettingRadiobtnList.Items[2].Selected = true;
                    }
                }

                if (product.Weight.HasValue)
                {
                    txtProductWeight.Text = product.Weight.Value.ToString("N2");
                }

                this.BindEditCategoryList();

                // Inventory
                TList<SKU> skuList = skuAdmin.GetByProductID(this.ItemId);

                if (skuList != null && skuList.Count > 0)
                {
                    ViewState["productSkuId"] = skuList[0].SKUID.ToString();

                    // Set the existing value in hidden
                    existSKUvalue.Value = skuList[0].SKU;

                    SKUInventory skuInventory = skuAdmin.GetSkuInventoryBySKU(skuList[0].SKU);

                    if (skuInventory != null)
                    {
                        txtProductQuantity.Text = skuInventory.QuantityOnHand.GetValueOrDefault(0).ToString();
                        txt2COID.Text = skuInventory.SKU;
                    }

                    // This method will enable or disable validators based on the shippingtype
                    this.EnableValidators();

                    // Release the Resources
                    skuList.Dispose();
                }

                ProductReviewHistoryAdmin reviewAdmin = new ProductReviewHistoryAdmin();
                string changedFields = reviewAdmin.GetChangedFields(this.ItemId);

                // Remove unwanted Commas,
                while (!string.IsNullOrEmpty(changedFields) && changedFields.Substring(0, 1) == ",")
                {
                    changedFields = changedFields.Substring(1);
                }

                // Replace , with ~~ for JS manipulation.
                if (!string.IsNullOrEmpty(changedFields))
                {
                    hdnDirtyFields.Value = string.Format("~{0}~", changedFields);
                    hdnDirtyFieldsOrg.Value = changedFields;
                }

                // Disable the rate, inventory level and recurring billing settings from reviewer.
                UserStoreAccess uss = new UserStoreAccess();
				if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer)
                {
                    txtproductRetailPrice.Enabled = false;
                    txtproductSalePrice.Enabled = false;
                    txtProductQuantity.Enabled = false;
                    chkRecurringInd.Enabled = false;
                    txtStartupfee.Enabled = false;
                    txtBillevery.Enabled = false;
                    ddlBillEvery.Enabled = false;
                    txtContinueBill.Enabled = false;
                }
            }
        }

        /// <summary>
        /// Check the Category to the particular Product
        /// </summary>
        public void BindEditCategoryList()
        {
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();

            if (productCategoryAdmin != null)
            {
                DataSet MyDataset = productCategoryAdmin.GetByProductID(this.ItemId);
                foreach (DataRow dr in MyDataset.Tables[0].Rows)
                {
                    this.PopulateEditTreeView(dr["categoryid"].ToString());
                }
            }
        }

        /// <summary>
        /// Returns a Format Weight string
        /// </summary>
        /// <param name="fieldValue">The value of fieldvalue</param>
        /// <returns>Returns the formatted weight</returns>
        public string FormatNull(object fieldValue)
        {
            if (fieldValue == null)
            {
                return String.Empty;
            }
            else
            {
                if (Convert.ToDecimal(fieldValue.ToString()) == 0)
                {
                    return string.Empty;
                }
                else
                {
                    return fieldValue.ToString();
                }
            }
        }

        /// <summary>
        /// Format Retail and Wholesale price
        /// </summary>
        /// <param name="fieldValue">The value of fieldvalue</param>
        /// <returns>Returns the Formatted Price</returns>
        public string FormatPrice(object fieldValue)
        {
            if (fieldValue == null)
            {
                return String.Empty;
            }
            else
            {
                if (fieldValue.ToString().Equals("0"))
                {
                    return String.Empty;
                }
                else
                {
                    return String.Format("{0:c}", fieldValue).Substring(1).ToString();
                }
            }
        }
        #endregion

        #region Page load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.SelectedNodes = new StringBuilder();
            this.DeleteNodes = new StringBuilder();

            // Get product id value from query string
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
                this.ProductId = this.ItemId;
            }
            else if (Session["itemid"] != null)
            {
                this.ItemId = int.Parse(Session["itemid"].ToString());
                this.ProductId = this.ItemId;
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                if (Request.Params["returnurl"] != null)
                {
                    this.ItemId = 0;
                    Session["itemid"] = null;
                }

                if (Session["stepid"] != null)
                {
                    this.stepid = int.Parse(Session["stepid"].ToString());
                    Session["stepid"] = null;
                }

                // Dynamic error message for weight field compare validator
                WeightFieldCompareValidator.ErrorMessage = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorEnterValidProduct").ToString();

                // Toggle Is Tangible section
                chkShipInd.Attributes.Add("onclick", "setTangibleSectionEnabled()");

                // Toggle Recurring Billing section
                chkRecurringInd.Attributes.Add("onclick", "setRecurringSectionEnabled()");

                this.BindTreeViewCategory();

                // if edit func then bind the data fields
                if (this.ItemId > 0)
                {
                    tblShowImage.Visible = true;
                    pnlShowImage.Visible = true;
                    tblProductDescription.Visible = false;
                    this.BindEditData();
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditProduct").ToString() +  Server.HtmlEncode(txtProductName.Text.Trim());    
                }
                else
                {
                    tblShowImage.Visible = true;
                    pnlShowImage.Visible = false;
                    pnlImage.Visible = false;
                    tblProductDescription.Visible = true;
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEnterProduct").ToString();
                    InvSettingRadiobtnList.Items[2].Selected = true;
                }

                btnNext1.Visible = false;
                btnSubmit.Visible = true;
                btnClear.Visible = true;

                dvBottomButtons.Visible = true;
            }
        }

        #endregion

        #region General Events

        /// <summary>
        /// CategoryTreeView TreeNodeCheckChanged Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void CategoryTreeView_TreeNodeCheckChanged(object sender, TreeNodeEventArgs e)
        {
            // this.DisplaySelectedCategories();
        }

        /// <summary> 
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.ItemId > 0)
            {
                Response.Redirect(this.CancelLink + this.ItemId);
            }
            else
            {
                Response.Redirect(this.ListLink);
            }
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = true;
        }

        /// <summary>
        /// Shipping rule type selected index change event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ShippingTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.EnableValidators();
        }

        /// <summary>
        /// Redirecting to Next step
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnNext_Click(object sender, EventArgs e)
        {
            this.SaveProductInformation();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Save product Information
        /// </summary>
        private void SaveProductInformation()
        {
            // Declarations
            SKUAdmin skuAdminAccess = new SKUAdmin();
            ProductAdmin productAdmin = new ProductAdmin();
            SKUAttribute skuAttribute = new SKUAttribute();

            SKU sku = new SKU();
            SKUAdmin skuAdmin = new SKUAdmin();

            ProductCategory productCategory = new ProductCategory();
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();
            System.IO.FileInfo fileInfo = null;
            System.IO.FileInfo skufileInfo = null;

            bool retVal = false;

            // Set Product Properties
            // Passing Values 
            product.ProductID = this.ItemId;

            // If edit mode then get all the values first
            if (this.ItemId > 0)
            {
                product = productAdmin.GetByProductId(this.ItemId);

                if (ViewState["productSkuId"] != null)
                {
                    sku = skuAdminAccess.GetBySKUID(Convert.ToInt32(ViewState["productSkuId"].ToString()));
                }
                else
                {
                    // Inventory
                    TList<SKU> skuList = skuAdmin.GetByProductID(this.ItemId);
                    if (skuList.Count > 0)
                    {
                        sku = skuList[0];
                        ViewState["productSkuId"] = sku.SKUID;
                    }
                }

                bool isSkuExist = skuAdminAccess.IsSkuExist(txt2COID.Text.Trim(), this.ItemId);

                if (isSkuExist)
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSkuExists").ToString();
                    return;
                }

                productAdmin.DeleteProductCategories(this.ItemId);

                // Add Product Categories
                foreach (TreeNode Node in CategoryTreeView.CheckedNodes)
                {
                    ProductCategory prodCategory = new ProductCategory();
                    ProductAdmin prodAdmin = new ProductAdmin();
                    prodCategory.ActiveInd = true;
                    prodCategory.CategoryID = int.Parse(Node.Value);
                    prodCategory.ProductID = this.ItemId;
                    prodAdmin.AddProductCategory(prodCategory);
                }
            }

            // General Info
            product.Name = Server.HtmlEncode(txtProductName.Text);
            product.ImageFile = txtimagename.Text;
            product.ProductNum = Server.HtmlEncode(txtProductNum.Text);
            ProductTypeService productTypeService = new ProductTypeService();
            TList<ProductType> productTypes = productTypeService.GetAll();
            if (productTypes != null)
            {
                if (productTypes.Count > 0)
                {
                    product.ProductTypeID = Convert.ToInt32(productTypes[0].ProductTypeId);
                    this.ProductTypeId = product.ProductTypeID;
                }
                else
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogProductType").ToString());
                }
            }

            product.AffiliateUrl = txtAffiliateLink.Text.Trim();
            product.Description = Server.HtmlEncode(htmlPortalDesc.Html);
            product.ShortDescription = Server.HtmlEncode(txtShortDesc.Text);
            product.AdditionalInformation = Server.HtmlEncode(htmlShippingDesc.Html);
            product.FeaturesDesc = Server.HtmlEncode(htmlProductDetail.Html);
            product.RetailPrice = Convert.ToDecimal(txtproductRetailPrice.Text);
            product.InStockMsg = Server.HtmlEncode(txtInStockMsg.Text);
            product.OutOfStockMsg = Server.HtmlEncode(txtOutofStock.Text);
            product.ReviewStateID = Convert.ToInt32(ZNodeProductReviewState.Approved);

            if (txtproductSalePrice.Text.Trim().Length > 0)
            {
                product.SalePrice = Convert.ToDecimal(txtproductSalePrice.Text.Trim());
            }
            else
            {
                product.SalePrice = null;
            }

            if (txtproductshipweight.Text.Trim().Length > 0)
            {
                product.Weight = Convert.ToDecimal(txtproductshipweight.Text.Trim());
            }
            else
            {
                product.Weight = null;
            }

            // Product Height - Which will be used to determine the shipping cost
            if (txtProductHeight.Text.Trim().Length > 0)
            {
                product.Height = decimal.Parse(txtProductHeight.Text.Trim());
            }
            else
            {
                product.Height = null;
            }

            if (txtProductWidth.Text.Trim().Length > 0)
            {
                product.Width = decimal.Parse(txtProductWidth.Text.Trim());
            }
            else
            {
                product.Width = null;
            }

            if (txtProductLength.Text.Trim().Length > 0)
            {
                product.Length = decimal.Parse(txtProductLength.Text.Trim());
            }
            else
            {
                product.Length = null;
            }

            // Quantity Available
            int qty;
            decimal handling;
            decimal recAmount;
            decimal.TryParse(txtStartupfee.Text, out recAmount);
            int.TryParse(txtProductQuantity.Text, out qty);
            decimal.TryParse(txtHandling.Text, out handling);
            product.RecurringBillingInd = chkRecurringInd.Checked;
            if (chkRecurringInd.Checked)
            {
                product.RecurringBillingInitialAmount = recAmount;
                product.RecurringBillingPeriod = txtContinueBill.Text;
                product.RecurringBillingFrequency = txtBillevery.Text + "-" + ddlBillEvery.SelectedItem.Text;
            }

            product.ActiveInd = chkActiveInd.Checked;
            product.IsShippable = chkShipInd.Checked;

            // Inventory Setting - Out of Stock Options
            if (InvSettingRadiobtnList.SelectedValue.Equals("1"))
            {
                // Only Sell if Inventory Available - Set values
                product.TrackInventoryInd = true;
                product.AllowBackOrder = false;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("2"))
            {
                // Allow Back Order - Set values
                product.TrackInventoryInd = true;
                product.AllowBackOrder = true;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("3"))
            {
                // Don't Track Inventory - Set property values
                product.TrackInventoryInd = false;
                product.AllowBackOrder = false;
            }

            product.ShippingRate = handling;


            // Stock
            sku.ProductID = this.ItemId;
            sku.SKU = txt2COID.Text.Trim();
            sku.ActiveInd = true;

            SKUAdmin.UpdateQuantity(sku, Convert.ToInt32(txtProductQuantity.Text));

            // Database & Image Updates
            // Set update date
            product.UpdateDte = System.DateTime.Now;

            string NewchangedFields = hdnDirtyFields.Value;
            NewchangedFields = NewchangedFields.Replace("~~", ",");
            NewchangedFields = NewchangedFields.Replace("~", string.Empty);
            if (NewchangedFields != hdnDirtyFieldsOrg.Value)
            {
                product.ReviewStateID = (int)ZNodeProductReviewState.Approved;
            }

            // Create transaction
            TransactionManager tranManager = ConnectionScope.CreateTransaction();

            try
            {
                if (this.ItemId > 0)
                {
                    // PRODUCT UPDATE
                    if (uxProductImage.PostedFile != null)
                    {
                        // Validate image
                        if ((this.ItemId == 0 || RadioProductNewImage.Checked == true) && uxProductImage.PostedFile.FileName.Length > 0)
                        {
                            uxProductImage.AppendToFileName = "-" + this.ProductId.ToString();
                        }
                    }

                    // Upload File if this is a new product or the New Image option was selected for an existing product
                    if (RadioProductNewImage.Checked || this.ItemId == 0)
                    {
                        if (uxProductImage.PostedFile != null && !string.IsNullOrEmpty(uxProductImage.PostedFile.FileName))
                        {
                            bool isSuccess;
                            if (!string.IsNullOrEmpty(product.AccountID.ToString()))
                            {
                                isSuccess = uxProductImage.SaveImage("madmin", "0", product.AccountID.ToString());
                            }
							else if (!string.IsNullOrEmpty(product.PortalID.ToString()))
                            {
                                isSuccess = uxProductImage.SaveImage("fadmin", product.PortalID.ToString(), "0");
                            }
                            else
                            {
                                isSuccess = uxProductImage.SaveImage("sadmin", ZNodeConfigManager.SiteConfig.PortalID.ToString(), "0");
                            }
                            fileInfo = uxProductImage.FileInformation;
                            skufileInfo = uxProductImage.FileInformation;
                            if (!isSuccess)
                            {
                                if (uxProductImage.FileInformation.Name != string.Empty)
                                {
                                    if (!string.IsNullOrEmpty(product.AccountID.ToString()))
                                    {
                                        product.ImageFile = "Mall/" + product.AccountID.ToString() + "/" + uxProductImage.FileInformation.Name;
                                        sku.SKUPicturePath = "Mall/" + product.AccountID.ToString() + "/" + skufileInfo.Name;
                                    }
                                    else if (!string.IsNullOrEmpty(product.PortalID.ToString()))
                                    {
                                        product.ImageFile = "Turnkey/" + product.PortalID.ToString() + "/" + uxProductImage.FileInformation.Name;
                                        sku.SKUPicturePath = "Turnkey/" + product.PortalID.ToString() + "/" + skufileInfo.Name;
                                    }
                                    else
                                    {
                                        product.ImageFile = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + uxProductImage.FileInformation.Name;
                                        sku.SKUPicturePath = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + skufileInfo.Name;
                                    }
                                }

                                return;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(product.AccountID.ToString()))
                                {
                                    product.ImageFile = "Mall/" + product.AccountID.ToString() + "/" + uxProductImage.FileInformation.Name;
                                    sku.SKUPicturePath = "Mall/" + product.AccountID.ToString() + "/" + skufileInfo.Name;
                                }
                                else if (!string.IsNullOrEmpty(product.PortalID.ToString()))
                                {
                                    product.ImageFile = "Turnkey/" + product.PortalID.ToString() + "/" + uxProductImage.FileInformation.Name;
                                    sku.SKUPicturePath = "Turnkey/" + product.PortalID.ToString() + "/" + skufileInfo.Name;
                                }
                                else
                                {
                                    product.ImageFile = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + uxProductImage.FileInformation.Name;
                                    sku.SKUPicturePath = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + skufileInfo.Name;
                                }
                            }
                        }
                    }

                    if (sku.SKUID > 0)
                    {
                        // For this product already SKU if on exists
                        sku.UpdateDte = System.DateTime.Now;

                        retVal = productAdmin.Update(product);

                        // update sku;
                        skuAdminAccess.Update(sku);
                    }
                    else
                    {
                        retVal = productAdmin.Update(product);

                        // If Product doesn't have any SKUs yet,then create new SKU in the database
                        skuAdminAccess.Add(sku);
                    }

                    if (!retVal)
                    {
                        throw new ApplicationException();
                    }

                    // Delete existing SKUAttributes
                    skuAdminAccess.DeleteBySKUId(sku.SKUID);

                    if (NewchangedFields != hdnDirtyFieldsOrg.Value)
                    {
                        string changedFields = hdnDirtyFields.Value;
                        changedFields = changedFields.Replace("~~", ",");
                        this.UpdateReviewHistory(changedFields.Replace("~", string.Empty));
                    }
                }
                else
                {
                    // PRODUCT ADD
                    product.ActiveInd = true;

                    // UPDATE Account ID reference for Vendor.
                    AccountService accountService = new AccountService();
                    TList<Account> accountList = accountService.GetByUserID((Guid)Membership.GetUser().ProviderUserKey);
                    if (accountList != null && accountList.Count > 0)
                    {
                        product.AccountID = accountList[0].AccountID;
                    }

                    // If ProductType has SKUs, then insert sku with Product
                    retVal = productAdmin.Add(product, sku, out this.ProductId, out this.SKUId);

                    if (!retVal)
                    {
                        throw new ApplicationException();
                    }

                    // Add Category List for the Product
                    foreach (TreeNode Node in CategoryTreeView.CheckedNodes)
                    {
                        ProductCategory prodCategory = new ProductCategory();
                        ProductAdmin prodAdmin = new ProductAdmin();
                        prodCategory.ActiveInd = true;
                        prodCategory.CategoryID = int.Parse(Node.Value);
                        prodCategory.ProductID = this.ProductId;
                        prodAdmin.AddProductCategory(prodCategory);
                    }

                    // Image Validation
                    if (uxProductImage.PostedFile != null)
                    {
                        // Validate image
                        if ((this.ItemId == 0 || RadioProductNewImage.Checked == true) && uxProductImage.PostedFile.FileName.Length > 0)
                        {
                            uxProductImage.AppendToFileName = "-" + this.ProductId.ToString();
                        }
                    }

                    // Upload File if this is a new product or the New Image option was selected for an existing product
                    if (RadioProductNewImage.Checked || this.ItemId == 0)
                    {
                        if (uxProductImage.PostedFile != null && !string.IsNullOrEmpty(uxProductImage.PostedFile.FileName))
                        {
                            bool isSuccess = uxProductImage.SaveImage("sadmin", ZNodeConfigManager.SiteConfig.PortalID.ToString(), "0");
                            fileInfo = uxProductImage.FileInformation;
                            skufileInfo = uxProductImage.FileInformation;
                            if (!isSuccess)
                            {
                                if (uxProductImage.FileInformation.Name != string.Empty)
                                {
                                    product.ImageFile = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + uxProductImage.FileInformation.Name;
                                    sku.SKUPicturePath = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + skufileInfo.Name;
                                }
                            }
                            else
                            {
                                product.ImageFile = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + uxProductImage.FileInformation.Name;
                                sku.SKUPicturePath = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + skufileInfo.Name;
                            }
                        }
                    }

                    productAdmin.Update(product);
                }

                // Commit transaction
                tranManager.Commit();
            }
            catch (Exception exp)
            {
                // Error occurred so rollback transaction
                if (tranManager.IsOpen)
                {
                    tranManager.Rollback();
                }

                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(exp.Message.ToString());
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductUpdate").ToString(); 
                return;
            }

            // Redirect to next page
            UserStoreAccess uss = new UserStoreAccess();
            if (this.ItemId > 0)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEdit") + txtProductName.Text, txtProductName.Text);

                Response.Redirect(this.CancelLink + this.ItemId);
            }
            else
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogCreateProduct") + txtProductName.Text, txtProductName.Text);
                Session["itemid"] = this.ProductId;
                this.ItemId = this.ProductId;
                Response.Redirect(this.CancelLink + this.ItemId);
            }

            if (this.ItemId > 0)
            {
                product = productAdmin.GetByProductId(this.ItemId);
                product.ReviewStateID = (int)ZNodeProductReviewState.Approved;
                productAdmin.Update(product);
            }
        }

        /// <summary>
        /// Display the selected node in label.
        /// </summary>
        private void DisplaySelectedCategories()
        {
            StringBuilder categories = new StringBuilder();
            categories.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "TextCurrentCategory").ToString());
            TreeNodeCollection nodeCollection = CategoryTreeView.CheckedNodes;
            if (nodeCollection.Count > 0)
            {
                foreach (TreeNode node in nodeCollection)
                {
                    categories.Append(node.Text + "<BR>");
                }
            }
            else
            {
                categories.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "TextAnyCategory").ToString());
            }

            lblCategories.Text = categories.ToString();
        }

        /// <summary>
        /// Enable Validators Method
        /// </summary>
        private void EnableValidators()
        {
            weightBasedRangeValidator.Enabled = false;
            RequiredForWeightBasedoption.Enabled = false;
        }

        /// <summary>
        /// SetJSForDirtyFields Method
        /// </summary>
        /// <param name="page">The value of Page</param>
        private void SetJSForDirtyFields(ControlCollection page)
        {
            foreach (Control ctrl in page)
            {
                // SetDirtyHiddenMsgWithID 
                if (ctrl is TextBox)
                {
                    (ctrl as TextBox).Attributes.Add("onchange", string.Format("SetDirtyHiddenMsgWithID('{0}','{1}')", hdnDirtyFields.ClientID, (ctrl as TextBox).ToolTip));
                }

                if (ctrl is CheckBox)
                {
                    (ctrl as CheckBox).Attributes.Add("onchange", string.Format("SetDirtyHiddenMsgWithID('{0}','{1}')", hdnDirtyFields.ClientID, (ctrl as CheckBox).ToolTip));
                }
                else if (ctrl.HasControls())
                {
                    this.SetJSForDirtyFields(ctrl.Controls);
                }
            }
        }

        /// <summary>
        /// UpdateReviewHistory Method
        /// </summary>
        /// <param name="changedFields">The value of changedFields</param>
        private void UpdateReviewHistory(string changedFields)
        {
            // Add entry to Product Review history table.
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            ProductReviewHistory productReviewHistory = new ProductReviewHistory();
            productReviewHistory.ProductID = this.ItemId;
            productReviewHistory.VendorID = this.GetVendorId(this.ItemId);
            productReviewHistory.LogDate = DateTime.Now;
            productReviewHistory.Username = this.Page.User.Identity.Name;
            productReviewHistory.Status = ZNodeProductReviewState.Approved.ToString();
            productReviewHistory.Reason = string.Empty;
            productReviewHistory.Description = HttpUtility.HtmlEncode(changedFields);
            if (productReviewHistory.VendorID != 0)
            {
                bool isSuccess = productReviewHistoryAdmin.Add(productReviewHistory);
            }
        }

        /// <summary>
        /// Get the supplier ID
        /// </summary>
        /// <param name="productId">Product ID</param>
        /// <returns>Returns the products supplier ID</returns>
        private int GetVendorId(int productId)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = productAdmin.GetByProductId(productId);
            return Convert.ToInt32(product.AccountID);
        }

        /// <summary>
        /// Recursively expands parent nodes of a selected tree node
        /// </summary>
        /// <param name="nodeItem">The value of nodeItem</param>
        private void RecursivelyExpandTreeView(TreeNode nodeItem)
        {
            if (nodeItem.Parent != null)
            {
                nodeItem.Parent.ExpandAll();
                this.RecursivelyExpandTreeView(nodeItem.Parent);
            }
            else
            {
                nodeItem.Expand();
                return;
            }
        }

        /// <summary>
        /// Populates a treeview with store categories for a Product
        /// </summary>
        /// <param name="selectedCategoryId">selectedCategoryId value</param>
        private void PopulateEditTreeView(string selectedCategoryId)
        {
            foreach (TreeNode Tn in CategoryTreeView.Nodes)
            {
                if (Tn.Value.Equals(selectedCategoryId))
                {
                    Tn.Checked = true;
                }

                this.RecursivelyEditPopulateTreeView(Tn, selectedCategoryId);
            }
        }

        /// <summary>
        /// Recursively populate a particular node with it's children for a product
        /// </summary>
        /// <param name="childNodes">The value of childNodes</param>
        /// <param name="selectedCategoryid">The value of selectedCategoryid</param>
        private void RecursivelyEditPopulateTreeView(TreeNode childNodes, string selectedCategoryid)
        {
            foreach (TreeNode CNodes in childNodes.ChildNodes)
            {
                if (CNodes.Value.Equals(selectedCategoryid))
                {
                    CNodes.Checked = true;
                    this.RecursivelyExpandTreeView(CNodes);
                }

                this.RecursivelyEditPopulateTreeView(CNodes, selectedCategoryid);
            }
        }

        /// <summary>
        /// Recursively populate a particular node with it's children
        /// </summary>
        /// <param name="dataRow">The value of dbRow</param>
        /// <param name="parentNode">The value of parentNode</param>
        /// <param name="selectedCategoryId">The value of selectedCategoryId</param>
        private void RecursivelyPopulateTreeView(DataRow dataRow, TreeNode parentNode, string selectedCategoryId)
        {
            bool IsLeafNode = true;
            foreach (DataRow childRow in dataRow.GetChildRows("NodeRelation"))
            {
                if ((bool)childRow["VisibleInd"])
                {
                    IsLeafNode = false;

                    // Create new tree node
                    TreeNode tn = new TreeNode();
                    tn.Text = childRow["Name"].ToString();
                    tn.Value = childRow["categoryid"].ToString();

                    string categoryId = childRow["CategoryId"].ToString();

                    parentNode.ChildNodes.Add(tn);

                    if (selectedCategoryId.Equals(categoryId))
                    {
                        tn.Selected = true;
                        this.RecursivelyExpandTreeView(tn);
                    }

                    this.RecursivelyPopulateTreeView(childRow, tn, selectedCategoryId);
                }
            }

            if (IsLeafNode)
            {
                parentNode.SelectAction = TreeNodeSelectAction.None;
            }
        }
        #endregion
    }
}