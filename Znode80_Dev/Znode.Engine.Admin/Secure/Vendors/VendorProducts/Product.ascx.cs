using System;
using System.Collections;
using System.Web;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;

namespace  Znode.Engine.Admin.Secure.Vendors.VendorProducts
{
    /// <summary>
    /// Represents the SiteAdmin - Public_Templates_Professional_product_Product class
    /// </summary>
    public partial class Product : System.Web.UI.UserControl
    {
        #region Private Variables
        private ZNodeProduct product;
        private ZNodeProfile profile = new ZNodeProfile();
        private int ProductId;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Shopping cart quantity
        /// </summary>
        public int ShoppingCartQuantity
        {
            get
            {
                if (ViewState["Quantity"] != null)
                {
                    return (int)ViewState["Quantity"];
                }

                return 1;
            }
            
            set
            {
                ViewState["Quantity"] = value;
            }
        }
        #endregion

        #region Private Properties
        /// <summary>
        /// Gets the Product CategoryTitle Method
        /// </summary>
        /// <returns>Returns the Category Title</returns>
        private string ProductCategoryTitle
        {
            get
            {
                int zcid = 0;

                if (Request.QueryString["zpid"] != null)
                {
                    ZNode.Libraries.DataAccess.Service.ProductCategoryService service = new ZNode.Libraries.DataAccess.Service.ProductCategoryService();

                    TList<ProductCategory> pcategories = service.GetByProductID(this.ProductId);

                    if (pcategories.Count > 0)
                    {
                        zcid = pcategories[0].CategoryID;
                        Session["BreadCrumzcid"] = zcid;
                    }
                }
                else
                {
                    zcid = 0;
                }

                ZNode.Libraries.DataAccess.Service.CategoryService cservice = new ZNode.Libraries.DataAccess.Service.CategoryService();
                Category category = cservice.GetByCategoryID(Convert.ToInt32(zcid));

                if (category != null)
                {
                    return category.Title;
                }

                return string.Empty;
            }
        }
        #endregion

        #region Public Methods

        /// <summary>
        /// Update and Bind inventory status message and product price
        /// </summary>
        public void UpdateStatus()
        {
            // Hide Product price
            uxProductPrice.Visible = !this.product.CallForPricing;

            // Local Variables
            this.ShoppingCartQuantity = int.Parse(uxQty.SelectedValue);
            bool _ErrorFound = false;

            // Display Stock Message
            if (!_ErrorFound)
            {
                // Don't track Inventory - Items can always be added to the cart,TrackInventory is disabled
                if (!this.product.TrackInventoryInd && !this.product.AllowBackOrder && this.product.QuantityOnHand > 0)
                {
                    lblstockmessage.Text = this.product.InStockMsg;
                }
                else if (!this.product.TrackInventoryInd && !this.product.AllowBackOrder && this.product.QuantityOnHand <= 0)
                {
                    lblstockmessage.Text = string.Empty;
                }
                else if (this.product.TrackInventoryInd && !this.product.AllowBackOrder && this.product.QuantityOnHand >= this.ShoppingCartQuantity)
                {
                    lblstockmessage.Text = this.product.InStockMsg;
                }
                else if (this.product.AllowBackOrder && this.product.QuantityOnHand >= this.ShoppingCartQuantity)
                {
                    // Set Inventory stock message
                    lblstockmessage.Text = this.product.InStockMsg;
                }
                else if (this.product.AllowBackOrder && this.product.QuantityOnHand < this.ShoppingCartQuantity)
                {
                    lblstockmessage.Text = this.product.BackOrderMsg;
                }
                else
                {
                    // Display Out of stock message
                    lblstockmessage.Text = this.product.OutOfStockMsg;

                    _ErrorFound = true;
                }
            }

            bool hasAddOns = false;

            // Check if product has attributes
            if (this.product.ZNodeAddOnCollection.Count > 0)
            {
                hasAddOns = true;
            }

            // If product has Add-Ons, then validate that all Add-Ons Values have been selected by the customer
            if (hasAddOns)
            {
                bool retval = false;
                string statusMessage = string.Empty;

                // Get Selected Add-ons
                this.product.SelectedAddOnItems = uxProductAddOns.GetSelectedAddOns(out retval, out statusMessage);

                if (retval && !_ErrorFound)
                {
                    uxStatus.Text = statusMessage;
                }
            }

            // Check for 'Call for Pricing'
            if (this.product.CallForPricing)
            {
                lblstockmessage.Visible = false;
                uxCallForPricing.Visible = true;
                uxStatus.Visible = false;
            }
            else
            {
                lblstockmessage.Visible = true;
            }

            // Bind selected sku product price if one exists
            uxProductPrice.Quantity = this.ShoppingCartQuantity;
            uxProductPrice.Product = this.product;
            uxProductPrice.Bind();
        }
        #endregion

        #region Page Load

        /// <summary>
        /// Page Init Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Init(object sender, EventArgs e)
        {
            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this.ProductId = int.Parse(Request.Params["zpid"]);
            }
            else if (Request.Params["itemid"] != null)
            {
                this.ProductId = int.Parse(Request.Params["itemid"]);
            }
            else if (Session["itemid"] != null)
            {
                this.ProductId = int.Parse(Session["itemid"].ToString());
            }
            else
            {
                return;
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this.product = ZNodeProduct.Create(this.ProductId, 0, 0, true, null, true);
            HttpContext.Current.Items["Product"] = this.product;
        }
 
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // This code must be placed here for SEO Url Redirect to work and fix
            // postback problem with URL rewriting
            if (this.Page.Form != null)
            {
                this.Page.Form.Action = Request.RawUrl;
            }

            // Get product id from querystring  
            if (Request.Params["zpid"] != null)
            {
                this.ProductId = int.Parse(Request.Params["zpid"]);
            }
            else if (Request.Params["itemid"] != null)
            {
                this.ProductId = int.Parse(Request.Params["itemid"]);
            }
            else if (Session["itemid"] != null)
            {
                this.ProductId = int.Parse(Session["itemid"].ToString());
            }
            else
            {
                return;
            }

            // Retrieve product object from HttpContext (set previously in the page_preinit event of the page)
            this.product = ZNodeProduct.Create(this.ProductId, 0, 0, true, null, true);
            HttpContext.Current.Items["Product"] = this.product;

            NewItemImage.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/New.gif";

            // Bind all controls
            this.Bind();
        }
        #endregion

        #region General Events

        /// <summary>
        /// Send a Email Link Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EmailLink_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/EmailFriend.aspx?zpid=" + this.ProductId.ToString());
        }

        /// <summary>
        /// uantity Drop down list index changed event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Quantity_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.UpdateStatus();
        }

        /// <summary>
        /// Get catalog product image Name
        /// </summary>
        /// <param name="Title">The value of Title</param>
        /// <returns>Returns the Image name</returns>
        protected string GetImageName(string Title)
        {
            if (Title.Trim().Length > 0)
            {
                return Title;
            }
            else
            {
                return "&nbsp";
            }
        }
        #endregion

        #region Bind method
        /// <summary>
        /// Bind all the controls to the data
        /// </summary>
        private void Bind()
        {
            uxProductSwatches.ProductId = this.ProductId;
            pnlSwatches.Visible = uxProductSwatches.HasImages;

            uxProductPrice.Visible = this.profile.ShowPrice && !this.product.CallForPricing;
            pnlQty.Visible = this.profile.ShowPrice;

            bool showCartButton = this.profile.ShowAddToCart;

            // if out of stock and backorder disabled then hide the AddToCart button
            if (this.product.ZNodeAttributeTypeCollection.Count > 0)
            {
                // If product has attributes,then display the AddToCart button
                showCartButton &= true;
            }
            else if (this.product.AllowBackOrder && this.product.QuantityOnHand <= 0)
            {
                // Allow back Order
                showCartButton &= true;

                // Set back order message
                lblstockmessage.Text = this.product.BackOrderMsg;
            }
            else if (this.product.AllowBackOrder && this.product.QuantityOnHand > 0)
            {
                showCartButton &= true;

                // Set Item In-Stock message
                lblstockmessage.Text = this.product.InStockMsg;
            }
            else if (this.product.QuantityOnHand > 0 && this.product.TrackInventoryInd == true && !this.product.AllowBackOrder)
            {
                // Track Inventory
                showCartButton &= true;

                // Set In stock message
                lblstockmessage.Text = this.product.InStockMsg;
            }
            else if (this.product.AllowBackOrder == false && this.product.TrackInventoryInd == false && this.product.QuantityOnHand > 0)
            {
                // Don't track Inventory
                // Items can always be added to the cart,TrackInventory is disabled
                showCartButton &= true;

                // Set In stock message
                lblstockmessage.Text = this.product.InStockMsg;
            }
            else if (this.product.AllowBackOrder == false && this.product.TrackInventoryInd == false && this.product.QuantityOnHand <= 0)
            {
                // Don't track Inventory
                // Items can always be added to the cart,TrackInventory is disabled
                showCartButton &= true;
               
                // Set OutOf Stock message
                lblstockmessage.Text = string.Empty;
            }
            else
            {
                // Set OutOf Stock message
                lblstockmessage.Text = this.product.OutOfStockMsg;
                showCartButton &= false;
            }

            // Set Visible true for NewItem.
            if (this.product.NewProductInd)
            {
                NewItemImage.Visible = true;
            }

            // Set Visible true for FeaturedItem.
            if (this.product.FeaturedInd)
            {
                FeaturedItemImage.Visible = true;
            }

            uxCallForPricing.Text = ZNodeCatalogManager.MessageConfig.GetMessage("ProductCallForPricing");

            // add to cart button
            if (this.product.CallForPricing)
            {
                // If call for pricing is enabled,then hide AddToCart Button
                showCartButton &= false;
                FeaturedItemImage.Visible = false;
                lblstockmessage.Text = string.Empty;
            }

            uxCallForPricing.Visible = !showCartButton;

            // Set product properties       
            ProductTitle.Text = string.Format(ProductTitle.Text, this.product.Name);
            txtProductID.Text = this.product.ProductNum;
            ProductDescription.Text = Server.HtmlDecode(this.product.Description);

            // Set Manufacturer Name
            if (this.product.ManufacturerName != string.Empty)
            {
                BrandLabel.Visible = true;
                lblBrand.Text = this.product.ManufacturerName;
            }

            // Bind the MaxQuantity value to the Dropdown list
            // If Min quantity is not set in admin, set it to 1
            int minQty = this.product.MinQty == 0 ? 1 : this.product.MinQty;

            // If Max quantity is not set in admin , set it to 10
            int maxQty = this.product.MaxQty == 0 ? 10 : this.product.MaxQty;

            ArrayList quantityList = new ArrayList();

            for (int itemIndex = minQty; itemIndex <= maxQty; itemIndex++)
            {
                quantityList.Add(itemIndex);
            }
            
            uxQty.DataSource = quantityList;
            uxQty.DataBind();

            uxProductRelated.Product = this.product;
            uxProductRelated.Bind();
        }
        #endregion
    }
}