﻿<%@ Page Title="Manage Vendor Products - Review Image" Language="C#" MasterPageFile="~/Themes/Standard/content.master"
    AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Vendors.VendorProducts.ReviewImage" CodeBehind="ReviewImage.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="ProductReviewStateAutoComplete" Src="~/Controls/Default/ProductReviewStateAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ReviewStateLegend" Src="~/Controls/Default/ReviewStateLegend.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div>
            <div class="LeftFloat">
                <h1>
                    <asp:Localize runat="server" ID="ProductImages" Text='<%$ Resources:ZnodeAdminResource, TitleProductImages %>'></asp:Localize>
                </h1>
            </div>
            <div class="LeftFloat ImageButtons ValueStyle" style="width: 100%" align="right">
                <zn:Button runat="server" ID="btnBack" ButtonType="SubmitButton" OnClick="BtnBack_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonBackToListingPage %>'
                    CausesValidation="False" Width="175" />
            </div>
        </div>
        <div class="ClearBoth" align="left">
            <p>
                <asp:Localize runat="server" ID="ProductImage" Text='<%$ Resources:ZnodeAdminResource, TextProductImages %>'></asp:Localize>
            </p>
            <br />
        </div>
        <h4 class="SubTitle">
            <asp:Localize runat="server" ID="SearchImages" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchImages %>'></asp:Localize>
        </h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle" runat="server" id="ReviewerSearch">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="Vendor" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleVendor %>'></asp:Localize>
                        </span>
                        <br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtVendor" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="VendorId" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleVendorId %>'></asp:Localize>
                        </span>
                        <br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtVendorId" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle" runat="server" id="div2CO">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="SKU" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSKU %>'></asp:Localize>
                        </span>
                        <br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtSKU" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="ProductName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>'></asp:Localize>
                        </span>
                        <br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtProductName" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="VendorProductId" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleVendorProductId %>'></asp:Localize>
                        </span>
                        <br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtVendorproductId" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="ImageStatus" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleImageStatus %>'></asp:Localize>
                        </span>
                        <br />
                        <span class="ValueStyle">
                            <ZNode:ProductReviewStateAutoComplete ID="uxProductStatus" runat="server" />
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <br />
                </div>
                <div>
                    <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                    <zn:Button runat="server" ID="btnSearch" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="True" ValidationGroup="grpReports" />
                </div>
            </div>
        </asp:Panel>
        <div>
            <div style="text-align: right;">
                <zn:Button runat="server" ID="btnAccept" ButtonType="SubmitButton" OnClick="BtnAccept_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAccept %>' />
                <zn:Button runat="server" ID="btnReject" ButtonType="SubmitButton" OnClick="BtnReject_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonReject %>' />
            </div>
            <br />
            <ZNode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
            <asp:Label ID="lblMessage" runat="server" CssClass="Error" Text="" />
        </div>
        <div>
            <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <asp:UpdatePanel ID="upProductList" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="gvProductImages" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                    runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                    CssClass="Grid" Width="100%" GridLines="None" PageSize="10" OnPageIndexChanging="GvProductImages_PageIndexChanging"
                    AllowPaging="True" OnRowDataBound="GvProductImages_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="15">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAll" AutoPostBack="true" runat="server" OnCheckedChanged="ChkSelectAll_CheckedChanged" />

                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkProduct" runat="server" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus %>' ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <img id="imgStatus" src='<%# GetStatusImageIcon(DataBinder.Eval(Container.DataItem, "ReviewStateID"))%>'
                                    runat="server" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ProductImageId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left">
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                        </asp:BoundField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleImage %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <img id="SwapImage" alt="" src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString()) %>'
                                    runat="server" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ImageTypeName" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleImageType %>' HeaderStyle-HorizontalAlign="Left">
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                        </asp:BoundField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProductPage %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <img id="Img1" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                    runat="server" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCategoryPage %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <img id="Img2" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ShowOnCategoryPage").ToString()))%>'
                                    runat="server" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                            <ItemStyle HorizontalAlign="Center"></ItemStyle>
                        </asp:TemplateField>
                        <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left">
                            <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                        </asp:BoundField>
                    </Columns>
                    <EmptyDataTemplate>
                        <asp:Localize runat="server" ID="TextGenerateKeySettings" Text='<%$ Resources:ZnodeAdminResource, GridEmptyTextNoImage %>'></asp:Localize>
                    </EmptyDataTemplate>
                    <RowStyle CssClass="RowStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <FooterStyle CssClass="FooterStyle" />
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div style="float: right;">
            <ZNode:ReviewStateLegend ID="uxReviewStateLegend" runat="server" />
        </div>
    </div>
</asp:Content>
