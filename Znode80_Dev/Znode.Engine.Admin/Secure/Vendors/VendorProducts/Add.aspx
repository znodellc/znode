<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" MaintainScrollPositionOnPostback="true" Inherits="Znode.Engine.Admin.Secure.Vendors.VendorProducts.Add" CodeBehind="Add.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="uc1" %>
<%@ Register Src="~/Controls/Default/ImageUploader.ascx" TagName="UploadImage"
    TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">

    <script language="javascript" type="text/javascript">
        function postBackByObject(mEvent) {
            var o;
            // Internet Explorer    
            if (mEvent.srcElement) {
                o = mEvent.srcElement;
            }
                // Netscape and Firefox
            else if (mEvent.target) {
                o = mEvent.target;
            }
            if (o.tagName == "INPUT" && o.type == "checkbox") {
                __doPostBack("", "");
            }
        }
    </script>


    <asp:HiddenField ID="existSKUvalue" runat="server" />
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ID="btnNext1" Text='<%$ Resources:ZnodeAdminResource, ButtonNextStep%>' OnClick="BtnNext_Click" Width="100px"/>
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnClear" />
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnNext_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmit" />
        </div>
        <div style="clear: both">
            <asp:Label ID="Label1" CssClass="Error" runat="server"></asp:Label>
        </div>
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>

        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="FormView">
            <h4 class="SubTitle">
                <asp:Localize ID="SubTitleGeneral" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralInformation%>' runat="server"></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnTitleName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName%>' runat="server"></asp:Localize><span class="Asterix">*</span><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextProductName" Text='<%$ Resources:ZnodeAdminResource, HintTextProductName%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductName" ToolTip='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName%>' runat="server" Width="252px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtProductName"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductName%>' CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnTitleNumber" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductsNumber%>' runat="server"></asp:Localize><span class="Asterix">*</span><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextProductNumber" Text='<%$ Resources:ZnodeAdminResource, HintTextProductNumber%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductNum" ToolTip='<%$ Resources:ZnodeAdminResource, ColumnTitleProductsNumber%>' runat="server" Width="252px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="ProductName" runat="server" ControlToValidate="txtProductNum"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductNumber%>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            </div>
            <div>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ColumnTitleSkuorPart" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSkuorPart%>'></asp:Localize><span class="Asterix">*</span><br />
                    <small>
                        <asp:Localize runat="server" ID="HintTextProductCode" Text='<%$ Resources:ZnodeAdminResource, HintTextProductCode%>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txt2COID" ToolTip='<%$ Resources:ZnodeAdminResource, ColumnTitleSkuorPart%>' runat="server" Width="252px"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt2COID"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterSku%>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div visible="false" runat="server">
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ColumnTitleReceipt" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleReceipt%>'></asp:Localize><span class="Asterix">*</span><br />
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtReceiptDesc" ToolTip='<%$ Resources:ZnodeAdminResource, ColumnTitleReceipt%>' MaxLength="100" runat="server"
                        Width="252px"></asp:TextBox>
                    <asp:Label ID="lblReceiptDesc" runat="server" Visible="false"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtReceiptDesc"
                        CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredValidReceipt%>'></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnShort" Text='<%$ Resources:ZnodeAdminResource, ColumnShortDescription%>'></asp:Localize>
                <br />
                <small>
                    <asp:Localize runat="server" ID="HintTextShort" Text='<%$ Resources:ZnodeAdminResource, HintTextShortDescription%>'></asp:Localize></small>
                <br />
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtShortDesc" ToolTip='<%$ Resources:ZnodeAdminResource, ColumnShortDescription%>' MaxLength="255" Height="100px"
                    Rows="5" runat="server" Width="645px" TextMode="MultiLine"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnLong" Text='<%$ Resources:ZnodeAdminResource, ColumnLongDescription%>'></asp:Localize><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextCharacter" Text='<%$ Resources:ZnodeAdminResource, HintTextCharacterLimit%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="htmlPortalDesc" runat="server"></uc1:HtmlTextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnProductDetail" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductDetail%>'></asp:Localize><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextLimit" Text='<%$ Resources:ZnodeAdminResource, HintTextCharacterLimit%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="htmlProductDetail" runat="server"></uc1:HtmlTextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleShipping" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleShippingDescription%>'></asp:Localize><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextShipping" Text='<%$ Resources:ZnodeAdminResource, HintTextShipping%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <uc1:HtmlTextBox ID="htmlShippingDesc" runat="server"></uc1:HtmlTextBox>
            </div>
            <div id="Div1" visible="false" runat="server">
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ColumnTitlePortal" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePortalDescription%>'></asp:Localize><br />
                    <small>
                        <asp:Localize runat="server" ID="HintTextCharacterLimit" Text='<%$ Resources:ZnodeAdminResource, HintTextCharacterLimit%>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <uc1:HtmlTextBox ID="htmlPortalDesc1" runat="server"></uc1:HtmlTextBox>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleSelling" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelling%>'></asp:Localize><span class="Asterix">*</span><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextSellPrice" Text='<%$ Resources:ZnodeAdminResource, HintTextSellPrice%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtproductRetailPrice" ToolTip="Selling Price" runat="server" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductRetailPrice%>'
                    ControlToValidate="txtproductRetailPrice" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtproductRetailPrice"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareRetailPrice%>'
                    CssClass="Error" Display="Dynamic" />
                <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtproductRetailPrice"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeRetailPriceValue%>'
                    MaximumValue="99999999" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
            </div>
            <div visible="false" runat="server">
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ColumnTitleMsrp" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleMsrp%>'></asp:Localize>
                    <br />
                    <small>
                        <asp:Localize runat="server" ID="HintTextMsrp" Text='<%$ Resources:ZnodeAdminResource, HintTextMsrp%>'></asp:Localize>
                    </small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtproductSalePrice" ToolTip="MSRP" runat="server" MaxLength="10"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtproductSalePrice"
                        Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareSalePrice%>'
                        CssClass="Error" Display="Dynamic" />
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleShow" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleShowProduct%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkActiveInd" Checked="true" ToolTip='<%$ Resources:ZnodeAdminResource, TooltTipYourProduct%>' Text='<%$ Resources:ZnodeAdminResource, CheckboxMallProduct%>' runat="server" />
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleUrl" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleAffiliateUrl%>'></asp:Localize><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextAffiliate" Text='<%$ Resources:ZnodeAdminResource, HintTextAffiliate%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtAffiliateLink" Columns="50" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RecularExpressionValidator" runat="server" ControlToValidate="txtAffiliateLink"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidAffiliateUrl%>' CssClass="Error" Display="dynamic" ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularExpressionValidURL%>'></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleCategories" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCategories%>'></asp:Localize><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextCategories" Text='<%$ Resources:ZnodeAdminResource, HintTextCategories%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:UpdatePanel runat="server" ID="TreeViewUpdatePanel">
                    <ContentTemplate>
                        <div class="TreeFieldStyle" style="width: 300px; font-weight: normal;">
                            <asp:TreeView ID="CategoryTreeView"
                                runat="server" ImageSet="Arrows" ShowCheckBoxes="All" CssClass="TreeView" ShowLines="True"
                                ExpandDepth="0">
                                <ParentNodeStyle CssClass="ParentNodeStyle" Font-Bold="False" />
                                <HoverNodeStyle CssClass="HoverNodeStyle" Font-Underline="True" />
                                <SelectedNodeStyle CssClass="SelectedNodeStyle" Font-Underline="True"
                                    HorizontalPadding="0px" VerticalPadding="0px" />
                                <RootNodeStyle CssClass="RootNodeStyle" />
                                <LeafNodeStyle CssClass="LeafNodeStyle" />
                                <NodeStyle CssClass="NodeStyle" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black"
                                    HorizontalPadding="0px" NodeSpacing="0px" VerticalPadding="0px" />
                            </asp:TreeView>
                        </div>
                        <div class="ValueStyle" style="width: 300px;">
                            <asp:Label ID="lblCategories" runat="server"></asp:Label>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <br />
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnSelectImage1" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectImage1%>'></asp:Localize><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextImage" Text='<%$ Resources:ZnodeAdminResource, HintTextImage%>'></asp:Localize></small>
            </div>
            <div id="tblShowImage" runat="server" style="margin: 10px 0px 10px 0px;">
                <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;" runat="server"
                    id="pnlImage">
                    <asp:Image ID="Image1" runat="server" />
                </div>
                <div class="LeftFloat" style="margin-left: -1px;" runat="server" id="pnlUploadSection">
                    <asp:Panel runat="server" ID="pnlShowImage">
                        <div>
                            <asp:Localize runat="server" ID="ColumnSelectanOption" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectanOption%>'></asp:Localize>
                        </div>
                        <div>
                            <asp:RadioButton ID="RadioProductCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonCurrentImage%>' runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductCurrentImage_CheckedChanged"
                                Checked="True" /><br />
                            <asp:RadioButton ID="RadioProductNewImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNewImage%>' runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductNewImage_CheckedChanged" />
                        </div>
                    </asp:Panel>
                    <br />
                    <div id="tblProductDescription" runat="server" visible="false">
                        <div>
                            <ZNode:UploadImage ID="uxProductImage" runat="server"></ZNode:UploadImage>
                        </div>
                        <asp:Label runat="server" CssClass="Error" ForeColor="Red" ID="lblProductImageError"
                            Visible="False"></asp:Label>
                    </div>
                    <asp:Panel ID="pnlImageName" runat="server" Visible="false">
                        <div class="FieldStyle">
                            <asp:Localize runat="server" ID="ColumnImageFileName" Text='<%$ Resources:ZnodeAdminResource, ColumnImageFileName%>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtimagename" ToolTip='<%$ Resources:ZnodeAdminResource, ColumnImageFileName%>' runat="server"></asp:TextBox>
                        </div>
                    </asp:Panel>
                </div>
            </div>

            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="SubTitleSettings" Text='<%$ Resources:ZnodeAdminResource, SubTitleShippingSettings%>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnWeight" Text='<%$ Resources:ZnodeAdminResource, ColumnWeight%>'></asp:Localize>
                <br />
                <small>
                    <asp:Localize runat="server" ID="HintTextSettings" Text='<%$ Resources:ZnodeAdminResource, HintTextSettings%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtproductshipweight" runat="server" Width="46px" MaxLength="7"> </asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.WeightUnit %>
                <asp:RangeValidator Enabled="false" ID="RangeValidator1" runat="server"
                    ControlToValidate="txtproductshipweight" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeProductWeight%>'
                    MaximumValue="999999" MinimumValue="0.1" CultureInvariantValues="true" Type="Double"></asp:RangeValidator>
                <asp:RequiredFieldValidator Enabled="false" ID="RequiredFieldValidator4" runat="server"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductWeight%>' ControlToValidate="txtproductshipweight"
                    CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                <div>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtproductshipweight"
                        Type="Currency" Operator="DataTypeCheck" ErrorMessage='' CssClass="Error" Display="Dynamic" />
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnHeight" Text='<%$ Resources:ZnodeAdminResource, ColumnHeight%>'></asp:Localize><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextSettings1" Text='<%$ Resources:ZnodeAdminResource, HintTextSettings%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductHeight" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtProductHeight"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareProductWeight%>'
                    CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleWidth" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleWidth%>'></asp:Localize><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextSettings2" Text='<%$ Resources:ZnodeAdminResource, HintTextSettings%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductWidth" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="txtProductWidth"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareProductWeight%>'
                    CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnLength" Text='<%$ Resources:ZnodeAdminResource, ColumnLength%>'></asp:Localize><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextSettings3" Text='<%$ Resources:ZnodeAdminResource, HintTextSettings%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductLength" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="txtProductLength"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareProductWeight%>'
                    CssClass="Error" Display="Dynamic" />
            </div>


            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="SubTitleInventory" Text='<%$ Resources:ZnodeAdminResource, SubTitleInventoryOption%>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleInventory" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleUpdateInventory%>'></asp:Localize><span class="Asterix">*</span><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextInventory" Text='<%$ Resources:ZnodeAdminResource, HintTextUpdateInventory%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductQuantity" ToolTip='<%$ Resources:ZnodeAdminResource, ColumnTitleQuantityOnHand%>' runat="server" Rows="3">999</asp:TextBox>
                <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtProductQuantity"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber%>' MaximumValue="999999"
                    MinimumValue="-999999" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtProductQuantity"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductQuantity%>'></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnOutOfStock" Text='<%$ Resources:ZnodeAdminResource, ColumnOutOfStockOptions%>'></asp:Localize><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextOutOfStock" Text='<%$ Resources:ZnodeAdminResource, HintTextOutOfStock%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:RadioButtonList ID="InvSettingRadiobtnList" runat="server">
                    <asp:ListItem Value="1" Selected="True" Text='<%$ Resources:ZnodeAdminResource, RadioButtonDisableOutofstockProducts %>'></asp:ListItem>
                    <asp:ListItem Value="2" Text='<%$ Resources:ZnodeAdminResource, RadioButtonAllowBackOrderProducts %>'></asp:ListItem>
                    <asp:ListItem Value="3" Text='<%$ Resources:ZnodeAdminResource, RadioButtonDontTrackInventory %>'></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div visible="false" runat="server">
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ColumnInStockMessage" Text='<%$ Resources:ZnodeAdminResource, ColumnInStockMessage%>'></asp:Localize><br />
                    <small>
                        <asp:Localize runat="server" ID="HintTextCustomMessage" Text='<%$ Resources:ZnodeAdminResource, HintTextCustomMessage%>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtInStockMsg" ToolTip="In Stock Message" runat="server"></asp:TextBox>
                </div>
            </div>
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnOutOfStockMessage" Text='<%$ Resources:ZnodeAdminResource, ColumnOutOfStockMessage%>'></asp:Localize><br />
                <small>
                    <asp:Localize runat="server" ID="HintTextStockMessage" Text='<%$ Resources:ZnodeAdminResource, HintTextOutofStockMessage%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtOutofStock" ToolTip='<%$ Resources:ZnodeAdminResource, ColumnOutOfStockMessage%>' runat="server" Text='<%$ Resources:ZnodeAdminResource, TextBoxOutofStock%>'></asp:TextBox>
            </div>
            <div visible="false" runat="server">
                <h4 class="SubTitle">
                    <asp:Localize runat="server" ID="SubTitleCheckout" Text='<%$ Resources:ZnodeAdminResource, SubTitleCheckoutOptions%>'></asp:Localize></h4>
                <div class="FieldStyle">
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID="chkShipInd" ToolTip='<%$ Resources:ZnodeAdminResource, CheckBoxTangible%>' Text='<%$ Resources:ZnodeAdminResource, CheckBoxTangible%>' runat="server" />
                </div>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ColumnWeight1" Text='<%$ Resources:ZnodeAdminResource, ColumnWeight%>'></asp:Localize><br />
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtProductWeight" ToolTip='<%$ Resources:ZnodeAdminResource, ColumnWeight%>' Enabled="false" runat="server"
                        Width="46px" MaxLength="7"> </asp:TextBox>&nbsp;<asp:RangeValidator Enabled="false"
                            ID="weightBasedRangeValidator" runat="server" ControlToValidate="txtProductWeight"
                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeProductWeight%>'
                            MaximumValue="999999" MinimumValue="0.1" CultureInvariantValues="true" Type="Double"></asp:RangeValidator><asp:RequiredFieldValidator
                                Enabled="false" ID="RequiredForWeightBasedoption" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEnterWeight%>'
                                ControlToValidate="txtProductWeight" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator><div>
                                    <asp:CompareValidator ID="WeightFieldCompareValidator" runat="server" ControlToValidate="txtProductWeight"
                                        Type="Currency" Operator="DataTypeCheck" ErrorMessage='' CssClass="Error" Display="Dynamic" />
                                </div>
                </div>
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ColumnTitleHandling" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleHandling%>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtHandling" runat="server" Enabled="false" ToolTip="Handling" Width="46px"></asp:TextBox>&nbsp;&nbsp;&nbsp;<asp:CompareValidator
                        ID="CompareValidator5" runat="server" ControlToValidate="txtHandling" Type="Currency"
                        Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareProductHandling%>'
                        CssClass="Error" Display="Dynamic" />
                </div>
                <div class="FieldStyle">
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID="chkRecurringInd" ToolTip='<%$ Resources:ZnodeAdminResource, CheckBoxRecurring%>' Text='<%$ Resources:ZnodeAdminResource, CheckBoxRecurring%>'
                        runat="server" />
                </div>
                <div id="divRecurringBilling">
                    <div class="FieldStyle">
                        <asp:Localize runat="server" ID="ColumnTitleStartupFee" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStartupFee%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtStartupfee" Enabled="false" ToolTip='<%$ Resources:ZnodeAdminResource, ColumnTitleStartupFee%>' runat="server"
                            MaxLength="7"></asp:TextBox>
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize runat="server" ID="ColumnTitleBillEvery" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBillEvery%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtBillevery" Enabled="false" runat="server" MaxLength="7"></asp:TextBox>
                        <asp:DropDownList ID="ddlBillEvery" Enabled="false" ToolTip='<%$ Resources:ZnodeAdminResource, ColumnTitleBillEvery%>' runat="server">
                            <asp:ListItem Value="Day" Text='<%$ Resources:ZnodeAdminResource, DropDownTextDay%>'></asp:ListItem>
                            <asp:ListItem Value="Week" Text='<%$ Resources:ZnodeAdminResource, DropDownTextWeek%>'></asp:ListItem>
                            <asp:ListItem Value="Month" Text='<%$ Resources:ZnodeAdminResource, DropDownTextMonth%>' Selected="True"></asp:ListItem>
                            <asp:ListItem Value="Year" Text='<%$ Resources:ZnodeAdminResource, DropDownTextYear%>'></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize runat="server" ID="ColumnTitleBilling" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBilling%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtContinueBill" Enabled="false" ToolTip='<%$ Resources:ZnodeAdminResource, ColumnTitleBilling%>' runat="server"
                            MaxLength="7"></asp:TextBox>
                    </div>
                </div>
            </div>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="2" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div runat="server" id="dvBottomButtons">
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnBottomCancel" />
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnNext_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnBottomSubmit" />
        </div>
    </div>

    <asp:HiddenField runat="server" ID="hdnDirtyFields" />
    <asp:HiddenField runat="server" ID="hdnDirtyFieldsOrg" />
    <script language="javascript" type="text/javascript">

        function SetDirtyHiddenMsgWithID(id, message) {

            if (document.getElementById(id).value.indexOf('~' + message + '~') == -1)
                //When the user change something, we will log an ID in the hidden field
                document.getElementById(id).value += '~' + message + '~';
        }

        function setTangibleSectionEnabled() {

            var chkShipInd = document.getElementById('<%=chkShipInd.ClientID %>');
            var txtProductWeight = document.getElementById('<%=txtProductWeight.ClientID %>');
            var txtHandling = document.getElementById('<%=txtHandling.ClientID %>');
            txtProductWeight.disabled = !chkShipInd.checked;
            txtHandling.disabled = !chkShipInd.checked;
            if (chkShipInd.checked) txtProductWeight.focus();

        }

        function setRecurringSectionEnabled() {

            var chkRecurringInd = document.getElementById('<%=chkRecurringInd.ClientID %>');
            var txtStartupfee = document.getElementById('<%=txtStartupfee.ClientID %>');
            var txtBillevery = document.getElementById('<%=txtBillevery.ClientID %>');
            var ddlBillEvery = document.getElementById('<%=ddlBillEvery.ClientID %>');
            var txtContinueBill = document.getElementById('<%=txtContinueBill.ClientID %>');

            txtStartupfee.disabled = !chkRecurringInd.checked;
            txtBillevery.disabled = !chkRecurringInd.checked;
            ddlBillEvery.disabled = !chkRecurringInd.checked;
            txtContinueBill.disabled = !chkRecurringInd.checked;
            if (chkRecurringInd.checked) txtStartupfee.focus();

        }

    </script>

</asp:Content>
