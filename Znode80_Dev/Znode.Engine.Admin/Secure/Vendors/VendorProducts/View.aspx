<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Vendors.VendorProducts.View" Title="Manage Vendor Products - View" CodeBehind="View.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="AlternateImage" Src="~/Controls/Default/AlternateImage.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Marketing" Src="~/Controls/Default/Marketing.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Faceting" Src="~/Controls/Default/Faceting.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Options" Src="~/Controls/Default/AddOn.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Product" Src="~/Secure/Vendors/VendorProducts/Product.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ReviewStateLegend" Src="~/Controls/Default/ReviewStateLegend.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 80%; height: inherit;">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div class="LeftFloat" style="width: 20%;" align="right">
            <zn:Button runat="server" ButtonType="EditButton" Width="100px" OnClick="BtnBack_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonBack%>' ID="btnBack" CausesValidation="False" />
        </div>
    </div>
    <div class="ClearBoth">
    </div>
    <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    <div class="Display">
        <div class="ValueStyle">
            <asp:Label ID="VendorProductID" runat="server"></asp:Label>
            <br />
            <asp:Label ID="VendorName" runat="server" Text="Vendor: {0}"></asp:Label>

        </div>
    </div>
    <div class="ClearBoth">
    </div>
    <br />
    <div>
        <ajaxToolKit:TabContainer ID="tabProductDetails" runat="server">
            <ajaxToolKit:TabPanel ID="pnlGeneral" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="ColumnPostalCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleProductInfo%>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <asp:UpdatePanel ID="upProductInfo" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div>
                                <div>
                                    <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
                                </div>
                                <div>
                                    <uc1:Spacer ID="Spacer7" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                                </div>
                                <div align="left">
                                    <zn:Button runat="server" ButtonType="EditButton" Width="100px" OnClick="BtnTopEditProduct_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonEdit%>' ID="btnTopEditProduct" />
                                    <zn:Button runat="server" ButtonType="EditButton" Width="100px" OnClick="BtnReject_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDecline%>' ID="btnTopReject" />
                                    <zn:Button runat="server" ButtonType="EditButton" Width="100px" OnClick="BtnAccept_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonApprove%>' ID="btnTopAccept" />
                                </div>
                                <br />
                                <h4 class="SubTitle">
                                    <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitlePortalDisplay%>'></asp:Localize>
                                </h4>
                                <div class="Display">
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnProductName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductName%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblProdName" runat="server"></asp:Label>
                                        &#160;
                                    </div>
                                    <div class="MainStyleA">
                                        <div class="FieldStyleA">
                                            <asp:Localize ID="ColumnProductDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductDescription%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyleA">
                                            <asp:Label ID="lblProductDescription" runat="server"></asp:Label>
                                            &#160;
                                        </div>
                                    </div>
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnProductDetails" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductDetails%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblProductDetails" runat="server"></asp:Label>
                                        &#160;
                                    </div>
                                    <div class="MainStyleA">
                                        <div class="FieldStyleA">
                                            <asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingInformation%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyleA">
                                            <asp:Label ID="lblShippingDescription" runat="server"></asp:Label>
                                            &#160;
                                        </div>
                                    </div>
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnProductImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductImage%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Image ID="ItemImage" runat="server" />
                                        &#160;
                                    </div>
                                    <div class="FieldStyleA">
                                        <asp:Localize ID="ColumnSellingPrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSellingPrice%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Label ID="lblRetailPrice" runat="server" CssClass="Price"></asp:Label>
                                        &#160;
                                    </div>
                                    <div style="display: none">
                                        <div class="FieldStyle">
                                            <asp:Localize ID="ColumnMSRP" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnMSRP%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyle">
                                            <asp:Label ID="lblSalePrice" runat="server" CssClass="Price"></asp:Label>
                                            &#160;
                                        </div>
                                    </div>
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnShowProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShowProduct%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Image ID="imgShowProduct" runat="server" />
                                    </div>
                                    <div class="FieldStyleA" style="height: 20px;">
                                        <asp:Localize ID="ColumnPurchaseonMall" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPurchaseonMall%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA" style="height: 20px;">
                                        <asp:Image ID="imgEnablePurchaseOnPortal" runat="server" />
                                    </div>
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnAffiliateUrl" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAffiliateUrl%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblAffiliateUrl" runat="server"></asp:Label>
                                        &#160;
                                    </div>
                                    <div class="FieldStyleA">
                                        <asp:Localize ID="ColumnCategories" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCategories%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA" style="overflow: visible; height: auto;">
                                        <div class="TreeFieldStyle" style="width: 300px;">
                                            <asp:TreeView ID="CategoryTreeView" runat="server" Enabled="False" ImageSet="Arrows"
                                                ShowCheckBoxes="All" CssClass="TreeView" ShowLines="True" ExpandDepth="0">
                                                <ParentNodeStyle CssClass="ParentNodeStyle" Font-Bold="False" />
                                                <HoverNodeStyle CssClass="HoverNodeStyle" Font-Underline="False" />
                                                <SelectedNodeStyle CssClass="SelectedNodeStyle" Font-Underline="False"
                                                    HorizontalPadding="0px" VerticalPadding="0px" />
                                                <RootNodeStyle CssClass="RootNodeStyle" />
                                                <LeafNodeStyle CssClass="LeafNodeStyle" />
                                                <NodeStyle CssClass="NodeStyle" Font-Names="Verdana" Font-Size="8pt" ForeColor="Black"
                                                    HorizontalPadding="0px" NodeSpacing="0px" VerticalPadding="0px" />
                                            </asp:TreeView>
                                        </div>
                                        <div class="ValueStyle" style="width: 300px;">
                                            <asp:Label ID="lblCategories" runat="server"></asp:Label>
                                        </div>
                                        &#160;
                                    </div>
                                    <h4 class="SubTitle">
                                        <asp:Localize ID="TabTitleGeneral" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleShippingSettings%>'></asp:Localize></h4>
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnWeight" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWeight%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblshipweight" runat="server"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyleA">
                                        <asp:Localize ID="ColumnHeight" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnHeight%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Label ID="lblHeight" runat="server"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnWidth" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWidth%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblWidth" runat="server"></asp:Label>&nbsp;
                                    </div>
                                    <div class="FieldStyleA">
                                        <asp:Localize ID="ColumnLength" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLength%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Label ID="lblLength" runat="server"></asp:Label>&nbsp;
                                    </div>
                                    <div class="ClearBoth">
                                        <br />
                                    </div>
                                    <h4 class="SubTitle">
                                        <asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleInventoryOptions%>'></asp:Localize></h4>
                                    <div class="FieldStyle">
                                        <asp:Localize ID="ColumnQuantityOnHand" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnQuantityOnHand%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Label ID="lblQuantityOnHand" runat="server" CssClass="Price"></asp:Label>
                                    </div>
                                    <div class="FieldStyleA" style="height: 20px;">
                                        <asp:Image ID="imgTrackInventory" runat="server" />
                                        &#160;
                                    </div>
                                    <div class="ValueStyleA" style="height: 20px;">
                                        <asp:Localize ID="Localize5" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTrackInventory%>'></asp:Localize>
                                        &#160;
                                    </div>

                                    <div class="FieldStyle">
                                        <asp:Image ID="imgAllowBackOrder" runat="server" />
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Localize ID="ColumnBackOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBackOrder%>'></asp:Localize>
                                        &#160;
                                    </div>

                                    <div class="FieldStyle">
                                        <asp:Image ID="imgDontTrackInventory" runat="server" />
                                    </div>
                                    <div class="ValueStyle">
                                        <asp:Localize ID="ColumnNoTrackInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnNoTrackInventory%>'></asp:Localize>
                                        &#160;
                                    </div>
                                    <div class="ClearBoth">
                                        <br />
                                    </div>
                                    <div style="display: none">
                                        <div class="FieldStyleA">
                                            <asp:Localize ID="ColumnInStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnInStockMessage%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyleA">
                                            <asp:Label ID="lblInStockMessage" runat="server"></asp:Label>
                                            &#160;
                                        </div>
                                    </div>
                                    <div class="FieldStyleA">
                                        <asp:Localize ID="ColumnOutOfStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOutOfStockMessage%>'></asp:Localize>
                                        &#160;
                                    </div>
                                    <div class="ValueStyleA">
                                        <asp:Label ID="lblOutofStockMessage" runat="server"></asp:Label>
                                        &#160;
                                    </div>
                                    <div class="FieldStyle" style="display: none;">
                                        <asp:Localize ID="ColumnBackOrderMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBackOrderMessage%>'></asp:Localize>
                                    </div>
                                    <div class="ValueStyle" style="display: none;">
                                        <asp:Label ID="lblBackOrderMessage" runat="server"></asp:Label>
                                        &#160;
                                    </div>
                                    <div class="ClearBoth">
                                        <br />
                                    </div>
                                    <div style="display: none">
                                        <h4 class="SubTitle">
                                            <asp:Localize ID="SubTitleCheckoutOptions" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCheckoutOptions%>'></asp:Localize></h4>
                                        <div class="FieldStyle">
                                            <asp:Localize ID="ColumnTangible" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTangible%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyle">
                                            <img id="imgTangible" runat="server" />
                                            &#160;
                                        </div>
                                        <div class="ClearBoth">
                                            <br />
                                        </div>
                                        <div class="FieldStyleA">
                                            <asp:Localize ID="Localize4" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWeight%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyleA">
                                            <asp:Label ID="lblWeight" runat="server"></asp:Label>
                                            &#160;
                                        </div>
                                        <div class="FieldStyle">
                                            <asp:Localize ID="Localize6" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnHandling%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyle">
                                            <asp:Label ID="lblHandling" runat="server" CssClass="Price"></asp:Label>
                                            &#160;
                                        </div>
                                        <div class="FieldStyleA">
                                            <asp:Localize ID="Localize7" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRecurrring%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyleA">
                                            <img id="imgRecurring" runat="server" />
                                            &#160;
                                        </div>
                                        <div class="FieldStyle">
                                            <asp:Localize ID="Localize8" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnStartupFee%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyle">
                                            <asp:Label ID="lblStartupFee" runat="server"></asp:Label>
                                            &#160;
                                        </div>
                                        <div class="FieldStyleA">
                                            <asp:Localize ID="ColumnBillEvery" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBillEvery%>'></asp:Localize>
                                        </div>
                                        <div class="ValueStyleA">
                                            <asp:Label ID="lblBillingPeriod" runat="server"></asp:Label>
                                            &#160;
                                        </div>
                                    </div>
                                    <div class="ClearBoth">
                                        <br />
                                    </div>
                                    <div runat="server" id="divReviewHistory">
                                        <h4 class="SubTitle">
                                            <asp:Localize ID="SubTitleHistory" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleHistory%>'></asp:Localize></h4>
                                        <div>
                                            <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                                        </div>
                                        <div>
                                            <asp:GridView ID="gvReviewHistory" ShowFooter="True" CaptionAlign="Left" runat="server"
                                                ForeColor="Black" CellPadding="4" AutoGenerateColumns="False" CssClass="Grid"
                                                Width="100%" GridLines="None" AllowPaging="True" OnRowCommand="GvReviewHistory_RowCommand"
                                                OnPageIndexChanging="GvReviewHistory_PageIndexChanging">
                                                <Columns>
                                                    <asp:BoundField DataField="LogDate" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDate%>'>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="UserName" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleUser%>'>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Status" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus%>'>
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnRejectedReason%>' HeaderStyle-HorizontalAlign="Left">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblReason" runat="server" Text='<%# FormatReason(DataBinder.Eval(Container.DataItem,"Reason")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:LinkButton CssClass="Button" ID="ViewReviewDetails" Text="View" CommandArgument='<%# Eval("ProductReviewHistoryID") %>'
                                                                CommandName="View" runat="Server" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <EmptyDataTemplate>
                                                    <asp:Localize ID="RecordNotFoundReviewHistory" runat="server" Text='<%$ Resources:ZnodeAdminResource, RecordNotFoundReviewHistory%>'></asp:Localize>
                                                </EmptyDataTemplate>
                                                <RowStyle CssClass="RowStyle" />
                                                <HeaderStyle CssClass="HeaderStyle" />
                                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                                <FooterStyle CssClass="FooterStyle" />
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <div class="ClearBoth">
                                    <br />
                                </div>
                                <div align="left">
                                    <zn:Button runat="server" ButtonType="EditButton" Width="100px" OnClick="BtnTopEditProduct_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonEdit%>' ID="btnBottomEditProduct" />
                                    <zn:Button runat="server" ButtonType="EditButton" Width="100px" OnClick="BtnReject_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDecline%>' ID="btnBottomReject" />
                                    <zn:Button runat="server" ButtonType="EditButton" Width="100px" OnClick="BtnAccept_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonApprove%>' ID="btnBottomAccept" />
                                </div>
                                <br />
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlAlternateImages" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="TabTileAlternateImages" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTileAlternateImages%>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <div>
                        <uc1:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <h4 class="SubTitle">
                        <asp:Localize ID="Localize9" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleAlternateImages%>'></asp:Localize></h4>
                    <div style="margin-top: -15px;">
                        <uc1:Spacer ID="Spacer1" SpacerHeight="2" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <asp:UpdatePanel ID="updPnlGridThumb" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <ZNode:AlternateImage ID="uxAlternateImage" UserType="Reviewer" ImageType="AlternateImage"
                                runat="server"></ZNode:AlternateImage>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div>
                        <uc1:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <h4 class="SubTitle">
                        <asp:Localize ID="Localize10" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleColorSwatches%>'></asp:Localize></h4>
                    <div style="margin-top: -15px;">
                        <uc1:Spacer ID="Spacer4" SpacerHeight="2" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <ZNode:AlternateImage ID="uxSwatchImages" UserType="Reviewer" ImageType="Swatch"
                                runat="server"></ZNode:AlternateImage>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                    <div style="float: right; padding: 2px;">
                        <ZNode:ReviewStateLegend ID="uxReviewStateLegend" runat="server" />
                    </div>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlProductOptions" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="Localize11" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleAddOns%>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <div style="margin-top: -5px;">
                        <%--<uc1:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>--%>
                    </div>
                    <h4 class="SubTitle">
                        <asp:Localize ID="Localize12" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleProductAddOns%>'></asp:Localize></h4>
                    <p style="display: none">
                        <asp:Localize ID="SubTextProductAddOns" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextProductAddOns%>'></asp:Localize>
                    </p>
                    <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                    <asp:UpdatePanel ID="updPnlProductAddOnsGrid" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <%-- Place Add On User control here --%>
                            <ZNode:Options ID="Options1" runat="server" UserType="Reviewer"></ZNode:Options>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="tabpnlTags" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="Localize13" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleFacets%>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <asp:UpdatePanel ID="updPnlProductTagging" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <ZNode:Faceting runat="server" ID="Tagging" UserType="Reviewer"></ZNode:Faceting>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="pnlMarketing" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="Localize14" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleMarketing%>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <div>
                        <uc1:Spacer ID="Spacer20" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <ZNode:Marketing runat="server" ID="Marketing"></ZNode:Marketing>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
            <ajaxToolKit:TabPanel ID="tabPreview" runat="server">
                <HeaderTemplate>
                    <asp:Localize ID="Localize15" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitlePreview%>'></asp:Localize>
                </HeaderTemplate>
                <ContentTemplate>
                    <div>
                        <uc1:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <ZNode:Product runat="server" ID="uxProduct"></ZNode:Product>
                </ContentTemplate>
            </ajaxToolKit:TabPanel>
        </ajaxToolKit:TabContainer>
    </div>
</asp:Content>
