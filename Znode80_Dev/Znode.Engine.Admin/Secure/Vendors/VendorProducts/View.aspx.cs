﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.MobileControls;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Vendors.VendorProducts
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_Reviewer_View class
    /// </summary>
    public partial class View : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId;
        private string Mode = string.Empty;
        private string EditPageLink = "Add.aspx?itemid=";
        private string ListLink = "~/Secure/Vendors/VendorProducts/Default.aspx";
        private string ViewProductReviewLink = "~/Secure/Vendors/VendorProducts/ViewProductReview.aspx?itemid=";
        private string RejectProductReviewLink = "~/Secure/Vendors/VendorProducts/RejectProduct.aspx?itemid=";
        private string AcceptProductLink = "~/Secure/Vendors/VendorProducts/AcceptProduct.aspx?itemid=";
        private StringBuilder currentCategories = new StringBuilder();
        private ZNodeImage znodeImage = new ZNodeImage();
        #endregion

        #region Public Methods

        /// <summary>
        /// Binding Product Values into label Boxes
        /// </summary>
        public void BindViewData()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin prodAdmin = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product product = prodAdmin.GetByProductId(this.ItemId);

            DataSet ds = prodAdmin.GetProductDetails(this.ItemId);

            // Check for Number of Rows
            if (ds.Tables[0].Rows.Count != 0)
            {
                // Checking the user profile for roles and permission and then loading the data based on roles
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (!UserStoreAccess.CheckUserRoleInProduct(profiles, this.ItemId))
                {
                    Response.Redirect("Default.aspx", true);
                }
            }

            if (product != null)
            {
                VendorProductID.Text = Server.HtmlDecode(product.ProductNum);

                ZNode.Libraries.DataAccess.Service.SupplierService supplierService = new ZNode.Libraries.DataAccess.Service.SupplierService();
                Supplier supplier = supplierService.GetBySupplierID(product.SupplierID.GetValueOrDefault(0));

                // Get Vendor Name
                AccountAdmin accountAdmin = new AccountAdmin();
                PortalAdmin portalAdmin = new PortalAdmin();

                // For Franchise Admin
                if (!string.IsNullOrEmpty(product.PortalID.ToString()))
                {
                    Portal portal = portalAdmin.GetByPortalId(Convert.ToInt32(product.PortalID));
                    VendorName.Text = string.Format(VendorName.Text, portal.CompanyName);
                }

                // For Mall Admin
                else if (!string.IsNullOrEmpty(product.AccountID.ToString()))
                {
                    Address address = accountAdmin.GetDefaultBillingAddress(Convert.ToInt32(product.AccountID));
                    VendorName.Text = string.Format(VendorName.Text, address.FirstName + " " + address.LastName + " " + address.CompanyName);
                }
                else
                {
                    VendorName.Visible = false;
                }


                // General Information
                lblProdName.Text = product.Name;
                lblProductDescription.Text = Server.HtmlDecode(product.Description);
                lblProductDetails.Text = Server.HtmlDecode(product.FeaturesDesc);
                lblShippingDescription.Text = Server.HtmlDecode(product.AdditionalInformation);
                ItemImage.ImageUrl = this.znodeImage.GetImageHttpPathMedium(product.ImageFile);

                // Product Attributes
                if (product.RetailPrice.HasValue)
                {
                    lblRetailPrice.Text = this.FormatPrice(product.RetailPrice);
                }

                if (product.SalePrice.HasValue)
                {
                    lblSalePrice.Text = this.FormatPrice(product.SalePrice);
                }

                lblshipweight.Text = this.FormatProductWeight(product.Weight);

                if (product.Height.HasValue)
                {
                    lblHeight.Text = product.Height.Value.ToString("N2") + " " + ZNodeConfigManager.SiteConfig.DimensionUnit;
                }

                if (product.Width.HasValue)
                {
                    lblWidth.Text = product.Width.Value.ToString("N2") + " " + ZNodeConfigManager.SiteConfig.DimensionUnit;
                }

                if (product.Length.HasValue)
                {
                    lblLength.Text = product.Length.Value.ToString("N2") + " " + ZNodeConfigManager.SiteConfig.DimensionUnit;
                }

                imgShowProduct.ImageUrl = ZNode.Libraries.Admin.Helper.GetCheckMark(product.ActiveInd);
                imgEnablePurchaseOnPortal.ImageUrl = ZNode.Libraries.Admin.Helper.GetCheckMark(this.DisplayVisible(product.InventoryDisplay));
                if (!string.IsNullOrEmpty(product.AffiliateUrl))
                {
                    lblAffiliateUrl.Text = product.AffiliateUrl.ToString();
                }
                else
                {
                    lblAffiliateUrl.Text = string.Empty;
                }

                // Bind Inventory Options
                SKUAdmin skuAdmin = new SKUAdmin();
                TList<SKU> skuList = skuAdmin.GetByProductID(product.ProductID);

                if (skuList != null && skuList.Count > 0)
                {
                    SKUInventory skuInventory = skuAdmin.GetSkuInventoryBySKU(skuList[0].SKU);

                    if (skuInventory != null)
                    {
                        lblQuantityOnHand.Text = skuInventory.QuantityOnHand.GetValueOrDefault(0).ToString();
                        VendorProductID.Text = skuInventory.SKU;
                    }

                    // Release the Resources
                    skuList.Dispose();
                }

                // Inventory Options
                if (product.TrackInventoryInd.HasValue && product.AllowBackOrder.HasValue)
                {
                    if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value == false)
                    {
                        imgTrackInventory.ImageUrl = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                        imgAllowBackOrder.ImageUrl = this.SetCheckMarkImage();
                        imgDontTrackInventory.ImageUrl = this.SetCheckMarkImage();
                    }
                    else if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value)
                    {
                        imgTrackInventory.ImageUrl = this.SetCheckMarkImage();
                        imgAllowBackOrder.ImageUrl = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                        imgDontTrackInventory.ImageUrl = this.SetCheckMarkImage();
                    }
                    else if ((product.TrackInventoryInd.Value == false) && (product.AllowBackOrder.Value == false))
                    {
                        imgTrackInventory.ImageUrl = this.SetCheckMarkImage();
                        imgAllowBackOrder.ImageUrl = this.SetCheckMarkImage();
                        imgDontTrackInventory.ImageUrl = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                    }
                }
                else
                {
                    imgTrackInventory.ImageUrl = this.SetCheckMarkImage();
                    imgAllowBackOrder.ImageUrl = this.SetCheckMarkImage();
                    imgDontTrackInventory.ImageUrl = ZNode.Libraries.Admin.Helper.GetCheckMark(true);
                }

                // Checkout Optons
                imgTangible.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(Convert.ToBoolean(product.FreeShippingInd));
                lblWeight.Text = this.FormatProductWeight(product.Weight);
                lblHandling.Text = this.FormatPrice(product.ShippingRate);
                imgRecurring.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(product.RecurringBillingInd);
                lblStartupFee.Text = this.FormatPrice(product.RecurringBillingInitialAmount);
                if (product.RecurringBillingInd)
                {
                    lblBillingPeriod.Text = product.RecurringBillingPeriod;
                }

                // Inventory Setting - Stock Messages
                lblInStockMessage.Text = product.InStockMsg;
                lblOutofStockMessage.Text = product.OutOfStockMsg;
                lblBackOrderMessage.Text = product.BackOrderMsg;

                ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
                DataTable productCategoryDt = productCategoryAdmin.GetByProductID(this.ItemId).Tables[0];

                List<string> categoryList = new List<string>();
                foreach (DataRow row in productCategoryDt.Rows)
                {
                    categoryList.Add(row["CategoryID"].ToString());
                }

                // Bind Categories.
                this.PopulateAdminTreeView(categoryList);

                // Bind the review history.
                this.BindReviewHistory();

                // Set the buttons enabled state.
                if (product.ReviewStateID != null)
                {
                    // Product already approved state. Disable the Approve button
                    if (Convert.ToInt32(product.ReviewStateID) == (int)ZNodeProductReviewState.Approved)
                    {
                        btnTopAccept.Enabled = btnBottomAccept.Enabled = false;
                    }

                    // Product already declined state. Disable the Reject button
                    if (Convert.ToInt32(product.ReviewStateID) == (int)ZNodeProductReviewState.Declined)
                    {
                        btnTopReject.Enabled = btnBottomReject.Enabled = false;
                    }
                }
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductNotFound").ToString());
            }
        }

	    /// <summary>
	    /// Populates a treeview with store categories
	    /// </summary>
	    /// <param name="categoryList">The value of TList CategoryList</param>
	    public void PopulateAdminTreeView(List<string> categoryList)
	    {
		    ProductAdmin productAdmin = new ProductAdmin();
		    ZNode.Libraries.DataAccess.Entities.Product product = new ZNode.Libraries.DataAccess.Entities.Product();
		    product = productAdmin.GetByProductId(this.ItemId);
			int portalId = product.PortalID == null ? ZNodeConfigManager.SiteConfig.PortalID : Convert.ToInt32(product.PortalID) ;


			CategoryHelper categoryHelper = new CategoryHelper();
			System.Data.DataSet ds = categoryHelper.GetNavigationItems(portalId, ZNodeConfigManager.SiteConfig.LocaleID);

	       // Add the hierarchical relationship to the dataset
            ds.Relations.Add("NodeRelation", ds.Tables[0].Columns["CategoryNodeId"], ds.Tables[0].Columns["ParentCategoryNodeId"], false);

            foreach (DataRow dataRow in ds.Tables[0].Rows)
            {
                if (dataRow.IsNull("ParentCategoryNodeID"))
                {
                    if ((bool)dataRow["VisibleInd"])
                    {
                        // Create new tree node
                        TreeNode tn = new TreeNode();
                        tn.Text = dataRow["Name"].ToString();
                        tn.Value = dataRow["CategoryId"].ToString();

                        string categoryId = dataRow["CategoryId"].ToString();

                        // Add Root Node to Category Tree view
                        CategoryTreeView.Nodes.Add(tn);

                        if (categoryList.Contains(categoryId))
                        {
                            tn.Selected = true;
                            tn.Checked = true;

                            tn.Expand();

                            this.RecursivelyExpandTreeView(tn);
                        }

                        this.RecursivelyPopulateTreeView(dataRow, tn, categoryList);
                    }
                }
            }
        }

        /// <summary>
        /// Returns a Format Weight string
        /// </summary>
        /// <param name="fieldValue">The value of fieldValue</param>
        /// <returns>Returns the Formatted Weight</returns>
        public string FormatProductWeight(object fieldValue)
        {
            if (fieldValue == null)
            {
                return String.Empty;
            }
            else
            {
                if (Convert.ToDecimal(fieldValue.ToString()) == 0)
                {
                    return string.Empty;
                }
                else
                {
                    return fieldValue.ToString() + " " + ZNodeConfigManager.SiteConfig.WeightUnit;
                }
            }
        }

        /// <summary>
        /// Format the Price of a Product
        /// </summary>
        /// <param name="fieldValue">The value of fieldvalue</param>
        /// <returns>Returns the formatted price of the product</returns>
        public string FormatPrice(object fieldValue)
        {
            if (fieldValue == null)
            {
                return String.Empty;
            }
            else
            {
                if (Convert.ToDecimal(fieldValue) == 0)
                {
                    return String.Empty;
                }
                else
                {
                    return String.Format("{0:c}", fieldValue);
                }
            }
        }

        /// <summary>
        /// Validate for Null Values and return a Boolean Value
        /// </summary>
        /// <param name="fieldvalue">The value of fieldvalue</param>
        /// <returns>Returns bool value true or false</returns>
        public bool DisplayVisible(object fieldvalue)
        {
            if (fieldvalue == DBNull.Value)
            {
                return false;
            }
            else
            {
                if (Convert.ToInt32(fieldvalue) == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        /// <summary>
        /// Gets the Name of the Profile for this ProfileID
        /// </summary>
        /// <param name="profileId">The value of profileID</param>
        /// <returns>Returns the Profile Name</returns>
        public string GetProfileName(object profileId)
        {
            ProfileAdmin AdminAccess = new ProfileAdmin();
            if (profileId == null)
            {
                return "All Profile";
            }
            else
            {
                Profile profile = AdminAccess.GetByProfileID(int.Parse(profileId.ToString()));

                if (profile != null)
                {
                    return profile.Name;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Return cross-mark image path
        /// </summary>
        /// <returns>Returns the Cross Mark Image path</returns>
        public string SetCheckMarkImage()
        {
            return ZNode.Libraries.Admin.Helper.GetCheckMark(false);
        }

        /// <summary>
        /// Get Sku Quantity Method
        /// </summary>
        /// <param name="skuid">The value of skuid</param>
        /// <returns>Returns the Sku Quantity</returns>
        public string GetSkuQuantity(int skuid)
        {
            ZNode.Libraries.DataAccess.Service.SKUService ss = new ZNode.Libraries.DataAccess.Service.SKUService();
            return SKUAdmin.GetQuantity(ss.GetBySKUID(skuid)).ToString();
        }
        #endregion

        #region Page Load

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get mode value from querystring        
            if (Request.Params["mode"] != null)
            {
                this.Mode = Request.Params["mode"];
            }

            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                if (this.ItemId > 0)
                {
                    this.BindViewData();

                    lblTitle.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TitleVendorProduct").ToString(), lblProdName.Text.Trim());
                    VendorProductID.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TitleProductID").ToString(), Server.HtmlEncode(VendorProductID.Text.Trim()));
                }
                else
                {
                    throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductNotFound").ToString());
                }

                this.ResetTab();

                UserStoreAccess uss = new UserStoreAccess();
				if (uss.UserType == Znode.Engine.Common.ZNodeUserType.Admin || uss.UserType == Znode.Engine.Common.ZNodeUserType.Reviewer || uss.UserType == Znode.Engine.Common.ZNodeUserType.ReviewerManager)
                {
                    btnTopAccept.Visible = true;
                    btnTopReject.Visible = true;
                    btnBottomAccept.Visible = true;
                    btnBottomReject.Visible = true;
                }
                else
                {
                    // Hide the reivew history from Vendor
                    divReviewHistory.Visible = false;
                    btnTopAccept.Visible = false;
                    btnTopReject.Visible = false;
                    btnBottomAccept.Visible = false;
                    btnBottomReject.Visible = false;
                }

                Session.Remove("itemid");
            }
        }
        #endregion

        #region Protected Methods and Events

        /// <summary>
        /// Format the reason. If empty then return N/A
        /// </summary>
        /// <param name="reason">Reason object string</param>
        /// <returns>Returns the formatted reason.</returns>
        protected string FormatReason(object reason)
        {
            if (reason != null)
            {
                return reason.ToString() == string.Empty ? "N/A" : reason.ToString();
            }
            else
            {
                return "N/A";
            }
        }

        /// <summary>
        /// Redirecting to Product Edit Page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnTopEditProduct_Click(object sender, EventArgs e)
        {
            // Clear the Add Product Session
            Session["stepid"] = null;

            Response.Redirect(this.EditPageLink + this.ItemId);
        }

        /// <summary>
        /// Back Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListLink);
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GvReviewHistory_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "View")
            {
                Response.Redirect(this.ViewProductReviewLink + this.ItemId + "&historyid=" + e.CommandArgument.ToString());
            }
        }

        /// <summary>
        /// Reject Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnReject_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.RejectProductReviewLink + this.ItemId);
        }

        /// <summary>
        /// Accept Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAccept_Click(object sender, EventArgs e)
        {
            this.ApproveProduct();
        }

        /// <summary>
        /// Grid page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void GvReviewHistory_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvReviewHistory.PageIndex = e.NewPageIndex;
            this.BindReviewHistory();
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Approve the product state.
        /// </summary>
        private void ApproveProduct()
        {
            Response.Redirect(this.AcceptProductLink + this.ItemId, false);
        }

        /// <summary>
        /// This will automatically pre-select the tab according the query string value(mode)
        /// </summary>
        private void ResetTab()
        {
            int selectedIndex = 0;
            switch (this.Mode.ToLower())
            {
                case "alternateimages":
                case "1":
                    selectedIndex = 1;
                    break;

                case "options":
                case "2":
                    selectedIndex = 2;
                    break;
                case "tagging":
                case "3":
                    selectedIndex = 3;
                    break;
                case "marketing":
                case "4":
                    selectedIndex = 4;
                    break;
                default:
                    selectedIndex = 0;
                    break;
            }

            // Set the selected tab
            tabProductDetails.ActiveTabIndex = selectedIndex;
        }

        /// <summary>
        /// Recursively populate a particular node with it's children
        /// </summary>
        /// <param name="dataRow">The value of DataRow</param>
        /// <param name="parentNode">The value of parentNode</param>
        /// <param name="categoryList">The value of categoryList</param>
        private void RecursivelyPopulateTreeView(DataRow dataRow, TreeNode parentNode, List<string> categoryList)
        {
            foreach (DataRow childRow in dataRow.GetChildRows("NodeRelation"))
            {
                if ((bool)childRow["VisibleInd"])
                {
                    // Create new tree node
                    TreeNode tn = new TreeNode();
                    tn.Text = childRow["Name"].ToString();
                    tn.Value = childRow["CategoryId"].ToString();

                    string categoryId = childRow["CategoryId"].ToString();

                    parentNode.ChildNodes.Add(tn);

                    if (categoryList.Contains(categoryId))
                    {
                        tn.Checked = true;
                        tn.Expand();
                        this.RecursivelyExpandTreeView(tn);
                    }

                    this.RecursivelyPopulateTreeView(childRow, tn, categoryList);
                }
            }
        }

        /// <summary>
        /// Recursively expands parent nodes of a selected tree node
        /// </summary>
        /// <param name="nodeItem">The value of nodeItem</param>
        private void RecursivelyExpandTreeView(TreeNode nodeItem)
        {
            if (nodeItem.Parent != null)
            {
                nodeItem.Parent.ExpandAll();
                this.RecursivelyExpandTreeView(nodeItem.Parent);
            }
            else
            {
                nodeItem.Expand();
                return;
            }
        }

        /// <summary>
        /// Populates a treeview with store categories for a Product
        /// </summary>
        /// <param name="selectedCategoryId">The value of selectedCategoryId</param>
        private void PopulateEditTreeView(string selectedCategoryId)
        {
            foreach (TreeNode Tn in CategoryTreeView.Nodes)
            {
                if (Tn.Value.Equals(selectedCategoryId))
                {
                    Tn.Checked = true;
                }

                this.RecursivelyEditPopulateTreeView(Tn, selectedCategoryId);
            }
        }

        /// <summary>
        /// Recursively populate a particular node with it's children for a product
        /// </summary>
        /// <param name="childNodes">The value of childNodes</param>
        /// <param name="selectedCategoryid">The value of selectedCategoryid</param>
        private void RecursivelyEditPopulateTreeView(TreeNode childNodes, string selectedCategoryid)
        {
            foreach (TreeNode CNodes in childNodes.ChildNodes)
            {
                if (CNodes.Value.Equals(selectedCategoryid))
                {
                    CNodes.Checked = true;
                    this.RecursivelyExpandTreeView(CNodes);
                }

                this.RecursivelyEditPopulateTreeView(CNodes, selectedCategoryid);

                CategoryTreeView.Enabled = false;
            }
        }

        /// <summary>
        /// Display the selected node in label.
        /// </summary>
        private void DisplaySelectedCategories()
        {
            StringBuilder categories = new StringBuilder();
            categories.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "ColumnCurrentCategories").ToString());
            TreeNodeCollection nodeCollection = CategoryTreeView.CheckedNodes;
            if (nodeCollection.Count > 0)
            {
                foreach (TreeNode node in nodeCollection)
                {
                    categories.Append(node.Text + "<BR>");
                }
            }
            else
            {
                categories.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "ValidCategoryAssociation").ToString());
            }

            lblCategories.Text = categories.ToString();
        }

        /// <summary>
        /// Bind the review history
        /// </summary>
        private void BindReviewHistory()
        {
            // Add entry to Product Review history table.
            ProductReviewHistoryAdmin productReviewHistoryAdmin = new ProductReviewHistoryAdmin();
            TList<ProductReviewHistory> productReviewHistoryList = productReviewHistoryAdmin.GetByProductID(this.ItemId);
            productReviewHistoryList.Sort("ProductReviewHistoryID DESC");

            gvReviewHistory.DataSource = productReviewHistoryList;
            gvReviewHistory.DataBind();
        }
        #endregion
    }
}