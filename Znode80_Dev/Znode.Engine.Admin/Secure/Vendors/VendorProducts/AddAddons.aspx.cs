using System;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Vendors.VendorProducts
{
    /// <summary>
    /// Represents the SiteAdmin - Admin_Secure_Reviewer_add_addons class
    /// </summary>
    public partial class AddAddons : System.Web.UI.Page
    {
        #region Private Variables
        private static bool _isSearchEnabled;
	    private const string DetailsLink = "~/Secure/Vendors/VendorProducts/View.aspx?mode=options";
	    private const string AddLink = "~/Secure/Vendors/VendorProducts/Add.aspx?";
	    private int _itemId = 0;
        private int _pageId = 0;
        #endregion

        #region Helper Methods
        /// <summary>
        /// Gets the name of the Addon for this AddonId
        /// </summary>
        /// <param name="addOnId">The value of addOnId</param>
        /// <returns>Returns the AddOn Name</returns>
        public string GetAddOnName(object addOnId)
        {
            ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();
            AddOn _addOn = AdminAccess.GetByAddOnId(int.Parse(addOnId.ToString()));

            if (_addOn != null)
            {
                return _addOn.Name;
            }

            return string.Empty;
        }
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param> 
        protected void Page_Load(object sender, EventArgs e)
        {
			//Buttons are disabled if no Rows found when calling SearchAddons()
			btnAddSelectedAddons.Visible = true;
			btntopAddSelectedAddons.Visible = true;
            btnBottomCancel.Visible = true;
            btnCancelTop.Visible = true;
         
            // Get ItemId from querystring        
            _itemId = Request.Params["itemid"] != null ? int.Parse(Request.Params["itemid"]) : 0;

            if (Request.Params["pageid"] != null)
            {
                _pageId = int.Parse(Request.Params["pageid"]);
            }

            if (!Page.IsPostBack)
            {
                SearchAddons();
                BindProductName();
            }
        }
        #endregion

        #region Bind Events

        /// <summary>
        /// Bind AddOn grid with filtered data
        /// </summary>
        protected void SearchAddons()
        {
			const int localeId = 0;
            var adminAccess = new ProductAddOnAdmin();
			var searchResults = adminAccess.SearchAddOns(Server.HtmlEncode(txtAddonName.Text.Trim()), Server.HtmlEncode(txtAddOnTitle.Text.Trim()), txtAddOnsku.Text.Trim(), localeId);
			
			//If No Results hide buttons, Bind will show NoResults Msg
			if (searchResults.Tables[0].Rows.Count == 0)
			{
				btnAddSelectedAddons.Visible = false;
				btntopAddSelectedAddons.Visible = false;
                btnBottomCancel.Visible = false;
			    btnCancelTop.Visible = false;
			}

	        uxAddOnGrid.DataSource = searchResults;
			uxAddOnGrid.DataBind();
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// AddOn Grid items page event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxAddOnGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxAddOnGrid.PageIndex = e.NewPageIndex;

            if (_isSearchEnabled)
            {
                this.SearchAddons();
            }
            else
            {
                this.BindAddons();
            }
        }

        /// <summary>
        /// ProductAdd-Ons Grid Row command event - occurs when Manage button is fired.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxAddOnGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // cell in the GridView control.
                GridViewRow selectedRow = uxAddOnGrid.Rows[index];
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Add Selected Addons Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddSelectedAddons_Click(object sender, EventArgs e)
        {
            ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();
            StringBuilder sb = new StringBuilder();
            StringBuilder addonName = new StringBuilder();

			//Put checkBox Values into a list of strings
	        var checkBoxValues = (from GridViewRow row in uxAddOnGrid.Rows 
								  select (CheckBox) row.Cells[0].FindControl("chkProductAddon") 
								  into checkBoxControl select checkBoxControl.Checked.ToString()).ToList();

			//Find how many checkBoxes are not checked
	        var falseCounter = checkBoxValues.Count(checkBox => checkBox.Contains("F"));
			
			//If no checkBoxes are checked, return an error message
	        if (falseCounter == checkBoxValues.Count)
			{
				lblAddOnErrorMessage.Visible = true;
                lblAddOnErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAddonList").ToString(); 
				return;
			}

            // Loop through the grid values
            foreach (GridViewRow row in uxAddOnGrid.Rows)
            {
                CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProductAddon") as CheckBox;

                // Get AddOnId
                int AddOnID = int.Parse(row.Cells[1].Text);

                if (check.Checked)
                {
                    ProductAddOn ProdAddOnEntity = new ProductAddOn();

                    // Set Properties
                    ProdAddOnEntity.ProductID = this._itemId;
                    ProdAddOnEntity.AddOnID = AddOnID;

                    if (AdminAccess.IsAddOnExists(ProdAddOnEntity))
                    {
                        AdminAccess.AddNewProductAddOn(ProdAddOnEntity);
                        addonName.Append(this.GetAddOnName(AddOnID) + ",");
                        check.Checked = false;
                    }
                    else
                    {
                        sb.Append(this.GetAddOnName(AddOnID) + ",");
                        lblAddOnErrorMessage.Visible = true;
                    }
                }
            }

            if (sb.ToString().Length > 0)
            {
                sb.Remove(sb.ToString().Length - 1, 1);

                // Display Error message
                lblAddOnErrorMessage.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductAddon").ToString(), sb.ToString());
            }
            else
            {
                ProductAdmin prodAdmin = new ProductAdmin();
                ZNode.Libraries.DataAccess.Entities.Product product = prodAdmin.GetByProductId(this._itemId);
                if (addonName.Length > 0)
                {
                    addonName.Remove(addonName.Length - 1, 1);

                    string Associatename = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogProductAddon").ToString(), addonName, product.Name); 
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, Associatename, product.Name);

                    ProductReviewHistoryAdmin reviewAdmin = new ProductReviewHistoryAdmin();
                    reviewAdmin.UpdateEditReviewHistory(this._itemId, "AddOn");
                }

                if (this._pageId > 0)
                {
                    Session["stepid"] = 2;

                    Response.Redirect(AddLink + "itemid=" + this._itemId);
                }
                else
                {
                    Response.Redirect(DetailsLink + "&itemid=" + this._itemId);
                }
            }
        }

        /// <summary>
        /// Addon Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddOnSearch_Click(object sender, EventArgs e)
        {
            this.SearchAddons();
            _isSearchEnabled = true;
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this._pageId > 0)
            {
                Session["stepid"] = 2;

                Response.Redirect(AddLink + "itemid=" + this._itemId);
            }
            else
            {
                Response.Redirect(DetailsLink + "&itemid=" + this._itemId);
            }
        }

        /// <summary>
        /// Clear Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddOnClear_Click(object sender, EventArgs e)
        {
            // Bind Grid
            this.BindAddons();

            // Reset Text fields & Bool variable
            txtAddonName.Text = string.Empty;
            txtAddOnTitle.Text = string.Empty;
            txtAddOnsku.Text = string.Empty;
            _isSearchEnabled = false;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Bind Product Name Method
        /// </summary>
        private void BindProductName()
        {
            ProductAdmin ProductAdminAccess = new ProductAdmin();
            ZNode.Libraries.DataAccess.Entities.Product entity = ProductAdminAccess.GetByProductId(this._itemId);
            if (entity != null)
            {
                lblTitle.Text = lblTitle.Text + " for \"" + entity.Name + "\"";
            }
        }

        /// <summary>
        /// Bind Addon Grid - all Addons
        /// </summary>
        private void BindAddons()
        {
            ProductAddOnAdmin ProdAddonAdminAccess = new ProductAddOnAdmin();

            // List of Addons
            uxAddOnGrid.DataSource = ProdAddonAdminAccess.GetAllAddOns();
            uxAddOnGrid.DataBind();
        }
        #endregion
    }
}