<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Vendors.VendorProducts.Default" ValidateRequest="false" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ProductReviewStateAutoComplete" Src="~/Controls/Default/ProductReviewStateAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ReviewStateLegend" Src="~/Controls/Default/ReviewStateLegend.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div>
            <div class="LeftFloat" style="width: 50%">
                <h1>
                    <asp:Localize ID="LinkTextVendorProducts" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextVendorProducts %>'></asp:Localize>
                </h1>
            </div>
            <div class="ButtonStyle" style="display: none;">
                <zn:LinkButton ID="btnAddProduct" runat="server" CausesValidation="False"
                    ButtonType="Button" OnClick="BtnAddProduct_Click" Text='<%$ Resources:ZnodeAdminResource, LinkButtonAddNewProduct %>'
                    ButtonPriority="Primary" />
            </div>
        </div>
        <div class="ClearBoth" align="left">
            <p>
                <asp:Localize ID="VendorProducts" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextVendorProducts %>'></asp:Localize>
            </p>
            <br />
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="SearchVendorProducts" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchVendorProducts %>'></asp:Localize></h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle" runat="server" id="ReviewerSearch">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleVendor %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtVendor" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleVendorId" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleVendorId %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtVendorId" runat="server"></asp:TextBox></span>
                        <div>
                            <asp:RegularExpressionValidator ID="regvendorid" runat="server" ControlToValidate="txtVendorId"
                                ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidVendorId %>' Display="Dynamic" ValidationExpression="[0-9A-Za-z\s\',.:&%#$@/_-]+"
                                CssClass="Error"></asp:RegularExpressionValidator>
                        </div>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleStoreName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStoreName %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlPortal" runat="server">
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle" runat="server" id="div2CO">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleSKU" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSKU %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txt2coid" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleProductName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductname" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleVendorProductId" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleVendorProductId %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtVendorproductId" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleProductStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductStatus %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <ZNode:ProductReviewStateAutoComplete ID="txtProductStatus" runat="server" />
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <br />
                </div>
                <div>


                    <zn:Button ID="btnClear" runat="server" ButtonType="CancelButton" CausesValidation="false" OnClick="BtnClear_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonClear %>" />
                    <zn:Button ID="btnSearch" runat="server" ButtonType="SubmitButton" CausesValidation="true" OnClick="BtnSearch_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonSearch %>" />


                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle">
            <asp:Localize ID="ProductList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleProductList %>'></asp:Localize></h4>
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <asp:UpdatePanel ID="upProductList" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
                    GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
                    OnRowCommand="UxGrid_RowCommand" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyText %>'
                    Width="100%" CellPadding="4" OnRowDataBound="UxGrid_RowDataBound" OnDataBound="UxGrid_DataBound">
                    <Columns>
                        <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                            <HeaderTemplate>
                                <asp:CheckBox ID="chkSelectAll" AutoPostBack="true" runat="server" OnCheckedChanged="ChkSelectAll_CheckedChanged" />
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="chkProduct" runat="server" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="productid" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleImage %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <a href='<%# "view.aspx?itemid=" + DataBinder.Eval(Container.DataItem,"productid")%>'
                                    id="LinkView">
                                    <img alt=" " src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                                        runat="server" style="border: none" />
                                </a>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:HyperLinkField DataNavigateUrlFields="productid" DataNavigateUrlFormatString="view.aspx?itemid={0}"
                            DataTextField="name" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:HyperLinkField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSellingPrice %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# FormatPrice(DataBinder.Eval(Container.DataItem,"RetailPrice")) %>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleVendor %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# GetVendorStoreName(DataBinder.Eval(Container.DataItem, "Vendor"), DataBinder.Eval(Container.DataItem, "PortalID"))%>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleChangedFields %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# GetChangedFields(DataBinder.Eval(Container.DataItem, "ProductID"))%>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:TemplateField>
                        <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleInStock %>' DataField="Quantityonhand" HeaderStyle-HorizontalAlign="Left">
                            <HeaderStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <img alt="" id="Img1" src='<%# GetStausMark(DataBinder.Eval(Container.DataItem, "ReviewStateId").ToString())%>'
                                    runat="server" />
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                            <ItemTemplate>
                                <asp:LinkButton ID="Button1" CommandName="Manage" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"productid")%>'
                                    Text='<%$ Resources:ZnodeAdminResource, LinkManage %>' runat="server" CssClass="actionlink" />
                                &nbsp;<asp:LinkButton ID="Button5" CommandName="Delete" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"productid")%>'
                                    Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' runat="server" CssClass="actionlink" />
                            </ItemTemplate>
                            <ItemStyle Wrap="False" />
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <PagerStyle CssClass="PagerStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                </asp:GridView>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <div id="divButton" runat="server">
                <div style="float: left">
                    <zn:Button ID="btnDeclineall" runat="server" ButtonType="EditButton" Width="160px" CausesValidation="false" OnClick="BtnDeclineall_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonDeclineSelected %>" />
                    <zn:Button ID="btnApproveAll" runat="server" ButtonType="EditButton" Width="160px" CausesValidation="false" OnClick="BtnApproveAll_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonApproveSelected %>" />
                    <zn:Button ID="btnReviewImages" runat="server" ButtonType="EditButton" Width="160px" CausesValidation="false" OnClick="BtnReviewImages_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonReviewImages %>" />
                </div>
                <div style="float: right; margin-top: -10px;">
                    <ZNode:ReviewStateLegend ID="uxReviewStateLegend" runat="server" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
