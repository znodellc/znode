﻿<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" CodeBehind="Default.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Vendors.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="LeftMargin">

        <h1><asp:Localize ID="Vendors" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleVendors %>'></asp:Localize></h1>
        <hr />

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/ReviewVendorProducts.png" />
            </div>
            <div class="Shortcut"><a id="A2" href="~/Secure/Vendors/VendorProducts/Default.aspx" runat="server"><asp:Localize ID="VendorProducts" runat="server" Text='<%$ Resources:ZnodeAdminResource,  LinkTextVendorProducts %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize ID="VendorProductsText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextVendorProducts %>'></asp:Localize></p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/mall-admin.png" />
            </div>
            <div class="Shortcut"><a id="A6" href="~/Secure/Vendors/VendorAccounts/Default.aspx" runat="server"><asp:Localize ID="VendorAccounts" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextVendorAccounts %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize ID="VendorAccountsText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextVendorAccounts %>'></asp:Localize></p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/franchise-admin.png" />
            </div>
            <div class="Shortcut"><a id="A7" href="~/Secure/Vendors/FranchiseAdministrators/Default.aspx" runat="server"><asp:Localize ID="FranchiseAdministrators" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextFranchiseAdministrators %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize ID="FranchiseAdministratorsText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextFranchiseAdministrator %>'></asp:Localize></p>
            </div>
        </div>

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/RejectionMessages.png" />
            </div>
            <div class="Shortcut">
                <a id="A15" href="~/Secure/Vendors/RejectionMessages/Default.aspx" runat="server"><asp:Localize ID="RejectionMessages" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextRejectionMessages %>'></asp:Localize></a>
            </div>
            <p><asp:Localize ID="RejectionMessagesText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextRejectionMessages %>'></asp:Localize></p>

        </div>
    </div>
</asp:Content>
