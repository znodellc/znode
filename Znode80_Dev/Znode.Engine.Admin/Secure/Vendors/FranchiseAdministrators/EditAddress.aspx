﻿<%@ Page Language="C#" AutoEventWireup="True"  MasterPageFile="~/Themes/Standard/edit.master" CodeBehind="EditAddress.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Vendors.FranchiseAdministrators.EditAddress" %>

<%@ Register Src="~/Controls/Default/Accounts/Address.ascx" TagName="Address" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
   <ZNode:Address ID="uxSiteAddress" runat="server" RoleName="admin" />
</asp:Content>