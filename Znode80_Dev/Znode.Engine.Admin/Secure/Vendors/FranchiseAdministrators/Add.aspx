﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="true" CodeBehind="Add.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Vendors.FranchiseAdministrators.Add" %>

<%@ Register TagPrefix="ZNode" TagName="spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <asp:Panel runat="server" ID="pnlCreateAccount">
        <div class="FormView">
            <div>
                <h1>
                    <asp:Localize ID="TitleFranchiseAccount" Text='<%$ Resources:ZnodeAdminResource,TitleFranchiseAccount%>' runat="server"></asp:Localize></h1>
                <p>
                    <asp:Localize ID="TextFranchiseAccount" Text='<%$ Resources:ZnodeAdminResource,TextFranchiseAccount%>' runat="server"></asp:Localize>
                </p>
                <div class="FieldStyle1">
                    <b style="text-decoration: underline">
                        <asp:Localize ID="Note" Text='<%$ Resources:ZnodeAdminResource,TextNote%>' runat="server"></asp:Localize></b><asp:Label ID="lblInstructions" runat="server" Text=""></asp:Label>
                </div>
            </div>
            <div class="FormView">
                <div>
                    <ZNode:spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:spacer>
                </div>
                <div>
                    <div class="ClearBoth" style="float: left">
                        <br />
                        <asp:Label ID="ErrorMessage" CssClass="Error" runat="server" Text=""></asp:Label>
                    </div>
                </div>
                <h4 class="SubTitle">
                    <asp:Localize ID="LoginInformation" Text='<%$ Resources:ZnodeAdminResource,SubTitleLoginInformation%>' runat="server"></asp:Localize></h4>
                <div class="FieldStyle">
                    <asp:Localize ID="FranchiseNum" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleFranchiseNumber%>' runat="server"></asp:Localize><span class="Asterix">*</span><br />
                    <small>
                        <asp:Localize ID="TextFranchiseNo" Text='<%$ Resources:ZnodeAdminResource,TextFranchiseNumber%>' runat="server"></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="FranchiseNumber" runat="server" CssClass="TextField" MaxLength="50"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="FranchiseNumberRequired" runat="server" ControlToValidate="FranchiseNumber"
                            ErrorMessage='<%$ Resources:ZnodeAdminResource,RequiredFranchiseNumber%>' ToolTip='<%$ Resources:ZnodeAdminResource,RequiredFranchiseNumber%>' ValidationGroup="uxCreateUserWizard"
                            CssClass="Error" Display="Dynamic" SetFocusOnError="True">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="TextUserName" Text='<%$ Resources:ZnodeAdminResource,ColumnUserName%>' runat="server"></asp:Localize><span class="Asterix">*</span><br />
                    <small>
                        <asp:Localize ID="TextUserNameColumn" Text='<%$ Resources:ZnodeAdminResource,TextUserName%>' runat="server"></asp:Localize></small>
                </div>

                <div class="ValueStyle">
                    <asp:TextBox ID="UserName" runat="server" autocomplete="off" CssClass="TextField"></asp:TextBox>
                    <div>
                        <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                            ErrorMessage='<%$ Resources:ZnodeAdminResource,RequiredUsername%>' ToolTip='<%$ Resources:ZnodeAdminResource,RequiredUsername%>' ValidationGroup="uxCreateUserWizard"
                            CssClass="Error" Display="Dynamic" SetFocusOnError="True">
                        </asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="ClearBoth">
                </div>

                <h4 class="SubTitle">
                    <asp:Localize ID="StoreSettings" Text='<%$ Resources:ZnodeAdminResource,SubtitleStoreSettings%>' runat="server"></asp:Localize><span class="Asterix">*</span><br />
                </h4>

                <div class="FieldStyle">
                    <asp:Localize ID="StoreName" Text='<%$ Resources:ZnodeAdminResource,ColumnStoreName%>' runat="server"></asp:Localize><span class="Asterix">*</span><br />
                    <small>
                        <asp:Localize ID="TextColumnStoreName" Text='<%$ Resources:ZnodeAdminResource,TextColumnStoreName%>' runat="server"></asp:Localize></small>
                </div>

                <div class="ValueStyle">
                    <asp:TextBox ID="txtBillingCompanyName" runat="server" CssClass="TextField" Columns="30"
                        MaxLength="100"></asp:TextBox>
                    <br />
                    <asp:RequiredFieldValidator ID="Requiredfieldvalidator11" runat="server"
                        ControlToValidate="txtBillingCompanyName" CssClass="Error" Display="Dynamic"
                        ErrorMessage="Store Name is required" ValidationGroup="uxCreateUserWizard" SetFocusOnError="True">
                    </asp:RequiredFieldValidator>
                    <br />
                    <div>
                    </div>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnTheme" Text='<%$ Resources:ZnodeAdminResource,ColumnTheme%>' runat="server"></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlTheme" runat="server" />
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="SiteURL" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleSiteURL%>' runat="server"></asp:Localize><br />
                    <small>
                        <asp:Localize ID="TextSiteURL" Text='<%$ Resources:ZnodeAdminResource,ColumnTextSiteURL%>' runat="server"></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtStoreUrl" runat="server" CssClass="TextField" Columns="30" MaxLength="100"></asp:TextBox><div>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtStoreUrl" ValidationGroup="uxCreateUserWizard"
                            ErrorMessage='<%$ Resources:ZnodeAdminResource,RegularValidURLStar%>' CssClass="Error" Display="dynamic" ValidationExpression="^(http\:\/\/[a-zA-Z0-9_\-/]+(?:\.[a-zA-Z0-9_\-/]+)*)$" SetFocusOnError="True">
                        </asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="SelectLogo" Text='<%$ Resources:ZnodeAdminResource,ColumnSelectLogo%>' runat="server"></asp:Localize><br />
                    <small>
                        <asp:Localize ID="TextLogo" Text='<%$ Resources:ZnodeAdminResource,TextLogo%>' runat="server"></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <asp:FileUpload ID="RegisterUploadImage" runat="server" BorderStyle="Inset" EnableViewState="true" />
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="RegisterUploadImage"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource,RegularValidUploadImage%>' SetFocusOnError="True"
                        ValidationExpression="^.+\.((jpg)|(JPG)|(gif)|(GIF)|(jpeg)|(JPEG)|(png)|(PNG))$" Display="Dynamic"
                        ValidationGroup="uxCreateUserWizard"> </asp:RegularExpressionValidator>
                </div>

                <div class="ClearBoth">
                </div>
                <h4 class="SubTitle">
                    <asp:Localize ID="ContactInfo" Text='<%$ Resources:ZnodeAdminResource,SubTitleContact%>' runat="server"></asp:Localize></h4>

                <div class="FieldStyle">
                    <asp:Localize ID="FirstName" Text='<%$ Resources:ZnodeAdminResource,ColumnFirstName%>' runat="server"></asp:Localize><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtBillingFirstName" runat="server" CssClass="TextField" Columns="30"
                        MaxLength="100"></asp:TextBox><div>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator9" ErrorMessage='<%$ Resources:ZnodeAdminResource,RequiredFirstName%>'
                                ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingFirstName" runat="server"
                                Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="LastName" Text='<%$ Resources:ZnodeAdminResource,ColumnLastName%>' runat="server"></asp:Localize><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtBillingLastName" runat="server" CssClass="TextField" Columns="30"
                        MaxLength="100"></asp:TextBox><div>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator10" ErrorMessage='<%$ Resources:ZnodeAdminResource,RequiredLastName%>'
                                ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingLastName" runat="server"
                                Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="EmailAddress" Text='<%$ Resources:ZnodeAdminResource,ColumnEmailAddress%>' runat="server"></asp:Localize><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtBillingEmail" runat="server" CssClass="TextField"></asp:TextBox><div>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" ErrorMessage='<%$ Resources:ZnodeAdminResource,RequiredEmailAddress%>'
                            ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingEmail" runat="server"
                            Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="regemailID" runat="server" ControlToValidate="txtBillingEmail"
                            ErrorMessage='<%$ Resources:ZnodeAdminResource,ValidEmail%>' Display="Dynamic" ValidationExpression='[\w\.-]+(\+[\w-]*)?@([\w-]+\.)+[\w-]+'
                            CssClass="Error" ValidationGroup="uxCreateUserWizard" SetFocusOnError="True"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="PhoneNo" Text='<%$ Resources:ZnodeAdminResource,ColumnPhoneNumber%>' runat="server"></asp:Localize><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtBillingPhoneNumber" runat="server" CssClass="TextField" Columns="30"
                        MaxLength="100"></asp:TextBox><div>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" ErrorMessage='<%$ Resources:ZnodeAdminResource,RequiredPhoneNumber%>'
                                ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingPhoneNumber"
                                runat="server" Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="Street1" Text='<%$ Resources:ZnodeAdminResource,ColumnStreet1%>' runat="server"></asp:Localize><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtBillingStreet1" runat="server" CssClass="TextField" Columns="30"
                        MaxLength="100"></asp:TextBox><div>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" ErrorMessage='<%$ Resources:ZnodeAdminResource,RequiredStreet1%>'
                                ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingStreet1" runat="server"
                                Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="Street2" Text='<%$ Resources:ZnodeAdminResource,ColumnStreet2%>' runat="server"></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtBillingStreet2" runat="server" CssClass="TextField" Columns="30"
                        MaxLength="100"></asp:TextBox>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="City" runat="server" Text='<%$ Resources:ZnodeAdminResource,ColumnTitleCity%>' /><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtBillingCity" runat="server" CssClass="TextField" Columns="30"
                        MaxLength="100"></asp:TextBox><div>
                            <asp:RequiredFieldValidator ID="Requiredfieldvalidator4" ErrorMessage='<%$ Resources:ZnodeAdminResource,RequiredCity%>'
                                ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingCity" runat="server"
                                Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                        </div>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="State" Text='<%$ Resources:ZnodeAdminResource,SubTextState%>' runat="server"></asp:Localize><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtBillingState" runat="server" Width="40" Columns="10" MaxLength="2"></asp:TextBox><div>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator6" ErrorMessage='<%$ Resources:ZnodeAdminResource,RequiredState%>'
                            ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingState" runat="server"
                            Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="PostalCode" Text='<%$ Resources:ZnodeAdminResource,ColumnPostalCode%>' runat="server"></asp:Localize><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtBillingPinCode" runat="server" Width="40" Columns="10"></asp:TextBox><div>
                        <asp:RequiredFieldValidator ID="Requiredfieldvalidator5" ErrorMessage='<%$ Resources:ZnodeAdminResource,RequiredPostalCode%>'
                            ValidationGroup="uxCreateUserWizard" ControlToValidate="txtBillingPinCode" runat="server"
                            Display="Dynamic" CssClass="Error" SetFocusOnError="True"></asp:RequiredFieldValidator>
                    </div>
                    <ZNode:spacer ID="Spacer1" SpacerHeight="30" SpacerWidth="10" runat="server"></ZNode:spacer>
                    <div class="ValueStyle">
                        <zn:Button runat="server" ID="ibRegister" ButtonType="SubmitButton" OnClick="IbRegister_Click" Text='<%$ Resources:ZnodeAdminResource,ButtonSubmit%>' CausesValidation="True" ValidationGroup="uxCreateUserWizard" />
                        <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource,ButtonCancel%>' CausesValidation="False" />

                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
    <asp:Panel ID="pnlConfirm" runat="server" Visible="False" CssClass="SuccessMsg">
        <br />
        <br />
        <p>
            <asp:Localize ID="SuccessMsg" Text='<%$ Resources:ZnodeAdminResource,TextSuccessMsg%>' runat="server"></asp:Localize></p>
        <div class="Clear">
            <ZNode:spacer ID="Spacer9" EnableViewState="false" SpacerHeight="20" SpacerWidth="10"
                runat="server"></ZNode:spacer>
        </div>
    </asp:Panel>


</asp:Content>
