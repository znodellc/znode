﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Znode.Engine.Admin.Secure.Vendors.FranchiseAdministrators {
    
    
    public partial class Default {
        
        /// <summary>
        /// TitleFranchiseAccount control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize TitleFranchiseAccount;
        
        /// <summary>
        /// TextFranchiseAdministrator control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Localize TextFranchiseAdministrator;
        
        /// <summary>
        /// AddFranchiseAdmin control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Znode.Engine.Common.CustomClasses.WebControls.LinkButton AddFranchiseAdmin;
        
        /// <summary>
        /// uxCustomer control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Znode.Engine.Admin.Controls.Default.Accounts.Customer uxCustomer;
    }
}
