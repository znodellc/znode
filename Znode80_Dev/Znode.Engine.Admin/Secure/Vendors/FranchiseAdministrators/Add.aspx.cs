﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;
using System.Linq;
using ZNode.Libraries.ECommerce.Utilities;

namespace  Znode.Engine.Admin.Secure.Vendors.FranchiseAdministrators
{
    public partial class Add : System.Web.UI.Page
    {
		private MembershipProvider _adminMembershipProvider = Membership.Providers["ZNodeAdminMembershipProvider"];

		public MembershipProvider AdminMembershipProvider
		{
			get { return _adminMembershipProvider; }
			set { _adminMembershipProvider = value; }
		}

        #region Page Load Event

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ibRegister.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            ibRegister.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit.gif") + "'");
            btnClear.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnClear.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel.gif") + "'");


            // check if SSL is required
            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {
                    string inURL = Request.Url.ToString();
                    string outURL = inURL.ToLower().Replace("http://", "https://");

                    Response.Redirect(outURL);
                }
            }

            if (this.Page != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtAccountTitle.Text");
            }

            if (!Page.IsPostBack)
            {
                this.BindTheme();
            }
			//Displayed due to new ResetPwd Implementation.
            lblInstructions.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextInstructions").ToString();
        }
        #endregion

        #region Protected Events
        /// <summary>
        /// Occurrs when register button clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void IbRegister_Click(object sender, EventArgs e)
        {
            this.RegisterUser();
        }

        /// <summary>
        /// Occurs when Clear button clicked.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            var link = "~/Secure/Vendors/FranchiseAdministrators/Default.aspx";
            Response.Redirect(link);
        }

		/// <summary>
		/// Sends a new password in an email to the newly created account by id
		/// </summary>
		/// <param name="accountId">Takes an integer for the accountId parameter.</param>
		protected bool SendNewUserEmail(int accountId)
		{
			var accountAdmin = new AccountAdmin();
			var portalId = ZNodeConfigManager.SiteConfig.PortalID;
			var portalAdmin = new PortalAdmin();
			var portal = portalAdmin.GetByPortalId(portalId);
			var storeName = portal.StoreName;

			var account = accountAdmin.GetByAccountID(accountId);

			if (account == null || !account.UserID.HasValue) return false;

			var user = AdminMembershipProvider.GetUser(account.UserID.Value, false);

			if (user == null)
			{
                ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCreateEmail").ToString();
				ZNodeLoggingBase.LogMessage(ErrorMessage.Text);
				return false;
			}

			if (String.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SMTPServer))
			{
                ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogSMTPSettings").ToString() + storeName;
				ZNodeLoggingBase.LogMessage(ErrorMessage.Text);
				return false;
			}

			var senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            var subject = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextNewUserCreation").ToString(), storeName);
			var toAddress = account.Email;

			var currentCulture = ZNodeCatalogManager.CultureInfo;

			var defaultTemplatePath =
				Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "NewUserAccount_en.htm"));

			var templatePath = currentCulture != string.Empty
								   ? Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "NewUserAccount_" +
													currentCulture + ".htm")
								   : Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath,
																 "NewUserAccount_en.htm"));

			// Check for language specific file existence.
			if (!File.Exists(templatePath))
			{
				// Check the default template file existence.
				if (!File.Exists(defaultTemplatePath))
				{
                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCreateTemplateFile").ToString();
					return false;
				}
				templatePath = defaultTemplatePath;
			}
			
			try
			{
				string body;
				using (var streamReader = new StreamReader(templatePath))
				{
					body = streamReader.ReadToEnd();
					
				}

				var userName = user.UserName;
				var regularExpName = new Regex("#UserName#", RegexOptions.IgnoreCase);
				body = regularExpName.Replace(body, userName);

				var userPassword = user.GetPassword();
				var regularExpPassword = new Regex("#Password#", RegexOptions.IgnoreCase);
				body = regularExpPassword.Replace(body, userPassword);

				var regularUrl = new Regex("#Url#", RegexOptions.IgnoreCase);
               
                var franchiseLoginUrl = WebConfigurationManager.AppSettings["AdminWebsiteUrl"] + "/franchiseadmin";
                if (string.IsNullOrEmpty(WebConfigurationManager.AppSettings["AdminWebsiteUrl"]))
                {
                    franchiseLoginUrl = "http://" + ZNodeConfigManager.DomainConfig.DomainName + "/franchiseadmin";
                }
                body = regularUrl.Replace(body, franchiseLoginUrl);
				
				ZNodeEmail.SendEmail(toAddress, senderEmail, String.Empty, subject, body, true);
			}
			catch (Exception ex)
			{
                ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogPasswordMail").ToString();
				ZNodeLoggingBase.LogMessage(ex.Message);
				return false;
			}

			return true;
		}
        #endregion

        #region Private Methods
        /// <summary>
        /// Bind theme file list
        /// </summary>
        private void BindTheme()
        {
            ddlTheme.DataSource = DataRepository.ThemeProvider.GetAll();
            ddlTheme.DataTextField = "Name";
            ddlTheme.DataValueField = "ThemeID";
            ddlTheme.DataBind();

            ddlTheme.SelectedIndex = 0;

        }

        /// <summary>
        /// This function registers a new user 
        /// </summary>
        private void RegisterUser()
        {
			MembershipCreateStatus createStatus;
			MembershipUser user;

			var newlyCreatedPassword = Membership.GeneratePassword(8, 0);
            var tranManager = ConnectionScope.CreateTransaction();
			var accountProfileServ = new AccountProfileService();
			var znodeUserAccount = new ZNodeUserAccount();
			var accountService = new AccountService();
			var addressService = new AddressService();
			var accountAdmin = new AccountAdmin();
			var log = new ZNodeLogging();
			
			var portalId = CreateStore();
			var profileId = CreateStoreProfile(portalId);

           
				if (UserName.Text.Trim().Length <= 0)
				{
					if (tranManager.IsOpen)
						tranManager.Rollback();
                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorEnterUsername").ToString();
					return;
				}

				if (FranchiseNumber.Text.Length > 0)
				{
                    TList<Account> account = accountService.Find(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextExternalAccountNo").ToString(), FranchiseNumber.Text.Trim()));
					
					if (account != null &&  account.Count > 0)
					{
						if (account[0].UserID != null)
						{
							if (tranManager.IsOpen)
								tranManager.Rollback();
							ErrorMessage.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFranchiseNum").ToString(), FranchiseNumber.Text, ZNodeConfigManager.SiteConfig.CustomerServiceEmail);
							return;
						}
					}
				}
				
                if (portalId == 0)
                {
					if (tranManager.IsOpen)
						tranManager.Rollback();
					if(string.IsNullOrEmpty(ErrorMessage.Text))
                        ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInvalidPortal").ToString();
					return;
                }
				
                if (profileId == 0)
                {
                    if (tranManager.IsOpen)
						tranManager.Rollback();
                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountProfile").ToString();
                    return;
                }

                // Check if loginName already exists
                if (!znodeUserAccount.IsLoginNameAvailable(portalId, UserName.Text.Trim()))
                {
	                if (tranManager.IsOpen)
		                tranManager.Rollback();
                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorExistingLoginName").ToString();
                    return;
                }
				
				log.LogActivityTimerStart();     
				
	            user = AdminMembershipProvider.CreateUser(UserName.Text.Trim(), newlyCreatedPassword,
	                                                      txtBillingEmail.Text.Trim(), null, null, true, Guid.NewGuid(),
	                                                      out createStatus);

                if (user == null || createStatus != MembershipCreateStatus.Success || user.ProviderUserKey == null)
                {
	                if (tranManager.IsOpen)
						tranManager.Rollback();
                    
                    log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text);
                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorTemplateFile").ToString();
                    return;
                }

				var newUserAccount = new Account
					{
						UserID = (Guid) user.ProviderUserKey,
						CompanyName = txtBillingCompanyName.Text,
						ExternalAccountNo = FranchiseNumber.Text.Trim(),
						Email = txtBillingEmail.Text.Trim(),
						CreateDte = DateTime.Now,
						UpdateDte = DateTime.Now,
						ProfileID = profileId,
						CreateUser = HttpContext.Current.User.Identity.Name
					};
                   
				// Register account
                if (newUserAccount.AccountID > 0)
                {
                    accountService.Update(newUserAccount);
                }
                else
                {
                    accountService.Insert(newUserAccount);
					accountAdmin.UpdateLastPasswordChangedDate(newUserAccount.UserID.ToString());
                }
				
				var address = accountAdmin.GetDefaultBillingAddress(newUserAccount.AccountID) ?? new Address
	                {
		                IsDefaultShipping = true,
		                IsDefaultBilling = true,
		                AccountID = newUserAccount.AccountID
	                };

	            // Copy info to billing
                address.FirstName = Server.HtmlEncode(txtBillingFirstName.Text);
                address.LastName = Server.HtmlEncode(txtBillingLastName.Text);
                address.CompanyName = Server.HtmlEncode(txtBillingCompanyName.Text);
                address.Street = Server.HtmlEncode(txtBillingStreet1.Text);
                address.Street1 = Server.HtmlEncode(txtBillingStreet2.Text);
                address.City = Server.HtmlEncode(txtBillingCity.Text);
                address.StateCode = Server.HtmlEncode(txtBillingState.Text);
                address.PhoneNumber = txtBillingPhoneNumber.Text;
                address.PostalCode = txtBillingPinCode.Text;

                // Default country code for United States
                address.CountryCode = "US";
                address.Name = "Default Address";

                // Insert shipping address as default billing address.
                if (address.AddressID > 0)
                {
                    addressService.Update(address);
                }
                else
                {
                    addressService.Insert(address);
                }

                // Add Account Profile
                var accountProfile = new AccountProfile {AccountID = newUserAccount.AccountID, ProfileID = profileId};
				
				accountProfileServ.Insert(accountProfile);

                Roles.AddUserToRole(UserName.Text.Trim(), "FRANCHISE");

				//Log password for useraccount
                ZNodeUserAccount.LogPassword((Guid)user.ProviderUserKey, newlyCreatedPassword);

                // Create an Profile for the selected user
                var newProfile = ProfileCommon.Create(UserName.Text, true);

                // Properties Value
                newProfile.StoreAccess = portalId.ToString(CultureInfo.InvariantCulture);

                // Save profile - must be done since we explicitly created it 
                newProfile.Save();

				log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateSuccess, UserName.Text);

				var emailSent = SendNewUserEmail(newUserAccount.AccountID);

				if (!emailSent)
				{
					//If email fails, still save the user & provide error message
					if (tranManager.IsOpen) 
						tranManager.Commit();
					
					log.LogActivityTimerEnd((int)ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text);
					
					if (string.IsNullOrEmpty(ErrorMessage.Text))
                        ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSendingUserAccount").ToString();
					return;
				}

                pnlCreateAccount.Visible = false;

                tranManager.Commit();

                Response.Redirect("~/Secure/Vendors/FranchiseAdministrators/View.aspx?mode=0&itemid=" + newUserAccount.AccountID + "&pagefrom=franchise");
            

        }

        /// <summary>
        /// Create New Profile for new store based on StoreName_ProfileID.
        /// </summary>
        /// <param name="portalId">Create portal profile with Portal Id.</param>
        /// <returns>Returns the created profile Id.</returns>
        private int CreateStoreProfile(int portalId)
        {
            try
            {
                ProfileService profileService = new ProfileService();
                Profile profile = new Profile();
                profile.Name = string.Format("{0}_{1}", txtBillingCompanyName.Text, portalId);
                ZNodeProfile znodeProfile = new ZNodeProfile();
                profile.ShowPricing = znodeProfile.ShowPrice;
                profile.UseWholesalePricing = znodeProfile.UseWholesalePricing;
                profileService.Insert(profile);

                if (profile.ProfileID > 0)
                {
                    PortalProfileService service = new PortalProfileService();
                    PortalProfile portalProfile = new PortalProfile();
                    portalProfile.PortalID = portalId;
                    portalProfile.ProfileID = profile.ProfileID;
                    service.Insert(portalProfile);

                    PortalService portalService = new PortalService();
                    Portal portal = portalService.GetByPortalID(portalId);
                    portal.DefaultAnonymousProfileID = profile.ProfileID;
                    portal.DefaultRegisteredProfileID = profile.ProfileID;
                    portalService.Update(portal);
                }

                return profile.ProfileID;
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCreatingStoreProfile").ToString(), Environment.NewLine, ex.ToString()));
            }

            return 0;
        }

        /// <summary>
        /// Create new store setting.
        /// </summary>
        /// <returns>Returns the created portal Id.</returns>
        private int CreateStore()
        {
            try
            {
                StoreSettingsAdmin storeAdmin = new StoreSettingsAdmin();
                Portal portal = new Portal();

                DomainAdmin domainAdmin = new DomainAdmin();
                Domain domain = new Domain();
                bool status;
                string domainName = string.Empty;

                // Domain Checking
                if (!string.IsNullOrEmpty(txtStoreUrl.Text.Trim()))
                {
                    // Set the domainName
                    domainName = txtStoreUrl.Text.Trim().ToLower();

                    if (domainName.Contains("http://") || domainName.Contains("https://"))
                    {
                        domainName = domainName.Replace("http://", string.Empty).Replace("https://", string.Empty);
                    }

                    if (domainName.Contains(":"))
                    {
                        // Strip off the port number so we won't conflict with debuggers and other services that may specify a different port.
                        domainName = domainName.Substring(0, domainName.IndexOf(":"));
                    }

                    if (domainName.StartsWith("www."))
                    {
                        domainName = domainName.Remove(0, 4);
                    }

                    // Remove any trailing "/"
                    if (domainName.EndsWith("/"))
                    {
                        domainName = domainName.Remove(domainName.Length - 1);
                    }

                    DomainService domainService = new DomainService();
                    TList<Domain> domains = domainService.Find(string.Format("DomainName='{0}'", domainName.Trim()));

                    if (domains != null && domains.Count > 0)
                    {
                        ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDomainNameShouldbeUnique").ToString();
                        return 0;
                    }
                }

                // Portal Creation: 

                // Get Exisitng Portal Settings By portalID
                Portal existingPortal = storeAdmin.GetByPortalId(ZNodeConfigManager.SiteConfig.PortalID);

                portal.FedExCSPKey = string.Empty;
                portal.FedExCSPPassword = string.Empty;
                portal.FedExClientProductVersion = string.Empty;
                portal.FedExClientProductId = string.Empty;

                portal.PortalID = 0;
                portal.AdminEmail = txtBillingEmail.Text;
                portal.CompanyName = txtBillingCompanyName.Text;
                portal.CustomerServiceEmail = txtBillingEmail.Text;
                portal.CustomerServicePhoneNumber = txtBillingPhoneNumber.Text;
                portal.InclusiveTax = false;
                portal.ActiveInd = true;
                portal.SalesEmail = txtBillingEmail.Text;
                portal.SalesPhoneNumber = txtBillingPhoneNumber.Text;
                portal.StoreName = txtBillingCompanyName.Text;
                portal.UseSSL = false;

                // Default Customer Review Status
                portal.DefaultReviewStatus = "N";

                // Default to English
                portal.LocaleID = 43;
                portal.FedExAccountNumber = string.Empty;
                portal.FedExAddInsurance = false;
                portal.FedExProductionKey = string.Empty;
                portal.FedExSecurityCode = string.Empty;
                portal.InclusiveTax = false;
                portal.SMTPPassword = string.Empty;
                portal.SMTPServer = string.Empty;
                portal.SMTPUserName = string.Empty;

                portal.SiteWideAnalyticsJavascript = string.Empty;
                portal.SiteWideBottomJavascript = string.Empty;
                portal.SiteWideTopJavascript = string.Empty;
                portal.OrderReceiptAffiliateJavascript = string.Empty;
                portal.GoogleAnalyticsCode = string.Empty;
                portal.UPSKey = string.Empty;
                portal.UPSPassword = string.Empty;
                portal.UPSUserName = string.Empty;

                // Default values from existing portal
                portal.SMTPPort = existingPortal.SMTPPort;
				portal.SMTPPassword = existingPortal.SMTPPassword;
				portal.SMTPServer = existingPortal.SMTPServer;
				portal.SMTPUserName = existingPortal.SMTPUserName;
                portal.WeightUnit = existingPortal.WeightUnit;
                portal.DimensionUnit = existingPortal.DimensionUnit;
                portal.CurrencyTypeID = existingPortal.CurrencyTypeID;
                portal.DefaultOrderStateID = (int)ZNode.Libraries.ECommerce.Entities.ZNodeOrderState.PENDING_APPROVAL;
                portal.DefaultProductReviewStateID = (int)ZNodeProductReviewState.PendingApproval;

                portal.MaxCatalogCategoryDisplayThumbnails = existingPortal.MaxCatalogCategoryDisplayThumbnails;
                portal.MaxCatalogDisplayColumns = existingPortal.MaxCatalogDisplayColumns;
                portal.MaxCatalogDisplayItems = existingPortal.MaxCatalogDisplayItems;
                portal.MaxCatalogItemCrossSellWidth = existingPortal.MaxCatalogItemCrossSellWidth;
                portal.MaxCatalogItemLargeWidth = existingPortal.MaxCatalogItemLargeWidth;
                portal.MaxCatalogItemMediumWidth = existingPortal.MaxCatalogItemMediumWidth;
                portal.MaxCatalogItemSmallThumbnailWidth = existingPortal.MaxCatalogItemSmallThumbnailWidth;
                portal.MaxCatalogItemSmallWidth = existingPortal.MaxCatalogItemSmallWidth;
                portal.MaxCatalogItemThumbnailWidth = existingPortal.MaxCatalogItemThumbnailWidth;
                portal.ImageNotAvailablePath = existingPortal.ImageNotAvailablePath;

                portal.ShopByPriceMin = 5;
                portal.ShopByPriceMax = 500;
                portal.ShopByPriceIncrement = 20;

                // Set logo path
                if (RegisterUploadImage.PostedFile.FileName == string.Empty)
				{
					portal.LogoPath = string.Empty;
				}

                bool check = false;
                check = storeAdmin.InsertStore(portal);

                if (check)
                {
					// Update LogoPath based on PortalId
					Portal store = null;
					store = storeAdmin.GetByPortalId(portal.PortalID);

					if (RegisterUploadImage.PostedFile.FileName != string.Empty)
					{
						byte[] imageData = new byte[RegisterUploadImage.PostedFile.InputStream.Length];
						RegisterUploadImage.PostedFile.InputStream.Read(imageData, 0, (int)RegisterUploadImage.PostedFile.InputStream.Length);

						string fileName = ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + "Turnkey/" + portal.PortalID + "/" + System.IO.Path.GetFileNameWithoutExtension(RegisterUploadImage.PostedFile.FileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(RegisterUploadImage.PostedFile.FileName);
						//string fileName = ZNodeConfigManager.EnvironmentConfig.OriginalImagePath + System.IO.Path.GetFileNameWithoutExtension(RegisterUploadImage.PostedFile.FileName) + "_" + DateTime.Now.ToString("ddMMyyhhmmss") + System.IO.Path.GetExtension(RegisterUploadImage.PostedFile.FileName);

						ZNodeStorageManager.WriteBinaryStorage(imageData, fileName);
						store.LogoPath = fileName;
						check = storeAdmin.Update(store);
					}

                    // Portal Country Service
                    PortalCountryService portalCountryService = new PortalCountryService();
                    PortalCountry portalCountry = new PortalCountry();

                    // Add PortalCountry List
                    portalCountry.PortalID = portal.PortalID;
                    portalCountry.BillingActive = true;
                    portalCountry.ShippingActive = true;
                    portalCountry.CountryCode = "US";
                    check = portalCountryService.Insert(portalCountry);

                    // Delete the portalCatalog
                    CatalogAdmin catalogAdmin = new CatalogAdmin();
                    catalogAdmin.DeleteportalCatalog(portal.PortalID);

                    Catalog catalog = new Catalog();

                    // Create default catalog as StoreName_Catalog
                    catalog.Name = txtBillingCompanyName.Text + "_Default";
                    catalog.PortalID = portal.PortalID;
                    catalog.IsActive = true;
                    catalogAdmin.Insert(catalog);

                    PortalCatalog portalCatalog = new PortalCatalog();

                    // Add the new Selection
                    portalCatalog.PortalID = portal.PortalID;
                    portalCatalog.CatalogID = catalog.CatalogID;

                    // default set NULL to new store
                    portalCatalog.ThemeID = int.Parse(ddlTheme.SelectedValue);
                    var cssList = DataRepository.CSSProvider.GetByThemeID(int.Parse(ddlTheme.SelectedValue));
                    
                    if(cssList.Any())
					portalCatalog.CSSID = cssList.First().CSSID; // "Default";

                    // default English language.
                    portalCatalog.LocaleID = 43;
                    catalogAdmin.AddPortalCatalog(portalCatalog);

                    // Domain Creation
                    if (!string.IsNullOrEmpty(txtStoreUrl.Text.Trim()))
                    {
                        // Domain creation.
                        domain.PortalID = portal.PortalID;
                        domain.DomainName = domainName;
                        domain.IsActive = true;
                        try
                        {
                            portal = storeAdmin.GetByPortalId(portal.PortalID);

                            status = domainAdmin.Insert(domain);

                            // Log Activity
                            string associatename = "Added Domain " + domainName + " - " + portal.StoreName;
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associatename, portal.StoreName);
                        }
                        catch (Exception ex)
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                            status = false;
                        }
                    }

                    // Add Default Content Pages
                    ContentPageService contentPageService = new ContentPageService();
                    ContentPage contentPage = new ContentPage();
                    ContentPageAdmin contentPageAdmin = new ContentPageAdmin();
                    ContentPageQuery filter = new ContentPageQuery();

                    // Get ContentPages list using existing portalID and LocaleId
                    filter.Append(ContentPageColumn.LocaleId, existingPortal.LocaleID.ToString());
                    filter.Append(ContentPageColumn.PortalID, existingPortal.PortalID.ToString());
                    TList<ContentPage> contentPageList = contentPageService.Find(filter.GetParameters());

                    if (contentPageList.Count > 0)
                    {
                        foreach (ContentPage page in contentPageList)
                        {
                            contentPage = page.Clone() as ContentPage;
                            contentPage.ContentPageID = -1;
                            contentPage.PortalID = portal.PortalID;
                            contentPage.LocaleId = portalCatalog.LocaleID;
                            contentPage.SEOURL = null;
                            contentPage.ThemeID = portalCatalog.ThemeID;
                            contentPageAdmin.AddPage(contentPage, string.Empty, portal.PortalID, portalCatalog.LocaleID.ToString(), HttpContext.Current.User.Identity.Name, null, false);
                        }
                    }

                    // Create Message Config
                    storeAdmin.CreateMessage(portal.PortalID.ToString(), "43");

                    // Default Tax Type
                    TaxClassService taxTypeService = new TaxClassService();
                    TaxClass taxClass = new TaxClass();
                    taxClass.Name = "Default";
                    taxClass.PortalID = portal.PortalID;
                    taxClass.DisplayOrder = 1;
                    taxClass.ActiveInd = true;
                    check = taxTypeService.Insert(taxClass);

                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Creation of Store - " + txtBillingCompanyName.Text, txtBillingCompanyName.Text);

                    // Log Activity
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeSuccess, HttpContext.Current.User.Identity.Name);

                    return portal.PortalID;
                }
                else
                {
                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCreatingAccount").ToString();

                    // Log Activity
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.StoreSettingsChangeFailed, HttpContext.Current.User.Identity.Name);
                }
            }
            catch (Exception ex)
            {
                ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCreatingAccount").ToString();

                // Log Activity
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity(9002, HttpContext.Current.User.Identity.Name);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }

            return 0;
        }
        #endregion
    }
}
