﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" CodeBehind="Default.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Vendors.FranchiseAdministrators.Default" %>
<%@ Register Src="~/Controls/Default/Accounts/Customer.ascx" TagName="CustomerList" TagPrefix="ZNode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div align="center">
            <div class="LeftFloat" style="width: 70%; text-align: left">
                <h1>
                   <asp:Localize ID="TitleFranchiseAccount" Text='<%$ Resources:ZnodeAdminResource,LinkTextFranchiseAdministrators%>' runat="server"></asp:Localize></h1>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="LeftFloat">
                <p>
                    <asp:Localize ID="TextFranchiseAdministrator" Text='<%$ Resources:ZnodeAdminResource,TextFranchiseAdministrator%>' runat="server"></asp:Localize></h1>
                </p>
            </div>
            
            <div style="float: right;">
              <zn:LinkButton ID="AddFranchiseAdmin" runat="server"  CausesValidation="false"  Text='<%$ Resources:ZnodeAdminResource,TitleAddFranchiseAdmin%>' 
                OnCommand="AddFranchiseAdmin_Click"
                ButtonType="Button" ButtonPriority="Primary"/>

            </div>
</div>
 <ZNode:CustomerList ID="uxCustomer" runat="server" RoleName = "franchise" />
</asp:Content>
