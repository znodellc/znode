﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" CodeBehind="Edit.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Vendors.VendorAccounts.Edit" %>

<%@ Register Src="~/Controls/Default/Accounts/EditAccount.ascx" TagName="SiteAdmin" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <ZNode:SiteAdmin ID="uxCustomer" runat="server" PageFrom="admin" />

</asp:Content>
