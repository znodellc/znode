<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Vendors.VendorAccounts.AddNote" ValidateRequest="false"
    Title="Untitled Page" CodeBehind="AddNote.aspx.cs" %>

<%@ Register Src="~/Controls/Default/Accounts/AddNote.ascx" TagName="NotesAdd" TagPrefix="ZNode" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <ZNode:NotesAdd ID="uxNotesAdd" runat="server" RoleName="admin" />
</asp:Content>
