﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" CodeBehind="Default.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Vendors.VendorAccounts.Default" %>
<%@ Register Src="~/Controls/Default/Accounts/Customer.ascx" TagName="CustomerList" TagPrefix="ZNode" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div align="center">
            <div class="LeftFloat" style="width: 70%; text-align: left">
                <h1>
                   <asp:Localize ID="TitleVendorAccount" Text='<%$ Resources:ZnodeAdminResource,LinkTextVendorAccounts%>' runat="server"></asp:Localize></h1>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="LeftFloat">
                <p>
                   <asp:Localize ID="TextVendorAccount" Text='<%$ Resources:ZnodeAdminResource,TextVendorAccounts%>' runat="server"></asp:Localize>
                </p>
            </div>
            
            <div style="float: right;">
               <zn:LinkButton ID="AddMallAdmin" runat="server"  CausesValidation="false"  Text='<%$ Resources:ZnodeAdminResource,TitleAddVendorAccount%>' 
                OnCommand="AddMallAdmin_Click"
                ButtonType="Button" ButtonPriority="Primary"/>
            </div>
</div>
 <ZNode:CustomerList ID="uxCustomer" runat="server" RoleName = "vendor" />
</asp:Content>
