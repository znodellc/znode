<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Vendors.VendorAccounts.AddAffiliateSettings"
    Title="Affiliate Info" Codebehind="AddAffiliateSettings.aspx.cs" %>

<%@ Register Src="~/Controls/Default/Accounts/AffiliateSettings.ascx" TagName="AffiliateSettings" TagPrefix="ZNode" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <ZNode:AffiliateSettings ID="uxPermission" runat="server" RoleName = "admin" />

</asp:Content>
