﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service; 
using ZNode.Libraries.ECommerce.Analytics;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.ECommerce.UserAccount;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Vendors.VendorAccounts
{
    public partial class Add : System.Web.UI.Page
    {
		private MembershipProvider _adminMembershipProvider = Membership.Providers["ZNodeAdminMembershipProvider"];

	    public MembershipProvider AdminMembershipProvider
	    {
			get { return _adminMembershipProvider; }
			set { _adminMembershipProvider = value; }
	    }

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ibRegister.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit_highlight.gif") + "'");
            ibRegister.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_submit.gif") + "'");
            btnClear.Attributes.Add("onmouseover", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel_highlight.gif") + "'");
            btnClear.Attributes.Add("onmouseout", "this.src='" + Page.ResolveClientUrl("~/Themes/Images/buttons/button_cancel.gif") + "'");

            // Check if SSL is required
            if (ZNodeConfigManager.SiteConfig.UseSSL)
            {
                if (!Request.IsSecureConnection)
                {
                    string inURL = Request.Url.ToString();
                    string outURL = inURL.ToLower().Replace("http://", "https://");

                    Response.Redirect(outURL);
                }
            }

            if (this.Page != null)
            {
                ZNodeResourceManager resourceManager = new ZNodeResourceManager();
                this.Page.Title = resourceManager.GetLocalResourceObject(this.TemplateControl.AppRelativeVirtualPath, "txtAccountTitle.Text");
            }
        }

        protected override void OnPreRender(EventArgs e)
        {
            // ibRegister.ImageUrl = "~/themes/" + ZNodeCatalogManager.Theme + "/Images/Register.gif";        
        }
        #endregion

        #region Events
        /// <summary>
        /// Register Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LbRegister_Click(object sender, EventArgs e)
        {
            this.RegisterUser();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            string link = "~/Secure/Vendors/VendorAccounts/Default.aspx";
            Response.Redirect(link);
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// This function registers a new user 
        /// </summary>
        private void RegisterUser()
        {
            var transaction = ConnectionScope.CreateTransaction();
			var log = new ZNode.Libraries.ECommerce.Utilities.ZNodeLogging();
			var userAccount = new ZNodeUserAccount();
			var profileId = ZNodeProfile.DefaultRegisteredProfileId;
			var accountService = new AccountService();
			var accountAdmin = new AccountAdmin();
			var addressService = new AddressService();
			var newlyCreatedPassword = Membership.GeneratePassword(8, 1);
			MembershipCreateStatus createStatus;
			MembershipUser user;
			
			try
            {
                log.LogActivityTimerStart();

				// Check if UserName already exists with an Account
				if (!userAccount.IsLoginNameAvailable(ZNodeConfigManager.SiteConfig.PortalID, UserName.Text.Trim()))
				{
					log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text, null, null, "User name already exists", null);
                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorExistingLoginName").ToString();
					return;
				}

                if (profileId == 0)
                {
                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountProfile").ToString();
					log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text, null, null, "Profile is not setup correctly for current store.", null);
					return;
                }
				
				//TODO: Database can have emptystring for ExternalAccountNo, so adding length check, then proceeding
	            if (FranchiseNumber.Text.Length > 0)
				{
                    TList<Account> account = accountService.Find(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextExternalAccountNo").ToString(), FranchiseNumber.Text.Trim()));		
					if (account.Count > 0)
					{
						if (account[0].UserID != null)
						{
							user = Membership.GetUser(account[0].UserID);
							log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text);
                            ErrorMessage.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorExternalNumberAssociate").ToString(), FranchiseNumber.Text.Trim(), ZNodeConfigManager.SiteConfig.CustomerServiceEmail);
							return;
						}
					}
				}
				
				user = AdminMembershipProvider.CreateUser(Server.HtmlEncode(UserName.Text.Trim()), newlyCreatedPassword.Trim(), 
															txtBillingEmail.Text.Trim(), null, null, true, 
															Guid.NewGuid(), out createStatus);

				if (user == null)
                {
                    log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text);
                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorIfNoUser").ToString();
                    return;
                }
				
                // Create UserAccount
                var newUserAccount = new Account
	                {
		                UserID = (Guid) user.ProviderUserKey,
		                CompanyName = txtBillingCompanyName.Text,
		                ExternalAccountNo = FranchiseNumber.Text.Trim(),
		                Email = txtBillingEmail.Text.Trim(),
		                CreateDte = DateTime.Now,
		                UpdateDte = DateTime.Now,
		                CreateUser = HttpContext.Current.User.Identity.Name
	                };

	            // Register UserAccount
                if (newUserAccount.AccountID > 0)
                {
                    accountService.Update(newUserAccount);
                }
                else
                {
                    accountService.Insert(newUserAccount);
					accountAdmin.UpdateLastPasswordChangedDate(newUserAccount.UserID.ToString());
                }
				
				var address = accountAdmin.GetDefaultBillingAddress(newUserAccount.AccountID) ?? new Address
	                {
		                IsDefaultBilling = true,
		                IsDefaultShipping = true,
		                AccountID = newUserAccount.AccountID
	                };

	            // Copy info to billing                
                address.FirstName = txtBillingFirstName.Text;
                address.LastName = txtBillingLastName.Text;
                address.PhoneNumber = txtBillingPhoneNumber.Text.Trim();
                address.Street = txtBillingStreet1.Text.Trim();
                address.Street1 = txtBillingStreet2.Text.Trim();
                address.City = txtBillingCity.Text.Trim();
                address.CompanyName = txtBillingCompanyName.Text.Trim();
                address.StateCode = txtBillingState.Text.Trim();
                address.PostalCode = txtBillingPinCode.Text;

                // Default country code for United States
                address.CountryCode = "US";
                address.Name = "Default Address";

                // Register UserAccount Address & set address as default billing address.
                if (address.AddressID > 0)
                {
                    addressService.Update(address);
                }
                else
                {
                    addressService.Insert(address);
                }

				var accountProfile = new AccountProfile {AccountID = newUserAccount.AccountID, ProfileID = profileId};

	            var accountProfileServ = new AccountProfileService();
                
				accountProfileServ.Insert(accountProfile);

                Roles.AddUserToRole(UserName.Text.Trim(), "VENDOR");

                // Log password
                //ZNodeUserAccount.LogPassword((Guid)user.ProviderUserKey, newlyCreatedPassword);

                // Create an Profile for the selected user
                var newProfile = ProfileCommon.Create(UserName.Text, true);

                // Properties Value
                newProfile.StoreAccess = ZNodeConfigManager.SiteConfig.PortalID.ToString(CultureInfo.InvariantCulture);

                // Save profile - must be done since we explicitly created it 
                newProfile.Save();

                // Create a new record in the supplier table
                AddSupplier();
				
				//TODO: Send email, provide popup
	            var emailSent = SendNewUserEmail(newUserAccount.AccountID);
				
				if (!emailSent)
				{
					//Forcing commit even if email isn't delivered.
					if (transaction.IsOpen)
						transaction.Commit();

                    if (string.IsNullOrEmpty(ErrorMessage.Text))
                        ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCredential").ToString();
					log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text);
					return;
				}

                pnlCreateAccount.Visible = false;

                transaction.Commit();

				log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateSuccess, UserName.Text);

                Response.Redirect("~/Secure/Vendors/VendorAccounts/View.aspx?mode=0&itemid=" + newUserAccount.AccountID + "&pagefrom=vendor");
            }
            catch (Exception ex)
            {
	            if (transaction.IsOpen)
		            transaction.Rollback();

                ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogUserAccount").ToString();
				log.LogActivityTimerEnd((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.LoginCreateFailed, UserName.Text, null, null, ex.Message, null);
			}
        }

        private bool AddSupplier()
        {
            var supplierAdmin = new SupplierAdmin();
            var supplier = new Supplier
	            {
		            Name = txtBillingCompanyName.Text.Trim(),
		            Description = string.Empty,
		            ContactFirstName = txtBillingFirstName.Text.Trim(),
		            ContactLastName = txtBillingLastName.Text.Trim(),
		            ContactPhone = txtBillingPhoneNumber.Text,
		            ContactEmail = txtBillingEmail.Text,
		            NotificationEmailID = txtBillingEmail.Text,
		            EmailNotificationTemplate = string.Empty,
		            EnableEmailNotification = true,
		            DisplayOrder = 500,
		            ActiveInd = true,
		            Custom1 = string.Empty,
		            Custom2 = string.Empty,
		            Custom3 = string.Empty,
		            Custom4 = string.Empty,
		            Custom5 = string.Empty,
		            ExternalSupplierNo = FranchiseNumber.Text.Trim(),
					SupplierTypeID = 1 // Assign Default Supplier Type Id
	            };

	        var check = supplierAdmin.Insert(supplier);

            // Log Activity
            var associateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogCreateSupplier").ToString() + txtBillingCompanyName.Text;
            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associateName, txtBillingCompanyName.Text);

            return check;
        }
        #endregion

		/// <summary>
		/// Sends a new password in an email to the newly created account by id
		/// </summary>
		/// <param name="accountId">Takes an integer for the accountId parameter.</param>
		protected bool SendNewUserEmail(int accountId)
		{
			var accountAdmin = new AccountAdmin();
			var portalId = ZNodeConfigManager.SiteConfig.PortalID;
			var portalAdmin = new PortalAdmin();
			var portal = portalAdmin.GetByPortalId(portalId);
			var storeName = portal.StoreName;
			string body;

			var account = accountAdmin.GetByAccountID(accountId);

			if (account == null || !account.UserID.HasValue) return false;

			var user = AdminMembershipProvider.GetUser(account.UserID.Value, false);

			if (user == null)
			{
                ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCreateEmail").ToString();
				ZNodeLoggingBase.LogMessage(ErrorMessage.Text);
				return false;
			}

			if (String.IsNullOrEmpty(ZNodeConfigManager.SiteConfig.SMTPServer))
			{
                ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogSMTPSettings").ToString() +storeName;
				ZNodeLoggingBase.LogMessage(ErrorMessage.Text);
				return false;
			}

			var senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            var subject = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextNewUserCreation").ToString(), storeName);
			var toAddress = account.Email;

			var currentCulture = ZNodeCatalogManager.CultureInfo;

			var defaultTemplatePath =
				Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath, "NewUserAccount_en.htm"));

			var templatePath = currentCulture != string.Empty
								   ? Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "NewUserAccount_" +
													currentCulture + ".htm")
								   : Server.MapPath(Path.Combine(ZNodeConfigManager.EnvironmentConfig.ConfigPath,
																 "NewUserAccount_en.htm"));

			// Check for language specific file existence.
			if (!File.Exists(templatePath))
			{
				// Check the default template file existence.
				if (!File.Exists(defaultTemplatePath))
				{
                    ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountCreateTemplateFile").ToString();
					return false;
				}
				templatePath = defaultTemplatePath;
			}

			using (var streamReader = new StreamReader(templatePath))
			{
				body = streamReader.ReadToEnd();
				
			}
			try
			{
				var userName = user.UserName;
				var regularExpName = new Regex("#UserName#", RegexOptions.IgnoreCase);
				body = regularExpName.Replace(body, userName);

				var userPassword = user.GetPassword();
				var regularExpPassword = new Regex("#Password#", RegexOptions.IgnoreCase);
				body = regularExpPassword.Replace(body, userPassword);

				var regularUrl = new Regex("#Url#", RegexOptions.IgnoreCase);
               
                var vendorLoginUrl = WebConfigurationManager.AppSettings["AdminWebsiteUrl"] + "/malladmin";
                if (string.IsNullOrEmpty(WebConfigurationManager.AppSettings["AdminWebsiteUrl"]))
                {
                    vendorLoginUrl = "http://" + ZNodeConfigManager.DomainConfig.DomainName + "/malladmin";
                }
                body = regularUrl.Replace(body, vendorLoginUrl);
				ZNodeEmail.SendEmail(toAddress, senderEmail, String.Empty, subject, body, true);
			}
			catch (Exception ex)
			{
                ErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogPasswordMail").ToString();
				ZNodeLoggingBase.LogMessage(ex.Message);
				return false;
			}

			return true;
		}
    }
}
