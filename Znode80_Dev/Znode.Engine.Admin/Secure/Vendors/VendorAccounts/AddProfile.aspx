﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="true" CodeBehind="AddProfile.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Vendors.VendorAccounts.AddProfile" %>

<%@ Register Src="~/Controls/Default/Accounts/Profile.ascx" TagName="Profile" TagPrefix="ZNode" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
 <ZNode:Profile ID="uxProfile" runat="server" RoleName = "admin" />
</asp:Content>
