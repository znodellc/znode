<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="true" Title="Access Denied" %>

<%@ Register Src="~/Controls/Default/spacer.ascx" TagName="spacer" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h1>
        <asp:Localize ID="TitleAccessDenied" Text='<%$ Resources:ZnodeAdminResource,TitleRequest%>' runat="server"></asp:Localize>
    </h1>
    <asp:Localize ID="TextFeature" Text='<%$ Resources:ZnodeAdminResource,TextFeature%>' runat="server"></asp:Localize>
    <div>
        <uc1:spacer ID="Spacer1" SpacerHeight="20" SpacerWidth="10" runat="server"></uc1:spacer>
    </div>
    <div>
        <a href="javascript: history.go(-1)">
            <asp:Localize ID="LinkBack" Text='<%$ Resources:ZnodeAdminResource,LinkBack%>' runat="server"></asp:Localize>
        </a>
    </div>
    <div>
        <uc1:spacer ID="Spacer8" SpacerHeight="200" SpacerWidth="10" runat="server"></uc1:spacer>
    </div>
</asp:Content>

