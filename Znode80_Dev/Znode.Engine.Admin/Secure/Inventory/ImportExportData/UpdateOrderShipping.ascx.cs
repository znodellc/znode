using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the SiteAdmin - PlugIns_DataManager_UpdateOrderShipping class
    /// </summary>
    public partial class UpdateOrderShipping : System.Web.UI.UserControl
    {
        #region Events

        /// <summary>
        /// Event fired when Submit button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            Stream fileStream = Request.Files[0].InputStream;
            StreamReader fileReader = new StreamReader(fileStream);

            int ErrorCount = 0;
            int SqlErrorCount = 0;
            bool status = false;
            int indexRowCount = 0;
            int parsedInt;
            DateTime parsedDate = new DateTime();
            int MaxErrorCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ImportMaxErrorCount"].ToString());
            ZNodeShippingStatusDataSet impShipStatus = new ZNodeShippingStatusDataSet();
            string dataRecord = fileReader.ReadLine();
            if (dataRecord == null)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordsFound").ToString();
                return;
            }

            dataRecord = fileReader.ReadLine();

            while (dataRecord != null)
            {
                indexRowCount++;

                if (ErrorCount >= MaxErrorCount)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogMaximumCount").ToString(), MaxErrorCount.ToString()));
                    ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateFailed").ToString();
                    return;
                }

                string[] data = dataRecord.Split('|');

                if (data.Length != 3)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogInputFieldMissing").ToString(), indexRowCount));
                    ErrorCount++;

                    dataRecord = fileReader.ReadLine();
                    continue;
                }

                try
                {
                    ZNodeShippingStatusDataSet.ZNodeShippingStatusRow dr = impShipStatus.ZNodeShippingStatus.NewZNodeShippingStatusRow();
                    dr.OrderId = 0;
                    if (int.TryParse(data[0], out parsedInt))
                    {
                        dr.OrderId = parsedInt;
                    }

                    if (DateTime.TryParse(data[1], out parsedDate))
                    {
                        dr.ShipDate = parsedDate;
                    }

                    dr.TrackingNumber = data[2].ToString();

                    impShipStatus.ZNodeShippingStatus.AddZNodeShippingStatusRow(dr);
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                    ErrorCount++;
                }

                dataRecord = fileReader.ReadLine();
            }

            if (impShipStatus.ZNodeShippingStatus != null && impShipStatus.ZNodeShippingStatus.Rows.Count == 0)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordsFound").ToString();
                return;
            }

            if (impShipStatus.Tables[0].Rows.Count > 0)
            {
                DataManagerAdmin managerAdmin = new DataManagerAdmin();
                status = managerAdmin.UploadShippingstatus(impShipStatus.ZNodeShippingStatus, out SqlErrorCount);
            }

            if ((ErrorCount > 0 || SqlErrorCount > 0) && status)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorRowsNotUpdated").ToString();
                return;
            }

            if (!status)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateFailed").ToString();
            }
            else
            {
                Response.Redirect("~/Secure/Inventory/ImportExportData/Default.aspx");
            }
        }

        /// <summary>
        /// Event fired when Cancel button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Inventory/ImportExportData/Default.aspx");
        }
        #endregion
    }
}