using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace  Znode.Engine.Admin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the SiteAdmin - PlugIns_DataManager_DownloadAttributes class
    /// </summary>
    public partial class DownloadAttributes : System.Web.UI.UserControl
    {
        #region Private member Variables
        private Helper adminHelper = new Helper();
        private AttributeTypeHelper helper = new AttributeTypeHelper();
        private string DefaultPageLink = "~/Secure/Inventory/ImportExportData/default.aspx";
        #endregion

        #region Helper Methods
        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="strFileName">The value of file name</param>
        /// <param name="gridViewControl">The Grid View Control</param>
        public void ExportDataToExcel(string strFileName, GridView gridViewControl)
        {
            Response.Clear();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + strFileName);

            // Set as Excel as the primary format
            Response.AddHeader("Content-Type", "application/Excel");
            Response.ContentType = "application/Excel";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            // Remove controls from Column Headers
            if (gridViewControl.HeaderRow != null && gridViewControl.HeaderRow.Cells != null)
            {
                for (int rw = 0; rw < gridViewControl.Rows.Count; rw++)
                {
                    GridViewRow row = gridViewControl.Rows[rw];
                    for (int ct = 0; ct < row.Cells.Count; ct++)
                    {
                        // Save header text if found
                        string headerText = row.Cells[ct].Text;

                        // Check for controls in header
                        if (row.Cells[ct].HasControls())
                        {
                            // Check for link buttons (used in sorting)
                            if (row.Cells[ct].Controls[0].GetType().ToString() == "System.Web.UI.WebControls.CheckBox")
                            {
                                // Link button found, get text
                                headerText = ((CheckBox)row.Cells[ct].Controls[0]).Checked.ToString();
                            }

                            // Remove controls from header
                            row.Cells[ct].Controls.Clear();
                        }

                        // Reassign header text
                        row.Cells[ct].Text = headerText;
                    }
                }
            }

            gridViewControl.RenderControl(htw);
            Response.Write(sw.ToString());

            gridViewControl.Dispose();

            Response.End();
        }

        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="fileName">The value of filename</param>
        /// <param name="Data">The value of Data</param>
        public void ExportDataToCSV(string fileName, byte[] Data)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.AddHeader("Content-Type", "application/Excel");

            // Set as text as the primary format
            Response.ContentType = "text/csv";
            Response.ContentType = "application/vnd.xls";
            Response.AddHeader("Pragma", "public");

            Response.BinaryWrite(Data);
            Response.End();
        }
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(ltrlError.Text.Trim()))
                {
                    lnkLog.Visible = true;
                }
                else
                {
                    lnkLog.Visible = false;
                }

                // Bind Catalogs
                this.BindCatalog();

                // Bind Attribute List
                this.BindList();
            }
        }

        #endregion

        #region Protected Methods and Events
        /// <summary>
        /// Bind Catalog List
        /// </summary>
        protected void BindCatalog()
        {
            CatalogAdmin admin = new CatalogAdmin();
            ListItem listitem = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), "0");

            DataSet ds = admin.GetAllCatalogs().ToDataSet(false);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                }
            }

            DataView dataView = ds.Tables[0].DefaultView;
            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                dataView.RowFilter = string.Concat("CatalogID IN (", admin.GetCatalogIDsByPortalIDs(profiles.StoreAccess), ")");
            }

            ddlCatalog.DataSource = dataView;
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogId";
            ddlCatalog.DataBind();

            if (string.Compare(profiles.StoreAccess, "AllStores") == 0)
            {
                ddlCatalog.Items.Insert(0, listitem);
            }
        }

        /// <summary>
        /// Bind Attribute Data
        /// </summary>
        /// <returns>Returns the DataSet</returns>
        protected DataSet BindAttributeData()
        {
            int attributeTypeId = int.Parse(lstAttributeTypeList.SelectedValue);

            ProductAttributeService attributeService = new ProductAttributeService();
            TList<ProductAttribute> attribute;

            if (ddlCatalog.SelectedValue != "0" && attributeTypeId == 0)
            {
                DataSet ds = this.helper.GetAttributesByCatalogID(int.Parse(ddlCatalog.SelectedValue));
                return ds;
            }
            else if (attributeTypeId != 0)
            {
                attribute = attributeService.GetByAttributeTypeId(attributeTypeId);
            }
            else
            {
                attribute = attributeService.GetAll();
            }

            return attribute.ToDataSet(true);
        }

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            DataDownloadAdmin adminAccess = new DataDownloadAdmin();

            try
            {
                if (lstAttributeTypeList.SelectedValue.ToLower() == "none")
                {
                    ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoAttributesFound").ToString();
                    return;
                }

                DataSet ds = this.BindAttributeData();

                if (ds.Tables[0].Rows.Count != 0)
                {
                    // Save as Excel Sheet
                    if (ddlFileSaveType.SelectedValue == ".xls")
                    {
                        // Temp Grid control
                        GridView gridView = new GridView();
                        gridView.DataSource = ds;
                        gridView.DataBind();
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Attributes", "Attributes");

                        this.ExportDataToExcel("Attributes.xls", gridView);
                    }
                    else
                    {
                        // Save as CSV Format
                        // Set Formatted Data from dataset object
                        string strData = adminAccess.Export(ds, true);

                        byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(strData);
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Attributes", "Attributes");

                        this.ExportDataToCSV("Attributes.csv", data);
                    }
                }
                else
                {
                    ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDownloadAtribute").ToString();
                    return;
                }
            }
            catch (Exception ex)
            {
                lnkLog.Visible = true;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFailedRequest").ToString();
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.DefaultPageLink);
        }

        /// <summary>
        /// Catalog Dropdown Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlCatalog_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindList();
            ltrlError.Text = string.Empty;
        }

        /// <summary>
        /// Attribute Type Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstAttributeTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ltrlError.Text = string.Empty;
        }

        #endregion

        #region Bind Methods
        /// <summary>
        /// Binds the Attribute type DropDownList
        /// </summary>
        private void BindList()
        {
            AttributeTypeAdmin attributeAdmin = new AttributeTypeAdmin();
            lstAttributeTypeList.Items.Clear();
            DataSet ds = new DataSet();

            if (ddlCatalog.SelectedValue == "0")
            {
                ds = attributeAdmin.GetAll().ToDataSet(false);
            }
            else
            {
                ds = this.helper.GetAttributesByCatalogID(int.Parse(ddlCatalog.SelectedValue));
            }

            if (ds.Tables[0].Rows.Count > 0)
            {
                lstAttributeTypeList.DataSource = ds.Tables[0].DefaultView;
                lstAttributeTypeList.DataTextField = "Name";
                lstAttributeTypeList.DataValueField = "AttributeTypeId";
                lstAttributeTypeList.DataBind();
                ListItem Li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), "0");
                lstAttributeTypeList.Items.Insert(0, Li);
            }
            else
            {
                ListItem Li = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordFound").ToString(), "None");
                lstAttributeTypeList.Items.Insert(0, Li);
            }
        }
        #endregion
    }
}