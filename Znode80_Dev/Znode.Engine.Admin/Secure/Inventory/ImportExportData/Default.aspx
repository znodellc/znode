<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="true" %>

<script runat="server">

    protected void Page_Init(object sender, EventArgs e)
    {
        //Session["SelectedMenuItem"] = Request.Url.PathAndQuery;
    }
    
</script>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <h1><asp:Localize runat="server" ID="Export" Text='<%$ Resources:ZnodeAdminResource, TitleExport %>'></asp:Localize></h1>
         <hr /> 

        <div class="ImageAlign">
             <div class="ImportExportDownloadsImage">
                <img src="/Themes/Images/DownloadInventory.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A5" href="~/Secure/Inventory/ImportExportData/Download.aspx?filter=Inventory" runat="server"><asp:Localize runat="server" ID="ExportInventory" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportInventory %>'></asp:Localize> </a></div>
         
        </div>
       
        <div class="ImageAlign">
             <div class="ImportExportDownloadsImage">
                <img src="/Themes/Images/DownloadPricing.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A4" href="~/Secure/Inventory/ImportExportData/Download.aspx?filter=pricing" runat="server"><asp:Localize runat="server" ID="ExportPricing" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportPricing %>'></asp:Localize></a></div>
          
        </div>
        
        <div class="ImageAlign">
             <div class="ImportExportDownloadsImage">
                <img src="/Themes/Images/DownloadProducts.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A1" href="~/Secure/Inventory/ImportExportData/Download.aspx?filter=Product" runat="server"><asp:Localize runat="server" ID="ExportProducts" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportProducts %>'></asp:Localize></a></div>
         
        </div>
          
        <div class="ImageAlign">
             <div class="ImportExportDownloadsImage">
                <img src="/Themes/Images/DownloadAttributes.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A11" href="~/Secure/Inventory/ImportExportData/DownloadAttributes.aspx" runat="server"><asp:Localize runat="server" ID="ExportAttributes" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportAttributes %>'></asp:Localize> </a></div>
           
        </div>
        
        <div class="ImageAlign">
             <div class="ImportExportDownloadsImage">
                <img src="/Themes/Images/DownloadSKU.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A8" href="~/Secure/Inventory/ImportExportData/DownloadSku.aspx" runat="server"><asp:Localize runat="server" ID="ExportSKUs" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportSKUs %>'></asp:Localize></a></div>
           
        </div>
         
        <div class="ImageAlign">
             <div class="ImportExportDownloadsImage">
                <img src="/Themes/Images/DownloadFacets.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A12" href="~/Secure/Inventory/ImportExportData/DownloadFacets.aspx" runat="server"><asp:Localize runat="server" ID="ExportFacets" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportFacets %>'></asp:Localize> </a></div>
           
        </div>
        
        <div class="ImageAlign">
             <div class="ImportExportDownloadsImage">
                <img src="/Themes/Images/DownloadShippingStatus.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A14" href="~/Secure/Inventory/ImportExportData/DownloadOrderShipping.aspx" runat="server"><asp:Localize runat="server" ID="ExportShippingStatus" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportShippingStatus %>'></asp:Localize> </a></div>
          
        </div> 
        
          <div class="ImageAlign">
             <div class="ImportExportDownloadsImage">
                <img src="/Themes/Images/DownloadPricing.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A16" href="~/Secure/Inventory/ImportExportData/DownloadCustomerPricing.aspx" runat="server"><asp:Localize runat="server" ID="ExportCustomerPricing" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportCustomerPricing %>'></asp:Localize> </a></div>
          
        </div> 
        
        <h1><asp:Localize runat="server" ID="Import" Text='<%$ Resources:ZnodeAdminResource, TitleImport %>'></asp:Localize> </h1>
        <p><asp:Localize runat="server" ID="TextImport" Text='<%$ Resources:ZnodeAdminResource, TitleTextImport %>'></asp:Localize> </p>
        <hr />
        <div class="ImageAlign">
             <div class="ImportExportImage">
                <img src="/Themes/Images/UploadInventory.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A2" href="~/Secure/Inventory/ImportExportData/UpdateInventory.aspx" runat="server"><asp:Localize runat="server" ID="ImportInventory" Text='<%$ Resources:ZnodeAdminResource, LinkTextImportInventory %>'></asp:Localize>  </a></div>
           
        </div>
        
        <div class="ImageAlign">
             <div class="ImportExportImage">
                <img src="/Themes/Images/UploadPricing.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A6" href="~/Secure/Inventory/ImportExportData/UpdatePricing.aspx" runat="server"> <asp:Localize runat="server" ID="ImportPricing" Text='<%$ Resources:ZnodeAdminResource, LinkTextImportPricing %>'></asp:Localize> </a></div>
           
        </div>
        
        <div class="ImageAlign">
             <div class="ImportExportImage">
                <img src="/Themes/Images/UploadProducts.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A20" href="~/Secure/Inventory/ImportExportData/UploadProduct.aspx" runat="server"> <asp:Localize runat="server" ID="ImportProducts" Text='<%$ Resources:ZnodeAdminResource, LinkTextImportProducts %>'></asp:Localize> </a></div>
           
        </div>
        
        <div class="ImageAlign">
             <div class="ImportExportImage">
                <img src="/Themes/Images/UploadAttributes.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A13" href="~/Secure/Inventory/ImportExportData/UploadAttributes.aspx" runat="server"> <asp:Localize runat="server" ID="ImportAttributes" Text='<%$ Resources:ZnodeAdminResource, LinkTextImportAttributes %>'></asp:Localize> </a></div>
          
        </div>
       
        <div class="ImageAlign">
             <div class="ImportExportImage">
                <img src="/Themes/Images/UploadSKU.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A9" href="~/Secure/Inventory/ImportExportData/UpdateSku.aspx" runat="server"> <asp:Localize runat="server" ID="ImportSKUs" Text='<%$ Resources:ZnodeAdminResource, LinkTextImportSKUs %>'></asp:Localize> </a></div>
           
        </div>
         
        <div class="ImageAlign">
             <div class="ImportExportImage">
                <img src="/Themes/Images/UploadFacets.png" />
            </div>
            <div class="ImportExportShortcuts" ><a id="A10" href="~/Secure/Inventory/ImportExportData/UploadFacets.aspx" runat="server"> <asp:Localize runat="server" ID="ImportFacets" Text='<%$ Resources:ZnodeAdminResource, LinkTextImportFacets %>'></asp:Localize> </a></div>
          
        </div>
         
        <div class="ImageAlign">
             <div class="ImportExportImage">
                <img src="/Themes/Images/UploadShippingStatus.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A3" href="~/Secure/Inventory/ImportExportData/UpdateOrderShipping.aspx" runat="server"> <asp:Localize runat="server" ID="ImportShippingStatus" Text='<%$ Resources:ZnodeAdminResource, LinkTextImportShippingStatus %>'></asp:Localize> </a></div>
          
        </div>
        
        <div class="ImageAlign">
             <div class="ImportExportImage" >
                <img src="/Themes/Images/UploadZipCodeData.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A7" href="~/Secure/Inventory/ImportExportData/UploadZipcode.aspx" runat="server"><asp:Localize runat="server" ID="ImportZipCodeData" Text='<%$ Resources:ZnodeAdminResource, LinkTextImportZipCodeData %>'></asp:Localize> </a></div>
           
        </div>

          <div class="ImageAlign">
             <div class="ImportExportImage">
                <img src="/Themes/Images/UploadPricing.png" />
            </div>
            <div class="ImportExportShortcuts"><a id="A15" href="~/Secure/Inventory/ImportExportData/UploadCustomerPricing.aspx" runat="server"><asp:Localize runat="server" ID="ImportCustomerPricing" Text='<%$ Resources:ZnodeAdminResource, LinkTextImportCustomerPricing %>'></asp:Localize></a></div>
          
        </div>
        
  </div>
</asp:Content>
