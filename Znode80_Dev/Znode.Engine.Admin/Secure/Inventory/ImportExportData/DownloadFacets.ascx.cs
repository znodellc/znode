using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;

namespace  Znode.Engine.Admin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the Download Facets - PlugIns_DataManager_DownloadFacets class
    /// </summary>
    public partial class DownloadFacets : System.Web.UI.UserControl
    {
        #region Private member Variables
        private string DefaultPageLink = "~/Secure/Inventory/ImportExportData/Default.aspx";
        private Helper adminHelper = new Helper();
        private CategoryHelper categoryhelper = new CategoryHelper();
        #endregion

        #region Helper Methods
        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="strFileName">The value of strFileName</param>
        /// <param name="gridViewControl">The value of Grid View Control</param>
        public void ExportDataToExcel(string strFileName, GridView gridViewControl)
        {
            try
            {
                Response.ClearContent();
                Response.AddHeader("content-disposition", "attachment; filename=" + strFileName);

                // Set as Excel as the primary format
                Response.AddHeader("Content-Type", "application/Excel");
                Response.ContentType = "application/Excel";
                System.IO.StringWriter sw = new System.IO.StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                gridViewControl.RenderControl(htw);
                Response.Write(sw.ToString());

                gridViewControl.Dispose();

                Response.End();
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
            }

            Response.Clear();
        }

        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="fileName">The value of FileName</param>
        /// <param name="Data">The value of Data</param>
        public void ExportDataToCSV(string fileName, byte[] Data)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.AddHeader("Content-Type", "application/Excel");

            // Set as text as the primary format
            Response.ContentType = "text/csv";
            Response.ContentType = "application/vnd.xls";
            Response.AddHeader("Pragma", "public");

            Response.BinaryWrite(Data);
            Response.End();
        }

        #endregion

        #region Page_Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (!string.IsNullOrEmpty(ltrlError.Text.Trim()))
                {
                    lnkLog.Visible = true;
                }
                else
                {
                    lnkLog.Visible = false;
                }

                // Bind Catalogs
                this.BindCatalog();
            }

            ltrlError.Text = string.Empty;
        }

        #endregion

        #region Events
        /// <summary>
        /// Event fired when Download Inventory Button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
		protected void BtnDownloadFacetInventory_Click(object sender, EventArgs e)
        {
            DataDownloadAdmin adminAccess = new DataDownloadAdmin();
            DataManagerAdmin datamanagerAdmin = new DataManagerAdmin();

            try
            {
                int lstCategoryValue = 0;
                if (lstCategory.SelectedValue != string.Empty)
                {
                    lstCategoryValue = Convert.ToInt32(lstCategory.SelectedValue);
                }

                DataSet ds = datamanagerAdmin.GetDownloadFacets(lstCategoryValue, Convert.ToInt32(ddlCatalog.SelectedValue));

                if (ds.Tables[0].Rows.Count != 0)
                {
                    ZNodeFacetsDataSet downloadFacets = new ZNodeFacetsDataSet();
                    int parsedInt;

                    foreach (DataRow data in ds.Tables[0].Rows)
                    {
                        ZNodeFacetsDataSet.ZNodeFacetRow dataRow = downloadFacets.ZNodeFacet.NewZNodeFacetRow();

                        dataRow.FacetGroupLabel = data["FacetGroupLabel"].ToString();
                        dataRow.ControlType = data["ControlType"].ToString();
                        if (int.TryParse(data["CatalogID"].ToString(), out parsedInt))
                        {
                            dataRow.CatalogID = parsedInt;
                        }

                        if (int.TryParse(data["DisplayOrder"].ToString(), out parsedInt))
                        {
                            dataRow.DisplayOrder = parsedInt;
                        }

                        dataRow.FacetName = data["FacetName"].ToString().ToString();

                        if (int.TryParse(data["FacetDisplayOrder"].ToString(), out parsedInt))
                        {
                            dataRow.FacetDisplayOrder = parsedInt;
                        }

                        dataRow.IconPath = data["IconPath"].ToString();

                        if (int.TryParse(data["ProductID"].ToString(), out parsedInt))
                        {
                            dataRow.ProductID = parsedInt;
                        }

                        if (int.TryParse(data["CategoryID"].ToString(), out parsedInt))
                        {
                            dataRow.CategoryID = parsedInt;
                        }

                        if (int.TryParse(data["CategoryDisplayOrder"].ToString(), out parsedInt))
                        {
                            dataRow.CategoryDisplayOrder = parsedInt;
                        }

                        downloadFacets.ZNodeFacet.AddZNodeFacetRow(dataRow);
                    }

                    // Save as Excel Sheet
                    if (ddlFileSaveType.SelectedValue == ".xls")
                    {
                        // Temp Grid control
                        GridView gridView = new GridView();
                        gridView.DataSource = downloadFacets.ZNodeFacet;
                        gridView.DataBind();
						ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Facets", "Facets");

                        this.ExportDataToExcel("Facets.xls", gridView);
                    }
                    else
                    {
                        // Save as CSV Format
                        // Set Formatted Data from dataset object
                        string strData = adminAccess.Export(ds, true);

                        byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(strData);

						ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Facets", "Facets");

						this.ExportDataToCSV("Facets.csv", data);
                    }
                }
                else
                {
                    ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotFoundFacet").ToString();
                    return;
                }
            }
            catch (Exception ex)
            {
                lnkLog.Visible = true;
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFailedRequest").ToString();
            }
        }

        /// <summary>
        /// Event fired when Cancel Button is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.DefaultPageLink);
        }

        #endregion

        #region Bind Methods

        /// <summary>
        /// Bind Catalog List
        /// </summary>
        protected void BindCatalog()
        {
            CatalogAdmin admin = new CatalogAdmin();
            ListItem listitem = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), "0");

            DataSet ds = admin.GetAllCatalogs().ToDataSet(false);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                }
            }

            DataView dataView = ds.Tables[0].DefaultView;
            ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);

            if (string.Compare(profiles.StoreAccess, "AllStores") != 0)
            {
                dataView.RowFilter = string.Concat("CatalogID IN (", admin.GetCatalogIDsByPortalIDs(profiles.StoreAccess), ")");
            }

            ddlCatalog.DataSource = dataView;
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogId";
            ddlCatalog.DataBind();

            if (string.Compare(profiles.StoreAccess, "AllStores") == 0)
            {
                ddlCatalog.Items.Insert(0, listitem);
            }

            int CatalogId = Convert.ToInt32(ddlCatalog.SelectedValue);
            this.BindCategory(CatalogId);
        }

        /// <summary>
        /// Catalog Dropdown Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Ddlcatalog_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            int CatalogId = Convert.ToInt32(ddlCatalog.SelectedValue);
            this.BindCategory(CatalogId);
            ltrlError.Text = string.Empty;
        }

        /// <summary>
        /// Category List Control Selected Index Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void LstCategory_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            ltrlError.Text = string.Empty;
        }

        /// <summary>
        /// Bind Category Method
        /// </summary>
        /// <param name="CatalogId">The value of Catalog Id</param>
        protected void BindCategory(int CatalogId)
        {
            ZNode.Libraries.DataAccess.Custom.CategoryHelper categoryHelper = new ZNode.Libraries.DataAccess.Custom.CategoryHelper();
            DataSet categoryDs = new DataSet();
            if (CatalogId > 0)
            {
                categoryDs = categoryHelper.GetCategoryByCatalogID(CatalogId);
            }
            else
            {
                categoryDs = categoryHelper.GetCategories();
            }

            if (categoryDs.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < categoryDs.Tables[0].Rows.Count; index++)
                {
                    categoryDs.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(categoryDs.Tables[0].Rows[index]["Name"].ToString());
                }
            }

            lstCategory.DataSource = categoryDs;
            lstCategory.DataTextField = "Name";
            lstCategory.DataValueField = "CategoryID";
            lstCategory.DataBind();
            ListItem allItem = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString(), string.Empty);
            lstCategory.Items.Insert(0, allItem);
        }

        #endregion
    }
}