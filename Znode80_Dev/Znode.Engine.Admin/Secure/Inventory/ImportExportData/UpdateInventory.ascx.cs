using System;
using System.IO;
using System.Text;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;

namespace  Znode.Engine.Admin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the SiteAdmin -  PlugIns_DataManager_UpdateInventory class
    /// </summary>
    public partial class UpdateInventory : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private int ErrorCount = 0;
        private StringBuilder errorMessage = new StringBuilder();
        #endregion

        #region Events
        /// <summary>
        /// Event fierd when Submit button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            Stream fileStream = Request.Files[0].InputStream;
            StreamReader fileReader = new StreamReader(fileStream);

            ZNodeSKUInventoryDataset impSKUInventory = new ZNodeSKUInventoryDataset();
            string dataRecord = fileReader.ReadLine();

            if (dataRecord == null)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateFailed").ToString();
                return;
            }

            dataRecord = fileReader.ReadLine();

            int SqlErrorCount = 0;
            bool status = false;
            int parsedInt;
            int indexRowCount = 0;
            int MaxErrorCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ImportMaxErrorCount"].ToString());

            while (dataRecord != null)
            {
                indexRowCount++;

                if (ErrorCount >= MaxErrorCount)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogUploadProduct").ToString());
                    ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateFailed").ToString();
                    return;
                }

                try
                {
                    string[] data = dataRecord.Split('|');

                    if (data.Length != 3)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogInputFieldMissing").ToString(), indexRowCount));
                        ErrorCount++;
                        errorMessage = errorMessage.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInputFieldsMissing").ToString() + indexRowCount);
                        dataRecord = fileReader.ReadLine();
                        continue;
                    }

                    ZNodeSKUInventoryDataset.ZNodeSKUInventoryRow dataRow = impSKUInventory.ZNodeSKUInventory.NewZNodeSKUInventoryRow();
                    dataRow.SKU = data[0];

                    if (data[1].ToString() != string.Empty)
                    {
                        if (int.TryParse(data[1], out parsedInt))
                        {
                            dataRow.QuantityOnHand = parsedInt;
                        }
                        else
                        {
                            DisplayError("QuantityOnHand", indexRowCount);
                            break;
                        }
                    }

                    if (data[2].ToString() != string.Empty)
                    {
                        if (int.TryParse(data[2], out parsedInt))
                        {
                            dataRow.ReOrderLevel = parsedInt;
                        }
                        else
                        {
                            DisplayError("ReOrderLevel", indexRowCount);
                            break;
                        }
                    }

                    impSKUInventory.ZNodeSKUInventory.AddZNodeSKUInventoryRow(dataRow);
                }
                catch (Exception exception)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(exception.ToString());
                    ErrorCount++;
                }

                dataRecord = fileReader.ReadLine();
            }
           
                if (impSKUInventory.ZNodeSKUInventory.Rows.Count > 0)
                {
                    DataManagerAdmin managerAdmin = new DataManagerAdmin();
                    status = managerAdmin.UploadInventory(impSKUInventory.ZNodeSKUInventory, out SqlErrorCount);
                }
           

            if ((ErrorCount > 0 || SqlErrorCount > 0) && status)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFileUpload").ToString() + errorMessage;              
                return;
            }

            if (!status)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateFailed").ToString();
            }
            else if (status)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Exported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Upload Inventory", "Inventory");
                Response.Redirect("~/Secure/Inventory/ImportExportData/Default.aspx");
            }
        }

        /// <summary>
        /// Event fierd when Cancel button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Inventory/ImportExportData/Default.aspx");
        }

        #endregion

        #region Private Methods

        private void DisplayError(string columnName, int row)
        {
            ZNodeLoggingBase.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogUploadInventory").ToString(),columnName, row));
            ErrorCount++;
            errorMessage = errorMessage.Append(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogUploadInventory").ToString(), columnName , row));
        }

        #endregion

    }
}