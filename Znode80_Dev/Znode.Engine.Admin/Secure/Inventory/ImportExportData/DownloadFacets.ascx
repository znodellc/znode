<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="Znode.Engine.Admin.Secure.Inventory.ImportExportData.DownloadFacets" CodeBehind="DownloadFacets.ascx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<div class="Form">
    <h1>
        <asp:Localize runat="server" ID="ExportFacets" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportFacets %>'></asp:Localize></h1>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div class="SearchForm">
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="Catalog" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCatalog %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlCatalog" AutoPostBack="true" OnSelectedIndexChanged="Ddlcatalog_OnSelectedIndexChanged"
                        runat="server">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="Category" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCategoryName %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="lstCategory" AutoPostBack="true" runat="server" OnSelectedIndexChanged="LstCategory_OnSelectedIndexChanged">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer5" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="FileType" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFileType %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlFileSaveType" runat="server">
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextFileSaveXls %>' Value=".xls"></asp:ListItem>
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextFileSaveCsv %>' Value=".csv" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="ClearBoth">
            <div class="Error">
                <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
            </div>
            <div>
                <a href="../../../Data/Default/Logs/ZnodeLog.txt" id="lnkLog" runat="server"
                    visible="false" target="_blank"><asp:Localize runat="server" ID="ViewLog" Text='<%$ Resources:ZnodeAdminResource, LinkTextViewLog %>'></asp:Localize></a>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer4" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth">
            </div>
            <div>
                  <zn:Button ID="btnDownloadFacet" runat="server" Width="120px" ButtonType="EditButton" OnClick="BtnDownloadFacetInventory_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonExportFacets %>' />
                  <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
            </div>
        </div>
    </div>
</div>
