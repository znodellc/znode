<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Inventory.ImportExportData.UpdateSku" CodeBehind="UpdateSku.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<div class="Form SearchForm">
    <h1>
        <asp:Localize runat="server" ID="ImportIventory" Text='<%$ Resources:ZnodeAdminResource, TitleImportSKUData %>'></asp:Localize></h1>
    <div>
        <ZNode:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div class="RowStyle">
        <div class="ItemStyle">
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="DelimitedFile" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectDelimitedFile %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <input type="file" id="UploadFile" runat="server" width="350px" />
                <asp:RequiredFieldValidator Display="dynamic" ControlToValidate="UploadFile" CssClass="Error"
                    ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDelimitedFile %>'></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="UploadFile"
                    CssClass="Error" Display="dynamic" ValidationExpression=".*(\.[cC][sS][vV])"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidDelimitedFile %>'></asp:RegularExpressionValidator>
            </div>
        </div>
    </div>
    <div class="RowStyle">
        <div class="ItemStyle">
        </div>
    </div>
    <div class="ClearBoth">
    </div>
    <div class="Error">
        <asp:Literal EnableViewState="false" ID="ltrlError" runat="server"></asp:Literal>
    </div>
    <div>
        <ZNode:Spacer ID="Spacer3" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div class="ClearBoth">
        <zn:Button ID="btnSubmit" runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true"/>
        <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
    </div>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
</div>
