﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Framework.Business;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;

namespace Znode.Engine.Admin.Secure.Inventory.ImportExportData
{
    public partial class DownloadCustomerPricing : System.Web.UI.UserControl
    { 
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack) return;
            ltrlError.Text = string.Empty;
            lnkLog.Visible = false;
            ltrlTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "LinkTextExportCustomerPricing").ToString();
        }
        
         /// <summary>
        /// Event fired when Cancel Button is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Inventory/ImportExportData/Default.aspx");
        }
        /// <summary>
        /// Event fired when Download Inventory Button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                var adminAccess = new DataDownloadAdmin();
                var dataManager = new DataManagerAdmin();
                
                var ds = dataManager.GetAllCustomerPricing();


				//if (ds.Tables[0]!=null && ds.Tables[0].Rows.Count>0)
				//{
                    if (ddlFileSaveType.SelectedValue == ".xls")
                    {
                        // Temp Grid control
                        var gridView = new GridView {DataSource = ds};
                        gridView.DataBind();
                        ZNodeLoggingBase.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Inventory", "Inventory");
                        this.ExportDataToExcel("CustomerPricing.xls", gridView);
                    }
                    else
                    {
                        // Set Formatted Data from dataset
                        string strData = adminAccess.Export(ds, true);
                        byte[] data = System.Text.Encoding.ASCII.GetBytes(strData);
                        ZNodeLoggingBase.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Imported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Download - Inventory", "Inventory");
                        this.ExportDataToCsv("CustomerPricing.csv", data);
                    }
				//}
				//else
				//{
				//	ltrlError.Text = @"Unable to download Customer Pricing. No data found.";
				//	return;
				//}
            }
            catch (Exception ex)
            {
                lnkLog.Visible = true;
                ZNodeLoggingBase.LogMessage(ex.ToString());
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFailedRequest").ToString();
            }
        }

        #region Helper Methods
      

        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="strFileName">The value of File Name</param>
        /// <param name="gridViewControl">Grid View Control</param>
        public void ExportDataToExcel(string strFileName, GridView gridViewControl)
        {
            Response.Clear();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + strFileName);

            // Set as Excel as the primary format
            Response.AddHeader("Content-Type", "application/Excel");
            Response.ContentType = "application/Excel";
            var sw = new System.IO.StringWriter();
            var htw = new HtmlTextWriter(sw);

            // Remove controls from Column Headers
            if (gridViewControl.HeaderRow != null)
            {
                for (var rw = 0; rw < gridViewControl.Rows.Count; rw++)
                {
                    var row = gridViewControl.Rows[rw];
                    for (var ct = 0; ct < row.Cells.Count; ct++)
                    {
                        // Save header text if found
                        string headerText = row.Cells[ct].Text;

                        // Check for controls in header
                        if (row.Cells[ct].HasControls())
                        {
                            // Check for link buttons (used in sorting)
                            if (row.Cells[ct].Controls[0].GetType().ToString() == "System.Web.UI.WebControls.CheckBox")
                            {
                                // Link button found, get text
                                headerText = ((CheckBox)row.Cells[ct].Controls[0]).Checked.ToString();
                            }

                            // Remove controls from header
                            row.Cells[ct].Controls.Clear();
                        }

                        // Reassign header text
                        row.Cells[ct].Text = headerText;
                    }
                }
            }

            gridViewControl.RenderControl(htw);
            Response.Write(sw.ToString());

            gridViewControl.Dispose();

            Response.End();
        }

        /// <summary>
        ///  Returns string for a Given Dataset Values
        /// </summary>
        /// <param name="fileName">The value of File Name</param>
        /// <param name="data">The value of Data</param>
        public void ExportDataToCsv(string fileName, byte[] data)
        {
            Response.Clear();
            Response.ClearHeaders();
            Response.ClearContent();
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.AddHeader("Content-Type", "application/Excel");

            // Set as text as the primary format
            Response.ContentType = "text/csv";
            Response.ContentType = "application/vnd.xls";
            Response.AddHeader("Pragma", "public");

            Response.BinaryWrite(data);
            Response.End();
        }
        #endregion
    }
}