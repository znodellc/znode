using System;
using System.IO;
using System.Text;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the SiteAdmin - PlugIns_DataManager_UpdatePricing class
    /// </summary>
    /// 

    public partial class UpdatePricing : System.Web.UI.UserControl
    {
        #region Private Member Variables
        private string DefaultPageLink = "~/Secure/Inventory/ImportExportData/Default.aspx";
		private int ErrorCount = 0;
		private StringBuilder errorMessage = new StringBuilder();
        #endregion

        #region Events

        /// <summary>
        /// Event fired when Submit button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        /// 
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            Stream fileStream = Request.Files[0].InputStream;
            StreamReader fileReader = new StreamReader(fileStream);
			//StringBuilder errorMessage = new StringBuilder();

            //int ErrorCount = 0;
            int SqlErrorCount = 0;
            bool status = false;
            int indexRowCount = 0;
            int parsedInt;
            decimal parsedDec;  
            int MaxErrorCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ImportMaxErrorCount"].ToString());
            ZNodePricingDataSet impPricing = new ZNodePricingDataSet();
            string dataRecord = fileReader.ReadLine();
            if (dataRecord == null)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordsFound").ToString();
                return;
            }
            
            dataRecord = fileReader.ReadLine();
            bool IsfirstSkuvalue = false;
            int lastproductId = 0;
            string firstretailPrice;
            while (dataRecord != null)
            {
                indexRowCount++;

                if (ErrorCount >= MaxErrorCount)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogMaximumCount").ToString(), MaxErrorCount.ToString()));
                    ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateFailed").ToString();                 
                    return;
                }

                string[] data = dataRecord.Split('|');

                if (data.Length !=9)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogInputFieldMissing").ToString(), indexRowCount));
                    ErrorCount++;
                    errorMessage = errorMessage.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInputFieldsMissing").ToString() + indexRowCount);

                    dataRecord = fileReader.ReadLine();
                    continue;
                }  
                try
                { 

                    ZNodePricingDataSet.ZNodeProductRow dr = impPricing.ZNodeProduct.NewZNodeProductRow();
                    if (int.TryParse(data[0], out parsedInt)) 
                    { 
                        dr.ProductID = parsedInt;
                        if (lastproductId != dr.ProductID)
                        {
                            lastproductId = dr.ProductID;
                            IsfirstSkuvalue = true;
                        }
                        else
                        {
                            IsfirstSkuvalue = false;
                        } 
                       
                    }

	                if (data[1].ToString() != string.Empty)
	                {
		                if (int.TryParse(data[1], out parsedInt))
		                {
			                dr.SKUId = parsedInt;
		                }
		                else
		                {
			                DisplayError("SKU ID",indexRowCount);
			                break;
		                }
	                }

	                if(data[2].ToString()!= string.Empty)
	                {
		                if (int.TryParse(data[2], out parsedInt))
		                {
			                dr.AddOnvalueId = parsedInt;
		                }
		                else
		                {
							DisplayError("AddOn Value",indexRowCount);
			                break;
		                }
	                }

	                dr.Name = data[3].ToString();
                    dr.ProductNum = data[4];
                    dr.Sku = data[5];


                    if (IsfirstSkuvalue == true)
                    {
                        firstretailPrice = data[6].ToString();
                        if (firstretailPrice != string.Empty)
                        {
                            if (decimal.TryParse(data[6], out parsedDec))
                            {
                                dr.RetailPrice = parsedDec;
                            }
							else
                            {
	                            DisplayError("Retail Price", indexRowCount);
								break;
							}
                        }
                    }
                    else
                    {
                        if (decimal.TryParse(data[6], out parsedDec))
                        {
                            dr.RetailPrice = parsedDec;
                        }
						else
						{
							DisplayError("Retail Price", indexRowCount);
							break;
						}
                    }


					if (data[7].ToString() != string.Empty)
	                {
		                if (decimal.TryParse(data[7], out parsedDec))
		                {
			                dr.SalePrice = parsedDec;
		                }
		                else
		                {
							DisplayError("Sale Price", indexRowCount);
			                break;
		                }
	                }


					if (data[8].ToString() != string.Empty)
	                {
		                if (decimal.TryParse(data[8], out parsedDec))
		                {
			                dr.WholesalePrice = parsedDec;
		                }
		                else
		                {
							DisplayError("Wholesale Price", indexRowCount);
			                break;
		                }
	                }
	                dr.IsfirstSku = IsfirstSkuvalue;
                    impPricing.ZNodeProduct.AddZNodeProductRow(dr);
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                    ErrorCount++;
                }
                
                dataRecord = fileReader.ReadLine();
            }

            if (impPricing.Tables[0].Rows.Count > 0)
            {
                DataManagerAdmin managerAdmin = new DataManagerAdmin();
                status = managerAdmin.UploadPricing(impPricing.ZNodeProduct, out SqlErrorCount);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Exported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Upload Pricing", "Pricing");
            }

            if ((ErrorCount > 0 || SqlErrorCount > 0) && status)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFileUpload").ToString() + errorMessage;              
                return;
            }

            if (!status)
			{
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateFailed").ToString();                      
            }
            else
            {
                Response.Redirect("~/Secure/Inventory/ImportExportData/Default.aspx");
            }
        }

        /// <summary>
        /// Event fired when Cancel button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.DefaultPageLink);
        }
        #endregion

		#region Private Methods

	    private void DisplayError(string columnName, int row)
	    {
            ZNodeLoggingBase.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogUploadPricing").ToString(), columnName, row));
		    ErrorCount++;
            errorMessage = errorMessage.Append(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogUploadInventory").ToString(), columnName, row));
	    }

	    #endregion
	}
}