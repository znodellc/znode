using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the Site Admin -  PlugIns_DataManager_UpdateSku class
    /// </summary>
    public partial class UpdateSku : System.Web.UI.UserControl
    {
        /// <summary>
        /// Event fierd when Submit button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            Stream fileStream = Request.Files[0].InputStream;
            StreamReader fileReader = new StreamReader(fileStream);

            int ErrorCount = 0;
            int SqlErrorCount = 0;
            bool status = false;
            int indexRowCount = 0;
            int parsedInt;
            decimal parsedDec;

            int MaxErrorCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ImportMaxErrorCount"].ToString());
            ZNodeSKUDataSet impSKU = new ZNodeSKUDataSet();
            string dataRecord = fileReader.ReadLine();
            if (dataRecord == null)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordsFound").ToString();
                return;
            }

            dataRecord = fileReader.ReadLine();

            while (dataRecord != null)
            {
                indexRowCount++;

                if (ErrorCount >= MaxErrorCount)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogUploadProduct").ToString());
                    ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateFailed").ToString();
                    return;
                }

                string[] data = dataRecord.Split('|');

                if (data.Length != 13)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogInputFieldMissing").ToString(), indexRowCount));
                    ErrorCount++;

                    dataRecord = fileReader.ReadLine();
                    continue;
                }

                try
                {
                    ZNodeSKUDataSet.ZNodeSKURow dr = impSKU.ZNodeSKU.NewZNodeSKURow();
                    dr.SKUID = 0;
                    if (int.TryParse(data[0], out parsedInt))
                    {
                        dr.SKUID = parsedInt;
                    }

                    dr.ProductID = int.Parse(data[1].ToString());
                    dr.SKU = data[2].ToString();
                    dr.Note = data[3];
                    if (decimal.TryParse(data[4], out parsedDec))
                    {
                        dr.WeightAdditional = parsedDec;
                    }
                    dr.SKUPicturePath = data[5];
                    dr.ImageAltTag = data[6];
                    if (int.TryParse(data[7], out parsedInt))
                    {
                        dr.DisplayOrder = parsedInt;
                    }

                    if (decimal.TryParse(data[8], out parsedDec))
                    {
                        dr.RetailPriceOverride = parsedDec;
                    }

                    if (decimal.TryParse(data[9], out parsedDec))
                    {
                        dr.SalePriceOverride = parsedDec;
                    }

                    if (decimal.TryParse(data[10], out parsedDec))
                    {
                        dr.WholesalePriceOverride = parsedDec;
                    }

                    dr.ActiveInd = bool.Parse(data[11].ToString());

					if (int.TryParse(data[12], out parsedInt))
					{
						dr.ExternalID = parsedInt;
					}

                    impSKU.ZNodeSKU.AddZNodeSKURow(dr);
                }
                catch (Exception ex)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                    ErrorCount++;
                }

                dataRecord = fileReader.ReadLine();
            }

            if (impSKU.Tables[0].Rows.Count > 0)
            {
                DataManagerAdmin managerAdmin = new DataManagerAdmin();
                status = managerAdmin.UploadSKU(impSKU.ZNodeSKU, out SqlErrorCount);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Exported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Upload SKUS", "SKUS");
            }

            if ((ErrorCount > 0 || SqlErrorCount > 0) && status)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorRowsNotUpdated").ToString();
                return;
            }
            
            if (!status)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateFailed").ToString();
            }
            else
            {
                Response.Redirect("~/Secure/Inventory/ImportExportData/Default.aspx");
            }
        }

        /// <summary>
        /// Event fierd when Cancel button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Inventory/ImportExportData/Default.aspx");
        }
    }
}