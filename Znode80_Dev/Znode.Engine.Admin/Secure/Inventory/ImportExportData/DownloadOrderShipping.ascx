<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Inventory.ImportExportData.DownloadOrderShipping" CodeBehind="DownloadOrderShipping.ascx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<div class="SearchForm">
    <h1>
        <asp:Localize runat="server" ID="ExportSKUs" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportShippingStatus %>'></asp:Localize></h1>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <span class="FieldStyle">
                    <asp:Localize runat="server" ID="BeginDate" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBeginDate %>'></asp:Localize></span>
                <br />
                <span class="ValueStyle">
                    <asp:TextBox ID="txtStartDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="imgbtnStartDt"
                        runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" /><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredBeginDate %>'
                        ControlToValidate="txtStartDate" ValidationGroup="grpReports" CssClass="Error"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
                        CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidDate %>'
                        ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                        ValidationGroup="grpReports"></asp:RegularExpressionValidator>
                    <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                        PopupPosition="TopLeft" runat="server" TargetControlID="txtStartDate">
                    </ajaxToolKit:CalendarExtender>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtStartDate"
                        ControlToValidate="txtEndDate" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareBeginDate %>'
                        ValidationGroup="grpReports" Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                </span>
            </div>
            <div class="ItemStyle">
                <span class="FieldStyle">
                    <asp:Localize runat="server" ID="EndDate" Text='<%$ Resources:ZnodeAdminResource, ColumnEndDate %>'></asp:Localize></span><br />
                <span class="ValueStyle">
                    <asp:TextBox ID="txtEndDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="ImgbtnEndDt"
                        runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" /><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEndDate"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredEndDate %>' ValidationGroup="grpReports" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEndDate"
                        CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidDate %>'
                        ValidationExpression="((^(10|12|0?[13578])([/])(3[01]|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(11|0?[469])([/])(30|[12][0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(2[0-8]|1[0-9]|0?[1-9])([/])((1[8-9]\d{2})|([2-9]\d{3}))$)|(^(0?2)([/])(29)([/])([2468][048]00)$)|(^(0?2)([/])(29)([/])([3579][26]00)$)|(^(0?2)([/])(29)([/])([1][89][0][48])$)|(^(0?2)([/])(29)([/])([2-9][0-9][0][48])$)|(^(0?2)([/])(29)([/])([1][89][2468][048])$)|(^(0?2)([/])(29)([/])([2-9][0-9][2468][048])$)|(^(0?2)([/])(29)([/])([1][89][13579][26])$)|(^(0?2)([/])(29)([/])([2-9][0-9][13579][26])$))"
                        ValidationGroup="grpReports"></asp:RegularExpressionValidator>
                    <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt" PopupPosition="TopLeft"
                        runat="server" TargetControlID="txtEndDate">
                    </ajaxToolKit:CalendarExtender>

                </span>
            </div>
        </div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="FileType" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFileType %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlFileSaveType" runat="server">
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextFileSaveXls %>' Value=".xls"></asp:ListItem>
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextFileSaveCsv %>' Value=".csv" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div>
                    <ZNode:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="3" runat="server"></ZNode:Spacer>
                </div>
                <div class="ClearBoth">
                    <div class="Error">
                        <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
                    </div>
                    <div>
                        <a href="../../../Data/Default/logs/ZnodeLog.txt" id="lnkLog" runat="server"
                            visible="false" target="_blank">
                            <asp:Localize runat="server" ID="ViewLog" Text='<%$ Resources:ZnodeAdminResource, LinkTextViewLog %>'></asp:Localize></a>
                    </div>
                    <div>
                        <ZNode:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                    </div>
                </div>
            </div>
            <div class="ClearBoth">
            </div>
            <div>
                <zn:Button ID="btnDownloadInventory" runat="server" Width="150px" ButtonType="EditButton" OnClick="BtnDownloadProdInventory_Click" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportShippingStatus  %>' />
                <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
            </div>
        </div>
    </div>
</div>
