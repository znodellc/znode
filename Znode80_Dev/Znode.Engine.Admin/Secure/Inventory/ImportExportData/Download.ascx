<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Inventory.ImportExportData.Download" CodeBehind="Download.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<div class="Form SearchForm">
    <h1>
        <asp:Literal ID="ltrlTitle" runat="server"></asp:Literal></h1>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <asp:Panel ID="pnlProductTypes" runat="server" Visible="false">
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="SelectproductType" Text='<%$ Resources:ZnodeAdminResource, SelectProductType %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlProductType" runat="server">
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropdownTextAll %>' Selected="True" Value="0"></asp:ListItem>
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextAttributesOnly %>' Value="1"></asp:ListItem>
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextAddOnValuesOnly %>' Value="2"></asp:ListItem>
                        <asp:ListItem Text="AddOnValues Only" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
        </div>
    </asp:Panel>
    <div class="RowStyle">
        <div class="ItemStyle">
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="FileType" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFileType %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlFileSaveType" runat="server">
                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextFileSaveXls %>' Value=".xls"></asp:ListItem>
                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextFileSaveCsv %>' Value=".csv" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="RowStyle" id="divCatalog" runat="server" visible="false">
        <div class="ItemStyle">
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="SelectCatalog" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectCatalog %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlCatalog" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlCatalog_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div>
        <ZNode:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div class="ClearBoth">
        <br />
        <div class="Error">
            <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
        </div>
        <div>
            <a href="../../../Data/Default/Logs/ZNodeLog.txt" id="lnkLog" runat="server"
                visible="false" target="_blank">
                <asp:Localize runat="server" ID="TextViewLog" Text='<%$ Resources:ZnodeAdminResource, LinkTextViewLog %>'></asp:Localize></a>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <!-- Download product inventory Panel -->
        <asp:Panel ID="pnlDownloadInventory" runat="server" Visible="false">
            <div>
                <zn:Button ID="BtnDownloadInventory" runat="server" CommandArgument="1" ButtonType="EditButton" OnClick="BtnDownloadProdInventory_Click" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportInventory %>' />
                <zn:Button ID="InventoryCancelButton" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
            </div>
        </asp:Panel>
        <!-- Download sku inventory Panel -->
        <asp:Panel ID="pnlDownloadSkuInventory" runat="server" Visible="false">
            <div>
                <zn:Button ID="btnDownloadSkuInventory" runat="server" CommandArgument="2" ButtonType="SubmitButton" OnClick="BtnDownloadProdInventory_Click" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportInventory %>' />
                <zn:Button ID="SkuInventoryCancelButton" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
            </div>
        </asp:Panel>
        <!-- Download AddOnValue inventory Panel -->
        <asp:Panel ID="pnlDownloadAddOnValueInventory" runat="server" Visible="false">
            <div>

                <zn:Button ID="btnDownloadAddOnValueInventory" runat="server" CommandArgument="3" ButtonType="SubmitButton" OnClick="BtnDownloadProdInventory_Click" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportInventory %>' />
                <zn:Button ID="AddOnValueInventoryCancelButton" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
            </div>
        </asp:Panel>
        <!-- Download Product prices Panel -->
        <asp:Panel ID="pnlDownloadPricing" runat="server" Visible="false">
            <div>

                <zn:Button ID="btnDownloadProductPricing" runat="server" Width="120px" ButtonType="EditButton" OnClick="BtnDownloadProductPricing_Click" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportPricing %>' />
                <zn:Button ID="PricingCancelButton" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
            </div>
        </asp:Panel>
        <!-- Download product Panel -->
        <asp:Panel ID="pnlDownloadProduct" runat="server" Visible="false" CssClass="ClearBoth">
            <div>
                <zn:Button ID="btnDownloadProduct" runat="server" Width="120px" ButtonType="EditButton" OnClick="BtnDownloadProduct_Click" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportProducts %>' />
                <zn:Button ID="ProductCancelButton" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
            </div>
        </asp:Panel>
    </div>
</div>
