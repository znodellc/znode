<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="Znode.Engine.Admin.Secure.Inventory.ImportExportData.UploadZipcode" CodeBehind="UploadZipcode.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<div class="Form">
    <h1><asp:Localize runat="server" ID="ImportZipCode" Text='<%$ Resources:ZnodeAdminResource, LinkTextImportZipCodeData %>'></asp:Localize></h1>
    
    <p>
       <asp:Localize runat="server" ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, TextZipCodeData %>'></asp:Localize>
    </p>
    <p>
       <asp:Localize runat="server" ID="Localize2" Text='<%$ Resources:ZnodeAdminResource, TextZipCodeDoc %>'></asp:Localize>
    </p>
    <p>
        <strong>  <asp:Localize runat="server" ID="Localize3" Text='<%$ Resources:ZnodeAdminResource, TextWarning %>'></asp:Localize> </strong>  <asp:Localize runat="server" ID="Localize4" Text='<%$ Resources:ZnodeAdminResource, TextActionCannotUndone %>'></asp:Localize>
    </p>
    <div>
        <ZNode:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <asp:Panel ID="uploadPanel" runat="server" CssClass="SearchForm">
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                 <asp:Localize runat="server" ID="Localize6" Text='<%$ Resources:ZnodeAdminResource, ColumnTitlePathDelimitedFile %>'></asp:Localize>
                </div>
                <div class="HintStyle">
                      <asp:Localize runat="server" ID="Localize5" Text='<%$ Resources:ZnodeAdminResource, SubTextZipCodeData %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtInputFile" runat="server" Width="300px"></asp:TextBox>
                    <asp:RequiredFieldValidator Display="dynamic" ControlToValidate="txtInputFile" CssClass="Error"
                        ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDelimitedFile %>'></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtInputFile"
                        CssClass="Error" Display="dynamic" ValidationExpression=".*(\.[cC][sS][vV])"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidDelimitedFile %>'></asp:RegularExpressionValidator>
                </div>
            </div>
        </div>
        <div class="Error ClearBoth">
            <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="ClearBoth">
             <zn:Button ID="btnSubmit" runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" />
        <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />

        </div>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
    </asp:Panel>
    <div>
        <div class="SuccessMark">
            <asp:Literal ID="ltrlmsg" runat="server" Visible="false"></asp:Literal>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer4" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div>
            <zn:Button runat="server"  ID="btnGoback" CausesValidation="True" Text='<%$ Resources:ZnodeAdminResource, LinkBack%>' runat="server"
                OnClick="BtnCancel_Click" Visible="False" ButtonType="EditButton" Width="100px"/>
        </div>
    </div>
</div>
