﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploadCustomerPricing.ascx.cs" Inherits="Znode.Engine.Admin.Secure.Inventory.ImportExportData.UploadCustomerPricing" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<div class="Form SearchForm">
    <h1><asp:Localize runat="server" ID="ImportCustomerPricing" Text='<%$ Resources:ZnodeAdminResource, LinkTextImportCustomerPricing %>'></asp:Localize></h1>

    <div>
        <ZNode:Spacer ID="Spacer8" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div class="RowStyle">
        <div class="ItemStyle">
            <div class="FieldStyle"> <asp:Localize runat="server" ID="DelimitedFile" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectDelimitedFile %>'></asp:Localize></div>
            <div class="ValueStyle">
                <input type="file" id="UploadFile" runat="server" width="350px" />
                <asp:RequiredFieldValidator Display="dynamic" ControlToValidate="UploadFile" CssClass="Error" ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDelimitedFile %>'></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="UploadFile" CssClass="Error" Display="dynamic" ValidationExpression=".*(\.[cC][sS][vV])" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidCSV %>'></asp:RegularExpressionValidator>
            </div>
        </div>
    </div>

    <div class="ClearBoth"></div>

    <div class="Error">
        <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
    </div>
    <div class="SuccessMark">
        <asp:Label ID="lblText" runat="server"></asp:Label>
    </div>
    <div>
        <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div class="ClearBoth">
        
         <zn:Button ID="btnSubmit" runat="server" ButtonType="SubmitButton" OnClick="btnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true"/>
         <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" OnClick="btnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
    </div>

    <div>
        <ZNode:Spacer ID="Spacer3" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
</div>
