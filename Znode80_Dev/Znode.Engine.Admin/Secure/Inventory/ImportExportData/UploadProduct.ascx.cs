using System;
using System.IO;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;

namespace  Znode.Engine.Admin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the Siteadmin- PlugIns_DataManager_UploadProduct class
    /// </summary>
    public partial class UploadProduct : System.Web.UI.UserControl
    {
        #region Events
        /// <summary>
        /// Event fired when Submit button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            Stream fileStream = Request.Files[0].InputStream;
            StreamReader fileReader = new StreamReader(fileStream);

            ZNodeProductDataset impProduct = new ZNodeProductDataset();

            string dataRecord = fileReader.ReadLine();

            if (dataRecord == null)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordsFound").ToString();
                return;
            }

            dataRecord = fileReader.ReadLine();

            int ErrorCount = 0;
            int SqlErrorCount = 0;
            bool status = false;
            int parsedInt;
            decimal parsedDec;
            bool parsedBool;
	        string firstWholeSalePrice;
	        string firstSalePrice;
            int indexRowCount = 0;
            int MaxErrorCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ImportMaxErrorCount"].ToString());

            while (dataRecord != null)
            {
                indexRowCount++;

				if (ErrorCount >= MaxErrorCount)
				{
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogUploadProduct").ToString());
                    ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogUploadProduct").ToString();
					return;
				}

                string[] data = dataRecord.Split('|');

                if (data.Length != 45)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogInputFieldMissing").ToString(), indexRowCount));
                    ErrorCount++;

                    dataRecord = fileReader.ReadLine();
                    continue;
                }

                try
                {
                    ZNodeProductDataset.ZNodeProductRow dataRow = impProduct.ZNodeProduct.NewZNodeProductRow();
                    if (int.TryParse(data[0], out parsedInt))
                    {
                        dataRow.ProductID = parsedInt;
                    }

                    dataRow.Name = data[3].ToString();
                    dataRow.ShortDescription = Server.HtmlEncode(data[4].ToString());
                    dataRow.Description = Server.HtmlEncode(data[5].ToString());
                    dataRow.FeaturesDesc = Server.HtmlEncode(data[6].ToString());
                    dataRow.ProductNum = data[7].ToString();
                    dataRow.ProductTypeID = int.Parse(data[8].ToString());

                    if (decimal.TryParse(data[9], out parsedDec))
                    {
                        dataRow.RetailPrice = parsedDec;
                    }
					else
					{
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInvalidRetailPrice").ToString(), indexRowCount));
                        ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInvalidRetailPrice").ToString() + indexRowCount;
						return;

					}
					//To check Sale Price
	                firstSalePrice = data[10].ToString();
	                if (firstSalePrice != string.Empty)
	                {
		                if (decimal.TryParse(data[10], out parsedDec))
		                {
			                dataRow.SalePrice = parsedDec;
		                }
		                else
		                {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInvalidSalePrice").ToString(), indexRowCount));
                            ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInvalidSalePrice").ToString() + indexRowCount;
			                return;

		                }
	                }
	                else
	                {
						if (decimal.TryParse(data[10], out parsedDec))
						{
							dataRow.SalePrice = parsedDec;
						}
	                }
	                //To check Whose sale Price
					firstWholeSalePrice = data[11].ToString();
	                if (firstWholeSalePrice != string.Empty)
	                {
		                if (decimal.TryParse(data[11], out parsedDec))
		                {
			                dataRow.WholesalePrice = parsedDec;
		                }
		                else
		                {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInvalidWholeSalePrice").ToString(), indexRowCount));
                            ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInvalidWholeSalePrice").ToString() + indexRowCount;
			                return;

		                }
	                }
	                else
	                {
		                if (decimal.TryParse(data[11], out parsedDec))
		                {
			                dataRow.WholesalePrice = parsedDec;
		                }
	                }
	                dataRow.ImageFile = data[12].ToString();

                    if (decimal.TryParse(data[13], out parsedDec))
                    {
                        dataRow.Weight = parsedDec;
                    }

                    if (decimal.TryParse(data[14], out parsedDec))
                    {
                        dataRow.Length = parsedDec;
                    }

                    if (decimal.TryParse(data[15], out parsedDec))
                    {
                        dataRow.Width = parsedDec;
                    }

                    if (decimal.TryParse(data[16], out parsedDec))
                    {
                        dataRow.Height = parsedDec;
                    }

                    if (int.TryParse(data[17], out parsedInt))
                    {
                        dataRow.DisplayOrder = parsedInt;
                    }

                    dataRow.ActiveInd = false;
                    if (bool.TryParse(data[18].ToString(), out parsedBool))
                    {
                        dataRow.ActiveInd = parsedBool;
                    }

                    dataRow.CallForPricing = false;

                    if (bool.TryParse(data[19].ToString(), out parsedBool))
                    {
                        dataRow.CallForPricing = parsedBool;
                    }

                    dataRow.HomepageSpecial = false;

                    if (bool.TryParse(data[20].ToString(), out parsedBool))
                    {
                        dataRow.HomepageSpecial = parsedBool;
                    }

                    dataRow.CategorySpecial = false;

                    if (bool.TryParse(data[21].ToString(), out parsedBool))
                    {
                        dataRow.CategorySpecial = parsedBool;
                    }

                    dataRow.InventoryDisplay = 0;

                    if (int.TryParse(data[22].ToString(), out parsedInt))
                    {
                        dataRow.InventoryDisplay = parsedInt;
                    }

                    dataRow.Keywords = data[23].ToString();

                    if (int.TryParse(data[24], out parsedInt))
                    {
                        dataRow.ManufacturerID = parsedInt;
                    }

                    if (int.TryParse(data[25], out parsedInt))
                    {
                        dataRow.ShippingRuleTypeID = parsedInt;
                    }

                    dataRow.SEOTitle = data[26].ToString();
                    dataRow.SEOKeywords = data[27].ToString();
                    dataRow.SEODescription = data[28].ToString();
                    dataRow.InStockMsg = data[29].ToString();
                    dataRow.OutOfStockMsg = data[30].ToString();

                    dataRow.TrackInventoryInd = false;
                    if (bool.TryParse(data[31].ToString(), out parsedBool))
                    {
                        dataRow.TrackInventoryInd = parsedBool;
                    }

                    dataRow.FreeShippingInd = false;
                    if (bool.TryParse(data[32].ToString(), out parsedBool))
                    {
                        dataRow.FreeShippingInd = parsedBool;
                    }

                    dataRow.NewProductInd = false;
                    if (bool.TryParse(data[33].ToString(), out parsedBool))
                    {
                        dataRow.NewProductInd = parsedBool;
                    }

                    dataRow.SEOURL = data[34].ToString();

                    if (int.TryParse(data[35], out parsedInt))
                    {
                        dataRow.MaxQty = parsedInt;
                    }

                    dataRow.ShipSeparately = false;

                    if (bool.TryParse(data[36].ToString(), out parsedBool))
                    {
                        dataRow.ShipSeparately = parsedBool;
                    }

                    dataRow.FeaturedInd = false;

                    if (bool.TryParse(data[37].ToString(), out parsedBool))
                    {
                        dataRow.FeaturedInd = parsedBool;
                    }

                    dataRow.RecurringBillingInd = data[38].ToString();
                    dataRow.RecurringBillingInstallmentInd = data[39].ToString();

                    if (int.TryParse(data[40], out parsedInt))
                    {
                        dataRow.PortalID = parsedInt;
                    }

                    if (int.TryParse(data[41].ToString(), out parsedInt))
                    {
                        dataRow.ReviewStateID = parsedInt;
                    }

                    if (bool.TryParse(data[42].ToString(), out parsedBool))
                    {
                        dataRow.Franchisable = parsedBool;
                    }

                    if (int.TryParse(data[43], out parsedInt))
                    {
                        dataRow.ExpirationPeriod = parsedInt;
                    }

                    if (int.TryParse(data[44], out parsedInt))
                    {
                        dataRow.ExpirationFrequency = parsedInt;
                    }

                    // Existing Product
                    if (dataRow.ProductID > 0)
                    {
                        impProduct.ZNodeProduct.AddZNodeProductRow(dataRow);
                    }
                    else
                    {
                        // New product
                        dataRow.SKU = data[1].ToString();
                        if (int.TryParse(data[2], out parsedInt))
                        {
                            dataRow.QuantityOnHand = parsedInt;
                        }

                        SKUAdmin skuCheck = new SKUAdmin();

                        // If the SKU already exists we won't be able to add the Product, Log an error.    
                        if (skuCheck.IsSkuExist(dataRow.SKU, 0))
                        {
                            ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogSKUAlreadyExists").ToString(), dataRow.SKU));
                        }
                        else
                        {
                            impProduct.ZNodeProduct.AddZNodeProductRow(dataRow);
                        }
                    }
                }
                catch (Exception exception)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(exception.ToString());
                    ErrorCount++;
                }

                dataRecord = fileReader.ReadLine();
            }

            if (impProduct.Tables[0].Rows.Count > 0)
            {
                DataManagerAdmin managerAdmin = new DataManagerAdmin();
                status = managerAdmin.UploadProducts(impProduct.ZNodeProduct, out SqlErrorCount);
            }

            impProduct.Dispose();

            if ((ErrorCount > 0 || SqlErrorCount > 0) && status)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorRowsNotUpdated").ToString();
                return;
            }

            if (!status)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductUploadFailed").ToString();
            }
            else if (status)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Exported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Upload Products", "Products");
                Response.Redirect("~/Secure/Inventory/ImportExportData/Default.aspx");
            }
        }

        /// <summary>
        /// Event fired when Cancel button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Inventory/ImportExportData/Default.aspx");
        }
        #endregion
    }
}