﻿using System;
using System.IO;
using System.Text;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.Framework.Business;

namespace Znode.Engine.Admin.Secure.Inventory.ImportExportData
{
    public partial class UploadCustomerPricing : System.Web.UI.UserControl
    {
        #region Events
        /// <summary>
        /// Event fired when Submit button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lblText.Text = string.Empty;
            ltrlError.Text = string.Empty;

            var fileStream = Request.Files[0].InputStream;
            var fileReader = new StreamReader(fileStream);

            var impPricing = new ZNodeCustomerBasedPricingDataSet();

            string dataRecord = fileReader.ReadLine();

            if (dataRecord == null)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordsFound").ToString();
                return;
            }

            dataRecord = fileReader.ReadLine();

            var errorCount = 0;
            var sqlErrorCount = 0;
            var status = false;
            var indexRowCount = 0;
            var maxErrorCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ImportMaxErrorCount"].ToString());
            StringBuilder errorMessage = new StringBuilder();

            while (dataRecord != null)
            {
                indexRowCount++;

                if (errorCount >= maxErrorCount)
                {
                    ZNodeLoggingBase.LogMessage(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogCustomerPricingMaxCount").ToString());
                    return;
                }

                string[] data = dataRecord.Split('|');

                if (data.Length != 4)
                {
                    ZNodeLoggingBase.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogCutomerpricingInputFieldMissing").ToString(), indexRowCount));
                    errorCount++;
                    errorMessage = errorMessage.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInputFieldsMissing").ToString() + indexRowCount);
                    dataRecord = fileReader.ReadLine();
                    continue;
                   
                }

                try
                {
                    var dataRow = impPricing.ZNodeCustomerPricing.NewZNodeCustomerPricingRow();
                    int parsedInt;
                    if (int.TryParse(data[0], out parsedInt))
                    {
                        dataRow.CustomerPricingID = parsedInt;
                    }

                    dataRow.ExternalAccountNo = data[1];
                  
                    if (string.IsNullOrEmpty(dataRow.ExternalAccountNo))
                    {
                        ZNodeLoggingBase.LogMessage(string.Format(
                            this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogCPAccountNumber").ToString(), indexRowCount));
                        errorCount++;
                        errorMessage = errorMessage.Append(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInvalidAccountNumber").ToString(), indexRowCount));
                        break;
                    }
                    if (dataRow.ExternalAccountNo.Length > 50)
                    {
                        ZNodeLoggingBase.LogMessage(string.Format(
                            this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogCPAccountNumber").ToString(), indexRowCount));
                        errorCount++;
                        errorMessage = errorMessage.Append(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAccountNumberExceeded").ToString(), indexRowCount));
                        break;
                    }


                    decimal parsedDec;
                    if (decimal.TryParse(data[2].TrimEnd(','), out parsedDec))
                    {
                        dataRow.NegotiatedPrice = parsedDec;
                    }
                    else
                    {
                        ZNodeLoggingBase.LogMessage(string.Format(
                             this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogCPNegotiatedPrice").ToString(), indexRowCount));
                        errorCount++;
                        errorMessage = errorMessage.Append(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNegotiatedPrice").ToString(), indexRowCount));
                        break;
                    }

					dataRow.SKUExternalID = data[3];
					if (string.IsNullOrEmpty(dataRow.SKUExternalID))
					{
						ZNodeLoggingBase.LogMessage(string.Format(
                            this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogCPInvalidSKU").ToString(), indexRowCount));
						errorCount++;
						errorMessage = errorMessage.Append(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInvalidSKU").ToString(), indexRowCount));
						break;
					}

                    impPricing.ZNodeCustomerPricing.AddZNodeCustomerPricingRow(dataRow);

                }
                catch (Exception exception)
                {
                    ZNodeLoggingBase.LogMessage(exception.ToString());
                    errorCount++;
                }

                dataRecord = fileReader.ReadLine();
            }
            if (errorCount > 0)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorFileUpload").ToString() + errorMessage;
                return;
            }

            if (impPricing.Tables[0].Rows.Count > 0)
            {
                var managerAdmin = new DataManagerAdmin();
                status = managerAdmin.UploadCustomerPricing(impPricing.ZNodeCustomerPricing, out sqlErrorCount);
            }

            impPricing.Dispose();

            if (!status)
            {
                ltrlError.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorInvalidInputField").ToString(), (sqlErrorCount));
                return;
            }
            else
            {
                ZNodeLoggingBase.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.Exported, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Upload Customer Pricing", "CustomerPricing");
                lblText.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextSuccessCustomerPricing").ToString();

            }
        }

        /// <summary>
        /// Event fired when Cancel button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Inventory/ImportExportData/Default.aspx");
        }
        #endregion
    }
}