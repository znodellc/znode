<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Inventory.ImportExportData.DownloadSku" CodeBehind="DownloadSku.ascx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="ProductAutoComplete" Src="~/Controls/Default/ProductAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<div class="SearchForm">
    <h1>
        <asp:Localize runat="server" ID="ExportSKUs" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportSKUs %>'></asp:Localize></h1>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="Catalog" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCatalog %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlCatalog" AutoPostBack="true" OnSelectedIndexChanged="DdlCatalog_SelectedIndexChanged"
                        runat="server">
                    </asp:DropDownList>
                </div>
            </div>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer6" SpacerHeight="30" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="ProductName" Text='<%$ Resources:ZnodeAdminResource, ColumnProductName %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <ZNode:ProductAutoComplete ID="lstProduct" runat="server" />
                </div>
            </div>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <asp:Panel ID="pnlAttribteslist" Visible="true" runat="server">
                    <div class="FieldStyle">
                        <asp:Localize runat="server" ID="SelectProductAttribute" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectProductAttributes %>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:PlaceHolder ID="ControlPlaceHolder" runat="server"></asp:PlaceHolder>
                    </div>
                    <div><small>
                        <asp:Localize runat="server" ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, TextSelectProductAttributes %>'></asp:Localize></small></div>
                </asp:Panel>
            </div>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="RowStyle">
            <div class="ItemStyle">
                <div class="FieldStyle">
                    <asp:Localize runat="server" ID="FileType" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFileType %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlFileSaveType" runat="server">
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextFileSaveXls %>' Value=".xls"></asp:ListItem>
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextFileSaveCsv %>' Value=".csv" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div>
                    <ZNode:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="3" runat="server"></ZNode:Spacer>
                </div>
                <div class="ClearBoth">
                    <div class="Error">
                        <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
                    </div>
                    <div>
                        <a href="../../../Data/Default/Logs/ZnodeLog.txt" id="lnkLog" runat="server"
                            visible="false" target="_blank">
                            <asp:Localize runat="server" ID="ViewLog" Text='<%$ Resources:ZnodeAdminResource, LinkTextViewLog %>'></asp:Localize></a>
                    </div>
                    <div>
                        <ZNode:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                    </div>
                </div>
            </div>
            <div class="ClearBoth">
            </div>
            <div>

                <zn:Button ID="btnDownloadInventory" runat="server" Width="150px" ButtonType="EditButton" OnClick="BtnDownloadProdInventory_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonExportSKU  %>' />
                <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
            </div>
        </div>
    </div>
</div>
