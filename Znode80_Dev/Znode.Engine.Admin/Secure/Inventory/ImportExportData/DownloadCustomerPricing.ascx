﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DownloadCustomerPricing.ascx.cs" Inherits="Znode.Engine.Admin.Secure.Inventory.ImportExportData.DownloadCustomerPricing" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<div class="Form SearchForm">
    <h1>
        <asp:Literal ID="ltrlTitle" runat="server"></asp:Literal></h1>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div class="RowStyle">
        <div class="ItemStyle">
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="FileType" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFileType %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlFileSaveType" runat="server">
                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextFileSaveXls %>' Value=".xls"></asp:ListItem>
                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextFileSaveCsv %>' Value=".csv" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div>
        <ZNode:Spacer ID="Spacer2" SpacerHeight="1" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div class="ClearBoth"></div>

    <div class="Error">
        <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
    </div>
    <div>
        <a href="../../../Data/Default/Logs/ZNodeLog.txt" id="lnkLog" runat="server"
            visible="false" target="_blank">
            <asp:Localize runat="server" ID="ViewLog" Text='<%$ Resources:ZnodeAdminResource, LinkTextViewLog %>'></asp:Localize></a>
    </div>
    <div>
        <ZNode:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div>
        <zn:Button ID="btnSubmit" runat="server" CommandArgument="2" Width="175px" ButtonType="EditButton" OnClick="btnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportCustomerPricing %>' />
        <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" OnClick="btnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
    </div>

</div>
