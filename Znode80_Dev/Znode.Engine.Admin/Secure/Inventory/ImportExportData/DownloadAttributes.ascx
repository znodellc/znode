<%@ Control Language="C#" AutoEventWireup="True"
    Inherits="Znode.Engine.Admin.Secure.Inventory.ImportExportData.DownloadAttributes" CodeBehind="DownloadAttributes.ascx.cs" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<div class="Form SearchForm">
    <h1> <asp:Localize runat="server" ID="ExportAttributes" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportAttributes %>'></asp:Localize></h1>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div>
        <ZNode:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div class="RowStyle" id="divCatalog" runat="server">
        <div class="ItemStyle">
            <div class="FieldStyle">
               <asp:Localize runat="server" ID="SelectCatalog" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectCatalog %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlCatalog" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlCatalog_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="RowStyle">
        <div class="ItemStyle">
            <div class="FieldStyle">
                 <asp:Localize runat="server" ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectAttributeType %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="lstAttributeTypeList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="LstAttributeTypeList_SelectedIndexChanged" />
            </div>
        </div>
    </div>
    <div class="RowStyle">
        <div class="ItemStyle">
            <div class="FieldStyle">
              <asp:Localize runat="server" ID="FileType" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFileType %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlFileSaveType" runat="server">
                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextFileSaveXls %>' Value=".xls"></asp:ListItem>
                    <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextFileSaveCsv %>' Value=".csv" Selected="True"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div>
        <ZNode:Spacer ID="Spacer3" SpacerHeight="1" SpacerWidth="3" runat="server"></ZNode:Spacer>
    </div>
    <div class="ClearBoth">
        <div class="Error">
            <asp:Literal ID="ltrlError" runat="server"></asp:Literal>
        </div>
        <div>
            <a href="../../../Data/Default/Config/ActivityLog.txt" id="lnkLog" runat="server"
                visible="false" target="_blank"><asp:Localize runat="server" ID="ViewLog" Text='<%$ Resources:ZnodeAdminResource, LinkTextViewLog %>'></asp:Localize></a>
        </div>
        <div>
            <ZNode:Spacer ID="Spacer4" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
        <div class="ClearBoth">
            <div>
               <zn:Button ID="btnDownloadProduct" runat="server" Width="150px" ButtonType="EditButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, LinkTextExportAttributes %>' />
               <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" />
            </div>
        </div>
    </div>
</div>
