using System;
using System.IO;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;

namespace  Znode.Engine.Admin.Secure.Inventory.ImportExportData
{
    /// <summary>
    /// Represents the SiteAdmin - PlugIns_DataManager_UploadFacets class
    /// </summary>
    public partial class UploadFacets : System.Web.UI.UserControl
    {
        #region Protected Methods and Events
        /// <summary>
        /// Event fired when Submit button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            Stream fileStream = Request.Files[0].InputStream;
            StreamReader fileReader = new StreamReader(fileStream);

            ZNodeFacetsDataSet impFacets = new ZNodeFacetsDataSet();
            string dataRecord = fileReader.ReadLine();

            if (dataRecord == null)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoRecordsFound").ToString();
                return;
            }

            dataRecord = fileReader.ReadLine();

            int ErrorCount = 0;
            int SqlErrorCount = 0;
            bool status = false;
            int parsedInt;
            int indexRowCount = 0;
            int MaxErrorCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ImportMaxErrorCount"].ToString());

            while (dataRecord != null)
            {
                indexRowCount++;

                if (ErrorCount >= MaxErrorCount)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogUploadProduct").ToString());
                    return;
                }

                string[] data = dataRecord.Split('|');
                
                if (data.Length != 10)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogInputFieldMissing").ToString(), indexRowCount));
                    ErrorCount++;

                    dataRecord = fileReader.ReadLine();
                    continue;
                }

                try
                {
                    ZNodeFacetsDataSet.ZNodeFacetRow dataRow = impFacets.ZNodeFacet.NewZNodeFacetRow();

                    dataRow.FacetGroupLabel = data[0];
                    dataRow.ControlType = data[1];
                    if (int.TryParse(data[2], out parsedInt)) 
                    { 
                        dataRow.CatalogID = parsedInt; 
                    }

                    if (int.TryParse(data[3], out parsedInt)) 
                    { 
                        dataRow.DisplayOrder = parsedInt; 
                    }

                    dataRow.FacetName = data[4];

                    if (int.TryParse(data[5], out parsedInt)) 
                    { 
                        dataRow.FacetDisplayOrder = parsedInt; 
                    }

                    dataRow.IconPath = data[6];

                    if (int.TryParse(data[7], out parsedInt)) 
                    { 
                        dataRow.ProductID = parsedInt; 
                    }

                    if (int.TryParse(data[8], out parsedInt)) 
                    { 
                        dataRow.CategoryID = parsedInt; 
                    }

                    if (int.TryParse(data[9], out parsedInt)) 
                    {
                        dataRow.CategoryDisplayOrder = parsedInt;
                    }

                    impFacets.ZNodeFacet.AddZNodeFacetRow(dataRow);
                }
                catch (Exception exception)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(exception.ToString());
                    ErrorCount++;
                }

                dataRecord = fileReader.ReadLine();
            }

            if (impFacets.Tables[0].Rows.Count > 0)
            {
                DataManagerAdmin managerAdmin = new DataManagerAdmin();
                status = managerAdmin.UploadFacets(impFacets.ZNodeFacet, out SqlErrorCount);
            }

            if ((ErrorCount > 0 || SqlErrorCount > 0) && status)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorRowsNotUpdated").ToString();
                return;
            }

            if (!status)
            {
                ltrlError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUpdateFailed").ToString();        
            }
            else if (status)
            {
                Response.Redirect("~/Secure/Inventory/ImportExportData/Default.aspx");
            }
        }

        /// <summary>
        /// Event fired when Cancel button is triggered.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Inventory/ImportExportData/Default.aspx");
        }
        #endregion
    }
}