using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;

namespace  Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.AddOnTypes
{
    /// <summary>
    /// Represents the Site Admin - Admin_Secure_catalog_product_addons_list class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Variables
        private static bool IsSearchEnabled = false;
        private string CatalogImagePath = string.Empty;
        private string AddLink = "~/Secure/Inventory/ReferenceTypes/AddOnTypes/add.aspx";
        private string ViewLink = "~/Secure/Inventory/ReferenceTypes/AddOnTypes/view.aspx";
        private string DeleteLink = "~/Secure/Inventory/ReferenceTypes/AddOnTypes/delete.aspx";
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindLocaleDropdown();
                this.BindGridData();
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Add New Product AddOn Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddCategory_Click(object sender, System.EventArgs e)
        {
            Response.Redirect(this.AddLink);
        }
       
        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.SearchAddOns();
            IsSearchEnabled = true;
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
        }
        #endregion

        #region Grid Events

        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            // Check if search is Enabled
            if (IsSearchEnabled)
            {
                uxGrid.PageIndex = e.NewPageIndex;
                
                // Bind grid
                this.SearchAddOns(); 
            }
            else
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindGridData();
            }
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // Cell in the GridView control.
                GridViewRow selectedRow = uxGrid.Rows[index];

                TableCell Idcell = selectedRow.Cells[0];
                string Id = Idcell.Text;

                if (e.CommandName == "Manage")
                {
                    this.ViewLink = this.ViewLink + "?itemid=" + Id;
                    Response.Redirect(this.ViewLink);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.DeleteLink + "?itemid=" + Id);
                }
            }
        }

        #endregion

        #region Bind Methods

        /// <summary>
        /// Bind the locale dropdown
        /// </summary>
        private void BindLocaleDropdown()
        {
            ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();

            // Bind the dropdown.
            ddlLocales.DataSource = AdminAccess.GetAllLocaleId();
            ddlLocales.DataTextField = "LocaleDescription";
            ddlLocales.DataValueField = "LocaleId";
            ddlLocales.DataBind();

            if (ddlLocales.Items.Count == 0)
            {
                lblLocale.Visible = false;
                ddlLocales.Visible = false;
            }
        }

        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            ProductAddOnAdmin ProdAddonAdminAccess = new ProductAddOnAdmin();
            uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(ProdAddonAdminAccess.GetAllAddOns(), true);
            uxGrid.DataBind();
        }

        private void SearchAddOns()
        {
            ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();
            int localeId = 0;
            int.TryParse(ddlLocales.SelectedValue, out localeId);
            uxGrid.DataSource = UserStoreAccess.CheckStoreAccess(AdminAccess.SearchAddOns(Server.HtmlEncode(txtAddonName.Text.Trim()), Server.HtmlEncode(txtAddOnTitle.Text.Trim()), txtsku.Text.Trim(), localeId).Tables[0], true);
            uxGrid.DataBind();
        }
        #endregion
    }
}