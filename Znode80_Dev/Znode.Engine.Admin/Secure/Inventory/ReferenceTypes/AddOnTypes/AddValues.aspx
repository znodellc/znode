<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false"
    Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.AddOnTypes.AddValues" CodeBehind="AddValues.aspx.cs" Title="AddOn Value" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
            <div>
                <uc1:Spacer ID="Spacer3" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <div>
                <asp:Label ID="lblAddonValueMsg" CssClass="Error" EnableViewState="false" runat="server"></asp:Label>
            </div>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ID="btnSubmitTop" OnClick="BtnAddOnValueSubmit_Click" ValidationGroup="grpAddOnValue" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancelTop" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
        </div>
        <div class="ClearBoth" align="left">
        </div>
        <br />
        <h4 class="SubTitle">
            <asp:Localize ID="GeneralSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralSettings %>'></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize ID="Label" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLabel %>'></asp:Localize><span class="Asterix">*</span><br />
            <small>
                <asp:Localize ID="LabelText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextLabel %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddOnValueName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ValidationGroup="grpAddOnValue" ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAddOnValueName"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredLabel %>' CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
        </div>

        <asp:Panel ID="pnlAddOnTextBox" runat="server" Visible="false">
            <div class="FieldStyle">
                <asp:Localize ID="DescriptionOptionValue" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDescription %>'></asp:Localize><span class="Asterix">*</span><br />
                <small>
                    <asp:Localize ID="DescriptionOptionValueText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDescriptionOptionValue %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                    ValidationGroup="grpAddOnValue" runat="server" ControlToValidate="txtDescription"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDescription %>' CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
            </div>
        </asp:Panel>

        <div class="FieldStyle">
            <asp:Localize ID="RetailPrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRetailPrice %>'></asp:Localize><span class="Asterix">*</span><br />
            <small>Enter the retail price for this product Add-On.</small>
        </div>
        <div class="ValueStyle">
            <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox
                Text="0" ID="txtAddOnValueRetailPrice" runat="server" MaxLength="7"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="grpAddOnValue"
                runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredRetailPrice %>' ControlToValidate="txtAddOnValueRetailPrice"
                CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator1" ValidationGroup="grpAddOnValue" runat="server"
                ControlToValidate="txtAddOnValueRetailPrice" Type="Currency" Operator="DataTypeCheck"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidRetailPrice %>' CssClass="Error"
                Display="Dynamic" />
            <asp:RangeValidator ID="RangeValidator1" ValidationGroup="grpAddOnValue" runat="server"
                ControlToValidate="txtAddOnValueRetailPrice" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangePrice %>'
                MaximumValue="999999" MinimumValue="-999999" Type="Currency" Display="Dynamic"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="SalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSalePrice %>'></asp:Localize><br />
            <small>
                <asp:Localize ID="SalePriceAddOnText" runat="server" Text='<%$ Resources:ZnodeAdminResource,  ColumnTextSalePriceAddOn %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox
                ID="txtSalePrice" runat="server" MaxLength="7"></asp:TextBox>
            <asp:RangeValidator CssClass="Error" ID="RangeValidator3" ValidationGroup="grpAddOnValue"
                runat="server" ControlToValidate="txtSalePrice" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeSalePrice %>'
                MaximumValue="999999" MinimumValue="-999999" Type="Currency" Display="Dynamic"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="WholeSalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWholeSalePrice %>'></asp:Localize><br />
            <small>
                <asp:Localize ID="WholeSalePriceAddOnText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextWholeSalePrice %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox
                ID="txtWholeSalePrice" runat="server" MaxLength="7"></asp:TextBox><asp:RangeValidator
                    CssClass="Error" ID="RangeValidator4" ValidationGroup="grpAddOnValue" runat="server"
                    ControlToValidate="txtWholeSalePrice" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeWholeSalePrice %>'
                    MaximumValue="999999" MinimumValue="-999999" Type="Currency" Display="Dynamic"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="Supplier" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSupplier %>'></asp:Localize><br />
            <small>
                <asp:Localize ID="SupplierText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextSupplier %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlSupplier" runat="server" />
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="TaxClass" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTaxClass %>'></asp:Localize><br />
            <small>Select tax class for this product Add-on.</small>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlTaxClass" runat="server" />
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="DisplaySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDisplaySettings %>'></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize ID="DisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder %>'></asp:Localize><span class="Asterix">*</span><br />
            <small>
                <asp:Localize ID="DisplayOrderText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextAddOnDisplayOrder %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddonValueDispOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" Display="Dynamic"
                ValidationGroup="grpAddOnValue" CssClass="Error" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrder %>' ControlToValidate="txtAddonValueDispOrder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="txtAddonValueDispOrder"
                Display="Dynamic" ValidationGroup="grpAddOnValue" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeWholeNumber %>'
                MaximumValue="999999999" MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle"></div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkIsDefault" runat="server" Text="" />
            <asp:Localize ID="DefaultAddOnValue" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDefaultAddOnValue %>'></asp:Localize><br />
        </div>

        <h4 class="SubTitle">
            <asp:Localize ID="InventorySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleInventorySettings %>'></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize ID="SKu" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSKU %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddOnValueSKU" runat="server" MaxLength="100"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtAddOnValueSKU"
                CssClass="Error" ValidationGroup="grpAddOnValue" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredValidSKU %>'></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="QuantityOnHand" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnQuantityOnHand %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddOnValueQuantity" runat="server" Rows="3">9999</asp:TextBox>
            <asp:RangeValidator ValidationGroup="grpAddOnValue" ID="RangeValidator2" runat="server"
                ControlToValidate="txtAddOnValueQuantity" CssClass="Error" Display="Dynamic"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeQuantity %>' MaximumValue="999999" MinimumValue="0"
                SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
            <asp:RequiredFieldValidator ValidationGroup="grpAddOnValue" ID="RequiredFieldValidator6"
                runat="server" ControlToValidate="txtAddOnValueQuantity" CssClass="Error" Display="Dynamic"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredQuantity %>'></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ReOrderLevel" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnReOrderLevel %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtReOrder" runat="server"></asp:TextBox>
            <asp:RangeValidator ValidationGroup="grpAddOnValue" ID="RangeValidator5" runat="server"
                ControlToValidate="txtReOrder" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeQuantity %>'
                MaximumValue="999999" MinimumValue="0" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="ShippingSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleShippingSettings %>'></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize ID="FreeShipping" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFreeShipping %>'></asp:Localize><br />
            <small>
                <asp:Localize ID="FreeShippingText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextFreeShipping %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkFreeShippingInd" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableFreeShipping %>'
                runat="server" />
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="ShippingType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectShippingType %>'></asp:Localize><span class="Asterix">*</span><br />
            <small>
                <asp:Localize ID="ShippingTypeText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextShippingType %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ShippingTypeList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ShippingTypeList_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="Weight" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnItemWeight %>'></asp:Localize><br />
            <small>
                <asp:Localize ID="WeightText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextItemWeight %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddOnValueWeight" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.WeightUnit %>
            <asp:RangeValidator Enabled="false" ID="weightBasedRangeValidator" runat="server"
                ControlToValidate="txtAddOnValueWeight" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeWeight %>'
                MaximumValue="9999999" MinimumValue="0.1" CultureInvariantValues="true" Type="Double"
                ValidationGroup="grpAddOnValue"></asp:RangeValidator>
            <asp:RequiredFieldValidator Enabled="false" ID="RequiredForWeightBasedoption" runat="server"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredWeight %>' ControlToValidate="txtAddOnValueWeight"
                CssClass="Error" Display="Dynamic" ValidationGroup="grpAddOnValue"></asp:RequiredFieldValidator>
            <asp:CompareValidator ID="CompareValidator3" ValidationGroup="grpAddOnValue" runat="server"
                ControlToValidate="txtAddOnValueWeight" Type="Currency" Operator="DataTypeCheck"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidWeight %>' CssClass="Error" Display="Dynamic" />
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="HeightColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnHeight %>'></asp:Localize><br />
            <small>
                <asp:Localize ID="HeightText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextHeight %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="Height" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>
            <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="Height"
                Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidHeight %>'
                CssClass="Error" Display="Dynamic" />
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="WidthColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWidth %>'></asp:Localize><br />
            <small>
                <asp:Localize ID="WidthText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextWidth %>'></asp:Localize></small>
        </div>
        <div>
            <div class="ValueStyle">
                <asp:TextBox ID="Width" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>
                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="Width"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidWidth %>'
                    CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="LengthColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLength %>'></asp:Localize><br />
                <small>
                    <asp:Localize ID="LengthText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextLength %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="Length" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>
                <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="Length"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidLength %>'
                    CssClass="Error" Display="Dynamic" />
            </div>
            <h4 class="SubTitle">
                <asp:Localize ID="RecurringBillingSubTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleRecurringBillingSettings %>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="RecurringBilling" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRecurringBilling %>'></asp:Localize>
            </div>
            <div class="ValueStyleText">
                <asp:CheckBox ID="chkRecurringBillingInd" AutoPostBack="true" OnCheckedChanged="ChkRecurringBillingInd_CheckedChanged"
                    runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableRecurringBilling %>' />
            </div>
            <asp:Panel ID="pnlRecurringBilling" runat="server" Visible="false">
                <div class="FieldStyle">
                    <asp:Localize ID="BillingAmount" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBillingAmount %>'></asp:Localize><span class="Asterix">*</span><br />
                    <small>
                        <asp:Localize ID="BillingAmountText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextBillingAmount %>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;
                    <asp:TextBox ID="txtRecurringBillingInitialAmount" MaxLength="10" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ValidationGroup="grpAddOnValue" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredBillingAmount %>'
                        ControlToValidate="txtRecurringBillingInitialAmount" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="txtRecurringBillingInitialAmount" ValidationGroup="grpAddOnValue"
                        Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidBillingAmount %>'
                        CssClass="Error" Display="Dynamic" /><br />
                    <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="txtRecurringBillingInitialAmount" ValidationGroup="grpAddOnValue"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeBillingAmount %>'
                        MaximumValue="999999.99" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="BillingPeriod" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBillingPeriod %>'></asp:Localize><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlBillingPeriods" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DdlBillingPeriods_SelectedIndexChanged">
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextWeekly %>' Value="WEEK"></asp:ListItem>
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextMonthly %>' Value="MONTH"></asp:ListItem>
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextYearly %>' Value="YEAR"></asp:ListItem>
                    </asp:DropDownList>
                </div>

            </asp:Panel>
            <div>
                <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <div class="ClearBoth">
            </div>
            <div>
                <zn:Button runat="server" ID="btnSubmitBottom" OnClick="BtnAddOnValueSubmit_Click" CausesValidation="true" ValidationGroup="grpAddOnValue" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
                <zn:Button runat="server" ID="btnCancelBottom" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
            </div>
        </div>
    </div>
</asp:Content>
