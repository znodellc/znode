using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.AddOnTypes
{
    /// <summary>
    /// Represents the Site Admin - Admin_Secure_catalog_product_addons_view class
    /// </summary>
    public partial class View : System.Web.UI.Page
    {
        #region Protected Variables
        private int _ItemId;        
        private string CatalogImagePath = string.Empty;
        private string AddOnValuepageLink = "~/Secure/Inventory/ReferenceTypes/AddOnTypes/addvalues.aspx?itemid=";
        private string EditLink = "~/Secure/Inventory/ReferenceTypes/AddOnTypes/add.aspx?itemid=";
        private string ListLink = "~/Secure/Inventory/ReferenceTypes/AddOnTypes/default.aspx";
        #endregion

        /// <summary>
        /// Gets or sets the item Id.
        /// </summary>
        public int ItemId
        {
            get { return this._ItemId; }
            set { this._ItemId = value; }
        }

        #region Helper Methods
        /// <summary>
        /// Set Image Method
        /// </summary>
        /// <returns>Returns the Image</returns>
        public string SetImage()
        {
            return ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse("false"));
        }

        /// <summary>
        /// Get Quantity Method
        /// </summary>
        /// <param name="addOnValueId">The value of addOnValueId</param>
        /// <returns>Returns the Quantity</returns>
        public string GetQuantity(int addOnValueId)
        {
            ZNode.Libraries.DataAccess.Service.AddOnValueService avs = new ZNode.Libraries.DataAccess.Service.AddOnValueService();
            return ProductAdmin.GetQuantity(avs.GetByAddOnValueID(addOnValueId)).ToString();
        }

        /// <summary>
        /// Get Reorder Level Method
        /// </summary>
        /// <param name="addOnValueId">The value of addOnValueId</param>
        /// <returns>Returns the ReOrder Level</returns>
        public string GetReOrderLevel(int addOnValueId)
        {
            ZNode.Libraries.DataAccess.Service.AddOnValueService avs = new ZNode.Libraries.DataAccess.Service.AddOnValueService();
            return ProductAdmin.GetAddOnInventory(avs.GetByAddOnValueID(addOnValueId)).ReOrderLevel.ToString();
        }
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Create New AddOnValue Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddNewAddOnValues_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddOnValuepageLink + this.ItemId);
        }

        /// <summary>
        /// Back button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Btnback_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListLink);
        }

        /// <summary>
        /// Edit Add-On Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditAddOn_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.EditLink + this.ItemId);
        }

        /// <summary>
        /// BAck to Add-On Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BacktoAddOn_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListLink);
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Add Client side event to the Delete Button in the Grid.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                // Retrieve the Button control from the Seventh column.
                LinkButton DeleteButton = (LinkButton)e.Row.Cells[7].FindControl("btnDelete");

                // Set the Button's CommandArgument property with the row's index.
                DeleteButton.CommandArgument = e.Row.RowIndex.ToString();

                // Add Client Side confirmation
                DeleteButton.OnClientClick = "return confirm('" + this.GetGlobalResourceObject("ZnodeAdminResource","ConfirmDeleteAddOn").ToString() + "');";
            }
        }
        
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGrid();
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // Property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate
                // Cell in the GridView control.
                GridViewRow selectedRow = uxGrid.Rows[index];

                TableCell Idcell = selectedRow.Cells[0];
                string Id = Idcell.Text;

                if (e.CommandName == "Edit")
                {
                    this.EditLink = this.AddOnValuepageLink + this.ItemId + "&AddOnValueId=" + Id;
                    Response.Redirect(this.EditLink);
                }
                
                if (e.CommandName == "Delete")
                {
                    ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();

                    // Get the Addon name from ItemId
                    AddOn addon = new AddOn();
                    addon = AdminAccess.GetByAddOnId(this.ItemId);
                    string addonname = addon.Name;

                    // Get AddOnValue name from Id
                    AddOnValue addOnValue = new AddOnValue();
                    addOnValue = AdminAccess.GetByAddOnValueID(int.Parse(Id));

                    string associatename = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteAddOnValues").ToString() + addOnValue.Name;

                    bool Status = AdminAccess.DeleteAddOnValue(int.Parse(Id));

                    if (Status)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associatename, addonname);

                        this.BindGrid();
                    }
                }
            }
        }
        #endregion

        #region Bind Methods
        /// <summary>
        ///  Bind data to grid
        /// </summary>
        private void BindGrid()
        {
            ProductAddOnAdmin AddOnValueAdmin = new ProductAddOnAdmin();
            TList<AddOnValue> ValueList = AddOnValueAdmin.GetAddOnValuesByAddOnId(this.ItemId);
            if (ValueList != null)
            {
                ValueList.Sort("DisplayOrder");
            }

            uxGrid.DataSource = ValueList;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        private void BindData()
        {
            ProductAddOnAdmin AddOnAdmin = new ProductAddOnAdmin();
            AddOn addOnEntity = AddOnAdmin.GetByAddOnId(this.ItemId);

            if (addOnEntity != null)
            {
                lblTitle.Text = addOnEntity.Name;
                lblName.Text = addOnEntity.Name;
                lblAddOnTitle.Text = addOnEntity.Title;
                lblDisplayOrder.Text = addOnEntity.DisplayOrder.ToString();
                lblDisplayType.Text = addOnEntity.DisplayType.ToString();

                // Display Settings
                chkOptionalInd.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(addOnEntity.OptionalInd.ToString()));

                // Inventory Setting - Out of Stock Options
                if (addOnEntity.TrackInventoryInd && addOnEntity.AllowBackOrder == false)
                {
                    chkCartInventoryEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse("true"));
                    chkIsBackOrderEnabled.Src = this.SetImage();
                    chkIstrackInvEnabled.Src = this.SetImage();
                }
                else if (addOnEntity.TrackInventoryInd && addOnEntity.AllowBackOrder)
                {
                    chkCartInventoryEnabled.Src = this.SetImage();
                    chkIsBackOrderEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse("true"));
                    chkIstrackInvEnabled.Src = this.SetImage();
                }
                else if ((addOnEntity.TrackInventoryInd == false) && (addOnEntity.AllowBackOrder == false))
                {
                    chkCartInventoryEnabled.Src = this.SetImage();
                    chkIsBackOrderEnabled.Src = this.SetImage();
                    chkIstrackInvEnabled.Src = ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse("true"));
                }

                this.BindGrid();

                // Inventory Setting - Stock Messages
                lblInStockMsg.Text = addOnEntity.InStockMsg;
                lblOutofStock.Text = addOnEntity.OutOfStockMsg;
                lblBackOrderMsg.Text = addOnEntity.BackOrderMsg;

            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotFoundAddOnRequested").ToString());
            }
        }
        #endregion       
    }
}