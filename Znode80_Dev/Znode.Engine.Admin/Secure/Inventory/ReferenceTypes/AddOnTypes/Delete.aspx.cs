using System;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.AddOnTypes
{
    /// <summary>
    /// Represents the Site Admin - Admin_Secure_catalog_product_addons_delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Variables
        private string _ProductAddOnName = string.Empty;
        private int ItemId;
        private string AddonName = string.Empty;
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets the Product Addon Name
        /// </summary>
        public string ProductAddOnName
        {
            get 
            { 
                return this._ProductAddOnName; 
            }

            set 
            { 
                this._ProductAddOnName = value; 
            }
        }
        #endregion

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            this.BindData();

            DeleteConfirmText.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "TextConfirmDeleteAddOn").ToString(), this.ProductAddOnName);
        }

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the screen
        /// </summary>
        protected void BindData()
        {
            ProductAddOnAdmin AddOnAdmin = new ProductAddOnAdmin();
            AddOn Entity = AddOnAdmin.GetByAddOnId(this.ItemId);

            if (Entity != null)
            {
                this.ProductAddOnName = Entity.Name;
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotFoundProductAddOn").ToString());
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Inventory/ReferenceTypes/AddOnTypes/default.aspx");
        }

        /// <summary>
        /// Delete Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            bool retval = false;

            try
            {
                ProductAddOnAdmin AddOnAdmin = new ProductAddOnAdmin();
                AddOn AddOn = new AddOn();
                AddOn.AddOnID = this.ItemId;
                this.AddonName = AddOn.Name;

                retval = AddOnAdmin.DeleteAddOn(AddOn);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }

            if (retval)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteAddOnValues").ToString() + this.AddonName, this.AddonName);
                Response.Redirect("~/Secure/Inventory/ReferenceTypes/AddOnTypes/Default.aspx");
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorDeleteAddOnValues").ToString();
            }
        }
        #endregion
    }
}