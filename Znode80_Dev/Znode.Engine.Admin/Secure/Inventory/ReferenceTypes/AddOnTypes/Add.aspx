<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.AddOnTypes.Add" ValidateRequest="false" Title="Product AddOn Type - Add" CodeBehind="Add.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox" TagPrefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
            <div>
                <asp:Label ID="lblMsg" CssClass="Error" EnableViewState="false" runat="server"></asp:Label>
            </div>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ID="btnSubmitTop" OnClick="BtnSubmit_Click" ValidationGroup="grpAddOn" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>'/>
            <zn:Button runat="server" ID="btnCancelTop" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>'/>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <h4 class="SubTitle"><asp:Localize ID="GeneralSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralSettings %>'></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnName %>'></asp:Localize><span class="Asterix">*</span><br />
            <small><asp:Localize ID="AddOnNameText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextAddOnName %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtName" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredAddOnName %>' ValidationGroup="grpAddOn" CssClass="Error"
                Display="Dynamic"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="TitleColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitle %>'></asp:Localize><span class="Asterix">*</span><br />
            <small><asp:Localize ID="AddOnTitleText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextAddOnTitle %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtAddOnTitle" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAddOnTitle"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredAddOnTitle %>' ValidationGroup="grpAddOn" CssClass="Error"
                Display="Dynamic"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="AddOnDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAddOnDescription %>'></asp:Localize><br />
            <small><asp:Localize ID="AddOnDescriptionText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextAddOnDescription %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <uc1:HtmlTextBox ID="ctrlHtmlText" runat="server"></uc1:HtmlTextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="AddOnDisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder %>'></asp:Localize><span class="Asterix">*</span><br />
            <small><asp:Localize ID="AddOnDisplayOrderText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextAddOnDisplayOrder %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator3" CssClass="Error" runat="server" Display="Dynamic"
                ValidationGroup="grpAddOn" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrder %>' ControlToValidate="txtDisplayOrder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" CssClass="Error" ControlToValidate="txtDisplayOrder"
                Display="Dynamic" ValidationGroup="grpAddOn" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeWholeNumber %>'
                MaximumValue="999999999" MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="DisplayType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayType %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="ddlDisplayType" runat="server">
                <asp:ListItem Value="DropDownList" Text='<%$ Resources:ZnodeAdminResource, DropDownTextDropDownList %>'></asp:ListItem>
                <asp:ListItem Value="RadioButton" Text='<%$ Resources:ZnodeAdminResource, DropDownTextRadioButtons %>'></asp:ListItem>
                <asp:ListItem Value="CheckBox" Text='<%$ Resources:ZnodeAdminResource, DropDownTextCheckBox %>'></asp:ListItem>
                <asp:ListItem Value="TextBox" Text='<%$ Resources:ZnodeAdminResource, DropDownTextTextBox %>'></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="FieldStyle" runat="server" id="lblLocale" style="display: none;">
            <asp:Localize ID="Locale" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLocale %>'></asp:Localize> <span class="Asterix">*</span>
        </div>
        <div class="ValueStyle" style="display: none;">
            <asp:DropDownList ID="ddlLocales" runat="server" AppendDataBoundItems="true">
                <asp:ListItem Value="" Text='<%$ Resources:ZnodeAdminResource, DropDownTextSelect %>'></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="FieldStyle"></div>
        <div class="ValueStyleText">
            <asp:CheckBox ID="chkOptionalInd" CssClass="HintStyle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAddOnOptional %>' Height="30px" />
        </div>

        <h4 class="SubTitle"><asp:Localize ID="InventorySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleInventorySettings %>'></asp:Localize></h4>
        <div class="FieldStyle">
            <asp:Localize ID="OutOfStockOptions" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOutOfStockOptions %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:RadioButtonList ID="InvSettingRadiobtnList" runat="server">
                <asp:ListItem Selected="True" Value="1" Text='<%$ Resources:ZnodeAdminResource, ColumnTrackInventory %>'></asp:ListItem>
                <asp:ListItem Value="2" Text='<%$ Resources:ZnodeAdminResource, ColumnBackOrder %>'></asp:ListItem>
                <asp:ListItem Value="3" Text='<%$ Resources:ZnodeAdminResource, ColumnNoTrackInventory %>'></asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="InStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnInStockMessage %>'></asp:Localize><br />
            <small><asp:Localize ID="InStockMessageText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextInStockMessage %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtInStockMsg" runat="server"></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="OutOfStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOutOfStockMessage %>'></asp:Localize><br />
            <small><asp:Localize ID="OutOfStockMessageText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextOutOfStockMessage %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtOutofStock" runat="server" Text='<%$ Resources:ZnodeAdminResource, ValueOutOfStock %>'></asp:TextBox>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="BackOrderMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBackOrderMessage %>'></asp:Localize><br />
            <small><asp:Localize ID="BackOrderMessageText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextBackOrderMessage %>'></asp:Localize></small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="txtBackOrderMsg" runat="server"></asp:TextBox>
        </div>

        <div class="ClearBoth">
        </div>

        <div>
             <zn:Button runat="server" ID="btnSubmitBottom" OnClick="BtnSubmit_Click" ValidationGroup="grpAddOn" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true"/>
             <zn:Button runat="server" ID="btnCancelBottom" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>'/>
        </div>
    </div>
</asp:Content>
