<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.AddOnTypes.View" ValidateRequest="false" Title="Product AddOn Type - View" CodeBehind="View.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Display">
        <div class="LeftFloat" style="width: 50%; text-align: left">
            <h1><asp:Localize ID="ProductAddOn" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleProductAddOn %>'></asp:Localize>
                <asp:Label ID="lblTitle" runat="server" Text="Label"></asp:Label></h1>
        </div>
        <div style="text-align: right">
              <zn:Button runat="server" ID="btneditAddon" OnClick="EditAddOn_Click" Width="150px" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonEditAddOn %>' />
              <zn:Button runat="server" ID="btnBack" OnClick="BacktoAddOn_Click" Width="150px" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonBackToAddOn %>' />
        </div>
        <div class="ClearBoth">
        </div>
        <h4 class="SubTitle"><asp:Localize ID="GeneralInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralInformation %>'></asp:Localize></h4>
        <div class="FieldStyle" nowrap="nowrap">
            <asp:Localize ID="Name" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnName %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:Label ID="lblName" runat="server"></asp:Label>&nbsp;
        </div>
        <div class="FieldStyleA" nowrap="nowrap">
            <asp:Localize ID="TitleColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitle %>'></asp:Localize>
        </div>
        <div class="ValueStyleA">
            <asp:Label ID="lblAddOnTitle" runat="server"></asp:Label>&nbsp;
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="DisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:Label ID="lblDisplayOrder" runat="server"></asp:Label>
        </div>
        <div class="FieldStyleA">
            <asp:Localize ID="DisplayType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayType %>'></asp:Localize>
        </div>
        <div class="ValueStyleA">
            <asp:Label ID="lblDisplayType" runat="server"></asp:Label>&nbsp;
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="Optional" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOptionalAddOn %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <img id="chkOptionalInd" runat="server" alt="" src="" />
        </div>

        <div class="ClearBoth">
            <br />
        </div>

        <h4 class="SubTitle"><asp:Localize ID="InventorySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleInventorySettings %>'></asp:Localize></h4>
        <div class="FieldStyleImg">
            <img id="chkCartInventoryEnabled" runat="server" alt="" src='' />
        </div>
        <div class="ValueStyleImg">
            <asp:Localize ID="TrackInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTrackInventory %>'></asp:Localize>
        </div>
        <div class="FieldStyleImgA">
            <img id="chkIsBackOrderEnabled" runat="server" alt="" src='' />
        </div>
        <div class="ValueStyleImgA">
            <asp:Localize ID="BackOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBackOrder %>'></asp:Localize>
        </div>
        <div class="FieldStyleImg">
            <img id="chkIstrackInvEnabled" runat="server" alt="" src="" />
        </div>
        <div class="ValueStyleImg">
            <asp:Localize ID="NoTrackInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnNoTrackInventory %>'></asp:Localize>
        </div>
        <div class="FieldStyleA">
            <asp:Localize ID="InStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnInStockMessage %>'></asp:Localize>
        </div>
        <div class="ValueStyleA">
            <asp:Label ID="lblInStockMsg" runat="server"></asp:Label>&nbsp;
        </div>
        <div class="FieldStyle" nowrap="nowrap">
            <asp:Localize ID="OutOfStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOutOfStockMessage %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:Label ID="lblOutofStock" runat="server"></asp:Label>&nbsp;
        </div>
        <div class="FieldStyleA" nowrap="nowrap">
            <asp:Localize ID="BackOrderMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBackOrderMessage %>'></asp:Localize>
        </div>
        <div class="ValueStyleA">
            <asp:Label ID="lblBackOrderMsg" runat="server"></asp:Label>&nbsp;
        </div>
        <div class="ClearBoth">
            <br />
            <div style="width: 100%; text-align: right; display: inline-table;">
                <zn:LinkButton ID="btnAddNewAddOnValues" runat="server"
                    ButtonType="Button" OnClick="BtnAddNewAddOnValues_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddValue %>'
                    ButtonPriority="Primary" />
            </div>
            <div>
                <uc1:Spacer ID="Spacer6" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
            </div>
            <h4 class="GridTitle"><asp:Localize ID="GridTitleAddOnValues" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleAddOnValues %>'></asp:Localize>
            </h4>
            <asp:GridView OnRowDataBound="UxGrid_RowDataBound" ID="uxGrid" runat="server" CssClass="Grid"
                AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" GridLines="None"
                OnPageIndexChanging="UxGrid_PageIndexChanging" CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand"
                Width="100%" EnableSortingAndPagingCallbacks="False" PageSize="25" AllowSorting="True"
                EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundAddOnTypes %>'
                OnRowDeleting="UxGrid_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="AddOnValueId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <a href='AddValues.aspx?itemid=<%=ItemId %>&AddOnValueId=<%# DataBinder.Eval(Container.DataItem, "AddOnvalueId").ToString()%>'>
                                <%# Eval("Name") %></a>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="SKU" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProductsSKU %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleQuantityOnHand %>'>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="quantity" Text='<%# GetQuantity((int)Eval("AddOnvalueId")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleReOrderLevel %>'>
                        <ItemTemplate>
                            <asp:Label runat="server" ID="ReOrderLevel" Text='<%# GetReOrderLevel((int)Eval("AddOnvalueId")) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePrice %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <%# DataBinder.Eval(Container.DataItem, "Retailprice","{0:c}").ToString()%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDefault %>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "DefaultInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField CommandName="Edit" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' ButtonType="Link">
                        <ControlStyle CssClass="actionlink" />
                    </asp:ButtonField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton CommandName="Delete" CausesValidation="false" ID="btnDelete" runat="server"
                                Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CssClass="actionlink" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
        </div>

    </div>
</asp:Content>
