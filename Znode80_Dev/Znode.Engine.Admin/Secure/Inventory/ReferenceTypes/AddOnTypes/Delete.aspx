<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.AddOnTypes.Delete" Title="Product AddOn Type - Delete" CodeBehind="Delete.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h5><asp:Localize ID="PleaseConfirmText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm %>'></asp:Localize></h5>
    <p>
        <asp:label ID="DeleteConfirmText" runat="server"></asp:label>
    </p>
    <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label><br />
    <br />
    <zn:Button runat="server" ID="btnDelete" OnClick="BtnDelete_Click" CausesValidation="True" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>' />
    <zn:Button runat="server" ID="btnCancel" OnClick="BtnCancel_Click" CausesValidation="False" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
    <br />
    <br />
    <br />
</asp:Content>

