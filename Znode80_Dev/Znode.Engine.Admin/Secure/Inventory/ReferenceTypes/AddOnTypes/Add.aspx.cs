using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace  Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.AddOnTypes
{
    /// <summary>
    /// Represents the Site Admin - Admin_Secure_catalog_product_addons_add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        private int ProductId = 0;
        private bool mode = false;
        private string ListLink = "~/Secure/Inventory/ReferenceTypes/AddOnTypes/default.aspx";
        private string ViewLink = "~/Secure/Inventory/ReferenceTypes/AddOnTypes/view.aspx?itemid=";
        private string CancelLink = "~/Secure/Inventory/ReferenceTypes/AddOnTypes/view.aspx?itemid=";
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            // Get productid from querystring        
            if (Request.Params["zpid"] != null)
            {
                this.ProductId = int.Parse(Request.Params["zpid"]);
            }

            // Reset the edit fields
            if (Request.Params["mode"] != null)
            {
                this.mode = bool.Parse(Request.Params["mode"]);
            }

            // Redirect directly to product details page
            if (this.mode)
            {
                // Reset the URL with Product details page
                this.ListLink = "~/Secure/Inventory/Products/view.aspx?itemid=" + this.ProductId + "&mode=addons";
                this.ViewLink = this.ListLink;
                this.CancelLink = this.ListLink;
            }

            if (Page.IsPostBack == false)
            {
                // Bind the language dropdown.
                this.BindLocaleDropdown();

                // If edit func then bind the data fields
                if (this.ItemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource","TitleEditAddOn").ToString();
                    this.BindEditData();
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddAddOn").ToString();
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAddOnAdmin AddonAdmin = new ProductAddOnAdmin();
            AddOn AddOnEntityObject = new AddOn();

            if (this.ItemId > 0)
            {
                AddOnEntityObject = AddonAdmin.GetByAddOnId(this.ItemId);
            }
            
            // Set properties - General settings
            AddOnEntityObject.Name = Server.HtmlEncode(txtName.Text.Trim());
            AddOnEntityObject.Title = Server.HtmlEncode(txtAddOnTitle.Text.Trim());
            AddOnEntityObject.DisplayOrder = int.Parse(txtDisplayOrder.Text.Trim());
            AddOnEntityObject.OptionalInd = chkOptionalInd.Checked;
            AddOnEntityObject.Description = ctrlHtmlText.Html;
            AddOnEntityObject.DisplayType = ddlDisplayType.SelectedItem.Value;

            // Set properties - Inventory settings
            // Out of Stock Options
            if (InvSettingRadiobtnList.SelectedValue.Equals("1"))
            {
                // Only Sell if Inventory Available - Set values
                AddOnEntityObject.TrackInventoryInd = true;
                AddOnEntityObject.AllowBackOrder = false;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("2"))
            {
                // Allow Back Order - Set values
                AddOnEntityObject.TrackInventoryInd = true;
                AddOnEntityObject.AllowBackOrder = true;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("3"))
            {
                // Don't Track Inventory - Set property values
                AddOnEntityObject.TrackInventoryInd = false;
                AddOnEntityObject.AllowBackOrder = false;
            }

            // Inventory Setting - Stock Messages
            if (txtOutofStock.Text.Trim().Length == 0)
            {
                AddOnEntityObject.OutOfStockMsg = this.GetGlobalResourceObject("ZnodeAdminResource", "ValueOutOfStock").ToString();
            }
            else
            {
                AddOnEntityObject.OutOfStockMsg = Server.HtmlEncode(txtOutofStock.Text.Trim());
            }
            
            AddOnEntityObject.InStockMsg = Server.HtmlEncode(txtInStockMsg.Text.Trim());
            AddOnEntityObject.BackOrderMsg = Server.HtmlEncode(txtBackOrderMsg.Text.Trim());

            if (!string.IsNullOrEmpty(ddlLocales.SelectedValue))
            {
                AddOnEntityObject.LocaleId = int.Parse(ddlLocales.SelectedValue);
            }
            else
            {
                AddOnEntityObject.LocaleId = 43;
            }

            bool retval = false;

            if (this.ItemId > 0)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, GetGlobalResourceObject("ZnodeAdminResource","ActivityLogEditAddOns").ToString() + txtName.Text.Trim(), txtName.Text.Trim());

                retval = AddonAdmin.UpdateNewProductAddOn(AddOnEntityObject);
            }
            else
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddAddOns").ToString() + txtName.Text.Trim(), txtName.Text.Trim());

                retval = AddonAdmin.CreateNewProductAddOn(AddOnEntityObject, out this.ItemId);
            }

            if (retval)
            {
                if (this.mode)
                {
                    Response.Redirect(this.ViewLink);
                }

                Response.Redirect(this.ViewLink + this.ItemId);
            }
            else
            {
                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorAddOn").ToString();
                lblMsg.CssClass = "Error";
                return;
            }
        }

        /// <summary>
        /// Cancel Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.ItemId > 0)
            {
                Response.Redirect(this.CancelLink + this.ItemId);
            }
            else
            {
                Response.Redirect(this.ListLink);
            }
        }
        #endregion

        #region Bind Data

        /// <summary>
        /// Bind the locale dropdown
        /// </summary>
        private void BindLocaleDropdown()
        {
            LocaleService localeService = new LocaleService();
            TList<Locale> localeList = localeService.GetAll();
            localeList.Sort("LocaleDescription");

            // Bind the dropdown.
            ddlLocales.DataSource = localeList;
            ddlLocales.DataTextField = "LocaleDescription";
            ddlLocales.DataValueField = "LocaleId";
            ddlLocales.DataBind();

            if (ddlLocales.Items.Count == 0)
            {
                lblLocale.Visible = false;
                ddlLocales.Visible = false;
            }
        }

        /// <summary>
        /// Bind data to the fields on the edit screen
        /// </summary>
        private void BindEditData()
        {
            ProductAddOnAdmin AddOnAdmin = new ProductAddOnAdmin();
            AddOn addOnEntity = AddOnAdmin.GetByAddOnId(this.ItemId);

            if (addOnEntity != null)
            {
                lblTitle.Text += addOnEntity.Name;
                txtName.Text = Server.HtmlDecode(addOnEntity.Name);
                txtAddOnTitle.Text = Server.HtmlDecode(addOnEntity.Title);
                txtDisplayOrder.Text = addOnEntity.DisplayOrder.ToString();
                chkOptionalInd.Checked = addOnEntity.OptionalInd;
                ctrlHtmlText.Html = addOnEntity.Description;
                ddlDisplayType.SelectedValue = addOnEntity.DisplayType;

                // Inventory Setting - Out of Stock Options
                if (addOnEntity.TrackInventoryInd && addOnEntity.AllowBackOrder == false)
                {
                    InvSettingRadiobtnList.Items[0].Selected = true;
                }
                else if (addOnEntity.TrackInventoryInd && addOnEntity.AllowBackOrder)
                {
                    InvSettingRadiobtnList.Items[1].Selected = true;
                }
                else if (addOnEntity.TrackInventoryInd == false && addOnEntity.AllowBackOrder == false)
                {
                    InvSettingRadiobtnList.Items[2].Selected = true;
                }

                // Inventory Setting - Stock Messages
                txtInStockMsg.Text = Server.HtmlDecode(addOnEntity.InStockMsg);
                txtOutofStock.Text = Server.HtmlDecode(addOnEntity.OutOfStockMsg);
                txtBackOrderMsg.Text = Server.HtmlDecode(addOnEntity.BackOrderMsg);

                // Bind the selected locale in the dropdown.
                ListItem li = ddlLocales.Items.FindByValue(addOnEntity.LocaleId.ToString());
                if (li != null)
                {
                    li.Selected = true;
                }
            }
            else
            {
                throw new ApplicationException(GetGlobalResourceObject("ZnodeAdminResource","RecordNotFoundAddOnRequested").ToString());
            }
        }
        #endregion
    }
}