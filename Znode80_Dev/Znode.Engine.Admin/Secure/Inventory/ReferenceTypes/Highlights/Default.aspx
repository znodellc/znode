<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.Highlights.Default" Title="Manage Highlights" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Localize runat="server" ID="TitleHighlight" Text='<%$ Resources:ZnodeAdminResource, TitleHighlight %>'></asp:Localize>
            </h1>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAddHighlight" runat="server"
                ButtonType="Button" OnClick="BtnAddHighlight_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonAddHighlight %>'
                ButtonPriority="Primary" />
        </div>
        <div class="ClearBoth">
            <p>
                <asp:Localize runat="server" ID="TextCreateHighlight" Text='<%$ Resources:ZnodeAdminResource, TextCreateHighlight %>'></asp:Localize>
            </p>
            <br />
        </div>
        <h4 class="SubTitle">
            <asp:Localize runat="server" ID="SubTitleSearchHightlight" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchHightlight %>'></asp:Localize>
        </h4>
        <asp:Panel ID="pnlSearch" runat="server" DefaultButton="btnSearch">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="ColumnTitleName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>'></asp:Localize>
                        </span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtName" runat="server" ValidationGroup="grpSearch"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="ColumnTitleType" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleType %>'></asp:Localize>
                        </span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlHighlightType" runat="server">
                            </asp:DropDownList>
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <zn:Button runat="server" ID="btnClearSearch"  ButtonType="CancelButton" OnClick="BtnClearSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                    <zn:Button runat="server" ID="btnSearch" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle">
            <asp:Localize runat="server" ID="GridTitleHightlights" Text='<%$ Resources:ZnodeAdminResource, GridTitleHightlights %>'></asp:Localize>
        </h4>
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <asp:GridView ID="uxGrid" Width="100%" CssClass="Grid" CellPadding="4" CaptionAlign="Left"
            GridLines="None" runat="server" AutoGenerateColumns="False" AllowPaging="True"
            ForeColor="Black" OnPageIndexChanging="UxGrid_PageIndexChanging" OnRowCommand="UxGrid_RowCommand"
            PageSize="25" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyNoHighlightsText %>'>
            <FooterStyle CssClass="FooterStyle" />
            <Columns>
                <asp:BoundField DataField="HighlightID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left"
                    Visible="true" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleImage %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='<%# "add.aspx?itemid=" + DataBinder.Eval(Container.DataItem,"Highlightid")%>'
                            id="LinkView">
                            <img id="Img1" alt="" src='<%# GetImagePath(Eval("ImageFile").ToString()) %>' runat="server"
                                style="border: none" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                    <HeaderTemplate>
                        <asp:Localize runat="server" ID="ColumnTitleEnableHyperlink" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEnableHyperlink %>'></asp:Localize>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <img alt="" id="Img2" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark((bool)DataBinder.Eval(Container.DataItem, "DisplayPopup"))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleType %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# GetHighlightTypeName(DataBinder.Eval(Container.DataItem, "HighlightId")) %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' DataField="DisplayOrder" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsActive %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img id="Img3" alt='' src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark((bool)DataBinder.Eval(Container.DataItem, "ActiveInd"))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:LinkButton ID="Button1" CommandName="Edit" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"HighlightID")%>'
                            Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' runat="server" CssClass="actionlink" />
                        &nbsp;<asp:LinkButton ID="Button5" CommandName="Delete" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"HighlightID")%>'
                            Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' runat="server" CssClass="actionlink" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="RowStyle" />
            <EditRowStyle CssClass="EditRowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>
