using System;
using System.Data;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Utilities;

namespace  Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.Highlights
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Setup.ReferenceTypes.Highlights - Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string AddHighlightPageLink = "~/Secure/Inventory/ReferenceTypes/Highlights/Add.aspx";
        private string DeletePageLink = "~/Secure/Inventory/ReferenceTypes/Highlights/Delete.aspx";
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindHighLightType();
                this.BindSearchData();
            }
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.AddHighlightPageLink + "?itemid=" + e.CommandArgument);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.DeletePageLink + "?itemid=" + e.CommandArgument);
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Add Highlights Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddHighlight_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddHighlightPageLink);
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClearSearch_Click(object sender, EventArgs e)
        {
            ddlHighlightType.ClearSelection();
            txtName.Text = string.Empty;
            this.BindHighLightType();
            this.BindSearchData();
        }

        #endregion

        #region Private Methods
       
        /// <summary>
        /// Get Image Path
        /// </summary>
        /// <param name="filename">The value of filename</param>
        /// <returns>Returns the Image Path</returns>
        protected string GetImagePath(string filename)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(filename);
        }
        
        /// <summary>
        /// Get HighLight Type Name
        /// </summary>
        /// <param name="obj">The value of object obj</param>
        /// <returns>Returns the Highlight Type Name</returns>
        protected string GetHighlightTypeName(object obj)
        {
            HighlightService hs = new HighlightService();
            Highlight highlight = hs.GetByHighlightID(int.Parse(obj.ToString()));
            hs.DeepLoad(highlight);

            if (highlight != null && highlight.HighlightTypeIDSource != null)
            {
                return highlight.HighlightTypeIDSource.Name;
            }

            return null;
        }
        
        /// <summary>
        /// Binds Highlight Type drop-down list
        /// </summary>
        private void BindHighLightType()
        {
            ZNode.Libraries.Admin.HighlightAdmin highlight = new HighlightAdmin();
            DataSet dt = new DataSet();
            ddlHighlightType.DataSource = highlight.GetAllHighLightType();
            ddlHighlightType.DataTextField = "Name";
            ddlHighlightType.DataValueField = "HighlightTypeId";
            ddlHighlightType.DataBind();
            ListItem item2 = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString().ToUpper(), string.Empty);
            ddlHighlightType.Items.Insert(0, item2);
            ddlHighlightType.SelectedIndex = 0;
        }

        /// <summary>
        /// Binds Search Data
        /// </summary>
        private void BindSearchData()
        {
            HighlightService hs = new HighlightService();
            HighlightQuery hq = new HighlightQuery();

            if (txtName.Text.Trim().Length > 0)
            {
                hq.AppendLike(HighlightColumn.Name, "%" + Server.HtmlEncode(txtName.Text) + "%");
            }

            if (ddlHighlightType.SelectedValue.Trim().Length > 0)
            {
                hq.Append(HighlightColumn.HighlightTypeID, ddlHighlightType.SelectedValue);
            }

            TList<Highlight> highlightList = hs.Find(hq.GetParameters());

            DataSet ds = highlightList.ToDataSet(false);
            DataView dv = new DataView(UserStoreAccess.CheckStoreAccess(ds.Tables[0], true));
            dv.Sort = "DisplayOrder Asc";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        #endregion
    }
}