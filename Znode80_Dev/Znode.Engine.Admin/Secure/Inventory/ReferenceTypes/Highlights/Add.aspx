<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.Highlights.Add" Title="Manage Highlights - Edit" ValidateRequest="false"
    CodeBehind="Add.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox" TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/ImageUploader.ascx" TagName="UploadImage" TagPrefix="ZNode" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblHeading" Text='<%$ Resources:ZnodeAdminResource, TitleCreateHightlight %>' runat="server" /></h1>
        </div>

        <div class="ClearBoth">
        </div>

        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
        <asp:Label ID="lblError" CssClass="Error" runat="server"></asp:Label>
        <h4 class="SubTitle">
            <asp:Localize runat="server" ID="SubTitleGeneralSettings" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralSettings %>'></asp:Localize>
        </h4>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="HighlightName" runat="server"></asp:TextBox><asp:RequiredFieldValidator
                ID="RequiredFieldValidator2" runat="server" ControlToValidate="HighlightName"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredName %>' CssClass="Error" Display="dynamic"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regName" runat="server" ControlToValidate="HighlightName"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidName %>' Display="Dynamic" ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularName %>'
                CssClass="Error"></asp:RegularExpressionValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleHighlightType" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleHighlightType %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="HighlightType" runat="server">
            </asp:DropDownList>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleDisplayOrder" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="DisplayOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator1" runat="server" CssClass="Error" Display="Dynamic"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrder %>' ControlToValidate="DisplayOrder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator2" runat="server" CssClass="Error" ControlToValidate="DisplayOrder"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>' MaximumValue="999999999"
                MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
        </div>
        <div class="ValueStyle">
            <asp:CheckBox ID='VisibleInd' runat='server' Checked="true" Text='<%$ Resources:ZnodeAdminResource, CheckBoxEnableHighlight %>'></asp:CheckBox>
        </div>
        <div class="FieldStyle" runat="server" id="lblLocale" style="display: none;">
            <asp:Localize runat="server" ID="ColumnTitleLocale" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleLocale %>'></asp:Localize>
            <span class="Asterix">*</span>
        </div>
        <div class="ValueStyle" style="display: none;">
            <asp:DropDownList ID="ddlLocales" runat="server" AppendDataBoundItems="true">
                <asp:ListItem Value="" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelect %>'></asp:ListItem>
            </asp:DropDownList>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" CssClass="Error" runat="server" ControlToValidate="ddlLocales"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredLocale %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <asp:Panel ID="HighlightImageHeader" runat="server">
            <h4 class="SubTitle">
                <asp:Localize runat="server" ID="ColumnTitleHighlightImage" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleHighlightImage %>'></asp:Localize>
            </h4>
            <small>
                <asp:Label runat="server" ID="textadd" Text='<%$ Resources:ZnodeAdminResource, SubTextImageFormat %>'></asp:Label></small>
            <small>
                <asp:Label runat="server" ID="textedit" Text='<%$ Resources:ZnodeAdminResource, SubTextImageFormat %>'></asp:Label></small>
            <div id="tblShowImage" runat="server" visible="false" style="margin: 10px 0px 10px 0px;">
                <div class="LeftFloat" style="width: 300px;" id="pnlImage" runat="server">
                    <asp:Image ID="HighlightImage" runat="server" />
                </div>
                <div class="LeftFloat" style="margin-left: -1px;" runat="server" id="pnlUploadImage">
                    <asp:Panel runat="server" ID="pnlShowOption">
                        <div>
                            <asp:Localize runat="server" ID="ColumnSelectanOption" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectanOption %>'></asp:Localize>
                        </div>
                        <div>
                            <asp:RadioButton ID="RadioHighlightCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonCurrentImage %>' runat="server"
                                GroupName="Highlight Image" AutoPostBack="True" OnCheckedChanged="RadioHighlightCurrentImage_CheckedChanged"
                                Checked="True" />
                            <br />
                            <asp:RadioButton ID="RadioHighlightNewImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNewImage %>' runat="server"
                                GroupName="Highlight Image" AutoPostBack="True" OnCheckedChanged="RadioHighlightNewImage_CheckedChanged" />
                            <br />
                            <asp:RadioButton ID="RadioHighlightNoImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNoImage %>' runat="server" GroupName="Highlight Image"
                                AutoPostBack="True" OnCheckedChanged="RadioHighlightNoImage_CheckedChanged" />
                        </div>
                    </asp:Panel>
                    <br />
                    <div id="tblHighlight" runat="server" visible="false">
                        <div class="FieldStyle">
                            <asp:Localize runat="server" ID="ColumnTitleSelectImage" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectImage %>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <ZNode:UploadImage ID="UploadHighlightImage" runat="server"></ZNode:UploadImage>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize runat="server" ID="ColumnTitleImageAltText" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleImageAltText %>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtImageAltTag" runat="server"></asp:TextBox>
                    </div>
                    <div>
                        <asp:Label ID="llblErrorMsg" runat="server" CssClass="Error" ForeColor="Red" Text=""
                            Visible="False"></asp:Label>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="SubTitle">
            <asp:Localize runat="server" ID="ColumnTitleHightlightSettings" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleHightlightSettings %>'></asp:Localize>
        </h4>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleoOnClick" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleoOnClick %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:RadioButton AutoPostBack="true" Checked="true" OnCheckedChanged="DisplayPopup_CheckedChanged" GroupName="Display"
                ID="rdbEnableHyperlink" Text='<%$ Resources:ZnodeAdminResource, RadioButtonHightlightText %>'
                runat="server" /><br />
            <asp:RadioButton ID="rdbHyperlinkExternal" Text='<%$ Resources:ZnodeAdminResource, RadioButtonHightlightURL %>' GroupName="Display"
                AutoPostBack="true" runat="server" OnCheckedChanged="HyperlinkChk_CheckedChanged" />
        </div>
        <asp:Panel ID="pnlHyperLinkInternal" runat="server" Visible="false">
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTitleDisplayText" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayText %>'></asp:Localize>
                <br />
                <small>
                    <asp:Localize runat="server" ID="HintSubTextHighlight" Text='<%$ Resources:ZnodeAdminResource, HintSubTextHighlight %>'></asp:Localize>
                </small>
            </div>
            <div class="ValueStyle">
                <ZNode:HtmlTextBox ID="Description" runat="server"></ZNode:HtmlTextBox>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlHyperLinkExternal" runat="server" Visible="false">
            <div class="FieldStyle">
                <asp:Localize runat="server" ID="ColumnTextHyperlink" Text='<%$ Resources:ZnodeAdminResource, ColumnTextHyperlink %>'></asp:Localize>
                <br />
                <small>
                    <asp:Localize runat="server" ID="HintSubTextWebAddress" Text='<%$ Resources:ZnodeAdminResource, HintSubTextWebAddress %>'></asp:Localize>
                </small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtHyperlink" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtHyperlink" CssClass="Error"
                    ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularHyperlink %>' runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularInvalidURL %>'></asp:RegularExpressionValidator>
            </div>
        </asp:Panel>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <zn:Button runat="server" ID="btnSubmitBottom" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true" />
            <zn:Button runat="server" ID="btnCancelBottom" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
        </div>
    </div>
</asp:Content>
