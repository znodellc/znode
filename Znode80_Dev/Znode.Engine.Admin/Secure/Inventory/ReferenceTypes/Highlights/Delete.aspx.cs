using System;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.Highlights
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Setup.ReferenceTypes.Highlights - Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId;
        private string _HighlightName = string.Empty;
        private string CancelLink = "~/Secure/Inventory/ReferenceTypes/Highlights/Default.aspx";
        #endregion

        /// <summary>
        /// Gets or sets the highlight name
        /// </summary>
        public string HighlightName
        {
            get { return _HighlightName; }
            set { _HighlightName = value; }
        }

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring   
            if (Request.Params["Itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["Itemid"].ToString());
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                if (this.ItemId > 0)
                {
                    HighlightAdmin AdminAccess = new HighlightAdmin();
                    Highlight entity = AdminAccess.GetByHighlightID(this.ItemId);

                    this.HighlightName = entity.Name;
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Delete Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            HighlightAdmin AdminAccess = new HighlightAdmin();
            Highlight entity = AdminAccess.GetByHighlightID(this.ItemId);
            this.HighlightName = entity.Name;

            bool check = AdminAccess.Delete(this.ItemId);
            if (check)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteHighlight") + this.HighlightName, this.HighlightName);
                Response.Redirect(this.CancelLink);
            }
            else
            {
                lblErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteHighlight").ToString();
            }
        }

        /// <summary>
        /// Cancel Button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelLink);
        }
        #endregion
    }
}