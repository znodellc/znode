<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.Highlights.Delete" Title="Mange Highlights - Delete" CodeBehind="Delete.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <h5>
        <asp:Localize runat="server" ID="TextPleaseConfirm" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm %>'></asp:Localize>
    </h5>

    <p>
        <asp:Localize runat="server" ID="TextDeleteHighlight" Text='<%$ Resources:ZnodeAdminResource, TextDeleteHighlight %>'></asp:Localize>
        <b><%=HighlightName%></b><asp:Localize runat="server" ID="TextChangeUndone" Text='<%$ Resources:ZnodeAdminResource, TextChangeUndone %>'></asp:Localize>
    </p>
    <asp:Label CssClass="Error" ID="lblErrorMessage" runat="server" Text=''></asp:Label>
    <div>
        <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>
    <div>
        <zn:Button runat="server" ID="btnDelete" ButtonType="CancelButton" OnClick="BtnDelete_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>' CausesValidation="True" />
        <zn:Button runat="server" ID="btnCancel" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
    </div>
    <div>
        <ZNode:Spacer ID="Spacer1" SpacerHeight="25" SpacerWidth="10" runat="server"></ZNode:Spacer>
    </div>
</asp:Content>
