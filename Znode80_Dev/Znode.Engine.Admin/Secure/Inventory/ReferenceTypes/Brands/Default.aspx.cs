using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.Brands
{
    /// <summary>
    /// Repesents the Znode.Engine.Admin.Secure.Setup.ReferenceTypes.Brands - Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Member Variables
        private static bool IsSearchEnabled = false;
        private string CatalogImagePath = string.Empty;
        private string AddLink = "~/Secure/Inventory/ReferenceTypes/Brands/Add.aspx";
        private int ItemId = 0;
        #endregion

        #region Protected properties
        /// <summary>
        /// Gets or sets the Grid View Sort Direction
        /// </summary>
        private string GridViewSortDirection
        {
            get
            {
                return ViewState["SortDirection"] as string ?? "ASC";
            }

            set
            {
                ViewState["SortDirection"] = value;
            }
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["ItemID"] != null)
            {
                this.ItemId = int.Parse(Request.Params["ItemID"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindGridData();
            }

            lblError.Visible = false;
        }
        #endregion

        #region General Events

        /// <summary>
        /// Add Manufacturer Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddManufacturer_Click(object sender, EventArgs e)
        {
            // Redirect to Add Manufacturer Page
            Response.Redirect(this.AddLink);
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
            IsSearchEnabled = true;
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
        #endregion

        #region Grid Events

        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            if (IsSearchEnabled)
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindSearchData();
            }
            else
            {
                uxGrid.PageIndex = e.NewPageIndex;
                this.BindGridData();
            }
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Edit")
                {
                    this.AddLink = this.AddLink + "?itemid=" + Id;
                    Response.Redirect(this.AddLink);
                }
                else if (e.CommandName == "Delete")
                {
                    ZNode.Libraries.Admin.ManufacturerAdmin adminaccess = new ZNode.Libraries.Admin.ManufacturerAdmin();
                    bool Check = false;
                    Manufacturer _manufacturer = new Manufacturer();
                    _manufacturer = adminaccess.GetByManufactureId(int.Parse(Id));

                    string BrandName = _manufacturer.Name;
                    Check = adminaccess.Delete(int.Parse(Id));

                    if (Check)
                    {
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogDeleteBrand").ToString() + BrandName, BrandName);
                        this.BindGridData();
                    }
                    else
                    {
                        lblError.Visible = true;
                        lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorDeleteBrand").ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Grid Sorting Method
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (IsSearchEnabled)
            {
                ManufacturerAdmin manufaturer = new ManufacturerAdmin();
                DataSet ds = manufaturer.GetManufacturerBySearchData(txtManufacturerName.Text.Trim());
                uxGrid.DataSource = this.SortDataTable(ds, e.SortExpression, true);
                uxGrid.DataBind();
            }
            else
            {
                ManufacturerAdmin ManuAdmin = new ManufacturerAdmin();
                DataSet ds = ManuAdmin.GetAll().ToDataSet(true);
                uxGrid.DataSource = this.SortDataTable(ds, e.SortExpression, true);
                uxGrid.DataBind();
            }

            if (this.GetSortDirection() == "DESC")
            {
                e.SortDirection = SortDirection.Descending;
            }
            else
            {
                e.SortDirection = SortDirection.Ascending;
            }
        }

        /// <summary>
        /// Grid Row Deleting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGridData();
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Sorting Data
        /// </summary>
        /// <param name="dataSet">The Dataset to be sorted</param>
        /// <param name="GridViewSortExpression">The value of GridView Sort Expression</param>
        /// <param name="isPageIndexChanging">Boolean value Is Page Index Changing or not</param>
        /// <returns>Returns the sorted DataView</returns>
        protected DataView SortDataTable(DataSet dataSet, string GridViewSortExpression, bool isPageIndexChanging)
        {
            if (dataSet != null)
            {
                DataView dataView = new DataView(dataSet.Tables[0]);

                if (GridViewSortExpression.Length > 0)
                {
                    if (isPageIndexChanging)
                    {
                        dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, this.GridViewSortDirection);
                    }
                    else
                    {
                        dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, this.GetSortDirection());
                    }
                }

                return dataView;
            }
            else
            {
                return new DataView();
            }
        }

        /// <summary>
        /// Get Sort Direction
        /// </summary>
        /// <returns>Returns the Sort Direction</returns>
        private string GetSortDirection()
        {
            switch (this.GridViewSortDirection)
            {
                case "ASC":
                    this.GridViewSortDirection = "DESC";
                    break;

                case "DESC":
                    this.GridViewSortDirection = "ASC";
                    break;
            }

            return this.GridViewSortDirection;
        }

        #region Bind Methods
        /// <summary>
        /// Bind data to grid
        /// </summary>
        private void BindGridData()
        {
            ZNode.Libraries.Admin.ManufacturerAdmin ManuAdmin = new ZNode.Libraries.Admin.ManufacturerAdmin();
            DataSet ds = ManuAdmin.GetAll().ToDataSet(true);
            DataView dv = new DataView(UserStoreAccess.CheckStoreAccess(ds.Tables[0], true));
            dv.Sort = "Name";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Bind Search Data Method
        /// </summary>
        private void BindSearchData()
        {
            ManufacturerAdmin manufaturer = new ManufacturerAdmin();
            DataSet ds = manufaturer.GetManufacturerBySearchData(Server.HtmlEncode(txtManufacturerName.Text.Trim()));
            DataView dv = new DataView(UserStoreAccess.CheckStoreAccess(ds.Tables[0], true));
            dv.Sort = "Name";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }

        #endregion

        #endregion
    }
}