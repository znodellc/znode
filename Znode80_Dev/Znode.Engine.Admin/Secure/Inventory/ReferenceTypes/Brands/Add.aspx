<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.Brands.Add" CodeBehind="Add.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ID="btnSubmitTop" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>'/>
            <zn:Button runat="server" ID="btnCancelTop" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="BrandName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBrandName %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID='Name' runat='server' MaxLength="50" Columns="50"></asp:TextBox><asp:RequiredFieldValidator
                ID="RequiredFieldValidator1" runat="server" ControlToValidate="Name" Display="Dynamic"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredBrandName %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="DescriptionColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAddOnDescription %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="Description" runat="server" Rows="10" MaxLength="4000" TextMode="MultiLine"
                Columns="50"></asp:TextBox>
        </div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        <div class="FieldStyle">
            <asp:Localize ID="EmailAddress" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEmailAddress %>'></asp:Localize>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID='EmailId' runat='server' MaxLength="50" Columns="50"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="EmailId"
                CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidEmailID %>' SetFocusOnError="True"
                ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
        </div>
        <asp:Panel ID="TempPanel" runat="server" Style="display: none">
            <div class="FieldStyle">
               <asp:Localize ID="DropShipper" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDropShipper %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="DropShipInd" runat="server" Text="" />
            </div>
            <uc1:Spacer ID="Spacer3" SpacerHeight="5" SpacerWidth="10" runat="server"></uc1:Spacer>
            <div class="FieldStyle">
                <asp:Localize ID="EmailTemplate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEmailNotificationTemplate %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="NotificationTemplate" runat="server" Rows="15" Columns="50" TextMode="MultiLine"></asp:TextBox>
            </div>
            <div class="FieldStyle">
               <asp:Localize ID="Custom1Column" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCustom1 %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="Custom1" runat="server" Rows="10" MaxLength="4000" Columns="50"
                    TextMode="MultiLine"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="Custom2Column" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCustom2 %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="Custom2" runat="server" Rows="10" MaxLength="4000" Columns="50"
                    TextMode="MultiLine"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="Custom3Column" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnCustom3 %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="Custom3" runat="server" Rows="10" MaxLength="4000" Columns="50"
                    TextMode="MultiLine"></asp:TextBox>
            </div>
        </asp:Panel>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="10" runat="server"></uc1:Spacer>
            <div class="FieldStyle">
                <asp:Localize ID="IsActive" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnIsActive %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="CheckActiveInd" runat="server" Text="" CssClass="FieldStyle" Checked="true" />
            </div>
            <asp:Label ID="lblError" runat="server"></asp:Label>
        </div>
        <p>
        </p>
        <div class="ClearBoth">
            <br />
        </div>
        <div>
            <zn:Button runat="server" ID="btnSubmitBottom" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="True"/>
            <zn:Button runat="server" ID="btnCancelBottom" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
        </div>
    </div>
</asp:Content>
