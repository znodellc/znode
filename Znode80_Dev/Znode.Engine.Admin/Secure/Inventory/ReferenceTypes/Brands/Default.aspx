<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.Brands.Default" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1><asp:Localize ID="Brands" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextBrands %>'></asp:Localize></h1>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAddManufacturer" runat="server"
                ButtonType="Button" OnClick="BtnAddManufacturer_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonAddBrand %>'
                ButtonPriority="Primary" />
        </div>
        <div class="ClearBoth">
            <p>
                <asp:Localize ID="BrandsText" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextBrands %>'></asp:Localize>
            </p>
            <br />
        </div>

        <h4 class="SubTitle"><asp:Localize ID="SearchBrands" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchBrands %>'></asp:Localize></h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="Name" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnName %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtManufacturerName" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <zn:Button runat="server" ID="btnClear" CausesValidation="False" OnClick="BtnClear_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' />
                    <zn:Button runat="server" ID="btnSearch" OnClick="BtnSearch_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle"><asp:Localize ID="GridTitleBrands" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleBrandsList %>'></asp:Localize></h4>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" PageSize="25"
            OnRowDeleting="UxGrid_RowDeleting" OnSorting="UxGrid_Sorting" AllowSorting="True"
            EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundBrands %>'>
            <Columns>
                <asp:BoundField DataField="manufacturerid" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="50%">
                    <ItemTemplate>
                        <a href='add.aspx?itemid=<%# DataBinder.Eval(Container.DataItem, "Manufacturerid").ToString()%>'>
                            <%# DataBinder.Eval(Container.DataItem, "Name").ToString()%></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsActive %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img id="Img1" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnEdit" CssClass="actionlink" runat="server" CommandName="Edit" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>'
                            CommandArgument='<%# Eval("manufacturerid") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnDelete" CssClass="actionlink" runat="server" CommandName="Delete"
                            Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CommandArgument='<%# Eval("manufacturerid") %>' OnClientClick="return DeleteConfirmationBrand();"/>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
        <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
    </div>
    
    <script language="javascript" type="text/javascript">
        function DeleteConfirmationBrand()
        {
            return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ConfirmDeleteBrand")) %>');
    }
    </script>
</asp:Content>
