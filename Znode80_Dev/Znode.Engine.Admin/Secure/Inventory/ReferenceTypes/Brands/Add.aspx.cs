using System;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.Brands
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Setup.ReferenceTypes.Brands -  Add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId = 0;
        #endregion

        #region Bind Data

        /// <summary>
        /// Bind Edit Datas
        /// </summary>
        public void BindEditDatas()
        {
            ManufacturerAdmin ManuAdmin = new ManufacturerAdmin();
            Manufacturer _manufacturer = ManuAdmin.GetByManufactureId(this.ItemId);
            if (_manufacturer != null)
            {
                Name.Text = Server.HtmlDecode(_manufacturer.Name);
                Description.Text = Server.HtmlDecode(_manufacturer.Description);
                CheckActiveInd.Checked = _manufacturer.ActiveInd;
                if (_manufacturer.IsDropShipper.HasValue)
                {
                    DropShipInd.Checked = _manufacturer.IsDropShipper.Value;
                }

                EmailId.Text = _manufacturer.EmailID;
                NotificationTemplate.Text = _manufacturer.EmailNotificationTemplate;
                Custom1.Text = Server.HtmlDecode(_manufacturer.Custom1);
                Custom2.Text = Server.HtmlDecode(_manufacturer.Custom2);
                Custom3.Text = Server.HtmlDecode(_manufacturer.Custom3);
            }
        }
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                if (this.ItemId > 0)
                {
                    this.BindEditDatas();
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditBrand").ToString() + Server.HtmlEncode(Name.Text.Trim());
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddBrand").ToString();
                }
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ManufacturerAdmin ManuAdmin = new ManufacturerAdmin();
            Manufacturer _manufacturer = new Manufacturer();
            if (this.ItemId > 0)
            {
                _manufacturer = ManuAdmin.GetByManufactureId(this.ItemId);
            }

            _manufacturer.Name = Server.HtmlEncode(Name.Text);
            _manufacturer.Description = Server.HtmlEncode(Description.Text);
            _manufacturer.ActiveInd = CheckActiveInd.Checked;
            _manufacturer.IsDropShipper = DropShipInd.Checked;
            _manufacturer.EmailID = EmailId.Text.Trim();
            _manufacturer.EmailNotificationTemplate = NotificationTemplate.Text.Trim();
            _manufacturer.Custom1 = Server.HtmlEncode(Custom1.Text.Trim());
            _manufacturer.Custom2 = Server.HtmlEncode(Custom2.Text.Trim());
            _manufacturer.Custom3 = Server.HtmlEncode(Custom3.Text.Trim());

            bool check = false;

            if (this.ItemId > 0)
            {
                check = ManuAdmin.Update(_manufacturer);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogEditBrand").ToString() + Name.Text, Name.Text);
            }
            else
            {
                check = ManuAdmin.Insert(_manufacturer);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddBrand").ToString() + Name.Text, Name.Text);
            }

            if (check)
            {
                // Redirect to main page
                Response.Redirect("Default.aspx");
            }
            else
            {
                // Display error message
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoteAdd").ToString();
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("Default.aspx");
        }
        #endregion
    }
}