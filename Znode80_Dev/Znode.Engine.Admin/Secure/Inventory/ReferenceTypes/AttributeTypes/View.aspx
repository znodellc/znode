<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false"
    Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.AttributeTypes.View" CodeBehind="View.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 50%; text-align: left">
            <h1><asp:Localize ID="AttributeValues" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleAttributeValues %>'></asp:Localize>
                <asp:Label ID="lblAttributeType" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right">
            <zn:Button ID="AttributeTypeList" Width="150px" runat="server" Text='<%$ Resources:ZnodeAdminResource, ButtonBackToAttributes %>' OnClick="AttributeTypeList_Click" ButtonType="EditButton" />
            <zn:Button ID="AddAttribute" Width="100px" runat="server" Text='<%$ Resources:ZnodeAdminResource, ButtonAddValue %>' OnClick="AddAttribute_Click" ButtonType="EditButton" />
        </div>
        <div align="left">
            <span style="float: left">
                <asp:Label ID="FailureText" runat="Server" EnableViewState="false" CssClass="Error" />
            </span>

        </div>
        <h4 class="GridTitle"><asp:Localize ID="GridTitleAttributeValue" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleAttributeValues %>'></asp:Localize>
        </h4>
        <uc1:Spacer ID="Spacer" runat="server" SpacerHeight="10" />
        <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundAttributes %>'
            ForeColor="#333333" GridLines="None" Width="100%" OnPageIndexChanging="UxGrid_PageIndexChanging"
            OnRowCommand="UxGrid_RowCommand" OnRowDeleting="UxGrid_RowDeleting">
            <Columns>
                <asp:BoundField DataField="Name" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleAttributeValues %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="EditAttribute" CommandName="Edit" CommandArgument='<%# Eval("AttributeId") %>'
                            runat="server" CssClass="actionlink" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="DeleteAttribute" CommandName="Delete" CommandArgument='<%# Eval("AttributeId") %>'
                            runat="server" CssClass="actionlink" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' OnClientClick="return DeleteConfirmationAttributeValue();" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="RowStyle" />
            <EditRowStyle CssClass="EditRowStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        </asp:GridView>
        <uc1:Spacer ID="Spacer5" SpacerHeight="1" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    
    <script language="javascript" type="text/javascript">
    function DeleteConfirmationAttributeValue()
    {
        return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ConfirmDeleteAttributeValue")) %>');
    }
    </script>

</asp:Content>


