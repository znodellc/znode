<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.AttributeTypes.Default" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1><asp:Localize ID="AttributeTypes" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleAttributeTypes %>'></asp:Localize>
            </h1>
        </div>

        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAddAttributeTypes" runat="server"
                ButtonType="Button" OnClick="BtnAdd_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonAttributeType %>'
                ButtonPriority="Primary" />

        </div>
        <p class="ClearBoth">
            <asp:Localize ID="TextAttributeTypes" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextAttributeTypes %>'></asp:Localize>
        </p>
        <div class="ClearBoth" align="left">
            <br />
        </div>

        <h4 class="SubTitle"><asp:Localize ID="SearchAttributes" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchAttributes %>'></asp:Localize></h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="Name" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnName %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtAttributeName" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <zn:Button runat="server" ID="btnClear" CausesValidation="False" OnClick="BtnClear_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>'/>
                    <zn:Button runat="server" ID="btnSearch" OnClick="BtnSearch_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>'/>
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle"><asp:Localize ID="GridTitleAttribute" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleAttributes %>'></asp:Localize></h4>
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnSorting="UxGrid_Sorting" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" Width="100%" EnableSortingAndPagingCallbacks="False"
            PageSize="25" AllowSorting="false" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundAttributes %>'>
            <Columns>
                <asp:BoundField DataField="AttributeTypeId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField SortExpression="Name" HtmlEncode="false" DataField="Name" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="40%" />
                <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnView" Width="90px" CssClass="actionlink" runat="server" CommandName="View" Text='<%$ Resources:ZnodeAdminResource, LinkSeeValues %>'
                            ButtonType="Button" CommandArgument='<%# Eval("AttributeTypeId") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnEdit" Width="60px" CssClass="actionlink" runat="server" CommandName="Edit" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>'
                            ButtonType="Button" CommandArgument='<%# Eval("AttributeTypeId") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btndelete" Width="60px" CssClass="actionlink" runat="server" CommandName="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' 
                            ButtonType="Button" CommandArgument='<%# Eval("AttributeTypeId") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>
