using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace  Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.AttributeTypes
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Setup.ReferenceTypes.AttributeTypes.Add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        private string CancelLink = "~/Secure/Inventory/ReferenceTypes/AttributeTypes/Default.aspx";
        private AttributeTypeAdmin _AttributeTypeAccess = new AttributeTypeAdmin();
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                // Bind Locale dropdown.
                this.BindLocaleDropdown();

                // If edit func then bind the data fields
                if (this.ItemId > 0)
                {
                    this.BindEditData();
                    AttributeType _AttributeTypeList = _AttributeTypeAccess.GetByAttributeTypeId(this.ItemId);
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditAttribute").ToString();
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddAttribute").ToString();
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            AttributeTypeAdmin _AttributeTypeAccess = new AttributeTypeAdmin();
            AttributeType _NewAttributetype = new AttributeType();

            // If edit mode then retrieve data first
            if (this.ItemId > 0)
            {
                _NewAttributetype = _AttributeTypeAccess.GetByAttributeTypeId(this.ItemId);
            }

            // Set values
            _NewAttributetype.Name = Server.HtmlEncode(Name.Text.Trim());
            _NewAttributetype.DisplayOrder = int.Parse(DisplayOrder.Text.Trim());
            _NewAttributetype.IsPrivate = false;

            // For Default to English Locale
            if (!string.IsNullOrEmpty(ddlLocales.SelectedValue))
            {
                _NewAttributetype.LocaleId = int.Parse(ddlLocales.SelectedValue); 
            }
            else
            {
                _NewAttributetype.LocaleId = 43;
            }
            // Update or Add
            bool Checkbool = false;

            if (this.ItemId > 0)
            {
                Checkbool = _AttributeTypeAccess.Update(_NewAttributetype);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditAttribute").ToString() + Name.Text, Name.Text);
            }
            else
            {
                Checkbool = _AttributeTypeAccess.Add(_NewAttributetype);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddAttribute").ToString() + Name.Text, Name.Text);
            }

            if (Checkbool)
            {
                // Redirect to main page
                Response.Redirect(this.CancelLink);
            }
            else
            {
                // Display error message
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorNoteAdd").ToString();
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            // Redirect to main page
            Response.Redirect(this.CancelLink);
        }
        #endregion

        #region Bind Data

        /// <summary>
        ///  Bind data to the fields 
        /// </summary>
        private void BindEditData()
        {
            AttributeType _AttributeTypeList = _AttributeTypeAccess.GetByAttributeTypeId(this.ItemId);

            // Get Attribute Type Values
            if (_AttributeTypeList != null)
            {
                Name.Text = Server.HtmlDecode(_AttributeTypeList.Name);
                DisplayOrder.Text = _AttributeTypeList.DisplayOrder.ToString();

                ListItem li = ddlLocales.Items.FindByValue(_AttributeTypeList.LocaleId.ToString());
                if (li != null)
                {
                    li.Selected = true;
                }
            }
        }

        /// <summary>
        /// Bind the locale dropdown
        /// </summary>
        private void BindLocaleDropdown()
        {
            LocaleService localeService = new LocaleService();
            TList<Locale> localeList = localeService.GetAll();
            localeList.Sort("LocaleDescription");

            // Bind the dropdown.
            ddlLocales.DataSource = localeList;
            ddlLocales.DataTextField = "LocaleDescription";
            ddlLocales.DataValueField = "LocaleId";
            ddlLocales.DataBind();

            if (ddlLocales.Items.Count == 0)
            {
                lblLocale.Visible = false;
                ddlLocales.Visible = false;
            }
        }

        #endregion
    }
}