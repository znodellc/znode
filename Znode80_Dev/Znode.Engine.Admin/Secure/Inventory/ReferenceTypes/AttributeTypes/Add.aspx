<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.AttributeTypes.Add" CodeBehind="Add.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label></h1>
        </div>
        <div class="ClearBoth">
        </div>
        <asp:Label ID="lblError" runat="server" />
        <div class="FieldStyle">
            <asp:Localize ID="AttributeTypeName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnAttributeTypeName %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="Name" runat="server" MaxLength="50" Columns="30"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" CssClass="Error" runat="server" ControlToValidate="Name"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredAttributeTypeName %>' SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize ID="DisplayOrderColumn" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder %>'></asp:Localize><span class="Asterix">*</span><br />
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="DisplayOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic"
                ErrorMessage='<%$ Resources:ZnodeAdminResource,  RequiredDisplayOrder %>' CssClass="Error" ControlToValidate="DisplayOrder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" CssClass="Error" ControlToValidate="DisplayOrder"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeWholeNumber %>' MaximumValue="999999999"
                MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle" runat="server" id="lblLocale" style="display: none">
            <asp:Localize ID="Locale" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLocale %>'></asp:Localize> <span class="Asterix">*</span>
        </div>
        <div class="ValueStyle" style="display: none">
            <asp:DropDownList ID="ddlLocales" runat="server" AppendDataBoundItems="true">
                <asp:ListItem Value="" Text='<%$ Resources:ZnodeAdminResource, DropDownTextSelect %>'></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
             <zn:Button runat="server" ID="btnSubmitBottom" OnClick="BtnSubmit_Click" ButtonType="SubmitButton" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' CausesValidation="true"/>
             <zn:Button runat="server" ID="btnCancelBottom" OnClick="BtnCancel_Click" ButtonType="CancelButton" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False"/>
        </div>
    </div>
</asp:Content>
