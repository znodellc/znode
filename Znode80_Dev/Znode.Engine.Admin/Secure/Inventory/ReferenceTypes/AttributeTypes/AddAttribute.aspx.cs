using System;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.AttributeTypes
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Setup.ReferenceTypes.AttributeTypes.AddAttribute class
    /// </summary>
    public partial class AddAttribute : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId = 0;
        private int AttributeId = 0;
        private string ViewLink = "~/Secure/Inventory/ReferenceTypes/AttributeTypes/View.aspx?itemid=";
        private AttributeTypeAdmin _AttributeTypeAccess = new AttributeTypeAdmin();
        private AttributeType _AttributeTypeList = new AttributeType();
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
                _AttributeTypeList = _AttributeTypeAccess.GetByAttributeTypeId(this.ItemId);
            }

            // Get AttributeId from QueryString
            if (Request.Params["AttributeID"] != null)
            {
                this.AttributeId = int.Parse(Request.Params["AttributeID"]);
            }

            if (!Page.IsPostBack)
            {
                // Check for Edit Mode
                if (this.AttributeId > 0 && this.ItemId > 0)
                {
                    // Bind Data into fields
                    this.BindData();
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditAttributeValue").ToString() +_AttributeTypeList.Name;
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddAttributeValue").ToString() + _AttributeTypeList.Name;
                }
            }
        }
        #endregion

        #region Events
        /// <summary>
        /// Submit button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            // Declarations
            AttributeTypeAdmin _AdminAccess = new AttributeTypeAdmin();
            ProductAttribute _ProductAttribute = new ProductAttribute();

            // Check for Edit Mode
            if (this.AttributeId > 0)
            {
                _ProductAttribute = _AdminAccess.GetByAttributeID(this.AttributeId);
            }

            // Set Values
            _ProductAttribute.Name = Server.HtmlEncode(Name.Text.Trim());
            _ProductAttribute.DisplayOrder = int.Parse(DisplayOrder.Text.Trim());
            _ProductAttribute.AttributeTypeId = this.ItemId;
            _ProductAttribute.ExternalId = null;
            _ProductAttribute.OldAttributeId = null;
            _ProductAttribute.IsActive = true;

            bool status = false;

            if (this.AttributeId > 0)
            {
                // Update Product Attribute
                status = _AdminAccess.UpdateProductAttribute(_ProductAttribute);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditAttributeValue").ToString() + Name.Text, Name.Text);
            }
            else
            {
                status = _AdminAccess.AddProductAttribute(_ProductAttribute);
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddAttributeValue").ToString() + Name.Text, Name.Text);
            }

            if (status)
            {
                // Redirect to main page
                Response.Redirect(this.ViewLink + this.ItemId);
            }
            else
            {
                // Display error message
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoteAdd").ToString();
            }
        }

        /// <summary>
        /// Cancel button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            // Redirect to main page
            Response.Redirect(this.ViewLink + this.ItemId);
        }
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Edit Attribute Datas
        /// </summary>
        private void BindData()
        {
            // Declarations
            AttributeTypeAdmin _AdminAccess = new AttributeTypeAdmin();
            ProductAttribute _ProductAttribute = _AdminAccess.GetByAttributeID(this.AttributeId);

            // Check Product Attribute for null
            if (_ProductAttribute != null)
            {
                Name.Text = Server.HtmlDecode(_ProductAttribute.Name);
                DisplayOrder.Text = _ProductAttribute.DisplayOrder.ToString();
            }
        }
        #endregion
    }
}