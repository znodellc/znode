using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;

namespace  Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.AttributeTypes
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Setup.ReferenceTypes.AttributeTypes.Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string AddLink = "~/Secure/Inventory/ReferenceTypes/AttributeTypes/add.aspx";
        private string DeleteLink = "~/Secure/Inventory/ReferenceTypes/AttributeTypes/delete.aspx?itemid=";
        private string ViewLink = "~/Secure/Inventory/ReferenceTypes/AttributeTypes/view.aspx?itemid=";
        private string EditLink = "~/Secure/Inventory/ReferenceTypes/AttributeTypes/add.aspx?itemid=";

        #endregion

        #region Protected properties
        /// <summary>
        /// Gets or sets the Grid View Sort Direction
        /// </summary>
        private string GridViewSortDirection
        {
            get
            {
                return ViewState["SortDirection"] as string ?? "ASC";
            }

            set
            {
                ViewState["SortDirection"] = value;
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindSearchData();
            }
        }

        /// <summary>
        /// Add Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAdd_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddLink);
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
        }
        #endregion

        #region Grid Events
        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandArgument.ToString() == "page")
            {
            }
            else
            {
                string Id = e.CommandArgument.ToString();

                if (e.CommandName == "Edit")
                {
                    Response.Redirect(this.EditLink + Id);
                }
                else if (e.CommandName == "View")
                {
                    Response.Redirect(this.ViewLink + Id);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.DeleteLink + Id);
                }
            }
        }

        /// <summary>
        /// Grid Sorting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            AttributeTypeAdmin AttributeAccess = new AttributeTypeAdmin();
            DataSet ds = AttributeAccess.GetAttributeBySearchData(Server.HtmlEncode(txtAttributeName.Text.Trim()));
            uxGrid.DataSource = this.SortDataTable(ds, e.SortExpression, true);
            uxGrid.DataBind();

            if (this.GetSortDirection() == "DESC")
            {
                e.SortDirection = SortDirection.Descending;
            }
            else
            {
                e.SortDirection = SortDirection.Ascending;
            }
        }

        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        /// <summary>
        /// Sorting Data
        /// </summary>
        /// <param name="dataSet">The Dataset to be sorted</param>
        /// <param name="gridViewSortExpression">The value of GridView Sort Expression</param>
        /// <param name="isPageIndexChanging">Boolean value for Page Index Changing </param>
        /// <returns>Returns a Data View</returns>
        protected DataView SortDataTable(DataSet dataSet, string gridViewSortExpression, bool isPageIndexChanging)
        {
            if (dataSet != null)
            {
                DataView dataView = new DataView(dataSet.Tables[0]);

                if (gridViewSortExpression.Length > 0)
                {
                    if (isPageIndexChanging)
                    {
                        dataView.Sort = string.Format("{0} {1}", gridViewSortExpression, this.GridViewSortDirection);
                    }
                    else
                    {
                        dataView.Sort = string.Format("{0} {1}", gridViewSortExpression, this.GetSortDirection());
                    }
                }

                return dataView;
            }
            else
            {
                return new DataView();
            }
        }

        #endregion

        #region Bind Methods
       
        /// <summary>
        /// Bind Search Data
        /// </summary>
        private void BindSearchData()
        {
            AttributeTypeService AttributeAccess = new AttributeTypeService();
            AttributeTypeQuery query = new AttributeTypeQuery();
            if (txtAttributeName.Text.Trim().Length > 0)
            {
                query.AppendLike(AttributeTypeColumn.Name, "%" + Server.HtmlEncode(txtAttributeName.Text) + "%");
            }

            DataSet ds = AttributeAccess.Find(query.GetParameters()).ToDataSet(true);
            DataView dv = new DataView(UserStoreAccess.CheckStoreAccess(ds.Tables[0], true));
            dv.Sort = "Name";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Get Sort Direction Method
        /// </summary>
        /// <returns>Returns the sort direction</returns>
        private string GetSortDirection()
        {
            switch (this.GridViewSortDirection)
            {
                case "ASC":
                    this.GridViewSortDirection = "DESC";
                    break;

                case "DESC":
                    this.GridViewSortDirection = "ASC";
                    break;
            }

            return this.GridViewSortDirection;
        }
        #endregion
    }
}