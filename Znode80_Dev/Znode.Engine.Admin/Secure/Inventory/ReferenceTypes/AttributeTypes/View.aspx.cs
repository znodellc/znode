using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.AttributeTypes
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Setup.ReferenceTypes.AttributeTypes.View class
    /// </summary>
    public partial class View : System.Web.UI.Page
    {
        #region Private Member Variables
        private int ItemId = 0;
        private string AddAttributeLink = "~/Secure/Inventory/ReferenceTypes/AttributeTypes/AddAttribute.aspx?itemid=";
        private string ListLink = "~/Secure/Inventory/ReferenceTypes/AttributeTypes/Default.aspx";
        private string EditLink = "~/Secure/Inventory/ReferenceTypes/AttributeTypes/Add.aspx?itemid=";
        private string AssociateName = string.Empty;
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }

            if (!Page.IsPostBack)
            {
                this.BindGrid();
                this.BindData();
            }
        }

        #endregion
       
        #region General Events

        /// <summary>
        /// List Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AttributeTypeList_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ListLink);
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void AddAttribute_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddAttributeLink + this.ItemId);
        }

        /// <summary>
        /// Edit Attribute Type Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void EditAttributeType_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.EditLink + this.ItemId);
        }

        #endregion

        #region Grid Events

        /// <summary>
        /// Grid Page Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
        }

        /// <summary>
        /// Grid Row Command Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                // Get the Value from the command argument
                string Id = e.CommandArgument.ToString();
                if (e.CommandName == "Edit")
                {
                    // Redirect to Attribute Edit page
                    Response.Redirect(this.AddAttributeLink + this.ItemId + "&AttributeID=" + Id);
                }

                if (e.CommandName == "Delete")
                {
                    AttributeTypeAdmin atrributeTypeAdmin = new AttributeTypeAdmin();
                    ProductAttribute attribute = atrributeTypeAdmin.GetByAttributeID(int.Parse(Id));

                    AttributeType attributeType = new AttributeType();
                    attributeType = atrributeTypeAdmin.GetByAttributeTypeId(this.ItemId);

                    string attributeValueName = attribute.Name;
                    string attributeTypeName = attributeType.Name;

                    this.AssociateName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource","ActivityLogDeleteAttributeValue").ToString(), attributeValueName, attributeTypeName);

                    if (atrributeTypeAdmin.DeleteProductAttribute(attribute))
                    {
                        // Nothing todo here
                        ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, attributeTypeName);
                    }
                    else
                    {
                        FailureText.Text = this.GetGlobalResourceObject("ZnodeAdminResource","ErrorAttributeValue").ToString();
                    }
                }
            }
        }

        /// <summary>
        /// Grid Row Deleting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindGrid();
        }

        #endregion

        #region Private Methods

        private void BindGrid()
        {
            AttributeTypeAdmin adminAccess = new AttributeTypeAdmin();
            TList<ProductAttribute> prodAttributeList = new TList<ProductAttribute>();
            prodAttributeList = adminAccess.GetByAttributeTypeID(this.ItemId);

            foreach (ProductAttribute prodattribute in prodAttributeList)
            {
                prodattribute.Name = Server.HtmlDecode(prodattribute.Name);
            }

            uxGrid.DataSource = prodAttributeList;

            uxGrid.DataBind();
        }
        
        /// <summary>
        /// Bind Datas Method
        /// </summary>
        private void BindData()
        {
            AttributeTypeAdmin adminAccess = new AttributeTypeAdmin();
            AttributeType attributeTypeList = adminAccess.GetByAttributeTypeId(this.ItemId);

            if (attributeTypeList != null)
            {
                lblAttributeType.Text = attributeTypeList.Name;
            }
        }
        #endregion
    }
}