using System;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.ProductTypes
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Setup.ReferenceTypes.ProductTypes.Delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Variables
        private int ItemId;
        private string _ProductCategoryName = string.Empty;
        private string ProductTypeName = string.Empty;
        #endregion

        #region Public Properties

        /// <summary>
        /// Gets or sets the Product Category Name
        /// </summary>
        public string ProductCategoryName
        {
            get
            {
                return this._ProductCategoryName;
            }

            set
            {
                this._ProductCategoryName = value;
            }
        }
        
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
                this.BindData();
            }
            else
            {
                this.ItemId = 0;
            }
        }
        #endregion

        #region Bind Data
        /// <summary>
        /// Bind data to the fields on the screen
        /// </summary>
        protected void BindData()
        {
            ProductTypeAdmin ProductTypeAdmin = new ProductTypeAdmin();
            ProductType ProdType = ProductTypeAdmin.GetByProdTypeId(this.ItemId);

            if (ProdType != null)
            {
                this.ProductCategoryName = ProdType.Name;
            }
            else
            {
                throw new ApplicationException(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorRequestProductType").ToString());
            }
        }
        #endregion

        #region Events

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
        }

        /// <summary>
        /// Delete Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            bool Check = false;
            try
            {
                ProductTypeAdmin ProdTypeAdmin = new ProductTypeAdmin();
                ProductType ProdType = new ProductType();
                ProdType.ProductTypeId = this.ItemId;

                ProductType _ProdType = ProdTypeAdmin.GetByProdTypeId(this.ItemId);
                this.ProductTypeName = _ProdType.Name;
                Check = ProdTypeAdmin.Delete(ProdType);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
            }
            
            if (Check)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteProductType") + this.ProductTypeName, this.ProductTypeName);

                Response.Redirect("default.aspx");
            }
            else
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorDeleteProductType").ToString();
            }
        }
        #endregion
    }
}