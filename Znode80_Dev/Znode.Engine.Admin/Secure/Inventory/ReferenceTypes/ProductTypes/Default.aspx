<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master"
    AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.ProductTypes.Default"
    Title="Manage Product Types" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div>
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Localize runat="server" ID="LinkTextProductTypes" Text='<%$ Resources:ZnodeAdminResource, LinkTextProductTypes %>'></asp:Localize>
            </h1>
        </div>
        <div class="ButtonStyle">
            <zn:LinkButton ID="btnAddProductType" runat="server" CausesValidation="False"
                ButtonType="Button" OnClick="BtnAddProductType_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddProductType %>'
                ButtonPriority="Primary" />
        </div>
        <div class="ClearBoth">
            <p>
                <asp:Localize runat="server" ID="TextProductType" Text='<%$ Resources:ZnodeAdminResource, TextAddProductType %>'></asp:Localize>
            </p>
            <br />
        </div>
        <h4 class="SubTitle">
            <asp:Localize runat="server" ID="SubTitleSearchProductType" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchProductType %>'></asp:Localize>
        </h4>
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="ColumnTitleProductType" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductType %>'></asp:Localize>
                        </span>
                        <br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductType" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize runat="server" ID="ColumnTitleDescription" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDescription %>'></asp:Localize>
                        </span>
                        <br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtDescription" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <zn:Button runat="server" ID="btnClear" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' CausesValidation="False" />
                    <zn:Button runat="server" ID="btnSearch" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>'  />
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle">
            <asp:Localize runat="server" ID="GridTitleProductType" Text='<%$ Resources:ZnodeAdminResource, GridTitleProductType %>'></asp:Localize>
        </h4>
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label><asp:GridView
            ID="uxGrid" runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
            CaptionAlign="Left" OnRowCommand="UxGrid_RowCommand" OnSorting="UxGrid_Sorting"
            Width="100%" PageSize="25" OnRowDeleting="UxGrid_RowDeleting" AllowSorting="False"
            EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyNoProductTypeText %>' OnRowDataBound="uxGrid_RowDataBound">
            <Columns>
                <asp:BoundField DataField="producttypeid" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProductType %>' SortExpression="Name" HeaderStyle-HorizontalAlign="Left"
                    ItemStyle-Width="30%">
                    <ItemTemplate>
                        <asp:Label ID="lblProductTypeName" runat="server" Text='<%# GetProductTypeEditHyperlink(DataBinder.Eval(Container.DataItem, "Name"), DataBinder.Eval(Container.DataItem, "ProductTypeId"),
                        DataBinder.Eval(Container.DataItem, "IsGiftCard") )%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Description" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDescription %>'
                    HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnView" runat="server" CssClass="actionlink" CommandName="View"
                            Text='<%$ Resources:ZnodeAdminResource, LinkAttribute %>' CommandArgument='<%# Eval("producttypeid") %>' Enabled='<%# (Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsGiftCard"))==true?false:true) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnEdit" runat="server" CssClass="actionlink" CommandName="Edit"
                            Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' CommandArgument='<%# Eval("producttypeid") %>' Enabled='<%# (Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsGiftCard"))==true?false:true) %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="btnDelete" runat="server" CssClass="actionlink" CommandName="Delete"
                            Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CommandArgument='<%# Eval("producttypeid") %>' Enabled='<%# (Convert.ToBoolean(DataBinder.Eval(Container.DataItem, "IsGiftCard"))==true?false:true) %>' />
                        <asp:Label ID="lblGiftCard" Visible="false" runat="server" Text='<%# Eval("IsGiftCard") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
        <div>
            <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="10" runat="server"></uc1:Spacer>
        </div>
    </div>
</asp:Content>
