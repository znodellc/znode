<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True"
    Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.ProductTypes.View" ValidateRequest="false" CodeBehind="View.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">

    <script language="javascript" type="text/javascript">
        function DeleteAttributeConfirmation() {
            return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ConfirmDeleteAttribute")) %>');
        }
    </script>

    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Localize runat="server" ID="TextGenerateKeySettings" Text='<%$ Resources:ZnodeAdminResource, TitleProductTypeAttribute %>'></asp:Localize><asp:Label ID="lblProductType" runat="server"></asp:Label>"
            </h1>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ID="ProductTypeList" ButtonType="EditButton" OnClick="ProductTypeList_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonBackToProductType %>' Width="100px" />&nbsp;&nbsp;
            <zn:Button runat="server" ID="Button1" ButtonType="EditButton" OnClick="AddAttributeType_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonAddAttribute %>' Width="125px" />
        </div>
        <div class="ClearBoth"></div>
        <div>
            <asp:Label ID="lblError" CssClass="Error" runat="server"
                Text='<%$ Resources:ZnodeAdminResource, ErrorAddingNewAttribute %>' Visible="false"></asp:Label>
        </div>
        <br />
        <div>
            <asp:Label ID="lblErrorMessage" CssClass="Error" runat="server" Visible="false"></asp:Label>
        </div>
        <uc1:Spacer ID="Spacer" runat="server" SpacerHeight="10" />
        <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyNoAttributesText %>'
            GridLines="None" Width="100%" OnPageIndexChanging="UxGrid_PageIndexChanging"
            OnRowCommand="UxGrid_RowCommand" OnRowDeleting="UxGrid_RowDeleting">
            <Columns>
                <asp:BoundField DataField="ProductAttributeTypeID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleAttribute %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:LinkButton ID="DeleteAttribute" CommandName="Delete" CssClass="actionlink" CommandArgument='<%# Eval("ProductAttributeTypeID") %>'
                            runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' Width="100px" OnClientClick="return DeleteAttributeConfirmation()" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle CssClass="RowStyle" />
            <EditRowStyle CssClass="EditRowStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
        </asp:GridView>
        <uc1:Spacer ID="Spacer5" SpacerHeight="100" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
</asp:Content>
