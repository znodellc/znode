using System;
using System.Data;
using System.Web.UI.WebControls;
using Znode.Engine.Common;

namespace  Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.ProductTypes
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Setup.ReferenceTypes.ProductTypes.Default class
    /// </summary>
    public partial class Default : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string CatalogImagePath = string.Empty;
        private string AddLink = "~/Secure/Inventory/ReferenceTypes/ProductTypes/add.aspx";
        private string ViewLink = "~/Secure/Inventory/ReferenceTypes/ProductTypes/view.aspx";
        private string EditLink = "~/Secure/Inventory/ReferenceTypes/ProductTypes/add.aspx";
        private string DeleteLink = "~/Secure/Inventory/ReferenceTypes/ProductTypes/delete.aspx";
        private string portalIds = string.Empty;
        #endregion

        #region Protected properties
        /// <summary>
        /// Gets or sets the Grid View Sort Direction
        /// </summary>
        private string GridViewSortDirection
        {
            get
            {
                return ViewState["SortDirection"] as string ?? "ASC";
            }

            set
            {
                ViewState["SortDirection"] = value;
            }
        }

        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.BindSearchData();
            }
        }
        #endregion

        #region Bind Methods

        /// <summary>
        /// Bind Search Data to Grid
        /// </summary>
        protected void BindSearchData()
        {
            ZNode.Libraries.Admin.ProductTypeAdmin prodTypeAdmin = new ZNode.Libraries.Admin.ProductTypeAdmin();
            DataSet ds = prodTypeAdmin.GetProductTypeBySearchData(Server.HtmlEncode(txtproductType.Text.Trim()), Server.HtmlEncode(txtDescription.Text.Trim()));
            DataView dv = new DataView(UserStoreAccess.CheckStoreAccess(ds.Tables[0], true));
            dv.Sort = "Name";
            uxGrid.DataSource = dv;
            uxGrid.DataBind();
        }
        #endregion

        #region General Events

        /// <summary>
        /// Get the product type name hyperlink
        /// </summary>
        /// <param name="name">Product type name.</param>
        /// <param name="productTypeId">Product Type Id to set in item Id</param>
        /// <param name="isGiftCard">True if gift card else null.</param>
        /// <returns>Returns the hyperlink tag.</returns>
        protected string GetProductTypeEditHyperlink(object name, object productTypeId, object isGiftCard)
        {
            if (isGiftCard == null || Convert.ToBoolean(isGiftCard) == false)
            {
                return string.Format("<a href='add.aspx?itemid={0}'>{1}</a>", productTypeId.ToString(), name.ToString());

            }
            else
            {
                return name.ToString();
            }
        }

        /// <summary>
        /// Add Product Type Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddProductType_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.AddLink);
        }

        /// <summary>
        /// Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Clear Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("default.aspx");
        }

        #endregion

        #region Grid Events
        /// <summary>
        /// Event triggered when the grid page is changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchData();
        }

        /// <summary>
        /// Event triggered when an item is deleted from the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            this.BindSearchData();
        }

        /// <summary>
        /// Grid Sorting Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_Sorting(object sender, GridViewSortEventArgs e)
        {
            ZNode.Libraries.Admin.ProductTypeAdmin prodTypeAdmin = new ZNode.Libraries.Admin.ProductTypeAdmin();
            DataSet ds = prodTypeAdmin.GetProductTypeBySearchData(txtproductType.Text.Trim(), txtDescription.Text.Trim());
            uxGrid.DataSource = this.SortDataTable(ds, e.SortExpression, true);
            uxGrid.DataBind();

            if (this.GetSortDirection() == "DESC")
            {
                e.SortDirection = SortDirection.Descending;
            }
            else
            {
                e.SortDirection = SortDirection.Ascending;
            }
        }

        /// <summary>
        /// Event triggered when a command button is clicked on the grid
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "page")
            {
            }
            else
            {
                string Id = e.CommandArgument.ToString();
               


                if (e.CommandName == "Edit")
                {
                    this.EditLink = this.EditLink + "?itemid=" + Id;
                    Response.Redirect(this.EditLink);
                }
                else if (e.CommandName == "View")
                {
                    this.ViewLink = this.ViewLink + "?itemid=" + Id;
                    Response.Redirect(this.ViewLink);
                }
                else if (e.CommandName == "Delete")
                {
                    Response.Redirect(this.DeleteLink + "?itemid=" + Id);
                }
            }
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Sorting Data
        /// </summary>
        /// <param name="dataSet">The DataSet to be sorted</param>
        /// <param name="gridViewSortExpression">Grid View Sort Expression</param>
        /// <param name="isPageIndexChanging">Is Page Index Changing Value</param>
        /// <returns>Returns the sorted DataView</returns>
        protected DataView SortDataTable(DataSet dataSet, string gridViewSortExpression, bool isPageIndexChanging)
        {
            if (dataSet != null)
            {
                DataView dataView = new DataView(dataSet.Tables[0]);

                if (gridViewSortExpression.Length > 0)
                {
                    if (isPageIndexChanging)
                    {
                        dataView.Sort = string.Format("{0} {1}", gridViewSortExpression, this.GridViewSortDirection);
                    }
                    else
                    {
                        dataView.Sort = string.Format("{0} {1}", gridViewSortExpression, this.GetSortDirection());
                    }
                }

                return dataView;
            }
            else
            {
                return new DataView();
            }
        }

        /// <summary>
        /// Get Sort Direction
        /// </summary>
        /// <returns>Returns the Sort Direction</returns>
        private string GetSortDirection()
        {
            switch (this.GridViewSortDirection)
            {
                case "ASC":
                    this.GridViewSortDirection = "DESC";
                    break;

                case "DESC":
                    this.GridViewSortDirection = "ASC";
                    break;
            }

            return this.GridViewSortDirection;
        }

        #endregion

        protected void uxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                LinkButton viewButton = (LinkButton)e.Row.Cells[4].FindControl("btnView");
                LinkButton editButton = (LinkButton)e.Row.Cells[5].FindControl("btnEdit");
                LinkButton deleteButton = (LinkButton)e.Row.Cells[6].FindControl("btnDelete");
                Label lblgiftcard = (Label)e.Row.Cells[1].FindControl("lblGiftCard");
                if (lblgiftcard.Text == "True")
                {
                    viewButton.Visible = false;
                    editButton.Visible = false;
                    deleteButton.Visible = false;
                }
                else
                {
                    viewButton.Visible = true;
                    editButton.Visible = true;
                    deleteButton.Visible = true;
                }
            }
        }
    }
}