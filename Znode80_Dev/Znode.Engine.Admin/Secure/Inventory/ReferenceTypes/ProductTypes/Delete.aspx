<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" 
    Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.ProductTypes.Delete" Title="Product Types - Delete" Codebehind="Delete.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h5>
        <asp:Localize runat="server" ID="TextGenerateKeySettings" Text='<%$ Resources:ZnodeAdminResource, TextPleaseConfirm  %>'></asp:Localize>
    </h5>
    <p>
        <asp:Localize runat="server" ID="TextDeleteProductTypeConfirm" Text='<%$ Resources:ZnodeAdminResource, TextDeleteProductTypeConfirm  %>'></asp:Localize>
        <b>"<%=ProductCategoryName%>"</b>.<asp:Localize runat="server" ID="Localize2" Text='<%$ Resources:ZnodeAdminResource, TextProductTypeDelete  %>'></asp:Localize>
        <asp:Localize runat="server" ID="Localize1" Text='<%$ Resources:ZnodeAdminResource, TextDeleteProductTypeConfirm  %>'></asp:Localize>
    </p>
    <p>
        <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label></p>
    <div>
        <zn:Button runat="server" ID="btnDelete"  ButtonType="CancelButton" OnClick="BtnDelete_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonDelete %>' CausesValidation="true" />
        <zn:Button runat="server" ID="btnCancel" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
    </div>
    <br />
    <br />
    <br />
</asp:Content>
