using System;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace  Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.ProductTypes
{
    /// <summary>
    /// Represents the Znode.Engine.Admin.Secure.Setup.ReferenceTypes.ProductTypes.AssociateType class
    /// </summary>
    public partial class AssociateType : System.Web.UI.Page
    {
        #region Private Member Variables
        private int ItemId = 0;
        private string RedirectLink = "~/Secure/Inventory/ReferenceTypes/ProductTypes/view.aspx?itemid=";
        private string AssociateName = string.Empty;
        #endregion

        #region Page Load
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }

            if (!Page.IsPostBack)
            {
                this.BindList();
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                AttributeTypeAdmin _AttributeAdmin = new AttributeTypeAdmin();
                ProductTypeAttribute _typeList = new ProductTypeAttribute();

                // Set Values
                _typeList.ProductTypeId = this.ItemId;

                if (lstAttributeTypeList.SelectedIndex != -1)
                {
                    _typeList.AttributeTypeId = int.Parse(lstAttributeTypeList.SelectedValue);
                }

                ProductTypeAdmin _ProductTypeAdmin = new ProductTypeAdmin();
                ProductType _ProductType = new ProductType();
                _ProductType = _ProductTypeAdmin.GetByProdTypeId(this.ItemId);

                bool Check = false;

                Check = _AttributeAdmin.AddProductTypeAttribute(_typeList);

                if (Check)
                {
                    this.AssociateName =string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAssociateAttribute").ToString(), lstAttributeTypeList.SelectedItem.Text, _ProductType.Name);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, _ProductType.Name);
                    
                    // Redirect to List Page
                    Response.Redirect(this.RedirectLink + this.ItemId);
                }
                else
                {
                    // Display Error Message
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAddingAttribute").ToString();
                    lblError.Visible = true;
                }
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.ToString());
                
                // Display Error Message
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNoteAdd").ToString();
                lblError.Visible = true;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            // Redirect to Main Page
            Response.Redirect(this.RedirectLink + this.ItemId);
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Binds the Attribute type DropDownList
        /// </summary>
        private void BindList()
        {
            AttributeTypeAdmin _AttributeAdmin = new AttributeTypeAdmin();
            TList<AttributeType> _attributeList = new TList<AttributeType>();
            _attributeList = _AttributeAdmin.GetAll();
            foreach (AttributeType attributetype in _attributeList)
            {
                attributetype.Name = Server.HtmlDecode(attributetype.Name);
            }

            lstAttributeTypeList.DataSource = UserStoreAccess.CheckStoreAccess(_attributeList, true);
            lstAttributeTypeList.DataTextField = "Name";
            lstAttributeTypeList.DataValueField = "AttributeTypeId";
            lstAttributeTypeList.DataBind();
        }
        #endregion
    }
}