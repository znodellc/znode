<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" ValidateRequest="false"
    Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.ProductTypes.AssociateType" CodeBehind="AssociateType.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Localize runat="server" ID="TitleEditProductTypeAttribute" Text='<%$ Resources:ZnodeAdminResource, TitleEditProductTypeAttribute %>'></asp:Localize>
            </h1>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ID="btnSubmitTop" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancelTop" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
        </div>
        <div class="ClearBoth">
        </div>
        <asp:Label ID="lblError" runat="server" CssClass="Error" Visible="False"></asp:Label>
        <div class="FieldStyle">

            <asp:Localize runat="server" ID="ColumnTitleSelectAttributeType" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSelectAttributeType %>'></asp:Localize>
            <span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:DropDownList ID="lstAttributeTypeList" runat="server" /><asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="lstAttributeTypeList"
                CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ErrorSelectAttributeType %>'
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <zn:Button runat="server" ID="btnSubmitBottom" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancelBottom" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' />
        </div>
    </div>
</asp:Content>
