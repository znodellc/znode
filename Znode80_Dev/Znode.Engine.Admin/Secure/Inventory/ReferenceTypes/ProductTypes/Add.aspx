<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="Znode.Engine.Admin.Secure.Inventory.ReferenceTypes.ProductTypes.Add" ValidateRequest="false" Title="Product Types - Add"
    CodeBehind="Add.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <div class="FormView">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>
        <div style="text-align: right">
            <zn:Button runat="server" ID="btnSubmitTop" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancelTop" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleProductTypeName" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductTypeName %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="Name" runat="server" MaxLength="50" Columns="50"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="Name"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductType %>' CssClass="Error"
                SetFocusOnError="True"></asp:RequiredFieldValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleFranchisable" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFranchisable %>'></asp:Localize>
            <br />
        </div>
        <div class="ValueStyle">
            <asp:CheckBox ID="chkFranchisble" runat="server" Text='<%$ Resources:ZnodeAdminResource, CheckBoxFranchiseable %>' />
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleDisplayOrder" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>'></asp:Localize><span class="Asterix">*</span>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="DisplayOrder" runat="server" MaxLength="9" Columns="5"></asp:TextBox>
            <asp:RequiredFieldValidator ID="Requiredfieldvalidator2" runat="server" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredDisplayOrder %>'
                CssClass="Error" ControlToValidate="DisplayOrder"></asp:RequiredFieldValidator>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="DisplayOrder"
                Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber %>' CssClass="Error" MaximumValue="999999999"
                MinimumValue="1" Type="Integer"></asp:RangeValidator>
        </div>
        <div class="FieldStyle">
            <asp:Localize runat="server" ID="ColumnTitleDescription" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDescription %>'></asp:Localize>
            <br />
            <small>
                <asp:Localize runat="server" ID="HintSubTextDescription" Text='<%$ Resources:ZnodeAdminResource, HintSubTextDescription %>'></asp:Localize>
            </small>
        </div>
        <div class="ValueStyle">
            <asp:TextBox ID="Description" runat="server" Height="172px" MaxLength="4000" TextMode="MultiLine"
                Width="420px"></asp:TextBox>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <asp:Label ID="lblError" CssClass="Error" runat="server"></asp:Label>
        </div>
        <br />
        <div>
            <zn:Button runat="server" ID="btnSubmitBottom" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit %>' />
            <zn:Button runat="server" ID="btnCancelBottom" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel %>' CausesValidation="False" />
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnProductName" Value="" />
</asp:Content>
