<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Inventory.Default" Title="Inventory - Default" CodeBehind="Default.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">


     <div class="LeftMargin">
        <h1><asp:Localize runat="server" ID="Inventory" Text='<%$ Resources:ZnodeAdminResource, TitleInventory %>'></asp:Localize></h1>

        <hr />

        <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/NewProducts.png" />
            </div>
            <div class="Shortcut"><a id="A3" href="~/Secure/Inventory/Products/Default.aspx" runat="server"><asp:Localize runat="server" ID="Products" Text='<%$ Resources:ZnodeAdminResource, LinkTextProducts %>'></asp:Localize></a></div>
            <div class="LeftAlign">
                <p><asp:Localize ID="TextProducts" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextProducts %>'></asp:Localize></p>
            </div>
        </div>

      
         <div class="ImageAlign">
            <div class="Image">
                <img src="../../Themes/Images/ImportExport.png" /></div>
            <div class="Shortcut">
                <a id="A1" href="~/Secure/Inventory/ImportExportData/Default.aspx" runat="server"><asp:Localize runat="server" ID="Import" Text='<%$ Resources:ZnodeAdminResource, LinkTextImport %>'></asp:Localize></a>
            </div>
       
               <p><asp:Localize ID="TextImport" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextImport %>'></asp:Localize></p>
           
        </div>

          <h1><asp:Localize ID="TitleReferenceTypes" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleReferenceTypes %>'></asp:Localize></h1>
    <hr />

    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/Brands.png" />
        </div>
        <div class="Shortcut"><a id="A12" href="~/Secure/Inventory/ReferenceTypes/Brands/Default.aspx" runat="server"><asp:Localize ID="Brands" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextBrands %>'></asp:Localize></a></div>
        <div class="LeftAlign">
            <p><asp:Localize ID="TextBrands" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextBrands %>'></asp:Localize></p>
        </div>
    </div>

    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/ProductTypes.png" />
        </div>

        <div class="Shortcut"><a id="A5" href="~/Secure/Inventory/ReferenceTypes/ProductTypes/default.aspx" runat="server"><asp:Localize ID="ProductType" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextProductTypes %>'></asp:Localize></a></div>
        <div class="LeftAlign">
            <p><asp:Localize ID="TextProductType" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextProductTypes %>'></asp:Localize></p>
        </div>
    </div>


    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/attributes.png" />
        </div>
        <div class="Shortcut"><a id="A2" href="~/Secure/Inventory/ReferenceTypes/AttributeTypes/Default.aspx" runat="server"><asp:Localize ID="AttributeType" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextAttributeTypes %>'></asp:Localize></a></div>
        <div class="LeftAlign">
            <p>
               <asp:Localize ID="TextAttributeType" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextAttributeTypes %>'></asp:Localize>
            </p>
        </div>
    </div>


    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/add-ons.png" />
        </div>
        <div class="Shortcut"><a id="A4" href="~/Secure/Inventory/ReferenceTypes/AddOnTypes/Default.aspx" runat="server"><asp:Localize ID="AddOnTypes" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextAddOnTypes %>'></asp:Localize></a></div>
        <div class="LeftAlign">
            <p><asp:Localize ID="TextAddOnTypes" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextAddOnTypes %>'></asp:Localize></p>
        </div>
    </div>


    <div class="ImageAlign">
        <div class="Image">
            <img src="../../Themes/Images/highlights.png" />
        </div>
        <div class="Shortcut"><a id="A9" href="~/Secure/Inventory/ReferenceTypes/Highlights/Default.aspx" runat="server"><asp:Localize ID="HighLights" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextHighLights %>'></asp:Localize></a></div>
        <div class="LeftAlign">
            <p><asp:Localize ID="TextHighLights" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextCreateHighlight %>'></asp:Localize></p>
        </div>
    </div>

    </div>
</asp:Content>

