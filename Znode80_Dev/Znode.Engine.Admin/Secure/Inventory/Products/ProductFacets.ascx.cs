﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Custom;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.Framework.Business;
using System.Linq;
namespace Znode.Engine.Admin.Secure.Inventory.Products
{
	/// <summary>
	/// Represents the Site Admin  Admin_Secure_catalog_product_product_facets class
	/// </summary>
	public partial class ProductFacets : System.Web.UI.UserControl
	{
		#region Private Member Variables
		private int ItemId = 0;
		private int SkuId = 0;
		private string _ItemType = "ProductId";
		private string AssociateName = string.Empty;

		/// <summary>
		/// Gets or sets the Item Type
		/// </summary>
		public string ItemType
		{
			get
			{
				return this._ItemType;
			}

			set
			{
				this._ItemType = value;
			}
		}
		#endregion

		#region Page Events

		protected void Page_Load(object sender, EventArgs e)
		{
			// Get ItemId from querystring        
			if (Request.Params["itemid"] != null)
			{
				this.ItemId = int.Parse(Request.Params["itemid"]);
			}

			// Get ItemId from querystring        
			if (Request.Params["skuid"] != null)
			{
				this.SkuId = int.Parse(Request.Params["skuid"]);
			}

			if (!Page.IsPostBack)
			{
				this.BindGrid();
			}

		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Grid Data Bound Event
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void UxGrid_DataBound(object sender, EventArgs e)
		{
			bool visible = false;
			bool row = true;
			foreach (GridViewRow gvr in uxGrid.Rows)
			{
				gvr.Visible = (gvr.FindControl("dlFacetNames") as DataList).Items.Count > 0;

				if (gvr.Visible)
				{
					gvr.CssClass = row ? "RowStyle" : "AlternatingRowStyle";
					row = row ? false : true;
				}

				visible = visible || gvr.Visible;
			}

			uxGrid.Visible = visible;

			lblEmptyGrid.Visible = !uxGrid.Visible;
		}

		/// <summary>
		/// Add Client side event to the Delete Button in the Grid.
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				// Retrieve the Button control from the Fourth column.
				LinkButton DeleteButton = (LinkButton)e.Row.Cells[3].FindControl("DeleteFacet");

				// Add Client Side confirmation
                DeleteButton.OnClientClick = this.GetGlobalResourceObject("ZnodeAdminResource", "ConfirmDelete").ToString();
			}
		}

		protected void UxGrid_RowDeleting(object sender, GridViewDeleteEventArgs e)
		{
			this.BindFacets();
		}

		/// <summary>
		/// Bind facet Group
		/// </summary>
		/// <param name="tgg">The value of FacetGroup TList</param>
		protected void BindFacetGroup(TList<FacetGroup> tgg)
		{
			this.ddlFacetGroups.DataSource = tgg.OrderBy(x=>x.DisplayOrder);
			this.ddlFacetGroups.DataTextField = "FacetGroupLabel";
			this.ddlFacetGroups.DataValueField = "FacetGroupId";
			this.ddlFacetGroups.DataBind();

			if (tgg.Count == 0)
			{
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "RecordNotAssociatedFacetCategory").ToString();
				lblError.Visible = true;
			}
		}

		/// <summary>
		/// Bind Facets Method
		/// </summary>
		protected void BindFacets()
		{
			if (!string.IsNullOrEmpty(ddlFacetGroups.SelectedValue))
			{
				FacetService ts = new FacetService();

				this.chklstFacets.DataSource = ts.GetByFacetGroupID(Convert.ToInt32(ddlFacetGroups.SelectedValue));
				this.chklstFacets.DataTextField = "FacetName";
				this.chklstFacets.DataValueField = "FacetId";
				this.chklstFacets.DataBind();

				FacetProductSKUService tpss = new FacetProductSKUService();
				TList<FacetProductSKU> tpsList;
				if (this._ItemType == "SkuId")
				{
					tpsList = tpss.GetBySKUID(this.SkuId);
				}
				else
				{
					tpsList = tpss.GetByProductID(this.ItemId);
				}

				if (tpsList.Count > 0)
				{
					foreach (ListItem li in chklstFacets.Items)
					{
						li.Selected = tpsList.Find("FacetID", Convert.ToInt32(li.Value)) != null;
					}
				}
			}
		}

		/// <summary>
		/// Bind Grid method
		/// </summary>
		protected void BindGrid()
		{
			TList<FacetGroup> facet = this.GetAssociatedFacetGroups();
			this.BindFacetGroup(facet);

			uxGrid.Visible = true;

			uxGrid.DataSource = facet;

			uxGrid.DataBind();
		}

		/// <summary>
		/// Get Facet Names Method
		/// </summary>
		/// <param name="facetGroupId">The value of Facet Group id</param>
		/// <returns>Returns Facet Names</returns>
		protected string GetFacetNames(object facetGroupId)
		{
			FacetProductSKUService tpss = new FacetProductSKUService();
			TList<FacetProductSKU> tpsList;
			if (this._ItemType == "SkuId")
			{
				tpsList = tpss.GetBySKUID(this.SkuId);
			}
			else
			{
				tpsList = tpss.GetByProductID(this.ItemId);
			}

			FacetService ts = new FacetService();
			string facetNames = string.Empty;
			if (tpsList.Count > 0)
			{
				foreach (FacetProductSKU tps in tpsList)
				{
					Facet facet = ts.GetByFacetID(tps.FacetID.Value);
					if (facet.FacetGroupID == Convert.ToInt32(facetGroupId))
					{
						if (!string.IsNullOrEmpty(facetNames))
						{
							facetNames += ",";
						}

						facetNames += facet.FacetName;
					}
				}
			}

			return facetNames;
		}

		/// <summary>
		/// Get Facets Method
		/// </summary>
		/// <param name="facetGroupId">The value of facetGroupID</param>
		/// <returns>Returns Facet TList</returns>
		protected TList<Facet> GetFacets(object facetGroupId)
		{
			FacetProductSKUService tpss = new FacetProductSKUService();
			TList<FacetProductSKU> tpsList;
			if (this._ItemType == "SkuId")
			{
				tpsList = tpss.GetBySKUID(this.SkuId);
			}
			else
			{
				tpsList = tpss.GetByProductID(this.ItemId);
			}

			FacetService ts = new FacetService();
			TList<Facet> tfacet = new TList<Facet>();
			if (tpsList.Count > 0)
			{
				foreach (FacetProductSKU tps in tpsList)
				{
					Facet facet = ts.GetByFacetID(tps.FacetID.Value);
					if (facet.FacetGroupID == Convert.ToInt32(facetGroupId))
					{
						tfacet.Add(facet);
					}
				}
			}

			return tfacet;
		}

		/// <summary>
		/// Update Button Click Event
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void Update_Click(object sender, EventArgs e)
		{
			int flag = 0;
			StringBuilder sb = new StringBuilder();

			FacetProductSKUService service = new FacetProductSKUService();
			TList<FacetProductSKU> tpsList;
			if (this._ItemType == "SkuId")
			{
				tpsList = service.GetBySKUID(this.SkuId);
			}
			else
			{
				tpsList = service.GetByProductID(this.ItemId);
			}

			foreach (ListItem li in chklstFacets.Items)
			{
				if (li.Selected == true)
				{
					flag = 1;
					lblError.Visible = false;
					if (tpsList.Find("FacetID", Convert.ToInt32(li.Value)) == null)
					{
						FacetProductSKU tps = new FacetProductSKU();
						tps.FacetID = Convert.ToInt32(li.Value);
						if (this._ItemType == "SkuId")
						{
							tps.SKUID = this.SkuId;
						}
						else
						{
							tps.ProductID = this.ItemId;
						}

						service.Save(tps);
					}
					sb.Append(li.Text + ",");
				}
				else
				{
					FacetProductSKU tp;
					if ((tp = tpsList.Find("FacetID", Convert.ToInt32(li.Value))) != null)
					{
						service.Delete(tp);
					}
				}


			}

			if (flag == 0)
			{
				lblError.Visible = true;
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSelectAnyFacet").ToString();
				return;
			}


			ProductAdmin prodAdmin = new ProductAdmin();
			Product entity = prodAdmin.GetByProductId(this.ItemId);

			sb.Remove(sb.Length - 1, 1);
			this.AssociateName = "Associated  Facet " + sb + " to Product " + entity.Name;
			ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, entity.Name);
			pnlFacetsList.Visible = true;
			tpsList.Dispose();
			service = null;
			this.BindGrid();
			this.pnlEditFacets.Visible = false;



		}

		/// <summary>
		/// Cancel Button Click Event
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void Cancel_Click(object sender, EventArgs e)
		{
			this.pnlEditFacets.Visible = false;
			pnlFacetsList.Visible = true;
			lblError.Visible = false;
		}

		/// <summary>
		/// Add Button Click Event
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void Add_Click(object sender, EventArgs e)
		{
			lblError.Visible = false;
			this.pnlEditFacets.Visible = true;
			pnlFacetsList.Visible = false;
			if (ddlFacetGroups.Items.Count > 0)
			{
				this.ddlFacetGroups.SelectedIndex = 0;
			}

			this.BindFacets();
		}

		/// <summary>
		/// Facet Group Drop Down list Selected Index Changed Event
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void DdlFacetGroups_SelectedIndexChanged(object sender, EventArgs e)
		{
			this.BindFacets();
		}

		/// <summary>
		/// Grid Row Command Event
		/// </summary>
		/// <param name="sender">Sender object that raised the event.</param>
		/// <param name="e">Event Argument of the object that contains event data.</param>
		protected void UxGrid_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			ProductAdmin prodAdmin = new ProductAdmin();
			Product entity = prodAdmin.GetByProductId(this.ItemId);

			if (e.CommandName == "FacetEdit")
			{
				ListItem li = this.ddlFacetGroups.Items.FindByValue(e.CommandArgument as string);
				if (li != null)
				{
					this.ddlFacetGroups.SelectedIndex = this.ddlFacetGroups.Items.IndexOf(li);
					this.BindFacets();
					this.pnlEditFacets.Visible = true;
					pnlFacetsList.Visible = false;
				}

				this.AssociateName = "Associated Facets " + li.Text + " to Product " + entity.Name;
				ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, entity.Name);
			}
			else if (e.CommandName == "FacetDelete")
			{
				FacetService service = new FacetService();
				TList<Facet> facetList = service.GetByFacetGroupID(Convert.ToInt32(e.CommandArgument));

				StringBuilder sb = new StringBuilder();
				foreach (Facet facet in facetList)
				{
					sb.Append(facet.FacetName + ",");
					this.RemoveFacetProductSku(facet);
				}
                this.AssociateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeleteAssociateFacet").ToString() + sb + " and Product " + entity.Name;
				ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, entity.Name);
			}
		}

		#endregion

		#region Private Methods

		/// <summary>
		/// Remove Facet Product SKU Method
		/// </summary>
		/// <param name="facet">Instance of Facet</param>
		private void RemoveFacetProductSku(Facet facet)
		{
			FacetProductSKUService service = new FacetProductSKUService();
			TList<FacetProductSKU> tps = service.GetByFacetID(facet.FacetID);

			foreach (FacetProductSKU tp in tps)
			{
				if (this._ItemType == "SkuId")
				{
					if (tp.SKUID == this.SkuId)
					{
						service.Delete(tp);
					}
				}
				else
				{
					if (tp.ProductID == this.ItemId)
					{
						service.Delete(tp);
					}
				}
			}

			this.BindGrid();
		}

		/// <summary>
		/// Get Associated Facet Groups
		/// </summary>
		/// <returns>Returns the FacetGroup TList</returns>
		private TList<FacetGroup> GetAssociatedFacetGroups()
		{
			FacetGroupService tgs = new FacetGroupService();
			FacetService ts = new FacetService();
			TList<FacetGroup> facet = new TList<FacetGroup>();

			if (this.ItemId > 0)
			{
				ProductService ps = new ProductService();

				Product product = ps.DeepLoadByProductID(this.ItemId, true, ZNode.Libraries.DataAccess.Data.DeepLoadType.IncludeChildren, typeof(TList<ProductCategory>));

				if (product != null && product.ProductCategoryCollection != null)
				{
					foreach (ProductCategory pc in product.ProductCategoryCollection)
					{
						foreach (FacetGroup tg in tgs.GetByCategoryIDFromFacetGroupCategory(pc.CategoryID))
						{
							if (facet.IndexOf(tg) == -1 && ts.GetByFacetGroupID(tg.FacetGroupID).Count > 0)
							{
								facet.Add(tg);
							}
						}
					}
				}
			}

			return facet;
		}
		#endregion
	}
}