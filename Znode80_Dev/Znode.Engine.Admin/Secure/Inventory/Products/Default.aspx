<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Inventory.Products.Default" Title="Manage Products - List" ValidateRequest="false" CodeBehind="Default.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ManufacturerAutoComplete" Src="~/Controls/Default/ManufacturerAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ProductTypeAutoComplete" Src="~/Controls/Default/ProductTypeAutoComplete.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div>
            <div class="LeftFloat" style="width: 50%">
                <h1><asp:Localize ID="products" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleProducts %>' /> 
                </h1>
            </div>
            <div class="ButtonStyle">
                <zn:LinkButton ID="btnAddProduct" runat="server" CausesValidation="False"
                    ButtonType="Button" OnClick="BtnAddProduct_Click" Text='<%$ Resources:ZnodeAdminResource, TitleAddNewProduct %>' 
                    ButtonPriority="Primary" />
            </div>
        </div>
        <div class="ClearBoth" align="left">
            <p>
             <asp:Localize ID="TextProducts" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextProducts %>'></asp:Localize>
            
            </p>
            <br />
        </div>
        <h4 class="SubTitle"><asp:Localize ID="SearchProducts" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleSearchProducts %>'></asp:Localize> 
            </h4>
        <asp:Panel ID="SearchPanel" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="ProductName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>' ></asp:Localize> </span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductname" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="ProductNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductsNumber %>' ></asp:Localize> </span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductnumber" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="eProductsSKU" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductsSKU %>' ></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtsku" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="Catalog" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCatalog %>'></asp:Localize> </span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlCatalog" runat="server" AutoPostBack="true"></asp:DropDownList></span>
                    </div>
                </div>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="ColumnTitleBrand" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBrand %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <ZNode:ManufacturerAutoComplete runat="server" ID="txtManufacturer" />
                        </span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="ProductType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductType %>'></asp:Localize> </span><br />
                        <span class="ValueStyle">
                            <ZNode:ProductTypeAutoComplete ID="txtProductType" runat="server" />
                        </span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle"><asp:Localize ID="ProductCategory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductCategory %>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <ZNode:CategoryAutoComplete ID="txtCategory" runat="server" />
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <br />
                </div>
                <div>  
                      
                <zn:Button ID="btnClear" runat="server" ButtonType="CancelButton" CausesValidation="false" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' />
     
                 <zn:Button ID="btnSearch" runat="server" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />
     
                </div>
            </div>
        </asp:Panel>
        <br />
        <h4 class="GridTitle"> <asp:Localize ID="ProductList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleProductList %>'></asp:Localize></h4>
        <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
        <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
            GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
            OnRowCommand="UxGrid_RowCommand" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridEmptyText %>' 
            Width="100%" CellPadding="4">
            <Columns>
                <asp:BoundField DataField="productid" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, GridColumnTitleImage %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href='<%# "view.aspx?itemid=" + DataBinder.Eval(Container.DataItem,"productid")%>'
                            id="LinkView">
                            <img alt=" " src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                                runat="server" style="border: none" />
                        </a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:HyperLinkField DataNavigateUrlFields="productid" DataNavigateUrlFormatString="view.aspx?itemid={0}"
                    DataTextField="name" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>'  HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnRetailPrice %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# FormatPrice(DataBinder.Eval(Container.DataItem,"RetailPrice")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSalePrice %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# FormatPrice(DataBinder.Eval(Container.DataItem,"SalePrice")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleWholesalePrice %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# FormatPrice(DataBinder.Eval(Container.DataItem,"WholesalePrice")) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleInStock %>' DataField="Quantityonhand" HeaderStyle-HorizontalAlign="Left" />
                <asp:BoundField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder %>' DataField="displayorder" HeaderStyle-HorizontalAlign="Left" />
                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsActive %>' HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                            runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:LinkButton ID="linkbtnBanage" CommandName="Manage" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"productid")%>'
                            Text='<%$ Resources:ZnodeAdminResource,LinkManage %>'  runat="server" CssClass="actionlink" />
                        <asp:LinkButton ID="LinkBtnCopy" CommandName="Copy" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"productid")%>'
                            Text='<%$ Resources:ZnodeAdminResource,LinkCopy %>' runat="server" CssClass="actionlink" />
                        &nbsp;<asp:LinkButton ID="LinkBtnDelete" CommandName="Delete" CommandArgument='<%#DataBinder.Eval(Container.DataItem,"productid")%>'
                            Text='<%$ Resources:ZnodeAdminResource,LinkDelete %>' runat="server" CssClass="actionlink" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle CssClass="FooterStyle" />
            <RowStyle CssClass="RowStyle" />
            <PagerStyle CssClass="PagerStyle" />
            <HeaderStyle CssClass="HeaderStyle" />
            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
        </asp:GridView>
    </div>
</asp:Content>
