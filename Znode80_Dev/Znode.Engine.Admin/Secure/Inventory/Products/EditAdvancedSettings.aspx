<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True"
    ValidateRequest="false" Inherits="Znode.Engine.Admin.Secure.Inventory.Products.EditAdvancedSettings"
    Title="Manage Products - Edit Advanced Settings" CodeBehind="EditAdvancedSettings.aspx.cs" %>

<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleProductEditSettings %>'></asp:Label></h1>
        </div>
        <div style="text-align: right">
            <zn:Button ID="btnSubmitTop" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
            <zn:Button ID="btnCancelTop" runat="server" ButtonType="CancelButton" CausesValidation="False" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
        </div>
        <div class="ClearBoth">
        </div>
        <asp:Label ID="lblError" runat="server" CssClass="Error"></asp:Label>
        <div class="FormView">
            <h4 class="SubTitle">
                <asp:Localize ID="DisplaySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDisplaySettings %>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="DisplayProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayProduct %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox Checked="true" ID="CheckEnabled" Text='<%$ Resources:ZnodeAdminResource, CheckboxDisplayProduct %>' runat="server" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="HomePageSpecial" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleHomePageSpecial %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="CheckHomeSpecial" Text='<%$ Resources:ZnodeAdminResource, CheckBoxFeatureItem %>' runat="server" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="NewItem" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleNewItem %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="CheckNewItem" Text='<%$ Resources:ZnodeAdminResource, CheckBoxNewIcon %>' runat="server" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="FeaturedItem" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFeaturedItem %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="ChkFeaturedProduct" Text='<%$ Resources:ZnodeAdminResource, CheckBoxFeatureIcon %>' runat="server" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="CallForPricing" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCallForPricing %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="CheckCallPricing" Text='<%$ Resources:ZnodeAdminResource, CheckBoxPromptCallForPricing %>' runat="server" />
            </div>
            <div class="FieldStyle" style="display: none;">
                <asp:Localize ID="DisplayInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayInventory %>'></asp:Localize>
            </div>
            <div class="ValueStyle" style="display: none;">
                <asp:CheckBox ID="CheckDisplayInventory" Text='<%$ Resources:ZnodeAdminResource, CheckBoxProductInvetoryDisplayed %>' runat="server" />
            </div>
            <asp:Panel ID="pnlFranchise" runat="server">
                <div class="FieldStyle">
                    <asp:Localize ID="Franchisable" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFranchisable %>'></asp:Localize>
                </div>
                <div class="ValueStyle">
                    <asp:CheckBox ID="CheckFranchisable" Text='<%$ Resources:ZnodeAdminResource, CheckBoxProductEnableFranchise %>' runat="server" />
                </div>
            </asp:Panel>
            <h4 class="SubTitle">
                <asp:Localize ID="InventorySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleInventorySettings %>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="OutofStockOptions" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOutofStockOptions %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:RadioButtonList ID="InvSettingRadiobtnList" runat="server">
                    <asp:ListItem Selected="True" Value="1" Text='<%$ Resources:ZnodeAdminResource, RadioButtonDisableOutofstockProducts %>'> </asp:ListItem>
                    <asp:ListItem Value="2" Text='<%$ Resources:ZnodeAdminResource, RadioButtonAllowBackOrderProducts %>'></asp:ListItem>
                    <asp:ListItem Value="3" Text='<%$ Resources:ZnodeAdminResource, RadioButtonDontTrackInventory %>'></asp:ListItem>
                </asp:RadioButtonList>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="InStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleInStockMessage %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtInStockMsg" runat="server"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="OutofStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOutofStockMessage %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtOutofStock" runat="server" Text='<%$ Resources:ZnodeAdminResource, ValueOutOfStock %>'> </asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="BackOrderMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBackOrderMessage %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtBackOrderMsg" runat="server"></asp:TextBox>
            </div>
            <div style="display: none;">
                <small>
                    <asp:Localize ID="IndicateProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextIndicateProduct %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkDropShip" runat="server" Visible="false" Text='<%$ Resources:ZnodeAdminResource, SubTextDropShip %>' />
            </div>
            <h4 class="SubTitle">
                <asp:Localize ID="RecurringBillingSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleRecurringBillingSettings %>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="RecurringBilling" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRecurringBilling %>'></asp:Localize>
            </div>
            <div class="ValueStyleText">
                <asp:CheckBox ID="chkRecurringBillingInd" AutoPostBack="true" OnCheckedChanged="ChkRecurringBillingInd_CheckedChanged" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEnableRecurringBilling %>' />
            </div>
            <asp:Panel ID="pnlRecurringBilling" runat="server" Visible="false">
                <div class="FieldStyle">
                    <asp:Localize ID="BillingAmount" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBillingAmount %>'></asp:Localize><span class="Asterix">*</span><br />
                    <small>
                        <asp:Localize ID="BillingPeriod" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextBillingPeriod %>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;
                    <asp:TextBox ID="txtRecurringBillingInitialAmount" MaxLength="10" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredBillingAmount %>'
                        ControlToValidate="txtRecurringBillingInitialAmount" CssClass="Error" Display="Dynamic"></asp:RequiredFieldValidator><br />
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtRecurringBillingInitialAmount"
                        Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeBillingAmount %>'
                        CssClass="Error" Display="Dynamic" /><br />
                    <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtRecurringBillingInitialAmount"
                        ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidBillingAmount %>'
                        MaximumValue="99999999" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="BillingPeriods" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBillingPeriod %>'></asp:Localize><span class="Asterix">*</span>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlBillingPeriods" AutoPostBack="true" runat="server" OnSelectedIndexChanged="DdlBillingPeriods_SelectedIndexChanged">
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextWeekly %>' Value="WEEK"></asp:ListItem>
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextMonthly %>' Value="MONTH"></asp:ListItem>
                        <asp:ListItem Text='<%$ Resources:ZnodeAdminResource, DropDownTextYearly %>' Value="YEAR"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </asp:Panel>
        </div>
        <h4 class="SubTitle">
            <asp:Localize ID="SeoSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSeoSettings %>'></asp:Localize></h4>

        <div class="FormView">
            <div class="FieldStyle">
                <asp:Localize ID="SEOTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOTitle %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOTitle" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="SEOKeywords" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOKeywords %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOMetaKeywords" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="SEODescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEODescription %>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOMetaDescription" runat="server" MaxLength="500" Columns="50"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="SEOFriendlyPageName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOFriendlyPageName %>'></asp:Localize>
                <br />
                <small>
                    <asp:Localize ID="UseOnlyCharacters" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextUseOnlyCharacters %>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtSEOUrl" runat="server" MaxLength="100" Columns="50"></asp:TextBox>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtSEOUrl"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularSEOFriendlyName %>'
                    SetFocusOnError="True" ValidationExpression='<%$ Resources:ZnodeAdminResource, ValidSEOUrl %>'></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle" style="display: none">
            </div>
            <div class="ValueStyleText">
                <asp:CheckBox ID="chkAddURLRedirect" runat="server" Text='<%$ Resources:ZnodeAdminResource, CheckBoxRedirectUrl %>' />
            </div>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <zn:Button ID="btnSubmitBottom" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
            <zn:Button ID="btnCancelBottom" runat="server" ButtonType="CancelButton" CausesValidation="False" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
        </div>
    </div>
</asp:Content>
