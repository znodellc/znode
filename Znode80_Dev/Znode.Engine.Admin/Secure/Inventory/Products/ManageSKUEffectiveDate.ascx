﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ManageSKUEffectiveDate.ascx.cs"
    Inherits="Znode.Engine.Admin.Secure.Inventory.Products.ManageSKUEffectiveDate" %>
<%@ Register TagPrefix="znode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Panel runat="server" ID="pnlSkuProfile">
    <asp:UpdatePanel runat="server" ID="upSkuProfile">
        <ContentTemplate>
            <div style="float: left; width: 50%;">
                <h4 class="GridTitle"><asp:Literal ID="ProfileSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleProfileSettings %>'></asp:Literal></h4>
            </div>
            <div class="ButtonStyle" style="clear: right; float: right; padding-top: 10px; padding-right: 10px;">
                <zn:LinkButton ID="AddProfile" runat="server"  CausesValidation="false" ButtonType="Button"
                    Text='<%$ Resources:ZnodeAdminResource, GridTitleProfileSettings %>' OnClick="Add_Click"  ButtonPriority="Primary" />

            </div>
			<div style="clear: both;">
            <asp:GridView ID="uxGrid" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                CaptionAlign="Left" CellPadding="4" CssClass="Grid" GridLines="None" Width="50%"
                DataKeyNames="SkuProfileEffectiveID" OnRowEditing="uxGrid_EditCommand" OnRowDeleting="uxGrid_DeleteCommand" >
                <Columns>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProfilesName %>'>
                        <ItemTemplate><%# Eval("Name")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProfileEffectiveDate %>'>
                        <ItemTemplate><%# Eval("EffectiveDate", "{0:MM/dd/yyyy}")%></ItemTemplate>
                    </asp:TemplateField>
                    <asp:ButtonField ButtonType="Link" CommandName="Edit" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' />
					<asp:TemplateField >
                            <ItemTemplate>
                                <asp:LinkButton ID="DeleteProfile" CommandName="Delete" 
                                    runat="server" CssClass="actionlink" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' OnClientClick="return DeleteConfirmation();"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                </Columns>
                <RowStyle CssClass="RowStyle" />
                <EditRowStyle CssClass="EditRowStyle" />
                <EmptyDataTemplate><asp:Literal ID="DataSKUProfile" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridNoDataSKUProfile %>'></asp:Literal>
                   
                </EmptyDataTemplate>
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
            </asp:GridView>
				</div>
            <znode:Spacer ID="Spacer1" runat="server" SpacerHeight="10" />

            <asp:Label runat="server" ID="lblError" CssClass="Error"></asp:Label>

            <zn:Button ID="btnShowPopup" runat="server" Style="display: none" />
            <ajaxToolKit:ModalPopupExtender ID="mdlPopup" runat="server" TargetControlID="btnShowPopup"
                PopupControlID="pnlEditSkuProfile" BackgroundCssClass="modalBackground" />

            <asp:Panel runat="server" ID="pnlEditSkuProfile" Style="display: none;" CssClass="PopupStyle" Width="500">
                <h4 class="GridTitle">
                    <asp:Label runat="server" ID="lblTitle"></asp:Label></h4>
                <div class="FieldStyle">
                   <asp:Literal ID="DataSKUProfile" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProfileName %>'></asp:Literal> :<br />
                    <small> <asp:Literal ID="Literal1" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextProfileAssociate %>'></asp:Literal></small>
                </div>
                <div class="ValueStyle">
                    <asp:DropDownList ID="ddlProfile" runat="server">
                    </asp:DropDownList>
                </div>
                <div class="FieldStyle">
                    <asp:Literal ID="EffectiveDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEffectiveDate %>'></asp:Literal><br />
                    <small><asp:Literal ID="ProfileEffectiveDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextProfileEffectiveDate %>'></asp:Literal></small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox runat="server" ID="txtEffectiveDate"></asp:TextBox>&nbsp;<asp:ImageButton
                        ID="imgbtnStartDt" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" />&nbsp;
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularValidDate %>'
                        ControlToValidate="txtEffectiveDate" ValidationGroup="skuprofile" CssClass="Error"
                        Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtEffectiveDate"
                        CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularStartDate %>' 
                        ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularProfileEffectiveStartDate %>'  
                        ValidationGroup="skuprofile"></asp:RegularExpressionValidator>
                    <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                        runat="server" TargetControlID="txtEffectiveDate">
                    </ajaxToolKit:CalendarExtender>
                </div>
                <div class="ClearBoth">
                    <br />
                </div>
                <div id="pnlSKUDateButtons" runat="server">
                   
                     <zn:Button ID="btnSubmitBottom" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="Update_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
       
                     <zn:Button ID="btnCancelBottom" runat="server" ButtonType="CancelButton"  CausesValidation="True" OnClick="Cancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
         

                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
 <script language="javascript" type="text/javascript">
     function DeleteConfirmation() {
         return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ConfirmDelete")) %>');
     }
  </script>