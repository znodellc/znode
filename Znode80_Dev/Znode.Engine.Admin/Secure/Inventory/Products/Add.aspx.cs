using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Utilities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin Admin_Secure_products_add class
    /// </summary>
    public partial class Add : System.Web.UI.Page
    {
        #region protected Member Variables
        private string cancelLink = "View.aspx?itemid=";
        private string ListLink = "Default.aspx";
        private string productImageName = string.Empty;
        private int itemId;
        private int skuId = 0;
        private int productId = 0;
        private StringBuilder selectedNodes = null;
        private StringBuilder deleteNodes = null;
        private int productTypeId = 0;
        private bool isSuccess;
        private string pImagePath = string.Empty;
        private string pskuImagepath = string.Empty;

        #endregion
          
        #region Page load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {


            this.selectedNodes = new StringBuilder();
            this.deleteNodes = new StringBuilder();

            // Get product id value from query string
            if (Request.Params["itemid"] != null)
            {
                this.itemId = Convert.ToInt32(Request.Params["itemid"].ToString());
                this.productId = this.itemId;
            }
            else
            {
                this.itemId = 0;
            } 
            if (!Page.IsPostBack)
            {
                // Dynamic error message for weight field compare validator
                WeightFieldCompareValidator.ErrorMessage = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ValidCompareWeight").ToString(), 2.5.ToString());

                // Bind shipping options
                this.BindShippingTypes();

                this.BindExpirationFrequency();

                // If edit func then bind the data fields
                if (this.itemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleEditProduct").ToString();
                    this.BindData();
                    this.BindEditData();
                    tblShowImage.Visible = true;
                    pnlShowImage.Visible = true;
                    tblProductDescription.Visible = false;
                    pnlUploadSection.Attributes["style"] += "border-left: solid 1px #cccccc;padding-left: 10px; ";
                }
                else
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleAddProduct").ToString();
                    this.BindData();
                    tblShowImage.Visible = true;
                    pnlShowImage.Visible = false;
                    pnlImage.Visible = false;
                    tblProductDescription.Visible = true;
                    DivAttributes.Visible = false;
                }
                EnableValidators();

            }

            // Bind SKU Attributes Dynamically
            if (!string.IsNullOrEmpty(ProductTypeList.Value))
            {
                this.Bind(int.Parse(ProductTypeList.Value));

                if (this.itemId > 0)
                {
                    // Bind Edit SKU Attributes
                    this.BindAttributes(int.Parse(ProductTypeList.Value));
                }
				
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "Error", "<script>answer = confirm('Please add a product type before you add a new product'); if(answer != '0'){ location='Default.aspx' } </script>");
            }
            if (chkFreeShippingInd.Checked == true)
            { 
               this.dvShipSeparate.Style.Add("display", "none");
            }
            else
            {
                this.dvShipSeparate.Style.Add("display", "block");
            }

        }
        #endregion

        #region General Events

        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            SKUAdmin skuAdminAccess = new SKUAdmin();
            ProductAdmin productAdmin = new ProductAdmin();
            SKUAttribute skuAttribute = new SKUAttribute();
            SKU sku = new SKU();
            ProductCategory productCategory = new ProductCategory();
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            Product product = new Product();
            System.IO.FileInfo fileInfo = null;
            System.IO.FileInfo skufileInfo = null;

            bool retVal = false;

            int existingProductTypeId = 0;

            // Passing Values 
            product.ProductID = this.itemId;

            // If edit mode then get all the values first
            if (this.itemId > 0)
            {
                product = productAdmin.GetByProductId(this.itemId);
                existingProductTypeId = product.ProductTypeID;
                if (ViewState["productSkuId"] != null)
                {
                    sku.SKUID = int.Parse(ViewState["productSkuId"].ToString());
                }

                bool isSkuExist = skuAdminAccess.IsSkuExist(txtProductSKU.Text.Trim(), this.itemId);

                if (isSkuExist)
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSKUorPart").ToString();
                    return;
                }
            }
            else
            {
                bool isSkuExist = skuAdminAccess.IsSkuExist(txtProductSKU.Text.Trim(), this.itemId);

                if (isSkuExist)
                {
                    lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSKUorPart").ToString(); 
                    return;
                }
            }

            // General Info
            product.Name = Server.HtmlEncode(txtProductName.Text);
            product.ImageFile = txtimagename.Text;
            product.ProductNum = txtProductNum.Text;

            product.ProductTypeID = int.Parse(ProductTypeList.Value);
            if (divExpirationDays.Visible)
            {
                product.ExpirationPeriod = Convert.ToInt32(txtExporationPeriod.Text);
                product.ExpirationFrequency = Convert.ToInt32(ddlExpirationFrequency.SelectedValue);
            }

            // MANUFACTURER
            if (!string.IsNullOrEmpty(ManufacturerList.Value))
            {
                product.ManufacturerID = Convert.ToInt32(ManufacturerList.Value);
            }
            else
            {
                product.ManufacturerID = null;
            }

            // Supplier
            if (string.IsNullOrEmpty(SupplierList.Value))
            {
                product.SupplierID = null;
            }
            else
            {
                product.SupplierID = Convert.ToInt32(SupplierList.Value);
            }

            product.DownloadLink = Server.HtmlEncode(txtDownloadLink.Text.Trim());
            product.ShortDescription = Server.HtmlEncode(txtshortdescription.Text);
            product.Description = Server.HtmlEncode(ctrlHtmlText.Html);
            product.RetailPrice = Convert.ToDecimal(txtproductRetailPrice.Text);
            product.FeaturesDesc = ctrlHtmlPrdFeatures.Html.Trim();
            product.Specifications = ctrlHtmlPrdSpec.Html.Trim();
            product.AdditionalInformation = ctrlHtmlProdInfo.Html.Trim();

            if (txtproductSalePrice.Text.Trim().Length > 0)
            {
                product.SalePrice = Convert.ToDecimal(txtproductSalePrice.Text.Trim());
            }
            else
            {
                product.SalePrice = null;
            }

            if (txtProductWholeSalePrice.Text.Trim().Length > 0)
            {
                product.WholesalePrice = Convert.ToDecimal(txtProductWholeSalePrice.Text.Trim());
            }
            else
            {
                product.WholesalePrice = null;
            }

            // Quantity Available
            int qty;
            int reorder;
            int.TryParse(txtProductQuantity.Text, out qty);
            int.TryParse(txtReOrder.Text, out reorder);

            product.MinQty = 1;
            if (!string.IsNullOrEmpty(txtMinQuantity.Text.Trim()))
            {
                product.MinQty = Convert.ToInt32(txtMinQuantity.Text);
            }

            product.MaxQty = 10;
            if (!string.IsNullOrEmpty(txtMaxQuantity.Text.Trim()))
            {
                product.MaxQty = Convert.ToInt32(txtMaxQuantity.Text);
            }

            // Display Settings
            product.DisplayOrder = int.Parse(txtDisplayOrder.Text.Trim());

            // Tax Settings        
            if (ddlTaxClass.SelectedIndex != -1)
            {
                product.TaxClassID = int.Parse(ddlTaxClass.SelectedValue);
            }

            // Shipping Option setting
            product.ShippingRuleTypeID = Convert.ToInt32(ShippingTypeList.SelectedValue);
            product.FreeShippingInd = chkFreeShippingInd.Checked;
            product.ShipSeparately = chkShipSeparately.Checked;

            if (txtProductWeight.Text.Trim().Length > 0)
            {
                product.Weight = Convert.ToDecimal(txtProductWeight.Text.Trim());
            }
            else
            {
                product.Weight = null;
            }

            // Product Height - Which will be used to determine the shipping cost
            if (txtProductHeight.Text.Trim().Length > 0)
            {
                product.Height = decimal.Parse(txtProductHeight.Text.Trim());
            }
            else
            {
                product.Height = null;
            }

            if (txtProductWidth.Text.Trim().Length > 0)
            {
                product.Width = decimal.Parse(txtProductWidth.Text.Trim());
            }
            else
            {
                product.Width = null;
            }

            if (txtProductLength.Text.Trim().Length > 0)
            {
                product.Length = decimal.Parse(txtProductLength.Text.Trim());
            }
            else
            {
                product.Length = null;
            }

            decimal shippingRate = 0;
            decimal.TryParse(txtShippingRate.Text, out shippingRate);
            product.ShippingRate = shippingRate;
            if (product.ShippingRuleTypeID != (int)Znode.Engine.Shipping.ZnodeShippingRuleType.FixedRatePerItem)
            {
                product.ShippingRate = null;
            }

            // Stock
            sku.ProductID = this.itemId;
            sku.SKU = txtProductSKU.Text.Trim();

            // Update SKU if Supplier is given
            if (string.IsNullOrEmpty(SupplierList.Value))
            {
                sku.SupplierID = null;
            }
            else
            {
                sku.SupplierID = Convert.ToInt32(SupplierList.Value);
            }

            sku.ActiveInd = true;

            if (txtReOrder.Text.Trim().Length > 0)
            {
                // Update Quantity SKU
                SKUAdmin.UpdateQuantity(sku, Convert.ToInt32(txtProductQuantity.Text), Convert.ToInt32(txtReOrder.Text.Trim()));
            }
            else
            {
                SKUAdmin.UpdateQuantity(sku, Convert.ToInt32(txtProductQuantity.Text));
            }

            product.ImageAltTag = txtImageAltTag.Text.Trim();

            // Set update date
            product.UpdateDte = System.DateTime.Now;

            DataSet MyAttributeTypeDataSet = productAdmin.GetAttributeTypeByProductTypeID(int.Parse(ProductTypeList.Value));

            // Create transaction
            TransactionManager tranManager = ConnectionScope.CreateTransaction();

            try
            {
                // PRODUCT UPDATE
                if (this.itemId > 0)
                {

                    // Upload File if this is a new product or the New Image option was selected for an existing product
                    if (RadioProductNewImage.Checked)
                    {
                        if (uxProductImage.PostedFile != null && !string.IsNullOrEmpty(uxProductImage.PostedFile.FileName))
                        {
                            uxProductImage.AppendToFileName = "-" + this.productId.ToString();
                            fileInfo = uxProductImage.FileInformation;
                            skufileInfo = uxProductImage.FileInformation;

                            if (!string.IsNullOrEmpty(product.PortalID.ToString()))
                            {
                                isSuccess = uxProductImage.SaveImage("fadmin", product.PortalID.ToString(), "0");
                                pImagePath = "Turnkey/" + product.PortalID + "/" + uxProductImage.FileInformation.Name;
                                pskuImagepath = "Turnkey/" + product.PortalID + "/" + skufileInfo.Name;
                            }
                            else if (!string.IsNullOrEmpty(product.AccountID.ToString()))
                            {
                                isSuccess = uxProductImage.SaveImage("madmin", "0", product.AccountID.ToString());
                                pImagePath = "Mall/" + product.AccountID + "/" + uxProductImage.FileInformation.Name;
                                pskuImagepath = "Mall/" + product.AccountID + "/" + skufileInfo.Name;
                            }
                            else
                            {
                                isSuccess = uxProductImage.SaveImage("sadmin", ZNodeConfigManager.SiteConfig.PortalID.ToString(), "0");
                                pImagePath = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + uxProductImage.FileInformation.Name;
                                pskuImagepath = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + skufileInfo.Name;
                            }


                            if (!isSuccess)
                            {
                                if (uxProductImage.FileInformation.Name != string.Empty)
                                {
                                    product.ImageFile = pImagePath;
                                    sku.SKUPicturePath = pskuImagepath;
                                }

                                return;
                            }
                            else
                            {
                                product.ImageFile = pImagePath;
                                sku.SKUPicturePath = pskuImagepath;
                            }
                        }
                    }

                    // Update product Sku and Product values
                    if (MyAttributeTypeDataSet.Tables[0].Rows.Count > 0)
                    {
                        // If ProductType has SKU's
                        if (sku.SKUID > 0)
                        {
                            // For this product already SKU if on exists
                            sku.UpdateDte = System.DateTime.Now;

                            // Check whether Duplicate attributes is created
                            string Attributes = String.Empty;

                            DataSet MyAttributeTypeDataSet1 = productAdmin.GetAttributeTypeByProductTypeID(int.Parse(ProductTypeList.Value));

                            foreach (DataRow MyDataRow in MyAttributeTypeDataSet1.Tables[0].Rows)
                            {
                                System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                                if (lstControl != null)
                                {
                                    int selValue = int.Parse(lstControl.SelectedValue);

                                    Attributes += selValue.ToString() + ",";
                                }
                            }

                            // Split the string
                            string Attribute = Attributes.Substring(0, Attributes.Length - 1);

                            // To check SKU combination is already exists.
                            bool RetValue = skuAdminAccess.CheckSKUAttributes(this.itemId, sku.SKUID, Attribute);

                            if (!RetValue)
                            {
                                // Then Update the database with new property values
                                retVal = productAdmin.Update(product, sku);
                            }
                            else
                            {
                                // Throw error if duplicate attribute
                                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAttributeExist").ToString();
                                return;
                            }
                        }
                        else
                        {
                            retVal = productAdmin.Update(product);

                            // If Product doesn't have any SKUs yet,then create new SKU in the database
                            skuAdminAccess.Add(sku);
                        }
                    }
                    else
                    {
                        retVal = productAdmin.Update(product);

                        // If User modifies product type for this product,
                        if (existingProductTypeId != product.ProductTypeID)
                        {
                            // then Remove all the existing SKUs for this product
                            skuAdminAccess.DeleteByProductId(this.itemId);

                            // Add default product sku
                            skuAdminAccess.Add(sku);
                        }
                    }

                    if (!retVal)
                    {
                        throw new ApplicationException();
                    }

                    // Delete existing SKUAttributes
                    skuAdminAccess.DeleteBySKUId(sku.SKUID);

                    // Add SKU Attributes                
                    foreach (DataRow MyDataRow in MyAttributeTypeDataSet.Tables[0].Rows)
                    {
                        System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                        int selValue = int.Parse(lstControl.SelectedValue);

                        if (selValue > 0)
                        {
                            skuAttribute.AttributeId = selValue;
                        }

                        skuAttribute.SKUID = sku.SKUID;

                        skuAdminAccess.AddSKUAttribute(skuAttribute);
                    }
                }
                else
                {
                    // PRODUCT ADD
                    product.ActiveInd = true;
                    product.ReviewStateID = (int)ZNodeProductReviewState.Approved;

                    // If ProductType has SKUs, then insert sku with Product
                    retVal = productAdmin.Add(product, sku, out this.productId, out this.skuId);

                    if (!retVal)
                    {
                        throw new ApplicationException();
                    }

                    // Add SKU Attributes
                    foreach (DataRow MyDataRow in MyAttributeTypeDataSet.Tables[0].Rows)
                    {
                        System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + MyDataRow["AttributeTypeId"].ToString());

                        int selValue = int.Parse(lstControl.SelectedValue);

                        if (selValue > 0)
                        {
                            skuAttribute.AttributeId = selValue;
                        }

                        skuAttribute.SKUID = this.skuId;

                        skuAdminAccess.AddSKUAttribute(skuAttribute);
                    }

                    if (uxProductImage.PostedFile != null)
                    {
                        // Validate image
                        if ((this.itemId == 0 || RadioProductNewImage.Checked == true) && uxProductImage.PostedFile.FileName.Length > 0)
                        {
                            uxProductImage.AppendToFileName = "-" + this.productId.ToString();
                        }
                    }

                    // Upload File if this is a new product or the New Image option was selected for an existing product
                    if (RadioProductNewImage.Checked || this.itemId == 0)
                    {
                        if (uxProductImage.PostedFile != null && !string.IsNullOrEmpty(uxProductImage.PostedFile.FileName))
                        {
                            fileInfo = uxProductImage.FileInformation;
                            skufileInfo = uxProductImage.FileInformation;

                            if (!string.IsNullOrEmpty(product.PortalID.ToString()))
                            {
                                isSuccess = uxProductImage.SaveImage("fadmin", product.PortalID.ToString(), "0");
                                pImagePath = "Turnkey/" + product.PortalID + "/" + uxProductImage.FileInformation.Name;
                                pskuImagepath = "Turnkey/" + product.PortalID + "/" + skufileInfo.Name;
                            }
                            else if (!string.IsNullOrEmpty(product.AccountID.ToString()))
                            {
                                isSuccess = uxProductImage.SaveImage("madmin", "0", product.AccountID.ToString());
                                pImagePath = "Mall/" + product.AccountID + "/" + uxProductImage.FileInformation.Name;
                                pskuImagepath = "Mall/" + product.AccountID + "/" + skufileInfo.Name;
                            }
                            else
                            {
                                isSuccess = uxProductImage.SaveImage("sadmin", ZNodeConfigManager.SiteConfig.PortalID.ToString(), "0");
                                pImagePath = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + uxProductImage.FileInformation.Name;
                                pskuImagepath = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + skufileInfo.Name;
                            }

                            //bool isSuccess = uxProductImage.SaveImage("sadmin", ZNodeConfigManager.SiteConfig.PortalID.ToString(), "0");
                            if (!isSuccess)
                            {
                                if (uxProductImage.FileInformation.Name != string.Empty)
                                {
                                    if (!string.IsNullOrEmpty(product.PortalID.ToString()))
                                    {
                                        product.ImageFile = "Turnkey/" + product.PortalID + "/" + uxProductImage.FileInformation.Name;
                                        sku.SKUPicturePath = "Turnkey/" + product.PortalID + "/" + skufileInfo.Name;
                                    }
                                    else if (!string.IsNullOrEmpty(product.AccountID.ToString()))
                                    {
                                        product.ImageFile = "Mall/" + product.AccountID + "/" + uxProductImage.FileInformation.Name;
                                        sku.SKUPicturePath = "Mall/" + product.AccountID + "/" + skufileInfo.Name;
                                    }
                                    else
                                    {
                                        product.ImageFile = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + uxProductImage.FileInformation.Name;
                                        sku.SKUPicturePath = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + skufileInfo.Name;
                                    }
                                }
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(product.PortalID.ToString()))
                                {
                                    product.ImageFile = "Turnkey/" + product.PortalID + "/" + uxProductImage.FileInformation.Name;
                                    sku.SKUPicturePath = "Turnkey/" + product.PortalID + "/" + skufileInfo.Name;
                                }
                                else if (!string.IsNullOrEmpty(product.AccountID.ToString()))
                                {
                                    product.ImageFile = "Mall/" + product.AccountID + "/" + uxProductImage.FileInformation.Name;
                                    sku.SKUPicturePath = "Mall/" + product.AccountID + "/" + skufileInfo.Name;
                                }
                                else
                                {
                                    product.ImageFile = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + uxProductImage.FileInformation.Name;
                                    sku.SKUPicturePath = "Turnkey/" + ZNodeConfigManager.SiteConfig.PortalID + "/" + skufileInfo.Name;
                                }
                            }
                        }
                    }

                    productAdmin.Update(product);
                }

                // Commit transaction
                tranManager.Commit();
            }
            catch (Exception ex)
            {
                ZNodeLogging.LogMessage(ex.ToString());
                // Error occurred so rollback transaction
                if (tranManager.IsOpen)
                {
                    tranManager.Rollback();
                }

                lblMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductUpdate").ToString();
                return;
            }

            // Redirect to next page
            if (this.itemId > 0)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Edit of Product - " + txtProductName.Text, txtProductName.Text);

                string ViewLink = "~/Secure/Inventory/Products/View.aspx?itemid=" + this.itemId.ToString();
                Response.Redirect(ViewLink);
            }
            else
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Creation of Product - " + txtProductName.Text, txtProductName.Text);

                string NextLink = "~/Secure/Inventory/Products/View.aspx?itemid=" + this.productId.ToString();
                Response.Redirect(NextLink);
            }
        }

        /// <summary> 
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            if (this.itemId > 0)
            {
                Response.Redirect(this.cancelLink + this.itemId);
            }
            else
            {
                Response.Redirect(this.ListLink);
            }
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductCurrentImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = false;
        }

        /// <summary>
        /// Radio Button Check Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void RadioProductNewImage_CheckedChanged(object sender, EventArgs e)
        {
            tblProductDescription.Visible = true;
        }

        /// <summary>
        /// Shipping rule type selected index change event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ShippingTypeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.EnableValidators();
        }
        #endregion

        #region Helper Methods

        /// <summary>
        /// Bind the gift card type product expiration frequency value.
        /// </summary>
        private void BindExpirationFrequency()
        {
            List<ListItem> expirationFrequencyList = new List<ListItem>() 
            { 
                new ListItem("Day(s)", "1"), 
                new ListItem("Week(s)", "7"), 
                new ListItem("Month(s)", "30"), 
                new ListItem("Year(s)", "365") 
            };

            ddlExpirationFrequency.Items.AddRange(expirationFrequencyList.ToArray());
        }

        /// <summary>
        /// Bind control display based on properties set-Dynamically adds DropDownList for Each AttributeType
        /// </summary>
        /// <param name="productTypeID">Product Type ID</param>
        private void Bind(int productTypeID)
        {
            this.productTypeId = productTypeID;
            divExpirationDays.Visible = false;
            DivAttributes.Visible = false;
            GiftCardService giftCardService = new GiftCardService();
            ProductTypeAdmin productTypeAdmin = new ProductTypeAdmin();
            ProductType productType = productTypeAdmin.GetByProdTypeId(productTypeID);

            if (productType.IsGiftCard != null && productType.IsGiftCard == true)
            {
                // Show the gift card expiration field.
                divExpirationDays.Visible = true;
            }
            else
            {
                // Bind Attributes
                ProductAdmin adminAccess = new ProductAdmin();
                DataSet MyDataSet = adminAccess.GetAttributeTypeByProductTypeID(productTypeID);

                // Repeat until Number of Attributetypes for this Product
                foreach (DataRow dr in MyDataSet.Tables[0].Rows)
                {
                    // Get all the Attribute for this Attribute
                    DataSet attributeDataSet = new DataSet();
                    attributeDataSet = adminAccess.GetByAttributeTypeID(int.Parse(dr["attributetypeid"].ToString()));

                    DataView dv = new DataView(attributeDataSet.Tables[0]);

                    // Create Instance for the DropDownlist
                    System.Web.UI.WebControls.DropDownList lstControl = new DropDownList();
                    lstControl.ID = "lstAttribute" + dr["AttributeTypeId"].ToString();

                    ListItem li = new ListItem(dr["Name"].ToString(), "0");
                    li.Selected = true;
                    dv.Sort = "DisplayOrder ASC";

                    // AttributeDataSet;
                    lstControl.DataSource = dv;
                    lstControl.DataTextField = "Name";
                    lstControl.DataValueField = "AttributeId";
                    lstControl.DataBind();
                    lstControl.Items.Insert(0, li);

                    if (!Convert.ToBoolean(dr["IsPrivate"]))
                    {
                        // Add the Control to Place Holder
                        ControlPlaceHolder.Controls.Add(lstControl);

                        RequiredFieldValidator FieldValidator = new RequiredFieldValidator();
                        FieldValidator.ID = "Validator" + dr["AttributeTypeId"].ToString();
                        FieldValidator.ControlToValidate = "lstAttribute" + dr["AttributeTypeId"].ToString();
                        FieldValidator.ErrorMessage = "Select " + dr["Name"].ToString();
                        FieldValidator.Display = ValidatorDisplay.Dynamic;
                        FieldValidator.CssClass = "Error";
                        FieldValidator.InitialValue = "0";

                        ControlPlaceHolder.Controls.Add(FieldValidator);
                        Literal lit1 = new Literal();
                        lit1.Text = "&nbsp;&nbsp;";
                        ControlPlaceHolder.Controls.Add(lit1);
                    }
                }

                if (MyDataSet.Tables[0].Rows.Count == 0)
                {
                    DivAttributes.Visible = false;
                    pnlquantity.Visible = true;
                    pnlSupplier.Visible = true;
                }
                else
                {
                    DivAttributes.Visible = true;
                    pnlquantity.Visible = false;
                    pnlSupplier.Visible = false;
                }
            }
        }

        /// <summary>
        /// Returns a Format Weight string
        /// </summary>
        /// <param name="fieldValue">The field value</param>
        /// <returns>Returns bool value true or false</returns>
        private string FormatNull(object fieldValue)
        {
            if (fieldValue == null)
            {
                return String.Empty;
            }
            else
            {
                if (Convert.ToDecimal(fieldValue.ToString()) == 0)
                {
                    return string.Empty;
                }
                else
                {
                    return fieldValue.ToString();
                }
            }
        }

        /// <summary>
        /// Format Retail and Wholesale price
        /// </summary>
        /// <param name="fieldValue">The Field Value</param>
        /// <returns>Returns the Formatted price</returns>
        private string FormatPrice(object fieldValue)
        {
            if (fieldValue == null)
            {
                return String.Empty;
            }
            else
            {
                if (fieldValue.ToString().Equals("0"))
                {
                    return String.Empty;
                }
                else
                {
                    return String.Format("{0:c}", fieldValue).Substring(1).ToString();
                }
            }
        }

        /// <summary>
        /// Enable Validators Method
        /// </summary>
        private void EnableValidators()
        {
            pnlShippingRate.Visible = false;

            if (ShippingTypeList.SelectedValue.Equals("3"))
            {
                pnlShippingRate.Visible = true;
            }

            weightBasedRangeValidator.Enabled = false;
            RequiredForWeightBasedoption.Enabled = false;

            if (ShippingTypeList.SelectedValue.Equals("2"))
            {
                weightBasedRangeValidator.Enabled = true;
                RequiredForWeightBasedoption.Enabled = true;
            }
        }
        #endregion

        #region Bind Edit Data
        /// <summary>
        /// Bind value for Particular Product
        /// </summary>
        private void BindEditData()
        {
            ProductAdmin productAdmin = new ProductAdmin();
            Product product = new Product();
            ProductCategory productCategory = new ProductCategory();
            SKUAdmin skuAdmin = new SKUAdmin();

            if (this.itemId > 0)
            {
                // Checking the user profile for roles and permission and then loading the data based on roles
                ProfileCommon profiles = (ProfileCommon)ProfileCommon.Create(Page.User.Identity.Name, true);
                if (!UserStoreAccess.CheckUserRoleInProduct(profiles, this.itemId))
                {
                    Response.Redirect("Default.aspx", true);
                }

                product = productAdmin.GetByProductId(this.itemId);
                ProductService productService = new ProductService();
                productService.DeepLoad(product);
                productCategory = productAdmin.GetProductCategoryByProductID(this.itemId);

                // General Information - Section1
                lblTitle.Text += product.Name;
                txtProductName.Text = Server.HtmlDecode(product.Name);
                txtProductNum.Text = Server.HtmlDecode(product.ProductNum);

                if (product.ProductTypeID > 0)
                {
                    this.productTypeId = product.ProductTypeID;
                    ProductTypeList.IsEnabled = this.IsEnabled();
                    ProductTypeList.Value = product.ProductTypeID.ToString();
                    ProductTypeList.DataBind();
                    ProductTypeAdmin productTypeAdmin = new ProductTypeAdmin();
                    ProductType productType = productTypeAdmin.GetByProdTypeId(this.productTypeId);
                    if (productType != null && productType.IsGiftCard == true)
                    {
                        divExpirationDays.Visible = true;
                        txtExporationPeriod.Text = product.ExpirationPeriod.Value.ToString();
                        ddlExpirationFrequency.SelectedIndex = ddlExpirationFrequency.Items.IndexOf(ddlExpirationFrequency.Items.FindByValue(product.ExpirationFrequency.Value.ToString()));
                    }
                }

                if (product.ManufacturerID != null)
                {
                    ManufacturerList.Value = product.ManufacturerID.ToString();
                    ManufacturerList.Text = product.ManufacturerIDSource.Name.ToString();
                }

                if (product.SupplierID != null)
                {
                    SupplierList.Value = product.SupplierID.ToString();
                   
					var orderLineItemList = productAdmin.GetTotalOrderLineItemBySKU(product.SupplierID, ZNodeConfigManager.SiteConfig.PortalID);
					//if Order is thier in Order Line Item
	                if (orderLineItemList > 0)
	                {
		                pnlSupplier.Enabled = false;
	                }
	                else
	                {
						pnlSupplier.Enabled = true;
	                }
	                SupplierList.Text = product.SupplierIDSource.Name.ToString();

                }

                // Product Description and Image - Section2
                txtshortdescription.Text = Server.HtmlDecode(product.ShortDescription);
                ctrlHtmlText.Html = Server.HtmlDecode(product.Description);

                // Additional Info
                ctrlHtmlPrdFeatures.Html = product.FeaturesDesc;
                ctrlHtmlPrdSpec.Html = product.Specifications;
                ctrlHtmlProdInfo.Html = product.AdditionalInformation;

                ZNodeImage znodeImage = new ZNodeImage();
                Image1.ImageUrl = znodeImage.GetImageHttpPathMedium(product.ImageFile);

                // Displaying the Image file name in a textbox           
                txtimagename.Text = product.ImageFile;
                txtImageAltTag.Text = product.ImageAltTag;

                // Product properties
                if (product.RetailPrice.HasValue)
                {
                    txtproductRetailPrice.Text = product.RetailPrice.Value.ToString("N");
                }

                if (product.SalePrice.HasValue)
                {
                    txtproductSalePrice.Text = product.SalePrice.Value.ToString("N");
                }

                if (product.WholesalePrice.HasValue)
                {
                    txtProductWholeSalePrice.Text = product.WholesalePrice.Value.ToString("N");
                }

                txtDownloadLink.Text = Server.HtmlDecode(product.DownloadLink);

                // Tax Classes
                if (product.TaxClassID.HasValue)
                {
                    ddlTaxClass.SelectedValue = product.TaxClassID.Value.ToString();
                }

                txtShippingRate.Text = product.ShippingRate.GetValueOrDefault().ToString("N");

                // Display Settings            
                txtDisplayOrder.Text = product.DisplayOrder.ToString();

                // Inventory
                TList<SKU> skuList = skuAdmin.GetByProductID(this.itemId);

                if (skuList != null && skuList.Count > 0)
                {
                    ViewState["productSkuId"] = skuList[0].SKUID.ToString();
                    txtProductSKU.Text = skuList[0].SKU;

                    // Set the existing value in hidden
                    existSKUvalue.Value = skuList[0].SKU;

                    SKUInventory skuInventory = skuAdmin.GetSkuInventoryBySKU(skuList[0].SKU);

                    if (skuInventory != null)
                    {
                        txtProductQuantity.Text = skuInventory.QuantityOnHand.GetValueOrDefault(0).ToString();
                        txtReOrder.Text = skuInventory.ReOrderLevel.GetValueOrDefault(0).ToString();
                    }

                    this.BindAttributes(product.ProductTypeID);
                }

                if (product.MinQty.HasValue)
                {
                    txtMinQuantity.Text = product.MinQty.Value.ToString();
                }

                if (product.MaxQty.HasValue)
                {
                    txtMaxQuantity.Text = product.MaxQty.Value.ToString();
                }

                // Shipping settings
                ShippingTypeList.SelectedValue = product.ShippingRuleTypeID.ToString();

                // This method will enable or disable validators based on the shippingtype
                this.EnableValidators();
                chkShipSeparately.Checked = product.ShipSeparately;
                if (product.FreeShippingInd.HasValue)
                {
                    chkFreeShippingInd.Checked = product.FreeShippingInd.Value;
                    if (chkFreeShippingInd.Checked)
                    {
                        chkShipSeparately.Enabled = false;
                        chkShipSeparately.Checked = false;
                    }
                }



                if (product.Weight.HasValue)
                {
                    txtProductWeight.Text = product.Weight.Value.ToString("N2");
                }

                if (product.Height.HasValue)
                {
                    txtProductHeight.Text = product.Height.Value.ToString("N2");
                }

                if (product.Width.HasValue)
                {
                    txtProductWidth.Text = product.Width.Value.ToString("N2");
                }

                if (product.Length.HasValue)
                {
                    txtProductLength.Text = product.Length.Value.ToString("N2");
                }

                // Release the Resources
                skuList.Dispose();
            }
        }

        /// <summary>
        /// Binds the Sku Attributes for this Product
        /// </summary>
        /// <param name="productTypeID">Product Type ID</param>
        private void BindAttributes(int productTypeID)
        {
            SKUAdmin adminSKU = new SKUAdmin();
            ProductAdmin adminAccess = new ProductAdmin();

            if (ViewState["productSkuId"] != null)
            {
                // Get SKUID from the ViewState
                DataSet SkuDataSet = adminSKU.GetBySKUId(int.Parse(ViewState["productSkuId"].ToString()));
                DataSet MyDataSet = adminAccess.GetAttributeTypeByProductTypeID(productTypeID);

                foreach (DataRow dr in MyDataSet.Tables[0].Rows)
                {
                    foreach (DataRow Dr in SkuDataSet.Tables[0].Rows)
                    {
                        System.Web.UI.WebControls.DropDownList lstControl = (System.Web.UI.WebControls.DropDownList)ControlPlaceHolder.FindControl("lstAttribute" + dr["AttributeTypeId"].ToString());

                        if (lstControl != null)
                        {
                            lstControl.SelectedValue = Dr["Attributeid"].ToString();
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Returns the boolean value
        /// </summary>
        /// <returns>Returns true or false</returns>
        private bool IsEnabled()
        {
            if (this.itemId > 0)
            {
                ParentChildProductAdmin parentChildProduct = new ParentChildProductAdmin();
                TList<ParentChildProduct> _parentChildProductList = parentChildProduct.GetParentChildProductByProductId(this.itemId);

                if (_parentChildProductList.Count > 0)
                {
                    return false;
                }
            }

            return true;
        }

        #endregion

        #region Bind Data
        /// <summary>
        /// Bind Shipping option list
        /// </summary>
        private void BindShippingTypes()
        {
            // Bind ShippingRuleTypes
            ShippingAdmin shippingAdmin = new ShippingAdmin();
            ShippingTypeList.DataSource = shippingAdmin.GetShippingRuleTypes();
            ShippingTypeList.DataTextField = "description";
            ShippingTypeList.DataValueField = "shippingruletypeid";
            ShippingTypeList.DataBind();
        }

        /// <summary>
        /// Bind Tax Class
        /// </summary>
        private void BindData()
        {
            TaxRuleAdmin TaxRuleAdmin = new TaxRuleAdmin();
            TList<TaxClass> taxClass = TaxRuleAdmin.GetAllTaxClass();
			taxClass.ApplyFilter(delegate(ZNode.Libraries.DataAccess.Entities.TaxClass tax)
				{ return tax.ActiveInd == true && (tax.PortalID == null || tax.PortalID == ZNodeConfigManager.SiteConfig.PortalID) ; });
            taxClass.Sort("DisplayOrder");
            ddlTaxClass.DataSource = taxClass;
            ddlTaxClass.DataTextField = "name";
            ddlTaxClass.DataValueField = "TaxClassID";
            ddlTaxClass.DataBind();
        }
         


        #endregion

         
    }
}