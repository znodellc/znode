﻿<%@ Control Language="C#" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Inventory.Products.ProductFacets" CodeBehind="ProductFacets.ascx.cs" %>
<%@ Register TagPrefix="znode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Panel ID="pnlCategories" runat="server">
</asp:Panel>
<!-- Product List -->
<asp:UpdatePanel runat="server" ID="upProductFacets">
    <ContentTemplate>
        <asp:Panel ID="pnlFacetsList" runat="server">
            <h4 class="GridTitle"><asp:Literal ID="GridTitleFacets" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleFacets %>'></asp:Literal></h4>
            <div>
                <div class="TabDescription" style="width: 80%;">
                        <p><asp:Literal ID="SubTextFacet" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextFacet %>'></asp:Literal></p>
                    <asp:Label runat="server" ID="lblEmptyGrid"><asp:Literal ID="notAssociatedFacet" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextNotAssociatedFacet %>'></asp:Literal><%=ItemType.Replace("Id","") %></asp:Label>
                </div>
              <div class="ButtonStyle" style="clear: right; float: right; padding-top: 10px; padding-right: 10px;"> 
                    <zn:LinkButton ID="LinkButton1" runat="server"
                        ButtonType="Button" OnClick="Add_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource,TitleAssociateFacet %>'
                        ButtonPriority="Primary" />
                </div>
            </div>

            <znode:Spacer ID="Spacer3" runat="server" SpacerHeight="10" />
            <div style="clear: both;">
                <asp:GridView ID="uxGrid" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                    CaptionAlign="Left" CellPadding="4" CssClass="Grid" GridLines="None" Width="100%"
                    OnRowCommand="UxGrid_RowCommand" OnDataBound="UxGrid_DataBound" OnRowDataBound="UxGrid_RowDataBound"
                    OnRowDeleting="UxGrid_RowDeleting">
                    <Columns>
                        <asp:BoundField DataField="FacetGroupLabel" HeaderText='<%$ resources:ZnodeAdminResource,ColumnTitleFacetGroupName %>' HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText='<%$ resources:ZnodeAdminResource,ColumnTitleFacetNames %>' ItemStyle-Width="60%">
                            <ItemTemplate>
                                <asp:DataList runat="server" ID="dlFacetNames" DataSource='<%# GetFacets(Eval("FacetGroupId")) %>'
                                    RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblFacetNames" Text='<%# Eval("FacetName") %>'></asp:Label>,
                                    </ItemTemplate>
                                </asp:DataList>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:LinkButton ID="EditFacet" CommandName="FacetEdit" CommandArgument='<%# Eval("FacetGroupID") %>'
                                    runat="server" CssClass="actionlink" Text='<%$ resources:ZnodeAdminResource,LinkEdit %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-Width="10%">
                            <ItemTemplate>
                                <asp:LinkButton ID="DeleteFacet" CommandName="FacetDelete" CommandArgument='<%# Eval("FacetGroupID") %>'
                                    runat="server" CssClass="actionlink" Text='<%$ resources:ZnodeAdminResource,LinkDelete %>' OnClientClick="return DeleteConfirmation();"/>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <RowStyle CssClass="RowStyle" />
                    <EditRowStyle CssClass="EditRowStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <EmptyDataTemplate>
                       <asp:Literal ID="notAssociatedFacet" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextNotAssociatedFacet %>'></asp:Literal>  
                    <%=ItemType.Replace("Id","") %>
                    </EmptyDataTemplate>
                </asp:GridView>
            </div>
        </asp:Panel>


        <div>
        </div>

        <asp:Panel runat="server" ID="pnlEditFacets" Visible="false" CssClass="FormView">
            <h4 class="SubTitle">
                <asp:Label runat="server" ID="lblAddEdit"></asp:Label><asp:Literal ID="SelectFacetstoAssociate" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSelectFacetstoAssociate %>'></asp:Literal> </h4>

            <div>
                <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                    ForeColor="red"></asp:Label>
            </div>
            <znode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></znode:Spacer>
            <div class="FieldStyle">
             <asp:Label runat="server" ID="Label1"></asp:Label><asp:Literal ID="TitleFacetGroup" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFacetGroup %>'></asp:Literal> 
                   <br />
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlFacetGroups" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DdlFacetGroups_SelectedIndexChanged">
                </asp:DropDownList>
            </div>
            <div class="FieldStyle">
               <asp:Literal ID="Facets" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFacets %>'></asp:Literal> 
                 <br />
            </div>
            <div class="ValueStyle">
                <asp:CheckBoxList ID="chklstFacets" RepeatColumns="5" RepeatLayout="Table" RepeatDirection="Horizontal"
                    runat="server">
                </asp:CheckBoxList>
                <asp:Label runat="server" ID="lblNoFacets" Visible="false" Text='<%$ resources:ZnodeAdminResource,RecordNotFoundFacet %>'> </asp:Label>
            </div>

            <znode:Spacer ID="Spacer1" runat="server" SpacerHeight="10" />
            <div class="ClearBoth">
                <br />
            </div>
            <div> 
               <zn:Button ID="btnSubmitBottom" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="Update_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
       
               <zn:Button ID="btnCancelBottom" runat="server" ButtonType="CancelButton"  CausesValidation="True" OnClick="Cancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
         
            </div>
            <znode:Spacer ID="Spacer4" runat="server" SpacerHeight="10" />
        </asp:Panel>

    </ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="uxFacetUpdateProgress" runat="server" AssociatedUpdatePanelID="upProductFacets" DisplayAfter="10">
    <ProgressTemplate>
        <div id="ajaxProgressBg"></div>
        <div id="ajaxProgress"></div>
    </ProgressTemplate>
</asp:UpdateProgress>

 <script language="javascript" type="text/javascript">
     function DeleteConfirmation() {
         return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ConfirmDelete")) %>');
     }
  </script>