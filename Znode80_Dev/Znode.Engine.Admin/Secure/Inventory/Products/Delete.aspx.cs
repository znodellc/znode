using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin Admin_Secure_products_delete class
    /// </summary>
    public partial class Delete : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int ItemId;
        private string _ProductName = string.Empty;
        private string CancelLink = "Default.aspx";
        #endregion

        /// <summary>
        /// Gets or sets the product name
        /// </summary>
        public string ProductName
        {
            get { return _ProductName; }
            set { _ProductName = value; }
        }

        #region Bind Data

        /// <summary>
        /// Bind the data for a Product id
        /// </summary>
        public void BindData()
        {
            ProductAdmin _ProductAdmin = new ProductAdmin();
            Product ProductList = _ProductAdmin.GetByProductId(this.ItemId);
            if (ProductList != null)
            {
                this.ProductName = ProductList.Name;
            }
        }
        #endregion

        #region General Events

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelLink);
        }

        /// <summary>
        /// Delete button click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnDelete_Click(object sender, EventArgs e)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            Product product = productAdmin.GetByProductId(this.ItemId);
            this.ProductName = product.Name;
            bool Retvalue = productAdmin.DeleteByProductID(this.ItemId);

            if (Retvalue)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, "Delete Product - " + this.ProductName, this.ProductName);

                Response.Redirect(this.CancelLink);
            }
            else
            {
                lblErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductCannotDelete").ToString();
            }
        }
        #endregion

        #region Page Load

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring   
            if (Request.Params["Itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["Itemid"].ToString());
                this.BindData();
            }
            else
            {
                this.ItemId = 0;
            }
        }
        #endregion
    }
}