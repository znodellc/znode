﻿<%@ Page Title="Manage Products - Add Product Category" Language="C#" MasterPageFile="~/Themes/Standard/edit.master"
    AutoEventWireup="true" CodeBehind="AddProductCategory.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Inventory.Products.AddProductCategory" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="server">
    <div>
        <div>
            <div class="LeftFloat" style="width: 75%">
                <h1>
                    <asp:Localize ID="TitleAddProductCategory" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleAddProductCategory%>'></asp:Localize>
                    <asp:Label ID="lblProdName" runat="server" Text="Label"></asp:Label>
                </h1>
            </div>
            <br />
            <div class="LeftFloat" style="width: 18%" align="right">
                <zn:Button runat="server" ButtonType="EditButton" Width="200px" OnClick="BtnBack_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonBackProductDetail%>' ID="btnBack" />
            </div>
        </div>

        <!-- Search product panel -->
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <h4 class="SubTitle">
                    <asp:Localize ID="SubTitleSearchCategory" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchCategory%>'></asp:Localize></h4>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleCategory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCategory%>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtDepartmentName" runat="server"></asp:TextBox></span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <br />
                </div>
                <div>
                    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear%>' CausesValidation="False" ID="btnClear" />
                    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch%>' CausesValidation="true" ID="btnSearch" />
                </div>
            </div>
            <div style="margin-bottom: 10px;"></div>
        </asp:Panel>
        <!-- Department List -->
        <asp:Panel ID="pnlDepartmentList" runat="server" Visible="false">
            <h4 class="GridTitle">
                <asp:Localize ID="ColumnPostalCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleCategoryList%>'></asp:Localize></h4>
            <br />
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
                GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
                OnRowDataBound="UxGrid_RowDataBound" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundProductCategory%>'
                Width="100%" CellPadding="4">
                <Columns>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSelect%>' ItemStyle-Width="50" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkCategory" runat="server" />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="CategoryId" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID%>' HeaderStyle-HorizontalAlign="Left">
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName%>' HeaderStyle-HorizontalAlign="Left">
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Title" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitle%>' HeaderStyle-HorizontalAlign="Left">
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                    ForeColor="red"></asp:Label>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div class="LeftFloat">
                <zn:Button runat="server" Width="200px" ButtonType="EditButton" OnClick="Update_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonAddSelectedCategories%>' ID="Button2" />
            </div>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
             <zn:Button runat="server" ButtonType="CancelButton" OnClick="Cancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelBottom" />

        </asp:Panel>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
    </div>
</asp:Content>
