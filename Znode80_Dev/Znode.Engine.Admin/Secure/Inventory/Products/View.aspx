<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True"
    Inherits="Znode.Engine.Admin.Secure.Inventory.Products.View" ValidateRequest="false" Title="Manage Products - View" CodeBehind="View.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register TagPrefix="uc1" TagName="ProductFacets" Src="ProductFacets.ascx" %>
<%@ Register TagPrefix="uc2" TagName="ProductTags" Src="ProductTags.ascx" %>
<%@ Register TagPrefix="uc3" TagName="ProductCustomerPricing" Src="~/Controls/Default/Product/ProductCustomerBasedPricing.ascx" %>
<%@ Register TagPrefix="ajaxToolKit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=3.0.30512.20315, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">

    <script type="text/javascript">
        function ActiveTabChanged(sender, e) {
            var Tab = document.getElementById('<%=tabProductDetails.ClientID%>');
		    var activeTabIndex = document.getElementById('<%=hdnActiveTabIndex.ClientID%>');
		    activeTabIndex.value = sender.get_activeTabIndex();
		}
    </script>
    <script language="javascript" type="text/javascript">
        function DeleteConfirmation() {
            return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","ConfirmDelete")) %>');
        }
    </script>
    <script language="javascript" type="text/javascript">
        function ImageDeleteConfirmation() {
            return confirm('<%= Convert.ToString(GetGlobalResourceObject("ZnodeAdminResource","TextDeleteConfirmProductImage")) %>');
        }
    </script>




    <div>
        <div class="LeftFloat" style="width: 70%;">
            <h1>
                <asp:Localize ID="ProductDetails" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleProductDetails %>'></asp:Localize>
                - <%= lblProdName.Text.Trim() %></h1>
        </div>
        <div class="LeftFloat" style="width: 29%" align="right">
            <zn:Button runat="server" ID="btnBack" CausesValidation="False"  ButtonType="EditButton" OnClick="BtnBack_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonProductList %>' Width="150px"/>
        </div>
    </div>
    <div class="ClearBoth" align="left">
        <hr />
    </div>
    <asp:HiddenField ID="hdnActiveTabIndex" runat="server" Value="0" />
    <div>
        <uc1:Spacer ID="Spacer8" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <ajaxToolKit:TabContainer ID="tabProductDetails" OnClientActiveTabChanged="ActiveTabChanged"
        TabStripPlacement="Top" runat="server" ActiveTabIndex="11">

        <ajaxToolKit:TabPanel ID="pnlGeneral" runat="server">

            <HeaderTemplate>
                <asp:Localize ID="ProductInfo" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleProductInfo %>'></asp:Localize>
            </HeaderTemplate>

            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer7" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div style="width: 100%; margin-bottom: 20px;">
                    <div style="float: left;">
                       <zn:Button runat="server" Width="150px" OnClick="EditProduct_Click" ID="EditProduct" ButtonType="EditButton" Text='<%$ Resources:ZnodeAdminResource, ButtonEditInformation %>' />
                    </div>
                    <script type="text/javascript" language="javascript">
                        var $j = jQuery.noConflict();

                        var isAuthenticated = function () {
                            var retval = false;

                            $j.ajax({
                                url: "/plugins/x-commerce/admin/Account/IsAuthenticated",
                                type: "POST",
                                success: function (data) {
                                    retval = data.IsAuthenticated;
                                },
                                async: false
                            });

                            return retval;
                        };

                        var refreshProductDetailsPage = function () {
                            if (window.opener && !window.opener.closed) {
                                window.opener.location.reload(true);
                            }
                        };

                        var stateChanged = false;

                        $j(document).ready(function () {

                            var $marketplaceModal = $find("marketplaceModal");
                            var $marketplaceIFrame = $j("#AccountFrame");

                            $j("div.btn-group ul.dropdown-menu li a").live("click", function (evt) {
                                evt.preventDefault();
                                if (isAuthenticated()) {
                                    var src;
                                    src = $j(evt.target).attr("href");
                                    $marketplaceIFrame.attr("src", src);
                                    $marketplaceModal.show();
                                }
                                else {
                                    refreshProductDetailsPage();
                                }
                            });

                            $j("button#closeMarketplaceAccountModal").live("click", function (evt) {
                                evt.preventDefault();
                                if (stateChanged) {
                                    refreshProductDetailsPage();
                                }
                                else {
                                    $marketplaceModal.hide();
                                    $marketplaceIFrame.attr("src", "");
                                }
                            });
                        });
                    </script>
                    <div id="btnXCommerceAdmin" runat="server" clientidmode="Static">
                    </div>
                </div>
                <zn:Button ID="btnShowMarketplaceModal" runat="server" Style="display: none" ClientIDMode="Static" />
                <ajaxToolKit:ModalPopupExtender ID="marketplaceModal" runat="server" TargetControlID="btnShowMarketplaceModal" PopupControlID="pnlMarketplaceActions" BackgroundCssClass="modalBackground" ClientIDMode="Static" />
                <asp:Panel ID="pnlMarketplaceActions" runat="server" Style="display: none;" CssClass="PopupStyle" Width="60%" Height="65%">
                    <div style="width: 100%; height: 98%;">
                        <button class="close" id="closeMarketplaceAccountModal" style="float: right;">x</button>
                        <iframe src="" clientidmode="Static" runat="server" id="AccountFrame" style="border: 0px; border-top: 1px solid #EEE; width: 100%; height: 95%; margin-top: 1%;" marginheight="0" marginwidth="0" frameborder="0"></iframe>
                    </div>
                </asp:Panel>
                <br />
                <h4 class="SubTitle">
                    <asp:Localize ID="GeneralInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralInformation%>'></asp:Localize></h4>
                <div class="Display">
                    <div class="FieldStyleA">
                        <asp:Localize ID="ProductName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductName%>'></asp:Localize>
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblProdName" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="BrandName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBrandName%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblBrandName" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        <asp:Localize ID="ProductCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductCode%>'></asp:Localize>
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblProdNum" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="MinSelectableQuantity" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnMinSelectableQuantity%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblMinQuantity" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        <asp:Localize ID="MaxSelectableQuantity" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnMaxSelectableQuantity%>'></asp:Localize>
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblMaxQuantity" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="ProductType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductType%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblProdType" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div runat="server" id="divExpirationPeriod" visible="false">
                        <div class="FieldStyle">
                            <asp:Localize ID="ExpirationPeriod" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnExpirationPeriod%>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:Label ID="lblExpirationPeriod" runat="server"></asp:Label>&nbsp;
                        </div>
                    </div>
                    <div class="FieldStyleA">
                        <asp:Localize ID="SupplierName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSupplierName%>'></asp:Localize>
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblSupplierName" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="RetailPrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRetailPrice%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblRetailPrice" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        <asp:Localize ID="SalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSalePrice%>'></asp:Localize>
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblSalePrice" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="WholeSalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleWholesalePrice%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblWholeSalePrice" runat="server" CssClass="Price"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        <asp:Localize ID="TaxClass" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTaxClass%>'></asp:Localize>
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblTaxClass" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="DownloadLink" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDownloadLink%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblDownloadLink" runat="server" Text="Label"></asp:Label>&nbsp;
                    </div>
                    <div class="ClearBoth">
                        <br />
                    </div>
                    <h4 class="SubTitle">
                        <asp:Localize ID="DisplaySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDisplaySettings%>'></asp:Localize></h4>
                    <div class="FieldStyle">
                        <asp:Localize ID="DisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblProdDisplayOrder" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="ClearBoth">
                        <br />
                    </div>
                    <h4 class="SubTitle">
                        <asp:Localize ID="ShippingSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleShippingSettings%>'></asp:Localize></h4>
                    <div class="FieldStyleA">
                        <asp:Localize ID="FreeShipping" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFreeShipping%>'></asp:Localize>
                    </div>
                    <div class="ValueStyleA">
                        <img id="freeShippingInd" runat="server" />
                    </div>
                    <div class="FieldStyleA" style="display: none">
                        <asp:Localize ID="ShipSeparately" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShipSeparately%>'></asp:Localize>
                    </div>
                    <div class="ValueStyleA" style="display: none">
                        <img id="shipSeparatelyInd" runat="server" />
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="ShippingRule" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingRule%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblShippingRuleTypeName" runat="server" />&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        <asp:Localize ID="ShippingRate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingRate%>'></asp:Localize>
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblShippingRate" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="Weight" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWeight%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblWeight" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        <asp:Localize ID="Height" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnHeight%>'></asp:Localize>
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblHeight" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="Width" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWidth%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblWidth" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        <asp:Localize ID="Length" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLength%>'></asp:Localize>
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblLength" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="ClearBoth">
                        <br />
                    </div>
                    <h4 class="SubTitle">
                        <asp:Localize ID="ProductImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductImage%>'></asp:Localize></h4>
                    <div class="Image">
                        <asp:Image ID="ItemImage" runat="server" />
                    </div>
                    <h4 class="SubTitle">
                        <asp:Localize ID="ShortDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShortDescription%>'></asp:Localize></h4>
                    <div class="ShortDescription">
                        <asp:Label ID="lblShortDescription" runat="server"></asp:Label>
                    </div>
                    <br />
                    <h4 class="SubTitle">
                        <asp:Localize ID="ProductDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleProductDescription%>'></asp:Localize></h4>
                    <div class="Description">
                        <asp:Label ID="lblProductDescription" runat="server"></asp:Label>
                    </div>
                    <div>
                        <uc1:Spacer ID="Spacer2" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                </div>
                <h4 class="SubTitle">
                    <asp:Localize ID="ProductFeatures" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductFeatures%>'></asp:Localize></h4>
                <div class="Features">
                    <asp:Label ID="lblProductFeatures" runat="server"></asp:Label>
                </div>
                <br />
                <h4 class="SubTitle">
                    <asp:Localize ID="ProductSpecifications" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductSpecifications%>'></asp:Localize></h4>
                <div class="Features">
                    <asp:Label ID="lblproductspecification" runat="server"></asp:Label>
                </div>
                <br />
                <h4 class="SubTitle">
                    <asp:Localize ID="ShippingInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingInformation%>'></asp:Localize></h4>
                <div class="Features">
                    <asp:Label ID="lbladditionalinfo" runat="server"></asp:Label>
                </div>

            </ContentTemplate>

        </ajaxToolKit:TabPanel>

        <ajaxToolKit:TabPanel ID="pnlAdvancedSettings" runat="server">

            <HeaderTemplate>
                <asp:Localize ID="Settings" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleSettings %>'></asp:Localize>
            </HeaderTemplate>

            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer9" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div align="left">
                     <zn:Button runat="server" ID="Button2" OnClick="EditAdvancedSettings_Click" ButtonType="EditButton" Width="150px" Text='<%$ Resources:ZnodeAdminResource, ButtonEditSettings %>' />
                </div>
                <br />
                <h4 class="SubTitle">
                    <asp:Localize ID="DisplaySetting" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDisplaySettings%>'></asp:Localize></h4>
                <div class="Display">
                    <tr class="RowStyle">
                        <div class="FieldStyleImg">
                            <asp:Localize ID="Enabled" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleEnabled%>'></asp:Localize>
                        </div>
                        <div class="ValueStyleImg">
                            <img id="chkProductEnabled" runat="server" alt="" src="" />
                        </div>
                        <div class="FieldStyleImgA">
                            <asp:Localize ID="HomePageSpecial" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleHomePageSpecial%>'></asp:Localize>
                        </div>
                        <div class="ValueStyleImgA">
                            <img id="chkIsSpecialProduct" runat="server" alt="" src="" />
                        </div>
                        <div class="FieldStyleImg">
                            <asp:Localize ID="NewItem" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleNewItem%>'></asp:Localize>
                        </div>
                        <div class="ValueStyleImg">
                            <img id="chkIsNewItem" runat="server" alt="" src="" />
                        </div>
                        <div class="FieldStyleImgA">
                            <asp:Localize ID="FeaturedItem" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFeaturedItem%>'></asp:Localize>
                        </div>
                        <div class="ValueStyleImgA">
                            <img id="FeaturedProduct" runat="server" alt="" src="" />
                        </div>
                        <div class="FieldStyleImg">
                            <asp:Localize ID="CallForPricing" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCallForPricing%>'></asp:Localize>
                        </div>
                        <div class="ValueStyleImg">
                            <img id="chkProductPricing" runat="server" alt="" src="" />
                        </div>
                        <div class="DisplayNone">
                            <asp:Localize ID="DisplayInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayInventory%>'></asp:Localize>
                        </div>
                        <div class="DisplayNone">
                            <img id="chkproductInventory" runat="server" alt="" src="" />
                        </div>
                        <asp:Panel ID="pnlFranchise" runat="server">
                            <div class="FieldStyleImgA">
                                <asp:Localize ID="Franchisable" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleFranchisable%>'></asp:Localize>
                            </div>
                            <div class="ValueStyleImgA">
                                <img id="chkFranchisable" runat="server" alt="" src="" />
                            </div>
                        </asp:Panel>
                        <div class="ClearBoth">
                            <br />
                        </div>
                        <h4 class="SubTitle">
                            <asp:Localize ID="InventorySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleInventorySettings%>'></asp:Localize></h4>
                        <div class="FieldStyleImg">
                            <img id="chkCartInventoryEnabled" runat="server" alt="" src='' />
                        </div>
                        <div class="ValueStyleImg">

                            <asp:Localize ID="rackInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTrackInventory%>'></asp:Localize>
                        </div>
                        <div class="FieldStyleImgA">
                            <img id="chkIsBackOrderEnabled" runat="server" alt="" src='' />
                        </div>
                        <div class="ValueStyleImgA">

                            <asp:Localize ID="BackOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBackOrder%>'></asp:Localize>
                        </div>
                        <div class="FieldStyleImg">
                            <img id="chkIstrackInvEnabled" runat="server" alt="" src="" />
                        </div>
                        <div class="ValueStyleImg">

                            <asp:Localize ID="NoTrackInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnNoTrackInventory%>'></asp:Localize>
                        </div>
                        <div class="FieldStyleA">
                            <asp:Localize ID="InStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleInStockMessage%>'></asp:Localize>
                        </div>
                        <div class="ValueStyleA">
                            <asp:Label ID="lblInStockMsg" runat="server"></asp:Label>&nbsp;
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="OutofStockMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOutofStockMessage%>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:Label ID="lblOutofStock" runat="server"></asp:Label>&nbsp;
                        </div>
                        <div class="FieldStyleA">
                            <asp:Localize ID="BackOrderMessage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBackOrderMessage%>'></asp:Localize>
                        </div>
                        <div class="ValueStyleA">
                            <asp:Label ID="lblBackOrderMsg" runat="server"></asp:Label>&nbsp;
                        </div>
                        <div class="FieldStyleImg" style="display: none;">
                            <asp:Localize ID="DropShip" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextDropShip%>'></asp:Localize>
                        </div>
                        <div class="ValueStyleImg" style="display: none;">
                            <img id="IsDropShipEnabled" runat="server" alt="" src='' />
                        </div>
                        <div class="ClearBoth">
                            <br />
                        </div>
                        <h4 class="SubTitle">
                            <asp:Localize ID="RecurringBillingSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleRecurringBillingSettings%>'></asp:Localize></h4>
                        <div class="FieldStyleImg">
                            <asp:Localize ID="RecurringBilling" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRecurringBilling%>'></asp:Localize>
                        </div>
                        <div class="ValueStyleImg">
                            <img id="imgRecurringBillingInd" runat="server" alt="" src='' />
                        </div>
                        <div class="FieldStyleA">
                            <asp:Localize ID="Amount" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleAmount%>'></asp:Localize>
                        </div>
                        <div class="ValueStyleA">
                            <asp:Label ID="lblRecurringBillingInitialAmount" runat="server"></asp:Label>&nbsp;
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="Period" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnPeriod%>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <asp:Label ID="lblBillingPeriod" runat="server"></asp:Label>&nbsp;
                        </div>
                        <div class="ClearBoth">
                            <br />
                        </div>
                </div>
                <h4 class="SubTitle">
                    <asp:Localize ID="SeoSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSeoSettings%>'></asp:Localize></h4>
                <div class="Display">
                    <div class="FieldStyle">
                        <asp:Localize ID="SEOTitle" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOTitle%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblSEOTitle" runat="server"></asp:Label>
                    </div>
                    <div class="FieldStyleA">
                        <asp:Localize ID="SEOKeywords" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOKeywords%>'></asp:Localize>
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblSEOKeywords" runat="server" CssClass="Price"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="SEODescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEODescription%>'></asp:Localize>
                    </div>
                    <div class="ValueStyle">
                        <asp:Label ID="lblSEODescription" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="FieldStyleA">
                        <asp:Localize ID="SEOURL" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSEOURL%>'></asp:Localize>
                    </div>
                    <div class="ValueStyleA">
                        <asp:Label ID="lblSEOURL" runat="server"></asp:Label>&nbsp;
                    </div>
                    <div class="ClearBoth">
                    </div>
                </div>

            </ContentTemplate>

        </ajaxToolKit:TabPanel>

        <ajaxToolKit:TabPanel ID="TabDepartments" runat="server">

            <HeaderTemplate>
                <asp:Localize ID="Categories" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleCategories %>'></asp:Localize>
            </HeaderTemplate>

            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer5" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div class="ButtonStyle">
                    <zn:LinkButton ID="btnAssociateDepartment" runat="server"
                        ButtonType="Button" OnClick="BtnAssociateDepartment_Click" Text='<%$ Resources:ZnodeAdminResource, LinkAssociateCategories %>'
                        ButtonPriority="Primary" />
                </div>
                <div>
                    <uc1:Spacer ID="Spacer10" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="gvProductDepartment" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                            runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                            CssClass="Grid" Width="100%" GridLines="None" AllowPaging="True" PageSize="5"
                            OnPageIndexChanging="GvProductDepartment_PageIndexChanging" OnRowCommand="GvProductDepartment_RowCommand">
                            <Columns>
                                <asp:BoundField DataField="ProductCategoryId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="Name" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName %>' HeaderStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                <asp:BoundField DataField="Title" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitle %>' HeaderStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkRemove %>' CommandArgument='<%# Eval("ProductCategoryId") %>'
                                            CommandName="RemoveItem" CssClass="actionlink" runat="server" OnClientClick="return DeleteConfirmation();" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <asp:Localize ID="CategoryData" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridEmptyCategoryData%>'></asp:Localize>
                            </EmptyDataTemplate>
                            <RowStyle CssClass="RowStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            <FooterStyle CssClass="FooterStyle" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </ContentTemplate>

        </ajaxToolKit:TabPanel>

        <ajaxToolKit:TabPanel ID="pnlManageinventory" runat="server">

            <HeaderTemplate>
                <asp:Localize ID="SKUs" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleSKUs %>'></asp:Localize>
            </HeaderTemplate>

            <ContentTemplate>
                <div class="Form">
                    <div>
                        <uc1:Spacer ID="Spacer15" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                    </div>
                    <asp:Panel ID="pnlSKUAttributes" runat="server">
                        <div class="ButtonStyle">
                            <zn:LinkButton ID="butAddNewSKU" runat="server"
                                ButtonType="Button" OnClick="BtnAddSKU_Click" Text='<%$ Resources:ZnodeAdminResource, LinkAddSKUorPart %>'
                                ButtonPriority="Primary" />
                        </div>

                        <div class="SearchForm">
                            <div class="SubTitle">
                                <asp:Localize ID="SearchSkus" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchSkus %>'></asp:Localize>
                            </div>
                            <div class="ValueStyle">
                                <asp:PlaceHolder ID="ControlPlaceHolder" runat="server"></asp:PlaceHolder>
                            </div>
                            <div>
                                <zn:Button ID="btnClear" runat="server" ButtonType="CancelButton" CausesValidation="false" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear %>' />

                                <zn:Button ID="btnSearch" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch %>' />


                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <h4 class="GridTitle">
                    <asp:Localize ID="CurrentInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleCurrentInventory %>'></asp:Localize>
                </h4>
                <asp:UpdatePanel ID="updPnlInventoryDisplayGrid" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="uxGridInventoryDisplay" Width="100%" CssClass="Grid" CellPadding="4"
                            CaptionAlign="Left" GridLines="None" runat="server" AutoGenerateColumns="False"
                            AllowPaging="True" ForeColor="Black" OnPageIndexChanging="UxGridInventoryDisplay_PageIndexChanging"
                            OnRowCommand="UxGridInventoryDisplay_RowCommand" OnRowDeleting="UxGridInventoryDisplay_RowDeleting"
                            PageSize="25">
                            <FooterStyle CssClass="FooterStyle" />
                            <Columns>
                                <asp:BoundField DataField="sku" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnSKU %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="skuid" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnQuantityOnHand %>'>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="quantity" Text='<%# GetSkuQuantity((int)Eval("skuid")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleReOrderLevel %>'>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="reorderlevel" Text='<%# GetSkuReorderLevel((int)Eval("skuid")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsActive %>' HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <img alt="" id="Img3" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                            runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div id="Test" runat="server">
                                            <asp:LinkButton CssClass="actionlink" ID="EditSKU" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' CommandArgument='<%# Eval("skuid") %>'
                                                CommandName="Edit" runat="Server" />
                                            <asp:LinkButton ID="RemoveSKU" CssClass="actionlink" Text='<%$ Resources:ZnodeAdminResource, LinkRemove %>' CommandArgument='<%# Eval("skuid") %>'
                                                CommandName="Delete" runat="Server" Visible='<%# HasAttributes  %>' OnClientClick="return DeleteConfirmation();" />
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="RowStyle" />
                            <EditRowStyle CssClass="EditRowStyle" />
                            <PagerStyle CssClass="PagerStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                        </asp:GridView>
                        <uc1:Spacer ID="Spacer3" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                        <asp:Label ID="lblSkuErrorMsg" runat="server" CssClass="Error"></asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </ContentTemplate>

        </ajaxToolKit:TabPanel>

        <ajaxToolKit:TabPanel ID="pnlBundleProduct" runat="server">

            <HeaderTemplate>
                <asp:Localize ID="Bundles" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleBundles%>'></asp:Localize>
            </HeaderTemplate>

            <ContentTemplate>
                <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
                <div class="ButtonStyle">
                    <zn:LinkButton ID="AddBundleProducts" runat="server"
                        ButtonType="Button" OnClick="AddBundleProducts_Click" CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, LinkAssociateProduct %>' ButtonPriority="Primary" />
                </div>

                <br />
                <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                <%--  <asp:UpdatePanel ID="updPnlBundleProductGrid" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>--%>
                <asp:GridView ID="uxBundleProductGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                    runat="server" CellPadding="4" AutoGenerateColumns="False" CssClass="Grid" Width="100%"
                    OnPageIndexChanging="UxBundleProductGrid_PageIndexChanging" OnRowCommand="UxBundleProductGrid_RowCommand"
                    OnRowDataBound="UxBundleProductGrid_RowDataBound" OnRowDeleting="UxBundleProductGrid_RowDeleting"
                    GridLines="None" AllowPaging="True" PageSize="10">
                    <Columns>
                        <asp:BoundField DataField="ChildProductId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="lblProductName" runat="server" Text='<%# GetProductName(Eval("ChildProductId")) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsActive %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <img alt="" id="Img1" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(GetProductIsActive(Eval("ChildProductId")))%>'
                                    runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkRemove %>' CommandArgument='<%# Eval("ParentChildProductId") %>'
                                    CommandName="RemoveItem" CssClass="actionlink" runat="server" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>
                        <asp:Localize ID="GridBundleEmptyData" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridBundleEmptyData %>'></asp:Localize>
                    </EmptyDataTemplate>
                    <RowStyle CssClass="RowStyle" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <FooterStyle CssClass="FooterStyle" />
                </asp:GridView>
                <%--   </ContentTemplate>
                </asp:UpdatePanel>--%>
            </ContentTemplate>

        </ajaxToolKit:TabPanel>

        <ajaxToolKit:TabPanel ID="pnlFacets" runat="server">

            <HeaderTemplate>
                <asp:Localize ID="tabFacets" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleFacets%>'></asp:Localize>
            </HeaderTemplate>

            <ContentTemplate>
                <uc1:ProductFacets runat="server" />
            </ContentTemplate>

        </ajaxToolKit:TabPanel>

        <ajaxToolKit:TabPanel ID="pnlTags" runat="server">

            <HeaderTemplate>
                <asp:Localize ID="Tags" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleTags%>'></asp:Localize>
            </HeaderTemplate>

            <ContentTemplate>
                <uc2:ProductTags runat="server"></uc2:ProductTags>
            </ContentTemplate>

        </ajaxToolKit:TabPanel>

        <ajaxToolKit:TabPanel runat="server" ID="pnlCustomeBasedPricing">

            <HeaderTemplate>
                <asp:Localize ID="CustomerBasedPricing" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleCustomerBasedPricing%>'></asp:Localize>
            </HeaderTemplate>

            <ContentTemplate>
                <uc3:ProductCustomerPricing runat="server" />
            </ContentTemplate>

        </ajaxToolKit:TabPanel>

        <ajaxToolKit:TabPanel ID="pnlAlternateImages" runat="server">

            <HeaderTemplate>
                <asp:Localize ID="Images" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleImages%>'></asp:Localize>
            </HeaderTemplate>

            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer13" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div class="ButtonStyle">
                    <zn:LinkButton ID="Button1" runat="server"
                        ButtonType="Button" OnClick="AddProductView_Click" Text='<%$ Resources:ZnodeAdminResource, LinkTextAddAlternateProductImage%>'
                        ButtonPriority="Primary" />
                </div>
                <div>
                    <uc1:Spacer ID="Spacer23" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                <asp:UpdatePanel ID="updPnlGridThumb" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="GridThumb" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                            runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                            CssClass="Grid" Width="100%" GridLines="None" OnRowCommand="GridThumb_RowCommand"
                            AllowPaging="True" OnPageIndexChanging="GridThumb_PageIndexChanging" OnRowDeleting="GridThumb_RowDeleting"
                            PageSize="5">
                            <Columns>
                                <asp:BoundField DataField="productimageid" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText="Image" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <img id="SwapImage" alt="" src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                                            runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="Name" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnName %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="ImageTypeName" HeaderText="Image Type" HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProductPage %>' HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <img id="Img1" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                            runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleCategoryPage %>' HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <img id="Img2" alt="" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ShowOnCategoryPage").ToString()))%>'
                                            runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="DisplayOrder" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDisplayOrder %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton CssClass="actionlink" ID="EditProductView" Text='<%$ Resources:ZnodeAdminResource, LinkEdit%>' CommandArgument='<%# Eval("productimageid") %>'
                                            CommandName="Edit" runat="Server" />&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:LinkButton ID="Delete" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>' CommandArgument='<%# Eval("productimageid") %>'
                                            CommandName="RemoveItem" CssClass="actionlink" runat="server" OnClientClick="return ImageDeleteConfirmation();" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataTemplate>
                                <asp:Localize ID="GridImageEmptyData" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridImageEmptyData%>'></asp:Localize>
                            </EmptyDataTemplate>
                            <RowStyle CssClass="RowStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            <FooterStyle CssClass="FooterStyle" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </ContentTemplate>

        </ajaxToolKit:TabPanel>

        <ajaxToolKit:TabPanel ID="pnlProductOptions" runat="server">

            <HeaderTemplate>
                <asp:Localize ID="AddOns" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleAddOns%>'></asp:Localize>
            </HeaderTemplate>

            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer14" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div>
                    <div class="TabDescription" style="width: 80%;">
                        <p>
                            <asp:Localize ID="TextAddonProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextAddonProduct %>'></asp:Localize>
                        </p>
                    </div>
                    <div class="ButtonStyle" style="clear: right; float: right; padding-top: 10px; padding-right: 10px;">
                        <zn:LinkButton ID="btnAddNewAddOn" runat="server"
                            ButtonType="Button" OnClick="BtnAddNewAddOn_Click" Text='<%$ Resources:ZnodeAdminResource, LinkButtonAssociateAddOns %>'
                            ButtonPriority="Primary" />
                    </div>
                </div>
                <div class="ClearBoth"></div>
                <div>
                    <uc1:Spacer ID="Spacer19" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                <asp:UpdatePanel ID="updPnlProductAddOnsGrid" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView OnRowDataBound="UxGridProductAddOns_RowDataBound" ID="uxGridProductAddOns"
                            runat="server" CssClass="Grid" AllowPaging="True" AutoGenerateColumns="False"
                            CellPadding="4" GridLines="None" OnPageIndexChanging="UxGridProductAddOns_PageIndexChanging"
                            CaptionAlign="Left" OnRowCommand="UxGridProductAddOns_RowCommand" Width="100%"
                            EnableSortingAndPagingCallbacks="False" PageSize="15" AllowSorting="True" EmptyDataText='<%$ Resources:ZnodeAdminResource, RecordNotFoundAddOn %>'>
                            <Columns>
                                <asp:BoundField DataField="ProductAddOnId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnName %>' HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# GetAddOnName(Eval("AddonId")) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitle %>' HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# GetAddOnTitle(Eval("AddonId")) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton CommandName="Remove" CausesValidation="false" ID="btnDelete" runat="server"
                                            Text='<%$ Resources:ZnodeAdminResource, LinkRemove %>' CssClass="actionlink" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle CssClass="FooterStyle" />
                            <RowStyle CssClass="RowStyle" />
                            <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </ContentTemplate>


        </ajaxToolKit:TabPanel>

        <ajaxToolKit:TabPanel ID="pnlTieredPricing" runat="server">

            <HeaderTemplate>
                <asp:Localize ID="TieredPricing" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleTieredPricing%>'></asp:Localize>
            </HeaderTemplate>

            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer16" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div>
                    <div class="TabDescription" style="width: 80%;">
                        <p>
                            <asp:Localize ID="textTieredPricing" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextTieredPricing %>'></asp:Localize>
                        </p>
                    </div>
                    <div class="ButtonStyle" style="float: right; padding-top: 10px; padding-right: 10px;">
                        <zn:LinkButton ID="AddTieredPricing" runat="server"
                            ButtonType="Button" OnClick="AddTieredPricing_Click" Text='<%$ Resources:ZnodeAdminResource, LinkButtonAddPricingTier %>'
                            ButtonPriority="Primary" />
                    </div>
                </div>
                <div class="ClearBoth"></div>
                <div>
                    <uc1:Spacer ID="Spacer20" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <!-- Update Panel for grid paging that are used to avoid the postbacks -->
                <asp:UpdatePanel ID="updPnlTieredPricingGrid" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="uxGridTieredPricing" Width="100%" CssClass="Grid" CellPadding="4"
                            CaptionAlign="Left" GridLines="None" runat="server" AutoGenerateColumns="False"
                            AllowPaging="True" ForeColor="Black" OnPageIndexChanging="UxGridTieredPricing_PageIndexChanging"
                            OnRowDataBound="UxGridTieredPricing_RowDataBound" OnRowCommand="UxGridTieredPricing_RowCommand"
                            OnRowDeleting="UxGridTieredPricing_RowDeleting" PageSize="25" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridTieredPriceEmptyData %>'>
                            <FooterStyle CssClass="FooterStyle" />
                            <Columns>
                                <asp:BoundField DataField="ProductTierID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnProfileName %>' HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# GetProfileName(DataBinder.Eval(Container.DataItem, "ProfileID"))%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="TierStart" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleTierStart %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="TierEnd" HeaderText='<%$ Resources:ZnodeAdminResource, ColumntitleTierEnd %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePrice %>' HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# DataBinder.Eval(Container.DataItem,"Price","{0:c}") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <div id="Div1" runat="server">
                                            <asp:LinkButton CssClass="actionlink" ID="EditTieredPricing" Text='<%$ Resources:ZnodeAdminResource, LinkEdit %>' CommandArgument='<%# Eval("ProductTierID") %>'
                                                CommandName="Edit" runat="Server" />
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnRemoveTieredPricing" CssClass="actionlink" Text='<%$ Resources:ZnodeAdminResource, LinkDelete %>'
                                            CommandArgument='<%# Eval("ProductTierID") %>' CommandName="Delete" runat="Server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="RowStyle" />
                            <EditRowStyle CssClass="EditRowStyle" />
                            <PagerStyle CssClass="PagerStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </ContentTemplate>

        </ajaxToolKit:TabPanel>

        <ajaxToolKit:TabPanel ID="pnlHighlights" runat="server">

            <HeaderTemplate>
                <asp:Localize ID="Highlights" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleHighlights%>'></asp:Localize>
            </HeaderTemplate>

            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer17" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>


                </div>

                <div>
                    <div class="TabDescription" style="width: 75%;">
                        <p>
                            <asp:Localize ID="TextHighlight" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextHighlight%>'></asp:Localize>
                        </p>
                    </div>
                    <div class="NewButtonStyle">
                        <zn:LinkButton ID="btnAddNewHighlight" runat="server"
                            ButtonType="Button" OnClick="BtnAddHighlight_Click" Text='<%$ Resources:ZnodeAdminResource, LinkButtonAssociateHighlight%>'
                            ButtonPriority="Primary" />


                    </div>
                </div>
                <div class="ClearBoth"></div>
                <div>
                    <uc1:Spacer ID="Spacer22" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>


                </div>

                <uc1:Spacer ID="Spacer12" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>


                <div>
                    <asp:UpdatePanel ID="updPnlHighlights" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="uxGridHighlights" Width="100%" CssClass="Grid" CellPadding="4"
                                CaptionAlign="Left" GridLines="None" runat="server" AutoGenerateColumns="False"
                                AllowPaging="True" ForeColor="Black" OnPageIndexChanging="UxGridHighlights_PageIndexChanging"
                                OnRowDataBound="UxGridHighlights_RowDataBound" OnRowDeleting="UxGridHighlights_RowDeleting"
                                OnRowCommand="UxGridHighlights_RowCommand" PageSize="25" EmptyDataText="No highlights found for this Product.">
                                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                                <Columns>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Localize ID="TitleID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleID%>'></asp:Localize>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem,"HighlightId")%>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Localize ID="ColumnName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnName%>'></asp:Localize>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem,"Name") %>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="ImageTypeName" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleType%>'>
                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <HeaderTemplate>
                                            <asp:Localize ID="ColumnName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder%>'></asp:Localize>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <%# DataBinder.Eval(Container.DataItem,"DisplayOrder")%>
                                        </ItemTemplate>

                                        <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="DeleteHighlight" CssClass="actionlink" Text='<%$ Resources:ZnodeAdminResource, LinkRemove%>' CommandArgument='<%# Eval("ProductHighlightID") %>'
                                                CommandName="Delete" runat="Server" />

                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>

                                <EditRowStyle CssClass="EditRowStyle" />

                                <FooterStyle CssClass="FooterStyle" />

                                <HeaderStyle CssClass="HeaderStyle" />

                                <PagerStyle CssClass="PagerStyle" />

                                <RowStyle CssClass="RowStyle" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>

            </ContentTemplate>

        </ajaxToolKit:TabPanel>

        <ajaxToolKit:TabPanel ID="pnlDigitalAsset" runat="server">

            <HeaderTemplate>
                <asp:Localize ID="DigitalAssets" runat="server" Text='<%$ Resources:ZnodeAdminResource, TabTitleDigitalAssets%>'></asp:Localize>
            </HeaderTemplate>

            <ContentTemplate>
                <div>
                    <uc1:Spacer ID="Spacer18" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                </div>
                <div>
                    <div class="TabDescription" style="width: 75%;">
                        <p>
                            <asp:Localize ID="TextDigitalAssets" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextDigitalAssets %>'></asp:Localize>
                            <br />
                            <asp:Localize ID="TextDigitalAssetsProducts" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextDigitalAssetsProducts %>'></asp:Localize>
                        </p>
                    </div>
                    <div class="NewButtonStyle">
                        <zn:LinkButton ID="btnAddDigitalAsset" runat="server"
                            ButtonType="Button" OnClick="BtnAddDigitalAsset_Click" Text='<%$ Resources:ZnodeAdminResource, LinkButtonAddDigitalAsset %>'
                            ButtonPriority="Primary" />
                    </div>
                </div>
                <div class="ClearBoth"></div>
                <uc1:Spacer ID="Spacer11" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
                <asp:UpdatePanel ID="updPnlDigitalAsset" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="uxGridDigitalAsset" Width="100%" CssClass="Grid" CellPadding="4"
                            CaptionAlign="Left" GridLines="None" runat="server" AutoGenerateColumns="False"
                            AllowPaging="True" ForeColor="Black" OnPageIndexChanging="UxGridDigitalAsset_PageIndexChanging"
                            OnRowCommand="UxGridDigitalAsset_RowCommand"
                            OnRowDataBound="UxGridDigitalAsset_RowDataBound"
                            OnRowDeleting="UxGridDigitalAsset_RowDeleting" PageSize="25" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridDigitalAssetsEmptyData %>'>
                            <FooterStyle CssClass="FooterStyle" />
                            <Columns>
                                <asp:BoundField DataField="DigitalAssetID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="DigitalAsset" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnDigitalAsset %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="70%" />
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleAssigned %>' HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <img alt="" id="Img2" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(IsDigitalAssetAssigned(DataBinder.Eval(Container.DataItem, "OrderLineItemId")))%>'
                                            runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnRemoveDigitalAsset" CssClass="actionlink" Text='<%$ Resources:ZnodeAdminResource, LinkRemove %>'
                                            CommandArgument='<%# Eval("DigitalAssetID") %>' CommandName="Delete" runat="Server"
                                            Visible='<%# !(IsDigitalAssetAssigned(DataBinder.Eval(Container.DataItem, "OrderLineItemId")))%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <RowStyle CssClass="RowStyle" />
                            <EditRowStyle CssClass="EditRowStyle" />
                            <PagerStyle CssClass="PagerStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>

            </ContentTemplate>

        </ajaxToolKit:TabPanel>

    </ajaxToolKit:TabContainer>
</asp:Content>
