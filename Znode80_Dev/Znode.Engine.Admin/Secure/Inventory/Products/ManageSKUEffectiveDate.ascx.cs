﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.DataAccess.Data;
using ZNode.Libraries.Framework.Business;
using System.Data;
using Znode.Engine.Common;

namespace  Znode.Engine.Admin.Secure.Inventory.Products
{
    public partial class ManageSKUEffectiveDate : System.Web.UI.UserControl
    {
        #region Properties

        private int SkuProfileEffectiveId
        {
            get
            {
                if (ViewState["SkuProfileEffectiveId"] != null)
                {
                    return Convert.ToInt32(ViewState["SkuProfileEffectiveId"]);
                }

                return 0;
            }

            set
            {
                ViewState["SkuProfileEffectiveId"] = value;
            }
        }

        protected int SkuID
        {
            get
            {
                int skuId = 0;
                if (Request.QueryString["skuid"] != null)
                {
                    int.TryParse(Request.QueryString["skuid"], out skuId);
                }
                return skuId;
            }
        }

        #endregion

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            this.pnlSkuProfile.Visible = SkuID > 0;

            if (!this.IsPostBack && SkuID > 0)
            {
                BindGridData();
            }

        }

        #endregion

        #region Control Events

        protected void Update_Click(object sender, EventArgs e)
        {
            SKUProfileEffective skuProfile = new SKUProfileEffective();
            if (this.SkuProfileEffectiveId > 0)
            {
                skuProfile = DataRepository.SKUProfileEffectiveProvider.GetBySkuProfileEffectiveID(this.SkuProfileEffectiveId);
            }

            skuProfile.SkuId = this.SkuID;
            skuProfile.ProfileId = Convert.ToInt32(this.ddlProfile.SelectedValue);
            skuProfile.EffectiveDate = Convert.ToDateTime(this.txtEffectiveDate.Text);
            DataRepository.SKUProfileEffectiveProvider.Save(skuProfile);

            // rebind the grid.
            uxGrid.EditIndex = -1;
            BindGridData();
            // this.pnlEditSkuProfile.Visible = false;
            ((Znode.Engine.Admin.Secure.Inventory.Products.AddSku)this.Page).ShowSubmitButton = true;

        }

        protected void Cancel_Click(object sender, EventArgs e)
        {

            uxGrid.EditIndex = -1;
            BindGridData();
            //this.pnlEditSkuProfile.Visible = false;
            ((Znode.Engine.Admin.Secure.Inventory.Products.AddSku)this.Page).ShowSubmitButton = true;


        }

        protected void Add_Click(object sender, EventArgs e)
        {

            lblError.Text = string.Empty;
            this.SkuProfileEffectiveId = 0;

            // this.pnlEditSkuProfile.Visible = true;
            if (BindProfile())
            {
                mdlPopup.Show();
                if (this.ddlProfile.Items.Count > 0)
                {
                    this.ddlProfile.SelectedIndex = 0;
                }

                this.BindEditData();
                mdlPopup.Show();
            }

        }

        protected void uxGrid_EditCommand(object sender, GridViewEditEventArgs e)
        {
            lblError.Text = string.Empty;
            this.SkuProfileEffectiveId = Convert.ToInt32(uxGrid.DataKeys[e.NewEditIndex].Value);
            this.pnlEditSkuProfile.Visible = true;
            BindProfile();
            BindEditData();
        }


        protected void uxGrid_DeleteCommand(object sender, GridViewDeleteEventArgs e)
        {
            this.SkuProfileEffectiveId = Convert.ToInt32(uxGrid.DataKeys[e.RowIndex].Value);
            DataRepository.SKUProfileEffectiveProvider.Delete(SkuProfileEffectiveId);
            BindGridData();
        }

        #endregion

        #region Helper Methods

        private void BindGridData()
        {
            SKUAdmin skuAdmin = new SKUAdmin();
            DataSet skuProfiles = skuAdmin.GetSkuProfileEffectiveBySkuID(SkuID);
            this.uxGrid.DataSource = skuProfiles;
            this.uxGrid.DataBind();
            uxGrid.EditIndex = -1;

        }

        private void BindEditData()
        {
            if (this.SkuProfileEffectiveId > 0)
            {
                lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTitleEditDetails").ToString();
                SKUProfileEffective skuProfile = DataRepository.SKUProfileEffectiveProvider.GetBySkuProfileEffectiveID(SkuProfileEffectiveId);
                this.ddlProfile.SelectedValue = skuProfile.ProfileId.ToString();
                this.txtEffectiveDate.Text = skuProfile.EffectiveDate.ToShortDateString();
                mdlPopup.Show();
            }
            else
            {
                lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "SubTitleAddDetails").ToString();
                this.txtEffectiveDate.Text = DateTime.Now.ToShortDateString();
            }

            ((Znode.Engine.Admin.Secure.Inventory.Products.AddSku)this.Page).ShowSubmitButton = false;
        }

        private bool BindProfile()
        {
            // Get profiles
            StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();
            TList<Profile> profiles = UserStoreAccess.CheckProfileAccess(settingsAdmin.GetProfiles());
            TList<SKUProfileEffective> skuProfile = DataRepository.SKUProfileEffectiveProvider.GetBySkuId(SkuID);
            skuProfile.ApplyFilter(delegate(SKUProfileEffective sp) { return sp.SkuProfileEffectiveID != SkuProfileEffectiveId; });

            profiles.ApplyFilter(delegate(Profile p)
            {
                return skuProfile.FirstOrDefault(delegate(SKUProfileEffective sp)
                {
                    return sp.ProfileId == p.ProfileID;
                }) == null;
            });
            this.ddlProfile.DataSource = profiles;
            this.ddlProfile.DataTextField = "Name";
            this.ddlProfile.DataValueField = "ProfileID";
            this.ddlProfile.DataBind();

            if (ddlProfile.Items.Count == 0)
            {
                lblError.Text =this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAlreadyAssociatedProfile").ToString();
                //pnlEditSkuProfile.Visible = false;
                return false;
            }

            return true;
        }

        #endregion


    }
}