using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Utilities;

namespace Znode.Engine.Admin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin Admin_Secure_catalog_product_addbundleproduct class
    /// </summary>
    public partial class AddBundleProduct : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int _ItemId = 0;
        private string ViewPage = "View.aspx?itemid=";
        #endregion

        /// <summary>
        /// Gets or sets the item Id.
        /// </summary>
        public int ItemId
        {
            get { return _ItemId; }
            set { _ItemId = value; }
        }

        #region Events

        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                this.BindData();
            }
        }

        /// <summary>
        /// Update Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Update_Click(object sender, EventArgs e)
        {
            this.RememberOldValues();
            ParentChildProductAdmin _ParentChildProductAdmin = new ParentChildProductAdmin();
            Dictionary<int, string> productIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];
            bool status = true;

            if (productIdList != null)
            {
                StringBuilder sb = new StringBuilder();
                StringBuilder productName = new StringBuilder();
                ProductAdmin _prodAdmin = new ProductAdmin();
                Product _prodEntity = _prodAdmin.GetByProductId(this.ItemId);

                foreach (KeyValuePair<int, string> pair in productIdList)
                {
                    // Get ProductId
                    int ChildProductId = pair.Key;

                    status = _ParentChildProductAdmin.Insert(this.ItemId, ChildProductId);

                    productName.Append(pair.Value + ",");

                    if (!status)
                    {
                        sb.Append(pair.Value + ",");
                        lblError.Visible = true;
                    }
                }

                Session.Remove("CHECKEDITEMS");

                if (sb.ToString().Length > 0)
                {
                    sb.Remove(sb.ToString().Length - 1, 1);

                    // Display Error message
                    lblError.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAssociateBundleProduct").ToString(),sb.ToString());

                    foreach (GridViewRow row in uxGrid.Rows)
                    {
                        CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProduct") as CheckBox;

                        if (check != null)
                        {
                            check.Checked = false;
                        }
                    }
                }
                else
                {
                    productName.Remove(productName.Length - 1, 1);

                    string AssociationName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAssociateBundleProduct").ToString(), productName, _prodEntity.Name);

                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, AssociationName, _prodEntity.Name);

                    Response.Redirect(this.ViewPage + this.ItemId + "&mode=bundle");
                }
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ViewPage + this.ItemId + "&mode=bundle");
        }

        /// <summary>
        /// Search Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindSearchProduct();
        }

        /// <summary>
        /// Clear Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddBundleProduct.aspx?itemid=" + this.ItemId);
        }

        /// <summary>
        /// Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.RememberOldValues();
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindSearchProduct();
        }

        /// <summary>
        /// Grid Row Data Bound Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Dictionary<int, string> productIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];

                if (productIdList != null)
                {
                    CheckBox check = (CheckBox)e.Row.Cells[0].FindControl("chkProduct") as CheckBox;
                    int id = int.Parse(e.Row.Cells[1].Text);

                    if (productIdList.ContainsKey(id) && check != null)
                    {
                        check.Checked = true;
                    }
                }
            }
        }

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.ViewPage + this.ItemId + "&mode=bundle");
        }
        #endregion

        #region Bind Methods

        /// <summary>
        /// Bind Data Method
        /// </summary>
        private void BindData()
        {
            CatalogAdmin catalogAdmin = new CatalogAdmin();
            TList<Catalog> catalogList = catalogAdmin.GetAllCatalogs();
            DataSet ds = catalogList.ToDataSet(false);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < ds.Tables[0].Rows.Count; index++)
                {
                    ds.Tables[0].Rows[index]["Name"] = Server.HtmlDecode(ds.Tables[0].Rows[index]["Name"].ToString());
                }
            }

            ddlCatalog.DataSource = ds;
            ddlCatalog.DataTextField = "Name";
            ddlCatalog.DataValueField = "CatalogID";
            ddlCatalog.DataBind();
            ListItem item3 = new ListItem(this.GetGlobalResourceObject("ZnodeAdminResource", "DropdownTextAll").ToString().ToUpper(), "0");
            ddlCatalog.Items.Insert(0, item3);
            ddlCatalog.SelectedIndex = 0;
        }

        /// <summary>
        /// Search a Product by name,productnumber,sku,manufacturer,category,producttype
        /// </summary>
        private void BindSearchProduct()
        {
            ProductAdmin prodadmin = new ProductAdmin();
            DataSet ds = prodadmin.SearchProduct(Server.HtmlEncode(txtproductname.Text.Trim()), txtproductnumber.Text.Trim(), txtsku.Text.Trim(), string.Empty, dproducttype.Value, dproductcategory.Value, ddlCatalog.SelectedValue);

            if (ds.Tables[0] == null || ds.Tables[0].Rows.Count == 0)
            {
                btnCancelBottom.Visible = false;
                btnSubmitBottom.Visible = false;
                lblSelect.Visible = false;
            }
            else
            {
                btnCancelBottom.Visible = true;
                btnSubmitBottom.Visible = true;
            }
            pnlProductList.Visible = true;
            uxGrid.DataSource = ds.Tables[0];
            uxGrid.DataBind();
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Get ImagePath
        /// </summary>
        /// <param name="Imagefile">Image File</param>
        /// <returns>Returns the Image Path</returns>
        protected string GetImagePath(string Imagefile)
        {
            ZNodeImage znodeImage = new ZNodeImage();
            return znodeImage.GetImageHttpPathThumbnail(Imagefile);
        }

        /// <summary>
        /// Remember Old Values Method
        /// </summary>
        private void RememberOldValues()
        {
            Dictionary<int, string> productIdList = new Dictionary<int, string>();

            // Loop through the grid values
            foreach (GridViewRow row in uxGrid.Rows)
            {
                CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProduct") as CheckBox;

                // Check in the Session
                if (Session["CHECKEDITEMS"] != null)
                {
                    productIdList = (Dictionary<int, string>)Session["CHECKEDITEMS"];
                }

                int id = int.Parse(row.Cells[1].Text);

                if (check.Checked)
                {
                    if (!productIdList.ContainsKey(id))
                    {
                        productIdList.Add(id, row.Cells[3].Text);
                    }
                }
                else
                {
                    productIdList.Remove(id);
                }
            }

            if (productIdList.Count > 0)
            {
                Session["CHECKEDITEMS"] = productIdList;
            }
            if (productIdList.Count == 0 && uxGrid.Rows.Count > 0)
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorValidBundleProduct").ToString();
                lblError.Visible = true;
            }
        }

        /// <summary>
        /// Method to enable the Checkbox, if it is not the same product or not the parent bundle product
        /// </summary>
        /// <param name="productId"></param>
        /// <param name="parentProduct"></param>
        /// <returns></returns>
        public bool GetCheckBoxEnable(int productId, string parentProduct)
        {
            if (productId == this.ItemId || parentProduct == "1")
            {
                return false;
            }

            return true;
        }

        #endregion
    }
}