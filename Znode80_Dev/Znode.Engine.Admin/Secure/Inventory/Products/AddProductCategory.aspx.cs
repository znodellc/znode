﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Admin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin AddProductCategory class
    /// </summary>
    public partial class AddProductCategory : System.Web.UI.Page
    {
        #region Protected Member Variables
        private int itemId = 0;
        private string viewPage = "View.aspx?itemid=";
        private string selectedCategorySessionKey = "SelectedCategorySessionKey";
        #endregion

        #region Page Load
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.itemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {
                Session.Remove(this.selectedCategorySessionKey);

                this.BindDepartment();
            }
        }
        #endregion

        /// <summary>
        /// Back Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnBack_Click(object sender, EventArgs e)
        {
            this.RedirectToViewPage();
        }

        /// <summary>
        /// Clear Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnClear_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddProductCategory.aspx?itemid=" + this.itemId);
        }

        /// <summary>
        /// Search Button Click event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSearch_Click(object sender, EventArgs e)
        {
            this.BindDepartment();
        }



        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Cancel_Click(object sender, EventArgs e)
        {
            this.RedirectToViewPage();
        }

        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindDepartment();
        }

        protected void UxGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Dictionary<int, string> categoryIdList = (Dictionary<int, string>)Session[this.selectedCategorySessionKey];

                if (categoryIdList != null)
                {
                    CheckBox checkBox = (CheckBox)e.Row.Cells[0].FindControl("chkCategory") as CheckBox;
                    int id = int.Parse(e.Row.Cells[1].Text);
                    if (categoryIdList.ContainsKey(id) && checkBox != null)
                    {
                        checkBox.Checked = true;
                    }
                }
            }
        }

        #region Helper Methods

        /// <summary>
        /// Redirect to product view page with mode (departments) value.
        /// </summary>
        private void RedirectToViewPage()
        {
            Response.Redirect(this.viewPage + this.itemId + "&mode=departments");
        }

        /// <summary>
        /// Search a department by name.
        /// </summary>
        private void BindDepartment()
        {
            CategoryAdmin categoryAdmin = new CategoryAdmin();
            string departmentName = Server.HtmlEncode(txtDepartmentName.Text);
            TList<Category> categoryList = categoryAdmin.GetCategoriesBySearchData(departmentName);

            // Get Product Name by Product Id
            ZNode.Libraries.Admin.ProductAdmin prodAdmin = new ProductAdmin();
            Product product = prodAdmin.GetByProductId(this.itemId);
            lblProdName.Text = product.Name;

            // Get the already associated category Id to exclude from the category list page.
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            DataSet ds = productCategoryAdmin.GetByProductID(this.itemId);
            List<int> categoryIdList = new List<int>();
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    categoryIdList.Add((int)dr["CategoryId"]);
                }

                categoryList.ApplyFilter(delegate(Category category)
                {
                    return !categoryIdList.Contains(category.CategoryID);
                });
            }
            categoryList.Sort("Name");
            if (categoryList.Count > 0)
            {
                pnlDepartmentList.Visible = true;
                Button2.Visible = true;
                btnCancelBottom.Visible = true;
            }
            else
            {
                pnlDepartmentList.Visible = true;
                Button2.Visible = false;
                btnCancelBottom.Visible = false;
            }

            uxGrid.DataSource = categoryList;
            uxGrid.DataBind();
        }

        /// <summary>
        /// Remember Old Values Method
        /// </summary>
        private void RememberOldValues()
        {
            Dictionary<int, string> categoryIdList = new Dictionary<int, string>();

            // Loop through the grid values
            foreach (GridViewRow row in uxGrid.Rows)
            {
                CheckBox checkBox = (CheckBox)row.Cells[0].FindControl("chkCategory") as CheckBox;

                // Check in the Session
                if (Session[this.selectedCategorySessionKey] != null)
                {
                    categoryIdList = (Dictionary<int, string>)Session[this.selectedCategorySessionKey];
                }

                int id = int.Parse(row.Cells[1].Text);

                if (checkBox.Checked)
                {
                    if (!categoryIdList.ContainsKey(id))
                    {
                        categoryIdList.Add(id, row.Cells[3].Text);
                    }
                }
                else
                {
                    categoryIdList.Remove(id);
                }
            }

            if (categoryIdList.Count > 0)
            {
                Session[this.selectedCategorySessionKey] = categoryIdList;
            }
            if (categoryIdList.Count == 0 && uxGrid.Rows.Count > 0)
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductCategoryExist").ToString();
                lblError.Visible = true;
            }
        }
        #endregion

        protected void Update_Click(object sender, EventArgs e)
        {
            this.RememberOldValues();

            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            Dictionary<int, string> categoryIdList = (Dictionary<int, string>)Session[this.selectedCategorySessionKey];
            bool isUpdated = true;

            if (categoryIdList != null)
            {
                StringBuilder sb = new StringBuilder();
                StringBuilder categoryName = new StringBuilder();
                foreach (KeyValuePair<int, string> pair in categoryIdList)
                {
                    // Get ProductId
                    int categoryId = pair.Key;

                    isUpdated = productCategoryAdmin.Insert(this.itemId, categoryId);
                    categoryName.Append(pair.Value + ",");

                    if (!isUpdated)
                    {
                        sb.Append(pair.Value + ",");
                        lblError.Visible = true;
                    }
                }

                Session.Remove(this.selectedCategorySessionKey);

                if (sb.ToString().Length > 0)
                {
                    sb.Remove(sb.ToString().Length - 1, 1);

                    // Display Error message
                    lblError.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductCategorySelect").ToString(), sb.ToString());

                    foreach (GridViewRow row in uxGrid.Rows)
                    {
                        CheckBox checkBox = (CheckBox)row.Cells[0].FindControl("chkCategory") as CheckBox;
                        if (checkBox != null)
                        {
                            checkBox.Checked = false;
                        }
                    }
                }
                else
                {
                    ProductAdmin productAdmin = new ProductAdmin();
                    Product product = productAdmin.GetByProductId(this.itemId);
                    string productName = string.Empty;

                    categoryName.Remove(categoryName.Length - 1, 1);
                    if (product != null)
                    {
                        productName = product.Name;
                    }

                    string associationName = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogAddProductCategory").ToString(), categoryName, productName);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, associationName, productName);

                    this.RedirectToViewPage();
                }
            }
        }
    }
}
