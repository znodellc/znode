<%@ Page Language="C#" AutoEventWireup="True" MasterPageFile="~/Themes/Standard/edit.master" ValidateRequest="false" Title="Manage Products - Add Bundle Products" Inherits="Znode.Engine.Admin.Secure.Inventory.Products.AddBundleProduct" CodeBehind="AddBundleProduct.aspx.cs" %>

<%@ Register TagPrefix="ZNode" TagName="CategoryAutoComplete" Src="~/Controls/Default/CategoryAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="ProductTypeAutoComplete" Src="~/Controls/Default/ProductTypeAutoComplete.ascx" %>
<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div>
            <div class="LeftFloat" style="width: 50%">
                <h1>
                    <asp:Localize ID="TitleAddBundleProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleAddBundleProduct%>'></asp:Localize></h1>
                <p style="width: 870px;">
                    <asp:Localize ID="TextAddBundleProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextAddBundleProduct%>'></asp:Localize></p>
            </div>
            <div class="LeftFloat" style="width: 49%;" align="right">
                 <zn:Button runat="server" Width="180px" ButtonType="EditButton" OnClick="BtnBack_Click"  CausesValidation="False" Text='<%$ Resources:ZnodeAdminResource, ButtonBackProductDetail%>' ID="btnBack"  />
            </div>
        </div>
        <!-- Search product panel -->
        <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
            <div class="SearchForm">
                <h4 class="SubTitle">
                    <asp:Localize ID="SubTitleSearchProduct" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchProduct%>'></asp:Localize></h4>
                <div class="RowStyle">
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleProductName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName%>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductname" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleProductNumber" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductsNumber%>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtproductnumber" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleSKU" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSKU%>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:TextBox ID="txtsku" runat="server"></asp:TextBox></span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleCatalog" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCatalog%>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <asp:DropDownList ID="ddlCatalog" runat="server"></asp:DropDownList></span>
                    </div>
                </div>
                <div class="RowStyle">

                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleProductType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductType%>'></asp:Localize></span><br />
                        <span class="ValueStyle">
                            <ZNode:ProductTypeAutoComplete ID="dproducttype" runat="server" />
                        </span>
                    </div>
                    <div class="ItemStyle">
                        <span class="FieldStyle">
                            <asp:Localize ID="ColumnTitleProductCategories" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleProductCategories%>'></asp:Localize></span>
                        <br />
                        <span class="ValueStyle">
                            <ZNode:CategoryAutoComplete ID="dproductcategory" runat="server" />
                        </span>
                    </div>
                </div>
                <div class="ClearBoth">
                    <br />
                </div>
                <div>
                    <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnClear_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonClear%>'  CausesValidation="False" ID="btnClear" />
                    <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSearch_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSearch%>'  ID="btnSearch" />
                </div>
            </div>
        </asp:Panel>
        <!-- Product List -->
        <asp:Panel ID="pnlProductList" runat="server" Visible="false">
            <h4 class="GridTitle">
                <asp:Localize ID="GridTitleProductList" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitleProductList%>'></asp:Localize></h4>
            <br />
            <small>
                <asp:Label ID="lblSelect" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTextProductAdd%>'></asp:Label></small>
            <asp:GridView ID="uxGrid" runat="server" CssClass="Grid" CaptionAlign="Left" AutoGenerateColumns="False"
                GridLines="None" AllowPaging="True" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging"
                OnRowDataBound="UxGrid_RowDataBound" EmptyDataText='<%$ Resources:ZnodeAdminResource, ErrorNoProducts%>'
                Width="100%" CellPadding="4">
                <Columns>
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleSelect%>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:CheckBox ID="chkProduct" runat="server" Enabled='<%# GetCheckBoxEnable(int.Parse(DataBinder.Eval(Container.DataItem, "ProductId").ToString()), DataBinder.Eval(Container.DataItem,"ParentProduct").ToString()) %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="ProductId" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleID%>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleImage%>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img id="prodImage" alt=" " src='<%# GetImagePath(DataBinder.Eval(Container.DataItem, "ImageFile").ToString())%>'
                                runat="server" style="border: none" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleName%>' HeaderStyle-HorizontalAlign="Left" />
                    <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleBundleIsActive%>' HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <img alt="" id="chMark" src='<%# ZNode.Libraries.Admin.Helper.GetCheckMark(bool.Parse(DataBinder.Eval(Container.DataItem, "ActiveInd").ToString()))%>'
                                runat="server" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle CssClass="FooterStyle" />
                <RowStyle CssClass="RowStyle" />
                <PagerStyle CssClass="PagerStyle" />
                <HeaderStyle CssClass="HeaderStyle" />
                <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
            </asp:GridView>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <asp:Label ID="lblError" runat="server" Text="Label" Visible="false" CssClass="Error"
                    ForeColor="red"></asp:Label>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <div>
                <zn:Button runat="server" ButtonType="SubmitButton" OnClick="Update_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' ID="btnSubmitBottom" />
                <zn:Button runat="server" ButtonType="CancelButton" OnClick="Cancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelBottom" />
            </div>
        </asp:Panel>
        <div>
            <ZNode:Spacer ID="Spacer3" SpacerHeight="20" SpacerWidth="3" runat="server"></ZNode:Spacer>
        </div>
    </div>
</asp:Content>
