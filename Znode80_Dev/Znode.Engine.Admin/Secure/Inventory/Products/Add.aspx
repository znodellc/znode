<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" EnableEventValidation="false"
    ValidateRequest="false" Title="Manage Products - Add" Inherits="Znode.Engine.Admin.Secure.Inventory.Products.Add" CodeBehind="Add.aspx.cs" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<%@ Register Src="~/Controls/Default/ProductTypeAutoComplete.ascx" TagName="ProductTypeAutoComplete"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/SupplierAutoComplete.ascx" TagName="SupplierAutoComplete"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/ManufacturerAutoComplete.ascx" TagName="ManufacturerAutoComplete"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/ImageUploader.ascx" TagName="UploadImage"
    TagPrefix="ZNode" %>
<%@ Register Src="~/Controls/Default/HtmlTextBox.ascx" TagName="HtmlTextBox"
    TagPrefix="ZNode" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <script language="javascript" type="text/javascript">


        function onCheckboxChanged() {

            var chkfree = document.getElementById('ctl00_ctl00_uxMainContent_uxMainContent_chkFreeShippingInd');
            var chkseparate = document.getElementById('ctl00_ctl00_uxMainContent_uxMainContent_chkShipSeparately');
            var separateHide = document.getElementById('ctl00_ctl00_uxMainContent_uxMainContent_dvShipSeparate');

            if (chkfree.checked) { 
                chkseparate.checked = false;
                separateHide.style.display = "none"; 
            }
            else {
                chkseparate.disabled = false;
                if (chkseparate.parentElement.tagName == 'SPAN')
                    chkseparate.parentElement.disabled = false;

                separateHide.style.display = "block";

            }

        }

    </script>
    <asp:HiddenField ID="existSKUvalue" runat="server" />
    <div class="Form">
        <div class="LeftFloat" style="width: 70%; text-align: left">
            <h1>
                <asp:Label ID="lblTitle" runat="server"></asp:Label>
            </h1>
        </div>

        <div style="clear: both">
            <asp:Label ID="Label1" CssClass="Error" runat="server"></asp:Label>
        </div>
        <%--<div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" DisplayMode="List"
                ShowMessageBox="True" ShowSummary="False" />
        </div>--%>
        <div>
            <asp:Label ID="lblMsg" runat="server" CssClass="Error"></asp:Label>
        </div>

        <div>
            <uc1:Spacer ID="Spacer1" SpacerHeight="5" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div class="FormView">
            <h4 class="SubTitle"><asp:Localize ID="GeneralInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleGeneralInformation%>'></asp:Localize></h4>
            <div class="FieldStyle">
               <asp:Localize ID="ColumnProductName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductName%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductName" runat="server" Width="152px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtProductName"
                ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductName%>' CssClass="Error" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                <asp:RegularExpressionValidator ID="regProductName" runat="server" ControlToValidate="txtProductName"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularProductName%>' Display="Dynamic" ValidationExpression="[0-9A-Za-z()\s\',.:&%#$@_-]+"
                    CssClass="Error"></asp:RegularExpressionValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnSKUorPart" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSKUorPart%>'></asp:Localize><span class="Asterix">*</span><br />
                <small><asp:Localize ID="ColumnTextSKUorPart" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextSKUorPart%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductSKU" MaxLength="100" runat="server"></asp:TextBox>
                <asp:Label ID="lblProductSKU" runat="server" Visible="false"></asp:Label>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtProductSKU"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredSKUorPart%>' SetFocusOnError="true"></asp:RequiredFieldValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnProductCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductCode%>'></asp:Localize><span class="Asterix">*</span><br />
                <small><asp:Localize ID="ColumnTextProductCode" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextProductCode%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductNum" runat="server" Width="152px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="ProductName" runat="server" ControlToValidate="txtProductNum"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductNumber%>' CssClass="Error" Display="dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
            </div>

            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <div class="FieldStyle">
                        <asp:Localize ID="ColumnProductType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductType%>'></asp:Localize><br />
                        <small><asp:Localize ID="ColumnTextProductType" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextProductType%>'></asp:Localize></small>
                    </div>
                    <div class="ValueStyle">
                        <ZNode:ProductTypeAutoComplete ID="ProductTypeList" runat="server" Width="152px"
                            IsRequired="true" AutoPostBack="true" />
                    </div>
                    <div runat="server" id="DivAttributes" visible="false">
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnProductAttributes" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductAttributes%>'></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:PlaceHolder ID="ControlPlaceHolder" runat="server"></asp:PlaceHolder>
                        </div>
                    </div>
                    <div id="divExpirationDays" runat="Server" visible="false">
                        <div class="FieldStyle" id="div1" runat="Server">
                            <asp:Localize ID="ColumnExpirationPeriod" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnExpirationPeriod%>'></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtExporationPeriod" runat="server"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtExporationPeriod"
                                CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredExpirationPeriod%>' SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:RangeValidator ID="RangeValidator6" runat="server" ControlToValidate="txtExporationPeriod"
                                Display="Dynamic" CssClass="Error" ErrorMessage="Enter a valid expiration period."
                                MinimumValue="1" MaximumValue="9999" Type="Integer"></asp:RangeValidator>
                        </div>
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnExpirationFrequency" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnExpirationFrequency%>'></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:DropDownList ID="ddlExpirationFrequency" runat="server">
                            </asp:DropDownList>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ProductTypeList" />
                </Triggers>
            </asp:UpdatePanel>

            <div class="FieldStyle">
                <asp:Localize ID="ColumnBrand" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnBrand%>'></asp:Localize><br />
                <small><asp:Localize ID="ColumnTextBrand" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextBrand%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <ZNode:ManufacturerAutoComplete ID="ManufacturerList" runat="server" Width="152px" />
            </div>
            <asp:Panel ID="pnlSupplier" runat="server">
                <div class="FieldStyle">
                   <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductSupplier%>'></asp:Localize>
                   <br />
                    <small><asp:Localize ID="Localize2" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextProductSupplier%>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <ZNode:SupplierAutoComplete ID="SupplierList" runat="server" />
                </div>
            </asp:Panel>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnDownloadLink" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDownloadLink%>'></asp:Localize><br />
                <small><asp:Localize ID="ColumnTextDownloadLink" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextDownloadLink%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDownloadLink" Columns="50" runat="server"></asp:TextBox>
            </div>
            <h4 class="SubTitle"><asp:Localize ID="SubTitlePricing" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitlePricing%>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnRetailPrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnRetailPrice%>'></asp:Localize><span class="Asterix">*</span>
            </div>
            <div class="ValueStyle">
                <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox
                    ID="txtproductRetailPrice" runat="server" MaxLength="10"></asp:TextBox>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductRetailPrice%>'
                    ControlToValidate="txtproductRetailPrice" CssClass="Error" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtproductRetailPrice"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareRetailPrice%>'
                    CssClass="Error" Display="Dynamic" /><br />
				<div style="margin-left: 155px;">
                <asp:RangeValidator ID="RangeValidator5" runat="server" ControlToValidate="txtproductRetailPrice"
                   ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeRetailPrice%>'
                    MaximumValue="99999999" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
				</div>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnSalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSalePrice%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix()%>&nbsp;<asp:TextBox
                    ID="txtproductSalePrice" runat="server" MaxLength="10"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtproductSalePrice"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareSalePrice%>'
                    CssClass="Error" Display="Dynamic" />
				<div style="margin-left: 155px;">
                <asp:RangeValidator ID="RangeValidator7" runat="server" ControlToValidate="txtproductSalePrice"
                  ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeProductSalePrice%>'
                    MaximumValue="99999999" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
				</div>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnWholesalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWholeSalePrice%>'></asp:Localize><br />
                 <small><asp:Localize ID="ColumnTextProductWholesalePrice" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextProductWholesalePrice%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;<asp:TextBox
                    ID="txtProductWholeSalePrice" runat="server" MaxLength="10"></asp:TextBox>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtProductWholeSalePrice"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareWholeSalePrice%>' 
                    CssClass="Error" Display="Dynamic" />
				<div style="margin-left: 155px;">
                <asp:RangeValidator ID="RangeValidator8" runat="server" ControlToValidate="txtProductWholeSalePrice"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeProductWholeSalePrice%>' 
                    MaximumValue="99999999" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
				</div>
            </div>
            <div class="FieldStyle">
               <asp:Localize ID="ColumnTaxClass" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTaxClass%>'></asp:Localize>
                <br />
                <small><asp:Localize ID="ColumnTextProductTaxClass" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextProductTaxClass%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:DropDownList ID="ddlTaxClass" runat="server" />
            </div>
            <h4 class="SubTitle"><asp:Localize ID="SubTitleInventory" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleInventory%>'></asp:Localize></h4>
            <asp:Panel ID="pnlquantity" runat="server">
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnQuantityOnHand" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnQuantityOnHand%>'></asp:Localize><span class="Asterix">*</span><br />
                    <small><asp:Localize ID="ColumnTextQuantityOnHand" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextQuantityOnHand%>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtProductQuantity" runat="server" Rows="3">999</asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator2" runat="server" ControlToValidate="txtProductQuantity"
                        CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeProductQuantity%>' MaximumValue="999999"
                        MinimumValue="-999999" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtProductQuantity"
                        CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductQuantity%>' SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnReOrderLevel" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnReOrderLevel%>'></asp:Localize><br />
                    <small><asp:Localize ID="ColumnTextReOrderLevel" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextReOrderLevel%>'></asp:Localize></small>
                </div>
                <div class="ValueStyle">
                    <asp:TextBox ID="txtReOrder" runat="server"></asp:TextBox>
                    <asp:RangeValidator ID="RangeValidator1" runat="server" ControlToValidate="txtReOrder"
                        CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeReOrder%>' MaximumValue="999999"
                        MinimumValue="-999999" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
                </div>
            </asp:Panel>
            <div class="FieldStyle">
                <asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnMinSelectableQuantity%>'></asp:Localize><br />
                <small><asp:Localize ID="Localize4" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextMinSelectableQuantity%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtMinQuantity" runat="server" Rows="3">1</asp:TextBox>
                <asp:RangeValidator ID="rvMinQuantity" runat="server" ControlToValidate="txtMinQuantity"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber%>' MaximumValue="999999"
                    MinimumValue="1" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnMaxSelectableQuantity" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnMaxSelectableQuantity%>'></asp:Localize><span class="Asterix">*</span><br />
                <small><asp:Localize ID="ColumnTextMaxSelectableQuantity" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextMaxSelectableQuantity%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtMaxQuantity" runat="server" Rows="3">10</asp:TextBox>
                <asp:RangeValidator ID="RangeValidator4" runat="server" ControlToValidate="txtMaxQuantity"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeEnterWholeNumber%>' MaximumValue="999999"
                    MinimumValue="1" SetFocusOnError="True" Type="Integer"></asp:RangeValidator>
                <asp:CompareValidator ID="cvQuantityRange" runat="server" ControlToValidate="txtMaxQuantity"
                    ControlToCompare="txtMinQuantity" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareQuantityRange%>'
                    Operator="GreaterThanEqual" SetFocusOnError="True" Type="Integer"></asp:CompareValidator>
            </div>
            <h4 class="SubTitle"><asp:Localize ID="SubTitleDisplaySettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDisplaySettings%>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnDisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnDisplayOrder%>'></asp:Localize><span class="Asterix">*</span><br />
                <small><asp:Localize ID="ColumnTextDisplayOrder" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextDisplayOrder%>'></asp:Localize></small>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtDisplayOrder"
                    CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductDisplayOrder%>' SetFocusOnError="true"></asp:RequiredFieldValidator>
                <asp:RangeValidator ID="RangeValidator3" runat="server" ControlToValidate="txtDisplayOrder"
                    Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeDisplayOrder%>' MaximumValue="999999999"
                    MinimumValue="1" Type="Integer"></asp:RangeValidator>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtDisplayOrder" runat="server" MaxLength="9" Columns="5">500</asp:TextBox>
            </div>
            <h4 class="SubTitle"><asp:Localize ID="SubTitleShippingSettings" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleShippingSettings%>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnFreeShipping" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFreeShipping%>'></asp:Localize>	
            </div>
            <div class="ValueStyle">
                <asp:CheckBox ID="chkFreeShippingInd" onclick="onCheckboxChanged();" Text='<%$ Resources:ZnodeAdminResource, ColumnTextProductFreeShipping%>'
                    runat="server" />
            </div>
               <asp:UpdatePanel ID="upnlShippingType" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
            <div id="dvShipSeparate" runat="server">

                <div class="FieldStyle" style="display:none">
                   <asp:Localize ID="ColumnShipSeparately" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShipSeparately%>'></asp:Localize>
                </div>
                <div class="ValueStyle" style="display:none">
                    <asp:CheckBox ID="chkShipSeparately" Text='<%$ Resources:ZnodeAdminResource, ColumnTextShipSeparately%>' runat="server" />
                </div>
             
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnShippingCost" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingCost%>'></asp:Localize><span class="Asterix">*</span>
                        </div>
                        <div class="ValueStyle">
                            <asp:DropDownList AutoPostBack="true" ID="ShippingTypeList" runat="server" OnSelectedIndexChanged="ShippingTypeList_SelectedIndexChanged">
                            </asp:DropDownList>
                        </div>
                        <asp:Panel ID="pnlShippingRate" runat="server" Visible="false">
                            <div class="FieldStyle">
                                 <asp:Localize ID="ColumnShippingRate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingRate%>'></asp:Localize><br />
                                <small><asp:Localize ID="ColumnTextShippingRate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextShippingRate%>'></asp:Localize></small>
                            </div>
                            <div class="ValueStyle">
                                <%= ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix() %>&nbsp;
                            <asp:TextBox ID="txtShippingRate" runat="server" Width="46px" MaxLength="7" Text="0"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter a shipping rate for this Product"
                                    ControlToValidate="txtShippingRate" CssClass="Error" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                                <asp:CompareValidator runat="server" ControlToValidate="txtShippingRate" Type="Currency"
                                    Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource,CompareShippingRate%>'
                                    CssClass="Error" Display="Dynamic" />
                                <asp:RangeValidator runat="server" ControlToValidate="txtShippingRate" ErrorMessage='<%$ Resources:ZnodeAdminResource,RangeShippingRate%>'
                                    MaximumValue="99999999" Type="Currency" MinimumValue="0" Display="Dynamic" CssClass="Error"></asp:RangeValidator>
                            </div>
                        </asp:Panel>
                  

            </div>

            <h4 class="SubTitle"><asp:Localize ID="SubTitleProductAttributes" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleProductAttributes%>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnWeight" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWeight%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductWeight" runat="server" Width="46px" MaxLength="7"> </asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.WeightUnit %>
                <asp:RangeValidator Enabled="false" ID="weightBasedRangeValidator" runat="server"
                    ControlToValidate="txtProductWeight" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RangeProductWeight%>'
                    MaximumValue="999999" MinimumValue="0.1" CultureInvariantValues="true" Type="Double"></asp:RangeValidator>
               <asp:RequiredFieldValidator Enabled="false" ID="RequiredForWeightBasedoption" runat="server"
                    ErrorMessage='<%$ Resources:ZnodeAdminResource, RequiredProductWeight%>' ControlToValidate="txtProductWeight"
                    CssClass="Error" Display="Dynamic" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="WeightFieldCompareValidator" runat="server" ControlToValidate="txtProductWeight"
                        Type="Currency" Operator="DataTypeCheck" ErrorMessage='' CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnHeight" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnHeight%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductHeight" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="txtProductHeight"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareProductHeight%>'
                    CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnWidth" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnWidth%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductWidth" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="txtProductWidth"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareProductWidth%>'
                    CssClass="Error" Display="Dynamic" />
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnLength" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLength%>'></asp:Localize>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtProductLength" runat="server" Width="46px"></asp:TextBox>&nbsp;<%= ZNode.Libraries.Framework.Business.ZNodeConfigManager.SiteConfig.DimensionUnit %>&nbsp;&nbsp;
                <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="txtProductLength"
                    Type="Currency" Operator="DataTypeCheck" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareProductLength%>'
                    CssClass="Error" Display="Dynamic" />
            </div>
          </ContentTemplate>
                </asp:UpdatePanel>
            <h4 class="SubTitle"><asp:Localize ID="ColumnProductImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductImage%>'></asp:Localize></h4>
            <small><asp:Localize ID="ColumnTextProductImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextProductImage%>'></asp:Localize></small>
            <div id="tblShowImage" runat="server" visible="false" style="margin: 10px 0px 10px 0px;">
                <div class="LeftFloat" style="width: 300px; border-right: solid 1px #cccccc;" runat="server"
                    id="pnlImage">
                    <asp:Image ID="Image1" runat="server" />
                </div>
                <div class="LeftFloat" style="margin-left: -1px;" runat="server" id="pnlUploadSection">
                    <asp:Panel runat="server" ID="pnlShowImage">
                        <div class="FieldStyle" style="margin-bottom:4px;">
                           <asp:Localize ID="ColumnSelectanOption" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectanOption%>'></asp:Localize>	
                        </div>
                        <div class="ClearBoth"></div>
                        <div>
                            <asp:RadioButton ID="RadioProductCurrentImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonCurrentImage%>' runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductCurrentImage_CheckedChanged"
                                Checked="True" /><br />
                            <asp:RadioButton ID="RadioProductNewImage" Text='<%$ Resources:ZnodeAdminResource, RadioButtonNewImage%>' runat="server"
                                GroupName="Department Image" AutoPostBack="True" OnCheckedChanged="RadioProductNewImage_CheckedChanged" />
                        </div>
                    </asp:Panel>
                    <br />
                    <div id="tblProductDescription" runat="server" visible="false">
                        <div class="FieldStyle">
                           <asp:Localize ID="ColumnSelectanImage" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnSelectanImage%>'></asp:Localize>
                        </div>
                        <div class="ValueStyle">
                            <ZNode:UploadImage ID="uxProductImage" runat="server"></ZNode:UploadImage>
                        </div>
                    </div>
                    <div class="FieldStyle">
                        <asp:Localize ID="ColumnProductImageALTText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductImageALTText%>'></asp:Localize><br />
                        <small><asp:Localize ID="ColumnTextProductImageALTText" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextProductImageALTText%>'></asp:Localize></small>
                    </div>
                    <div class="ValueStyle">
                        <asp:TextBox ID="txtImageAltTag" runat="server"></asp:TextBox>
                    </div>
                    <div>
                        <asp:Label ID="lblProductImageError" runat="server" CssClass="Error" ForeColor="Red"
                            Text="" Visible="False"></asp:Label>
                    </div>
                    <asp:Panel ID="pnlImageName" runat="server" Visible="false">
                        <div class="FieldStyle">
                            <asp:Localize ID="ColumnImageFileName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnImageFileName%>'></asp:Localize>	
                        </div>
                        <div class="ValueStyle">
                            <asp:TextBox ID="txtimagename" runat="server"></asp:TextBox>
                        </div>
                    </asp:Panel>
                </div>
            </div>
            <div class="ClearBoth">
                <br />
            </div>
            <h4 class="SubTitle"><asp:Localize ID="Localize5" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleDescription%>'></asp:Localize></h4>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnShortDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShortDescription%>'></asp:Localize><br />
                <small><asp:Localize ID="ColumnTitleShortDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleShortDescription%>'></asp:Localize></small>
            </div>
            <div class="ValueStyle">
                <asp:TextBox ID="txtshortdescription" runat="server" Width="300px" TextMode="MultiLine"
                    Height="75px" MaxLength="100"></asp:TextBox>
            </div>
            <div class="FieldStyle">
                 <asp:Localize ID="ColumnLongDescription" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLongDescription%>'></asp:Localize>
            </div>
            <div class="HtmlValueStyle">
                <ZNode:HtmlTextBox ID="ctrlHtmlText" runat="server"></ZNode:HtmlTextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnProductFeatures" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductFeatures%>'></asp:Localize><br />
                <small><asp:Localize ID="ColumnTextProductFeatures" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextProductFeatures%>'></asp:Localize></small>
            </div>
            <div class="HtmlValueStyle">
                <ZNode:HtmlTextBox ID="ctrlHtmlPrdFeatures" runat="server"></ZNode:HtmlTextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnProductSpecifications" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnProductSpecifications%>'></asp:Localize><br />
                <small><asp:Localize ID="ColumnTextProductSpecifications" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextProductSpecifications%>'></asp:Localize></small>
            </div>
            <div class="HtmlValueStyle">
                <ZNode:HtmlTextBox ID="ctrlHtmlPrdSpec" runat="server"></ZNode:HtmlTextBox>
            </div>
            <div class="FieldStyle">
                <asp:Localize ID="ColumnShippingInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnShippingInformation%>'></asp:Localize><br />
                <small><asp:Localize ID="ColumnTextShippingInformation" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTextShippingInformation%>'></asp:Localize></small>
            </div>
            <div class="HtmlValueStyle">
                <ZNode:HtmlTextBox ID="ctrlHtmlProdInfo" runat="server"></ZNode:HtmlTextBox>
            </div>
        </div>
        <div class="ClearBoth">
        </div>
        <div>
            <uc1:Spacer ID="Spacer8" SpacerHeight="20" SpacerWidth="3" runat="server"></uc1:Spacer>
        </div>
        <div>
            <zn:Button runat="server" ButtonType="SubmitButton" OnClick="BtnSubmit_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonSubmit%>' CausesValidation="true" ID="btnSubmitTop"  />
            <zn:Button runat="server" ButtonType="CancelButton" OnClick="BtnCancel_Click" Text='<%$ Resources:ZnodeAdminResource, ButtonCancel%>' CausesValidation="False" ID="btnCancelTop" />
        </div>
    </div>
</asp:Content>
