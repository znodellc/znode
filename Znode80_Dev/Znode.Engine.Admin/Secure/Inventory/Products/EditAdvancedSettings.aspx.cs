using System;
using System.Web.UI;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Framework.Business;

namespace  Znode.Engine.Admin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin Admin_Secure_catalog_product_edit_advancedsettings class
    /// </summary>
    public partial class EditAdvancedSettings : System.Web.UI.Page
    {
        #region Protected Member Variables
        private string AssociateName = string.Empty;
        private int ItemId = 0;
        private string ManagePageLink = "~/Secure/Inventory/Products/View.aspx?mode=advanced&itemid=";
        private string CancelPageLink = "~/Secure/Inventory/Products/View.aspx?mode=advanced&itemid=";

        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"].ToString());
            }

            if (!Page.IsPostBack)
            {

                if (this.ItemId > 0)
                {
                    lblTitle.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TitleProductEditSettings").ToString();

                    // Bind Details
                    this.Bind();
                }
            }
        }
        #endregion

        #region General Events
        /// <summary>
        /// Submit Button Click Event - Fires when Submit button is triggered
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnSubmit_Click(object sender, EventArgs e)
        {
            ProductAdmin productAdmin = new ProductAdmin();
            ProductCategoryAdmin productCategoryAdmin = new ProductCategoryAdmin();
            Product product = new Product();
            UrlRedirectAdmin urlRedirectAdmin = new UrlRedirectAdmin();
            string mappedSEOUrl = string.Empty;

            // If edit mode then get all the values first
            if (this.ItemId > 0)
            {
                product = productAdmin.GetByProductId(this.ItemId);

                if (product.SEOURL != null)
                {
                    mappedSEOUrl = product.SEOURL;
                }
            }

            // Set properties Display Settings
            product.ActiveInd = CheckEnabled.Checked;
            product.HomepageSpecial = CheckHomeSpecial.Checked;
            product.InventoryDisplay = Convert.ToByte(false);
            product.CallForPricing = CheckCallPricing.Checked;
            product.NewProductInd = CheckNewItem.Checked;
            product.FeaturedInd = ChkFeaturedProduct.Checked;
            product.Franchisable = CheckFranchisable.Checked;


            // Set properties
            product.SEOTitle = Server.HtmlEncode(txtSEOTitle.Text.Trim());
            product.SEOKeywords = Server.HtmlEncode(txtSEOMetaKeywords.Text.Trim());
            product.SEODescription = Server.HtmlEncode(txtSEOMetaDescription.Text.Trim());
            product.SEOURL = null;

            if (txtSEOUrl.Text.Trim().Length > 0)
            {
                product.SEOURL = txtSEOUrl.Text.Trim().Replace(" ", "-");
            }

            if (string.Compare(product.SEOURL, mappedSEOUrl, true) != 0)
            {
                if (urlRedirectAdmin.SeoUrlExists(product.SEOURL, product.ProductID))
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSEOFriendlyURLAlreadyExist").ToString();
                    return;
                }
            }


            if (!CheckFranchisable.Checked)
            {
                bool Check = productCategoryAdmin.RemoveProductCategory(product.ProductID);
                if (Check)
                {
                    this.AssociateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogDeletedfranchisable").ToString() + product.Name;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.DeleteObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, product.Name);
                }
            }

            // Inventory Setting - Out of Stock Options
            if (InvSettingRadiobtnList.SelectedValue.Equals("1"))
            {
                // Only Sell if Inventory Available - Set values
                product.TrackInventoryInd = true;
                product.AllowBackOrder = false;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("2"))
            {
                // Allow Back Order - Set values
                product.TrackInventoryInd = true;
                product.AllowBackOrder = true;
            }
            else if (InvSettingRadiobtnList.SelectedValue.Equals("3"))
            {
                // Don't Track Inventory - Set property values
                product.TrackInventoryInd = false;
                product.AllowBackOrder = false;
            }

            // Inventory Setting - Stock Messages
            if (txtOutofStock.Text.Trim().Length == 0)
            {
                product.OutOfStockMsg = this.GetGlobalResourceObject("ZnodeAdminResource", "ValueOutOfStock").ToString(); 
            }
            else
            {
                product.OutOfStockMsg = Server.HtmlEncode(txtOutofStock.Text.Trim());
            }

            product.InStockMsg = Server.HtmlEncode(txtInStockMsg.Text.Trim());
            product.BackOrderMsg = Server.HtmlEncode(txtBackOrderMsg.Text.Trim());
            product.DropShipInd = chkDropShip.Checked;

            // Recurring Billing settings
            if (chkRecurringBillingInd.Checked)
            {
                product.RecurringBillingInitialAmount = Convert.ToDecimal(txtRecurringBillingInitialAmount.Text);
                product.RecurringBillingInd = chkRecurringBillingInd.Checked;
                product.RecurringBillingPeriod = ddlBillingPeriods.SelectedItem.Value;
                product.RecurringBillingFrequency = "1";
                product.RecurringBillingTotalCycles = 0;
            }
            else
            {
                product.RecurringBillingInd = false;
                product.RecurringBillingInstallmentInd = false;
                product.RecurringBillingInitialAmount = null;
            }

            bool status = false;

            try
            {
                if (this.ItemId > 0)
                {
                    // PRODUCT UPDATE
                    status = productAdmin.Update(product);
                }

                if (status)
                {
                    this.AssociateName = this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEditAdvanceSettings").ToString() + product.Name;
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.EditObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, this.AssociateName, product.Name);

                    if (chkAddURLRedirect.Checked)
                    {
                        urlRedirectAdmin.AddUrlRedirectTable(SEOUrlType.Product, mappedSEOUrl, product.SEOURL, product.ProductID.ToString());
                    }

                    urlRedirectAdmin.UpdateUrlRedirect(mappedSEOUrl);

                    product = productAdmin.GetByProductId(this.ItemId);

                    Response.Redirect(this.ManagePageLink + this.ItemId);
                }
                else
                {
                    lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnabletoUpdateAdvanceSettings").ToString();
                }
            }
            catch (Exception)
            {
                lblError.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnabletoUpdateAdvanceSettings").ToString();
                return;
            }
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.CancelPageLink + this.ItemId);
        }

        /// <summary>
        /// Recurring Billing Ind Checked Changed Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void ChkRecurringBillingInd_CheckedChanged(object sender, EventArgs e)
        {
            if (chkRecurringBillingInd.Checked)
            {
                pnlRecurringBilling.Visible = true;
            }
            else
            {
                pnlRecurringBilling.Visible = false;
            }
        }

        /// <summary>
        /// Billing Period Selected Index Changed
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void DdlBillingPeriods_SelectedIndexChanged(object sender, EventArgs e)
        {
            //this.BindBillingFrequency();
        }
        #endregion

        #region Helper Methods
        #endregion

        #region Bind Methods
        /// <summary>
        /// Bind Method
        /// </summary>
        private void Bind()
        {
            // Create Instance for Product Admin and Product entity
            ZNode.Libraries.Admin.ProductAdmin ProdAdmin = new ProductAdmin();
            Product product = ProdAdmin.GetByProductId(this.ItemId);

            if (product != null)
            {
                // Display Settings                                             
                CheckEnabled.Checked = product.ActiveInd;
                CheckHomeSpecial.Checked = product.HomepageSpecial;
                CheckCallPricing.Checked = product.CallForPricing;
                ChkFeaturedProduct.Checked = product.FeaturedInd;

                if (product.NewProductInd.HasValue)
                {
                    CheckNewItem.Checked = product.NewProductInd.Value;
                }

                // Inventory Setting - Out of Stock Options
                if (product.AllowBackOrder.HasValue && product.TrackInventoryInd.HasValue)
                {
                    if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value == false)
                    {
                        InvSettingRadiobtnList.Items[0].Selected = true;
                    }
                    else if (product.TrackInventoryInd.Value && product.AllowBackOrder.Value)
                    {
                        InvSettingRadiobtnList.Items[1].Selected = true;
                    }
                    else if (product.TrackInventoryInd.Value == false && product.AllowBackOrder.Value == false)
                    {
                        InvSettingRadiobtnList.Items[2].Selected = true;
                    }
                }
				else
				{
					InvSettingRadiobtnList.Items[2].Selected = true;
				}

                // Inventory Setting - Stock Messages
                txtInStockMsg.Text = Server.HtmlDecode(product.InStockMsg);
                txtOutofStock.Text = Server.HtmlDecode(product.OutOfStockMsg);
                txtBackOrderMsg.Text = Server.HtmlDecode(product.BackOrderMsg);

                if (product.DropShipInd.HasValue)
                {
                    chkDropShip.Checked = product.DropShipInd.Value;
                }

                lblTitle.Text += "\"" + product.Name + "\"";

                CheckFranchisable.Checked = product.Franchisable;

                // Recurring Billing
                chkRecurringBillingInd.Checked = product.RecurringBillingInd;
                pnlRecurringBilling.Visible = product.RecurringBillingInd;
                if (product.RecurringBillingInitialAmount.HasValue)
                    txtRecurringBillingInitialAmount.Text = product.RecurringBillingInitialAmount.Value.ToString("N");
                ddlBillingPeriods.SelectedValue = product.RecurringBillingPeriod;

                // SEO
                txtSEOTitle.Text = Server.HtmlDecode(product.SEOTitle);
                txtSEOMetaKeywords.Text = Server.HtmlDecode(product.SEOKeywords);
                txtSEOMetaDescription.Text = Server.HtmlDecode(product.SEODescription);
                txtSEOUrl.Text = product.SEOURL;

                if (ZNodeConfigManager.EnvironmentConfig.LicenseType == ZNodeLicenseType.Multifront)
                {
                    pnlFranchise.Visible = false;
                }
                else
                {
                    pnlFranchise.Visible = true;
                }
            }
        }
        #endregion
    }
}