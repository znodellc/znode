﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProductTags.ascx.cs" Inherits="Znode.Engine.Admin.Secure.Inventory.Products.ProductTags" %>
<%@ Register TagPrefix="znode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:UpdatePanel runat="server" ID="upProductTags">
	<ContentTemplate>
		<asp:Panel ID="pnlTagsList" runat="server">
			<div class="FormView">
				<h4 class="GridTitle"> <asp:Literal ID="GridTitleFacets" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleAssociatedTags %>'></asp:Literal></h4>
				<div>
					<div class="TabDescription" style="width: 80%;">
						<p> 
                            <asp:Literal ID="Literal1" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextListofTagsAssociateProduct %>'></asp:Literal>
						</p>
					</div>
					<asp:Panel ID="pnlAddTag" runat="server">
						<div>
						</div>
						<div class="ValueStyle">
							<asp:TextBox ID="txtTagNames" runat="server" Columns="50" TextMode="MultiLine"
								Rows="12"></asp:TextBox>
							<div>
								<znode:Spacer ID="Spacer" SpacerHeight="15" SpacerWidth="3" runat="server"></znode:Spacer>
							</div>
						</div>
						<div class="ClearBoth"></div>
						<div>
							 
                             <zn:Button ID="btnSubmitBottom" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="Update_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
       
                             <zn:Button ID="btnCancelBottom" runat="server" ButtonType="CancelButton"  CausesValidation="True" OnClick="Cancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
         

						</div>
					</asp:Panel>
				</div>
				<div class="FieldStyle">
					<asp:Label ID="lblMsg" CssClass="Error" runat="server"></asp:Label>
				</div>
				<br />
				<znode:Spacer ID="Spacer66" runat="server" SpacerHeight="10" />
			</div>
		</asp:Panel>
	</ContentTemplate>
</asp:UpdatePanel>
<asp:UpdateProgress ID="uxTagUpdateProgress" runat="server" AssociatedUpdatePanelID="upProductTags" DisplayAfter="10">
	<ProgressTemplate>
		<div id="ajaxProgressBg"></div>
		<div id="ajaxProgress"></div>
	</ProgressTemplate>
</asp:UpdateProgress>
