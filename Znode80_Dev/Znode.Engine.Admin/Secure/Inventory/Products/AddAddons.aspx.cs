using System;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Znode.Engine.Common;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;

namespace Znode.Engine.Admin.Secure.Inventory.Products
{
    /// <summary>
    /// Represents the Site Admin  Admin_Secure_catalog_product_add_addons class
    /// </summary>
    public partial class AddAddons : System.Web.UI.Page
    {
        #region Private Variables
        private static bool IsSearchEnabled = false;
        private string DetailsLink = "~/Secure/Inventory/Products/View.aspx?mode=addons";
        private int ItemId = 0;
        #endregion

        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            // Get ItemId from querystring        
            if (Request.Params["itemid"] != null)
            {
                this.ItemId = int.Parse(Request.Params["itemid"]);
            }
            else
            {
                this.ItemId = 0;
            }

            if (!Page.IsPostBack)
            {
                this.BindAddons();
                this.BindProductName();
            }
        }
        #endregion

        #region Grid Events

        /// <summary>
        /// AddOn Grid items page event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxAddOnGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            uxAddOnGrid.PageIndex = e.NewPageIndex;

            if (IsSearchEnabled)
            {
                this.SearchAddons();
            }
            else
            {
                this.BindAddons();
            }
        }

        /// <summary>
        /// ProductAdd-Ons Grid Row command event - occurs when Manage button is fired.
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxAddOnGrid_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page")
            {
            }
            else
            {
                // Convert the row index stored in the CommandArgument
                // property to an Integer.
                int index = Convert.ToInt32(e.CommandArgument);

                // Get the values from the appropriate cell in the GridView control.
                GridViewRow selectedRow = uxAddOnGrid.Rows[index];
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Add Selected Addons Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddSelectedAddons_Click(object sender, EventArgs e)
        {
            ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();
            StringBuilder sb = new StringBuilder();
            StringBuilder addonName = new StringBuilder();

            // Loop through the grid values
            foreach (GridViewRow row in uxAddOnGrid.Rows)
            {
                CheckBox check = (CheckBox)row.Cells[0].FindControl("chkProductAddon") as CheckBox;

                // Get AddOnId
                int AddOnID = int.Parse(row.Cells[1].Text);

                if (check.Checked)
                {
                    ProductAddOn ProdAddOnEntity = new ProductAddOn();

                    // Set Properties
                    ProdAddOnEntity.ProductID = this.ItemId;
                    ProdAddOnEntity.AddOnID = AddOnID;

                    if (AdminAccess.IsAddOnExists(ProdAddOnEntity))
                    {
                        AdminAccess.AddNewProductAddOn(ProdAddOnEntity);
                        addonName.Append(this.GetAddOnName(AddOnID) + ",");
                        check.Checked = false;
                    }
                    else
                    {
                        sb.Append(this.GetAddOnName(AddOnID) + ",");
                        lblAddOnErrorMessage.Visible = true;
                    }
                }
            }

            if (sb.ToString().Length > 0)
            {
                sb.Remove(sb.ToString().Length - 1, 1);

                // Display Error message
                lblAddOnErrorMessage.Text = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorProductAddon").ToString(), sb.ToString());
            }
            else
            {
                ProductAdmin prodAdmin = new ProductAdmin();
                Product product = prodAdmin.GetByProductId(this.ItemId);
                if (addonName.Length > 0)
                {
                    addonName.Remove(addonName.Length - 1, 1);

                    string Associatename = string.Format(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogProductAddon").ToString(), addonName, product.Name);
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogActivity((int)ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.ErrorNum.CreateObject, UserStoreAccess.GetCurrentUserName(), null, null, null, null, Associatename, product.Name);
                    Response.Redirect(this.DetailsLink + "&itemid=" + this.ItemId);
                }
                else
                {
                    lblAddOnErrorMessage.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorValidProductAddon").ToString();
                    lblAddOnErrorMessage.Visible = true;
                }
            }
        }

        /// <summary>
        /// Addon Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddOnSearch_Click(object sender, EventArgs e)
        {
            this.SearchAddons();
            IsSearchEnabled = true;
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(this.DetailsLink + "&itemid=" + this.ItemId);
        }

        /// <summary>
        /// Clear Search Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void BtnAddOnClear_Click(object sender, EventArgs e)
        {
            // bind Grid
            this.BindAddons();
            // Reset Text fields & Bool variable
            txtAddonName.Text = string.Empty;
            txtAddOnTitle.Text = string.Empty;
            txtAddOnsku.Text = string.Empty;
            IsSearchEnabled = false;
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Gets the name of the Addon for this AddonId
        /// </summary>
        /// <param name="addOnId">The value of Add on Id</param>
        /// <returns>Returns the Add on name</returns>
        private string GetAddOnName(object addOnId)
        {
            ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();
            AddOn _addOn = AdminAccess.GetByAddOnId(int.Parse(addOnId.ToString()));

            if (_addOn != null)
            {
                return _addOn.Name;
            }

            return string.Empty;
        }


        /// <summary>
        /// Bind AddOn grid with filtered data
        /// </summary>
        private void SearchAddons()
        {
            ProductAddOnAdmin AdminAccess = new ProductAddOnAdmin();
            int localeId = 0;
            uxAddOnGrid.DataSource = AdminAccess.SearchAddOns(Server.HtmlEncode(txtAddonName.Text.Trim()), Server.HtmlEncode(txtAddOnTitle.Text.Trim()), txtAddOnsku.Text.Trim(), localeId);
            uxAddOnGrid.DataBind();
            DisplayButtons();
        }

        /// <summary>
        /// Bind Product Name
        /// </summary>
        private void BindProductName()
        {
            ProductAdmin ProductAdminAccess = new ProductAdmin();
            Product entity = ProductAdminAccess.GetByProductId(this.ItemId);
            if (entity != null)
            {
                lblTitle.Text = lblTitle.Text + "" + entity.Name;
            }
        }

        /// <summary>
        /// Bind Addon Grid - all Addons
        /// </summary>
        private void BindAddons()
        {
            ProductAddOnAdmin ProdAddonAdminAccess = new ProductAddOnAdmin();

            // List of Addons
            uxAddOnGrid.DataSource = ProdAddonAdminAccess.GetAllAddOns();
            uxAddOnGrid.DataBind();
            DisplayButtons();
        }

        /// <summary>
        /// To Enable / Disable Buttons Add and Cancel Buttons
        /// </summary>
        private void DisplayButtons()
        {
            if (uxAddOnGrid.Rows.Count != 0)
            {
                btnAddSelectedAddons.Visible = true;
                btnBottomCancel.Visible = true;
                return;
            }
            btnAddSelectedAddons.Visible = false;
            btnBottomCancel.Visible = false;
        }
        #endregion
    }
}