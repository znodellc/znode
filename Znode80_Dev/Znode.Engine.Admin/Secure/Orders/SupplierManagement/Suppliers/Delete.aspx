<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.SupplierManagement.Suppliers.Delete" CodeBehind="Delete.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="SupplierDelete" Src="~/Controls/Default/Providers/Suppliers/SupplierDelete.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:SupplierDelete ID="ctlSupplierDelete" runat="server" RedirectUrl="~/Secure/Orders/SupplierManagement/Suppliers/Default.aspx" />
</asp:Content>
