<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.SupplierManagement.Suppliers.Default" ValidateRequest="false" CodeBehind="Default.aspx.cs" %>
<%@ Register TagPrefix="Znode" TagName="SupplierList" Src="~/Controls/Default/Providers/Suppliers/SupplierList.ascx" %>

<asp:Content ID="Content" runat="server" ContentPlaceHolderID="uxMainContent">
	<Znode:SupplierList ID="ctlSupplierList" runat="server"
		ListPageUrl="~/Secure/Orders/SupplierManagement/Suppliers/Default.aspx"
		AddPageUrl="~/Secure/Orders/SupplierManagement/Suppliers/Add.aspx"
		DeletePageUrl="~/Secure/Orders/SupplierManagement/Suppliers/Delete.aspx" />
</asp:Content>
