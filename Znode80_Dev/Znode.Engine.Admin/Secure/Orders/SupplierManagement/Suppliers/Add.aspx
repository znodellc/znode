<%@ Page Language="C#" MasterPageFile="~/Themes/Standard/edit.master" AutoEventWireup="True" Inherits="Znode.Engine.Admin.Secure.Orders.SupplierManagement.Suppliers.Add" ValidateRequest="false" CodeBehind="Add.aspx.cs" %>
<%@ Register TagPrefix="zn" TagName="SupplierAdd" Src="~/Controls/Default/Providers/Suppliers/SupplierAdd.ascx" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="uxMainContent">
	<zn:SupplierAdd ID="ctlSupplierAdd" runat="server" />
</asp:Content>
