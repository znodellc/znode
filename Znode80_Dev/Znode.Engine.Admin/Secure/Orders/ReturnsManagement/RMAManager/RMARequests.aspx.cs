﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.Admin;
using System.Data;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Service;
using ZNode.Libraries.ECommerce.Payment;
using ZNode.Libraries.ECommerce.Entities;

namespace  Znode.Engine.Admin.Secure.Orders.ReturnsManagement.RMAManager
{
    public partial class RMARequests : System.Web.UI.Page
    {
        private int OrderID = 0;
        private int itemID = 0;
        private string mode = string.Empty;
        private int requestStatusID = 1;
        private int giftcardid = 0;
        private string flag = string.Empty;
        private string listlink = "~/Secure/Orders/ReturnsManagement/RMAManager/Default.aspx";
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if ((Request.Params["orderid"] != null) && (Request.Params["orderid"].Length != 0))
            {
                this.OrderID = int.Parse(Request.Params["orderid"].ToString());
                lblOrderID.Text = this.OrderID.ToString();
            }

            if ((Request.Params["itemid"] != null) && (Request.Params["itemid"].Length != 0))
            {
                this.itemID = int.Parse(Request.Params["itemid"].ToString());
            }

            if ((Request.Params["mode"] != null) && (Request.Params["mode"].Length != 0))
            {
                this.mode = Request.Params["mode"].ToString();
            }
            if ((Request.Params["giftcardid"] != null) && (Request.Params["giftcardid"].Length != 0))
            {
                this.giftcardid = int.Parse(Request.Params["giftcardid"].ToString());
            }
            if ((Request.Params["flag"] != null) && (Request.Params["flag"].Length != 0))
            {
                this.flag = Request.Params["flag"].ToString();
            }

            if (!IsPostBack)
            {
                lblRequestDate.Text = DateTime.Now.ToString("MM/dd/yyyy");
                Session["CHECKEDITEMS"] = null;
                this.txtComments.Attributes.Add("onKeyPress", "return textboxMultilineMaxNumber(this,500);");

                if (mode == "edit")
                {
                    BindEditData();
                }
                else
                {
                    BindGrid();
                    lblRMARequestHeader.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextRMARequest").ToString() + GenerateRequestNumber();
                    btnSubmit.Visible = true;
                    btnAuthorizeRMA.Visible = false;
                    btnDenyRMA.Visible = false;

                }
            }

        }

        #region Private & Public Methods

        public bool EnableCheck(object flag)
        {
            if (flag.ToString() == "True")
                return false;
            return true;
        }
        /// <summary>
        /// Send mail to user with 
        /// </summary>
        protected void SendMail_Giftcard()
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = ZNodeConfigManager.SiteConfig.StoreName;


            OrderAdmin _OrderAdmin = new OrderAdmin();
            Order orderObject = _OrderAdmin.GetOrderByOrderID(OrderID);

		

            RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.RMAConfiguration> rmaConfigs = rmaConfigAdmin.GetAllRMAConfiguration();
			ZNode.Libraries.DataAccess.Entities.RMAConfiguration rmaconfiguration = rmaConfigs[0];

            string messageText = "";

	        if (rmaconfiguration.EnableEmailNotification == true)
	        {
		        messageText += "Dear " + orderObject.BillingFirstName + " " + orderObject.BillingLastName + ",";
		        if (rmaConfigs.Count > 0)
		        {
			        messageText += "<br /><br />";
			        messageText += rmaConfigs[0].GCNotification;
			        messageText += "<br />";
		        }

		        messageText += "<br />";

                messageText += this.GetGlobalResourceObject("ZnodeAdminResource", "TextCardNumber").ToString() + lblCardNo.Text + "<br />";
                messageText += this.GetGlobalResourceObject("ZnodeAdminResource", "TextAmount").ToString() + lblGiftAmount.Text + "<br />";
                messageText += this.GetGlobalResourceObject("ZnodeAdminResource", "TextExpirationDate").ToString() + hdnExpiry.Value + "<br />";

		        messageText += "<br />";
		        if (rmaConfigs.Count > 0)
		        {

			        if (rmaconfiguration.EnableEmailNotification.HasValue && rmaconfiguration.EnableEmailNotification.Value &&
			            rmaconfiguration.Email.Length > 0)
			        {
				        senderEmail = rmaconfiguration.Email;
			        }
                    messageText += this.GetGlobalResourceObject("ZnodeAdminResource", "TextRegards").ToString() + "<br />";
			        messageText += ZNodeConfigManager.SiteConfig.StoreName + "<br />";
			        messageText += rmaconfiguration.DisplayName + "<br />";
			        messageText += rmaconfiguration.Address + "<br />";
			        messageText += rmaconfiguration.Email + "<br />";

		        }

		        try
		        {
			        // Send mail
			        ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(orderObject.BillingEmailId, senderEmail, String.Empty,
			                                                                subject, messageText, true);

		        }
		        catch (Exception)
		        {
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnabletosendmail").ToString(); 
		        }
	        }
        }


        /// <summary>
        /// Send mail to user with 
        /// </summary>
        protected void SendStatusMail(int RMAItemId)
        {
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = ZNodeConfigManager.SiteConfig.StoreName;
            string ReceiverMailId = "";
            RMARequestAdmin rmaAdmin = new RMARequestAdmin();
            DataSet reportds = rmaAdmin.GetRMARequestReport(RMAItemId);
            if (reportds.Tables[0].Rows.Count > 0)
            {
                string sign = string.Empty;
                if (reportds.Tables[0].Rows[0]["EnableEmailNotification"].ToString() == "True" && reportds.Tables[0].Rows[0]["CustomerNotification"].ToString().Length > 0)
                {
                    sign += this.GetGlobalResourceObject("ZnodeAdminResource", "TextRegards").ToString() + "<br />"; 
                    sign += ZNodeConfigManager.SiteConfig.StoreName + "<br />";
                    sign += reportds.Tables[0].Rows[0]["DepartmentName"].ToString() + "<br />";
                    sign += reportds.Tables[0].Rows[0]["DepartmentAddress"].ToString() + "<br />";
                    sign += reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString() + "<br />";

                    if (reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString().Length > 0)
                        senderEmail = reportds.Tables[0].Rows[0]["DepartmentEmail"].ToString();
                    ReceiverMailId = reportds.Tables[0].Rows[0]["BillingEmailId"].ToString();

                    string messageText = "";


                    messageText = "Dear " + reportds.Tables[0].Rows[0]["CustomerName"].ToString() + ", <br />";



                    messageText += "<br />";

                    messageText += reportds.Tables[0].Rows[0]["CustomerNotification"].ToString() + "<br />";

                    messageText += "<br />";
                    messageText += sign;


                    try
                    {
	                 		// Send mail
		                    ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(ReceiverMailId, senderEmail, String.Empty,
		                                                                            subject, messageText, true);
                    }
                    catch
                    {
                        lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnabletosendmail").ToString(); 
                    }
                }
            }
        }


        /// <summary>
        /// Auto Generate REquest Number 
        /// </summary>
        /// <returns></returns>
        private string GenerateRequestNumber()
        {
            //Format: [store id]-[yy]-xxx-zzzzz
            //yy: year
            //xxx: alpha characters
            //zzzzz: alphanumeric characters
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; 
            Random random = new Random();
            string xxx = new string(
                Enumerable.Repeat(chars, 3)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            string zzzzz = new string(
                 Enumerable.Repeat(chars, 5)
              .Select(s => s[random.Next(s.Length)])
              .ToArray());

            return ZNodeConfigManager.SiteConfig.PortalID + "-" + DateTime.Now.ToString("yy") + "-" + xxx + "-" + zzzzz;
        }
        /// <summary>
        /// Update RMA Request comments 
        /// </summary>
        private void UpdateComments()
        {
            //Update comments
            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
            RMARequest rmaRequest = rmaRequestAdmin.GetByRMARequestID(this.itemID);

            rmaRequest.Comments = Server.HtmlEncode(txtComments.Text);
            rmaRequestAdmin.Update(rmaRequest);
        }
        /// <summary>
        /// Calculate total price
        /// </summary>
        private void CalculateTotal()
        {
            foreach (GridViewRow row in uxGrid.Rows)
            {
                DropDownList ddl = (DropDownList)row.Cells[8].FindControl("uxQty");
                if (ddl != null && ddl.Items.Count > 0)
                {
					decimal price = Convert.ToDecimal(((System.Web.UI.DataBoundLiteralControl)(row.Cells[11].Controls[0])).Text.Trim().Replace("(", string.Empty).Replace(")", string.Empty).Replace(ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix(), " ").Trim());
					row.Cells[12].Text = String.Format("{0:c}", price * Convert.ToInt32(ddl.SelectedValue));
                }

            }

        }
        /// <summary>
        /// Get Reason for return name
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetReasonName(object id)
        {
            RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
            ReasonForReturn reasonForReturn = rmaConfigAdmin.GetByReasonFoReturnID(int.Parse(id.ToString()));

            if (reasonForReturn != null)
                return reasonForReturn.Name;
            else
                return string.Empty;

        }

        /// <summary>
        /// Calculate Sub total
        /// </summary>
        private void CalculateSubTotal()
        {
            RememberOldValues();
            Dictionary<int, List<string>> itemList = new Dictionary<int, List<string>>();


            if (Session["CHECKEDITEMS"] != null)
                itemList = (Dictionary<int, List<string>>)Session["CHECKEDITEMS"];

            decimal subtotal = 0.0m;
            decimal tax = 0.0m;
            decimal total = 0.0m;
            foreach (KeyValuePair<int, List<string>> pair in itemList)
            {
                List<string> values = pair.Value;
                int selectedId = pair.Key;
                subtotal += Convert.ToDecimal(values[1]);
                tax += Convert.ToDecimal(values[4]) * Convert.ToInt32(values[0]);
            }
            total = subtotal + tax;
            lblSubTotal.Text = String.Format("{0:c}", subtotal);
            lblTax.Text = String.Format("{0:c}", tax);
            lblTotal.Text = String.Format("{0:c}", total);

        }
        //Column Fields and index
        //0 - New Item (Check box) - chkItem
        //1 - edit item (check box) - chkReturnItem
        //2- OrderlineItem Id
        //3- Name
        //4-ProductNum
        //5-Description
        //6-SKU
        //7-RMAQuantity
        //8-Quantity - Dropdown
        //9-Reasonforreturn
        //10-ReasonforReturn Dropdown
        //11 - Price
        //12-Subtotal
        //13-MaxQuantity
        //14-RMAMaxQuantity
        //15-Salestax
        /// <summary>
        /// Bind Orderline item Grid
        /// </summary>       
        private void BindGrid()
        {
            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
            DataSet _OrderLineItems = rmaRequestAdmin.GetRMAOrderLineItemsByOrderID(this.OrderID, itemID, 0, flag);
            // ZNode.Libraries.Admin.OrderAdmin _OrderLineItemAdmin = new ZNode.Libraries.Admin.OrderAdmin();
            //TList<OrderLineItem> _OrderLineItems = _OrderLineItemAdmin.GetOrderLineItemByOrderID(this.OrderID);


            if (mode == "edit")
            {

                uxGrid.Columns[0].Visible = false;
                uxGrid.Columns[8].Visible = true;

                uxGrid.Columns[9].Visible = true;
                uxGrid.Columns[10].Visible = false;
                uxGrid.DataSource = _OrderLineItems.Tables[0];
                uxGrid.DataBind();
                CalculateTotal();
                CalculateSubTotal();

                //if( !uxGrid.Columns[1].Visible)
                //CalculateSubTotal();
                if (!uxGrid.Columns[0].Visible && !uxGrid.Columns[1].Visible)
                {
                    decimal subtotal = 0.0m;
                    decimal tax = 0.0m;
                    foreach (DataRow dr in _OrderLineItems.Tables[0].Rows)
                    {
                        subtotal += (Convert.ToDecimal(dr["Price"].ToString()) - Convert.ToDecimal(dr["DiscountAmount"].ToString())) * int.Parse(dr["RMAQuantity"].ToString());
                        tax += Convert.ToDecimal(dr["taxcost"].ToString()) * int.Parse(dr["RMAQuantity"].ToString());
                    }
                    lblSubTotal.Text = String.Format("{0:c}", subtotal);
                    lblTax.Text = String.Format("{0:c}", tax);
                    lblTotal.Text = String.Format("{0:c}", subtotal + tax);
                }

            }
            else
            {
                uxGrid.Columns[0].Visible = true;
                uxGrid.Columns[1].Visible = false;
                uxGrid.Columns[7].Visible = false;
                uxGrid.Columns[9].Visible = false;
                uxGrid.DataSource = _OrderLineItems;
                uxGrid.DataBind();
                CalculateTotal();
                CalculateSubTotal();

            }


            if (uxGrid.Rows.Count <= 1)
            {
                btnDeselectAll.Visible = false;
                btnSelectAll.Visible = false;
            }
            else
            {
                btnDeselectAll.Visible = true;
                btnSelectAll.Visible = true;
            }

        }

        /// <summary>
        /// Format Gift card data
        /// </summary>
        /// <param name="cardnumber"></param>
        /// <param name="amount"></param>
        /// <param name="expirydate"></param>
        /// <returns></returns>
        public string FormatGiftCard(object cardnumber, object amount, object expirydate)
        {
            return "Gift&nbsp;card&nbsp;issued:&nbsp;" + cardnumber.ToString() + "&nbsp;for&nbsp;" + String.Format("{0:c}", amount) + "<br />Expiration&nbsp;Date&nbsp;:&nbsp;" + Convert.ToDateTime(expirydate).ToString("MM/dd/yyyy");

        }

        /// <summary>
        /// Bind Edit page data
        /// </summary>
        private void BindEditData()
        {
            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
            RMARequest rmaRequest = rmaRequestAdmin.GetByRMARequestID(this.itemID);

            if (rmaRequest != null)
            {

                // ddlReason.SelectedValue = rmaRequest.ReasonForReturnID.ToString();
                lblRequestDate.Text = rmaRequest.RequestDate.ToString("MM/dd/yyyy");
                txtComments.Text = Server.HtmlDecode(rmaRequest.Comments);
                requestStatusID = rmaRequest.RequestStatusID.Value;

                btnSubmit.Visible = false;
                btnAuthorizeRMA.Visible = true;
                btnDenyRMA.Visible = true;
                if (flag == "append")
                {
                    lblRMARequestHeader.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextAppendRMA").ToString() +rmaRequest.RequestNumber;
                 }
                else
                    lblRMARequestHeader.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "TextRMARequest").ToString() + rmaRequest.RequestNumber;
                btnSelectAll.Enabled = false;
                btnDeselectAll.Enabled = false;
                btnVoid.Visible = false;
                btnRefund.Visible = false;
                btnIssueGC.Visible = false;

                OrderAdmin _OrderAdmin = new OrderAdmin();
                Order orderObject = _OrderAdmin.GetOrderByOrderID(OrderID);

                if (orderObject.PaymentTypeId == (int)ZNode.Libraries.ECommerce.Entities.PaymentType.CREDIT_CARD)
                {
                    StoreSettingsAdmin settingsAdmin = new StoreSettingsAdmin();
                    PaymentSetting paymentSetting = settingsAdmin.GetPaymentSettingByID(orderObject.PaymentSettingID.Value);
                    if (paymentSetting.IsRMACompatible.HasValue && paymentSetting.IsRMACompatible.Value)
                        btnRefund.Visible = true;
                }
             
                hdnpaymentid.Value = orderObject.PaymentTypeId.ToString();

                if (requestStatusID == Convert.ToInt32(ZNodeRequestState.Authorized))
                {
                    //GetStartedDialog.Show();
                 

                    btnSelectAll.Enabled = true;
                    btnDeselectAll.Enabled = true;
                    txtComments.Enabled = true;
                    uxGrid.Columns[1].Visible = true;
                    btnVoid.Visible = true;
                    btnIssueGC.Visible = true;

                    btnAuthorizeRMA.Visible = false;
                    btnDenyRMA.Visible = false;


                    hdnreturned.Value = "true";


                }
                else if (requestStatusID == Convert.ToInt32(ZNodeRequestState.ReturnedOrRefunded))
                {
                    DataSet giftDS = rmaRequestAdmin.GetGiftCardByRMARequest(itemID);
                    if (giftDS.Tables[0].Rows.Count > 0)
                    {
                        pnlGiftcard.Visible = true;
                        rptGiftCard.DataSource = giftDS.Tables[0];
                        rptGiftCard.DataBind();
                    }
                    btnDenyRMA.Visible = false;
                    btnVoid.Visible = false;
                    btnAuthorizeRMA.Visible = false;

                    if (flag == "append")
                    {
                        btnSelectAll.Enabled = true;
                        btnDeselectAll.Enabled = true;
                        txtComments.Enabled = true;
                        uxGrid.Columns[1].Visible = true;
                        btnVoid.Visible = false;
                        btnIssueGC.Visible = true;

                        btnAuthorizeRMA.Visible = false;
                        btnDenyRMA.Visible = false;

                        hdnreturned.Value = "true";
                    }
                    if (giftcardid > 0)
                    {
                        GiftCardAdmin giftcardAdmin = new GiftCardAdmin();
                        GiftCard giftcard = giftcardAdmin.GetByGiftCardId(giftcardid);
                        if (giftcard != null)
                        {
                            lblCardNo.Text = giftcard.CardNumber;
                            lblGiftAmount.Text = string.Format("{0:c}", giftcard.Amount);
                            hdnExpiry.Value = giftcard.ExpirationDate.Value.ToString("MM/dd/yyyy");
                        }
                        GetGiftCardDialog.Show();
                    }


                }
                BindGrid();


            }
        }
        /// <summary>
        /// Bind Reasons
        /// </summary>
        private void BindReasons(DropDownList ddlReason)
        {
            DataSet reasonForReturnList = null;

            if (Session["ReasonList"] == null)
            {
                RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
                reasonForReturnList = rmaConfigAdmin.GetReasonForReturn();
            }
            else
                reasonForReturnList = (DataSet)Session["ReasonList"];

            Session["ReasonList"] = reasonForReturnList;

            ddlReason.DataSource = reasonForReturnList;
            ddlReason.DataTextField = "Name";
            ddlReason.DataValueField = "ReasonForReturnID";
            ddlReason.DataBind();

            ListItem li = new ListItem();
            li.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "DropDownSelectOne").ToString();
            li.Value = "0";

            ddlReason.Items.Insert(0, li);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        public string GetPrice(object price, object discount)
        {
	        return String.Format("{0:c}", Convert.ToDecimal(price.ToString()) - decimal.Parse(discount.ToString()));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="price"></param>
        /// <returns></returns>
        public string GetTotal(object price, object discount, object qty)
        {
            if (int.Parse(qty.ToString()) > 0)
                return String.Format("{0:c}", (Convert.ToDecimal(price.ToString()) - Convert.ToDecimal(discount.ToString())) * int.Parse(qty.ToString()));
            else
                return string.Empty;
        }
        /// <summary>
        /// 
        /// </summary>
        private void RePopulateValues()
        {
            Dictionary<int, List<string>> itemIdList = (Dictionary<int, List<string>>)Session["CHECKEDITEMS"];

            if (mode == string.Empty)
                RePopulateDropdownValues();

            if (itemIdList != null && itemIdList.Count > 0)
            {
                foreach (GridViewRow row in uxGrid.Rows)
                {
                    int index = Convert.ToInt32(row.Cells[2].Text);
                    if (itemIdList.ContainsKey(index))
                    {
                        if (mode == "edit")
                        {
                            CheckBox myCheckBox = (CheckBox)row.FindControl("chkReturnItem");
                            myCheckBox.Checked = true;
                        }
                        else
                        {
                            CheckBox myCheckBox = (CheckBox)row.FindControl("chkItem");
                            myCheckBox.Checked = true;
                        }
                    }
                }
            }

        }


        /// <summary>
        /// Remember Old Values Method
        /// </summary>
        private void RememberOldValues()
        {
            //Dictionary values - List of strings
            //0 - Quantiy
            //1 - Total price
            //2 - Reason

            Dictionary<int, List<string>> itemIdList = new Dictionary<int, List<string>>();


            // Loop through the grid values
            foreach (GridViewRow row in uxGrid.Rows)
            {
                CheckBox check;
                if (mode == "edit")
                    check = (CheckBox)row.Cells[1].FindControl("chkReturnItem") as CheckBox;
                else
                    check = (CheckBox)row.Cells[0].FindControl("chkItem") as CheckBox;

                // Check in the Session
                if (Session["CHECKEDITEMS"] != null)
                {
                    itemIdList = (Dictionary<int, List<string>>)Session["CHECKEDITEMS"];
                }

                int id = int.Parse(row.Cells[2].Text);

                if (check.Checked && check.Enabled)
                {
                    DropDownList ddlqty = (DropDownList)row.Cells[8].FindControl("uxQty");
                    DropDownList ddlReason = (DropDownList)row.Cells[10].FindControl("uxReason");
                    string totalAmount = "";
                    string reasoncode = "";
                    string price = "";
                    string tax = row.Cells[15].Text;

                    if (mode == string.Empty)
                    {
                        totalAmount = row.Cells[12].Text.Replace('$', ' ').Trim();
                        reasoncode = ddlReason.SelectedValue;
                        price = ((System.Web.UI.DataBoundLiteralControl)(row.Cells[11].Controls[0])).Text.Replace(ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix(), "").Trim();

                    }
                    else
                    {
                        try
                        {
                            totalAmount = ((System.Web.UI.DataBoundLiteralControl)(row.Cells[12].Controls[0])).Text.Replace(ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix(), "").Trim();
                        }
                        catch
                        {
                            totalAmount = row.Cells[12].Text.Replace('$', ' ').Trim();
                        }
                    } 

                    if (!itemIdList.ContainsKey(id))
                    {
                        List<string> myValues = new List<string>();
                        myValues.Add(ddlqty.SelectedValue.ToString());
                        myValues.Add(totalAmount);
                        myValues.Add(reasoncode);
                        myValues.Add(price);
                        myValues.Add(tax);
                        itemIdList.Add(id, myValues);

                    }
                    else
                    {
                        List<string> existingvalue = itemIdList[id];
                        existingvalue[0] = ddlqty.SelectedValue.ToString();
                        existingvalue[1] = totalAmount;
                        existingvalue[2] = reasoncode;
                    }

                }
                else
                {
                    itemIdList.Remove(id);
                }
            }

            if (itemIdList.Count > 0)
            {
                Session["CHECKEDITEMS"] = itemIdList;
            }

        }

        /// <summary>
        /// 
        /// </summary>
        private void RePopulateDropdownValues()
        {
            Dictionary<int, List<string>> qtyList = (Dictionary<int, List<string>>)Session["CHECKEDITEMS"];
            if (qtyList != null && qtyList.Count > 0)
            {
                foreach (GridViewRow row in uxGrid.Rows)
                {
                    int index = Convert.ToInt32(row.Cells[2].Text);
                    if (qtyList.ContainsKey(index))
                    {
                        List<string> values = qtyList[index];
                        DropDownList ddlqty = (DropDownList)row.Cells[8].FindControl("uxQty");
                        ddlqty.SelectedValue = values[0];
                        decimal price = Convert.ToDecimal(((System.Web.UI.DataBoundLiteralControl)(row.Cells[11].Controls[0])).Text.Replace(ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix(), "").Trim());
                        row.Cells[12].Text = String.Format("{0:c}", price * Convert.ToInt32(ddlqty.SelectedValue));
                        DropDownList ddlReason = (DropDownList)row.Cells[8].FindControl("uxReason");
                        ddlReason.SelectedValue = values[2];

                    }
                }
            }
        }

        #endregion


        #region Button click Events
      
        /// <summary>
        /// GC No Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnGCNo_Click(object sender, EventArgs e)
        {
            Response.Redirect(listlink);
        }

        /// <summary>
        /// GC Yes Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnGCYes_Click(object sender, EventArgs e)
        {
            SendMail_Giftcard();
            Response.Redirect(listlink);
        }
    
        /// <summary>
        ///  Select all Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnSelectAll_Click(object sender, EventArgs e)
        {
            lblErrorMsg.Text = "";
            RememberOldValues();
            foreach (GridViewRow row in uxGrid.Rows)
            {
                if (mode != "edit")
                {
                    CheckBox chk = (CheckBox)row.Cells[0].FindControl("chkItem");
                    if (chk.Enabled)
                        chk.Checked = true;
                }

                if (Convert.ToBoolean(hdnreturned.Value))
                {
                    CheckBox chk = (CheckBox)row.Cells[1].FindControl("chkReturnItem");
                    if (chk.Enabled)
                        chk.Checked = true;
                }
            }
            CalculateTotal();
            CalculateSubTotal();
        }
        /// <summary>
        ///  Deselect all Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnDeselectAll_Click(object sender, EventArgs e)
        {
            lblErrorMsg.Text = "";
            RememberOldValues();
            foreach (GridViewRow row in uxGrid.Rows)
            {
                if (mode != "edit")
                {
                    CheckBox chk = (CheckBox)row.Cells[0].FindControl("chkItem");
                    if (chk.Enabled)
                        chk.Checked = false;
                }

                if (Convert.ToBoolean(hdnreturned.Value))
                {
                    CheckBox chk = (CheckBox)row.Cells[1].FindControl("chkReturnItem");
                    if (chk.Enabled)
                        chk.Checked = false;
                }


            }
            CalculateTotal();
            CalculateSubTotal();

        }

        /// <summary>
        ///  Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            if (mode == "edit")  
                Response.Redirect("~/Secure/Orders/ReturnsManagement/RMAManager/Default.aspx");
            else
                Response.Redirect("~/Secure/Orders/OrderManagement/ViewOrders/Default.aspx");
        }
        /// <summary>
        ///  Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            if (Page.IsValid)
            {
                lblErrorMsg.Text = "";
                RememberOldValues();

                Dictionary<int, List<string>> itemIdList = new Dictionary<int, List<string>>();
                if (Session["CHECKEDITEMS"] != null)
                    itemIdList = (Dictionary<int, List<string>>)Session["CHECKEDITEMS"];


                if (itemIdList.Count == 0)
                {
                    lblErrorMsg.Text = "*At least one item must be selected to complete this Return Merchandise Authorization request.";
                    CalculateTotal();
                    return;
                }
                else
                {
                    foreach (KeyValuePair<int, List<string>> pair in itemIdList)
                    {
                        List<string> values = pair.Value;
                        if (int.Parse(values[2]) == 0)
                        {
                            CalculateTotal();
                            lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorMustSelectReturnReason").ToString(); 
                            return;
                        }
                    }
                }


                GetAuthorizeDialog.Show();
            }

        }

        /// <summary>
        ///  Deny RMA Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnDenyRMA_Click(object sender, EventArgs e)
        {
            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
            RMARequest rmaRequest = rmaRequestAdmin.GetByRMARequestID(this.itemID);

            if (rmaRequest != null)
            {
                rmaRequest.RequestStatusID = Convert.ToInt32(ZNodeRequestState.Void);
                rmaRequest.Comments = Server.HtmlEncode(txtComments.Text);
            }
            rmaRequestAdmin.Update(rmaRequest);
            Response.Redirect("~/Secure/Orders/ReturnsManagement/RMAManager/Default.aspx");
        }

        /// <summary>
        ///  Authorize RMA Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnAuthorizeRMA_Click(object sender, EventArgs e)
        {

            GetAuthorizeDialog.Show();
        }
        /// <summary>
        ///  OK Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnOk_Click(object sender, EventArgs e)
        {
            RememberOldValues();

            Dictionary<int, List<string>> itemIdList = new Dictionary<int, List<string>>();
            if (Session["CHECKEDITEMS"] != null)
                itemIdList = (Dictionary<int, List<string>>)Session["CHECKEDITEMS"];


            if (itemIdList.Count <= 0)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAtleastOneMustSelect").ToString(); 
                return;
            }
            CalculateTotal();
            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
            RMARequest rmaRequest = new RMARequest();
            rmaRequest.RequestDate = DateTime.Now;
            //rmaRequest.ReasonForReturnID = Convert.ToInt32(ddlReason.SelectedValue);
            rmaRequest.Comments = Server.HtmlEncode(txtComments.Text);
            rmaRequest.RequestStatusID = Convert.ToInt32(ZNodeRequestState.Authorized);
            rmaRequest.UpdatedDate = DateTime.Now;
            rmaRequest.RequestNumber = lblRMARequestHeader.Text.Replace(this.GetGlobalResourceObject("ZnodeAdminResource", "TextRMARequest").ToString(), "");

            rmaRequest.TaxCost = Convert.ToDecimal(lblTax.Text.Replace(ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix(), ""));
            rmaRequest.SubTotal = Convert.ToDecimal(lblSubTotal.Text.Replace(ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix(), "").Trim());
            rmaRequest.Total = Convert.ToDecimal(lblTotal.Text.Replace(ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix(), "").Trim());
            int rmaRequestID = 0;
             
            bool status = false;
            status = rmaRequestAdmin.Add(rmaRequest, out rmaRequestID);


            foreach (KeyValuePair<int, List<string>> pair in itemIdList)
            {
                List<string> values = pair.Value;
                int orderlineitemid = pair.Key;
                RMARequestItem item = new RMARequestItem();
                item.RMARequestID = rmaRequestID;
                item.OrderLineItemID = orderlineitemid;
                item.Quantity = int.Parse(values[0]);
                item.Price = Convert.ToDecimal(values[3]);
                item.ReasonForReturnID = int.Parse(values[2]);
                rmaRequestAdmin.Add(item);

            }
            Session.Remove("CHECKEDITEMS");

		
            SendStatusMail(rmaRequestID);

            Response.Redirect("~/Secure/Orders/ReturnsManagement/RMAManager/Default.aspx?itemid=" + rmaRequestID + "&orderid=" + OrderID + "&Email=" + chkEmail.Checked);
        }


        /// <summary>
        ///  Void  Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnVoid_Click(object sender, EventArgs e)
        {
            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
            RMARequest rmaRequest = rmaRequestAdmin.GetByRMARequestID(this.itemID);

            if (rmaRequest != null)
            {
                rmaRequest.Comments = Server.HtmlEncode(txtComments.Text);
                rmaRequest.RequestStatusID = Convert.ToInt32(ZNodeRequestState.Void); ;

            }
            rmaRequestAdmin.Update(rmaRequest);
	
		    SendStatusMail(this.itemID);
	        Response.Redirect("~/Secure/Orders/ReturnsManagement/RMAManager/Default.aspx");
        }

        private void Refund(string rmaitems, string qtylist, decimal amount)
        {
            // Retrieve order
            ZNode.Libraries.Admin.OrderAdmin orderAdmin = new ZNode.Libraries.Admin.OrderAdmin();
            Order order = orderAdmin.GetOrderByOrderID(this.OrderID);
            ZNode.Libraries.Framework.Business.ZNodeEncryption enc = new ZNode.Libraries.Framework.Business.ZNodeEncryption();

            // Get payment settings
            int paymentSettingID = (int)order.PaymentSettingID;
            PaymentSettingService pss = new PaymentSettingService();
            PaymentSetting ps = pss.GetByPaymentSettingID(paymentSettingID);

            // Set gateway info
            GatewayInfo gi = new GatewayInfo();
            gi.GatewayLoginID = enc.DecryptData(ps.GatewayUsername);
            gi.GatewayPassword = enc.DecryptData(ps.GatewayPassword);
            gi.TransactionKey = enc.DecryptData(ps.TransactionKey);
            gi.Vendor = ps.Vendor;
            gi.Partner = ps.Partner;
            gi.TestMode = ps.TestMode;
            gi.Gateway = (GatewayType)ps.GatewayTypeID;

            string creditCardExp = Convert.ToString(order.CardExp);
            if (creditCardExp == null)
            {
                creditCardExp = string.Empty;
            }

            // Set credit card
            CreditCard cc = new CreditCard();
            cc.Amount = amount;
            cc.OrderID = order.OrderID;
            cc.TransactionID = order.CardTransactionID;

            GatewayResponse resp = new GatewayResponse();

            if ((GatewayType)ps.GatewayTypeID == GatewayType.AUTHORIZE)
            {
                GatewayAuthorize auth = new GatewayAuthorize();
                resp = auth.RefundPayment(gi, cc);
            }
            else
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorNotSupportCreditCartRefunds").ToString(); 
                return;
            }

            if (resp.IsSuccess)
            {

                RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
                RMARequest rmaRequest = rmaRequestAdmin.GetByRMARequestID(this.itemID);

                if (rmaRequest != null)
                {
                    rmaRequest.RequestStatusID = Convert.ToInt32(ZNodeRequestState.ReturnedOrRefunded); ;
                    rmaRequest.Comments = Server.HtmlEncode(txtComments.Text);
                }
                rmaRequestAdmin.Update(rmaRequest);


                string[] lineitems = rmaitems.Split(',');
                string[] qtyItems = qtylist.Split(',');
                //Add RMAREquestItem with Refund Transaction id
                for (int i = 0; i < lineitems.Length; i++)
                {
                    RMARequestItem item = new RMARequestItem();
                    int orderlineitemid = Convert.ToInt32(lineitems[i]);
                    item.RMARequestID = this.itemID;
                    item.Quantity = Convert.ToInt32(qtyItems[i]);
                    item.OrderLineItemID = orderlineitemid;
                    item.IsReturnable = true;
                    item.TansactionId = resp.TransactionId;
                    item.IsReceived = true;
                    rmaRequestAdmin.Add(item);

                }

                SendStatusMail(this.itemID);
                // Response.Redirect("~/Secure/Orders/Returns/RMA/RMAManager.aspx?mode=edit&itemid=" + rmaid.ToString() + "&orderid=" + OrderID.ToString());
                Response.Redirect("~/Secure/Orders/ReturnsManagement/RMAManager/Default.aspx");


            }
            else
            {
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                sb.Append(this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorCouldNotCompleteRequest").ToString());
                sb.Append(resp.ResponseText);
                lblErrorMsg.Text = sb.ToString();
            }
        }
        /// <summary>
        ///  Refund RMA Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnRefund_Click(object sender, EventArgs e)
        {
            RememberOldValues();
            CalculateTotal();
            decimal amount = 0.0m;
            Dictionary<int, List<string>> itemIdList = new Dictionary<int, List<string>>();

            if (Session["CHECKEDITEMS"] != null)
                itemIdList = (Dictionary<int, List<string>>)Session["CHECKEDITEMS"];


            if (itemIdList.Count == 0)
            {
                if (flag == "append")
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAtleastOneItemSelect").ToString();
                else
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorMustSelectOneItemforRefund").ToString();
                return;
            }

            string litemid = string.Empty;
            string qtyList = string.Empty;
            foreach (KeyValuePair<int, List<string>> pair in itemIdList)
            {
                List<string> values = pair.Value;
                int selectedId = pair.Key;
                litemid += selectedId + ",";
                qtyList += values[0].ToString() + ",";
            }

            amount = Convert.ToDecimal(lblTotal.Text.Replace(ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix(), "").Trim());



            if (litemid.Length > 0)
            {
                litemid = litemid.Remove(litemid.Length - 1);
                qtyList = qtyList.Remove(qtyList.Length - 1);
            }


            if (amount == 0.0m)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSelectedItemPriceGreaterThanZero").ToString();
                return;
            }

            Session.Remove("CHECKEDITEMS");

            //this.UpdateComments();
            this.Refund(litemid, qtyList, amount);
        }

        /// <summary>
        ///  Issue gift card RMA Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnIssueGC_Click(object sender, EventArgs e)
        {

            RememberOldValues();
            CalculateTotal();
            DateTime ExpiryDate = DateTime.Now;

            decimal amount = 0.0m;
            Dictionary<int, List<string>> itemIdList = new Dictionary<int, List<string>>();

            if (Session["CHECKEDITEMS"] != null)
                itemIdList = (Dictionary<int, List<string>>)Session["CHECKEDITEMS"];



            if (itemIdList.Count == 0)
            {
                if (flag == "append")
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAtleastOneItemSelect").ToString();
                else
                    lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorAtleastOneItemSelectForGiftCard").ToString();
                return;
            }
            string litemid = string.Empty;
            string qtyList = string.Empty;
            foreach (KeyValuePair<int, List<string>> pair in itemIdList)
            {
                List<string> values = pair.Value;
                int selectedId = pair.Key;
                litemid += selectedId + ",";

                qtyList += values[0].ToString() + ",";
            }

            if (litemid.Length > 0)
            {
                litemid = litemid.Remove(litemid.Length - 1);
                qtyList = qtyList.Remove(qtyList.Length - 1);
            }

            amount = Convert.ToDecimal(lblTotal.Text.Replace(ZNode.Libraries.ECommerce.Catalog.ZNodeCurrencyManager.GetCurrencyPrefix(), "").Trim());
            if (amount == 0.0m)
            {
                lblErrorMsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorSelectedItemPriceGreaterThanZero").ToString();
                return;
            }
            Session.Remove("CHECKEDITEMS");
            this.UpdateComments();

            if (flag == "append")
                Response.Redirect("~/Secure/Marketing/Promotions/GiftCards/Add.aspx?mode=RMA&item=" + this.itemID.ToString() + "&items=" + litemid + "&qty=" + qtyList + "&orderid=" + OrderID + "&flag=append" + "&amount=" + string.Format("{0:0.00}", amount));
            Response.Redirect("~/Secure/Marketing/Promotions/GiftCards/Add.aspx?mode=RMA&item=" + this.itemID.ToString() + "&items=" + litemid + "&qty=" + qtyList + "&orderid=" + OrderID + "&amount=" + string.Format("{0:0.00}", amount));
        }

        #endregion


        #region Gridview Events
        /// <summary>
        /// Request Status Row
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_RowCreated(object sender, GridViewRowEventArgs e)
        {


        }
        /// <summary>
        /// Request Status Row
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow || e.Row.RowType == DataControlRowType.Header || e.Row.RowType == DataControlRowType.Footer)
            {
                e.Row.Cells[7].Visible = false;
                e.Row.Cells[13].Visible = false;
                e.Row.Cells[14].Visible = false;
                e.Row.Cells[15].Visible = false;
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                int orderlineitemid = int.Parse(e.Row.Cells[2].Text);
                int maxQty = 0;
                int rmamaxqty = int.Parse(e.Row.Cells[14].Text);
                int rmaqty = 0;
                if (e.Row.Cells[7].Text != "")
                    rmaqty = int.Parse(e.Row.Cells[7].Text);

                if (mode == "")
                    maxQty = int.Parse(e.Row.Cells[13].Text) - rmamaxqty;
                else
                    maxQty = rmamaxqty - rmaqty;



                DropDownList ddlQty = (DropDownList)e.Row.Cells[8].FindControl("uxQty");
                if (maxQty == 0 && flag == "append")
                {
                    maxQty = rmamaxqty;
                    ddlQty.Enabled = false;
                    CheckBox chk = (CheckBox)e.Row.Cells[1].FindControl("chkReturnItem");
                    chk.Checked = true;
                    chk.Enabled = false;
                }

                for (int i = 1; i <= maxQty; i++)
                {
                    ddlQty.Items.Add(i.ToString());
                }

                ddlQty.SelectedValue = maxQty.ToString();


                if (mode == "")
                {
                    DropDownList ddlreason = (DropDownList)e.Row.Cells[10].FindControl("uxReason");
                    BindReasons(ddlreason);
                }


            }

        }
        /// <summary>
        /// Order Items Grid Page Index Changing Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void UxGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.RememberOldValues();
            uxGrid.PageIndex = e.NewPageIndex;
            this.BindGrid();
            RePopulateValues();
        }
        #endregion

        #region Checkbox and Dropdown Events
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chk_CheckedChanged(object sender, EventArgs e)
        {
            if (chkEmail.Checked || chkPrint.Checked)
                btnOk.Enabled = true;
            else
                btnOk.Enabled = false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkItem_CheckedChanged(object sender, EventArgs e)
        {
            RememberOldValues();
            CalculateTotal();
            CalculateSubTotal();
        }
        /// <summary>
        /// DropDown Index change event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void Quantity_SelectedIndexChanged(object sender, EventArgs e)
        {
            CalculateTotal();
            CalculateSubTotal();
        }
        #endregion
    }
}