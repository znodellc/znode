﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Orders.ReturnsManagement.RMAManager.Default" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="uxMainContent">
    <div>
        <div>
            <div class="LeftFloat" style="width: 70%; text-align: left">
                <h1>
                    <asp:Localize ID="TitleRMAConfiguration" runat="server" Text='<%$ Resources:ZnodeAdminResource, LinkTextRMAManager %>'></asp:Localize></h1>
                <p>
                    <asp:Localize ID="TextRMAManager" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextRMAManager %>'></asp:Localize>
                </p>
            </div>
            <div align="left">
                <h4 class="SubTitle">
                    <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleSearchRMA %>'></asp:Localize><span style="text-transform: lowercase;">s</span></h4>
                <asp:Panel ID="Test" DefaultButton="btnSearch" runat="server">
                    <div class="SearchForm">
                        <div class="RowStyle">
                            <div class="ItemStyle">
                                <span class="FieldStyle">
                                    <asp:Localize ID="ColumnTitleStoreName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStoreName %>'></asp:Localize></span><br />
                                <span class="ValueStyle">
                                    <asp:DropDownList ID="ddlPortal" runat="server">
                                    </asp:DropDownList>
                                </span>
                            </div>
                            <div class="RowStyle">
                                <div class="ItemStyle">
                                    <span class="FieldStyle">
                                        <asp:Localize ID="ColumnTitleRMAID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleRMAID %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtRMAID" runat="server"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="regRMAID" runat="server" ControlToValidate="txtRMAID"
                                            Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularEntervalidRMAID %>' SetFocusOnError="true"
                                            ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularExpressionID %>' ValidationGroup="grpSearch" CssClass="Error"></asp:RegularExpressionValidator>
                                    </span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">
                                        <asp:Localize ID="Localize3" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderID %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtorderid" runat="server"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtorderid"
                                            Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, ValidOrderId %>' SetFocusOnError="true"
                                            ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularExpressionID %>' ValidationGroup="grpSearch" CssClass="Error"></asp:RegularExpressionValidator>
                                    </span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">
                                        <asp:Localize ID="FirstName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnFirstName %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtfirstname" runat="server"></asp:TextBox></span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">
                                        <asp:Localize ID="LastName" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnLastName %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtlastname" runat="server"></asp:TextBox></span>
                                </div>
                            </div>

                            <div class="RowStyle">
                                <div class="ItemStyle">
                                    <span class="FieldStyle">
                                        <asp:Localize ID="TitleBeginDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleBeginDate %>'></asp:Localize></span>
                                    <br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtStartDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="imgbtnStartDt"
                                            runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" /><br />

                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtStartDate"
                                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularStartDate %>'
                                            ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularProfileEffectiveStartDate %>'
                                            ValidationGroup="grpSearch"></asp:RegularExpressionValidator>
                                        <ajaxToolKit:CalendarExtender ID="CalendarExtender1" Enabled="true" PopupButtonID="imgbtnStartDt"
                                            runat="server" TargetControlID="txtStartDate">
                                        </ajaxToolKit:CalendarExtender>
                                    </span>
                                </div>
                                <div class="ItemStyle">
                                    <span class="FieldStyle">
                                        <asp:Localize ID="ColumnEndDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnEndDate %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:TextBox ID="txtEndDate" Text='' runat="server" />&nbsp;<asp:ImageButton ID="ImgbtnEndDt"
                                            runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Themes/images/SmallCalendar.gif" /><br />

                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtEndDate"
                                            CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, RegularStartDate %>'
                                            ValidationExpression='<%$ Resources:ZnodeAdminResource, RegularProfileEffectiveStartDate %>'
                                            ValidationGroup="grpSearch"></asp:RegularExpressionValidator>
                                        <ajaxToolKit:CalendarExtender ID="CalendarExtender2" Enabled="true" PopupButtonID="ImgbtnEndDt"
                                            runat="server" TargetControlID="txtEndDate">
                                        </ajaxToolKit:CalendarExtender>
                                        <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtStartDate"
                                            ValidationGroup="grpSearch"
                                            ControlToValidate="txtEndDate" CssClass="Error" Display="Dynamic" ErrorMessage='<%$ Resources:ZnodeAdminResource, CompareBeginDate %>'
                                            Operator="GreaterThanEqual" Type="Date"></asp:CompareValidator>
                                    </span>
                                </div>

                                <div class="ItemStyle">
                                    <span class="FieldStyle">
                                        <asp:Localize ID="ColumnTitleStatus" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus %>'></asp:Localize></span><br />
                                    <span class="ValueStyle">
                                        <asp:DropDownList ID="listRequestStatus" runat="server">
                                        </asp:DropDownList>
                                    </span>
                                </div>
                            </div>
                            <div class="ClearBoth">
                                <zn:Button ID="btnClear" runat="server" ButtonType="CancelButton" CausesValidation="false" OnClick="BtnClearSearch_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonClear %>" />
                                <zn:Button ID="btnSearch" runat="server" ButtonType="SubmitButton" CausesValidation="True" OnClick="BtnSearch_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSearch %>" ValidationGroup="grpSearch" />

                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <br />
                <h4 class="SubTitle">
                    <asp:Localize ID="SubTitleRMAList" runat="server" Text='<%$ Resources:ZnodeAdminResource, SubTitleRMAList %>'></asp:Localize>
                </h4>

            </div>
            <div>
                <asp:Label ID="lblmsg" runat="server" CssClass="Error"></asp:Label>
            </div>
            <div>
                <asp:GridView ID="uxGrid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                    CaptionAlign="Left" CellPadding="4" CssClass="Grid" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridRMAEmptyData %>'
                    EnableSortingAndPagingCallbacks="False" GridLines="None" OnPageIndexChanging="UxGrid_PageIndexChanging"
                    OnRowCommand="UxGrid_RowCommand" PageSize="25" Width="100%">
                    <Columns>
                        <asp:HyperLinkField DataNavigateUrlFields="RMARequestID,OrderID" DataNavigateUrlFormatString='ViewRMARequest.aspx?itemid={0}&orderid={1}'
                            DataTextField="RMARequestID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleRMAID %>' SortExpression="RMARequestID" HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="OrderID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleOrderID %>' HeaderStyle-HorizontalAlign="Left" />
                        <asp:BoundField DataField="RequestDate" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleDate %>' SortExpression="OrderDate" ItemStyle-Width="120"
                            DataFormatString="{0:MM/dd/yyyy}" HtmlEncode="False" HeaderStyle-HorizontalAlign="Left" />
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStore %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <%# GetStoreName(DataBinder.Eval(Container.DataItem,"PortalID").ToString()) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="BillingFirstName" HeaderStyle-HorizontalAlign="Left">
                            <HeaderTemplate>
                                <asp:Label ID="headerTotal" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleCustomerName %>' runat="server"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="CustomerName" Text='<%# ReturnName(Eval("BillingFirstName"),Eval("BillingLastName")) %>'
                                    runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField SortExpression="OrderStateID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleStatus %>' HeaderStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:Label ID="RequestStatus" Text='<%# Eval("RequestStatus") %>'
                                    runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField> 
                        <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleActions %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="220px">
                            <ItemTemplate>
                                <%# GetViewURL(DataBinder.Eval(Container.DataItem,"RMARequestID"),DataBinder.Eval(Container.DataItem,"OrderId")) %>
                                <%# GetEditURL(DataBinder.Eval(Container.DataItem,"RMARequestID"),DataBinder.Eval(Container.DataItem,"OrderId"),DataBinder.Eval(Container.DataItem,"RequestStatus")) %>
                                <%# GetAppendURL(DataBinder.Eval(Container.DataItem,"RMARequestID"),DataBinder.Eval(Container.DataItem,"OrderId"),DataBinder.Eval(Container.DataItem,"RequestStatus")) %>
                                <%# GetDeleteURL(DataBinder.Eval(Container.DataItem,"RMARequestID"),DataBinder.Eval(Container.DataItem,"RequestNumber")) %>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <FooterStyle CssClass="FooterStyle" />
                    <RowStyle CssClass="RowStyle" />
                    <PagerStyle CssClass="PagerStyle" Font-Underline="True" />
                    <HeaderStyle CssClass="HeaderStyle" />
                    <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                    <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" />
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
