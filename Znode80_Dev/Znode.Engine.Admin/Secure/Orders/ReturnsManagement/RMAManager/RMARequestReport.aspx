﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="RMARequestReport.aspx.cs"
    Inherits="Znode.Engine.Admin.Secure.Orders.ReturnsManagement.RMAManager.RMARequestReport" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<html>
<head>
    <title><asp:Localize ID="TitleRMARequestReport" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleRMARequestReport %>'></asp:Localize></title>
    <meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />

    <style type="text/css">
        #ImageButtons .editbutton
        {
            background-color:#e17c39;
	        -webkit-border-top-left-radius:5px;
	        -moz-border-radius-topleft:5px;
	        border-top-left-radius:5px;
	        -webkit-border-top-right-radius:5px;
	        -moz-border-radius-topright:5px;
	        border-top-right-radius:5px;
	        -webkit-border-bottom-right-radius:5px;
	        -moz-border-radius-bottomright:5px;
	        border-bottom-right-radius:5px;
	        -webkit-border-bottom-left-radius:5px;
	        -moz-border-radius-bottomleft:5px;
	        border-bottom-left-radius:5px;
	        text-indent:0;
	        display:inline-block;
	        color:#ffffff;
	        font-family:Arial, Calibri, Sans-Serif;
	        font-size:11px;
	        font-weight:normal;
	        font-style:normal;
	        height:19px;
	        line-height:15px;
	        width:auto;
	        text-decoration:none;
	        text-align:center;
            border: none;
            cursor: pointer;
            vertical-align: top;
            text-align: center;
            text-transform: uppercase;
        }

            #ImageButtons .editbutton:hover
            {
                background-color:#b34d04;
            }

        .Msg
        {
            font-family: arial,Calibri;
            font-size: 12px;
        }
    </style>
    <script type="Text/JavaScript">
        function WindowClose() {
            window.close();
        }
    </script>

</head>
<body>
    <form runat="server" id="rmareport">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true">
            <Scripts>
                <asp:ScriptReference Path="~/js/Safari3AjaxHack.js" />
            </Scripts>
        </asp:ScriptManager>
        <div>
            <div id="ImageButtons" style="text-align: right;">

           <zn:Button ID="btnSendMail" runat="server"  Width="100px" ButtonType="EditButton" CausesValidation="true" OnClick="btnSendMail_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSendMail %>" />
           <zn:Button ID="Button1" runat="server"  Width="100px" ButtonType="EditButton" CausesValidation="true" OnClientClick=WindowClose() Text="<%$ Resources:ZnodeAdminResource, ButtonClosewithoutX %>" />
          
            </div>
            <div>
                <ZNode:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <asp:Label ID="lblmsg" runat="server" CssClass="Msg"></asp:Label>
            </div>
            <div>
                <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>
            <div>
                <rsweb:ReportViewer ShowBackButton="False" ID="objReportViewer" runat="server" Width="100%" AsyncRendering="false" Height="650px"
                    Font-Names="Verdana" Font-Size="8pt" ShowExportControls="true" ShowPageNavigationControls="true"
                    ShowCredentialPrompts="false" ShowDocumentMapButton="false" ShowFindControls="false"
                    ShowPrintButton="true" ShowRefreshButton="false" ShowZoomControl="false">
                </rsweb:ReportViewer>
            </div>
        </div>
    </form>
</body>
</html>
