﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using ZNode.Libraries.Admin;
using Microsoft.Reporting.WebForms;
using ZNode.Libraries.Framework.Business;
using ZNode.Libraries.DataAccess.Entities;
using ZNode.Libraries.ECommerce.Catalog;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Net.Mail;
using System.Net.Mime;


namespace  Znode.Engine.Admin.Secure.Orders.ReturnsManagement.RMAManager
{
    public partial class RMARequestReport : System.Web.UI.Page
    {
        private int itemID = 0;
        private int OrderID = 0;

       

        protected void Page_Load(object sender, EventArgs e)
        {

            if ((Request.Params["itemid"] != null) && (Request.Params["itemid"].Length != 0))
            {
                this.itemID = int.Parse(Request.Params["itemid"].ToString());

            }
            if ((Request.Params["orderid"] != null) && (Request.Params["orderid"].Length != 0))
            {
                this.OrderID = int.Parse(Request.Params["orderid"].ToString());
            }

            if ((Request.Params["Email"] != null) && (Request.Params["Email"].Length != 0))
            {
                if (bool.Parse(Request.Params["Email"].ToString()))
                    btnSendMail.Visible = true;
                else
                    btnSendMail.Visible = false;
            }
            RMARequestAdmin rmarequestAdmin = new RMARequestAdmin();

            DataSet reportDataSet = null;
            objReportViewer.Reset();
            objReportViewer.LocalReport.DataSources.Clear();
            reportDataSet = rmarequestAdmin.GetRMARequestReport(itemID);
            this.objReportViewer.PageCountMode = PageCountMode.Actual; 
            string barcodePath = string.Empty;
            string data = string.Empty;
            if (reportDataSet.Tables[0].Rows.Count > 0)
            {
                barcodePath = Page.ResolveClientUrl(ZNodeConfigManager.EnvironmentConfig.DataPath) + "Images\\Barcode\\";

                if (!Directory.Exists(barcodePath))
                    Directory.CreateDirectory(barcodePath);

                if (System.Configuration.ConfigurationManager.AppSettings["BarcodePath"] != null)
                    barcodePath = System.Configuration.ConfigurationManager.AppSettings["BarcodePath"].ToString();

                data = reportDataSet.Tables[0].Rows[0]["RMANumber"].ToString();
                barcodePath = barcodePath + data + ".Jpeg";

                if (!ZNodeStorageManager.Exists(barcodePath) && data.Length>0)
                {
                    barcodePath = this.GenerateBarcode(data);
                }
            }
            objReportViewer.ProcessingMode = ProcessingMode.Local;
            this.objReportViewer.LocalReport.EnableExternalImages = true;

            this.objReportViewer.LocalReport.ReportPath = "Secure/Reports/RMAReport.rdlc";
            objReportViewer.LocalReport.SetParameters(new ReportParameter("BarcodePath", @"File:///" +Server.MapPath(barcodePath)));
            ReportParameter param1 = new ReportParameter("CurrentLanguage", System.Globalization.CultureInfo.CurrentCulture.Name);
            objReportViewer.LocalReport.SetParameters(new ReportParameter[] { param1 }); 
                
            
            objReportViewer.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", reportDataSet.Tables[0]));
            objReportViewer.LocalReport.Refresh();
        } 

        /// <summary>
        /// Send Mail Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnSendMail_Click(object sender, EventArgs e)
        {
            this.SendMail();
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Orders/ReturnsManagement/RMAManager/RMARequests.aspx?mode=edit&itemid=" + itemID + "&orderid=" + OrderID);
        }
        /// <summary>
        /// Convert Bitmap to Byte array
        /// </summary>
        /// <param name="bmp"></param>
        /// <returns></returns>
        private byte[] BmpToBytes(Bitmap bmp)
        {
            MemoryStream ms = new MemoryStream();

            bmp.Save(ms, ImageFormat.Jpeg);
            byte[] bmpBytes = ms.GetBuffer();
            bmp.Dispose();

            ms.Close();
            return bmpBytes;

        }
        /// <summary>
        /// Generate and save Barcode
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private string GenerateBarcode(string data)
        {
            Bitmap barcode = new Bitmap(1, 1);
            Font threeofnine = new Font("Free 3 of 9", 25, FontStyle.Regular, GraphicsUnit.Point);

            Graphics graphics = Graphics.FromImage(barcode);

            SizeF datasize = graphics.MeasureString(data, threeofnine);

            barcode = new Bitmap(barcode, datasize.ToSize());
            graphics = Graphics.FromImage(barcode);

            graphics.Clear(Color.White);
            graphics.TextRenderingHint = System.Drawing.Text.TextRenderingHint.SingleBitPerPixel;

            graphics.DrawString(data, threeofnine, new SolidBrush(Color.Black), 0, 0);

            graphics.Flush();
            threeofnine.Dispose();
            graphics.Dispose();
            

            string barcodePath =  Page.ResolveClientUrl(ZNodeConfigManager.EnvironmentConfig.DataPath) + "Images\\Barcode\\";

            if (!Directory.Exists(barcodePath))
                Directory.CreateDirectory(barcodePath);

            if (System.Configuration.ConfigurationManager.AppSettings["BarcodePath"] != null)
                barcodePath = System.Configuration.ConfigurationManager.AppSettings["BarcodePath"].ToString();

            barcodePath = barcodePath + data + ".Jpeg";
            if (!ZNodeStorageManager.Exists(barcodePath))
            {
                
                byte[] imageData = BmpToBytes(barcode);
                ZNodeStorageManager.WriteBinaryStorage(imageData, barcodePath);
            }

            return barcodePath;
        }
       
        /// <summary>
        /// Send mail to user with 
        /// </summary>
        protected void SendMail()
        {
            lblmsg.Text = "";
            string smtpServer = ZNodeConfigManager.SiteConfig.SMTPServer;
            string senderEmail = ZNodeConfigManager.SiteConfig.CustomerServiceEmail;
            string subject = ZNodeConfigManager.SiteConfig.StoreName;

            OrderAdmin _OrderAdmin = new OrderAdmin();
            Order orderObject = _OrderAdmin.GetOrderByOrderID(OrderID);
           
            DataSet reportDataSet = null;
            RMARequestAdmin rmarequestAdmin = new RMARequestAdmin();
            reportDataSet = rmarequestAdmin.GetRMARequestReport(itemID);

            string messageText = string.Empty;

            messageText = GenerateRMAReceipt(reportDataSet);
            if (reportDataSet.Tables[0].Rows[0]["RMADepartmentEmail"] != null && reportDataSet.Tables[0].Rows[0]["RMADepartmentEmail"].ToString() != string.Empty)
                senderEmail = reportDataSet.Tables[0].Rows[0]["RMADepartmentEmail"].ToString();

            string path= Server.MapPath(ZNodeConfigManager.EnvironmentConfig.DataPath) + "Images\\Barcode\\"  + reportDataSet.Tables[0].Rows[0]["RMANumber"].ToString()+".Jpeg";
            path = path.Replace("\\", "/");
            path = path.Replace("//", "/");
            LinkedResource barcodesource = new LinkedResource(path, MediaTypeNames.Image.Jpeg);
            barcodesource.ContentId = "barcode";
            // done HTML formatting in the next line to display my logo
            AlternateView av1 = AlternateView.CreateAlternateViewFromString(messageText, null, MediaTypeNames.Text.Html);
            av1.LinkedResources.Add(barcodesource);
            try
            {
                //string BCC = string.Empty;
                // Send mail
                ZNode.Libraries.Framework.Business.ZNodeEmail.SendEmail(orderObject.BillingEmailId, senderEmail, string.Empty, subject, av1, true);

                lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorMailsentsucessfully").ToString();
                lblmsg.ForeColor = Color.Black;
                
            }
            catch (Exception ex)
            {
               //log unable to send mail
                if (ex.InnerException != null)
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.InnerException.ToString());
                }
                else
                {
                    ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(ex.Message);
                }

                lblmsg.Text = this.GetGlobalResourceObject("ZnodeAdminResource", "ErrorUnabletosendMailCheckSettings").ToString();
                lblmsg.ForeColor = Color.Red;
            }
        }

        /// <summary>
        /// Generate the RMA receipt
        /// </summary>
        /// <param name="RMA">RMA Details</param>
        /// <returns>Returns the rma receipt</returns>
        public string GenerateRMAReceipt(DataSet rmads)
        {
            string htmlTemplatePath = string.Empty;
            string CurrentCulture = string.Empty;
            string defaultHtmlTemplatePath = string.Empty;

            // TemplatePath
            CurrentCulture = ZNodeCatalogManager.CultureInfo;

            defaultHtmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "RMAReport.htm");
     
            if (CurrentCulture != string.Empty)
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "RMAReport_" + CurrentCulture + ".htm");
            }
            else
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "RMAReport.htm");
            }

            // For Mobile Browser
            if (ZNodeCatalogManager.Theme == "Mobile" || ZNodeCatalogManager.Theme == "MobileApp" || (HttpContext.Current.Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null))
            {
                htmlTemplatePath = HttpContext.Current.Server.MapPath(ZNodeConfigManager.EnvironmentConfig.ConfigPath + "RMAReport_Mobile.htm");
            }

            // Check the receipt template exists, if not exists then set default receipt template
            FileInfo fileinfo = new FileInfo(htmlTemplatePath);

            if (!fileinfo.Exists)
            {
                htmlTemplatePath = defaultHtmlTemplatePath;
            }

            // Order DataTable
            // Create Order Table
            DataTable RMATable = new DataTable();

            RMATable.Columns.Add("StoreName", typeof(string));
            RMATable.Columns.Add("PhoneNumber", typeof(string));
            RMATable.Columns.Add("RMANumber", typeof(string));
            RMATable.Columns.Add("CustomerName",typeof(string));
            RMATable.Columns.Add("OrderID", typeof(int));
            RMATable.Columns.Add("StyleSheetPath",typeof(string));
            RMATable.Columns.Add("shippingdirections", typeof(string));
            RMATable.Columns.Add("Subtotal", typeof(string));
            RMATable.Columns.Add("Tax", typeof(string));
            RMATable.Columns.Add("RMATotal", typeof(string));
            RMATable.Columns.Add("Signature", typeof(string));
            //RMATable = rmads.Tables[0];

            // Create a NewRow.
            DataRow rmaDBRow = RMATable.NewRow();

            if (rmads.Tables[0].Rows.Count > 0)
            {
                RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
                TList<ZNode.Libraries.DataAccess.Entities.RMAConfiguration> rmaConfigs = rmaConfigAdmin.GetAllRMAConfiguration();
                ZNode.Libraries.DataAccess.Entities.RMAConfiguration rmaconfiguration = rmaConfigs[0];
                string sign = string.Empty;

                sign += "<br />";
                sign += rmads.Tables[0].Rows[0]["StoreName"].ToString() + "<br />";
                sign += rmaconfiguration.DisplayName + "<br />";
                sign += rmaconfiguration.Address + "<br />";
                sign += rmaconfiguration.Email + "<br />";


                rmaDBRow["Signature"] = sign;
                rmaDBRow["StoreName"] = rmads.Tables[0].Rows[0]["StoreName"].ToString();
                rmaDBRow["PhoneNumber"] = rmads.Tables[0].Rows[0]["PhoneNumber"].ToString();
                rmaDBRow["RMANumber"] = rmads.Tables[0].Rows[0]["RMANumber"].ToString();
                rmaDBRow["CustomerName"] = rmads.Tables[0].Rows[0]["CustomerName"].ToString();
                rmaDBRow["OrderID"] = Convert.ToInt16(rmads.Tables[0].Rows[0]["OrderID"]);
                rmaDBRow["shippingdirections"] = rmads.Tables[0].Rows[0]["shippingdirections"].ToString();
                rmaDBRow["Subtotal"] = String.Format("{0:C}", rmads.Tables[0].Rows[0]["subtotal"]);
                rmaDBRow["Tax"] = String.Format("{0:C}", rmads.Tables[0].Rows[0]["taxcost"]);
                rmaDBRow["RMATotal"] = String.Format("{0:C}", rmads.Tables[0].Rows[0]["RMATotal"]);

            }

            // Create OrderlineItem Table
            DataTable OrderlineItemTable = new DataTable();
            OrderlineItemTable.Columns.Add("Name", typeof(string));
            OrderlineItemTable.Columns.Add("ProductNum", typeof(string));
            OrderlineItemTable.Columns.Add("Description", typeof(string));
            OrderlineItemTable.Columns.Add("SKU", typeof(string));
            OrderlineItemTable.Columns.Add("Price", typeof(string));
            OrderlineItemTable.Columns.Add("Quantity", typeof(string));
            OrderlineItemTable.Columns.Add("Total", typeof(string));

            try
            {
                string cssFilePath = string.Empty;

                // If request comes from mobile then load the theme from ZNodePortal.MobileTheme
                if (HttpContext.Current.Session["cs"] != null && ZNodeConfigManager.SiteConfig.MobileTheme != null)
                {
                    cssFilePath = System.Web.HttpContext.Current.Server.MapPath("~/Themes/" + ZNodeConfigManager.SiteConfig.MobileTheme + "/Receipt/receipt.css");
                }
                else
                {
                    cssFilePath = System.Web.HttpContext.Current.Server.MapPath("~/Themes/" + ZNode.Libraries.ECommerce.Catalog.ZNodeCatalogManager.Theme + "/Receipt/receipt.css");
                }

                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.
                using (StreamReader streamReader = new StreamReader(cssFilePath))
                {
                    rmaDBRow["StyleSheetPath"] = streamReader.ReadToEnd();
                }

                // Add rows to Order datatable.
                RMATable.Rows.Add(rmaDBRow);
            }
            catch (Exception ex)
            {
                ZNode.Libraries.ECommerce.Utilities.ZNodeLogging.LogMessage(this.GetGlobalResourceObject("ZnodeAdminResource", "ActivityLogEmailReceiptStyleSheetIssue").ToString() + ex.Message);
            }

            if (rmads.Tables[0].Rows.Count > 0)
            {
                for (int index = 0; index < rmads.Tables[0].Rows.Count; index++)
                {
                    DataRow orderlineItemDBRow = OrderlineItemTable.NewRow();

                    orderlineItemDBRow["Name"] = rmads.Tables[0].Rows[index]["Name"].ToString();
                    orderlineItemDBRow["ProductNum"] = rmads.Tables[0].Rows[index]["ProductNum"].ToString();
                    orderlineItemDBRow["Description"] = rmads.Tables[0].Rows[index]["Description"].ToString();
                    orderlineItemDBRow["SKU"] = rmads.Tables[0].Rows[index]["SKU"].ToString();
                    orderlineItemDBRow["Price"] =String.Format("{0:C}", Convert.ToDecimal(rmads.Tables[0].Rows[index]["Price"].ToString()));
                    orderlineItemDBRow["Total"] = String.Format("{0:C}", Convert.ToDecimal(rmads.Tables[0].Rows[index]["Total"].ToString()));
                    orderlineItemDBRow["Quantity"] = rmads.Tables[0].Rows[index]["Quantity"].ToString();
                    OrderlineItemTable.Rows.Add(orderlineItemDBRow);
                  
                }
                
            }
            // Html Parser
            // Parse the template
            ZNodeHtmlTemplate template = new ZNodeHtmlTemplate();

            // set the html template path
            template.Path = htmlTemplatePath;

            // Parse Order
            template.Parse(RMATable.CreateDataReader());

            // Parse OrderLineItem
            template.Parse("LineItems", OrderlineItemTable.CreateDataReader());

            // template html output
            return template.Output;
        }

        
    }
}