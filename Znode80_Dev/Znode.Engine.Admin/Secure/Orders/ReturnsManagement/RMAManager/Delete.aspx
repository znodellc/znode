﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/content.master" AutoEventWireup="true" CodeBehind="Delete.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Orders.ReturnsManagement.RMAManager.Delete" %>

<%@ Register TagPrefix="uc1" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="uxMainContent" runat="Server">
    <h5>
        <asp:Label ID="deletecoupon" runat="server"><asp:Localize ID="TitleDeleteRMARequest" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleDeleteRMARequest %>'></asp:Localize> <b><%=RequestNo%></b></asp:Label></h5>
    <div>
        <uc1:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <p>
        <asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, TitleDeleteRMARequest %>'></asp:Localize>
    </p>
    <p>
        <asp:Label ID="lblErrorMsg" runat="server" Visible="False" CssClass="Error"></asp:Label>
    </p>
    <div>
        <uc1:Spacer ID="Spacer1" SpacerHeight="10" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>
    <div> 

          <zn:Button ID="btnDelete" runat="server" ButtonType="CancelButton"  CausesValidation="false" OnClick="BtnDelete_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
          <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton" CausesValidation="false" OnClick="BtnCancel_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonCancel %>" />
         

    </div>
    <div>
        <uc1:Spacer ID="Spacer8" SpacerHeight="15" SpacerWidth="3" runat="server"></uc1:Spacer>
    </div>

</asp:Content>
