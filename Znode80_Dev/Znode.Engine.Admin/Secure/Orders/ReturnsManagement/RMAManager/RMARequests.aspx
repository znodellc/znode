﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Themes/Standard/content.master"
    AutoEventWireup="true" CodeBehind="RMARequests.aspx.cs" Inherits="Znode.Engine.Admin.Secure.Orders.ReturnsManagement.RMAManager.RMARequests" %>

<%@ Register TagPrefix="ZNode" TagName="Spacer" Src="~/Controls/Default/spacer.ascx" %>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="uxMainContent">
    <div align="center">
        <div>
            <div class="LeftFloat" style="width: auto; text-align: left">
                <h1>
                    <asp:Label ID="lblRMARequestHeader" runat="server" Text="" /></h1>
            </div>
            <asp:HiddenField ID="hdnreturned" runat="server" Value="false" />
            <div class="ClearBoth">
            </div>
            <div class="LeftFloat" style="width: 50%; text-align: left">
                <div class="ViewForm100">
                    <div class="FieldStyle">
                        <asp:Localize ID="ColumnOrderID" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnOrderID %>'></asp:Localize> 
                        <asp:Label ID="lblOrderID" runat="server" />
                    </div>
                </div>
            </div>
            <div style="float: right;" class="ViewForm100">
                <div class="FieldStyle">
                    <asp:Localize ID="ColumnTitleDate" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleDateRMA %>'></asp:Localize>
                    <asp:Label ID="lblRequestDate" runat="server" />
                </div>
            </div>
            <div class="ClearBoth">
            </div>
            <div align="left">
                <h4 class="GridTitle"> <asp:Localize ID="OrderLineitems" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridTitlesOrderLineitems %>'></asp:Localize></h4>
                <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:GridView ID="uxGrid" ShowFooter="true" ShowHeader="true" CaptionAlign="Left"
                            runat="server" ForeColor="Black" CellPadding="4" AutoGenerateColumns="False"
                            CssClass="Grid" Width="100%" GridLines="None" EmptyDataText='<%$ Resources:ZnodeAdminResource, GridOrderlineEmptyData %>'
                            AllowPaging="True" OnRowCreated="UxGrid_RowCreated" OnRowDataBound="UxGrid_OnRowDataBound" PageSize="10" OnPageIndexChanging="UxGrid_PageIndexChanging">
                            <Columns>
                                <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkItem" AutoPostBack="True" OnCheckedChanged="chkItem_CheckedChanged" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleIsValidReturn %>'  HeaderStyle-HorizontalAlign="Left" Visible="false">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkReturnItem" AutoPostBack="True" OnCheckedChanged="chkItem_CheckedChanged" Checked='<%#  !EnableCheck(Eval("ISReturnable"))  %>' Enabled='<%# EnableCheck(Eval("ISReturnable")) %>' runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="OrderLineItemID" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleLineItemID %>' HeaderStyle-HorizontalAlign="Left"
                                    HeaderStyle-Width="100px" />
                                <asp:BoundField DataField="Name" HtmlEncode="false" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProductName %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="ProductNum" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnProductCode %>' HeaderStyle-HorizontalAlign="Left" Visible="false" />
                                <asp:TemplateField HeaderText="Description" HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# DataBinder.Eval(Container.DataItem, "Description") %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="SKU" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleProductsSKU %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="RMAQuantity" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleQuantity %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleQuantity %>' HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="uxQty" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Quantity_SelectedIndexChanged"
                                            CssClass="Quantity">
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleReasonforReturn %>' HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%# GetReasonName(Eval("ReasonForReturnID"))%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleReasonforReturn %>' HeaderStyle-HorizontalAlign="Left" ItemStyle-Wrap="false">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="uxReason" runat="server">
                                        </asp:DropDownList>

                                    </ItemTemplate>
                                    <HeaderStyle HorizontalAlign="Left"></HeaderStyle>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitlePrice %>' HeaderStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <%#GetPrice(Eval("price"), Eval("DiscountAmount"))%>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleTotal %>' HeaderStyle-HorizontalAlign="Left" meta:resourcekey="TemplateFieldResource6">
                                    <ItemTemplate>
                                      <%#GetTotal(Eval("price"), Eval("DiscountAmount"), Eval("RMAQuantity"))%>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Right" />
                                    <HeaderStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="MaxQuantity" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleMaxQuantity %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="RMAMaxQuantity" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleRMAMaxQuantity %>' HeaderStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="SalesTax" HeaderText='<%$ Resources:ZnodeAdminResource, ColumnTitleTax %>' HeaderStyle-HorizontalAlign="Left" />

                            </Columns>
                            <EmptyDataTemplate>
                                 <asp:Localize ID="GridOrderlineEmptyData" runat="server" Text='<%$ Resources:ZnodeAdminResource, GridOrderlineEmptyData %>'></asp:Localize>
                              
                            </EmptyDataTemplate>
                            <RowStyle CssClass="RowStyle" />
                            <HeaderStyle CssClass="HeaderStyle" />
                            <AlternatingRowStyle CssClass="AlternatingRowStyle" />
                            <FooterStyle CssClass="FooterStyle" />
                            <PagerStyle CssClass="PagerStyle" />
                        </asp:GridView>

                        <asp:Label ID="lblErrorMsg" CssClass="Error" Text="" runat="server"></asp:Label>
                        <div>
                            <ZNode:Spacer ID="Spacer2" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>
                        <div>
                            <div>
                                
                                 <zn:Button ID="btnDeselectAll" runat="server" ButtonType="EditButton"  CausesValidation="false" OnClick="btnDeselectAll_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonDeselectAll %>" />
                                 <zn:Button ID="btnSelectAll" runat="server" ButtonType="EditButton" CausesValidation="false" OnClick="btnSelectAll_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSelectAll %>" />
         

                            </div>
                        </div>
                        <div class="FieldStyleRMATotal">
                            <div class="ViewForm100">
                                <div class="FieldStyleRMA"> <asp:Localize ID="SubTotal" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleSubTotal %>'></asp:Localize></div>
                                <div class="ValueStyleRMA">
                                    <asp:Label ID="lblSubTotal" Text="$0.00" runat="server"></asp:Label></div>
                                <div></div>
                                <div class="FieldStyleRMA"><asp:Localize ID="Localize1" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTax %>'></asp:Localize> </div>
                                <div class="ValueStyleRMA">
                                    <asp:Label ID="lblTax" Text="$0.00" runat="server"></asp:Label></div>
                                <div></div>
                                <div class="FieldStyleRMA"><asp:Localize ID="ColumnTitleTotalSmall" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleTotal %>'></asp:Localize> </div>
                                <div class="ValueStyleRMA">
                                    <asp:Label ID="lblTotal" Text="$0.00" runat="server"></asp:Label></div>
                                <div></div>
                            </div>
                        </div>
                        <div>
                            <ZNode:Spacer ID="Spacer10" SpacerHeight="50" SpacerWidth="3" runat="server"></ZNode:Spacer>
                        </div>

                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <div class="ClearBoth">
            </div>
            <div class="ViewForm100">
                <div class="FieldGiftCardPanel">
                    <asp:Panel ID="pnlGiftcard" runat="server" GroupingText="Gift Card" Visible="false">
                        <asp:Repeater ID="rptGiftCard" runat="server">
                            <ItemTemplate>
                                <div style="text-align: left">
                                    <asp:Label ID="lblGiftcard" CssClass="ValueStyle" runat="Server" Text='<%# FormatGiftCard(Eval("CardNumber"),Eval("Amount"),Eval("ExpirationDate")) %>' />
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </asp:Panel>
                </div>
            </div>
            <div class="FieldStyleRMARight">
                 <asp:Localize ID="ColumnTitleComments" runat="server" Text='<%$ Resources:ZnodeAdminResource, ColumnTitleComments %>'></asp:Localize><br />
                <script type="text/javascript">
                    function textboxMultilineMaxNumber(txt, maxLen) {
                        try {
                            if (txt.value.length > (maxLen - 1))
                                return false;
                        } catch (e) {
                        }
                    }
                </script>
                <asp:TextBox ID="txtComments" runat="server" Width="320px" Rows="5" TextMode="MultiLine"></asp:TextBox>
                <asp:HiddenField ID="hdnpaymentid" runat="server" Value="" />
            </div>
            <div class="ClearBoth">
            </div>
            <div>
                <ZNode:Spacer ID="Spacer100" SpacerHeight="10" SpacerWidth="3" runat="server"></ZNode:Spacer>
            </div>

            <div  style="float: right;"> 

               <zn:Button ID="btnCancel" runat="server" ButtonType="CancelButton"  CausesValidation="true" OnClick="btnCancel_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonCancel %>" />
               <zn:Button ID="btnSubmit" runat="server" ButtonType="SubmitButton" CausesValidation="true" OnClick="btnSubmit_Click" Text="<%$ Resources:ZnodeAdminResource, ButtonSubmit %>" />
         

                <zn:Button ID="btnDenyRMA" runat="server" ButtonType="EditButton"  CausesValidation="true" OnClick="btnDenyRMA_Click" Visible="false" Text="<%$ Resources:ZnodeAdminResource,  ButtonDenyRMA %>" />
               
                <zn:Button ID="btnAuthorizeRMA" Width="125px" runat="server" ButtonType="EditButton"  CausesValidation="true" OnClick="btnAuthorizeRMA_Click" Visible="false" Text="<%$ Resources:ZnodeAdminResource,  ButtonAuthorizeRMA %>" />
             
                 <zn:Button ID="btnVoid" runat="server" ButtonType="EditButton"  CausesValidation="true" OnClick="btnVoid_Click" Visible="false"  Text="<%$ Resources:ZnodeAdminResource,  ButtonVoid %>" Width="100px" />
             
                <zn:Button ID="btnRefund" runat="server" ButtonType="EditButton"  CausesValidation="true" OnClick="btnRefund_Click" Visible="false" Text="<%$ Resources:ZnodeAdminResource,  ButtonRefund %>" />
             
                <zn:Button ID="btnIssueGC" Width="125px" runat="server" ButtonType="EditButton"  CausesValidation="true" OnClick="btnIssueGC_Click" Visible="false" Text="<%$ Resources:ZnodeAdminResource,  ButtonIssueGiftCard %>" />
              
            </div>
            <div class="ClearBoth">
            </div>

        </div>
    </div>
    <!-- Modal Dialogs -->
    <zn:Button ID="btnGetStartedDialog" runat="server" Style="display: none" />
    <ajaxToolKit:ModalPopupExtender ID="GetAuthorizeDialog" runat="server" TargetControlID="btnGetStartedDialog"
        PopupControlID="PopupAuthorizePanel" BackgroundCssClass="modalBackground" />
    <asp:Panel ID="PopupAuthorizePanel" runat="server" CssClass="PopupAuthorize" Style="display: none;"
        GroupingText='<%$ Resources:ZnodeAdminResource, TabTitleRMAReport %>'>
        <asp:UpdatePanel ID="UpdatePanelAuthorize" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <div style="text-align: center">
                    <asp:CheckBox ID="chkEmail" runat="server" AutoPostBack="true" Text='<%$ Resources:ZnodeAdminResource, CheckBoxEMailRMAReport %>'
                        OnCheckedChanged="chk_CheckedChanged" /><br />
                    <asp:CheckBox ID="chkPrint" runat="server" AutoPostBack="true" Text='<%$ Resources:ZnodeAdminResource, CheckBoxPrintRMAReport %>'
                        OnCheckedChanged="chk_CheckedChanged" />
                    <br />
                    <br />
                    <div> 
                         <zn:Button ID="btnOk" runat="server" ButtonType="EditButton"  CausesValidation="false" OnClick="btnOk_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonOk %>" Width="100px" />
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <ajaxToolKit:ModalPopupExtender ID="GetGiftCardDialog" runat="server" TargetControlID="btnGetStartedDialog"
        PopupControlID="pnlGC" BackgroundCssClass="modalBackground" />
    <asp:Panel ID="pnlGC" runat="server" CssClass="PopupConfirmation"  Style="display: none;"
        GroupingText="Gift Card">
        <div>
            <br />
        </div>
        <div>
            <asp:Localize ID="GridOrderlineEmptyData" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextGiftcardissued %>'></asp:Localize><asp:Label ID="lblCardNo" runat="server"></asp:Label>
            <asp:Localize ID="TextFor" runat="server" Text='<%$ Resources:ZnodeAdminResource, TextFor %>'></asp:Localize>
            <asp:Label ID="lblGiftAmount" runat="server"></asp:Label>.
            <asp:HiddenField ID="hdnExpiry" runat="server" Value="" />
            <asp:Localize ID="EmailCustomer" runat="server" Text='<%$ Resources:ZnodeAdminResource, ConfirmEmailCustomer %>'></asp:Localize>
        </div>
        <br />
        <div> 
             <zn:Button ID="btnGCYes" runat="server" ButtonType="EditButton"  CausesValidation="false" OnClick="btnGCYes_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonYes %>" />
               
             <zn:Button ID="btnGCNo" runat="server" ButtonType="EditButton"  CausesValidation="false" OnClick="btnGCNo_Click" Text="<%$ Resources:ZnodeAdminResource,  ButtonNo %>" />
              
        </div>
    </asp:Panel>
</asp:Content>
