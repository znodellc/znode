﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZNode.Libraries.Admin;
using ZNode.Libraries.DataAccess.Entities;
using System.Data;

namespace  Znode.Engine.Admin.Secure.Orders.ReturnsManagement.RMAConfiguration
{
    public partial class Default : System.Web.UI.Page
    {
        #region Page Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                //this.txtGCNotification.Attributes.Add("onKeyPress", "return textboxMultilineMaxNumber(this,500);");

                this.Bind();
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Bind Page Data 
        /// </summary>
        private void Bind()
        {
            this.BindViewdata();
            this.BindReasonForReturn();
            this.BindRequestStatus();

        }

        /// <summary>
        /// Bind General tab view Data 
        /// </summary>
        private void BindViewdata()
        {
            RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.RMAConfiguration> rmaConfigList = rmaConfigAdmin.GetAllRMAConfiguration();

            if (rmaConfigList.Count > 0)
            {
                ZNode.Libraries.DataAccess.Entities.RMAConfiguration rmaConfig = rmaConfigList[0];

                if(rmaConfig.MaxDays.HasValue)
                txtMaxNoOfDays.Text = rmaConfig.MaxDays.Value.ToString();
                txtDisplayName.Text = Server.HtmlDecode(rmaConfig.DisplayName);
                txtdepartmentemail.Text = rmaConfig.Email;
                txtAddress.Text = Server.HtmlDecode(rmaConfig.Address);
                txtShippingDirection.Text = Server.HtmlDecode(rmaConfig.ShippingDirections);
                if (rmaConfig.EnableEmailNotification.HasValue && rmaConfig.EnableEmailNotification.Value)
                    ddlEnable.SelectedValue = "1";
                else
                    ddlEnable.SelectedValue = "0";

                if (rmaConfig.GCExpirationPeriod.HasValue)
                    txtGCExpiration.Text = rmaConfig.GCExpirationPeriod.Value.ToString();
                txtGCNotification.Text = Server.HtmlDecode(rmaConfig.GCNotification);
            }
        }


        /// <summary>
        /// Bind Reason for return Data 
        /// </summary>
        private void BindReasonForReturn()
        {
            RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
            gvReason.DataSource = rmaConfigAdmin.GetAllReasonForReturn();
            gvReason.DataBind();
        }

        /// <summary>
        /// Bind Request status Data 
        /// </summary>
        private void BindRequestStatus()
        {
            RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
            gvRequeststatus.DataSource = rmaConfigAdmin.GetAllRequestStatus();
            gvRequeststatus.DataBind();
        }

        private void DisableReasonForReturn()
        {
            txtName.Enabled = false;
            ddlReasonenabled.Enabled = false;
            btnSaveReason.Visible = false;
            btnCancelReason.Visible = false;
            btnReasonOk.Visible = true;

        }
        private void EnableReasonForReturn()
        {
            txtName.Enabled = true;
            ddlReasonenabled.Enabled = true;
            btnSaveReason.Visible = true;
            btnCancelReason.Visible = true;
            btnReasonOk.Visible = false;
        }

        private void BindReasonForReturnData(int ReasonForReturnID)
        {
            RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
            ReasonForReturn reasonForReturn = rmaConfigAdmin.GetByReasonFoReturnID(ReasonForReturnID);

            if (reasonForReturn != null)
            {
                txtName.Text = reasonForReturn.Name;
                if (reasonForReturn.IsEnabled)
                    ddlReasonenabled.SelectedValue = "1";
                else
                    ddlReasonenabled.SelectedValue = "0";

            }
        }
        private void DisableRequestStatus()
        {
            txtStatusName.Enabled = false;
            txtCustomerNotification.Enabled = false;
            txtAdminNotification.Enabled = false;
            btnSaveStatus.Visible = false;
            btnCancelStatus.Visible = false;
            btnRequestOK.Visible = true;
        }
        private void EnableRequestStatus()
        {
            txtCustomerNotification.Enabled = true;
            txtAdminNotification.Enabled = true;
            btnSaveStatus.Visible = true;
            btnCancelStatus.Visible = true;
            btnRequestOK.Visible = false;
        }
        private void BindRequestStatusData(int RequestStatusID)
        {
            RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
            RequestStatus requestStatus = rmaConfigAdmin.GetByRequestStatusID(RequestStatusID);

            if (requestStatus != null)
            {
                txtStatusName.Text = Server.HtmlDecode(requestStatus.Name);
                txtCustomerNotification.Text = Server.HtmlDecode(requestStatus.CustomerNotification);
                txtAdminNotification.Text = Server.HtmlDecode(requestStatus.AdminNotification);

            }
        }
        /// <summary>
        /// Hide the delete button
        /// </summary>
        /// <param name="DomainName">Reason ID</param>
        /// <returns>Hides the delete button</returns>
        protected bool HideDeleteButton(string ReasonId)
        {
            RMARequestAdmin rmaRequestAdmin = new RMARequestAdmin();
            DataSet ds= rmaRequestAdmin.GetAllRMARequestByReasonID(Convert.ToInt32(ReasonId));

            if (ds.Tables[0].Rows.Count > 0)
                return false;
            else
                return true;
        }

        #endregion



        #region Button Click Events
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            ZNode.Libraries.DataAccess.Entities.RMAConfiguration rmaConfig = new ZNode.Libraries.DataAccess.Entities.RMAConfiguration();

            RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.RMAConfiguration> rmaConfigList = rmaConfigAdmin.GetAllRMAConfiguration();

            bool status = false;
            if (rmaConfigList.Count > 0)
            {
                rmaConfig = rmaConfigList[0];
            }

            rmaConfig.MaxDays = Convert.ToInt32(txtMaxNoOfDays.Text);
            rmaConfig.DisplayName = Server.HtmlEncode(txtDisplayName.Text.Trim());
            rmaConfig.Email = txtdepartmentemail.Text.Trim();
            rmaConfig.Address = Server.HtmlEncode(txtAddress.Text.Trim());
            rmaConfig.ShippingDirections = Server.HtmlEncode(txtShippingDirection.Text.Trim());
            if (ddlEnable.SelectedValue == "1")
                rmaConfig.EnableEmailNotification = true;
            else
                rmaConfig.EnableEmailNotification = false;


            if (rmaConfig.RMAConfigID > 0)              
                status = rmaConfigAdmin.Update(rmaConfig);           
            else
                status = rmaConfigAdmin.Add(rmaConfig);

        }
        /// <summary>
        /// GC Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnGCSubmit_Click(object sender, EventArgs e)
        {
            ZNode.Libraries.DataAccess.Entities.RMAConfiguration rmaConfig = new ZNode.Libraries.DataAccess.Entities.RMAConfiguration();

            RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();
            TList<ZNode.Libraries.DataAccess.Entities.RMAConfiguration> rmaConfigList = rmaConfigAdmin.GetAllRMAConfiguration();

            bool status = false;
            if (rmaConfigList.Count > 0)
            {
                rmaConfig = rmaConfigList[0];
            }
            rmaConfig.GCExpirationPeriod = Convert.ToInt32(txtGCExpiration.Text);         
            rmaConfig.GCNotification = Server.HtmlEncode(txtGCNotification.Text.Trim());
          

            if (rmaConfig.RMAConfigID > 0)          
                status = rmaConfigAdmin.Update(rmaConfig);          
            else
                status = rmaConfigAdmin.Add(rmaConfig);

        }
        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnGCCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Orders/Default.aspx");
        }


        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Secure/Orders/Default.aspx");
        }
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnSaveReason_Click(object sender, EventArgs e)
        {
            RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();

            ReasonForReturn reasonForReturn = new ReasonForReturn();
            reasonForReturn.ReasonForReturnID = Convert.ToInt32(hdnReasonID.Value);
            reasonForReturn.Name = txtName.Text;
            if (ddlReasonenabled.SelectedValue == "1")
                reasonForReturn.IsEnabled = true;
            else
                reasonForReturn.IsEnabled = false;

            if (reasonForReturn.ReasonForReturnID > 0)
                rmaConfigAdmin.Update(reasonForReturn);
            else
                rmaConfigAdmin.Add(reasonForReturn);  
           
            BindReasonForReturn();
            Session["ReasonList"] = null;
        }

        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnCancelReason_Click(object sender, EventArgs e)
        { 
            pnlDiplayReasonGrid.Visible = true;
        }
        /// <summary>
        /// Ok Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnReasonOk_Click(object sender, EventArgs e)
        { 
            pnlDiplayReasonGrid.Visible = true;
        }
        /// <summary>
        /// Submit Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnSaveStatus_Click(object sender, EventArgs e)
        {
            RMAConfigurationAdmin rmaConfigAdmin = new RMAConfigurationAdmin();

            RequestStatus requestStatus = rmaConfigAdmin.GetByRequestStatusID(Convert.ToInt32(hdnRequestID.Value));
            bool status = false;
            if (requestStatus != null)
            {
                requestStatus.CustomerNotification = Server.HtmlEncode(txtCustomerNotification.Text);
                requestStatus.AdminNotification = Server.HtmlEncode(txtAdminNotification.Text);

                status = rmaConfigAdmin.Update(requestStatus);
            } 
            pnlRequeststatus.Visible = true;
        }


        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnCancelStatus_Click(object sender, EventArgs e)
        { 
            pnlRequeststatus.Visible = true;
        }
        /// <summary>
        /// Cancel Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void btnRequestOK_Click(object sender, EventArgs e)
        { 
            pnlRequeststatus.Visible = true;
        }


        /// <summary>
        /// Create Reason for Return Button Click Event
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void lbCreateReason_Click(object sender, EventArgs e)
        {  
            pnlCreateReason.Visible = true;
            mdlPopup.Show(); 
            EnableReasonForReturn();
            txtName.Text = "";
            hdnReasonID.Value = "0";
          
        }
        #endregion



        #region Gridview Events
        /// <summary>
        /// Reason for Return Row command
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void gvReason_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "EditReason")
            {
                hdnReasonID.Value = e.CommandArgument.ToString(); 
                pnlCreateReason.Visible = true;
                mdlPopup.Show(); 
                this.BindReasonForReturnData(int.Parse(e.CommandArgument.ToString()));
                this.EnableReasonForReturn();
              
            }
            else if (e.CommandName == "RemoveItem")
            {
                RMAConfigurationAdmin rmaConfigurationAdmin = new RMAConfigurationAdmin();

                bool Status = rmaConfigurationAdmin.DeleteReasonForReturn(int.Parse(e.CommandArgument.ToString()));

                this.BindReasonForReturn();
            }
            
        }

        /// <summary>
        /// Request Status Row
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void gvRequeststatus_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }

        /// <summary>
        /// Request Status Row command
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void gvRequeststatus_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "EditStatus")
            {
                hdnRequestID.Value = e.CommandArgument.ToString(); 
                mdlPopupReqStatus.Show();
                this.EnableRequestStatus();
                this.BindRequestStatusData(int.Parse(e.CommandArgument.ToString()));
            }
            else if (e.CommandName == "DeleteStatus")
            {
                RMAConfigurationAdmin rmaConfigurationAdmin = new RMAConfigurationAdmin();

                bool Status = rmaConfigurationAdmin.DeleteRequestStatus(int.Parse(e.CommandArgument.ToString()));

                this.BindRequestStatus();
            } 
        }

        /// <summary>
        /// Reason for Return Row
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void gvReason_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }

        /// <summary>
        /// Reason for Return next page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void gvReason_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvReason.PageIndex = e.NewPageIndex;
            this.BindReasonForReturn();
        }

        /// <summary>
        /// Request Status  next page
        /// </summary>
        /// <param name="sender">Sender object that raised the event.</param>
        /// <param name="e">Event Argument of the object that contains event data.</param>
        protected void gvRequeststatus_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvRequeststatus.PageIndex = e.NewPageIndex;
            this.BindRequestStatus();
        }

        #endregion


    }
}